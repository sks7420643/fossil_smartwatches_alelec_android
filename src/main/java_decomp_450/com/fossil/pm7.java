package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm7 {
    @DexIgnore
    public static /* final */ lm7 a; // = new lm7("ZERO");
    @DexIgnore
    public static /* final */ kd7<Object, ib7.b, Object> b; // = a.INSTANCE;
    @DexIgnore
    public static /* final */ kd7<el7<?>, ib7.b, el7<?>> c; // = b.INSTANCE;
    @DexIgnore
    public static /* final */ kd7<sm7, ib7.b, sm7> d; // = d.INSTANCE;
    @DexIgnore
    public static /* final */ kd7<sm7, ib7.b, sm7> e; // = c.INSTANCE;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements kd7<Object, ib7.b, Object> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(2);
        }

        @DexIgnore
        public final Object invoke(Object obj, ib7.b bVar) {
            if (!(bVar instanceof el7)) {
                return obj;
            }
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            int intValue = num != null ? num.intValue() : 1;
            return intValue == 0 ? bVar : Integer.valueOf(intValue + 1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements kd7<el7<?>, ib7.b, el7<?>> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final el7<?> invoke(el7<?> el7, ib7.b bVar) {
            if (el7 != null) {
                return el7;
            }
            if (!(bVar instanceof el7)) {
                bVar = null;
            }
            return (el7) bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements kd7<sm7, ib7.b, sm7> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(2);
        }

        @DexIgnore
        public final sm7 invoke(sm7 sm7, ib7.b bVar) {
            if (bVar instanceof el7) {
                ((el7) bVar).a(sm7.a(), sm7.c());
            }
            return sm7;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements kd7<sm7, ib7.b, sm7> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(2);
        }

        @DexIgnore
        public final sm7 invoke(sm7 sm7, ib7.b bVar) {
            if (bVar instanceof el7) {
                sm7.a(((el7) bVar).a(sm7.a()));
            }
            return sm7;
        }
    }

    @DexIgnore
    public static final Object a(ib7 ib7) {
        Object fold = ib7.fold(0, b);
        if (fold != null) {
            return fold;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public static final Object b(ib7 ib7, Object obj) {
        if (obj == null) {
            obj = a(ib7);
        }
        if (obj == 0) {
            return a;
        }
        if (obj instanceof Integer) {
            return ib7.fold(new sm7(ib7, ((Number) obj).intValue()), d);
        }
        if (obj != null) {
            return ((el7) obj).a(ib7);
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }

    @DexIgnore
    public static final void a(ib7 ib7, Object obj) {
        if (obj != a) {
            if (obj instanceof sm7) {
                ((sm7) obj).b();
                ib7.fold(obj, e);
                return;
            }
            Object fold = ib7.fold(null, c);
            if (fold != null) {
                ((el7) fold).a(ib7, obj);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        }
    }
}
