package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p01 extends s91 {
    @DexIgnore
    public /* final */ UUID[] b;
    @DexIgnore
    public /* final */ qk1[] c;

    @DexIgnore
    public p01(x71 x71, UUID[] uuidArr, qk1[] qk1Arr) {
        super(x71);
        this.b = uuidArr;
        this.c = qk1Arr;
    }
}
