package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h15 extends g15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K;
    @DexIgnore
    public long I;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        K = sparseIntArray;
        sparseIntArray.put(2131362093, 1);
        K.put(2131362638, 2);
        K.put(2131363030, 3);
        K.put(2131362517, 4);
        K.put(2131362377, 5);
        K.put(2131362608, 6);
        K.put(2131362223, 7);
        K.put(2131362203, 8);
        K.put(2131362501, 9);
        K.put(2131362265, 10);
        K.put(2131362094, 11);
        K.put(2131362639, 12);
        K.put(2131363342, 13);
        K.put(2131362647, 14);
        K.put(2131362497, 15);
        K.put(2131362467, 16);
        K.put(2131362258, 17);
    }
    */

    @DexIgnore
    public h15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 18, J, K));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public h15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[11], (View) objArr[8], (FlexibleTextInputEditText) objArr[7], (FlexibleButton) objArr[17], (FlexibleButton) objArr[10], (FlexibleTextView) objArr[5], (FlexibleButton) objArr[16], (FlexibleTextView) objArr[15], (FlexibleButton) objArr[9], (FlexibleTextView) objArr[4], (FlexibleTextInputLayout) objArr[6], (RTLImageView) objArr[2], (RTLImageView) objArr[12], (RTLImageView) objArr[14], (FrameLayout) objArr[0], (ScrollView) objArr[3], (FlexibleTextView) objArr[13]);
        this.I = -1;
        ((g15) this).F.setTag(null);
        a(view);
        f();
    }
}
