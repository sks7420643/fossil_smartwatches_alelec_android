package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class oi0 extends Enum<oi0> {
    @DexIgnore
    public static /* final */ oi0 a;
    @DexIgnore
    public static /* final */ oi0 b;
    @DexIgnore
    public static /* final */ oi0 c;
    @DexIgnore
    public static /* final */ oi0 d;
    @DexIgnore
    public static /* final */ oi0 e;
    @DexIgnore
    public static /* final */ oi0 f;
    @DexIgnore
    public static /* final */ oi0 g;
    @DexIgnore
    public static /* final */ oi0 h;
    @DexIgnore
    public static /* final */ oi0 i;
    @DexIgnore
    public static /* final */ oi0 j;
    @DexIgnore
    public static /* final */ oi0 k;
    @DexIgnore
    public static /* final */ oi0 l;
    @DexIgnore
    public static /* final */ oi0 m;
    @DexIgnore
    public static /* final */ /* synthetic */ oi0[] n;
    @DexIgnore
    public static /* final */ ls1 o; // = new ls1(null);

    /*
    static {
        oi0 oi0 = new oi0("SUCCESS", 0, 0);
        a = oi0;
        oi0 oi02 = new oi0("NOT_START", 1, 1);
        b = oi02;
        oi0 oi03 = new oi0("GATT_ERROR", 3, 7);
        c = oi03;
        oi0 oi04 = new oi0("UNEXPECTED_RESULT", 4, 8);
        d = oi04;
        oi0 oi05 = new oi0("INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT", 5, 9);
        e = oi05;
        oi0 oi06 = new oi0("BLUETOOTH_OFF", 6, 10);
        f = oi06;
        oi0 oi07 = new oi0("UNSUPPORTED", 7, 11);
        g = oi07;
        oi0 oi08 = new oi0("HID_PROXY_NOT_CONNECTED", 8, 256);
        h = oi08;
        oi0 oi09 = new oi0("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 9, 257);
        i = oi09;
        oi0 oi010 = new oi0("HID_INPUT_DEVICE_DISABLED", 10, 258);
        j = oi010;
        oi0 oi011 = new oi0("HID_UNKNOWN_ERROR", 11, 511);
        k = oi011;
        oi0 oi012 = new oi0("INTERRUPTED", 12, 254);
        l = oi012;
        oi0 oi013 = new oi0("CONNECTION_DROPPED", 13, 255);
        m = oi013;
        n = new oi0[]{oi0, oi02, new oi0("WRONG_STATE", 2, 6), oi03, oi04, oi05, oi06, oi07, oi08, oi09, oi010, oi011, oi012, oi013};
    }
    */

    @DexIgnore
    public oi0(String str, int i2, int i3) {
    }

    @DexIgnore
    public static oi0 valueOf(String str) {
        return (oi0) Enum.valueOf(oi0.class, str);
    }

    @DexIgnore
    public static oi0[] values() {
        return (oi0[]) n.clone();
    }
}
