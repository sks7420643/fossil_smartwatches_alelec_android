package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y10 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements y10 {
        @DexIgnore
        public /* final */ px a;
        @DexIgnore
        public /* final */ az b;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> c;

        @DexIgnore
        public a(InputStream inputStream, List<ImageHeaderParser> list, az azVar) {
            u50.a(azVar);
            this.b = azVar;
            u50.a((Object) list);
            this.c = list;
            this.a = new px(inputStream, azVar);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeStream(this.a.b(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public void b() {
            this.a.c();
        }

        @DexIgnore
        @Override // com.fossil.y10
        public ImageHeaderParser.ImageType c() throws IOException {
            return xw.b(this.c, this.a.b(), this.b);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public int a() throws IOException {
            return xw.a(this.c, this.a.b(), this.b);
        }
    }

    @DexIgnore
    int a() throws IOException;

    @DexIgnore
    Bitmap a(BitmapFactory.Options options) throws IOException;

    @DexIgnore
    void b();

    @DexIgnore
    ImageHeaderParser.ImageType c() throws IOException;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y10 {
        @DexIgnore
        public /* final */ az a;
        @DexIgnore
        public /* final */ List<ImageHeaderParser> b;
        @DexIgnore
        public /* final */ rx c;

        @DexIgnore
        public b(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, az azVar) {
            u50.a(azVar);
            this.a = azVar;
            u50.a((Object) list);
            this.b = list;
            this.c = new rx(parcelFileDescriptor);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public Bitmap a(BitmapFactory.Options options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.c.b().getFileDescriptor(), null, options);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public void b() {
        }

        @DexIgnore
        @Override // com.fossil.y10
        public ImageHeaderParser.ImageType c() throws IOException {
            return xw.b(this.b, this.c, this.a);
        }

        @DexIgnore
        @Override // com.fossil.y10
        public int a() throws IOException {
            return xw.a(this.b, this.c, this.a);
        }
    }
}
