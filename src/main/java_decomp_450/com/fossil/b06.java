package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b06 implements Factory<a06> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<ComplicationLastSettingRepository> c;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> d;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppLastSettingRepository> f;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> g;
    @DexIgnore
    public /* final */ Provider<ch5> h;

    @DexIgnore
    public b06(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<RingStyleRepository> provider5, Provider<WatchAppLastSettingRepository> provider6, Provider<WatchFaceRepository> provider7, Provider<ch5> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static b06 a(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<RingStyleRepository> provider5, Provider<WatchAppLastSettingRepository> provider6, Provider<WatchFaceRepository> provider7, Provider<ch5> provider8) {
        return new b06(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static a06 a(DianaPresetRepository dianaPresetRepository, ComplicationRepository complicationRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppRepository watchAppRepository, RingStyleRepository ringStyleRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository, ch5 ch5) {
        return new a06(dianaPresetRepository, complicationRepository, complicationLastSettingRepository, watchAppRepository, ringStyleRepository, watchAppLastSettingRepository, watchFaceRepository, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public a06 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }
}
