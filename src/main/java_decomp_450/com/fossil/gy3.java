package com.fossil;

import com.fossil.dz3;
import com.fossil.wz3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import com.misfit.frameworks.common.constants.Constants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gy3<K, V> extends vw3<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient by3<K, ? extends vx3<V>> map;
    @DexIgnore
    public /* final */ transient int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gy3<K, V>.f {
        @DexIgnore
        public a(gy3 gy3) {
            super(gy3, null);
        }

        @DexIgnore
        @Override // com.fossil.gy3.f
        public Map.Entry<K, V> a(K k, V v) {
            return yy3.a((Object) k, (Object) v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gy3<K, V>.f {
        @DexIgnore
        public b(gy3 gy3) {
            super(gy3, null);
        }

        @DexIgnore
        @Override // com.fossil.gy3.f
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> {
        @DexIgnore
        public zy3<K, V> a;
        @DexIgnore
        public Comparator<? super K> b;
        @DexIgnore
        public Comparator<? super V> c;

        @DexIgnore
        public c() {
            this(bz3.a().a().b());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public c<K, V> a(K k, V v) {
            bx3.a(k, v);
            this.a.put(k, v);
            return this;
        }

        @DexIgnore
        public c(zy3<K, V> zy3) {
            this.a = zy3;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.gy3$c<K, V> */
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public c<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            return a(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public c<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                a(entry);
            }
            return this;
        }

        @DexIgnore
        public gy3<K, V> a() {
            if (this.c != null) {
                Iterator<Collection<V>> it = this.a.asMap().values().iterator();
                while (it.hasNext()) {
                    Collections.sort((List) it.next(), this.c);
                }
            }
            if (this.b != null) {
                ty3<K, V> b2 = bz3.a().a().b();
                for (Map.Entry entry : jz3.from(this.b).onKeys().immutableSortedCopy(this.a.asMap().entrySet())) {
                    b2.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                this.a = b2;
            }
            return gy3.copyOf(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends vx3<Map.Entry<K, V>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ gy3<K, V> multimap;

        @DexIgnore
        public d(gy3<K, V> gy3) {
            this.multimap = gy3;
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.multimap.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return this.multimap.isPartialView();
        }

        @DexIgnore
        public int size() {
            return this.multimap.size();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, java.util.Collection, java.lang.Iterable
        public j04<Map.Entry<K, V>> iterator() {
            return this.multimap.entryIterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public static /* final */ wz3.b<gy3> a; // = wz3.a(gy3.class, Constants.MAP);
        @DexIgnore
        public static /* final */ wz3.b<gy3> b; // = wz3.a(gy3.class, "size");
        @DexIgnore
        public static /* final */ wz3.b<jy3> c; // = wz3.a(jy3.class, "emptySet");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends hy3<K> {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.dz3, com.fossil.vx3, com.fossil.hy3
        public boolean contains(Object obj) {
            return gy3.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.dz3
        public int count(Object obj) {
            Collection collection = (Collection) gy3.this.map.get(obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.dz3
        public Set<K> elementSet() {
            return gy3.this.keySet();
        }

        @DexIgnore
        @Override // com.fossil.hy3
        public dz3.a<K> getEntry(int i) {
            Map.Entry<K, ? extends vx3<V>> entry = gy3.this.map.entrySet().asList().get(i);
            return ez3.a(entry.getKey(), ((Collection) entry.getValue()).size());
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return gy3.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<K, V> extends vx3<V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ transient gy3<K, V> a;

        @DexIgnore
        public h(gy3<K, V> gy3) {
            this.a = gy3;
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            return this.a.containsValue(obj);
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public int copyIntoArray(Object[] objArr, int i) {
            Iterator it = this.a.map.values().iterator();
            while (it.hasNext()) {
                i = ((vx3) it.next()).copyIntoArray(objArr, i);
            }
            return i;
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, java.util.Collection, java.lang.Iterable
        public j04<V> iterator() {
            return this.a.valueIterator();
        }
    }

    @DexIgnore
    public gy3(by3<K, ? extends vx3<V>> by3, int i) {
        this.map = by3;
        this.size = i;
    }

    @DexIgnore
    public static <K, V> c<K, V> builder() {
        return new c<>();
    }

    @DexIgnore
    public static <K, V> gy3<K, V> copyOf(zy3<? extends K, ? extends V> zy3) {
        if (zy3 instanceof gy3) {
            gy3<K, V> gy3 = (gy3) zy3;
            if (!gy3.isPartialView()) {
                return gy3;
            }
        }
        return ay3.copyOf((zy3) zy3);
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of() {
        return ay3.of();
    }

    @DexIgnore
    @Override // com.fossil.zy3
    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public /* bridge */ /* synthetic */ boolean containsEntry(Object obj, Object obj2) {
        return super.containsEntry(obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public boolean containsKey(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public boolean containsValue(Object obj) {
        return obj != null && super.containsValue(obj);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Map<K, Collection<V>> createAsMap() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public abstract vx3<V> get(K k);

    @DexIgnore
    @Override // com.fossil.vw3
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    public abstract gy3<V, K> inverse();

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.map.isPartialView();
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    @CanIgnoreReturnValue
    @Deprecated
    public boolean put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    @CanIgnoreReturnValue
    @Deprecated
    public boolean remove(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public int size() {
        return this.size;
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of(K k, V v) {
        return ay3.of((Object) k, (Object) v);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.by3<K, ? extends com.fossil.vx3<V>>, com.fossil.by3<K, java.util.Collection<V>> */
    @Override // com.fossil.vw3, com.fossil.zy3
    public by3<K, Collection<V>> asMap() {
        return (by3<K, ? extends vx3<V>>) this.map;
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public vx3<Map.Entry<K, V>> createEntries() {
        return new d(this);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public hy3<K> createKeys() {
        return new g();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public vx3<V> createValues() {
        return new h(this);
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public vx3<Map.Entry<K, V>> entries() {
        return (vx3) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public j04<Map.Entry<K, V>> entryIterator() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public iy3<K> keySet() {
        return this.map.keySet();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public hy3<K> keys() {
        return (hy3) super.keys();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(zy3<? extends K, ? extends V> zy3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public vx3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    @CanIgnoreReturnValue
    @Deprecated
    public vx3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public j04<V> valueIterator() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public vx3<V> values() {
        return (vx3) super.values();
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of(K k, V v, K k2, V v2) {
        return ay3.of((Object) k, (Object) v, (Object) k2, (Object) v2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f<T> extends j04<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> a;
        @DexIgnore
        public K b;
        @DexIgnore
        public Iterator<V> c;

        @DexIgnore
        public f() {
            this.a = gy3.this.asMap().entrySet().iterator();
            this.b = null;
            this.c = qy3.a();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext() || this.c.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.c.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.a.next();
                this.b = next.getKey();
                this.c = next.getValue().iterator();
            }
            return a(this.b, this.c.next());
        }

        @DexIgnore
        public /* synthetic */ f(gy3 gy3, a aVar) {
            this();
        }
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return ay3.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3);
    }

    @DexIgnore
    public static <K, V> gy3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return ay3.copyOf((Iterable) iterable);
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return ay3.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4);
    }

    @DexIgnore
    public static <K, V> gy3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return ay3.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4, (Object) k5, (Object) v5);
    }
}
