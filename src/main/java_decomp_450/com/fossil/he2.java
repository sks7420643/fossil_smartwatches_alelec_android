package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he2 extends nk2 implements ge2 {
    @DexIgnore
    public he2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    @DexIgnore
    @Override // com.fossil.ge2
    public final void c(int i) throws RemoteException {
        Parcel E = E();
        E.writeInt(i);
        a(2, E);
    }
}
