package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rh7<T> extends pk7 implements ik7, fb7<T>, yi7 {
    @DexIgnore
    public /* final */ ib7 b;
    @DexIgnore
    public /* final */ ib7 c;

    @DexIgnore
    public rh7(ib7 ib7, boolean z) {
        super(z);
        this.c = ib7;
        this.b = ib7.plus(this);
    }

    @DexIgnore
    @Override // com.fossil.yi7
    public ib7 a() {
        return this.b;
    }

    @DexIgnore
    public void a(Throwable th, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public String c() {
        return ej7.a((Object) this) + " was cancelled";
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public final void f(Throwable th) {
        vi7.a(this.b, th);
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public final ib7 getContext() {
        return this.b;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.pk7
    public final void i(Object obj) {
        if (obj instanceof li7) {
            li7 li7 = (li7) obj;
            a(li7.a, li7.a());
            return;
        }
        m(obj);
    }

    @DexIgnore
    @Override // com.fossil.ik7, com.fossil.pk7
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public String k() {
        String a = si7.a(this.b);
        if (a == null) {
            return super.k();
        }
        return '\"' + a + "\":" + super.k();
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public final void l() {
        o();
    }

    @DexIgnore
    public void m(T t) {
    }

    @DexIgnore
    public final void n() {
        a((ik7) this.c.get(ik7.o));
    }

    @DexIgnore
    public void o() {
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public final void resumeWith(Object obj) {
        Object h = h(mi7.a(obj));
        if (h != qk7.b) {
            l(h);
        }
    }

    @DexIgnore
    public final <R> void a(bj7 bj7, R r, kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7) {
        n();
        bj7.invoke(kd7, r, this);
    }

    @DexIgnore
    public void l(Object obj) {
        a(obj);
    }
}
