package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm4 {
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("variantKey")
    public String b;

    @DexIgnore
    public nm4(String str, String str2) {
        ee7.b(str, "id");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof nm4)) {
            return false;
        }
        nm4 nm4 = (nm4) obj;
        return ee7.a(this.a, nm4.a) && ee7.a(this.b, nm4.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "Flag(id=" + this.a + ", variantKey=" + this.b + ")";
    }
}
