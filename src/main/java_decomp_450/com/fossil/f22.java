package com.fossil;

import android.os.RemoteException;
import com.fossil.v02;
import com.fossil.v02.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f22<A extends v02.b, ResultT> {
    @DexIgnore
    public /* final */ k02[] a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, oo3<ResultT> oo3) throws RemoteException;

    @DexIgnore
    public boolean a() {
        return this.b;
    }

    @DexIgnore
    public final k02[] b() {
        return this.a;
    }
}
