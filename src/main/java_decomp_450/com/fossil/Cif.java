package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ig;
import com.fossil.ng;
import com.fossil.qf;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* renamed from: com.fossil.if  reason: invalid class name */
public class Cif<T> {
    @DexIgnore
    public /* final */ xg a;
    @DexIgnore
    public /* final */ ig<T> b;
    @DexIgnore
    public Executor c; // = o3.d();
    @DexIgnore
    public /* final */ List<c<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public qf<T> f;
    @DexIgnore
    public qf<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public qf.e i; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.if$a")
    /* renamed from: com.fossil.if$a */
    public class a extends qf.e {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.qf.e
        public void a(int i, int i2) {
            Cif.this.a.a(i, i2, null);
        }

        @DexIgnore
        @Override // com.fossil.qf.e
        public void b(int i, int i2) {
            Cif.this.a.b(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.qf.e
        public void c(int i, int i2) {
            Cif.this.a.c(i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.if$b")
    /* renamed from: com.fossil.if$b */
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ qf a;
        @DexIgnore
        public /* final */ /* synthetic */ qf b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ qf d;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.if$b$a")
        /* renamed from: com.fossil.if$b$a */
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ ng.c a;

            @DexIgnore
            public a(ng.c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void run() {
                b bVar = b.this;
                Cif ifVar = Cif.this;
                if (ifVar.h == bVar.c) {
                    ifVar.a(bVar.d, bVar.b, this.a, bVar.a.f, bVar.e);
                }
            }
        }

        @DexIgnore
        public b(qf qfVar, qf qfVar2, int i, qf qfVar3, Runnable runnable) {
            this.a = qfVar;
            this.b = qfVar2;
            this.c = i;
            this.d = qfVar3;
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            Cif.this.c.execute(new a(tf.a(this.a.e, this.b.e, Cif.this.b.b())));
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.if$c */
    public interface c<T> {
        @DexIgnore
        void a(qf<T> qfVar, qf<T> qfVar2);
    }

    @DexIgnore
    public Cif(RecyclerView.g gVar, ng.d<T> dVar) {
        this.a = new hg(gVar);
        this.b = new ig.a(dVar).a();
    }

    @DexIgnore
    public T a(int i2) {
        qf<T> qfVar = this.f;
        if (qfVar == null) {
            qf<T> qfVar2 = this.g;
            if (qfVar2 != null) {
                return qfVar2.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        qfVar.c(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int a() {
        qf<T> qfVar = this.f;
        if (qfVar != null) {
            return qfVar.size();
        }
        qf<T> qfVar2 = this.g;
        if (qfVar2 == null) {
            return 0;
        }
        return qfVar2.size();
    }

    @DexIgnore
    public void a(qf<T> qfVar) {
        a(qfVar, null);
    }

    @DexIgnore
    public void a(qf<T> qfVar, Runnable runnable) {
        if (qfVar != null) {
            if (this.f == null && this.g == null) {
                this.e = qfVar.g();
            } else if (qfVar.g() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        qf<T> qfVar2 = this.f;
        if (qfVar != qfVar2) {
            qf<T> qfVar3 = this.g;
            if (qfVar3 != null) {
                qfVar2 = qfVar3;
            }
            if (qfVar == null) {
                int a2 = a();
                qf<T> qfVar4 = this.f;
                if (qfVar4 != null) {
                    qfVar4.a(this.i);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.a.c(0, a2);
                a(qfVar2, null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = qfVar;
                qfVar.a((List) null, this.i);
                this.a.b(0, qfVar.size());
                a(null, qfVar, runnable);
            } else {
                qf<T> qfVar5 = this.f;
                if (qfVar5 != null) {
                    qfVar5.a(this.i);
                    this.g = (qf) this.f.j();
                    this.f = null;
                }
                qf<T> qfVar6 = this.g;
                if (qfVar6 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new b(qfVar6, (qf) qfVar.j(), i2, qfVar, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(qf<T> qfVar, qf<T> qfVar2, ng.c cVar, int i2, Runnable runnable) {
        qf<T> qfVar3 = this.g;
        if (qfVar3 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = qfVar;
        this.g = null;
        tf.a(this.a, qfVar3.e, qfVar.e, cVar);
        qfVar.a((List) qfVar2, this.i);
        if (!this.f.isEmpty()) {
            int a2 = tf.a(cVar, qfVar3.e, qfVar2.e, i2);
            qf<T> qfVar4 = this.f;
            qfVar4.c(Math.max(0, Math.min(qfVar4.size() - 1, a2)));
        }
        a(qfVar3, this.f, runnable);
    }

    @DexIgnore
    public final void a(qf<T> qfVar, qf<T> qfVar2, Runnable runnable) {
        for (c<T> cVar : this.d) {
            cVar.a(qfVar, qfVar2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(c<T> cVar) {
        this.d.add(cVar);
    }
}
