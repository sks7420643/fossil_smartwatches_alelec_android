package com.fossil;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sj4 implements Factory<rj4> {
    @DexIgnore
    public /* final */ Provider<Map<Class<? extends he>, Provider<he>>> a;

    @DexIgnore
    public sj4(Provider<Map<Class<? extends he>, Provider<he>>> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static sj4 a(Provider<Map<Class<? extends he>, Provider<he>>> provider) {
        return new sj4(provider);
    }

    @DexIgnore
    public static rj4 a(Map<Class<? extends he>, Provider<he>> map) {
        return new rj4(map);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rj4 get() {
        return a(this.a.get());
    }
}
