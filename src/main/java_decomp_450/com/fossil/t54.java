package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t54 extends v54.d.e {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.e.a {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;

        @DexIgnore
        @Override // com.fossil.v54.d.e.a
        public v54.d.e.a a(int i) {
            this.a = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.e.a
        public v54.d.e.a b(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.e.a
        public v54.d.e.a a(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.e.a
        public v54.d.e.a a(boolean z) {
            this.d = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.e.a
        public v54.d.e a() {
            String str = "";
            if (this.a == null) {
                str = str + " platform";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (this.c == null) {
                str = str + " buildVersion";
            }
            if (this.d == null) {
                str = str + " jailbroken";
            }
            if (str.isEmpty()) {
                return new t54(this.a.intValue(), this.b, this.c, this.d.booleanValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.e
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.e
    public int b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.e
    public String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.e
    public boolean d() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.e)) {
            return false;
        }
        v54.d.e eVar = (v54.d.e) obj;
        if (this.a != eVar.b() || !this.b.equals(eVar.c()) || !this.c.equals(eVar.a()) || this.d != eVar.d()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ (this.d ? 1231 : 1237);
    }

    @DexIgnore
    public String toString() {
        return "OperatingSystem{platform=" + this.a + ", version=" + this.b + ", buildVersion=" + this.c + ", jailbroken=" + this.d + "}";
    }

    @DexIgnore
    public t54(int i, String str, String str2, boolean z) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = z;
    }
}
