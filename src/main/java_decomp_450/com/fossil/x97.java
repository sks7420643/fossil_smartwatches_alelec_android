package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x97 extends w97 {
    @DexIgnore
    public static final <T> int a(Iterable<? extends T> iterable, int i) {
        ee7.b(iterable, "$this$collectionSizeOrDefault");
        return iterable instanceof Collection ? ((Collection) iterable).size() : i;
    }

    @DexIgnore
    public static final <T> boolean b(Collection<? extends T> collection) {
        return collection.size() > 2 && (collection instanceof ArrayList);
    }

    @DexIgnore
    public static final <T> Collection<T> a(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        ee7.b(iterable, "$this$convertToSetForSetOperationWith");
        ee7.b(iterable2, "source");
        if (iterable instanceof Set) {
            return (Collection) iterable;
        }
        if (!(iterable instanceof Collection)) {
            return ea7.m(iterable);
        }
        if ((iterable2 instanceof Collection) && ((Collection) iterable2).size() < 2) {
            return (Collection) iterable;
        }
        Collection<T> collection = (Collection) iterable;
        if (b(collection)) {
            return ea7.m(iterable);
        }
        return collection;
    }

    @DexIgnore
    public static final <T> List<T> a(Iterable<? extends Iterable<? extends T>> iterable) {
        ee7.b(iterable, "$this$flatten");
        ArrayList arrayList = new ArrayList();
        Iterator<? extends Iterable<? extends T>> it = iterable.iterator();
        while (it.hasNext()) {
            ba7.a((Collection) arrayList, (Iterable) it.next());
        }
        return arrayList;
    }
}
