package com.fossil;

import com.fossil.xg5;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<fz5> c;
    @DexIgnore
    public ArrayList<fz5> d;
    @DexIgnore
    public List<? extends xg5.a> e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public WatchFaceWrapper g;

    @DexIgnore
    public dz5(String str, String str2, ArrayList<fz5> arrayList, ArrayList<fz5> arrayList2, List<? extends xg5.a> list, boolean z, WatchFaceWrapper watchFaceWrapper) {
        ee7.b(str, "mPresetId");
        ee7.b(str2, "mPresetName");
        ee7.b(arrayList, "mComplications");
        ee7.b(arrayList2, "mWatchApps");
        ee7.b(list, "mPermissionFeatures");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = arrayList2;
        this.e = list;
        this.f = z;
        this.g = watchFaceWrapper;
    }

    @DexIgnore
    public final ArrayList<fz5> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.xg5$a>, java.util.List<com.fossil.xg5$a> */
    public final List<xg5.a> c() {
        return this.e;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dz5)) {
            return false;
        }
        dz5 dz5 = (dz5) obj;
        return ee7.a(this.a, dz5.a) && ee7.a(this.b, dz5.b) && ee7.a(this.c, dz5.c) && ee7.a(this.d, dz5.d) && ee7.a(this.e, dz5.e) && this.f == dz5.f && ee7.a(this.g, dz5.g);
    }

    @DexIgnore
    public final ArrayList<fz5> f() {
        return this.d;
    }

    @DexIgnore
    public final WatchFaceWrapper g() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<fz5> arrayList = this.c;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<fz5> arrayList2 = this.d;
        int hashCode4 = (hashCode3 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        List<? extends xg5.a> list = this.e;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        boolean z = this.f;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = (hashCode5 + i2) * 31;
        WatchFaceWrapper watchFaceWrapper = this.g;
        if (watchFaceWrapper != null) {
            i = watchFaceWrapper.hashCode();
        }
        return i4 + i;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.a + ", mPresetName=" + this.b + ", mComplications=" + this.c + ", mWatchApps=" + this.d + ", mPermissionFeatures=" + this.e + ", mIsActive=" + this.f + ", mWatchFaceWrapper=" + this.g + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ dz5(String str, String str2, ArrayList arrayList, ArrayList arrayList2, List list, boolean z, WatchFaceWrapper watchFaceWrapper, int i, zd7 zd7) {
        this(str, str2, arrayList, arrayList2, list, z, (i & 64) != 0 ? null : watchFaceWrapper);
    }
}
