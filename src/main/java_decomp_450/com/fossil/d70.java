package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d70 extends w60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<d70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public d70 createFromParcel(Parcel parcel) {
            return (d70) w60.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public d70[] newArray(int i) {
            return new d70[i];
        }
    }

    @DexIgnore
    public d70(byte b, byte b2, Set<? extends j90> set, boolean z, boolean z2) {
        super(b, b2, set, true, z, z2);
        if (!(!d().isEmpty())) {
            throw new IllegalArgumentException("Day Of Week must not be empty.");
        }
    }

    @DexIgnore
    public final Set<j90> getDaysOfWeek() {
        return d();
    }

    @DexIgnore
    public d70(byte b, byte b2, Set<? extends j90> set) {
        this(b, b2, set, false, true);
    }
}
