package com.fossil;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yg4 {
    @DexIgnore
    public static /* final */ String[] b; // = {"UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT"};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{0, 327708, 327710, 327709, 656318}, new int[]{590318, 0, 327710, 327709, 656318}, new int[]{262158, 590300, 0, 590301, 932798}, new int[]{327709, 327708, 656318, 0, 327710}, new int[]{327711, 656380, 656382, 656381, 0}};
    @DexIgnore
    public static /* final */ int[][] d;
    @DexIgnore
    public static /* final */ int[][] e;
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<ah4> {
        @DexIgnore
        public a(yg4 yg4) {
        }

        @DexIgnore
        /* renamed from: a */
        public int compare(ah4 ah4, ah4 ah42) {
            return ah4.b() - ah42.b();
        }
    }

    /*
    static {
        int[][] iArr = (int[][]) Array.newInstance(int.class, 5, 256);
        d = iArr;
        iArr[0][32] = 1;
        for (int i = 65; i <= 90; i++) {
            d[0][i] = (i - 65) + 2;
        }
        d[1][32] = 1;
        for (int i2 = 97; i2 <= 122; i2++) {
            d[1][i2] = (i2 - 97) + 2;
        }
        d[2][32] = 1;
        for (int i3 = 48; i3 <= 57; i3++) {
            d[2][i3] = (i3 - 48) + 2;
        }
        int[][] iArr2 = d;
        iArr2[2][44] = 12;
        iArr2[2][46] = 13;
        int[] iArr3 = {0, 32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 27, 28, 29, 30, 31, 64, 92, 94, 95, 96, 124, 126, 127};
        for (int i4 = 0; i4 < 28; i4++) {
            d[3][iArr3[i4]] = i4;
        }
        int[] iArr4 = {0, 13, 0, 0, 0, 0, 33, 39, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 58, 59, 60, 61, 62, 63, 91, 93, 123, 125};
        for (int i5 = 0; i5 < 31; i5++) {
            if (iArr4[i5] > 0) {
                d[4][iArr4[i5]] = i5;
            }
        }
        int[][] iArr5 = (int[][]) Array.newInstance(int.class, 6, 6);
        e = iArr5;
        for (int[] iArr6 : iArr5) {
            Arrays.fill(iArr6, -1);
        }
        int[][] iArr7 = e;
        iArr7[0][4] = 0;
        iArr7[1][4] = 0;
        iArr7[1][0] = 28;
        iArr7[3][4] = 0;
        iArr7[2][4] = 0;
        iArr7[2][0] = 15;
    }
    */

    @DexIgnore
    public yg4(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.ch4 a() {
        /*
            r8 = this;
            com.fossil.ah4 r0 = com.fossil.ah4.e
            java.util.List r0 = java.util.Collections.singletonList(r0)
            r1 = 0
            r2 = 0
        L_0x0008:
            byte[] r3 = r8.a
            int r4 = r3.length
            if (r2 >= r4) goto L_0x004e
            int r4 = r2 + 1
            int r5 = r3.length
            if (r4 >= r5) goto L_0x0015
            byte r3 = r3[r4]
            goto L_0x0016
        L_0x0015:
            r3 = 0
        L_0x0016:
            byte[] r5 = r8.a
            byte r5 = r5[r2]
            r6 = 13
            if (r5 == r6) goto L_0x003a
            r6 = 44
            r7 = 32
            if (r5 == r6) goto L_0x0036
            r6 = 46
            if (r5 == r6) goto L_0x0032
            r6 = 58
            if (r5 == r6) goto L_0x002e
        L_0x002c:
            r3 = 0
            goto L_0x003f
        L_0x002e:
            if (r3 != r7) goto L_0x002c
            r3 = 5
            goto L_0x003f
        L_0x0032:
            if (r3 != r7) goto L_0x002c
            r3 = 3
            goto L_0x003f
        L_0x0036:
            if (r3 != r7) goto L_0x002c
            r3 = 4
            goto L_0x003f
        L_0x003a:
            r5 = 10
            if (r3 != r5) goto L_0x002c
            r3 = 2
        L_0x003f:
            if (r3 <= 0) goto L_0x0047
            java.util.Collection r0 = a(r0, r2, r3)
            r2 = r4
            goto L_0x004b
        L_0x0047:
            java.util.Collection r0 = r8.a(r0, r2)
        L_0x004b:
            int r2 = r2 + 1
            goto L_0x0008
        L_0x004e:
            com.fossil.yg4$a r1 = new com.fossil.yg4$a
            r1.<init>(r8)
            java.lang.Object r0 = java.util.Collections.min(r0, r1)
            com.fossil.ah4 r0 = (com.fossil.ah4) r0
            byte[] r1 = r8.a
            com.fossil.ch4 r0 = r0.a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yg4.a():com.fossil.ch4");
    }

    @DexIgnore
    public final Collection<ah4> a(Iterable<ah4> iterable, int i) {
        LinkedList linkedList = new LinkedList();
        for (ah4 ah4 : iterable) {
            a(ah4, i, linkedList);
        }
        return a(linkedList);
    }

    @DexIgnore
    public final void a(ah4 ah4, int i, Collection<ah4> collection) {
        char c2 = (char) (this.a[i] & 255);
        boolean z = d[ah4.c()][c2] > 0;
        ah4 ah42 = null;
        for (int i2 = 0; i2 <= 4; i2++) {
            int i3 = d[i2][c2];
            if (i3 > 0) {
                if (ah42 == null) {
                    ah42 = ah4.b(i);
                }
                if (!z || i2 == ah4.c() || i2 == 2) {
                    collection.add(ah42.a(i2, i3));
                }
                if (!z && e[ah4.c()][i2] >= 0) {
                    collection.add(ah42.b(i2, i3));
                }
            }
        }
        if (ah4.a() > 0 || d[ah4.c()][c2] == 0) {
            collection.add(ah4.a(i));
        }
    }

    @DexIgnore
    public static Collection<ah4> a(Iterable<ah4> iterable, int i, int i2) {
        LinkedList linkedList = new LinkedList();
        for (ah4 ah4 : iterable) {
            a(ah4, i, i2, linkedList);
        }
        return a(linkedList);
    }

    @DexIgnore
    public static void a(ah4 ah4, int i, int i2, Collection<ah4> collection) {
        ah4 b2 = ah4.b(i);
        collection.add(b2.a(4, i2));
        if (ah4.c() != 4) {
            collection.add(b2.b(4, i2));
        }
        if (i2 == 3 || i2 == 4) {
            collection.add(b2.a(2, 16 - i2).a(2, 1));
        }
        if (ah4.a() > 0) {
            collection.add(ah4.a(i).a(i + 1));
        }
    }

    @DexIgnore
    public static Collection<ah4> a(Iterable<ah4> iterable) {
        LinkedList linkedList = new LinkedList();
        for (ah4 ah4 : iterable) {
            boolean z = true;
            Iterator it = linkedList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ah4 ah42 = (ah4) it.next();
                if (ah42.a(ah4)) {
                    z = false;
                    break;
                } else if (ah4.a(ah42)) {
                    it.remove();
                }
            }
            if (z) {
                linkedList.add(ah4);
            }
        }
        return linkedList;
    }
}
