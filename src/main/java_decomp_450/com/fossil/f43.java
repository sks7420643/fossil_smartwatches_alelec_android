package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f43 implements g43 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true);

    @DexIgnore
    @Override // com.fossil.g43
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
