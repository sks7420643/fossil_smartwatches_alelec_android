package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ye2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ve2 a;
    @DexIgnore
    public /* final */ /* synthetic */ xe2 b;

    @DexIgnore
    public ye2(xe2 xe2, ve2 ve2) {
        this.b = xe2;
        this.a = ve2;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.b.a.handleIntent(this.a.a);
        this.a.a();
    }
}
