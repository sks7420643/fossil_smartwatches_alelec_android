package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf4 extends JsonWriter {
    @DexIgnore
    public static /* final */ Writer t; // = new a();
    @DexIgnore
    public static /* final */ le4 u; // = new le4("closed");
    @DexIgnore
    public /* final */ List<JsonElement> q; // = new ArrayList();
    @DexIgnore
    public String r;
    @DexIgnore
    public JsonElement s; // = he4.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Writer {
        @DexIgnore
        @Override // java.io.Closeable, java.io.Writer, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer, java.io.Flushable
        public void flush() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Writer
        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public jf4() {
        super(t);
    }

    @DexIgnore
    public final void a(JsonElement jsonElement) {
        if (this.r != null) {
            if (!jsonElement.h() || l()) {
                ((ie4) peek()).a(this.r, jsonElement);
            }
            this.r = null;
        } else if (this.q.isEmpty()) {
            this.s = jsonElement;
        } else {
            JsonElement peek = peek();
            if (peek instanceof de4) {
                ((de4) peek).a(jsonElement);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter b(String str) throws IOException {
        if (this.q.isEmpty() || this.r != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof ie4) {
            this.r = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter c() throws IOException {
        de4 de4 = new de4();
        a(de4);
        this.q.add(de4);
        return this;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.google.gson.stream.JsonWriter, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.q.isEmpty()) {
            this.q.add(u);
            return;
        }
        throw new IOException("Incomplete document");
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter d(boolean z) throws IOException {
        a(new le4(Boolean.valueOf(z)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter e() throws IOException {
        ie4 ie4 = new ie4();
        a(ie4);
        this.q.add(ie4);
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter, java.io.Flushable
    public void flush() throws IOException {
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter g() throws IOException {
        if (this.q.isEmpty() || this.r != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof de4) {
            List<JsonElement> list = this.q;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter k() throws IOException {
        if (this.q.isEmpty() || this.r != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof ie4) {
            List<JsonElement> list = this.q;
            list.remove(list.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public final JsonElement peek() {
        List<JsonElement> list = this.q;
        return list.get(list.size() - 1);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter r() throws IOException {
        a(he4.a);
        return this;
    }

    @DexIgnore
    public JsonElement w() {
        if (this.q.isEmpty()) {
            return this.s;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.q);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter e(String str) throws IOException {
        if (str == null) {
            r();
            return this;
        }
        a(new le4(str));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter e(long j) throws IOException {
        a(new le4((Number) Long.valueOf(j)));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter a(Boolean bool) throws IOException {
        if (bool == null) {
            r();
            return this;
        }
        a(new le4(bool));
        return this;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonWriter
    public JsonWriter a(Number number) throws IOException {
        if (number == null) {
            r();
            return this;
        }
        if (!o()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new le4(number));
        return this;
    }
}
