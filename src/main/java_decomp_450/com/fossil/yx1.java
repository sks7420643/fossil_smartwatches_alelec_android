package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx1 implements Factory<xx1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<String> b;
    @DexIgnore
    public /* final */ Provider<Integer> c;

    @DexIgnore
    public yx1(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static yx1 a(Provider<Context> provider, Provider<String> provider2, Provider<Integer> provider3) {
        return new yx1(provider, provider2, provider3);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public xx1 get() {
        return new xx1(this.a.get(), this.b.get(), this.c.get().intValue());
    }
}
