package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qz6 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static /* final */ String d; // = "qz6";
    @DexIgnore
    public /* final */ FragmentManager a;
    @DexIgnore
    public /* final */ List<Fragment> b;
    @DexIgnore
    public int[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public a(qz6 qz6, View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;

        @DexIgnore
        public b(RecyclerView.ViewHolder viewHolder) {
            this.a = viewHolder;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            this.a.itemView.removeOnAttachStateChangeListener(this);
            go5 go5 = (go5) qz6.this.b.get(this.a.getAdapterPosition());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = qz6.d;
            local.d(str, "onBindViewHolder tag=" + go5.d1() + " childFragmentManager=" + qz6.this.a + "this=" + this);
            Fragment b2 = qz6.this.a.b(go5.d1());
            if (b2 != null) {
                nc b3 = qz6.this.a.b();
                b3.d(b2);
                b3.d();
            }
            nc b4 = qz6.this.a.b();
            b4.a(view.getId(), go5, go5.d1());
            b4.d();
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public qz6(FragmentManager fragmentManager, List<Fragment> list) {
        this.a = fragmentManager;
        this.b = list;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        int size = this.b.size();
        int[] iArr = this.c;
        if (iArr == null || size != iArr.length) {
            this.c = new int[size];
        }
        return size;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        this.c[i] = this.b.get(i).getId();
        int[] iArr = this.c;
        if (iArr[i] == 0) {
            iArr[i] = View.generateViewId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = d;
            local.d(str, "getItemId: position = " + i + ", id = " + this.c[i]);
        }
        return (long) this.c[i];
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "onBindViewHolder: position = " + i);
        Fragment b2 = this.a.b(viewHolder.itemView.getId());
        if (b2 != null) {
            nc b3 = this.a.b();
            b3.d(b2);
            b3.d();
        }
        viewHolder.itemView.setId((int) getItemId(i));
        viewHolder.itemView.addOnAttachStateChangeListener(new b(viewHolder));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new a(this, frameLayout);
    }
}
