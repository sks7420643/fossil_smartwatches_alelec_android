package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j46 extends dl4<i46> {
    @DexIgnore
    void D0();

    @DexIgnore
    void S();

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void h(boolean z);

    @DexIgnore
    void i(List<WeatherLocationWrapper> list);
}
