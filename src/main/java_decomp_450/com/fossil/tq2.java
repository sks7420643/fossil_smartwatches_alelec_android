package com.fossil;

import android.content.Context;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tq2<T> {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static volatile cr2 h;
    @DexIgnore
    public static hr2 i; // = new hr2(vq2.a);
    @DexIgnore
    public static /* final */ AtomicInteger j; // = new AtomicInteger();
    @DexIgnore
    public /* final */ dr2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;
    @DexIgnore
    public /* final */ boolean f;

    /*
    static {
        new AtomicReference();
    }
    */

    @DexIgnore
    public tq2(dr2 dr2, String str, T t, boolean z) {
        this.d = -1;
        if (dr2.a != null) {
            this.a = dr2;
            this.b = str;
            this.c = t;
            this.f = z;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    @Deprecated
    public static void a(Context context) {
        synchronized (g) {
            cr2 cr2 = h;
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (cr2 == null || cr2.a() != context) {
                fq2.e();
                fr2.a();
                oq2.a();
                h = new gq2(context, sr2.a((tr2) new wq2(context)));
                j.incrementAndGet();
            }
        }
    }

    @DexIgnore
    public static void c() {
        j.incrementAndGet();
    }

    @DexIgnore
    public static final /* synthetic */ boolean d() {
        return true;
    }

    @DexIgnore
    public abstract T a(Object obj);

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T b() {
        /*
            r7 = this;
            boolean r0 = r7.f
            if (r0 != 0) goto L_0x0011
            com.fossil.hr2 r0 = com.fossil.tq2.i
            java.lang.String r1 = r7.b
            boolean r0 = r0.a(r1)
            java.lang.String r1 = "Attempt to access PhenotypeFlag not via codegen. All new PhenotypeFlags must be accessed through codegen APIs. If you believe you are seeing this error by mistake, you can add your flag to the exemption list located at //java/com/google/android/libraries/phenotype/client/lockdown/flags.textproto. Send the addition CL to ph-reviews@. See go/phenotype-android-codegen for information about generated code. See go/ph-lockdown for more information about this error."
            com.fossil.or2.b(r0, r1)
        L_0x0011:
            java.util.concurrent.atomic.AtomicInteger r0 = com.fossil.tq2.j
            int r0 = r0.get()
            int r1 = r7.d
            if (r1 >= r0) goto L_0x0115
            monitor-enter(r7)
            int r1 = r7.d     // Catch:{ all -> 0x0112 }
            if (r1 >= r0) goto L_0x0110
            com.fossil.cr2 r1 = com.fossil.tq2.h     // Catch:{ all -> 0x0112 }
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0028
            r4 = 1
            goto L_0x0029
        L_0x0028:
            r4 = 0
        L_0x0029:
            java.lang.String r5 = "Must call PhenotypeFlag.init() first"
            com.fossil.or2.b(r4, r5)     // Catch:{ all -> 0x0112 }
            android.content.Context r4 = r1.a()     // Catch:{ all -> 0x0112 }
            com.fossil.oq2 r4 = com.fossil.oq2.a(r4)     // Catch:{ all -> 0x0112 }
            java.lang.String r5 = "gms:phenotype:phenotype_flag:debug_bypass_phenotype"
            java.lang.Object r4 = r4.zza(r5)     // Catch:{ all -> 0x0112 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x0112 }
            if (r4 == 0) goto L_0x004d
            java.util.regex.Pattern r5 = com.fossil.cq2.c     // Catch:{ all -> 0x0112 }
            java.util.regex.Matcher r4 = r5.matcher(r4)     // Catch:{ all -> 0x0112 }
            boolean r4 = r4.matches()     // Catch:{ all -> 0x0112 }
            if (r4 == 0) goto L_0x004d
            goto L_0x004e
        L_0x004d:
            r2 = 0
        L_0x004e:
            r3 = 0
            if (r2 != 0) goto L_0x0091
            com.fossil.dr2 r2 = r7.a     // Catch:{ all -> 0x0112 }
            android.net.Uri r2 = r2.a     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x0078
            android.content.Context r2 = r1.a()     // Catch:{ all -> 0x0112 }
            com.fossil.dr2 r4 = r7.a     // Catch:{ all -> 0x0112 }
            android.net.Uri r4 = r4.a     // Catch:{ all -> 0x0112 }
            boolean r2 = com.fossil.rq2.a(r2, r4)     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x0076
            android.content.Context r2 = r1.a()     // Catch:{ all -> 0x0112 }
            android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ all -> 0x0112 }
            com.fossil.dr2 r4 = r7.a     // Catch:{ all -> 0x0112 }
            android.net.Uri r4 = r4.a     // Catch:{ all -> 0x0112 }
            com.fossil.fq2 r2 = com.fossil.fq2.a(r2, r4)     // Catch:{ all -> 0x0112 }
            goto L_0x0080
        L_0x0076:
            r2 = r3
            goto L_0x0080
        L_0x0078:
            android.content.Context r2 = r1.a()     // Catch:{ all -> 0x0112 }
            com.fossil.fr2 r2 = com.fossil.fr2.a(r2, r3)     // Catch:{ all -> 0x0112 }
        L_0x0080:
            if (r2 == 0) goto L_0x00ba
            java.lang.String r4 = r7.a()     // Catch:{ all -> 0x0112 }
            java.lang.Object r2 = r2.zza(r4)     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x00ba
            java.lang.Object r2 = r7.a(r2)     // Catch:{ all -> 0x0112 }
            goto L_0x00bb
        L_0x0091:
            java.lang.String r2 = "PhenotypeFlag"
            r4 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r4)     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x00ba
            java.lang.String r2 = "PhenotypeFlag"
            java.lang.String r4 = "Bypass reading Phenotype values for flag: "
            java.lang.String r5 = r7.a()     // Catch:{ all -> 0x0112 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0112 }
            int r6 = r5.length()     // Catch:{ all -> 0x0112 }
            if (r6 == 0) goto L_0x00b1
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x0112 }
            goto L_0x00b7
        L_0x00b1:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x0112 }
            r5.<init>(r4)     // Catch:{ all -> 0x0112 }
            r4 = r5
        L_0x00b7:
            android.util.Log.d(r2, r4)     // Catch:{ all -> 0x0112 }
        L_0x00ba:
            r2 = r3
        L_0x00bb:
            if (r2 == 0) goto L_0x00be
            goto L_0x00df
        L_0x00be:
            android.content.Context r2 = r1.a()     // Catch:{ all -> 0x0112 }
            com.fossil.oq2 r2 = com.fossil.oq2.a(r2)     // Catch:{ all -> 0x0112 }
            com.fossil.dr2 r4 = r7.a     // Catch:{ all -> 0x0112 }
            java.lang.String r4 = r4.b     // Catch:{ all -> 0x0112 }
            java.lang.String r4 = r7.a(r4)     // Catch:{ all -> 0x0112 }
            java.lang.Object r2 = r2.zza(r4)     // Catch:{ all -> 0x0112 }
            if (r2 == 0) goto L_0x00d9
            java.lang.Object r2 = r7.a(r2)     // Catch:{ all -> 0x0112 }
            goto L_0x00da
        L_0x00d9:
            r2 = r3
        L_0x00da:
            if (r2 == 0) goto L_0x00dd
            goto L_0x00df
        L_0x00dd:
            T r2 = r7.c     // Catch:{ all -> 0x0112 }
        L_0x00df:
            com.fossil.tr2 r1 = r1.b()     // Catch:{ all -> 0x0112 }
            java.lang.Object r1 = r1.zza()     // Catch:{ all -> 0x0112 }
            com.fossil.pr2 r1 = (com.fossil.pr2) r1     // Catch:{ all -> 0x0112 }
            boolean r4 = r1.zza()     // Catch:{ all -> 0x0112 }
            if (r4 == 0) goto L_0x010c
            java.lang.Object r1 = r1.zzb()     // Catch:{ all -> 0x0112 }
            com.fossil.pq2 r1 = (com.fossil.pq2) r1     // Catch:{ all -> 0x0112 }
            com.fossil.dr2 r2 = r7.a     // Catch:{ all -> 0x0112 }
            android.net.Uri r2 = r2.a     // Catch:{ all -> 0x0112 }
            com.fossil.dr2 r4 = r7.a     // Catch:{ all -> 0x0112 }
            java.lang.String r4 = r4.c     // Catch:{ all -> 0x0112 }
            java.lang.String r5 = r7.b     // Catch:{ all -> 0x0112 }
            java.lang.String r1 = r1.a(r2, r3, r4, r5)     // Catch:{ all -> 0x0112 }
            if (r1 != 0) goto L_0x0108
            T r2 = r7.c     // Catch:{ all -> 0x0112 }
            goto L_0x010c
        L_0x0108:
            java.lang.Object r2 = r7.a(r1)     // Catch:{ all -> 0x0112 }
        L_0x010c:
            r7.e = r2     // Catch:{ all -> 0x0112 }
            r7.d = r0     // Catch:{ all -> 0x0112 }
        L_0x0110:
            monitor-exit(r7)     // Catch:{ all -> 0x0112 }
            goto L_0x0115
        L_0x0112:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0112 }
            throw r0
        L_0x0115:
            T r0 = r7.e
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tq2.b():java.lang.Object");
    }

    @DexIgnore
    public /* synthetic */ tq2(dr2 dr2, String str, Object obj, boolean z, zq2 zq2) {
        this(dr2, str, obj, z);
    }

    @DexIgnore
    public final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final String a() {
        return a(this.a.c);
    }

    @DexIgnore
    public static tq2<Long> b(dr2 dr2, String str, long j2, boolean z) {
        return new zq2(dr2, str, Long.valueOf(j2), true);
    }

    @DexIgnore
    public static tq2<Boolean> b(dr2 dr2, String str, boolean z, boolean z2) {
        return new yq2(dr2, str, Boolean.valueOf(z), true);
    }

    @DexIgnore
    public static tq2<Double> b(dr2 dr2, String str, double d2, boolean z) {
        return new br2(dr2, str, Double.valueOf(d2), true);
    }

    @DexIgnore
    public static tq2<String> b(dr2 dr2, String str, String str2, boolean z) {
        return new ar2(dr2, str, str2, true);
    }

    @DexIgnore
    public static final /* synthetic */ pr2 b(Context context) {
        new sq2();
        return sq2.a(context);
    }
}
