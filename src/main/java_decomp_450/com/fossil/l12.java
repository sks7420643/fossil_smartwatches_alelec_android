package com.fossil;

import com.fossil.i12;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l12<R extends i12, S extends i12> {
    @DexIgnore
    public abstract c12<S> a(R r);

    @DexIgnore
    public abstract Status a(Status status);
}
