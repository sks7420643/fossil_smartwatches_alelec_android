package com.fossil;

import com.fossil.ng;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp5 extends ng.d<ActivitySummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        ee7.b(activitySummary, "oldItem");
        ee7.b(activitySummary2, "newItem");
        return ee7.a(activitySummary, activitySummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        ee7.b(activitySummary, "oldItem");
        ee7.b(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
