package com.fossil;

import android.content.ComponentName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ComponentName a;
    @DexIgnore
    public /* final */ /* synthetic */ zk3 b;

    @DexIgnore
    public bl3(zk3 zk3, ComponentName componentName) {
        this.b = zk3;
        this.a = componentName;
    }

    @DexIgnore
    public final void run() {
        this.b.c.a(this.a);
    }
}
