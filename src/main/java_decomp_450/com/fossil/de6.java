package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de6 implements Factory<ce6> {
    @DexIgnore
    public static ce6 a(ae6 ae6, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        return new ce6(ae6, userRepository, sleepSummariesRepository, portfolioApp);
    }
}
