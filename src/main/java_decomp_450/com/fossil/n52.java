package com.fossil;

import android.util.Log;
import com.fossil.v02;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n52 implements ho3<Map<p12<?>, String>> {
    @DexIgnore
    public c22 a;
    @DexIgnore
    public /* final */ /* synthetic */ m52 b;

    @DexIgnore
    public n52(m52 m52, c22 c22) {
        this.b = m52;
        this.a = c22;
    }

    @DexIgnore
    public final void a() {
        this.a.onComplete();
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3<Map<p12<?>, String>> no3) {
        this.b.f.lock();
        try {
            if (!this.b.s) {
                this.a.onComplete();
                return;
            }
            if (no3.e()) {
                Map unused = this.b.u = new n4(this.b.b.size());
                for (k52 k52 : this.b.b.values()) {
                    this.b.u.put(k52.a(), i02.e);
                }
            } else if (no3.a() instanceof x02) {
                x02 x02 = (x02) no3.a();
                if (this.b.q) {
                    Map unused2 = this.b.u = new n4(this.b.b.size());
                    for (k52 k522 : this.b.b.values()) {
                        p12 a2 = k522.a();
                        i02 connectionResult = x02.getConnectionResult((z02<? extends v02.d>) k522);
                        if (this.b.a(k522, connectionResult)) {
                            this.b.u.put(a2, new i02(16));
                        } else {
                            this.b.u.put(a2, connectionResult);
                        }
                    }
                } else {
                    Map unused3 = this.b.u = x02.zaj();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", no3.a());
                Map unused4 = this.b.u = Collections.emptyMap();
            }
            if (this.b.c()) {
                this.b.t.putAll(this.b.u);
                if (this.b.k() == null) {
                    this.b.i();
                    this.b.j();
                    this.b.i.signalAll();
                }
            }
            this.a.onComplete();
            this.b.f.unlock();
        } finally {
            this.b.f.unlock();
        }
    }
}
