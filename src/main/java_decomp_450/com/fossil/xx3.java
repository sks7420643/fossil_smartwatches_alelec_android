package com.fossil;

import com.fossil.by3;
import java.io.Serializable;
import java.lang.Enum;
import java.util.EnumMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx3<K extends Enum<K>, V> extends by3.c<K, V> {
    @DexIgnore
    public /* final */ transient EnumMap<K, V> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K extends Enum<K>, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumMap<K, V> delegate;

        @DexIgnore
        public b(EnumMap<K, V> enumMap) {
            this.delegate = enumMap;
        }

        @DexIgnore
        public Object readResolve() {
            return new xx3(this.delegate);
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> by3<K, V> asImmutable(EnumMap<K, V> enumMap) {
        int size = enumMap.size();
        if (size == 0) {
            return by3.of();
        }
        if (size != 1) {
            return new xx3(enumMap);
        }
        Map.Entry entry = (Map.Entry) py3.b(enumMap.entrySet());
        return by3.of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean containsKey(Object obj) {
        return this.e.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.by3.c
    public j04<Map.Entry<K, V>> entryIterator() {
        return yy3.b(this.e.entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof xx3) {
            obj = ((xx3) obj).e;
        }
        return this.e.equals(obj);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3
    public V get(Object obj) {
        return this.e.get(obj);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.by3
    public j04<K> keyIterator() {
        return qy3.e(this.e.keySet().iterator());
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    @Override // com.fossil.by3
    public Object writeReplace() {
        return new b(this.e);
    }

    @DexIgnore
    public xx3(EnumMap<K, V> enumMap) {
        this.e = enumMap;
        jw3.a(!enumMap.isEmpty());
    }
}
