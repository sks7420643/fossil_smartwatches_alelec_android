package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik0 extends xp0 {
    @DexIgnore
    public static /* final */ li0 CREATOR; // = new li0(null);
    @DexIgnore
    public /* final */ q90 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public ik0(byte b, int i, q90 q90) {
        super(ru0.APP_NOTIFICATION_EVENT, b, false, 4);
        this.d = q90;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.t0, this.d.a()), r51.b4, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    public /* synthetic */ ik0(Parcel parcel, zd7 zd7) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(q90.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (q90) readParcelable;
            this.e = parcel.readInt();
            return;
        }
        ee7.a();
        throw null;
    }
}
