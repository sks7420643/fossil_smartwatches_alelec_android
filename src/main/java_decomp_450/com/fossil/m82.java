package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m82 implements Parcelable.Creator<n82> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ n82 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Bundle bundle = null;
        k02[] k02Arr = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                bundle = j72.a(parcel, a);
            } else if (a2 == 2) {
                k02Arr = (k02[]) j72.b(parcel, a, k02.CREATOR);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                i = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new n82(bundle, k02Arr, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ n82[] newArray(int i) {
        return new n82[i];
    }
}
