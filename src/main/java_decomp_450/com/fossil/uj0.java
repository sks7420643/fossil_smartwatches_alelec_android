package com.fossil;

import com.fossil.pc0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj0 extends fe7 implements kd7<byte[], qk1, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public uj0(km1 km1) {
        super(2);
        this.a = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(byte[] bArr, qk1 qk1) {
        c90 c90;
        km1 km1;
        pc0.a aVar;
        byte[] bArr2 = bArr;
        qk1 qk12 = qk1;
        c90[] values = c90.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                c90 = null;
                break;
            }
            c90 = values[i];
            if (c90.a() == qk12) {
                break;
            }
            i++;
        }
        if (!(c90 == null || (aVar = (km1 = this.a).r) == null)) {
            aVar.a(km1, bArr2, c90);
        }
        return i97.a;
    }
}
