package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yd4 {
    @DexIgnore
    public /* final */ Field a;

    @DexIgnore
    public yd4(Field field) {
        we4.a(field);
        this.a = field;
    }

    @DexIgnore
    public <T extends Annotation> T a(Class<T> cls) {
        return (T) this.a.getAnnotation(cls);
    }
}
