package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface u82 extends IInterface {
    @DexIgnore
    ab2 zzb() throws RemoteException;

    @DexIgnore
    int zzc() throws RemoteException;
}
