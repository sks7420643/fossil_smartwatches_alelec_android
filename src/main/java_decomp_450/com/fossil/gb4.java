package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import com.fossil.fb4;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gb4 implements Runnable {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ PowerManager.WakeLock b;
    @DexIgnore
    public /* final */ FirebaseInstanceId c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends BroadcastReceiver {
        @DexIgnore
        public gb4 a;

        @DexIgnore
        public a(gb4 gb4) {
            this.a = gb4;
        }

        @DexIgnore
        public void a() {
            if (FirebaseInstanceId.q()) {
                Log.d("FirebaseInstanceId", "Connectivity change received registered");
            }
            this.a.a().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            gb4 gb4 = this.a;
            if (gb4 != null && gb4.b()) {
                if (FirebaseInstanceId.q()) {
                    Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
                }
                this.a.c.a(this.a, 0);
                this.a.a().unregisterReceiver(this);
                this.a = null;
            }
        }
    }

    @DexIgnore
    public gb4(FirebaseInstanceId firebaseInstanceId, long j) {
        this.c = firebaseInstanceId;
        this.a = j;
        PowerManager.WakeLock newWakeLock = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
        this.b = newWakeLock;
        newWakeLock.setReferenceCounted(false);
    }

    @DexIgnore
    public final void a(String str) {
        if ("[DEFAULT]".equals(this.c.d().c())) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(this.c.d().c());
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Invoking onNewToken for app: ".concat(valueOf) : new String("Invoking onNewToken for app: "));
            }
            Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
            intent.putExtra("token", str);
            eb4.d(a(), intent);
        }
    }

    @DexIgnore
    public boolean b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public boolean c() throws IOException {
        fb4.a i = this.c.i();
        if (!this.c.a(i)) {
            return true;
        }
        try {
            String a2 = this.c.a();
            if (a2 == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if (i == null || (i != null && !a2.equals(i.a))) {
                a(a2);
            }
            return true;
        } catch (IOException e) {
            if (ga4.a(e.getMessage())) {
                String message = e.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 52);
                sb.append("Token retrieval failed: ");
                sb.append(message);
                sb.append(". Will retry token retrieval");
                Log.w("FirebaseInstanceId", sb.toString());
                return false;
            } else if (e.getMessage() == null) {
                Log.w("FirebaseInstanceId", "Token retrieval failed without exception message. Will retry token retrieval");
                return false;
            } else {
                throw e;
            }
        } catch (SecurityException unused) {
            Log.w("FirebaseInstanceId", "Token retrieval failed with SecurityException. Will retry token retrieval");
            return false;
        }
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public void run() {
        if (eb4.b().b(a())) {
            this.b.acquire();
        }
        try {
            this.c.a(true);
            if (!this.c.k()) {
                this.c.a(false);
                if (eb4.b().b(a())) {
                    this.b.release();
                }
            } else if (!eb4.b().a(a()) || b()) {
                if (c()) {
                    this.c.a(false);
                } else {
                    this.c.a(this.a);
                }
                if (eb4.b().b(a())) {
                    this.b.release();
                }
            } else {
                new a(this).a();
                if (eb4.b().b(a())) {
                    this.b.release();
                }
            }
        } catch (IOException e) {
            String message = e.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 93);
            sb.append("Topic sync or token retrieval failed on hard failure exceptions: ");
            sb.append(message);
            sb.append(". Won't retry the operation.");
            Log.e("FirebaseInstanceId", sb.toString());
            this.c.a(false);
            if (eb4.b().b(a())) {
                this.b.release();
            }
        } catch (Throwable th) {
            if (eb4.b().b(a())) {
                this.b.release();
            }
            throw th;
        }
    }

    @DexIgnore
    public Context a() {
        return this.c.d().b();
    }
}
