package com.fossil;

import com.fossil.g74;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h74 implements g74 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ File[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;

    @DexIgnore
    public h74(File file) {
        this(file, Collections.emptyMap());
    }

    @DexIgnore
    @Override // com.fossil.g74
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.c);
    }

    @DexIgnore
    @Override // com.fossil.g74
    public String b() {
        String e = e();
        return e.substring(0, e.lastIndexOf(46));
    }

    @DexIgnore
    @Override // com.fossil.g74
    public File c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public File[] d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public String e() {
        return c().getName();
    }

    @DexIgnore
    @Override // com.fossil.g74
    public g74.a getType() {
        return g74.a.JAVA;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public void remove() {
        z24 a2 = z24.a();
        a2.a("Removing report at " + this.a.getPath());
        this.a.delete();
    }

    @DexIgnore
    public h74(File file, Map<String, String> map) {
        this.a = file;
        this.b = new File[]{file};
        this.c = new HashMap(map);
    }
}
