package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq6 extends oq6 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ int l; // = (Calendar.getInstance().get(1) - 110);
    @DexIgnore
    public Calendar e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = this.e.get(2);
    @DexIgnore
    public int h; // = this.e.get(1);
    @DexIgnore
    public int i; // = this.e.getActualMaximum(5);
    @DexIgnore
    public /* final */ pq6 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = sq6.class.getSimpleName();
        ee7.a((Object) simpleName, "BirthdayPresenter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public sq6(pq6 pq6) {
        ee7.b(pq6, "mView");
        this.j = pq6;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        this.e = instance;
        this.f = instance.get(5);
    }

    @DexIgnore
    @Override // com.fossil.oq6
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current day: " + this.f + ", new day: " + i2);
        if (i2 >= 1 && i2 <= this.i) {
            this.f = i2;
        }
    }

    @DexIgnore
    @Override // com.fossil.oq6
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current month: " + this.g + ", new month: " + i2);
        if (i2 >= 1 && i2 <= 12) {
            this.g = i2;
            k();
        }
    }

    @DexIgnore
    @Override // com.fossil.oq6
    public void c(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Current year: " + this.h + ", new year: " + i2);
        if (i2 >= l) {
            this.h = i2;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        i();
        this.j.a(new r87<>(1, Integer.valueOf(this.i)), new r87<>(1, 12), new r87<>(Integer.valueOf(l), Integer.valueOf(this.e.get(1))));
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        i();
    }

    @DexIgnore
    @Override // com.fossil.oq6
    public void h() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "confirmBirthday " + this.f + '/' + this.g + '/' + this.h);
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g + -1, this.f);
        pq6 pq6 = this.j;
        ee7.a((Object) instance, "calendar");
        Date time = instance.getTime();
        ee7.a((Object) time, "calendar.time");
        pq6.c(time);
    }

    @DexIgnore
    public final void i() {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        this.e = instance;
        this.f = instance.get(5);
        this.g = this.e.get(2);
        this.h = this.e.get(1);
        this.i = this.e.getActualMaximum(5);
    }

    @DexIgnore
    public void j() {
        this.j.a(this);
    }

    @DexIgnore
    public final void k() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.h, this.g - 1, 1);
        this.i = instance.getActualMaximum(5);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Day range of " + this.g + '/' + this.h + ": " + this.i);
        this.j.a(1, this.i);
    }
}
