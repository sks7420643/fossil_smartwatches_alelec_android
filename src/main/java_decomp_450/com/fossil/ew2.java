package com.fossil;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew2 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ byte[] b;

    /*
    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        byte[] bArr2 = b;
        gv2.a(bArr2, 0, bArr2.length, false);
    }
    */

    @DexIgnore
    public static int a(long j) {
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }

    @DexIgnore
    public static <T> T a(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    public static String b(byte[] bArr) {
        return new String(bArr, a);
    }

    @DexIgnore
    public static int c(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }

    @DexIgnore
    public static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static boolean a(byte[] bArr) {
        return dz2.a(bArr);
    }

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    @DexIgnore
    public static boolean a(jx2 jx2) {
        if (!(jx2 instanceof lu2)) {
            return false;
        }
        lu2 lu2 = (lu2) jx2;
        return false;
    }

    @DexIgnore
    public static Object a(Object obj, Object obj2) {
        return ((jx2) obj).f().a((jx2) obj2).b();
    }
}
