package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc2 implements Parcelable.Creator<DataPoint> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        gc2 gc2 = null;
        mc2[] mc2Arr = null;
        gc2 gc22 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    gc2 = (gc2) j72.a(parcel, a, gc2.CREATOR);
                    break;
                case 2:
                default:
                    j72.v(parcel, a);
                    break;
                case 3:
                    j = j72.s(parcel, a);
                    break;
                case 4:
                    j2 = j72.s(parcel, a);
                    break;
                case 5:
                    mc2Arr = (mc2[]) j72.b(parcel, a, mc2.CREATOR);
                    break;
                case 6:
                    gc22 = (gc2) j72.a(parcel, a, gc2.CREATOR);
                    break;
                case 7:
                    j3 = j72.s(parcel, a);
                    break;
                case 8:
                    j4 = j72.s(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new DataPoint(gc2, j, j2, mc2Arr, gc22, j3, j4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataPoint[] newArray(int i) {
        return new DataPoint[i];
    }
}
