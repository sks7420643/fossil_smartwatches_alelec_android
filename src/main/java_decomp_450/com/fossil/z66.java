package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z66 implements Factory<yb6> {
    @DexIgnore
    public static yb6 a(u66 u66) {
        yb6 e = u66.e();
        c87.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
