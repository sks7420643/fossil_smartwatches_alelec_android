package com.fossil;

import androidx.fragment.app.FragmentActivity;
import com.fossil.fl4;
import com.fossil.fn5;
import com.fossil.qn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj6 extends kj6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public /* final */ lj6 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ qd5 g;
    @DexIgnore
    public /* final */ fn5 h;
    @DexIgnore
    public /* final */ qn5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return oj6.j;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<qn5.d, qn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ oj6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(oj6 oj6) {
            this.a = oj6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oj6.k.a(), "deleteUser - onSuccess");
            this.a.e.h();
            if (((xi6) this.a.e).isActive()) {
                this.a.e.S0();
            }
        }

        @DexIgnore
        public void a(qn5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(oj6.k.a(), "deleteUser - onError");
            this.a.e.h();
            this.a.e.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1", f = "DeleteAccountPresenter.kt", l = {108}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super SKUModel> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(oj6 oj6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oj6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                String str = "";
                if (sku == null) {
                    sku = str;
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName != null) {
                    str = deviceName;
                }
                hashMap.put("Device_Name", str);
                this.this$0.g.a("feedback_submit", hashMap);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1", f = "DeleteAccountPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super SKUModel> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(oj6 oj6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oj6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                String str = "";
                if (sku == null) {
                    sku = str;
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName != null) {
                    str = deviceName;
                }
                hashMap.put("Device_Name", str);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.g.a("feedback_open", hashMap);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<fn5.d, fn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ oj6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ fn5.d $responseValue;

            @DexIgnore
            public a(fn5.d dVar) {
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = oj6.k.a();
                local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                FLogger.INSTANCE.getLocal().d(oj6.k.a(), "getRequestSubject");
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                FLogger.INSTANCE.getLocal().d(oj6.k.a(), "getTags");
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(oj6 oj6) {
            this.a = oj6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oj6.k.a(), "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.e.a((ZendeskFeedbackConfiguration) new a(dVar));
        }

        @DexIgnore
        public void a(fn5.b bVar) {
            ee7.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(oj6.k.a(), "sendFeedback onError");
        }
    }

    /*
    static {
        String simpleName = oj6.class.getSimpleName();
        ee7.a((Object) simpleName, "DeleteAccountPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public oj6(lj6 lj6, DeviceRepository deviceRepository, qd5 qd5, UserRepository userRepository, fn5 fn5, qn5 qn5) {
        ee7.b(lj6, "mView");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(qd5, "mAnalyticsHelper");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(fn5, "mGetZendeskInformation");
        ee7.b(qn5, "mDeleteLogoutUserUseCase");
        this.e = lj6;
        this.f = deviceRepository;
        this.g = qd5;
        this.h = fn5;
        this.i = qn5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void h() {
        FLogger.INSTANCE.getLocal().d(j, "deleteUser");
        this.e.j();
        qn5 qn5 = this.i;
        lj6 lj6 = this.e;
        if (lj6 != null) {
            FragmentActivity activity = ((xi6) lj6).getActivity();
            if (activity != null) {
                qn5.a(new qn5.b(0, new WeakReference(activity)), new b(this));
                return;
            }
            throw new x87("null cannot be cast to non-null type android.app.Activity");
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void i() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void j() {
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public void k() {
        this.e.a(this);
    }

    @DexIgnore
    @Override // com.fossil.kj6
    public void a(String str) {
        ee7.b(str, "subject");
        this.h.a(new fn5.c(str), new e(this));
    }
}
