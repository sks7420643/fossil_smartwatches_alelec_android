package com.fossil;

import com.facebook.share.internal.VideoUploader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc7 implements hg7<File> {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ nc7 b;
    @DexIgnore
    public /* final */ gd7<File, Boolean> c;
    @DexIgnore
    public /* final */ gd7<File, i97> d;
    @DexIgnore
    public /* final */ kd7<File, IOException, i97> e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends c {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(File file) {
            super(file);
            ee7.b(file, "rootDir");
            if (k97.a) {
                boolean isDirectory = file.isDirectory();
                if (k97.a && !isDirectory) {
                    throw new AssertionError("rootDir must be verified to be directory beforehand.");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public c(File file) {
            ee7.b(file, "root");
            this.a = file;
        }

        @DexIgnore
        public final File a() {
            return this.a;
        }

        @DexIgnore
        public abstract File b();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.gd7<? super java.io.File, java.lang.Boolean> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.gd7<? super java.io.File, com.fossil.i97> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.kd7<? super java.io.File, ? super java.io.IOException, com.fossil.i97> */
    /* JADX WARN: Multi-variable type inference failed */
    public lc7(File file, nc7 nc7, gd7<? super File, Boolean> gd7, gd7<? super File, i97> gd72, kd7<? super File, ? super IOException, i97> kd7, int i) {
        this.a = file;
        this.b = nc7;
        this.c = gd7;
        this.d = gd72;
        this.e = kd7;
        this.f = i;
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<File> iterator() {
        return new b();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ lc7(File file, nc7 nc7, gd7 gd7, gd7 gd72, kd7 kd7, int i, int i2, zd7 zd7) {
        this(file, (i2 & 2) != 0 ? nc7.TOP_DOWN : nc7, gd7, gd72, kd7, (i2 & 32) != 0 ? Integer.MAX_VALUE : i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends n97<File> {
        @DexIgnore
        public /* final */ ArrayDeque<c> c; // = new ArrayDeque<>();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends a {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public boolean e;
            @DexIgnore
            public /* final */ /* synthetic */ b f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, File file) {
                super(file);
                ee7.b(file, "rootDir");
                this.f = bVar;
            }

            @DexIgnore
            @Override // com.fossil.lc7.c
            public File b() {
                if (!this.e && this.c == null) {
                    gd7 c2 = lc7.this.c;
                    if (c2 != null && !((Boolean) c2.invoke(a())).booleanValue()) {
                        return null;
                    }
                    File[] listFiles = a().listFiles();
                    this.c = listFiles;
                    if (listFiles == null) {
                        kd7 d2 = lc7.this.e;
                        if (d2 != null) {
                            i97 i97 = (i97) d2.invoke(a(), new fc7(a(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.e = true;
                    }
                }
                File[] fileArr = this.c;
                if (fileArr != null) {
                    int i = this.d;
                    if (fileArr == null) {
                        ee7.a();
                        throw null;
                    } else if (i < fileArr.length) {
                        if (fileArr != null) {
                            this.d = i + 1;
                            return fileArr[i];
                        }
                        ee7.a();
                        throw null;
                    }
                }
                if (!this.b) {
                    this.b = true;
                    return a();
                }
                gd7 e2 = lc7.this.d;
                if (e2 != null) {
                    i97 i972 = (i97) e2.invoke(a());
                }
                return null;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lc7$b$b")
        /* renamed from: com.fossil.lc7$b$b  reason: collision with other inner class name */
        public final class C0110b extends c {
            @DexIgnore
            public boolean b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0110b(b bVar, File file) {
                super(file);
                ee7.b(file, "rootFile");
                if (k97.a) {
                    boolean isFile = file.isFile();
                    if (k97.a && !isFile) {
                        throw new AssertionError("rootFile must be verified to be file beforehand.");
                    }
                }
            }

            @DexIgnore
            @Override // com.fossil.lc7.c
            public File b() {
                if (this.b) {
                    return null;
                }
                this.b = true;
                return a();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class c extends a {
            @DexIgnore
            public boolean b;
            @DexIgnore
            public File[] c;
            @DexIgnore
            public int d;
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, File file) {
                super(file);
                ee7.b(file, "rootDir");
                this.e = bVar;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:32:0x0087, code lost:
                if (r0.length == 0) goto L_0x008e;
             */
            @DexIgnore
            @Override // com.fossil.lc7.c
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.io.File b() {
                /*
                    r10 = this;
                    boolean r0 = r10.b
                    r1 = 0
                    if (r0 != 0) goto L_0x0028
                    com.fossil.lc7$b r0 = r10.e
                    com.fossil.lc7 r0 = com.fossil.lc7.this
                    com.fossil.gd7 r0 = r0.c
                    if (r0 == 0) goto L_0x0020
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x0020
                    return r1
                L_0x0020:
                    r0 = 1
                    r10.b = r0
                    java.io.File r0 = r10.a()
                    return r0
                L_0x0028:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L_0x004d
                    int r2 = r10.d
                    if (r0 == 0) goto L_0x0049
                    int r0 = r0.length
                    if (r2 >= r0) goto L_0x0034
                    goto L_0x004d
                L_0x0034:
                    com.fossil.lc7$b r0 = r10.e
                    com.fossil.lc7 r0 = com.fossil.lc7.this
                    com.fossil.gd7 r0 = r0.d
                    if (r0 == 0) goto L_0x0048
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    com.fossil.i97 r0 = (com.fossil.i97) r0
                L_0x0048:
                    return r1
                L_0x0049:
                    com.fossil.ee7.a()
                    throw r1
                L_0x004d:
                    java.io.File[] r0 = r10.c
                    if (r0 != 0) goto L_0x00a3
                    java.io.File r0 = r10.a()
                    java.io.File[] r0 = r0.listFiles()
                    r10.c = r0
                    if (r0 != 0) goto L_0x0080
                    com.fossil.lc7$b r0 = r10.e
                    com.fossil.lc7 r0 = com.fossil.lc7.this
                    com.fossil.kd7 r0 = r0.e
                    if (r0 == 0) goto L_0x0080
                    java.io.File r2 = r10.a()
                    com.fossil.fc7 r9 = new com.fossil.fc7
                    java.io.File r4 = r10.a()
                    r5 = 0
                    r7 = 2
                    r8 = 0
                    java.lang.String r6 = "Cannot list files in a directory"
                    r3 = r9
                    r3.<init>(r4, r5, r6, r7, r8)
                    java.lang.Object r0 = r0.invoke(r2, r9)
                    com.fossil.i97 r0 = (com.fossil.i97) r0
                L_0x0080:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L_0x008e
                    if (r0 == 0) goto L_0x008a
                    int r0 = r0.length
                    if (r0 != 0) goto L_0x00a3
                    goto L_0x008e
                L_0x008a:
                    com.fossil.ee7.a()
                    throw r1
                L_0x008e:
                    com.fossil.lc7$b r0 = r10.e
                    com.fossil.lc7 r0 = com.fossil.lc7.this
                    com.fossil.gd7 r0 = r0.d
                    if (r0 == 0) goto L_0x00a2
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    com.fossil.i97 r0 = (com.fossil.i97) r0
                L_0x00a2:
                    return r1
                L_0x00a3:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L_0x00b0
                    int r1 = r10.d
                    int r2 = r1 + 1
                    r10.d = r2
                    r0 = r0[r1]
                    return r0
                L_0x00b0:
                    com.fossil.ee7.a()
                    throw r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.lc7.b.c.b():java.io.File");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
            if (lc7.this.a.isDirectory()) {
                this.c.push(a(lc7.this.a));
            } else if (lc7.this.a.isFile()) {
                this.c.push(new C0110b(this, lc7.this.a));
            } else {
                b();
            }
        }

        @DexIgnore
        @Override // com.fossil.n97
        public void a() {
            File d2 = d();
            if (d2 != null) {
                b(d2);
            } else {
                b();
            }
        }

        @DexIgnore
        public final File d() {
            File b;
            while (true) {
                c peek = this.c.peek();
                if (peek == null) {
                    return null;
                }
                b = peek.b();
                if (b == null) {
                    this.c.pop();
                } else if (ee7.a(b, peek.a()) || !b.isDirectory() || this.c.size() >= lc7.this.f) {
                    return b;
                } else {
                    this.c.push(a(b));
                }
            }
            return b;
        }

        @DexIgnore
        public final a a(File file) {
            int i = mc7.a[lc7.this.b.ordinal()];
            if (i == 1) {
                return new c(this, file);
            }
            if (i == 2) {
                return new a(this, file);
            }
            throw new p87();
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public lc7(File file, nc7 nc7) {
        this(file, nc7, null, null, null, 0, 32, null);
        ee7.b(file, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ee7.b(nc7, "direction");
    }
}
