package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b97 implements Collection<a97>, ye7 {
    @DexIgnore
    public /* final */ int[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends wa7 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ int[] b;

        @DexIgnore
        public a(int[] iArr) {
            ee7.b(iArr, "array");
            this.b = iArr;
        }

        @DexIgnore
        @Override // com.fossil.wa7
        public int a() {
            int i = this.a;
            int[] iArr = this.b;
            if (i < iArr.length) {
                this.a = i + 1;
                int i2 = iArr[i];
                a97.c(i2);
                return i2;
            }
            throw new NoSuchElementException(String.valueOf(this.a));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(int[] iArr, Object obj) {
        return (obj instanceof b97) && ee7.a(iArr, ((b97) obj).b());
    }

    @DexIgnore
    public static int b(int[] iArr) {
        if (iArr != null) {
            return Arrays.hashCode(iArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(int[] iArr) {
        return iArr.length == 0;
    }

    @DexIgnore
    public static wa7 d(int[] iArr) {
        return new a(iArr);
    }

    @DexIgnore
    public static String e(int[] iArr) {
        return "UIntArray(storage=" + Arrays.toString(iArr) + ")";
    }

    @DexIgnore
    public boolean a(int i) {
        return a(this.a, i);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(a97 a97) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends a97> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ int[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof a97) {
            return a(((a97) obj).a());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<a97>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.wa7' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<a97> iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i) {
        return t97.a(iArr, i);
    }

    @DexIgnore
    public static boolean a(int[] iArr, Collection<a97> collection) {
        boolean z;
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof a97) || !t97.a(iArr, t.a())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }
}
