package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn3 extends i72 implements i12 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nn3> CREATOR; // = new pn3();
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public nn3(List<String> list, String str) {
        this.a = list;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public final Status a() {
        if (this.b != null) {
            return Status.e;
        }
        return Status.i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.b(parcel, 1, this.a, false);
        k72.a(parcel, 2, this.b, false);
        k72.a(parcel, a2);
    }
}
