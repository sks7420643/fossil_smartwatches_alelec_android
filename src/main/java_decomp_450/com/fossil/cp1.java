package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp1 extends s81 {
    @DexIgnore
    public /* final */ byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cp1(ri1 ri1, en0 en0, byte[] bArr, String str, int i) {
        super(ri1, en0, wm0.A0, true, gq0.b.b(ri1.u, pb1.UI_ENCRYPTED_FILE), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, 64);
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(yz0.a(super.i(), r51.D0, Integer.valueOf(this.T.length)), r51.S0, Long.valueOf(ik1.a.a(this.T, ng1.CRC32)));
    }
}
