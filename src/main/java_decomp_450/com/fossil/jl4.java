package com.fossil;

import android.os.Build;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jl4 {
    @DexIgnore
    public jl4() {
        Executors.newFixedThreadPool(Build.MANUFACTURER.equalsIgnoreCase(LocationUtils.HUAWEI_MODEL) ? 1 : 5);
    }

    @DexIgnore
    public static void a() {
        new jl4();
    }
}
