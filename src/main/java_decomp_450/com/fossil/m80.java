package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int c; // = 65535;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<m80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final m80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new m80(yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 2"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public m80 createFromParcel(Parcel parcel) {
            return new m80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public m80[] newArray(int i) {
            return new m80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public m80 m40createFromParcel(Parcel parcel) {
            return new m80(parcel, null);
        }
    }

    /*
    static {
        ve7 ve7 = ve7.a;
    }
    */

    @DexIgnore
    public m80(int i) throws IllegalArgumentException {
        super(o80.DAILY_TOTAL_ACTIVE_MINUTE);
        this.b = i;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.b).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int i = c;
        int i2 = this.b;
        if (!(i2 >= 0 && i >= i2)) {
            StringBuilder b2 = yh0.b("minute(");
            b2.append(this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            throw new IllegalArgumentException(yh0.a(b2, c, "]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(m80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((m80) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
    }

    @DexIgnore
    public final int getMinute() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public Integer d() {
        return Integer.valueOf(this.b);
    }

    @DexIgnore
    public /* synthetic */ m80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readInt();
        e();
    }
}
