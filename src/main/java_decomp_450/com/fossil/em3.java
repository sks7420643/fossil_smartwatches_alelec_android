package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<em3> CREATOR; // = new hm3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Long d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Double g;

    @DexIgnore
    public em3(gm3 gm3) {
        this(gm3.c, gm3.d, gm3.e, gm3.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b, false);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d, false);
        k72.a(parcel, 5, (Float) null, false);
        k72.a(parcel, 6, this.e, false);
        k72.a(parcel, 7, this.f, false);
        k72.a(parcel, 8, this.g, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final Object zza() {
        Long l = this.d;
        if (l != null) {
            return l;
        }
        Double d2 = this.g;
        if (d2 != null) {
            return d2;
        }
        String str = this.e;
        if (str != null) {
            return str;
        }
        return null;
    }

    @DexIgnore
    public em3(String str, long j, Object obj, String str2) {
        a72.b(str);
        this.a = 2;
        this.b = str;
        this.c = j;
        this.f = str2;
        if (obj == null) {
            this.d = null;
            this.g = null;
            this.e = null;
        } else if (obj instanceof Long) {
            this.d = (Long) obj;
            this.g = null;
            this.e = null;
        } else if (obj instanceof String) {
            this.d = null;
            this.g = null;
            this.e = (String) obj;
        } else if (obj instanceof Double) {
            this.d = null;
            this.g = (Double) obj;
            this.e = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    @DexIgnore
    public em3(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        this.a = i;
        this.b = str;
        this.c = j;
        this.d = l;
        if (i == 1) {
            this.g = f2 != null ? Double.valueOf(f2.doubleValue()) : null;
        } else {
            this.g = d2;
        }
        this.e = str2;
        this.f = str3;
    }
}
