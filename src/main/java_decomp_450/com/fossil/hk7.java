package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hk7 extends ok7<ik7> {
    @DexIgnore
    public /* final */ gd7<Throwable, i97> e;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.gd7<? super java.lang.Throwable, com.fossil.i97> */
    /* JADX WARN: Multi-variable type inference failed */
    public hk7(ik7 ik7, gd7<? super Throwable, i97> gd7) {
        super(ik7);
        this.e = gd7;
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void b(Throwable th) {
        this.e.invoke(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        b(th);
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public String toString() {
        return "InvokeOnCompletion[" + ej7.a(this) + '@' + ej7.b(this) + ']';
    }
}
