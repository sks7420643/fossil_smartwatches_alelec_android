package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class an extends pm {
    @DexIgnore
    public static /* final */ String j; // = im.a("WorkContinuationImpl");
    @DexIgnore
    public /* final */ dn a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ dm c;
    @DexIgnore
    public /* final */ List<? extends sm> d;
    @DexIgnore
    public /* final */ List<String> e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<an> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public lm i;

    @DexIgnore
    public an(dn dnVar, List<? extends sm> list) {
        this(dnVar, null, dm.KEEP, list, null);
    }

    @DexIgnore
    public lm a() {
        if (!this.h) {
            hp hpVar = new hp(this);
            this.a.g().a(hpVar);
            this.i = hpVar.b();
        } else {
            im.a().e(j, String.format("Already enqueued work ids (%s)", TextUtils.join(", ", this.e)), new Throwable[0]);
        }
        return this.i;
    }

    @DexIgnore
    public dm b() {
        return this.c;
    }

    @DexIgnore
    public List<String> c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public List<an> e() {
        return this.g;
    }

    @DexIgnore
    public List<? extends sm> f() {
        return this.d;
    }

    @DexIgnore
    public dn g() {
        return this.a;
    }

    @DexIgnore
    public boolean h() {
        return a(this, new HashSet());
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h = true;
    }

    @DexIgnore
    public an(dn dnVar, String str, dm dmVar, List<? extends sm> list) {
        this(dnVar, str, dmVar, list, null);
    }

    @DexIgnore
    public an(dn dnVar, String str, dm dmVar, List<? extends sm> list, List<an> list2) {
        this.a = dnVar;
        this.b = str;
        this.c = dmVar;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(this.d.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (an anVar : list2) {
                this.f.addAll(anVar.f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String b2 = ((sm) list.get(i2)).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }

    @DexIgnore
    public static boolean a(an anVar, Set<String> set) {
        set.addAll(anVar.c());
        Set<String> a2 = a(anVar);
        for (String str : set) {
            if (a2.contains(str)) {
                return true;
            }
        }
        List<an> e2 = anVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (an anVar2 : e2) {
                if (a(anVar2, set)) {
                    return true;
                }
            }
        }
        set.removeAll(anVar.c());
        return false;
    }

    @DexIgnore
    public static Set<String> a(an anVar) {
        HashSet hashSet = new HashSet();
        List<an> e2 = anVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (an anVar2 : e2) {
                hashSet.addAll(anVar2.c());
            }
        }
        return hashSet;
    }
}
