package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.fl4;
import com.fossil.sn6;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn6 extends el4 {
    @DexIgnore
    public /* final */ LiveData<List<WorkoutSetting>> e; // = ed.a(null, 0, new d(this, null), 3, null);
    @DexIgnore
    public /* final */ MutableLiveData<b> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ sn6 g;
    @DexIgnore
    public /* final */ WorkoutSettingRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        WAITING_FOR_CONFIRMATION,
        SKIP,
        SUCCESS,
        FAIL_DEVICE_DISCONNECT,
        FAIL_UNKNOWN
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$checkDataChanged$1", f = "WorkoutSettingViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $newList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zn6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zn6 zn6, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zn6;
            this.$newList = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$newList, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                WorkoutSettingRepository a2 = this.this$0.h;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.getWorkoutSettingList(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.a((List) obj, this.$newList)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is changed, ask user to set data to watch");
                el4.a(this.this$0, false, true, 1, null);
                this.this$0.e().a(b.WAITING_FOR_CONFIRMATION);
            } else {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is not changed, just skip");
                el4.a(this.this$0, false, true, 1, null);
                this.this$0.e().a(b.SKIP);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$mWorkoutSettingsLiveData$1", f = "WorkoutSettingViewModel.kt", l = {20, 20}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<vd<List<? extends WorkoutSetting>>, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public vd p$;
        @DexIgnore
        public /* final */ /* synthetic */ zn6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zn6 zn6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zn6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (vd) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(vd<List<? extends WorkoutSetting>> vdVar, fb7<? super i97> fb7) {
            return ((d) create(vdVar, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            vd vdVar;
            vd vdVar2;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                vdVar2 = this.p$;
                WorkoutSettingRepository a2 = this.this$0.h;
                this.L$0 = vdVar2;
                this.L$1 = vdVar2;
                this.label = 1;
                obj = a2.getWorkoutSettingList(this);
                if (obj == a) {
                    return a;
                }
                vdVar = vdVar2;
            } else if (i == 1) {
                vdVar2 = (vd) this.L$1;
                vdVar = (vd) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                vd vdVar3 = (vd) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.L$0 = vdVar;
            this.label = 2;
            if (vdVar2.a(obj, this) == a) {
                return a;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<sn6.d, sn6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ zn6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(zn6 zn6) {
            this.a = zn6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sn6.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch success");
            el4.a(this.a, false, true, 1, null);
            this.a.e().a(b.SUCCESS);
        }

        @DexIgnore
        public void a(sn6.c cVar) {
            ee7.b(cVar, "errorValue");
            el4.a(this.a, false, true, 1, null);
            if (!xg5.b.a(PortfolioApp.g0.c().getApplicationContext(), xg5.c.BLUETOOTH_CONNECTION)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch fail, missed permissions");
                this.a.a(ib5.BLUETOOTH_OFF);
                return;
            }
            int b = cVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingViewModel", "set WorkoutSetting to Watch - onError - lastErrorCode = " + b);
            if (b == 1101) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(cVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026de(errorValue.errorCodes)");
                zn6 zn6 = this.a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    zn6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (b != 8888) {
                this.a.e().a(b.FAIL_UNKNOWN);
            } else {
                this.a.e().a(b.FAIL_DEVICE_DISCONNECT);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public zn6(sn6 sn6, WorkoutSettingRepository workoutSettingRepository) {
        ee7.b(sn6, "mSetWorkoutSetting");
        ee7.b(workoutSettingRepository, "mWorkoutSettingRepository");
        this.g = sn6;
        this.h = workoutSettingRepository;
        this.g.d();
    }

    @DexIgnore
    public final void b(List<WorkoutSetting> list) {
        ee7.b(list, "workoutSettingList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch()");
        el4.a(this, true, false, 2, null);
        this.g.a(new sn6.b(list), new e(this));
    }

    @DexIgnore
    public final MutableLiveData<b> e() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<List<WorkoutSetting>> f() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        this.g.e();
        super.onCleared();
    }

    @DexIgnore
    public final void a(List<WorkoutSetting> list) {
        ee7.b(list, "newList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "checkDataChanged()");
        el4.a(this, true, false, 2, null);
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, list, null), 3, null);
    }

    @DexIgnore
    public final boolean a(List<WorkoutSetting> list, List<WorkoutSetting> list2) {
        for (WorkoutSetting workoutSetting : list2) {
            Iterator<WorkoutSetting> it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    WorkoutSetting next = it.next();
                    if (workoutSetting.getType() == next.getType() && (workoutSetting.getAskMeFirst() != next.getAskMeFirst() || workoutSetting.getEnable() != next.getEnable())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
