package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pu implements cv {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public pu() {
        this(2500, 1, 1.0f);
    }

    @DexIgnore
    @Override // com.fossil.cv
    public int a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.cv
    public int b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.b <= this.c;
    }

    @DexIgnore
    public pu(int i, int i2, float f) {
        this.a = i;
        this.c = i2;
        this.d = f;
    }

    @DexIgnore
    @Override // com.fossil.cv
    public void a(fv fvVar) throws fv {
        this.b++;
        int i = this.a;
        this.a = i + ((int) (((float) i) * this.d));
        if (!c()) {
            throw fvVar;
        }
    }
}
