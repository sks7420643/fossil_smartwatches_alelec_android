package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv5 implements Factory<fv5> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;
    @DexIgnore
    public /* final */ Provider<dm5> b;

    @DexIgnore
    public gv5(Provider<QuickResponseRepository> provider, Provider<dm5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static gv5 a(Provider<QuickResponseRepository> provider, Provider<dm5> provider2) {
        return new gv5(provider, provider2);
    }

    @DexIgnore
    public static fv5 a(QuickResponseRepository quickResponseRepository, dm5 dm5) {
        return new fv5(quickResponseRepository, dm5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fv5 get() {
        return a(this.a.get(), this.b.get());
    }
}
