package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr1 extends zk0 {
    @DexIgnore
    public /* final */ rf0 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fr1(ri1 ri1, en0 en0, rf0 rf0, String str, int i) {
        super(ri1, en0, wm0.H0, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = rf0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        za0 c = this.C.c();
        if (c == null || c.e()) {
            a(this.C);
            return;
        }
        zr0 zr0 = zr0.d;
        r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.ASSET.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.I0, true, 1796, zr0.a((short) 1796, r60, (Object) new za0[]{this.C.c()}), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new hn1(this), new gp1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public final void a(rf0 rf0) {
        zk0.a(this, new dn1(((zk0) this).w, ((zk0) this).x, rf0, pb1.UI_SCRIPT, null, 16), new lj1(this), new il1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }
}
