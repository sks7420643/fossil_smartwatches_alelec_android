package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n54 extends v54.d.AbstractC0206d.a.b.c {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> c;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.a.b.c d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.c.AbstractC0211a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> c;
        @DexIgnore
        public v54.d.AbstractC0206d.a.b.c d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c.AbstractC0211a a(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c.AbstractC0211a b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c.AbstractC0211a a(w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> w54) {
            if (w54 != null) {
                this.c = w54;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c.AbstractC0211a a(v54.d.AbstractC0206d.a.b.c cVar) {
            this.d = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c.AbstractC0211a a(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.c.AbstractC0211a
        public v54.d.AbstractC0206d.a.b.c a() {
            String str = "";
            if (this.a == null) {
                str = str + " type";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (this.e == null) {
                str = str + " overflowCount";
            }
            if (str.isEmpty()) {
                return new n54(this.a, this.b, this.c, this.d, this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.c
    public v54.d.AbstractC0206d.a.b.c a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.c
    public w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.c
    public int c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.c
    public String d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.c
    public String e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        v54.d.AbstractC0206d.a.b.c cVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b.c)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b.c cVar2 = (v54.d.AbstractC0206d.a.b.c) obj;
        if (!this.a.equals(cVar2.e()) || ((str = this.b) != null ? !str.equals(cVar2.d()) : cVar2.d() != null) || !this.c.equals(cVar2.b()) || ((cVar = this.d) != null ? !cVar.equals(cVar2.a()) : cVar2.a() != null) || this.e != cVar2.c()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        String str = this.b;
        int i = 0;
        int hashCode2 = (((hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003) ^ this.c.hashCode()) * 1000003;
        v54.d.AbstractC0206d.a.b.c cVar = this.d;
        if (cVar != null) {
            i = cVar.hashCode();
        }
        return ((hashCode2 ^ i) * 1000003) ^ this.e;
    }

    @DexIgnore
    public String toString() {
        return "Exception{type=" + this.a + ", reason=" + this.b + ", frames=" + this.c + ", causedBy=" + this.d + ", overflowCount=" + this.e + "}";
    }

    @DexIgnore
    public n54(String str, String str2, w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> w54, v54.d.AbstractC0206d.a.b.c cVar, int i) {
        this.a = str;
        this.b = str2;
        this.c = w54;
        this.d = cVar;
        this.e = i;
    }
}
