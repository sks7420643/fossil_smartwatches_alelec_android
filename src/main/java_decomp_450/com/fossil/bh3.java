package com.fossil;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh3 {
    @DexIgnore
    public /* final */ oh3 a;

    @DexIgnore
    public bh3(oh3 oh3) {
        this.a = oh3;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.isEmpty()) {
            this.a.e().x().a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.a.c().g();
        if (!a()) {
            this.a.e().z().a("Install Referrer Reporter is not available");
            return;
        }
        eh3 eh3 = new eh3(this, str);
        this.a.c().g();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.a.f().getPackageManager();
        if (packageManager == null) {
            this.a.e().x().a("Failed to obtain Package Manager to verify binding conditions for Install Referrer");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.a.e().z().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (serviceInfo.name == null || !"com.android.vending".equals(str2) || !a()) {
                this.a.e().w().a("Play Store version 8.3.73 or higher required for Install Referrer");
                return;
            }
            try {
                this.a.e().B().a("Install Referrer Service is", e92.a().a(this.a.f(), new Intent(intent), eh3, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.a.e().t().a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        try {
            ia2 b = ja2.b(this.a.f());
            if (b == null) {
                this.a.e().B().a("Failed to get PackageManager for Install Referrer Play Store compatibility check");
                return false;
            } else if (b.b("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            this.a.e().B().a("Failed to retrieve Play Store version for Install Referrer", e);
            return false;
        }
    }

    @DexIgnore
    public final Bundle a(String str, xq2 xq2) {
        this.a.c().g();
        if (xq2 == null) {
            this.a.e().w().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle a2 = xq2.a(bundle);
            if (a2 != null) {
                return a2;
            }
            this.a.e().t().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.a.e().t().a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
