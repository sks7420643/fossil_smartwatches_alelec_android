package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o41 extends bn0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> E; // = yz0.a(((bn0) this).C, w97.a((Object[]) new ul0[]{ul0.ASYNC}));
    @DexIgnore
    public /* final */ w90 F;

    @DexIgnore
    public o41(ri1 ri1, en0 en0, w90 w90) {
        super(ri1, en0, wm0.n0, new nl1(w90, ri1));
        this.F = w90;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.bn0
    public ArrayList<ul0> f() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.bn0
    public JSONObject i() {
        return yz0.a(super.i(), r51.c4, this.F.a());
    }
}
