package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nd2 implements Parcelable.Creator<od2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ od2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        DataSet dataSet = null;
        IBinder iBinder = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                dataSet = (DataSet) j72.a(parcel, a, DataSet.CREATOR);
            } else if (a2 == 2) {
                iBinder = j72.p(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                z = j72.i(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new od2(dataSet, iBinder, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ od2[] newArray(int i) {
        return new od2[i];
    }
}
