package com.fossil;

import android.os.Parcel;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx0 extends xp0 {
    @DexIgnore
    public static /* final */ jv0 CREATOR; // = new jv0(null);
    @DexIgnore
    public /* final */ ov0[] d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore
    public bx0(byte b, ov0[] ov0Arr, byte[] bArr) {
        super(ru0.BACKGROUND_SYNC_EVENT, b, false, 4);
        this.d = ov0Arr;
        this.e = bArr;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (ov0 ov0 : this.d) {
            jSONArray.put(ov0.a());
        }
        return yz0.a(yz0.a(super.a(), r51.L1, jSONArray), r51.M1, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public byte[] b() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(bx0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            bx0 bx0 = (bx0) obj;
            return ((xp0) this).b == ((xp0) bx0).b && Arrays.equals(this.d, bx0.d) && Arrays.equals(this.e, bx0.e);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.e) + (((((xp0) this).b * 31) + this.d.hashCode()) * 31);
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(((xp0) this).b);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.d, i);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.e);
        }
    }

    @DexIgnore
    public /* synthetic */ bx0(Parcel parcel, zd7 zd7) {
        super(parcel);
        Object[] createTypedArray = parcel.createTypedArray(ov0.CREATOR);
        if (createTypedArray != null) {
            this.d = (ov0[]) createTypedArray;
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                this.e = createByteArray;
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
