package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wh7 {
    @DexIgnore
    public static /* synthetic */ Object a(ib7 ib7, kd7 kd7, int i, Object obj) throws InterruptedException {
        if ((i & 1) != 0) {
            ib7 = jb7.INSTANCE;
        }
        return vh7.a(ib7, kd7);
    }

    @DexIgnore
    public static final <T> T a(ib7 ib7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) throws InterruptedException {
        ib7 ib72;
        vj7 vj7;
        Thread currentThread = Thread.currentThread();
        gb7 gb7 = (gb7) ib7.get(gb7.m);
        if (gb7 == null) {
            vj7 = fl7.b.b();
            ib72 = si7.a(bk7.a, ib7.plus(vj7));
        } else {
            vj7 vj72 = null;
            if (!(gb7 instanceof vj7)) {
                gb7 = null;
            }
            vj7 vj73 = (vj7) gb7;
            if (vj73 != null) {
                if (vj73.q()) {
                    vj72 = vj73;
                }
                if (vj72 != null) {
                    vj7 = vj72;
                    ib72 = si7.a(bk7.a, ib7);
                }
            }
            vj7 = fl7.b.a();
            ib72 = si7.a(bk7.a, ib7);
        }
        th7 th7 = new th7(ib72, currentThread, vj7);
        th7.a(bj7.DEFAULT, th7, kd7);
        return (T) th7.p();
    }
}
