package com.fossil;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz implements az {
    @DexIgnore
    public /* final */ gz<a, Object> a; // = new gz<>();
    @DexIgnore
    public /* final */ b b; // = new b();
    @DexIgnore
    public /* final */ Map<Class<?>, NavigableMap<Integer, Integer>> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, zy<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cz<a> {
        @DexIgnore
        public a a(int i, Class<?> cls) {
            a aVar = (a) b();
            aVar.a(i, cls);
            return aVar;
        }

        @DexIgnore
        @Override // com.fossil.cz
        public a a() {
            return new a(this);
        }
    }

    @DexIgnore
    public iz(int i) {
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.az
    public synchronized <T> void a(T t) {
        Class<?> cls = t.getClass();
        zy<T> a2 = a((Class) cls);
        int a3 = a2.a(t);
        int a4 = a2.a() * a3;
        if (c(a4)) {
            a a5 = this.b.a(a3, cls);
            this.a.a(a5, t);
            NavigableMap<Integer, Integer> b2 = b(cls);
            Integer num = (Integer) b2.get(Integer.valueOf(a5.b));
            Integer valueOf = Integer.valueOf(a5.b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            b2.put(valueOf, Integer.valueOf(i));
            this.f += a4;
            b();
        }
    }

    @DexIgnore
    @Override // com.fossil.az
    public synchronized <T> T b(int i, Class<T> cls) {
        a aVar;
        Integer ceilingKey = b((Class<?>) cls).ceilingKey(Integer.valueOf(i));
        if (a(i, ceilingKey)) {
            aVar = this.b.a(ceilingKey.intValue(), cls);
        } else {
            aVar = this.b.a(i, cls);
        }
        return (T) a(aVar, cls);
    }

    @DexIgnore
    public final boolean c(int i) {
        return i <= this.e / 2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements lz {
        @DexIgnore
        public /* final */ b a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Class<?> c;

        @DexIgnore
        public a(b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(int i, Class<?> cls) {
            this.b = i;
            this.c = cls;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b == aVar.b && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b * 31;
            Class<?> cls = this.c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "Key{size=" + this.b + "array=" + this.c + '}';
        }

        @DexIgnore
        @Override // com.fossil.lz
        public void a() {
            this.a.a(this);
        }
    }

    @DexIgnore
    public final boolean c() {
        int i = this.f;
        return i == 0 || this.e / i >= 2;
    }

    @DexIgnore
    public final void c(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> b2 = b(cls);
        Integer num = (Integer) b2.get(Integer.valueOf(i));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            b2.remove(Integer.valueOf(i));
        } else {
            b2.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
        }
    }

    @DexIgnore
    public final void b() {
        b(this.e);
    }

    @DexIgnore
    public final void b(int i) {
        while (this.f > i) {
            Object a2 = this.a.a();
            u50.a(a2);
            zy b2 = b(a2);
            this.f -= b2.a(a2) * b2.a();
            c(b2.a(a2), a2.getClass());
            if (Log.isLoggable(b2.getTag(), 2)) {
                Log.v(b2.getTag(), "evicted: " + b2.a(a2));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.az
    public synchronized <T> T a(int i, Class<T> cls) {
        return (T) a(this.b.a(i, cls), cls);
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> b(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(cls, treeMap);
        return treeMap;
    }

    @DexIgnore
    public final <T> T a(a aVar, Class<T> cls) {
        zy<T> a2 = a((Class) cls);
        T t = (T) a(aVar);
        if (t != null) {
            this.f -= a2.a(t) * a2.a();
            c(a2.a(t), cls);
        }
        if (t != null) {
            return t;
        }
        if (Log.isLoggable(a2.getTag(), 2)) {
            Log.v(a2.getTag(), "Allocated " + aVar.b + " bytes");
        }
        return a2.newArray(aVar.b);
    }

    @DexIgnore
    public final <T> zy<T> b(T t) {
        return a((Class) t.getClass());
    }

    @DexIgnore
    public final <T> T a(a aVar) {
        return (T) this.a.a(aVar);
    }

    @DexIgnore
    public final boolean a(int i, Integer num) {
        return num != null && (c() || num.intValue() <= i * 8);
    }

    @DexIgnore
    @Override // com.fossil.az
    public synchronized void a() {
        b(0);
    }

    @DexIgnore
    @Override // com.fossil.az
    public synchronized void a(int i) {
        if (i >= 40) {
            try {
                a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i >= 20 || i == 15) {
            b(this.e / 2);
        }
    }

    @DexIgnore
    public final <T> zy<T> a(Class<T> cls) {
        zy<T> zyVar;
        fz fzVar;
        zy<T> zyVar2 = (zy<T>) this.d.get(cls);
        if (zyVar2 == null) {
            if (cls.equals(int[].class)) {
                fzVar = new hz();
            } else if (cls.equals(byte[].class)) {
                fzVar = new fz();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.d.put(cls, zyVar2);
        }
        return zyVar;
    }
}
