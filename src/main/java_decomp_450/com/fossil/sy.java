package com.fossil;

import com.fossil.hy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sy<Data, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ b9<List<Throwable>> a;
    @DexIgnore
    public /* final */ List<? extends hy<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public sy(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<hy<Data, ResourceType, Transcode>> list, b9<List<Throwable>> b9Var) {
        this.a = b9Var;
        u50.a((Collection) list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public uy<Transcode> a(jx<Data> jxVar, ax axVar, int i, int i2, hy.a<ResourceType> aVar) throws py {
        List<Throwable> a2 = this.a.a();
        u50.a((Object) a2);
        List<Throwable> list = a2;
        try {
            return a(jxVar, axVar, i, i2, aVar, list);
        } finally {
            this.a.a(list);
        }
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }

    @DexIgnore
    public final uy<Transcode> a(jx<Data> jxVar, ax axVar, int i, int i2, hy.a<ResourceType> aVar, List<Throwable> list) throws py {
        int size = this.b.size();
        uy<Transcode> uyVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                uyVar = ((hy) this.b.get(i3)).a(jxVar, i, i2, axVar, aVar);
            } catch (py e) {
                list.add(e);
            }
            if (uyVar != null) {
                break;
            }
        }
        if (uyVar != null) {
            return uyVar;
        }
        throw new py(this.c, new ArrayList(list));
    }
}
