package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f75 extends e75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131361905, 1);
        B.put(2131362517, 2);
        B.put(2131361902, 3);
        B.put(2131362121, 4);
        B.put(2131362751, 5);
        B.put(2131362450, 6);
        B.put(2131363197, 7);
        B.put(2131362941, 8);
    }
    */

    @DexIgnore
    public f75(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public f75(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleAutoCompleteTextView) objArr[3], (RTLImageView) objArr[1], (ImageView) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (View) objArr[5], (RecyclerView) objArr[8], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[7]);
        this.z = -1;
        ((e75) this).x.setTag(null);
        a(view);
        f();
    }
}
