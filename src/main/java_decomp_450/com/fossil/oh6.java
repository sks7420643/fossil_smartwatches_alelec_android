package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.fl4;
import com.fossil.mn5;
import com.fossil.on5;
import com.fossil.qn5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh6 extends mh6 {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ a G; // = new a(null);
    @DexIgnore
    public /* final */ SleepSummariesRepository A;
    @DexIgnore
    public /* final */ qn5 B;
    @DexIgnore
    public /* final */ fp4 C;
    @DexIgnore
    public /* final */ ro4 D;
    @DexIgnore
    public /* final */ ch5 E;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
    @DexIgnore
    public /* final */ ArrayList<b> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Device> g; // = new ArrayList<>();
    @DexIgnore
    public MFUser h;
    @DexIgnore
    public LiveData<qx6<ActivityStatistic>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<qx6<ActivitySummary>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<qx6<SleepStatistic>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ LiveData<List<Device>> l; // = this.x.getAllDeviceAsLiveData();
    @DexIgnore
    public ActivityStatistic.ActivityDailyBest m;
    @DexIgnore
    public long n;
    @DexIgnore
    public /* final */ kn7 o; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ Handler p; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean q; // = true;
    @DexIgnore
    public /* final */ Runnable r; // = new h(this);
    @DexIgnore
    public /* final */ g s; // = new g(this);
    @DexIgnore
    public /* final */ WeakReference<nh6> t;
    @DexIgnore
    public /* final */ PortfolioApp u;
    @DexIgnore
    public /* final */ mn5 v;
    @DexIgnore
    public /* final */ on5 w;
    @DexIgnore
    public /* final */ DeviceRepository x;
    @DexIgnore
    public /* final */ UserRepository y;
    @DexIgnore
    public /* final */ SummariesRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return oh6.F;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(String str, boolean z, String str2, int i, boolean z2, boolean z3) {
            ee7.b(str, "serial");
            ee7.b(str2, "deviceName");
            this.a = str;
            this.b = z;
            this.c = str2;
            this.d = i;
            this.e = z2;
            this.f = z3;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final boolean d() {
            return this.e;
        }

        @DexIgnore
        public final boolean e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && this.b == bVar.b && ee7.a(this.c, bVar.c) && this.d == bVar.d && this.e == bVar.e && this.f == bVar.f;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            boolean z = this.b;
            int i2 = 1;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i6 = (((i5 + i) * 31) + this.d) * 31;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z3 = this.f;
            if (z3 == 0) {
                i2 = z3;
            }
            return i9 + i2;
        }

        @DexIgnore
        public String toString() {
            return "DeviceWrapper(serial=" + this.a + ", isConnected=" + this.b + ", deviceName=" + this.c + ", batteryLevel=" + this.d + ", isActive=" + this.e + ", isLatestFw=" + this.f + ")";
        }

        @DexIgnore
        public final int a() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1", f = "HomeProfilePresenter.kt", l = {420}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAddNewDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1$challenge$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.D.a(new String[]{"running", "waiting"}, vt4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(oh6 oh6, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oh6;
            this.$isAddNewDevice = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$isAddNewDevice, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.q) {
                    ti7 b = qj7.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                } else {
                    nh6 nh6 = this.this$0.n().get();
                    if (nh6 != null) {
                        nh6.a(false, null, this.$isAddNewDevice);
                    }
                    return i97.a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mn4 mn4 = (mn4) obj;
            if (mn4 == null) {
                this.this$0.E.a(pb7.a(false));
                nh6 nh62 = this.this$0.n().get();
                if (nh62 != null) {
                    nh62.a(false, mn4, this.$isAddNewDevice);
                }
            } else {
                nh6 nh63 = this.this$0.n().get();
                if (nh63 != null) {
                    nh63.a(true, mn4, this.$isAddNewDevice);
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1", f = "HomeProfilePresenter.kt", l = {285}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1$socialProfile$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super fo4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super fo4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.C.b();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(oh6 oh6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oh6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            String str = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                oh6 oh6 = this.this$0;
                Boolean a3 = oh6.E.a();
                ee7.a((Object) a3, "sharedPreferencesManager.bcStatus()");
                oh6.q = a3.booleanValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = oh6.G.a();
                local.e(a4, "loadProfile - isBCOn: " + this.this$0.q);
                if (this.this$0.q) {
                    ti7 b = qj7.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                } else {
                    nh6 nh6 = this.this$0.n().get();
                    if (nh6 != null) {
                        nh6.J(null);
                    }
                    return i97.a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            fo4 fo4 = (fo4) obj;
            nh6 nh62 = this.this$0.n().get();
            if (nh62 != null) {
                if (fo4 != null) {
                    str = fo4.g();
                }
                nh62.J(str);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<mn5.a, fl4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ oh6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(oh6 oh6) {
            this.a = oh6;
        }

        @DexIgnore
        public void a(fl4.a aVar) {
            ee7.b(aVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mn5.a aVar) {
            ee7.b(aVar, "responseValue");
            MFUser a2 = aVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = oh6.G.a();
            local.d(a3, "loadUser " + a2);
            if (a2 != null) {
                this.a.a(a2);
                nh6 nh6 = this.a.n().get();
                if (nh6 != null) {
                    nh6.updateUser(a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements fl4.e<qn5.d, qn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ oh6 a;

        @DexIgnore
        public f(oh6 oh6) {
            this.a = oh6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oh6.G.a(), "logOut success, hide loading}");
            nh6 nh6 = this.a.n().get();
            if (nh6 != null) {
                nh6.h();
                nh6.p0();
            }
        }

        @DexIgnore
        public void a(qn5.c cVar) {
            ee7.b(cVar, "errorValue");
            nh6 nh6 = this.a.n().get();
            if (nh6 != null) {
                nh6.h();
                nh6.a(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ oh6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(oh6 oh6) {
            this.a = oh6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = oh6.G.a();
            local.d(a2, "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
            boolean z = true;
            if (mh7.b(stringExtra, this.a.u.c(), true) && (!this.a.l().isEmpty())) {
                Iterator<T> it = this.a.l().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.c(), (Object) stringExtra)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 == null) {
                    return;
                }
                if (intExtra == ConnectionStateChange.GATT_ON.ordinal() || intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                    if (intExtra != ConnectionStateChange.GATT_ON.ordinal()) {
                        z = false;
                    }
                    t2.a(z);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = oh6.G.a();
                    local2.d(a3, "active device status change connected=" + t2.e());
                    nh6 nh6 = this.a.n().get();
                    if (nh6 != null) {
                        nh6.b(this.a.l());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ oh6 a;

        @DexIgnore
        public h(oh6 oh6) {
            this.a = oh6;
        }

        @DexIgnore
        public final void run() {
            nh6 nh6;
            nh6 nh62 = this.a.n().get();
            Boolean valueOf = nh62 != null ? Boolean.valueOf(nh62.isActive()) : null;
            if (valueOf == null) {
                ee7.a();
                throw null;
            } else if (valueOf.booleanValue() && (!this.a.l().isEmpty()) && (nh6 = this.a.n().get()) != null) {
                nh6.d0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1", f = "HomeProfilePresenter.kt", l = {385, 391}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $imageUri;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $bitmap;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Bitmap bitmap, fb7 fb7) {
                super(2, fb7);
                this.$bitmap = bitmap;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$bitmap, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Bitmap bitmap = this.$bitmap;
                    if (bitmap != null) {
                        return sw6.a(bitmap);
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1$bitmap$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super Bitmap>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Bitmap> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return hd5.a(PortfolioApp.g0.c()).b().a(this.this$0.$imageUri).a(((r40) ((r40) new r40().a(iy.a)).a(true)).a((ex<Bitmap>) new vd5())).c(200, 200).get();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(oh6 oh6, Uri uri, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oh6;
            this.$imageUri = uri;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, this.$imageUri, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ti7 c = this.this$0.c();
                b bVar = new b(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(c, bVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                mFUser = (MFUser) this.L$2;
                Bitmap bitmap = (Bitmap) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                try {
                    t87.a(obj);
                    ee7.a(obj, "withContext(IO) { Bitmap\u2026ncodeToBase64(bitmap!!) }");
                    mFUser.setProfilePicture((String) obj);
                    this.this$0.b(this.this$0.m());
                } catch (Exception e) {
                    e.printStackTrace();
                    nh6 nh6 = this.this$0.n().get();
                    if (nh6 != null) {
                        nh6.C();
                    }
                }
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Bitmap bitmap2 = (Bitmap) obj;
            MFUser m = this.this$0.m();
            if (m != null) {
                ti7 c2 = this.this$0.c();
                a aVar = new a(bitmap2, null);
                this.L$0 = yi7;
                this.L$1 = bitmap2;
                this.L$2 = m;
                this.label = 2;
                obj = vh7.a(c2, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                mFUser = m;
                ee7.a(obj, "withContext(IO) { Bitmap\u2026ncodeToBase64(bitmap!!) }");
                mFUser.setProfilePicture((String) obj);
            }
            this.this$0.b(this.this$0.m());
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1", f = "HomeProfilePresenter.kt", l = {406}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$1$pickerImageIntent$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Intent>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ FragmentActivity $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(FragmentActivity fragmentActivity, fb7 fb7) {
                super(2, fb7);
                this.$it = fragmentActivity;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Intent> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return je5.b(this.$it);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(oh6 oh6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oh6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nh6 nh6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                nh6 nh62 = this.this$0.n().get();
                if (nh62 != null) {
                    if (nh62 != null) {
                        FragmentActivity activity = ((es5) nh62).getActivity();
                        if (activity != null) {
                            ti7 b = this.this$0.b();
                            a aVar = new a(activity, null);
                            this.L$0 = yi7;
                            this.L$1 = nh62;
                            this.L$2 = activity;
                            this.L$3 = activity;
                            this.label = 1;
                            obj = vh7.a(b, aVar, this);
                            if (obj == a2) {
                                return a2;
                            }
                            nh6 = nh62;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                    }
                }
                return i97.a;
            } else if (i == 1) {
                FragmentActivity fragmentActivity = (FragmentActivity) this.L$3;
                FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$2;
                nh6 = (nh6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Intent intent = (Intent) obj;
            if (intent != null) {
                ((es5) nh6).startActivityForResult(intent, 1234);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$1", f = "HomeProfilePresenter.kt", l = {158, 161}, m = "invokeSuspend")
    public static final class k extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qx6<? extends ActivitySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ k a;

            @DexIgnore
            public a(k kVar) {
                this.a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<ActivitySummary> qx6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = oh6.G.a();
                local.d(a2, "XXX- summaryChanged -- summary=" + qx6);
                ActivitySummary activitySummary = null;
                if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                    if (qx6 != null) {
                        activitySummary = qx6.c();
                    }
                    this.a.this$0.n = activitySummary != null ? (long) activitySummary.getSteps() : 0;
                    oh6 oh6 = this.a.this$0;
                    oh6.a(oh6.m, this.a.this$0.n);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements zd<qx6<? extends ActivityStatistic>> {
            @DexIgnore
            public /* final */ /* synthetic */ k a;

            @DexIgnore
            public b(k kVar) {
                this.a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<ActivityStatistic> qx6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = oh6.G.a();
                local.d(a2, "start - mActivityStatisticLiveData -- resource=" + qx6);
                ActivityStatistic.ActivityDailyBest activityDailyBest = null;
                if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                    ActivityStatistic c = qx6 != null ? qx6.c() : null;
                    nh6 nh6 = this.a.this$0.n().get();
                    if (nh6 != null) {
                        nh6.a(c);
                    }
                    oh6 oh6 = this.a.this$0;
                    if (c != null) {
                        activityDailyBest = c.getStepsBestDay();
                    }
                    oh6.m = activityDailyBest;
                    oh6 oh62 = this.a.this$0;
                    oh62.a(oh62.m, this.a.this$0.n);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements zd<qx6<? extends SleepStatistic>> {
            @DexIgnore
            public /* final */ /* synthetic */ k a;

            @DexIgnore
            public c(k kVar) {
                this.a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<SleepStatistic> qx6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = oh6.G.a();
                local.d(a2, "start - mSleepStatisticLiveData -- resource=" + qx6);
                SleepStatistic sleepStatistic = null;
                if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                    if (qx6 != null) {
                        qx6.c();
                    }
                    nh6 nh6 = this.a.this$0.n().get();
                    if (nh6 != null) {
                        if (qx6 != null) {
                            sleepStatistic = qx6.c();
                        }
                        nh6.a(sleepStatistic);
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d<T> implements zd<List<? extends Device>> {
            @DexIgnore
            public /* final */ /* synthetic */ k a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ ArrayList $deviceWrappers;
                @DexIgnore
                public /* final */ /* synthetic */ List $devices;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public Object L$4;
                @DexIgnore
                public Object L$5;
                @DexIgnore
                public boolean Z$0;
                @DexIgnore
                public boolean Z$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ d this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oh6$k$d$a$a")
                /* renamed from: com.fossil.oh6$k$d$a$a  reason: collision with other inner class name */
                public static final class C0151a extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Device $deviceModel;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0151a(Device device, fb7 fb7, a aVar) {
                        super(2, fb7);
                        this.$deviceModel = device;
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0151a aVar = new C0151a(this.$deviceModel, fb7, this.this$0);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                        return ((C0151a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            return this.this$0.this$0.a.this$0.x.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class b extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ String $activeSerial;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public b(String str, fb7 fb7) {
                        super(2, fb7);
                        this.$activeSerial = str;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        b bVar = new b(this.$activeSerial, fb7);
                        bVar.p$ = (yi7) obj;
                        return bVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                        return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            yw6 a2 = yw6.h.a();
                            String str = this.$activeSerial;
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = a2.a(str, null, this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(d dVar, List list, ArrayList arrayList, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = dVar;
                    this.$devices = list;
                    this.$deviceWrappers = arrayList;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$devices, this.$deviceWrappers, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:20:0x00ed  */
                /* JADX WARNING: Removed duplicated region for block: B:27:0x0147  */
                /* JADX WARNING: Removed duplicated region for block: B:31:0x0162 A[Catch:{ all -> 0x026a }] */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x0165 A[Catch:{ all -> 0x026a }] */
                /* JADX WARNING: Removed duplicated region for block: B:35:0x0175 A[Catch:{ all -> 0x026a }] */
                /* JADX WARNING: Removed duplicated region for block: B:75:0x0245 A[Catch:{ all -> 0x026a }] */
                /* JADX WARNING: Removed duplicated region for block: B:81:0x0283  */
                /* JADX WARNING: Removed duplicated region for block: B:84:0x02db  */
                /* JADX WARNING: Removed duplicated region for block: B:87:0x0302  */
                @Override // com.fossil.ob7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r28) {
                    /*
                        r27 = this;
                        r1 = r27
                        java.lang.String r0 = "release"
                        java.lang.Object r2 = com.fossil.nb7.a()
                        int r3 = r1.label
                        r4 = 3
                        r5 = 2
                        r6 = 0
                        r7 = 0
                        r8 = 1
                        if (r3 == 0) goto L_0x007c
                        if (r3 == r8) goto L_0x0070
                        if (r3 == r5) goto L_0x004e
                        if (r3 != r4) goto L_0x0046
                        boolean r3 = r1.Z$1
                        boolean r9 = r1.Z$0
                        java.lang.Object r10 = r1.L$5
                        java.lang.String r10 = (java.lang.String) r10
                        java.lang.Object r11 = r1.L$4
                        com.portfolio.platform.data.model.Device r11 = (com.portfolio.platform.data.model.Device) r11
                        java.lang.Object r12 = r1.L$3
                        java.util.Iterator r12 = (java.util.Iterator) r12
                        java.lang.Object r13 = r1.L$2
                        java.lang.String r13 = (java.lang.String) r13
                        java.lang.Object r14 = r1.L$1
                        com.fossil.kn7 r14 = (com.fossil.kn7) r14
                        java.lang.Object r15 = r1.L$0
                        com.fossil.yi7 r15 = (com.fossil.yi7) r15
                        com.fossil.t87.a(r28)     // Catch:{ all -> 0x006d }
                        r8 = r28
                        r21 = r10
                        r17 = r12
                        r25 = r14
                        r16 = r15
                        r15 = r1
                        r14 = r3
                        r12 = r9
                        r3 = r13
                        goto L_0x0154
                    L_0x0046:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r2)
                        throw r0
                    L_0x004e:
                        java.lang.Object r3 = r1.L$4
                        com.portfolio.platform.data.model.Device r3 = (com.portfolio.platform.data.model.Device) r3
                        java.lang.Object r9 = r1.L$3
                        java.util.Iterator r9 = (java.util.Iterator) r9
                        java.lang.Object r10 = r1.L$2
                        java.lang.String r10 = (java.lang.String) r10
                        java.lang.Object r11 = r1.L$1
                        r14 = r11
                        com.fossil.kn7 r14 = (com.fossil.kn7) r14
                        java.lang.Object r11 = r1.L$0
                        com.fossil.yi7 r11 = (com.fossil.yi7) r11
                        com.fossil.t87.a(r28)
                        r13 = r28
                        r12 = r9
                        r15 = r11
                        r11 = r1
                        goto L_0x011c
                    L_0x006d:
                        r0 = move-exception
                        goto L_0x0324
                    L_0x0070:
                        java.lang.Object r3 = r1.L$1
                        com.fossil.kn7 r3 = (com.fossil.kn7) r3
                        java.lang.Object r9 = r1.L$0
                        com.fossil.yi7 r9 = (com.fossil.yi7) r9
                        com.fossil.t87.a(r28)
                        goto L_0x0098
                    L_0x007c:
                        com.fossil.t87.a(r28)
                        com.fossil.yi7 r9 = r1.p$
                        com.fossil.oh6$k$d r3 = r1.this$0
                        com.fossil.oh6$k r3 = r3.a
                        com.fossil.oh6 r3 = r3.this$0
                        com.fossil.kn7 r3 = r3.o
                        r1.L$0 = r9
                        r1.L$1 = r3
                        r1.label = r8
                        java.lang.Object r10 = r3.a(r6, r1)
                        if (r10 != r2) goto L_0x0098
                        return r2
                    L_0x0098:
                        r14 = r3
                        com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                        com.fossil.oh6$a r10 = com.fossil.oh6.G
                        java.lang.String r10 = r10.a()
                        java.lang.StringBuilder r11 = new java.lang.StringBuilder
                        r11.<init>()
                        java.lang.String r12 = "updatedDevices "
                        r11.append(r12)
                        java.util.List r12 = r1.$devices
                        r11.append(r12)
                        java.lang.String r12 = " currentDevices "
                        r11.append(r12)
                        com.fossil.oh6$k$d r12 = r1.this$0
                        com.fossil.oh6$k r12 = r12.a
                        com.fossil.oh6 r12 = r12.this$0
                        java.util.ArrayList r12 = r12.g
                        r11.append(r12)
                        java.lang.String r11 = r11.toString()
                        r3.d(r10, r11)
                        com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                        com.portfolio.platform.PortfolioApp r3 = r3.c()
                        java.lang.String r3 = r3.c()
                        com.fossil.oh6$k$d r10 = r1.this$0
                        com.fossil.oh6$k r10 = r10.a
                        com.fossil.oh6 r10 = r10.this$0
                        r10.a(r3)
                        java.util.List r10 = r1.$devices
                        java.util.Iterator r10 = r10.iterator()
                        r11 = r1
                    L_0x00e7:
                        boolean r12 = r10.hasNext()
                        if (r12 == 0) goto L_0x026f
                        java.lang.Object r12 = r10.next()
                        com.portfolio.platform.data.model.Device r12 = (com.portfolio.platform.data.model.Device) r12
                        com.fossil.oh6$k$d r13 = r11.this$0
                        com.fossil.oh6$k r13 = r13.a
                        com.fossil.oh6 r13 = r13.this$0
                        com.fossil.ti7 r13 = r13.c()
                        com.fossil.oh6$k$d$a$a r15 = new com.fossil.oh6$k$d$a$a
                        r15.<init>(r12, r6, r11)
                        r11.L$0 = r9
                        r11.L$1 = r14
                        r11.L$2 = r3
                        r11.L$3 = r10
                        r11.L$4 = r12
                        r11.label = r5
                        java.lang.Object r13 = com.fossil.vh7.a(r13, r15, r11)
                        if (r13 != r2) goto L_0x0115
                        return r2
                    L_0x0115:
                        r15 = r9
                        r26 = r10
                        r10 = r3
                        r3 = r12
                        r12 = r26
                    L_0x011c:
                        r9 = r13
                        java.lang.String r9 = (java.lang.String) r9
                        com.fossil.oh6$k$d r13 = r11.this$0
                        com.fossil.oh6$k r13 = r13.a
                        com.fossil.oh6 r13 = r13.this$0
                        com.fossil.ti7 r13 = r13.b()
                        com.fossil.oh6$k$d$a$b r8 = new com.fossil.oh6$k$d$a$b
                        r8.<init>(r10, r6)
                        r11.L$0 = r15
                        r11.L$1 = r14
                        r11.L$2 = r10
                        r11.L$3 = r12
                        r11.L$4 = r3
                        r11.L$5 = r9
                        r11.Z$0 = r7
                        r11.Z$1 = r7
                        r11.label = r4
                        java.lang.Object r8 = com.fossil.vh7.a(r13, r8, r11)
                        if (r8 != r2) goto L_0x0147
                        return r2
                    L_0x0147:
                        r21 = r9
                        r17 = r12
                        r25 = r14
                        r16 = r15
                        r12 = 0
                        r14 = 0
                        r15 = r11
                        r11 = r3
                        r3 = r10
                    L_0x0154:
                        java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ all -> 0x026a }
                        boolean r24 = r8.booleanValue()     // Catch:{ all -> 0x026a }
                        int r8 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        r9 = 100
                        if (r8 <= r9) goto L_0x0165
                        r22 = 100
                        goto L_0x016b
                    L_0x0165:
                        int r8 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        r22 = r8
                    L_0x016b:
                        java.lang.String r8 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        boolean r8 = com.fossil.ee7.a(r8, r3)     // Catch:{ all -> 0x026a }
                        if (r8 == 0) goto L_0x0245
                        com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x026a }
                        com.portfolio.platform.PortfolioApp r8 = r8.c()     // Catch:{ all -> 0x026a }
                        java.lang.String r9 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        int r8 = r8.f(r9)     // Catch:{ all -> 0x026a }
                        if (r8 != r5) goto L_0x0188
                        r20 = 1
                        goto L_0x018a
                    L_0x0188:
                        r20 = 0
                    L_0x018a:
                        r23 = 1
                        java.util.ArrayList r8 = r15.$deviceWrappers     // Catch:{ all -> 0x026a }
                        com.fossil.oh6$b r9 = new com.fossil.oh6$b     // Catch:{ all -> 0x026a }
                        java.lang.String r19 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        r18 = r9
                        r18.<init>(r19, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x026a }
                        r8.add(r7, r9)     // Catch:{ all -> 0x026a }
                        com.fossil.oh6$k$d r8 = r15.this$0     // Catch:{ all -> 0x026a }
                        com.fossil.oh6$k r8 = r8.a     // Catch:{ all -> 0x026a }
                        com.fossil.oh6 r8 = r8.this$0     // Catch:{ all -> 0x026a }
                        java.lang.ref.WeakReference r8 = r8.n()     // Catch:{ all -> 0x026a }
                        java.lang.Object r8 = r8.get()     // Catch:{ all -> 0x026a }
                        com.fossil.nh6 r8 = (com.fossil.nh6) r8     // Catch:{ all -> 0x026a }
                        if (r8 == 0) goto L_0x0242
                        java.lang.String r9 = "debug"
                        boolean r9 = com.fossil.ee7.a(r0, r9)     // Catch:{ all -> 0x026a }
                        if (r9 != 0) goto L_0x01f2
                        java.lang.String r9 = "staging"
                        boolean r9 = com.fossil.ee7.a(r0, r9)     // Catch:{ all -> 0x026a }
                        if (r9 == 0) goto L_0x01bf
                        goto L_0x01f2
                    L_0x01bf:
                        java.lang.String r9 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        boolean r9 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r9)     // Catch:{ all -> 0x026a }
                        if (r9 == 0) goto L_0x01de
                        r9 = 10
                        int r10 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        r12 = 1
                        if (r12 <= r10) goto L_0x01d3
                        goto L_0x01da
                    L_0x01d3:
                        if (r9 < r10) goto L_0x01da
                        r8.c(r12, r12)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x01da:
                        r8.c(r7, r12)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x01de:
                        r12 = 1
                        r9 = 25
                        int r10 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        if (r12 <= r10) goto L_0x01e8
                        goto L_0x01ee
                    L_0x01e8:
                        if (r9 < r10) goto L_0x01ee
                        r8.c(r12, r7)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x01ee:
                        r8.c(r7, r7)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x01f2:
                        java.lang.String r9 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        boolean r9 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r9)     // Catch:{ all -> 0x026a }
                        if (r9 == 0) goto L_0x0220
                        int r9 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x026a }
                        com.portfolio.platform.PortfolioApp r10 = r10.c()     // Catch:{ all -> 0x026a }
                        com.fossil.ch5 r10 = r10.v()     // Catch:{ all -> 0x026a }
                        int r10 = r10.z()     // Catch:{ all -> 0x026a }
                        if (r9 >= r10) goto L_0x021b
                        int r9 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        if (r9 <= 0) goto L_0x021b
                        r9 = 1
                        r8.c(r9, r9)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x021b:
                        r9 = 1
                        r8.c(r7, r9)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x0220:
                        int r9 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x026a }
                        com.portfolio.platform.PortfolioApp r10 = r10.c()     // Catch:{ all -> 0x026a }
                        com.fossil.ch5 r10 = r10.v()     // Catch:{ all -> 0x026a }
                        int r10 = r10.z()     // Catch:{ all -> 0x026a }
                        if (r9 >= r10) goto L_0x023f
                        int r9 = r11.getBatteryLevel()     // Catch:{ all -> 0x026a }
                        if (r9 <= 0) goto L_0x023f
                        r9 = 1
                        r8.c(r9, r7)     // Catch:{ all -> 0x026a }
                        goto L_0x0242
                    L_0x023f:
                        r8.c(r7, r7)     // Catch:{ all -> 0x026a }
                    L_0x0242:
                        r19 = r15
                        goto L_0x025e
                    L_0x0245:
                        java.util.ArrayList r8 = r15.$deviceWrappers     // Catch:{ all -> 0x026a }
                        com.fossil.oh6$b r13 = new com.fossil.oh6$b     // Catch:{ all -> 0x026a }
                        java.lang.String r10 = r11.getDeviceId()     // Catch:{ all -> 0x026a }
                        r9 = r13
                        r11 = r12
                        r12 = r21
                        r4 = r13
                        r13 = r22
                        r19 = r15
                        r15 = r24
                        r9.<init>(r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x026a }
                        r8.add(r4)     // Catch:{ all -> 0x026a }
                    L_0x025e:
                        r9 = r16
                        r10 = r17
                        r11 = r19
                        r14 = r25
                        r4 = 3
                        r8 = 1
                        goto L_0x00e7
                    L_0x026a:
                        r0 = move-exception
                        r14 = r25
                        goto L_0x0324
                    L_0x026f:
                        java.util.ArrayList r0 = r11.$deviceWrappers
                        com.fossil.oh6$k$d r2 = r11.this$0
                        com.fossil.oh6$k r2 = r2.a
                        com.fossil.oh6 r2 = r2.this$0
                        java.util.ArrayList r2 = r2.l()
                        boolean r0 = com.fossil.ee7.a(r0, r2)
                        r2 = 1
                        r0 = r0 ^ r2
                        if (r0 == 0) goto L_0x029f
                        com.fossil.oh6$k$d r0 = r11.this$0
                        com.fossil.oh6$k r0 = r0.a
                        com.fossil.oh6 r0 = r0.this$0
                        java.util.ArrayList r0 = r0.l()
                        r0.clear()
                        com.fossil.oh6$k$d r0 = r11.this$0
                        com.fossil.oh6$k r0 = r0.a
                        com.fossil.oh6 r0 = r0.this$0
                        java.util.ArrayList r0 = r0.l()
                        java.util.ArrayList r2 = r11.$deviceWrappers
                        r0.addAll(r2)
                    L_0x029f:
                        com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                        com.fossil.oh6$a r2 = com.fossil.oh6.G
                        java.lang.String r2 = r2.a()
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder
                        r4.<init>()
                        java.lang.String r5 = "update new device list "
                        r4.append(r5)
                        com.fossil.oh6$k$d r5 = r11.this$0
                        com.fossil.oh6$k r5 = r5.a
                        com.fossil.oh6 r5 = r5.this$0
                        java.util.ArrayList r5 = r5.l()
                        r4.append(r5)
                        java.lang.String r4 = r4.toString()
                        r0.d(r2, r4)
                        com.fossil.oh6$k$d r0 = r11.this$0
                        com.fossil.oh6$k r0 = r0.a
                        com.fossil.oh6 r0 = r0.this$0
                        java.lang.ref.WeakReference r0 = r0.n()
                        java.lang.Object r0 = r0.get()
                        com.fossil.nh6 r0 = (com.fossil.nh6) r0
                        if (r0 == 0) goto L_0x02e8
                        com.fossil.oh6$k$d r2 = r11.this$0
                        com.fossil.oh6$k r2 = r2.a
                        com.fossil.oh6 r2 = r2.this$0
                        java.util.ArrayList r2 = r2.l()
                        r0.b(r2)
                    L_0x02e8:
                        com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                        com.portfolio.platform.PortfolioApp r0 = r0.c()
                        com.fossil.ch5 r0 = r0.v()
                        long r2 = r0.e(r3)
                        long r4 = java.lang.System.currentTimeMillis()
                        long r4 = r4 - r2
                        r2 = 60000(0xea60, double:2.9644E-319)
                        int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
                        if (r0 >= 0) goto L_0x030b
                        com.fossil.oh6$k$d r0 = r11.this$0
                        com.fossil.oh6$k r0 = r0.a
                        com.fossil.oh6 r0 = r0.this$0
                        r0.o()
                    L_0x030b:
                        com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                        com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                        com.fossil.oh6$a r2 = com.fossil.oh6.G
                        java.lang.String r2 = r2.a()
                        java.lang.String r3 = "updatedDevices done"
                        r0.d(r2, r3)
                        com.fossil.i97 r0 = com.fossil.i97.a
                        r14.a(r6)
                        com.fossil.i97 r0 = com.fossil.i97.a
                        return r0
                    L_0x0324:
                        r14.a(r6)
                        throw r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.oh6.k.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public d(k kVar) {
                this.a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(List<Device> list) {
                ArrayList arrayList = new ArrayList();
                this.a.this$0.g.clear();
                ArrayList h = this.a.this$0.g;
                if (list != null) {
                    h.addAll(list);
                    ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(this, list, arrayList, null), 3, null);
                    return;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(oh6 oh6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = oh6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            k kVar = new k(this.this$0, fb7);
            kVar.p$ = (yi7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((k) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00ac  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r3) goto L_0x0023
                if (r1 != r2) goto L_0x001b
                java.lang.Object r0 = r7.L$1
                com.fossil.oh6 r0 = (com.fossil.oh6) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x0099
            L_0x001b:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x0023:
                java.lang.Object r1 = r7.L$1
                com.fossil.oh6 r1 = (com.fossil.oh6) r1
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r8)
                goto L_0x004c
            L_0x002f:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r4 = r7.p$
                com.fossil.oh6 r1 = r7.this$0
                com.portfolio.platform.data.source.SummariesRepository r8 = r1.z
                java.util.Date r5 = new java.util.Date
                r5.<init>()
                r7.L$0 = r4
                r7.L$1 = r1
                r7.label = r3
                java.lang.Object r8 = r8.getSummary(r5, r7)
                if (r8 != r0) goto L_0x004c
                return r0
            L_0x004c:
                androidx.lifecycle.LiveData r8 = (androidx.lifecycle.LiveData) r8
                r1.j = r8
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.fossil.oh6$a r1 = com.fossil.oh6.G
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "XXX- summaryLiveObj "
                r5.append(r6)
                com.fossil.oh6 r6 = r7.this$0
                androidx.lifecycle.LiveData r6 = r6.j
                r5.append(r6)
                java.lang.String r5 = r5.toString()
                r8.d(r1, r5)
                com.fossil.oh6 r8 = r7.this$0
                com.portfolio.platform.data.source.SummariesRepository r1 = r8.z
                androidx.lifecycle.LiveData r1 = r1.getActivityStatistic(r3)
                r8.i = r1
                com.fossil.oh6 r8 = r7.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r1 = r8.A
                r7.L$0 = r4
                r7.L$1 = r8
                r7.label = r2
                java.lang.Object r1 = r1.getSleepStatistic(r3, r7)
                if (r1 != r0) goto L_0x0097
                return r0
            L_0x0097:
                r0 = r8
                r8 = r1
            L_0x0099:
                androidx.lifecycle.LiveData r8 = (androidx.lifecycle.LiveData) r8
                r0.k = r8
                com.fossil.oh6 r8 = r7.this$0
                java.lang.ref.WeakReference r8 = r8.n()
                java.lang.Object r8 = r8.get()
                com.fossil.nh6 r8 = (com.fossil.nh6) r8
                if (r8 == 0) goto L_0x00f4
                com.fossil.oh6 r0 = r7.this$0
                androidx.lifecycle.LiveData r0 = r0.j
                if (r8 == 0) goto L_0x00ec
                r1 = r8
                com.fossil.es5 r1 = (com.fossil.es5) r1
                com.fossil.oh6$k$a r2 = new com.fossil.oh6$k$a
                r2.<init>(r7)
                r0.a(r1, r2)
                com.fossil.oh6 r0 = r7.this$0
                androidx.lifecycle.LiveData r0 = r0.i
                androidx.lifecycle.LifecycleOwner r8 = (androidx.lifecycle.LifecycleOwner) r8
                com.fossil.oh6$k$b r1 = new com.fossil.oh6$k$b
                r1.<init>(r7)
                r0.a(r8, r1)
                com.fossil.oh6 r0 = r7.this$0
                androidx.lifecycle.LiveData r0 = r0.k
                com.fossil.oh6$k$c r1 = new com.fossil.oh6$k$c
                r1.<init>(r7)
                r0.a(r8, r1)
                com.fossil.oh6 r0 = r7.this$0
                androidx.lifecycle.LiveData r0 = r0.l
                com.fossil.oh6$k$d r1 = new com.fossil.oh6$k$d
                r1.<init>(r7)
                r0.a(r8, r1)
                goto L_0x00f4
            L_0x00ec:
                com.fossil.x87 r8 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment"
                r8.<init>(r0)
                throw r8
            L_0x00f4:
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.oh6.k.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ oh6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ nh6 $view;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oh6$l$a$a")
            /* renamed from: com.fossil.oh6$l$a$a  reason: collision with other inner class name */
            public static final class C0152a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0152a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0152a aVar = new C0152a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                    return ((C0152a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        UserRepository p = this.this$0.this$0.a.y;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = p.getCurrentUser(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(nh6 nh6, fb7 fb7, l lVar) {
                super(2, fb7);
                this.$view = nh6;
                this.this$0 = lVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$view, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 c = this.this$0.a.c();
                    C0152a aVar = new C0152a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(c, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFUser mFUser = (MFUser) obj;
                if (mFUser != null) {
                    this.$view.updateUser(mFUser);
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public l(oh6 oh6) {
            this.a = oh6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(oh6.G.a(), ".Inside updateUser onSuccess");
            nh6 nh6 = this.a.n().get();
            if (nh6 != null) {
                ee7.a((Object) nh6, "view");
                if (nh6.isActive()) {
                    nh6.h();
                    ik7 unused = xh7.b(this.a.e(), null, null, new a(nh6, null, this), 3, null);
                }
            }
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = oh6.G.a();
            local.d(a2, ".Inside updateUser onError errorCode=" + cVar.a());
            nh6 nh6 = this.a.n().get();
            if (nh6 != null) {
                ee7.a((Object) nh6, "view");
                if (nh6.isActive()) {
                    nh6.h();
                    nh6.a(cVar.a(), "");
                }
            }
        }
    }

    /*
    static {
        String simpleName = oh6.class.getSimpleName();
        ee7.a((Object) simpleName, "HomeProfilePresenter::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public oh6(WeakReference<nh6> weakReference, PortfolioApp portfolioApp, mn5 mn5, on5 on5, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, qn5 qn5, fp4 fp4, ro4 ro4, ch5 ch5) {
        ee7.b(weakReference, "mWeakRefView");
        ee7.b(portfolioApp, "mApp");
        ee7.b(mn5, "mGetUser");
        ee7.b(on5, "mUpdateUser");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(qn5, "mDeleteLogoutUserUseCase");
        ee7.b(fp4, "socialProfileRepository");
        ee7.b(ro4, "challengeRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        this.t = weakReference;
        this.u = portfolioApp;
        this.v = mn5;
        this.w = on5;
        this.x = deviceRepository;
        this.y = userRepository;
        this.z = summariesRepository;
        this.A = sleepSummariesRepository;
        this.B = qn5;
        this.C = fp4;
        this.D = ro4;
        this.E = ch5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        PortfolioApp portfolioApp = this.u;
        g gVar = this.s;
        portfolioApp.registerReceiver(gVar, new IntentFilter(this.u.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        p();
        i();
        ik7 unused = xh7.b(e(), null, null, new k(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        nh6 nh6 = this.t.get();
        if (nh6 != null) {
            LiveData<qx6<ActivitySummary>> liveData = this.j;
            if (nh6 != null) {
                liveData.a((es5) nh6);
                LifecycleOwner lifecycleOwner = (LifecycleOwner) nh6;
                this.l.a(lifecycleOwner);
                this.i.a(lifecycleOwner);
                this.k.a(lifecycleOwner);
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
            }
        }
        this.p.removeCallbacksAndMessages(null);
        try {
            this.u.unregisterReceiver(this.s);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "stop with " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public void i() {
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public void j() {
        nh6 nh6 = this.t.get();
        if (nh6 != null) {
            nh6.j();
            qn5 qn5 = this.B;
            if (nh6 != null) {
                FragmentActivity activity = ((es5) nh6).getActivity();
                if (activity != null) {
                    qn5.a(new qn5.b(1, new WeakReference(activity)), new f(this));
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.app.Activity");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public void k() {
        ik7 unused = xh7.b(e(), null, null, new j(this, null), 3, null);
    }

    @DexIgnore
    public final ArrayList<b> l() {
        return this.f;
    }

    @DexIgnore
    public final MFUser m() {
        return this.h;
    }

    @DexIgnore
    public final WeakReference<nh6> n() {
        return this.t;
    }

    @DexIgnore
    public final void o() {
        this.p.removeCallbacksAndMessages(null);
        this.p.postDelayed(this.r, 60000);
    }

    @DexIgnore
    public final void p() {
        this.v.a((fl4.b) null, new e(this));
    }

    @DexIgnore
    public void q() {
        nh6 nh6 = this.t.get();
        if (nh6 != null) {
            nh6.a(this);
        }
    }

    @DexIgnore
    public final void b(MFUser mFUser) {
        if (mFUser != null) {
            nh6 nh6 = this.t.get();
            if (nh6 != null) {
                nh6.j();
            }
            this.w.a(new on5.b(mFUser), new l(this));
        }
    }

    @DexIgnore
    public void b(Intent intent) {
        ee7.b(intent, "intent");
        nh6 nh6 = this.t.get();
        Boolean valueOf = nh6 != null ? Boolean.valueOf(nh6.isActive()) : null;
        if (valueOf == null) {
            ee7.a();
            throw null;
        } else if (valueOf.booleanValue()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, PortfolioApp.g0.c().c(), true)) {
                nh6 nh62 = this.t.get();
                if (nh62 != null) {
                    nh62.d0();
                }
                o();
            }
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.h = mFUser;
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public void a(Intent intent) {
        Uri a2 = je5.a(intent, PortfolioApp.g0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .onActivityResult imageUri=");
        if (a2 != null) {
            sb.append(a2);
            local.d(str, sb.toString());
            if (PortfolioApp.g0.c().a(intent, a2)) {
                ik7 unused = xh7.b(e(), null, null, new i(this, a2, null), 3, null);
                return;
            }
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.mh6
    public void a(boolean z2) {
        ik7 unused = xh7.b(e(), null, null, new c(this, z2, null), 3, null);
    }

    @DexIgnore
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "checkToHideWorkoutSettings, serial = " + str);
        boolean a2 = be5.o.a(FossilDeviceSerialPatternUtil.getDeviceBySerial(str));
        nh6 nh6 = this.t.get();
        if (nh6 != null) {
            nh6.x(!a2);
        }
    }

    @DexIgnore
    public final void a(ActivityStatistic.ActivityDailyBest activityDailyBest, long j2) {
        if (j2 > (activityDailyBest != null ? (long) activityDailyBest.getValue() : 0)) {
            lh6 lh6 = new lh6(new Date(), j2);
            nh6 nh6 = this.t.get();
            if (nh6 != null) {
                nh6.a(lh6);
            }
        }
    }
}
