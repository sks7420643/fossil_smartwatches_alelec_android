package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.FossilNotificationListenerService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj5 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, ek5 ek5) {
        fossilNotificationListenerService.h = ek5;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, mk5 mk5) {
        fossilNotificationListenerService.i = mk5;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, pk5 pk5) {
        fossilNotificationListenerService.j = pk5;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, pj4 pj4) {
        fossilNotificationListenerService.p = pj4;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, ch5 ch5) {
        fossilNotificationListenerService.q = ch5;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, UserRepository userRepository) {
        fossilNotificationListenerService.r = userRepository;
    }
}
