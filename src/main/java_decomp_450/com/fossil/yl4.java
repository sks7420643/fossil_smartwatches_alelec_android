package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ String a; // = eh5.l.a().b("onPrimaryButton");
    @DexIgnore
    public ArrayList<ez5> b;
    @DexIgnore
    public d c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ yl4 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(yl4 yl4, View view) {
            super(view);
            ee7.b(view, "view");
            this.c = yl4;
            View findViewById = view.findViewById(2131361960);
            ee7.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.a = (FlexibleButton) findViewById;
            View findViewById2 = view.findViewById(2131363239);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.b = (TextView) findViewById2;
            if (!TextUtils.isEmpty(yl4.c())) {
                int parseColor = Color.parseColor(yl4.c());
                Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230982);
                if (drawable != null) {
                    drawable.setTint(parseColor);
                    this.a.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
            bf5.a(this.a, this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.a.a("flexible_button_disabled");
                this.a.setClickable(false);
                this.b.setText(PortfolioApp.g0.c().getString(2131886548));
                return;
            }
            this.a.a("flexible_button_primary");
            this.a.setClickable(true);
            this.b.setText(PortfolioApp.g0.c().getString(2131886535));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == 2131361960) {
                this.c.c.y();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(ez5 ez5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void a(String str, String str2);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void x();

        @DexIgnore
        void y();
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public yl4(ArrayList<ez5> arrayList, d dVar) {
        ee7.b(arrayList, "mData");
        ee7.b(dVar, "mListener");
        this.b = arrayList;
        this.c = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) this.b.get(i).hashCode();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (i == this.b.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizePresetDetailAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            ez5 ez5 = this.b.get(i);
            ee7.a((Object) ez5, "mData[position]");
            ez5 ez52 = ez5;
            if (i != 0) {
                z = false;
            }
            bVar.a(ez52, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.b.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558696, viewGroup, false);
            ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558672, viewGroup, false);
        ee7.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(ArrayList<ez5> arrayList) {
        ee7.b(arrayList, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizePresetDetailAdapter", "setData - data=" + arrayList);
        this.b.clear();
        this.b.addAll(arrayList);
        this.b.add(new ez5("", "", new ArrayList(), new ArrayList(), false));
        notifyDataSetChanged();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public CustomizeWidget b;
        @DexIgnore
        public CustomizeWidget c;
        @DexIgnore
        public CustomizeWidget d;
        @DexIgnore
        public ImageView e;
        @DexIgnore
        public TextView f;
        @DexIgnore
        public View g;
        @DexIgnore
        public FlexibleButton h;
        @DexIgnore
        public ImageView i;
        @DexIgnore
        public ez5 j;
        @DexIgnore
        public View p;
        @DexIgnore
        public View q;
        @DexIgnore
        public View r;
        @DexIgnore
        public View s;
        @DexIgnore
        public /* final */ /* synthetic */ yl4 t;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yl4 yl4, View view) {
            super(view);
            ee7.b(view, "view");
            this.t = yl4;
            View findViewById = view.findViewById(2131363310);
            ee7.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            TextView textView = (TextView) findViewById;
            this.a = textView;
            textView.setOnClickListener(this);
            View findViewById2 = view.findViewById(2131363454);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.wa_top)");
            this.b = (CustomizeWidget) findViewById2;
            View findViewById3 = view.findViewById(2131363453);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.wa_middle)");
            this.c = (CustomizeWidget) findViewById3;
            View findViewById4 = view.findViewById(2131363452);
            ee7.a((Object) findViewById4, "view.findViewById(R.id.wa_bottom)");
            this.d = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(2131362738);
            ee7.a((Object) findViewById5, "view.findViewById(R.id.iv_watch_theme_background)");
            this.i = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(2131363401);
            ee7.a((Object) findViewById6, "view.findViewById(R.id.v_underline)");
            this.s = findViewById6;
            View findViewById7 = view.findViewById(2131362757);
            ee7.a((Object) findViewById7, "view.findViewById(R.id.line_bottom)");
            this.r = findViewById7;
            View findViewById8 = view.findViewById(2131362758);
            ee7.a((Object) findViewById8, "view.findViewById(R.id.line_center)");
            this.q = findViewById8;
            View findViewById9 = view.findViewById(2131362759);
            ee7.a((Object) findViewById9, "view.findViewById(R.id.line_top)");
            this.p = findViewById9;
            String b2 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                int parseColor = Color.parseColor(b2);
                this.s.setBackgroundColor(parseColor);
                this.r.setBackgroundColor(parseColor);
                this.q.setBackgroundColor(parseColor);
                this.p.setBackgroundColor(parseColor);
            }
            View findViewById10 = view.findViewById(2131362736);
            ee7.a((Object) findViewById10, "view.findViewById(R.id.iv_warning)");
            this.e = (ImageView) findViewById10;
            View findViewById11 = view.findViewById(2131362748);
            ee7.a((Object) findViewById11, "view.findViewById(R.id.layout_right)");
            this.g = findViewById11;
            View findViewById12 = view.findViewById(2131361971);
            ee7.a((Object) findViewById12, "view.findViewById(R.id.btn_right)");
            this.h = (FlexibleButton) findViewById12;
            View findViewById13 = view.findViewById(2131363283);
            ee7.a((Object) findViewById13, "view.findViewById(R.id.tv_left)");
            this.f = (TextView) findViewById13;
            this.g.setOnClickListener(this);
            this.f.setOnClickListener(this);
            this.h.setOnClickListener(this);
            this.b.setOnClickListener(this);
            this.c.setOnClickListener(this);
            this.d.setOnClickListener(this);
            this.e.setOnClickListener(this);
            if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.g0.c().c())) {
                this.i.setImageResource(2131230972);
            }
        }

        @DexIgnore
        public final List<a9<View, String>> a() {
            a9[] a9VarArr = new a9[6];
            TextView textView = this.a;
            a9VarArr[0] = new a9(textView, textView.getTransitionName());
            View view = this.s;
            a9VarArr[1] = new a9(view, view.getTransitionName());
            ImageView imageView = this.i;
            if (imageView != null) {
                a9VarArr[2] = new a9(imageView, imageView.getTransitionName());
                View view2 = this.r;
                a9VarArr[3] = new a9(view2, view2.getTransitionName());
                View view3 = this.q;
                a9VarArr[4] = new a9(view3, view3.getTransitionName());
                View view4 = this.p;
                a9VarArr[5] = new a9(view4, view4.getTransitionName());
                return w97.c(a9VarArr);
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<a9<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.b;
            CustomizeWidget customizeWidget2 = this.c;
            CustomizeWidget customizeWidget3 = this.d;
            return w97.c(new a9(customizeWidget, customizeWidget.getTransitionName()), new a9(customizeWidget2, customizeWidget2.getTransitionName()), new a9(customizeWidget3, customizeWidget3.getTransitionName()));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.ez5 */
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: com.fossil.ez5 */
        /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: com.fossil.ez5 */
        /* JADX WARN: Multi-variable type inference failed */
        @SuppressLint({"UseSparseArrays"})
        public void onClick(View view) {
            String str;
            String c2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.j == null) && view != null) {
                Object obj = null;
                switch (view.getId()) {
                    case 2131361971:
                        this.t.c.x();
                        return;
                    case 2131362736:
                        Iterator it = this.t.b.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Object next = it.next();
                                if (((ez5) next).e()) {
                                    obj = next;
                                }
                            }
                        }
                        ez5 ez5 = (ez5) obj;
                        if (ez5 != null) {
                            xg5 xg5 = xg5.b;
                            d b2 = this.t.c;
                            if (b2 != null) {
                                xg5.a(xg5, ((yr5) b2).getContext(), (List) ez5.b(), false, false, false, (Integer) null, 60, (Object) null);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
                        }
                        return;
                    case 2131363283:
                        if (this.t.b.size() > 2) {
                            Object obj2 = this.t.b.get(1);
                            ee7.a(obj2, "mData[1]");
                            ez5 ez52 = (ez5) obj2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("current ");
                            ez5 ez53 = this.j;
                            sb.append(ez53 != null ? ez53.d() : null);
                            sb.append(" nextPreset ");
                            sb.append(ez52.d());
                            local2.d("HybridCustomizePresetDetailAdapter", sb.toString());
                            d b3 = this.t.c;
                            ez5 ez54 = this.j;
                            if (ez54 != null) {
                                boolean e2 = ez54.e();
                                ez5 ez55 = this.j;
                                if (ez55 != null) {
                                    b3.b(e2, ez55.d(), ez52.d(), ez52.c());
                                    return;
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case 2131363310:
                        d b4 = this.t.c;
                        ez5 ez56 = this.j;
                        String str2 = "";
                        if (ez56 == null || (str = ez56.d()) == null) {
                            str = str2;
                        }
                        ez5 ez57 = this.j;
                        if (!(ez57 == null || (c2 = ez57.c()) == null)) {
                            str2 = c2;
                        }
                        b4.a(str, str2);
                        return;
                    case 2131363452:
                        this.t.c.a(this.j, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363453:
                        this.t.c.a(this.j, a(), b(), "middle", getAdapterPosition());
                        return;
                    case 2131363454:
                        this.t.c.a(this.j, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        public final void a(ez5 ez5, boolean z) {
            ee7.b(ez5, "preset");
            if (z) {
                xg5 xg5 = xg5.b;
                d b2 = this.t.c;
                if (b2 != null) {
                    if (xg5.a(xg5, ((yr5) b2).getContext(), (List) ez5.b(), false, false, false, (Integer) null, 56, (Object) null)) {
                        FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "warning gone");
                        this.e.setVisibility(8);
                    } else {
                        FLogger.INSTANCE.getLocal().d("HybridCustomizePresetDetailAdapter", "warning visible");
                        this.e.setVisibility(0);
                    }
                    this.g.setSelected(true);
                    this.h.setText(PortfolioApp.g0.c().getString(2131886544));
                    this.h.setClickable(false);
                    this.h.a("flexible_button_right_applied");
                    if (!TextUtils.isEmpty(this.t.c())) {
                        int parseColor = Color.parseColor(this.t.c());
                        Drawable drawable = PortfolioApp.g0.c().getDrawable(2131231051);
                        if (drawable != null) {
                            drawable.setTint(parseColor);
                            this.h.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                        }
                    }
                    this.g.setClickable(false);
                    if (this.t.b.size() == 2) {
                        this.f.setVisibility(4);
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
                }
            } else {
                this.g.setSelected(false);
                this.h.setText(PortfolioApp.g0.c().getString(2131886536));
                this.h.setClickable(true);
                this.h.a("flexible_button_right_apply");
                this.h.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.g.setClickable(true);
                this.f.setVisibility(0);
            }
            this.j = ez5;
            this.a.setText(ez5.d());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(ez5.a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizePresetDetailAdapter", "build with microApps=" + arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                fz5 fz5 = (fz5) it.next();
                String c2 = fz5.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                this.b.c(fz5.a());
                                this.b.h();
                            }
                        } else if (c2.equals("middle")) {
                            this.c.c(fz5.a());
                            this.c.h();
                        }
                    } else if (c2.equals("bottom")) {
                        this.d.c(fz5.a());
                        this.d.h();
                    }
                }
            }
        }
    }
}
