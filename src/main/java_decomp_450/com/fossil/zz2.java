package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz2 implements wz2 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true);

    @DexIgnore
    @Override // com.fossil.wz2
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
