package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ij7<T> extends rh7<T> implements hj7<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "kotlinx.coroutines.DeferredCoroutine", f = "Builders.common.kt", l = {99}, m = "await$suspendImpl")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ij7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ij7 ij7, fb7 fb7) {
            super(fb7);
            this.this$0 = ij7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return ij7.a(this.this$0, this);
        }
    }

    @DexIgnore
    public ij7(ib7 ib7, boolean z) {
        super(ib7, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object a(com.fossil.ij7 r4, com.fossil.fb7 r5) {
        /*
            boolean r0 = r5 instanceof com.fossil.ij7.a
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.fossil.ij7$a r0 = (com.fossil.ij7.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ij7$a r0 = new com.fossil.ij7$a
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r4 = r0.L$0
            com.fossil.ij7 r4 = (com.fossil.ij7) r4
            com.fossil.t87.a(r5)
            goto L_0x0043
        L_0x002d:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "call to 'resume' before 'invoke' with coroutine"
            r4.<init>(r5)
            throw r4
        L_0x0035:
            com.fossil.t87.a(r5)
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r4.d(r0)
            if (r5 != r1) goto L_0x0043
            return r1
        L_0x0043:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ij7.a(com.fossil.ij7, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.hj7
    public Object c(fb7<? super T> fb7) {
        return a(this, fb7);
    }
}
