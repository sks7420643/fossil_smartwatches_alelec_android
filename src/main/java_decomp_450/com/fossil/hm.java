package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hm {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hm {
        @DexIgnore
        @Override // com.fossil.hm
        public gm a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static hm a() {
        return new a();
    }

    @DexIgnore
    public abstract gm a(String str);

    @DexIgnore
    public final gm b(String str) {
        gm a2 = a(str);
        return a2 == null ? gm.a(str) : a2;
    }
}
