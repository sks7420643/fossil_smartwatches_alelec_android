package com.fossil;

import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lg0 extends k60 implements Parcelable {
    @DexIgnore
    public o31 a;

    @DexIgnore
    public final void a(o31 o31) {
        this.a = o31;
    }

    @DexIgnore
    public final byte[] b() {
        o31 o31 = this.a;
        if (o31 != null) {
            int length = o31.a.length + 3 + d().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            ee7.a((Object) allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            o31 o312 = this.a;
            if (o312 != null) {
                allocate.put(o312.a);
                allocate.putShort((short) length);
                allocate.put((byte) c());
                allocate.put(d());
                byte[] array = allocate.array();
                ee7.a((Object) array, "byteBuffer.array()");
                return array;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract byte[] d();
}
