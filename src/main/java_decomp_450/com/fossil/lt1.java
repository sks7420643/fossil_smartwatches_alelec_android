package com.fossil;

import com.fossil.jt1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt1 extends jt1 {
    @DexIgnore
    public /* final */ Integer a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends jt1.a {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a a(Integer num) {
            this.a = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a b(String str) {
            this.h = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a c(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a d(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a e(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a f(String str) {
            this.f = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a g(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1.a a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.jt1.a
        public jt1 a() {
            return new lt1(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, null);
        }
    }

    @DexIgnore
    public /* synthetic */ lt1(Integer num, String str, String str2, String str3, String str4, String str5, String str6, String str7, a aVar) {
        this.a = num;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String b() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String d() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jt1)) {
            return false;
        }
        Integer num = this.a;
        if (num != null ? num.equals(((lt1) obj).a) : ((lt1) obj).a == null) {
            String str = this.b;
            if (str != null ? str.equals(((lt1) obj).b) : ((lt1) obj).b == null) {
                String str2 = this.c;
                if (str2 != null ? str2.equals(((lt1) obj).c) : ((lt1) obj).c == null) {
                    String str3 = this.d;
                    if (str3 != null ? str3.equals(((lt1) obj).d) : ((lt1) obj).d == null) {
                        String str4 = this.e;
                        if (str4 != null ? str4.equals(((lt1) obj).e) : ((lt1) obj).e == null) {
                            String str5 = this.f;
                            if (str5 != null ? str5.equals(((lt1) obj).f) : ((lt1) obj).f == null) {
                                String str6 = this.g;
                                if (str6 != null ? str6.equals(((lt1) obj).g) : ((lt1) obj).g == null) {
                                    String str7 = this.h;
                                    if (str7 == null) {
                                        if (((lt1) obj).h == null) {
                                            return true;
                                        }
                                    } else if (str7.equals(((lt1) obj).h)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String f() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public String g() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.jt1
    public Integer h() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        int i = 0;
        int hashCode = ((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003;
        String str = this.b;
        int hashCode2 = (hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003;
        String str2 = this.c;
        int hashCode3 = (hashCode2 ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.d;
        int hashCode4 = (hashCode3 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        String str4 = this.e;
        int hashCode5 = (hashCode4 ^ (str4 == null ? 0 : str4.hashCode())) * 1000003;
        String str5 = this.f;
        int hashCode6 = (hashCode5 ^ (str5 == null ? 0 : str5.hashCode())) * 1000003;
        String str6 = this.g;
        int hashCode7 = (hashCode6 ^ (str6 == null ? 0 : str6.hashCode())) * 1000003;
        String str7 = this.h;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode7 ^ i;
    }

    @DexIgnore
    public String toString() {
        return "AndroidClientInfo{sdkVersion=" + this.a + ", model=" + this.b + ", hardware=" + this.c + ", device=" + this.d + ", product=" + this.e + ", osBuild=" + this.f + ", manufacturer=" + this.g + ", fingerprint=" + this.h + "}";
    }
}
