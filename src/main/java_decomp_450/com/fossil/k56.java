package com.fossil;

import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k56 implements Factory<j56> {
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> b;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> c;

    @DexIgnore
    public k56(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static k56 a(Provider<HybridPresetRepository> provider, Provider<MicroAppLastSettingRepository> provider2, Provider<MicroAppRepository> provider3) {
        return new k56(provider, provider2, provider3);
    }

    @DexIgnore
    public static j56 a(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        return new j56(hybridPresetRepository, microAppLastSettingRepository, microAppRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public j56 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
