package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.qm;
import com.fossil.zo;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnknownNullness"})
public interface ap {
    @DexIgnore
    int a(qm.a aVar, String... strArr);

    @DexIgnore
    int a(String str, long j);

    @DexIgnore
    List<zo> a();

    @DexIgnore
    List<zo> a(int i);

    @DexIgnore
    List<zo> a(long j);

    @DexIgnore
    void a(zo zoVar);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void a(String str, cm cmVar);

    @DexIgnore
    List<zo> b();

    @DexIgnore
    List<zo.b> b(String str);

    @DexIgnore
    void b(String str, long j);

    @DexIgnore
    List<zo> c();

    @DexIgnore
    List<String> c(String str);

    @DexIgnore
    qm.a d(String str);

    @DexIgnore
    List<String> d();

    @DexIgnore
    int e();

    @DexIgnore
    zo e(String str);

    @DexIgnore
    int f(String str);

    @DexIgnore
    List<cm> g(String str);

    @DexIgnore
    int h(String str);
}
