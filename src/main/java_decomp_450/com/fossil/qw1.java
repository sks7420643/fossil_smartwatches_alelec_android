package com.fossil;

import com.fossil.tw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw1 extends tw1 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tw1.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1.a a(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1.a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1.a c(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1.a a(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1.a b(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tw1.a
        public tw1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " maxStorageSizeInBytes";
            }
            if (this.b == null) {
                str = str + " loadBatchSize";
            }
            if (this.c == null) {
                str = str + " criticalSectionEnterTimeoutMs";
            }
            if (this.d == null) {
                str = str + " eventCleanUpAge";
            }
            if (this.e == null) {
                str = str + " maxBlobByteSizePerRow";
            }
            if (str.isEmpty()) {
                return new qw1(this.a.longValue(), this.b.intValue(), this.c.intValue(), this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.tw1
    public int a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.tw1
    public long b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.tw1
    public int c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.tw1
    public int d() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.tw1
    public long e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof tw1)) {
            return false;
        }
        tw1 tw1 = (tw1) obj;
        if (this.b == tw1.e() && this.c == tw1.c() && this.d == tw1.a() && this.e == tw1.b() && this.f == tw1.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        long j2 = this.e;
        return this.f ^ ((((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.c) * 1000003) ^ this.d) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "EventStoreConfig{maxStorageSizeInBytes=" + this.b + ", loadBatchSize=" + this.c + ", criticalSectionEnterTimeoutMs=" + this.d + ", eventCleanUpAge=" + this.e + ", maxBlobByteSizePerRow=" + this.f + "}";
    }

    @DexIgnore
    public qw1(long j, int i, int i2, long j2, int i3) {
        this.b = j;
        this.c = i;
        this.d = i2;
        this.e = j2;
        this.f = i3;
    }
}
