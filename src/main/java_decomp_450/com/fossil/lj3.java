package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ r43 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService e;

    @DexIgnore
    public lj3(AppMeasurementDynamiteService appMeasurementDynamiteService, r43 r43, String str, String str2, boolean z) {
        this.e = appMeasurementDynamiteService;
        this.a = r43;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    public final void run() {
        this.e.a.E().a(this.a, this.b, this.c, this.d);
    }
}
