package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z62 {
    @DexIgnore
    public static /* final */ b a; // = new g82();

    @DexIgnore
    public interface a<R extends i12, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        w02 a(Status status);
    }

    @DexIgnore
    public static <R extends i12, T> no3<T> a(c12<R> c12, a<R, T> aVar) {
        b bVar = a;
        oo3 oo3 = new oo3();
        c12.a(new f82(c12, oo3, aVar, bVar));
        return oo3.a();
    }

    @DexIgnore
    public static <R extends i12, T extends h12<R>> no3<T> a(c12<R> c12, T t) {
        return a(c12, new i82(t));
    }

    @DexIgnore
    public static <R extends i12> no3<Void> a(c12<R> c12) {
        return a(c12, new h82());
    }
}
