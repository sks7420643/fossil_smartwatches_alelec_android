package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.nw;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rw implements nw {
    @DexIgnore
    public static /* final */ String u; // = "rw";
    @DexIgnore
    public int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ nw.a c;
    @DexIgnore
    public ByteBuffer d;
    @DexIgnore
    public byte[] e;
    @DexIgnore
    public short[] f;
    @DexIgnore
    public byte[] g;
    @DexIgnore
    public byte[] h;
    @DexIgnore
    public byte[] i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public int k;
    @DexIgnore
    public pw l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public Boolean s;
    @DexIgnore
    public Bitmap.Config t;

    @DexIgnore
    public rw(nw.a aVar, pw pwVar, ByteBuffer byteBuffer, int i2) {
        this(aVar);
        a(pwVar, byteBuffer, i2);
    }

    @DexIgnore
    public int a(int i2) {
        if (i2 >= 0) {
            pw pwVar = this.l;
            if (i2 < pwVar.c) {
                return pwVar.e.get(i2).i;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public void b() {
        this.k = (this.k + 1) % this.l.c;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public int c() {
        return this.l.c;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public void clear() {
        this.l = null;
        byte[] bArr = this.i;
        if (bArr != null) {
            this.c.a(bArr);
        }
        int[] iArr = this.j;
        if (iArr != null) {
            this.c.a(iArr);
        }
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.c.a(bitmap);
        }
        this.m = null;
        this.d = null;
        this.s = null;
        byte[] bArr2 = this.e;
        if (bArr2 != null) {
            this.c.a(bArr2);
        }
    }

    @DexIgnore
    @Override // com.fossil.nw
    public int d() {
        int i2;
        if (this.l.c <= 0 || (i2 = this.k) < 0) {
            return 0;
        }
        return a(i2);
    }

    @DexIgnore
    @Override // com.fossil.nw
    public ByteBuffer e() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public void f() {
        this.k = -1;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public int g() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.nw
    public int h() {
        return this.d.limit() + this.i.length + (this.j.length * 4);
    }

    @DexIgnore
    public final Bitmap i() {
        Boolean bool = this.s;
        Bitmap a2 = this.c.a(this.r, this.q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.t);
        a2.setHasAlpha(true);
        return a2;
    }

    @DexIgnore
    public final int j() {
        int k2 = k();
        if (k2 <= 0) {
            return k2;
        }
        ByteBuffer byteBuffer = this.d;
        byteBuffer.get(this.e, 0, Math.min(k2, byteBuffer.remaining()));
        return k2;
    }

    @DexIgnore
    public final int k() {
        return this.d.get() & 255;
    }

    @DexIgnore
    public final void b(ow owVar) {
        ow owVar2 = owVar;
        int[] iArr = this.j;
        int i2 = owVar2.d;
        int i3 = owVar2.b;
        int i4 = owVar2.c;
        int i5 = owVar2.a;
        boolean z = this.k == 0;
        int i6 = this.r;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        int i7 = 0;
        byte b2 = -1;
        while (i7 < i2) {
            int i8 = (i7 + i3) * i6;
            int i9 = i8 + i5;
            int i10 = i9 + i4;
            int i11 = i8 + i6;
            if (i11 < i10) {
                i10 = i11;
            }
            int i12 = owVar2.c * i7;
            int i13 = i9;
            while (i13 < i10) {
                byte b3 = bArr[i12];
                byte b4 = b3 & 255;
                if (b4 != b2) {
                    int i14 = iArr2[b4];
                    if (i14 != 0) {
                        iArr[i13] = i14;
                    } else {
                        b2 = b3;
                    }
                }
                i12++;
                i13++;
                i2 = i2;
            }
            i7++;
            owVar2 = owVar;
        }
        Boolean bool = this.s;
        this.s = Boolean.valueOf((bool != null && bool.booleanValue()) || (this.s == null && z && b2 != -1));
    }

    @DexIgnore
    public final void c(ow owVar) {
        int i2;
        int i3;
        short s2;
        rw rwVar = this;
        if (owVar != null) {
            rwVar.d.position(owVar.j);
        }
        if (owVar == null) {
            pw pwVar = rwVar.l;
            i2 = pwVar.f;
            i3 = pwVar.g;
        } else {
            i2 = owVar.c;
            i3 = owVar.d;
        }
        int i4 = i2 * i3;
        byte[] bArr = rwVar.i;
        if (bArr == null || bArr.length < i4) {
            rwVar.i = rwVar.c.b(i4);
        }
        byte[] bArr2 = rwVar.i;
        if (rwVar.f == null) {
            rwVar.f = new short[4096];
        }
        short[] sArr = rwVar.f;
        if (rwVar.g == null) {
            rwVar.g = new byte[4096];
        }
        byte[] bArr3 = rwVar.g;
        if (rwVar.h == null) {
            rwVar.h = new byte[4097];
        }
        byte[] bArr4 = rwVar.h;
        int k2 = k();
        int i5 = 1 << k2;
        int i6 = i5 + 1;
        int i7 = i5 + 2;
        int i8 = k2 + 1;
        int i9 = (1 << i8) - 1;
        int i10 = 0;
        for (int i11 = 0; i11 < i5; i11++) {
            sArr[i11] = 0;
            bArr3[i11] = (byte) i11;
        }
        byte[] bArr5 = rwVar.e;
        int i12 = i8;
        int i13 = i7;
        int i14 = i9;
        int i15 = 0;
        int i16 = 0;
        byte b2 = 0;
        int i17 = 0;
        int i18 = 0;
        byte b3 = -1;
        byte b4 = 0;
        int i19 = 0;
        while (true) {
            if (i10 < i4) {
                if (i15 == 0) {
                    i15 = j();
                    if (i15 <= 0) {
                        rwVar.o = 3;
                        break;
                    }
                    i16 = 0;
                }
                i17 += (bArr5[i16] & 255) << b2;
                i16++;
                i15--;
                int i20 = b2 + 8;
                int i21 = i13;
                int i22 = i12;
                byte b5 = b3;
                byte b6 = b4;
                while (true) {
                    if (i20 < i22) {
                        b3 = b5;
                        i13 = i21;
                        b2 = i20;
                        rwVar = this;
                        b4 = b6;
                        i8 = i8;
                        i12 = i22;
                        break;
                    }
                    byte b7 = i17 & i14;
                    i17 >>= i22;
                    i20 -= i22;
                    if (b7 == i5) {
                        i14 = i9;
                        i22 = i8;
                        i21 = i7;
                        i7 = i21;
                        b5 = -1;
                    } else if (b7 == i6) {
                        b2 = i20;
                        b4 = b6;
                        i13 = i21;
                        i8 = i8;
                        i7 = i7;
                        b3 = b5;
                        i12 = i22;
                        rwVar = this;
                        break;
                    } else if (b5 == -1) {
                        bArr2[i18] = bArr3[b7];
                        i18++;
                        i10++;
                        b5 = b7;
                        b6 = b5;
                        i7 = i7;
                        i20 = i20;
                    } else {
                        if (b7 >= i21) {
                            bArr4[i19] = (byte) b6;
                            i19++;
                            s2 = b5;
                        } else {
                            s2 = b7;
                        }
                        while (s2 >= i5) {
                            bArr4[i19] = bArr3[s2];
                            i19++;
                            s2 = sArr[s2];
                        }
                        b6 = bArr3[s2] & 255;
                        byte b8 = (byte) b6;
                        bArr2[i18] = b8;
                        while (true) {
                            i18++;
                            i10++;
                            if (i19 <= 0) {
                                break;
                            }
                            i19--;
                            bArr2[i18] = bArr4[i19];
                        }
                        if (i21 < 4096) {
                            sArr[i21] = (short) b5;
                            bArr3[i21] = b8;
                            i21++;
                            if ((i21 & i14) == 0 && i21 < 4096) {
                                i22++;
                                i14 += i21;
                            }
                        }
                        b5 = b7;
                        i7 = i7;
                        i20 = i20;
                        bArr4 = bArr4;
                    }
                }
            } else {
                break;
            }
        }
        Arrays.fill(bArr2, i18, i4, (byte) 0);
    }

    @DexIgnore
    public rw(nw.a aVar) {
        this.b = new int[256];
        this.t = Bitmap.Config.ARGB_8888;
        this.c = aVar;
        this.l = new pw();
    }

    @DexIgnore
    @Override // com.fossil.nw
    public synchronized Bitmap a() {
        if (this.l.c <= 0 || this.k < 0) {
            if (Log.isLoggable(u, 3)) {
                String str = u;
                Log.d(str, "Unable to decode frame, frameCount=" + this.l.c + ", framePointer=" + this.k);
            }
            this.o = 1;
        }
        if (this.o != 1) {
            if (this.o != 2) {
                this.o = 0;
                if (this.e == null) {
                    this.e = this.c.b(255);
                }
                ow owVar = this.l.e.get(this.k);
                int i2 = this.k - 1;
                ow owVar2 = i2 >= 0 ? this.l.e.get(i2) : null;
                int[] iArr = owVar.k != null ? owVar.k : this.l.a;
                this.a = iArr;
                if (iArr == null) {
                    if (Log.isLoggable(u, 3)) {
                        String str2 = u;
                        Log.d(str2, "No valid color table found for frame #" + this.k);
                    }
                    this.o = 1;
                    return null;
                }
                if (owVar.f) {
                    System.arraycopy(iArr, 0, this.b, 0, iArr.length);
                    int[] iArr2 = this.b;
                    this.a = iArr2;
                    iArr2[owVar.h] = 0;
                    if (owVar.g == 2 && this.k == 0) {
                        this.s = true;
                    }
                }
                return a(owVar, owVar2);
            }
        }
        if (Log.isLoggable(u, 3)) {
            String str3 = u;
            Log.d(str3, "Unable to decode frame, status=" + this.o);
        }
        return null;
    }

    @DexIgnore
    public synchronized void a(pw pwVar, ByteBuffer byteBuffer, int i2) {
        if (i2 > 0) {
            int highestOneBit = Integer.highestOneBit(i2);
            this.o = 0;
            this.l = pwVar;
            this.k = -1;
            ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
            this.d = asReadOnlyBuffer;
            asReadOnlyBuffer.position(0);
            this.d.order(ByteOrder.LITTLE_ENDIAN);
            this.n = false;
            Iterator<ow> it = pwVar.e.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().g == 3) {
                        this.n = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.p = highestOneBit;
            this.r = pwVar.f / highestOneBit;
            this.q = pwVar.g / highestOneBit;
            this.i = this.c.b(pwVar.f * pwVar.g);
            this.j = this.c.a(this.r * this.q);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.nw
    public void a(Bitmap.Config config) {
        if (config == Bitmap.Config.ARGB_8888 || config == Bitmap.Config.RGB_565) {
            this.t = config;
            return;
        }
        throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
    }

    @DexIgnore
    public final Bitmap a(ow owVar, ow owVar2) {
        int i2;
        int i3;
        Bitmap bitmap;
        int[] iArr = this.j;
        int i4 = 0;
        if (owVar2 == null) {
            Bitmap bitmap2 = this.m;
            if (bitmap2 != null) {
                this.c.a(bitmap2);
            }
            this.m = null;
            Arrays.fill(iArr, 0);
        }
        if (owVar2 != null && owVar2.g == 3 && this.m == null) {
            Arrays.fill(iArr, 0);
        }
        if (owVar2 != null && (i3 = owVar2.g) > 0) {
            if (i3 == 2) {
                if (!owVar.f) {
                    pw pwVar = this.l;
                    int i5 = pwVar.l;
                    if (owVar.k == null || pwVar.j != owVar.h) {
                        i4 = i5;
                    }
                }
                int i6 = owVar2.d;
                int i7 = this.p;
                int i8 = i6 / i7;
                int i9 = owVar2.b / i7;
                int i10 = owVar2.c / i7;
                int i11 = owVar2.a / i7;
                int i12 = this.r;
                int i13 = (i9 * i12) + i11;
                int i14 = (i8 * i12) + i13;
                while (i13 < i14) {
                    int i15 = i13 + i10;
                    for (int i16 = i13; i16 < i15; i16++) {
                        iArr[i16] = i4;
                    }
                    i13 += this.r;
                }
            } else if (i3 == 3 && (bitmap = this.m) != null) {
                int i17 = this.r;
                bitmap.getPixels(iArr, 0, i17, 0, 0, i17, this.q);
            }
        }
        c(owVar);
        if (owVar.e || this.p != 1) {
            a(owVar);
        } else {
            b(owVar);
        }
        if (this.n && ((i2 = owVar.g) == 0 || i2 == 1)) {
            if (this.m == null) {
                this.m = i();
            }
            Bitmap bitmap3 = this.m;
            int i18 = this.r;
            bitmap3.setPixels(iArr, 0, i18, 0, 0, i18, this.q);
        }
        Bitmap i19 = i();
        int i20 = this.r;
        i19.setPixels(iArr, 0, i20, 0, 0, i20, this.q);
        return i19;
    }

    @DexIgnore
    public final void a(ow owVar) {
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int[] iArr = this.j;
        int i7 = owVar.d;
        int i8 = this.p;
        int i9 = i7 / i8;
        int i10 = owVar.b / i8;
        int i11 = owVar.c / i8;
        int i12 = owVar.a / i8;
        boolean z2 = this.k == 0;
        int i13 = this.p;
        int i14 = this.r;
        int i15 = this.q;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        Boolean bool = this.s;
        int i16 = 8;
        int i17 = 0;
        int i18 = 0;
        int i19 = 1;
        while (i17 < i9) {
            Boolean bool2 = bool;
            if (owVar.e) {
                if (i18 >= i9) {
                    i2 = i9;
                    int i20 = i19 + 1;
                    if (i20 == 2) {
                        i19 = i20;
                        i18 = 4;
                    } else if (i20 == 3) {
                        i19 = i20;
                        i18 = 2;
                        i16 = 4;
                    } else if (i20 != 4) {
                        i19 = i20;
                    } else {
                        i19 = i20;
                        i18 = 1;
                        i16 = 2;
                    }
                } else {
                    i2 = i9;
                }
                i3 = i18 + i16;
            } else {
                i2 = i9;
                i3 = i18;
                i18 = i17;
            }
            int i21 = i18 + i10;
            boolean z3 = i13 == 1;
            if (i21 < i15) {
                int i22 = i21 * i14;
                int i23 = i22 + i12;
                int i24 = i23 + i11;
                int i25 = i22 + i14;
                if (i25 < i24) {
                    i24 = i25;
                }
                i4 = i3;
                int i26 = i17 * i13 * owVar.c;
                if (z3) {
                    int i27 = i23;
                    while (i27 < i24) {
                        int i28 = iArr2[bArr[i26] & 255];
                        if (i28 != 0) {
                            iArr[i27] = i28;
                        } else if (z2 && bool2 == null) {
                            bool2 = true;
                        }
                        i26 += i13;
                        i27++;
                        i10 = i10;
                    }
                } else {
                    i6 = i10;
                    int i29 = ((i24 - i23) * i13) + i26;
                    int i30 = i23;
                    while (true) {
                        i5 = i11;
                        if (i30 >= i24) {
                            break;
                        }
                        int a2 = a(i26, i29, owVar.c);
                        if (a2 != 0) {
                            iArr[i30] = a2;
                        } else if (z2 && bool2 == null) {
                            bool2 = true;
                        }
                        i26 += i13;
                        i30++;
                        i11 = i5;
                    }
                    bool = bool2;
                    i17++;
                    i10 = i6;
                    i11 = i5;
                    i9 = i2;
                    i18 = i4;
                }
            } else {
                i4 = i3;
            }
            i6 = i10;
            i5 = i11;
            bool = bool2;
            i17++;
            i10 = i6;
            i11 = i5;
            i9 = i2;
            i18 = i4;
        }
        if (this.s == null) {
            if (bool == null) {
                z = false;
            } else {
                z = bool.booleanValue();
            }
            this.s = Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        for (int i10 = i2; i10 < this.p + i2; i10++) {
            byte[] bArr = this.i;
            if (i10 >= bArr.length || i10 >= i3) {
                break;
            }
            int i11 = this.a[bArr[i10] & 255];
            if (i11 != 0) {
                i5 += (i11 >> 24) & 255;
                i6 += (i11 >> 16) & 255;
                i7 += (i11 >> 8) & 255;
                i8 += i11 & 255;
                i9++;
            }
        }
        int i12 = i2 + i4;
        for (int i13 = i12; i13 < this.p + i12; i13++) {
            byte[] bArr2 = this.i;
            if (i13 >= bArr2.length || i13 >= i3) {
                break;
            }
            int i14 = this.a[bArr2[i13] & 255];
            if (i14 != 0) {
                i5 += (i14 >> 24) & 255;
                i6 += (i14 >> 16) & 255;
                i7 += (i14 >> 8) & 255;
                i8 += i14 & 255;
                i9++;
            }
        }
        if (i9 == 0) {
            return 0;
        }
        return ((i5 / i9) << 24) | ((i6 / i9) << 16) | ((i7 / i9) << 8) | (i8 / i9);
    }
}
