package com.fossil;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jo implements rn, vm {
    @DexIgnore
    public static /* final */ String q; // = im.a("SystemFgDispatcher");
    @DexIgnore
    public Context a;
    @DexIgnore
    public dn b;
    @DexIgnore
    public /* final */ vp c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public String e;
    @DexIgnore
    public em f;
    @DexIgnore
    public /* final */ Map<String, em> g;
    @DexIgnore
    public /* final */ Map<String, zo> h;
    @DexIgnore
    public /* final */ Set<zo> i;
    @DexIgnore
    public /* final */ sn j;
    @DexIgnore
    public b p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WorkDatabase a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(WorkDatabase workDatabase, String str) {
            this.a = workDatabase;
            this.b = str;
        }

        @DexIgnore
        public void run() {
            zo e = this.a.f().e(this.b);
            if (e != null && e.b()) {
                synchronized (jo.this.d) {
                    jo.this.h.put(this.b, e);
                    jo.this.i.add(e);
                    jo.this.j.a(jo.this.i);
                }
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(int i, int i2, Notification notification);

        @DexIgnore
        void a(int i, Notification notification);

        @DexIgnore
        void stop();
    }

    @DexIgnore
    public jo(Context context) {
        this.a = context;
        dn a2 = dn.a(this.a);
        this.b = a2;
        this.c = a2.g();
        this.e = null;
        this.f = null;
        this.g = new LinkedHashMap();
        this.i = new HashSet();
        this.h = new HashMap();
        this.j = new sn(this.a, this.c, this);
        this.b.d().a(this);
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        b bVar;
        Map.Entry<String, em> entry;
        synchronized (this.d) {
            zo remove = this.h.remove(str);
            if (remove != null ? this.i.remove(remove) : false) {
                this.j.a(this.i);
            }
        }
        this.f = this.g.remove(str);
        if (!str.equals(this.e)) {
            em emVar = this.f;
            if (emVar != null && (bVar = this.p) != null) {
                bVar.a(emVar.c());
            }
        } else if (this.g.size() > 0) {
            Iterator<Map.Entry<String, em>> it = this.g.entrySet().iterator();
            Map.Entry<String, em> next = it.next();
            while (true) {
                entry = next;
                if (!it.hasNext()) {
                    break;
                }
                next = it.next();
            }
            this.e = entry.getKey();
            if (this.p != null) {
                em value = entry.getValue();
                this.p.a(value.c(), value.a(), value.b());
                this.p.a(value.c());
            }
        }
    }

    @DexIgnore
    public void b() {
        this.p = null;
        synchronized (this.d) {
            this.j.a();
        }
        this.b.d().b(this);
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void b(List<String> list) {
    }

    @DexIgnore
    public final void c(Intent intent) {
        im.a().c(q, String.format("Started foreground service %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        this.c.a(new a(this.b.f(), stringExtra));
    }

    @DexIgnore
    public void d(Intent intent) {
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            c(intent);
            b(intent);
        } else if ("ACTION_NOTIFY".equals(action)) {
            b(intent);
        } else if ("ACTION_CANCEL_WORK".equals(action)) {
            a(intent);
        }
    }

    @DexIgnore
    public final void b(Intent intent) {
        int i2 = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        im.a().a(q, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (notification != null && this.p != null) {
            this.g.put(stringExtra, new em(intExtra, notification, intExtra2));
            if (TextUtils.isEmpty(this.e)) {
                this.e = stringExtra;
                this.p.a(intExtra, intExtra2, notification);
                return;
            }
            this.p.a(intExtra, notification);
            if (intExtra2 != 0 && Build.VERSION.SDK_INT >= 29) {
                for (Map.Entry<String, em> entry : this.g.entrySet()) {
                    i2 |= entry.getValue().a();
                }
                em emVar = this.g.get(this.e);
                if (emVar != null) {
                    this.p.a(emVar.c(), i2, emVar.b());
                }
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        if (this.p != null) {
            im.a().b(q, "A callback already exists.", new Throwable[0]);
        } else {
            this.p = bVar;
        }
    }

    @DexIgnore
    public void a() {
        im.a().c(q, "Stopping foreground service", new Throwable[0]);
        b bVar = this.p;
        if (bVar != null) {
            em emVar = this.f;
            if (emVar != null) {
                bVar.a(emVar.c());
                this.f = null;
            }
            this.p.stop();
        }
    }

    @DexIgnore
    public final void a(Intent intent) {
        im.a().c(q, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
            this.b.a(UUID.fromString(stringExtra));
        }
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void a(List<String> list) {
        if (!list.isEmpty()) {
            for (String str : list) {
                im.a().a(q, String.format("Constraints unmet for WorkSpec %s", str), new Throwable[0]);
                this.b.b(str);
            }
        }
    }
}
