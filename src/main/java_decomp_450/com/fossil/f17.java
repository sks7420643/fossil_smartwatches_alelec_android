package com.fossil;

import android.net.NetworkInfo;
import com.fossil.o17;
import com.squareup.picasso.Picasso;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f17 extends ThreadPoolExecutor {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends FutureTask<o07> implements Comparable<a> {
        @DexIgnore
        public /* final */ o07 a;

        @DexIgnore
        public a(o07 o07) {
            super(o07, null);
            this.a = o07;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(a aVar) {
            Picasso.e k = this.a.k();
            Picasso.e k2 = aVar.a.k();
            return k == k2 ? this.a.a - aVar.a.a : k2.ordinal() - k.ordinal();
        }
    }

    @DexIgnore
    public f17() {
        super(3, 3, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new o17.f());
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void a(NetworkInfo networkInfo) {
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            a(3);
            return;
        }
        int type = networkInfo.getType();
        if (type == 0) {
            int subtype = networkInfo.getSubtype();
            switch (subtype) {
                case 1:
                case 2:
                    a(1);
                    return;
                case 3:
                case 4:
                case 5:
                case 6:
                    break;
                default:
                    switch (subtype) {
                        case 12:
                            break;
                        case 13:
                        case 14:
                        case 15:
                            a(3);
                            return;
                        default:
                            a(3);
                            return;
                    }
            }
            a(2);
        } else if (type == 1 || type == 6 || type == 9) {
            a(4);
        } else {
            a(3);
        }
    }

    @DexIgnore
    @Override // java.util.concurrent.AbstractExecutorService, java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        a aVar = new a((o07) runnable);
        execute(aVar);
        return aVar;
    }

    @DexIgnore
    public final void a(int i) {
        setCorePoolSize(i);
        setMaximumPoolSize(i);
    }
}
