package com.fossil;

import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.lo7;
import com.fossil.yp7;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp7 extends yp7.j implements un7 {
    @DexIgnore
    public /* final */ vn7 b;
    @DexIgnore
    public /* final */ no7 c;
    @DexIgnore
    public Socket d;
    @DexIgnore
    public Socket e;
    @DexIgnore
    public eo7 f;
    @DexIgnore
    public jo7 g;
    @DexIgnore
    public yp7 h;
    @DexIgnore
    public ar7 i;
    @DexIgnore
    public zq7 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 1;
    @DexIgnore
    public /* final */ List<Reference<fp7>> n; // = new ArrayList();
    @DexIgnore
    public long o; // = Long.MAX_VALUE;

    @DexIgnore
    public bp7(vn7 vn7, no7 no7) {
        this.b = vn7;
        this.c = no7;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090 A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r17, int r18, int r19, int r20, boolean r21, com.fossil.qn7 r22, com.fossil.co7 r23) {
        /*
            r16 = this;
            r7 = r16
            r8 = r22
            r9 = r23
            com.fossil.jo7 r0 = r7.g
            if (r0 != 0) goto L_0x0150
            com.fossil.no7 r0 = r7.c
            com.fossil.nn7 r0 = r0.a()
            java.util.List r0 = r0.b()
            com.fossil.ap7 r10 = new com.fossil.ap7
            r10.<init>(r0)
            com.fossil.no7 r1 = r7.c
            com.fossil.nn7 r1 = r1.a()
            javax.net.ssl.SSLSocketFactory r1 = r1.j()
            if (r1 != 0) goto L_0x0074
            com.fossil.wn7 r1 = com.fossil.wn7.h
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0067
            com.fossil.no7 r0 = r7.c
            com.fossil.nn7 r0 = r0.a()
            com.fossil.go7 r0 = r0.k()
            java.lang.String r0 = r0.g()
            com.fossil.mq7 r1 = com.fossil.mq7.d()
            boolean r1 = r1.b(r0)
            if (r1 == 0) goto L_0x0046
            goto L_0x0086
        L_0x0046:
            com.fossil.dp7 r1 = new com.fossil.dp7
            java.net.UnknownServiceException r2 = new java.net.UnknownServiceException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "CLEARTEXT communication to "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = " not permitted by network security policy"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.<init>(r0)
            r1.<init>(r2)
            throw r1
        L_0x0067:
            com.fossil.dp7 r0 = new com.fossil.dp7
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "CLEARTEXT communication not enabled for client"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0074:
            com.fossil.no7 r0 = r7.c
            com.fossil.nn7 r0 = r0.a()
            java.util.List r0 = r0.e()
            com.fossil.jo7 r1 = com.fossil.jo7.H2_PRIOR_KNOWLEDGE
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x0143
        L_0x0086:
            r11 = 0
            r12 = r11
        L_0x0088:
            com.fossil.no7 r0 = r7.c     // Catch:{ IOException -> 0x00f9 }
            boolean r0 = r0.c()     // Catch:{ IOException -> 0x00f9 }
            if (r0 == 0) goto L_0x00a9
            r1 = r16
            r2 = r17
            r3 = r18
            r4 = r19
            r5 = r22
            r6 = r23
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x00f9 }
            java.net.Socket r0 = r7.d     // Catch:{ IOException -> 0x00f9 }
            if (r0 != 0) goto L_0x00a4
            goto L_0x00c6
        L_0x00a4:
            r13 = r17
            r14 = r18
            goto L_0x00b0
        L_0x00a9:
            r13 = r17
            r14 = r18
            r7.a(r13, r14, r8, r9)     // Catch:{ IOException -> 0x00f7 }
        L_0x00b0:
            r15 = r20
            r7.a(r10, r15, r8, r9)     // Catch:{ IOException -> 0x00f5 }
            com.fossil.no7 r0 = r7.c     // Catch:{ IOException -> 0x00f5 }
            java.net.InetSocketAddress r0 = r0.d()     // Catch:{ IOException -> 0x00f5 }
            com.fossil.no7 r1 = r7.c     // Catch:{ IOException -> 0x00f5 }
            java.net.Proxy r1 = r1.b()     // Catch:{ IOException -> 0x00f5 }
            com.fossil.jo7 r2 = r7.g     // Catch:{ IOException -> 0x00f5 }
            r9.a(r8, r0, r1, r2)     // Catch:{ IOException -> 0x00f5 }
        L_0x00c6:
            com.fossil.no7 r0 = r7.c
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x00e0
            java.net.Socket r0 = r7.d
            if (r0 == 0) goto L_0x00d3
            goto L_0x00e0
        L_0x00d3:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Too many tunnel connections attempted: 21"
            r0.<init>(r1)
            com.fossil.dp7 r1 = new com.fossil.dp7
            r1.<init>(r0)
            throw r1
        L_0x00e0:
            com.fossil.yp7 r0 = r7.h
            if (r0 == 0) goto L_0x00f4
            com.fossil.vn7 r1 = r7.b
            monitor-enter(r1)
            com.fossil.yp7 r0 = r7.h     // Catch:{ all -> 0x00f1 }
            int r0 = r0.b()     // Catch:{ all -> 0x00f1 }
            r7.m = r0     // Catch:{ all -> 0x00f1 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f1 }
            goto L_0x00f4
        L_0x00f1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f1 }
            throw r0
        L_0x00f4:
            return
        L_0x00f5:
            r0 = move-exception
            goto L_0x0100
        L_0x00f7:
            r0 = move-exception
            goto L_0x00fe
        L_0x00f9:
            r0 = move-exception
            r13 = r17
            r14 = r18
        L_0x00fe:
            r15 = r20
        L_0x0100:
            java.net.Socket r1 = r7.e
            com.fossil.ro7.a(r1)
            java.net.Socket r1 = r7.d
            com.fossil.ro7.a(r1)
            r7.e = r11
            r7.d = r11
            r7.i = r11
            r7.j = r11
            r7.f = r11
            r7.g = r11
            r7.h = r11
            com.fossil.no7 r1 = r7.c
            java.net.InetSocketAddress r3 = r1.d()
            com.fossil.no7 r1 = r7.c
            java.net.Proxy r4 = r1.b()
            r5 = 0
            r1 = r23
            r2 = r22
            r6 = r0
            r1.a(r2, r3, r4, r5, r6)
            if (r12 != 0) goto L_0x0135
            com.fossil.dp7 r12 = new com.fossil.dp7
            r12.<init>(r0)
            goto L_0x0138
        L_0x0135:
            r12.addConnectException(r0)
        L_0x0138:
            if (r21 == 0) goto L_0x0142
            boolean r0 = r10.a(r0)
            if (r0 == 0) goto L_0x0142
            goto L_0x0088
        L_0x0142:
            throw r12
        L_0x0143:
            com.fossil.dp7 r0 = new com.fossil.dp7
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.String r2 = "H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0150:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bp7.a(int, int, int, int, boolean, com.fossil.qn7, com.fossil.co7):void");
    }

    @DexIgnore
    public void b() {
        ro7.a(this.d);
    }

    @DexIgnore
    public final lo7 c() throws IOException {
        lo7.a aVar = new lo7.a();
        aVar.a(this.c.a().k());
        aVar.a("CONNECT", (RequestBody) null);
        aVar.b("Host", ro7.a(this.c.a().k(), true));
        aVar.b("Proxy-Connection", "Keep-Alive");
        aVar.b("User-Agent", so7.a());
        lo7 a = aVar.a();
        Response.a aVar2 = new Response.a();
        aVar2.a(a);
        aVar2.a(jo7.HTTP_1_1);
        aVar2.a(407);
        aVar2.a("Preemptive Authenticate");
        aVar2.a(ro7.c);
        aVar2.b(-1);
        aVar2.a(-1L);
        aVar2.b("Proxy-Authenticate", "OkHttp-Preemptive");
        lo7 authenticate = this.c.a().g().authenticate(this.c, aVar2.a());
        return authenticate != null ? authenticate : a;
    }

    @DexIgnore
    public eo7 d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.h != null;
    }

    @DexIgnore
    public no7 f() {
        return this.c;
    }

    @DexIgnore
    public Socket g() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.c.a().k().g());
        sb.append(":");
        sb.append(this.c.a().k().k());
        sb.append(", proxy=");
        sb.append(this.c.b());
        sb.append(" hostAddress=");
        sb.append(this.c.d());
        sb.append(" cipherSuite=");
        eo7 eo7 = this.f;
        sb.append(eo7 != null ? eo7.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, qn7 qn7, co7 co7) throws IOException {
        lo7 c2 = c();
        go7 g2 = c2.g();
        int i5 = 0;
        while (i5 < 21) {
            a(i2, i3, qn7, co7);
            c2 = a(i3, i4, c2, g2);
            if (c2 != null) {
                ro7.a(this.d);
                this.d = null;
                this.j = null;
                this.i = null;
                co7.a(qn7, this.c.d(), this.c.b(), null);
                i5++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, qn7 qn7, co7 co7) throws IOException {
        Socket socket;
        Proxy b2 = this.c.b();
        nn7 a = this.c.a();
        if (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) {
            socket = a.i().createSocket();
        } else {
            socket = new Socket(b2);
        }
        this.d = socket;
        co7.a(qn7, this.c.d(), b2);
        this.d.setSoTimeout(i3);
        try {
            mq7.d().a(this.d, this.c.d(), i2);
            try {
                this.i = ir7.a(ir7.b(this.d));
                this.j = ir7.a(ir7.a(this.d));
            } catch (NullPointerException e2) {
                if ("throw with null exception".equals(e2.getMessage())) {
                    throw new IOException(e2);
                }
            }
        } catch (ConnectException e3) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.c.d());
            connectException.initCause(e3);
            throw connectException;
        }
    }

    @DexIgnore
    public final void a(ap7 ap7, int i2, qn7 qn7, co7 co7) throws IOException {
        if (this.c.a().j() != null) {
            co7.g(qn7);
            a(ap7);
            co7.a(qn7, this.f);
            if (this.g == jo7.HTTP_2) {
                a(i2);
            }
        } else if (this.c.a().e().contains(jo7.H2_PRIOR_KNOWLEDGE)) {
            this.e = this.d;
            this.g = jo7.H2_PRIOR_KNOWLEDGE;
            a(i2);
        } else {
            this.e = this.d;
            this.g = jo7.HTTP_1_1;
        }
    }

    @DexIgnore
    public final void a(int i2) throws IOException {
        this.e.setSoTimeout(0);
        yp7.h hVar = new yp7.h(true);
        hVar.a(this.e, this.c.a().k().g(), this.i, this.j);
        hVar.a(this);
        hVar.a(i2);
        yp7 a = hVar.a();
        this.h = a;
        a.e();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0137 A[Catch:{ all -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013d A[Catch:{ all -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0140  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.ap7 r8) throws java.io.IOException {
        /*
            r7 = this;
            com.fossil.no7 r0 = r7.c
            com.fossil.nn7 r0 = r0.a()
            javax.net.ssl.SSLSocketFactory r1 = r0.j()
            r2 = 0
            java.net.Socket r3 = r7.d     // Catch:{ AssertionError -> 0x0130 }
            com.fossil.go7 r4 = r0.k()     // Catch:{ AssertionError -> 0x0130 }
            java.lang.String r4 = r4.g()     // Catch:{ AssertionError -> 0x0130 }
            com.fossil.go7 r5 = r0.k()     // Catch:{ AssertionError -> 0x0130 }
            int r5 = r5.k()     // Catch:{ AssertionError -> 0x0130 }
            r6 = 1
            java.net.Socket r1 = r1.createSocket(r3, r4, r5, r6)     // Catch:{ AssertionError -> 0x0130 }
            javax.net.ssl.SSLSocket r1 = (javax.net.ssl.SSLSocket) r1     // Catch:{ AssertionError -> 0x0130 }
            com.fossil.wn7 r8 = r8.a(r1)     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            boolean r3 = r8.c()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            if (r3 == 0) goto L_0x0041
            com.fossil.mq7 r3 = com.fossil.mq7.d()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            com.fossil.go7 r4 = r0.k()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            java.lang.String r4 = r4.g()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            java.util.List r5 = r0.e()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            r3.a(r1, r4, r5)     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
        L_0x0041:
            r1.startHandshake()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            javax.net.ssl.SSLSession r3 = r1.getSession()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            com.fossil.eo7 r4 = com.fossil.eo7.a(r3)     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            javax.net.ssl.HostnameVerifier r5 = r0.d()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            com.fossil.go7 r6 = r0.k()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            java.lang.String r6 = r6.g()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            boolean r3 = r5.verify(r6, r3)     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            if (r3 != 0) goto L_0x00d8
            java.util.List r8 = r4.c()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            boolean r2 = r8.isEmpty()     // Catch:{ AssertionError -> 0x012b, all -> 0x0128 }
            java.lang.String r3 = "Hostname "
            if (r2 != 0) goto L_0x00b6
            r2 = 0
            java.lang.Object r8 = r8.get(r2)
            java.security.cert.X509Certificate r8 = (java.security.cert.X509Certificate) r8
            javax.net.ssl.SSLPeerUnverifiedException r2 = new javax.net.ssl.SSLPeerUnverifiedException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            com.fossil.go7 r0 = r0.k()
            java.lang.String r0 = r0.g()
            r4.append(r0)
            java.lang.String r0 = " not verified:\n    certificate: "
            r4.append(r0)
            java.lang.String r0 = com.fossil.sn7.a(r8)
            r4.append(r0)
            java.lang.String r0 = "\n    DN: "
            r4.append(r0)
            java.security.Principal r0 = r8.getSubjectDN()
            java.lang.String r0 = r0.getName()
            r4.append(r0)
            java.lang.String r0 = "\n    subjectAltNames: "
            r4.append(r0)
            java.util.List r8 = com.fossil.rq7.a(r8)
            r4.append(r8)
            java.lang.String r8 = r4.toString()
            r2.<init>(r8)
            throw r2
        L_0x00b6:
            javax.net.ssl.SSLPeerUnverifiedException r8 = new javax.net.ssl.SSLPeerUnverifiedException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            com.fossil.go7 r0 = r0.k()
            java.lang.String r0 = r0.g()
            r2.append(r0)
            java.lang.String r0 = " not verified (no certificates)"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r8.<init>(r0)
            throw r8
        L_0x00d8:
            com.fossil.sn7 r3 = r0.a()
            com.fossil.go7 r0 = r0.k()
            java.lang.String r0 = r0.g()
            java.util.List r5 = r4.c()
            r3.a(r0, r5)
            boolean r8 = r8.c()
            if (r8 == 0) goto L_0x00f9
            com.fossil.mq7 r8 = com.fossil.mq7.d()
            java.lang.String r2 = r8.b(r1)
        L_0x00f9:
            r7.e = r1
            com.fossil.sr7 r8 = com.fossil.ir7.b(r1)
            com.fossil.ar7 r8 = com.fossil.ir7.a(r8)
            r7.i = r8
            java.net.Socket r8 = r7.e
            com.fossil.qr7 r8 = com.fossil.ir7.a(r8)
            com.fossil.zq7 r8 = com.fossil.ir7.a(r8)
            r7.j = r8
            r7.f = r4
            if (r2 == 0) goto L_0x011a
            com.fossil.jo7 r8 = com.fossil.jo7.get(r2)
            goto L_0x011c
        L_0x011a:
            com.fossil.jo7 r8 = com.fossil.jo7.HTTP_1_1
        L_0x011c:
            r7.g = r8
            if (r1 == 0) goto L_0x0127
            com.fossil.mq7 r8 = com.fossil.mq7.d()
            r8.a(r1)
        L_0x0127:
            return
        L_0x0128:
            r8 = move-exception
            r2 = r1
            goto L_0x013e
        L_0x012b:
            r8 = move-exception
            r2 = r1
            goto L_0x0131
        L_0x012e:
            r8 = move-exception
            goto L_0x013e
        L_0x0130:
            r8 = move-exception
        L_0x0131:
            boolean r0 = com.fossil.ro7.a(r8)     // Catch:{ all -> 0x012e }
            if (r0 == 0) goto L_0x013d
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x012e }
            r0.<init>(r8)     // Catch:{ all -> 0x012e }
            throw r0     // Catch:{ all -> 0x012e }
        L_0x013d:
            throw r8     // Catch:{ all -> 0x012e }
        L_0x013e:
            if (r2 == 0) goto L_0x0147
            com.fossil.mq7 r0 = com.fossil.mq7.d()
            r0.a(r2)
        L_0x0147:
            com.fossil.ro7.a(r2)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bp7.a(com.fossil.ap7):void");
    }

    @DexIgnore
    public final lo7 a(int i2, int i3, lo7 lo7, go7 go7) throws IOException {
        String str = "CONNECT " + ro7.a(go7, true) + " HTTP/1.1";
        while (true) {
            rp7 rp7 = new rp7(null, null, this.i, this.j);
            this.i.d().a((long) i2, TimeUnit.MILLISECONDS);
            this.j.d().a((long) i3, TimeUnit.MILLISECONDS);
            rp7.a(lo7.c(), str);
            rp7.a();
            Response.a a = rp7.a(false);
            a.a(lo7);
            Response a2 = a.a();
            long a3 = kp7.a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            sr7 b2 = rp7.b(a3);
            ro7.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            int e2 = a2.e();
            if (e2 != 200) {
                if (e2 == 407) {
                    lo7 authenticate = this.c.a().g().authenticate(this.c, a2);
                    if (authenticate == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    } else if ("close".equalsIgnoreCase(a2.b(WebSocketHandler.HEADER_CONNECTION))) {
                        return authenticate;
                    } else {
                        lo7 = authenticate;
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a2.e());
                }
            } else if (this.i.buffer().h() && this.j.buffer().h()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        }
    }

    @DexIgnore
    public boolean a(nn7 nn7, no7 no7) {
        if (this.n.size() >= this.m || this.k || !po7.a.a(this.c.a(), nn7)) {
            return false;
        }
        if (nn7.k().g().equals(f().a().k().g())) {
            return true;
        }
        if (this.h == null || no7 == null || no7.b().type() != Proxy.Type.DIRECT || this.c.b().type() != Proxy.Type.DIRECT || !this.c.d().equals(no7.d()) || no7.a().d() != rq7.a || !a(nn7.k())) {
            return false;
        }
        try {
            nn7.a().a(nn7.k().g(), d().c());
            return true;
        } catch (SSLPeerUnverifiedException unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(go7 go7) {
        if (go7.k() != this.c.a().k().k()) {
            return false;
        }
        if (go7.g().equals(this.c.a().k().g())) {
            return true;
        }
        if (this.f == null || !rq7.a.a(go7.g(), (X509Certificate) this.f.c().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public ip7 a(OkHttpClient okHttpClient, Interceptor.Chain chain, fp7 fp7) throws SocketException {
        if (this.h != null) {
            return new xp7(okHttpClient, chain, fp7, this.h);
        }
        this.e.setSoTimeout(chain.a());
        this.i.d().a((long) chain.a(), TimeUnit.MILLISECONDS);
        this.j.d().a((long) chain.b(), TimeUnit.MILLISECONDS);
        return new rp7(okHttpClient, fp7, this.i, this.j);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public boolean a(boolean z) {
        if (this.e.isClosed() || this.e.isInputShutdown() || this.e.isOutputShutdown()) {
            return false;
        }
        yp7 yp7 = this.h;
        if (yp7 != null) {
            return yp7.e(System.nanoTime());
        }
        if (z) {
            try {
                int soTimeout = this.e.getSoTimeout();
                try {
                    this.e.setSoTimeout(1);
                    if (this.i.h()) {
                        this.e.setSoTimeout(soTimeout);
                        return false;
                    }
                    this.e.setSoTimeout(soTimeout);
                    return true;
                } catch (Throwable th) {
                    this.e.setSoTimeout(soTimeout);
                    throw th;
                }
            } catch (SocketTimeoutException unused) {
            } catch (IOException unused2) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yp7.j
    public void a(aq7 aq7) throws IOException {
        aq7.a(tp7.REFUSED_STREAM);
    }

    @DexIgnore
    @Override // com.fossil.yp7.j
    public void a(yp7 yp7) {
        synchronized (this.b) {
            this.m = yp7.b();
        }
    }

    @DexIgnore
    @Override // com.fossil.un7
    public jo7 a() {
        return this.g;
    }
}
