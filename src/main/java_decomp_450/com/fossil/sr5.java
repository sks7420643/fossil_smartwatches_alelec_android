package com.fossil;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.be5;
import com.fossil.cy6;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr5 extends ho5 implements c76, ro5, cy6.g {
    @DexIgnore
    public static /* final */ a X; // = new a(null);
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public n86 F;
    @DexIgnore
    public j76 G;
    @DexIgnore
    public s96 H;
    @DexIgnore
    public ac6 I;
    @DexIgnore
    public ed6 J;
    @DexIgnore
    public wa6 K;
    @DexIgnore
    public qw6<q25> L;
    @DexIgnore
    public b76 M;
    @DexIgnore
    public /* final */ ArrayList<Fragment> N; // = new ArrayList<>();
    @DexIgnore
    public int O;
    @DexIgnore
    public vz6 P;
    @DexIgnore
    public ObjectAnimator Q;
    @DexIgnore
    public int R; // = -1;
    @DexIgnore
    public iw S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public HashMap W;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final sr5 a() {
            return new sr5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar b;

        @DexIgnore
        public b(sr5 sr5, ProgressBar progressBar) {
            this.a = sr5;
            this.b = progressBar;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.b.setProgress(0);
            this.b.setVisibility(4);
            this.a.R = -1;
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ q25 a;
        @DexIgnore
        public /* final */ /* synthetic */ Animation b;
        @DexIgnore
        public /* final */ /* synthetic */ sr5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                ee7.b(str, "serial");
                ee7.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "onImageCallback, filePath=" + str2);
                if (!TextUtils.isEmpty(str2)) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onImageCallback, new logo");
                    sr5.c(this.a.c).a(str2).a((ImageView) this.a.a.O);
                    c cVar = this.a;
                    cVar.a.O.startAnimation(cVar.b);
                }
            }
        }

        @DexIgnore
        public c(q25 q25, Animation animation, sr5 sr5) {
            this.a = q25;
            this.b = animation;
            this.c = sr5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "activeDeviceSerialLiveData onChange " + str);
            if (!TextUtils.isEmpty(str)) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                ee7.a((Object) str, "serial");
                with.setSerialNumber(str).setSerialPrefix(be5.o.b(str)).setType(Constants.DeviceType.TYPE_BRAND_LOGO).setImageCallback(new a(this)).download();
                return;
            }
            this.a.O.setImageResource(2131231229);
            this.a.O.startAnimation(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public d(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(4);
            sr5.b(this.a).a(4);
            this.a.o(4);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public f(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                ee7.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public g(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final boolean onLongClick(View view) {
            DebugActivity.a aVar = DebugActivity.M;
            FragmentActivity requireActivity = this.a.requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            aVar.a(requireActivity);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public h(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(0);
            sr5.b(this.a).a(0);
            this.a.o(0);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public i(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(1);
            sr5.b(this.a).a(1);
            this.a.o(1);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public j(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(2);
            sr5.b(this.a).a(2);
            this.a.o(2);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public k(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(3);
            sr5.b(this.a).a(3);
            this.a.o(3);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sr5 a;

        @DexIgnore
        public l(sr5 sr5) {
            this.a = sr5;
        }

        @DexIgnore
        public final void onClick(View view) {
            sr5.b(this.a).b(5);
            sr5.b(this.a).a(5);
            this.a.o(5);
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements AppBarLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ q25 a;

        @DexIgnore
        public m(q25 q25) {
            this.a = q25;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.c
        public void a(AppBarLayout appBarLayout, int i) {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a.X;
            ee7.a((Object) customSwipeRefreshLayout, "binding.srlPullToSync");
            customSwipeRefreshLayout.setEnabled(Math.abs(i) == 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ qe7 b;

        @DexIgnore
        public n(qe7 qe7, FlexibleProgressBar flexibleProgressBar, qe7 qe72, sr5 sr5, int i) {
            this.a = flexibleProgressBar;
            this.b = qe72;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FlexibleProgressBar flexibleProgressBar = this.a;
            ee7.a((Object) flexibleProgressBar, "it");
            int i = this.b.element;
            ee7.a((Object) valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                flexibleProgressBar.setProgress(i + ((Integer) animatedValue).intValue());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ sr5 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public o(qe7 qe7, FlexibleProgressBar flexibleProgressBar, qe7 qe72, sr5 sr5, int i) {
            this.a = flexibleProgressBar;
            this.b = sr5;
            this.c = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            ee7.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            ee7.b(animator, "animation");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "onAnimationEnd " + this.c);
            sr5 sr5 = this.b;
            FlexibleProgressBar flexibleProgressBar = this.a;
            ee7.a((Object) flexibleProgressBar, "it");
            sr5.a(flexibleProgressBar, this.c);
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            ee7.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            ee7.b(animator, "animation");
        }
    }

    @DexIgnore
    public sr5() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.T = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("hybridInactiveTab");
        this.U = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        this.V = Color.parseColor(b4 != null ? b4 : str);
    }

    @DexIgnore
    public static final /* synthetic */ b76 b(sr5 sr5) {
        b76 b76 = sr5.M;
        if (b76 != null) {
            return b76;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ iw c(sr5 sr5) {
        iw iwVar = sr5.S;
        if (iwVar != null) {
            return iwVar;
        }
        ee7.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void C(boolean z2) {
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                FlexibleTextView flexibleTextView = a2.M;
                ee7.a((Object) flexibleTextView, "it.ftvLowBattery");
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886794);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{"25%"}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                NestedScrollView nestedScrollView = a2.P;
                ee7.a((Object) nestedScrollView, "it.nsvLowBattery");
                nestedScrollView.setVisibility(0);
                return;
            }
            NestedScrollView nestedScrollView2 = a2.P;
            ee7.a((Object) nestedScrollView2, "it.nsvLowBattery");
            nestedScrollView2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void D() {
        AppBarLayout appBarLayout;
        if (isActive()) {
            qw6<q25> qw6 = this.L;
            if (qw6 != null) {
                q25 a2 = qw6.a();
                if (a2 != null && (appBarLayout = a2.q) != null) {
                    appBarLayout.a(true, true);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void J() {
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.e(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void P() {
        if (isActive()) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.c(childFragmentManager);
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void Q0() {
        if (isActive()) {
            qw6<q25> qw6 = this.L;
            if (qw6 != null) {
                q25 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    ee7.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = a2.Q;
                    ee7.a((Object) flexibleProgressBar, "it.pbProgress");
                    flexibleProgressBar.setMax(100);
                    a2.X.setDisableSwipe(true);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void T(boolean z2) {
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                if (z2) {
                    FlexibleProgressBar flexibleProgressBar = a2.Y;
                    ee7.a((Object) flexibleProgressBar, "syncProgress");
                    flexibleProgressBar.setVisibility(0);
                } else {
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    ee7.a((Object) flexibleProgressBar2, "syncProgress");
                    flexibleProgressBar2.setVisibility(4);
                }
                View view = a2.d0;
                ee7.a((Object) view, "vBorderBottom");
                view.setVisibility(4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void W0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.U(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void Y0() {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "showAutoSync");
        n(0);
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                customSwipeRefreshLayout.g();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.W;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeDashboardFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void f(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "updateOtaProgress " + i2 + " isActive " + isActive());
        if (isActive()) {
            qw6<q25> qw6 = this.L;
            if (qw6 != null) {
                q25 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    ee7.a((Object) constraintLayout, "it.clUpdateFw");
                    if (constraintLayout.getVisibility() != 0) {
                        ConstraintLayout constraintLayout2 = a2.A;
                        ee7.a((Object) constraintLayout2, "it.clUpdateFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar = a2.Q;
                        ee7.a((Object) flexibleProgressBar, "it.pbProgress");
                        flexibleProgressBar.setMax(100);
                    }
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    ee7.a((Object) flexibleProgressBar2, "it.syncProgress");
                    if (flexibleProgressBar2.getVisibility() == 0) {
                        g1();
                    }
                    a2.X.setDisableSwipe(true);
                    FlexibleProgressBar flexibleProgressBar3 = a2.Q;
                    ee7.a((Object) flexibleProgressBar3, "it.pbProgress");
                    flexibleProgressBar3.setProgress(i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        b76 b76 = this.M;
        if (b76 == null) {
            return;
        }
        if (b76 != null) {
            int i2 = b76.i();
            if (!this.N.isEmpty()) {
                int size = this.N.size();
                int i3 = 0;
                while (i3 < size) {
                    if (this.N.get(i3) instanceof ro5) {
                        Fragment fragment = this.N.get(i3);
                        if (fragment != null) {
                            ((ro5) fragment).r(i3 == i2);
                        } else {
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                        }
                    }
                    i3++;
                }
            }
            if (i2 == 0) {
                V("steps_view");
            } else if (i2 == 1) {
                V("active_minutes_view");
            } else if (i2 == 2) {
                V("calories_view");
            } else if (i2 == 3) {
                V("heart_rate_view");
            } else if (i2 == 5) {
                V("sleep_view");
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "cancelSyncProgress");
        ObjectAnimator objectAnimator = this.Q;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.Y;
                ee7.a((Object) flexibleProgressBar, "it.syncProgress");
                flexibleProgressBar.setVisibility(4);
                FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                ee7.a((Object) flexibleProgressBar2, "it.syncProgress");
                flexibleProgressBar2.setProgress(0);
                a2.X.c();
            }
            b76 b76 = this.M;
            if (b76 != null) {
                b76.a(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void h1() {
        Fragment b2 = getChildFragmentManager().b(m86.r.a());
        Fragment b3 = getChildFragmentManager().b("DashboardActiveTimeFragment");
        Fragment b4 = getChildFragmentManager().b("DashboardCaloriesFragment");
        Fragment b5 = getChildFragmentManager().b(zb6.r.a());
        Fragment b6 = getChildFragmentManager().b(dd6.r.a());
        Fragment b7 = getChildFragmentManager().b(va6.r.a());
        if (b2 == null) {
            b2 = m86.r.b();
        }
        if (b3 == null) {
            b3 = i76.q.a();
        }
        if (b4 == null) {
            b4 = new r96();
        }
        if (b5 == null) {
            b5 = zb6.r.b();
        }
        if (b6 == null) {
            b6 = dd6.r.b();
        }
        if (b7 == null) {
            b7 = va6.r.b();
        }
        this.N.clear();
        this.N.add(b2);
        this.N.add(b3);
        this.N.add(b4);
        this.N.add(b5);
        this.N.add(b7);
        this.N.add(b6);
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                b76 b76 = this.M;
                if (b76 != null) {
                    be5.a aVar = be5.o;
                    if (b76 == null) {
                        ee7.d("mPresenter");
                        throw null;
                    } else if (aVar.a(b76.h())) {
                        FlexibleFitnessTab flexibleFitnessTab = a2.E;
                        ee7.a((Object) flexibleFitnessTab, "flexibleTabActiveTime");
                        flexibleFitnessTab.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab2 = a2.I;
                        ee7.a((Object) flexibleFitnessTab2, "flexibleTabHeartRate");
                        flexibleFitnessTab2.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab3 = a2.H;
                        ee7.a((Object) flexibleFitnessTab3, "flexibleTabGoalTracking");
                        flexibleFitnessTab3.setVisibility(8);
                    } else {
                        FlexibleFitnessTab flexibleFitnessTab4 = a2.E;
                        ee7.a((Object) flexibleFitnessTab4, "flexibleTabActiveTime");
                        flexibleFitnessTab4.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab5 = a2.I;
                        ee7.a((Object) flexibleFitnessTab5, "flexibleTabHeartRate");
                        flexibleFitnessTab5.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                        ee7.a((Object) flexibleFitnessTab6, "flexibleTabGoalTracking");
                        flexibleFitnessTab6.setVisibility(0);
                    }
                }
                ViewPager2 viewPager2 = a2.W;
                ee7.a((Object) viewPager2, "rvTabs");
                viewPager2.setAdapter(new qz6(getChildFragmentManager(), this.N));
                if (a2.W.getChildAt(0) != null) {
                    View childAt = a2.W.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.W;
                ee7.a((Object) viewPager22, "rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            tj4 f2 = PortfolioApp.g0.c().f();
            if (b2 != null) {
                m86 m86 = (m86) b2;
                if (b3 != null) {
                    i76 i76 = (i76) b3;
                    r96 r96 = (r96) b4;
                    if (b5 != null) {
                        zb6 zb6 = (zb6) b5;
                        if (b6 != null) {
                            dd6 dd6 = (dd6) b6;
                            if (b7 != null) {
                                f2.a(new u66(m86, i76, r96, zb6, dd6, (va6) b7)).a(this);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                        }
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        String b2 = eh5.l.a().b("onDianaStepsTab");
        if (b2 != null) {
            this.g = Color.parseColor(b2);
            i97 i97 = i97.a;
        } else {
            this.g = v6.a(requireContext(), 2131099947);
            i97 i972 = i97.a;
        }
        String b3 = eh5.l.a().b("onDianaActiveMinutesTab");
        if (b3 != null) {
            this.h = Color.parseColor(b3);
            i97 i973 = i97.a;
        } else {
            this.h = v6.a(requireContext(), 2131099947);
            i97 i974 = i97.a;
        }
        String b4 = eh5.l.a().b("onDianaActiveCaloriesTab");
        if (b4 != null) {
            this.i = Color.parseColor(b4);
            i97 i975 = i97.a;
        } else {
            this.i = v6.a(requireContext(), 2131099947);
            i97 i976 = i97.a;
        }
        String b5 = eh5.l.a().b("onDianaHeartRateTab");
        if (b5 != null) {
            this.j = Color.parseColor(b5);
            i97 i977 = i97.a;
        } else {
            this.j = v6.a(requireContext(), 2131099947);
            i97 i978 = i97.a;
        }
        String b6 = eh5.l.a().b("onDianaSleepTab");
        if (b6 != null) {
            this.p = Color.parseColor(b6);
            i97 i979 = i97.a;
        } else {
            this.p = v6.a(requireContext(), 2131099947);
            i97 i9710 = i97.a;
        }
        String b7 = eh5.l.a().b("onDianaInactiveTab");
        if (b7 != null) {
            this.q = Color.parseColor(b7);
            i97 i9711 = i97.a;
        } else {
            this.q = v6.a(requireContext(), 2131099951);
            i97 i9712 = i97.a;
        }
        String b8 = eh5.l.a().b("onHybridStepsTab");
        if (b8 != null) {
            this.r = Color.parseColor(b8);
            i97 i9713 = i97.a;
        } else {
            this.r = v6.a(requireContext(), 2131099947);
            i97 i9714 = i97.a;
        }
        String b9 = eh5.l.a().b("onHybridActiveCaloriesTab");
        if (b9 != null) {
            this.s = Color.parseColor(b9);
            i97 i9715 = i97.a;
        } else {
            this.s = v6.a(requireContext(), 2131099947);
            i97 i9716 = i97.a;
        }
        String b10 = eh5.l.a().b("onHybridSleepTab");
        if (b10 != null) {
            this.t = Color.parseColor(b10);
            i97 i9717 = i97.a;
        } else {
            this.t = v6.a(requireContext(), 2131099947);
            i97 i9718 = i97.a;
        }
        String b11 = eh5.l.a().b("onHybridInactiveTab");
        if (b11 != null) {
            this.u = Color.parseColor(b11);
            i97 i9719 = i97.a;
        } else {
            this.u = v6.a(requireContext(), 2131099952);
            i97 i9720 = i97.a;
        }
        String b12 = eh5.l.a().b("onHybridGoalTrackingTab");
        if (b12 != null) {
            this.v = Color.parseColor(b12);
            i97 i9721 = i97.a;
        } else {
            this.v = v6.a(requireContext(), 2131099947);
            i97 i9722 = i97.a;
        }
        String b13 = eh5.l.a().b("dianaStepsTab");
        if (b13 != null) {
            this.w = Color.parseColor(b13);
            i97 i9723 = i97.a;
        } else {
            this.w = v6.a(requireContext(), 2131099812);
            i97 i9724 = i97.a;
        }
        String b14 = eh5.l.a().b("dianaActiveMinutesTab");
        if (b14 != null) {
            this.x = Color.parseColor(b14);
            i97 i9725 = i97.a;
        } else {
            this.x = v6.a(requireContext(), 2131099807);
            i97 i9726 = i97.a;
        }
        String b15 = eh5.l.a().b("dianaActiveCaloriesTab");
        if (b15 != null) {
            this.y = Color.parseColor(b15);
            i97 i9727 = i97.a;
        } else {
            this.y = v6.a(requireContext(), 2131099806);
            i97 i9728 = i97.a;
        }
        String b16 = eh5.l.a().b("dianaHeartRateTab");
        if (b16 != null) {
            this.z = Color.parseColor(b16);
            i97 i9729 = i97.a;
        } else {
            this.z = v6.a(requireContext(), 2131099808);
            i97 i9730 = i97.a;
        }
        String b17 = eh5.l.a().b("dianaSleepTab");
        if (b17 != null) {
            this.A = Color.parseColor(b17);
            i97 i9731 = i97.a;
        } else {
            this.A = v6.a(requireContext(), 2131099810);
            i97 i9732 = i97.a;
        }
        String b18 = eh5.l.a().b("dianaInactiveTab");
        if (b18 != null) {
            Color.parseColor(b18);
            i97 i9733 = i97.a;
        } else {
            v6.a(requireContext(), 2131099809);
            i97 i9734 = i97.a;
        }
        String b19 = eh5.l.a().b("hybridStepsTab");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "ABCD=" + b19);
        String b20 = eh5.l.a().b("hybridStepsTab");
        if (b20 != null) {
            this.B = Color.parseColor(b20);
            i97 i9735 = i97.a;
        } else {
            this.B = v6.a(requireContext(), 2131099947);
            i97 i9736 = i97.a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDashboardFragment", "hybridStepsColor=" + this.B);
        String b21 = eh5.l.a().b("hybridActiveCaloriesTab");
        if (b21 != null) {
            this.C = Color.parseColor(b21);
            i97 i9737 = i97.a;
        } else {
            this.C = v6.a(requireContext(), 2131099947);
            i97 i9738 = i97.a;
        }
        String b22 = eh5.l.a().b("hybridSleepTab");
        if (b22 != null) {
            this.D = Color.parseColor(b22);
            i97 i9739 = i97.a;
        } else {
            this.D = v6.a(requireContext(), 2131099947);
            i97 i9740 = i97.a;
        }
        String b23 = eh5.l.a().b("hybridInactiveTab");
        if (b23 != null) {
            Color.parseColor(b23);
            i97 i9741 = i97.a;
        } else {
            v6.a(requireContext(), 2131099952);
            i97 i9742 = i97.a;
        }
        String b24 = eh5.l.a().b("hybridGoalTrackingTab");
        if (b24 != null) {
            this.E = Color.parseColor(b24);
            i97 i9743 = i97.a;
        } else {
            this.E = v6.a(requireContext(), 2131099857);
            i97 i9744 = i97.a;
        }
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                String b25 = eh5.l.a().b("primaryText");
                if (b25 != null) {
                    a2.O.setColorFilter(Color.parseColor(b25), PorterDuff.Mode.SRC_ATOP);
                    i97 i9745 = i97.a;
                }
                a2.w.setBackgroundColor(this.T);
                a2.f0.setBackgroundColor(this.V);
                a2.t.setBackgroundColor(this.V);
                a2.A.setBackgroundColor(this.V);
                a2.u.setBackgroundColor(this.V);
                a2.z.setBackgroundColor(this.V);
                i97 i9746 = i97.a;
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                RingProgressBar ringProgressBar = a2.T;
                ee7.a((Object) ringProgressBar, "it.rpbBiggest");
                a(ringProgressBar, RingProgressBar.b.STEPS);
                be5.a aVar = be5.o;
                b76 b76 = this.M;
                if (b76 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (aVar.a(b76.h())) {
                    RingProgressBar ringProgressBar2 = a2.V;
                    ee7.a((Object) ringProgressBar2, "it.rpbSmallest");
                    ringProgressBar2.setVisibility(0);
                    RingProgressBar ringProgressBar3 = a2.S;
                    ee7.a((Object) ringProgressBar3, "it.rpbBig");
                    a(ringProgressBar3, RingProgressBar.b.ACTIVE_TIME);
                    RingProgressBar ringProgressBar4 = a2.U;
                    ee7.a((Object) ringProgressBar4, "it.rpbMedium");
                    a(ringProgressBar4, RingProgressBar.b.CALORIES);
                    RingProgressBar ringProgressBar5 = a2.V;
                    ee7.a((Object) ringProgressBar5, "it.rpbSmallest");
                    a(ringProgressBar5, RingProgressBar.b.SLEEP);
                    FlexibleFitnessTab flexibleFitnessTab = a2.E;
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886630);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026n_StepsToday_Label__Mins)");
                    flexibleFitnessTab.b(a3);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.F;
                    String a4 = ig5.a(PortfolioApp.g0.c(), 2131886632);
                    ee7.a((Object) a4, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab2.b(a4);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String a5 = ig5.a(PortfolioApp.g0.c(), 2131886626);
                    ee7.a((Object) a5, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab3.b(a5);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.I;
                    String a6 = ig5.a(PortfolioApp.g0.c(), 2131886631);
                    ee7.a((Object) a6, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
                    flexibleFitnessTab4.b(a6);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.J;
                    String a7 = ig5.a(PortfolioApp.g0.c(), 2131886627);
                    ee7.a((Object) a7, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab5.b(a7);
                } else {
                    RingProgressBar ringProgressBar6 = a2.V;
                    ee7.a((Object) ringProgressBar6, "it.rpbSmallest");
                    ringProgressBar6.setVisibility(0);
                    RingProgressBar ringProgressBar7 = a2.S;
                    ee7.a((Object) ringProgressBar7, "it.rpbBig");
                    a(ringProgressBar7, RingProgressBar.b.CALORIES);
                    RingProgressBar ringProgressBar8 = a2.U;
                    ee7.a((Object) ringProgressBar8, "it.rpbMedium");
                    a(ringProgressBar8, RingProgressBar.b.SLEEP);
                    RingProgressBar ringProgressBar9 = a2.V;
                    ee7.a((Object) ringProgressBar9, "it.rpbSmallest");
                    a(ringProgressBar9, RingProgressBar.b.GOAL);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.F;
                    String a8 = ig5.a(PortfolioApp.g0.c(), 2131886727);
                    ee7.a((Object) a8, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab6.b(a8);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.G;
                    String a9 = ig5.a(PortfolioApp.g0.c(), 2131886724);
                    ee7.a((Object) a9, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab7.b(a9);
                    FlexibleFitnessTab flexibleFitnessTab8 = a2.H;
                    String a10 = ig5.a(PortfolioApp.g0.c(), 2131886726);
                    ee7.a((Object) a10, "LanguageHelper.getString\u2026epsToday_Label__OfNumber)");
                    flexibleFitnessTab8.b(a10);
                    FlexibleFitnessTab flexibleFitnessTab9 = a2.J;
                    String a11 = ig5.a(PortfolioApp.g0.c(), 2131886725);
                    ee7.a((Object) a11, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab9.b(a11);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final void n(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("runProgress state ");
        sb.append(i2);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        ee7.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("HomeDashboardFragment", sb.toString());
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null && (flexibleProgressBar = a2.Y) != null) {
                qe7 qe7 = new qe7();
                qe7.element = 0;
                qe7 qe72 = new qe7();
                qe72.element = 0;
                this.R = i2;
                int i3 = 1000;
                if (i2 == 0) {
                    qe7.element = 0;
                    qe72.element = 2000;
                    i3 = 2000;
                } else if (i2 == 1) {
                    qe7.element = 2000;
                    i3 = 7000;
                    qe72.element = SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS;
                } else if (i2 != 2) {
                    i3 = 0;
                } else {
                    qe7.element = 9000;
                    qe72.element = 1000;
                }
                ee7.a((Object) flexibleProgressBar, "it");
                flexibleProgressBar.setProgress(qe7.element);
                ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", i3);
                this.Q = ofInt;
                if (ofInt != null) {
                    ofInt.setDuration((long) qe72.element);
                    ofInt.addUpdateListener(new n(qe72, flexibleProgressBar, qe7, this, i2));
                    ofInt.addListener(new o(qe72, flexibleProgressBar, qe7, this, i2));
                    ofInt.start();
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void o(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "scroll to position=" + i2);
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                a2.F.d(this.U);
                a2.J.d(this.U);
                a2.G.d(this.U);
                a2.E.d(this.U);
                a2.I.d(this.U);
                a2.H.d(this.U);
                be5.a aVar = be5.o;
                b76 b76 = this.M;
                if (b76 != null) {
                    if (aVar.a(b76.h())) {
                        a2.F.c(this.q);
                        a2.E.c(this.q);
                        a2.G.c(this.q);
                        a2.I.c(this.q);
                        a2.J.c(this.q);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.w);
                            a2.F.c(this.g);
                            a2.F.d(this.w);
                        } else if (i2 == 1) {
                            a2.f0.setBackgroundColor(this.x);
                            a2.E.d(this.x);
                            a2.E.c(this.h);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.y);
                            a2.G.d(this.y);
                            a2.G.c(this.i);
                        } else if (i2 == 3) {
                            a2.f0.setBackgroundColor(this.z);
                            a2.I.d(this.z);
                            a2.I.c(this.j);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.A);
                            a2.J.d(this.A);
                            a2.J.c(this.p);
                        }
                    } else {
                        a2.F.c(this.u);
                        a2.H.c(this.u);
                        a2.G.c(this.u);
                        a2.J.c(this.u);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.B);
                            a2.F.c(this.r);
                            a2.F.d(this.B);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.C);
                            a2.G.d(this.C);
                            a2.G.c(this.s);
                        } else if (i2 == 4) {
                            a2.f0.setBackgroundColor(this.E);
                            a2.H.d(this.E);
                            a2.H.c(this.v);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.D);
                            a2.J.d(this.D);
                            a2.J.c(this.t);
                        }
                    }
                    qw6<q25> qw62 = this.L;
                    if (qw62 != null) {
                        q25 a3 = qw62.a();
                        if (!(a3 == null || (viewPager2 = a3.W) == null)) {
                            viewPager2.a(i2, false);
                        }
                        this.O = i2;
                        b76 b762 = this.M;
                        if (b762 != null) {
                            b762.a(i2);
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    } else {
                        ee7.d("mBinding");
                        throw null;
                    }
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onActivityCreated");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.L = new qw6<>(this, (q25) qb.a(layoutInflater, 2131558573, viewGroup, false, a1()));
        i1();
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        b76 b76 = this.M;
        if (b76 == null) {
            return;
        }
        if (b76 != null) {
            b76.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        b76 b76 = this.M;
        if (b76 == null) {
            return;
        }
        if (b76 != null) {
            o(b76.i());
            j1();
            b76 b762 = this.M;
            if (b762 != null) {
                b762.f();
                jf5 c1 = c1();
                if (c1 != null) {
                    c1.d();
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        h1();
        iw a2 = aw.a(this);
        ee7.a((Object) a2, "Glide.with(this)");
        this.S = a2;
        qw6<q25> qw6 = this.L;
        vz6 vz6 = null;
        if (qw6 != null) {
            q25 a3 = qw6.a();
            if (a3 != null) {
                PortfolioApp.g0.c().d().a(getViewLifecycleOwner(), new c(a3, AnimationUtils.loadAnimation(requireContext(), 2130771996), this));
                if (tm4.a.a().e()) {
                    String h2 = PortfolioApp.g0.c().h();
                    if (h2 != null) {
                        String upperCase = h2.toUpperCase();
                        ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        FlexibleTextView flexibleTextView = a3.K;
                        ee7.a((Object) flexibleTextView, "binding.ftvDescription");
                        we7 we7 = we7.a;
                        String a4 = ig5.a(getContext(), 2131887015);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026rABrandSmartwatchToStart)");
                        String format = String.format(a4, Arrays.copyOf(new Object[]{upperCase}, 1));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView.setText(format);
                    } else {
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    String a5 = ig5.a(getContext(), 2131887015);
                    ee7.a((Object) a5, "withoutDeviceText");
                    String a6 = mh7.a(a5, "%s", "", false, 4, (Object) null);
                    FlexibleTextView flexibleTextView2 = a3.K;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(a6);
                }
                FlexibleProgressBar flexibleProgressBar = a3.Y;
                ee7.a((Object) flexibleProgressBar, "binding.syncProgress");
                flexibleProgressBar.setMax(10000);
                a3.X.setOnRefreshListener(new e(a3, this));
                View headView = a3.X.getHeadView();
                if (headView instanceof vz6) {
                    vz6 = headView;
                }
                this.P = vz6;
                a3.D.setOnClickListener(new f(this));
                if (!PortfolioApp.g0.c().G()) {
                    a3.O.setOnLongClickListener(new g(this));
                }
                a3.q.a((AppBarLayout.d) new m(a3));
                a3.F.setOnClickListener(new h(this));
                a3.E.setOnClickListener(new i(this));
                a3.G.setOnClickListener(new j(this));
                a3.I.setOnClickListener(new k(this));
                a3.J.setOnClickListener(new l(this));
                a3.H.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void q(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDashboardFragment", "syncCompleted - success: " + z2);
        if (!z2) {
            g1();
        } else {
            n(1);
        }
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("HomeDashboardFragment visible=");
        sb.append(z2);
        sb.append(", tracer=");
        sb.append(c1());
        sb.append(", isRunning=");
        jf5 c1 = c1();
        sb.append(c1 != null ? Boolean.valueOf(c1.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z2) {
            jf5 c12 = c1();
            if (c12 != null) {
                c12.d();
            }
            if (this.L != null) {
                int i2 = this.R;
                if (i2 == 2 || i2 == -1) {
                    g1();
                    return;
                }
                return;
            }
            return;
        }
        jf5 c13 = c1();
        if (c13 != null) {
            c13.a("");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CustomSwipeRefreshLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ q25 a;
        @DexIgnore
        public /* final */ /* synthetic */ sr5 b;

        @DexIgnore
        public e(q25 q25, sr5 sr5) {
            this.a = q25;
            this.b = sr5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.d
        public void a(boolean z) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onEndSwipe");
            this.b.T(z);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.d
        public void b() {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onStartSwipe");
            FlexibleProgressBar flexibleProgressBar = this.a.Y;
            ee7.a((Object) flexibleProgressBar, "binding.syncProgress");
            flexibleProgressBar.setVisibility(4);
            View view = this.a.d0;
            ee7.a((Object) view, "binding.vBorderBottom");
            view.setVisibility(0);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.d
        public void a() {
            FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onRefresh");
            sr5.b(this.b).j();
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void c(boolean z2) {
        if (isActive()) {
            qw6<q25> qw6 = this.L;
            if (qw6 != null) {
                q25 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    ee7.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(8);
                    a2.X.setDisableSwipe(false);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void b(Date date) {
        ee7.b(date, "date");
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.a0;
                ee7.a((Object) flexibleTextView, "tvToday");
                we7 we7 = we7.a;
                String string = PortfolioApp.g0.c().getString(2131886645);
                ee7.a((Object) string, "PortfolioApp.instance.ge\u2026ay_Title__TodayMonthDate)");
                String format = String.format(string, Arrays.copyOf(new Object[]{ze5.a(date)}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(RingProgressBar ringProgressBar, RingProgressBar.b bVar) {
        int i2 = tr5.a[bVar.ordinal()];
        if (i2 == 1) {
            ringProgressBar.setIconSource(2131231185);
            ringProgressBar.c(this.x, this.V);
            ringProgressBar.a("dianaActiveMinuteRing", "dianaUnfilledRing");
        } else if (i2 == 2) {
            ringProgressBar.setIconSource(2131231189);
            ringProgressBar.c(this.w, this.V);
            be5.a aVar = be5.o;
            b76 b76 = this.M;
            if (b76 == null) {
                ee7.d("mPresenter");
                throw null;
            } else if (aVar.a(b76.h())) {
                ringProgressBar.a("dianaStepsRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridStepsRing", "hybridUnfilledRing");
            }
        } else if (i2 == 3) {
            ringProgressBar.setIconSource(2131231186);
            ringProgressBar.c(this.y, this.V);
            be5.a aVar2 = be5.o;
            b76 b762 = this.M;
            if (b762 == null) {
                ee7.d("mPresenter");
                throw null;
            } else if (aVar2.a(b762.h())) {
                ringProgressBar.a("dianaActiveCaloriesRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridActiveCaloriesRing", "hybridUnfilledRing");
            }
        } else if (i2 == 4) {
            ringProgressBar.setIconSource(2131231188);
            ringProgressBar.c(this.A, this.V);
            be5.a aVar3 = be5.o;
            b76 b763 = this.M;
            if (b763 == null) {
                ee7.d("mPresenter");
                throw null;
            } else if (aVar3.a(b763.h())) {
                ringProgressBar.a("dianaSleepRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.a("hybridSleepRing", "hybridUnfilledRing");
            }
        } else if (i2 == 5) {
            ringProgressBar.setIconSource(2131231187);
            ringProgressBar.c(this.E, this.V);
            ringProgressBar.a("hybridGoalTrackingRing", "hybridUnfilledRing");
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void b(String str, String str2) {
        vz6 vz6 = this.P;
        if (vz6 != null) {
            vz6.a(str, str2);
        }
    }

    @DexIgnore
    public void a(b76 b76) {
        ee7.b(b76, "presenter");
        this.M = b76;
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary) {
        float f2;
        float f3;
        float f4;
        float f5;
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 != null) {
                int a3 = ge5.c.a(activitySummary, gb5.ACTIVE_TIME);
                float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                if (a3 > 0) {
                    f2 = (activitySummary != null ? (float) activitySummary.getActiveTime() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a3);
                } else {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a4 = ge5.c.a(activitySummary, gb5.TOTAL_STEPS);
                if (a4 > 0) {
                    f3 = (activitySummary != null ? (float) activitySummary.getSteps() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a4);
                } else {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a5 = ge5.c.a(activitySummary, gb5.CALORIES);
                if (a5 > 0) {
                    f4 = (activitySummary != null ? (float) activitySummary.getCalories() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a5);
                } else {
                    f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a6 = ge5.c.a(mFSleepDay);
                if (a6 > 0) {
                    f5 = (mFSleepDay != null ? (float) mFSleepDay.getSleepMinutes() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a6);
                } else {
                    f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a7 = ge5.c.a(goalTrackingSummary);
                if (a7 > 0) {
                    if (goalTrackingSummary != null) {
                        f6 = (float) goalTrackingSummary.getTotalTracked();
                    }
                    f6 /= (float) a7;
                }
                boolean z2 = true;
                boolean z3 = f3 >= 1.0f && f4 >= 1.0f && f5 >= 1.0f;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "updateVisualization steps: " + f3 + ", time: " + f2 + ", calories: " + f4 + ", sleep: " + f5);
                be5.a aVar = be5.o;
                b76 b76 = this.M;
                if (b76 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (aVar.a(b76.h())) {
                    if (!z3 || f2 < 1.0f) {
                        z2 = false;
                    }
                    a2.T.a(f3, z2);
                    a2.S.a(f2, z2);
                    a2.U.a(f4, z2);
                    a2.V.a(f5, z2);
                } else {
                    if (!z3 || f6 < 1.0f) {
                        z2 = false;
                    }
                    a2.T.a(f3, z2);
                    a2.S.a(f4, z2);
                    a2.U.a(f5, z2);
                    a2.V.a(f6, z2);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z2) {
        int i2;
        double d2;
        double d3;
        int i3;
        int i4;
        PortfolioApp portfolioApp;
        String str;
        String str2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("setDataSummaryForTabs - latestGoalTrackingTarget=");
        sb.append(num);
        sb.append(", ");
        sb.append("heartRateResting=");
        sb.append(num2);
        sb.append(", isNewSession=");
        sb.append(z2);
        sb.append(", mBinding.get()=");
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            sb.append(qw6.a());
            sb.append(", hashCode=");
            sb.append(hashCode());
            local.d("HomeDashboardFragment", sb.toString());
            qw6<q25> qw62 = this.L;
            if (qw62 != null) {
                q25 a2 = qw62.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.g0.c();
                    int sleepMinutes = mFSleepDay != null ? mFSleepDay.getSleepMinutes() : 0;
                    int intValue = num2 != null ? num2.intValue() : 0;
                    if (activitySummary != null) {
                        d2 = activitySummary.getSteps();
                        i2 = activitySummary.getActiveTime();
                        d3 = activitySummary.getCalories();
                    } else {
                        d3 = 0.0d;
                        d2 = 0.0d;
                        i2 = 0;
                    }
                    if (goalTrackingSummary != null) {
                        i3 = goalTrackingSummary.getGoalTarget();
                        i4 = goalTrackingSummary.getTotalTracked();
                    } else {
                        i4 = 0;
                        i3 = 0;
                    }
                    if (i3 == 0 && num != null) {
                        i3 = num.intValue();
                    }
                    String a3 = ig5.a(c2, 2131887287);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDashboardFragment", "setDataSummaryForTabs - steps=" + d2 + ", activeTime=" + i2 + ", " + "calories=" + d3 + ", goalTarget=" + i3 + ", goalTotalTracked=" + i4 + ", " + "sleepMinutes=" + sleepMinutes + ", resting=" + intValue);
                    FlexibleFitnessTab flexibleFitnessTab = a2.F;
                    int i5 = (d2 > 0.0d ? 1 : (d2 == 0.0d ? 0 : -1));
                    String b2 = (i5 != 0 || !z2) ? af5.a.b(Integer.valueOf(af7.a(d2))) : a3;
                    ee7.a((Object) b2, "if (steps == 0.0 && isNe\u2026ormat(steps.roundToInt())");
                    flexibleFitnessTab.c(b2);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.E;
                    String a4 = (i2 != 0 || !z2) ? af5.a.a(Integer.valueOf(i2)) : a3;
                    ee7.a((Object) a4, "if (activeTime == 0 && i\u2026iveTimeFormat(activeTime)");
                    flexibleFitnessTab2.c(a4);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String a5 = (d3 != 0.0d || !z2) ? af5.a.a(Float.valueOf((float) d3)) : a3;
                    ee7.a((Object) a5, "if (calories == 0.0 && i\u2026ormat(calories.toFloat())");
                    flexibleFitnessTab3.c(a5);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.J;
                    if (sleepMinutes != 0 || !z2) {
                        portfolioApp = c2;
                        StringBuilder sb2 = new StringBuilder();
                        we7 we7 = we7.a;
                        Locale locale = Locale.US;
                        ee7.a((Object) locale, "Locale.US");
                        String format = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes / 60)}, 1));
                        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        sb2.append(format);
                        sb2.append(":");
                        we7 we72 = we7.a;
                        Locale locale2 = Locale.US;
                        ee7.a((Object) locale2, "Locale.US");
                        String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes % 60)}, 1));
                        ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                        sb2.append(format2);
                        str = sb2.toString();
                    } else {
                        portfolioApp = c2;
                        str = ig5.a(portfolioApp, 2131887289);
                    }
                    ee7.a((Object) str, "if (sleepMinutes == 0 &&\u2026              .toString()");
                    flexibleFitnessTab4.c(str);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.H;
                    if (i3 != 0 || !z2) {
                        we7 we73 = we7.a;
                        String a6 = ig5.a(portfolioApp, 2131886705);
                        ee7.a((Object) a6, "LanguageHelper.getString\u2026ingToday_Label__OfNumber)");
                        str2 = String.format(a6, Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
                        ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                    } else {
                        str2 = a3;
                    }
                    ee7.a((Object) str2, "if (goalTarget == 0 && i\u2026l__OfNumber), goalTarget)");
                    flexibleFitnessTab5.b(str2);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                    String valueOf = (i4 != 0 || !z2) ? String.valueOf(i4) : a3;
                    ee7.a((Object) valueOf, "if (goalTotalTracked == \u2026alTotalTracked.toString()");
                    flexibleFitnessTab6.c(valueOf);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.I;
                    String valueOf2 = (i5 != 0 || !z2) ? String.valueOf(intValue) : a3;
                    ee7.a((Object) valueOf2, "if (steps == 0.0 && isNe\u2026e else resting.toString()");
                    flexibleFitnessTab7.c(valueOf2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if ((str.length() > 0) && ee7.a((Object) str, (Object) "ASK_TO_CANCEL_WORKOUT") && getActivity() != null) {
            if (i2 == 2131363229) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "don't quit");
                b76 b76 = this.M;
                if (b76 != null) {
                    b76.a(PortfolioApp.g0.c().c(), false);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131363307) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "quit and sync");
                b76 b762 = this.M;
                if (b762 != null) {
                    b762.a(PortfolioApp.g0.c().c(), true);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(ProgressBar progressBar, int i2) {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "animateSyncProgressEnd " + progressBar);
        if (i2 == 0) {
            progressBar.setVisibility(0);
        } else if (i2 == 1) {
            n(2);
        } else if (i2 == 2) {
            TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, progressBar.getY(), progressBar.getY() - ((float) progressBar.getHeight()));
            translateAnimation.setDuration((long) SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            translateAnimation.setAnimationListener(new b(this, progressBar));
            progressBar.startAnimation(translateAnimation);
            qw6<q25> qw6 = this.L;
            if (qw6 != null) {
                q25 a2 = qw6.a();
                if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                    customSwipeRefreshLayout.c();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c76
    public void a(boolean z2, boolean z3, boolean z4) {
        qw6<q25> qw6 = this.L;
        if (qw6 != null) {
            q25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z2 || !z4) {
                ConstraintLayout constraintLayout = a2.B;
                ee7.a((Object) constraintLayout, "binding.clVisualization");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.y;
                ee7.a((Object) constraintLayout2, "binding.clNoDevice");
                constraintLayout2.setVisibility(8);
                a2.X.setByPass(!z3);
                if (!z4 && !z3) {
                    ImageView imageView = a2.N;
                    ee7.a((Object) imageView, "binding.ivNoWatchFound");
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            ConstraintLayout constraintLayout3 = a2.B;
            ee7.a((Object) constraintLayout3, "binding.clVisualization");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.y;
            ee7.a((Object) constraintLayout4, "binding.clNoDevice");
            constraintLayout4.setVisibility(0);
            a2.X.setByPass(true);
            ImageView imageView2 = a2.N;
            ee7.a((Object) imageView2, "binding.ivNoWatchFound");
            imageView2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
