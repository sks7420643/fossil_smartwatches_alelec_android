package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk4 implements Factory<ie5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<pj4> c;
    @DexIgnore
    public /* final */ Provider<ch5> d;

    @DexIgnore
    public kk4(wj4 wj4, Provider<Context> provider, Provider<pj4> provider2, Provider<ch5> provider3) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static kk4 a(wj4 wj4, Provider<Context> provider, Provider<pj4> provider2, Provider<ch5> provider3) {
        return new kk4(wj4, provider, provider2, provider3);
    }

    @DexIgnore
    public static ie5 a(wj4 wj4, Context context, pj4 pj4, ch5 ch5) {
        ie5 a2 = wj4.a(context, pj4, ch5);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ie5 get() {
        return a(this.a, this.b.get(), this.c.get(), this.d.get());
    }
}
