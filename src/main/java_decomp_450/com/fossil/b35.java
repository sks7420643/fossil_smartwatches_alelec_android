package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b35 extends a35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131362522, 1);
        z.put(2131362932, 2);
        z.put(2131362365, 3);
        z.put(2131362523, 4);
        z.put(2131363012, 5);
        z.put(2131362602, 6);
    }
    */

    @DexIgnore
    public b35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 7, y, z));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public b35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[4], (TabLayout) objArr[6], (FlexibleProgressBar) objArr[2], (ConstraintLayout) objArr[0], (ViewPager2) objArr[5]);
        this.x = -1;
        ((a35) this).v.setTag(null);
        a(view);
        f();
    }
}
