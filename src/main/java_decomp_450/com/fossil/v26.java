package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v26 extends q26 {
    @DexIgnore
    public a06 e;
    @DexIgnore
    public LiveData<List<WatchFace>> f;
    @DexIgnore
    public List<WatchFace> g; // = new ArrayList();
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public DianaPreset j;
    @DexIgnore
    public String k;
    @DexIgnore
    public zd<String> l; // = new c(this);
    @DexIgnore
    public zd<List<WatchFace>> m; // = new f(this);
    @DexIgnore
    public /* final */ r26 n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ DianaPresetRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1", f = "CustomizeThemePresenter.kt", l = {241}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super DianaPreset>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super DianaPreset> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.p.getActivePresetBySerial(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(v26 v26, WatchFaceWrapper watchFaceWrapper, fb7 fb7) {
            super(2, fb7);
            this.this$0 = v26;
            this.$watchFaceWrapper = watchFaceWrapper;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$watchFaceWrapper, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            v26 v26;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.j == null) {
                    this.this$0.n.b();
                    v26 v262 = this.this$0;
                    ti7 d = v262.c();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = v262;
                    this.label = 1;
                    obj = vh7.a(d, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                    v26 = v262;
                } else {
                    v26 v263 = this.this$0;
                    v263.a(v263.j, this.$watchFaceWrapper);
                    return i97.a;
                }
            } else if (i == 1) {
                v26 = (v26) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            v26.j = (DianaPreset) obj;
            this.this$0.n.a();
            v26 v264 = this.this$0;
            v264.a(v264.j, this.$watchFaceWrapper);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ v26 a;

        @DexIgnore
        public c(v26 v26) {
            this.a = v26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper e = v26.e(this.a).e(str);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("CustomizeThemePresenter", " show watchface " + e);
                if (e != null) {
                    this.a.n.a(str, e.getType());
                    this.a.a(e.getType());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2", f = "CustomizeThemePresenter.kt", l = {FacebookRequestErrorClassification.EC_INVALID_TOKEN}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2$1", f = "CustomizeThemePresenter.kt", l = {FacebookRequestErrorClassification.EC_INVALID_TOKEN}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    WatchFaceRepository k = this.this$0.this$0.o;
                    String c = PortfolioApp.g0.c().c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (k.getWatchFacesFromServer(c, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(v26 v26, fb7 fb7) {
            super(2, fb7);
            this.this$0 = v26;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1", f = "CustomizeThemePresenter.kt", l = {Action.Music.MUSIC_END_ACTION}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1", f = "CustomizeThemePresenter.kt", l = {215, 223, 228}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $id;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v26$e$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$2", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.v26$e$a$a  reason: collision with other inner class name */
            public static final class C0204a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0204a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0204a aVar = new C0204a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0204a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.this$0.n.b(this.this$0.this$0.$watchFaceWrapper);
                        this.this$0.this$0.this$0.n.a();
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$3", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.this$0.n.a();
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$id = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$id, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0154 A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r17) {
                /*
                    r16 = this;
                    r1 = r16
                    java.lang.Object r2 = com.fossil.nb7.a()
                    int r0 = r1.label
                    r3 = 0
                    r4 = 3
                    r5 = 2
                    r6 = 1
                    if (r0 == 0) goto L_0x0063
                    if (r0 == r6) goto L_0x0046
                    if (r0 == r5) goto L_0x0029
                    if (r0 != r4) goto L_0x0021
                    java.lang.Object r0 = r1.L$1
                    java.lang.Exception r0 = (java.lang.Exception) r0
                    java.lang.Object r2 = r1.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r17)
                    goto L_0x0171
                L_0x0021:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r2)
                    throw r0
                L_0x0029:
                    java.lang.Object r0 = r1.L$4
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r1.L$3
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r1.L$2
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r1.L$1
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r1.L$0
                    r5 = r0
                    com.fossil.yi7 r5 = (com.fossil.yi7) r5
                    com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x0043 }
                    goto L_0x0183
                L_0x0043:
                    r0 = move-exception
                    goto L_0x0157
                L_0x0046:
                    java.lang.Object r0 = r1.L$5
                    com.fossil.oe7 r0 = (com.fossil.oe7) r0
                    java.lang.Object r0 = r1.L$4
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r1.L$3
                    java.lang.String r0 = (java.lang.String) r0
                    java.lang.Object r0 = r1.L$2
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r6 = r1.L$1
                    java.lang.String r6 = (java.lang.String) r6
                    java.lang.Object r7 = r1.L$0
                    com.fossil.yi7 r7 = (com.fossil.yi7) r7
                    com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x0155 }
                    goto L_0x00f3
                L_0x0063:
                    com.fossil.t87.a(r17)
                    com.fossil.yi7 r7 = r1.p$
                    com.fossil.v26$e r0 = r1.this$0
                    com.fossil.v26 r0 = r0.this$0
                    java.lang.String r8 = r1.$id
                    r0.i = r8
                    com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r0 = r0.c()
                    java.lang.String r0 = r0.c()
                    com.fossil.v26$e r8 = r1.this$0
                    com.fossil.v26 r8 = r8.this$0
                    com.portfolio.platform.data.source.WatchFaceRepository r8 = r8.o
                    com.fossil.qb5 r9 = com.fossil.qb5.BACKGROUND
                    java.util.List r8 = r8.getWatchFacesWithType(r0, r9)
                    boolean r9 = r8.isEmpty()
                    r9 = r9 ^ r6
                    if (r9 == 0) goto L_0x00f5
                    java.lang.Object r9 = com.fossil.ea7.d(r8)
                    com.portfolio.platform.data.model.diana.preset.WatchFace r9 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r9
                    java.lang.String r9 = r9.getId()
                    com.fossil.v26$e r10 = r1.this$0
                    com.fossil.v26 r10 = r10.this$0
                    com.portfolio.platform.data.source.DianaPresetRepository r10 = r10.p
                    java.util.List r10 = r10.getAllPresets(r0)
                    com.fossil.oe7 r11 = new com.fossil.oe7
                    r11.<init>()
                    r12 = 0
                    r11.element = r12
                    java.util.Iterator r12 = r10.iterator()
                L_0x00b2:
                    boolean r13 = r12.hasNext()
                    if (r13 == 0) goto L_0x00d0
                    java.lang.Object r13 = r12.next()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r13 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r13
                    java.lang.String r14 = r13.getWatchFaceId()
                    java.lang.String r15 = r1.$id
                    boolean r14 = com.fossil.ee7.a(r14, r15)
                    if (r14 == 0) goto L_0x00b2
                    r11.element = r6
                    r13.setWatchFaceId(r9)
                    goto L_0x00b2
                L_0x00d0:
                    boolean r12 = r11.element
                    if (r12 == 0) goto L_0x00f5
                    com.fossil.v26$e r12 = r1.this$0
                    com.fossil.v26 r12 = r12.this$0
                    com.portfolio.platform.data.source.DianaPresetRepository r12 = r12.p
                    r1.L$0 = r7
                    r1.L$1 = r0
                    r1.L$2 = r8
                    r1.L$3 = r9
                    r1.L$4 = r10
                    r1.L$5 = r11
                    r1.label = r6
                    java.lang.Object r6 = r12.upsertPresetList(r10, r1)
                    if (r6 != r2) goto L_0x00f1
                    return r2
                L_0x00f1:
                    r6 = r0
                    r0 = r8
                L_0x00f3:
                    r8 = r0
                    r0 = r6
                L_0x00f5:
                    com.fossil.v26$e r6 = r1.this$0
                    com.fossil.v26 r6 = r6.this$0
                    com.portfolio.platform.data.source.WatchFaceRepository r6 = r6.o
                    java.lang.String r9 = r1.$id
                    com.fossil.qb5 r10 = com.fossil.qb5.PHOTO
                    r6.deleteWatchFace(r9, r10)
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder
                    r6.<init>()
                    java.lang.String r9 = r1.$id
                    r6.append(r9)
                    java.lang.String r9 = "_img"
                    r6.append(r9)
                    java.lang.String r6 = r6.toString()
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    r9.<init>()
                    java.lang.String r10 = r1.$id
                    r9.append(r10)
                    java.lang.String r10 = "_bin"
                    r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    com.fossil.v26$e r10 = r1.this$0
                    com.fossil.v26 r10 = r10.this$0
                    com.portfolio.platform.data.source.WatchFaceRepository r10 = r10.o
                    r10.deleteBackgroundPhoto(r6, r9)
                    com.fossil.v26$e r10 = r1.this$0
                    com.fossil.v26 r10 = r10.this$0
                    com.fossil.tk7 r10 = r10.d()
                    com.fossil.v26$e$a$a r11 = new com.fossil.v26$e$a$a
                    r11.<init>(r1, r3)
                    r1.L$0 = r7
                    r1.L$1 = r0
                    r1.L$2 = r8
                    r1.L$3 = r6
                    r1.L$4 = r9
                    r1.label = r5
                    java.lang.Object r0 = com.fossil.vh7.a(r10, r11, r1)
                    if (r0 != r2) goto L_0x0183
                    return r2
                L_0x0155:
                    r0 = move-exception
                    r5 = r7
                L_0x0157:
                    com.fossil.v26$e r6 = r1.this$0
                    com.fossil.v26 r6 = r6.this$0
                    com.fossil.tk7 r6 = r6.d()
                    com.fossil.v26$e$a$b r7 = new com.fossil.v26$e$a$b
                    r7.<init>(r1, r3)
                    r1.L$0 = r5
                    r1.L$1 = r0
                    r1.label = r4
                    java.lang.Object r3 = com.fossil.vh7.a(r6, r7, r1)
                    if (r3 != r2) goto L_0x0171
                    return r2
                L_0x0171:
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.String r3 = r0.getMessage()
                    java.lang.String r4 = "CustomizeThemePresenter"
                    r2.e(r4, r3)
                    r0.printStackTrace()
                L_0x0183:
                    com.fossil.i97 r0 = com.fossil.i97.a
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.v26.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(v26 v26, WatchFaceWrapper watchFaceWrapper, fb7 fb7) {
            super(2, fb7);
            this.this$0 = v26;
            this.$watchFaceWrapper = watchFaceWrapper;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$watchFaceWrapper, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.n.b();
                String id = this.$watchFaceWrapper.getId();
                ti7 d = this.this$0.c();
                a aVar = new a(this, id, null);
                this.L$0 = yi7;
                this.L$1 = id;
                this.label = 1;
                if (vh7.a(d, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<List<? extends WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ v26 a;

        @DexIgnore
        public f(v26 v26) {
            this.a = v26;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchFace> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "loadWatchFace " + list);
            v26 v26 = this.a;
            ee7.a((Object) list, "it");
            v26.b(list);
            this.a.n.v(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1", f = "CustomizeThemePresenter.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $watchFaceList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$backgroundWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends WatchFaceWrapper>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $backgroundList;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$backgroundList = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$backgroundList, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends WatchFaceWrapper>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List list = this.$backgroundList;
                    DianaPreset a = v26.e(this.this$0.this$0).a().a();
                    return yc5.a(list, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$photoWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends WatchFaceWrapper>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $photoList;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(g gVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$photoList = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$photoList, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends WatchFaceWrapper>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List list = this.$photoList;
                    DianaPreset a = v26.e(this.this$0.this$0).a().a();
                    return yc5.a(list, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(v26 v26, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = v26;
            this.$watchFaceList = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$watchFaceList, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:60:0x01bd  */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x01e3  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 2
                r3 = 0
                r4 = 1
                if (r1 == 0) goto L_0x003d
                if (r1 == r4) goto L_0x002c
                if (r1 != r2) goto L_0x0024
                java.lang.Object r0 = r12.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r12.L$2
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r1 = r12.L$1
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x018e
            L_0x0024:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x002c:
                java.lang.Object r1 = r12.L$2
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r5 = r12.L$1
                java.util.List r5 = (java.util.List) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
                goto L_0x016e
            L_0x003d:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r6 = r12.p$
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r5 = "showCurrentWatchFace cacheList "
                r1.append(r5)
                com.fossil.v26 r5 = r12.this$0
                java.util.List r5 = r5.g
                r1.append(r5)
                java.lang.String r5 = " updatelist "
                r1.append(r5)
                java.util.List r5 = r12.$watchFaceList
                r1.append(r5)
                java.lang.String r1 = r1.toString()
                java.lang.String r5 = "CustomizeThemePresenter"
                r13.d(r5, r1)
                com.fossil.v26 r13 = r12.this$0
                java.util.List r13 = r13.g
                java.util.List r1 = r12.$watchFaceList
                boolean r13 = com.fossil.ee7.a(r13, r1)
                if (r13 == 0) goto L_0x007f
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x007f:
                java.util.List r13 = r12.$watchFaceList
                java.util.ArrayList r1 = new java.util.ArrayList
                r1.<init>()
                java.util.Iterator r13 = r13.iterator()
            L_0x008a:
                boolean r7 = r13.hasNext()
                r8 = 0
                if (r7 == 0) goto L_0x00b3
                java.lang.Object r7 = r13.next()
                r9 = r7
                com.portfolio.platform.data.model.diana.preset.WatchFace r9 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r9
                int r9 = r9.getWatchFaceType()
                com.fossil.qb5 r10 = com.fossil.qb5.PHOTO
                int r10 = r10.getValue()
                if (r9 != r10) goto L_0x00a5
                r8 = 1
            L_0x00a5:
                java.lang.Boolean r8 = com.fossil.pb7.a(r8)
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x008a
                r1.add(r7)
                goto L_0x008a
            L_0x00b3:
                int r13 = r1.size()
                com.fossil.v26 r7 = r12.this$0
                int r7 = r7.h
                if (r13 >= r7) goto L_0x0105
                boolean r13 = r1.isEmpty()
                if (r13 == 0) goto L_0x00e2
                com.fossil.v26 r13 = r12.this$0
                com.portfolio.platform.data.model.diana.preset.DianaPreset r13 = r13.j
                if (r13 == 0) goto L_0x00d8
                java.lang.String r13 = r13.getWatchFaceId()
                if (r13 == 0) goto L_0x00d8
                com.fossil.v26 r0 = r12.this$0
                r0.a(r13)
            L_0x00d8:
                com.fossil.v26 r13 = r12.this$0
                java.util.List r0 = r12.$watchFaceList
                r13.g = r0
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x00e2:
                com.fossil.v26 r13 = r12.this$0
                java.lang.String r13 = r13.i
                com.fossil.v26 r7 = r12.this$0
                com.fossil.a06 r7 = com.fossil.v26.e(r7)
                androidx.lifecycle.MutableLiveData r7 = r7.a()
                java.lang.Object r7 = r7.a()
                com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r7
                if (r7 == 0) goto L_0x00ff
                java.lang.String r7 = r7.getWatchFaceId()
                goto L_0x0100
            L_0x00ff:
                r7 = r3
            L_0x0100:
                boolean r13 = com.fossil.ee7.a(r13, r7)
                goto L_0x0106
            L_0x0105:
                r13 = 1
            L_0x0106:
                com.fossil.v26 r7 = r12.this$0
                int r9 = r1.size()
                r7.h = r9
                if (r13 == 0) goto L_0x021e
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.String r7 = "showCurrentWatchFace() needUpdate == true"
                r13.d(r5, r7)
                java.util.List r13 = r12.$watchFaceList
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                java.util.Iterator r13 = r13.iterator()
            L_0x0127:
                boolean r7 = r13.hasNext()
                if (r7 == 0) goto L_0x0151
                java.lang.Object r7 = r13.next()
                r9 = r7
                com.portfolio.platform.data.model.diana.preset.WatchFace r9 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r9
                int r9 = r9.getWatchFaceType()
                com.fossil.qb5 r10 = com.fossil.qb5.BACKGROUND
                int r10 = r10.getValue()
                if (r9 != r10) goto L_0x0142
                r9 = 1
                goto L_0x0143
            L_0x0142:
                r9 = 0
            L_0x0143:
                java.lang.Boolean r9 = com.fossil.pb7.a(r9)
                boolean r9 = r9.booleanValue()
                if (r9 == 0) goto L_0x0127
                r5.add(r7)
                goto L_0x0127
            L_0x0151:
                com.fossil.v26 r13 = r12.this$0
                com.fossil.ti7 r13 = r13.c()
                com.fossil.v26$g$a r7 = new com.fossil.v26$g$a
                r7.<init>(r12, r5, r3)
                r12.L$0 = r6
                r12.L$1 = r1
                r12.L$2 = r5
                r12.label = r4
                java.lang.Object r13 = com.fossil.vh7.a(r13, r7, r12)
                if (r13 != r0) goto L_0x016b
                return r0
            L_0x016b:
                r11 = r5
                r5 = r1
                r1 = r11
            L_0x016e:
                java.util.List r13 = (java.util.List) r13
                com.fossil.v26 r7 = r12.this$0
                com.fossil.ti7 r7 = r7.c()
                com.fossil.v26$g$b r8 = new com.fossil.v26$g$b
                r8.<init>(r12, r5, r3)
                r12.L$0 = r6
                r12.L$1 = r5
                r12.L$2 = r1
                r12.L$3 = r13
                r12.label = r2
                java.lang.Object r1 = com.fossil.vh7.a(r7, r8, r12)
                if (r1 != r0) goto L_0x018c
                return r0
            L_0x018c:
                r0 = r13
                r13 = r1
            L_0x018e:
                java.util.List r13 = (java.util.List) r13
                com.fossil.v26 r1 = r12.this$0
                r1.a(r0)
                com.fossil.v26 r1 = r12.this$0
                com.fossil.r26 r1 = r1.n
                com.fossil.qb5 r2 = com.fossil.qb5.BACKGROUND
                r1.a(r0, r2)
                com.fossil.v26 r1 = r12.this$0
                com.fossil.r26 r1 = r1.n
                com.fossil.qb5 r2 = com.fossil.qb5.PHOTO
                r1.a(r13, r2)
                com.fossil.v26 r1 = r12.this$0
                java.util.List r1 = r1.g
                int r1 = r1.size()
                java.util.List r2 = r12.$watchFaceList
                int r2 = r2.size()
                if (r1 <= r2) goto L_0x01e3
                boolean r1 = r13.isEmpty()
                r1 = r1 ^ r4
                if (r1 == 0) goto L_0x01d0
                com.fossil.v26 r0 = r12.this$0
                java.lang.Object r13 = com.fossil.ea7.f(r13)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r13 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r13
                r0.c(r13)
                goto L_0x0217
            L_0x01d0:
                boolean r13 = r0.isEmpty()
                r13 = r13 ^ r4
                if (r13 == 0) goto L_0x0217
                com.fossil.v26 r13 = r12.this$0
                java.lang.Object r0 = com.fossil.ea7.f(r0)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r0 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r0
                r13.c(r0)
                goto L_0x0217
            L_0x01e3:
                com.fossil.v26 r13 = r12.this$0
                com.fossil.a06 r13 = com.fossil.v26.e(r13)
                androidx.lifecycle.MutableLiveData r13 = r13.g()
                java.lang.Object r13 = r13.a()
                java.lang.String r13 = (java.lang.String) r13
                if (r13 == 0) goto L_0x0217
                com.fossil.v26 r0 = r12.this$0
                com.fossil.a06 r0 = com.fossil.v26.e(r0)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r0 = r0.e(r13)
                if (r0 == 0) goto L_0x0217
                com.fossil.v26 r1 = r12.this$0
                com.fossil.r26 r1 = r1.n
                com.fossil.qb5 r2 = r0.getType()
                r1.a(r13, r2)
                com.fossil.v26 r13 = r12.this$0
                com.fossil.qb5 r0 = r0.getType()
                r13.a(r0)
            L_0x0217:
                com.fossil.v26 r13 = r12.this$0
                java.util.List r0 = r12.$watchFaceList
                r13.g = r0
            L_0x021e:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.v26.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFace $activeWatchFace;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, WatchFace watchFace, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$activeWatchFace = watchFace;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$activeWatchFace, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    WatchFace watchFace = this.$activeWatchFace;
                    if (watchFace == null || watchFace.getWatchFaceType() != qb5.PHOTO.getValue()) {
                        this.this$0.this$0.n.G0();
                    } else {
                        this.this$0.this$0.n.O0();
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(DianaPreset dianaPreset, fb7 fb7, v26 v26) {
            super(2, fb7);
            this.$currentPreset = dianaPreset;
            this.this$0 = v26;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.$currentPreset, fb7, this.this$0);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                v26 v26 = this.this$0;
                v26.j = v26.p.getActivePresetBySerial(PortfolioApp.g0.c().c());
                String watchFaceId = this.$currentPreset.getWatchFaceId();
                WatchFace watchFaceWithId = this.this$0.o.getWatchFaceWithId(watchFaceId);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("CustomizeThemePresenter", "currentPreset observer activeWatchFace " + watchFaceWithId);
                tk7 c = qj7.c();
                a aVar = new a(this, watchFaceWithId, null);
                this.L$0 = yi7;
                this.L$1 = watchFaceId;
                this.L$2 = watchFaceWithId;
                this.label = 1;
                if (vh7.a(c, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                WatchFace watchFace = (WatchFace) this.L$2;
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1", f = "CustomizeThemePresenter.kt", l = {108, 110}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchFaceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super WatchFaceWrapper>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFace $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(WatchFace watchFace, fb7 fb7, i iVar) {
                super(2, fb7);
                this.$it = watchFace;
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super WatchFaceWrapper> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    WatchFace watchFace = this.$it;
                    DianaPreset a = v26.e(this.this$0.this$0).a().a();
                    return yc5.b(watchFace, a != null ? a.getComplications() : null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1$watchFace$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super WatchFace>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super WatchFace> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.o.getWatchFaceWithId(this.this$0.$watchFaceId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(v26 v26, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = v26;
            this.$watchFaceId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, this.$watchFaceId, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r4) goto L_0x0027
                if (r1 != r3) goto L_0x001f
                java.lang.Object r0 = r6.L$2
                com.portfolio.platform.data.model.diana.preset.WatchFace r0 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r0
                java.lang.Object r0 = r6.L$1
                com.portfolio.platform.data.model.diana.preset.WatchFace r0 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r0
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x007e
            L_0x001f:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0027:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0060
            L_0x002f:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r1 = r6.p$
                com.fossil.v26 r7 = r6.this$0
                java.lang.String r7 = r7.k
                java.lang.String r5 = r6.$watchFaceId
                boolean r7 = com.fossil.ee7.a(r7, r5)
                r7 = r7 ^ r4
                if (r7 == 0) goto L_0x00a2
                com.fossil.v26 r7 = r6.this$0
                java.lang.String r5 = r6.$watchFaceId
                r7.k = r5
                com.fossil.v26 r7 = r6.this$0
                com.fossil.ti7 r7 = r7.c()
                com.fossil.v26$i$b r5 = new com.fossil.v26$i$b
                r5.<init>(r6, r2)
                r6.L$0 = r1
                r6.label = r4
                java.lang.Object r7 = com.fossil.vh7.a(r7, r5, r6)
                if (r7 != r0) goto L_0x0060
                return r0
            L_0x0060:
                com.portfolio.platform.data.model.diana.preset.WatchFace r7 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r7
                if (r7 == 0) goto L_0x00a2
                com.fossil.v26 r4 = r6.this$0
                com.fossil.ti7 r4 = r4.c()
                com.fossil.v26$i$a r5 = new com.fossil.v26$i$a
                r5.<init>(r7, r2, r6)
                r6.L$0 = r1
                r6.L$1 = r7
                r6.L$2 = r7
                r6.label = r3
                java.lang.Object r7 = com.fossil.vh7.a(r4, r5, r6)
                if (r7 != r0) goto L_0x007e
                return r0
            L_0x007e:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r7 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r7
                com.fossil.v26 r0 = r6.this$0
                com.fossil.a06 r0 = com.fossil.v26.e(r0)
                java.util.concurrent.CopyOnWriteArrayList r0 = r0.j()
                boolean r0 = r0.contains(r7)
                if (r0 != 0) goto L_0x009d
                com.fossil.v26 r0 = r6.this$0
                com.fossil.a06 r0 = com.fossil.v26.e(r0)
                java.util.concurrent.CopyOnWriteArrayList r0 = r0.j()
                r0.add(r7)
            L_0x009d:
                com.fossil.v26 r0 = r6.this$0
                r0.c(r7)
            L_0x00a2:
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.v26.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public v26(r26 r26, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository) {
        ee7.b(r26, "mView");
        ee7.b(watchFaceRepository, "watchFaceRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        this.n = r26;
        this.o = watchFaceRepository;
        this.p = dianaPresetRepository;
    }

    @DexIgnore
    public static final /* synthetic */ a06 e(v26 v26) {
        a06 a06 = v26.e;
        if (a06 != null) {
            return a06;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "onStart");
        a06 a06 = this.e;
        if (a06 != null) {
            MutableLiveData<String> g2 = a06.g();
            r26 r26 = this.n;
            if (r26 != null) {
                g2.a((s26) r26, this.l);
                LiveData<List<WatchFace>> watchFacesLiveDataWithSerial = this.o.getWatchFacesLiveDataWithSerial(PortfolioApp.g0.c().c());
                this.f = watchFacesLiveDataWithSerial;
                if (watchFacesLiveDataWithSerial != null) {
                    watchFacesLiveDataWithSerial.a((LifecycleOwner) this.n, this.m);
                }
                a06 a062 = this.e;
                if (a062 != null) {
                    DianaPreset a2 = a062.a().a();
                    if (a2 != null) {
                        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new h(a2, null, this), 3, null);
                        return;
                    }
                    return;
                }
                ee7.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "stop()");
        a06 a06 = this.e;
        if (a06 != null) {
            a06.g().b(this.l);
            LiveData<List<WatchFace>> liveData = this.f;
            if (liveData != null) {
                liveData.b(this.m);
                return;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void h() {
        ArrayList arrayList;
        List<WatchFace> a2;
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "checkAddingPhoto()");
        LiveData<List<WatchFace>> liveData = this.f;
        if (liveData == null || (a2 = liveData.a()) == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            for (T t : a2) {
                if (t.getWatchFaceType() == qb5.PHOTO.getValue()) {
                    arrayList.add(t);
                }
            }
        }
        if (arrayList == null || arrayList.size() != 20) {
            this.n.M0();
        } else {
            this.n.L();
        }
    }

    @DexIgnore
    public void i() {
        this.n.a(this);
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void c(WatchFaceWrapper watchFaceWrapper) {
        ee7.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "setWatchFace " + watchFaceWrapper);
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            if (a2 != null) {
                a2.setWatchFaceId(watchFaceWrapper.getId());
                ee7.a((Object) a2, "currentPreset");
                a(a2);
                return;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(List<WatchFace> list) {
        ik7 unused = xh7.b(e(), null, null, new g(this, list, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void b(WatchFaceWrapper watchFaceWrapper) {
        ee7.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "removePhotoWatchFace, watchFaceWrapper = " + watchFaceWrapper);
        ik7 unused = xh7.b(e(), null, null, new e(this, watchFaceWrapper, null), 3, null);
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateCurrentPreset=" + dianaPreset);
        a06 a06 = this.e;
        if (a06 != null) {
            a06.a(dianaPreset);
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void a(String str) {
        ee7.b(str, "watchFaceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateSelectedPhotoWatchFaces - watchFaceId=" + str);
        ik7 unused = xh7.b(e(), null, null, new i(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void a(a06 a06) {
        ee7.b(a06, "viewModel");
        this.e = a06;
    }

    @DexIgnore
    @Override // com.fossil.q26
    public void a(WatchFaceWrapper watchFaceWrapper) {
        ee7.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "checkToRemovePhoto watchFaceWrapper = " + watchFaceWrapper);
        ik7 unused = xh7.b(e(), null, null, new b(this, watchFaceWrapper, null), 3, null);
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset, WatchFaceWrapper watchFaceWrapper) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showRemovePhotoDialog()");
        if (ee7.a((Object) (dianaPreset != null ? dianaPreset.getWatchFaceId() : null), (Object) watchFaceWrapper.getId())) {
            this.n.g0();
        } else {
            this.n.z0();
        }
    }

    @DexIgnore
    public final void a(qb5 qb5) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "navigateToActiveWatchFace()");
        String str = this.i;
        if (!(str == null || mh7.a(str))) {
            this.i = null;
            this.n.O0();
        } else if (qb5 == qb5.BACKGROUND) {
            this.n.G0();
        } else if (qb5 == qb5.PHOTO) {
            this.n.O0();
        }
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        boolean z = false;
        for (T t : list) {
            if (t.getBackground() != null || t.getCombination() == null || t.getTopComplication() == null || t.getBottomComplication() == null || t.getLeftComplication() == null || t.getRightComplication() == null) {
                z = true;
            }
        }
        if (z) {
            FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "Some watchface files are missing, retry download for those files");
            ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
        }
    }
}
