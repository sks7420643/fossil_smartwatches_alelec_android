package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r66 implements Factory<q66> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ r66 a; // = new r66();
    }

    @DexIgnore
    public static r66 a() {
        return a.a;
    }

    @DexIgnore
    public static q66 b() {
        return new q66();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public q66 get() {
        return b();
    }
}
