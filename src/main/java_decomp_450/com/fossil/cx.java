package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cx<T, Z> {
    @DexIgnore
    uy<Z> a(T t, int i, int i2, ax axVar) throws IOException;

    @DexIgnore
    boolean a(T t, ax axVar) throws IOException;
}
