package com.fossil;

import android.app.job.JobInfo;
import com.fossil.aw1;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dw1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public by1 a;
        @DexIgnore
        public Map<dt1, b> b; // = new HashMap();

        @DexIgnore
        public a a(by1 by1) {
            this.a = by1;
            return this;
        }

        @DexIgnore
        public a a(dt1 dt1, b bVar) {
            this.b.put(dt1, bVar);
            return this;
        }

        @DexIgnore
        public dw1 a() {
            if (this.a == null) {
                throw new NullPointerException("missing required property: clock");
            } else if (this.b.keySet().size() >= dt1.values().length) {
                Map<dt1, b> map = this.b;
                this.b = new HashMap();
                return dw1.a(this.a, map);
            } else {
                throw new IllegalStateException("Not all priorities have been configured");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract a a(long j);

            @DexIgnore
            public abstract a a(Set<c> set);

            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(long j);
        }

        @DexIgnore
        public static a d() {
            aw1.b bVar = new aw1.b();
            bVar.a(Collections.emptySet());
            return bVar;
        }

        @DexIgnore
        public abstract long a();

        @DexIgnore
        public abstract Set<c> b();

        @DexIgnore
        public abstract long c();
    }

    @DexIgnore
    public enum c {
        NETWORK_UNMETERED,
        DEVICE_IDLE,
        DEVICE_CHARGING
    }

    @DexIgnore
    public static dw1 a(by1 by1) {
        a c2 = c();
        dt1 dt1 = dt1.DEFAULT;
        b.a d = b.d();
        d.a(30000);
        d.b(LogBuilder.MAX_INTERVAL);
        c2.a(dt1, d.a());
        dt1 dt12 = dt1.HIGHEST;
        b.a d2 = b.d();
        d2.a(1000);
        d2.b(LogBuilder.MAX_INTERVAL);
        c2.a(dt12, d2.a());
        dt1 dt13 = dt1.VERY_LOW;
        b.a d3 = b.d();
        d3.a(LogBuilder.MAX_INTERVAL);
        d3.b(LogBuilder.MAX_INTERVAL);
        d3.a(a(c.NETWORK_UNMETERED, c.DEVICE_IDLE));
        c2.a(dt13, d3.a());
        c2.a(by1);
        return c2.a();
    }

    @DexIgnore
    public static a c() {
        return new a();
    }

    @DexIgnore
    public abstract by1 a();

    @DexIgnore
    public abstract Map<dt1, b> b();

    @DexIgnore
    public static dw1 a(by1 by1, Map<dt1, b> map) {
        return new zv1(by1, map);
    }

    @DexIgnore
    public long a(dt1 dt1, long j, int i) {
        long a2 = j - a().a();
        b bVar = b().get(dt1);
        return Math.min(Math.max(((long) Math.pow(2.0d, (double) (i - 1))) * bVar.a(), a2), bVar.c());
    }

    @DexIgnore
    public JobInfo.Builder a(JobInfo.Builder builder, dt1 dt1, long j, int i) {
        builder.setMinimumLatency(a(dt1, j, i));
        a(builder, b().get(dt1).b());
        return builder;
    }

    @DexIgnore
    public final void a(JobInfo.Builder builder, Set<c> set) {
        if (set.contains(c.NETWORK_UNMETERED)) {
            builder.setRequiredNetworkType(2);
        } else {
            builder.setRequiredNetworkType(1);
        }
        if (set.contains(c.DEVICE_CHARGING)) {
            builder.setRequiresCharging(true);
        }
        if (set.contains(c.DEVICE_IDLE)) {
            builder.setRequiresDeviceIdle(true);
        }
    }

    @DexIgnore
    public static <T> Set<T> a(T... tArr) {
        return Collections.unmodifiableSet(new HashSet(Arrays.asList(tArr)));
    }
}
