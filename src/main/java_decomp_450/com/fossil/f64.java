package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f64 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        String a(File file) throws IOException;
    }

    @DexIgnore
    public f64(Context context, a aVar) {
        this.a = context;
        this.b = aVar;
    }

    @DexIgnore
    public byte[] a(BufferedReader bufferedReader) throws IOException {
        return a(b(bufferedReader));
    }

    @DexIgnore
    public final JSONArray b(BufferedReader bufferedReader) throws IOException {
        JSONArray jSONArray = new JSONArray();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return jSONArray;
            }
            JSONObject b2 = b(readLine);
            if (b2 != null) {
                jSONArray.put(b2);
            }
        }
    }

    @DexIgnore
    public final File a(String str) {
        File file = new File(str);
        return !file.exists() ? a(file) : file;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        h64 a2 = i64.a(str);
        if (a2 != null && a(a2)) {
            try {
                try {
                    return a(this.b.a(a(a2.d)), a2);
                } catch (JSONException e) {
                    z24.a().a("Could not create a binary image json string", e);
                    return null;
                }
            } catch (IOException e2) {
                z24 a3 = z24.a();
                a3.a("Could not generate ID for file " + a2.d, e2);
            }
        }
        return null;
    }

    @DexIgnore
    public final File a(File file) {
        if (Build.VERSION.SDK_INT < 9 || !file.getAbsolutePath().startsWith("/data")) {
            return file;
        }
        try {
            return new File(this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 0).nativeLibraryDir, file.getName());
        } catch (PackageManager.NameNotFoundException e) {
            z24.a().b("Error getting ApplicationInfo", e);
            return file;
        }
    }

    @DexIgnore
    public static byte[] a(JSONArray jSONArray) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("binary_images", jSONArray);
            return jSONObject.toString().getBytes(Charset.forName("UTF-8"));
        } catch (JSONException e) {
            z24.a().d("Binary images string is null", e);
            return new byte[0];
        }
    }

    @DexIgnore
    public static JSONObject a(String str, h64 h64) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("base_address", h64.a);
        jSONObject.put("size", h64.b);
        jSONObject.put("name", h64.d);
        jSONObject.put("uuid", str);
        return jSONObject;
    }

    @DexIgnore
    public static boolean a(h64 h64) {
        return (h64.c.indexOf(120) == -1 || h64.d.indexOf(47) == -1) ? false : true;
    }
}
