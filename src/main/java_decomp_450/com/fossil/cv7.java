package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.qu7;
import com.fossil.tu7;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cv7 {
    @DexIgnore
    public static /* final */ cv7 a; // = d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static class b extends cv7 {
        @DexIgnore
        @Override // com.fossil.cv7
        public boolean a(Method method) {
            return method.isDefault();
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public List<? extends tu7.a> b() {
            return Collections.singletonList(av7.a);
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public int c() {
            return 1;
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            Constructor declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
            declaredConstructor.setAccessible(true);
            return ((MethodHandles.Lookup) declaredConstructor.newInstance(cls, -1)).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public List<? extends qu7.a> a(Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(su7.a);
            arrayList.add(new uu7(executor));
            return Collections.unmodifiableList(arrayList);
        }
    }

    @DexIgnore
    public static cv7 d() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                return new a();
            }
        } catch (ClassNotFoundException unused) {
        }
        try {
            Class.forName("java.util.Optional");
            return new b();
        } catch (ClassNotFoundException unused2) {
            return new cv7();
        }
    }

    @DexIgnore
    public static cv7 e() {
        return a;
    }

    @DexIgnore
    public List<? extends qu7.a> a(Executor executor) {
        return Collections.singletonList(new uu7(executor));
    }

    @DexIgnore
    public Executor a() {
        return null;
    }

    @DexIgnore
    public boolean a(Method method) {
        return false;
    }

    @DexIgnore
    public List<? extends tu7.a> b() {
        return Collections.emptyList();
    }

    @DexIgnore
    public int c() {
        return 0;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends cv7 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cv7$a$a")
        /* renamed from: com.fossil.cv7$a$a  reason: collision with other inner class name */
        public static class ExecutorC0031a implements Executor {
            @DexIgnore
            public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

            @DexIgnore
            public void execute(Runnable runnable) {
                this.a.post(runnable);
            }
        }

        @DexIgnore
        @Override // com.fossil.cv7
        @IgnoreJRERequirement
        public boolean a(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public List<? extends tu7.a> b() {
            if (Build.VERSION.SDK_INT >= 24) {
                return Collections.singletonList(av7.a);
            }
            return Collections.emptyList();
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public int c() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public Executor a() {
            return new ExecutorC0031a();
        }

        @DexIgnore
        @Override // com.fossil.cv7
        public List<? extends qu7.a> a(Executor executor) {
            if (executor != null) {
                uu7 uu7 = new uu7(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(uu7);
                }
                return Arrays.asList(su7.a, uu7);
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        throw new UnsupportedOperationException();
    }
}
