package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed6 extends bd6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> f;
    @DexIgnore
    public /* final */ cd6 g;
    @DexIgnore
    public /* final */ SleepSummariesRepository h;
    @DexIgnore
    public /* final */ SleepSessionsRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ pj4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1", f = "DashboardSleepPresenter.kt", l = {65, 73}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ed6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements te5.a {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // com.fossil.te5.a
            public final void a(te5.g gVar) {
                ee7.b(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardSleepPresenter", "onStatusChange status=" + gVar);
                if (gVar.a()) {
                    this.a.this$0.k().d();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ed6$b$b")
        /* renamed from: com.fossil.ed6$b$b  reason: collision with other inner class name */
        public static final class C0052b<T> implements zd<qf<SleepSummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0052b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<SleepSummary> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
                local.d("DashboardSleepPresenter", sb.toString());
                if (qfVar != null) {
                    this.a.this$0.k().a(qfVar);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1", f = "DashboardSleepPresenter.kt", l = {65}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository g = this.this$0.this$0.k;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = g.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ed6 ed6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ed6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00bb  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r11.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0036
                if (r1 == r3) goto L_0x002e
                if (r1 != r2) goto L_0x0026
                java.lang.Object r0 = r11.L$4
                com.fossil.ed6 r0 = (com.fossil.ed6) r0
                java.lang.Object r1 = r11.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r11.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r12)
                goto L_0x0095
            L_0x0026:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L_0x002e:
                java.lang.Object r1 = r11.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r12)
                goto L_0x0052
            L_0x0036:
                com.fossil.t87.a(r12)
                com.fossil.yi7 r1 = r11.p$
                com.fossil.ed6 r12 = r11.this$0
                com.fossil.ti7 r12 = r12.c()
                com.fossil.ed6$b$c r4 = new com.fossil.ed6$b$c
                r5 = 0
                r4.<init>(r11, r5)
                r11.L$0 = r1
                r11.label = r3
                java.lang.Object r12 = com.fossil.vh7.a(r12, r4, r11)
                if (r12 != r0) goto L_0x0052
                return r0
            L_0x0052:
                com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
                if (r12 == 0) goto L_0x00c3
                java.lang.String r3 = r12.getCreatedAt()
                java.util.Date r7 = com.fossil.zd5.d(r3)
                com.fossil.ed6 r3 = r11.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r4 = r3.h
                com.fossil.ed6 r5 = r11.this$0
                com.portfolio.platform.data.source.SleepSessionsRepository r5 = r5.i
                com.fossil.ed6 r6 = r11.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r6 = r6.j
                java.lang.String r8 = "createdDate"
                com.fossil.ee7.a(r7, r8)
                com.fossil.ed6 r8 = r11.this$0
                com.fossil.pj4 r8 = r8.l
                com.fossil.ed6$b$a r9 = new com.fossil.ed6$b$a
                r9.<init>(r11)
                r11.L$0 = r1
                r11.L$1 = r12
                r11.L$2 = r12
                r11.L$3 = r7
                r11.L$4 = r3
                r11.label = r2
                r10 = r11
                java.lang.Object r12 = r4.getSummariesPaging(r5, r6, r7, r8, r9, r10)
                if (r12 != r0) goto L_0x0094
                return r0
            L_0x0094:
                r0 = r3
            L_0x0095:
                com.portfolio.platform.data.Listing r12 = (com.portfolio.platform.data.Listing) r12
                r0.f = r12
                com.fossil.ed6 r12 = r11.this$0
                com.fossil.cd6 r12 = r12.k()
                com.fossil.ed6 r0 = r11.this$0
                com.portfolio.platform.data.Listing r0 = r0.f
                if (r0 == 0) goto L_0x00c3
                androidx.lifecycle.LiveData r0 = r0.getPagedList()
                if (r0 == 0) goto L_0x00c3
                if (r12 == 0) goto L_0x00bb
                com.fossil.dd6 r12 = (com.fossil.dd6) r12
                com.fossil.ed6$b$b r1 = new com.fossil.ed6$b$b
                r1.<init>(r11)
                r0.a(r12, r1)
                goto L_0x00c3
            L_0x00bb:
                com.fossil.x87 r12 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment"
                r12.<init>(r0)
                throw r12
            L_0x00c3:
                com.fossil.i97 r12 = com.fossil.i97.a
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ed6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ed6(cd6 cd6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        ee7.b(cd6, "mView");
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSleepSessionsRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(pj4, "mAppExecutors");
        this.g = cd6;
        this.h = sleepSummariesRepository;
        this.i = sleepSessionsRepository;
        this.j = fitnessDataRepository;
        this.k = userRepository;
        this.l = pj4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
    }

    @DexIgnore
    @Override // com.fossil.bd6
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.bd6
    public void i() {
        LiveData<qf<SleepSummary>> pagedList;
        try {
            cd6 cd6 = this.g;
            Listing<SleepSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (cd6 != null) {
                    pagedList.a((dd6) cd6);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.bd6
    public void j() {
        vc7<i97> retry;
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final cd6 k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        Boolean w = zd5.w(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "start isDateTodayDate " + w + " listingPage " + this.f);
        if (!w.booleanValue()) {
            this.e = new Date();
            Listing<SleepSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }
}
