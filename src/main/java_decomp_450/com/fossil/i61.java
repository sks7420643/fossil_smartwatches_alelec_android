package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i61 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i61(vv0 vv0) {
        super(1);
        this.a = vv0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        this.a.G = ((j31) v81).L;
        vv0 vv0 = this.a;
        long j = vv0.I;
        long j2 = vv0.G;
        if (j >= j2) {
            vv0.I = j2;
            this.a.m();
        } else {
            zk0.a(vv0, new j31(Math.max(0L, vv0.G - ((long) 4)), vv0.P, ((zk0) vv0).w, 0, 8), new i61(vv0), new g81(vv0), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        }
        return i97.a;
    }
}
