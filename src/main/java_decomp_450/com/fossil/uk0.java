package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk0 extends bn0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> E; // = yz0.a(((bn0) this).C, w97.a((Object[]) new ul0[]{ul0.DEVICE_CONFIG}));
    @DexIgnore
    public ab0 F;

    @DexIgnore
    public uk0(ri1 ri1, en0 en0) {
        super(ri1, en0, wm0.I, new ua1(ri1));
    }

    @DexIgnore
    @Override // com.fossil.bn0
    public void b(v81 v81) {
        this.F = ((ua1) v81).M;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        ab0 ab0 = this.F;
        return ab0 != null ? ab0 : new ab0(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.bn0
    public ArrayList<ul0> f() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.d0;
        ab0 ab0 = this.F;
        return yz0.a(k, r51, ab0 != null ? ab0.a() : null);
    }
}
