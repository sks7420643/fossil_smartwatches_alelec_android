package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ss7 extends OutputStream {
    @DexIgnore
    public static /* final */ byte[] f; // = new byte[0];
    @DexIgnore
    public /* final */ List<byte[]> a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public int e;

    @DexIgnore
    public ss7() {
        this(1024);
    }

    @DexIgnore
    public final void a(int i) {
        if (this.b < this.a.size() - 1) {
            this.c += this.d.length;
            int i2 = this.b + 1;
            this.b = i2;
            this.d = this.a.get(i2);
            return;
        }
        byte[] bArr = this.d;
        if (bArr == null) {
            this.c = 0;
        } else {
            i = Math.max(bArr.length << 1, i - this.c);
            this.c += this.d.length;
        }
        this.b++;
        byte[] bArr2 = new byte[i];
        this.d = bArr2;
        this.a.add(bArr2);
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    @DexIgnore
    public String toString() {
        return new String(a());
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i > bArr.length || i2 < 0 || (i3 = i + i2) > bArr.length || i3 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            synchronized (this) {
                int i4 = this.e + i2;
                int i5 = this.e - this.c;
                while (i2 > 0) {
                    int min = Math.min(i2, this.d.length - i5);
                    System.arraycopy(bArr, i3 - i2, this.d, i5, min);
                    i2 -= min;
                    if (i2 > 0) {
                        a(i4);
                        i5 = 0;
                    }
                }
                this.e = i4;
            }
        }
    }

    @DexIgnore
    public ss7(int i) {
        this.a = new ArrayList();
        if (i >= 0) {
            synchronized (this) {
                a(i);
            }
            return;
        }
        throw new IllegalArgumentException("Negative initial size: " + i);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public synchronized void write(int i) {
        int i2 = this.e - this.c;
        if (i2 == this.d.length) {
            a(this.e + 1);
            i2 = 0;
        }
        this.d[i2] = (byte) i;
        this.e++;
    }

    @DexIgnore
    public synchronized byte[] a() {
        int i = this.e;
        if (i == 0) {
            return f;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (byte[] bArr2 : this.a) {
            int min = Math.min(bArr2.length, i);
            System.arraycopy(bArr2, 0, bArr, i2, min);
            i2 += min;
            i -= min;
            if (i == 0) {
                break;
            }
        }
        return bArr;
    }
}
