package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn4 {
    @DexIgnore
    @te4("challengeId")
    public String a;
    @DexIgnore
    @te4("name")
    public String b;
    @DexIgnore
    @te4("challengeType")
    public String c;
    @DexIgnore
    @te4("target")
    public Integer d;
    @DexIgnore
    @te4("duration")
    public Integer e;

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final Integer c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof pn4)) {
            return false;
        }
        pn4 pn4 = (pn4) obj;
        return ee7.a(this.a, pn4.a) && ee7.a(this.b, pn4.b) && ee7.a(this.c, pn4.c) && ee7.a(this.d, pn4.d) && ee7.a(this.e, pn4.e);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Integer num = this.d;
        int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.e;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeData(id=" + this.a + ", name=" + this.b + ", type=" + this.c + ", target=" + this.d + ", duration=" + this.e + ")";
    }
}
