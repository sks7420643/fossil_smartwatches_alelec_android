package com.fossil;

import android.os.Parcel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s31 extends f01 {
    @DexIgnore
    public static /* final */ z11 CREATOR; // = new z11(null);

    @DexIgnore
    public s31(rg0 rg0, r60 r60) {
        super(rg0, r60);
    }

    @DexIgnore
    @Override // com.fossil.f01
    public List<pe1> b() {
        return v97.a(new cv0());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public /* synthetic */ s31(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
