package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum gm4 {
    BUDDY_CHALLENGE("buddy_challenge"),
    COMMUTE_TIME("commute_time");
    
    @DexIgnore
    public /* final */ String strType;

    @DexIgnore
    public gm4(String str) {
        this.strType = str;
    }

    @DexIgnore
    public final String getStrType() {
        return this.strType;
    }
}
