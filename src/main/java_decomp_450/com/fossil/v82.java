package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v82 extends eg2 implements u82 {
    @DexIgnore
    public v82(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    @Override // com.fossil.u82
    public final ab2 zzb() throws RemoteException {
        Parcel a = a(1, zza());
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.u82
    public final int zzc() throws RemoteException {
        Parcel a = a(2, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
