package com.fossil;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e00<Data> implements m00<File, Data> {
    @DexIgnore
    public /* final */ d<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements n00<File, Data> {
        @DexIgnore
        public /* final */ d<Data> a;

        @DexIgnore
        public a(d<Data> dVar) {
            this.a = dVar;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public final m00<File, Data> a(q00 q00) {
            return new e00(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b() {
            super(new a());
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<ParcelFileDescriptor> {
            @DexIgnore
            @Override // com.fossil.e00.d
            public Class<ParcelFileDescriptor> getDataClass() {
                return ParcelFileDescriptor.class;
            }

            @DexIgnore
            @Override // com.fossil.e00.d
            public ParcelFileDescriptor a(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, SQLiteDatabase.CREATE_IF_NECESSARY);
            }

            @DexIgnore
            public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }
        }
    }

    @DexIgnore
    public interface d<Data> {
        @DexIgnore
        Data a(File file) throws FileNotFoundException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a<InputStream> {
        @DexIgnore
        public e() {
            super(new a());
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<InputStream> {
            @DexIgnore
            @Override // com.fossil.e00.d
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            @Override // com.fossil.e00.d
            public InputStream a(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public e00(d<Data> dVar) {
        this.a = dVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public m00.a<Data> a(File file, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(file), new c(file, this.a));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Data> implements ix<Data> {
        @DexIgnore
        public /* final */ File a;
        @DexIgnore
        public /* final */ d<Data> b;
        @DexIgnore
        public Data c;

        @DexIgnore
        public c(File file, d<Data> dVar) {
            this.a = file;
            this.b = dVar;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super Data> aVar) {
            try {
                Data a2 = this.b.a(this.a);
                this.c = a2;
                aVar.a((Object) a2);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
            Data data = this.c;
            if (data != null) {
                try {
                    this.b.a((Object) data);
                } catch (IOException unused) {
                }
            }
        }
    }
}
