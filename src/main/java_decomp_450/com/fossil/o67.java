package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o67 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ a47 b;
    @DexIgnore
    public /* final */ /* synthetic */ c47 c;

    @DexIgnore
    public o67(Context context, a47 a47, c47 c47) {
        this.a = context;
        this.b = a47;
        this.c = c47;
    }

    @DexIgnore
    public final void run() {
        try {
            b47 b47 = new b47(this.a, z37.a(this.a, false, this.b), this.c.a, this.b);
            b47.g().c = this.c.c;
            new t47(b47).a();
        } catch (Throwable th) {
            z37.m.a(th);
            z37.a(this.a, th);
        }
    }
}
