package com.fossil;

import android.app.ActivityManager;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a74 {
    @DexIgnore
    public static /* final */ x64 a; // = x64.a("0");
    @DexIgnore
    public static /* final */ x64 b; // = x64.a("Unity");

    @DexIgnore
    public static void a(z64 z64, String str, String str2, long j) throws Exception {
        z64.a(1, x64.a(str2));
        z64.a(2, x64.a(str));
        z64.a(3, j);
    }

    @DexIgnore
    public static void a(z64 z64, String str, String str2, String str3, String str4, int i, String str5) throws Exception {
        x64 a2 = x64.a(str);
        x64 a3 = x64.a(str2);
        x64 a4 = x64.a(str3);
        x64 a5 = x64.a(str4);
        x64 a6 = str5 != null ? x64.a(str5) : null;
        z64.c(7, 2);
        z64.e(a(a2, a3, a4, a5, i, a6));
        z64.a(1, a2);
        z64.a(2, a3);
        z64.a(3, a4);
        z64.a(6, a5);
        if (a6 != null) {
            z64.a(8, b);
            z64.a(9, a6);
        }
        z64.a(10, i);
    }

    @DexIgnore
    public static void a(z64 z64, String str, String str2, boolean z) throws Exception {
        x64 a2 = x64.a(str);
        x64 a3 = x64.a(str2);
        z64.c(8, 2);
        z64.e(a(a2, a3, z));
        z64.a(1, 3);
        z64.a(2, a2);
        z64.a(3, a3);
        z64.a(4, z);
    }

    @DexIgnore
    public static void a(z64 z64, int i, String str, int i2, long j, long j2, boolean z, int i3, String str2, String str3) throws Exception {
        x64 a2 = a(str);
        x64 a3 = a(str3);
        x64 a4 = a(str2);
        z64.c(9, 2);
        z64.e(a(i, a2, i2, j, j2, z, i3, a4, a3));
        z64.a(3, i);
        z64.a(4, a2);
        z64.d(5, i2);
        z64.a(6, j);
        z64.a(7, j2);
        z64.a(10, z);
        z64.d(12, i3);
        if (a4 != null) {
            z64.a(13, a4);
        }
        if (a3 != null) {
            z64.a(14, a3);
        }
    }

    @DexIgnore
    public static void a(z64 z64, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        x64 a2 = x64.a(str);
        x64 a3 = a(str2);
        x64 a4 = a(str3);
        int b2 = z64.b(1, a2) + 0;
        if (str2 != null) {
            b2 += z64.b(2, a3);
        }
        if (str3 != null) {
            b2 += z64.b(3, a4);
        }
        z64.c(6, 2);
        z64.e(b2);
        z64.a(1, a2);
        if (str2 != null) {
            z64.a(2, a3);
        }
        if (str3 != null) {
            z64.a(3, a4);
        }
    }

    @DexIgnore
    public static void a(z64 z64, long j, String str, n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, byte[] bArr, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, String str2, String str3, Float f, int i3, boolean z, long j2, long j3) throws Exception {
        x64 x64;
        x64 a2 = x64.a(str2);
        x64 x642 = null;
        if (str3 == null) {
            x64 = null;
        } else {
            x64 = x64.a(str3.replace(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, ""));
        }
        if (bArr != null) {
            x642 = x64.a(bArr);
        } else {
            z24.a().a("No log data to include with this event.");
        }
        z64.c(10, 2);
        z64.e(a(j, str, n84, thread, stackTraceElementArr, threadArr, list, i, map, runningAppProcessInfo, i2, a2, x64, f, i3, z, j2, j3, x642));
        z64.a(1, j);
        z64.a(2, x64.a(str));
        a(z64, n84, thread, stackTraceElementArr, threadArr, list, i, a2, x64, map, runningAppProcessInfo, i2);
        a(z64, f, i3, z, i2, j2, j3);
        a(z64, x642);
    }

    @DexIgnore
    public static void a(z64 z64, String str) throws Exception {
        x64 a2 = x64.a(str);
        z64.c(7, 2);
        int b2 = z64.b(2, a2);
        z64.e(z64.l(5) + z64.j(b2) + b2);
        z64.c(5, 2);
        z64.e(b2);
        z64.a(2, a2);
    }

    @DexIgnore
    public static void a(z64 z64, n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, x64 x64, x64 x642, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) throws Exception {
        z64.c(3, 2);
        z64.e(a(n84, thread, stackTraceElementArr, threadArr, list, i, x64, x642, map, runningAppProcessInfo, i2));
        a(z64, n84, thread, stackTraceElementArr, threadArr, list, i, x64, x642);
        if (map != null && !map.isEmpty()) {
            a(z64, map);
        }
        if (runningAppProcessInfo != null) {
            z64.a(3, runningAppProcessInfo.importance != 100);
        }
        z64.d(4, i2);
    }

    @DexIgnore
    public static void a(z64 z64, n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, x64 x64, x64 x642) throws Exception {
        z64.c(1, 2);
        z64.e(a(n84, thread, stackTraceElementArr, threadArr, list, i, x64, x642));
        a(z64, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            a(z64, threadArr[i2], list.get(i2), 0, false);
        }
        a(z64, n84, 1, i, 2);
        z64.c(3, 2);
        z64.e(a());
        z64.a(1, a);
        z64.a(2, a);
        z64.a(3, 0L);
        z64.c(4, 2);
        z64.e(a(x64, x642));
        z64.a(1, 0L);
        z64.a(2, 0L);
        z64.a(3, x64);
        if (x642 != null) {
            z64.a(4, x642);
        }
    }

    @DexIgnore
    public static void a(z64 z64, Map<String, String> map) throws Exception {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            z64.c(2, 2);
            z64.e(a(entry.getKey(), entry.getValue()));
            z64.a(1, x64.a(entry.getKey()));
            String value = entry.getValue();
            if (value == null) {
                value = "";
            }
            z64.a(2, x64.a(value));
        }
    }

    @DexIgnore
    public static void a(z64 z64, n84 n84, int i, int i2, int i3) throws Exception {
        z64.c(i3, 2);
        z64.e(a(n84, 1, i2));
        z64.a(1, x64.a(n84.b));
        String str = n84.a;
        if (str != null) {
            z64.a(3, x64.a(str));
        }
        int i4 = 0;
        for (StackTraceElement stackTraceElement : n84.c) {
            a(z64, 4, stackTraceElement, true);
        }
        n84 n842 = n84.d;
        if (n842 == null) {
            return;
        }
        if (i < i2) {
            a(z64, n842, i + 1, i2, 6);
            return;
        }
        while (n842 != null) {
            n842 = n842.d;
            i4++;
        }
        z64.d(7, i4);
    }

    @DexIgnore
    public static void a(z64 z64, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) throws Exception {
        z64.c(1, 2);
        z64.e(a(thread, stackTraceElementArr, i, z));
        z64.a(1, x64.a(thread.getName()));
        z64.d(2, i);
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            a(z64, 3, stackTraceElement, z);
        }
    }

    @DexIgnore
    public static void a(z64 z64, int i, StackTraceElement stackTraceElement, boolean z) throws Exception {
        z64.c(i, 2);
        z64.e(a(stackTraceElement, z));
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            z64.a(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            z64.a(1, 0L);
        }
        z64.a(2, x64.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            z64.a(3, x64.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            z64.a(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i2 = 4;
        }
        z64.d(5, i2);
    }

    @DexIgnore
    public static void a(z64 z64, Float f, int i, boolean z, int i2, long j, long j2) throws Exception {
        z64.c(5, 2);
        z64.e(a(f, i, z, i2, j, j2));
        if (f != null) {
            z64.a(1, f.floatValue());
        }
        z64.b(2, i);
        z64.a(3, z);
        z64.d(4, i2);
        z64.a(5, j);
        z64.a(6, j2);
    }

    @DexIgnore
    public static void a(z64 z64, x64 x64) throws Exception {
        if (x64 != null) {
            z64.c(6, 2);
            z64.e(a(x64));
            z64.a(1, x64);
        }
    }

    @DexIgnore
    public static int a(x64 x64, x64 x642, x64 x643, x64 x644, int i, x64 x645) {
        int b2 = z64.b(1, x64) + 0 + z64.b(2, x642) + z64.b(3, x643) + z64.b(6, x644);
        if (x645 != null) {
            b2 = b2 + z64.b(8, b) + z64.b(9, x645);
        }
        return b2 + z64.e(10, i);
    }

    @DexIgnore
    public static int a(x64 x64, x64 x642, boolean z) {
        return z64.e(1, 3) + 0 + z64.b(2, x64) + z64.b(3, x642) + z64.b(4, z);
    }

    @DexIgnore
    public static int a(int i, x64 x64, int i2, long j, long j2, boolean z, int i3, x64 x642, x64 x643) {
        int i4;
        int i5;
        int i6 = 0;
        int e = z64.e(3, i) + 0;
        if (x64 == null) {
            i4 = 0;
        } else {
            i4 = z64.b(4, x64);
        }
        int g = e + i4 + z64.g(5, i2) + z64.b(6, j) + z64.b(7, j2) + z64.b(10, z) + z64.g(12, i3);
        if (x642 == null) {
            i5 = 0;
        } else {
            i5 = z64.b(13, x642);
        }
        int i7 = g + i5;
        if (x643 != null) {
            i6 = z64.b(14, x643);
        }
        return i7 + i6;
    }

    @DexIgnore
    public static int a(x64 x64, x64 x642) {
        int b2 = z64.b(1, 0L) + 0 + z64.b(2, 0L) + z64.b(3, x64);
        return x642 != null ? b2 + z64.b(4, x642) : b2;
    }

    @DexIgnore
    public static int a(long j, String str, n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, x64 x64, x64 x642, Float f, int i3, boolean z, long j2, long j3, x64 x643) {
        int b2 = z64.b(1, j) + 0 + z64.b(2, x64.a(str));
        int a2 = a(n84, thread, stackTraceElementArr, threadArr, list, i, x64, x642, map, runningAppProcessInfo, i2);
        int a3 = a(f, i3, z, i2, j2, j3);
        int l = b2 + z64.l(3) + z64.j(a2) + a2 + z64.l(5) + z64.j(a3) + a3;
        if (x643 == null) {
            return l;
        }
        int a4 = a(x643);
        return l + z64.l(6) + z64.j(a4) + a4;
    }

    @DexIgnore
    public static int a(n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, x64 x64, x64 x642, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int a2 = a(n84, thread, stackTraceElementArr, threadArr, list, i, x64, x642);
        boolean z = true;
        int l = z64.l(1) + z64.j(a2) + a2 + 0;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                int a3 = a(entry.getKey(), entry.getValue());
                l += z64.l(2) + z64.j(a3) + a3;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance == 100) {
                z = false;
            }
            l += z64.b(3, z);
        }
        return l + z64.g(4, i2);
    }

    @DexIgnore
    public static int a(n84 n84, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, x64 x64, x64 x642) {
        int a2 = a(thread, stackTraceElementArr, 4, true);
        int l = z64.l(1) + z64.j(a2) + a2 + 0;
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            int a3 = a(threadArr[i2], list.get(i2), 0, false);
            l += z64.l(1) + z64.j(a3) + a3;
        }
        int a4 = a(n84, 1, i);
        int a5 = a();
        int a6 = a(x64, x642);
        return l + z64.l(2) + z64.j(a4) + a4 + z64.l(3) + z64.j(a5) + a5 + z64.l(3) + z64.j(a6) + a6;
    }

    @DexIgnore
    public static int a(String str, String str2) {
        int b2 = z64.b(1, x64.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b2 + z64.b(2, x64.a(str2));
    }

    @DexIgnore
    public static int a(Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = 0 + z64.b(1, f.floatValue());
        }
        return i3 + z64.f(2, i) + z64.b(3, z) + z64.g(4, i2) + z64.b(5, j) + z64.b(6, j2);
    }

    @DexIgnore
    public static int a(x64 x64) {
        return z64.b(1, x64);
    }

    @DexIgnore
    public static int a(n84 n84, int i, int i2) {
        int i3 = 0;
        int b2 = z64.b(1, x64.a(n84.b)) + 0;
        String str = n84.a;
        if (str != null) {
            b2 += z64.b(3, x64.a(str));
        }
        for (StackTraceElement stackTraceElement : n84.c) {
            int a2 = a(stackTraceElement, true);
            b2 += z64.l(4) + z64.j(a2) + a2;
        }
        n84 n842 = n84.d;
        if (n842 == null) {
            return b2;
        }
        if (i < i2) {
            int a3 = a(n842, i + 1, i2);
            return b2 + z64.l(6) + z64.j(a3) + a3;
        }
        while (n842 != null) {
            n842 = n842.d;
            i3++;
        }
        return b2 + z64.g(7, i3);
    }

    @DexIgnore
    public static int a() {
        return z64.b(1, a) + 0 + z64.b(2, a) + z64.b(3, 0L);
    }

    @DexIgnore
    public static int a(StackTraceElement stackTraceElement, boolean z) {
        int i;
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i = z64.b(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i = z64.b(1, 0L);
        }
        int b2 = i + 0 + z64.b(2, x64.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b2 += z64.b(3, x64.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b2 += z64.b(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i2 = 2;
        }
        return b2 + z64.g(5, i2);
    }

    @DexIgnore
    public static int a(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int b2 = z64.b(1, x64.a(thread.getName())) + z64.g(2, i);
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            int a2 = a(stackTraceElement, z);
            b2 += z64.l(3) + z64.j(a2) + a2;
        }
        return b2;
    }

    @DexIgnore
    public static x64 a(String str) {
        if (str == null) {
            return null;
        }
        return x64.a(str);
    }
}
