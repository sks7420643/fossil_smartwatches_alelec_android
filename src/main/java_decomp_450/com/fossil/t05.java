package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t05 extends s05 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        U.put(2131363229, 2);
        U.put(2131363310, 3);
        U.put(2131362499, 4);
        U.put(2131362047, 5);
        U.put(2131362758, 6);
        U.put(2131362759, 7);
        U.put(2131362757, 8);
        U.put(2131362640, 9);
        U.put(2131362021, 10);
        U.put(2131363463, 11);
        U.put(2131363455, 12);
        U.put(2131363462, 13);
        U.put(2131363459, 14);
        U.put(2131362543, 15);
        U.put(2131362658, 16);
        U.put(2131362548, 17);
        U.put(2131362118, 18);
        U.put(2131363454, 19);
        U.put(2131363355, 20);
        U.put(2131363453, 21);
        U.put(2131363354, 22);
        U.put(2131363452, 23);
        U.put(2131363353, 24);
        U.put(2131363379, 25);
        U.put(2131362167, 26);
        U.put(2131362998, 27);
    }
    */

    @DexIgnore
    public t05(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 28, T, U));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.S != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.S = 1;
        }
        g();
    }

    @DexIgnore
    public t05(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (View) objArr[10], (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[18], (CardView) objArr[26], (FlexibleTextView) objArr[4], (View) objArr[15], (View) objArr[17], (ImageView) objArr[9], (ImageView) objArr[16], (View) objArr[8], (View) objArr[6], (View) objArr[7], (ConstraintLayout) objArr[0], (ViewPager2) objArr[27], (RTLImageView) objArr[2], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[20], (View) objArr[25], (CustomizeWidget) objArr[23], (CustomizeWidget) objArr[21], (CustomizeWidget) objArr[19], (CustomizeWidget) objArr[12], (CustomizeWidget) objArr[14], (CustomizeWidget) objArr[13], (CustomizeWidget) objArr[11]);
        this.S = -1;
        ((s05) this).D.setTag(null);
        a(view);
        f();
    }
}
