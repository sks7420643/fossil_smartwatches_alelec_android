package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n70 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<n70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public n70 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    return new n70(readString, readInt, readString2);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public n70[] newArray(int i) {
            return new n70[i];
        }
    }

    @DexIgnore
    public n70(String str, int i, String str2) throws IllegalArgumentException {
        this.a = str;
        this.b = i;
        this.c = str2;
        if (!(str2.length() <= 10)) {
            StringBuilder b2 = yh0.b("traffic(");
            b2.append(this.c);
            b2.append(") length must be less than or equal to 10");
            throw new IllegalArgumentException(b2.toString().toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.r, this.a), r51.x4, Integer.valueOf(this.b)), r51.z4, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.b;
    }

    @DexIgnore
    public final String getDestination() {
        return this.a;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
    }
}
