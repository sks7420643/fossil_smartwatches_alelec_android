package com.fossil;

import com.fossil.bw2;
import com.fossil.bw2.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bw2<MessageType extends bw2<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends ju2<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, bw2<?, ?>> zzd; // = new ConcurrentHashMap();
    @DexIgnore
    public ty2 zzb; // = ty2.d();
    @DexIgnore
    public int zzc; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<MessageType extends b<MessageType, BuilderType>, BuilderType> extends bw2<MessageType, BuilderType> implements lx2 {
        @DexIgnore
        public qv2<e> zzc; // = qv2.g();

        @DexIgnore
        public final qv2<e> zza() {
            if (this.zzc.b()) {
                this.zzc = (qv2) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T extends bw2<T, ?>> extends ku2<T> {
        @DexIgnore
        public c(T t) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<ContainingType extends jx2, Type> extends lv2<ContainingType, Type> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements sv2<e> {
        @DexIgnore
        @Override // com.fossil.sv2
        public final mx2 a(mx2 mx2, jx2 jx2) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final int zza() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final iz2 zzb() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final pz2 zzc() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final boolean zzd() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final boolean zze() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.sv2
        public final sx2 a(sx2 sx2, sx2 sx22) {
            throw new NoSuchMethodError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class f {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {1, 2, 3, 4, 5, 6, 7};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore
    public static hw2 m() {
        return cw2.zzd();
    }

    @DexIgnore
    public static gw2 n() {
        return ww2.zzd();
    }

    @DexIgnore
    public static <E> jw2<E> o() {
        return xx2.zzd();
    }

    @DexIgnore
    public abstract Object a(int i, Object obj, Object obj2);

    @DexIgnore
    @Override // com.fossil.ju2
    public final void a(int i) {
        this.zzc = i;
    }

    @DexIgnore
    @Override // com.fossil.ju2
    public final int c() {
        return this.zzc;
    }

    @DexIgnore
    @Override // com.fossil.lx2
    public final /* synthetic */ jx2 d() {
        return (bw2) a(f.f, (Object) null, (Object) null);
    }

    @DexIgnore
    public final <MessageType extends bw2<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> BuilderType e() {
        return (BuilderType) ((a) a(f.e, (Object) null, (Object) null));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return yx2.a().a(this).zza(this, (bw2) obj);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.jx2
    public final /* synthetic */ mx2 f() {
        a aVar = (a) a(f.e, (Object) null, (Object) null);
        aVar.a((bw2) this);
        return aVar;
    }

    @DexIgnore
    public int hashCode() {
        int i = ((ju2) this).zza;
        if (i != 0) {
            return i;
        }
        int zza = yx2.a().a(this).zza(this);
        ((ju2) this).zza = zza;
        return zza;
    }

    @DexIgnore
    @Override // com.fossil.jx2
    public final /* synthetic */ mx2 i() {
        return (a) a(f.e, (Object) null, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.lx2
    public final boolean j() {
        return a(this, Boolean.TRUE.booleanValue());
    }

    @DexIgnore
    @Override // com.fossil.jx2
    public final int k() {
        if (this.zzc == -1) {
            this.zzc = yx2.a().a(this).zzb(this);
        }
        return this.zzc;
    }

    @DexIgnore
    public final BuilderType l() {
        BuilderType buildertype = (BuilderType) ((a) a(f.e, (Object) null, (Object) null));
        buildertype.a(this);
        return buildertype;
    }

    @DexIgnore
    public String toString() {
        return ox2.a(this, super.toString());
    }

    @DexIgnore
    @Override // com.fossil.jx2
    public final void a(iv2 iv2) throws IOException {
        yx2.a().a(this).a(this, kv2.a(iv2));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<MessageType extends bw2<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends iu2<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType a;
        @DexIgnore
        public MessageType b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public a(MessageType messagetype) {
            this.a = messagetype;
            this.b = (MessageType) ((bw2) messagetype.a(f.d, null, null));
        }

        @DexIgnore
        public final BuilderType a(MessageType messagetype) {
            if (this.c) {
                l();
                this.c = false;
            }
            a(this.b, messagetype);
            return this;
        }

        @DexIgnore
        public final BuilderType b(byte[] bArr, int i, int i2, nv2 nv2) throws iw2 {
            if (this.c) {
                l();
                this.c = false;
            }
            try {
                yx2.a().a(this.b).a(this.b, bArr, 0, i2 + 0, new ou2(nv2));
                return this;
            } catch (iw2 e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw iw2.zza();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.bw2$a */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) this.a.a(f.e, null, null);
            aVar.a((bw2) b());
            return aVar;
        }

        @DexIgnore
        @Override // com.fossil.lx2
        public final /* synthetic */ jx2 d() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.lx2
        public final boolean j() {
            return bw2.a((bw2) this.b, false);
        }

        @DexIgnore
        public void l() {
            MessageType messagetype = (MessageType) ((bw2) this.b.a(f.d, null, null));
            a(messagetype, this.b);
            this.b = messagetype;
        }

        @DexIgnore
        /* renamed from: m */
        public MessageType b() {
            if (this.c) {
                return this.b;
            }
            MessageType messagetype = this.b;
            yx2.a().a(messagetype).zzc(messagetype);
            this.c = true;
            return this.b;
        }

        @DexIgnore
        /* renamed from: n */
        public final MessageType g() {
            MessageType messagetype = (MessageType) ((bw2) b());
            if (messagetype.j()) {
                return messagetype;
            }
            throw new ry2(messagetype);
        }

        @DexIgnore
        public static void a(MessageType messagetype, MessageType messagetype2) {
            yx2.a().a(messagetype).zzb(messagetype, messagetype2);
        }

        @DexIgnore
        @Override // com.fossil.iu2
        public final /* synthetic */ iu2 a(byte[] bArr, int i, int i2, nv2 nv2) throws iw2 {
            b(bArr, 0, i2, nv2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.iu2
        public final /* synthetic */ iu2 a(byte[] bArr, int i, int i2) throws iw2 {
            b(bArr, 0, i2, nv2.a());
            return this;
        }
    }

    @DexIgnore
    public static <T extends bw2<?, ?>> T a(Class<T> cls) {
        T t;
        T t2;
        bw2<?, ?> bw2 = zzd.get(cls);
        if (bw2 == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                bw2 = (T) zzd.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t == null) {
            t = (T) ((bw2) ((bw2) bz2.a(cls)).a(f.f, (Object) null, (Object) null));
            if (t != null) {
                zzd.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t2;
    }

    @DexIgnore
    public static <T extends bw2<?, ?>> void a(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    @DexIgnore
    public static Object a(jx2 jx2, String str, Object[] objArr) {
        return new ay2(jx2, str, objArr);
    }

    @DexIgnore
    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static final <T extends bw2<T, ?>> boolean a(T t, boolean z) {
        byte byteValue = ((Byte) t.a(f.a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzd2 = yx2.a().a(t).zzd(t);
        if (z) {
            t.a(f.b, zzd2 ? t : null, null);
        }
        return zzd2;
    }

    @DexIgnore
    public static gw2 a(gw2 gw2) {
        int size = gw2.size();
        return gw2.zzc(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static <E> jw2<E> a(jw2<E> jw2) {
        int size = jw2.size();
        return jw2.zza(size == 0 ? 10 : size << 1);
    }
}
