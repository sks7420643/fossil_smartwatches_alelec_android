package com.fossil;

import com.fossil.fitness.WorkoutMode;
import com.fossil.fitness.WorkoutType;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionItem;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay6 {
    @DexIgnore
    public static final WorkoutDetectionSetting a(List<WorkoutSetting> list) {
        ee7.b(list, "$this$toWorkoutDetectionSetting");
        ArrayList arrayList = new ArrayList();
        for (WorkoutSetting workoutSetting : list) {
            WorkoutType a = ac5.Companion.a(workoutSetting.getType());
            WorkoutMode a2 = ub5.Companion.a(workoutSetting.getMode());
            WorkoutDetectionItem workoutDetectionItem = new WorkoutDetectionItem();
            workoutDetectionItem.setType(a);
            workoutDetectionItem.setMode(a2);
            workoutDetectionItem.setEnable(workoutSetting.getEnable());
            workoutDetectionItem.setUserConfirmation(workoutSetting.getAskMeFirst());
            workoutDetectionItem.setStartLatency(workoutSetting.getStartLatency());
            workoutDetectionItem.setPauseLatency(workoutSetting.getPauseLatency());
            workoutDetectionItem.setResumeLatency(workoutSetting.getResumeLatency());
            workoutDetectionItem.setStopLatency(workoutSetting.getStopLatency());
            arrayList.add(workoutDetectionItem);
        }
        return new WorkoutDetectionSetting(arrayList);
    }
}
