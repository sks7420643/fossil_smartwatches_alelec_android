package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr4 implements Factory<yr4> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<fp4> b;

    @DexIgnore
    public zr4(Provider<ch5> provider, Provider<fp4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static zr4 a(Provider<ch5> provider, Provider<fp4> provider2) {
        return new zr4(provider, provider2);
    }

    @DexIgnore
    public static yr4 a(ch5 ch5, fp4 fp4) {
        return new yr4(ch5, fp4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public yr4 get() {
        return a(this.a.get(), this.b.get());
    }
}
