package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public long c;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public og3(String str, String str2, Bundle bundle, long j) {
        this.a = str;
        this.b = str2;
        this.d = bundle == null ? new Bundle() : bundle;
        this.c = j;
    }

    @DexIgnore
    public static og3 a(ub3 ub3) {
        return new og3(ub3.a, ub3.c, ub3.b.zzb(), ub3.d);
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String str2 = this.a;
        String valueOf = String.valueOf(this.d);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final ub3 a() {
        return new ub3(this.a, new tb3(new Bundle(this.d)), this.b, this.c);
    }
}
