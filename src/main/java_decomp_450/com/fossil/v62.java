package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v62 implements w62 {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public v62(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    @Override // com.fossil.w62
    public final void a(u62 u62, m62 m62) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            obtain.writeStrongBinder(u62 != null ? u62.asBinder() : null);
            if (m62 != null) {
                obtain.writeInt(1);
                m62.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.a.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.a;
    }
}
