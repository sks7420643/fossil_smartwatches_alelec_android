package com.fossil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class di5 extends BaseDbProvider implements ci5 {
    @DexIgnore
    public static /* final */ String a; // = "di5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.di5$a$a")
        /* renamed from: com.fossil.di5$a$a  reason: collision with other inner class name */
        public class C0039a implements UpgradeCommand {
            @DexIgnore
            public C0039a(a aVar) {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE microAppSetting ADD COLUMN pinType INTEGER DEFAULT 0");
            }
        }

        @DexIgnore
        public a() {
            put(2, new C0039a(this));
        }
    }

    @DexIgnore
    public di5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public boolean a(MicroAppSetting microAppSetting) {
        if (microAppSetting != null) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, "addOrUpdateSetting microAppId=" + microAppSetting.getMicroAppId() + ", setting=" + microAppSetting.getSetting());
                e().createOrUpdate(microAppSetting);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = a;
                local2.e(str2, "addOrUpdateSetting Exception=" + e);
                return false;
            }
        } else {
            FLogger.INSTANCE.getLocal().e(a, "addOrUpdateSetting microAppSetting null");
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public boolean b() {
        FLogger.INSTANCE.getLocal().e(a, "getAllMicroAppSettingList");
        try {
            TableUtils.clearTable(e().getConnectionSource(), MicroAppSetting.class);
            return true;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "getAllMicroAppSettingList Exception=" + e);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public MicroAppSetting c(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.e(str2, "getSettingByMicroAppId microAppId=" + str);
        try {
            QueryBuilder<MicroAppSetting, Integer> queryBuilder = e().queryBuilder();
            queryBuilder.where().eq("appId", str);
            MicroAppSetting queryForFirst = queryBuilder.queryForFirst();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.d(str3, "Inside .getSettingByMicroAppId in thread=" + Thread.currentThread().getName());
            if (queryForFirst != null) {
                return queryForFirst;
            }
            FLogger.INSTANCE.getLocal().e(a, "getSettingByMicroAppId microAppSetting null");
            return null;
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = a;
            local3.e(str4, "getSettingByMicroAppId Exception=" + e);
            return null;
        }
    }

    @DexIgnore
    public final Dao<MicroAppSetting, Integer> e() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(MicroAppSetting.class);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{MicroAppSetting.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new a();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        FLogger.INSTANCE.getLocal().e(a, "getPendingMicroAppSettings");
        try {
            QueryBuilder<MicroAppSetting, Integer> queryBuilder = e().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            List<MicroAppSetting> query = queryBuilder.query();
            if (query != null) {
                return query;
            }
            FLogger.INSTANCE.getLocal().e(a, "getPendingMicroAppSettings microAppSetting null");
            return new ArrayList();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "getPendingMicroAppSettings Exception=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public void a(String str, int i) {
        MicroAppSetting c = c(str);
        if (c != null) {
            c.setPinType(i);
            a(c);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci5
    public List<MicroAppSetting> c() {
        FLogger.INSTANCE.getLocal().e(a, "getAllMicroAppSettingList");
        try {
            List<MicroAppSetting> query = e().queryBuilder().query();
            if (query != null) {
                return query;
            }
            FLogger.INSTANCE.getLocal().e(a, "getAllMicroAppSettingList microAppSetting null");
            return new ArrayList();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "getAllMicroAppSettingList Exception=" + e);
            return null;
        }
    }
}
