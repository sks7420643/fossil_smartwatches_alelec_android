package com.fossil;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.iy1;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg3 extends hi3 {
    @DexIgnore
    public static /* final */ Pair<String, Long> D; // = new Pair<>("", 0L);
    @DexIgnore
    public /* final */ ch3 A; // = new ch3(this, "deferred_attribution_cache", null);
    @DexIgnore
    public /* final */ ah3 B; // = new ah3(this, "deferred_attribution_cache_timestamp", 0);
    @DexIgnore
    public /* final */ xg3 C; // = new xg3(this, "default_event_parameters", null);
    @DexIgnore
    public SharedPreferences c;
    @DexIgnore
    public zg3 d;
    @DexIgnore
    public /* final */ ah3 e; // = new ah3(this, "last_upload", 0);
    @DexIgnore
    public /* final */ ah3 f; // = new ah3(this, "last_upload_attempt", 0);
    @DexIgnore
    public /* final */ ah3 g; // = new ah3(this, "backoff", 0);
    @DexIgnore
    public /* final */ ah3 h; // = new ah3(this, "last_delete_stale", 0);
    @DexIgnore
    public /* final */ ah3 i; // = new ah3(this, "midnight_offset", 0);
    @DexIgnore
    public /* final */ ah3 j; // = new ah3(this, "first_open_time", 0);
    @DexIgnore
    public /* final */ ah3 k; // = new ah3(this, "app_install_time", 0);
    @DexIgnore
    public /* final */ ch3 l; // = new ch3(this, "app_instance_id", null);
    @DexIgnore
    public String m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public long o;
    @DexIgnore
    public /* final */ ah3 p; // = new ah3(this, "time_before_start", ButtonService.CONNECT_TIMEOUT);
    @DexIgnore
    public /* final */ ah3 q; // = new ah3(this, "session_timeout", 1800000);
    @DexIgnore
    public /* final */ yg3 r; // = new yg3(this, "start_new_session", true);
    @DexIgnore
    public /* final */ ch3 s; // = new ch3(this, "non_personalized_ads", null);
    @DexIgnore
    public /* final */ yg3 t; // = new yg3(this, "allow_remote_dynamite", false);
    @DexIgnore
    public /* final */ ah3 u; // = new ah3(this, "last_pause_time", 0);
    @DexIgnore
    public boolean v;
    @DexIgnore
    public yg3 w; // = new yg3(this, "app_backgrounded", false);
    @DexIgnore
    public yg3 x; // = new yg3(this, "deep_link_retrieval_complete", false);
    @DexIgnore
    public ah3 y; // = new ah3(this, "deep_link_retrieval_attempts", 0);
    @DexIgnore
    public /* final */ ch3 z; // = new ch3(this, "firebase_feature_rollouts", null);

    @DexIgnore
    public wg3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final Pair<String, Boolean> a(String str) {
        g();
        long c2 = zzm().c();
        if (this.m != null && c2 < this.o) {
            return new Pair<>(this.m, Boolean.valueOf(this.n));
        }
        this.o = c2 + l().a(str, wb3.b);
        iy1.b(true);
        try {
            iy1.a a = iy1.a(f());
            if (a != null) {
                this.m = a.a();
                this.n = a.b();
            }
            if (this.m == null) {
                this.m = "";
            }
        } catch (Exception e2) {
            e().A().a("Unable to get advertising id", e2);
            this.m = "";
        }
        iy1.b(false);
        return new Pair<>(this.m, Boolean.valueOf(this.n));
    }

    @DexIgnore
    public final String b(String str) {
        g();
        String str2 = (String) a(str).first;
        MessageDigest x2 = jm3.x();
        if (x2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, x2.digest(str2.getBytes())));
    }

    @DexIgnore
    public final void c(String str) {
        g();
        SharedPreferences.Editor edit = s().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final void d(String str) {
        g();
        SharedPreferences.Editor edit = s().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final void m() {
        SharedPreferences sharedPreferences = f().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.c = sharedPreferences;
        boolean z2 = sharedPreferences.getBoolean("has_been_opened", false);
        this.v = z2;
        if (!z2) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.d = new zg3(this, "health_monitor", Math.max(0L, wb3.c.a(null).longValue()));
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final boolean q() {
        return true;
    }

    @DexIgnore
    public final SharedPreferences s() {
        g();
        n();
        return this.c;
    }

    @DexIgnore
    public final String t() {
        g();
        return s().getString("gmp_app_id", null);
    }

    @DexIgnore
    public final String u() {
        g();
        return s().getString("admob_app_id", null);
    }

    @DexIgnore
    public final Boolean v() {
        g();
        if (!s().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(s().getBoolean("use_service", false));
    }

    @DexIgnore
    public final void w() {
        g();
        Boolean x2 = x();
        SharedPreferences.Editor edit = s().edit();
        edit.clear();
        edit.apply();
        if (x2 != null) {
            b(x2.booleanValue());
        }
    }

    @DexIgnore
    public final Boolean x() {
        g();
        if (s().contains("measurement_enabled")) {
            return Boolean.valueOf(s().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    @DexIgnore
    public final String y() {
        g();
        String string = s().getString("previous_os_version", null);
        h().n();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = s().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    @DexIgnore
    public final boolean z() {
        return this.c.contains("deferred_analytics_collection");
    }

    @DexIgnore
    public final void b(boolean z2) {
        g();
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("measurement_enabled", z2);
        edit.apply();
    }

    @DexIgnore
    public final void c(boolean z2) {
        g();
        e().B().a("App measurement setting deferred collection", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("deferred_analytics_collection", z2);
        edit.apply();
    }

    @DexIgnore
    public final void a(boolean z2) {
        g();
        SharedPreferences.Editor edit = s().edit();
        edit.putBoolean("use_service", z2);
        edit.apply();
    }

    @DexIgnore
    public final boolean a(long j2) {
        return j2 - this.q.a() > this.u.a();
    }
}
