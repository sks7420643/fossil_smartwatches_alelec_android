package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ lf7 b;

    @DexIgnore
    public vg7(String str, lf7 lf7) {
        ee7.b(str, "value");
        ee7.b(lf7, "range");
        this.a = str;
        this.b = lf7;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof vg7)) {
            return false;
        }
        vg7 vg7 = (vg7) obj;
        return ee7.a(this.a, vg7.a) && ee7.a(this.b, vg7.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        lf7 lf7 = this.b;
        if (lf7 != null) {
            i = lf7.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "MatchGroup(value=" + this.a + ", range=" + this.b + ")";
    }
}
