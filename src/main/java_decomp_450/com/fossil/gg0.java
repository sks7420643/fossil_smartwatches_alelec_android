package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gg0 extends dg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gg0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                    return new gg0(readString, createByteArray);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gg0[] newArray(int i) {
            return new gg0[i];
        }
    }

    @DexIgnore
    public gg0(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    @Override // com.fossil.dg0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            gg0 gg0 = (gg0) obj;
            if (c() == gg0.c() || ee7.a((Object) yz0.a((int) c()), (Object) gg0.getFileName()) || ee7.a((Object) getFileName(), (Object) yz0.a((int) gg0.c()))) {
                return true;
            }
            return false;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.file.NotificationIcon");
    }

    @DexIgnore
    @Override // com.fossil.dg0
    public int hashCode() {
        return (int) c();
    }
}
