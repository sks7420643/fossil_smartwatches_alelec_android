package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lu extends fv {
    @DexIgnore
    public Intent mResolutionIntent;

    @DexIgnore
    public lu() {
    }

    @DexIgnore
    public String getMessage() {
        if (this.mResolutionIntent != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }

    @DexIgnore
    public Intent getResolutionIntent() {
        return this.mResolutionIntent;
    }

    @DexIgnore
    public lu(Intent intent) {
        this.mResolutionIntent = intent;
    }

    @DexIgnore
    public lu(vu vuVar) {
        super(vuVar);
    }

    @DexIgnore
    public lu(String str) {
        super(str);
    }

    @DexIgnore
    public lu(String str, Exception exc) {
        super(str, exc);
    }
}
