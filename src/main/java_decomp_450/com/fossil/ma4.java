package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ma4 {
    @DexIgnore
    public static ma4 e;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public b c; // = new b();
    @DexIgnore
    public int d; // = 1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ Messenger b;
        @DexIgnore
        public c c;
        @DexIgnore
        public /* final */ Queue<e<?>> d;
        @DexIgnore
        public /* final */ SparseArray<e<?>> e;

        @DexIgnore
        public b() {
            this.a = 0;
            this.b = new Messenger(new qg2(Looper.getMainLooper(), new na4(this)));
            this.d = new ArrayDeque();
            this.e = new SparseArray<>();
        }

        @DexIgnore
        public synchronized boolean a(e<?> eVar) {
            int i = this.a;
            if (i == 0) {
                this.d.add(eVar);
                d();
                return true;
            } else if (i == 1) {
                this.d.add(eVar);
                return true;
            } else if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        int i2 = this.a;
                        StringBuilder sb = new StringBuilder(26);
                        sb.append("Unknown state: ");
                        sb.append(i2);
                        throw new IllegalStateException(sb.toString());
                    }
                }
                return false;
            } else {
                this.d.add(eVar);
                c();
                return true;
            }
        }

        @DexIgnore
        public final /* synthetic */ void b() {
            e<?> poll;
            while (true) {
                synchronized (this) {
                    if (this.a == 2) {
                        if (this.d.isEmpty()) {
                            f();
                            return;
                        }
                        poll = this.d.poll();
                        this.e.put(poll.a, poll);
                        ma4.this.b.schedule(new sa4(this, poll), 30, TimeUnit.SECONDS);
                    } else {
                        return;
                    }
                }
                c(poll);
            }
        }

        @DexIgnore
        public void c() {
            ma4.this.b.execute(new qa4(this));
        }

        @DexIgnore
        public void d() {
            a72.b(this.a == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!e92.a().a(ma4.this.a, intent, this, 1)) {
                a(0, "Unable to bind to service");
            } else {
                ma4.this.b.schedule(new oa4(this), 30, TimeUnit.SECONDS);
            }
        }

        @DexIgnore
        public synchronized void e() {
            if (this.a == 1) {
                a(1, "Timed out while binding");
            }
        }

        @DexIgnore
        public synchronized void f() {
            if (this.a == 2 && this.d.isEmpty() && this.e.size() == 0) {
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
                }
                this.a = 3;
                e92.a().a(ma4.this.a, this);
            }
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service connected");
            }
            ma4.this.b.execute(new pa4(this, iBinder));
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service disconnected");
            }
            ma4.this.b.execute(new ra4(this));
        }

        @DexIgnore
        public void c(e<?> eVar) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(eVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
                sb.append("Sending ");
                sb.append(valueOf);
                Log.d("MessengerIpcClient", sb.toString());
            }
            try {
                this.c.a(eVar.a(ma4.this.a, this.b));
            } catch (RemoteException e2) {
                a(2, e2.getMessage());
            }
        }

        @DexIgnore
        public boolean a(Message message) {
            int i = message.arg1;
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                StringBuilder sb = new StringBuilder(41);
                sb.append("Received response to request: ");
                sb.append(i);
                Log.d("MessengerIpcClient", sb.toString());
            }
            synchronized (this) {
                e<?> eVar = this.e.get(i);
                if (eVar == null) {
                    StringBuilder sb2 = new StringBuilder(50);
                    sb2.append("Received response for unknown request: ");
                    sb2.append(i);
                    Log.w("MessengerIpcClient", sb2.toString());
                    return true;
                }
                this.e.remove(i);
                f();
                eVar.a(message.getData());
                return true;
            }
        }

        @DexIgnore
        public final /* synthetic */ void b(e eVar) {
            a(eVar.a);
        }

        @DexIgnore
        public synchronized void a(int i, String str) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(str);
                Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
            }
            int i2 = this.a;
            if (i2 == 0) {
                throw new IllegalStateException();
            } else if (i2 == 1 || i2 == 2) {
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Unbinding service");
                }
                this.a = 4;
                e92.a().a(ma4.this.a, this);
                a(new f(i, str));
            } else if (i2 == 3) {
                this.a = 4;
            } else if (i2 != 4) {
                int i3 = this.a;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i3);
                throw new IllegalStateException(sb.toString());
            }
        }

        @DexIgnore
        public void a(f fVar) {
            for (e<?> eVar : this.d) {
                eVar.a(fVar);
            }
            this.d.clear();
            for (int i = 0; i < this.e.size(); i++) {
                this.e.valueAt(i).a(fVar);
            }
            this.e.clear();
        }

        @DexIgnore
        public synchronized void a(int i) {
            e<?> eVar = this.e.get(i);
            if (eVar != null) {
                StringBuilder sb = new StringBuilder(31);
                sb.append("Timing out request: ");
                sb.append(i);
                Log.w("MessengerIpcClient", sb.toString());
                this.e.remove(i);
                eVar.a(new f(3, "Timed out waiting for response"));
                f();
            }
        }

        @DexIgnore
        public final /* synthetic */ void a() {
            a(2, "Service disconnected");
        }

        @DexIgnore
        public final /* synthetic */ void a(IBinder iBinder) {
            synchronized (this) {
                if (iBinder == null) {
                    a(0, "Null service connection");
                    return;
                }
                try {
                    this.c = new c(iBinder);
                    this.a = 2;
                    c();
                } catch (RemoteException e2) {
                    a(0, e2.getMessage());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Messenger a;
        @DexIgnore
        public /* final */ v94 b;

        @DexIgnore
        public c(IBinder iBinder) throws RemoteException {
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("android.os.IMessenger".equals(interfaceDescriptor)) {
                this.a = new Messenger(iBinder);
                this.b = null;
            } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
                this.b = new v94(iBinder);
                this.a = null;
            } else {
                String valueOf = String.valueOf(interfaceDescriptor);
                Log.w("MessengerIpcClient", valueOf.length() != 0 ? "Invalid interface descriptor: ".concat(valueOf) : new String("Invalid interface descriptor: "));
                throw new RemoteException();
            }
        }

        @DexIgnore
        public void a(Message message) throws RemoteException {
            Messenger messenger = this.a;
            if (messenger != null) {
                messenger.send(message);
                return;
            }
            v94 v94 = this.b;
            if (v94 != null) {
                v94.a(message);
                return;
            }
            throw new IllegalStateException("Both messengers are null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends e<Void> {
        @DexIgnore
        public d(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.ma4.e
        public void b(Bundle bundle) {
            if (bundle.getBoolean("ack", false)) {
                a((Object) null);
            } else {
                a(new f(4, "Invalid response to one way request"));
            }
        }

        @DexIgnore
        @Override // com.fossil.ma4.e
        public boolean b() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends Exception {
        @DexIgnore
        public /* final */ int errorCode;

        @DexIgnore
        public f(int i, String str) {
            super(str);
            this.errorCode = i;
        }

        @DexIgnore
        public int getErrorCode() {
            return this.errorCode;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends e<Bundle> {
        @DexIgnore
        public g(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.ma4.e
        public void b(Bundle bundle) {
            Bundle bundle2 = bundle.getBundle("data");
            if (bundle2 == null) {
                bundle2 = Bundle.EMPTY;
            }
            a((Object) bundle2);
        }

        @DexIgnore
        @Override // com.fossil.ma4.e
        public boolean b() {
            return false;
        }
    }

    @DexIgnore
    public ma4(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static synchronized ma4 a(Context context) {
        ma4 ma4;
        synchronized (ma4.class) {
            if (e == null) {
                e = new ma4(context, mg2.a().a(1, new ba2("MessengerIpcClient"), rg2.a));
            }
            ma4 = e;
        }
        return ma4;
    }

    @DexIgnore
    public no3<Bundle> b(int i, Bundle bundle) {
        return a(new g(a(), i, bundle));
    }

    @DexIgnore
    public no3<Void> a(int i, Bundle bundle) {
        return a(new d(a(), i, bundle));
    }

    @DexIgnore
    public final synchronized <T> no3<T> a(e<T> eVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(eVar);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.c.a((e<?>) eVar)) {
            b bVar = new b();
            this.c = bVar;
            bVar.a((e<?>) eVar);
        }
        return eVar.a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ oo3<T> b; // = new oo3<>();
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ Bundle d;

        @DexIgnore
        public e(int i, int i2, Bundle bundle) {
            this.a = i;
            this.c = i2;
            this.d = bundle;
        }

        @DexIgnore
        public Message a(Context context, Messenger messenger) {
            Message obtain = Message.obtain();
            obtain.what = this.c;
            obtain.arg1 = this.a;
            obtain.replyTo = messenger;
            Bundle bundle = new Bundle();
            bundle.putBoolean("oneWay", b());
            bundle.putString("pkg", context.getPackageName());
            bundle.putBundle("data", this.d);
            obtain.setData(bundle);
            return obtain;
        }

        @DexIgnore
        public abstract void b(Bundle bundle);

        @DexIgnore
        public abstract boolean b();

        @DexIgnore
        public String toString() {
            int i = this.c;
            int i2 = this.a;
            boolean b2 = b();
            StringBuilder sb = new StringBuilder(55);
            sb.append("Request { what=");
            sb.append(i);
            sb.append(" id=");
            sb.append(i2);
            sb.append(" oneWay=");
            sb.append(b2);
            sb.append("}");
            return sb.toString();
        }

        @DexIgnore
        public no3<T> a() {
            return this.b.a();
        }

        @DexIgnore
        public void a(T t) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(t);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length());
                sb.append("Finishing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.a(t);
        }

        @DexIgnore
        public void a(f fVar) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(fVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length());
                sb.append("Failing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.a(fVar);
        }

        @DexIgnore
        public void a(Bundle bundle) {
            if (bundle.getBoolean("unsupported", false)) {
                a(new f(4, "Not supported by GmsCore"));
            } else {
                b(bundle);
            }
        }
    }

    @DexIgnore
    public final synchronized int a() {
        int i;
        i = this.d;
        this.d = i + 1;
        return i;
    }
}
