package com.fossil;

import android.content.Context;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import java.lang.ref.WeakReference;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u31 {
    @DexIgnore
    public static WeakReference<Context> a;
    @DexIgnore
    public static String b; // = new String();
    @DexIgnore
    public static dh0 c; // = new dh0("", "", "");
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static Object f; // = new Object();
    @DexIgnore
    public static /* final */ u31 g; // = new u31();

    /*
    static {
        new dh0("", "", "");
        String str = Build.VERSION.RELEASE;
        String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        if (str == null) {
            str = str2;
        }
        d = str;
        String str3 = Build.MODEL;
        if (str3 != null) {
            str2 = str3;
        }
        e = str2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        b = str;
    }

    @DexIgnore
    public final String b() {
        return d;
    }

    @DexIgnore
    public final String c() {
        return e;
    }

    @DexIgnore
    public final int d() {
        return (TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 1000) / 60;
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final Object f() {
        return f;
    }

    @DexIgnore
    public final void a(dh0 dh0) {
        c = dh0;
        wl0 wl0 = wl0.h;
        ((xp1) wl0).c.a(new dh0(c.c() + "/sdk_log", c.a(), c.b()));
        xn1 xn1 = xn1.h;
        ((xp1) xn1).c.a(new dh0(c.c() + "/raw_minute_data", c.a(), c.b()));
        yl1 yl1 = yl1.h;
        ((xp1) yl1).c.a(new dh0(c.c() + "/raw_hardware_log", c.a(), c.b()));
        ak1 ak1 = ak1.h;
        ((xp1) ak1).c.a(new dh0(c.c() + "/gps_data", c.a(), c.b()));
    }

    @DexIgnore
    public final void a(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            a = new WeakReference<>(applicationContext);
        }
    }

    @DexIgnore
    public final Context a() {
        WeakReference<Context> weakReference = a;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public final void a(Object obj) {
        f = obj;
    }
}
