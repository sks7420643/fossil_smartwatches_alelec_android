package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fx7 {
    @DexIgnore
    public static final a a = a.f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ boolean a; // = (Build.VERSION.SDK_INT >= 29);
        @DexIgnore
        public static /* final */ String[] b; // = {"_display_name", "_data", FieldType.FOREIGN_ID_FIELD_SUFFIX, "title", "bucket_id", "bucket_display_name", "width", "height", "date_modified", "mime_type", "datetaken"};
        @DexIgnore
        public static /* final */ String[] c; // = {"_display_name", "_data", FieldType.FOREIGN_ID_FIELD_SUFFIX, "title", "bucket_id", "bucket_display_name", "datetaken", "width", "height", "date_modified", "mime_type", "duration"};
        @DexIgnore
        public static /* final */ String[] d; // = {MessengerShareContentUtility.MEDIA_TYPE, "_display_name"};
        @DexIgnore
        public static /* final */ String[] e; // = {"bucket_id", "bucket_display_name"};
        @DexIgnore
        public static /* final */ /* synthetic */ a f; // = new a();

        @DexIgnore
        public final String[] a() {
            return e;
        }

        @DexIgnore
        public final String[] b() {
            return b;
        }

        @DexIgnore
        public final String[] c() {
            return c;
        }

        @DexIgnore
        public final String[] d() {
            return d;
        }

        @DexIgnore
        public final boolean e() {
            return a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static int a(fx7 fx7, int i) {
            if (i != 1) {
                return i != 3 ? 0 : 2;
            }
            return 1;
        }

        @DexIgnore
        public static Uri a(fx7 fx7) {
            Uri contentUri = MediaStore.Files.getContentUri("external");
            ee7.a((Object) contentUri, "MediaStore.Files.getContentUri(VOLUME_EXTERNAL)");
            return contentUri;
        }

        @DexIgnore
        public static int b(fx7 fx7, Cursor cursor, String str) {
            ee7.b(cursor, "$this$getInt");
            ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getInt(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static long c(fx7 fx7, Cursor cursor, String str) {
            ee7.b(cursor, "$this$getLong");
            ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getLong(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static String d(fx7 fx7, Cursor cursor, String str) {
            ee7.b(cursor, "$this$getString");
            ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            String string = cursor.getString(cursor.getColumnIndex(str));
            ee7.a((Object) string, "getString(getColumnIndex(columnName))");
            return string;
        }

        @DexIgnore
        public static String a(fx7 fx7, Integer num) {
            if (num == null || num.intValue() == 2) {
                return "";
            }
            if (num.intValue() == 1) {
                return "AND " + "width > 0 AND height > 0";
            }
            return "AND (media_type = 3 or (" + "media_type = 1 AND " + "width > 0 AND height > 0" + "))";
        }

        @DexIgnore
        public static /* synthetic */ List a(fx7 fx7, Context context, String str, int i, int i2, int i3, long j, ax7 ax7, yw7 yw7, int i4, Object obj) {
            if (obj == null) {
                return fx7.a(context, str, i, i2, (i4 & 16) != 0 ? 0 : i3, j, ax7, (i4 & 128) != 0 ? null : yw7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getAssetFromGalleryId");
        }

        @DexIgnore
        public static double a(fx7 fx7, Cursor cursor, String str) {
            ee7.b(cursor, "$this$getDouble");
            ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
            return cursor.getDouble(cursor.getColumnIndex(str));
        }

        @DexIgnore
        public static List<String> a(fx7 fx7, Context context, List<String> list) {
            ee7.b(context, "context");
            ee7.b(list, "ids");
            String a = ea7.a(list, null, null, null, 0, null, null, 63, null);
            try {
                if (context.getContentResolver().delete(fx7.a(), "_id in (?)", new String[]{a}) > 0) {
                    return list;
                }
                return w97.a();
            } catch (Exception unused) {
                return w97.a();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003c, code lost:
            r7 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
            com.fossil.hc7.a(r8, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
            throw r7;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean a(com.fossil.fx7 r6, android.content.Context r7, java.lang.String r8) {
            /*
                java.lang.String r6 = "context"
                com.fossil.ee7.b(r7, r6)
                java.lang.String r6 = "id"
                com.fossil.ee7.b(r8, r6)
                java.lang.String r6 = "_id"
                java.lang.String[] r2 = new java.lang.String[]{r6}
                android.content.ContentResolver r0 = r7.getContentResolver()
                com.fossil.ex7 r6 = com.fossil.ex7.f
                android.net.Uri r1 = r6.a()
                r6 = 1
                java.lang.String[] r4 = new java.lang.String[r6]
                r7 = 0
                r4[r7] = r8
                java.lang.String r3 = "MediaStore.Files.FileColumns._ID = ?"
                r5 = 0
                android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)
                r0 = 0
                if (r8 != 0) goto L_0x002e
                com.fossil.hc7.a(r8, r0)
                return r7
            L_0x002e:
                int r1 = r8.getCount()     // Catch:{ all -> 0x003a }
                if (r1 < r6) goto L_0x0035
                goto L_0x0036
            L_0x0035:
                r6 = 0
            L_0x0036:
                com.fossil.hc7.a(r8, r0)
                return r6
            L_0x003a:
                r6 = move-exception
                throw r6     // Catch:{ all -> 0x003c }
            L_0x003c:
                r7 = move-exception
                com.fossil.hc7.a(r8, r6)
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.fx7.b.a(com.fossil.fx7, android.content.Context, java.lang.String):boolean");
        }

        @DexIgnore
        public static String a(fx7 fx7, int i, ax7 ax7, ArrayList<String> arrayList) {
            ee7.b(ax7, "filterOptions");
            ee7.b(arrayList, "args");
            String d = ax7.d();
            String[] c = ax7.c();
            String b = ax7.b();
            String[] a = ax7.a();
            if (i == 1) {
                arrayList.add(String.valueOf(1));
                String str = "AND media_type = ?" + "AND " + d;
                ba7.a(arrayList, c);
                return str;
            } else if (i != 2) {
                String str2 = "AND (" + MessengerShareContentUtility.MEDIA_TYPE + " = ? OR (" + MessengerShareContentUtility.MEDIA_TYPE + " = ? AND " + b + ")) AND " + d;
                arrayList.add(String.valueOf(1));
                arrayList.add(String.valueOf(3));
                ba7.a(arrayList, a);
                ba7.a(arrayList, c);
                return str2;
            } else {
                arrayList.add(String.valueOf(3));
                ba7.a(arrayList, c);
                String str3 = ("AND media_type = ?" + "AND " + d) + "AND " + b;
                ba7.a(arrayList, a);
                return str3;
            }
        }
    }

    @DexIgnore
    Bitmap a(Context context, String str, int i, int i2, Integer num);

    @DexIgnore
    Uri a();

    @DexIgnore
    bx7 a(Context context, String str, int i, long j, ax7 ax7);

    @DexIgnore
    zw7 a(Context context, InputStream inputStream, String str, String str2);

    @DexIgnore
    zw7 a(Context context, byte[] bArr, String str, String str2);

    @DexIgnore
    String a(Context context, String str, boolean z);

    @DexIgnore
    List<bx7> a(Context context, int i, long j, ax7 ax7);

    @DexIgnore
    List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7);

    @DexIgnore
    List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7, yw7 yw7);

    @DexIgnore
    List<String> a(Context context, List<String> list);

    @DexIgnore
    void a(Context context, zw7 zw7, byte[] bArr);

    @DexIgnore
    boolean a(Context context, String str);

    @DexIgnore
    byte[] a(Context context, zw7 zw7, boolean z);

    @DexIgnore
    zw7 b(Context context, String str);

    @DexIgnore
    ub c(Context context, String str);

    @DexIgnore
    void k();
}
