package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e56 implements Factory<d56> {
    @DexIgnore
    public static d56 a(PortfolioApp portfolioApp, c56 c56, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, a56 a56, ch5 ch5) {
        return new d56(portfolioApp, c56, microAppRepository, hybridPresetRepository, a56, ch5);
    }
}
