package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj7<T> extends jm7<T> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater e; // = AtomicIntegerFieldUpdater.newUpdater(nj7.class, "_decision");
    @DexIgnore
    public volatile int _decision; // = 0;

    @DexIgnore
    public nj7(ib7 ib7, fb7<? super T> fb7) {
        super(ib7, fb7);
    }

    @DexIgnore
    @Override // com.fossil.jm7, com.fossil.pk7
    public void a(Object obj) {
        l(obj);
    }

    @DexIgnore
    @Override // com.fossil.rh7, com.fossil.jm7
    public void l(Object obj) {
        if (!q()) {
            mj7.a(mb7.a(((jm7) this).d), mi7.a(obj, ((jm7) this).d));
        }
    }

    @DexIgnore
    public final Object p() {
        if (r()) {
            return nb7.a();
        }
        Object b = qk7.b(h());
        if (!(b instanceof li7)) {
            return b;
        }
        throw ((li7) b).a;
    }

    @DexIgnore
    public final boolean q() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!e.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean r() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!e.compareAndSet(this, 0, 1));
        return true;
    }
}
