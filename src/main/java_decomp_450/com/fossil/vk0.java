package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk0 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vk0(vv0 vv0, byte[] bArr) {
        super(1);
        this.a = vv0;
        this.b = bArr;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        vv0 vv0 = this.a;
        vv0.J = vv0.J + ((long) this.b.length);
        long d = this.a.J;
        vv0 vv02 = this.a;
        if (d + vv02.I >= vv02.D) {
            vv02.J = 0L;
            this.a.n();
        } else {
            vv02.m();
        }
        return i97.a;
    }
}
