package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nu1 implements Executor {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ Runnable a;

        @DexIgnore
        public a(Runnable runnable) {
            this.a = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.a.run();
            } catch (Exception e) {
                kv1.a("Executor", "Background execution failure.", (Throwable) e);
            }
        }
    }

    @DexIgnore
    public nu1(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.a.execute(new a(runnable));
    }
}
