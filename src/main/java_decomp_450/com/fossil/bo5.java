package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo5 extends fl4<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.b {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public a(SignUpSocialAuth signUpSocialAuth) {
            ee7.b(signUpSocialAuth, "socialAuth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            ee7.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase", f = "SignUpSocialUseCase.kt", l = {23, 26}, m = "run")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bo5 bo5, fb7 fb7) {
            super(fb7);
            this.this$0 = bo5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((a) null, (fb7<Object>) this);
        }
    }

    @DexIgnore
    public bo5(UserRepository userRepository, ch5 ch5) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(a aVar, fb7 fb7) {
        return a(aVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SignUpSocialUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.bo5.a r10, com.fossil.fb7<java.lang.Object> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.bo5.d
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.bo5$d r0 = (com.fossil.bo5.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.bo5$d r0 = new com.fossil.bo5$d
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 600(0x258, float:8.41E-43)
            r4 = 2
            r5 = 1
            java.lang.String r6 = ""
            java.lang.String r7 = "SignUpSocialUseCase"
            if (r2 == 0) goto L_0x0052
            if (r2 == r5) goto L_0x0042
            if (r2 != r4) goto L_0x003a
            java.lang.Object r10 = r0.L$1
            com.fossil.bo5$a r10 = (com.fossil.bo5.a) r10
            java.lang.Object r10 = r0.L$0
            com.fossil.bo5 r10 = (com.fossil.bo5) r10
            com.fossil.t87.a(r11)
            goto L_0x0097
        L_0x003a:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0042:
            java.lang.Object r10 = r0.L$1
            com.fossil.bo5$a r10 = (com.fossil.bo5.a) r10
            java.lang.Object r2 = r0.L$0
            com.fossil.bo5 r2 = (com.fossil.bo5) r2
            com.fossil.t87.a(r11)
            r8 = r11
            r11 = r10
            r10 = r2
            r2 = r8
            goto L_0x007b
        L_0x0052:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = "running UseCase"
            r11.d(r7, r2)
            if (r10 != 0) goto L_0x0069
            com.fossil.bo5$b r10 = new com.fossil.bo5$b
            r10.<init>(r3, r6)
            goto L_0x0108
        L_0x0069:
            com.portfolio.platform.data.source.UserRepository r11 = r9.d
            r0.L$0 = r9
            r0.L$1 = r10
            r0.label = r5
            java.lang.Object r11 = r11.getCurrentUser(r0)
            if (r11 != r1) goto L_0x0078
            return r1
        L_0x0078:
            r2 = r11
            r11 = r10
            r10 = r9
        L_0x007b:
            if (r2 == 0) goto L_0x0084
            com.fossil.bo5$c r10 = new com.fossil.bo5$c
            r10.<init>()
            goto L_0x0108
        L_0x0084:
            com.portfolio.platform.data.source.UserRepository r2 = r10.d
            com.portfolio.platform.data.SignUpSocialAuth r5 = r11.a()
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r4
            java.lang.Object r11 = r2.signUpSocial(r5, r0)
            if (r11 != r1) goto L_0x0097
            return r1
        L_0x0097:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r0 = r11 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00c2
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = "signUpEmail success"
            r0.d(r7, r2)
            com.fossil.ch5 r10 = r10.e
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r11 = r11.a()
            com.portfolio.platform.data.model.MFUser$Auth r11 = (com.portfolio.platform.data.model.MFUser.Auth) r11
            if (r11 == 0) goto L_0x00b9
            java.lang.String r1 = r11.getAccessToken()
        L_0x00b9:
            r10.y(r1)
            com.fossil.bo5$c r10 = new com.fossil.bo5$c
            r10.<init>()
            goto L_0x0108
        L_0x00c2:
            boolean r10 = r11 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x0103
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "signUpEmail failed code="
            r0.append(r2)
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            if (r2 == 0) goto L_0x00e2
            java.lang.String r1 = r2.getMessage()
        L_0x00e2:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.d(r7, r0)
            com.fossil.bo5$b r10 = new com.fossil.bo5$b
            int r0 = r11.a()
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x00ff
            java.lang.String r11 = r11.getMessage()
            if (r11 == 0) goto L_0x00ff
            r6 = r11
        L_0x00ff:
            r10.<init>(r0, r6)
            goto L_0x0108
        L_0x0103:
            com.fossil.bo5$b r10 = new com.fossil.bo5$b
            r10.<init>(r3, r6)
        L_0x0108:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bo5.a(com.fossil.bo5$a, com.fossil.fb7):java.lang.Object");
    }
}
