package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.y62;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<v93> CREATOR; // = new ja3();
    @DexIgnore
    public /* final */ LatLng a;
    @DexIgnore
    public /* final */ LatLng b;
    @DexIgnore
    public /* final */ LatLng c;
    @DexIgnore
    public /* final */ LatLng d;
    @DexIgnore
    public /* final */ LatLngBounds e;

    @DexIgnore
    public v93(LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.a = latLng;
        this.b = latLng2;
        this.c = latLng3;
        this.d = latLng4;
        this.e = latLngBounds;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof v93)) {
            return false;
        }
        v93 v93 = (v93) obj;
        return this.a.equals(v93.a) && this.b.equals(v93.b) && this.c.equals(v93.c) && this.d.equals(v93.d) && this.e.equals(v93.e);
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(this.a, this.b, this.c, this.d, this.e);
    }

    @DexIgnore
    public final String toString() {
        y62.a a2 = y62.a(this);
        a2.a("nearLeft", this.a);
        a2.a("nearRight", this.b);
        a2.a("farLeft", this.c);
        a2.a("farRight", this.d);
        a2.a("latLngBounds", this.e);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, (Parcelable) this.a, i, false);
        k72.a(parcel, 3, (Parcelable) this.b, i, false);
        k72.a(parcel, 4, (Parcelable) this.c, i, false);
        k72.a(parcel, 5, (Parcelable) this.d, i, false);
        k72.a(parcel, 6, (Parcelable) this.e, i, false);
        k72.a(parcel, a2);
    }
}
