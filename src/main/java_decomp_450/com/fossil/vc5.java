package com.fossil;

import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc5 {
    @DexIgnore
    public static final String a(String str, String str2) {
        ee7.b(str2, DatabaseFieldConfigLoader.FIELD_NAME_DEFAULT_VALUE);
        return str == null || mh7.a(str) ? str2 : str;
    }

    @DexIgnore
    public static final UserDisplayUnit a(MFUser mFUser) {
        UserDisplayUnit.TemperatureUnit temperatureUnit;
        UserDisplayUnit.DistanceUnit distanceUnit;
        String str = null;
        if (mFUser == null) {
            return null;
        }
        MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
        String temperature = unitGroup != null ? unitGroup.getTemperature() : null;
        if (ee7.a((Object) temperature, (Object) ob5.METRIC.getValue())) {
            temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
        } else if (ee7.a((Object) temperature, (Object) ob5.IMPERIAL.getValue())) {
            temperatureUnit = UserDisplayUnit.TemperatureUnit.F;
        } else {
            temperatureUnit = UserDisplayUnit.TemperatureUnit.C;
        }
        MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
        if (unitGroup2 != null) {
            str = unitGroup2.getDistance();
        }
        if (ee7.a((Object) str, (Object) ob5.METRIC.getValue())) {
            distanceUnit = UserDisplayUnit.DistanceUnit.KM;
        } else if (ee7.a((Object) str, (Object) ob5.IMPERIAL.getValue())) {
            distanceUnit = UserDisplayUnit.DistanceUnit.MILE;
        } else {
            distanceUnit = UserDisplayUnit.DistanceUnit.KM;
        }
        return new UserDisplayUnit(temperatureUnit, distanceUnit);
    }
}
