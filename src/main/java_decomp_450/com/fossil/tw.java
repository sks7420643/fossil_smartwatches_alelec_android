package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class tw extends Enum<tw> {
    @DexIgnore
    public static /* final */ tw DEFAULT;
    @DexIgnore
    public static /* final */ tw PREFER_ARGB_8888; // = new tw("PREFER_ARGB_8888", 0);
    @DexIgnore
    public static /* final */ tw PREFER_RGB_565;
    @DexIgnore
    public static /* final */ /* synthetic */ tw[] a;

    /*
    static {
        tw twVar = new tw("PREFER_RGB_565", 1);
        PREFER_RGB_565 = twVar;
        tw twVar2 = PREFER_ARGB_8888;
        a = new tw[]{twVar2, twVar};
        DEFAULT = twVar2;
    }
    */

    @DexIgnore
    public tw(String str, int i) {
    }

    @DexIgnore
    public static tw valueOf(String str) {
        return (tw) Enum.valueOf(tw.class, str);
    }

    @DexIgnore
    public static tw[] values() {
        return (tw[]) a.clone();
    }
}
