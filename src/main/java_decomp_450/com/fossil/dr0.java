package com.fossil;

import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class dr0 extends Enum<dr0> implements mw0 {
    @DexIgnore
    public static /* final */ dr0 c;
    @DexIgnore
    public static /* final */ dr0 d;
    @DexIgnore
    public static /* final */ dr0 e;
    @DexIgnore
    public static /* final */ dr0 f;
    @DexIgnore
    public static /* final */ /* synthetic */ dr0[] g;
    @DexIgnore
    public static /* final */ hp0 h; // = new hp0(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        dr0 dr0 = new dr0("SUCCESS", 0, (byte) 0);
        c = dr0;
        dr0 dr02 = new dr0("SIZE_OVER_LIMIT", 7, (byte) 7);
        d = dr02;
        dr0 dr03 = new dr0("FIRMWARE_INTERNAL_ERROR", 8, (byte) 128);
        e = dr03;
        dr0 dr04 = new dr0("FIRMWARE_INTERNAL_ERROR_NOT_SUPPORT", 16, (byte) 136);
        f = dr04;
        g = new dr0[]{dr0, new dr0("INVALID_OPERATION_DATA", 1, (byte) 1), new dr0("OPERATION_IN_PROGRESS", 2, (byte) 2), new dr0("MISS_PACKET", 3, (byte) 3), new dr0("SOCKET_BUSY", 4, (byte) 4), new dr0("VERIFICATION_FAIL", 5, (byte) 5), new dr0("OVERFLOW", 6, (byte) 6), dr02, dr03, new dr0("FIRMWARE_INTERNAL_ERROR_NOT_OPEN", 9, (byte) 129), new dr0("FIRMWARE_INTERNAL_ERROR_ACCESS_ERROR", 10, (byte) 130), new dr0("FIRMWARE_INTERNAL_ERROR_NOT_FOUND", 11, (byte) 131), new dr0("FIRMWARE_INTERNAL_ERROR_NOT_VALID", 12, (byte) 132), new dr0("FIRMWARE_INTERNAL_ERROR_ALREADY_CREATE", 13, (byte) 133), new dr0("FIRMWARE_INTERNAL_ERROR_NOT_ENOUGH_MEMORY", 14, (byte) 134), new dr0("FIRMWARE_INTERNAL_ERROR_NOT_IMPLEMENTED", 15, (byte) 135), dr04, new dr0("FIRMWARE_INTERNAL_ERROR_SOCKET_BUSY", 17, (byte) 137), new dr0("FIRMWARE_INTERNAL_ERROR_SOCKET_ALREADY_OPEN", 18, (byte) 138), new dr0("FIRMWARE_INTERNAL_ERROR_INPUT_DATA_INVALID", 19, (byte) 139), new dr0("FIRMWARE_INTERNAL_NOT_AUTHENTICATE", 20, (byte) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL), new dr0("FIRMWARE_INTERNAL_SIZE_OVER_LIMIT", 21, (byte) 141)};
    }
    */

    @DexIgnore
    public dr0(String str, int i, byte b2) {
        this.b = b2;
    }

    @DexIgnore
    public static dr0 valueOf(String str) {
        return (dr0) Enum.valueOf(dr0.class, str);
    }

    @DexIgnore
    public static dr0[] values() {
        return (dr0[]) g.clone();
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public boolean a() {
        return this == c;
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public String getLogName() {
        return this.a;
    }
}
