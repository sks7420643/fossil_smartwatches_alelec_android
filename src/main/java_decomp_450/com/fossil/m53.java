package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m53 implements Parcelable.Creator<LocationRequest> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationRequest createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        long j4 = 0;
        int i = 102;
        boolean z = false;
        int i2 = Integer.MAX_VALUE;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    j = j72.s(parcel, a);
                    break;
                case 3:
                    j2 = j72.s(parcel, a);
                    break;
                case 4:
                    z = j72.i(parcel, a);
                    break;
                case 5:
                    j3 = j72.s(parcel, a);
                    break;
                case 6:
                    i2 = j72.q(parcel, a);
                    break;
                case 7:
                    f = j72.n(parcel, a);
                    break;
                case 8:
                    j4 = j72.s(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new LocationRequest(i, j, j2, z, j3, i2, f, j4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationRequest[] newArray(int i) {
        return new LocationRequest[i];
    }
}
