package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv0 extends eo0 {
    @DexIgnore
    public /* final */ nm1 j; // = nm1.VERY_HIGH;
    @DexIgnore
    public dd1 k; // = dd1.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean l;

    @DexIgnore
    public mv0(boolean z, cx0 cx0) {
        super(aq0.b, cx0);
        this.l = z;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.a(this.l);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public nm1 b() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.e;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        c(s91);
        x71 x71 = s91.a;
        if (x71.a != z51.SUCCESS) {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        } else if (this.k == dd1.CONNECTED) {
            lk0 = lk0.a(((eo0) this).d, null, oi0.a, null, 5);
        } else {
            lk0 = lk0.a(((eo0) this).d, null, oi0.d, null, 5);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        dd1 dd1;
        return (s91 instanceof nv0) && ((dd1 = ((nv0) s91).b) == dd1.CONNECTED || dd1 == dd1.DISCONNECTED);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.k = ((nv0) s91).b;
    }
}
