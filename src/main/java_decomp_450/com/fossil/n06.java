package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.ComplicationActivitySetting;
import com.portfolio.platform.data.source.CategoryRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n06 extends i06 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public a06 e;
    @DexIgnore
    public /* final */ MutableLiveData<Complication> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Complication>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<String, Boolean, Parcelable>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson j; // = new Gson();
    @DexIgnore
    public ArrayList<Category> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> l;
    @DexIgnore
    public /* final */ zd<String> m;
    @DexIgnore
    public /* final */ LiveData<List<Complication>> n;
    @DexIgnore
    public /* final */ zd<List<Complication>> o;
    @DexIgnore
    public /* final */ zd<v87<String, Boolean, Parcelable>> p;
    @DexIgnore
    public /* final */ j06 q;
    @DexIgnore
    public /* final */ CategoryRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1", f = "ComplicationsPresenter.kt", l = {212}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ n06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return n06.g(this.this$0.this$0).f(this.this$0.$complicationId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(n06 n06, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = n06;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$complicationId, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00b7  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d7  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00d3 A[EDGE_INSN: B:29:0x00d3->B:23:0x00d3 ?: BREAK  , SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L_0x0026
                if (r1 != r2) goto L_0x001e
                java.lang.Object r0 = r7.L$2
                com.fossil.se7 r0 = (com.fossil.se7) r0
                boolean r1 = r7.Z$0
                java.lang.Object r2 = r7.L$1
                com.fossil.se7 r2 = (com.fossil.se7) r2
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r8)
                goto L_0x005b
            L_0x001e:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x0026:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r8 = r7.p$
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                r1.element = r3
                com.fossil.wd5 r4 = com.fossil.wd5.b
                java.lang.String r5 = r7.$complicationId
                boolean r4 = r4.c(r5)
                if (r4 == 0) goto L_0x0061
                com.fossil.n06 r5 = r7.this$0
                com.fossil.ti7 r5 = r5.b()
                com.fossil.n06$b$a r6 = new com.fossil.n06$b$a
                r6.<init>(r7, r3)
                r7.L$0 = r8
                r7.L$1 = r1
                r7.Z$0 = r4
                r7.L$2 = r1
                r7.label = r2
                java.lang.Object r8 = com.fossil.vh7.a(r5, r6, r7)
                if (r8 != r0) goto L_0x0058
                return r0
            L_0x0058:
                r0 = r1
                r2 = r0
                r1 = r4
            L_0x005b:
                android.os.Parcelable r8 = (android.os.Parcelable) r8
                r0.element = r8
                r4 = r1
                r1 = r2
            L_0x0061:
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                java.lang.String r0 = com.fossil.n06.s
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r5 = "checkSettingOfSelectedComplication complicationId="
                r2.append(r5)
                java.lang.String r5 = r7.$complicationId
                r2.append(r5)
                java.lang.String r5 = " settings="
                r2.append(r5)
                T r5 = r1.element
                android.os.Parcelable r5 = (android.os.Parcelable) r5
                r2.append(r5)
                java.lang.String r2 = r2.toString()
                r8.d(r0, r2)
                T r8 = r1.element
                android.os.Parcelable r8 = (android.os.Parcelable) r8
                if (r8 == 0) goto L_0x00f1
                com.fossil.n06 r8 = r7.this$0
                com.fossil.a06 r8 = com.fossil.n06.g(r8)
                androidx.lifecycle.MutableLiveData r8 = r8.a()
                java.lang.Object r8 = r8.a()
                com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
                if (r8 == 0) goto L_0x00f1
                com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r8.clone()
                java.util.ArrayList r0 = r8.getComplications()
                java.util.Iterator r0 = r0.iterator()
            L_0x00b1:
                boolean r2 = r0.hasNext()
                if (r2 == 0) goto L_0x00d3
                java.lang.Object r2 = r0.next()
                r5 = r2
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r5 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r5
                java.lang.String r5 = r5.getId()
                java.lang.String r6 = r7.$complicationId
                boolean r5 = com.fossil.ee7.a(r5, r6)
                java.lang.Boolean r5 = com.fossil.pb7.a(r5)
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x00b1
                r3 = r2
            L_0x00d3:
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r3 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r3
                if (r3 == 0) goto L_0x00f1
                com.fossil.n06 r0 = r7.this$0
                com.google.gson.Gson r0 = r0.j
                T r2 = r1.element
                android.os.Parcelable r2 = (android.os.Parcelable) r2
                java.lang.String r0 = r0.a(r2)
                r3.setSettings(r0)
                com.fossil.n06 r0 = r7.this$0
                com.fossil.a06 r0 = com.fossil.n06.g(r0)
                r0.a(r8)
            L_0x00f1:
                com.fossil.n06 r8 = r7.this$0
                androidx.lifecycle.MutableLiveData r8 = r8.i
                com.fossil.v87 r0 = new com.fossil.v87
                java.lang.String r2 = r7.$complicationId
                java.lang.Boolean r3 = com.fossil.pb7.a(r4)
                T r1 = r1.element
                android.os.Parcelable r1 = (android.os.Parcelable) r1
                r0.<init>(r2, r3, r1)
                r8.a(r0)
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.n06.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore
        public c(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = n06.s;
            local.d(k, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.q.d(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mCategoryOfSelectedComplicationTransformation$1$2$1", f = "ComplicationsPresenter.kt", l = {73}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ n06 $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n06$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mCategoryOfSelectedComplicationTransformation$1$2$1$allCategory$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.n06$d$a$a  reason: collision with other inner class name */
            public static final class C0132a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0132a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0132a aVar = new C0132a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                    return ((C0132a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return this.this$0.$this_run.r.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(n06 n06, fb7 fb7) {
                super(2, fb7);
                this.$this_run = n06;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$this_run, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = qj7.b();
                    C0132a aVar = new C0132a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (!list.isEmpty()) {
                    this.$this_run.g.a((Object) ((Category) list.get(0)).getId());
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(Complication complication) {
            ik7 unused = this.a.c(complication.getComplicationId());
            if (complication != null) {
                String str = (String) this.a.g.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = n06.s;
                local.d(k, "transform from selected complication to category currentCategory=" + str + " compsCategories=" + complication.getCategories());
                ArrayList<String> categories = complication.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.g.a((Object) categories.get(0));
                } else {
                    this.a.g.a((Object) str);
                }
            } else {
                n06 n06 = this.a;
                ik7 unused2 = xh7.b(n06.e(), null, null, new a(n06, null), 3, null);
            }
            return this.a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore
        public e(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<Complication> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = n06.s;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged complications by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(k, sb.toString());
            if (list != null) {
                this.a.q.h(list);
                Complication a2 = n06.g(this.a).e().a();
                if (a2 != null) {
                    this.a.q.b(a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $complicationByCategories;
            @DexIgnore
            public /* final */ /* synthetic */ DianaPreset $currentPreset;
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $filteredComplicationByCategories;
            @DexIgnore
            public /* final */ /* synthetic */ String $selectedComplicationId;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, DianaPreset dianaPreset, List list, String str, ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$currentPreset = dianaPreset;
                this.$complicationByCategories = list;
                this.$selectedComplicationId = str;
                this.$filteredComplicationByCategories = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$currentPreset, this.$complicationByCategories, this.$selectedComplicationId, this.$filteredComplicationByCategories, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                T t;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    if (this.$currentPreset != null) {
                        for (Complication complication : this.$complicationByCategories) {
                            Iterator<T> it = this.$currentPreset.getComplications().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    t = null;
                                    break;
                                }
                                t = it.next();
                                T t2 = t;
                                boolean z = true;
                                if (!ee7.a((Object) t2.getId(), (Object) complication.getComplicationId()) || !(!ee7.a((Object) t2.getId(), (Object) this.$selectedComplicationId))) {
                                    z = false;
                                }
                                if (pb7.a(z).booleanValue()) {
                                    break;
                                }
                            }
                            if (t == null || ee7.a((Object) complication.getComplicationId(), (Object) "empty")) {
                                this.$filteredComplicationByCategories.add(complication);
                            }
                        }
                    }
                    this.this$0.a.h.a(this.$filteredComplicationByCategories);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public f(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<Complication>> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = n06.s;
            local.d(k, "transform from category to list complication with category=" + str);
            a06 g = n06.g(this.a);
            ee7.a((Object) str, "category");
            List<Complication> a2 = g.a(str);
            ArrayList arrayList = new ArrayList();
            DianaPreset a3 = n06.g(this.a).a().a();
            Complication a4 = n06.g(this.a).e().a();
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, a3, a2, a4 != null ? a4.getComplicationId() : null, arrayList, null), 3, null);
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<v87<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore
        public g(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<String, Boolean, ? extends Parcelable> v87) {
            if (v87 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String k = n06.s;
                local.d(k, "onLiveDataChanged setting of " + v87.getFirst() + " isSettingRequired " + v87.getSecond().booleanValue() + ' ');
                String str = "";
                if (v87.getSecond().booleanValue()) {
                    if (((Parcelable) v87.getThird()) == null) {
                        str = wd5.b.a(v87.getFirst());
                    }
                    this.a.q.a(true, v87.getFirst(), str, (Parcelable) v87.getThird());
                    return;
                }
                this.a.q.a(false, v87.getFirst(), str, (Parcelable) null);
                Complication a2 = n06.g(this.a).e().a();
                if (a2 != null) {
                    j06 k2 = this.a.q;
                    String a3 = ig5.a(PortfolioApp.g0.c(), a2.getDescriptionKey(), a2.getDescription());
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    k2.z(a3);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1", f = "ComplicationsPresenter.kt", l = {171}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ n06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$start$1$allCategory$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.r.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(n06 n06, fb7 fb7) {
            super(2, fb7);
            this.this$0 = n06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) obj) {
                if (!n06.g(this.this$0).a(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.k.clear();
            this.this$0.k.addAll(arrayList);
            this.this$0.q.a((List<Category>) this.this$0.k);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<Complication> {
        @DexIgnore
        public /* final */ /* synthetic */ n06 a;

        @DexIgnore
        public i(n06 n06) {
            this.a = n06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Complication complication) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = n06.s;
            local.d(k, "onLiveDataChanged selectedComplication value=" + complication);
            this.a.f.a(complication);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = n06.class.getSimpleName();
        ee7.a((Object) simpleName, "ComplicationsPresenter::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public n06(j06 j06, CategoryRepository categoryRepository) {
        ee7.b(j06, "mView");
        ee7.b(categoryRepository, "mCategoryRepository");
        this.q = j06;
        this.r = categoryRepository;
        LiveData<String> b2 = ge.b(this.f, new d(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026omplicationLiveData\n    }");
        this.l = b2;
        this.m = new c(this);
        LiveData<List<Complication>> b3 = ge.b(this.l, new f(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.n = b3;
        this.o = new e(this);
        this.p = new g(this);
    }

    @DexIgnore
    public static final /* synthetic */ a06 g(n06 n06) {
        a06 a06 = n06.e;
        if (a06 != null) {
            return a06;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void b(String str) {
        T t;
        ee7.b(str, "newComplicationId");
        a06 a06 = this.e;
        if (a06 != null) {
            Complication b2 = a06.b(str);
            FLogger.INSTANCE.getLocal().d(s, "onUserChooseComplication " + b2);
            if (b2 != null) {
                a06 a062 = this.e;
                if (a062 != null) {
                    DianaPreset a2 = a062.a().a();
                    if (a2 != null) {
                        DianaPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        a06 a063 = this.e;
                        if (a063 != null) {
                            String a3 = a063.f().a();
                            if (a3 != null) {
                                ee7.a((Object) a3, "mDianaCustomizeViewModel\u2026ComplicationPos().value!!");
                                String str2 = a3;
                                a06 a064 = this.e;
                                if (a064 == null) {
                                    ee7.d("mDianaCustomizeViewModel");
                                    throw null;
                                } else if (!a064.h(str)) {
                                    Iterator<DianaPresetComplicationSetting> it = clone.getComplications().iterator();
                                    while (it.hasNext()) {
                                        DianaPresetComplicationSetting next = it.next();
                                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                                            arrayList.add(new DianaPresetComplicationSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getComplications().clear();
                                    clone.getComplications().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(s, "Update current preset=" + clone);
                                    a06 a065 = this.e;
                                    if (a065 != null) {
                                        a065.a(clone);
                                    } else {
                                        ee7.d("mDianaCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = a2.getComplications().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t = null;
                                            break;
                                        }
                                        t = it2.next();
                                        if (ee7.a((Object) t.getId(), (Object) str)) {
                                            break;
                                        }
                                    }
                                    T t2 = t;
                                    if (t2 != null) {
                                        a06 a066 = this.e;
                                        if (a066 != null) {
                                            a066.j(t2.getPosition());
                                        } else {
                                            ee7.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.d("mDianaCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final ik7 c(String str) {
        return xh7.b(e(), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(s, "onStart");
        this.l.a(this.m);
        this.n.a(this.o);
        this.i.a(this.p);
        if (this.k.isEmpty()) {
            ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
        } else {
            this.q.a((List<Category>) this.k);
        }
        a06 a06 = this.e;
        if (a06 != null) {
            LiveData<Complication> e2 = a06.e();
            j06 j06 = this.q;
            if (j06 != null) {
                e2.a((k06) j06, new i(this));
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        a06 a06 = this.e;
        if (a06 != null) {
            LiveData<Complication> e2 = a06.e();
            j06 j06 = this.q;
            if (j06 != null) {
                e2.a((k06) j06);
                this.h.b(this.o);
                this.g.b(this.m);
                this.i.b(this.p);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void h() {
        T t;
        String str;
        T t2;
        String str2;
        T t3;
        String str3;
        String id;
        a06 a06 = this.e;
        T t4 = null;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            String str4 = "empty";
            if (a2 != null) {
                Iterator<T> it = a2.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                T t5 = t;
                if (t5 == null || (str = t5.getId()) == null) {
                    str = str4;
                }
                Iterator<T> it2 = a2.getComplications().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    if (ee7.a((Object) t2.getPosition(), (Object) "bottom")) {
                        break;
                    }
                }
                T t6 = t2;
                if (t6 == null || (str2 = t6.getId()) == null) {
                    str2 = str4;
                }
                Iterator<T> it3 = a2.getComplications().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    t3 = it3.next();
                    if (ee7.a((Object) t3.getPosition(), (Object) "right")) {
                        break;
                    }
                }
                T t7 = t3;
                if (t7 == null || (str3 = t7.getId()) == null) {
                    str3 = str4;
                }
                Iterator<T> it4 = a2.getComplications().iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    }
                    T next = it4.next();
                    if (ee7.a((Object) next.getPosition(), (Object) ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        t4 = next;
                        break;
                    }
                }
                T t8 = t4;
                if (!(t8 == null || (id = t8.getId()) == null)) {
                    str4 = id;
                }
                this.q.a(str, str2, str3, str4);
                return;
            }
            this.q.a(str4, str4, str4, str4);
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    @Override // com.fossil.i06
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r6 = this;
            com.fossil.a06 r0 = r6.e
            java.lang.String r1 = "mDianaCustomizeViewModel"
            r2 = 0
            if (r0 == 0) goto L_0x008a
            androidx.lifecycle.LiveData r0 = r0.e()
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.diana.Complication r0 = (com.portfolio.platform.data.model.diana.Complication) r0
            if (r0 == 0) goto L_0x0089
            com.fossil.a06 r3 = r6.e
            if (r3 == 0) goto L_0x0085
            androidx.lifecycle.MutableLiveData r1 = r3.a()
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r1 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r1
            if (r1 == 0) goto L_0x0054
            java.util.ArrayList r1 = r1.getComplications()
            if (r1 == 0) goto L_0x0054
            java.util.Iterator r1 = r1.iterator()
        L_0x002d:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x0049
            java.lang.Object r3 = r1.next()
            r4 = r3
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r4 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r4
            java.lang.String r5 = r0.getComplicationId()
            java.lang.String r4 = r4.getId()
            boolean r4 = com.fossil.ee7.a(r5, r4)
            if (r4 == 0) goto L_0x002d
            r2 = r3
        L_0x0049:
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r2
            if (r2 == 0) goto L_0x0054
            java.lang.String r1 = r2.getSettings()
            if (r1 == 0) goto L_0x0054
            goto L_0x0056
        L_0x0054:
            java.lang.String r1 = ""
        L_0x0056:
            java.lang.String r0 = r0.getComplicationId()
            int r2 = r0.hashCode()
            r3 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
            if (r2 == r3) goto L_0x0077
            r3 = 134170930(0x7ff4932, float:3.8411156E-34)
            if (r2 == r3) goto L_0x0069
            goto L_0x0089
        L_0x0069:
            java.lang.String r2 = "second-timezone"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0089
            com.fossil.j06 r0 = r6.q
            r0.e(r1)
            goto L_0x0089
        L_0x0077:
            java.lang.String r2 = "commute-time"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0089
            com.fossil.j06 r0 = r6.q
            r0.c(r1)
            goto L_0x0089
        L_0x0085:
            com.fossil.ee7.d(r1)
            throw r2
        L_0x0089:
            return
        L_0x008a:
            com.fossil.ee7.d(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n06.i():void");
    }

    @DexIgnore
    public void j() {
        this.q.a(this);
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void a(a06 a06) {
        ee7.b(a06, "viewModel");
        this.e = a06;
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void a(String str) {
        ee7.b(str, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "category change " + str);
        this.g.a(str);
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void a(String str, Parcelable parcelable) {
        T t;
        ee7.b(str, "complicationId");
        ee7.b(parcelable, MicroAppSetting.SETTING);
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it = clone.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getId(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    t2.setSettings(this.j.a(parcelable));
                }
                FLogger.INSTANCE.getLocal().d(s, "update current preset with new setting " + parcelable + " of " + str);
                a06 a062 = this.e;
                if (a062 != null) {
                    a062.a(clone);
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.i06
    public void a(boolean z) {
        T t;
        ComplicationActivitySetting complicationActivitySetting;
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                a06 a062 = this.e;
                if (a062 != null) {
                    Complication a3 = a062.e().a();
                    if (a3 != null) {
                        String component1 = a3.component1();
                        Iterator<T> it = clone.getComplications().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            if (ee7.a((Object) t.getId(), (Object) component1)) {
                                break;
                            }
                        }
                        T t2 = t;
                        if (t2 != null && (complicationActivitySetting = (ComplicationActivitySetting) this.j.a(t2.getSettings(), ComplicationActivitySetting.class)) != null) {
                            complicationActivitySetting.setRingEnabled(z);
                            t2.setSettings(this.j.a(complicationActivitySetting));
                            a06 a063 = this.e;
                            if (a063 != null) {
                                a063.a(clone);
                            } else {
                                ee7.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        }
                    }
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
