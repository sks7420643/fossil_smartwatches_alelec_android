package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q43 extends IInterface {
    @DexIgnore
    void beginAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException;

    @DexIgnore
    void endAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void generateEventId(r43 r43) throws RemoteException;

    @DexIgnore
    void getAppInstanceId(r43 r43) throws RemoteException;

    @DexIgnore
    void getCachedAppInstanceId(r43 r43) throws RemoteException;

    @DexIgnore
    void getConditionalUserProperties(String str, String str2, r43 r43) throws RemoteException;

    @DexIgnore
    void getCurrentScreenClass(r43 r43) throws RemoteException;

    @DexIgnore
    void getCurrentScreenName(r43 r43) throws RemoteException;

    @DexIgnore
    void getGmpAppId(r43 r43) throws RemoteException;

    @DexIgnore
    void getMaxUserProperties(String str, r43 r43) throws RemoteException;

    @DexIgnore
    void getTestFlag(r43 r43, int i) throws RemoteException;

    @DexIgnore
    void getUserProperties(String str, String str2, boolean z, r43 r43) throws RemoteException;

    @DexIgnore
    void initForTests(Map map) throws RemoteException;

    @DexIgnore
    void initialize(ab2 ab2, qn2 qn2, long j) throws RemoteException;

    @DexIgnore
    void isDataCollectionEnabled(r43 r43) throws RemoteException;

    @DexIgnore
    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException;

    @DexIgnore
    void logEventAndBundle(String str, String str2, Bundle bundle, r43 r43, long j) throws RemoteException;

    @DexIgnore
    void logHealthData(int i, String str, ab2 ab2, ab2 ab22, ab2 ab23) throws RemoteException;

    @DexIgnore
    void onActivityCreated(ab2 ab2, Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void onActivityDestroyed(ab2 ab2, long j) throws RemoteException;

    @DexIgnore
    void onActivityPaused(ab2 ab2, long j) throws RemoteException;

    @DexIgnore
    void onActivityResumed(ab2 ab2, long j) throws RemoteException;

    @DexIgnore
    void onActivitySaveInstanceState(ab2 ab2, r43 r43, long j) throws RemoteException;

    @DexIgnore
    void onActivityStarted(ab2 ab2, long j) throws RemoteException;

    @DexIgnore
    void onActivityStopped(ab2 ab2, long j) throws RemoteException;

    @DexIgnore
    void performAction(Bundle bundle, r43 r43, long j) throws RemoteException;

    @DexIgnore
    void registerOnMeasurementEventListener(nn2 nn2) throws RemoteException;

    @DexIgnore
    void resetAnalyticsData(long j) throws RemoteException;

    @DexIgnore
    void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void setCurrentScreen(ab2 ab2, String str, String str2, long j) throws RemoteException;

    @DexIgnore
    void setDataCollectionEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setDefaultEventParameters(Bundle bundle) throws RemoteException;

    @DexIgnore
    void setEventInterceptor(nn2 nn2) throws RemoteException;

    @DexIgnore
    void setInstanceIdProvider(on2 on2) throws RemoteException;

    @DexIgnore
    void setMeasurementEnabled(boolean z, long j) throws RemoteException;

    @DexIgnore
    void setMinimumSessionDuration(long j) throws RemoteException;

    @DexIgnore
    void setSessionTimeoutDuration(long j) throws RemoteException;

    @DexIgnore
    void setUserId(String str, long j) throws RemoteException;

    @DexIgnore
    void setUserProperty(String str, String str2, ab2 ab2, boolean z, long j) throws RemoteException;

    @DexIgnore
    void unregisterOnMeasurementEventListener(nn2 nn2) throws RemoteException;
}
