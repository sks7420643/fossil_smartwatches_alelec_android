package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class na0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ pa0 a;
    @DexIgnore
    public zg0 b;
    @DexIgnore
    public /* final */ bh0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<na0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public na0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                pa0 valueOf = pa0.valueOf(readString);
                parcel.setDataPosition(0);
                switch (t01.a[valueOf.ordinal()]) {
                    case 1:
                        return ha0.CREATOR.createFromParcel(parcel);
                    case 2:
                        return ra0.CREATOR.createFromParcel(parcel);
                    case 3:
                        return sa0.CREATOR.createFromParcel(parcel);
                    case 4:
                        return ja0.CREATOR.createFromParcel(parcel);
                    case 5:
                        return ka0.CREATOR.createFromParcel(parcel);
                    case 6:
                        return ia0.CREATOR.createFromParcel(parcel);
                    case 7:
                        return la0.CREATOR.createFromParcel(parcel);
                    case 8:
                        return ea0.CREATOR.createFromParcel(parcel);
                    case 9:
                        return ma0.CREATOR.createFromParcel(parcel);
                    case 10:
                        return qa0.CREATOR.createFromParcel(parcel);
                    case 11:
                        return ga0.CREATOR.createFromParcel(parcel);
                    case 12:
                        return fa0.CREATOR.createFromParcel(parcel);
                    default:
                        throw new p87();
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public na0[] newArray(int i) {
            return new na0[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ na0(pa0 pa0, zg0 zg0, bh0 bh0, int i) {
        this(pa0, (i & 2) != 0 ? zg0.TOP_SHORT_PRESS_RELEASE : zg0, (i & 4) != 0 ? new lt0() : bh0);
    }

    @DexIgnore
    public final void a(zg0 zg0) {
        this.b = zg0;
    }

    @DexIgnore
    public JSONObject b() {
        return new JSONObject();
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            na0 na0 = (na0) obj;
            return this.a == na0.a && this.b == na0.b && !(ee7.a(this.c, na0.c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchApp");
    }

    @DexIgnore
    public final zg0 getButtonEvent() {
        return this.b;
    }

    @DexIgnore
    public final pa0 getId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (this.a.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.a.a()).put("button_evt", yz0.a(this.b));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public na0(pa0 pa0, zg0 zg0, bh0 bh0) {
        this.a = pa0;
        this.b = zg0;
        this.c = bh0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public na0(android.os.Parcel r5) {
        /*
            r4 = this;
            java.lang.String r0 = r5.readString()
            r1 = 0
            if (r0 == 0) goto L_0x0030
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.ee7.a(r0, r2)
            com.fossil.pa0 r0 = com.fossil.pa0.valueOf(r0)
            com.fossil.zg0[] r2 = com.fossil.zg0.values()
            int r3 = r5.readInt()
            r2 = r2[r3]
            java.lang.Class<com.fossil.bh0> r3 = com.fossil.bh0.class
            java.lang.ClassLoader r3 = r3.getClassLoader()
            android.os.Parcelable r5 = r5.readParcelable(r3)
            if (r5 == 0) goto L_0x002c
            com.fossil.bh0 r5 = (com.fossil.bh0) r5
            r4.<init>(r0, r2, r5)
            return
        L_0x002c:
            com.fossil.ee7.a()
            throw r1
        L_0x0030:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.na0.<init>(android.os.Parcel):void");
    }
}
