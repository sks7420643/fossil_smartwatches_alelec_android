package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.zp5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu5 extends go5 implements bu5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<w35> f;
    @DexIgnore
    public au5 g;
    @DexIgnore
    public zp5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cu5.j;
        }

        @DexIgnore
        public final cu5 b() {
            return new cu5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements zp5.b {
        @DexIgnore
        public /* final */ /* synthetic */ cu5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(cu5 cu5) {
            this.a = cu5;
        }

        @DexIgnore
        @Override // com.fossil.zp5.b
        public void a() {
            cu5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cu5 a;

        @DexIgnore
        public c(cu5 cu5) {
            this.a = cu5;
        }

        @DexIgnore
        public final void onClick(View view) {
            cu5.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ cu5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(cu5 cu5) {
            this.a = cu5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            zp5 a2 = this.a.h;
            if (a2 != null) {
                a2.a(String.valueOf(charSequence));
            }
            this.a.T(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ w35 a;

        @DexIgnore
        public e(w35 w35) {
            this.a = w35;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                w35 w35 = this.a;
                ee7.a((Object) w35, "binding");
                w35.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cu5 a;

        @DexIgnore
        public f(cu5 cu5) {
            this.a = cu5;
        }

        @DexIgnore
        public final void onClick(View view) {
            cu5.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w35 a;

        @DexIgnore
        public g(w35 w35) {
            this.a = w35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.r.setText("");
        }
    }

    /*
    static {
        String simpleName = cu5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ au5 b(cu5 cu5) {
        au5 au5 = cu5.g;
        if (au5 != null) {
            return au5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.bu5
    public void R(boolean z) {
        FlexibleButton flexibleButton;
        int i2;
        FlexibleButton flexibleButton2;
        qw6<w35> qw6 = this.f;
        if (qw6 != null) {
            w35 a2 = qw6.a();
            if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                flexibleButton2.setEnabled(z);
            }
            qw6<w35> qw62 = this.f;
            if (qw62 != null) {
                w35 a3 = qw62.a();
                if (a3 != null && (flexibleButton = a3.q) != null) {
                    if (z) {
                        i2 = v6.a(PortfolioApp.g0.c(), 2131099971);
                    } else {
                        i2 = v6.a(PortfolioApp.g0.c(), 2131099827);
                    }
                    flexibleButton.setBackgroundColor(i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<w35> qw6 = this.f;
        if (qw6 != null) {
            w35 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.t;
                ee7.a((Object) imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.t;
            ee7.a((Object) imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.bu5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        au5 au5 = this.g;
        if (au5 != null) {
            au5.h();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        w35 w35 = (w35) qb.a(layoutInflater, 2131558589, viewGroup, false, a1());
        w35.u.setOnClickListener(new c(this));
        w35.r.addTextChangedListener(new d(this));
        w35.r.setOnFocusChangeListener(new e(w35));
        w35.q.setOnClickListener(new f(this));
        w35.t.setOnClickListener(new g(w35));
        w35.w.setIndexBarVisibility(true);
        w35.w.setIndexBarHighLateTextVisibility(true);
        w35.w.setIndexbarHighLateTextColor(2131099703);
        w35.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        w35.w.setIndexTextSize(9);
        w35.w.setIndexBarTextColor(2131099703);
        Typeface a2 = c7.a(requireContext(), 2131296256);
        String b2 = eh5.l.a().b("primaryText");
        Typeface c2 = eh5.l.a().c("nonBrandTextStyle5");
        if (c2 != null) {
            a2 = c2;
        }
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            w35.w.setIndexBarTextColor(parseColor);
            w35.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (a2 != null) {
            w35.w.setTypeface(a2);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = w35.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext(), 1, false));
        this.f = new qw6<>(this, w35);
        ee7.a((Object) w35, "binding");
        return w35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        au5 au5 = this.g;
        if (au5 != null) {
            au5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        au5 au5 = this.g;
        if (au5 != null) {
            au5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.bu5
    public void s() {
    }

    @DexIgnore
    public void a(au5 au5) {
        ee7.b(au5, "presenter");
        this.g = au5;
    }

    @DexIgnore
    @Override // com.fossil.bu5
    public void a(List<bt5> list, FilterQueryProvider filterQueryProvider) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        ee7.b(list, "listContactWrapper");
        ee7.b(filterQueryProvider, "filterQueryProvider");
        zp5 zp5 = new zp5(null, list);
        this.h = zp5;
        if (zp5 != null) {
            zp5.a(new b(this));
            zp5 zp52 = this.h;
            if (zp52 != null) {
                zp52.a(filterQueryProvider);
                qw6<w35> qw6 = this.f;
                if (qw6 != null) {
                    w35 a2 = qw6.a();
                    if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.h);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.bu5
    public void a(Cursor cursor) {
        zp5 zp5 = this.h;
        if (zp5 != null) {
            zp5.c(cursor);
        }
    }
}
