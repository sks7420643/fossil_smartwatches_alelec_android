package com.fossil;

import android.os.IBinder;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p42 implements IBinder.DeathRecipient, r42 {
    @DexIgnore
    public /* final */ WeakReference<BasePendingResult<?>> a;
    @DexIgnore
    public /* final */ WeakReference<v52> b;
    @DexIgnore
    public /* final */ WeakReference<IBinder> c;

    @DexIgnore
    public p42(BasePendingResult<?> basePendingResult, v52 v52, IBinder iBinder) {
        this.b = new WeakReference<>(v52);
        this.a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.r42
    public final void a(BasePendingResult<?> basePendingResult) {
        a();
    }

    @DexIgnore
    public final void binderDied() {
        a();
    }

    @DexIgnore
    public final void a() {
        BasePendingResult<?> basePendingResult = this.a.get();
        v52 v52 = this.b.get();
        if (!(v52 == null || basePendingResult == null)) {
            v52.a(basePendingResult.e().intValue());
        }
        IBinder iBinder = this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    @DexIgnore
    public /* synthetic */ p42(BasePendingResult basePendingResult, v52 v52, IBinder iBinder, q42 q42) {
        this(basePendingResult, null, iBinder);
    }
}
