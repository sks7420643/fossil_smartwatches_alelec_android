package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class if5 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public qd5 c;
    @DexIgnore
    public Map<String, String> d; // = new HashMap();
    @DexIgnore
    public Map<String, Object> e; // = new HashMap();
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public b h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void E0();

        @DexIgnore
        void a0();
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public if5(qd5 qd5, String str, String str2) {
        ee7.b(qd5, "analyticsHelper");
        ee7.b(str, "traceName");
        this.c = qd5;
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (str2 != null) {
            valueOf = str2 + LocaleConverter.LOCALE_DELIMITER + valueOf;
        }
        this.a = valueOf;
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    public final void c() {
        this.h = null;
    }

    @DexIgnore
    public final void d() {
        if (this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is already started");
            return;
        }
        this.f = true;
        Map<String, String> map = this.d;
        if (map != null) {
            map.put("trace_id", this.a);
        }
        qd5 qd5 = this.c;
        if (qd5 != null) {
            qd5.a(this.b + "_start", this.d);
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
        this.g = System.currentTimeMillis();
        b bVar = this.h;
        if (bVar != null) {
            bVar.a0();
        }
    }

    @DexIgnore
    public String toString() {
        return "Tracer name: " + this.b + ", id: " + this.a + ", running: " + this.f;
    }

    @DexIgnore
    public final if5 a(String str, String str2) {
        ee7.b(str, "paramName");
        ee7.b(str2, "paramValue");
        Map<String, String> map = this.d;
        if (map != null) {
            map.put(str, str2);
        }
        return this;
    }

    @DexIgnore
    public final if5 a(b bVar) {
        ee7.b(bVar, "tracingCallback");
        this.h = bVar;
        return this;
    }

    @DexIgnore
    public final void a(gf5 gf5) {
        ee7.b(gf5, "analyticsEvent");
        gf5.a("trace_id", this.a);
        gf5.a();
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put(Constants.RESULT, str);
                qd5 qd5 = this.c;
                if (qd5 != null) {
                    qd5.a(this.b + "_end", map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.E0();
        }
    }

    @DexIgnore
    public final void a(String str, boolean z, String str2) {
        ee7.b(str, "serial");
        ee7.b(str2, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put("cache_hit", Integer.valueOf(z ? 1 : 0));
                map.put("Serial_Number", str);
                map.put(Constants.RESULT, str2);
                qd5 qd5 = this.c;
                if (qd5 != null) {
                    qd5.a(this.b + "_end", map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.E0();
        }
    }
}
