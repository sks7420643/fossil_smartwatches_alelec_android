package com.fossil;

import android.text.TextUtils;
import com.fossil.fl4;
import com.fossil.rd5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw5 extends fl4<fl4.b, a, fl4.a> {
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.d {
        @DexIgnore
        public /* final */ List<at5> a;

        @DexIgnore
        public a(List<at5> list) {
            ee7.b(list, "apps");
            jw3.a(list, "apps cannot be null!", new Object[0]);
            ee7.a((Object) list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.a = list;
        }

        @DexIgnore
        public final List<at5> a() {
            return this.a;
        }
    }

    @DexIgnore
    public nw5() {
        String simpleName = nw5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public Object a(fl4.b bVar, fb7<Object> fb7) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List<AppFilter> allAppFilters = ah5.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<rd5.b> it = rd5.g.a().iterator();
        while (it.hasNext()) {
            rd5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !mh7.b(next.b(), PortfolioApp.g0.c().getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), pb7.a(false));
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    ee7.a((Object) next2, "appFilter");
                    if (ee7.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                at5 at5 = new at5();
                at5.setInstalledApp(installedApp);
                at5.setUri(next.c());
                at5.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(at5);
            }
        }
        aa7.c(linkedList);
        return new a(linkedList);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return this.d;
    }
}
