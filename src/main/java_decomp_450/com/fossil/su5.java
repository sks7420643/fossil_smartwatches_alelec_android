package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ju5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su5 extends mu5 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public ju5 f;
    @DexIgnore
    public /* final */ nu5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<ju5.a> {
        @DexIgnore
        public /* final */ /* synthetic */ su5 a;

        @DexIgnore
        public b(su5 su5) {
            this.a = su5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ju5.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String i = su5.h;
            local.d(i, "NotificationSettingChanged value = " + aVar);
            this.a.e = aVar.b();
            this.a.g.i(aVar.a());
        }
    }

    /*
    static {
        new a(null);
        String simpleName = su5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public su5(nu5 nu5) {
        ee7.b(nu5, "mView");
        this.g = nu5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "start: isCall = " + this.e);
        ju5 ju5 = this.f;
        if (ju5 != null) {
            MutableLiveData<ju5.a> a2 = ju5.a();
            nu5 nu5 = this.g;
            if (nu5 != null) {
                a2.a((ou5) nu5, new b(this));
                this.g.a(a(this.e));
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        ee7.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
        ju5 ju5 = this.f;
        if (ju5 != null) {
            MutableLiveData<ju5.a> a2 = ju5.a();
            nu5 nu5 = this.g;
            if (nu5 != null) {
                a2.a((ou5) nu5);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        ee7.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // com.fossil.mu5
    public void a(ju5 ju5) {
        ee7.b(ju5, "viewModel");
        this.f = ju5;
    }

    @DexIgnore
    public final String a(boolean z) {
        if (z) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886094);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return a2;
        }
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886095);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return a3;
    }

    @DexIgnore
    @Override // com.fossil.mu5
    public void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i);
        ju5.a aVar = new ju5.a(i, this.e);
        ju5 ju5 = this.f;
        if (ju5 != null) {
            ju5.a().a(aVar);
        } else {
            ee7.d("mNotificationSettingViewModel");
            throw null;
        }
    }
}
