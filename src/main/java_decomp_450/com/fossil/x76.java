package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x76 implements Factory<f86> {
    @DexIgnore
    public static f86 a(u76 u76) {
        f86 c = u76.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
