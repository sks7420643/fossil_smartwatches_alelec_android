package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d37 extends u27 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    @Override // com.fossil.u27
    public boolean a() {
        String str;
        String str2 = this.c;
        if (str2 == null || str2.length() == 0 || this.c.length() > 1024) {
            str = "checkArgs fail, scope is invalid";
        } else {
            String str3 = this.d;
            if (str3 == null || str3.length() <= 1024) {
                return true;
            }
            str = "checkArgs fail, state is invalid";
        }
        p27.a("MicroMsg.SDK.SendAuth.Req", str);
        return false;
    }

    @DexIgnore
    @Override // com.fossil.u27
    public int b() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.u27
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putString("_wxapi_sendauth_req_scope", this.c);
        bundle.putString("_wxapi_sendauth_req_state", this.d);
    }
}
