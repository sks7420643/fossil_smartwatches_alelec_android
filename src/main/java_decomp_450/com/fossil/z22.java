package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z22 extends kn3 {
    @DexIgnore
    public /* final */ WeakReference<q22> a;

    @DexIgnore
    public z22(q22 q22) {
        this.a = new WeakReference<>(q22);
    }

    @DexIgnore
    @Override // com.fossil.jn3
    public final void a(tn3 tn3) {
        q22 q22 = this.a.get();
        if (q22 != null) {
            q22.a.a(new y22(this, q22, q22, tn3));
        }
    }
}
