package com.fossil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj7 extends wj7 implements Runnable {
    @DexIgnore
    public static volatile Thread _thread;
    @DexIgnore
    public static volatile int debugStatus;
    @DexIgnore
    public static /* final */ long g;
    @DexIgnore
    public static /* final */ fj7 h;

    /*
    static {
        Long l;
        fj7 fj7 = new fj7();
        h = fj7;
        vj7.b(fj7, false, 1, null);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        g = timeUnit.toNanos(l.longValue());
    }
    */

    @DexIgnore
    public final synchronized void C() {
        if (E()) {
            debugStatus = 3;
            B();
            notifyAll();
        }
    }

    @DexIgnore
    public final synchronized Thread D() {
        Thread thread;
        thread = _thread;
        if (thread == null) {
            thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
            _thread = thread;
            thread.setDaemon(true);
            thread.start();
        }
        return thread;
    }

    @DexIgnore
    public final boolean E() {
        int i = debugStatus;
        return i == 2 || i == 3;
    }

    @DexIgnore
    public final synchronized boolean F() {
        if (E()) {
            return false;
        }
        debugStatus = 1;
        notifyAll();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.wj7, com.fossil.jj7
    public rj7 a(long j, Runnable runnable) {
        return b(j, runnable);
    }

    @DexIgnore
    @Override // com.fossil.xj7
    public Thread r() {
        Thread thread = _thread;
        return thread != null ? thread : D();
    }

    @DexIgnore
    public void run() {
        fl7.b.a(this);
        gl7 a = hl7.a();
        if (a != null) {
            a.b();
        }
        try {
            if (F()) {
                long j = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long n = n();
                    if (n == Long.MAX_VALUE) {
                        int i = (j > Long.MAX_VALUE ? 1 : (j == Long.MAX_VALUE ? 0 : -1));
                        if (i == 0) {
                            gl7 a2 = hl7.a();
                            long a3 = a2 != null ? a2.a() : System.nanoTime();
                            if (i == 0) {
                                j = g + a3;
                            }
                            long j2 = j - a3;
                            if (j2 <= 0) {
                                _thread = null;
                                C();
                                gl7 a4 = hl7.a();
                                if (a4 != null) {
                                    a4.d();
                                }
                                if (!z()) {
                                    r();
                                    return;
                                }
                                return;
                            }
                            n = qf7.b(n, j2);
                        } else {
                            n = qf7.b(n, g);
                        }
                    }
                    if (n > 0) {
                        if (E()) {
                            _thread = null;
                            C();
                            gl7 a5 = hl7.a();
                            if (a5 != null) {
                                a5.d();
                            }
                            if (!z()) {
                                r();
                                return;
                            }
                            return;
                        }
                        gl7 a6 = hl7.a();
                        if (a6 != null) {
                            a6.a(this, n);
                        } else {
                            LockSupport.parkNanos(this, n);
                        }
                    }
                }
            }
        } finally {
            _thread = null;
            C();
            gl7 a7 = hl7.a();
            if (a7 != null) {
                a7.d();
            }
            if (!z()) {
                r();
            }
        }
    }
}
