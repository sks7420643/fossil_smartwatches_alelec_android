package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv2 {
    @DexIgnore
    public static /* final */ pv2<?> a; // = new ov2();
    @DexIgnore
    public static /* final */ pv2<?> b; // = c();

    @DexIgnore
    public static pv2<?> a() {
        return a;
    }

    @DexIgnore
    public static pv2<?> b() {
        pv2<?> pv2 = b;
        if (pv2 != null) {
            return pv2;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    @DexIgnore
    public static pv2<?> c() {
        try {
            return (pv2) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
