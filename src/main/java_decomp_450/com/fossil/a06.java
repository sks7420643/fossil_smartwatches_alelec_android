package com.fossil;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a06 extends he {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public static /* final */ a H; // = new a(null);
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository A;
    @DexIgnore
    public /* final */ WatchAppRepository B;
    @DexIgnore
    public /* final */ RingStyleRepository C;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository D;
    @DexIgnore
    public /* final */ WatchFaceRepository E;
    @DexIgnore
    public /* final */ ch5 F;
    @DexIgnore
    public DianaPreset a;
    @DexIgnore
    public MutableLiveData<DianaPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Complication> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<WatchApp> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<WatchFaceWrapper> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Complication> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> m; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<List<WatchFace>> n;
    @DexIgnore
    public List<WatchFace> o;
    @DexIgnore
    public DianaComplicationRingStyle p;
    @DexIgnore
    public /* final */ Gson q; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> r;
    @DexIgnore
    public /* final */ LiveData<String> s;
    @DexIgnore
    public /* final */ LiveData<Complication> t;
    @DexIgnore
    public /* final */ LiveData<WatchApp> u;
    @DexIgnore
    public /* final */ LiveData<Boolean> v;
    @DexIgnore
    public /* final */ zd<DianaPreset> w;
    @DexIgnore
    public /* final */ zd<List<WatchFace>> x;
    @DexIgnore
    public /* final */ DianaPresetRepository y;
    @DexIgnore
    public /* final */ ComplicationRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return a06.G;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public b(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v5, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX DEBUG: Multi-variable search result rejected for r11v6, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(DianaPreset dianaPreset) {
            T t;
            T t2;
            boolean z;
            boolean z2;
            FLogger.INSTANCE.getLocal().d(a06.H.a(), "current preset change=" + dianaPreset);
            if (dianaPreset != null) {
                String str = (String) this.a.g.a();
                String str2 = (String) this.a.h.a();
                FLogger.INSTANCE.getLocal().d(a06.H.a(), "selectedComplicationPos=" + str + " selectedComplicationId=" + str2);
                this.a.m.a((Object) dianaPreset.getWatchFaceId());
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    t = null;
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    T t3 = t2;
                    if (!ee7.a((Object) t3.getPosition(), (Object) str) || mh7.b(t3.getId(), str2, true)) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                T t4 = t2;
                if (t4 != null) {
                    FLogger.INSTANCE.getLocal().d(a06.H.a(), "Update new complication id=" + t4.getId() + " at position=" + str);
                    this.a.h.a((Object) t4.getId());
                }
                String str3 = (String) this.a.j.a();
                String str4 = (String) this.a.k.a();
                Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    T t5 = next;
                    if (!ee7.a((Object) t5.getPosition(), (Object) str3) || mh7.b(t5.getId(), str4, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t6 = t;
                if (t6 != null) {
                    FLogger.INSTANCE.getLocal().d(a06.H.a(), "Update new watchapp id=" + t6.getId() + " at position=" + str3);
                    this.a.k.a((Object) t6.getId());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1", f = "DianaCustomizeViewModel.kt", l = {155, 160}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1$1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.b.a(this.this$0.this$0.w);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(a06 a06, String str, String str2, String str3, fb7 fb7) {
            super(2, fb7);
            this.this$0 = a06;
            this.$presetId = str;
            this.$watchAppPos = str2;
            this.$complicationPos = str3;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$watchAppPos, this.$complicationPos, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = a06.H.a();
                local.d(a3, "init presetId=" + this.$presetId + " originalPreset=" + this.this$0.a + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos);
                if (this.this$0.a == null) {
                    a06 a06 = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (a06.a(str, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.a((Object) this.$complicationPos);
            this.this$0.j.a((Object) this.$watchAppPos);
            tk7 c = qj7.c();
            a aVar = new a(this, null);
            this.L$0 = yi7;
            this.label = 2;
            if (vh7.a(c, aVar, this) == a2) {
                return a2;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initFromSaveInstanceState$1", f = "DianaCustomizeViewModel.kt", l = {171, 174, 182}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $savedOriginalPreset;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initFromSaveInstanceState$1$1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.b.a(this.this$0.this$0.w);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a06 a06, String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3, fb7 fb7) {
            super(2, fb7);
            this.this$0 = a06;
            this.$presetId = str;
            this.$savedOriginalPreset = dianaPreset;
            this.$savedCurrentPreset = dianaPreset2;
            this.$watchAppPos = str2;
            this.$complicationPos = str3;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$watchAppPos, this.$complicationPos, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ed A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00f2  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 0
                r3 = 3
                r4 = 2
                r5 = 1
                if (r1 == 0) goto L_0x0035
                if (r1 == r5) goto L_0x002c
                if (r1 == r4) goto L_0x0023
                if (r1 != r3) goto L_0x001b
                java.lang.Object r0 = r9.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r10)
                goto L_0x00ee
            L_0x001b:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x0023:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x00bd
            L_0x002c:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x00c4
            L_0x0035:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r1 = r9.p$
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                com.fossil.a06$a r6 = com.fossil.a06.H
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "initFromSaveInstanceState presetId "
                r7.append(r8)
                java.lang.String r8 = r9.$presetId
                r7.append(r8)
                java.lang.String r8 = " savedOriginalPreset="
                r7.append(r8)
                com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r9.$savedOriginalPreset
                r7.append(r8)
                r8 = 44
                r7.append(r8)
                java.lang.String r8 = " savedCurrentPreset="
                r7.append(r8)
                com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r9.$savedCurrentPreset
                r7.append(r8)
                java.lang.String r8 = " watchAppPos="
                r7.append(r8)
                java.lang.String r8 = r9.$watchAppPos
                r7.append(r8)
                java.lang.String r8 = " complicationPos="
                r7.append(r8)
                java.lang.String r8 = r9.$complicationPos
                r7.append(r8)
                java.lang.String r8 = " currentPreset"
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                r10.d(r6, r7)
                com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = r9.$savedOriginalPreset
                if (r10 != 0) goto L_0x00a1
                com.fossil.a06 r10 = r9.this$0
                java.lang.String r4 = r9.$presetId
                r9.L$0 = r1
                r9.label = r5
                java.lang.Object r10 = r10.a(r4, r9)
                if (r10 != r0) goto L_0x00c4
                return r0
            L_0x00a1:
                com.fossil.a06 r10 = r9.this$0
                r10.k()
                com.fossil.a06 r10 = r9.this$0
                com.portfolio.platform.data.model.diana.preset.DianaPreset r5 = r9.$savedCurrentPreset
                if (r5 == 0) goto L_0x00b1
                com.portfolio.platform.data.model.diana.preset.DianaPreset r5 = r5.clone()
                goto L_0x00b2
            L_0x00b1:
                r5 = r2
            L_0x00b2:
                r9.L$0 = r1
                r9.label = r4
                java.lang.Object r10 = r10.a(r5, r9)
                if (r10 != r0) goto L_0x00bd
                return r0
            L_0x00bd:
                com.fossil.a06 r10 = r9.this$0
                com.portfolio.platform.data.model.diana.preset.DianaPreset r4 = r9.$savedOriginalPreset
                r10.a = r4
            L_0x00c4:
                com.fossil.a06 r10 = r9.this$0
                androidx.lifecycle.MutableLiveData r10 = r10.g
                java.lang.String r4 = r9.$complicationPos
                r10.a(r4)
                com.fossil.a06 r10 = r9.this$0
                androidx.lifecycle.MutableLiveData r10 = r10.j
                java.lang.String r4 = r9.$watchAppPos
                r10.a(r4)
                com.fossil.tk7 r10 = com.fossil.qj7.c()
                com.fossil.a06$d$a r4 = new com.fossil.a06$d$a
                r4.<init>(r9, r2)
                r9.L$0 = r1
                r9.label = r3
                java.lang.Object r10 = com.fossil.vh7.a(r10, r4, r9)
                if (r10 != r0) goto L_0x00ee
                return r0
            L_0x00ee:
                com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = r9.$savedCurrentPreset
                if (r10 == 0) goto L_0x00fd
                com.fossil.a06 r10 = r9.this$0
                androidx.lifecycle.MutableLiveData r10 = r10.b
                com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = r9.$savedCurrentPreset
                r10.a(r0)
            L_0x00fd:
                com.fossil.i97 r10 = com.fossil.i97.a
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.a06.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a06 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(fb7 fb7, a06 a06, DianaPreset dianaPreset, fb7 fb72) {
            super(2, fb7);
            this.this$0 = a06;
            this.$currentPreset$inlined = dianaPreset;
            this.$continuation$inlined = fb72;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(fb7, this.this$0, this.$currentPreset$inlined, this.$continuation$inlined);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                LiveData m = this.this$0.n;
                if (m == null) {
                    return null;
                }
                m.a(this.this$0.x);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel", f = "DianaCustomizeViewModel.kt", l = {451}, m = "initializeCustomizeTheme")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a06 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(a06 a06, fb7 fb7) {
            super(fb7);
            this.this$0 = a06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((DianaPreset) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel", f = "DianaCustomizeViewModel.kt", l = {414}, m = "initializePreset")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a06 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(a06 a06, fb7 fb7) {
            super(fb7);
            this.this$0 = a06;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public h(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Complication> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a06.H.a();
            local.d(a2, "transformComplicationIdToModel complicationId=" + str);
            a06 a06 = this.a;
            ee7.a((Object) str, "id");
            this.a.i.a(a06.b(str));
            return this.a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public i(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a06.H.a();
            local.d(a2, "transformComplicationPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.b.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    this.a.h.b(t2.getId());
                }
            }
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public j(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<WatchApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a06.H.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            a06 a06 = this.a;
            ee7.a((Object) str, "id");
            WatchApp d = a06.d(str);
            if (d == null) {
                d = cf5.c.a();
            }
            this.a.l.a(d);
            return this.a.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public k(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a06.H.a();
            local.d(a2, "transformWatchAppPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.b.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    this.a.k.b(t2.getId());
                } else {
                    this.a.k.b("empty_watch_app_id");
                }
            }
            return this.a.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore
        public l(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(DianaPreset dianaPreset) {
            ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
            Gson d = this.a.q;
            DianaPreset e = this.a.a;
            if (e != null) {
                boolean a2 = xc5.a(complications, d, e.getComplications());
                ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
                Gson d2 = this.a.q;
                DianaPreset e2 = this.a.a;
                if (e2 != null) {
                    boolean c = xc5.c(watchapps, d2, e2.getWatchapps());
                    String watchFaceId = dianaPreset.getWatchFaceId();
                    DianaPreset e3 = this.a.a;
                    if (e3 != null) {
                        boolean a3 = ee7.a((Object) watchFaceId, (Object) e3.getWatchFaceId());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a4 = a06.H.a();
                        local.d(a4, "isComplicationSettingListChange " + a2 + " isWatchAppChanged " + c + " isThemeChanged " + a3);
                        if (!a2 || !c || !a3) {
                            this.a.c.a((Object) true);
                        } else {
                            this.a.c.a((Object) false);
                        }
                        return this.a.c;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<List<? extends WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ a06 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1", f = "DianaCustomizeViewModel.kt", l = {517}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a06$m$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1$1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.a06$m$a$a  reason: collision with other inner class name */
            public static final class C0007a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $list;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0007a(a aVar, List list, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$list = list;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0007a aVar = new C0007a(this.this$0, this.$list, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                    return ((C0007a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.a.f.clear();
                        return pb7.a(this.this$0.this$0.a.f.addAll(this.$list));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = mVar;
                this.$it = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    if (!ee7.a(this.this$0.a.o, this.$it)) {
                        this.this$0.a.o = this.$it;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = a06.H.a();
                        local.d(a2, "watchFaceObserver onChanged caching " + this.this$0.a.o + " updatedWatchFace " + this.$it);
                        List list = this.$it;
                        ee7.a((Object) list, "it");
                        DianaPreset dianaPreset = (DianaPreset) this.this$0.a.b.a();
                        List<WatchFaceWrapper> a3 = yc5.a(list, dianaPreset != null ? dianaPreset.getComplications() : null);
                        tk7 c = qj7.c();
                        C0007a aVar = new C0007a(this, a3, null);
                        this.L$0 = yi7;
                        this.L$1 = a3;
                        this.label = 1;
                        if (vh7.a(c, aVar, this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    List list2 = (List) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public m(a06 a06) {
            this.a = a06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchFace> list) {
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(this, list, null), 3, null);
        }
    }

    /*
    static {
        String simpleName = a06.class.getSimpleName();
        ee7.a((Object) simpleName, "DianaCustomizeViewModel::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    public a06(DianaPresetRepository dianaPresetRepository, ComplicationRepository complicationRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppRepository watchAppRepository, RingStyleRepository ringStyleRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository, ch5 ch5) {
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(complicationLastSettingRepository, "mLastComplicationSettingRepository");
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(ringStyleRepository, "mRingStyleRepository");
        ee7.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        this.y = dianaPresetRepository;
        this.z = complicationRepository;
        this.A = complicationLastSettingRepository;
        this.B = watchAppRepository;
        this.C = ringStyleRepository;
        this.D = watchAppLastSettingRepository;
        this.E = watchFaceRepository;
        this.F = ch5;
        LiveData<String> b2 = ge.b(this.g, new i(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026plicationIdLiveData\n    }");
        this.r = b2;
        LiveData<String> b3 = ge.b(this.j, new k(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026dWatchAppIdLiveData\n    }");
        this.s = b3;
        LiveData<Complication> b4 = ge.b(this.r, new h(this));
        ee7.a((Object) b4, "Transformations.switchMa\u2026omplicationLiveData\n    }");
        this.t = b4;
        LiveData<WatchApp> b5 = ge.b(this.s, new j(this));
        ee7.a((Object) b5, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.u = b5;
        LiveData<Boolean> b6 = ge.b(this.b, new l(this));
        ee7.a((Object) b6, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.v = b6;
        this.w = new b(this);
        this.x = new m(this);
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = G;
        local.d(str, " same =" + ee7.a(this.a, this.b.a()) + "onCleared originalPreset=" + this.a + " currentPreset=" + this.b.a());
        this.b.b(this.w);
        LiveData<List<WatchFace>> liveData = this.n;
        if (liveData != null) {
            liveData.b(this.x);
        }
        super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        LiveData<Boolean> liveData = this.v;
        if (liveData != null) {
            return liveData;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final DianaComplicationRingStyle c() {
        return this.p;
    }

    @DexIgnore
    public final DianaPreset d() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Complication> e() {
        return this.t;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v3, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v11, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX WARN: Multi-variable type inference failed */
    public final Parcelable f(String str) {
        CommuteTimeSetting commuteTimeSetting;
        T t2;
        T t3;
        String settings;
        ee7.b(str, "complicationId");
        Gson gson = new Gson();
        if (!wd5.b.c(str)) {
            return null;
        }
        DianaPreset a2 = a().a();
        String str2 = "";
        if (a2 != null) {
            Iterator<T> it = a2.getComplications().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                t3 = it.next();
                if (ee7.a((Object) t3.getId(), (Object) str)) {
                    break;
                }
            }
            T t4 = t3;
            if (!(t4 == null || (settings = t4.getSettings()) == null)) {
                str2 = settings;
            }
        }
        if (str2.length() == 0) {
            ComplicationLastSetting complicationLastSetting = this.A.getComplicationLastSetting(str);
            if (complicationLastSetting != null) {
                str2 = complicationLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it2 = clone.getComplications().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    if (ee7.a((Object) t2.getId(), (Object) str)) {
                        break;
                    }
                }
                T t5 = t2;
                if (t5 != null) {
                    t5.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (sc5.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 134170930) {
                    return null;
                }
                if (!str.equals("second-timezone")) {
                    return null;
                }
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) gson.a(str2, SecondTimezoneSetting.class);
                if (TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                    return null;
                }
                commuteTimeSetting = secondTimezoneSetting;
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeSetting commuteTimeSetting2 = (CommuteTimeSetting) gson.a(str2, CommuteTimeSetting.class);
                boolean isEmpty = TextUtils.isEmpty(commuteTimeSetting2.getAddress());
                commuteTimeSetting = commuteTimeSetting2;
                if (isEmpty) {
                    return null;
                }
            }
            return commuteTimeSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(G, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v3, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v11, resolved type: com.portfolio.platform.data.model.setting.WeatherWatchAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    public final Parcelable g(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        T t2;
        T t3;
        String settings;
        ee7.b(str, "appId");
        if (!cf5.c.f(str)) {
            return null;
        }
        DianaPreset a2 = a().a();
        String str2 = "";
        if (a2 != null) {
            Iterator<T> it = a2.getWatchapps().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                t3 = it.next();
                if (ee7.a((Object) t3.getId(), (Object) str)) {
                    break;
                }
            }
            T t4 = t3;
            if (!(t4 == null || (settings = t4.getSettings()) == null)) {
                str2 = settings;
            }
        }
        if (str2.length() == 0) {
            WatchAppLastSetting watchAppLastSetting = this.D.getWatchAppLastSetting(str);
            if (watchAppLastSetting != null) {
                str2 = watchAppLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it2 = clone.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    if (ee7.a((Object) t2.getId(), (Object) str)) {
                        break;
                    }
                }
                T t5 = t2;
                if (t5 != null) {
                    t5.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (sc5.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 1223440372) {
                    return null;
                }
                if (!str.equals("weather")) {
                    return null;
                }
                WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) this.q.a(str2, WeatherWatchAppSetting.class);
                List<WeatherLocationWrapper> locations = weatherWatchAppSetting.getLocations();
                ArrayList arrayList = new ArrayList();
                for (T t6 : locations) {
                    T t7 = t6;
                    if (!TextUtils.isEmpty(t7 != null ? t7.getId() : null)) {
                        arrayList.add(t6);
                    }
                }
                if (!(!arrayList.isEmpty())) {
                    return null;
                }
                commuteTimeWatchAppSetting = weatherWatchAppSetting;
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.q.a(str2, CommuteTimeWatchAppSetting.class);
                boolean z2 = !commuteTimeWatchAppSetting2.getAddresses().isEmpty();
                commuteTimeWatchAppSetting = commuteTimeWatchAppSetting2;
                if (!z2) {
                    return null;
                }
            }
            return commuteTimeWatchAppSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(G, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    public final LiveData<WatchApp> h() {
        return this.u;
    }

    @DexIgnore
    public final MutableLiveData<String> i() {
        return this.j;
    }

    @DexIgnore
    public final void j(String str) {
        ee7.b(str, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = G;
        local.d(str2, "setSelectedComplicationPos complicationPos=" + str);
        this.g.a(str);
    }

    @DexIgnore
    public final void k(String str) {
        ee7.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = G;
        local.d(str2, "setSelectedWatchApp watchAppPos=" + str);
        this.j.a(str);
    }

    @DexIgnore
    public final boolean l() {
        Boolean a2 = this.c.a();
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    @DexIgnore
    public final Complication b(String str) {
        T t2;
        ee7.b(str, "complicationId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (ee7.a((Object) str, (Object) t2.getComplicationId())) {
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public final List<WatchApp> c(String str) {
        ee7.b(str, "category");
        CopyOnWriteArrayList<WatchApp> copyOnWriteArrayList = this.e;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            if (t2.getCategories().contains(str)) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final WatchApp d(String str) {
        T t2;
        ee7.b(str, "watchAppId");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (ee7.a((Object) str, (Object) t2.getWatchappId())) {
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public final WatchFaceWrapper e(String str) {
        T t2;
        ee7.b(str, "watchFaceId");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (ee7.a((Object) str, (Object) t2.getId())) {
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public final boolean h(String str) {
        DianaPreset a2;
        ee7.b(str, "complicationId");
        if (!ee7.a((Object) str, (Object) "empty") && (a2 = this.b.a()) != null) {
            Iterator<DianaPresetComplicationSetting> it = a2.getComplications().iterator();
            while (it.hasNext()) {
                if (ee7.a((Object) it.next().getId(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean i(String str) {
        DianaPreset a2;
        ee7.b(str, "watchAppId");
        if (!ee7.a((Object) str, (Object) "empty") && (a2 = this.b.a()) != null) {
            Iterator<DianaPresetWatchAppSetting> it = a2.getWatchapps().iterator();
            while (it.hasNext()) {
                DianaPresetWatchAppSetting next = it.next();
                next.component1();
                if (ee7.a((Object) next.component2(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final ik7 a(String str, String str2, String str3) {
        ee7.b(str, "presetId");
        return xh7.b(zi7.a(qj7.a()), null, null, new c(this, str, str3, str2, null), 3, null);
    }

    @DexIgnore
    public final CopyOnWriteArrayList<WatchFaceWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public final void k() {
        Object obj;
        this.d.clear();
        this.d.addAll(this.z.getAllComplicationRaw());
        Boolean a2 = this.F.a();
        List d2 = ea7.d((Collection) this.B.getAllWatchAppRaw());
        if (!a2.booleanValue()) {
            Iterator it = d2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (ee7.a((Object) ((WatchApp) obj).getWatchappId(), (Object) "buddy-challenge")) {
                    break;
                }
            }
            WatchApp watchApp = (WatchApp) obj;
            if (watchApp != null) {
                d2.remove(watchApp);
            }
        }
        this.e.clear();
        this.e.addAll(d2);
    }

    @DexIgnore
    public final ik7 a(String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3) {
        ee7.b(str, "presetId");
        return xh7.b(zi7.a(qj7.a()), null, null, new d(this, str, dianaPreset, dianaPreset2, str3, str2, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<DianaPreset> a() {
        return this.b;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ee7.b(dianaPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = G;
        local.d(str, "savePreset newPreset=" + dianaPreset);
        this.b.a(dianaPreset.clone());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.fossil.a06.g
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.a06$g r0 = (com.fossil.a06.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.a06$g r0 = new com.fossil.a06$g
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r7 = r0.L$2
            com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r7
            java.lang.Object r1 = r0.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.a06 r0 = (com.fossil.a06) r0
            com.fossil.t87.a(r8)
            goto L_0x007e
        L_0x0035:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003d:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.fossil.a06.G
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "initializePreset presetId="
            r4.append(r5)
            r4.append(r7)
            java.lang.String r4 = r4.toString()
            r8.d(r2, r4)
            com.portfolio.platform.data.source.DianaPresetRepository r8 = r6.y
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r8.getPresetById(r7)
            r6.k()
            if (r8 == 0) goto L_0x006c
            com.portfolio.platform.data.model.diana.preset.DianaPreset r2 = r8.clone()
            goto L_0x006d
        L_0x006c:
            r2 = 0
        L_0x006d:
            r0.L$0 = r6
            r0.L$1 = r7
            r0.L$2 = r8
            r0.label = r3
            java.lang.Object r7 = r6.a(r2, r0)
            if (r7 != r1) goto L_0x007c
            return r1
        L_0x007c:
            r0 = r6
            r7 = r8
        L_0x007e:
            if (r7 == 0) goto L_0x008f
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r7.clone()
            r0.a = r8
            androidx.lifecycle.MutableLiveData<com.portfolio.platform.data.model.diana.preset.DianaPreset> r8 = r0.b
            com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = r7.clone()
            r8.a(r7)
        L_0x008f:
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a06.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.diana.preset.DianaPreset r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.fossil.a06.f
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.a06$f r0 = (com.fossil.a06.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.a06$f r0 = new com.fossil.a06$f
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r8 = r0.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r0.L$2
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
            java.lang.Object r8 = r0.L$1
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.a06 r8 = (com.fossil.a06) r8
            com.fossil.t87.a(r9)
            goto L_0x00dc
        L_0x003a:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0042:
            com.fossil.t87.a(r9)
            if (r8 == 0) goto L_0x00de
            java.lang.String r9 = r8.getSerialNumber()
            com.portfolio.platform.data.RingStyleRepository r2 = r7.C
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r2 = r2.getRingStylesBySerial(r9)
            r7.p = r2
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.fossil.a06.G
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "defaultRingStyle "
            r5.append(r6)
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r6 = r7.p
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r2.d(r4, r5)
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper> r2 = r7.f
            r2.clear()
            com.portfolio.platform.data.source.WatchFaceRepository r2 = r7.E
            java.util.List r2 = r2.getWatchFacesWithSerial(r9)
            r7.o = r2
            if (r2 == 0) goto L_0x0091
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper> r4 = r7.f
            java.util.ArrayList r5 = r8.getComplications()
            java.util.List r2 = com.fossil.yc5.a(r2, r5)
            boolean r2 = r4.addAll(r2)
            com.fossil.pb7.a(r2)
        L_0x0091:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.fossil.a06.G
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "watchFaceWrapper "
            r5.append(r6)
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper> r6 = r7.f
            r5.append(r6)
            java.lang.String r6 = " cachingWatchFace "
            r5.append(r6)
            java.util.List<com.portfolio.platform.data.model.diana.preset.WatchFace> r6 = r7.o
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r2.d(r4, r5)
            com.portfolio.platform.data.source.WatchFaceRepository r2 = r7.E
            androidx.lifecycle.LiveData r2 = r2.getWatchFacesLiveDataWithSerial(r9)
            r7.n = r2
            com.fossil.tk7 r2 = com.fossil.qj7.c()
            com.fossil.a06$e r4 = new com.fossil.a06$e
            r5 = 0
            r4.<init>(r5, r7, r8, r0)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.L$2 = r8
            r0.L$3 = r9
            r0.label = r3
            java.lang.Object r9 = com.fossil.vh7.a(r2, r4, r0)
            if (r9 != r1) goto L_0x00dc
            return r1
        L_0x00dc:
            com.fossil.i97 r9 = (com.fossil.i97) r9
        L_0x00de:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a06.a(com.portfolio.platform.data.model.diana.preset.DianaPreset, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final MutableLiveData<String> f() {
        return this.g;
    }

    @DexIgnore
    public final MutableLiveData<String> g() {
        return this.m;
    }

    @DexIgnore
    public final List<Complication> a(String str) {
        ee7.b(str, "category");
        CopyOnWriteArrayList<Complication> copyOnWriteArrayList = this.d;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            if (t2.getCategories().contains(str)) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }
}
