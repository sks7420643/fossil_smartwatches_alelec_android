package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.k37;
import com.fossil.u27;
import com.fossil.v27;
import com.fossil.zx6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WXEntryActivity extends Activity implements k37 {
    @DexIgnore
    @Override // com.fossil.k37
    public void a(u27 u27) {
        zx6.a().a(u27);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        zx6.a().a(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        zx6.a().a(getIntent(), this);
    }

    @DexIgnore
    @Override // com.fossil.k37
    public void a(v27 v27) {
        zx6.a().a(v27);
        finish();
    }
}
