package com.fossil.wearables.fsl.sleep;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "sleep_goal")
public class MFSleepGoal extends BaseModel {
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_MINUTE; // = "minute";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_OFFSET; // = "timezoneOffset";
    @DexIgnore
    @DatabaseField(columnName = "date")
    public String date;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_MINUTE)
    public int minute;
    @DexIgnore
    @DatabaseField(columnName = "timezoneOffset")
    public int timezoneOffset;

    @DexIgnore
    public MFSleepGoal() {
    }

    @DexIgnore
    public String getDate() {
        return this.date;
    }

    @DexIgnore
    public int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public void setDate(String str) {
        this.date = str;
    }

    @DexIgnore
    public void setMinute(int i) {
        this.minute = i;
    }

    @DexIgnore
    public void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public MFSleepGoal(String str, int i, int i2) {
        this.date = str;
        this.timezoneOffset = i2;
        this.minute = i;
    }
}
