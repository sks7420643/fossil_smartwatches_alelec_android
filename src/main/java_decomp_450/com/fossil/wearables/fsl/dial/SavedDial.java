package com.fossil.wearables.fsl.dial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SavedDial extends BaseModel implements DialListItem {
    @DexIgnore
    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    public byte[] imageBytes;
    @DexIgnore
    public ArrayList<ConfigItem> initialConfigItems;
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<ConfigItem> mConfigItems;
    @DexIgnore
    @DatabaseField(canBeNull = false)
    public String mDisplayName;
    @DexIgnore
    @DatabaseField(canBeNull = false)
    public String templateDialId;

    @DexIgnore
    public SavedDial() {
    }

    @DexIgnore
    public ForeignCollection<ConfigItem> getConfigItems() {
        return this.mConfigItems;
    }

    @DexIgnore
    public String getDisplayName() {
        return this.mDisplayName;
    }

    @DexIgnore
    public String getTemplateDialId() {
        return this.templateDialId;
    }

    @DexIgnore
    public Bitmap getThumbnailBitmap() {
        byte[] bArr = this.imageBytes;
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
    }

    @DexIgnore
    public Drawable getThumbnailDrawable(Context context) {
        return null;
    }

    @DexIgnore
    public void setDisplayName(String str) {
        this.mDisplayName = str;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=");
        sb.append(getDbRowId());
        sb.append(", ");
        sb.append("mDisplayName=");
        sb.append(this.mDisplayName);
        sb.append(", ");
        sb.append("size=");
        sb.append(this.mConfigItems.size());
        Iterator<E> it = this.mConfigItems.iterator();
        while (it.hasNext()) {
            sb.append("\n- ");
            sb.append((Object) it.next());
        }
        return sb.toString();
    }

    @DexIgnore
    public SavedDial(String str, String str2) {
        this.mDisplayName = str;
        this.templateDialId = str2;
    }
}
