package com.fossil.wearables.fsl.fitness;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Reconciler {
    @DexIgnore
    public double calories;
    @DexIgnore
    public double distance;
    @DexIgnore
    public double steps;

    @DexIgnore
    public Reconciler(SampleRaw sampleRaw, List<SampleRaw> list) {
        BigDecimal bigDecimal = BigDecimal.ZERO;
        BigDecimal bigDecimal2 = bigDecimal;
        BigDecimal bigDecimal3 = bigDecimal2;
        for (SampleRaw sampleRaw2 : list) {
            BigDecimal bigDecimal4 = new BigDecimal(((double) ovelappingTimeInterval(sampleRaw.startTime, sampleRaw.endTime, sampleRaw2.startTime, sampleRaw2.endTime)) / ((double) sampleRaw.getDuration()));
            bigDecimal = bigDecimal.add(reconciledAmount(new BigDecimal(sampleRaw.steps), new BigDecimal(sampleRaw2.steps), bigDecimal4));
            bigDecimal2 = bigDecimal2.add(reconciledAmount(new BigDecimal(sampleRaw.calories), new BigDecimal(sampleRaw2.calories), bigDecimal4));
            bigDecimal3 = bigDecimal3.add(reconciledAmount(new BigDecimal(sampleRaw.distance), new BigDecimal(sampleRaw2.distance), bigDecimal4));
        }
        this.steps = sampleRaw.steps + bigDecimal.setScale(0, 4).doubleValue();
        this.calories = sampleRaw.calories + bigDecimal2.setScale(0, 4).doubleValue();
        this.distance = sampleRaw.distance + bigDecimal3.setScale(0, 4).doubleValue();
    }

    @DexIgnore
    public static long ovelappingTimeInterval(Date date, Date date2, Date date3, Date date4) {
        if (date.getTime() <= date3.getTime()) {
            date = date3;
        }
        if (date2.getTime() >= date4.getTime()) {
            date2 = date4;
        }
        return Math.max(0L, date2.getTime() - date.getTime());
    }

    @DexIgnore
    private BigDecimal reconciledAmount(BigDecimal bigDecimal, BigDecimal bigDecimal2, BigDecimal bigDecimal3) {
        BigDecimal multiply = bigDecimal.multiply(bigDecimal3);
        BigDecimal multiply2 = bigDecimal2.multiply(bigDecimal3);
        if (multiply.compareTo(multiply2) > 0) {
            return multiply2.negate();
        }
        return multiply.negate();
    }
}
