package com.fossil.wearables.fsl.fitness;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FitnessProviderImpl extends BaseDbProvider implements FitnessProvider {
    @DexIgnore
    public static /* final */ int DEFAULT_ACTIVE_TIME_GOAL; // = 3;
    @DexIgnore
    public static /* final */ int DEFAULT_CALORIES_GOAL; // = 1200;
    @DexIgnore
    public static /* final */ int DEFAULT_DAILY_STEP_GOAL; // = 10000;
    @DexIgnore
    public static /* final */ String TAG; // = "FitnessProviderImpl";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN createdAt BIGINT");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN updatedAt BIGINT");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon2_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon2_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE sampleraw ADD COLUMN pinType INTEGER DEFAULT 0");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN pinType INTEGER DEFAULT 0");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon3_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon3_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE sampleraw ADD COLUMN uaPinType INTEGER DEFAULT 0");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon4_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon4_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN intensities");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon5_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon5_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("CREATE TABLE activitySettings (currentStepGoal INTEGER, currentCaloriesGoal INTEGER, currentActiveTimeGoal INTEGER, id VARCHAR);");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN stepGoal INTEGER DEFAULT 0;");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN caloriesGoal INTEGER DEFAULT 0;");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN activeTimeGoal INTEGER DEFAULT 0;");
                sQLiteDatabase.execSQL("ALTER TABLE sampleday ADD COLUMN activeTime INTEGER DEFAULT 0;");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
            put(3, new Anon2_Level2());
            put(4, new Anon3_Level2());
            put(5, new Anon4_Level2());
            put(6, new Anon5_Level2());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements Callable {
        @DexIgnore
        public /* final */ /* synthetic */ List val$sampleDays;

        @DexIgnore
        public Anon2(List list) {
            this.val$sampleDays = list;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() {
            for (SampleDay sampleDay : this.val$sampleDays) {
                FitnessProviderImpl.this.updateSampleDay(sampleDay);
            }
            return null;
        }
    }

    @DexIgnore
    public FitnessProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private SampleDay calculateSample(SampleDay sampleDay, SampleDay sampleDay2) {
        MFLogger.d(TAG, "calculateSample - currentSample=" + sampleDay + ", newSample=" + sampleDay2);
        double steps = sampleDay2.getSteps() + sampleDay.getSteps();
        double calories = sampleDay2.getCalories() + sampleDay.getCalories();
        double distance = sampleDay2.getDistance() + sampleDay.getDistance();
        List<Integer> intensities = sampleDay2.getIntensities();
        List<Integer> intensities2 = sampleDay.getIntensities();
        for (int i = 0; i < intensities.size(); i++) {
            intensities2.set(i, Integer.valueOf(intensities.get(i).intValue() + intensities2.get(i).intValue()));
        }
        sampleDay.setSteps(steps);
        sampleDay.setCalories(calories);
        sampleDay.setDistance(distance);
        sampleDay.setCreatedAt(sampleDay.getCreatedAt());
        sampleDay.setIntensities(intensities2);
        sampleDay.setActiveTimeGoal(sampleDay2.getActiveTimeGoal());
        sampleDay.setCaloriesGoal(sampleDay2.getCaloriesGoal());
        sampleDay.setActiveTimeGoal(sampleDay2.getActiveTimeGoal());
        if (sampleDay2.getSteps() != sampleDay.getSteps()) {
            sampleDay.setUpdatedAt(System.currentTimeMillis());
        }
        return sampleDay;
    }

    @DexIgnore
    private String formatDate(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(1);
        int i2 = instance.get(2);
        int i3 = instance.get(5);
        return String.format(Locale.US, "%04d%02d%02d", Integer.valueOf(i), Integer.valueOf(i2 + 1), Integer.valueOf(i3));
    }

    @DexIgnore
    private Dao<SampleDay, String> getSampleDayDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(SampleDay.class);
    }

    @DexIgnore
    private Dao<SampleRaw, String> getSampleRawDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(SampleRaw.class);
    }

    @DexIgnore
    public void addDownloadedSampleRaw(SampleRaw sampleRaw) {
        MFLogger.d(TAG, "addDownloadedSampleRaw - sample=" + sampleRaw);
        try {
            if (getSampleRawDao().queryForId(sampleRaw.getUri().toASCIIString()) == null) {
                getSampleRawDao().create(sampleRaw);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "addDownloadedSampleRaw - e=" + e.toString());
        }
    }

    @DexIgnore
    public void addSample(SampleRaw sampleRaw) {
        MFLogger.d(TAG, "addSample - sample=" + sampleRaw);
        try {
            if (getSampleRawDao().queryForId(sampleRaw.getUri().toASCIIString()) == null) {
                sampleRaw.setTimeZone(TimeZone.getDefault().getID());
                getSampleRawDao().create(sampleRaw);
                updateSampleDays(Interpolator.interpolateDays(sampleRaw.startTime, sampleRaw.endTime, sampleRaw.getTimeZone(), sampleRaw.steps, sampleRaw.calories, sampleRaw.distance));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "addSample - e=" + e.toString());
        } catch (Exception e2) {
            MFLogger.e(TAG, "addSample - ex=" + e2.toString());
        }
    }

    @DexIgnore
    public void clearActivitySettings() {
        MFLogger.d(TAG, ".clearActivitySettings");
        try {
            TableUtils.clearTable(getActivitySettingsDao().getConnectionSource(), ActivitySettings.class);
        } catch (Exception e) {
            MFLogger.e(TAG, "clearActivitySettings Exception=" + e);
        }
    }

    @DexIgnore
    public boolean createOrUpdateActivitySettings(ActivitySettings activitySettings) {
        MFLogger.d(TAG, ".createOrUpdateActivitySettings");
        try {
            Dao<ActivitySettings, String> activitySettingsDao = getActivitySettingsDao();
            ActivitySettings queryForFirst = activitySettingsDao.queryBuilder().queryForFirst();
            if (queryForFirst != null) {
                queryForFirst.setCurrentActiveTimeGoal(activitySettings.getCurrentActiveTimeGoal());
                queryForFirst.setCurrentStepGoal(activitySettings.getCurrentStepGoal());
                queryForFirst.setCurrentCaloriesGoal(activitySettings.getCurrentCaloriesGoal());
                activitySettingsDao.update(queryForFirst);
                return true;
            }
            activitySettingsDao.create(activitySettings);
            return true;
        } catch (Exception e) {
            MFLogger.e(TAG, ".createOrUpdateActivitySettings - e=" + e);
            return false;
        }
    }

    @DexIgnore
    public ActivitySettings getActivitySettings() {
        MFLogger.d(TAG, ".getActivitySettings");
        try {
            return getActivitySettingsDao().queryBuilder().queryForFirst();
        } catch (Exception e) {
            MFLogger.e(TAG, ".getActivitySettings - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public Dao<ActivitySettings, String> getActivitySettingsDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(ActivitySettings.class);
    }

    @DexIgnore
    public List<DailyGoal> getAllDailyGoals() {
        MFLogger.d(TAG, "getAllDailyGoals");
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DailyGoal, String> queryBuilder = getDailyGoalDao().queryBuilder();
            queryBuilder.orderBy("year", true).orderBy("month", true).orderBy("day", true);
            return getDailyGoalDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getAllDailyGoals - e=" + e.toString());
            return arrayList;
        } catch (Exception e2) {
            MFLogger.e(TAG, "getAllDailyGoals - ex=" + e2.toString());
            return arrayList;
        }
    }

    @DexIgnore
    public List<SampleDay> getAllSampleDays() {
        MFLogger.d(TAG, "getAllSampleDays");
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            queryBuilder.orderBy("year", true).orderBy("month", true).orderBy("day", true);
            return getSampleDayDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getAllSampleDays - e=" + e.toString());
            return arrayList;
        } catch (Exception e2) {
            MFLogger.e(TAG, "getAllSampleDays - ex=" + e2.toString());
            return arrayList;
        }
    }

    @DexIgnore
    public DailyGoal getDailyGoal(Calendar calendar) {
        MFLogger.d(TAG, "getDailyGoal - calendarDay=" + calendar);
        return getDailyGoal(calendar.get(1), calendar.get(2) + 1, calendar.get(5));
    }

    @DexIgnore
    public Dao<DailyGoal, String> getDailyGoalDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(DailyGoal.class);
    }

    @DexIgnore
    public List<DailyGoal> getDailyGoalInRange(Date date, Date date2) {
        MFLogger.d(TAG, "getDailyGoalInRange - fromDate=" + date + ", toDate=" + date2);
        String formatDate = formatDate(date);
        String formatDate2 = formatDate(date2);
        try {
            return getDailyGoalDao().queryRaw(String.format(Locale.US, "select * from %s where (substr('0000' || %s, -4, 4) || substr('00' || %s, -2, 2) || substr('00' || %s, -2, 2)) between ? and ? order by %s, %s, %s", DailyGoal.TABLE_NAME, "year", "month", "day", "year", "month", "day"), (RawRowMapper<UO>) getDailyGoalDao().getRawRowMapper(), formatDate, formatDate2).getResults();
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getDailyGoalInRange - ex=" + e.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{SampleDay.class, SampleRaw.class, DailyGoal.class, ActivitySettings.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 6;
    }

    @DexIgnore
    public Date getEndDateOfSampleDay() {
        MFLogger.d(TAG, "getEndDateOfSampleDay");
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            queryBuilder.orderBy("year", false);
            queryBuilder.orderBy("month", false);
            queryBuilder.orderBy("day", false);
            SampleDay queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst == null) {
                return null;
            }
            Calendar instance = Calendar.getInstance();
            instance.set(queryForFirst.getYear(), queryForFirst.getMonth() - 1, queryForFirst.getDay());
            return instance.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getEndDateOfSampleDay - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public Date getEndDateOfSampleRaw() {
        MFLogger.d(TAG, "getEndDateOfSampleRaw");
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.orderBy(SampleRaw.COLUMN_END_TIME, false);
            SampleRaw queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst.getEndTime();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getEndDateOfSampleRaw - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public int getLastDailyGoal() {
        MFLogger.d(TAG, "getLastDailyGoal");
        try {
            QueryBuilder<DailyGoal, String> queryBuilder = getDailyGoalDao().queryBuilder();
            queryBuilder.where().not().eq("steps", 0);
            queryBuilder.orderBy("year", false).orderBy("month", false).orderBy("day", false);
            DailyGoal queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst == null) {
                return 10000;
            }
            return queryForFirst.getSteps();
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getLastDailyGoal - ex=" + e.toString());
            return 10000;
        }
    }

    @DexIgnore
    public int getLastDailyGoalFromDate(Date date) {
        MFLogger.d(TAG, "getLastDailyGoalFromDate - date=" + date);
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i = instance.get(1);
        int i2 = instance.get(2);
        int i3 = instance.get(5);
        try {
            QueryBuilder<DailyGoal, String> queryBuilder = getDailyGoalDao().queryBuilder();
            Where<DailyGoal, String> where = queryBuilder.where();
            int i4 = i2 + 1;
            where.or(where.lt("year", Integer.valueOf(i)), where.and(where.eq("year", Integer.valueOf(i)), where.lt("month", Integer.valueOf(i4)), new Where[0]), where.and(where.eq("year", Integer.valueOf(i)), where.eq("month", Integer.valueOf(i4)), where.le("day", Integer.valueOf(i3))));
            queryBuilder.orderBy("year", false).orderBy("month", false).orderBy("day", false);
            DailyGoal queryForFirst = queryBuilder.queryForFirst();
            MFLogger.d(TAG, "getLastDailyGoalFromDate - QUERY=" + queryBuilder.prepareStatementString());
            if (queryForFirst != null) {
                MFLogger.d(TAG, "getLastDailyGoalFromDate - dailyGoal=" + queryForFirst.getDay() + "/" + queryForFirst.getMonth() + "/" + queryForFirst.getYear() + ", step=" + queryForFirst.getSteps());
            }
            if (queryForFirst == null) {
                return 10000;
            }
            return queryForFirst.getSteps();
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getLastDailyGoalFromDate - ex=" + e.toString());
            return 10000;
        }
    }

    @DexIgnore
    public SampleRaw getLastSampleRaw() {
        MFLogger.d(TAG, "getLastSampleRaw");
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.orderBy(SampleRaw.COLUMN_END_TIME, false);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            MFLogger.e(TAG, "getLastSampleRaw - e=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public List<SampleRaw> getListSampleRawByType(int i) {
        MFLogger.d(TAG, "getListSampleRawByType - uaPinType=" + i);
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.setWhere(queryBuilder.where().eq(SampleRaw.COLUMN_UA_PIN_TYPE, Integer.valueOf(i)));
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.wearables.fsl.fitness.SampleDay getNearestSampleDayFromDate(java.util.Date r30) {
        /*
            r29 = this;
            r0 = r30
            java.lang.String r1 = "/"
            java.lang.String r2 = "day"
            java.lang.String r3 = "month"
            java.lang.String r4 = "year"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "getLastStepGoalFromDate - date="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "FitnessProviderImpl"
            com.misfit.frameworks.common.log.MFLogger.d(r6, r5)
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r5.setTime(r0)
            r7 = 1
            int r0 = r5.get(r7)
            r8 = 2
            int r9 = r5.get(r8)
            r10 = 5
            int r5 = r5.get(r10)
            r10 = 0
            com.j256.ormlite.dao.Dao r12 = r29.getSampleDayDao()     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.QueryBuilder r12 = r12.queryBuilder()     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r13 = r12.where()     // Catch:{ Exception -> 0x00f1 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r14 = r13.lt(r4, r14)     // Catch:{ Exception -> 0x00f1 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r15 = r13.eq(r4, r15)     // Catch:{ Exception -> 0x00f1 }
            int r9 = r9 + r7
            java.lang.Integer r11 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r11 = r13.lt(r3, r11)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where[] r8 = new com.j256.ormlite.stmt.Where[r10]     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r8 = r13.and(r15, r11, r8)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where[] r11 = new com.j256.ormlite.stmt.Where[r7]     // Catch:{ Exception -> 0x00f1 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r0 = r13.eq(r4, r0)     // Catch:{ Exception -> 0x00f1 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r9 = r13.eq(r3, r9)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where[] r15 = new com.j256.ormlite.stmt.Where[r7]     // Catch:{ Exception -> 0x00f1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r5 = r13.le(r2, r5)     // Catch:{ Exception -> 0x00f1 }
            r15[r10] = r5     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.Where r0 = r13.and(r0, r9, r15)     // Catch:{ Exception -> 0x00f1 }
            r11[r10] = r0     // Catch:{ Exception -> 0x00f1 }
            r13.or(r14, r8, r11)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.QueryBuilder r0 = r12.orderBy(r4, r10)     // Catch:{ Exception -> 0x00f1 }
            com.j256.ormlite.stmt.QueryBuilder r0 = r0.orderBy(r3, r10)     // Catch:{ Exception -> 0x00f1 }
            r0.orderBy(r2, r10)     // Catch:{ Exception -> 0x00f1 }
            java.lang.Object r0 = r12.queryForFirst()     // Catch:{ Exception -> 0x00f1 }
            r2 = r0
            com.fossil.wearables.fsl.fitness.SampleDay r2 = (com.fossil.wearables.fsl.fitness.SampleDay) r2     // Catch:{ Exception -> 0x00f1 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ee }
            r0.<init>()     // Catch:{ Exception -> 0x00ee }
            java.lang.String r3 = "getLastSampleDayFromDate - QUERY="
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r3 = r12.prepareStatementString()     // Catch:{ Exception -> 0x00ee }
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ee }
            com.misfit.frameworks.common.log.MFLogger.d(r6, r0)     // Catch:{ Exception -> 0x00ee }
            if (r2 == 0) goto L_0x010f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ee }
            r0.<init>()     // Catch:{ Exception -> 0x00ee }
            java.lang.String r3 = "getLastSampleDayFromDate - dailyGoal="
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            int r3 = r2.getDay()     // Catch:{ Exception -> 0x00ee }
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            r0.append(r1)     // Catch:{ Exception -> 0x00ee }
            int r3 = r2.getMonth()     // Catch:{ Exception -> 0x00ee }
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            r0.append(r1)     // Catch:{ Exception -> 0x00ee }
            int r1 = r2.getYear()     // Catch:{ Exception -> 0x00ee }
            r0.append(r1)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r1 = ", step="
            r0.append(r1)     // Catch:{ Exception -> 0x00ee }
            double r3 = r2.getSteps()     // Catch:{ Exception -> 0x00ee }
            r0.append(r3)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ee }
            com.misfit.frameworks.common.log.MFLogger.d(r6, r0)     // Catch:{ Exception -> 0x00ee }
            goto L_0x010f
        L_0x00ee:
            r0 = move-exception
            r11 = r2
            goto L_0x00f3
        L_0x00f1:
            r0 = move-exception
            r11 = 0
        L_0x00f3:
            r0.printStackTrace()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "getLastSampleDayFromDate - ex="
            r1.append(r2)
            java.lang.String r0 = r0.toString()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.misfit.frameworks.common.log.MFLogger.e(r6, r0)
            r2 = r11
        L_0x010f:
            if (r2 != 0) goto L_0x0168
            org.joda.time.DateTime r0 = new org.joda.time.DateTime
            r0.<init>()
            org.joda.time.DateTimeZone r1 = r0.getZone()
            java.util.TimeZone r1 = r1.toTimeZone()
            java.util.Date r2 = r0.toDate()
            boolean r2 = r1.inDaylightTime(r2)
            if (r2 == 0) goto L_0x012f
            int r2 = r1.getDSTSavings()
            r21 = r2
            goto L_0x0131
        L_0x012f:
            r21 = 0
        L_0x0131:
            com.fossil.wearables.fsl.fitness.SampleDay r2 = new com.fossil.wearables.fsl.fitness.SampleDay
            int r17 = r0.getYear()
            int r18 = r0.getMonthOfYear()
            int r19 = r0.getDayOfMonth()
            java.lang.String r20 = r1.getID()
            r22 = 0
            r24 = 0
            r26 = 0
            r0 = 3
            java.lang.Integer[] r0 = new java.lang.Integer[r0]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)
            r0[r10] = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)
            r0[r7] = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)
            r3 = 2
            r0[r3] = r1
            java.util.List r28 = java.util.Arrays.asList(r0)
            r16 = r2
            r16.<init>(r17, r18, r19, r20, r21, r22, r24, r26, r28)
        L_0x0168:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wearables.fsl.fitness.FitnessProviderImpl.getNearestSampleDayFromDate(java.util.Date):com.fossil.wearables.fsl.fitness.SampleDay");
    }

    @DexIgnore
    public List<SampleRaw> getPendingSampleRaws() {
        MFLogger.d(TAG, "getPendingSampleRaws");
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getPendingSampleRaws - ex=" + e.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public List<SampleRaw> getRawSamples(Date date, Date date2) {
        MFLogger.d(TAG, "getRawSamples - start=" + date + ", end=" + date2);
        ArrayList arrayList = new ArrayList();
        try {
            Dao<SampleRaw, String> sampleRawDao = getSampleRawDao();
            QueryBuilder<SampleRaw, String> queryBuilder = sampleRawDao.queryBuilder();
            Where<SampleRaw, String> where = queryBuilder.where();
            where.and(where.ge(SampleRaw.COLUMN_START_TIME, date), where.lt(SampleRaw.COLUMN_START_TIME, date2), new Where[0]);
            queryBuilder.setWhere(where);
            queryBuilder.orderBy(SampleRaw.COLUMN_START_TIME, true);
            return sampleRawDao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getRawSamples - e=" + e.toString());
            return arrayList;
        } catch (Exception e2) {
            MFLogger.e(TAG, "getRawSamples - ex=" + e2.toString());
            return arrayList;
        }
    }

    @DexIgnore
    public List<SampleRaw> getRawSamplesOverlapping(Date date, Date date2) {
        MFLogger.d(TAG, "getRawSamplesOverlapping - start=" + date + ", end=" + date2);
        ArrayList arrayList = new ArrayList();
        try {
            Dao<SampleRaw, String> sampleRawDao = getSampleRawDao();
            QueryBuilder<SampleRaw, String> queryBuilder = sampleRawDao.queryBuilder();
            Where<SampleRaw, String> where = queryBuilder.where();
            where.or(where.and(where.ge(SampleRaw.COLUMN_START_TIME, date), where.lt(SampleRaw.COLUMN_START_TIME, date2), new Where[0]), where.and(where.ge(SampleRaw.COLUMN_END_TIME, date), where.lt(SampleRaw.COLUMN_END_TIME, date2), new Where[0]), new Where[0]);
            queryBuilder.setWhere(where);
            return sampleRawDao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getRawSamplesOverlapping - e=" + e.toString());
            return arrayList;
        } catch (Exception e2) {
            MFLogger.e(TAG, "getRawSamplesOverlapping - ex=" + e2.toString());
            return arrayList;
        }
    }

    @DexIgnore
    public SampleDay getSampleDayByDate(int i, int i2, int i3) {
        MFLogger.d(TAG, "getSampleDayByDate - year=" + i + ", month=" + i2 + ", day=" + i3);
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            queryBuilder.where().eq("year", Integer.valueOf(i)).and().eq("month", Integer.valueOf(i2)).and().eq("day", Integer.valueOf(i3));
            return queryBuilder.queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getSampleDayByDate - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public List<SampleDay> getSampleDays(Calendar calendar, Calendar calendar2) {
        ArrayList arrayList;
        MFLogger.d(TAG, "getSampleDays - startCalendar=" + calendar + ", endCalendar=" + calendar2);
        ArrayList arrayList2 = new ArrayList();
        int i = calendar.get(1);
        int i2 = calendar.get(2) + 1;
        int i3 = calendar.get(5);
        int i4 = calendar2.get(1);
        int i5 = calendar2.get(2) + 1;
        int i6 = calendar2.get(5);
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            Where<SampleDay, String> where = queryBuilder.where();
            Where<SampleDay, String> eq = where.eq("year", Integer.valueOf(i));
            Where<SampleDay, String> eq2 = where.eq("month", Integer.valueOf(i2));
            arrayList = arrayList2;
            try {
                where.and(where.or(where.and(eq, eq2, where.ge("day", Integer.valueOf(i3))), where.and(where.eq("year", Integer.valueOf(i)), where.gt("month", Integer.valueOf(i2)), new Where[0]), where.gt("year", Integer.valueOf(i))), where.or(where.and(where.eq("year", Integer.valueOf(i4)), where.eq("month", Integer.valueOf(i5)), where.lt("day", Integer.valueOf(i6))), where.and(where.eq("year", Integer.valueOf(i4)), where.lt("month", Integer.valueOf(i5)), new Where[0]), where.lt("year", Integer.valueOf(i4))), new Where[0]);
                queryBuilder.setWhere(where);
                queryBuilder.orderBy("year", true).orderBy("month", true).orderBy("day", true);
                return getSampleDayDao().query(queryBuilder.prepare());
            } catch (SQLException e) {
                e = e;
            } catch (Exception e2) {
                e = e2;
                MFLogger.e(TAG, "getSampleDays - ex=" + e.toString());
                return arrayList;
            }
        } catch (SQLException e3) {
            e = e3;
            arrayList = arrayList2;
            e.printStackTrace();
            MFLogger.e(TAG, "getSampleDays - e=" + e.toString());
            return arrayList;
        } catch (Exception e4) {
            e = e4;
            arrayList = arrayList2;
            MFLogger.e(TAG, "getSampleDays - ex=" + e.toString());
            return arrayList;
        }
    }

    @DexIgnore
    public List<SampleRaw> getSampleRawsByIds(List<String> list) {
        MFLogger.d(TAG, "getSampleRawsByIds - ids=" + list);
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.where().in("id", list);
            queryBuilder.orderBy("id", true);
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getSampleRawsByIds - ex=" + e.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public SampleDay getSamplesForDay(int i, int i2, int i3) {
        MFLogger.d(TAG, "getSamplesForDay - day=" + i + ", month=" + i2 + ", year=" + i3);
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            Where<SampleDay, String> where = queryBuilder.where();
            where.and(where.eq("year", Integer.valueOf(i3)), where.eq("month", Integer.valueOf(i2)), where.eq("day", Integer.valueOf(i)));
            queryBuilder.setWhere(where);
            queryBuilder.orderBy("year", true).orderBy("month", true).orderBy("day", true);
            return getSampleDayDao().queryForFirst(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getSamplesForDay - e=" + e.toString());
        } catch (Exception e2) {
            MFLogger.e(TAG, "getSamplesForDay - ex=" + e2.toString());
        }
        return null;
    }

    @DexIgnore
    public Date getStartDateOfSampleDay() {
        MFLogger.d(TAG, "getStartDateOfSampleDay");
        try {
            QueryBuilder<SampleDay, String> queryBuilder = getSampleDayDao().queryBuilder();
            queryBuilder.orderBy("year", true);
            queryBuilder.orderBy("month", true);
            queryBuilder.orderBy("day", true);
            SampleDay queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst == null) {
                return null;
            }
            Calendar instance = Calendar.getInstance();
            instance.set(queryForFirst.getYear(), queryForFirst.getMonth() - 1, queryForFirst.getDay());
            return instance.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getStartDateOfSampleDay - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public Date getStartDateOfSampleRaw() {
        MFLogger.d(TAG, "getStartDateOfSampleRaw");
        try {
            QueryBuilder<SampleRaw, String> queryBuilder = getSampleRawDao().queryBuilder();
            queryBuilder.orderBy(SampleRaw.COLUMN_START_TIME, true);
            SampleRaw queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst.getStartTime();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getStartDateOfSampleRaw - ex=" + e.toString());
            return null;
        }
    }

    @DexIgnore
    public Pair<Integer, Integer> getTodayStepCountAndGoal() {
        MFLogger.d(TAG, "getTodayStepCountAndGoal");
        int i = 0;
        int i2 = 10000;
        SampleDay samplesForDay = getSamplesForDay(Calendar.getInstance());
        if (samplesForDay != null) {
            i = Integer.valueOf((int) samplesForDay.getSteps());
            i2 = Integer.valueOf(samplesForDay.getStepGoal());
        }
        return new Pair<>(i, i2);
    }

    @DexIgnore
    public void updateActiveTimeGoalSettings(int i) {
        MFLogger.d(TAG, ".updateActiveTimeGoalSettings");
        try {
            Dao<ActivitySettings, String> activitySettingsDao = getActivitySettingsDao();
            ActivitySettings queryForFirst = activitySettingsDao.queryBuilder().queryForFirst();
            if (queryForFirst != null) {
                queryForFirst.setCurrentActiveTimeGoal(i);
                activitySettingsDao.update(queryForFirst);
                return;
            }
            activitySettingsDao.create(new ActivitySettings(10000, 1200, i));
        } catch (Exception e) {
            MFLogger.e(TAG, ".updateCaloriesGoalSettings - e=" + e);
        }
    }

    @DexIgnore
    public void updateCaloriesGoalSettings(int i) {
        MFLogger.d(TAG, ".updateCaloriesGoalSettings");
        try {
            Dao<ActivitySettings, String> activitySettingsDao = getActivitySettingsDao();
            ActivitySettings queryForFirst = activitySettingsDao.queryBuilder().queryForFirst();
            if (queryForFirst != null) {
                queryForFirst.setCurrentCaloriesGoal(i);
                activitySettingsDao.update(queryForFirst);
                return;
            }
            activitySettingsDao.create(new ActivitySettings(10000, i, 3));
        } catch (Exception e) {
            MFLogger.e(TAG, ".updateCaloriesGoalSettings - e=" + e);
        }
    }

    @DexIgnore
    public void updateSampleDay(SampleDay sampleDay) {
        MFLogger.d(TAG, "updateSampleDay - sample=" + sampleDay);
        if (sampleDay != null) {
            try {
                SampleDay sampleDayByDate = getSampleDayByDate(sampleDay.getYear(), sampleDay.getMonth(), sampleDay.getDay());
                if (sampleDayByDate != null) {
                    sampleDay = calculateSample(sampleDayByDate, sampleDay);
                } else {
                    Calendar instance = Calendar.getInstance();
                    instance.set(sampleDay.year, sampleDay.month - 1, sampleDay.day);
                    SampleDay nearestSampleDayFromDate = getNearestSampleDayFromDate(instance.getTime());
                    sampleDay.setStepGoal(nearestSampleDayFromDate.getStepGoal());
                    sampleDay.setActiveTimeGoal(nearestSampleDayFromDate.getActiveTimeGoal());
                    sampleDay.setCaloriesGoal(nearestSampleDayFromDate.getCaloriesGoal());
                    sampleDay.setCreatedAt(System.currentTimeMillis());
                    sampleDay.setUpdatedAt(System.currentTimeMillis());
                }
                MFLogger.d(TAG, "updateSampleDay - sample to createOrUpdate=" + sampleDay);
                getSampleDayDao().createOrUpdate(sampleDay);
            } catch (Exception e) {
                e.printStackTrace();
                MFLogger.e(TAG, "updateSampleDay - ex=" + e.toString());
            }
        }
    }

    @DexIgnore
    public void updateSampleDays(List<SampleDay> list) {
        MFLogger.d(TAG, "updateSampleDays - sampleDays=" + list);
        try {
            getSampleDayDao().callBatchTasks(new Anon2(list));
        } catch (Exception e) {
            e.printStackTrace();
            MFLogger.e(TAG, "updateSampleDays - ex=" + e.toString());
        }
    }

    @DexIgnore
    public void updateSampleRawPinType(String str, int i) {
        MFLogger.d(TAG, "updateSampleRawPinType - sampleRawId=" + str + ", pinType=" + i);
        try {
            SampleRaw queryForId = getSampleRawDao().queryForId(str);
            if (queryForId != null) {
                queryForId.setPinType(i);
                getSampleRawDao().update(queryForId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "updateSampleRawPinType - e=" + e.toString());
        }
    }

    @DexIgnore
    public void updateSampleRawUAPinType(String str, int i) {
        MFLogger.d(TAG, "updateSampleRawPinType - sampleRawId=" + str + ", uaPinType=" + i);
        try {
            SampleRaw queryForId = getSampleRawDao().queryForId(str);
            if (queryForId != null) {
                queryForId.setUaPinType(i);
                getSampleRawDao().update(queryForId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "updateSampleRawPinType - e=" + e.toString());
        }
    }

    @DexIgnore
    public void updateStepGoal(int i, int i2, int i3, int i4) {
        MFLogger.d(TAG, ".updateStepGoal");
        try {
            Dao<SampleDay, String> sampleDayDao = getSampleDayDao();
            SampleDay queryForFirst = sampleDayDao.queryBuilder().where().eq("day", Integer.valueOf(i2)).and().eq("month", Integer.valueOf(i3)).eq("year", Integer.valueOf(i4)).queryForFirst();
            if (queryForFirst != null) {
                queryForFirst.setStepGoal(i);
                sampleDayDao.update(queryForFirst);
            }
        } catch (Exception e) {
            MFLogger.e(TAG, ".updateStepGoal - e=" + e);
        }
    }

    @DexIgnore
    public void updateStepGoalSettings(int i) {
        MFLogger.d(TAG, ".updateStepGoalSettings");
        try {
            Dao<ActivitySettings, String> activitySettingsDao = getActivitySettingsDao();
            ActivitySettings queryForFirst = activitySettingsDao.queryBuilder().queryForFirst();
            if (queryForFirst != null) {
                queryForFirst.setCurrentStepGoal(i);
                activitySettingsDao.update(queryForFirst);
                return;
            }
            activitySettingsDao.create(new ActivitySettings(i, 1200, 3));
        } catch (Exception e) {
            MFLogger.e(TAG, ".updateStepGoalSettings - e=" + e);
        }
    }

    @DexIgnore
    public DailyGoal getDailyGoal(int i, int i2, int i3) {
        MFLogger.e(TAG, "getDailyGoal - year=" + i + ", month=" + i2 + ", day=" + i3);
        List<DailyGoal> arrayList = new ArrayList<>();
        try {
            Dao<DailyGoal, String> dailyGoalDao = getDailyGoalDao();
            QueryBuilder<DailyGoal, String> queryBuilder = dailyGoalDao.queryBuilder();
            Where<DailyGoal, String> where = queryBuilder.where();
            where.or(where.and(where.eq("year", Integer.valueOf(i)), where.eq("month", Integer.valueOf(i2)), where.le("day", Integer.valueOf(i3))), where.and(where.eq("year", Integer.valueOf(i)), where.lt("month", Integer.valueOf(i2)), new Where[0]), where.lt("year", Integer.valueOf(i)));
            queryBuilder.setWhere(where);
            queryBuilder.orderBy("year", false).orderBy("month", false).orderBy("day", false);
            queryBuilder.limit(1L);
            arrayList = dailyGoalDao.query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            MFLogger.e(TAG, "getDailyGoal - e=" + e.toString());
        } catch (Exception e2) {
            MFLogger.e(TAG, "getDailyGoal - ex=" + e2.toString());
        }
        if (arrayList.size() > 0) {
            return arrayList.get(0);
        }
        MFLogger.d(TAG, "getDailyGoal - goals=NULL!!!");
        return null;
    }

    @DexIgnore
    public SampleDay getSamplesForDay(Calendar calendar) {
        MFLogger.d(TAG, "getSamplesForDay - calendar=" + calendar);
        int i = calendar.get(1);
        return getSamplesForDay(calendar.get(5), calendar.get(2) + 1, i);
    }
}
