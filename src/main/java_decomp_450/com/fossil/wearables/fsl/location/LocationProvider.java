package com.fossil.wearables.fsl.location;

import com.fossil.wearables.fsl.BaseProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface LocationProvider extends BaseProvider {
    @DexIgnore
    DeviceLocation getDeviceLocation(String str);

    @DexIgnore
    void saveDeviceLocation(DeviceLocation deviceLocation);
}
