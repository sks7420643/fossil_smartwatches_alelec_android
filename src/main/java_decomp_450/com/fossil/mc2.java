package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.fitness.data.MapValue;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mc2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mc2> CREATOR; // = new sc2();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public float c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Map<String, MapValue> e;
    @DexIgnore
    public int[] f;
    @DexIgnore
    public float[] g;
    @DexIgnore
    public byte[] h;

    @DexIgnore
    public mc2(int i) {
        this(i, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, null, null, null, null);
    }

    @DexIgnore
    @Deprecated
    public final void a(int i) {
        a72.b(this.a == 1, "Attempting to set an int value to a field that is not in INT32 format.  Please check the data type definition and use the right format.");
        this.b = true;
        this.c = Float.intBitsToFloat(i);
    }

    @DexIgnore
    @Deprecated
    public final void b(String str) {
        a(zj2.a(str));
    }

    @DexIgnore
    public final float e() {
        a72.b(this.a == 2, "Value is not in float format");
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof mc2)) {
            return false;
        }
        mc2 mc2 = (mc2) obj;
        int i = this.a;
        if (i == mc2.a && this.b == mc2.b) {
            switch (i) {
                case 1:
                    if (g() == mc2.g()) {
                        return true;
                    }
                    break;
                case 2:
                    return this.c == mc2.c;
                case 3:
                    return y62.a(this.d, mc2.d);
                case 4:
                    return y62.a(this.e, mc2.e);
                case 5:
                    return Arrays.equals(this.f, mc2.f);
                case 6:
                    return Arrays.equals(this.g, mc2.g);
                case 7:
                    return Arrays.equals(this.h, mc2.h);
                default:
                    if (this.c == mc2.c) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    @DexIgnore
    public final int g() {
        boolean z = true;
        if (this.a != 1) {
            z = false;
        }
        a72.b(z, "Value is not in int format");
        return Float.floatToRawIntBits(this.c);
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(Float.valueOf(this.c), this.d, this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    public final String toString() {
        if (!this.b) {
            return "unset";
        }
        switch (this.a) {
            case 1:
                return Integer.toString(g());
            case 2:
                return Float.toString(this.c);
            case 3:
                return this.d;
            case 4:
                return new TreeMap(this.e).toString();
            case 5:
                return Arrays.toString(this.f);
            case 6:
                return Arrays.toString(this.g);
            case 7:
                byte[] bArr = this.h;
                return t92.a(bArr, 0, bArr.length, false);
            default:
                return "unknown";
        }
    }

    @DexIgnore
    public final int v() {
        return this.a;
    }

    @DexIgnore
    public final boolean w() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        Bundle bundle;
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, v());
        k72.a(parcel, 2, w());
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d, false);
        if (this.e == null) {
            bundle = null;
        } else {
            bundle = new Bundle(this.e.size());
            for (Map.Entry<String, MapValue> entry : this.e.entrySet()) {
                bundle.putParcelable(entry.getKey(), entry.getValue());
            }
        }
        k72.a(parcel, 5, bundle, false);
        k72.a(parcel, 6, this.f, false);
        k72.a(parcel, 7, this.g, false);
        k72.a(parcel, 8, this.h, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public mc2(int i, boolean z, float f2, String str, Bundle bundle, int[] iArr, float[] fArr, byte[] bArr) {
        n4 n4Var;
        this.a = i;
        this.b = z;
        this.c = f2;
        this.d = str;
        if (bundle == null) {
            n4Var = null;
        } else {
            bundle.setClassLoader(MapValue.class.getClassLoader());
            n4Var = new n4(bundle.size());
            for (String str2 : bundle.keySet()) {
                n4Var.put(str2, (MapValue) bundle.getParcelable(str2));
            }
        }
        this.e = n4Var;
        this.f = iArr;
        this.g = fArr;
        this.h = bArr;
    }

    @DexIgnore
    @Deprecated
    public final void a(float f2) {
        a72.b(this.a == 2, "Attempting to set an float value to a field that is not in FLOAT format.  Please check the data type definition and use the right format.");
        this.b = true;
        this.c = f2;
    }
}
