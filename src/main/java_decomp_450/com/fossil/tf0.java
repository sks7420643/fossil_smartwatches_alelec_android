package com.fossil;

import com.facebook.share.internal.VideoUploader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum tf0 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop");
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final tf0 a(String str) {
            tf0[] values = tf0.values();
            for (tf0 tf0 : values) {
                if (ee7.a((Object) tf0.a(), (Object) str)) {
                    return tf0;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public tf0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }
}
