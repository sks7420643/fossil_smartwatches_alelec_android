package com.fossil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw2 extends nu2<String> implements tw2, RandomAccess {
    @DexIgnore
    public static /* final */ qw2 c;
    @DexIgnore
    public /* final */ List<Object> b;

    /*
    static {
        qw2 qw2 = new qw2();
        c = qw2;
        qw2.zzb();
    }
    */

    @DexIgnore
    public qw2() {
        this(10);
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final void a(tu2 tu2) {
        a();
        this.b.add(tu2);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.b.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.nu2
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    @Override // com.fossil.nu2
    public final void clear() {
        a();
        this.b.clear();
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        Object obj = this.b.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof tu2) {
            tu2 tu2 = (tu2) obj;
            String zzb = tu2.zzb();
            if (tu2.zzc()) {
                this.b.set(i, zzb);
            }
            return zzb;
        }
        byte[] bArr = (byte[]) obj;
        String b2 = ew2.b(bArr);
        if (ew2.a(bArr)) {
            this.b.set(i, b2);
        }
        return b2;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.b.remove(i);
        ((AbstractList) this).modCount++;
        return a(remove);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return a(this.b.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }

    @DexIgnore
    @Override // com.fossil.jw2
    public final /* synthetic */ jw2 zza(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.b);
            return new qw2(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final Object zzb(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final List<?> zzd() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final tw2 zze() {
        return zza() ? new vy2(this) : this;
    }

    @DexIgnore
    public qw2(int i) {
        this(new ArrayList(i));
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList, com.fossil.nu2
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof tw2) {
            collection = ((tw2) collection).zzd();
        }
        boolean addAll = this.b.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @DexIgnore
    public qw2(ArrayList<Object> arrayList) {
        this.b = arrayList;
    }

    @DexIgnore
    public static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof tu2) {
            return ((tu2) obj).zzb();
        }
        return ew2.b((byte[]) obj);
    }
}
