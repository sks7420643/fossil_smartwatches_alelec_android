package com.fossil;

import android.location.Location;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherInfoWatchAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static gh5 l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public CustomizeRealDataRepository e;
    @DexIgnore
    public DianaPresetRepository f;
    @DexIgnore
    public GoogleApiService g;
    @DexIgnore
    public String h;
    @DexIgnore
    public Weather i;
    @DexIgnore
    public kn7 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final synchronized gh5 a() {
            gh5 g;
            if (gh5.l == null) {
                gh5.l = new gh5(null);
            }
            g = gh5.l;
            if (g == null) {
                ee7.a();
                throw null;
            }
            return g;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {251}, m = "getAddressBaseOnLocation")
    public static final class b extends rb7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gh5 gh5, fb7 fb7) {
            super(fb7);
            this.this$0 = gh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(0.0d, 0.0d, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1", f = "WeatherManager.kt", l = {251}, m = "invokeSuspend")
    public static final class c extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gh5 gh5, double d, double d2, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = gh5;
            this.$lat = d;
            this.$lng = d2;
            this.$type = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new c(this.this$0, this.$lat, this.$lng, this.$type, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((c) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GoogleApiService d = this.this$0.d();
                StringBuilder sb = new StringBuilder();
                sb.append(this.$lat);
                sb.append(',');
                sb.append(this.$lng);
                String sb2 = sb.toString();
                String str = this.$type;
                this.label = 1;
                obj = d.getAddressWithType(sb2, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {279}, m = "getWeather")
    public static final class d extends rb7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(gh5 gh5, fb7 fb7) {
            super(fb7);
            this.this$0 = gh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(0.0d, 0.0d, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$getWeather$repoResponse$1", f = "WeatherManager.kt", l = {279}, m = "invokeSuspend")
    public static final class e extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gh5 gh5, double d, double d2, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = gh5;
            this.$lat = d;
            this.$lng = d2;
            this.$tempUnit = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new e(this.this$0, this.$lat, this.$lng, this.$tempUnit, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((e) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a();
                String valueOf = String.valueOf(this.$lat);
                String valueOf2 = String.valueOf(this.$lng);
                String str = this.$tempUnit;
                this.label = 1;
                obj = a2.getWeather(valueOf, valueOf2, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $currentLocation;
        @DexIgnore
        public /* final */ /* synthetic */ oe7 $isFromCaches;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit$inlined;
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, double d, double d2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$currentLat, this.$currentLong, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends Weather, ? extends Boolean>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    f fVar = this.this$0;
                    gh5 gh5 = fVar.this$0;
                    double d = this.$currentLat;
                    double d2 = this.$currentLong;
                    String str = fVar.$tempUnit$inlined;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gh5.a(d, d2, str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, double d, double d2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$currentLat, this.$currentLong, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    gh5 gh5 = this.this$0.this$0;
                    double d = this.$currentLat;
                    double d2 = this.$currentLong;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gh5.a(d, d2, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(f fVar, double d, double d2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, this.$currentLat, this.$currentLong, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    gh5 gh5 = this.this$0.this$0;
                    double d = this.$currentLat;
                    double d2 = this.$currentLong;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gh5.a(d, d2, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(Location location, oe7 oe7, fb7 fb7, gh5 gh5, fb7 fb72, String str) {
            super(2, fb7);
            this.$currentLocation = location;
            this.$isFromCaches = oe7;
            this.this$0 = gh5;
            this.$continuation$inlined = fb72;
            this.$tempUnit$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.$currentLocation, this.$isFromCaches, fb7, this.this$0, this.$continuation$inlined, this.$tempUnit$inlined);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:37:0x01f2  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x01fd  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r31) {
            /*
                r30 = this;
                r7 = r30
                java.lang.Object r8 = com.fossil.nb7.a()
                int r0 = r7.label
                r9 = 0
                r10 = 3
                r11 = 2
                r12 = 1
                if (r0 == 0) goto L_0x0070
                if (r0 == r12) goto L_0x0054
                if (r0 == r11) goto L_0x0038
                if (r0 != r10) goto L_0x0030
                java.lang.Object r0 = r7.L$3
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r0 = r7.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$1
                java.lang.String r0 = (java.lang.String) r0
                double r0 = r7.D$1
                double r2 = r7.D$0
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r31)
                r4 = r0
                r0 = r31
                goto L_0x01b9
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r7.L$3
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r0 = r7.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$1
                java.lang.String r0 = (java.lang.String) r0
                double r0 = r7.D$1
                double r2 = r7.D$0
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r31)
                r4 = r0
                r0 = r31
                goto L_0x016a
            L_0x0054:
                java.lang.Object r0 = r7.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r7.L$1
                r9 = r1
                java.lang.String r9 = (java.lang.String) r9
                double r1 = r7.D$1
                double r3 = r7.D$0
                java.lang.Object r5 = r7.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r31)
                r12 = r0
                r14 = r3
                r13 = r5
                r11 = r9
                r9 = r1
                r2 = r31
                goto L_0x00bb
            L_0x0070:
                com.fossil.t87.a(r31)
                com.fossil.yi7 r13 = r7.p$
                android.location.Location r0 = r7.$currentLocation
                if (r0 == 0) goto L_0x0210
                double r14 = r0.getLatitude()
                android.location.Location r0 = r7.$currentLocation
                double r4 = r0.getLongitude()
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.LocationSource r0 = r0.e()
                java.util.List r6 = r0.getWeatherAddressList()
                com.fossil.ti7 r2 = com.fossil.qj7.a()
                com.fossil.gh5$f$a r3 = new com.fossil.gh5$f$a
                r16 = 0
                r0 = r3
                r1 = r30
                r10 = r2
                r11 = r3
                r2 = r14
                r18 = r4
                r12 = r6
                r6 = r16
                r0.<init>(r1, r2, r4, r6)
                r7.L$0 = r13
                r7.D$0 = r14
                r0 = r18
                r7.D$1 = r0
                r7.L$1 = r9
                r7.L$2 = r12
                r2 = 1
                r7.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r10, r11, r7)
                if (r2 != r8) goto L_0x00b9
                return r8
            L_0x00b9:
                r11 = r9
                r9 = r0
            L_0x00bb:
                r6 = r2
                com.fossil.r87 r6 = (com.fossil.r87) r6
                com.fossil.gh5 r0 = r7.this$0
                java.lang.Object r1 = r6.getFirst()
                com.portfolio.platform.data.model.microapp.weather.Weather r1 = (com.portfolio.platform.data.model.microapp.weather.Weather) r1
                r0.i = r1
                com.fossil.oe7 r0 = r7.$isFromCaches
                java.lang.Object r1 = r6.getSecond()
                java.lang.Boolean r1 = (java.lang.Boolean) r1
                boolean r1 = r1.booleanValue()
                r0.element = r1
                boolean r0 = r12.isEmpty()
                r1 = 1
                r0 = r0 ^ r1
                if (r0 == 0) goto L_0x017e
                java.util.Iterator r0 = r12.iterator()
            L_0x00e3:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x0121
                java.lang.Object r1 = r0.next()
                com.portfolio.platform.data.model.microapp.weather.AddressOfWeather r1 = (com.portfolio.platform.data.model.microapp.weather.AddressOfWeather) r1
                com.fossil.gh5 r2 = r7.this$0
                r20 = r2
                r21 = r1
                r22 = r14
                r24 = r9
                boolean r2 = r20.a(r21, r22, r24)
                if (r2 == 0) goto L_0x00e3
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = com.fossil.gh5.k
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "in acceptable range, saved address="
                r3.append(r4)
                r3.append(r1)
                java.lang.String r3 = r3.toString()
                r0.d(r2, r3)
                java.lang.String r11 = r1.getAddress()
            L_0x0121:
                if (r11 != 0) goto L_0x01cc
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gh5.k
                java.lang.String r2 = "getWeatherBaseOnLocation(), there isn't any saved address near from current lat lng"
                r0.d(r1, r2)
                com.fossil.ti7 r4 = com.fossil.qj7.a()
                com.fossil.gh5$f$b r5 = new com.fossil.gh5$f$b
                r16 = 0
                r0 = r5
                r1 = r30
                r2 = r14
                r17 = r4
                r18 = r8
                r8 = r5
                r4 = r9
                r19 = r8
                r8 = r6
                r6 = r16
                r0.<init>(r1, r2, r4, r6)
                r7.L$0 = r13
                r7.D$0 = r14
                r7.D$1 = r9
                r7.L$1 = r11
                r7.L$2 = r12
                r7.L$3 = r8
                r0 = 2
                r7.label = r0
                r0 = r17
                r1 = r19
                java.lang.Object r0 = com.fossil.vh7.a(r0, r1, r7)
                r6 = r18
                if (r0 != r6) goto L_0x0168
                return r6
            L_0x0168:
                r4 = r9
                r2 = r14
            L_0x016a:
                r11 = r0
                java.lang.String r11 = (java.lang.String) r11
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.LocationSource r0 = r0.e()
                com.portfolio.platform.data.model.microapp.weather.AddressOfWeather r8 = new com.portfolio.platform.data.model.microapp.weather.AddressOfWeather
                r1 = r8
                r6 = r11
                r1.<init>(r2, r4, r6)
                r0.saveWeatherAddress(r8)
                goto L_0x01cc
            L_0x017e:
                r29 = r8
                r8 = r6
                r6 = r29
                com.fossil.ti7 r4 = com.fossil.qj7.a()
                com.fossil.gh5$f$c r5 = new com.fossil.gh5$f$c
                r16 = 0
                r0 = r5
                r1 = r30
                r2 = r14
                r26 = r4
                r27 = r5
                r4 = r9
                r28 = r6
                r6 = r16
                r0.<init>(r1, r2, r4, r6)
                r7.L$0 = r13
                r7.D$0 = r14
                r7.D$1 = r9
                r7.L$1 = r11
                r7.L$2 = r12
                r7.L$3 = r8
                r0 = 3
                r7.label = r0
                r0 = r26
                r1 = r27
                java.lang.Object r0 = com.fossil.vh7.a(r0, r1, r7)
                r1 = r28
                if (r0 != r1) goto L_0x01b7
                return r1
            L_0x01b7:
                r4 = r9
                r2 = r14
            L_0x01b9:
                r11 = r0
                java.lang.String r11 = (java.lang.String) r11
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.LocationSource r0 = r0.e()
                com.portfolio.platform.data.model.microapp.weather.AddressOfWeather r8 = new com.portfolio.platform.data.model.microapp.weather.AddressOfWeather
                r1 = r8
                r6 = r11
                r1.<init>(r2, r4, r6)
                r0.saveWeatherAddress(r8)
            L_0x01cc:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gh5.k
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "getWeatherBaseOnLocation(), currentAddress = "
                r2.append(r3)
                r2.append(r11)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = r0.i
                if (r0 == 0) goto L_0x01f5
                r0.setAddress(r11)
            L_0x01f5:
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = r0.i
                if (r0 == 0) goto L_0x020d
                java.util.Calendar r1 = java.util.Calendar.getInstance()
                java.lang.String r2 = "Calendar.getInstance()"
                com.fossil.ee7.a(r1, r2)
                long r1 = r1.getTimeInMillis()
                r0.setUpdatedAt(r1)
            L_0x020d:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x0210:
                com.fossil.ee7.a()
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {366, 188, 193}, m = "getWeatherBaseOnLocation")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(gh5 gh5, fb7 fb7) {
            super(fb7);
            this.this$0 = gh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForChanceOfRain$2", f = "WeatherManager.kt", l = {149, 155, 159}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, fb7 fb7, h hVar) {
                super(2, fb7);
                this.$tempUnit = str;
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$tempUnit, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends Weather, ? extends Boolean>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    gh5 gh5 = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gh5.a("chance-of-rain", str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(gh5 gh5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gh5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$serial, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00e6  */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x004d
                if (r1 == r4) goto L_0x0044
                if (r1 == r3) goto L_0x0033
                if (r1 != r2) goto L_0x002b
                java.lang.Object r0 = r9.L$4
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                java.lang.Object r0 = r9.L$3
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r9.L$2
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                java.lang.Object r0 = r9.L$1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r9.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r10)
                goto L_0x00fd
            L_0x002b:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x0033:
                java.lang.Object r1 = r9.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r3 = r9.L$1
                com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
                java.lang.Object r4 = r9.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r10)
                goto L_0x00d2
            L_0x0044:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                r4 = r1
                goto L_0x007b
            L_0x004d:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r10 = r9.p$
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.String r6 = com.fossil.gh5.k
                java.lang.String r7 = "getWeatherForChanceOfRain"
                r1.d(r6, r7)
                com.fossil.gh5 r1 = r9.this$0
                java.lang.String r6 = r9.$serial
                r1.h = r6
                com.fossil.gh5 r1 = r9.this$0
                com.portfolio.platform.data.source.UserRepository r1 = r1.f()
                r9.L$0 = r10
                r9.label = r4
                java.lang.Object r1 = r1.getCurrentUser(r9)
                if (r1 != r0) goto L_0x0079
                return r0
            L_0x0079:
                r4 = r10
                r10 = r1
            L_0x007b:
                com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
                if (r10 == 0) goto L_0x0100
                com.portfolio.platform.data.model.MFUser$UnitGroup r1 = r10.getUnitGroup()
                if (r1 == 0) goto L_0x008a
                java.lang.String r1 = r1.getTemperature()
                goto L_0x008b
            L_0x008a:
                r1 = r5
            L_0x008b:
                com.fossil.ob5 r6 = com.fossil.ob5.METRIC
                java.lang.String r6 = r6.getValue()
                boolean r6 = com.fossil.ee7.a(r1, r6)
                if (r6 == 0) goto L_0x009e
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r1 = r1.getValue()
                goto L_0x00b7
            L_0x009e:
                com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
                java.lang.String r6 = r6.getValue()
                boolean r1 = com.fossil.ee7.a(r1, r6)
                if (r1 == 0) goto L_0x00b1
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT
                java.lang.String r1 = r1.getValue()
                goto L_0x00b7
            L_0x00b1:
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r1 = r1.getValue()
            L_0x00b7:
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.gh5$h$a r7 = new com.fossil.gh5$h$a
                r7.<init>(r1, r5, r9)
                r9.L$0 = r4
                r9.L$1 = r10
                r9.L$2 = r1
                r9.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r6, r7, r9)
                if (r3 != r0) goto L_0x00cf
                return r0
            L_0x00cf:
                r8 = r3
                r3 = r10
                r10 = r8
            L_0x00d2:
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r6 = r10.component1()
                com.portfolio.platform.data.model.microapp.weather.Weather r6 = (com.portfolio.platform.data.model.microapp.weather.Weather) r6
                java.lang.Object r10 = r10.component2()
                java.lang.Boolean r10 = (java.lang.Boolean) r10
                boolean r10 = r10.booleanValue()
                if (r6 == 0) goto L_0x0100
                com.fossil.gh5 r5 = r9.this$0
                r9.L$0 = r4
                r9.L$1 = r3
                r9.L$2 = r6
                r9.L$3 = r1
                r9.Z$0 = r10
                r9.L$4 = r6
                r9.label = r2
                java.lang.Object r10 = r5.a(r6, r10, r9)
                if (r10 != r0) goto L_0x00fd
                return r0
            L_0x00fd:
                com.fossil.i97 r10 = com.fossil.i97.a
                r5 = r10
            L_0x0100:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$2", f = "WeatherManager.kt", l = {167, 173, 177}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, fb7 fb7, i iVar) {
                super(2, fb7);
                this.$tempUnit = str;
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$tempUnit, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends Weather, ? extends Boolean>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    gh5 gh5 = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gh5.a("weather", str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(gh5 gh5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gh5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, this.$serial, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00e6  */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x004d
                if (r1 == r4) goto L_0x0044
                if (r1 == r3) goto L_0x0033
                if (r1 != r2) goto L_0x002b
                java.lang.Object r0 = r9.L$4
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                java.lang.Object r0 = r9.L$3
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r9.L$2
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                java.lang.Object r0 = r9.L$1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r0 = r9.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r10)
                goto L_0x00fd
            L_0x002b:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x0033:
                java.lang.Object r1 = r9.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r3 = r9.L$1
                com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
                java.lang.Object r4 = r9.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r10)
                goto L_0x00d2
            L_0x0044:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                r4 = r1
                goto L_0x007b
            L_0x004d:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r10 = r9.p$
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.String r6 = com.fossil.gh5.k
                java.lang.String r7 = "getWeatherForTemperature"
                r1.d(r6, r7)
                com.fossil.gh5 r1 = r9.this$0
                java.lang.String r6 = r9.$serial
                r1.h = r6
                com.fossil.gh5 r1 = r9.this$0
                com.portfolio.platform.data.source.UserRepository r1 = r1.f()
                r9.L$0 = r10
                r9.label = r4
                java.lang.Object r1 = r1.getCurrentUser(r9)
                if (r1 != r0) goto L_0x0079
                return r0
            L_0x0079:
                r4 = r10
                r10 = r1
            L_0x007b:
                com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
                if (r10 == 0) goto L_0x0100
                com.portfolio.platform.data.model.MFUser$UnitGroup r1 = r10.getUnitGroup()
                if (r1 == 0) goto L_0x008a
                java.lang.String r1 = r1.getTemperature()
                goto L_0x008b
            L_0x008a:
                r1 = r5
            L_0x008b:
                com.fossil.ob5 r6 = com.fossil.ob5.METRIC
                java.lang.String r6 = r6.getValue()
                boolean r6 = com.fossil.ee7.a(r1, r6)
                if (r6 == 0) goto L_0x009e
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r1 = r1.getValue()
                goto L_0x00b7
            L_0x009e:
                com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
                java.lang.String r6 = r6.getValue()
                boolean r1 = com.fossil.ee7.a(r1, r6)
                if (r1 == 0) goto L_0x00b1
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT
                java.lang.String r1 = r1.getValue()
                goto L_0x00b7
            L_0x00b1:
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r1 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r1 = r1.getValue()
            L_0x00b7:
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.gh5$i$a r7 = new com.fossil.gh5$i$a
                r7.<init>(r1, r5, r9)
                r9.L$0 = r4
                r9.L$1 = r10
                r9.L$2 = r1
                r9.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r6, r7, r9)
                if (r3 != r0) goto L_0x00cf
                return r0
            L_0x00cf:
                r8 = r3
                r3 = r10
                r10 = r8
            L_0x00d2:
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r6 = r10.component1()
                com.portfolio.platform.data.model.microapp.weather.Weather r6 = (com.portfolio.platform.data.model.microapp.weather.Weather) r6
                java.lang.Object r10 = r10.component2()
                java.lang.Boolean r10 = (java.lang.Boolean) r10
                boolean r10 = r10.booleanValue()
                if (r6 == 0) goto L_0x0100
                com.fossil.gh5 r5 = r9.this$0
                r9.L$0 = r4
                r9.L$1 = r3
                r9.L$2 = r6
                r9.L$3 = r1
                r9.Z$0 = r10
                r9.L$4 = r6
                r9.label = r2
                java.lang.Object r10 = r5.b(r6, r10, r9)
                if (r10 != r0) goto L_0x00fd
                return r0
            L_0x00fd:
                com.fossil.i97 r10 = com.fossil.i97.a
                r5 = r10
            L_0x0100:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$2", f = "WeatherManager.kt", l = {83, 122, 130, 134}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WeatherLocationWrapper $location;
            @DexIgnore
            public /* final */ /* synthetic */ se7 $mWeatherWatchAppSetting$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ yi7 $this_withContext$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(WeatherLocationWrapper weatherLocationWrapper, fb7 fb7, String str, j jVar, yi7 yi7, se7 se7) {
                super(2, fb7);
                this.$location = weatherLocationWrapper;
                this.$tempUnit$inlined = str;
                this.this$0 = jVar;
                this.$this_withContext$inlined = yi7;
                this.$mWeatherWatchAppSetting$inlined = se7;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$location, fb7, this.$tempUnit$inlined, this.this$0, this.$this_withContext$inlined, this.$mWeatherWatchAppSetting$inlined);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends Weather, ? extends Boolean>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    WeatherLocationWrapper weatherLocationWrapper = this.$location;
                    Boolean a2 = weatherLocationWrapper != null ? pb7.a(weatherLocationWrapper.isUseCurrentLocation()) : null;
                    if (a2 == null) {
                        ee7.a();
                        throw null;
                    } else if (a2.booleanValue()) {
                        gh5 gh5 = this.this$0.this$0;
                        String str = this.$tempUnit$inlined;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = gh5.a("weather", str, this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        gh5 gh52 = this.this$0.this$0;
                        double lat = this.$location.getLat();
                        double lng = this.$location.getLng();
                        String str2 = this.$tempUnit$inlined;
                        this.L$0 = yi7;
                        this.label = 2;
                        obj = gh52.a(lat, lng, str2, this);
                        if (obj == a) {
                            return a;
                        }
                        return (r87) obj;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi73 = (yi7) this.L$0;
                    t87.a(obj);
                    return (r87) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (r87) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(gh5 gh5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gh5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, this.$serial, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:107:0x0421  */
        /* JADX WARNING: Removed duplicated region for block: B:108:0x0426  */
        /* JADX WARNING: Removed duplicated region for block: B:111:0x042e  */
        /* JADX WARNING: Removed duplicated region for block: B:112:0x0440  */
        /* JADX WARNING: Removed duplicated region for block: B:115:0x044c  */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x045b  */
        /* JADX WARNING: Removed duplicated region for block: B:119:0x0476  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x02ab  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0304  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x0390  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r31) {
            /*
                r30 = this;
                r7 = r30
                java.lang.Object r8 = com.fossil.nb7.a()
                int r0 = r7.label
                r9 = 0
                r10 = 4
                r11 = 3
                r12 = 2
                r13 = 1
                if (r0 == 0) goto L_0x0102
                if (r0 == r13) goto L_0x00f4
                if (r0 == r12) goto L_0x00ac
                if (r0 == r11) goto L_0x0065
                if (r0 != r10) goto L_0x005d
                int r0 = r7.I$1
                int r1 = r7.I$0
                java.lang.Object r2 = r7.L$12
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r2 = r7.L$11
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r3 = r7.L$10
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r3 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r3
                java.lang.Object r4 = r7.L$9
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r5 = r7.L$8
                com.fossil.r87 r5 = (com.fossil.r87) r5
                java.lang.Object r6 = r7.L$7
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r6 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r6
                java.lang.Object r9 = r7.L$6
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r9
                java.lang.Object r15 = r7.L$5
                com.portfolio.platform.data.model.diana.preset.DianaPreset r15 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r15
                java.lang.Object r11 = r7.L$4
                java.lang.String r11 = (java.lang.String) r11
                java.lang.Object r10 = r7.L$3
                com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
                java.lang.Object r12 = r7.L$2
                com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
                java.lang.Object r13 = r7.L$1
                com.fossil.se7 r13 = (com.fossil.se7) r13
                java.lang.Object r14 = r7.L$0
                com.fossil.yi7 r14 = (com.fossil.yi7) r14
                com.fossil.t87.a(r31)
                r18 = r0
                r0 = r31
                r29 = r8
                r8 = r7
                r7 = r29
                goto L_0x02f4
            L_0x005d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0065:
                int r0 = r7.I$1
                int r1 = r7.I$0
                java.lang.Object r2 = r7.L$12
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r3 = r7.L$11
                com.fossil.r87 r3 = (com.fossil.r87) r3
                java.lang.Object r3 = r7.L$10
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r3 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r3
                java.lang.Object r4 = r7.L$9
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r5 = r7.L$8
                com.fossil.r87 r5 = (com.fossil.r87) r5
                java.lang.Object r6 = r7.L$7
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r6 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r6
                java.lang.Object r9 = r7.L$6
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r9
                java.lang.Object r10 = r7.L$5
                com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
                java.lang.Object r11 = r7.L$4
                java.lang.String r11 = (java.lang.String) r11
                java.lang.Object r12 = r7.L$3
                com.portfolio.platform.data.model.diana.preset.DianaPreset r12 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r12
                java.lang.Object r13 = r7.L$2
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                java.lang.Object r14 = r7.L$1
                com.fossil.se7 r14 = (com.fossil.se7) r14
                java.lang.Object r15 = r7.L$0
                com.fossil.yi7 r15 = (com.fossil.yi7) r15
                com.fossil.t87.a(r31)
                r18 = r2
                r2 = r0
                r0 = r31
                r29 = r8
                r8 = r7
                r7 = r29
                goto L_0x0380
            L_0x00ac:
                int r0 = r7.I$1
                int r1 = r7.I$0
                java.lang.Object r2 = r7.L$12
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r3 = r7.L$11
                com.fossil.r87 r3 = (com.fossil.r87) r3
                java.lang.Object r4 = r7.L$10
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r4 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r4
                java.lang.Object r5 = r7.L$9
                java.util.List r5 = (java.util.List) r5
                java.lang.Object r6 = r7.L$8
                com.fossil.r87 r6 = (com.fossil.r87) r6
                java.lang.Object r6 = r7.L$7
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r6 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r6
                java.lang.Object r9 = r7.L$6
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r9
                java.lang.Object r10 = r7.L$5
                com.portfolio.platform.data.model.diana.preset.DianaPreset r10 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r10
                java.lang.Object r11 = r7.L$4
                java.lang.String r11 = (java.lang.String) r11
                java.lang.Object r12 = r7.L$3
                com.portfolio.platform.data.model.diana.preset.DianaPreset r12 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r12
                java.lang.Object r13 = r7.L$2
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                java.lang.Object r14 = r7.L$1
                com.fossil.se7 r14 = (com.fossil.se7) r14
                java.lang.Object r15 = r7.L$0
                com.fossil.yi7 r15 = (com.fossil.yi7) r15
                com.fossil.t87.a(r31)
                r17 = r15
                r15 = r9
                r9 = r1
                r1 = r31
                r29 = r8
                r8 = r7
                r7 = r29
                goto L_0x0417
            L_0x00f4:
                java.lang.Object r0 = r7.L$1
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r31)
                r2 = r31
                goto L_0x0139
            L_0x0102:
                com.fossil.t87.a(r31)
                com.fossil.yi7 r1 = r7.p$
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = com.fossil.gh5.k
                java.lang.String r3 = "getWeatherForWatchApp"
                r0.d(r2, r3)
                com.fossil.se7 r0 = new com.fossil.se7
                r0.<init>()
                r2 = 0
                r0.element = r2
                com.fossil.gh5 r2 = r7.this$0
                java.lang.String r3 = r7.$serial
                r2.h = r3
                com.fossil.gh5 r2 = r7.this$0
                com.portfolio.platform.data.source.UserRepository r2 = r2.f()
                r7.L$0 = r1
                r7.L$1 = r0
                r3 = 1
                r7.label = r3
                java.lang.Object r2 = r2.getCurrentUser(r7)
                if (r2 != r8) goto L_0x0139
                return r8
            L_0x0139:
                r10 = r0
                r11 = r1
                r12 = r2
                com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
                if (r12 == 0) goto L_0x048d
                com.portfolio.platform.data.model.MFUser$UnitGroup r0 = r12.getUnitGroup()
                if (r0 == 0) goto L_0x014b
                java.lang.String r0 = r0.getTemperature()
                goto L_0x014c
            L_0x014b:
                r0 = 0
            L_0x014c:
                com.fossil.ob5 r1 = com.fossil.ob5.METRIC
                java.lang.String r1 = r1.getValue()
                boolean r1 = com.fossil.ee7.a(r0, r1)
                if (r1 == 0) goto L_0x0160
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r0 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r0 = r0.getValue()
            L_0x015e:
                r13 = r0
                goto L_0x017a
            L_0x0160:
                com.fossil.ob5 r1 = com.fossil.ob5.IMPERIAL
                java.lang.String r1 = r1.getValue()
                boolean r0 = com.fossil.ee7.a(r0, r1)
                if (r0 == 0) goto L_0x0173
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r0 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT
                java.lang.String r0 = r0.getValue()
                goto L_0x015e
            L_0x0173:
                com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r0 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.CELSIUS
                java.lang.String r0 = r0.getValue()
                goto L_0x015e
            L_0x017a:
                com.fossil.gh5 r0 = r7.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r0 = r0.c()
                com.fossil.gh5 r1 = r7.this$0
                java.lang.String r1 = r1.h
                if (r1 == 0) goto L_0x0488
                com.portfolio.platform.data.model.diana.preset.DianaPreset r14 = r0.getActivePresetBySerial(r1)
                if (r14 == 0) goto L_0x048d
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gh5.k
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "getWeatherForWatchApp activePreset "
                r2.append(r3)
                r2.append(r14)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                java.util.ArrayList r0 = r14.getWatchapps()
                java.util.Iterator r0 = r0.iterator()
            L_0x01b4:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x01d6
                java.lang.Object r1 = r0.next()
                r2 = r1
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r2
                java.lang.String r2 = r2.getId()
                java.lang.String r3 = "weather"
                boolean r2 = com.fossil.ee7.a(r2, r3)
                java.lang.Boolean r2 = com.fossil.pb7.a(r2)
                boolean r2 = r2.booleanValue()
                if (r2 == 0) goto L_0x01b4
                goto L_0x01d7
            L_0x01d6:
                r1 = 0
            L_0x01d7:
                r15 = r1
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r15 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r15
                if (r15 == 0) goto L_0x048d
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gh5.k
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "getWeatherForWatchApp settings="
                r2.append(r3)
                java.lang.String r3 = r15.getSettings()
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                java.lang.String r0 = r15.getSettings()
                boolean r0 = com.fossil.sc5.a(r0)
                if (r0 != 0) goto L_0x021b
                com.google.gson.Gson r0 = new com.google.gson.Gson
                r0.<init>()
                java.lang.String r1 = r15.getSettings()
                java.lang.Class<com.portfolio.platform.data.model.setting.WeatherWatchAppSetting> r2 = com.portfolio.platform.data.model.setting.WeatherWatchAppSetting.class
                java.lang.Object r0 = r0.a(r1, r2)
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r0 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r0
                r10.element = r0
            L_0x021b:
                T r0 = r10.element
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r0 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r0
                if (r0 != 0) goto L_0x0228
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r0 = new com.portfolio.platform.data.model.setting.WeatherWatchAppSetting
                r0.<init>()
                r10.element = r0
            L_0x0228:
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r6 = new com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper
                r19 = 0
                r21 = 0
                r23 = 0
                r24 = 0
                r25 = 1
                r26 = 0
                r27 = 94
                r28 = 0
                java.lang.String r18 = ""
                r17 = r6
                r17.<init>(r18, r19, r21, r23, r24, r25, r26, r27, r28)
                T r0 = r10.element
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r0 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r0
                if (r0 == 0) goto L_0x0483
                java.util.List r0 = r0.getLocations()
                r0.add(r9, r6)
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                T r0 = r10.element
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r0 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r0
                if (r0 == 0) goto L_0x047e
                java.util.List r0 = r0.getLocations()
                java.util.Iterator r23 = r0.iterator()
            L_0x0261:
                boolean r0 = r23.hasNext()
                if (r0 == 0) goto L_0x0294
                java.lang.Object r0 = r23.next()
                r1 = r0
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r1 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r1
                r18 = 0
                r19 = 0
                com.fossil.gh5$j$a r20 = new com.fossil.gh5$j$a
                r2 = 0
                r0 = r20
                r3 = r13
                r4 = r30
                r9 = r5
                r5 = r11
                r25 = r6
                r6 = r10
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r21 = 3
                r22 = 0
                r17 = r11
                com.fossil.hj7 r0 = com.fossil.xh7.a(r17, r18, r19, r20, r21, r22)
                r9.add(r0)
                r5 = r9
                r6 = r25
                r9 = 0
                goto L_0x0261
            L_0x0294:
                r9 = r5
                r25 = r6
                int r0 = r9.size()
                r17 = r8
                r4 = r9
                r3 = r15
                r1 = 0
                r2 = 0
                r5 = 0
                r9 = 0
                r8 = r7
                r7 = r14
                r14 = r11
                r11 = r13
                r13 = r10
                r10 = r7
            L_0x02a9:
                if (r9 >= r0) goto L_0x0476
                if (r9 == 0) goto L_0x03cb
                r18 = r0
                r0 = 1
                if (r9 == r0) goto L_0x033b
                r0 = 2
                if (r9 == r0) goto L_0x02bb
                r0 = r18
                r16 = 1
                goto L_0x0472
            L_0x02bb:
                java.lang.Object r0 = r4.get(r9)
                com.fossil.hj7 r0 = (com.fossil.hj7) r0
                r8.L$0 = r14
                r8.L$1 = r13
                r8.L$2 = r12
                r8.L$3 = r10
                r8.L$4 = r11
                r8.L$5 = r7
                r8.L$6 = r15
                r8.L$7 = r6
                r8.L$8 = r5
                r8.L$9 = r4
                r8.L$10 = r3
                r8.L$11 = r2
                r8.L$12 = r1
                r8.I$0 = r9
                r1 = r18
                r8.I$1 = r1
                r1 = 4
                r8.label = r1
                java.lang.Object r0 = r0.c(r8)
                r1 = r17
                if (r0 != r1) goto L_0x02ed
                return r1
            L_0x02ed:
                r29 = r7
                r7 = r1
                r1 = r9
                r9 = r15
                r15 = r29
            L_0x02f4:
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r17 = r0.getFirst()
                r31 = r0
                r0 = r17
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                r17 = r2
                if (r0 == 0) goto L_0x032c
                T r2 = r13.element
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r2 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r2
                if (r2 == 0) goto L_0x0327
                java.util.List r2 = r2.getLocations()
                java.lang.Object r2 = r2.get(r1)
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r2 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r2
                if (r2 == 0) goto L_0x031b
                java.lang.String r2 = r2.getName()
                goto L_0x031c
            L_0x031b:
                r2 = 0
            L_0x031c:
                if (r2 == 0) goto L_0x0322
                r0.setAddress(r2)
                goto L_0x032c
            L_0x0322:
                com.fossil.ee7.a()
                r0 = 0
                throw r0
            L_0x0327:
                r0 = 0
                com.fossil.ee7.a()
                throw r0
            L_0x032c:
                r2 = r17
                r0 = r18
                r16 = 1
                r17 = r7
                r7 = r15
                r15 = r9
                r9 = r1
                r1 = r31
                goto L_0x0472
            L_0x033b:
                r0 = r18
                java.lang.Object r18 = r4.get(r9)
                r19 = r0
                r0 = r18
                com.fossil.hj7 r0 = (com.fossil.hj7) r0
                r8.L$0 = r14
                r8.L$1 = r13
                r8.L$2 = r12
                r8.L$3 = r10
                r8.L$4 = r11
                r8.L$5 = r7
                r8.L$6 = r15
                r8.L$7 = r6
                r8.L$8 = r5
                r8.L$9 = r4
                r8.L$10 = r3
                r8.L$11 = r2
                r8.L$12 = r1
                r8.I$0 = r9
                r2 = r19
                r8.I$1 = r2
                r18 = r1
                r1 = 3
                r8.label = r1
                java.lang.Object r0 = r0.c(r8)
                r1 = r17
                if (r0 != r1) goto L_0x0375
                return r1
            L_0x0375:
                r29 = r7
                r7 = r1
                r1 = r9
                r9 = r15
                r15 = r14
                r14 = r13
                r13 = r12
                r12 = r10
                r10 = r29
            L_0x0380:
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r17 = r0.getFirst()
                r31 = r0
                r0 = r17
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                r17 = r2
                if (r0 == 0) goto L_0x03b8
                T r2 = r14.element
                com.portfolio.platform.data.model.setting.WeatherWatchAppSetting r2 = (com.portfolio.platform.data.model.setting.WeatherWatchAppSetting) r2
                if (r2 == 0) goto L_0x03b3
                java.util.List r2 = r2.getLocations()
                java.lang.Object r2 = r2.get(r1)
                com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper r2 = (com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper) r2
                if (r2 == 0) goto L_0x03a7
                java.lang.String r2 = r2.getName()
                goto L_0x03a8
            L_0x03a7:
                r2 = 0
            L_0x03a8:
                if (r2 == 0) goto L_0x03ae
                r0.setAddress(r2)
                goto L_0x03b8
            L_0x03ae:
                com.fossil.ee7.a()
                r0 = 0
                throw r0
            L_0x03b3:
                r0 = 0
                com.fossil.ee7.a()
                throw r0
            L_0x03b8:
                r2 = r31
                r0 = r17
                r16 = 1
                r17 = r7
                r7 = r10
                r10 = r12
                r12 = r13
                r13 = r14
                r14 = r15
                r15 = r9
                r9 = r1
                r1 = r18
                goto L_0x0472
            L_0x03cb:
                r18 = r1
                r1 = r17
                java.lang.Object r17 = r4.get(r9)
                r19 = r1
                r1 = r17
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                r8.L$0 = r14
                r8.L$1 = r13
                r8.L$2 = r12
                r8.L$3 = r10
                r8.L$4 = r11
                r8.L$5 = r7
                r8.L$6 = r15
                r8.L$7 = r6
                r8.L$8 = r5
                r8.L$9 = r4
                r8.L$10 = r3
                r8.L$11 = r2
                r5 = r18
                r8.L$12 = r5
                r8.I$0 = r9
                r8.I$1 = r0
                r18 = r0
                r0 = 2
                r8.label = r0
                java.lang.Object r1 = r1.c(r8)
                r0 = r19
                if (r1 != r0) goto L_0x0407
                return r0
            L_0x0407:
                r17 = r14
                r14 = r13
                r13 = r12
                r12 = r10
                r10 = r7
                r7 = r0
                r0 = r18
                r29 = r3
                r3 = r2
                r2 = r5
                r5 = r4
                r4 = r29
            L_0x0417:
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r18 = r1.getFirst()
                com.portfolio.platform.data.model.microapp.weather.Weather r18 = (com.portfolio.platform.data.model.microapp.weather.Weather) r18
                if (r18 == 0) goto L_0x0426
                java.lang.String r18 = r18.getAddress()
                goto L_0x0428
            L_0x0426:
                r18 = 0
            L_0x0428:
                boolean r19 = android.text.TextUtils.isEmpty(r18)
                if (r19 == 0) goto L_0x0440
                com.portfolio.platform.PortfolioApp$a r18 = com.portfolio.platform.PortfolioApp.g0
                r31 = r0
                com.portfolio.platform.PortfolioApp r0 = r18.c()
                r19 = r2
                r2 = 2131886389(0x7f120135, float:1.9407355E38)
                java.lang.String r18 = com.fossil.ig5.a(r0, r2)
                goto L_0x0444
            L_0x0440:
                r31 = r0
                r19 = r2
            L_0x0444:
                java.lang.Object r0 = r1.getFirst()
                com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
                if (r0 == 0) goto L_0x045b
                java.lang.String r2 = com.fossil.yx6.a(r18)
                r18 = r1
                java.lang.String r1 = "Utils.formatLocationName(name)"
                com.fossil.ee7.a(r2, r1)
                r0.setAddress(r2)
                goto L_0x045d
            L_0x045b:
                r18 = r1
            L_0x045d:
                r0 = r31
                r2 = r3
                r3 = r4
                r4 = r5
                r5 = r18
                r1 = r19
                r16 = 1
                r29 = r17
                r17 = r7
                r7 = r10
                r10 = r12
                r12 = r13
                r13 = r14
                r14 = r29
            L_0x0472:
                int r9 = r9 + 1
                goto L_0x02a9
            L_0x0476:
                com.fossil.gh5 r0 = r8.this$0
                r0.a(r5, r2, r1)
                com.fossil.i97 r14 = com.fossil.i97.a
                goto L_0x048f
            L_0x047e:
                com.fossil.ee7.a()
                r0 = 0
                throw r0
            L_0x0483:
                r0 = 0
                com.fossil.ee7.a()
                throw r0
            L_0x0488:
                r0 = 0
                com.fossil.ee7.a()
                throw r0
            L_0x048d:
                r0 = 0
                r14 = r0
            L_0x048f:
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.j.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {322}, m = "showChanceOfRain")
    public static final class k extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(gh5 gh5, fb7 fb7) {
            super(fb7);
            this.this$0 = gh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Weather) null, false, (fb7<? super i97>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$showChanceOfRain$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class l extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(gh5 gh5, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gh5;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            l lVar = new l(this.this$0, this.$rainProbabilityPercent, fb7);
            lVar.p$ = (yi7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((l) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.b().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {308}, m = "showTemperature")
    public static final class m extends rb7 {
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(gh5 gh5, fb7 fb7) {
            super(fb7);
            this.this$0 = gh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.WeatherManager$showTemperature$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class n extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $tempInCelsius;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(gh5 gh5, float f, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gh5;
            this.$tempInCelsius = f;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            n nVar = new n(this.this$0, this.$tempInCelsius, fb7);
            nVar.p$ = (yi7) obj;
            return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((n) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.b().upsertCustomizeRealData(new CustomizeRealData("temperature", String.valueOf(this.$tempInCelsius)));
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = gh5.class.getSimpleName();
        ee7.a((Object) simpleName, "WeatherManager::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public gh5() {
        this.j = mn7.a(false, 1, null);
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final GoogleApiService d() {
        GoogleApiService googleApiService = this.g;
        if (googleApiService != null) {
            return googleApiService;
        }
        ee7.d("mGoogleApiService");
        throw null;
    }

    @DexIgnore
    public final LocationSource e() {
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            return locationSource;
        }
        ee7.d("mLocationSource");
        throw null;
    }

    @DexIgnore
    public final UserRepository f() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository b() {
        CustomizeRealDataRepository customizeRealDataRepository = this.e;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        ee7.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final DianaPresetRepository c() {
        DianaPresetRepository dianaPresetRepository = this.f;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        ee7.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final Object b(String str, fb7<? super i97> fb7) {
        return vh7.a(qj7.a(), new i(this, str, null), fb7);
    }

    @DexIgnore
    public final Object c(String str, fb7<? super i97> fb7) {
        return vh7.a(qj7.b(), new j(this, str, null), fb7);
    }

    @DexIgnore
    public /* synthetic */ gh5(zd7 zd7) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.portfolio.platform.data.model.microapp.weather.Weather r8, boolean r9, com.fossil.fb7<? super com.fossil.i97> r10) {
        /*
            r7 = this;
            boolean r0 = r10 instanceof com.fossil.gh5.m
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.gh5$m r0 = (com.fossil.gh5.m) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gh5$m r0 = new com.fossil.gh5$m
            r0.<init>(r7, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r8 = r0.L$2
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo r8 = (com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo) r8
            float r8 = r0.F$0
            boolean r9 = r0.Z$0
            java.lang.Object r8 = r0.L$1
            com.portfolio.platform.data.model.microapp.weather.Weather r8 = (com.portfolio.platform.data.model.microapp.weather.Weather) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.gh5 r8 = (com.fossil.gh5) r8
            com.fossil.t87.a(r10)
            goto L_0x00fa
        L_0x003b:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0043:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.gh5.k
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "showTemperature - forecast="
            r5.append(r6)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r6 = r8.getCurrently()
            java.lang.String r6 = r6.getForecast()
            r5.append(r6)
            java.lang.String r6 = ", temperature="
            r5.append(r6)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r6 = r8.getCurrently()
            com.portfolio.platform.data.model.microapp.weather.Temperature r6 = r6.getTemperature()
            if (r6 == 0) goto L_0x011a
            float r6 = r6.getCurrently()
            r5.append(r6)
            java.lang.String r6 = ", temperatureUnit="
            r5.append(r6)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r6 = r8.getCurrently()
            com.portfolio.platform.data.model.microapp.weather.Temperature r6 = r6.getTemperature()
            java.lang.String r6 = r6.getUnit()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r10.d(r2, r5)
            com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT$Companion r10 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.Companion
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r2 = r8.getCurrently()
            com.portfolio.platform.data.model.microapp.weather.Temperature r2 = r2.getTemperature()
            java.lang.String r2 = r2.getUnit()
            java.lang.String r5 = "weather.currently.temperature.unit"
            com.fossil.ee7.a(r2, r5)
            com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r10 = r10.getTempUnit(r2)
            com.portfolio.platform.data.model.microapp.weather.Weather$TEMP_UNIT r2 = com.portfolio.platform.data.model.microapp.weather.Weather.TEMP_UNIT.FAHRENHEIT
            if (r10 != r2) goto L_0x00c0
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r10 = r8.getCurrently()
            com.portfolio.platform.data.model.microapp.weather.Temperature r10 = r10.getTemperature()
            float r10 = r10.getCurrently()
            float r10 = com.fossil.xd5.c(r10)
            goto L_0x00cc
        L_0x00c0:
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r10 = r8.getCurrently()
            com.portfolio.platform.data.model.microapp.weather.Temperature r10 = r10.getTemperature()
            float r10 = r10.getCurrently()
        L_0x00cc:
            com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo r2 = r8.toWeatherComplicationAppInfo()
            java.lang.String r5 = r7.h
            if (r5 == 0) goto L_0x00dd
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r6.a(r2, r5)
        L_0x00dd:
            com.fossil.ti7 r5 = com.fossil.qj7.a()
            com.fossil.gh5$n r6 = new com.fossil.gh5$n
            r6.<init>(r7, r10, r4)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.Z$0 = r9
            r0.F$0 = r10
            r0.L$2 = r2
            r0.label = r3
            java.lang.Object r8 = com.fossil.vh7.a(r5, r6, r0)
            if (r8 != r1) goto L_0x00f9
            return r1
        L_0x00f9:
            r8 = r7
        L_0x00fa:
            com.fossil.qd5$a r10 = com.fossil.qd5.f
            java.lang.String r0 = "weather"
            com.fossil.if5 r10 = r10.c(r0)
            if (r10 == 0) goto L_0x0112
            java.lang.String r8 = r8.h
            if (r8 == 0) goto L_0x010e
            java.lang.String r1 = ""
            r10.a(r8, r9, r1)
            goto L_0x0112
        L_0x010e:
            com.fossil.ee7.a()
            throw r4
        L_0x0112:
            com.fossil.qd5$a r8 = com.fossil.qd5.f
            r8.e(r0)
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        L_0x011a:
            com.fossil.ee7.a()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.b(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        ee7.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final Object a(String str, fb7<? super i97> fb7) {
        return vh7.a(qj7.a(), new h(this, str, null), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf A[SYNTHETIC, Splitter:B:27:0x00bf] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fb A[Catch:{ all -> 0x01f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r22, java.lang.String r23, com.fossil.fb7<? super com.fossil.r87<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r24) {
        /*
            r21 = this;
            r1 = r21
            r0 = r24
            java.lang.String r2 = "weather"
            boolean r3 = r0 instanceof com.fossil.gh5.g
            if (r3 == 0) goto L_0x0019
            r3 = r0
            com.fossil.gh5$g r3 = (com.fossil.gh5.g) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.gh5$g r3 = new com.fossil.gh5$g
            r3.<init>(r1, r0)
        L_0x001e:
            java.lang.Object r0 = r3.result
            java.lang.Object r13 = com.fossil.nb7.a()
            int r4 = r3.label
            r14 = 3
            r5 = 2
            r6 = 1
            r15 = 0
            if (r4 == 0) goto L_0x0090
            if (r4 == r6) goto L_0x0078
            if (r4 == r5) goto L_0x005b
            if (r4 != r14) goto L_0x0053
            java.lang.Object r2 = r3.L$6
            com.fossil.oe7 r2 = (com.fossil.oe7) r2
            java.lang.Object r4 = r3.L$5
            android.location.Location r4 = (android.location.Location) r4
            java.lang.Object r4 = r3.L$4
            com.portfolio.platform.data.LocationSource$Result r4 = (com.portfolio.platform.data.LocationSource.Result) r4
            java.lang.Object r4 = r3.L$3
            com.fossil.kn7 r4 = (com.fossil.kn7) r4
            java.lang.Object r5 = r3.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r3.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r3 = r3.L$0
            com.fossil.gh5 r3 = (com.fossil.gh5) r3
            com.fossil.t87.a(r0)     // Catch:{ all -> 0x0074 }
            goto L_0x0173
        L_0x0053:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x005b:
            java.lang.Object r4 = r3.L$3
            com.fossil.kn7 r4 = (com.fossil.kn7) r4
            java.lang.Object r5 = r3.L$2
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r6 = r3.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r3.L$0
            com.fossil.gh5 r7 = (com.fossil.gh5) r7
            com.fossil.t87.a(r0)
            r11 = r4
            r15 = r5
            r12 = r6
            r14 = r7
            goto L_0x0129
        L_0x0074:
            r0 = move-exception
        L_0x0075:
            r1 = r15
            goto L_0x01fd
        L_0x0078:
            java.lang.Object r4 = r3.L$3
            com.fossil.kn7 r4 = (com.fossil.kn7) r4
            java.lang.Object r7 = r3.L$2
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = r3.L$1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r9 = r3.L$0
            com.fossil.gh5 r9 = (com.fossil.gh5) r9
            com.fossil.t87.a(r0)
            r11 = r4
            r12 = r7
            r0 = r8
            r10 = r9
            goto L_0x00ae
        L_0x0090:
            com.fossil.t87.a(r0)
            com.fossil.kn7 r0 = r1.j
            r3.L$0 = r1
            r4 = r22
            r3.L$1 = r4
            r7 = r23
            r3.L$2 = r7
            r3.L$3 = r0
            r3.label = r6
            java.lang.Object r8 = r0.a(r15, r3)
            if (r8 != r13) goto L_0x00aa
            return r13
        L_0x00aa:
            r11 = r0
            r10 = r1
            r0 = r4
            r12 = r7
        L_0x00ae:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x01f7 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()     // Catch:{ all -> 0x01f7 }
            java.lang.String r7 = com.fossil.gh5.k     // Catch:{ all -> 0x01f7 }
            java.lang.String r8 = "getWeatherBaseOnLocation"
            r4.d(r7, r8)     // Catch:{ all -> 0x01f7 }
            com.portfolio.platform.data.model.microapp.weather.Weather r4 = r10.i     // Catch:{ all -> 0x01f7 }
            if (r4 == 0) goto L_0x00f7
            java.util.Calendar r4 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x00f3 }
            java.lang.String r7 = "Calendar.getInstance()"
            com.fossil.ee7.a(r4, r7)     // Catch:{ all -> 0x00f3 }
            long r7 = r4.getTimeInMillis()     // Catch:{ all -> 0x00f3 }
            com.portfolio.platform.data.model.microapp.weather.Weather r4 = r10.i     // Catch:{ all -> 0x00f3 }
            if (r4 == 0) goto L_0x00ee
            long r16 = r4.getUpdatedAt()     // Catch:{ all -> 0x00f3 }
            long r7 = r7 - r16
            r4 = 60000(0xea60, float:8.4078E-41)
            long r14 = (long) r4
            int r4 = (r7 > r14 ? 1 : (r7 == r14 ? 0 : -1))
            if (r4 >= 0) goto L_0x00f7
            com.fossil.r87 r0 = new com.fossil.r87     // Catch:{ all -> 0x01d7 }
            com.portfolio.platform.data.model.microapp.weather.Weather r2 = r10.i     // Catch:{ all -> 0x01d7 }
            java.lang.Boolean r3 = com.fossil.pb7.a(r6)     // Catch:{ all -> 0x01d7 }
            r0.<init>(r2, r3)     // Catch:{ all -> 0x01d7 }
            r2 = 0
            r11.a(r2)
            return r0
        L_0x00ee:
            com.fossil.ee7.a()
            r2 = 0
            throw r2
        L_0x00f3:
            r0 = move-exception
            r4 = r11
            goto L_0x0075
        L_0x00f7:
            com.portfolio.platform.data.LocationSource r4 = r10.c     // Catch:{ all -> 0x01f1 }
            if (r4 == 0) goto L_0x01e6
            com.portfolio.platform.PortfolioApp r6 = r10.a     // Catch:{ all -> 0x01f1 }
            if (r6 == 0) goto L_0x01db
            r7 = 0
            r8 = 0
            r9 = 0
            r14 = 0
            r15 = 28
            r17 = 0
            r3.L$0 = r10     // Catch:{ all -> 0x01f1 }
            r3.L$1 = r0     // Catch:{ all -> 0x01f1 }
            r3.L$2 = r12     // Catch:{ all -> 0x01f1 }
            r3.L$3 = r11     // Catch:{ all -> 0x01f1 }
            r3.label = r5     // Catch:{ all -> 0x01f1 }
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r14
            r14 = r10
            r10 = r3
            r18 = r11
            r11 = r15
            r15 = r12
            r12 = r17
            java.lang.Object r4 = com.portfolio.platform.data.LocationSource.getLocation$default(r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x01e4 }
            if (r4 != r13) goto L_0x0125
            return r13
        L_0x0125:
            r12 = r0
            r0 = r4
            r11 = r18
        L_0x0129:
            com.portfolio.platform.data.LocationSource$Result r0 = (com.portfolio.platform.data.LocationSource.Result) r0
            com.portfolio.platform.data.LocationSource$ErrorState r4 = r0.getErrorState()
            com.portfolio.platform.data.LocationSource$ErrorState r5 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            r6 = 0
            if (r4 != r5) goto L_0x0185
            android.location.Location r2 = r0.getLocation()
            com.fossil.oe7 r10 = new com.fossil.oe7
            r10.<init>()
            r10.element = r6
            com.fossil.ti7 r9 = com.fossil.qj7.a()
            com.fossil.gh5$f r8 = new com.fossil.gh5$f
            r7 = 0
            r4 = r8
            r5 = r2
            r6 = r10
            r19 = r8
            r8 = r14
            r20 = r9
            r9 = r3
            r1 = r10
            r10 = r15
            r4.<init>(r5, r6, r7, r8, r9, r10)
            r3.L$0 = r14
            r3.L$1 = r12
            r3.L$2 = r15
            r3.L$3 = r11
            r3.L$4 = r0
            r3.L$5 = r2
            r3.L$6 = r1
            r0 = 3
            r3.label = r0
            r2 = r19
            r0 = r20
            java.lang.Object r0 = com.fossil.vh7.a(r0, r2, r3)
            if (r0 != r13) goto L_0x0170
            return r13
        L_0x0170:
            r2 = r1
            r4 = r11
            r3 = r14
        L_0x0173:
            com.fossil.r87 r0 = new com.fossil.r87     // Catch:{ all -> 0x0183 }
            com.portfolio.platform.data.model.microapp.weather.Weather r1 = r3.i     // Catch:{ all -> 0x0183 }
            boolean r2 = r2.element     // Catch:{ all -> 0x0183 }
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)     // Catch:{ all -> 0x0183 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0183 }
            r11 = r4
            r2 = 0
            goto L_0x01cf
        L_0x0183:
            r0 = move-exception
            goto L_0x01d9
        L_0x0185:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.fossil.gh5.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getWeatherBaseOnLocation getLocation error="
            r4.append(r5)
            com.portfolio.platform.data.LocationSource$ErrorState r0 = r0.getErrorState()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r1.d(r3, r0)
            com.fossil.qd5$a r0 = com.fossil.qd5.f
            com.fossil.if5 r0 = r0.c(r2)
            if (r0 == 0) goto L_0x01c0
            java.lang.String r1 = r14.h
            if (r1 == 0) goto L_0x01bb
            com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder$AppError r3 = com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.AppError.GET_TEMPERATURE_FAILED
            java.lang.String r3 = r3.getValue()
            r0.a(r1, r6, r3)
            goto L_0x01c0
        L_0x01bb:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x01c0:
            com.fossil.qd5$a r0 = com.fossil.qd5.f
            r0.e(r2)
            com.fossil.r87 r0 = new com.fossil.r87
            java.lang.Boolean r1 = com.fossil.pb7.a(r6)
            r2 = 0
            r0.<init>(r2, r1)     // Catch:{ all -> 0x01d3 }
        L_0x01cf:
            r11.a(r2)
            return r0
        L_0x01d3:
            r0 = move-exception
            r1 = r2
            r4 = r11
            goto L_0x01fd
        L_0x01d7:
            r0 = move-exception
            r4 = r11
        L_0x01d9:
            r1 = 0
            goto L_0x01fd
        L_0x01db:
            r18 = r11
            java.lang.String r0 = "mPortfolioApp"
            com.fossil.ee7.d(r0)
            r1 = 0
            throw r1
        L_0x01e4:
            r0 = move-exception
            goto L_0x01f4
        L_0x01e6:
            r18 = r11
            r1 = 0
            java.lang.String r0 = "mLocationSource"
            com.fossil.ee7.d(r0)     // Catch:{ all -> 0x01ef }
            throw r1
        L_0x01ef:
            r0 = move-exception
            goto L_0x01fb
        L_0x01f1:
            r0 = move-exception
            r18 = r11
        L_0x01f4:
            r4 = r18
            goto L_0x01d9
        L_0x01f7:
            r0 = move-exception
            r18 = r11
            r1 = r15
        L_0x01fb:
            r4 = r18
        L_0x01fd:
            r4.a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.a(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final boolean a(AddressOfWeather addressOfWeather, double d2, double d3) {
        Location location = new Location("LocationA");
        location.setLatitude(addressOfWeather.getLat());
        location.setLongitude(addressOfWeather.getLng());
        Location location2 = new Location("LocationB");
        location2.setLatitude(d2);
        location2.setLongitude(d3);
        float distanceTo = location.distanceTo(location2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "distance between two address=" + distanceTo);
        return distanceTo < ((float) VideoUploader.RETRY_DELAY_UNIT_MS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(double r19, double r21, com.fossil.fb7<? super java.lang.String> r23) {
        /*
            r18 = this;
            r8 = r18
            r9 = r19
            r11 = r21
            r0 = r23
            boolean r1 = r0 instanceof com.fossil.gh5.b
            if (r1 == 0) goto L_0x001b
            r1 = r0
            com.fossil.gh5$b r1 = (com.fossil.gh5.b) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x001b
            int r2 = r2 - r3
            r1.label = r2
            goto L_0x0020
        L_0x001b:
            com.fossil.gh5$b r1 = new com.fossil.gh5$b
            r1.<init>(r8, r0)
        L_0x0020:
            r13 = r1
            java.lang.Object r0 = r13.result
            java.lang.Object r14 = com.fossil.nb7.a()
            int r1 = r13.label
            r15 = 1
            if (r1 == 0) goto L_0x0046
            if (r1 != r15) goto L_0x003e
            java.lang.Object r1 = r13.L$1
            java.lang.String r1 = (java.lang.String) r1
            double r2 = r13.D$1
            double r2 = r13.D$0
            java.lang.Object r2 = r13.L$0
            com.fossil.gh5 r2 = (com.fossil.gh5) r2
            com.fossil.t87.a(r0)
            goto L_0x0098
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.gh5.k
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getAddressBaseOnLocation latLng="
            r2.append(r3)
            r2.append(r9)
            r3 = 44
            r2.append(r3)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            java.lang.String r7 = "administrative_area_level_1"
            com.fossil.gh5$c r6 = new com.fossil.gh5$c
            r16 = 0
            r0 = r6
            r1 = r18
            r2 = r19
            r4 = r21
            r17 = r6
            r6 = r7
            r15 = r7
            r7 = r16
            r0.<init>(r1, r2, r4, r6, r7)
            r13.L$0 = r8
            r13.D$0 = r9
            r13.D$1 = r11
            r13.L$1 = r15
            r0 = 1
            r13.label = r0
            r0 = r17
            java.lang.Object r0 = com.fossil.aj5.a(r0, r13)
            if (r0 != r14) goto L_0x0097
            return r14
        L_0x0097:
            r1 = r15
        L_0x0098:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            boolean r2 = r0 instanceof com.fossil.bj5
            java.lang.String r3 = ""
            if (r2 == 0) goto L_0x014f
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.ie4 r0 = (com.fossil.ie4) r0
            r2 = 0
            if (r0 == 0) goto L_0x00b2
            java.lang.String r4 = "results"
            com.fossil.de4 r0 = r0.b(r4)
            goto L_0x00b3
        L_0x00b2:
            r0 = r2
        L_0x00b3:
            if (r0 == 0) goto L_0x014e
            java.util.Iterator r0 = r0.iterator()
        L_0x00b9:
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x014e
            java.lang.Object r4 = r0.next()
            com.google.gson.JsonElement r4 = (com.google.gson.JsonElement) r4
            java.lang.String r5 = "result"
            com.fossil.ee7.a(r4, r5)
            com.fossil.ie4 r4 = r4.d()
            if (r4 == 0) goto L_0x00d7
            java.lang.String r5 = "address_components"
            com.fossil.de4 r4 = r4.b(r5)
            goto L_0x00d8
        L_0x00d7:
            r4 = r2
        L_0x00d8:
            if (r4 == 0) goto L_0x00b9
            java.util.Iterator r4 = r4.iterator()
        L_0x00de:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x00b9
            java.lang.Object r5 = r4.next()
            com.google.gson.JsonElement r5 = (com.google.gson.JsonElement) r5
            java.lang.String r6 = "addressComponent"
            com.fossil.ee7.a(r5, r6)
            com.fossil.ie4 r6 = r5.d()
            if (r6 == 0) goto L_0x00fc
            java.lang.String r7 = "types"
            com.fossil.de4 r6 = r6.b(r7)
            goto L_0x00fd
        L_0x00fc:
            r6 = r2
        L_0x00fd:
            if (r6 == 0) goto L_0x00de
            int r7 = r6.size()
            if (r7 <= 0) goto L_0x00de
            r7 = 0
            com.google.gson.JsonElement r6 = r6.get(r7)
            java.lang.String r7 = "types[0]"
            com.fossil.ee7.a(r6, r7)
            java.lang.String r6 = r6.f()
            boolean r6 = com.fossil.ee7.a(r6, r1)
            if (r6 == 0) goto L_0x00de
            com.fossil.ie4 r0 = r5.d()
            java.lang.String r1 = "long_name"
            com.google.gson.JsonElement r0 = r0.a(r1)
            java.lang.String r1 = "addressComponent.asJsonObject.get(\"long_name\")"
            com.fossil.ee7.a(r0, r1)
            java.lang.String r0 = r0.f()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.gh5.k
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "cityName="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            java.lang.String r1 = "cityName"
            com.fossil.ee7.a(r0, r1)
            return r0
        L_0x014e:
            return r3
        L_0x014f:
            boolean r0 = r0 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0154
            return r3
        L_0x0154:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.a(double, double, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(double r14, double r16, java.lang.String r18, com.fossil.fb7<? super com.fossil.r87<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r19) {
        /*
            r13 = this;
            r8 = r13
            r0 = r19
            boolean r1 = r0 instanceof com.fossil.gh5.d
            if (r1 == 0) goto L_0x0016
            r1 = r0
            com.fossil.gh5$d r1 = (com.fossil.gh5.d) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0016
            int r2 = r2 - r3
            r1.label = r2
            goto L_0x001b
        L_0x0016:
            com.fossil.gh5$d r1 = new com.fossil.gh5$d
            r1.<init>(r13, r0)
        L_0x001b:
            r9 = r1
            java.lang.Object r0 = r9.result
            java.lang.Object r10 = com.fossil.nb7.a()
            int r1 = r9.label
            r11 = 1
            if (r1 == 0) goto L_0x0041
            if (r1 != r11) goto L_0x0039
            java.lang.Object r1 = r9.L$1
            java.lang.String r1 = (java.lang.String) r1
            double r1 = r9.D$1
            double r1 = r9.D$0
            java.lang.Object r1 = r9.L$0
            com.fossil.gh5 r1 = (com.fossil.gh5) r1
            com.fossil.t87.a(r0)
            goto L_0x0067
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.t87.a(r0)
            com.fossil.gh5$e r12 = new com.fossil.gh5$e
            r7 = 0
            r0 = r12
            r1 = r13
            r2 = r14
            r4 = r16
            r6 = r18
            r0.<init>(r1, r2, r4, r6, r7)
            r9.L$0 = r8
            r0 = r14
            r9.D$0 = r0
            r0 = r16
            r9.D$1 = r0
            r0 = r18
            r9.L$1 = r0
            r9.label = r11
            java.lang.Object r0 = com.fossil.aj5.a(r12, r9)
            if (r0 != r10) goto L_0x0067
            return r10
        L_0x0067:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.gh5.k
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getWeather onResponse: response = "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0 instanceof com.fossil.bj5
            r2 = 0
            r3 = 0
            if (r1 == 0) goto L_0x00b9
            com.fossil.bj5 r0 = (com.fossil.bj5) r0
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x00af
            com.fossil.fj5 r1 = new com.fossil.fj5
            r1.<init>()
            java.lang.Object r0 = r0.a()
            com.fossil.ie4 r0 = (com.fossil.ie4) r0
            r1.a(r0)
            com.fossil.r87 r0 = new com.fossil.r87
            com.portfolio.platform.data.model.microapp.weather.Weather r1 = r1.a()
            java.lang.Boolean r2 = com.fossil.pb7.a(r3)
            r0.<init>(r1, r2)
            goto L_0x00c6
        L_0x00af:
            com.fossil.r87 r0 = new com.fossil.r87
            java.lang.Boolean r1 = com.fossil.pb7.a(r3)
            r0.<init>(r2, r1)
            goto L_0x00c6
        L_0x00b9:
            boolean r0 = r0 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00c7
            com.fossil.r87 r0 = new com.fossil.r87
            java.lang.Boolean r1 = com.fossil.pb7.a(r3)
            r0.<init>(r2, r1)
        L_0x00c6:
            return r0
        L_0x00c7:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.a(double, double, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.microapp.weather.Weather r8, boolean r9, com.fossil.fb7<? super com.fossil.i97> r10) {
        /*
            r7 = this;
            boolean r0 = r10 instanceof com.fossil.gh5.k
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.gh5$k r0 = (com.fossil.gh5.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gh5$k r0 = new com.fossil.gh5$k
            r0.<init>(r7, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r4) goto L_0x003a
            java.lang.Object r8 = r0.L$2
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r8 = (com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo) r8
            int r8 = r0.I$0
            boolean r9 = r0.Z$0
            java.lang.Object r8 = r0.L$1
            com.portfolio.platform.data.model.microapp.weather.Weather r8 = (com.portfolio.platform.data.model.microapp.weather.Weather) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.gh5 r8 = (com.fossil.gh5) r8
            com.fossil.t87.a(r10)
            goto L_0x00a5
        L_0x003a:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0042:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.gh5.k
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "showChanceOfRain - probability="
            r5.append(r6)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r6 = r8.getCurrently()
            float r6 = r6.getRainProbability()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r10.d(r2, r5)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r10 = r8.getCurrently()
            float r10 = r10.getRainProbability()
            r2 = 100
            float r2 = (float) r2
            float r10 = r10 * r2
            int r10 = (int) r10
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r2 = r8.toChanceOfRainComplicationAppInfo()
            java.lang.String r5 = r7.h
            if (r5 == 0) goto L_0x0088
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r6.a(r2, r5)
        L_0x0088:
            com.fossil.ti7 r5 = com.fossil.qj7.a()
            com.fossil.gh5$l r6 = new com.fossil.gh5$l
            r6.<init>(r7, r10, r3)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.Z$0 = r9
            r0.I$0 = r10
            r0.L$2 = r2
            r0.label = r4
            java.lang.Object r8 = com.fossil.vh7.a(r5, r6, r0)
            if (r8 != r1) goto L_0x00a4
            return r1
        L_0x00a4:
            r8 = r7
        L_0x00a5:
            com.fossil.qd5$a r10 = com.fossil.qd5.f
            java.lang.String r0 = "chance-of-rain"
            com.fossil.if5 r10 = r10.c(r0)
            if (r10 == 0) goto L_0x00bd
            java.lang.String r8 = r8.h
            if (r8 == 0) goto L_0x00b9
            java.lang.String r1 = ""
            r10.a(r8, r9, r1)
            goto L_0x00bd
        L_0x00b9:
            com.fossil.ee7.a()
            throw r3
        L_0x00bd:
            com.fossil.qd5$a r8 = com.fossil.qd5.f
            r8.e(r0)
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gh5.a(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(r87<Weather, Boolean> r87, r87<Weather, Boolean> r872, r87<Weather, Boolean> r873) {
        Weather first;
        Weather first2;
        Weather first3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "showWeatherWatchApp - firstWeather=" + r87 + ", secondWeather=" + r872 + ", thirdWeather=" + r873);
        WeatherWatchAppInfo weatherWatchAppInfo = (r87 == null || (first3 = r87.getFirst()) == null) ? null : first3.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo2 = (r872 == null || (first2 = r872.getFirst()) == null) ? null : first2.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo3 = (r873 == null || (first = r873.getFirst()) == null) ? null : first.toWeatherWatchAppInfo();
        if (weatherWatchAppInfo == null) {
            return;
        }
        if (weatherWatchAppInfo != null) {
            PortfolioApp.g0.c().a(new WeatherInfoWatchAppResponse(weatherWatchAppInfo, weatherWatchAppInfo2, weatherWatchAppInfo3), PortfolioApp.g0.c().c());
            if5 c2 = qd5.f.c("weather");
            if (c2 != null) {
                String str2 = this.h;
                if (str2 != null) {
                    Boolean second = r87 != null ? r87.getSecond() : null;
                    if (second != null) {
                        c2.a(str2, second.booleanValue(), "");
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            qd5.f.e("weather");
            return;
        }
        ee7.a();
        throw null;
    }
}
