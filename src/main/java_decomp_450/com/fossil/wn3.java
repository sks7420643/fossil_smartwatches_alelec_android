package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn3 extends v02.a<gn3, fn3> {
    @DexIgnore
    /* Return type fixed from 'com.fossil.v02$f' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.content.Context, android.os.Looper, com.fossil.j62, java.lang.Object, com.fossil.a12$b, com.fossil.a12$c] */
    @Override // com.fossil.v02.a
    public final /* synthetic */ gn3 a(Context context, Looper looper, j62 j62, fn3 fn3, a12.b bVar, a12.c cVar) {
        fn3 fn32 = fn3;
        if (fn32 == null) {
            fn32 = fn3.j;
        }
        return new gn3(context, looper, true, j62, fn32, bVar, cVar);
    }
}
