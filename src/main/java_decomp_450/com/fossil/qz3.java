package com.fossil;

import com.fossil.cy3;
import com.fossil.dy3;
import com.fossil.iy3;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz3<K, V> extends by3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] e;
    @DexIgnore
    public /* final */ transient cy3<K, V>[] f;
    @DexIgnore
    public /* final */ transient int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends iy3.b<K> {
        @DexIgnore
        @Weak
        public /* final */ qz3<K, V> map;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qz3$a$a")
        /* renamed from: com.fossil.qz3$a$a  reason: collision with other inner class name */
        public static class C0159a<K> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ by3<K, ?> map;

            @DexIgnore
            public C0159a(by3<K, ?> by3) {
                this.map = by3;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.keySet();
            }
        }

        @DexIgnore
        public a(qz3<K, V> qz3) {
            this.map = qz3;
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            return this.map.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.iy3.b
        public K get(int i) {
            return (K) this.map.e[i].getKey();
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.vx3, com.fossil.iy3
        public Object writeReplace() {
            return new C0159a(this.map);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends zx3<V> {
        @DexIgnore
        @Weak
        public /* final */ qz3<K, V> map;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a<V> implements Serializable {
            @DexIgnore
            public static /* final */ long serialVersionUID; // = 0;
            @DexIgnore
            public /* final */ by3<?, V> map;

            @DexIgnore
            public a(by3<?, V> by3) {
                this.map = by3;
            }

            @DexIgnore
            public Object readResolve() {
                return this.map.values();
            }
        }

        @DexIgnore
        public b(qz3<K, V> qz3) {
            this.map = qz3;
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            return (V) this.map.e[i].getValue();
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return this.map.size();
        }

        @DexIgnore
        @Override // com.fossil.vx3, com.fossil.zx3
        public Object writeReplace() {
            return new a(this.map);
        }
    }

    @DexIgnore
    public qz3(Map.Entry<K, V>[] entryArr, cy3<K, V>[] cy3Arr, int i) {
        this.e = entryArr;
        this.f = cy3Arr;
        this.g = i;
    }

    @DexIgnore
    public static void checkNoConflictInKeyBucket(Object obj, Map.Entry<?, ?> entry, cy3<?, ?> cy3) {
        while (cy3 != null) {
            by3.checkNoConflict(!obj.equals(cy3.getKey()), "key", entry, cy3);
            cy3 = cy3.getNextInKeyBucket();
        }
    }

    @DexIgnore
    public static <K, V> qz3<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> qz3<K, V> fromEntryArray(int i, Map.Entry<K, V>[] entryArr) {
        Map.Entry<K, V>[] entryArr2;
        cy3 cy3;
        jw3.b(i, entryArr.length);
        if (i == entryArr.length) {
            entryArr2 = entryArr;
        } else {
            entryArr2 = cy3.createEntryArray(i);
        }
        int a2 = sx3.a(i, 1.2d);
        cy3[] createEntryArray = cy3.createEntryArray(a2);
        int i2 = a2 - 1;
        for (int i3 = 0; i3 < i; i3++) {
            Map.Entry<K, V> entry = entryArr[i3];
            K key = entry.getKey();
            V value = entry.getValue();
            bx3.a(key, value);
            int a3 = sx3.a(key.hashCode()) & i2;
            cy3 cy32 = createEntryArray[a3];
            if (cy32 == null) {
                cy3 = (entry instanceof cy3) && ((cy3) entry).isReusable() ? (cy3) entry : new cy3(key, value);
            } else {
                cy3 = new cy3.b(key, value, cy32);
            }
            createEntryArray[a3] = cy3;
            entryArr2[i3] = cy3;
            checkNoConflictInKeyBucket(key, cy3, cy32);
        }
        return new qz3<>(entryArr2, createEntryArray, i2);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<Map.Entry<K, V>> createEntrySet() {
        return new dy3.b(this, this.e);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<K> createKeySet() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public vx3<V> createValues() {
        return new b(this);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3
    public V get(Object obj) {
        return (V) get(obj, this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.e.length;
    }

    @DexIgnore
    public static <V> V get(Object obj, cy3<?, V>[] cy3Arr, int i) {
        if (obj == null) {
            return null;
        }
        for (cy3<?, V> cy3 = cy3Arr[i & sx3.a(obj.hashCode())]; cy3 != null; cy3 = cy3.getNextInKeyBucket()) {
            if (obj.equals(cy3.getKey())) {
                return cy3.getValue();
            }
        }
        return null;
    }
}
