package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm6 implements Factory<mm6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public nm6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static nm6 a(Provider<ThemeRepository> provider) {
        return new nm6(provider);
    }

    @DexIgnore
    public static mm6 a(ThemeRepository themeRepository) {
        return new mm6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public mm6 get() {
        return a(this.a.get());
    }
}
