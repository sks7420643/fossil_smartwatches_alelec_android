package com.fossil;

import android.net.Uri;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr implements yr<Uri, File> {
    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        if (!ee7.a((Object) uri.getScheme(), (Object) "file")) {
            return false;
        }
        String a = iu.a(uri);
        return a != null && (ee7.a(a, "android_asset") ^ true);
    }

    @DexIgnore
    public File b(Uri uri) {
        ee7.b(uri, "data");
        return a8.a(uri);
    }
}
