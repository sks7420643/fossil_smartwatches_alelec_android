package com.fossil;

import com.fossil.s87;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl7 {
    @DexIgnore
    public static /* final */ int a; // = b(Throwable.class, -1);
    @DexIgnore
    public static /* final */ ReentrantReadWriteLock b; // = new ReentrantReadWriteLock();
    @DexIgnore
    public static /* final */ WeakHashMap<Class<? extends Throwable>, gd7<Throwable, Throwable>> c; // = new WeakHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            try {
                s87.a aVar = s87.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage(), th);
                if (newInstance != null) {
                    obj = s87.m60constructorimpl((Throwable) newInstance);
                    if (s87.m65isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                s87.a aVar2 = s87.Companion;
                obj = s87.m60constructorimpl(t87.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            try {
                s87.a aVar = s87.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th);
                if (newInstance != null) {
                    obj = s87.m60constructorimpl((Throwable) newInstance);
                    if (s87.m65isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                s87.a aVar2 = s87.Companion;
                obj = s87.m60constructorimpl(t87.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            try {
                s87.a aVar = s87.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage());
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    obj = s87.m60constructorimpl(th2);
                    if (s87.m65isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                s87.a aVar2 = s87.Companion;
                obj = s87.m60constructorimpl(t87.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements gd7<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object obj;
            try {
                s87.a aVar = s87.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[0]);
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    obj = s87.m60constructorimpl(th2);
                    if (s87.m65isFailureimpl(obj)) {
                        obj = null;
                    }
                    return (Throwable) obj;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                s87.a aVar2 = s87.Companion;
                obj = s87.m60constructorimpl(t87.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Integer.valueOf(t2.getParameterTypes().length), Integer.valueOf(t.getParameterTypes().length));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends fe7 implements gd7 {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends fe7 implements gd7 {
        @DexIgnore
        public static /* final */ g INSTANCE; // = new g();

        @DexIgnore
        public g() {
            super(1);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            return null;
        }
    }

    /*  JADX ERROR: StackOverflowError in pass: MarkFinallyVisitor
        java.lang.StackOverflowError
        	at jadx.core.dex.nodes.InsnNode.isSame(InsnNode.java:303)
        	at jadx.core.dex.instructions.InvokeNode.isSame(InvokeNode.java:77)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.sameInsns(MarkFinallyVisitor.java:451)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.compareBlocks(MarkFinallyVisitor.java:436)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:408)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:411)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ba  */
    public static final <E extends java.lang.Throwable> E a(E r9) {
        /*
            boolean r0 = r9 instanceof com.fossil.ri7
            r1 = 0
            if (r0 == 0) goto L_0x0028
            com.fossil.s87$a r0 = com.fossil.s87.Companion     // Catch:{ all -> 0x0012 }
            com.fossil.ri7 r9 = (com.fossil.ri7) r9     // Catch:{ all -> 0x0012 }
            java.lang.Throwable r9 = r9.createCopy()     // Catch:{ all -> 0x0012 }
            java.lang.Object r9 = com.fossil.s87.m60constructorimpl(r9)     // Catch:{ all -> 0x0012 }
            goto L_0x001d
        L_0x0012:
            r9 = move-exception
            com.fossil.s87$a r0 = com.fossil.s87.Companion
            java.lang.Object r9 = com.fossil.t87.a(r9)
            java.lang.Object r9 = com.fossil.s87.m60constructorimpl(r9)
        L_0x001d:
            boolean r0 = com.fossil.s87.m65isFailureimpl(r9)
            if (r0 == 0) goto L_0x0024
            goto L_0x0025
        L_0x0024:
            r1 = r9
        L_0x0025:
            java.lang.Throwable r1 = (java.lang.Throwable) r1
            return r1
        L_0x0028:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = com.fossil.wl7.b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.gd7<java.lang.Throwable, java.lang.Throwable>> r2 = com.fossil.wl7.c     // Catch:{ all -> 0x011b }
            java.lang.Class r3 = r9.getClass()     // Catch:{ all -> 0x011b }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x011b }
            com.fossil.gd7 r2 = (com.fossil.gd7) r2     // Catch:{ all -> 0x011b }
            r0.unlock()
            if (r2 == 0) goto L_0x0049
            java.lang.Object r9 = r2.invoke(r9)
            java.lang.Throwable r9 = (java.lang.Throwable) r9
            return r9
        L_0x0049:
            int r0 = com.fossil.wl7.a
            java.lang.Class r2 = r9.getClass()
            r3 = 0
            int r2 = b(r2, r3)
            if (r0 == r2) goto L_0x009e
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = com.fossil.wl7.b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r2 = r0.readLock()
            int r4 = r0.getWriteHoldCount()
            if (r4 != 0) goto L_0x0067
            int r4 = r0.getReadHoldCount()
            goto L_0x0068
        L_0x0067:
            r4 = 0
        L_0x0068:
            r5 = 0
        L_0x0069:
            if (r5 >= r4) goto L_0x0071
            r2.unlock()
            int r5 = r5 + 1
            goto L_0x0069
        L_0x0071:
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.gd7<java.lang.Throwable, java.lang.Throwable>> r5 = com.fossil.wl7.c     // Catch:{ all -> 0x0091 }
            java.lang.Class r9 = r9.getClass()     // Catch:{ all -> 0x0091 }
            com.fossil.wl7$f r6 = com.fossil.wl7.f.INSTANCE     // Catch:{ all -> 0x0091 }
            r5.put(r9, r6)     // Catch:{ all -> 0x0091 }
            com.fossil.i97 r9 = com.fossil.i97.a     // Catch:{ all -> 0x0091 }
        L_0x0085:
            if (r3 >= r4) goto L_0x008d
            r2.lock()
            int r3 = r3 + 1
            goto L_0x0085
        L_0x008d:
            r0.unlock()
            return r1
        L_0x0091:
            r9 = move-exception
        L_0x0092:
            if (r3 >= r4) goto L_0x009a
            r2.lock()
            int r3 = r3 + 1
            goto L_0x0092
        L_0x009a:
            r0.unlock()
            throw r9
        L_0x009e:
            java.lang.Class r0 = r9.getClass()
            java.lang.reflect.Constructor[] r0 = r0.getConstructors()
            com.fossil.wl7$e r2 = new com.fossil.wl7$e
            r2.<init>()
            java.util.List r0 = com.fossil.t97.c(r0, r2)
            java.util.Iterator r0 = r0.iterator()
            r2 = r1
        L_0x00b4:
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x00c6
            java.lang.Object r2 = r0.next()
            java.lang.reflect.Constructor r2 = (java.lang.reflect.Constructor) r2
            com.fossil.gd7 r2 = a(r2)
            if (r2 == 0) goto L_0x00b4
        L_0x00c6:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = com.fossil.wl7.b
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r4 = r0.readLock()
            int r5 = r0.getWriteHoldCount()
            if (r5 != 0) goto L_0x00d7
            int r5 = r0.getReadHoldCount()
            goto L_0x00d8
        L_0x00d7:
            r5 = 0
        L_0x00d8:
            r6 = 0
        L_0x00d9:
            if (r6 >= r5) goto L_0x00e1
            r4.unlock()
            int r6 = r6 + 1
            goto L_0x00d9
        L_0x00e1:
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            java.util.WeakHashMap<java.lang.Class<? extends java.lang.Throwable>, com.fossil.gd7<java.lang.Throwable, java.lang.Throwable>> r6 = com.fossil.wl7.c     // Catch:{ all -> 0x010e }
            java.lang.Class r7 = r9.getClass()     // Catch:{ all -> 0x010e }
            if (r2 == 0) goto L_0x00f2
            r8 = r2
            goto L_0x00f4
        L_0x00f2:
            com.fossil.wl7$g r8 = com.fossil.wl7.g.INSTANCE     // Catch:{ all -> 0x010e }
        L_0x00f4:
            r6.put(r7, r8)     // Catch:{ all -> 0x010e }
            com.fossil.i97 r6 = com.fossil.i97.a     // Catch:{ all -> 0x010e }
        L_0x00f9:
            if (r3 >= r5) goto L_0x0101
            r4.lock()
            int r3 = r3 + 1
            goto L_0x00f9
        L_0x0101:
            r0.unlock()
            if (r2 == 0) goto L_0x010d
            java.lang.Object r9 = r2.invoke(r9)
            r1 = r9
            java.lang.Throwable r1 = (java.lang.Throwable) r1
        L_0x010d:
            return r1
        L_0x010e:
            r9 = move-exception
        L_0x010f:
            if (r3 >= r5) goto L_0x0117
            r4.lock()
            int r3 = r3 + 1
            goto L_0x010f
        L_0x0117:
            r0.unlock()
            throw r9
        L_0x011b:
            r9 = move-exception
            r0.unlock()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wl7.a(java.lang.Throwable):java.lang.Throwable");
    }

    @DexIgnore
    public static final int b(Class<?> cls, int i) {
        Integer num;
        tc7.a((Class) cls);
        try {
            s87.a aVar = s87.Companion;
            num = s87.m60constructorimpl(Integer.valueOf(a(cls, 0, 1, null)));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            num = s87.m60constructorimpl(t87.a(th));
        }
        Integer valueOf = Integer.valueOf(i);
        if (s87.m65isFailureimpl(num)) {
            num = valueOf;
        }
        return ((Number) num).intValue();
    }

    @DexIgnore
    public static final gd7<Throwable, Throwable> a(Constructor<?> constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        int length = parameterTypes.length;
        if (length == 0) {
            return new d(constructor);
        }
        if (length == 1) {
            Class<?> cls = parameterTypes[0];
            if (ee7.a(cls, Throwable.class)) {
                return new b(constructor);
            }
            if (ee7.a(cls, String.class)) {
                return new c(constructor);
            }
            return null;
        } else if (length == 2 && ee7.a(parameterTypes[0], String.class) && ee7.a(parameterTypes[1], Throwable.class)) {
            return new a(constructor);
        } else {
            return null;
        }
    }

    @DexIgnore
    public static /* synthetic */ int a(Class cls, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return a(cls, i);
    }

    @DexIgnore
    public static final int a(Class<?> cls, int i) {
        do {
            Field[] declaredFields = cls.getDeclaredFields();
            int length = declaredFields.length;
            int i2 = 0;
            for (int i3 = 0; i3 < length; i3++) {
                if (!Modifier.isStatic(declaredFields[i3].getModifiers())) {
                    i2++;
                }
            }
            i += i2;
            cls = cls.getSuperclass();
        } while (cls != null);
        return i;
    }
}
