package com.fossil;

import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class op7 {
    @DexIgnore
    public static String a(lo7 lo7, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(lo7.e());
        sb.append(' ');
        if (b(lo7, type)) {
            sb.append(lo7.g());
        } else {
            sb.append(a(lo7.g()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(lo7 lo7, Proxy.Type type) {
        return !lo7.d() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(go7 go7) {
        String c = go7.c();
        String e = go7.e();
        if (e == null) {
            return c;
        }
        return c + '?' + e;
    }
}
