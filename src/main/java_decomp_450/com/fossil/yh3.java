package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yh3 implements Callable<List<wm3>> {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ ph3 d;

    @DexIgnore
    public yh3(ph3 ph3, String str, String str2, String str3) {
        this.d = ph3;
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<wm3> call() throws Exception {
        this.d.a.s();
        return this.d.a.k().b(this.a, this.b, this.c);
    }
}
