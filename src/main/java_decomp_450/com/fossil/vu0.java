package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu0 extends rr1 {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public /* final */ long P;
    @DexIgnore
    public /* final */ long Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ vu0(long j, long j2, short s, ri1 ri1, int i, int i2) {
        super(ln0.g, s, qa1.o, ri1, (i2 & 16) != 0 ? 3 : i);
        this.P = j;
        this.Q = j2;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = yz0.b(order.getInt(0));
            this.N = yz0.b(order.getInt(4));
            this.O = yz0.b(order.getInt(8));
            yz0.a(yz0.a(yz0.a(jSONObject, r51.J0, Long.valueOf(this.M)), r51.K0, Long.valueOf(this.N)), r51.L0, Long.valueOf(this.O));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81, com.fossil.rr1
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.a1, Long.valueOf(this.P)), r51.b1, Long.valueOf(this.Q));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(yz0.a(super.h(), r51.J0, Long.valueOf(this.M)), r51.K0, Long.valueOf(this.N)), r51.L0, Long.valueOf(this.O));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.P).putInt((int) this.Q).array();
        ee7.a((Object) array, "ByteBuffer.allocate(8)\n \u2026                 .array()");
        return array;
    }
}
