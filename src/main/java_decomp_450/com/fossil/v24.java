package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.fossil.o14;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v24 implements o14.b {
    @DexIgnore
    public e34 a;
    @DexIgnore
    public e34 b;

    @DexIgnore
    public void a(e34 e34) {
        this.b = e34;
    }

    @DexIgnore
    public void b(e34 e34) {
        this.a = e34;
    }

    @DexIgnore
    @Override // com.fossil.o14.b
    public void a(int i, Bundle bundle) {
        String string;
        z24 a2 = z24.a();
        a2.a("Received Analytics message: " + i + " " + bundle);
        if (bundle != null && (string = bundle.getString("name")) != null) {
            Bundle bundle2 = bundle.getBundle(NativeProtocol.WEB_DIALOG_PARAMS);
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            a(string, bundle2);
        }
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        e34 e34;
        if ("clx".equals(bundle.getString("_o"))) {
            e34 = this.a;
        } else {
            e34 = this.b;
        }
        a(e34, str, bundle);
    }

    @DexIgnore
    public static void a(e34 e34, String str, Bundle bundle) {
        if (e34 != null) {
            e34.a(str, bundle);
        }
    }
}
