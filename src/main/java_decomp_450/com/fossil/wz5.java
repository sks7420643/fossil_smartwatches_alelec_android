package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.fl4;
import com.fossil.z46;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz5 extends qz5 {
    @DexIgnore
    public a06 e;
    @DexIgnore
    public MutableLiveData<DianaPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<dz5> g; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<CustomizeRealData> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson i; // = new Gson();
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public WatchFaceWrapper k;
    @DexIgnore
    public /* final */ LiveData<dz5> l;
    @DexIgnore
    public /* final */ rz5 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public int o;
    @DexIgnore
    public /* final */ z46 p;
    @DexIgnore
    public /* final */ og5 q;
    @DexIgnore
    public /* final */ ch5 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$1", f = "DianaCustomizeEditPresenter.kt", l = {74}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wz5 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wz5$a$a")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$1$1", f = "DianaCustomizeEditPresenter.kt", l = {74}, m = "invokeSuspend")
        /* renamed from: com.fossil.wz5$a$a  reason: collision with other inner class name */
        public static final class C0237a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0237a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0237a aVar = new C0237a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((C0237a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository j = this.this$0.this$0.n;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = j.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wz5 wz5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wz5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            wz5 wz5;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                wz5 wz52 = this.this$0;
                ti7 b = qj7.b();
                C0237a aVar = new C0237a(this, null);
                this.L$0 = yi7;
                this.L$1 = wz52;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a) {
                    return a;
                }
                wz5 = wz52;
            } else if (i == 1) {
                wz5 = (wz5) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            wz5.j = (MFUser) obj;
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $it;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wz5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaPresetComplicationSetting $complication;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, DianaPresetComplicationSetting dianaPresetComplicationSetting, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$complication = dianaPresetComplicationSetting;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$complication, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return wz5.g(this.this$0.this$0).f(this.$complication.getId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaPresetWatchAppSetting $watchApp;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, DianaPresetWatchAppSetting dianaPresetWatchAppSetting, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$watchApp = dianaPresetWatchAppSetting;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$watchApp, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return wz5.g(this.this$0.this$0).g(this.$watchApp.getId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wz5$c$c")
        /* renamed from: com.fossil.wz5$c$c  reason: collision with other inner class name */
        public static final class C0238c implements fl4.e<z46.d, z46.b> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ if5 b;

            @DexIgnore
            public C0238c(c cVar, if5 if5) {
                this.a = cVar;
                this.b = if5;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(z46.d dVar) {
                ee7.b(dVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "setToWatch success");
                this.b.a("");
                this.a.this$0.l();
                this.a.this$0.m.m();
                this.a.this$0.m.g(true);
            }

            @DexIgnore
            public void a(z46.b bVar) {
                ee7.b(bVar, "errorValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "setToWatch onError, errorCode = " + bVar);
                this.a.this$0.l();
                this.a.this$0.m.m();
                int b2 = bVar.b();
                if (b2 != 1101) {
                    if (b2 == 8888) {
                        this.a.this$0.m.c();
                    } else if (!(b2 == 1112 || b2 == 1113)) {
                        this.a.this$0.m.p();
                    }
                    String arrayList = bVar.a().toString();
                    if5 if5 = this.b;
                    ee7.a((Object) arrayList, "errorCode");
                    if5.a(arrayList);
                    return;
                }
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                rz5 k = this.a.this$0.m;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    k.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    String arrayList2 = bVar.a().toString();
                    if5 if52 = this.b;
                    ee7.a((Object) arrayList2, "errorCode");
                    if52.a(arrayList2);
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DianaPreset dianaPreset, fb7 fb7, wz5 wz5, boolean z, DianaPreset dianaPreset2) {
            super(2, fb7);
            this.$it = dianaPreset;
            this.this$0 = wz5;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$currentPreset$inlined = dianaPreset2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.$it, fb7, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$currentPreset$inlined);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(11:33|34|35|36|(1:38)|39|(0)|42|64|66|(0)(0)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(7:96|97|98|99|(1:101)|102|(0)) */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0325, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x0357, code lost:
            if (((com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting) com.fossil.wz5.h(r8.this$0).a(r0.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.class)).getAddresses().isEmpty() != false) goto L_0x0323;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0159, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01b3, code lost:
            if (android.text.TextUtils.isEmpty(((com.portfolio.platform.data.model.setting.CommuteTimeSetting) com.fossil.wz5.h(r8.this$0).a(r0.getSettings(), com.portfolio.platform.data.model.setting.CommuteTimeSetting.class)).getAddress()) != false) goto L_0x0156;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:104:0x0315 A[Catch:{ Exception -> 0x0325 }] */
        /* JADX WARNING: Removed duplicated region for block: B:119:0x0379  */
        /* JADX WARNING: Removed duplicated region for block: B:123:0x039c  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x03fa  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00df  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0147 A[Catch:{ Exception -> 0x0159 }] */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01d9  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x0203  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x025d  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x02a7  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r23) {
            /*
                r22 = this;
                r1 = r22
                java.lang.Object r2 = com.fossil.nb7.a()
                int r0 = r1.label
                java.lang.String r3 = "last setting "
                java.lang.String r4 = "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)"
                java.lang.String r6 = "second-timezone"
                java.lang.String r7 = "check setting of "
                java.lang.String r9 = ""
                java.lang.String r10 = "exception when parse setting from json "
                r11 = 2
                java.lang.String r12 = "commute-time"
                r15 = 1
                java.lang.String r5 = "DianaCustomizeEditPresenter"
                if (r0 == 0) goto L_0x008f
                if (r0 == r15) goto L_0x0066
                if (r0 != r11) goto L_0x005e
                java.lang.Object r0 = r1.L$6
                r6 = r0
                java.util.Iterator r6 = (java.util.Iterator) r6
                java.lang.Object r0 = r1.L$5
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r0 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r0
                java.lang.Object r8 = r1.L$4
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r8 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r8
                java.lang.Object r11 = r1.L$3
                java.util.List r11 = (java.util.List) r11
                java.lang.Object r13 = r1.L$2
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r13 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r13
                java.lang.Object r14 = r1.L$1
                java.util.List r14 = (java.util.List) r14
                java.lang.Object r15 = r1.L$0
                com.fossil.yi7 r15 = (com.fossil.yi7) r15
                com.fossil.t87.a(r23)     // Catch:{ Exception -> 0x0050 }
                r18 = r4
                r20 = r7
                r19 = r9
                r4 = 2
                r7 = r23
                r21 = r8
                r8 = r1
                r1 = r21
                goto L_0x02f9
            L_0x0050:
                r0 = move-exception
                r18 = r4
                r20 = r7
                r19 = r9
                r21 = r8
                r8 = r1
                r1 = r21
                goto L_0x035d
            L_0x005e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0066:
                java.lang.Object r0 = r1.L$4
                r11 = r0
                java.util.Iterator r11 = (java.util.Iterator) r11
                java.lang.Object r0 = r1.L$3
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r0 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r0
                java.lang.Object r13 = r1.L$2
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r13 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r13
                java.lang.Object r14 = r1.L$1
                java.util.List r14 = (java.util.List) r14
                java.lang.Object r15 = r1.L$0
                com.fossil.yi7 r15 = (com.fossil.yi7) r15
                com.fossil.t87.a(r23)     // Catch:{ Exception -> 0x0087 }
                r8 = r1
                r20 = r7
                r19 = r9
                r1 = r23
                goto L_0x012b
            L_0x0087:
                r0 = move-exception
                r8 = r1
                r20 = r7
                r19 = r9
                goto L_0x01b9
            L_0x008f:
                com.fossil.t87.a(r23)
                com.fossil.yi7 r0 = r1.p$
                com.portfolio.platform.data.model.diana.preset.DianaPreset r11 = r1.$it
                java.util.ArrayList r11 = r11.getComplications()
                java.util.ArrayList r13 = new java.util.ArrayList
                r13.<init>()
                java.util.Iterator r11 = r11.iterator()
            L_0x00a3:
                boolean r14 = r11.hasNext()
                if (r14 == 0) goto L_0x00c8
                java.lang.Object r14 = r11.next()
                r15 = r14
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r15 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r15
                com.fossil.wd5 r8 = com.fossil.wd5.b
                java.lang.String r15 = r15.getId()
                boolean r8 = r8.c(r15)
                java.lang.Boolean r8 = com.fossil.pb7.a(r8)
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x00a3
                r13.add(r14)
                goto L_0x00a3
            L_0x00c8:
                boolean r8 = r13.isEmpty()
                r11 = 1
                r8 = r8 ^ r11
                if (r8 == 0) goto L_0x01df
                java.util.Iterator r8 = r13.iterator()
                r15 = r0
                r11 = r8
                r14 = r13
                r13 = 0
                r8 = r1
            L_0x00d9:
                boolean r0 = r11.hasNext()
                if (r0 == 0) goto L_0x01d9
                java.lang.Object r0 = r11.next()
                com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r0 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r0
                com.misfit.frameworks.buttonservice.log.FLogger r19 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r19.getLocal()
                r19 = r9
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                r9.append(r7)
                r9.append(r0)
                java.lang.String r9 = r9.toString()
                r1.d(r5, r9)
                java.lang.String r1 = r0.getSettings()     // Catch:{ Exception -> 0x01b6 }
                boolean r1 = com.fossil.sc5.a(r1)     // Catch:{ Exception -> 0x01b6 }
                if (r1 == 0) goto L_0x015b
                com.fossil.wz5 r1 = r8.this$0     // Catch:{ Exception -> 0x01b6 }
                com.fossil.ti7 r1 = r1.b()     // Catch:{ Exception -> 0x01b6 }
                com.fossil.wz5$c$a r9 = new com.fossil.wz5$c$a     // Catch:{ Exception -> 0x01b6 }
                r20 = r7
                r7 = 0
                r9.<init>(r8, r0, r7)     // Catch:{ Exception -> 0x0159 }
                r8.L$0 = r15     // Catch:{ Exception -> 0x0159 }
                r8.L$1 = r14     // Catch:{ Exception -> 0x0159 }
                r8.L$2 = r13     // Catch:{ Exception -> 0x0159 }
                r8.L$3 = r0     // Catch:{ Exception -> 0x0159 }
                r8.L$4 = r11     // Catch:{ Exception -> 0x0159 }
                r7 = 1
                r8.label = r7     // Catch:{ Exception -> 0x0159 }
                java.lang.Object r1 = com.fossil.vh7.a(r1, r9, r8)     // Catch:{ Exception -> 0x0159 }
                if (r1 != r2) goto L_0x012b
                return r2
            L_0x012b:
                android.os.Parcelable r1 = (android.os.Parcelable) r1     // Catch:{ Exception -> 0x0159 }
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0159 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ Exception -> 0x0159 }
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0159 }
                r9.<init>()     // Catch:{ Exception -> 0x0159 }
                r9.append(r3)     // Catch:{ Exception -> 0x0159 }
                r9.append(r1)     // Catch:{ Exception -> 0x0159 }
                java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0159 }
                r7.d(r5, r9)     // Catch:{ Exception -> 0x0159 }
                if (r1 == 0) goto L_0x0156
                com.fossil.wz5 r7 = r8.this$0     // Catch:{ Exception -> 0x0159 }
                com.google.gson.Gson r7 = r7.i     // Catch:{ Exception -> 0x0159 }
                java.lang.String r1 = r7.a(r1)     // Catch:{ Exception -> 0x0159 }
                r0.setSettings(r1)     // Catch:{ Exception -> 0x0159 }
                goto L_0x01d1
            L_0x0156:
                r13 = r0
                goto L_0x01dd
            L_0x0159:
                r0 = move-exception
                goto L_0x01b9
            L_0x015b:
                r20 = r7
                java.lang.String r1 = r0.getId()     // Catch:{ Exception -> 0x0159 }
                int r7 = r1.hashCode()     // Catch:{ Exception -> 0x0159 }
                r9 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
                if (r7 == r9) goto L_0x0193
                r9 = 134170930(0x7ff4932, float:3.8411156E-34)
                if (r7 == r9) goto L_0x0170
                goto L_0x01d1
            L_0x0170:
                boolean r1 = r1.equals(r6)     // Catch:{ Exception -> 0x0159 }
                if (r1 == 0) goto L_0x01d1
                com.fossil.wz5 r1 = r8.this$0     // Catch:{ Exception -> 0x0159 }
                com.google.gson.Gson r1 = r1.i     // Catch:{ Exception -> 0x0159 }
                java.lang.String r7 = r0.getSettings()     // Catch:{ Exception -> 0x0159 }
                java.lang.Class<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> r9 = com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class
                java.lang.Object r1 = r1.a(r7, r9)     // Catch:{ Exception -> 0x0159 }
                com.portfolio.platform.data.model.setting.SecondTimezoneSetting r1 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r1     // Catch:{ Exception -> 0x0159 }
                java.lang.String r1 = r1.getTimeZoneId()     // Catch:{ Exception -> 0x0159 }
                boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0159 }
                if (r1 == 0) goto L_0x01d1
                goto L_0x01b5
            L_0x0193:
                boolean r1 = r1.equals(r12)     // Catch:{ Exception -> 0x0159 }
                if (r1 == 0) goto L_0x01d1
                com.fossil.wz5 r1 = r8.this$0     // Catch:{ Exception -> 0x0159 }
                com.google.gson.Gson r1 = r1.i     // Catch:{ Exception -> 0x0159 }
                java.lang.String r7 = r0.getSettings()     // Catch:{ Exception -> 0x0159 }
                java.lang.Class<com.portfolio.platform.data.model.setting.CommuteTimeSetting> r9 = com.portfolio.platform.data.model.setting.CommuteTimeSetting.class
                java.lang.Object r1 = r1.a(r7, r9)     // Catch:{ Exception -> 0x0159 }
                com.portfolio.platform.data.model.setting.CommuteTimeSetting r1 = (com.portfolio.platform.data.model.setting.CommuteTimeSetting) r1     // Catch:{ Exception -> 0x0159 }
                java.lang.String r1 = r1.getAddress()     // Catch:{ Exception -> 0x0159 }
                boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0159 }
                if (r1 == 0) goto L_0x01d1
            L_0x01b5:
                goto L_0x0156
            L_0x01b6:
                r0 = move-exception
                r20 = r7
            L_0x01b9:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                r7.append(r10)
                r7.append(r0)
                java.lang.String r0 = r7.toString()
                r1.e(r5, r0)
            L_0x01d1:
                r1 = r22
                r9 = r19
                r7 = r20
                goto L_0x00d9
            L_0x01d9:
                r20 = r7
                r19 = r9
            L_0x01dd:
                r0 = r15
                goto L_0x01e7
            L_0x01df:
                r20 = r7
                r19 = r9
                r8 = r22
                r14 = r13
                r13 = 0
            L_0x01e7:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "setPresetToWatch missingSettingComplication "
                r7.append(r9)
                r7.append(r13)
                java.lang.String r7 = r7.toString()
                r1.d(r5, r7)
                if (r13 == 0) goto L_0x025d
                java.lang.String r0 = r13.getId()
                int r1 = r0.hashCode()
                r2 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
                if (r1 == r2) goto L_0x022f
                r2 = 134170930(0x7ff4932, float:3.8411156E-34)
                if (r1 == r2) goto L_0x0216
                goto L_0x0246
            L_0x0216:
                boolean r0 = r0.equals(r6)
                if (r0 == 0) goto L_0x0246
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886524(0x7f1201bc, float:1.940763E38)
                java.lang.String r9 = com.fossil.ig5.a(r0, r1)
                java.lang.String r0 = "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)"
                com.fossil.ee7.a(r9, r0)
                goto L_0x0248
            L_0x022f:
                boolean r0 = r0.equals(r12)
                if (r0 == 0) goto L_0x0246
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886362(0x7f12011a, float:1.94073E38)
                java.lang.String r9 = com.fossil.ig5.a(r0, r1)
                com.fossil.ee7.a(r9, r4)
                goto L_0x0248
            L_0x0246:
                r9 = r19
            L_0x0248:
                com.fossil.wz5 r0 = r8.this$0
                com.fossil.rz5 r0 = r0.m
                java.lang.String r1 = r13.getId()
                java.lang.String r2 = r13.getPosition()
                r3 = 1
                r0.a(r9, r1, r2, r3)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x025d:
                com.portfolio.platform.data.model.diana.preset.DianaPreset r1 = r8.$it
                java.util.ArrayList r1 = r1.getWatchapps()
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x026c:
                boolean r7 = r1.hasNext()
                if (r7 == 0) goto L_0x0291
                java.lang.Object r7 = r1.next()
                r9 = r7
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r9 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r9
                com.fossil.cf5 r11 = com.fossil.cf5.c
                java.lang.String r9 = r9.getId()
                boolean r9 = r11.f(r9)
                java.lang.Boolean r9 = com.fossil.pb7.a(r9)
                boolean r9 = r9.booleanValue()
                if (r9 == 0) goto L_0x026c
                r6.add(r7)
                goto L_0x026c
            L_0x0291:
                boolean r1 = r6.isEmpty()
                r7 = 1
                r1 = r1 ^ r7
                if (r1 == 0) goto L_0x037d
                java.util.Iterator r1 = r6.iterator()
                r15 = r0
                r11 = r6
                r6 = r1
                r1 = 0
            L_0x02a1:
                boolean r0 = r6.hasNext()
                if (r0 == 0) goto L_0x0379
                java.lang.Object r0 = r6.next()
                com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r0 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r0
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                r18 = r4
                r4 = r20
                r9.append(r4)
                r9.append(r0)
                java.lang.String r9 = r9.toString()
                r7.d(r5, r9)
                java.lang.String r7 = r0.getSettings()     // Catch:{ Exception -> 0x035a }
                boolean r7 = com.fossil.sc5.a(r7)     // Catch:{ Exception -> 0x035a }
                if (r7 == 0) goto L_0x0327
                com.fossil.wz5 r7 = r8.this$0     // Catch:{ Exception -> 0x035a }
                com.fossil.ti7 r7 = r7.b()     // Catch:{ Exception -> 0x035a }
                com.fossil.wz5$c$b r9 = new com.fossil.wz5$c$b     // Catch:{ Exception -> 0x035a }
                r20 = r4
                r4 = 0
                r9.<init>(r8, r0, r4)     // Catch:{ Exception -> 0x0325 }
                r8.L$0 = r15     // Catch:{ Exception -> 0x0325 }
                r8.L$1 = r14     // Catch:{ Exception -> 0x0325 }
                r8.L$2 = r13     // Catch:{ Exception -> 0x0325 }
                r8.L$3 = r11     // Catch:{ Exception -> 0x0325 }
                r8.L$4 = r1     // Catch:{ Exception -> 0x0325 }
                r8.L$5 = r0     // Catch:{ Exception -> 0x0325 }
                r8.L$6 = r6     // Catch:{ Exception -> 0x0325 }
                r4 = 2
                r8.label = r4     // Catch:{ Exception -> 0x0325 }
                java.lang.Object r7 = com.fossil.vh7.a(r7, r9, r8)     // Catch:{ Exception -> 0x0325 }
                if (r7 != r2) goto L_0x02f9
                return r2
            L_0x02f9:
                android.os.Parcelable r7 = (android.os.Parcelable) r7     // Catch:{ Exception -> 0x0325 }
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0325 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()     // Catch:{ Exception -> 0x0325 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0325 }
                r4.<init>()     // Catch:{ Exception -> 0x0325 }
                r4.append(r3)     // Catch:{ Exception -> 0x0325 }
                r4.append(r7)     // Catch:{ Exception -> 0x0325 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0325 }
                r9.d(r5, r4)     // Catch:{ Exception -> 0x0325 }
                if (r7 == 0) goto L_0x0323
                com.fossil.wz5 r4 = r8.this$0     // Catch:{ Exception -> 0x0325 }
                com.google.gson.Gson r4 = r4.i     // Catch:{ Exception -> 0x0325 }
                java.lang.String r4 = r4.a(r7)     // Catch:{ Exception -> 0x0325 }
                r0.setSettings(r4)     // Catch:{ Exception -> 0x0325 }
                goto L_0x0375
            L_0x0323:
                r14 = r0
                goto L_0x0380
            L_0x0325:
                r0 = move-exception
                goto L_0x035d
            L_0x0327:
                r20 = r4
                java.lang.String r4 = r0.getId()     // Catch:{ Exception -> 0x0325 }
                int r7 = r4.hashCode()     // Catch:{ Exception -> 0x0325 }
                r9 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
                if (r7 == r9) goto L_0x0337
                goto L_0x0375
            L_0x0337:
                boolean r4 = r4.equals(r12)     // Catch:{ Exception -> 0x0325 }
                if (r4 == 0) goto L_0x0375
                com.fossil.wz5 r4 = r8.this$0     // Catch:{ Exception -> 0x0325 }
                com.google.gson.Gson r4 = r4.i     // Catch:{ Exception -> 0x0325 }
                java.lang.String r7 = r0.getSettings()     // Catch:{ Exception -> 0x0325 }
                java.lang.Class<com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting> r9 = com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.class
                java.lang.Object r4 = r4.a(r7, r9)     // Catch:{ Exception -> 0x0325 }
                com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting r4 = (com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting) r4     // Catch:{ Exception -> 0x0325 }
                java.util.List r4 = r4.getAddresses()     // Catch:{ Exception -> 0x0325 }
                boolean r4 = r4.isEmpty()     // Catch:{ Exception -> 0x0325 }
                if (r4 == 0) goto L_0x0375
                goto L_0x0323
            L_0x035a:
                r0 = move-exception
                r20 = r4
            L_0x035d:
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                r7.append(r10)
                r7.append(r0)
                java.lang.String r0 = r7.toString()
                r4.e(r5, r0)
            L_0x0375:
                r4 = r18
                goto L_0x02a1
            L_0x0379:
                r18 = r4
                r14 = r1
                goto L_0x0380
            L_0x037d:
                r18 = r4
                r14 = 0
            L_0x0380:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "setPresetToWatch missingSettingWatchApp "
                r1.append(r2)
                r1.append(r14)
                java.lang.String r1 = r1.toString()
                r0.d(r5, r1)
                if (r14 == 0) goto L_0x03fa
                java.lang.String r0 = r14.getId()
                int r1 = r0.hashCode()
                r2 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
                if (r1 == r2) goto L_0x03ca
                r2 = 1223440372(0x48ec37f4, float:483775.62)
                if (r1 == r2) goto L_0x03af
                goto L_0x03e3
            L_0x03af:
                java.lang.String r1 = "weather"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x03e3
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886531(0x7f1201c3, float:1.9407643E38)
                java.lang.String r9 = com.fossil.ig5.a(r0, r1)
                java.lang.String r0 = "LanguageHelper.getString\u2026isplayWeatherOnYourWatch)"
                com.fossil.ee7.a(r9, r0)
                goto L_0x03e5
            L_0x03ca:
                boolean r0 = r0.equals(r12)
                if (r0 == 0) goto L_0x03e3
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886362(0x7f12011a, float:1.94073E38)
                java.lang.String r9 = com.fossil.ig5.a(r0, r1)
                r1 = r18
                com.fossil.ee7.a(r9, r1)
                goto L_0x03e5
            L_0x03e3:
                r9 = r19
            L_0x03e5:
                com.fossil.wz5 r0 = r8.this$0
                com.fossil.rz5 r0 = r0.m
                java.lang.String r1 = r14.getId()
                java.lang.String r2 = r14.getPosition()
                r3 = 0
                r0.a(r9, r1, r2, r3)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x03fa:
                com.fossil.xg5 r9 = com.fossil.xg5.b
                com.fossil.wz5 r0 = r8.this$0
                com.fossil.rz5 r0 = r0.m
                if (r0 == 0) goto L_0x048e
                com.fossil.sz5 r0 = (com.fossil.sz5) r0
                android.content.Context r10 = r0.getContext()
                com.fossil.xg5$a r11 = com.fossil.xg5.a.SET_BLE_COMMAND
                r12 = 0
                r13 = 0
                r14 = 0
                r15 = 0
                r16 = 60
                r17 = 0
                boolean r0 = com.fossil.xg5.a(r9, r10, r11, r12, r13, r14, r15, r16, r17)
                if (r0 != 0) goto L_0x041d
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x041d:
                com.fossil.wz5 r0 = r8.this$0
                int r0 = r0.k()
                if (r0 == 0) goto L_0x0450
                boolean r0 = r8.$shouldShowSkippedPermissions$inlined
                if (r0 == 0) goto L_0x0450
                com.fossil.xg5 r9 = com.fossil.xg5.b
                com.fossil.wz5 r0 = r8.this$0
                com.fossil.rz5 r0 = r0.m
                com.fossil.sz5 r0 = (com.fossil.sz5) r0
                android.content.Context r10 = r0.getContext()
                com.fossil.ve5 r0 = com.fossil.ve5.a
                com.portfolio.platform.data.model.diana.preset.DianaPreset r1 = r8.$it
                java.util.List r11 = r0.a(r1)
                r12 = 0
                r13 = 0
                r14 = 1
                r15 = 0
                r16 = 44
                r17 = 0
                boolean r0 = com.fossil.xg5.a(r9, r10, r11, r12, r13, r14, r15, r16, r17)
                if (r0 != 0) goto L_0x0450
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x0450:
                com.fossil.wz5 r0 = r8.this$0
                com.fossil.rz5 r0 = r0.m
                r0.k()
                com.fossil.wz5 r0 = r8.this$0
                int r0 = r0.k()
                r1 = 1
                if (r0 != r1) goto L_0x046b
                com.fossil.qd5$a r0 = com.fossil.qd5.f
                java.lang.String r1 = "set_complication"
                com.fossil.if5 r0 = r0.b(r1)
                goto L_0x0473
            L_0x046b:
                com.fossil.qd5$a r0 = com.fossil.qd5.f
                java.lang.String r1 = "set_watch_apps"
                com.fossil.if5 r0 = r0.b(r1)
            L_0x0473:
                r0.d()
                com.fossil.wz5 r1 = r8.this$0
                com.fossil.z46 r1 = r1.p
                com.fossil.z46$c r2 = new com.fossil.z46$c
                com.portfolio.platform.data.model.diana.preset.DianaPreset r3 = r8.$currentPreset$inlined
                r2.<init>(r3)
                com.fossil.wz5$c$c r3 = new com.fossil.wz5$c$c
                r3.<init>(r8, r0)
                r1.a(r2, r3)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x048e:
                com.fossil.x87 r0 = new com.fossil.x87
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wz5.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public d(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(DianaPreset dianaPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe current preset value=" + dianaPreset);
            MutableLiveData b = this.a.f;
            if (dianaPreset != null) {
                b.a(dianaPreset.clone());
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public e(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "start - observe selected watchApp value=" + str);
            rz5 k = this.a.m;
            if (str != null) {
                k.O(str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public f(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "observe selected complication pos=" + str);
            rz5 k = this.a.m;
            if (str != null) {
                k.E(str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<dz5> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public g(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(dz5 dz5) {
            if (dz5 != null) {
                this.a.m.a(dz5, wz5.g(this.a).c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public h(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper e = wz5.g(this.a).e(str);
                this.a.k = e;
                this.a.m.a(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore
        public i(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "update preset status isChanged=" + bool);
            rz5 k = this.a.m;
            if (bool != null) {
                k.f(bool.booleanValue());
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ wz5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter$wrapperPresetTransformations$1$1", f = "DianaCustomizeEditPresenter.kt", l = {96, 120}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<dz5>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaPreset $preset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$13;
            @DexIgnore
            public Object L$14;
            @DexIgnore
            public Object L$15;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, DianaPreset dianaPreset, fb7 fb7) {
                super(2, fb7);
                this.this$0 = jVar;
                this.$preset = dianaPreset;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$preset, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<dz5> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0114  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r27) {
                /*
                    r26 = this;
                    r0 = r26
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    java.lang.String r3 = "preset"
                    r4 = 2
                    r5 = 1
                    java.lang.String r6 = "DianaCustomizeEditPresenter"
                    if (r2 == 0) goto L_0x0094
                    if (r2 == r5) goto L_0x003d
                    if (r2 != r4) goto L_0x0035
                    java.lang.Object r1 = r0.L$6
                    java.util.ArrayList r1 = (java.util.ArrayList) r1
                    java.lang.Object r1 = r0.L$5
                    java.util.ArrayList r1 = (java.util.ArrayList) r1
                    java.lang.Object r1 = r0.L$4
                    com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r1
                    java.lang.Object r1 = r0.L$3
                    java.util.List r1 = (java.util.List) r1
                    java.lang.Object r1 = r0.L$2
                    java.util.ArrayList r1 = (java.util.ArrayList) r1
                    java.lang.Object r1 = r0.L$1
                    java.util.ArrayList r1 = (java.util.ArrayList) r1
                    java.lang.Object r1 = r0.L$0
                    com.fossil.vd r1 = (com.fossil.vd) r1
                    com.fossil.t87.a(r27)
                    goto L_0x0313
                L_0x0035:
                    java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                    java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                    r1.<init>(r2)
                    throw r1
                L_0x003d:
                    java.lang.Object r2 = r0.L$15
                    java.util.ArrayList r2 = (java.util.ArrayList) r2
                    java.lang.Object r7 = r0.L$14
                    java.lang.String r7 = (java.lang.String) r7
                    java.lang.Object r8 = r0.L$13
                    java.lang.String r8 = (java.lang.String) r8
                    java.lang.Object r9 = r0.L$12
                    java.lang.String r9 = (java.lang.String) r9
                    java.lang.Object r10 = r0.L$11
                    java.lang.String r10 = (java.lang.String) r10
                    java.lang.Object r11 = r0.L$10
                    com.portfolio.platform.data.model.diana.Complication r11 = (com.portfolio.platform.data.model.diana.Complication) r11
                    java.lang.Object r11 = r0.L$9
                    com.portfolio.platform.data.model.diana.Complication r11 = (com.portfolio.platform.data.model.diana.Complication) r11
                    java.lang.Object r11 = r0.L$8
                    java.lang.String r11 = (java.lang.String) r11
                    java.lang.Object r11 = r0.L$7
                    java.lang.String r11 = (java.lang.String) r11
                    java.lang.Object r11 = r0.L$6
                    java.util.Iterator r11 = (java.util.Iterator) r11
                    java.lang.Object r12 = r0.L$5
                    java.util.ArrayList r12 = (java.util.ArrayList) r12
                    java.lang.Object r13 = r0.L$4
                    com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r13 = (com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper) r13
                    java.lang.Object r14 = r0.L$3
                    java.util.List r14 = (java.util.List) r14
                    java.lang.Object r15 = r0.L$2
                    java.util.ArrayList r15 = (java.util.ArrayList) r15
                    java.lang.Object r4 = r0.L$1
                    java.util.ArrayList r4 = (java.util.ArrayList) r4
                    java.lang.Object r5 = r0.L$0
                    com.fossil.vd r5 = (com.fossil.vd) r5
                    com.fossil.t87.a(r27)
                    r23 = r6
                    r16 = r13
                    r17 = r14
                    r18 = r15
                    r6 = r27
                    r14 = r0
                    r0 = r11
                    r15 = r12
                    r11 = r9
                    r12 = r10
                    r9 = r7
                    r10 = r8
                    r7 = 1
                    goto L_0x01d2
                L_0x0094:
                    com.fossil.t87.a(r27)
                    com.fossil.vd r2 = r0.p$
                    java.util.ArrayList r4 = new java.util.ArrayList
                    r4.<init>()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r5 = r0.$preset
                    java.util.ArrayList r5 = r5.getComplications()
                    r4.addAll(r5)
                    java.util.ArrayList r5 = new java.util.ArrayList
                    r5.<init>()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = r0.$preset
                    java.util.ArrayList r7 = r7.getWatchapps()
                    r5.addAll(r7)
                    com.fossil.ve5 r7 = com.fossil.ve5.a
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = r0.$preset
                    com.fossil.ee7.a(r8, r3)
                    java.util.List r7 = r7.a(r8)
                    com.fossil.wz5$j r8 = r0.this$0
                    com.fossil.wz5 r8 = r8.a
                    com.fossil.a06 r8 = com.fossil.wz5.g(r8)
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r9 = r0.$preset
                    java.lang.String r9 = r9.getWatchFaceId()
                    com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r8 = r8.e(r9)
                    java.util.ArrayList r9 = new java.util.ArrayList
                    r9.<init>()
                    com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    java.lang.String r12 = "wrapperPresetTransformations presetChanged complications="
                    r11.append(r12)
                    r11.append(r4)
                    java.lang.String r12 = " watchapps="
                    r11.append(r12)
                    r11.append(r5)
                    java.lang.String r12 = " backgroundWrapper="
                    r11.append(r12)
                    r11.append(r8)
                    java.lang.String r11 = r11.toString()
                    r10.d(r6, r11)
                    java.util.Iterator r10 = r4.iterator()
                    r12 = r0
                    r13 = r5
                    r14 = r7
                    r15 = r8
                    r11 = r10
                    r5 = r2
                    r2 = r9
                L_0x010c:
                    boolean r7 = r11.hasNext()
                    java.lang.String r8 = ""
                    if (r7 == 0) goto L_0x01f6
                    java.lang.Object r7 = r11.next()
                    com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r7 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r7
                    java.lang.String r10 = r7.component1()
                    java.lang.String r7 = r7.component2()
                    com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>()
                    r27 = r8
                    java.lang.String r8 = "wrapperPresetTransformations find complicationId="
                    r0.append(r8)
                    r0.append(r7)
                    java.lang.String r0 = r0.toString()
                    r9.d(r6, r0)
                    com.fossil.wz5$j r0 = r12.this$0
                    com.fossil.wz5 r0 = r0.a
                    com.fossil.a06 r0 = com.fossil.wz5.g(r0)
                    com.portfolio.platform.data.model.diana.Complication r0 = r0.b(r7)
                    if (r0 == 0) goto L_0x01ec
                    java.lang.String r8 = r0.getComplicationId()
                    java.lang.String r9 = r0.getIcon()
                    if (r9 == 0) goto L_0x0157
                    goto L_0x0159
                L_0x0157:
                    r9 = r27
                L_0x0159:
                    com.portfolio.platform.PortfolioApp$a r17 = com.portfolio.platform.PortfolioApp.g0
                    r23 = r6
                    com.portfolio.platform.PortfolioApp r6 = r17.c()
                    r24 = r1
                    java.lang.String r1 = r0.getNameKey()
                    r25 = r8
                    java.lang.String r8 = r0.getName()
                    java.lang.String r1 = com.fossil.ig5.a(r6, r1, r8)
                    com.fossil.wz5$j r6 = r12.this$0
                    com.fossil.wz5 r6 = r6.a
                    com.fossil.og5 r17 = r6.q
                    com.fossil.wz5$j r6 = r12.this$0
                    com.fossil.wz5 r6 = r6.a
                    com.portfolio.platform.data.model.MFUser r18 = r6.j
                    com.fossil.wz5$j r6 = r12.this$0
                    com.fossil.wz5 r6 = r6.a
                    java.util.ArrayList r19 = r6.h
                    java.lang.String r20 = r0.getComplicationId()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r6 = r12.$preset
                    com.fossil.ee7.a(r6, r3)
                    r12.L$0 = r5
                    r12.L$1 = r4
                    r12.L$2 = r13
                    r12.L$3 = r14
                    r12.L$4 = r15
                    r12.L$5 = r2
                    r12.L$6 = r11
                    r12.L$7 = r10
                    r12.L$8 = r7
                    r12.L$9 = r0
                    r12.L$10 = r0
                    r12.L$11 = r10
                    r12.L$12 = r1
                    r12.L$13 = r9
                    r0 = r25
                    r12.L$14 = r0
                    r12.L$15 = r2
                    r7 = 1
                    r12.label = r7
                    r21 = r6
                    r22 = r12
                    java.lang.Object r6 = r17.a(r18, r19, r20, r21, r22)
                    r8 = r24
                    if (r6 != r8) goto L_0x01c4
                    return r8
                L_0x01c4:
                    r18 = r13
                    r17 = r14
                    r16 = r15
                    r15 = r2
                    r14 = r12
                    r12 = r10
                    r10 = r9
                    r9 = r0
                    r0 = r11
                    r11 = r1
                    r1 = r8
                L_0x01d2:
                    r13 = r6
                    java.lang.String r13 = (java.lang.String) r13
                    com.fossil.fz5 r6 = new com.fossil.fz5
                    r8 = r6
                    r8.<init>(r9, r10, r11, r12, r13)
                    boolean r2 = r2.add(r6)
                    com.fossil.pb7.a(r2)
                    r11 = r0
                    r12 = r14
                    r2 = r15
                    r15 = r16
                    r14 = r17
                    r13 = r18
                    goto L_0x01f0
                L_0x01ec:
                    r8 = r1
                    r23 = r6
                    r7 = 1
                L_0x01f0:
                    r0 = r26
                    r6 = r23
                    goto L_0x010c
                L_0x01f6:
                    r23 = r6
                    r27 = r8
                    r8 = r1
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r3 = "wrapperPresetTransformations presetChanged complicationsDetails="
                    r1.append(r3)
                    r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    r3 = r23
                    r0.d(r3, r1)
                    java.util.ArrayList r0 = new java.util.ArrayList
                    r0.<init>()
                    java.util.Iterator r1 = r13.iterator()
                L_0x0220:
                    boolean r6 = r1.hasNext()
                    if (r6 == 0) goto L_0x0278
                    java.lang.Object r6 = r1.next()
                    com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r6 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r6
                    java.lang.String r20 = r6.component1()
                    java.lang.String r6 = r6.component2()
                    com.fossil.wz5$j r7 = r12.this$0
                    com.fossil.wz5 r7 = r7.a
                    com.fossil.a06 r7 = com.fossil.wz5.g(r7)
                    com.portfolio.platform.data.model.diana.WatchApp r6 = r7.d(r6)
                    if (r6 == 0) goto L_0x0220
                    com.fossil.fz5 r7 = new com.fossil.fz5
                    java.lang.String r17 = r6.getWatchappId()
                    java.lang.String r9 = r6.getIcon()
                    if (r9 == 0) goto L_0x0251
                    r18 = r9
                    goto L_0x0253
                L_0x0251:
                    r18 = r27
                L_0x0253:
                    com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r9 = r9.c()
                    java.lang.String r10 = r6.getNameKey()
                    java.lang.String r6 = r6.getName()
                    java.lang.String r19 = com.fossil.ig5.a(r9, r10, r6)
                    r21 = 0
                    r22 = 16
                    r23 = 0
                    r16 = r7
                    r16.<init>(r17, r18, r19, r20, r21, r22, r23)
                    boolean r6 = r0.add(r7)
                    com.fossil.pb7.a(r6)
                    goto L_0x0220
                L_0x0278:
                    com.fossil.wz5$j r1 = r12.this$0
                    com.fossil.wz5 r1 = r1.a
                    androidx.lifecycle.MutableLiveData r1 = r1.g
                    java.lang.Object r1 = r1.a()
                    com.fossil.dz5 r1 = (com.fossil.dz5) r1
                    if (r1 == 0) goto L_0x029e
                    com.fossil.wz5$j r6 = r12.this$0
                    com.fossil.wz5 r6 = r6.a
                    java.util.ArrayList r7 = r1.a()
                    r6.a(r7, r2)
                    com.fossil.wz5$j r6 = r12.this$0
                    com.fossil.wz5 r6 = r6.a
                    java.util.ArrayList r1 = r1.f()
                    r6.b(r1, r0)
                L_0x029e:
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder
                    r6.<init>()
                    java.lang.String r7 = "wrapperPresetTransformations presetChanged watchAppsDetail="
                    r6.append(r7)
                    r6.append(r0)
                    java.lang.String r6 = r6.toString()
                    r1.d(r3, r6)
                    com.fossil.wz5$j r1 = r12.this$0
                    com.fossil.wz5 r1 = r1.a
                    androidx.lifecycle.MutableLiveData r1 = r1.g
                    com.fossil.dz5 r3 = new com.fossil.dz5
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r6 = r12.$preset
                    java.lang.String r6 = r6.getId()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = r12.$preset
                    java.lang.String r9 = r7.getName()
                    com.portfolio.platform.data.model.diana.preset.DianaPreset r7 = r12.$preset
                    boolean r16 = r7.isActive()
                    r7 = r3
                    r11 = r8
                    r8 = r6
                    r10 = r2
                    r6 = r11
                    r11 = r0
                    r24 = r6
                    r6 = r12
                    r12 = r14
                    r27 = r0
                    r0 = r13
                    r13 = r16
                    r16 = r2
                    r2 = r14
                    r14 = r15
                    r7.<init>(r8, r9, r10, r11, r12, r13, r14)
                    r1.a(r3)
                    com.fossil.wz5$j r1 = r6.this$0
                    com.fossil.wz5 r1 = r1.a
                    androidx.lifecycle.MutableLiveData r1 = r1.g
                    r6.L$0 = r5
                    r6.L$1 = r4
                    r6.L$2 = r0
                    r6.L$3 = r2
                    r6.L$4 = r15
                    r2 = r16
                    r6.L$5 = r2
                    r0 = r27
                    r6.L$6 = r0
                    r0 = 2
                    r6.label = r0
                    java.lang.Object r0 = r5.a(r1, r6)
                    r1 = r24
                    if (r0 != r1) goto L_0x0313
                    return r1
                L_0x0313:
                    com.fossil.i97 r0 = com.fossil.i97.a
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.wz5.j.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public j(wz5 wz5) {
            this.a = wz5;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<dz5> apply(DianaPreset dianaPreset) {
            return ed.a(null, 0, new a(this, dianaPreset, null), 3, null);
        }
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    public wz5(rz5 rz5, UserRepository userRepository, int i2, z46 z46, og5 og5, ch5 ch5) {
        ee7.b(rz5, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(z46, "mSetDianaPresetToWatchUseCase");
        ee7.b(og5, "mCustomizeRealDataManager");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.m = rz5;
        this.n = userRepository;
        this.o = i2;
        this.p = z46;
        this.q = og5;
        this.r = ch5;
        ik7 unused = xh7.b(e(), null, null, new a(this, null), 3, null);
        LiveData<dz5> b2 = ge.b(this.f, new j(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026erPreset)\n        }\n    }");
        this.l = b2;
    }

    @DexIgnore
    public static final /* synthetic */ a06 g(wz5 wz5) {
        a06 a06 = wz5.e;
        if (a06 != null) {
            return a06;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void l() {
        DianaPreset a2 = this.f.a();
        if (a2 != null) {
            a06 a06 = this.e;
            if (a06 != null) {
                DianaPreset d2 = a06.d();
                if (d2 != null && (!ee7.a((Object) a2.getWatchFaceId(), (Object) d2.getWatchFaceId()))) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditPresenter", "Watch face changed from " + d2.getWatchFaceId() + " to " + a2.getWatchFaceId());
                    qd5.f.c().a("set_background_manually");
                    return;
                }
                return;
            }
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        this.m.a(this);
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void c(String str, String str2) {
        T t;
        ee7.b(str, "fromPosition");
        ee7.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - fromPosition=" + str + ", toPosition=" + str2);
        if (!ee7.a((Object) str, (Object) str2)) {
            a06 a06 = this.e;
            T t2 = null;
            if (a06 != null) {
                DianaPreset a2 = a06.a().a();
                if (a2 != null) {
                    DianaPreset clone = a2.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (ee7.a((Object) t.getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = clone.getComplications().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (!(t3 == null || t4 == null)) {
                        t3.setPosition(str2);
                        t4.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapComplication - update preset " + clone);
                    a(clone);
                    return;
                }
                return;
            }
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void d(String str, String str2) {
        T t;
        ee7.b(str, "fromPosition");
        ee7.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!ee7.a((Object) str, (Object) str2)) {
            a06 a06 = this.e;
            T t2 = null;
            if (a06 != null) {
                DianaPreset a2 = a06.a().a();
                if (a2 != null) {
                    Iterator<T> it = a2.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (ee7.a((Object) t.getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = a2.getWatchapps().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.setPosition(str2);
                    }
                    if (t4 != null) {
                        t4.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "swapWatchApp - update preset");
                    ee7.a((Object) a2, "currentPreset");
                    a(a2);
                    return;
                }
                return;
            }
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.m.h(this.o);
        this.p.f();
        nj5.d.a(CommunicateMode.SET_COMPLICATION_APPS, CommunicateMode.SET_WATCH_APPS, CommunicateMode.SET_PRESET_APPS_DATA);
        a06 a06 = this.e;
        if (a06 != null) {
            MutableLiveData<DianaPreset> a2 = a06.a();
            rz5 rz5 = this.m;
            if (rz5 != null) {
                a2.a((sz5) rz5, new d(this));
                a06 a062 = this.e;
                if (a062 != null) {
                    a062.i().a((LifecycleOwner) this.m, new e(this));
                    a06 a063 = this.e;
                    if (a063 != null) {
                        a063.f().a((LifecycleOwner) this.m, new f(this));
                        this.l.a((LifecycleOwner) this.m, new g(this));
                        a06 a064 = this.e;
                        if (a064 != null) {
                            a064.g().a((LifecycleOwner) this.m, new h(this));
                            a06 a065 = this.e;
                            if (a065 != null) {
                                a065.b().a((LifecycleOwner) this.m, new i(this));
                            } else {
                                ee7.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("mDianaCustomizeViewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.p.g();
        a06 a06 = this.e;
        if (a06 != null) {
            MutableLiveData<DianaPreset> a2 = a06.a();
            rz5 rz5 = this.m;
            if (rz5 != null) {
                a2.a((sz5) rz5);
                a06 a062 = this.e;
                if (a062 != null) {
                    a062.i().a((LifecycleOwner) this.m);
                    a06 a063 = this.e;
                    if (a063 != null) {
                        a063.f().a((LifecycleOwner) this.m);
                        this.g.a((LifecycleOwner) this.m);
                        return;
                    }
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
                ee7.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void h() {
        a06 a06 = this.e;
        if (a06 != null) {
            boolean l2 = a06.l();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "isPresetChanged " + l2);
            if (l2) {
                this.m.o();
            } else {
                this.m.g(false);
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void i() {
        this.m.a(this.k);
    }

    @DexIgnore
    public final ArrayList<fz5> j() {
        dz5 a2 = this.l.a();
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    @DexIgnore
    public final int k() {
        return this.o;
    }

    @DexIgnore
    public final void b(int i2) {
        this.o = i2;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void b(String str) {
        ee7.b(str, "watchAppPos");
        a06 a06 = this.e;
        if (a06 != null) {
            a06.k(str);
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void a(a06 a06) {
        ee7.b(a06, "viewModel");
        this.e = a06;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void b(String str, String str2) {
        ee7.b(str, "watchAppId");
        ee7.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropWatchApp - watchAppId=" + str + ", toPosition=" + str2);
        a06 a06 = this.e;
        T t = null;
        if (a06 != null) {
            if (!a06.i(str)) {
                a06 a062 = this.e;
                if (a062 != null) {
                    DianaPreset a2 = a062.a().a();
                    if (a2 != null) {
                        Iterator<T> it = a2.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            T next = it.next();
                            if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                                t = next;
                                break;
                            }
                        }
                        T t2 = t;
                        if (t2 != null) {
                            t2.setId(str);
                        }
                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "dropWatchApp - update preset");
                        ee7.a((Object) a2, "currentPreset");
                        a(a2);
                    }
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
            if (str.hashCode() == -829740640 && str.equals("commute-time") && !this.r.l()) {
                this.r.o(true);
                this.m.f("commute-time");
                return;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void a(int i2) {
        this.o = i2;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void a(String str) {
        ee7.b(str, "complicationPos");
        a06 a06 = this.e;
        if (a06 != null) {
            a06.j(str);
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<CustomizeRealData> arrayList) {
        ee7.b(arrayList, "customizeRealDataList");
        this.h = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void a(String str, String str2) {
        ee7.b(str, "complicationId");
        ee7.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "dropComplication - complicationId=" + str + ", toPosition=" + str2);
        a06 a06 = this.e;
        T t = null;
        if (a06 == null) {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        } else if (!a06.h(str)) {
            a06 a062 = this.e;
            if (a062 != null) {
                DianaPreset a2 = a062.a().a();
                if (a2 != null) {
                    DianaPreset clone = a2.clone();
                    Iterator<T> it = clone.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    if (t2 != null) {
                        t2.setId(str);
                        t2.setSettings("");
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("DianaCustomizeEditPresenter", "dropComplication - newPreset=" + clone);
                    a(clone);
                    return;
                }
                return;
            }
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(List<fz5> list, List<fz5> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!ee7.a((Object) list.get(i2).a(), (Object) list2.get(i2).a())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                fz5 fz5 = list.get(i3);
                fz5 fz52 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process watch app list, item at " + i3 + " position: " + fz5.c() + ", oldId: " + fz5.a() + ", newId: " + fz52.a());
                String c2 = fz52.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            b(ViewHierarchy.DIMENSION_TOP_KEY, fz5.a(), fz52.a());
                        }
                    } else if (c2.equals("middle")) {
                        b("middle", fz5.a(), fz52.a());
                    }
                }
                b("bottom", fz5.a(), fz52.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process watch app list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    @Override // com.fossil.qz5
    public void a(boolean z) {
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditPresenter", "setPresetToWatch currentPreset=" + a2);
            if (a2 != null) {
                ik7 unused = xh7.b(e(), null, null, new c(a2, null, this, z, a2), 3, null);
                return;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditPresenter", "updateCurrentPreset=" + dianaPreset);
        a06 a06 = this.e;
        if (a06 != null) {
            a06.a(dianaPreset);
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putInt("KEY_CUSTOMIZE_TAB", this.o);
        }
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            if (!(a2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", new Gson().a(a2));
            }
            a06 a062 = this.e;
            if (a062 != null) {
                DianaPreset d2 = a062.d();
                if (!(d2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", new Gson().a(d2));
                }
                a06 a063 = this.e;
                if (a063 != null) {
                    String a3 = a063.f().a();
                    if (!(a3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_COMPLICATION_POS_SELECTED", a3);
                    }
                    a06 a064 = this.e;
                    if (a064 != null) {
                        String a4 = a064.i().a();
                        if (!(a4 == null || bundle == null)) {
                            bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a4);
                        }
                        a06 a065 = this.e;
                        if (a065 != null) {
                            String a5 = a065.i().a();
                            if (!(a5 == null || bundle == null)) {
                                bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a5);
                            }
                            return bundle;
                        }
                        ee7.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
                ee7.d("mDianaCustomizeViewModel");
                throw null;
            }
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        gf5 a2 = qd5.f.a("set_watch_apps_manually");
        a2.a("button", str);
        a2.a("old_app", str2);
        a2.a("new_app", str3);
        a2.a();
    }

    @DexIgnore
    public final void a(List<fz5> list, List<fz5> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                z = true;
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!ee7.a((Object) list.get(i2).a(), (Object) list2.get(i2).a())) {
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                fz5 fz5 = list.get(i3);
                fz5 fz52 = list2.get(i3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DianaCustomizeEditPresenter", "Process complication list, item at " + i3 + " position: " + fz5.c() + ", oldId: " + fz5.a() + ", newId: " + fz52.a());
                String c2 = fz52.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != 115029) {
                            if (hashCode == 3317767 && c2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                a(ViewHierarchy.DIMENSION_LEFT_KEY, fz5.a(), fz52.a());
                            }
                        } else if (c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a(ViewHierarchy.DIMENSION_TOP_KEY, fz5.a(), fz52.a());
                        }
                    } else if (c2.equals("bottom")) {
                        a("bottom", fz5.a(), fz52.a());
                    }
                }
                a("right", fz5.a(), fz52.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Process complication list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        gf5 a2 = qd5.f.a("set_complication_manually");
        a2.a("view", str);
        a2.a("old_complication", str2);
        a2.a("new_complication", str3);
        a2.a();
    }
}
