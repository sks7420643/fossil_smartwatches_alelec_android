package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu5 implements Factory<gu5> {
    @DexIgnore
    public static gu5 a(bu5 bu5, rl4 rl4, vu5 vu5, yu5 yu5, LoaderManager loaderManager) {
        return new gu5(bu5, rl4, vu5, yu5, loaderManager);
    }
}
