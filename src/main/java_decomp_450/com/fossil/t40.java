package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.fossil.jy;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t40<R> implements n40, b50, s40 {
    @DexIgnore
    public static /* final */ boolean D; // = Log.isLoggable("Request", 2);
    @DexIgnore
    public int A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public RuntimeException C;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ y50 b;
    @DexIgnore
    public /* final */ Object c;
    @DexIgnore
    public /* final */ q40<R> d;
    @DexIgnore
    public /* final */ o40 e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ cw g;
    @DexIgnore
    public /* final */ Object h;
    @DexIgnore
    public /* final */ Class<R> i;
    @DexIgnore
    public /* final */ k40<?> j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ ew m;
    @DexIgnore
    public /* final */ c50<R> n;
    @DexIgnore
    public /* final */ List<q40<R>> o;
    @DexIgnore
    public /* final */ g50<? super R> p;
    @DexIgnore
    public /* final */ Executor q;
    @DexIgnore
    public uy<R> r;
    @DexIgnore
    public jy.d s;
    @DexIgnore
    public long t;
    @DexIgnore
    public volatile jy u;
    @DexIgnore
    public a v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum a {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    @DexIgnore
    public t40(Context context, cw cwVar, Object obj, Object obj2, Class<R> cls, k40<?> k40, int i2, int i3, ew ewVar, c50<R> c50, q40<R> q40, List<q40<R>> list, o40 o40, jy jyVar, g50<? super R> g50, Executor executor) {
        this.a = D ? String.valueOf(super.hashCode()) : null;
        this.b = y50.b();
        this.c = obj;
        this.f = context;
        this.g = cwVar;
        this.h = obj2;
        this.i = cls;
        this.j = k40;
        this.k = i2;
        this.l = i3;
        this.m = ewVar;
        this.n = c50;
        this.d = q40;
        this.o = list;
        this.e = o40;
        this.u = jyVar;
        this.p = g50;
        this.q = executor;
        this.v = a.PENDING;
        if (this.C == null && cwVar.g()) {
            this.C = new RuntimeException("Glide request origin trace");
        }
    }

    @DexIgnore
    public static <R> t40<R> a(Context context, cw cwVar, Object obj, Object obj2, Class<R> cls, k40<?> k40, int i2, int i3, ew ewVar, c50<R> c50, q40<R> q40, List<q40<R>> list, o40 o40, jy jyVar, g50<? super R> g50, Executor executor) {
        return new t40<>(context, cwVar, obj, obj2, cls, k40, i2, i3, ewVar, c50, q40, list, o40, jyVar, g50, executor);
    }

    @DexIgnore
    @Override // com.fossil.s40
    public Object b() {
        this.b.a();
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void c() {
        synchronized (this.c) {
            g();
            this.b.a();
            this.t = q50.a();
            if (this.h == null) {
                if (v50.b(this.k, this.l)) {
                    this.z = this.k;
                    this.A = this.l;
                }
                a(new py("Received null model"), m() == null ? 5 : 3);
            } else if (this.v == a.RUNNING) {
                throw new IllegalArgumentException("Cannot restart a running request");
            } else if (this.v == a.COMPLETE) {
                a((uy<?>) this.r, sw.MEMORY_CACHE);
            } else {
                this.v = a.WAITING_FOR_SIZE;
                if (v50.b(this.k, this.l)) {
                    a(this.k, this.l);
                } else {
                    this.n.b(this);
                }
                if ((this.v == a.RUNNING || this.v == a.WAITING_FOR_SIZE) && i()) {
                    this.n.b(n());
                }
                if (D) {
                    a("finished run method in " + q50.a(this.t));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        r4.u.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    @DexIgnore
    @Override // com.fossil.n40
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clear() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r4.g()     // Catch:{ all -> 0x003c }
            com.fossil.y50 r1 = r4.b     // Catch:{ all -> 0x003c }
            r1.a()     // Catch:{ all -> 0x003c }
            com.fossil.t40$a r1 = r4.v     // Catch:{ all -> 0x003c }
            com.fossil.t40$a r2 = com.fossil.t40.a.CLEARED     // Catch:{ all -> 0x003c }
            if (r1 != r2) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x003c }
            return
        L_0x0013:
            r4.k()     // Catch:{ all -> 0x003c }
            com.fossil.uy<R> r1 = r4.r     // Catch:{ all -> 0x003c }
            r2 = 0
            if (r1 == 0) goto L_0x0020
            com.fossil.uy<R> r1 = r4.r     // Catch:{ all -> 0x003c }
            r4.r = r2     // Catch:{ all -> 0x003c }
            r2 = r1
        L_0x0020:
            boolean r1 = r4.h()     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x002f
            com.fossil.c50<R> r1 = r4.n     // Catch:{ all -> 0x003c }
            android.graphics.drawable.Drawable r3 = r4.n()     // Catch:{ all -> 0x003c }
            r1.c(r3)     // Catch:{ all -> 0x003c }
        L_0x002f:
            com.fossil.t40$a r1 = com.fossil.t40.a.CLEARED     // Catch:{ all -> 0x003c }
            r4.v = r1     // Catch:{ all -> 0x003c }
            monitor-exit(r0)     // Catch:{ all -> 0x003c }
            if (r2 == 0) goto L_0x003b
            com.fossil.jy r0 = r4.u
            r0.b(r2)
        L_0x003b:
            return
        L_0x003c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t40.clear():void");
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void d() {
        synchronized (this.c) {
            if (isRunning()) {
                clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean e() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.CLEARED;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean f() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    @DexIgnore
    public final void g() {
        if (this.B) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    @DexIgnore
    public final boolean h() {
        o40 o40 = this.e;
        return o40 == null || o40.f(this);
    }

    @DexIgnore
    public final boolean i() {
        o40 o40 = this.e;
        return o40 == null || o40.c(this);
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean isRunning() {
        boolean z2;
        synchronized (this.c) {
            if (this.v != a.RUNNING) {
                if (this.v != a.WAITING_FOR_SIZE) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    @DexIgnore
    public final boolean j() {
        o40 o40 = this.e;
        return o40 == null || o40.d(this);
    }

    @DexIgnore
    public final void k() {
        g();
        this.b.a();
        this.n.a((b50) this);
        jy.d dVar = this.s;
        if (dVar != null) {
            dVar.a();
            this.s = null;
        }
    }

    @DexIgnore
    public final Drawable l() {
        if (this.w == null) {
            Drawable k2 = this.j.k();
            this.w = k2;
            if (k2 == null && this.j.j() > 0) {
                this.w = a(this.j.j());
            }
        }
        return this.w;
    }

    @DexIgnore
    public final Drawable m() {
        if (this.y == null) {
            Drawable l2 = this.j.l();
            this.y = l2;
            if (l2 == null && this.j.m() > 0) {
                this.y = a(this.j.m());
            }
        }
        return this.y;
    }

    @DexIgnore
    public final Drawable n() {
        if (this.x == null) {
            Drawable r2 = this.j.r();
            this.x = r2;
            if (r2 == null && this.j.s() > 0) {
                this.x = a(this.j.s());
            }
        }
        return this.x;
    }

    @DexIgnore
    public final boolean o() {
        o40 o40 = this.e;
        return o40 == null || !o40.b().a();
    }

    @DexIgnore
    public final void p() {
        o40 o40 = this.e;
        if (o40 != null) {
            o40.a(this);
        }
    }

    @DexIgnore
    public final void q() {
        o40 o40 = this.e;
        if (o40 != null) {
            o40.e(this);
        }
    }

    @DexIgnore
    public final void r() {
        if (i()) {
            Drawable drawable = null;
            if (this.h == null) {
                drawable = m();
            }
            if (drawable == null) {
                drawable = l();
            }
            if (drawable == null) {
                drawable = n();
            }
            this.n.a(drawable);
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean a() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean b(n40 n40) {
        int i2;
        int i3;
        Object obj;
        Class<R> cls;
        k40<?> k40;
        ew ewVar;
        int size;
        int i4;
        int i5;
        Object obj2;
        Class<R> cls2;
        k40<?> k402;
        ew ewVar2;
        int size2;
        if (!(n40 instanceof t40)) {
            return false;
        }
        synchronized (this.c) {
            i2 = this.k;
            i3 = this.l;
            obj = this.h;
            cls = this.i;
            k40 = this.j;
            ewVar = this.m;
            size = this.o != null ? this.o.size() : 0;
        }
        t40 t40 = (t40) n40;
        synchronized (t40.c) {
            i4 = t40.k;
            i5 = t40.l;
            obj2 = t40.h;
            cls2 = t40.i;
            k402 = t40.j;
            ewVar2 = t40.m;
            size2 = t40.o != null ? t40.o.size() : 0;
        }
        return i2 == i4 && i3 == i5 && v50.a(obj, obj2) && cls.equals(cls2) && k40.equals(k402) && ewVar == ewVar2 && size == size2;
    }

    @DexIgnore
    public final Drawable a(int i2) {
        return k20.a(this.g, i2, this.j.x() != null ? this.j.x() : this.f.getTheme());
    }

    @DexIgnore
    @Override // com.fossil.b50
    public void a(int i2, int i3) {
        Object obj;
        this.b.a();
        Object obj2 = this.c;
        synchronized (obj2) {
            try {
                if (D) {
                    a("Got onSizeReady in " + q50.a(this.t));
                }
                if (this.v == a.WAITING_FOR_SIZE) {
                    this.v = a.RUNNING;
                    float w2 = this.j.w();
                    this.z = a(i2, w2);
                    this.A = a(i3, w2);
                    if (D) {
                        a("finished setup for calling load in " + q50.a(this.t));
                    }
                    obj = obj2;
                    try {
                        this.s = this.u.a(this.g, this.h, this.j.v(), this.z, this.A, this.j.u(), this.i, this.m, this.j.i(), this.j.y(), this.j.F(), this.j.D(), this.j.o(), this.j.B(), this.j.A(), this.j.z(), this.j.n(), this, this.q);
                        if (this.v != a.RUNNING) {
                            this.s = null;
                        }
                        if (D) {
                            a("finished onSizeReady in " + q50.a(this.t));
                        }
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                obj = obj2;
                throw th;
            }
        }
    }

    @DexIgnore
    public static int a(int i2, float f2) {
        return i2 == Integer.MIN_VALUE ? i2 : Math.round(f2 * ((float) i2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.t40<R> */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.uy<?> */
    /* JADX DEBUG: Multi-variable search result rejected for r7v1, resolved type: com.fossil.jy */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [com.fossil.uy<R>, com.fossil.uy, com.fossil.jy$d] */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        r5.u.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return;
     */
    @DexIgnore
    @Override // com.fossil.s40
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.fossil.uy<?> r6, com.fossil.sw r7) {
        /*
            r5 = this;
            com.fossil.y50 r0 = r5.b
            r0.a()
            r0 = 0
            java.lang.Object r1 = r5.c     // Catch:{ all -> 0x00b9 }
            monitor-enter(r1)     // Catch:{ all -> 0x00b9 }
            r5.s = r0     // Catch:{ all -> 0x00b6 }
            if (r6 != 0) goto L_0x002f
            com.fossil.py r6 = new com.fossil.py     // Catch:{ all -> 0x00b6 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b6 }
            r7.<init>()     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "Expected to receive a Resource<R> with an object of "
            r7.append(r2)     // Catch:{ all -> 0x00b6 }
            java.lang.Class<R> r2 = r5.i     // Catch:{ all -> 0x00b6 }
            r7.append(r2)     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = " inside, but instead got null."
            r7.append(r2)     // Catch:{ all -> 0x00b6 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00b6 }
            r6.<init>(r7)     // Catch:{ all -> 0x00b6 }
            r5.a(r6)     // Catch:{ all -> 0x00b6 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b6 }
            return
        L_0x002f:
            java.lang.Object r2 = r6.get()     // Catch:{ all -> 0x00b6 }
            if (r2 == 0) goto L_0x005c
            java.lang.Class<R> r3 = r5.i     // Catch:{ all -> 0x00b6 }
            java.lang.Class r4 = r2.getClass()     // Catch:{ all -> 0x00b6 }
            boolean r3 = r3.isAssignableFrom(r4)     // Catch:{ all -> 0x00b6 }
            if (r3 != 0) goto L_0x0042
            goto L_0x005c
        L_0x0042:
            boolean r3 = r5.j()     // Catch:{ all -> 0x00b6 }
            if (r3 != 0) goto L_0x0057
            r5.r = r0     // Catch:{ all -> 0x00b2 }
            com.fossil.t40$a r7 = com.fossil.t40.a.COMPLETE     // Catch:{ all -> 0x00b2 }
            r5.v = r7     // Catch:{ all -> 0x00b2 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b2 }
            if (r6 == 0) goto L_0x0056
            com.fossil.jy r7 = r5.u
            r7.b(r6)
        L_0x0056:
            return
        L_0x0057:
            r5.a(r6, r2, r7)
            monitor-exit(r1)
            return
        L_0x005c:
            r5.r = r0
            com.fossil.py r7 = new com.fossil.py
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "Expected to receive an object of "
            r0.append(r3)
            java.lang.Class<R> r3 = r5.i
            r0.append(r3)
            java.lang.String r3 = " but instead got "
            r0.append(r3)
            if (r2 == 0) goto L_0x007b
            java.lang.Class r3 = r2.getClass()
            goto L_0x007d
        L_0x007b:
            java.lang.String r3 = ""
        L_0x007d:
            r0.append(r3)
            java.lang.String r3 = "{"
            r0.append(r3)
            r0.append(r2)
            java.lang.String r3 = "} inside Resource{"
            r0.append(r3)
            r0.append(r6)
            java.lang.String r3 = "}."
            r0.append(r3)
            if (r2 == 0) goto L_0x009a
            java.lang.String r2 = ""
            goto L_0x009c
        L_0x009a:
            java.lang.String r2 = " To indicate failure return a null Resource object, rather than a Resource object containing null data."
        L_0x009c:
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r7.<init>(r0)
            r5.a(r7)
            monitor-exit(r1)
            if (r6 == 0) goto L_0x00b1
            com.fossil.jy r7 = r5.u
            r7.b(r6)
        L_0x00b1:
            return
        L_0x00b2:
            r7 = move-exception
            r0 = r6
            r6 = r7
            goto L_0x00b7
        L_0x00b6:
            r6 = move-exception
        L_0x00b7:
            monitor-exit(r1)
            throw r6
        L_0x00b9:
            r6 = move-exception
            if (r0 == 0) goto L_0x00c1
            com.fossil.jy r7 = r5.u
            r7.b(r0)
        L_0x00c1:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t40.a(com.fossil.uy, com.fossil.sw):void");
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(uy<R> uyVar, R r2, sw swVar) {
        boolean z2;
        boolean o2 = o();
        this.v = a.COMPLETE;
        this.r = uyVar;
        if (this.g.e() <= 3) {
            Log.d("Glide", "Finished loading " + r2.getClass().getSimpleName() + " from " + swVar + " for " + this.h + " with size [" + this.z + "x" + this.A + "] in " + q50.a(this.t) + " ms");
        }
        boolean z3 = true;
        this.B = true;
        try {
            if (this.o != null) {
                z2 = false;
                for (q40<R> q40 : this.o) {
                    z2 |= q40.a(r2, this.h, this.n, swVar, o2);
                }
            } else {
                z2 = false;
            }
            if (this.d == null || !this.d.a(r2, this.h, this.n, swVar, o2)) {
                z3 = false;
            }
            if (!z3 && !z2) {
                this.n.a(r2, this.p.a(swVar, o2));
            }
            this.B = false;
            q();
        } catch (Throwable th) {
            this.B = false;
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.s40
    public void a(py pyVar) {
        a(pyVar, 5);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(py pyVar, int i2) {
        boolean z2;
        this.b.a();
        synchronized (this.c) {
            pyVar.setOrigin(this.C);
            int e2 = this.g.e();
            if (e2 <= i2) {
                Log.w("Glide", "Load failed for " + this.h + " with size [" + this.z + "x" + this.A + "]", pyVar);
                if (e2 <= 4) {
                    pyVar.logRootCauses("Glide");
                }
            }
            this.s = null;
            this.v = a.FAILED;
            boolean z3 = true;
            this.B = true;
            try {
                if (this.o != null) {
                    z2 = false;
                    for (q40<R> q40 : this.o) {
                        z2 |= q40.a(pyVar, this.h, this.n, o());
                    }
                } else {
                    z2 = false;
                }
                if (this.d == null || !this.d.a(pyVar, this.h, this.n, o())) {
                    z3 = false;
                }
                if (!z2 && !z3) {
                    r();
                }
                this.B = false;
                p();
            } catch (Throwable th) {
                this.B = false;
                throw th;
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        Log.v("Request", str + " this: " + this.a);
    }
}
