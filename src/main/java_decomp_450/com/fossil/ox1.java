package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ox1 implements rx1.b {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public ox1(String str) {
        this.a = str;
    }

    @DexIgnore
    public static rx1.b a(String str) {
        return new ox1(str);
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return rx1.a(this.a, (SQLiteDatabase) obj);
    }
}
