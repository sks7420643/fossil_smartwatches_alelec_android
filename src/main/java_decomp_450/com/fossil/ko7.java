package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko7 implements qn7 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ pp7 b;
    @DexIgnore
    public /* final */ wq7 c;
    @DexIgnore
    public co7 d;
    @DexIgnore
    public /* final */ lo7 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends wq7 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.wq7
        public void i() {
            ko7.this.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends qo7 {
        @DexIgnore
        public /* final */ rn7 b;

        @DexIgnore
        public b(rn7 rn7) {
            super("OkHttp %s", ko7.this.h());
            this.b = rn7;
        }

        @DexIgnore
        public void a(ExecutorService executorService) {
            try {
                executorService.execute(this);
            } catch (RejectedExecutionException e) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                interruptedIOException.initCause(e);
                ko7.this.d.a(ko7.this, interruptedIOException);
                this.b.onFailure(ko7.this, interruptedIOException);
                ko7.this.a.m().b(this);
            } catch (Throwable th) {
                ko7.this.a.m().b(this);
                throw th;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0030 A[Catch:{ IOException -> 0x004e, all -> 0x0026, all -> 0x008d }] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0059 A[Catch:{ IOException -> 0x004e, all -> 0x0026, all -> 0x008d }] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0079 A[Catch:{ IOException -> 0x004e, all -> 0x0026, all -> 0x008d }] */
        @Override // com.fossil.qo7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b() {
            /*
                r5 = this;
                com.fossil.ko7 r0 = com.fossil.ko7.this
                com.fossil.wq7 r0 = r0.c
                r0.g()
                r0 = 0
                com.fossil.ko7 r1 = com.fossil.ko7.this     // Catch:{ IOException -> 0x004e, all -> 0x0026 }
                okhttp3.Response r0 = r1.f()     // Catch:{ IOException -> 0x004e, all -> 0x0026 }
                r1 = 1
                com.fossil.rn7 r2 = r5.b     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
                com.fossil.ko7 r3 = com.fossil.ko7.this     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
                r2.onResponse(r3, r0)     // Catch:{ IOException -> 0x0024, all -> 0x0022 }
            L_0x0016:
                com.fossil.ko7 r0 = com.fossil.ko7.this
                okhttp3.OkHttpClient r0 = r0.a
                com.fossil.ao7 r0 = r0.m()
                r0.b(r5)
                goto L_0x008c
            L_0x0022:
                r0 = move-exception
                goto L_0x0029
            L_0x0024:
                r0 = move-exception
                goto L_0x0051
            L_0x0026:
                r1 = move-exception
                r0 = r1
                r1 = 0
            L_0x0029:
                com.fossil.ko7 r2 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                r2.cancel()     // Catch:{ all -> 0x008d }
                if (r1 != 0) goto L_0x004d
                java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x008d }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
                r2.<init>()     // Catch:{ all -> 0x008d }
                java.lang.String r3 = "canceled due to "
                r2.append(r3)     // Catch:{ all -> 0x008d }
                r2.append(r0)     // Catch:{ all -> 0x008d }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x008d }
                r1.<init>(r2)     // Catch:{ all -> 0x008d }
                com.fossil.rn7 r2 = r5.b     // Catch:{ all -> 0x008d }
                com.fossil.ko7 r3 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                r2.onFailure(r3, r1)     // Catch:{ all -> 0x008d }
            L_0x004d:
                throw r0     // Catch:{ all -> 0x008d }
            L_0x004e:
                r1 = move-exception
                r0 = r1
                r1 = 0
            L_0x0051:
                com.fossil.ko7 r2 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                java.io.IOException r0 = r2.a(r0)     // Catch:{ all -> 0x008d }
                if (r1 == 0) goto L_0x0079
                com.fossil.mq7 r1 = com.fossil.mq7.d()     // Catch:{ all -> 0x008d }
                r2 = 4
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x008d }
                r3.<init>()     // Catch:{ all -> 0x008d }
                java.lang.String r4 = "Callback failure for "
                r3.append(r4)     // Catch:{ all -> 0x008d }
                com.fossil.ko7 r4 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                java.lang.String r4 = r4.i()     // Catch:{ all -> 0x008d }
                r3.append(r4)     // Catch:{ all -> 0x008d }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x008d }
                r1.a(r2, r3, r0)     // Catch:{ all -> 0x008d }
                goto L_0x0016
            L_0x0079:
                com.fossil.ko7 r1 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                com.fossil.co7 r1 = r1.d     // Catch:{ all -> 0x008d }
                com.fossil.ko7 r2 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                r1.a(r2, r0)     // Catch:{ all -> 0x008d }
                com.fossil.rn7 r1 = r5.b     // Catch:{ all -> 0x008d }
                com.fossil.ko7 r2 = com.fossil.ko7.this     // Catch:{ all -> 0x008d }
                r1.onFailure(r2, r0)     // Catch:{ all -> 0x008d }
                goto L_0x0016
            L_0x008c:
                return
            L_0x008d:
                r0 = move-exception
                com.fossil.ko7 r1 = com.fossil.ko7.this
                okhttp3.OkHttpClient r1 = r1.a
                com.fossil.ao7 r1 = r1.m()
                r1.b(r5)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ko7.b.b():void");
        }

        @DexIgnore
        public ko7 c() {
            return ko7.this;
        }

        @DexIgnore
        public String d() {
            return ko7.this.e.g().g();
        }
    }

    @DexIgnore
    public ko7(OkHttpClient okHttpClient, lo7 lo7, boolean z) {
        this.a = okHttpClient;
        this.e = lo7;
        this.f = z;
        this.b = new pp7(okHttpClient, z);
        a aVar = new a();
        this.c = aVar;
        aVar.a((long) okHttpClient.f(), TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public lo7 c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void cancel() {
        this.b.a();
    }

    @DexIgnore
    public final void d() {
        this.b.a(mq7.d().a("response.body().close()"));
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public boolean e() {
        return this.b.b();
    }

    @DexIgnore
    public Response f() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.a.s());
        arrayList.add(this.b);
        arrayList.add(new gp7(this.a.l()));
        arrayList.add(new to7(this.a.t()));
        arrayList.add(new zo7(this.a));
        if (!this.f) {
            arrayList.addAll(this.a.u());
        }
        arrayList.add(new hp7(this.f));
        Response a2 = new mp7(arrayList, null, null, null, 0, this.e, this, this.d, this.a.i(), this.a.B(), this.a.F()).a(this.e);
        if (!this.b.b()) {
            return a2;
        }
        ro7.a(a2);
        throw new IOException("Canceled");
    }

    @DexIgnore
    public String h() {
        return this.e.g().m();
    }

    @DexIgnore
    public String i() {
        StringBuilder sb = new StringBuilder();
        sb.append(e() ? "canceled " : "");
        sb.append(this.f ? "web socket" : "call");
        sb.append(" to ");
        sb.append(h());
        return sb.toString();
    }

    @DexIgnore
    public static ko7 a(OkHttpClient okHttpClient, lo7 lo7, boolean z) {
        ko7 ko7 = new ko7(okHttpClient, lo7, z);
        ko7.d = okHttpClient.o().a(ko7);
        return ko7;
    }

    @DexIgnore
    @Override // java.lang.Object
    public ko7 clone() {
        return a(this.a, this.e, this.f);
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public Response a() throws IOException {
        synchronized (this) {
            if (!this.g) {
                this.g = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        d();
        this.c.g();
        this.d.b(this);
        try {
            this.a.m().a(this);
            Response f2 = f();
            if (f2 != null) {
                this.a.m().b(this);
                return f2;
            }
            throw new IOException("Canceled");
        } catch (IOException e2) {
            IOException a2 = a(e2);
            this.d.a(this, a2);
            throw a2;
        } catch (Throwable th) {
            this.a.m().b(this);
            throw th;
        }
    }

    @DexIgnore
    public IOException a(IOException iOException) {
        if (!this.c.h()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    @Override // com.fossil.qn7
    public void a(rn7 rn7) {
        synchronized (this) {
            if (!this.g) {
                this.g = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        d();
        this.d.b(this);
        this.a.m().a(new b(rn7));
    }
}
