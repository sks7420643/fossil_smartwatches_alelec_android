package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum g90 {
    C(0),
    F(1);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final g90 a(int i) {
            g90[] values = g90.values();
            for (g90 g90 : values) {
                if (g90.a() == i) {
                    return g90;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public g90(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
