package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l26 extends g26 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<Complication> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Complication> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ h26 l;
    @DexIgnore
    public /* final */ ComplicationRepository m;
    @DexIgnore
    public /* final */ ch5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $results;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ l26 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.l26$a$a")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1", f = "ComplicationSearchPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.l26$a$a  reason: collision with other inner class name */
        public static final class C0108a extends zb7 implements kd7<yi7, fb7<? super List<Complication>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0108a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0108a aVar = new C0108a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<Complication>> fb7) {
                return ((C0108a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return ea7.d((Collection) this.this$0.this$0.m.queryComplicationByName(this.this$0.$query));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(l26 l26, String str, se7 se7, fb7 fb7) {
            super(2, fb7);
            this.this$0 = l26;
            this.$query = str;
            this.$results = se7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$query, this.$results, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            se7 se7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.$query.length() > 0) {
                    se7 se72 = this.$results;
                    ti7 a2 = this.this$0.c();
                    C0108a aVar = new C0108a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = se72;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                    se7 = se72;
                }
                this.this$0.i().b(this.this$0.a((List) this.$results.element));
                this.this$0.i = this.$query;
                return i97.a;
            } else if (i == 1) {
                se7 = (se7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            se7.element = (T) ((List) obj);
            this.this$0.i().b(this.this$0.a((List) this.$results.element));
            this.this$0.i = this.$query;
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1", f = "ComplicationSearchPresenter.kt", l = {41}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ l26 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1$1", f = "ComplicationSearchPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.j.addAll(this.this$0.this$0.m.getAllComplicationRaw());
                    ArrayList d = this.this$0.this$0.k;
                    ComplicationRepository c = this.this$0.this$0.m;
                    List<String> h = this.this$0.this$0.n.h();
                    ee7.a((Object) h, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
                    return pb7.a(d.addAll(c.getComplicationByIds(h)));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(l26 l26, fb7 fb7) {
            super(2, fb7);
            this.this$0 = l26;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j();
            return i97.a;
        }
    }

    @DexIgnore
    public l26(h26 h26, ComplicationRepository complicationRepository, ch5 ch5) {
        ee7.b(h26, "mView");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        this.l = h26;
        this.m = complicationRepository;
        this.n = ch5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.g26
    public void h() {
        this.i = "";
        this.l.n();
        j();
    }

    @DexIgnore
    public final h26 i() {
        return this.l;
    }

    @DexIgnore
    public final void j() {
        if (this.k.isEmpty()) {
            this.l.b(a(ea7.d((Collection) this.j)));
        } else {
            this.l.c(a(ea7.d((Collection) this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            h26 h26 = this.l;
            String str = this.i;
            if (str != null) {
                h26.b(str);
                String str2 = this.i;
                if (str2 != null) {
                    a(str2);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void k() {
        this.l.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4) {
        ee7.b(str, "topComplication");
        ee7.b(str2, "bottomComplication");
        ee7.b(str3, "rightComplication");
        ee7.b(str4, "leftComplication");
        this.e = str;
        this.f = str2;
        this.h = str3;
        this.g = str4;
    }

    @DexIgnore
    @Override // com.fossil.g26
    public void a(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        se7 se7 = new se7();
        se7.element = (T) new ArrayList();
        ik7 unused = xh7.b(e(), null, null, new a(this, str, se7, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.g26
    public void a(Complication complication) {
        ee7.b(complication, "selectedComplication");
        List<String> h2 = this.n.h();
        ee7.a((Object) h2, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
        if (!h2.contains(complication.getComplicationId())) {
            h2.add(0, complication.getComplicationId());
            if (h2.size() > 5) {
                h2 = h2.subList(0, 5);
            }
            this.n.b(h2);
        }
        this.l.a(complication);
    }

    @DexIgnore
    public final List<r87<Complication, String>> a(List<Complication> list) {
        ArrayList arrayList = new ArrayList();
        for (Complication complication : list) {
            String complicationId = complication.getComplicationId();
            if (ee7.a((Object) complicationId, (Object) this.e)) {
                arrayList.add(new r87(complication, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (ee7.a((Object) complicationId, (Object) this.f)) {
                arrayList.add(new r87(complication, "bottom"));
            } else if (ee7.a((Object) complicationId, (Object) this.h)) {
                arrayList.add(new r87(complication, "right"));
            } else if (ee7.a((Object) complicationId, (Object) this.g)) {
                arrayList.add(new r87(complication, ViewHierarchy.DIMENSION_LEFT_KEY));
            } else {
                arrayList.add(new r87(complication, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("bottom", this.f);
            bundle.putString(ViewHierarchy.DIMENSION_LEFT_KEY, this.g);
            bundle.putString("right", this.h);
        }
        return bundle;
    }
}
