package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.i12;
import com.fossil.v02;
import com.fossil.v02.b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r12<R extends i12, A extends v02.b> extends BasePendingResult<R> implements s12<R> {
    @DexIgnore
    public /* final */ v02.c<A> q;
    @DexIgnore
    public /* final */ v02<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r12(v02<?> v02, a12 a12) {
        super(a12);
        a72.a(a12, "GoogleApiClient must not be null");
        a72.a(v02, "Api must not be null");
        this.q = (v02.c<A>) v02.a();
        this.r = v02;
    }

    @DexIgnore
    public final void a(RemoteException remoteException) {
        c(new Status(8, remoteException.getLocalizedMessage(), null));
    }

    @DexIgnore
    public abstract void a(A a) throws RemoteException;

    @DexIgnore
    public final void b(A a) throws DeadObjectException {
        if (a instanceof f72) {
            a = ((f72) a).I();
        }
        try {
            a((v02.b) a);
        } catch (DeadObjectException e) {
            a((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            a(e2);
        }
    }

    @DexIgnore
    public final void c(Status status) {
        a72.a(!status.y(), "Failed result must not be success");
        R a = a(status);
        a((i12) a);
        d(a);
    }

    @DexIgnore
    public void d(R r2) {
    }

    @DexIgnore
    public final v02<?> h() {
        return this.r;
    }

    @DexIgnore
    public final v02.c<A> i() {
        return this.q;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.r12<R extends com.fossil.i12, A extends com.fossil.v02$b> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.i12 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.s12
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.a((i12) obj);
    }
}
