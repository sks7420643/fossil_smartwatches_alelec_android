package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq {
    @DexIgnore
    public static /* final */ b e; // = new b(null);
    @DexIgnore
    public /* final */ List<r87<Class<? extends Object>, yr<? extends Object, ?>>> a;
    @DexIgnore
    public /* final */ List<r87<Class<? extends Object>, zr<? extends Object, ?>>> b;
    @DexIgnore
    public /* final */ List<r87<Class<? extends Object>, pr<? extends Object>>> c;
    @DexIgnore
    public /* final */ List<fr> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<r87<Class<? extends Object>, yr<? extends Object, ?>>> a;
        @DexIgnore
        public /* final */ List<r87<Class<? extends Object>, zr<? extends Object, ?>>> b;
        @DexIgnore
        public /* final */ List<r87<Class<? extends Object>, pr<? extends Object>>> c;
        @DexIgnore
        public /* final */ List<fr> d;

        @DexIgnore
        public a() {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = new ArrayList();
        }

        @DexIgnore
        public final <T> a a(Class<T> cls, yr<T, ?> yrVar) {
            ee7.b(cls, "type");
            ee7.b(yrVar, "mapper");
            this.a.add(w87.a(cls, yrVar));
            return this;
        }

        @DexIgnore
        public final <T> a a(Class<T> cls, pr<T> prVar) {
            ee7.b(cls, "type");
            ee7.b(prVar, "fetcher");
            this.c.add(w87.a(cls, prVar));
            return this;
        }

        @DexIgnore
        public final a a(fr frVar) {
            ee7.b(frVar, "decoder");
            this.d.add(frVar);
            return this;
        }

        @DexIgnore
        public final jq a() {
            return new jq(ea7.n(this.a), ea7.n(this.b), ea7.n(this.c), ea7.n(this.d), null);
        }

        @DexIgnore
        public a(jq jqVar) {
            ee7.b(jqVar, "registry");
            this.a = ea7.d((Collection) jqVar.c());
            this.b = ea7.d((Collection) jqVar.d());
            this.c = ea7.d((Collection) jqVar.b());
            this.d = ea7.d((Collection) jqVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.List<? extends com.fossil.r87<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.yr<? extends java.lang.Object, ?>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.r87<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.zr<? extends java.lang.Object, ?>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends com.fossil.r87<? extends java.lang.Class<? extends java.lang.Object>, ? extends com.fossil.pr<? extends java.lang.Object>>> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends com.fossil.fr> */
    /* JADX WARN: Multi-variable type inference failed */
    public jq(List<? extends r87<? extends Class<? extends Object>, ? extends yr<? extends Object, ?>>> list, List<? extends r87<? extends Class<? extends Object>, ? extends zr<? extends Object, ?>>> list2, List<? extends r87<? extends Class<? extends Object>, ? extends pr<? extends Object>>> list3, List<? extends fr> list4) {
        this.a = list;
        this.b = list2;
        this.c = list3;
        this.d = list4;
    }

    @DexIgnore
    public final List<fr> a() {
        return this.d;
    }

    @DexIgnore
    public final List<r87<Class<? extends Object>, pr<? extends Object>>> b() {
        return this.c;
    }

    @DexIgnore
    public final List<r87<Class<? extends Object>, yr<? extends Object, ?>>> c() {
        return this.a;
    }

    @DexIgnore
    public final List<r87<Class<? extends Object>, zr<? extends Object, ?>>> d() {
        return this.b;
    }

    @DexIgnore
    public final a e() {
        return new a(this);
    }

    @DexIgnore
    public /* synthetic */ jq(List list, List list2, List list3, List list4, zd7 zd7) {
        this(list, list2, list3, list4);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042 A[LOOP:0: B:1:0x000d->B:12:0x0042, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0046 A[EDGE_INSN: B:26:0x0046->B:14:0x0046 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> com.fossil.pr<T> a(T r9) {
        /*
            r8 = this;
            java.lang.String r0 = "data"
            com.fossil.ee7.b(r9, r0)
            java.util.List<com.fossil.r87<java.lang.Class<? extends java.lang.Object>, com.fossil.pr<? extends java.lang.Object>>> r0 = r8.c
            int r1 = r0.size()
            r2 = 0
            r3 = 0
        L_0x000d:
            if (r3 >= r1) goto L_0x0045
            java.lang.Object r4 = r0.get(r3)
            r5 = r4
            com.fossil.r87 r5 = (com.fossil.r87) r5
            java.lang.Object r6 = r5.component1()
            java.lang.Class r6 = (java.lang.Class) r6
            java.lang.Object r5 = r5.component2()
            com.fossil.pr r5 = (com.fossil.pr) r5
            java.lang.Class r7 = r9.getClass()
            boolean r6 = r6.isAssignableFrom(r7)
            if (r6 == 0) goto L_0x003e
            if (r5 == 0) goto L_0x0036
            boolean r5 = r5.a(r9)
            if (r5 == 0) goto L_0x003e
            r5 = 1
            goto L_0x003f
        L_0x0036:
            com.fossil.x87 r9 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type coil.fetch.Fetcher<kotlin.Any>"
            r9.<init>(r0)
            throw r9
        L_0x003e:
            r5 = 0
        L_0x003f:
            if (r5 == 0) goto L_0x0042
            goto L_0x0046
        L_0x0042:
            int r3 = r3 + 1
            goto L_0x000d
        L_0x0045:
            r4 = 0
        L_0x0046:
            com.fossil.r87 r4 = (com.fossil.r87) r4
            if (r4 == 0) goto L_0x005b
            java.lang.Object r9 = r4.getSecond()
            if (r9 == 0) goto L_0x0053
            com.fossil.pr r9 = (com.fossil.pr) r9
            return r9
        L_0x0053:
            com.fossil.x87 r9 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type coil.fetch.Fetcher<T>"
            r9.<init>(r0)
            throw r9
        L_0x005b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unable to fetch data. No fetcher supports: "
            r0.append(r1)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r9 = r9.toString()
            r0.<init>(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jq.a(java.lang.Object):com.fossil.pr");
    }

    @DexIgnore
    public final <T> fr a(T t, ar7 ar7, String str) {
        fr frVar;
        ee7.b(t, "data");
        ee7.b(ar7, "source");
        List<fr> list = this.d;
        int size = list.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                frVar = null;
                break;
            }
            frVar = list.get(i);
            if (frVar.a(ar7, str)) {
                break;
            }
            i++;
        }
        fr frVar2 = frVar;
        if (frVar2 != null) {
            return frVar2;
        }
        throw new IllegalStateException(("Unable to decode data. No decoder supports: " + ((Object) t)).toString());
    }
}
