package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u54 extends v54.d.f {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    @Override // com.fossil.v54.d.f
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof v54.d.f) {
            return this.a.equals(((v54.d.f) obj).a());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "User{identifier=" + this.a + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.f.a {
        @DexIgnore
        public String a;

        @DexIgnore
        @Override // com.fossil.v54.d.f.a
        public v54.d.f.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.f.a
        public v54.d.f a() {
            String str = "";
            if (this.a == null) {
                str = str + " identifier";
            }
            if (str.isEmpty()) {
                return new u54(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public u54(String str) {
        this.a = str;
    }
}
