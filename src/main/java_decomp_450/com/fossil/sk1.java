package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk1 extends wi1<t60[], t60[]> {
    @DexIgnore
    public static /* final */ y71<t60[]>[] b; // = {new t91(), new nb1(), new hd1(new r60((byte) 3, (byte) 0))};
    @DexIgnore
    public static /* final */ uk1<t60[]>[] c; // = {new bf1(pb1.ALARM), new xg1(pb1.ALARM), new ui1(pb1.ALARM, new r60((byte) 3, (byte) 0))};
    @DexIgnore
    public static /* final */ sk1 d; // = new sk1();

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<t60[]>[] b() {
        return c;
    }

    @DexIgnore
    public final t60[] c(byte[] bArr) throws f41 {
        try {
            return t60.CREATOR.a(wi1.a.a(bArr));
        } catch (IllegalArgumentException e) {
            throw new f41(l21.INVALID_FILE_DATA, e.getLocalizedMessage(), e);
        }
    }

    @DexIgnore
    public final t60[] b(byte[] bArr) throws f41 {
        Object obj;
        byte[] a = wi1.a.a(bArr);
        if (a.length % 3 == 0) {
            ArrayList arrayList = new ArrayList();
            jf7 a2 = qf7.a(qf7.d(0, a.length), 3);
            int first = a2.getFirst();
            int last = a2.getLast();
            int a3 = a2.a();
            if (a3 < 0 ? first >= last : first <= last) {
                while (true) {
                    w60 a4 = w60.CREATOR.a(s97.a(a, first, first + 3));
                    if (a4 instanceof z60) {
                        obj = new y60((z60) a4, null, null, 6, null);
                    } else {
                        obj = a4 instanceof d70 ? new c70((d70) a4, null, null, 6, null) : null;
                    }
                    if (obj != null) {
                        arrayList.add(obj);
                    }
                    if (first == last) {
                        break;
                    }
                    first += a3;
                }
            }
            Object[] array = arrayList.toArray(new t60[0]);
            if (array != null) {
                return (t60[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new f41(l21.INVALID_FILE_DATA, yh0.a(yh0.b("Size("), a.length, ") not divide to 3."), null, 4);
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<t60[]>[] a() {
        return b;
    }

    @DexIgnore
    public final byte[] a(t60[] t60Arr) {
        v60 v60;
        ByteBuffer allocate = ByteBuffer.allocate(t60Arr.length * 3);
        for (t60 t60 : t60Arr) {
            v60[] c2 = t60.c();
            int length = c2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    v60 = null;
                    break;
                }
                v60 = c2[i];
                if (v60 instanceof w60) {
                    break;
                }
                i++;
            }
            if (v60 != null) {
                allocate.put(v60.c());
            }
        }
        byte[] array = allocate.array();
        ee7.a((Object) array, "entryDataBuffer.array()");
        return array;
    }

    @DexIgnore
    public final byte[] b(t60[] t60Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (t60 t60 : t60Arr) {
            byteArrayOutputStream.write(t60.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }
}
