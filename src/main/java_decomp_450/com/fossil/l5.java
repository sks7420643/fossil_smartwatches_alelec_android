package com.fossil;

import com.fossil.h5;
import com.fossil.i5;
import java.util.ArrayList;

public class l5 extends i5 {
    public float k0 = -1.0f;
    public int l0 = -1;
    public int m0 = -1;
    public h5 n0 = ((i5) this).t;
    public int o0;
    public boolean p0;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|(3:17|18|20)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.fossil.h5$d[] r0 = com.fossil.h5.d.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.l5.a.a = r0
                com.fossil.h5$d r1 = com.fossil.h5.d.LEFT     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.h5$d r1 = com.fossil.h5.d.RIGHT     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.h5$d r1 = com.fossil.h5.d.TOP     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.h5$d r1 = com.fossil.h5.d.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.h5$d r1 = com.fossil.h5.d.BASELINE     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_X     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_Y     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.fossil.l5.a.a     // Catch:{ NoSuchFieldError -> 0x006c }
                com.fossil.h5$d r1 = com.fossil.h5.d.NONE     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.l5.a.<clinit>():void");
        }
        */
    }

    public l5() {
        this.o0 = 0;
        this.p0 = false;
        new o5();
        ((i5) this).B.clear();
        ((i5) this).B.add(this.n0);
        int length = ((i5) this).A.length;
        for (int i = 0; i < length; i++) {
            ((i5) this).A[i] = this.n0;
        }
    }

    public int K() {
        return this.o0;
    }

    @Override // com.fossil.i5
    public h5 a(h5.d dVar) {
        switch (a.a[dVar.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(dVar.name());
    }

    @Override // com.fossil.i5
    public boolean b() {
        return true;
    }

    @Override // com.fossil.i5
    public ArrayList<h5> c() {
        return ((i5) this).B;
    }

    public void e(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    public void u(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    public void v(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    public void w(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            ((i5) this).B.clear();
            if (this.o0 == 1) {
                this.n0 = ((i5) this).s;
            } else {
                this.n0 = ((i5) this).t;
            }
            ((i5) this).B.add(this.n0);
            int length = ((i5) this).A.length;
            for (int i2 = 0; i2 < length; i2++) {
                ((i5) this).A[i2] = this.n0;
            }
        }
    }

    @Override // com.fossil.i5
    public void c(y4 y4Var) {
        if (l() != null) {
            int b = y4Var.b(this.n0);
            if (this.o0 == 1) {
                s(b);
                t(0);
                h(l().j());
                p(0);
                return;
            }
            s(0);
            t(b);
            p(l().t());
            h(0);
        }
    }

    @Override // com.fossil.i5
    public void a(int i) {
        i5 l = l();
        if (l != null) {
            if (K() == 1) {
                ((i5) this).t.d().a(1, l.t.d(), 0);
                ((i5) this).v.d().a(1, l.t.d(), 0);
                if (this.l0 != -1) {
                    ((i5) this).s.d().a(1, l.s.d(), this.l0);
                    ((i5) this).u.d().a(1, l.s.d(), this.l0);
                } else if (this.m0 != -1) {
                    ((i5) this).s.d().a(1, l.u.d(), -this.m0);
                    ((i5) this).u.d().a(1, l.u.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.k() == i5.b.FIXED) {
                    int i2 = (int) (((float) l.E) * this.k0);
                    ((i5) this).s.d().a(1, l.s.d(), i2);
                    ((i5) this).u.d().a(1, l.s.d(), i2);
                }
            } else {
                ((i5) this).s.d().a(1, l.s.d(), 0);
                ((i5) this).u.d().a(1, l.s.d(), 0);
                if (this.l0 != -1) {
                    ((i5) this).t.d().a(1, l.t.d(), this.l0);
                    ((i5) this).v.d().a(1, l.t.d(), this.l0);
                } else if (this.m0 != -1) {
                    ((i5) this).t.d().a(1, l.v.d(), -this.m0);
                    ((i5) this).v.d().a(1, l.v.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.r() == i5.b.FIXED) {
                    int i3 = (int) (((float) l.F) * this.k0);
                    ((i5) this).t.d().a(1, l.t.d(), i3);
                    ((i5) this).v.d().a(1, l.t.d(), i3);
                }
            }
        }
    }

    @Override // com.fossil.i5
    public void a(y4 y4Var) {
        j5 j5Var = (j5) l();
        if (j5Var != null) {
            h5 a2 = j5Var.a(h5.d.LEFT);
            h5 a3 = j5Var.a(h5.d.RIGHT);
            i5 i5Var = ((i5) this).D;
            boolean z = true;
            boolean z2 = i5Var != null && i5Var.C[0] == i5.b.WRAP_CONTENT;
            if (this.o0 == 0) {
                a2 = j5Var.a(h5.d.TOP);
                a3 = j5Var.a(h5.d.BOTTOM);
                i5 i5Var2 = ((i5) this).D;
                if (i5Var2 == null || i5Var2.C[1] != i5.b.WRAP_CONTENT) {
                    z = false;
                }
                z2 = z;
            }
            if (this.l0 != -1) {
                c5 a4 = y4Var.a(this.n0);
                y4Var.a(a4, y4Var.a(a2), this.l0, 6);
                if (z2) {
                    y4Var.b(y4Var.a(a3), a4, 0, 5);
                }
            } else if (this.m0 != -1) {
                c5 a5 = y4Var.a(this.n0);
                c5 a6 = y4Var.a(a3);
                y4Var.a(a5, a6, -this.m0, 6);
                if (z2) {
                    y4Var.b(a5, y4Var.a(a2), 0, 5);
                    y4Var.b(a6, a5, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                y4Var.a(y4.a(y4Var, y4Var.a(this.n0), y4Var.a(a2), y4Var.a(a3), this.k0, this.p0));
            }
        }
    }
}
