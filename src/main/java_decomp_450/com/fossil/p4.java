package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p4 {
    @DexIgnore
    public static final <T> o4<T> a(T... tArr) {
        ee7.b(tArr, "values");
        o4<T> o4Var = new o4<>(tArr.length);
        for (T t : tArr) {
            o4Var.add(t);
        }
        return o4Var;
    }
}
