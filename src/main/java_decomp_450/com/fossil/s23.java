package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s23 implements tr2<v23> {
    @DexIgnore
    public static s23 b; // = new s23();
    @DexIgnore
    public /* final */ tr2<v23> a;

    @DexIgnore
    public s23(tr2<v23> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((v23) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((v23) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((v23) b.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ v23 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public s23() {
        this(sr2.a(new u23()));
    }
}
