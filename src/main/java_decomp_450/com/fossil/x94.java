package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class x94 implements fo3 {
    @DexIgnore
    public /* final */ FirebaseInstanceId a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public x94(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    @Override // com.fossil.fo3
    public final Object then(no3 no3) {
        return this.a.a(this.b, this.c, no3);
    }
}
