package com.fossil;

import android.os.Process;
import com.fossil.oy;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Map<yw, d> b;
    @DexIgnore
    public /* final */ ReferenceQueue<oy<?>> c;
    @DexIgnore
    public oy.a d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile c f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zx$a$a")
        /* renamed from: com.fossil.zx$a$a  reason: collision with other inner class name */
        public class RunnableC0263a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable a;

            @DexIgnore
            public RunnableC0263a(a aVar, Runnable runnable) {
                this.a = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.a.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new RunnableC0263a(this, runnable), "glide-active-resources");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            zx.this.a();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WeakReference<oy<?>> {
        @DexIgnore
        public /* final */ yw a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public uy<?> c;

        @DexIgnore
        public d(yw ywVar, oy<?> oyVar, ReferenceQueue<? super oy<?>> referenceQueue, boolean z) {
            super(oyVar, referenceQueue);
            uy<?> uyVar;
            u50.a(ywVar);
            this.a = ywVar;
            if (!oyVar.f() || !z) {
                uyVar = null;
            } else {
                uy<?> e = oyVar.e();
                u50.a(e);
                uyVar = e;
            }
            this.c = uyVar;
            this.b = oyVar.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public zx(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new a()));
    }

    @DexIgnore
    public void a(oy.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.d = aVar;
            }
        }
    }

    @DexIgnore
    public synchronized oy<?> b(yw ywVar) {
        d dVar = this.b.get(ywVar);
        if (dVar == null) {
            return null;
        }
        oy<?> oyVar = (oy) dVar.get();
        if (oyVar == null) {
            a(dVar);
        }
        return oyVar;
    }

    @DexIgnore
    public zx(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.a = z;
        executor.execute(new b());
    }

    @DexIgnore
    public synchronized void a(yw ywVar, oy<?> oyVar) {
        d put = this.b.put(ywVar, new d(ywVar, oyVar, this.c, this.a));
        if (put != null) {
            put.a();
        }
    }

    @DexIgnore
    public synchronized void a(yw ywVar) {
        d remove = this.b.remove(ywVar);
        if (remove != null) {
            remove.a();
        }
    }

    @DexIgnore
    public void a(d dVar) {
        synchronized (this) {
            this.b.remove(dVar.a);
            if (dVar.b) {
                if (dVar.c != null) {
                    this.d.a(dVar.a, new oy<>(dVar.c, true, false, dVar.a, this.d));
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        while (!this.e) {
            try {
                a((d) this.c.remove());
                c cVar = this.f;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
