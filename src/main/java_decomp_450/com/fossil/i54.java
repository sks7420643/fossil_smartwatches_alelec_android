package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i54 extends v54.d.c {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.c.a {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a a(int i2) {
            this.a = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a b(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null model");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a c(int i2) {
            this.g = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a a(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a c(String str) {
            if (str != null) {
                this.i = str;
                return this;
            }
            throw new NullPointerException("Null modelClass");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a a(boolean z) {
            this.f = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a b(int i2) {
            this.c = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a a(String str) {
            if (str != null) {
                this.h = str;
                return this;
            }
            throw new NullPointerException("Null manufacturer");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c.a b(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.c.a
        public v54.d.c a() {
            String str = "";
            if (this.a == null) {
                str = str + " arch";
            }
            if (this.b == null) {
                str = str + " model";
            }
            if (this.c == null) {
                str = str + " cores";
            }
            if (this.d == null) {
                str = str + " ram";
            }
            if (this.e == null) {
                str = str + " diskSpace";
            }
            if (this.f == null) {
                str = str + " simulator";
            }
            if (this.g == null) {
                str = str + " state";
            }
            if (this.h == null) {
                str = str + " manufacturer";
            }
            if (this.i == null) {
                str = str + " modelClass";
            }
            if (str.isEmpty()) {
                return new i54(this.a.intValue(), this.b, this.c.intValue(), this.d.longValue(), this.e.longValue(), this.f.booleanValue(), this.g.intValue(), this.h, this.i);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public int a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public int b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public long c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public String d() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.c)) {
            return false;
        }
        v54.d.c cVar = (v54.d.c) obj;
        if (this.a == cVar.a() && this.b.equals(cVar.e()) && this.c == cVar.b() && this.d == cVar.g() && this.e == cVar.c() && this.f == cVar.i() && this.g == cVar.h() && this.h.equals(cVar.d()) && this.i.equals(cVar.f())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public String f() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public long g() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public int h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.d;
        long j2 = this.e;
        return ((((((((((((((((this.a ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ (this.f ? 1231 : 1237)) * 1000003) ^ this.g) * 1000003) ^ this.h.hashCode()) * 1000003) ^ this.i.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.v54.d.c
    public boolean i() {
        return this.f;
    }

    @DexIgnore
    public String toString() {
        return "Device{arch=" + this.a + ", model=" + this.b + ", cores=" + this.c + ", ram=" + this.d + ", diskSpace=" + this.e + ", simulator=" + this.f + ", state=" + this.g + ", manufacturer=" + this.h + ", modelClass=" + this.i + "}";
    }

    @DexIgnore
    public i54(int i2, String str, int i3, long j, long j2, boolean z, int i4, String str2, String str3) {
        this.a = i2;
        this.b = str;
        this.c = i3;
        this.d = j;
        this.e = j2;
        this.f = z;
        this.g = i4;
        this.h = str2;
        this.i = str3;
    }
}
