package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.cy6;
import com.fossil.lz5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r56 extends ho5 implements q56, CustomizeWidget.c, cy6.g, View.OnClickListener {
    @DexIgnore
    public p56 g;
    @DexIgnore
    public qw6<c35> h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public l56 j;
    @DexIgnore
    public c66 p;
    @DexIgnore
    public rj4 q;
    @DexIgnore
    public j56 r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ r56 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(r56 r56) {
            this.a = r56;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                ee7.a((Object) activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.B;
                ee7.a((Object) stringExtra, "presetId");
                ee7.a((Object) stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            c35 a2 = this.a.f1().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.D;
                ee7.a((Object) flexibleTextView, "binding.tvPresetName");
                flexibleTextView.setEllipsize(TextUtils.TruncateAt.END);
                a2.D.setSingleLine(true);
                FlexibleTextView flexibleTextView2 = a2.D;
                ee7.a((Object) flexibleTextView2, "binding.tvPresetName");
                flexibleTextView2.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            c35 a2 = this.a.f1().a();
            if (a2 != null) {
                zk5 zk5 = zk5.a;
                c35 a3 = this.a.f1().a();
                if (a3 != null) {
                    CardView cardView = a3.t;
                    ee7.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    zk5.a(cardView);
                    ViewPropertyAnimator animate = a2.G.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = a2.F.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = a2.E.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ r56 a;

        @DexIgnore
        public c(r56 r56) {
            this.a = r56;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.d(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.f(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.e(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.Y(ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ r56 a;

        @DexIgnore
        public d(r56 r56) {
            this.a = r56;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a("middle", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.d("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.f(str, "middle");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.e("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.Y("middle");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements lz5.b {
        @DexIgnore
        public /* final */ /* synthetic */ r56 a;

        @DexIgnore
        public e(r56 r56) {
            this.a = r56;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean a(View view, String str) {
            ee7.b(view, "view");
            ee7.b(str, "id");
            return this.a.a("bottom", view, str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void b(String str) {
            ee7.b(str, "label");
            this.a.d("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public boolean c(String str) {
            ee7.b(str, "fromPos");
            this.a.f(str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a(String str) {
            ee7.b(str, "label");
            this.a.e("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.lz5.b
        public void a() {
            this.a.Y("bottom");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.q56
    public void C(String str) {
        ee7.b(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        Z(str);
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void R() {
        ImageView imageView;
        if (isActive()) {
            qw6<c35> qw6 = this.h;
            if (qw6 != null) {
                c35 a2 = qw6.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    imageView.setImageResource(2131230972);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Y(String str) {
        ee7.b(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.e();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                l56 l56 = this.j;
                if (l56 != null) {
                    l56.f1();
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Z(String str) {
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.setSelectedWc(true);
                            a2.J.setSelectedWc(false);
                            a2.I.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.K.setSelectedWc(false);
                        a2.J.setSelectedWc(true);
                        a2.I.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.K.setSelectedWc(false);
                    a2.J.setSelectedWc(false);
                    a2.I.setSelectedWc(true);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void b(String str, String str2, String str3) {
        ee7.b(str, "message");
        ee7.b(str2, "microAppId");
        ee7.b(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            cy6.f fVar = new cy6.f(2131558480);
            fVar.a(2131363255, str);
            fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
            fVar.a(2131363307);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        ee7.b(str, "position");
        ee7.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.d();
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public final void e(String str, String str2) {
        ee7.b(str, "position");
        ee7.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.e();
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        p56 p56 = this.g;
        if (p56 != null) {
            p56.h();
            return false;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void f(String str, String str2) {
        ee7.b(str, "fromPosition");
        ee7.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.J.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.I.e();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                p56 p56 = this.g;
                if (p56 != null) {
                    p56.b(str, str2);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final qw6<c35> f1() {
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void g(boolean z) {
        FlexibleTextView flexibleTextView;
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (!(a2 == null || (flexibleTextView = a2.D) == null)) {
                flexibleTextView.setSingleLine(false);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        l56 l56 = (l56) getChildFragmentManager().b("MicroAppFragment");
        this.j = l56;
        if (l56 == null) {
            this.j = new l56();
        }
        l56 l562 = this.j;
        if (l562 != null) {
            this.i.add(l562);
        }
        tj4 f = PortfolioApp.g0.c().f();
        l56 l563 = this.j;
        if (l563 != null) {
            f.a(new h56(l563)).a(this);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    public final void h1() {
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.K;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                ee7.a((Object) putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new lz5(new c(this)), null, 8, null);
                CustomizeWidget customizeWidget2 = a2.J;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                ee7.a((Object) putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new lz5(new d(this)), null, 8, null);
                CustomizeWidget customizeWidget3 = a2.I;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                ee7.a((Object) putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new lz5(new e(this)), null, 8, null);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void k() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886761);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        W(a2);
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void m() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void o() {
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886530));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886528));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886529));
        fVar.a(2131363229);
        fVar.a(2131363307);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            rj4 rj4 = this.q;
            if (rj4 != null) {
                he a2 = je.a(hybridCustomizeEditActivity, rj4).a(j56.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                j56 j56 = (j56) a2;
                this.r = j56;
                p56 p56 = this.g;
                if (p56 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (j56 != null) {
                    p56.a(j56);
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && xg5.b.a(getContext(), xg5.c.BLUETOOTH_CONNECTION)) {
            p56 p56 = this.g;
            if (p56 != null) {
                p56.a(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362499) {
                p56 p56 = this.g;
                if (p56 != null) {
                    p56.a(true);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (id != 2131363229) {
                switch (id) {
                    case 2131363452:
                        p56 p562 = this.g;
                        if (p562 != null) {
                            p562.a("bottom");
                            return;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    case 2131363453:
                        p56 p563 = this.g;
                        if (p563 != null) {
                            p563.a("middle");
                            return;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    case 2131363454:
                        p56 p564 = this.g;
                        if (p564 != null) {
                            p564.a(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                p56 p565 = this.g;
                if (p565 != null) {
                    p565.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        c35 c35 = (c35) qb.a(layoutInflater, 2131558579, viewGroup, false, a1());
        g1();
        this.h = new qw6<>(this, c35);
        ee7.a((Object) c35, "binding");
        return c35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        p56 p56 = this.g;
        if (p56 != null) {
            p56.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        p56 p56 = this.g;
        if (p56 != null) {
            p56.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        V("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            ee7.a((Object) window, "activity.window");
            window.setEnterTransition(zk5.a.a());
            Window window2 = activity.getWindow();
            ee7.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(zk5.a.a(PortfolioApp.g0.c()));
            Intent intent = activity.getIntent();
            ee7.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new yk5(intent, PortfolioApp.g0.c()));
            a(activity);
        }
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.H.setBackgroundColor(Color.parseColor(b2));
                    a2.t.setBackgroundColor(Color.parseColor(b2));
                }
                a2.K.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.C.setOnClickListener(this);
                a2.u.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.B;
                ee7.a((Object) viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new qz6(getChildFragmentManager(), this.i));
                ViewPager2 viewPager22 = a2.B;
                ee7.a((Object) viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                if (a2.B.getChildAt(0) != null) {
                    View childAt = a2.B.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(2);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
            }
            h1();
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void p() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            ee7.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.g0.c().c(), false, false, 12, null);
        }
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        ee7.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public final boolean a(String str, View view, String str2) {
        ee7.b(str, "position");
        ee7.b(view, "view");
        ee7.b(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.K.e();
                    }
                } else if (str.equals("middle")) {
                    a2.J.e();
                }
            } else if (str.equals("bottom")) {
                a2.I.e();
            }
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            a2.I.setDragMode(false);
            l56 l56 = this.j;
            if (l56 != null) {
                l56.f1();
            }
            p56 p56 = this.g;
            if (p56 != null) {
                p56.a(str2, str);
                return true;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.q56
    public void f(boolean z) {
        qw6<c35> qw6 = this.h;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.u;
                ee7.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.u;
                ee7.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.u.setBackgroundResource(2131231274);
                return;
            }
            ImageButton imageButton3 = a2.u;
            ee7.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.u;
            ee7.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.u.setBackgroundResource(2131231275);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: com.fossil.fz5 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.fz5 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: com.fossil.fz5 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.q56
    public void a(ez5 ez5) {
        Object obj;
        Object obj2;
        ee7.b(ez5, "data");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + ez5.a());
        qw6<c35> qw6 = this.h;
        Object obj3 = null;
        if (qw6 != null) {
            c35 a2 = qw6.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(ez5.a());
                FlexibleTextView flexibleTextView = a2.D;
                ee7.a((Object) flexibleTextView, "it.tvPresetName");
                String d2 = ez5.d();
                if (d2 != null) {
                    String upperCase = d2.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        if (ee7.a((Object) ((fz5) obj).c(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                            break;
                        }
                    }
                    fz5 fz5 = (fz5) obj;
                    Iterator it2 = arrayList.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        obj2 = it2.next();
                        if (ee7.a((Object) ((fz5) obj2).c(), (Object) "middle")) {
                            break;
                        }
                    }
                    fz5 fz52 = (fz5) obj2;
                    Iterator it3 = arrayList.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        Object next = it3.next();
                        if (ee7.a((Object) ((fz5) next).c(), (Object) "bottom")) {
                            obj3 = next;
                            break;
                        }
                    }
                    CustomizeWidget customizeWidget = a2.K;
                    ee7.a((Object) customizeWidget, "it.waTop");
                    FlexibleTextView flexibleTextView2 = a2.G;
                    ee7.a((Object) flexibleTextView2, "it.tvWaTop");
                    a(customizeWidget, flexibleTextView2, fz5);
                    CustomizeWidget customizeWidget2 = a2.J;
                    ee7.a((Object) customizeWidget2, "it.waMiddle");
                    FlexibleTextView flexibleTextView3 = a2.F;
                    ee7.a((Object) flexibleTextView3, "it.tvWaMiddle");
                    a(customizeWidget2, flexibleTextView3, fz52);
                    CustomizeWidget customizeWidget3 = a2.I;
                    ee7.a((Object) customizeWidget3, "it.waBottom");
                    FlexibleTextView flexibleTextView4 = a2.E;
                    ee7.a((Object) flexibleTextView4, "it.tvWaBottom");
                    a(customizeWidget3, flexibleTextView4, (fz5) obj3);
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, FlexibleTextView flexibleTextView, fz5 fz5) {
        if (fz5 != null) {
            flexibleTextView.setText(fz5.b());
            customizeWidget.c(fz5.a());
            customizeWidget.h();
            a(customizeWidget, fz5.a());
            return;
        }
        flexibleTextView.setText("");
        customizeWidget.c("empty");
        customizeWidget.h();
        a(customizeWidget, "empty");
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(p56 p56) {
        ee7.b(p56, "presenter");
        this.g = p56;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i2 == 2131363229) {
                    g(false);
                    return;
                } else if (i2 == 2131363307) {
                    p56 p56 = this.g;
                    if (p56 != null) {
                        p56.a(true);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i2 == 2131363307 && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (ee7.a((Object) stringExtra2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                xg5 xg5 = xg5.b;
                Context context = getContext();
                if (context != null) {
                    xg5.a(xg5, context, ve5.a.b(stringExtra2), false, false, false, (Integer) null, 60, (Object) null);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                p56 p562 = this.g;
                if (p562 != null) {
                    ee7.a((Object) stringExtra, "toPos");
                    p562.a(stringExtra);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
        }
    }
}
