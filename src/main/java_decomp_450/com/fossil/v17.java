package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v17 implements z17 {
    @DexIgnore
    @Override // com.fossil.z17
    public s17 a() {
        return new s17(e(), f());
    }

    @DexIgnore
    @Override // com.fossil.z17
    public boolean b() {
        return Boolean.TRUE.equals(a("noResult"));
    }

    @DexIgnore
    @Override // com.fossil.z17
    public Boolean c() {
        return b("inTransaction");
    }

    @DexIgnore
    public boolean d() {
        return Boolean.TRUE.equals(a("continueOnError"));
    }

    @DexIgnore
    public final String e() {
        return (String) a("sql");
    }

    @DexIgnore
    public final List<Object> f() {
        return (List) a("arguments");
    }

    @DexIgnore
    public final Boolean b(String str) {
        Object a = a(str);
        if (a instanceof Boolean) {
            return (Boolean) a;
        }
        return null;
    }
}
