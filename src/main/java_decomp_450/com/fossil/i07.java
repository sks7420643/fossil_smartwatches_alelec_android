package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface i07 {
    @DexIgnore
    public static final i07 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements i07 {
        @DexIgnore
        @Override // com.fossil.i07
        public Map<Class<?>, Set<g07>> a(Object obj) {
            return d07.b(obj);
        }

        @DexIgnore
        @Override // com.fossil.i07
        public Map<Class<?>, h07> b(Object obj) {
            return d07.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<g07>> a(Object obj);

    @DexIgnore
    Map<Class<?>, h07> b(Object obj);
}
