package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ dn0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jl1(dn0 dn0) {
        super(1);
        this.a = dn0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        dn0 dn0 = this.a;
        long j = dn0.F;
        long j2 = dn0.G;
        long j3 = j + j2;
        dn0.H = j3;
        if (j3 != 0) {
            float f = (float) j3;
            dn0.I = ((float) j) / f;
            dn0.J = ((float) j2) / f;
        }
        this.a.n();
        return i97.a;
    }
}
