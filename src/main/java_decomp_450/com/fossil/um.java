package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class um implements om {
    @DexIgnore
    public /* final */ Handler a; // = f8.a(Looper.getMainLooper());

    @DexIgnore
    @Override // com.fossil.om
    public void a(long j, Runnable runnable) {
        this.a.postDelayed(runnable, j);
    }

    @DexIgnore
    @Override // com.fossil.om
    public void a(Runnable runnable) {
        this.a.removeCallbacks(runnable);
    }
}
