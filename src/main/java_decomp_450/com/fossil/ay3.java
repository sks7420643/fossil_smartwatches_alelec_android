package com.fossil;

import com.fossil.by3;
import com.fossil.gy3;
import com.fossil.zx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ay3<K, V> extends gy3<K, V> implements ty3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient ay3<V, K> f;

    @DexIgnore
    public ay3(by3<K, zx3<V>> by3, int i) {
        super(by3, i);
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.by3$b */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> copyOf(zy3<? extends K, ? extends V> zy3) {
        if (zy3.isEmpty()) {
            return of();
        }
        if (zy3 instanceof ay3) {
            ay3<K, V> ay3 = (ay3) zy3;
            if (!ay3.isPartialView()) {
                return ay3;
            }
        }
        by3.b bVar = new by3.b(zy3.asMap().size());
        int i = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : zy3.asMap().entrySet()) {
            zx3 copyOf = zx3.copyOf((Collection) entry.getValue());
            if (!copyOf.isEmpty()) {
                bVar.a(entry.getKey(), copyOf);
                i += copyOf.size();
            }
        }
        return new ay3<>(bVar.a(), i);
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of() {
        return hx3.INSTANCE;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.by3$b */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            by3.b builder = by3.builder();
            int i = 0;
            int i2 = 0;
            while (i < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    zx3.b builder2 = zx3.builder();
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        builder2.a(objectInputStream.readObject());
                    }
                    builder.a(readObject, builder2.a());
                    i2 += readInt2;
                    i++;
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                gy3.e.a.a(this, builder.a());
                gy3.e.b.a(this, i2);
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        wz3.a(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.ay3<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.ay3$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final ay3<V, K> a() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.a(entry.getValue(), entry.getKey());
        }
        ay3<V, K> a2 = builder.a();
        a2.f = this;
        return a2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends gy3.c<K, V> {
        @DexIgnore
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(K k, V v) {
            super.a((Object) k, (Object) v);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a((Map.Entry) entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        @CanIgnoreReturnValue
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a((Iterable) iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.gy3.c
        public ay3<K, V> a() {
            return (ay3) super.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of(K k, V v) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public ay3<V, K> inverse() {
        ay3<V, K> ay3 = this.f;
        if (ay3 != null) {
            return ay3;
        }
        ay3<V, K> a2 = a();
        this.f = a2;
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3, com.fossil.zy3
    public zx3<V> get(K k) {
        zx3<V> zx3 = (zx3) ((gy3) this).map.get(k);
        return zx3 == null ? zx3.of() : zx3;
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3
    @CanIgnoreReturnValue
    @Deprecated
    public zx3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.gy3, com.fossil.gy3, com.fossil.vw3
    @CanIgnoreReturnValue
    @Deprecated
    public zx3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.a((Iterable) iterable);
        return aVar.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        return builder.a();
    }

    @DexIgnore
    @Override // com.fossil.gy3
    public static <K, V> ay3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        builder.a((Object) k5, (Object) v5);
        return builder.a();
    }
}
