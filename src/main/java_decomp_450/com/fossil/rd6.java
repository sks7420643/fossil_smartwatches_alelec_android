package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rd6 implements Factory<ud6> {
    @DexIgnore
    public static ud6 a(pd6 pd6) {
        ud6 b = pd6.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
