package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum is0 {
    SUCCESS(0),
    NOT_START(1),
    REQUEST_ERROR(2),
    LACK_OF_SERVICE(3),
    LACK_OF_CHARACTERISTIC(4),
    INVALID_SERIAL_NUMBER(5),
    DATA_TRANSFER_RETRY_REACH_THRESHOLD(6),
    FLOW_BROKEN(7),
    EXCHANGED_VALUE_NOT_SATISFIED(8),
    CONNECTION_DROPPED(9),
    INVALID_FILE_LENGTH(10),
    MISMATCH_VERSION(11),
    INVALID_FILE_CRC(12),
    INVALID_RESPONSE(13),
    INVALID_DATA_LENGTH(14),
    UNSUPPORTED_FORMAT(15),
    WAITING_FOR_EXECUTION_TIMEOUT(16),
    NOT_ENOUGH_FILE_TO_PROCESS(17),
    INCOMPATIBLE_FIRMWARE(18),
    REQUEST_UNSUPPORTED(19),
    UNSUPPORTED_FILE_HANDLE(20),
    BLUETOOTH_OFF(21),
    AUTHENTICATION_FAILED(22),
    INVALID_PARAMETER(23),
    WRONG_RANDOM_NUMBER(24),
    SECRET_KEY_IS_REQUIRED(25),
    NOT_ALLOW_TO_START(26),
    DATABASE_ERROR(27),
    TARGET_FIRMWARE_NOT_MATCHED(28),
    HID_INPUT_DEVICE_DISABLED(257),
    INTERRUPTED(254);
    
    @DexIgnore
    public static /* final */ nq0 G; // = new nq0(null);

    @DexIgnore
    public is0(int i) {
    }
}
