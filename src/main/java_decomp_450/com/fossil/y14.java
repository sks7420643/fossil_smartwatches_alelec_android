package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y14 implements rj3 {
    @DexIgnore
    public /* final */ /* synthetic */ sn2 a;

    @DexIgnore
    public y14(sn2 sn2) {
        this.a = sn2;
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final Map<String, Object> a(String str, String str2, boolean z) {
        return this.a.a(str, str2, z);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final void b(String str, String str2, Bundle bundle) {
        this.a.a(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final String zza() {
        return this.a.e();
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final String zzb() {
        return this.a.f();
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final String zzc() {
        return this.a.c();
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final String zzd() {
        return this.a.b();
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final long zze() {
        return this.a.d();
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final void a(Bundle bundle) {
        this.a.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final void b(String str) {
        this.a.c(str);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final void zza(String str) {
        this.a.b(str);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final void a(String str, String str2, Bundle bundle) {
        this.a.b(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final List<Bundle> a(String str, String str2) {
        return this.a.b(str, str2);
    }

    @DexIgnore
    @Override // com.fossil.rj3
    public final int a(String str) {
        return this.a.d(str);
    }
}
