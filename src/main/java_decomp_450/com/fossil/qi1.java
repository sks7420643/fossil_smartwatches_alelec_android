package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qi1 extends fe7 implements gd7<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ qi1 a; // = new qi1();

    @DexIgnore
    public qi1() {
        super(1);
    }

    @DexIgnore
    /* renamed from: a */
    public final String invoke(BluetoothGattService bluetoothGattService) {
        StringBuilder sb = new StringBuilder();
        ee7.a((Object) bluetoothGattService, Constants.SERVICE);
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        ee7.a((Object) characteristics, "service.characteristics");
        sb.append(ea7.a(characteristics, "\n", null, null, 0, null, tg1.a, 30, null));
        return sb.toString();
    }
}
