package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.it;
import com.fossil.oy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements it.a {
        @DexIgnore
        public /* final */ /* synthetic */ ImageView a;
        @DexIgnore
        public /* final */ /* synthetic */ oy6 b;
        @DexIgnore
        public /* final */ /* synthetic */ qt4 c;

        @DexIgnore
        public a(ImageView imageView, oy6 oy6, qt4 qt4) {
            this.a = imageView;
            this.b = oy6;
            this.c = qt4;
        }

        @DexIgnore
        @Override // com.fossil.it.a
        public void a(Object obj) {
            ee7.b(obj, "data");
            it.a.C0082a.b(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.it.a
        public void onCancel(Object obj) {
            it.a.C0082a.a(this, obj);
        }

        @DexIgnore
        @Override // com.fossil.it.a
        public void a(Object obj, br brVar) {
            ee7.b(obj, "data");
            ee7.b(brVar, "source");
            it.a.C0082a.a(this, obj, brVar);
            this.a.setTag(this.c);
        }

        @DexIgnore
        @Override // com.fossil.it.a
        public void a(Object obj, Throwable th) {
            ee7.b(th, "throwable");
            it.a.C0082a.a(this, obj, th);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("Coil", "error: " + th.getMessage() + " - phoneDate: " + new Date() + " - realDate: " + vt4.a.a());
            this.a.setImageDrawable(this.b);
        }
    }

    @DexIgnore
    public static final void a(ImageView imageView, String str, String str2, oy6.b bVar, st4 st4) {
        ee7.b(imageView, "$this$setUrl");
        ee7.b(bVar, "drawableBuilder");
        ee7.b(st4, "colorGenerator");
        qt4 qt4 = new qt4(str, str2);
        pq.a(imageView);
        String valueOf = str2 == null || str2.length() == 0 ? ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR : String.valueOf(ph7.e(str2));
        if (str2 == null) {
            str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        oy6 b = bVar.b(valueOf, st4.a(str2));
        ee7.a((Object) b, "drawableBuilder.build(av\u2026tor.getColor(str ?: \"-\"))");
        if (str == null || str.length() == 0) {
            imageView.setImageDrawable(b);
            imageView.setTag(qt4);
            return;
        }
        lq b2 = iq.b();
        Context context = imageView.getContext();
        ee7.a((Object) context, "context");
        ft ftVar = new ft(context, b2.a());
        ftVar.b(str);
        ftVar.a(imageView);
        ftVar.a((Drawable) b);
        ftVar.a(new xt());
        ftVar.a((it.a) new a(imageView, b, qt4));
        b2.a(ftVar.t());
    }
}
