package com.fossil;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ak2<T extends IInterface> extends n62<T> {
    @DexIgnore
    public ak2(Context context, Looper looper, vj2 vj2, a12.b bVar, a12.c cVar, j62 j62) {
        super(context, looper, vj2.zzc(), j62, bVar, cVar);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public boolean D() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.n62
    public Set<Scope> a(Set<Scope> set) {
        return xd2.a(set);
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.n62
    public Set<Scope> e() {
        return z();
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public boolean n() {
        return !r92.a(w());
    }
}
