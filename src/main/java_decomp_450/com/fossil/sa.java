package com.fossil;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sa implements View.OnTouchListener {
    @DexIgnore
    public static /* final */ int w; // = ViewConfiguration.getTapTimeout();
    @DexIgnore
    public /* final */ a a; // = new a();
    @DexIgnore
    public /* final */ Interpolator b; // = new AccelerateInterpolator();
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public Runnable d;
    @DexIgnore
    public float[] e; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] f; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public float[] i; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] j; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] p; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;
        @DexIgnore
        public long e; // = Long.MIN_VALUE;
        @DexIgnore
        public long f; // = 0;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public long i; // = -1;
        @DexIgnore
        public float j;
        @DexIgnore
        public int k;

        @DexIgnore
        public final float a(float f2) {
            return (-4.0f * f2 * f2) + (f2 * 4.0f);
        }

        @DexIgnore
        public void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public void b(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public int c() {
            return this.h;
        }

        @DexIgnore
        public int d() {
            float f2 = this.c;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public int e() {
            float f2 = this.d;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public boolean f() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        @DexIgnore
        public void g() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = sa.a((int) (currentAnimationTimeMillis - this.e), 0, this.b);
            this.j = a(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        @DexIgnore
        public void h() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.e = currentAnimationTimeMillis;
            this.i = -1;
            this.f = currentAnimationTimeMillis;
            this.j = 0.5f;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public final float a(long j2) {
            if (j2 < this.e) {
                return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            long j3 = this.i;
            if (j3 < 0 || j2 < j3) {
                return sa.a(((float) (j2 - this.e)) / ((float) this.a), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f) * 0.5f;
            }
            long j4 = j2 - j3;
            float f2 = this.j;
            return (1.0f - f2) + (f2 * sa.a(((float) j4) / ((float) this.k), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f));
        }

        @DexIgnore
        public int b() {
            return this.g;
        }

        @DexIgnore
        public void a() {
            if (this.f != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float a2 = a(a(currentAnimationTimeMillis));
                this.f = currentAnimationTimeMillis;
                float f2 = ((float) (currentAnimationTimeMillis - this.f)) * a2;
                this.g = (int) (this.c * f2);
                this.h = (int) (f2 * this.d);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        @DexIgnore
        public void a(float f2, float f3) {
            this.c = f2;
            this.d = f3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            sa saVar = sa.this;
            if (saVar.t) {
                if (saVar.r) {
                    saVar.r = false;
                    saVar.a.h();
                }
                a aVar = sa.this.a;
                if (aVar.f() || !sa.this.c()) {
                    sa.this.t = false;
                    return;
                }
                sa saVar2 = sa.this;
                if (saVar2.s) {
                    saVar2.s = false;
                    saVar2.a();
                }
                aVar.a();
                sa.this.a(aVar.b(), aVar.c());
                da.a(sa.this.c, this);
            }
        }
    }

    @DexIgnore
    public sa(View view) {
        this.c = view;
        float f2 = Resources.getSystem().getDisplayMetrics().density;
        float f3 = (float) ((int) ((1575.0f * f2) + 0.5f));
        c(f3, f3);
        float f4 = (float) ((int) ((f2 * 315.0f) + 0.5f));
        d(f4, f4);
        d(1);
        b(Float.MAX_VALUE, Float.MAX_VALUE);
        e(0.2f, 0.2f);
        f(1.0f, 1.0f);
        c(w);
        f(500);
        e(500);
    }

    @DexIgnore
    public static float a(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    @DexIgnore
    public static int a(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    @DexIgnore
    public sa a(boolean z) {
        if (this.u && !z) {
            b();
        }
        this.u = z;
        return this;
    }

    @DexIgnore
    public abstract void a(int i2, int i3);

    @DexIgnore
    public abstract boolean a(int i2);

    @DexIgnore
    public sa b(float f2, float f3) {
        float[] fArr = this.f;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public abstract boolean b(int i2);

    @DexIgnore
    public sa c(float f2, float f3) {
        float[] fArr = this.p;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public sa d(float f2, float f3) {
        float[] fArr = this.j;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public sa e(float f2, float f3) {
        float[] fArr = this.e;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public sa f(float f2, float f3) {
        float[] fArr = this.i;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x0058;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            boolean r0 = r5.u
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r7.getActionMasked()
            r2 = 1
            if (r0 == 0) goto L_0x001a
            if (r0 == r2) goto L_0x0016
            r3 = 2
            if (r0 == r3) goto L_0x001e
            r6 = 3
            if (r0 == r6) goto L_0x0016
            goto L_0x0058
        L_0x0016:
            r5.b()
            goto L_0x0058
        L_0x001a:
            r5.s = r2
            r5.q = r1
        L_0x001e:
            float r0 = r7.getX()
            int r3 = r6.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r5.c
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r0 = r5.a(r1, r0, r3, r4)
            float r7 = r7.getY()
            int r6 = r6.getHeight()
            float r6 = (float) r6
            android.view.View r3 = r5.c
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r6 = r5.a(r2, r7, r6, r3)
            com.fossil.sa$a r7 = r5.a
            r7.a(r0, r6)
            boolean r6 = r5.t
            if (r6 != 0) goto L_0x0058
            boolean r6 = r5.c()
            if (r6 == 0) goto L_0x0058
            r5.d()
        L_0x0058:
            boolean r6 = r5.v
            if (r6 == 0) goto L_0x0061
            boolean r6 = r5.t
            if (r6 == 0) goto L_0x0061
            r1 = 1
        L_0x0061:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sa.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public final void b() {
        if (this.r) {
            this.t = false;
        } else {
            this.a.g();
        }
    }

    @DexIgnore
    public sa c(int i2) {
        this.h = i2;
        return this;
    }

    @DexIgnore
    public sa d(int i2) {
        this.g = i2;
        return this;
    }

    @DexIgnore
    public sa e(int i2) {
        this.a.a(i2);
        return this;
    }

    @DexIgnore
    public sa f(int i2) {
        this.a.b(i2);
        return this;
    }

    @DexIgnore
    public final float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.e[i2], f3, this.f[i2], f2);
        int i3 = (a2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (a2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
        if (i3 == 0) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f5 = this.i[i2];
        float f6 = this.j[i2];
        float f7 = this.p[i2];
        float f8 = f5 * f4;
        if (i3 > 0) {
            return a(a2 * f8, f6, f7);
        }
        return -a((-a2) * f8, f6, f7);
    }

    @DexIgnore
    public boolean c() {
        a aVar = this.a;
        int e2 = aVar.e();
        int d2 = aVar.d();
        return (e2 != 0 && b(e2)) || (d2 != 0 && a(d2));
    }

    @DexIgnore
    public final void d() {
        int i2;
        if (this.d == null) {
            this.d = new b();
        }
        this.t = true;
        this.r = true;
        if (this.q || (i2 = this.h) <= 0) {
            this.d.run();
        } else {
            da.a(this.c, this.d, (long) i2);
        }
        this.q = true;
    }

    @DexIgnore
    public final float a(float f2, float f3, float f4, float f5) {
        float f6;
        float a2 = a(f2 * f3, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4);
        float a3 = a(f3 - f5, a2) - a(f5, a2);
        if (a3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f6 = -this.b.getInterpolation(-a3);
        } else if (a3 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f6 = this.b.getInterpolation(a3);
        }
        return a(f6, -1.0f, 1.0f);
    }

    @DexIgnore
    public final float a(float f2, float f3) {
        if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i2 = this.g;
        if (i2 == 0 || i2 == 1) {
            if (f2 < f3) {
                if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return 1.0f - (f2 / f3);
                }
                if (!this.t || this.g != 1) {
                    return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                return 1.0f;
            }
        } else if (i2 == 2 && f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return f2 / (-f3);
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void a() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        this.c.onTouchEvent(obtain);
        obtain.recycle();
    }
}
