package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sw2 {
    @DexIgnore
    public static /* final */ sw2 a; // = new uw2();
    @DexIgnore
    public static /* final */ sw2 b; // = new xw2();

    @DexIgnore
    public sw2() {
    }

    @DexIgnore
    public static sw2 a() {
        return a;
    }

    @DexIgnore
    public static sw2 b() {
        return b;
    }

    @DexIgnore
    public abstract void a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);
}
