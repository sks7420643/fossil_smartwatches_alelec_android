package com.fossil;

import android.util.Log;
import com.fossil.i12;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k12<R extends i12> implements j12<R> {
    @DexIgnore
    @Override // com.fossil.j12
    public final void a(R r) {
        Status a = r.a();
        if (a.y()) {
            b(r);
            return;
        }
        a(a);
        if (r instanceof e12) {
            try {
                ((e12) r).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(r);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("ResultCallbacks", sb.toString(), e);
            }
        }
    }

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public abstract void b(R r);
}
