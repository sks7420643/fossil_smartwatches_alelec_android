package com.fossil;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o44 {
    @DexIgnore
    public static void a(File file, List<n44> list) {
        for (n44 n44 : list) {
            InputStream inputStream = null;
            try {
                inputStream = n44.b();
                if (inputStream != null) {
                    a(inputStream, new File(file, n44.a()));
                }
            } catch (IOException unused) {
            } catch (Throwable th) {
                t34.a((Closeable) null);
                throw th;
            }
            t34.a((Closeable) inputStream);
        }
    }

    @DexIgnore
    public static void a(InputStream inputStream, File file) throws IOException {
        if (inputStream != null) {
            byte[] bArr = new byte[8192];
            GZIPOutputStream gZIPOutputStream = null;
            try {
                GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(new FileOutputStream(file));
                while (true) {
                    try {
                        int read = inputStream.read(bArr);
                        if (read > 0) {
                            gZIPOutputStream2.write(bArr, 0, read);
                        } else {
                            gZIPOutputStream2.finish();
                            t34.a(gZIPOutputStream2);
                            return;
                        }
                    } catch (Throwable th) {
                        th = th;
                        gZIPOutputStream = gZIPOutputStream2;
                        t34.a(gZIPOutputStream);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                t34.a(gZIPOutputStream);
                throw th;
            }
        }
    }
}
