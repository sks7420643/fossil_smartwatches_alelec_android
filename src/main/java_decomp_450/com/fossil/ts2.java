package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts2 extends os2<E> {
    @DexIgnore
    public /* final */ transient int c;
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ /* synthetic */ os2 zzc;

    @DexIgnore
    public ts2(os2 os2, int i, int i2) {
        this.zzc = os2;
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        or2.a(i, this.d);
        return (E) this.zzc.get(i + this.c);
    }

    @DexIgnore
    public final int size() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.os2
    /* renamed from: zza */
    public final os2<E> subList(int i, int i2) {
        or2.a(i, i2, this.d);
        os2 os2 = this.zzc;
        int i3 = this.c;
        return (os2) os2.subList(i + i3, i2 + i3);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final Object[] zze() {
        return this.zzc.zze();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzf() {
        return this.zzc.zzf() + this.c;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzg() {
        return this.zzc.zzf() + this.c + this.d;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return true;
    }
}
