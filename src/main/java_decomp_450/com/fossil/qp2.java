package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp2 extends bw2<qp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ qp2 zzf;
    @DexIgnore
    public static volatile wx2<qp2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<qp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(qp2.zzf);
        }

        @DexIgnore
        public final a a(int i) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((qp2) ((bw2.a) this).b).b(i);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((qp2) ((bw2.a) this).b).a(j);
            return this;
        }
    }

    /*
    static {
        qp2 qp2 = new qp2();
        zzf = qp2;
        bw2.a(qp2.class, qp2);
    }
    */

    @DexIgnore
    public static a s() {
        return (a) zzf.e();
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zze = j;
    }

    @DexIgnore
    public final void b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long r() {
        return this.zze;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new qp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                wx2<qp2> wx2 = zzg;
                if (wx2 == null) {
                    synchronized (qp2.class) {
                        wx2 = zzg;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzf);
                            zzg = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
