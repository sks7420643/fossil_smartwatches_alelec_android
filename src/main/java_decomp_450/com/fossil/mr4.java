package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr4 extends he {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<mn4> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<List<xn4>, ServerError, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<dn4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<tt4, mn4>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gu4> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<String, String, String>> i; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<mn4> j;
    @DexIgnore
    public String k;
    @DexIgnore
    public mn4 l;
    @DexIgnore
    public Timer m;
    @DexIgnore
    public /* final */ ro4 n;
    @DexIgnore
    public /* final */ xo4 o;
    @DexIgnore
    public /* final */ ch5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mr4.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {121, 137, 139, 157}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAccepted;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mn4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, mn4 mn4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$challenge = mn4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$challenge, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Long a;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.n.b();
                    String s = this.$challenge.s();
                    if (s != null && (a = pb7.a(PortfolioApp.g0.c().n(s))) != null) {
                        return a;
                    }
                    this.this$0.this$0.p.a(pb7.a(true));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mr4$b$b")
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$currentChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.mr4$b$b  reason: collision with other inner class name */
        public static final class C0126b extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0126b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0126b bVar = new C0126b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
                return ((C0126b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n.a(new String[]{"running", "waiting"}, vt4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {137}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, int i, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ro4 b = this.this$0.this$0.n;
                    String a2 = mr4.a(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.a(a2, true, i2, str, (fb7<? super ko4<mn4>>) this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$result$2", f = "BCWaitingChallengeDetailViewModel.kt", l = {139}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, int i, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ro4 b = this.this$0.this$0.n;
                    String a2 = mr4.a(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.a(a2, i2, str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(mr4 mr4, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
            this.$isAccepted = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$isAccepted, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0110  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0139  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
                r17 = this;
                r0 = r17
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                r7 = 0
                r8 = 0
                if (r2 == 0) goto L_0x0073
                if (r2 == r6) goto L_0x0069
                if (r2 == r5) goto L_0x0051
                if (r2 == r4) goto L_0x0039
                if (r2 != r3) goto L_0x0031
                java.lang.Object r1 = r0.L$4
                com.fossil.mn4 r1 = (com.fossil.mn4) r1
                java.lang.Object r1 = r0.L$3
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r1 = r0.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r1 = r0.L$1
                com.fossil.mn4 r1 = (com.fossil.mn4) r1
                java.lang.Object r1 = r0.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r18)
                goto L_0x01e4
            L_0x0031:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0039:
                int r2 = r0.I$0
                java.lang.Object r4 = r0.L$2
                java.lang.String r4 = (java.lang.String) r4
                boolean r5 = r0.Z$0
                java.lang.Object r9 = r0.L$1
                com.fossil.mn4 r9 = (com.fossil.mn4) r9
                java.lang.Object r10 = r0.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r18)
                r11 = r4
                r4 = r18
                goto L_0x0106
            L_0x0051:
                int r2 = r0.I$0
                java.lang.Object r4 = r0.L$2
                java.lang.String r4 = (java.lang.String) r4
                boolean r5 = r0.Z$0
                java.lang.Object r9 = r0.L$1
                com.fossil.mn4 r9 = (com.fossil.mn4) r9
                java.lang.Object r10 = r0.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r18)
                r11 = r4
                r4 = r18
                goto L_0x00e5
            L_0x0069:
                java.lang.Object r2 = r0.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r18)
                r9 = r18
                goto L_0x009d
            L_0x0073:
                com.fossil.t87.a(r18)
                com.fossil.yi7 r2 = r0.p$
                com.fossil.mr4 r9 = r0.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.b
                java.lang.Boolean r10 = com.fossil.pb7.a(r6)
                com.fossil.r87 r10 = com.fossil.w87.a(r8, r10)
                r9.a(r10)
                com.fossil.ti7 r9 = com.fossil.qj7.b()
                com.fossil.mr4$b$b r10 = new com.fossil.mr4$b$b
                r10.<init>(r0, r8)
                r0.L$0 = r2
                r0.label = r6
                java.lang.Object r9 = com.fossil.vh7.a(r9, r10, r0)
                if (r9 != r1) goto L_0x009d
                return r1
            L_0x009d:
                r10 = r2
                com.fossil.mn4 r9 = (com.fossil.mn4) r9
                if (r9 != 0) goto L_0x0226
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                boolean r2 = r2.e()
                if (r2 == 0) goto L_0x0209
                com.portfolio.platform.PortfolioApp$a r11 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r11 = r11.c()
                java.lang.String r11 = r11.c()
                com.fossil.be5$a r12 = com.fossil.be5.o
                boolean r12 = r12.f(r11)
                if (r12 == 0) goto L_0x00c2
                r12 = 2
                goto L_0x00c3
            L_0x00c2:
                r12 = 3
            L_0x00c3:
                boolean r13 = r0.$isAccepted
                if (r13 == 0) goto L_0x00e8
                com.fossil.ti7 r4 = com.fossil.qj7.b()
                com.fossil.mr4$b$c r13 = new com.fossil.mr4$b$c
                r13.<init>(r0, r12, r11, r8)
                r0.L$0 = r10
                r0.L$1 = r9
                r0.Z$0 = r2
                r0.L$2 = r11
                r0.I$0 = r12
                r0.label = r5
                java.lang.Object r4 = com.fossil.vh7.a(r4, r13, r0)
                if (r4 != r1) goto L_0x00e3
                return r1
            L_0x00e3:
                r5 = r2
                r2 = r12
            L_0x00e5:
                com.fossil.ko4 r4 = (com.fossil.ko4) r4
                goto L_0x0108
            L_0x00e8:
                com.fossil.ti7 r5 = com.fossil.qj7.b()
                com.fossil.mr4$b$d r13 = new com.fossil.mr4$b$d
                r13.<init>(r0, r12, r11, r8)
                r0.L$0 = r10
                r0.L$1 = r9
                r0.Z$0 = r2
                r0.L$2 = r11
                r0.I$0 = r12
                r0.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r5, r13, r0)
                if (r4 != r1) goto L_0x0104
                return r1
            L_0x0104:
                r5 = r2
                r2 = r12
            L_0x0106:
                com.fossil.ko4 r4 = (com.fossil.ko4) r4
            L_0x0108:
                java.lang.Object r12 = r4.c()
                com.fossil.mn4 r12 = (com.fossil.mn4) r12
                if (r12 != 0) goto L_0x0139
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.b
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                com.fossil.r87 r2 = com.fossil.w87.a(r8, r2)
                r1.a(r2)
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.a
                com.fossil.r87 r2 = new com.fossil.r87
                java.lang.Boolean r3 = com.fossil.pb7.a(r7)
                com.portfolio.platform.data.model.ServerError r4 = r4.a()
                r2.<init>(r3, r4)
                r1.a(r2)
                goto L_0x0242
            L_0x0139:
                boolean r13 = r0.$isAccepted
                if (r13 == 0) goto L_0x015c
                com.fossil.mr4 r13 = r0.this$0
                androidx.lifecycle.MutableLiveData r13 = r13.i
                com.fossil.v87 r14 = new com.fossil.v87
                java.lang.String r15 = r12.f()
                com.portfolio.platform.PortfolioApp$a r16 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r16 = r16.c()
                java.lang.String r6 = r16.w()
                java.lang.String r3 = "bc_accept_challenge"
                r14.<init>(r3, r15, r6)
                r13.a(r14)
                goto L_0x017a
            L_0x015c:
                com.fossil.mr4 r3 = r0.this$0
                androidx.lifecycle.MutableLiveData r3 = r3.i
                com.fossil.v87 r6 = new com.fossil.v87
                java.lang.String r13 = r12.f()
                com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r14 = r14.c()
                java.lang.String r14 = r14.w()
                java.lang.String r15 = "bc_join_challenge"
                r6.<init>(r15, r13, r14)
                r3.a(r6)
            L_0x017a:
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.mr4$a r6 = com.fossil.mr4.r
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "acceptOrJoin - challenge: "
                r13.append(r14)
                r13.append(r12)
                java.lang.String r13 = r13.toString()
                r3.e(r6, r13)
                com.fossil.mr4 r3 = r0.this$0
                com.fossil.ch5 r3 = r3.p
                r3.b(r7)
                com.fossil.mr4 r3 = r0.this$0
                com.fossil.ch5 r3 = r3.p
                java.util.Date r6 = r12.e()
                if (r6 == 0) goto L_0x01ba
                long r13 = r6.getTime()
                java.lang.Long r6 = com.fossil.pb7.a(r13)
                if (r6 == 0) goto L_0x01ba
                goto L_0x01c0
            L_0x01ba:
                r13 = 0
                java.lang.Long r6 = com.fossil.pb7.a(r13)
            L_0x01c0:
                r3.a(r6)
                com.fossil.ti7 r3 = com.fossil.qj7.b()
                com.fossil.mr4$b$a r6 = new com.fossil.mr4$b$a
                r6.<init>(r0, r12, r8)
                r0.L$0 = r10
                r0.L$1 = r9
                r0.Z$0 = r5
                r0.L$2 = r11
                r0.I$0 = r2
                r0.L$3 = r4
                r0.L$4 = r12
                r2 = 4
                r0.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r3, r6, r0)
                if (r2 != r1) goto L_0x01e4
                return r1
            L_0x01e4:
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.b
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                com.fossil.r87 r2 = com.fossil.w87.a(r8, r2)
                r1.a(r2)
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.a
                com.fossil.r87 r2 = new com.fossil.r87
                r3 = 1
                java.lang.Boolean r3 = com.fossil.pb7.a(r3)
                r2.<init>(r3, r8)
                r1.a(r2)
                goto L_0x0242
            L_0x0209:
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.b
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                com.fossil.r87 r2 = com.fossil.w87.a(r8, r2)
                r1.a(r2)
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.g
                com.fossil.gu4 r2 = com.fossil.gu4.NEED_ACTIVE_DEVICE
                r1.a(r2)
                goto L_0x0242
            L_0x0226:
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.b
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                com.fossil.r87 r2 = com.fossil.w87.a(r8, r2)
                r1.a(r2)
                com.fossil.mr4 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.g
                com.fossil.gu4 r2 = com.fossil.gu4.JOIN_TOO_MUCH
                r1.a(r2)
            L_0x0242:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mr4.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {333, 334, 338, 344}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<un4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<un4>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.o.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsIn$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends xn4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ se7 $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $players;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(se7 se7, List list, fb7 fb7) {
                super(2, fb7);
                this.$friends = se7;
                this.$players = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.$friends, this.$players, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends xn4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return nt4.b(this.$friends.element, this.$players);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mr4$c$c")
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {333}, m = "invokeSuspend")
        /* renamed from: com.fossil.mr4$c$c  reason: collision with other inner class name */
        public static final class C0127c extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0127c(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0127c cVar = new C0127c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                return ((C0127c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xo4 c = this.this$0.this$0.o;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.c(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$playersWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {334}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    this.L$0 = this.p$;
                    this.label = 1;
                    obj = ro4.a(this.this$0.this$0.n, this.this$0.$challengeId, new String[]{"waiting", "running", "completed", "left_after_start"}, 0, this, 4, null);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mr4 mr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00b8  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x010a  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x014b  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 4
                r3 = 3
                r4 = 2
                r5 = 1
                r6 = 0
                if (r1 == 0) goto L_0x0069
                if (r1 == r5) goto L_0x0061
                if (r1 == r4) goto L_0x0053
                if (r1 == r3) goto L_0x0036
                if (r1 != r2) goto L_0x002e
                java.lang.Object r0 = r12.L$4
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r12.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r12.L$2
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r1 = r12.L$1
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0132
            L_0x002e:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0036:
                java.lang.Object r1 = r12.L$5
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r3 = r12.L$4
                com.fossil.se7 r3 = (com.fossil.se7) r3
                java.lang.Object r4 = r12.L$3
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r7 = r12.L$2
                com.fossil.ko4 r7 = (com.fossil.ko4) r7
                java.lang.Object r8 = r12.L$1
                com.fossil.ko4 r8 = (com.fossil.ko4) r8
                java.lang.Object r9 = r12.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r13)
                goto L_0x00d7
            L_0x0053:
                java.lang.Object r1 = r12.L$1
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r4 = r12.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r13)
                r8 = r1
                r9 = r4
                goto L_0x009d
            L_0x0061:
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0082
            L_0x0069:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r1 = r12.p$
                com.fossil.ti7 r13 = com.fossil.qj7.b()
                com.fossil.mr4$c$c r7 = new com.fossil.mr4$c$c
                r7.<init>(r12, r6)
                r12.L$0 = r1
                r12.label = r5
                java.lang.Object r13 = com.fossil.vh7.a(r13, r7, r12)
                if (r13 != r0) goto L_0x0082
                return r0
            L_0x0082:
                com.fossil.ko4 r13 = (com.fossil.ko4) r13
                com.fossil.ti7 r7 = com.fossil.qj7.b()
                com.fossil.mr4$c$d r8 = new com.fossil.mr4$c$d
                r8.<init>(r12, r6)
                r12.L$0 = r1
                r12.L$1 = r13
                r12.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r7, r8, r12)
                if (r4 != r0) goto L_0x009a
                return r0
            L_0x009a:
                r8 = r13
                r9 = r1
                r13 = r4
            L_0x009d:
                r7 = r13
                com.fossil.ko4 r7 = (com.fossil.ko4) r7
                java.lang.Object r13 = r7.c()
                r4 = r13
                java.util.List r4 = (java.util.List) r4
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                java.lang.Object r13 = r8.c()
                java.util.List r13 = (java.util.List) r13
                r1.element = r13
                java.util.List r13 = (java.util.List) r13
                if (r13 != 0) goto L_0x00dc
                com.fossil.ti7 r13 = com.fossil.qj7.b()
                com.fossil.mr4$c$a r10 = new com.fossil.mr4$c$a
                r10.<init>(r12, r6)
                r12.L$0 = r9
                r12.L$1 = r8
                r12.L$2 = r7
                r12.L$3 = r4
                r12.L$4 = r1
                r12.L$5 = r1
                r12.label = r3
                java.lang.Object r13 = com.fossil.vh7.a(r13, r10, r12)
                if (r13 != r0) goto L_0x00d6
                return r0
            L_0x00d6:
                r3 = r1
            L_0x00d7:
                java.util.List r13 = (java.util.List) r13
                r1.element = r13
                r1 = r3
            L_0x00dc:
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                com.fossil.mr4$a r3 = com.fossil.mr4.r
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "friendsInChallenge - players: "
                r10.append(r11)
                r10.append(r4)
                java.lang.String r11 = " - friends: "
                r10.append(r11)
                T r11 = r1.element
                java.util.List r11 = (java.util.List) r11
                r10.append(r11)
                java.lang.String r10 = r10.toString()
                r13.e(r3, r10)
                if (r4 == 0) goto L_0x014b
                T r13 = r1.element
                java.util.List r13 = (java.util.List) r13
                boolean r13 = r13.isEmpty()
                r13 = r13 ^ r5
                if (r13 == 0) goto L_0x015d
                com.fossil.ti7 r13 = com.fossil.qj7.a()
                com.fossil.mr4$c$b r3 = new com.fossil.mr4$c$b
                r3.<init>(r1, r4, r6)
                r12.L$0 = r9
                r12.L$1 = r8
                r12.L$2 = r7
                r12.L$3 = r4
                r12.L$4 = r1
                r12.label = r2
                java.lang.Object r13 = com.fossil.vh7.a(r13, r3, r12)
                if (r13 != r0) goto L_0x0131
                return r0
            L_0x0131:
                r0 = r4
            L_0x0132:
                java.util.List r13 = (java.util.List) r13
                com.fossil.mr4 r1 = r12.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.d
                com.fossil.v87 r2 = new com.fossil.v87
                int r0 = r0.size()
                java.lang.Integer r0 = com.fossil.pb7.a(r0)
                r2.<init>(r13, r6, r0)
                r1.a(r2)
                goto L_0x015d
            L_0x014b:
                com.fossil.mr4 r13 = r12.this$0
                androidx.lifecycle.MutableLiveData r13 = r13.d
                com.fossil.v87 r0 = new com.fossil.v87
                com.portfolio.platform.data.model.ServerError r1 = r7.a()
                r0.<init>(r6, r1, r6)
                r13.a(r0)
            L_0x015d:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mr4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {200}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {201, 212}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mr4$d$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.mr4$d$a$a  reason: collision with other inner class name */
            public static final class C0128a extends zb7 implements kd7<yi7, fb7<? super Long>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $synDataStr;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0128a(String str, fb7 fb7) {
                    super(2, fb7);
                    this.$synDataStr = str;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0128a aVar = new C0128a(this.$synDataStr, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Long> fb7) {
                    return ((C0128a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return pb7.a(PortfolioApp.g0.c().n(this.$synDataStr));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    ro4 b = this.this$0.this$0.n;
                    String a2 = mr4.a(this.this$0.this$0);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.a(a2, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    String str = (String) this.L$3;
                    Long l = (Long) this.L$2;
                    ko4 ko4 = (ko4) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    qe5.c.b();
                    this.this$0.this$0.a.a(new r87(pb7.a(true), null));
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ko4 ko42 = (ko4) obj;
                this.this$0.this$0.b.a(w87.a(null, pb7.a(false)));
                if (ko42.c() != null) {
                    this.this$0.this$0.i.a(new v87("bc_left_challenge_before_start", ((mn4) ko42.c()).f(), PortfolioApp.g0.c().w()));
                    this.this$0.this$0.p.a(pb7.a(false));
                    Long c = this.this$0.this$0.p.c();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = mr4.r.a();
                    local.e(a3, "leaveChallenge - consider sync data - endTimeOfUnsetChallenge: " + c);
                    if (c != null && c.longValue() == 0) {
                        String s = ((mn4) ko42.c()).s();
                        ti7 b2 = qj7.b();
                        C0128a aVar = new C0128a(s, null);
                        this.L$0 = yi7;
                        this.L$1 = ko42;
                        this.L$2 = c;
                        this.L$3 = s;
                        this.label = 2;
                        if (vh7.a(b2, aVar, this) == a) {
                            return a;
                        }
                        qe5.c.b();
                        this.this$0.this$0.a.a(new r87(pb7.a(true), null));
                        return i97.a;
                    }
                    this.this$0.this$0.p.a(pb7.a(0L));
                    qe5.c.b();
                    this.this$0.this$0.a.a(new r87(pb7.a(true), null));
                    return i97.a;
                }
                this.this$0.this$0.a.a(new r87(pb7.a(false), ko42.a()));
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(mr4 mr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {Action.Presenter.PREVIOUS}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(mr4 mr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                mr4 mr4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (mr4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a(w87.a(pb7.a(false), null));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel", f = "BCWaitingChallengeDetailViewModel.kt", l = {308}, m = "loadChallengeFromServer")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(mr4 mr4, fb7 fb7) {
            super(fb7);
            this.this$0 = mr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallengeFromServer$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {308}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(mr4 mr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 b = this.this$0.n;
                String a2 = mr4.a(this.this$0);
                this.L$0 = yi7;
                this.label = 1;
                obj = ro4.a(b, a2, (String) null, this, 2, (Object) null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadForVisitChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {105}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(mr4 mr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                mr4 mr4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (mr4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a(w87.a(null, pb7.a(false)));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ mr4 a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ mn4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mr4$i$a$a")
            /* renamed from: com.fossil.mr4$i$a$a  reason: collision with other inner class name */
            public static final class C0129a extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0129a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0129a aVar = new C0129a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
                    return ((C0129a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        i iVar = this.this$0.this$0;
                        if (iVar.b) {
                            iVar.c.a("running");
                        } else {
                            iVar.c.a("completed");
                        }
                        int c = this.this$0.this$0.a.n.c(this.this$0.this$0.c.f());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a = mr4.r.a();
                        local.e(a, "starAndStopChallenge - count: " + c);
                        if (c > 0) {
                            return pb7.a(this.this$0.this$0.a.n.a(this.this$0.this$0.c));
                        }
                        this.this$0.this$0.a.c.a(this.this$0.this$0.c);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, i iVar) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = qj7.b();
                    C0129a aVar = new C0129a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(b, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public i(mr4 mr4, boolean z, mn4 mn4) {
            this.a = mr4;
            this.b = z;
            this.c = mn4;
        }

        @DexIgnore
        public void run() {
            ik7 unused = xh7.b(ie.a(this.a), null, null, new a(null, this), 3, null);
        }
    }

    /*
    static {
        String simpleName = mr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCWaitingChallengeDetail\u2026el::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public mr4(ro4 ro4, xo4 xo4, ch5 ch5) {
        ee7.b(ro4, "challengeRepository");
        ee7.b(xo4, "friendRepository");
        ee7.b(ch5, "shared");
        this.n = ro4;
        this.o = xo4;
        this.p = ch5;
    }

    @DexIgnore
    public static final /* synthetic */ String a(mr4 mr4) {
        String str = mr4.k;
        if (str != null) {
            return str;
        }
        ee7.d("challengeId");
        throw null;
    }

    @DexIgnore
    public final void k() {
        ik7 unused = xh7.b(ie.a(this), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final void l() {
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void m() {
        LiveData<mn4> liveData = this.j;
        String str = null;
        mn4 a2 = liveData != null ? liveData.a() : null;
        if (a2 != null) {
            String w = PortfolioApp.g0.c().w();
            eo4 i2 = a2.i();
            if (i2 != null) {
                str = i2.b();
            }
            if (ee7.a((Object) w, (Object) str) && ee7.a((Object) "waiting", (Object) a2.p())) {
                long b2 = vt4.a.b();
                Date m2 = a2.m();
                if (b2 < (m2 != null ? m2.getTime() : 0)) {
                    this.e.a(ot4.a.f());
                    return;
                }
            }
            this.e.a(ot4.a.e());
        }
    }

    @DexIgnore
    public final void n() {
        Timer timer = this.m;
        if (timer != null) {
            timer.cancel();
        }
    }

    @DexIgnore
    public final void o() {
        l();
        String str = this.k;
        if (str != null) {
            a(str);
        } else {
            ee7.d("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void p() {
        mn4 a2 = this.c.a();
        mn4 mn4 = null;
        if (a2 == null) {
            LiveData<mn4> liveData = this.j;
            a2 = liveData != null ? liveData.a() : null;
        }
        if (a2 != null) {
            mn4 = nt4.a(a2);
        }
        a(mn4);
    }

    @DexIgnore
    public final LiveData<v87<String, String, String>> a() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<mn4> b() {
        return au4.a(this.c);
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> c() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<v87<List<xn4>, ServerError, Integer>> d() {
        return au4.a(this.d);
    }

    @DexIgnore
    public final LiveData<mn4> e() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, Boolean>> f() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> g() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<r87<tt4, mn4>> h() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<List<dn4>> i() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<gu4> j() {
        return this.g;
    }

    @DexIgnore
    public final void a(mn4 mn4, String str) {
        String str2;
        Date m2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = q;
        local.e(str3, "init - challenge: " + mn4 + " - visitId: " + str);
        if (str == null) {
            this.l = mn4;
            if (mn4 == null || (str2 = mn4.f()) == null) {
                str2 = "";
            }
            this.k = str2;
            ro4 ro4 = this.n;
            if (str2 != null) {
                this.j = au4.a(ro4.b(str2));
                a(mn4);
                if (vt4.a.b() < ((mn4 == null || (m2 = mn4.m()) == null) ? 0 : m2.getTime())) {
                    l();
                }
            } else {
                ee7.d("challengeId");
                throw null;
            }
        } else {
            this.k = str;
            b(str);
        }
        ro4 ro42 = this.n;
        String str4 = this.k;
        if (str4 != null) {
            ro42.a(str4, false);
            String str5 = this.k;
            if (str5 != null) {
                a(str5);
            } else {
                ee7.d("challengeId");
                throw null;
            }
        } else {
            ee7.d("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "loadForVisitChallenge - visitId: " + str);
        ik7 unused = xh7.b(ie.a(this), null, null, new h(this, null), 3, null);
    }

    @DexIgnore
    public final void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "acceptOrJoin - isAccepted: " + z);
        ik7 unused = xh7.b(ie.a(this), null, null, new b(this, z, null), 3, null);
    }

    @DexIgnore
    public final void a(tt4 tt4) {
        ee7.b(tt4, "option");
        LiveData<mn4> liveData = this.j;
        mn4 a2 = liveData != null ? liveData.a() : null;
        if (a2 != null) {
            int i2 = nr4.a[tt4.ordinal()];
            if (i2 == 1) {
                a(tt4.EDIT, a2);
            } else if (i2 == 2) {
                a(tt4.ADD_FRIENDS, a2);
            } else if (i2 == 3) {
                a(tt4.LEAVE, a2);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.mn4 r14) {
        /*
            r13 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.mr4.q
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "starAndStopChallenge - challenge: "
            r2.append(r3)
            r2.append(r14)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            if (r14 == 0) goto L_0x00db
            java.lang.String r0 = r14.p()
            java.lang.String r1 = "completed"
            boolean r0 = com.fossil.ee7.a(r1, r0)
            r1 = 1
            r0 = r0 ^ r1
            if (r0 == 0) goto L_0x00db
            java.util.Date r0 = r14.m()
            r2 = 0
            if (r0 == 0) goto L_0x0039
            long r4 = r0.getTime()
            goto L_0x003a
        L_0x0039:
            r4 = r2
        L_0x003a:
            java.util.Date r0 = r14.e()
            if (r0 == 0) goto L_0x0045
            long r6 = r0.getTime()
            goto L_0x0046
        L_0x0045:
            r6 = r2
        L_0x0046:
            java.lang.String r0 = r14.p()
            java.lang.String r8 = "waiting"
            boolean r0 = com.fossil.ee7.a(r8, r0)
            r8 = 0
            if (r0 == 0) goto L_0x0068
            com.fossil.vt4 r0 = com.fossil.vt4.a
            long r9 = r0.b()
            int r0 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x0066
            com.fossil.vt4 r0 = com.fossil.vt4.a
            long r8 = r0.b()
            long r8 = r4 - r8
            goto L_0x0072
        L_0x0066:
            r8 = r2
            goto L_0x0071
        L_0x0068:
            com.fossil.vt4 r0 = com.fossil.vt4.a
            long r0 = r0.b()
            long r0 = r6 - r0
            r8 = r0
        L_0x0071:
            r1 = 0
        L_0x0072:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.fossil.os4$a r10 = com.fossil.os4.n
            java.lang.String r10 = r10.a()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "starAndStopChallenge - startTime: "
            r11.append(r12)
            r11.append(r4)
            java.lang.String r4 = " - endTime: "
            r11.append(r4)
            r11.append(r6)
            r4 = 32
            r11.append(r4)
            java.lang.String r4 = "- exactTime: "
            r11.append(r4)
            com.fossil.vt4 r4 = com.fossil.vt4.a
            long r4 = r4.b()
            r11.append(r4)
            java.lang.String r4 = "- timeLeft: "
            r11.append(r4)
            r11.append(r8)
            java.lang.String r4 = " - isStart: "
            r11.append(r4)
            r11.append(r1)
            java.lang.String r4 = r11.toString()
            r0.e(r10, r4)
            int r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c2
            goto L_0x00c3
        L_0x00c2:
            r2 = r8
        L_0x00c3:
            java.util.Timer r0 = r13.m
            if (r0 == 0) goto L_0x00ca
            r0.cancel()
        L_0x00ca:
            java.util.Timer r0 = new java.util.Timer
            r0.<init>()
            r13.m = r0
            if (r0 == 0) goto L_0x00db
            com.fossil.mr4$i r4 = new com.fossil.mr4$i
            r4.<init>(r13, r1, r14)
            r0.schedule(r4, r2)
        L_0x00db:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mr4.a(com.fossil.mn4):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.mr4.f
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.mr4$f r0 = (com.fossil.mr4.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.mr4$f r0 = new com.fossil.mr4$f
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.mr4 r0 = (com.fossil.mr4) r0
            com.fossil.t87.a(r8)
            goto L_0x004e
        L_0x002d:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0035:
            com.fossil.t87.a(r8)
            com.fossil.ti7 r8 = com.fossil.qj7.b()
            com.fossil.mr4$g r2 = new com.fossil.mr4$g
            r4 = 0
            r2.<init>(r7, r4)
            r0.L$0 = r7
            r0.label = r3
            java.lang.Object r8 = com.fossil.vh7.a(r8, r2, r0)
            if (r8 != r1) goto L_0x004d
            return r1
        L_0x004d:
            r0 = r7
        L_0x004e:
            com.fossil.ko4 r8 = (com.fossil.ko4) r8
            java.lang.Object r1 = r8.c()
            com.fossil.mn4 r1 = (com.fossil.mn4) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.fossil.mr4.q
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "loadChallengeFromServer - challenge: "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r2.e(r4, r5)
            if (r1 == 0) goto L_0x0083
            r0.l = r1
            androidx.lifecycle.LiveData<com.fossil.mn4> r8 = r0.j
            if (r8 != 0) goto L_0x007f
            androidx.lifecycle.MutableLiveData<com.fossil.mn4> r8 = r0.c
            r8.a(r1)
        L_0x007f:
            r0.a(r1)
            goto L_0x00a7
        L_0x0083:
            com.portfolio.platform.data.model.ServerError r8 = r8.a()
            androidx.lifecycle.LiveData<com.fossil.mn4> r1 = r0.j
            if (r1 != 0) goto L_0x0099
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r0.h
            java.lang.Boolean r1 = com.fossil.pb7.a(r3)
            com.fossil.r87 r8 = com.fossil.w87.a(r1, r8)
            r0.a(r8)
            goto L_0x00a7
        L_0x0099:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r0.h
            r1 = 0
            java.lang.Boolean r1 = com.fossil.pb7.a(r1)
            com.fossil.r87 r8 = com.fossil.w87.a(r1, r8)
            r0.a(r8)
        L_0x00a7:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mr4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "friendsInChallenge - challengeId: " + str);
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void a(tt4 tt4, mn4 mn4) {
        this.f.a(new r87<>(tt4, mn4));
    }
}
