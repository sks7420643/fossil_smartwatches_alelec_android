package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ca0 {
    AUTO((byte) 0),
    CALL((byte) 1),
    TEXT((byte) 2),
    EMAIL((byte) 3),
    DEFAULT_OTHER_APPS((byte) 4),
    ONE_SHORT_VIBE((byte) 5),
    TWO_SHORT_VIBES((byte) 6),
    THREE_SHORT_VIBES((byte) 7),
    ONE_LONG_VIBE((byte) 8),
    NO_VIBE((byte) 9);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }
    }

    @DexIgnore
    public ca0(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
