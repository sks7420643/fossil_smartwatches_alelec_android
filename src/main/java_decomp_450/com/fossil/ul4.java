package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul4 extends RecyclerView.g<c> {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public List<Alarm> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Alarm alarm);

        @DexIgnore
        void b(Alarm alarm);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ g85 a;
        @DexIgnore
        public /* final */ /* synthetic */ ul4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.b((Alarm) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.a((Alarm) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ul4 ul4, g85 g85) {
            super(g85.d());
            ee7.b(g85, "binding");
            this.b = ul4;
            this.a = g85;
            g85.q.setOnClickListener(new a(this));
            this.a.u.setOnClickListener(new b(this));
            String b2 = eh5.l.a().b("nonBrandSurface");
            if (!TextUtils.isEmpty(b2)) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Alarm alarm, boolean z) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            FLogger.INSTANCE.getLocal().d(ul4.c, "Alarm: " + alarm + ", isSingleAlarm=" + z);
            int totalMinutes = alarm.getTotalMinutes();
            int hour = alarm.getHour();
            int minute = alarm.getMinute();
            View d = this.a.d();
            ee7.a((Object) d, "binding.root");
            if (DateFormat.is24HourFormat(d.getContext())) {
                FlexibleTextView flexibleTextView = this.a.s;
                ee7.a((Object) flexibleTextView, "binding.ftvTime");
                StringBuilder sb = new StringBuilder();
                we7 we7 = we7.a;
                Locale locale = Locale.US;
                ee7.a((Object) locale, "Locale.US");
                String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(hour)}, 1));
                ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                sb.append(format);
                sb.append(':');
                we7 we72 = we7.a;
                Locale locale2 = Locale.US;
                ee7.a((Object) locale2, "Locale.US");
                String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                sb.append(format2);
                flexibleTextView.setText(sb.toString());
            } else {
                int i = 12;
                if (totalMinutes < 720) {
                    if (hour == 0) {
                        hour = 12;
                    }
                    FlexibleTextView flexibleTextView2 = this.a.s;
                    ee7.a((Object) flexibleTextView2, "binding.ftvTime");
                    xe5 xe5 = xe5.b;
                    StringBuilder sb2 = new StringBuilder();
                    we7 we73 = we7.a;
                    Locale locale3 = Locale.US;
                    ee7.a((Object) locale3, "Locale.US");
                    String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(hour)}, 1));
                    ee7.a((Object) format3, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format3);
                    sb2.append(':');
                    we7 we74 = we7.a;
                    Locale locale4 = Locale.US;
                    ee7.a((Object) locale4, "Locale.US");
                    String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                    ee7.a((Object) format4, "java.lang.String.format(locale, format, *args)");
                    sb2.append(format4);
                    sb2.append(' ');
                    String sb3 = sb2.toString();
                    View d2 = this.a.d();
                    ee7.a((Object) d2, "binding.root");
                    String a2 = ig5.a(d2.getContext(), 2131886102);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    flexibleTextView2.setText(xe5.a(sb3, a2, 1.0f));
                } else {
                    if (hour > 12) {
                        i = hour - 12;
                    }
                    FlexibleTextView flexibleTextView3 = this.a.s;
                    ee7.a((Object) flexibleTextView3, "binding.ftvTime");
                    xe5 xe52 = xe5.b;
                    StringBuilder sb4 = new StringBuilder();
                    we7 we75 = we7.a;
                    Locale locale5 = Locale.US;
                    ee7.a((Object) locale5, "Locale.US");
                    String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                    ee7.a((Object) format5, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format5);
                    sb4.append(':');
                    we7 we76 = we7.a;
                    Locale locale6 = Locale.US;
                    ee7.a((Object) locale6, "Locale.US");
                    String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                    ee7.a((Object) format6, "java.lang.String.format(locale, format, *args)");
                    sb4.append(format6);
                    sb4.append(' ');
                    String sb5 = sb4.toString();
                    View d3 = this.a.d();
                    ee7.a((Object) d3, "binding.root");
                    String a3 = ig5.a(d3.getContext(), 2131886104);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                    flexibleTextView3.setText(xe52.a(sb5, a3, 1.0f));
                }
            }
            int[] days = alarm.getDays();
            int length = days != null ? days.length : 0;
            StringBuilder sb6 = new StringBuilder("");
            if (length > 0 && alarm.isRepeated()) {
                if (length == 7) {
                    sb6.append(ig5.a(PortfolioApp.g0.c(), 2131886108));
                    ee7.a((Object) sb6, "strDays.append(LanguageH\u2026_Alerts_Label__EveryDay))");
                } else {
                    if (length == 2) {
                        ul4 ul4 = this.b;
                        if (days == null) {
                            ee7.a();
                            throw null;
                        } else if (ul4.a(days)) {
                            sb6.append(ig5.a(PortfolioApp.g0.c(), 2131886109));
                            ee7.a((Object) sb6, "strDays.append(LanguageH\u2026rts_Label__EveryWeekend))");
                        }
                    }
                    if (days != null) {
                        s97.a(days);
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = 1;
                            while (true) {
                                if (i3 > 7) {
                                    break;
                                } else if (i3 == days[i2]) {
                                    sb6.append(xe5.b.a(i3));
                                    if (i2 < length - 1) {
                                        sb6.append(", ");
                                    }
                                } else {
                                    i3++;
                                }
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            FlexibleTextView flexibleTextView4 = this.a.t;
            ee7.a((Object) flexibleTextView4, "binding.ftvTitle");
            flexibleTextView4.setText(alarm.getTitle());
            FlexibleTextView flexibleTextView5 = this.a.r;
            ee7.a((Object) flexibleTextView5, "binding.ftvRepeatedDays");
            flexibleTextView5.setText(sb6.toString());
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
            ee7.a((Object) flexibleSwitchCompat, "binding.swEnabled");
            flexibleSwitchCompat.setChecked(alarm.isActive());
            if (z) {
                CardView cardView = this.a.q;
                ee7.a((Object) cardView, "binding.cvRoot");
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                oe5 b2 = oe5.b();
                ee7.a((Object) b2, "MeasureHelper.getInstance()");
                layoutParams.width = (int) (((float) b2.a()) - yx6.a(32.0f));
                return;
            }
            CardView cardView2 = this.a.q;
            ee7.a((Object) cardView2, "binding.cvRoot");
            ViewGroup.LayoutParams layoutParams2 = cardView2.getLayoutParams();
            oe5 b3 = oe5.b();
            ee7.a((Object) b3, "MeasureHelper.getInstance()");
            layoutParams2.width = (int) ((((float) b3.a()) - yx6.a(32.0f)) / 2.2f);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ul4.class.getSimpleName();
        ee7.a((Object) simpleName, "AlarmsAdapter::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        g85 a2 = g85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemAlarmBinding.inflate\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        Alarm alarm = this.a.get(i);
        boolean z = true;
        if (this.a.size() != 1) {
            z = false;
        }
        cVar.a(alarm, z);
    }

    @DexIgnore
    public final void a(List<Alarm> list) {
        ee7.b(list, "alarms");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        if (iArr.length != 2) {
            return false;
        }
        if ((iArr[0] == 1 && iArr[1] == 7) || (iArr[0] == 7 && iArr[1] == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
