package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.a14;
import com.fossil.e14;
import com.fossil.h14;
import com.fossil.iy3;
import com.fossil.zx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f14<T> extends c14<T> implements Serializable {
    @DexIgnore
    public /* final */ Type runtimeType;
    @DexIgnore
    public transient e14 typeResolver;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends a14.b<T> {
        @DexIgnore
        public a(Method method) {
            super(method);
        }

        @DexIgnore
        @Override // com.fossil.z04
        public f14<T> a() {
            return f14.this;
        }

        @DexIgnore
        @Override // com.fossil.z04
        public String toString() {
            return a() + CodelessMatcher.CURRENT_CLASS_NAME + super.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends a14.a<T> {
        @DexIgnore
        public b(Constructor constructor) {
            super(constructor);
        }

        @DexIgnore
        @Override // com.fossil.z04
        public f14<T> a() {
            return f14.this;
        }

        @DexIgnore
        @Override // com.fossil.a14.a
        public Type[] b() {
            return f14.this.resolveInPlace(super.b());
        }

        @DexIgnore
        @Override // com.fossil.z04
        public String toString() {
            return a() + "(" + ew3.c(", ").a((Object[]) b()) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends g14 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(TypeVariable<?> typeVariable) {
            throw new IllegalArgumentException(f14.this.runtimeType + "contains a type variable and is not safe for the operation");
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(WildcardType wildcardType) {
            a(wildcardType.getLowerBounds());
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(ParameterizedType parameterizedType) {
            a(parameterizedType.getActualTypeArguments());
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(GenericArrayType genericArrayType) {
            a(genericArrayType.getGenericComponentType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends g14 {
        @DexIgnore
        public /* final */ /* synthetic */ iy3.a b;

        @DexIgnore
        public d(f14 f14, iy3.a aVar) {
            this.b = aVar;
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(ParameterizedType parameterizedType) {
            this.b.a((Object) ((Class) parameterizedType.getRawType()));
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(Class<?> cls) {
            this.b.a((Object) cls);
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(GenericArrayType genericArrayType) {
            this.b.a((Object) h14.a((Class<?>) f14.of(genericArrayType.getGenericComponentType()).getRawType()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public /* final */ Type[] a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(Type[] typeArr, boolean z) {
            this.a = typeArr;
            this.b = z;
        }

        @DexIgnore
        public boolean a(Type type) {
            for (Type type2 : this.a) {
                boolean isSubtypeOf = f14.of(type2).isSubtypeOf(type);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }

        @DexIgnore
        public boolean b(Type type) {
            f14<?> of = f14.of(type);
            for (Type type2 : this.a) {
                boolean isSubtypeOf = of.isSubtypeOf(type2);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends f14<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient iy3<f14<? super T>> b;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        private Object readResolve() {
            return f14.this.getTypes().classes();
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public f14<T>.k classes() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public f14<T>.k interfaces() {
            throw new UnsupportedOperationException("classes().interfaces() not supported.");
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public Set<Class<? super T>> rawTypes() {
            return iy3.copyOf((Collection) i.b.a().a(f14.this.getRawTypes()));
        }

        @DexIgnore
        public /* synthetic */ f(f14 f14, a aVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.rx3, com.fossil.rx3, com.fossil.rx3, com.fossil.f14.k, com.fossil.f14.k, com.fossil.f14.k, com.fossil.mx3, com.fossil.mx3, com.fossil.px3
        public Set<f14<? super T>> delegate() {
            iy3<f14<? super T>> iy3 = this.b;
            if (iy3 != null) {
                return iy3;
            }
            iy3<f14<? super T>> b2 = lx3.a(i.a.a().a(f14.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).b();
            this.b = b2;
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends f14<T> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public h(Type type) {
            super(type, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i<K> {
        @DexIgnore
        public static /* final */ i<f14<?>> a; // = new a();
        @DexIgnore
        public static /* final */ i<Class<?>> b; // = new b();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a extends i<f14<?>> {
            @DexIgnore
            public a() {
                super(null);
            }

            @DexIgnore
            /* renamed from: a */
            public Iterable<? extends f14<?>> b(f14<?> f14) {
                return f14.getGenericInterfaces();
            }

            @DexIgnore
            /* renamed from: b */
            public Class<?> c(f14<?> f14) {
                return f14.getRawType();
            }

            @DexIgnore
            /* renamed from: c */
            public f14<?> d(f14<?> f14) {
                return f14.getGenericSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class b extends i<Class<?>> {
            @DexIgnore
            public b() {
                super(null);
            }

            @DexIgnore
            /* renamed from: a */
            public Iterable<? extends Class<?>> b(Class<?> cls) {
                return Arrays.asList(cls.getInterfaces());
            }

            @DexIgnore
            public Class<?> b(Class<?> cls) {
                return cls;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.f14.i
            public /* bridge */ /* synthetic */ Class c(Class<?> cls) {
                Class<?> cls2 = cls;
                b(cls2);
                return cls2;
            }

            @DexIgnore
            /* renamed from: c */
            public Class<?> d(Class<?> cls) {
                return cls.getSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends e<K> {
            @DexIgnore
            public c(i iVar, i iVar2) {
                super(iVar2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.f14$i$c */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.f14.i
            public zx3<K> a(Iterable<? extends K> iterable) {
                zx3.b builder = zx3.builder();
                for (Object obj : iterable) {
                    if (!c(obj).isInterface()) {
                        builder.a(obj);
                    }
                }
                return super.a((Iterable) builder.a());
            }

            @DexIgnore
            @Override // com.fossil.f14.i
            public Iterable<? extends K> b(K k) {
                return iy3.of();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class d extends jz3<K> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator a;
            @DexIgnore
            public /* final */ /* synthetic */ Map b;

            @DexIgnore
            public d(Comparator comparator, Map map) {
                this.a = comparator;
                this.b = map;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.Comparator */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.jz3, java.util.Comparator
            public int compare(K k, K k2) {
                return this.a.compare(this.b.get(k), this.b.get(k2));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class e<K> extends i<K> {
            @DexIgnore
            public /* final */ i<K> c;

            @DexIgnore
            public e(i<K> iVar) {
                super(null);
                this.c = iVar;
            }

            @DexIgnore
            @Override // com.fossil.f14.i
            public Class<?> c(K k) {
                return this.c.c(k);
            }

            @DexIgnore
            @Override // com.fossil.f14.i
            public K d(K k) {
                return this.c.d(k);
            }
        }

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public final i<K> a() {
            return new c(this, this);
        }

        @DexIgnore
        public abstract Iterable<? extends K> b(K k);

        @DexIgnore
        public abstract Class<?> c(K k);

        @DexIgnore
        public abstract K d(K k);

        @DexIgnore
        public /* synthetic */ i(a aVar) {
            this();
        }

        @DexIgnore
        public final zx3<K> a(K k) {
            return a((Iterable) zx3.of(k));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.f14$i<K> */
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public zx3<K> a(Iterable<? extends K> iterable) {
            HashMap b2 = yy3.b();
            Iterator<? extends K> it = iterable.iterator();
            while (it.hasNext()) {
                a(it.next(), b2);
            }
            return a(b2, jz3.natural().reverse());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: T */
        /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r0v12, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public final int a(K k, Map<? super K, Integer> map) {
            Integer num = map.get(k);
            if (num != null) {
                return num.intValue();
            }
            boolean isInterface = c(k).isInterface();
            int i = isInterface;
            for (T t : b(k)) {
                i = Math.max(i, a(t, map));
            }
            K d2 = d(k);
            int i2 = i;
            if (d2 != null) {
                int a2 = a(d2, map);
                int i3 = i == true ? 1 : 0;
                int i4 = i == true ? 1 : 0;
                i2 = Math.max(i3, a2);
            }
            int i5 = i2 + 1;
            map.put(k, Integer.valueOf(i5));
            return i5;
        }

        @DexIgnore
        public static <K, V> zx3<K> a(Map<K, V> map, Comparator<? super V> comparator) {
            return new d(comparator, map).immutableSortedCopy(map.keySet());
        }
    }

    @DexIgnore
    public enum j implements kw3<f14<?>> {
        IGNORE_TYPE_VARIABLE_OR_WILDCARD {
            @DexIgnore
            public boolean apply(f14<?> f14) {
                return !(f14.runtimeType instanceof TypeVariable) && !(f14.runtimeType instanceof WildcardType);
            }
        },
        INTERFACE_ONLY {
            @DexIgnore
            public boolean apply(f14<?> f14) {
                return f14.getRawType().isInterface();
            }
        };

        @DexIgnore
        public /* synthetic */ j(a aVar) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ f14(Type type, a aVar) {
        this(type);
    }

    @DexIgnore
    public static e any(Type[] typeArr) {
        return new e(typeArr, true);
    }

    @DexIgnore
    private f14<? super T> boundAsSuperclass(Type type) {
        f14<? super T> f14 = (f14<? super T>) of(type);
        if (f14.getRawType().isInterface()) {
            return null;
        }
        return f14;
    }

    @DexIgnore
    private zx3<f14<? super T>> boundsAsInterfaces(Type[] typeArr) {
        zx3.b builder = zx3.builder();
        for (Type type : typeArr) {
            f14<?> of = of(type);
            if (of.getRawType().isInterface()) {
                builder.a((Object) of);
            }
        }
        return builder.a();
    }

    @DexIgnore
    public static e every(Type[] typeArr) {
        return new e(typeArr, false);
    }

    @DexIgnore
    private f14<? extends T> getArraySubtype(Class<?> cls) {
        return (f14<? extends T>) of(newArrayClassOrGenericArrayType(getComponentType().getSubtype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.f14<?> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: com.fossil.f14<?> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r3v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<? super ?> */
    private f14<? super T> getArraySupertype(Class<? super T> cls) {
        f14<?> componentType = getComponentType();
        jw3.a(componentType, "%s isn't a super type of %s", cls, this);
        return (f14<? super T>) of(newArrayClassOrGenericArrayType(componentType.getSupertype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    private Type getOwnerTypeIfPresent() {
        Type type = this.runtimeType;
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getOwnerType();
        }
        if (type instanceof Class) {
            return ((Class) type).getEnclosingClass();
        }
        return null;
    }

    @DexIgnore
    private iy3<Class<? super T>> getRawTypes() {
        iy3.a builder = iy3.builder();
        new d(this, builder).a(this.runtimeType);
        return builder.a();
    }

    @DexIgnore
    private f14<? extends T> getSubtypeFromLowerBounds(Class<?> cls, Type[] typeArr) {
        if (typeArr.length > 0) {
            return (f14<? extends T>) of(typeArr[0]).getSubtype(cls);
        }
        throw new IllegalArgumentException(cls + " isn't a subclass of " + this);
    }

    @DexIgnore
    private f14<? super T> getSupertypeFromUpperBounds(Class<? super T> cls, Type[] typeArr) {
        for (Type type : typeArr) {
            f14<?> of = of(type);
            if (of.isSubtypeOf(cls)) {
                return (f14<? super T>) of.getSupertype(cls);
            }
        }
        throw new IllegalArgumentException(cls + " isn't a super type of " + this);
    }

    @DexIgnore
    private boolean is(Type type) {
        if (this.runtimeType.equals(type)) {
            return true;
        }
        if (!(type instanceof WildcardType)) {
            return false;
        }
        WildcardType wildcardType = (WildcardType) type;
        if (!every(wildcardType.getUpperBounds()).b(this.runtimeType) || !every(wildcardType.getLowerBounds()).a(this.runtimeType)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    private boolean isOwnedBySubtypeOf(Type type) {
        Iterator it = getTypes().iterator();
        while (it.hasNext()) {
            Type ownerTypeIfPresent = ((f14) it.next()).getOwnerTypeIfPresent();
            if (ownerTypeIfPresent != null && of(ownerTypeIfPresent).isSubtypeOf(type)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private boolean isSubtypeOfArrayType(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return false;
            }
            return of((Class) cls.getComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(((GenericArrayType) type).getGenericComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isSubtypeOfParameterizedType(ParameterizedType parameterizedType) {
        Class<? super Object> rawType = of(parameterizedType).getRawType();
        if (!someRawTypeIsSubclassOf(rawType)) {
            return false;
        }
        TypeVariable<Class<? super Object>>[] typeParameters = rawType.getTypeParameters();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i2 = 0; i2 < typeParameters.length; i2++) {
            if (!resolveType(typeParameters[i2]).is(actualTypeArguments[i2])) {
                return false;
            }
        }
        if (Modifier.isStatic(((Class) parameterizedType.getRawType()).getModifiers()) || parameterizedType.getOwnerType() == null || isOwnedBySubtypeOf(parameterizedType.getOwnerType())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    private boolean isSupertypeOfArray(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return cls.isAssignableFrom(Object[].class);
            }
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf(cls.getComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf(((GenericArrayType) this.runtimeType).getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isWrapper() {
        return y04.a().contains(this.runtimeType);
    }

    @DexIgnore
    public static Type newArrayClassOrGenericArrayType(Type type) {
        return h14.e.JAVA7.newArrayType(type);
    }

    @DexIgnore
    public static <T> f14<T> of(Class<T> cls) {
        return new h(cls);
    }

    @DexIgnore
    private Type[] resolveInPlace(Type[] typeArr) {
        for (int i2 = 0; i2 < typeArr.length; i2++) {
            typeArr[i2] = resolveType(typeArr[i2]).getType();
        }
        return typeArr;
    }

    @DexIgnore
    private f14<?> resolveSupertype(Type type) {
        f14<?> resolveType = resolveType(type);
        resolveType.typeResolver = this.typeResolver;
        return resolveType;
    }

    @DexIgnore
    private Type resolveTypeArgsForSubclass(Class<?> cls) {
        if ((this.runtimeType instanceof Class) && (cls.getTypeParameters().length == 0 || getRawType().getTypeParameters().length != 0)) {
            return cls;
        }
        f14 genericType = toGenericType(cls);
        return new e14().a(genericType.getSupertype(getRawType()).runtimeType, this.runtimeType).a(genericType.runtimeType);
    }

    @DexIgnore
    private boolean someRawTypeIsSubclassOf(Class<?> cls) {
        Iterator it = getRawTypes().iterator();
        while (it.hasNext()) {
            if (cls.isAssignableFrom((Class) it.next())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <T> f14<? extends T> toGenericType(Class<T> cls) {
        if (cls.isArray()) {
            return (f14<? extends T>) of(h14.b(toGenericType(cls.getComponentType()).runtimeType));
        }
        TypeVariable<Class<T>>[] typeParameters = cls.getTypeParameters();
        Type type = (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) ? null : toGenericType(cls.getEnclosingClass()).runtimeType;
        return (typeParameters.length > 0 || !(type == null || type == cls.getEnclosingClass())) ? (f14<? extends T>) of(h14.a(type, (Class<?>) cls, (Type[]) typeParameters)) : of((Class) cls);
    }

    @DexIgnore
    public final a14<T, T> constructor(Constructor<?> constructor) {
        jw3.a(constructor.getDeclaringClass() == getRawType(), "%s not declared by %s", constructor, getRawType());
        return new b(constructor);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof f14) {
            return this.runtimeType.equals(((f14) obj).runtimeType);
        }
        return false;
    }

    @DexIgnore
    public final f14<?> getComponentType() {
        Type a2 = h14.a(this.runtimeType);
        if (a2 == null) {
            return null;
        }
        return of(a2);
    }

    @DexIgnore
    public final zx3<f14<? super T>> getGenericInterfaces() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundsAsInterfaces(((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return boundsAsInterfaces(((WildcardType) type).getUpperBounds());
        }
        zx3.b builder = zx3.builder();
        for (Type type2 : getRawType().getGenericInterfaces()) {
            builder.a((Object) resolveSupertype(type2));
        }
        return builder.a();
    }

    @DexIgnore
    public final f14<? super T> getGenericSuperclass() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundAsSuperclass(((TypeVariable) type).getBounds()[0]);
        }
        if (type instanceof WildcardType) {
            return boundAsSuperclass(((WildcardType) type).getUpperBounds()[0]);
        }
        Type genericSuperclass = getRawType().getGenericSuperclass();
        if (genericSuperclass == null) {
            return null;
        }
        return (f14<? super T>) resolveSupertype(genericSuperclass);
    }

    @DexIgnore
    public final Class<? super T> getRawType() {
        return getRawTypes().iterator().next();
    }

    @DexIgnore
    public final f14<? extends T> getSubtype(Class<?> cls) {
        jw3.a(!(this.runtimeType instanceof TypeVariable), "Cannot get subtype of type variable <%s>", this);
        Type type = this.runtimeType;
        if (type instanceof WildcardType) {
            return getSubtypeFromLowerBounds(cls, ((WildcardType) type).getLowerBounds());
        }
        if (isArray()) {
            return getArraySubtype(cls);
        }
        jw3.a(getRawType().isAssignableFrom(cls), "%s isn't a subclass of %s", cls, this);
        return (f14<? extends T>) of(resolveTypeArgsForSubclass(cls));
    }

    @DexIgnore
    public final f14<? super T> getSupertype(Class<? super T> cls) {
        jw3.a(someRawTypeIsSubclassOf(cls), "%s is not a super class of %s", cls, this);
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return getSupertypeFromUpperBounds(cls, ((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return getSupertypeFromUpperBounds(cls, ((WildcardType) type).getUpperBounds());
        }
        return cls.isArray() ? getArraySupertype(cls) : (f14<? super T>) resolveSupertype(toGenericType(cls).runtimeType);
    }

    @DexIgnore
    public final Type getType() {
        return this.runtimeType;
    }

    @DexIgnore
    public final f14<T>.k getTypes() {
        return new k();
    }

    @DexIgnore
    public int hashCode() {
        return this.runtimeType.hashCode();
    }

    @DexIgnore
    public final boolean isArray() {
        return getComponentType() != null;
    }

    @DexIgnore
    public final boolean isPrimitive() {
        Type type = this.runtimeType;
        return (type instanceof Class) && ((Class) type).isPrimitive();
    }

    @DexIgnore
    public final boolean isSubtypeOf(f14<?> f14) {
        return isSubtypeOf(f14.getType());
    }

    @DexIgnore
    public final boolean isSupertypeOf(f14<?> f14) {
        return f14.isSubtypeOf(getType());
    }

    @DexIgnore
    public final a14<T, Object> method(Method method) {
        jw3.a(someRawTypeIsSubclassOf(method.getDeclaringClass()), "%s not declared by %s", method, this);
        return new a(method);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final f14<T> rejectTypeVariables() {
        new c().a(this.runtimeType);
        return this;
    }

    @DexIgnore
    public final f14<?> resolveType(Type type) {
        jw3.a(type);
        e14 e14 = this.typeResolver;
        if (e14 == null) {
            e14 = e14.b(this.runtimeType);
            this.typeResolver = e14;
        }
        return of(e14.a(type));
    }

    @DexIgnore
    public String toString() {
        return h14.e(this.runtimeType);
    }

    @DexIgnore
    public final f14<T> unwrap() {
        return isWrapper() ? of(y04.a((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public final <X> f14<T> where(d14<X> d14, f14<X> f14) {
        return new h(new e14().a(by3.of(new e14.d(d14.a), f14.runtimeType)).a(this.runtimeType));
    }

    @DexIgnore
    public final f14<T> wrap() {
        return isPrimitive() ? of(y04.b((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public Object writeReplace() {
        return of(new e14().a(this.runtimeType));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends f14<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ transient f14<T>.k b;
        @DexIgnore
        public transient iy3<f14<? super T>> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements kw3<Class<?>> {
            @DexIgnore
            public a(g gVar) {
            }

            @DexIgnore
            /* renamed from: a */
            public boolean apply(Class<?> cls) {
                return cls.isInterface();
            }
        }

        @DexIgnore
        public g(f14<T>.k kVar) {
            super();
            this.b = kVar;
        }

        @DexIgnore
        private Object readResolve() {
            return f14.this.getTypes().interfaces();
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public f14<T>.k classes() {
            throw new UnsupportedOperationException("interfaces().classes() not supported.");
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public f14<T>.k interfaces() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.f14.k
        public Set<Class<? super T>> rawTypes() {
            return lx3.a(i.b.a(f14.this.getRawTypes())).a(new a(this)).b();
        }

        @DexIgnore
        @Override // com.fossil.rx3, com.fossil.rx3, com.fossil.rx3, com.fossil.f14.k, com.fossil.f14.k, com.fossil.f14.k, com.fossil.mx3, com.fossil.mx3, com.fossil.px3
        public Set<f14<? super T>> delegate() {
            iy3<f14<? super T>> iy3 = this.c;
            if (iy3 != null) {
                return iy3;
            }
            iy3<f14<? super T>> b2 = lx3.a(this.b).a(j.INTERFACE_ONLY).b();
            this.c = b2;
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends rx3<f14<? super T>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient iy3<f14<? super T>> a;

        @DexIgnore
        public k() {
        }

        @DexIgnore
        public f14<T>.k classes() {
            return new f(f14.this, null);
        }

        @DexIgnore
        public f14<T>.k interfaces() {
            return new g(this);
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return iy3.copyOf((Collection) i.b.a(f14.this.getRawTypes()));
        }

        @DexIgnore
        @Override // com.fossil.rx3, com.fossil.rx3, com.fossil.rx3, com.fossil.mx3, com.fossil.mx3, com.fossil.px3
        public Set<f14<? super T>> delegate() {
            iy3<f14<? super T>> iy3 = this.a;
            if (iy3 != null) {
                return iy3;
            }
            iy3<f14<? super T>> b = lx3.a(i.a.a(f14.this)).a(j.IGNORE_TYPE_VARIABLE_OR_WILDCARD).b();
            this.a = b;
            return b;
        }
    }

    @DexIgnore
    public f14() {
        Type capture = capture();
        this.runtimeType = capture;
        jw3.b(!(capture instanceof TypeVariable), "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", capture);
    }

    @DexIgnore
    public static f14<?> of(Type type) {
        return new h(type);
    }

    @DexIgnore
    public final boolean isSubtypeOf(Type type) {
        jw3.a(type);
        if (type instanceof WildcardType) {
            return any(((WildcardType) type).getLowerBounds()).b(this.runtimeType);
        }
        Type type2 = this.runtimeType;
        if (type2 instanceof WildcardType) {
            return any(((WildcardType) type2).getUpperBounds()).a(type);
        }
        if (type2 instanceof TypeVariable) {
            if (type2.equals(type) || any(((TypeVariable) this.runtimeType).getBounds()).a(type)) {
                return true;
            }
            return false;
        } else if (type2 instanceof GenericArrayType) {
            return of(type).isSupertypeOfArray((GenericArrayType) this.runtimeType);
        } else {
            if (type instanceof Class) {
                return someRawTypeIsSubclassOf((Class) type);
            }
            if (type instanceof ParameterizedType) {
                return isSubtypeOfParameterizedType((ParameterizedType) type);
            }
            if (type instanceof GenericArrayType) {
                return isSubtypeOfArrayType((GenericArrayType) type);
            }
            return false;
        }
    }

    @DexIgnore
    public final boolean isSupertypeOf(Type type) {
        return of(type).isSubtypeOf(getType());
    }

    @DexIgnore
    public final <X> f14<T> where(d14<X> d14, Class<X> cls) {
        return where(d14, of((Class) cls));
    }

    @DexIgnore
    public f14(Class<?> cls) {
        Type capture = super.capture();
        if (capture instanceof Class) {
            this.runtimeType = capture;
        } else {
            this.runtimeType = of((Class) cls).resolveType(capture).runtimeType;
        }
    }

    @DexIgnore
    public f14(Type type) {
        jw3.a(type);
        this.runtimeType = type;
    }
}
