package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz6 extends he {
    @DexIgnore
    public /* final */ MutableLiveData<List<DebugFirmwareData>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> b; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1", f = "FirmwareDebugViewModel.kt", l = {16}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gd7 $block;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wz6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wz6 wz6, gd7 gd7, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wz6;
            this.$block = gd7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$block, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                gd7 gd7 = this.$block;
                this.L$0 = yi7;
                this.label = 1;
                obj = gd7.invoke(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a().a((List) obj);
            return i97.a;
        }
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> a() {
        return this.a;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean c() {
        if (this.a.a() != null) {
            List<DebugFirmwareData> a2 = this.a.a();
            if (a2 != null) {
                ee7.a((Object) a2, "firmwares.value!!");
                if (!a2.isEmpty()) {
                    return true;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.a;
        mutableLiveData.a(mutableLiveData.a());
        MutableLiveData<Firmware> mutableLiveData2 = this.b;
        mutableLiveData2.a(mutableLiveData2.a());
    }

    @DexIgnore
    public final ik7 a(gd7<? super fb7<? super List<DebugFirmwareData>>, ? extends Object> gd7) {
        ee7.b(gd7, "block");
        return xh7.b(zi7.a(qj7.a()), null, null, new a(this, gd7, null), 3, null);
    }

    @DexIgnore
    public final void a(Firmware firmware) {
        ee7.b(firmware, "firmware");
        this.b.a(firmware);
    }
}
