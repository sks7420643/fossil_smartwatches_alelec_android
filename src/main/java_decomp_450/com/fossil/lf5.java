package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lf5 extends Exception {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7323249827281485390L;

    @DexIgnore
    public lf5() {
    }

    @DexIgnore
    public lf5(String str) {
        super(str);
    }

    @DexIgnore
    public lf5(Throwable th) {
        super(th);
    }

    @DexIgnore
    public lf5(String str, Throwable th) {
        super(str, th);
    }
}
