package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h55 extends g55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362163, 1);
        P.put(2131362636, 2);
        P.put(2131361904, 3);
        P.put(2131362735, 4);
        P.put(2131362609, 5);
        P.put(2131362225, 6);
        P.put(2131362610, 7);
        P.put(2131362227, 8);
        P.put(2131363262, 9);
        P.put(2131362606, 10);
        P.put(2131363258, 11);
        P.put(2131362410, 12);
        P.put(2131362259, 13);
        P.put(2131362254, 14);
        P.put(2131362261, 15);
        P.put(2131362421, 16);
        P.put(2131362422, 17);
        P.put(2131363011, 18);
        P.put(2131362530, 19);
        P.put(2131362531, 20);
        P.put(2131363013, 21);
        P.put(2131363015, 22);
    }
    */

    @DexIgnore
    public h55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 23, O, P));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.N != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.N = 1;
        }
        g();
    }

    @DexIgnore
    public h55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FossilCircleImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[8], (FlexibleButton) objArr[14], (FlexibleButton) objArr[13], (FlexibleButton) objArr[15], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[20], (FlexibleTextInputLayout) objArr[10], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[7], (RTLImageView) objArr[2], (FossilCircleImageView) objArr[4], (ConstraintLayout) objArr[0], (RulerValuePicker) objArr[18], (RulerValuePicker) objArr[21], (ProgressButton) objArr[22], (FlexibleTextInputEditText) objArr[11], (FlexibleTextView) objArr[9]);
        this.N = -1;
        ((g55) this).H.setTag(null);
        a(view);
        f();
    }
}
