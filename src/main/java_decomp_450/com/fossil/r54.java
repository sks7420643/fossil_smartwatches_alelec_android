package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r54 extends v54.d.AbstractC0206d.c {
    @DexIgnore
    public /* final */ Double a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.c.a {
        @DexIgnore
        public Double a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a a(Double d2) {
            this.a = d2;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a b(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a b(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a a(boolean z) {
            this.c = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c.a a(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.c.a
        public v54.d.AbstractC0206d.c a() {
            String str = "";
            if (this.b == null) {
                str = str + " batteryVelocity";
            }
            if (this.c == null) {
                str = str + " proximityOn";
            }
            if (this.d == null) {
                str = str + " orientation";
            }
            if (this.e == null) {
                str = str + " ramUsed";
            }
            if (this.f == null) {
                str = str + " diskUsed";
            }
            if (str.isEmpty()) {
                return new r54(this.a, this.b.intValue(), this.c.booleanValue(), this.d.intValue(), this.e.longValue(), this.f.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public Double a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public int b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public long c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public int d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public long e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.c)) {
            return false;
        }
        v54.d.AbstractC0206d.c cVar = (v54.d.AbstractC0206d.c) obj;
        Double d2 = this.a;
        if (d2 != null ? d2.equals(cVar.a()) : cVar.a() == null) {
            if (this.b == cVar.b() && this.c == cVar.f() && this.d == cVar.d() && this.e == cVar.e() && this.f == cVar.c()) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.c
    public boolean f() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        Double d2 = this.a;
        int hashCode = ((((d2 == null ? 0 : d2.hashCode()) ^ 1000003) * 1000003) ^ this.b) * 1000003;
        int i = this.c ? 1231 : 1237;
        long j = this.e;
        long j2 = this.f;
        return ((((((hashCode ^ i) * 1000003) ^ this.d) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Device{batteryLevel=" + this.a + ", batteryVelocity=" + this.b + ", proximityOn=" + this.c + ", orientation=" + this.d + ", ramUsed=" + this.e + ", diskUsed=" + this.f + "}";
    }

    @DexIgnore
    public r54(Double d2, int i, boolean z, int i2, long j, long j2) {
        this.a = d2;
        this.b = i;
        this.c = z;
        this.d = i2;
        this.e = j;
        this.f = j2;
    }
}
