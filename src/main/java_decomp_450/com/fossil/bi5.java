package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Firmware;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi5 extends BaseDbProvider implements ai5 {
    @DexIgnore
    public static /* final */ String a; // = "bi5";

    @DexIgnore
    public bi5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.ai5
    public Firmware a(String str) {
        try {
            Where<Firmware, Integer> where = e().queryBuilder().where();
            where.eq("deviceModel", str);
            List<Firmware> query = where.query();
            if (query == null || query.isEmpty()) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local.e(str2, "Error inside " + a + ".getLatestFirmware with model " + str + ") - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final Dao<Firmware, Integer> e() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(Firmware.class);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{Firmware.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.ai5
    public void a(Firmware firmware) {
        try {
            e().createOrUpdate(firmware);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "Error inside " + a + ".addOrUpdatePhoto - e=" + e);
        }
    }
}
