package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d84 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ k44 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore
    public d84(String str, String str2, String str3, String str4, k44 k44, String str5, String str6, String str7, int i2) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = k44;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i2;
    }
}
