package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ oe0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<xe0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public xe0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(sb0.class.getClassLoader());
            if (readParcelable != null) {
                return new xe0((sb0) readParcelable, (df0) parcel.readParcelable(df0.class.getClassLoader()), (oe0[]) parcel.createTypedArray(oe0.CREATOR));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public xe0[] newArray(int i) {
            return new xe0[i];
        }
    }

    @DexIgnore
    public xe0(sb0 sb0, df0 df0, oe0[] oe0Arr) {
        super(sb0, df0);
        this.c = oe0Arr;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.c != null) {
                JSONArray jSONArray = new JSONArray();
                for (oe0 oe0 : this.c) {
                    jSONArray.put(oe0.b());
                }
                jSONObject.put("buddyChallengeApp._.config.challenge_list", jSONArray);
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        JSONObject b = super.b();
        r51 r51 = r51.d5;
        oe0[] oe0Arr = this.c;
        return yz0.a(b, r51, oe0Arr != null ? yz0.a(oe0Arr) : JSONObject.NULL);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        oe0[] oe0Arr;
        if (this == obj) {
            return true;
        }
        if (!ee7.a(xe0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            xe0 xe0 = (xe0) obj;
            oe0[] oe0Arr2 = this.c;
            if (oe0Arr2 == null || (oe0Arr = xe0.c) == null) {
                if (this.c == null || xe0.c == null) {
                    return false;
                }
            } else if (!Arrays.equals(oe0Arr2, oe0Arr)) {
                return false;
            }
            return true;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppListChallengesData");
    }

    @DexIgnore
    public final oe0[] getBuddyChallenges() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        oe0[] oe0Arr = this.c;
        return hashCode + (oe0Arr != null ? oe0Arr.hashCode() : 0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }

    @DexIgnore
    public xe0(sb0 sb0, oe0[] oe0Arr) {
        super(sb0, null);
        this.c = oe0Arr;
    }
}
