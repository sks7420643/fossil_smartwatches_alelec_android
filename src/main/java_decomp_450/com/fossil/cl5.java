package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.cy6;
import com.fossil.fl4;
import com.fossil.ku7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.xm5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.MFDeviceService;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class cl5 extends AppCompatActivity implements cy6.g, ku7.a {
    @DexIgnore
    public static IButtonConnectivity w;
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Handler d; // = new Handler();
    @DexIgnore
    public View e;
    @DexIgnore
    public TextView f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public UserRepository h;
    @DexIgnore
    public ch5 i;
    @DexIgnore
    public DeviceRepository j;
    @DexIgnore
    public ll4 p;
    @DexIgnore
    public xm5 q;
    @DexIgnore
    public String r;
    @DexIgnore
    public my6 s;
    @DexIgnore
    public /* final */ d t; // = new d(this);
    @DexIgnore
    public /* final */ e u; // = new e(this);
    @DexIgnore
    public /* final */ Runnable v; // = new b(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final IButtonConnectivity a() {
            return cl5.w;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            cl5.w = iButtonConnectivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements fl4.e<xm5.e, xm5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(xm5.e eVar) {
                ee7.b(eVar, "responseValue");
                this.a.a.b(String.valueOf(eVar.a()));
            }

            @DexIgnore
            public void a(xm5.b bVar) {
                ee7.b(bVar, "errorValue");
                this.a.a.b("Disconnected");
            }
        }

        @DexIgnore
        public b(cl5 cl5) {
            this.a = cl5;
        }

        @DexIgnore
        public final void run() {
            if (!TextUtils.isEmpty(this.a.r)) {
                xm5 c = this.a.c();
                String a2 = this.a.r;
                if (a2 != null) {
                    c.a(new xm5.d(a2), new a(this));
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;

        @DexIgnore
        public c(cl5 cl5) {
            this.a = cl5;
        }

        @DexIgnore
        public final void run() {
            Fragment b;
            try {
                if (this.a.s == null && (b = this.a.getSupportFragmentManager().b("ProgressDialogFragment")) != null) {
                    this.a.s = (my6) b;
                }
                if (this.a.s != null) {
                    FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog dismissAllowingStateLoss");
                    my6 b2 = this.a.s;
                    if (b2 != null) {
                        b2.dismissAllowingStateLoss();
                        this.a.s = (my6) null;
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e2 = this.a.e();
                local.d(e2, "Exception when dismiss progress dialog=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.BaseActivity$mButtonServiceConnection$1$onServiceConnected$1", f = "BaseActivity.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 142}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:25:0x006a  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0072  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r6.label
                    r2 = 0
                    r3 = 2
                    r4 = 1
                    if (r1 == 0) goto L_0x0029
                    if (r1 == r4) goto L_0x0021
                    if (r1 != r3) goto L_0x0019
                    java.lang.Object r0 = r6.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r7)     // Catch:{ Exception -> 0x0017 }
                    goto L_0x0056
                L_0x0017:
                    r7 = move-exception
                    goto L_0x007e
                L_0x0019:
                    java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r7.<init>(r0)
                    throw r7
                L_0x0021:
                    java.lang.Object r1 = r6.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r7)
                    goto L_0x0043
                L_0x0029:
                    com.fossil.t87.a(r7)
                    com.fossil.yi7 r1 = r6.p$
                    com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                    com.fossil.cl5$a r5 = com.fossil.cl5.x
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r5 = r5.a()
                    if (r5 == 0) goto L_0x00a6
                    r6.L$0 = r1
                    r6.label = r4
                    java.lang.Object r7 = r7.a(r5, r6)
                    if (r7 != r0) goto L_0x0043
                    return r0
                L_0x0043:
                    com.fossil.cl5$d r7 = r6.this$0
                    com.fossil.cl5 r7 = r7.a
                    com.portfolio.platform.data.source.UserRepository r7 = r7.d()
                    r6.L$0 = r1
                    r6.label = r3
                    java.lang.Object r7 = r7.getCurrentUser(r6)
                    if (r7 != r0) goto L_0x0056
                    return r0
                L_0x0056:
                    com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
                    if (r7 == 0) goto L_0x0076
                    com.fossil.cl5$a r0 = com.fossil.cl5.x
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = r0.a()
                    if (r0 == 0) goto L_0x0076
                    com.fossil.cl5$a r0 = com.fossil.cl5.x
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r0 = r0.a()
                    if (r0 == 0) goto L_0x0072
                    java.lang.String r7 = r7.getUserId()
                    r0.updateUserId(r7)
                    goto L_0x0076
                L_0x0072:
                    com.fossil.ee7.a()
                    throw r2
                L_0x0076:
                    com.fossil.cl5$d r7 = r6.this$0
                    com.fossil.cl5 r7 = r7.a
                    r7.m()
                    goto L_0x00a3
                L_0x007e:
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                    com.fossil.cl5$d r1 = r6.this$0
                    com.fossil.cl5 r1 = r1.a
                    java.lang.String r1 = r1.e()
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = ".onServiceConnected(), ex="
                    r2.append(r3)
                    r2.append(r7)
                    java.lang.String r2 = r2.toString()
                    r0.e(r1, r2)
                    r7.printStackTrace()
                L_0x00a3:
                    com.fossil.i97 r7 = com.fossil.i97.a
                    return r7
                L_0x00a6:
                    com.fossil.ee7.a()
                    throw r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cl5.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(cl5 cl5) {
            this.a = cl5;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ee7.b(componentName, "name");
            ee7.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.e(), "Button service connected");
            cl5.x.a(IButtonConnectivity.Stub.asInterface(iBinder));
            this.a.b(true);
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            ee7.b(componentName, "name");
            FLogger.INSTANCE.getLocal().d(this.a.e(), "Button service disconnected");
            this.a.b(false);
            cl5.x.a(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(cl5 cl5) {
            this.a = cl5;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ee7.b(componentName, "name");
            ee7.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(this.a.e(), "Misfit service connected");
            MFDeviceService.b bVar = (MFDeviceService.b) iBinder;
            this.a.a(bVar.a());
            PortfolioApp.g0.b(bVar);
            this.a.c(true);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            ee7.b(componentName, "name");
            this.a.c(false);
            this.a.a((MFDeviceService) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;

        @DexIgnore
        public f(cl5 cl5) {
            this.a = cl5;
        }

        @DexIgnore
        public final void run() {
            FragmentManager supportFragmentManager = this.a.getSupportFragmentManager();
            ee7.a((Object) supportFragmentManager, "supportFragmentManager");
            if (supportFragmentManager.t() > 1) {
                this.a.getSupportFragmentManager().E();
            } else {
                this.a.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cl5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public g(cl5 cl5, String str, boolean z) {
            this.a = cl5;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        public final void run() {
            try {
                if (!this.a.isDestroyed()) {
                    if (!this.a.isFinishing()) {
                        this.a.s = my6.c.a(this.b);
                        my6 b2 = this.a.s;
                        if (b2 != null) {
                            b2.setCancelable(this.c);
                            nc b3 = this.a.getSupportFragmentManager().b();
                            ee7.a((Object) b3, "supportFragmentManager.beginTransaction()");
                            my6 b4 = this.a.s;
                            if (b4 != null) {
                                b3.a(b4, "ProgressDialogFragment");
                                b3.b();
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a.e(), "Activity is destroy or finishing, no need to show dialog");
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e2 = this.a.e();
                local.d(e2, "Exception when showing progress dialog=" + e);
            }
        }
    }

    @DexIgnore
    public cl5() {
        String simpleName = getClass().getSimpleName();
        ee7.a((Object) simpleName, "this.javaClass.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    @Override // com.fossil.ku7.a
    public void a(int i2, List<String> list) {
        ee7.b(list, "perms");
    }

    @DexIgnore
    public final void a(MFDeviceService mFDeviceService) {
    }

    @DexIgnore
    @Override // com.fossil.ku7.a
    public void b(int i2, List<String> list) {
        ee7.b(list, "perms");
    }

    @DexIgnore
    public final void c(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final UserRepository d() {
        UserRepository userRepository = this.h;
        if (userRepository != null) {
            return userRepository;
        }
        ee7.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public final int f() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            ee7.a((Object) method, "currentClass.getMethod(\"getThemeResId\")");
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            if (invoke != null) {
                return ((Integer) invoke).intValue();
            }
            throw new x87("null cannot be cast to non-null type kotlin.Int");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(this.a, "Failed to get theme resource ID");
            return 0;
        }
    }

    @DexIgnore
    public void finish() {
        super.finish();
        if (h()) {
            overridePendingTransition(2130772013, 2130772016);
        }
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d("ProgressDialogFragment", "hideLoadingDialog");
        this.d.post(new c(this));
    }

    @DexIgnore
    public final boolean h() {
        return f() == 2131951631;
    }

    @DexIgnore
    public final void i() {
        try {
            m6.c(this);
        } catch (IllegalArgumentException unused) {
            finish();
        }
    }

    @DexIgnore
    public final void j() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
    }

    @DexIgnore
    public final void k() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public final void l() {
        if (Build.VERSION.SDK_INT >= 29) {
            startActivity(new Intent("android.settings.panel.action.INTERNET_CONNECTIVITY"));
        } else {
            startActivity(new Intent("android.settings.SETTINGS"));
        }
    }

    @DexIgnore
    public final synchronized void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("needToUpdateBLEWhenUpgradeLegacy - isNeedToUpdateBLE=");
        ch5 ch5 = this.i;
        if (ch5 != null) {
            sb.append(ch5.R());
            local.d(str, sb.toString());
            ch5 ch52 = this.i;
            if (ch52 == null) {
                ee7.d("mSharePrefs");
                throw null;
            } else if (ch52.R()) {
                try {
                    ll4 ll4 = this.p;
                    if (ll4 != null) {
                        ll4.b();
                    } else {
                        ee7.d("mMigrationManager");
                        throw null;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = this.a;
                    local2.e(str2, "needToUpdateBLEWhenUpgradeLegacy - e=" + e2);
                }
            }
        } else {
            ee7.d("mSharePrefs");
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        View view = this.e;
        if (view != null && this.g) {
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.e);
                    this.g = false;
                    this.d.removeCallbacks(this.v);
                } else {
                    throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        try {
            xm5 xm5 = this.q;
            if (xm5 != null) {
                xm5.g();
            } else {
                ee7.d("mGetRssi");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void o() {
        a(this, false, null, 2, null);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity
    public void onBackPressed() {
        runOnUiThread(new f(this));
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onCreate(Bundle bundle) {
        PortfolioApp.g0.c().f().a(this);
        super.onCreate(bundle);
        if (h()) {
            overridePendingTransition(2130772014, 2130772015);
        }
        dh5.o.a().b();
        Window window = getWindow();
        ee7.a((Object) window, "window");
        View decorView = window.getDecorView();
        ee7.a((Object) decorView, "window.decorView");
        decorView.setSystemUiVisibility(3328);
        qd5.f.c();
    }

    @DexIgnore
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ee7.b(menuItem, "item");
        if (menuItem.getItemId() == 16908332) {
            i();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onPause() {
        super.onPause();
        try {
            PortfolioApp.g0.c(this);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.e(str, "Inside " + this.a + ".onPause - exception=" + e2);
        }
        if (!PortfolioApp.g0.c().G()) {
            ch5 ch5 = this.i;
            if (ch5 == null) {
                ee7.d("mSharePrefs");
                throw null;
            } else if (ch5.O()) {
                n();
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onResume() {
        super.onResume();
        PortfolioApp.g0.b(this);
        a(false);
        if (PortfolioApp.g0.f()) {
            dh5.o.a().b();
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStart() {
        super.onStart();
        FLogger.INSTANCE.getLocal().d(this.a, "onStart()");
    }

    @DexIgnore
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d(this.a, "onStop()");
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void startActivityForResult(Intent intent, int i2) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, i2);
    }

    @DexIgnore
    public final void b(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final xm5 c() {
        xm5 xm5 = this.q;
        if (xm5 != null) {
            return xm5;
        }
        ee7.d("mGetRssi");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository b() {
        DeviceRepository deviceRepository = this.j;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        ee7.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public <T extends Service> void a(Class<? extends T>... clsArr) {
        ee7.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.a, "Service Tracking - startAndBindService " + clsArr);
        for (Class<? extends T> cls : clsArr) {
            if (ee7.a(cls, MFDeviceService.class)) {
                rx6.a.b(this, MFDeviceService.class, this.u, 0);
            } else if (ee7.a(cls, ButtonService.class)) {
                rx6.a.b(this, ButtonService.class, this.t, 1);
            }
        }
    }

    @DexIgnore
    public final <T extends Service> void b(Class<? extends T>... clsArr) {
        ee7.b(clsArr, "services");
        FLogger.INSTANCE.getLocal().d(this.a, "unbindServices()");
        for (Class<? extends T> cls : clsArr) {
            if (ee7.a(cls, MFDeviceService.class)) {
                if (this.b) {
                    FLogger.INSTANCE.getLocal().d(this.a, "Unbinding from mIsMisfitServiceBound");
                    rx6.a.a(this, this.u);
                    this.b = false;
                }
            } else if (ee7.a(cls, ButtonService.class) && this.c) {
                FLogger.INSTANCE.getLocal().d(this.a, "Unbinding from mButtonServiceBound");
                rx6.a.a(this, this.t);
                this.c = false;
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "title");
        a(false, str);
    }

    @DexIgnore
    public static /* synthetic */ void a(cl5 cl5, boolean z, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                str = "";
            }
            cl5.a(z, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showLoadingDialog");
    }

    @DexIgnore
    public final void a(boolean z, String str) {
        ee7.b(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "showLoadingDialog: cancelable = " + z);
        g();
        this.d.post(new g(this, str, z));
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "Inside .onDialogFragmentResult tag=" + str);
        if (str.hashCode() == 2009556792 && str.equals(InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131362413) {
            j();
        }
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "rssiInfo");
        TextView textView = this.f;
        if (textView != null) {
            textView.setText(str);
            this.d.postDelayed(this.v, 1000);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @TargetApi(23)
    public final void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 23) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            ee7.a((Object) window, "window");
            View decorView = window.getDecorView();
            ee7.a((Object) decorView, "window.decorView");
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (z) {
                decorView.setSystemUiVisibility(systemUiVisibility & -8193);
            } else {
                decorView.setSystemUiVisibility(systemUiVisibility | 8192);
            }
        }
    }

    @DexIgnore
    public final void a(Fragment fragment, int i2) {
        ee7.b(fragment, "fragment");
        a(fragment, (String) null, i2);
    }

    @DexIgnore
    public final void a(Fragment fragment, String str) {
        ee7.b(fragment, "fragment");
        ee7.b(str, "tag");
        a(fragment, str, 2131362149);
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i2) {
        ee7.b(fragment, "fragment");
        nc b2 = getSupportFragmentManager().b();
        b2.b(i2, fragment, str);
        b2.a(str);
        b2.b();
    }
}
