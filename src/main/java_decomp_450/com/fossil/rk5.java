package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rk5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ rk5 b; // = new rk5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<ActivitySample> b;
        @DexIgnore
        public /* final */ List<ActivitySummary> c;
        @DexIgnore
        public /* final */ List<MFSleepSession> d;
        @DexIgnore
        public /* final */ List<HeartRateSample> e;
        @DexIgnore
        public /* final */ List<DailyHeartRateSummary> f;
        @DexIgnore
        public /* final */ List<r87<Long, Long>> g;
        @DexIgnore
        public /* final */ List<WorkoutSession> h;
        @DexIgnore
        public /* final */ List<GFitSample> i;
        @DexIgnore
        public /* final */ List<GFitHeartRate> j;
        @DexIgnore
        public /* final */ List<GFitWorkoutSession> k;

        @DexIgnore
        public a(long j2, List<ActivitySample> list, List<ActivitySummary> list2, List<MFSleepSession> list3, List<HeartRateSample> list4, List<DailyHeartRateSummary> list5, List<r87<Long, Long>> list6, List<WorkoutSession> list7, List<GFitSample> list8, List<GFitHeartRate> list9, List<GFitWorkoutSession> list10) {
            ee7.b(list, "sampleRawList");
            ee7.b(list2, "summaryList");
            ee7.b(list3, "sleepSessionList");
            ee7.b(list4, "heartRateDataList");
            ee7.b(list5, "heartRateSummaryList");
            ee7.b(list6, "activeTimeList");
            ee7.b(list7, "workoutSessionList");
            ee7.b(list8, "gFitSampleList");
            ee7.b(list9, "gFitHeartRateList");
            ee7.b(list10, "gFitWorkoutSessionList");
            this.a = j2;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
            this.g = list6;
            this.h = list7;
            this.i = list8;
            this.j = list9;
            this.k = list10;
        }

        @DexIgnore
        public final List<r87<Long, Long>> a() {
            return this.g;
        }

        @DexIgnore
        public final List<GFitHeartRate> b() {
            return this.j;
        }

        @DexIgnore
        public final List<GFitSample> c() {
            return this.i;
        }

        @DexIgnore
        public final List<GFitWorkoutSession> d() {
            return this.k;
        }

        @DexIgnore
        public final List<HeartRateSample> e() {
            return this.e;
        }

        @DexIgnore
        public final List<DailyHeartRateSummary> f() {
            return this.f;
        }

        @DexIgnore
        public final long g() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> h() {
            return this.b;
        }

        @DexIgnore
        public final List<MFSleepSession> i() {
            return this.d;
        }

        @DexIgnore
        public final List<ActivitySummary> j() {
            return this.c;
        }

        @DexIgnore
        public final List<WorkoutSession> k() {
            return this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Integer invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Integer.valueOf(invoke(fitnessDataWrapper));
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            ee7.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements gd7<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Long invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Long.valueOf(invoke(fitnessDataWrapper));
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            ee7.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing", f = "DianaSyncDataProcessing.kt", l = {153, 164, 187, 195, 205, 213, 221, 228, 234}, m = "saveSyncResult")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$18;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(rk5 rk5, fb7 fb7) {
            super(fb7);
            this.this$0 = rk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, null, null, null, null, null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2", f = "DianaSyncDataProcessing.kt", l = {176}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1", f = "DianaSyncDataProcessing.kt", l = {177, 178}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ se7 $gFitActiveTime;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitHeartRate;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitSample;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitWorkoutSession;
            @DexIgnore
            public /* final */ /* synthetic */ List $listMFSleepSession;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, List list, se7 se7, List list2, List list3, List list4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$listGFitSample = list;
                this.$gFitActiveTime = se7;
                this.$listGFitHeartRate = list2;
                this.$listGFitWorkoutSession = list3;
                this.$listMFSleepSession = list4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$listGFitSample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    List<GFitHeartRate> list = this.$listGFitHeartRate;
                    List<GFitWorkoutSession> list2 = this.$listGFitWorkoutSession;
                    List<MFSleepSession> list3 = this.$listMFSleepSession;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$gFitActiveTime.element, list, list2, list3, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ThirdPartyRepository thirdPartyRepository = this.this$0.$thirdPartyRepository;
                this.L$0 = yi7;
                this.label = 2;
                obj = ThirdPartyRepository.uploadData$default(thirdPartyRepository, null, this, 1, null);
                return obj == a ? a : obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(a aVar, ThirdPartyRepository thirdPartyRepository, fb7 fb7) {
            super(2, fb7);
            this.$finalResult = aVar;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.$finalResult, this.$thirdPartyRepository, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                List<GFitSample> c = this.$finalResult.c();
                se7 se7 = new se7();
                se7.element = null;
                List<r87<Long, Long>> a3 = this.$finalResult.a();
                ArrayList arrayList = new ArrayList(x97.a(a3, 10));
                for (T t : a3) {
                    arrayList.add(w97.c((Long) t.getFirst(), (Long) t.getSecond()));
                }
                List n = ea7.n(x97.a((Iterable) arrayList));
                if (!n.isEmpty()) {
                    se7.element = (T) new GFitActiveTime(n);
                }
                List<MFSleepSession> i2 = this.$finalResult.i();
                List<GFitHeartRate> b = this.$finalResult.b();
                List<GFitWorkoutSession> d = this.$finalResult.d();
                ti7 b2 = qj7.b();
                a aVar = new a(this, c, se7, b, d, i2, null);
                this.L$0 = yi7;
                this.L$1 = c;
                this.L$2 = se7;
                this.L$3 = n;
                this.L$4 = i2;
                this.L$5 = b;
                this.L$6 = d;
                this.label = 1;
                Object a4 = vh7.a(b2, aVar, this);
                return a4 == a2 ? a2 : a4;
            } else if (i == 1) {
                List list = (List) this.L$6;
                List list2 = (List) this.L$5;
                List list3 = (List) this.L$4;
                List list4 = (List) this.L$3;
                se7 se72 = (se7) this.L$2;
                List list5 = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3", f = "DianaSyncDataProcessing.kt", l = {246}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository $heartRateSampleRepository;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository $heartRateSummaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1", f = "DianaSyncDataProcessing.kt", l = {247, 248, 250, 251, 253, 254, 256, 257}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ se7 $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ se7 $startDate;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, se7 se7, se7 se72, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$startDate = se7;
                this.$endDate = se72;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$startDate, this.$endDate, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00d3 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x00f7 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0121 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0145 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x016f A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:35:0x0180 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x0181 A[PHI: r12 
              PHI: (r12v1 java.lang.Object) = (r12v4 java.lang.Object), (r12v0 java.lang.Object) binds: [B:34:0x017e, B:4:0x0012] A[DONT_GENERATE, DONT_INLINE], RETURN] */
            @DexIgnore
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r12) {
                /*
                    r11 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r11.label
                    r2 = 1
                    switch(r1) {
                        case 0: goto L_0x0058;
                        case 1: goto L_0x0050;
                        case 2: goto L_0x0048;
                        case 3: goto L_0x003f;
                        case 4: goto L_0x0036;
                        case 5: goto L_0x002d;
                        case 6: goto L_0x0024;
                        case 7: goto L_0x001b;
                        case 8: goto L_0x0012;
                        default: goto L_0x000a;
                    }
                L_0x000a:
                    java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r12.<init>(r0)
                    throw r12
                L_0x0012:
                    java.lang.Object r0 = r11.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r12)
                    goto L_0x0181
                L_0x001b:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x0170
                L_0x0024:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x0146
                L_0x002d:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x0122
                L_0x0036:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x00f8
                L_0x003f:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x00d4
                L_0x0048:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x00aa
                L_0x0050:
                    java.lang.Object r1 = r11.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r12)
                    goto L_0x0086
                L_0x0058:
                    com.fossil.t87.a(r12)
                    com.fossil.yi7 r1 = r11.p$
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.ActivitiesRepository r3 = r12.$activityRepository
                    com.fossil.se7 r12 = r11.$startDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r4 = com.fossil.tx6.a(r12, r2)
                    com.fossil.se7 r12 = r11.$endDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r5 = com.fossil.tx6.a(r12, r2)
                    r6 = 0
                    r7 = 0
                    r9 = 12
                    r10 = 0
                    r11.L$0 = r1
                    r11.label = r2
                    r8 = r11
                    java.lang.Object r12 = com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(r3, r4, r5, r6, r7, r8, r9, r10)
                    if (r12 != r0) goto L_0x0086
                    return r0
                L_0x0086:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.SummariesRepository r12 = r12.$summaryRepository
                    com.fossil.se7 r3 = r11.$startDate
                    T r3 = r3.element
                    org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                    java.util.Date r3 = com.fossil.tx6.a(r3, r2)
                    com.fossil.se7 r4 = r11.$endDate
                    T r4 = r4.element
                    org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                    java.util.Date r4 = com.fossil.tx6.a(r4, r2)
                    r11.L$0 = r1
                    r5 = 2
                    r11.label = r5
                    java.lang.Object r12 = r12.loadSummaries(r3, r4, r11)
                    if (r12 != r0) goto L_0x00aa
                    return r0
                L_0x00aa:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.SleepSessionsRepository r3 = r12.$sleepSessionsRepository
                    com.fossil.se7 r12 = r11.$startDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r4 = com.fossil.tx6.a(r12, r2)
                    com.fossil.se7 r12 = r11.$endDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r5 = com.fossil.tx6.a(r12, r2)
                    r6 = 0
                    r7 = 0
                    r9 = 12
                    r10 = 0
                    r11.L$0 = r1
                    r12 = 3
                    r11.label = r12
                    r8 = r11
                    java.lang.Object r12 = com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r3, r4, r5, r6, r7, r8, r9, r10)
                    if (r12 != r0) goto L_0x00d4
                    return r0
                L_0x00d4:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.SleepSummariesRepository r12 = r12.$sleepSummariesRepository
                    com.fossil.se7 r3 = r11.$startDate
                    T r3 = r3.element
                    org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                    java.util.Date r3 = com.fossil.tx6.a(r3, r2)
                    com.fossil.se7 r4 = r11.$endDate
                    T r4 = r4.element
                    org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                    java.util.Date r4 = com.fossil.tx6.a(r4, r2)
                    r11.L$0 = r1
                    r5 = 4
                    r11.label = r5
                    java.lang.Object r12 = r12.fetchSleepSummaries(r3, r4, r11)
                    if (r12 != r0) goto L_0x00f8
                    return r0
                L_0x00f8:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository r3 = r12.$heartRateSampleRepository
                    com.fossil.se7 r12 = r11.$startDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r4 = com.fossil.tx6.a(r12, r2)
                    com.fossil.se7 r12 = r11.$endDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r5 = com.fossil.tx6.a(r12, r2)
                    r6 = 0
                    r7 = 0
                    r9 = 12
                    r10 = 0
                    r11.L$0 = r1
                    r12 = 5
                    r11.label = r12
                    r8 = r11
                    java.lang.Object r12 = com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r3, r4, r5, r6, r7, r8, r9, r10)
                    if (r12 != r0) goto L_0x0122
                    return r0
                L_0x0122:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r12 = r12.$heartRateSummaryRepository
                    com.fossil.se7 r3 = r11.$startDate
                    T r3 = r3.element
                    org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                    java.util.Date r3 = com.fossil.tx6.a(r3, r2)
                    com.fossil.se7 r4 = r11.$endDate
                    T r4 = r4.element
                    org.joda.time.DateTime r4 = (org.joda.time.DateTime) r4
                    java.util.Date r4 = com.fossil.tx6.a(r4, r2)
                    r11.L$0 = r1
                    r5 = 6
                    r11.label = r5
                    java.lang.Object r12 = r12.loadSummaries(r3, r4, r11)
                    if (r12 != r0) goto L_0x0146
                    return r0
                L_0x0146:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.WorkoutSessionRepository r3 = r12.$workoutSessionRepository
                    com.fossil.se7 r12 = r11.$startDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r4 = com.fossil.tx6.a(r12, r2)
                    com.fossil.se7 r12 = r11.$endDate
                    T r12 = r12.element
                    org.joda.time.DateTime r12 = (org.joda.time.DateTime) r12
                    java.util.Date r5 = com.fossil.tx6.a(r12, r2)
                    r6 = 0
                    r7 = 0
                    r9 = 12
                    r10 = 0
                    r11.L$0 = r1
                    r12 = 7
                    r11.label = r12
                    r8 = r11
                    java.lang.Object r12 = com.portfolio.platform.data.source.WorkoutSessionRepository.fetchWorkoutSessions$default(r3, r4, r5, r6, r7, r8, r9, r10)
                    if (r12 != r0) goto L_0x0170
                    return r0
                L_0x0170:
                    com.fossil.rk5$h r12 = r11.this$0
                    com.portfolio.platform.data.source.SummariesRepository r12 = r12.$summaryRepository
                    r11.L$0 = r1
                    r1 = 8
                    r11.label = r1
                    java.lang.Object r12 = r12.fetchActivityStatistic(r11)
                    if (r12 != r0) goto L_0x0181
                    return r0
                L_0x0181:
                    return r12
                    switch-data {0->0x0058, 1->0x0050, 2->0x0048, 3->0x003f, 4->0x0036, 5->0x002d, 6->0x0024, 7->0x001b, 8->0x0012, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.rk5.h.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(List list, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, fb7 fb7) {
            super(2, fb7);
            this.$fitnessDataList = list;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
            this.$heartRateSampleRepository = heartRateSampleRepository;
            this.$heartRateSummaryRepository = heartRateSummaryRepository;
            this.$workoutSessionRepository = workoutSessionRepository;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.$fitnessDataList, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, this.$heartRateSampleRepository, this.$heartRateSummaryRepository, this.$workoutSessionRepository, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                se7 se7 = new se7();
                se7.element = (T) ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getStartTimeTZ();
                se7 se72 = new se7();
                se72.element = (T) ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getEndTimeTZ();
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < se7.element.getMillis()) {
                        se7.element = (T) fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > se72.element.getMillis()) {
                        se72.element = (T) fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                ti7 b = qj7.b();
                a aVar = new a(this, se7, se72, null);
                this.L$0 = yi7;
                this.L$1 = se7;
                this.L$2 = se72;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                se7 se73 = (se7) this.L$2;
                se7 se74 = (se7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        String name = rk5.class.getName();
        ee7.a((Object) name, "DianaSyncDataProcessing::class.java.name");
        a = name;
    }
    */

    @DexIgnore
    public final a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        ee7.b(str, "serial");
        ee7.b(list, "syncData");
        ee7.b(mFUser, "user");
        ee7.b(userProfile, "userProfile");
        ee7.b(portfolioApp, "portfolioApp");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            return b(str, list, mFUser, userProfile, j, j2, portfolioApp);
        }
        a(str, "Sync data is empty");
        return aVar;
    }

    @DexIgnore
    public final a b(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        portfolioApp.a(CommunicateMode.SYNC, str, "Calculating sleep and activity...");
        String userId = mFUser.getUserId();
        v87<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = tx6.a(list, str, userId, 1000 * j);
        List<ActivitySample> first = a2.getFirst();
        List<ActivitySummary> second = a2.getSecond();
        List<GFitSample> third = a2.getThird();
        List<MFSleepSession> a3 = tx6.a(list, str);
        r87<List<HeartRateSample>, List<DailyHeartRateSummary>> a4 = a(list, userId);
        List<HeartRateSample> first2 = a4.getFirst();
        List<DailyHeartRateSummary> second2 = a4.getSecond();
        List<WorkoutSession> a5 = a(list, str, userId);
        List<GFitHeartRate> a6 = a(list);
        List<GFitWorkoutSession> b2 = b(list);
        int size = a3.size();
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        double d5 = 0.0d;
        int i = 0;
        int i2 = 0;
        for (T t : first) {
            d4 += t.getCalories();
            d5 += t.getDistance();
            d2 += t.getSteps();
            i += t.getActiveTime();
            Boolean w = zd5.w(t.getDate());
            ee7.a((Object) w, "DateHelper.isToday(it.date)");
            if (w.booleanValue()) {
                d3 += t.getSteps();
                i2 += t.getActiveTime();
            }
        }
        we7 we7 = we7.a;
        String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(new Object[]{String.valueOf(size)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        a(str, format);
        we7 we72 = we7.a;
        String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s, Calories=%s, DistanceWrapper=%s, ActiveTime=%s, TodayActiveTime=%s", Arrays.copyOf(new Object[]{Double.valueOf(d2), Double.valueOf(d3), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d5), Integer.valueOf(i), Integer.valueOf(i2)}, 8));
        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
        a(str, format2);
        a(str, "HeartRateWrapper data size: " + first2.size());
        FLogger.INSTANCE.getLocal().d(a, "Release=" + he5.a());
        if (!he5.a()) {
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Sleep sessions details: " + new Gson().a(a3));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - SampleRaw list details: " + new Gson().a(first));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate sample list details: " + first2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate summary list details: " + second2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Workout list details: " + new Gson().a(a5));
        }
        return new a(j2, first, second, a3, first2, second2, tx6.a(list), a5, third, a6, b2);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.g0.c().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0522  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x058d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x058e  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0646 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0647  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x06d5  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x072b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x0797 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0798  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x0825  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x087c  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x08ef A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x08f0  */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0910  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0995  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x03c5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0521 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.rk5.a r28, java.lang.String r29, com.portfolio.platform.data.source.SleepSessionsRepository r30, com.portfolio.platform.data.source.SummariesRepository r31, com.portfolio.platform.data.source.SleepSummariesRepository r32, com.portfolio.platform.data.source.HeartRateSampleRepository r33, com.portfolio.platform.data.source.HeartRateSummaryRepository r34, com.portfolio.platform.data.source.WorkoutSessionRepository r35, com.portfolio.platform.data.source.FitnessDataRepository r36, com.portfolio.platform.data.source.ActivitiesRepository r37, com.portfolio.platform.data.source.ThirdPartyRepository r38, com.portfolio.platform.PortfolioApp r39, com.fossil.ge5 r40, com.fossil.fb7<? super com.fossil.i97> r41) {
        /*
            r27 = this;
            r1 = r27
            r2 = r29
            r3 = r30
            r0 = r41
            boolean r4 = r0 instanceof com.fossil.rk5.f
            if (r4 == 0) goto L_0x001b
            r4 = r0
            com.fossil.rk5$f r4 = (com.fossil.rk5.f) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.fossil.rk5$f r4 = new com.fossil.rk5$f
            r4.<init>(r1, r0)
        L_0x0020:
            java.lang.Object r0 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            switch(r6) {
                case 0: goto L_0x03c5;
                case 1: goto L_0x0346;
                case 2: goto L_0x02f0;
                case 3: goto L_0x0290;
                case 4: goto L_0x0222;
                case 5: goto L_0x01c8;
                case 6: goto L_0x015e;
                case 7: goto L_0x00f2;
                case 8: goto L_0x0084;
                case 9: goto L_0x0033;
                default: goto L_0x002b;
            }
        L_0x002b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0033:
            java.lang.Object r2 = r4.L$18
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r4.L$17
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r2 = r4.L$16
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r4.L$15
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$13
            com.fossil.ge5 r2 = (com.fossil.ge5) r2
            java.lang.Object r2 = r4.L$12
            com.portfolio.platform.PortfolioApp r2 = (com.portfolio.platform.PortfolioApp) r2
            java.lang.Object r3 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r3 = (com.portfolio.platform.data.source.ThirdPartyRepository) r3
            java.lang.Object r3 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r3 = (com.portfolio.platform.data.source.ActivitiesRepository) r3
            java.lang.Object r3 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r3 = (com.portfolio.platform.data.source.FitnessDataRepository) r3
            java.lang.Object r3 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r3 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r3
            java.lang.Object r3 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r3 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r3
            java.lang.Object r3 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r3 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r3
            java.lang.Object r3 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r3 = (com.portfolio.platform.data.source.SleepSummariesRepository) r3
            java.lang.Object r3 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r3 = (com.portfolio.platform.data.source.SummariesRepository) r3
            java.lang.Object r3 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r3 = (com.portfolio.platform.data.source.SleepSessionsRepository) r3
            java.lang.Object r3 = r4.L$2
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r5 = r4.L$1
            com.fossil.rk5$a r5 = (com.fossil.rk5.a) r5
            java.lang.Object r4 = r4.L$0
            com.fossil.rk5 r4 = (com.fossil.rk5) r4
            com.fossil.t87.a(r0)
            goto L_0x0993
        L_0x0084:
            java.lang.Object r2 = r4.L$16
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r4.L$15
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r6 = r4.L$14
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r8 = r4.L$13
            com.fossil.ge5 r8 = (com.fossil.ge5) r8
            java.lang.Object r9 = r4.L$12
            com.portfolio.platform.PortfolioApp r9 = (com.portfolio.platform.PortfolioApp) r9
            java.lang.Object r10 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r10 = (com.portfolio.platform.data.source.ThirdPartyRepository) r10
            java.lang.Object r11 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r13 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r13
            java.lang.Object r14 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r14 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r14
            java.lang.Object r15 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r15 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r15
            java.lang.Object r7 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r7 = (com.portfolio.platform.data.source.SleepSummariesRepository) r7
            r28 = r2
            java.lang.Object r2 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r2 = (com.portfolio.platform.data.source.SummariesRepository) r2
            r29 = r2
            java.lang.Object r2 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = (com.portfolio.platform.data.source.SleepSessionsRepository) r2
            r30 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r31 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r32 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)
            r16 = r5
            r1 = r12
            r38 = r13
            r39 = r14
            r40 = r15
            r15 = r30
            r5 = r2
            r12 = r8
            r2 = r9
            r13 = r10
            r14 = r11
            r8 = r31
            r9 = r3
            r10 = r6
            r11 = r7
            r7 = r28
            r3 = r29
            r6 = r32
            goto L_0x0908
        L_0x00f2:
            java.lang.Object r2 = r4.L$16
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r4.L$15
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r6 = r4.L$14
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r4.L$13
            com.fossil.ge5 r7 = (com.fossil.ge5) r7
            java.lang.Object r8 = r4.L$12
            com.portfolio.platform.PortfolioApp r8 = (com.portfolio.platform.PortfolioApp) r8
            java.lang.Object r9 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r9 = (com.portfolio.platform.data.source.ThirdPartyRepository) r9
            java.lang.Object r10 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r10 = (com.portfolio.platform.data.source.ActivitiesRepository) r10
            java.lang.Object r11 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r11 = (com.portfolio.platform.data.source.FitnessDataRepository) r11
            java.lang.Object r12 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r12 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r12
            java.lang.Object r13 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r13 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r13
            java.lang.Object r14 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r14 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r14
            java.lang.Object r15 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r15 = (com.portfolio.platform.data.source.SleepSummariesRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r2 = (com.portfolio.platform.data.source.SummariesRepository) r2
            r29 = r2
            java.lang.Object r2 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = (com.portfolio.platform.data.source.SleepSessionsRepository) r2
            r30 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r31 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r32 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)
            r0 = r2
            r17 = r3
            r2 = r5
            r19 = r14
            r16 = r15
            r3 = r29
            r15 = r30
            r5 = r31
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r7
            r7 = r28
            r28 = r32
            goto L_0x085f
        L_0x015e:
            java.lang.Object r2 = r4.L$15
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r4.L$14
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r6 = r4.L$13
            com.fossil.ge5 r6 = (com.fossil.ge5) r6
            java.lang.Object r7 = r4.L$12
            com.portfolio.platform.PortfolioApp r7 = (com.portfolio.platform.PortfolioApp) r7
            java.lang.Object r8 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r9 = (com.portfolio.platform.data.source.ActivitiesRepository) r9
            java.lang.Object r10 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r10 = (com.portfolio.platform.data.source.FitnessDataRepository) r10
            java.lang.Object r11 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r11 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r11
            java.lang.Object r12 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r12 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r12
            java.lang.Object r13 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r13 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r13
            java.lang.Object r14 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r14 = (com.portfolio.platform.data.source.SleepSummariesRepository) r14
            java.lang.Object r15 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r15 = (com.portfolio.platform.data.source.SummariesRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = (com.portfolio.platform.data.source.SleepSessionsRepository) r2
            r29 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r30 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r31 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x01b8 }
            r17 = r28
            r18 = r3
            r1 = r5
            r16 = r15
            r15 = r29
            r5 = r30
            r3 = r31
            goto L_0x07a9
        L_0x01b8:
            r0 = move-exception
            r17 = r28
            r28 = r31
            r18 = r3
            r1 = r5
            r16 = r15
            r15 = r29
            r5 = r30
            goto L_0x07ec
        L_0x01c8:
            java.lang.Object r2 = r4.L$15
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r4.L$14
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r6 = r4.L$13
            com.fossil.ge5 r6 = (com.fossil.ge5) r6
            java.lang.Object r7 = r4.L$12
            com.portfolio.platform.PortfolioApp r7 = (com.portfolio.platform.PortfolioApp) r7
            java.lang.Object r8 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r9 = (com.portfolio.platform.data.source.ActivitiesRepository) r9
            java.lang.Object r10 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r10 = (com.portfolio.platform.data.source.FitnessDataRepository) r10
            java.lang.Object r11 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r11 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r11
            java.lang.Object r12 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r12 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r12
            java.lang.Object r13 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r13 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r13
            java.lang.Object r14 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r14 = (com.portfolio.platform.data.source.SleepSummariesRepository) r14
            java.lang.Object r15 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r15 = (com.portfolio.platform.data.source.SummariesRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = (com.portfolio.platform.data.source.SleepSessionsRepository) r2
            r29 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r30 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r31 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)
            r17 = r3
            r1 = r5
            r16 = r15
            r3 = r28
            r15 = r29
            r5 = r30
            r28 = r31
            goto L_0x070c
        L_0x0222:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r7 = (com.portfolio.platform.data.source.ThirdPartyRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r8 = (com.portfolio.platform.data.source.ActivitiesRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r9 = (com.portfolio.platform.data.source.FitnessDataRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r10 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r11 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r12 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r29 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r30 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x027b }
            r17 = r28
            r1 = r30
            r16 = r15
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r3
            r3 = r2
            r2 = r5
            r5 = r29
            goto L_0x064e
        L_0x027b:
            r0 = move-exception
            r17 = r28
            r28 = r30
            r16 = r0
            r1 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r3
            r3 = r2
            r2 = r5
            r5 = r29
            goto L_0x06a1
        L_0x0290:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r7 = (com.portfolio.platform.data.source.ThirdPartyRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r8 = (com.portfolio.platform.data.source.ActivitiesRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r9 = (com.portfolio.platform.data.source.FitnessDataRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r10 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r11 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r12 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r29 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r30 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x02e2 }
            r17 = r28
            r1 = r5
            r16 = r15
            r5 = r29
            r15 = r7
            r7 = r3
            r3 = r30
            goto L_0x0599
        L_0x02e2:
            r0 = move-exception
            r17 = r28
            r28 = r30
            r1 = r5
            r16 = r15
            r5 = r29
            r15 = r7
            r7 = r3
            goto L_0x05d4
        L_0x02f0:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r7 = (com.portfolio.platform.data.source.ThirdPartyRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r8 = (com.portfolio.platform.data.source.ActivitiesRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r9 = (com.portfolio.platform.data.source.FitnessDataRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r10 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r11 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r12 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r29 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r30 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)
            r17 = r28
            r16 = r5
            r1 = r14
            r5 = r29
            r14 = r8
            r8 = r6
            r6 = r15
            r15 = r7
            r7 = r3
            r3 = r2
            r2 = r30
            goto L_0x0539
        L_0x0346:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.ThirdPartyRepository r7 = (com.portfolio.platform.data.source.ThirdPartyRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ActivitiesRepository r8 = (com.portfolio.platform.data.source.ActivitiesRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.FitnessDataRepository r9 = (com.portfolio.platform.data.source.FitnessDataRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.WorkoutSessionRepository r10 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.HeartRateSummaryRepository r11 = (com.portfolio.platform.data.source.HeartRateSummaryRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.HeartRateSampleRepository r12 = (com.portfolio.platform.data.source.HeartRateSampleRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r28 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r29 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.rk5$a r2 = (com.fossil.rk5.a) r2
            r30 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.rk5 r2 = (com.fossil.rk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x03a6 }
            r16 = r28
            r1 = r2
            r0 = r3
            r3 = r15
            r2 = r29
            r15 = r7
            r7 = r30
            r24 = r14
            r14 = r8
            r8 = r24
            r25 = r13
            r13 = r9
            r9 = r25
            r26 = r12
            r12 = r10
            r10 = r26
            goto L_0x04cb
        L_0x03a6:
            r0 = move-exception
            r16 = r28
            r17 = r0
            r1 = r2
            r28 = r3
            r3 = r15
            r2 = r29
            r15 = r7
            r7 = r30
            r24 = r14
            r14 = r8
            r8 = r24
            r25 = r13
            r13 = r9
            r9 = r25
            r26 = r12
            r12 = r10
            r10 = r26
            goto L_0x04ad
        L_0x03c5:
            com.fossil.t87.a(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = "Save sync result - size of sleepSessions="
            r0.append(r6)
            java.util.List r6 = r28.i()
            int r6 = r6.size()
            r0.append(r6)
            java.lang.String r6 = ", size of sampleRaws="
            r0.append(r6)
            java.util.List r6 = r28.h()
            int r6 = r6.size()
            r0.append(r6)
            java.lang.String r6 = ", realTimeSteps="
            r0.append(r6)
            long r6 = r28.g()
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r7 = com.fossil.rk5.a
            r0.i(r7, r6)
            r1.a(r2, r6)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.c()
            boolean r0 = com.fossil.mh7.a(r0)
            if (r0 != 0) goto L_0x09aa
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.c()
            r7 = 1
            boolean r0 = com.fossil.mh7.b(r0, r2, r7)
            if (r0 != 0) goto L_0x042e
            goto L_0x09aa
        L_0x042e:
            java.lang.String r0 = "Saving sleep data"
            r1.a(r2, r0)
            java.util.List r0 = r28.i()     // Catch:{ Exception -> 0x0490 }
            r4.L$0 = r1     // Catch:{ Exception -> 0x0490 }
            r7 = r28
            r4.L$1 = r7     // Catch:{ Exception -> 0x048e }
            r4.L$2 = r2     // Catch:{ Exception -> 0x048e }
            r4.L$3 = r3     // Catch:{ Exception -> 0x048e }
            r8 = r31
            r4.L$4 = r8     // Catch:{ Exception -> 0x048c }
            r9 = r32
            r4.L$5 = r9     // Catch:{ Exception -> 0x048a }
            r10 = r33
            r4.L$6 = r10     // Catch:{ Exception -> 0x0488 }
            r11 = r34
            r4.L$7 = r11     // Catch:{ Exception -> 0x0486 }
            r12 = r35
            r4.L$8 = r12     // Catch:{ Exception -> 0x0484 }
            r13 = r36
            r4.L$9 = r13     // Catch:{ Exception -> 0x0482 }
            r14 = r37
            r4.L$10 = r14     // Catch:{ Exception -> 0x0480 }
            r15 = r38
            r4.L$11 = r15     // Catch:{ Exception -> 0x047e }
            r1 = r39
            r4.L$12 = r1     // Catch:{ Exception -> 0x047e }
            r1 = r40
            r4.L$13 = r1     // Catch:{ Exception -> 0x047e }
            r4.L$14 = r6     // Catch:{ Exception -> 0x047e }
            r1 = 1
            r4.label = r1     // Catch:{ Exception -> 0x047e }
            java.lang.Object r0 = r3.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x047e }
            if (r0 != r5) goto L_0x0475
            return r5
        L_0x0475:
            r1 = r27
            r0 = r40
            r16 = r6
            r6 = r39
            goto L_0x04cb
        L_0x047e:
            r0 = move-exception
            goto L_0x04a3
        L_0x0480:
            r0 = move-exception
            goto L_0x04a1
        L_0x0482:
            r0 = move-exception
            goto L_0x049f
        L_0x0484:
            r0 = move-exception
            goto L_0x049d
        L_0x0486:
            r0 = move-exception
            goto L_0x049b
        L_0x0488:
            r0 = move-exception
            goto L_0x0499
        L_0x048a:
            r0 = move-exception
            goto L_0x0497
        L_0x048c:
            r0 = move-exception
            goto L_0x0495
        L_0x048e:
            r0 = move-exception
            goto L_0x0493
        L_0x0490:
            r0 = move-exception
            r7 = r28
        L_0x0493:
            r8 = r31
        L_0x0495:
            r9 = r32
        L_0x0497:
            r10 = r33
        L_0x0499:
            r11 = r34
        L_0x049b:
            r12 = r35
        L_0x049d:
            r13 = r36
        L_0x049f:
            r14 = r37
        L_0x04a1:
            r15 = r38
        L_0x04a3:
            r1 = r27
            r28 = r40
            r17 = r0
            r16 = r6
            r6 = r39
        L_0x04ad:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r29 = r3
            java.lang.String r3 = "Saving sleep data. error="
            r0.append(r3)
            java.lang.String r3 = r17.getMessage()
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r1.a(r2, r0)
            r0 = r28
            r3 = r29
        L_0x04cb:
            r17 = r16
            r16 = r5
            java.lang.String r5 = "Saving sleep data. OK"
            r1.a(r2, r5)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            r28 = r0
            java.lang.String r0 = com.fossil.rk5.a
            r29 = r6
            java.lang.String r6 = ".saveSyncResult - Upload sleep sessions to server"
            r5.d(r0, r6)
            java.lang.String r0 = "Saving activity data"
            r1.a(r2, r0)
            com.fossil.rk5$g r0 = new com.fossil.rk5$g
            r5 = 0
            r0.<init>(r7, r15, r5)
            r4.L$0 = r1
            r4.L$1 = r7
            r4.L$2 = r2
            r4.L$3 = r3
            r4.L$4 = r8
            r4.L$5 = r9
            r4.L$6 = r10
            r4.L$7 = r11
            r4.L$8 = r12
            r4.L$9 = r13
            r4.L$10 = r14
            r4.L$11 = r15
            r6 = r29
            r4.L$12 = r6
            r5 = r28
            r4.L$13 = r5
            r28 = r1
            r1 = r17
            r4.L$14 = r1
            r1 = 2
            r4.label = r1
            java.lang.Object r0 = com.fossil.zi7.a(r0, r4)
            r1 = r16
            if (r0 != r1) goto L_0x0522
            return r1
        L_0x0522:
            r16 = r1
            r1 = r8
            r8 = r6
            r6 = r3
            r3 = r28
            r24 = r5
            r5 = r2
            r2 = r7
            r7 = r24
            r25 = r13
            r13 = r9
            r9 = r25
            r26 = r12
            r12 = r10
            r10 = r26
        L_0x0539:
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r29 = r14
            r28 = r15
            long r14 = r2.g()
            r7.a(r0, r14)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r14 = com.fossil.rk5.a
            java.lang.String r15 = ".saveSyncResult - Update heartbeat step by syncing"
            r0.d(r14, r15)
            java.util.List r0 = r2.h()     // Catch:{ Exception -> 0x05c2 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x05c2 }
            r4.L$1 = r2     // Catch:{ Exception -> 0x05c2 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x05c2 }
            r4.L$3 = r6     // Catch:{ Exception -> 0x05c2 }
            r4.L$4 = r1     // Catch:{ Exception -> 0x05c2 }
            r4.L$5 = r13     // Catch:{ Exception -> 0x05c2 }
            r4.L$6 = r12     // Catch:{ Exception -> 0x05c2 }
            r4.L$7 = r11     // Catch:{ Exception -> 0x05c2 }
            r4.L$8 = r10     // Catch:{ Exception -> 0x05c2 }
            r4.L$9 = r9     // Catch:{ Exception -> 0x05c2 }
            r14 = r29
            r4.L$10 = r14     // Catch:{ Exception -> 0x05be }
            r15 = r28
            r4.L$11 = r15     // Catch:{ Exception -> 0x05bc }
            r4.L$12 = r8     // Catch:{ Exception -> 0x05bc }
            r4.L$13 = r7     // Catch:{ Exception -> 0x05bc }
            r28 = r1
            r1 = r17
            r4.L$14 = r1     // Catch:{ Exception -> 0x05b8 }
            r17 = r1
            r1 = 3
            r4.label = r1     // Catch:{ Exception -> 0x05b6 }
            java.lang.Object r0 = r14.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x05b6 }
            r1 = r16
            if (r0 != r1) goto L_0x058e
            return r1
        L_0x058e:
            r16 = r6
            r6 = r8
            r8 = r14
            r14 = r28
            r24 = r3
            r3 = r2
            r2 = r24
        L_0x0599:
            java.lang.String r0 = "Saving activity data. OK"
            r2.a(r5, r0)     // Catch:{ Exception -> 0x05b2 }
            r24 = r16
            r16 = r1
            r1 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r6
            r6 = r24
            r25 = r3
            r3 = r2
            r2 = r25
            goto L_0x05fe
        L_0x05b2:
            r0 = move-exception
            r28 = r3
            goto L_0x05d4
        L_0x05b6:
            r0 = move-exception
            goto L_0x05c9
        L_0x05b8:
            r0 = move-exception
            r17 = r1
            goto L_0x05c9
        L_0x05bc:
            r0 = move-exception
            goto L_0x05c7
        L_0x05be:
            r0 = move-exception
            r15 = r28
            goto L_0x05c7
        L_0x05c2:
            r0 = move-exception
            r15 = r28
            r14 = r29
        L_0x05c7:
            r28 = r1
        L_0x05c9:
            r1 = r16
            r16 = r6
            r6 = r8
            r8 = r14
            r14 = r28
            r28 = r2
            r2 = r3
        L_0x05d4:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r29 = r6
            java.lang.String r6 = "Saving activity data. error="
            r3.append(r6)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.a(r5, r0)
            r3 = r2
            r6 = r16
            r2 = r28
            r16 = r1
            r1 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r29
        L_0x05fe:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r28 = r7
            java.lang.String r7 = com.fossil.rk5.a
            r29 = r8
            java.lang.String r8 = ".saveSyncResult - Update activities summary by syncing"
            r0.d(r7, r8)
            java.util.List r0 = r2.j()     // Catch:{ Exception -> 0x0690 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x0690 }
            r4.L$1 = r2     // Catch:{ Exception -> 0x0690 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x0690 }
            r4.L$3 = r6     // Catch:{ Exception -> 0x0690 }
            r4.L$4 = r1     // Catch:{ Exception -> 0x0690 }
            r4.L$5 = r14     // Catch:{ Exception -> 0x0690 }
            r4.L$6 = r13     // Catch:{ Exception -> 0x0690 }
            r4.L$7 = r12     // Catch:{ Exception -> 0x0690 }
            r4.L$8 = r11     // Catch:{ Exception -> 0x0690 }
            r4.L$9 = r10     // Catch:{ Exception -> 0x0690 }
            r4.L$10 = r9     // Catch:{ Exception -> 0x0690 }
            r4.L$11 = r15     // Catch:{ Exception -> 0x0690 }
            r7 = r29
            r4.L$12 = r7     // Catch:{ Exception -> 0x068c }
            r8 = r28
            r4.L$13 = r8     // Catch:{ Exception -> 0x068a }
            r28 = r2
            r2 = r17
            r4.L$14 = r2     // Catch:{ Exception -> 0x0686 }
            r17 = r2
            r2 = 4
            r4.label = r2     // Catch:{ Exception -> 0x0684 }
            java.lang.Object r0 = r1.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x0684 }
            r2 = r16
            if (r0 != r2) goto L_0x0647
            return r2
        L_0x0647:
            r16 = r6
            r6 = r7
            r7 = r15
            r15 = r1
            r1 = r28
        L_0x064e:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0679 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x0679 }
            r28 = r1
            java.lang.String r1 = com.fossil.rk5.a     // Catch:{ Exception -> 0x0677 }
            r29 = r6
            java.lang.String r6 = "XXX- Done save new activity to db"
            r0.d(r1, r6)     // Catch:{ Exception -> 0x0673 }
            java.lang.String r0 = "Saving activity summaries data. OK"
            r3.a(r5, r0)     // Catch:{ Exception -> 0x0673 }
            r0 = r28
            r6 = r8
            r8 = r7
            r7 = r29
            r24 = r16
            r16 = r2
            r2 = r15
            r15 = r24
            goto L_0x06c7
        L_0x0673:
            r0 = move-exception
            r6 = r29
            goto L_0x067e
        L_0x0677:
            r0 = move-exception
            goto L_0x067c
        L_0x0679:
            r0 = move-exception
            r28 = r1
        L_0x067c:
            r29 = r6
        L_0x067e:
            r1 = r15
            r15 = r16
            r16 = r0
            goto L_0x06a1
        L_0x0684:
            r0 = move-exception
            goto L_0x0697
        L_0x0686:
            r0 = move-exception
            r17 = r2
            goto L_0x0697
        L_0x068a:
            r0 = move-exception
            goto L_0x0695
        L_0x068c:
            r0 = move-exception
            r8 = r28
            goto L_0x0695
        L_0x0690:
            r0 = move-exception
            r8 = r28
            r7 = r29
        L_0x0695:
            r28 = r2
        L_0x0697:
            r2 = r16
            r16 = r0
            r24 = r15
            r15 = r6
            r6 = r7
            r7 = r24
        L_0x06a1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r29 = r1
            java.lang.String r1 = "Saving activity summaries data. error="
            r0.append(r1)
            java.lang.String r1 = r16.getMessage()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r3.a(r5, r0)
            r0 = r28
            r16 = r2
            r2 = r29
            r24 = r7
            r7 = r6
            r6 = r8
            r8 = r24
        L_0x06c7:
            java.util.List r1 = r0.e()
            boolean r18 = r1.isEmpty()
            r19 = 1
            r18 = r18 ^ 1
            if (r18 == 0) goto L_0x072b
            r4.L$0 = r3
            r4.L$1 = r0
            r4.L$2 = r5
            r4.L$3 = r15
            r4.L$4 = r2
            r4.L$5 = r14
            r4.L$6 = r13
            r4.L$7 = r12
            r4.L$8 = r11
            r4.L$9 = r10
            r4.L$10 = r9
            r4.L$11 = r8
            r4.L$12 = r7
            r4.L$13 = r6
            r28 = r0
            r0 = r17
            r4.L$14 = r0
            r4.L$15 = r1
            r0 = 5
            r4.label = r0
            java.lang.Object r0 = r13.insertFromDevice(r1, r4)
            r29 = r1
            r1 = r16
            if (r0 != r1) goto L_0x0707
            return r1
        L_0x0707:
            r16 = r2
            r2 = r3
            r3 = r29
        L_0x070c:
            java.lang.String r0 = "Saving heart rate data. OK"
            r2.a(r5, r0)
            r18 = r17
            r17 = r3
            r3 = r2
            r2 = r28
            r24 = r16
            r16 = r1
            r1 = r13
            r13 = r11
            r11 = r9
            r9 = r7
            r7 = r24
            r25 = r8
            r8 = r6
            r6 = r14
            r14 = r12
            r12 = r10
            r10 = r25
            goto L_0x0749
        L_0x072b:
            r28 = r0
            r29 = r1
            r1 = r16
            java.lang.String r0 = "No heart rate data."
            r3.a(r5, r0)
            r1 = r13
            r18 = r17
            r17 = r29
            r13 = r11
            r11 = r9
            r9 = r7
            r7 = r2
            r2 = r28
            r24 = r8
            r8 = r6
            r6 = r14
            r14 = r12
            r12 = r10
            r10 = r24
        L_0x0749:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r19 = r8
            java.lang.String r8 = com.fossil.rk5.a
            r28 = r9
            java.lang.String r9 = ".saveSyncResult - Update heartrate summary by syncing"
            r0.d(r8, r9)
            java.util.List r0 = r2.f()     // Catch:{ Exception -> 0x07d4 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x07d4 }
            r4.L$1 = r2     // Catch:{ Exception -> 0x07d4 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x07d4 }
            r4.L$3 = r15     // Catch:{ Exception -> 0x07d4 }
            r4.L$4 = r7     // Catch:{ Exception -> 0x07d4 }
            r4.L$5 = r6     // Catch:{ Exception -> 0x07d4 }
            r4.L$6 = r1     // Catch:{ Exception -> 0x07d4 }
            r4.L$7 = r14     // Catch:{ Exception -> 0x07d4 }
            r4.L$8 = r13     // Catch:{ Exception -> 0x07d4 }
            r4.L$9 = r12     // Catch:{ Exception -> 0x07d4 }
            r4.L$10 = r11     // Catch:{ Exception -> 0x07d4 }
            r4.L$11 = r10     // Catch:{ Exception -> 0x07d4 }
            r8 = r28
            r4.L$12 = r8     // Catch:{ Exception -> 0x07d2 }
            r9 = r19
            r4.L$13 = r9     // Catch:{ Exception -> 0x07d0 }
            r19 = r1
            r1 = r18
            r4.L$14 = r1     // Catch:{ Exception -> 0x07cc }
            r18 = r1
            r1 = r17
            r4.L$15 = r1     // Catch:{ Exception -> 0x07c8 }
            r17 = r1
            r1 = 6
            r4.label = r1     // Catch:{ Exception -> 0x07c6 }
            java.lang.Object r0 = r14.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x07c6 }
            r1 = r16
            if (r0 != r1) goto L_0x0798
            return r1
        L_0x0798:
            r16 = r7
            r7 = r8
            r8 = r10
            r10 = r12
            r12 = r14
            r14 = r6
            r6 = r9
            r9 = r11
            r11 = r13
            r13 = r19
            r24 = r3
            r3 = r2
            r2 = r24
        L_0x07a9:
            java.lang.String r0 = "Saving heartrate summaries data. OK"
            r2.a(r5, r0)     // Catch:{ Exception -> 0x07c2 }
            r0 = r2
            r2 = r3
            r3 = r16
            r16 = r1
            r1 = r13
            r13 = r11
            r11 = r9
            r9 = r7
            r24 = r8
            r8 = r6
            r6 = r14
            r14 = r12
            r12 = r10
            r10 = r24
            goto L_0x0817
        L_0x07c2:
            r0 = move-exception
            r28 = r3
            goto L_0x07ec
        L_0x07c6:
            r0 = move-exception
            goto L_0x07db
        L_0x07c8:
            r0 = move-exception
            r17 = r1
            goto L_0x07db
        L_0x07cc:
            r0 = move-exception
            r18 = r1
            goto L_0x07db
        L_0x07d0:
            r0 = move-exception
            goto L_0x07d9
        L_0x07d2:
            r0 = move-exception
            goto L_0x07d7
        L_0x07d4:
            r0 = move-exception
            r8 = r28
        L_0x07d7:
            r9 = r19
        L_0x07d9:
            r19 = r1
        L_0x07db:
            r1 = r16
            r28 = r2
            r2 = r3
            r16 = r7
            r7 = r8
            r8 = r10
            r10 = r12
            r12 = r14
            r14 = r6
            r6 = r9
            r9 = r11
            r11 = r13
            r13 = r19
        L_0x07ec:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r29 = r6
            java.lang.String r6 = "Saving heartrate summaries data. error="
            r3.append(r6)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.a(r5, r0)
            r0 = r2
            r6 = r14
            r3 = r16
            r2 = r28
            r16 = r1
            r14 = r12
            r1 = r13
            r12 = r10
            r13 = r11
            r10 = r8
            r11 = r9
            r8 = r29
            r9 = r7
        L_0x0817:
            java.util.List r7 = r2.k()
            boolean r19 = r7.isEmpty()
            r20 = 1
            r19 = r19 ^ 1
            if (r19 == 0) goto L_0x087c
            r4.L$0 = r0
            r4.L$1 = r2
            r4.L$2 = r5
            r4.L$3 = r15
            r4.L$4 = r3
            r4.L$5 = r6
            r4.L$6 = r1
            r4.L$7 = r14
            r4.L$8 = r13
            r4.L$9 = r12
            r4.L$10 = r11
            r4.L$11 = r10
            r4.L$12 = r9
            r4.L$13 = r8
            r19 = r1
            r1 = r18
            r4.L$14 = r1
            r1 = r17
            r4.L$15 = r1
            r4.L$16 = r7
            r1 = 7
            r4.label = r1
            java.lang.Object r1 = r13.insertFromDevice(r7, r4)
            r28 = r2
            r2 = r16
            if (r1 != r2) goto L_0x085b
            return r2
        L_0x085b:
            r16 = r6
            r6 = r18
        L_0x085f:
            java.lang.String r1 = "Saving workout sessions. OK"
            r0.a(r5, r1)
            r1 = r28
            r18 = r17
            r17 = r7
            r7 = r14
            r14 = r12
            r12 = r10
            r10 = r8
            r8 = r6
            r6 = r5
            r5 = r19
            r24 = r16
            r16 = r2
            r2 = r13
            r13 = r11
            r11 = r9
            r9 = r24
            goto L_0x089c
        L_0x087c:
            r19 = r1
            r28 = r2
            r2 = r16
            java.lang.String r1 = "No workout sessions data."
            r0.a(r5, r1)
            r1 = r28
            r2 = r13
            r13 = r11
            r11 = r9
            r9 = r6
            r6 = r5
            r5 = r19
            r24 = r17
            r17 = r7
            r7 = r14
            r14 = r12
            r12 = r10
            r10 = r8
            r8 = r18
            r18 = r24
        L_0x089c:
            com.misfit.frameworks.buttonservice.log.FLogger r19 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r19 = r19.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r20 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API
            com.misfit.frameworks.buttonservice.log.FLogger$Session r21 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PUSH_FITNESS_FILE
            java.lang.String r22 = com.fossil.rk5.a
            java.lang.String r23 = "[SYNC] Sync success, push pending fitness file size"
            r28 = r19
            r29 = r20
            r30 = r21
            r31 = r6
            r32 = r22
            r33 = r23
            r28.i(r29, r30, r31, r32, r33)
            r4.L$0 = r0
            r4.L$1 = r1
            r4.L$2 = r6
            r4.L$3 = r15
            r4.L$4 = r3
            r4.L$5 = r9
            r4.L$6 = r5
            r4.L$7 = r7
            r4.L$8 = r2
            r4.L$9 = r14
            r4.L$10 = r13
            r4.L$11 = r12
            r4.L$12 = r11
            r4.L$13 = r10
            r4.L$14 = r8
            r19 = r0
            r0 = r18
            r4.L$15 = r0
            r0 = r17
            r4.L$16 = r0
            r0 = 8
            r4.label = r0
            java.lang.Object r0 = r14.pushPendingFitnessData(r4)
            r20 = r1
            r1 = r16
            if (r0 != r1) goto L_0x08f0
            return r1
        L_0x08f0:
            r16 = r1
            r38 = r2
            r40 = r5
            r39 = r7
            r2 = r11
            r1 = r14
            r7 = r17
            r5 = r19
            r11 = r9
            r14 = r13
            r9 = r18
            r13 = r12
            r12 = r10
            r10 = r8
            r8 = r6
            r6 = r20
        L_0x0908:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            r17 = r7
            boolean r7 = r0 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x0995
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            r18 = r9
            java.lang.String r9 = com.fossil.rk5.a
            r19 = r10
            java.lang.String r10 = "XXX- push fitness data file success, get updated summary from server"
            r7.d(r9, r10)
            r7 = r0
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            java.util.List r7 = (java.util.List) r7
            if (r7 == 0) goto L_0x0997
            boolean r9 = r7.isEmpty()
            r10 = 1
            r9 = r9 ^ r10
            if (r9 == 0) goto L_0x0997
            com.fossil.ti7 r9 = com.fossil.qj7.a()
            com.fossil.rk5$h r10 = new com.fossil.rk5$h
            r20 = 0
            r28 = r10
            r29 = r7
            r30 = r14
            r31 = r3
            r32 = r15
            r33 = r11
            r34 = r40
            r35 = r39
            r36 = r38
            r37 = r20
            r28.<init>(r29, r30, r31, r32, r33, r34, r35, r36, r37)
            r4.L$0 = r5
            r4.L$1 = r6
            r4.L$2 = r8
            r4.L$3 = r15
            r4.L$4 = r3
            r4.L$5 = r11
            r5 = r40
            r4.L$6 = r5
            r3 = r39
            r4.L$7 = r3
            r3 = r38
            r4.L$8 = r3
            r4.L$9 = r1
            r4.L$10 = r14
            r4.L$11 = r13
            r4.L$12 = r2
            r4.L$13 = r12
            r6 = r19
            r4.L$14 = r6
            r3 = r18
            r4.L$15 = r3
            r1 = r17
            r4.L$16 = r1
            r4.L$17 = r0
            r4.L$18 = r7
            r0 = 9
            r4.label = r0
            java.lang.Object r0 = com.fossil.vh7.a(r9, r10, r4)
            r1 = r16
            if (r0 != r1) goto L_0x0992
            return r1
        L_0x0992:
            r3 = r8
        L_0x0993:
            r8 = r3
            goto L_0x0997
        L_0x0995:
            boolean r0 = r0 instanceof com.fossil.yi5
        L_0x0997:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.rk5.a
            java.lang.String r3 = "DONE save to database, delete data file"
            r0.d(r1, r3)
            r2.b(r8)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x09aa:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.rk5.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error inside "
            r2.append(r3)
            java.lang.String r3 = com.fossil.rk5.a
            r2.append(r3)
            java.lang.String r3 = ".saveSyncResult - Sync data does not match any user's device"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
            switch-data {0->0x03c5, 1->0x0346, 2->0x02f0, 3->0x0290, 4->0x0222, 5->0x01c8, 6->0x015e, 7->0x00f2, 8->0x0084, 9->0x0033, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rk5.a(com.fossil.rk5$a, java.lang.String, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.HeartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository, com.portfolio.platform.data.source.WorkoutSessionRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.portfolio.platform.PortfolioApp, com.fossil.ge5, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<GFitWorkoutSession> b(List<FitnessDataWrapper> list) {
        ArrayList arrayList;
        ArrayList arrayList2;
        int i;
        ArrayList arrayList3;
        ArrayList arrayList4 = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Iterator<T> it2 = it.next().getWorkouts().iterator();
            while (it2.hasNext()) {
                T next = it2.next();
                DateTime component2 = next.component2();
                DateTime component3 = next.component3();
                int component5 = next.component5();
                int component6 = next.component6();
                StepWrapper component11 = next.component11();
                CalorieWrapper component12 = next.component12();
                DistanceWrapper component13 = next.component13();
                HeartRateWrapper component14 = next.component14();
                long millis = component2.getMillis();
                long millis2 = component3.getMillis();
                ArrayList arrayList5 = new ArrayList();
                ArrayList arrayList6 = new ArrayList();
                ArrayList arrayList7 = new ArrayList();
                ArrayList arrayList8 = new ArrayList();
                int size = component11.getValues().size();
                if (size > 0) {
                    arrayList = arrayList4;
                    arrayList2 = arrayList8;
                    i = (int) ((((float) component5) / ((float) size)) * ((float) 1000));
                } else {
                    arrayList = arrayList4;
                    arrayList2 = arrayList8;
                    i = 0;
                }
                int i2 = 0;
                while (i2 < size) {
                    i2++;
                    arrayList5.add(new GFitWOStep(component11.getValues().get(i2).shortValue(), millis + ((long) (i2 * i)), millis + ((long) (i2 * i))));
                    component14 = component14;
                    size = size;
                }
                int size2 = component12.getValues().size();
                int i3 = size2 > 0 ? (int) ((((float) component5) / ((float) size2)) * ((float) 1000)) : 0;
                int i4 = 0;
                while (i4 < size2) {
                    i4++;
                    arrayList6.add(new GFitWOCalorie(component12.getValues().get(i4).floatValue(), millis + ((long) (i4 * i3)), millis + ((long) (i4 * i3))));
                }
                if (component13 != null) {
                    int size3 = component13.getValues().size();
                    int i5 = size3 > 0 ? (int) ((((float) component5) / ((float) size3)) * ((float) 1000)) : 0;
                    int i6 = 0;
                    while (i6 < size3) {
                        i6++;
                        arrayList7.add(new GFitWODistance((float) component13.getValues().get(i6).doubleValue(), millis + ((long) (i6 * i5)), millis + ((long) (i6 * i5))));
                    }
                }
                if (component14 != null) {
                    int size4 = component14.getValues().size();
                    int i7 = size4 > 0 ? (int) ((((float) component5) / ((float) size4)) * ((float) 1000)) : 0;
                    int i8 = 0;
                    while (i8 < size4) {
                        i8++;
                        arrayList2.add(new GFitWOHeartRate((float) component14.getValues().get(i8).shortValue(), ((long) (i8 * i7)) + millis, ((long) (i8 * i7)) + millis));
                    }
                }
                if (millis <= 0 || millis2 <= 0) {
                    arrayList3 = arrayList;
                    FLogger.INSTANCE.getLocal().d(a, "getGFitWorkoutSessions gFitWorkoutSession with invalid time");
                } else {
                    arrayList3 = arrayList;
                    arrayList3.add(new GFitWorkoutSession(millis, millis2, component6, arrayList5, arrayList6, arrayList7, arrayList2));
                }
                arrayList4 = arrayList3;
                it2 = it2;
                it = it;
            }
        }
        return arrayList4;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r21v1, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary */
    /* JADX DEBUG: Multi-variable search result rejected for r21v2, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary */
    /* JADX DEBUG: Multi-variable search result rejected for r21v3, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x03a7  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x03aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.r87<java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>, java.util.List<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>> a(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r55, java.lang.String r56) {
        /*
            r54 = this;
            r0 = r54
            r1 = r55
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            boolean r4 = r55.isEmpty()
            if (r4 == 0) goto L_0x001a
            com.fossil.r87 r1 = new com.fossil.r87
            r1.<init>(r2, r3)
            return r1
        L_0x001a:
            r4 = 2
            com.fossil.gd7[] r4 = new com.fossil.gd7[r4]
            com.fossil.rk5$c r5 = com.fossil.rk5.c.INSTANCE
            r6 = 0
            r4[r6] = r5
            com.fossil.rk5$d r5 = com.fossil.rk5.d.INSTANCE
            r7 = 1
            r4[r7] = r5
            java.util.Comparator r4 = com.fossil.bb7.a(r4)
            com.fossil.ea7.a(r1, r4)
            java.lang.Object r4 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r4 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r4
            long r4 = r4.getStartLongTime()
            java.util.Calendar r4 = com.fossil.zd5.c(r4)
            r5 = 13
            r4.set(r5, r6)
            r8 = 14
            r4.set(r8, r6)
            java.lang.String r9 = "startTime"
            com.fossil.ee7.a(r4, r9)
            java.lang.Object r9 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r9 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r9
            int r9 = r9.getTimezoneOffsetInSecond()
            int r9 = r9 * 1000
            java.util.TimeZone r9 = com.fossil.zd5.a(r9)
            r4.setTimeZone(r9)
            java.lang.Object r9 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r9 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r9
            long r9 = r9.getStartLongTime()
            java.lang.Object r11 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r11 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r11
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r11 = r11.getHeartRate()
            if (r11 == 0) goto L_0x0079
            int r11 = r11.getResolutionInSecond()
            goto L_0x007a
        L_0x0079:
            r11 = 0
        L_0x007a:
            long r11 = (long) r11
            r13 = 1000(0x3e8, double:4.94E-321)
            long r11 = r11 * r13
            long r9 = r9 + r11
            java.util.Calendar r9 = com.fossil.zd5.c(r9)
            r10 = 10
            int r11 = r4.get(r10)
            r12 = 12
            int r15 = r4.get(r12)
            com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r33 = new com.portfolio.platform.data.model.diana.heartrate.HeartRateSample
            r16 = r33
            r17 = 0
            java.util.Date r12 = r4.getTime()
            r18 = r12
            java.lang.String r10 = "startTime.time"
            com.fossil.ee7.a(r12, r10)
            long r19 = java.lang.System.currentTimeMillis()
            long r21 = java.lang.System.currentTimeMillis()
            org.joda.time.DateTime r10 = new org.joda.time.DateTime
            r23 = r10
            java.lang.String r12 = "endTime"
            com.fossil.ee7.a(r9, r12)
            long r8 = r9.getTimeInMillis()
            r10.<init>(r8)
            org.joda.time.DateTime r8 = new org.joda.time.DateTime
            r24 = r8
            long r9 = r4.getTimeInMillis()
            r8.<init>(r9)
            java.lang.Object r4 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r4 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r4
            int r25 = r4.getTimezoneOffsetInSecond()
            r26 = 2147483647(0x7fffffff, float:NaN)
            r27 = -2147483648(0xffffffff80000000, float:-0.0)
            r29 = 0
            r30 = 0
            r31 = 2048(0x800, float:2.87E-42)
            r32 = 0
            r28 = r56
            r16.<init>(r17, r18, r19, r21, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r4 = new com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary
            float r35 = r33.getAverage()
            java.util.Date r8 = new java.util.Date
            org.joda.time.DateTime r9 = r33.getStartTimeId()
            long r9 = r9.getMillis()
            r8.<init>(r9)
            long r37 = java.lang.System.currentTimeMillis()
            long r39 = java.lang.System.currentTimeMillis()
            int r41 = r33.getMin()
            int r42 = r33.getMax()
            int r43 = r33.getMinuteCount()
            com.portfolio.platform.data.model.diana.heartrate.Resting r44 = r33.getResting()
            r34 = r4
            r36 = r8
            r34.<init>(r35, r36, r37, r39, r41, r42, r43, r44)
            r8 = 0
            java.util.TimeZone r9 = java.util.TimeZone.getDefault()
            java.lang.String r10 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r9, r10)
            int r9 = r9.getRawOffset()
            int r9 = r9 / 1000
            com.fossil.se7 r10 = new com.fossil.se7
            r10.<init>()
            java.util.Iterator r1 = r55.iterator()
            r55 = r33
            r16 = 0
            r17 = 0
        L_0x0131:
            boolean r18 = r1.hasNext()
            java.lang.String r12 = "currentHeartRate.getStar\u2026oLocalDateTime().toDate()"
            java.lang.String r5 = "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)"
            r21 = 0
            if (r18 == 0) goto L_0x049f
            java.lang.Object r18 = r1.next()
            int r22 = r16 + 1
            if (r16 < 0) goto L_0x049b
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r18 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r18
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r16 = r18.getHeartRate()
            if (r16 == 0) goto L_0x046d
            int r13 = r16.component1()
            java.util.List r14 = r16.component4()
            int r6 = r18.getTimezoneOffsetInSecond()
            java.util.List r25 = r18.getResting()
            boolean r25 = r25.isEmpty()
            r25 = r25 ^ 1
            if (r25 == 0) goto L_0x017b
            java.util.List r7 = r18.getResting()
            r26 = r1
            r1 = 0
            java.lang.Object r7 = r7.get(r1)
            com.portfolio.platform.data.model.fitnessdata.RestingWrapper r7 = (com.portfolio.platform.data.model.fitnessdata.RestingWrapper) r7
            org.joda.time.DateTime r1 = r7.getStartTime()
            long r27 = r1.getMillis()
            goto L_0x017f
        L_0x017b:
            r26 = r1
            r27 = 0
        L_0x017f:
            r52 = r27
            r27 = r8
            r7 = r52
            java.util.List r1 = r18.getResting()
            r10.element = r1
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            r28 = r11
            java.lang.String r11 = com.fossil.rk5.a
            r29 = r15
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r0 = "Resting: restingStartTimeInSecond="
            r15.append(r0)
            r15.append(r7)
            java.lang.String r0 = ", restingValues="
            r15.append(r0)
            T r0 = r10.element
            java.util.List r0 = (java.util.List) r0
            java.lang.String r0 = com.fossil.wc5.a(r0)
            r15.append(r0)
            java.lang.String r0 = r15.toString()
            r1.d(r11, r0)
            int r0 = r9 * 1000
            java.lang.String[] r0 = java.util.TimeZone.getAvailableIDs(r0)
            java.util.Iterator r1 = r14.iterator()
            r7 = r4
            r8 = r27
            r11 = r28
            r15 = r29
            r14 = 0
            r4 = r55
        L_0x01cf:
            boolean r27 = r1.hasNext()
            if (r27 == 0) goto L_0x045e
            java.lang.Object r27 = r1.next()
            int r28 = r14 + 1
            if (r14 < 0) goto L_0x045a
            java.lang.Number r27 = (java.lang.Number) r27
            r30 = r1
            short r1 = r27.shortValue()
            long r31 = r18.getStartLongTime()
            int r14 = r14 * r13
            r33 = r2
            r51 = r3
            long r2 = (long) r14
            r23 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r23
            long r31 = r31 + r2
            java.util.Calendar r2 = com.fossil.zd5.c(r31)
            r3 = 13
            r14 = 0
            r2.set(r3, r14)
            r3 = 14
            r2.set(r3, r14)
            java.lang.String r3 = "currentStartTime"
            com.fossil.ee7.a(r2, r3)
            r3 = r0[r14]
            java.util.TimeZone r3 = java.util.TimeZone.getTimeZone(r3)
            r2.setTimeZone(r3)
            java.lang.Object r3 = r2.clone()
            if (r3 == 0) goto L_0x0452
            java.util.Calendar r3 = (java.util.Calendar) r3
            r14 = 13
            r3.add(r14, r13)
            com.fossil.i97 r20 = com.fossil.i97.a
            r32 = r0
            r14 = 10
            int r0 = r2.get(r14)
            r14 = 12
            int r20 = r2.get(r14)
            if (r0 != r11) goto L_0x02e1
            int r0 = r20 / 5
            int r14 = r15 / 5
            if (r0 != r14) goto L_0x02e1
            int r0 = r4.getTimezoneOffsetInSecond()
            if (r6 != r0) goto L_0x02e1
            if (r1 <= 0) goto L_0x0246
            int r0 = r17 + 1
            r4.setMinuteCount(r0)
            goto L_0x0248
        L_0x0246:
            r0 = r17
        L_0x0248:
            float r2 = (float) r1
            float r8 = r8 + r2
            if (r1 <= 0) goto L_0x025f
            int r2 = r4.getMin()
            if (r2 != 0) goto L_0x0256
            r4.setMin(r1)
            goto L_0x025f
        L_0x0256:
            int r2 = r4.getMin()
            if (r1 >= r2) goto L_0x025f
            r4.setMin(r1)
        L_0x025f:
            if (r1 <= 0) goto L_0x026a
            int r2 = r4.getMax()
            if (r1 <= r2) goto L_0x026a
            r4.setMax(r1)
        L_0x026a:
            org.joda.time.DateTime r1 = new org.joda.time.DateTime
            java.util.Date r2 = r3.getTime()
            r1.<init>(r2)
            r4.setEndTime(r1)
            if (r0 <= 0) goto L_0x027e
            float r1 = (float) r0
            float r1 = r8 / r1
            r4.setAverage(r1)
        L_0x027e:
            T r1 = r10.element
            java.util.List r1 = (java.util.List) r1
            java.util.Iterator r1 = r1.iterator()
        L_0x0286:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x02d5
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.model.fitnessdata.RestingWrapper r2 = (com.portfolio.platform.data.model.fitnessdata.RestingWrapper) r2
            int r3 = r2.getTimezoneOffsetInSecond()
            int r14 = r4.getTimezoneOffsetInSecond()
            if (r3 != r14) goto L_0x0286
            org.joda.time.DateTime r3 = r4.getStartTime()
            long r34 = r3.getMillis()
            org.joda.time.DateTime r3 = r2.getStartTime()
            long r36 = r3.getMillis()
            int r3 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
            if (r3 > 0) goto L_0x0286
            org.joda.time.DateTime r3 = r2.getStartTime()
            long r34 = r3.getMillis()
            org.joda.time.DateTime r3 = r4.getEndTime()
            long r36 = r3.getMillis()
            int r3 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1))
            if (r3 > 0) goto L_0x0286
            com.portfolio.platform.data.model.diana.heartrate.Resting r3 = new com.portfolio.platform.data.model.diana.heartrate.Resting
            org.joda.time.DateTime r14 = r4.getEndTime()
            int r2 = r2.getValue()
            r3.<init>(r14, r2)
            r4.setResting(r3)
            goto L_0x0286
        L_0x02d5:
            r17 = r0
            r0 = r33
            r14 = r51
            r33 = r9
            r51 = r10
            goto L_0x0442
        L_0x02e1:
            float r0 = r4.getAverage()
            r8 = 0
            float r11 = (float) r8
            int r0 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x0394
            org.joda.time.DateTime r0 = r4.getStartTimeId()
            int r0 = r0.getMinuteOfHour()
            int r0 = r0 % 5
            org.joda.time.DateTime r8 = r4.getStartTimeId()
            org.joda.time.DateTime r0 = r8.minusMinutes(r0)
            com.fossil.ee7.a(r0, r5)
            r4.setStartTimeId(r0)
            r0 = r33
            r0.add(r4)
            java.util.Date r8 = r7.getDate()
            org.joda.time.DateTime r11 = r4.getStartTimeId()
            org.joda.time.LocalDateTime r11 = r11.toLocalDateTime()
            java.util.Date r11 = r11.toDate()
            boolean r8 = com.fossil.zd5.d(r8, r11)
            if (r8 == 0) goto L_0x0327
            com.fossil.rk5 r8 = com.fossil.rk5.b
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r4 = r8.a(r7, r4)
            r7 = r4
            goto L_0x0396
        L_0x0327:
            java.util.Iterator r8 = r51.iterator()
        L_0x032b:
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x0347
            java.lang.Object r11 = r8.next()
            r14 = r11
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r14 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r14
            java.util.Date r14 = r14.getDate()
            java.util.Date r15 = r4.getDate()
            boolean r14 = com.fossil.zd5.d(r14, r15)
            if (r14 == 0) goto L_0x032b
            goto L_0x0349
        L_0x0347:
            r11 = r21
        L_0x0349:
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r11 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r11
            if (r11 == 0) goto L_0x035a
            r14 = r51
            r14.remove(r11)
            com.fossil.rk5 r7 = com.fossil.rk5.b
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r4 = r7.a(r11, r4)
            r7 = r4
            goto L_0x0398
        L_0x035a:
            r14 = r51
            r14.add(r7)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r7 = new com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary
            float r35 = r4.getAverage()
            org.joda.time.DateTime r8 = r4.getStartTimeId()
            org.joda.time.LocalDateTime r8 = r8.toLocalDateTime()
            java.util.Date r8 = r8.toDate()
            com.fossil.ee7.a(r8, r12)
            long r37 = java.lang.System.currentTimeMillis()
            long r39 = java.lang.System.currentTimeMillis()
            int r41 = r4.getMin()
            int r42 = r4.getMax()
            int r43 = r4.getMinuteCount()
            com.portfolio.platform.data.model.diana.heartrate.Resting r44 = r4.getResting()
            r34 = r7
            r36 = r8
            r34.<init>(r35, r36, r37, r39, r41, r42, r43, r44)
            goto L_0x0398
        L_0x0394:
            r0 = r33
        L_0x0396:
            r14 = r51
        L_0x0398:
            r4 = 10
            int r8 = r2.get(r4)
            r11 = 12
            int r15 = r2.get(r11)
            float r4 = (float) r1
            if (r1 <= 0) goto L_0x03aa
            r17 = 1
            goto L_0x03ac
        L_0x03aa:
            r17 = 0
        L_0x03ac:
            com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r20 = new com.portfolio.platform.data.model.diana.heartrate.HeartRateSample
            r34 = r20
            java.util.Date r11 = r2.getTime()
            r36 = r11
            r55 = r7
            java.lang.String r7 = "currentStartTime.time"
            com.fossil.ee7.a(r11, r7)
            long r37 = java.lang.System.currentTimeMillis()
            long r39 = java.lang.System.currentTimeMillis()
            org.joda.time.DateTime r7 = new org.joda.time.DateTime
            r11 = r8
            r33 = r9
            long r8 = r3.getTimeInMillis()
            r7.<init>(r8)
            int r8 = r6 / 3600
            org.joda.time.DateTimeZone r9 = org.joda.time.DateTimeZone.forOffsetHours(r8)
            org.joda.time.DateTime r7 = r7.withZone(r9)
            r41 = r7
            java.lang.String r9 = "DateTime(currentEndTime.\u2026neOffsetInSecond / 3600))"
            com.fossil.ee7.a(r7, r9)
            org.joda.time.DateTime r7 = new org.joda.time.DateTime
            r51 = r10
            long r9 = r2.getTimeInMillis()
            r7.<init>(r9)
            org.joda.time.DateTimeZone r8 = org.joda.time.DateTimeZone.forOffsetHours(r8)
            org.joda.time.DateTime r7 = r7.withZone(r8)
            r42 = r7
            java.lang.String r8 = "DateTime(currentStartTim\u2026neOffsetInSecond / 3600))"
            com.fossil.ee7.a(r7, r8)
            r48 = 0
            r49 = 2048(0x800, float:2.87E-42)
            r50 = 0
            r35 = r4
            r43 = r6
            r44 = r1
            r45 = r1
            r46 = r56
            r47 = r17
            r34.<init>(r35, r36, r37, r39, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r7 = com.fossil.rk5.a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "getHeartRateData startTime="
            r8.append(r9)
            java.util.Date r2 = r2.getTime()
            r8.append(r2)
            java.lang.String r2 = " endTime="
            r8.append(r2)
            java.util.Date r2 = r3.getTime()
            r8.append(r2)
            java.lang.String r2 = r8.toString()
            r1.d(r7, r2)
            r7 = r55
            r8 = r4
            r4 = r20
        L_0x0442:
            com.fossil.i97 r1 = com.fossil.i97.a
            r2 = r0
            r3 = r14
            r14 = r28
            r1 = r30
            r0 = r32
            r9 = r33
            r10 = r51
            goto L_0x01cf
        L_0x0452:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type java.util.Calendar"
            r0.<init>(r1)
            throw r0
        L_0x045a:
            com.fossil.w97.c()
            throw r21
        L_0x045e:
            r0 = r2
            r14 = r3
            r33 = r9
            r51 = r10
            r1 = 12
            r2 = 10
            r23 = 1000(0x3e8, double:4.94E-321)
            com.fossil.i97 r3 = com.fossil.i97.a
            goto L_0x0484
        L_0x046d:
            r26 = r1
            r0 = r2
            r27 = r8
            r33 = r9
            r51 = r10
            r28 = r11
            r23 = r13
            r29 = r15
            r1 = 12
            r2 = 10
            r14 = r3
            r7 = r4
            r4 = r55
        L_0x0484:
            r2 = r0
            r55 = r4
            r4 = r7
            r3 = r14
            r16 = r22
            r13 = r23
            r1 = r26
            r9 = r33
            r10 = r51
            r5 = 13
            r6 = 0
            r7 = 1
            r0 = r54
            goto L_0x0131
        L_0x049b:
            com.fossil.w97.c()
            throw r21
        L_0x049f:
            r0 = r2
            r14 = r3
            float r1 = r55.getAverage()
            r2 = 0
            float r2 = (float) r2
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0555
            org.joda.time.DateTime r1 = r55.getStartTimeId()
            int r1 = r1.getMinuteOfHour()
            int r1 = r1 % 5
            org.joda.time.DateTime r2 = r55.getStartTimeId()
            org.joda.time.DateTime r1 = r2.minusMinutes(r1)
            com.fossil.ee7.a(r1, r5)
            r2 = r55
            r2.setStartTimeId(r1)
            r0.add(r2)
            java.util.Date r1 = r4.getDate()
            org.joda.time.DateTime r3 = r2.getStartTimeId()
            org.joda.time.LocalDateTime r3 = r3.toLocalDateTime()
            java.util.Date r3 = r3.toDate()
            boolean r1 = com.fossil.zd5.d(r1, r3)
            if (r1 == 0) goto L_0x04e9
            r1 = r54
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r2 = r1.a(r4, r2)
            r14.add(r2)
            goto L_0x0562
        L_0x04e9:
            r1 = r54
            java.util.Iterator r3 = r14.iterator()
        L_0x04ef:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x050c
            java.lang.Object r5 = r3.next()
            r6 = r5
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r6 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r6
            java.util.Date r6 = r6.getDate()
            java.util.Date r7 = r2.getDate()
            boolean r6 = com.fossil.zd5.d(r6, r7)
            if (r6 == 0) goto L_0x04ef
            r21 = r5
        L_0x050c:
            r3 = r21
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r3 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r3
            if (r3 == 0) goto L_0x051d
            r14.remove(r3)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r2 = r1.a(r3, r2)
            r14.add(r2)
            goto L_0x0562
        L_0x051d:
            r14.add(r4)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r15 = new com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary
            float r4 = r2.getAverage()
            org.joda.time.DateTime r3 = r2.getStartTimeId()
            org.joda.time.LocalDateTime r3 = r3.toLocalDateTime()
            java.util.Date r5 = r3.toDate()
            com.fossil.ee7.a(r5, r12)
            long r6 = java.lang.System.currentTimeMillis()
            long r8 = java.lang.System.currentTimeMillis()
            int r10 = r2.getMin()
            int r11 = r2.getMax()
            int r12 = r2.getMinuteCount()
            com.portfolio.platform.data.model.diana.heartrate.Resting r13 = r2.getResting()
            r3 = r15
            r3.<init>(r4, r5, r6, r8, r10, r11, r12, r13)
            r14.add(r15)
            goto L_0x0562
        L_0x0555:
            r1 = r54
            float r3 = r4.getAverage()
            int r2 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x0562
            r14.add(r4)
        L_0x0562:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "heartrate "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = " \n summary "
            r3.append(r4)
            r3.append(r14)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "SyncDataExtensions"
            r2.d(r4, r3)
            com.fossil.r87 r2 = new com.fossil.r87
            r2.<init>(r0, r14)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rk5.a(java.util.List, java.lang.String):com.fossil.r87");
    }

    @DexIgnore
    public final List<GFitHeartRate> a(List<FitnessDataWrapper> list) {
        FLogger.INSTANCE.getLocal().d(a, "getGFitHeartRates");
        ArrayList arrayList = new ArrayList();
        ea7.a((Iterable) list, (Comparator) new b());
        for (T t : list) {
            int timezoneOffsetInSecond = t.getTimezoneOffsetInSecond();
            HeartRateWrapper heartRate = t.getHeartRate();
            if (heartRate != null) {
                int component1 = heartRate.component1();
                int i = 0;
                for (T t2 : heartRate.component4()) {
                    int i2 = i + 1;
                    if (i >= 0) {
                        short shortValue = t2.shortValue();
                        Calendar calendar = new DateTime(t.getStartLongTime() + (((long) (i * component1)) * 1000), DateTimeZone.forOffsetMillis(timezoneOffsetInSecond * 1000)).toCalendar(Locale.US);
                        calendar.set(13, 0);
                        calendar.set(14, 0);
                        Object clone = calendar.clone();
                        if (clone != null) {
                            Calendar calendar2 = (Calendar) clone;
                            calendar2.add(13, component1);
                            ee7.a((Object) calendar, "currentStartTime");
                            arrayList.add(new GFitHeartRate((float) shortValue, calendar.getTimeInMillis(), calendar2.getTimeInMillis()));
                            i = i2;
                        } else {
                            throw new x87("null cannot be cast to non-null type java.util.Calendar");
                        }
                    } else {
                        w97.c();
                        throw null;
                    }
                }
                continue;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final DailyHeartRateSummary a(DailyHeartRateSummary dailyHeartRateSummary, HeartRateSample heartRateSample) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "calculateHeartRateSummary - summary=" + dailyHeartRateSummary + ", sample=" + heartRateSample);
        int minuteCount = dailyHeartRateSummary.getMinuteCount() + heartRateSample.getMinuteCount();
        return new DailyHeartRateSummary(((dailyHeartRateSummary.getAverage() * ((float) dailyHeartRateSummary.getMinuteCount())) + (heartRateSample.getAverage() * ((float) heartRateSample.getMinuteCount()))) / ((float) minuteCount), dailyHeartRateSummary.getDate(), dailyHeartRateSummary.getCreatedAt(), System.currentTimeMillis(), Math.min(dailyHeartRateSummary.getMin(), heartRateSample.getMin()), Math.max(dailyHeartRateSummary.getMax(), heartRateSample.getMax()), minuteCount, heartRateSample.getResting() != null ? heartRateSample.getResting() : dailyHeartRateSummary.getResting());
    }

    @DexIgnore
    public final List<WorkoutSession> a(List<FitnessDataWrapper> list, String str, String str2) {
        FLogger.INSTANCE.getLocal().d(a, "getWorkoutData");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        ea7.a((Iterable) list, (Comparator) new e());
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            for (T t : it.next().getWorkouts()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local.d(str3, "getWorkoutData - value=" + ((Object) t));
                arrayList.add(new WorkoutSession(t, str, str2));
            }
        }
        return arrayList;
    }
}
