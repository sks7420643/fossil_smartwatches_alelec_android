package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo4 {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {98}, m = "createChallenge")
    public static final class a extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((qn4) null, 0, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {238}, m = "respond")
    public static final class a0 extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a0(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, false, 0, (String) null, (fb7<? super zi5<mn4>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$createChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {98}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.createChallenge(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$respond$response$1", f = "ChallengeRemoteDataSource.kt", l = {238}, m = "invokeSuspend")
    public static final class b0 extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b0(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b0(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((b0) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.respondInvitation(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {176}, m = "editChallenge")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String) null, (String) null, (Integer) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$editChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {176}, m = "invokeSuspend")
    public static final class d extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(qo4 qo4, String str, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$challengeId = str;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.this$0, this.$challengeId, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$challengeId;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.editChallenge(str, ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {19}, m = "fetchChallengesWithStatus")
    public static final class e extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String[]) null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchChallengesWithStatus$response$1", f = "ChallengeRemoteDataSource.kt", l = {19}, m = "invokeSuspend")
    public static final class f extends zb7 implements gd7<fb7<? super fv7<ApiResponse<mn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qo4 qo4, String[] strArr, int i, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$status = strArr;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new f(this.this$0, this.$status, this.$limit, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<mn4>>> fb7) {
            return ((f) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String[] strArr = this.$status;
                Integer a3 = pb7.a(this.$limit);
                this.label = 1;
                obj = a2.fetchChallengesWithStatus(strArr, a3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {306}, m = "fetchHistoryChallenges")
    public static final class g extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchHistoryChallenges$response$1", f = "ChallengeRemoteDataSource.kt", l = {306}, m = "invokeSuspend")
    public static final class h extends zb7 implements gd7<fb7<? super fv7<ApiResponse<yn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ int $offset;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(qo4 qo4, int i, int i2, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$limit = i;
            this.$offset = i2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new h(this.this$0, this.$limit, this.$offset, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<yn4>>> fb7) {
            return ((h) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                int i2 = this.$limit;
                int i3 = this.$offset;
                this.label = 1;
                obj = a2.fetchHistoryChallenges(i2, i3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {200}, m = "fetchPlayersInChallenge")
    public static final class i extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String[]) null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchPlayersInChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {200}, m = "invokeSuspend")
    public static final class j extends zb7 implements gd7<fb7<? super fv7<ApiResponse<jn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(qo4 qo4, String str, String[] strArr, int i, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$id = str;
            this.$status = strArr;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new j(this.this$0, this.$id, this.$status, this.$limit, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<jn4>>> fb7) {
            return ((j) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$id;
                String[] strArr = this.$status;
                Integer a3 = pb7.a(this.$limit);
                this.label = 1;
                obj = a2.invitedPlayers(str, strArr, a3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {295}, m = "fetchRecommendedChallenges")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$fetchRecommendedChallenges$response$1", f = "ChallengeRemoteDataSource.kt", l = {295}, m = "invokeSuspend")
    public static final class l extends zb7 implements gd7<fb7<? super fv7<ApiResponse<jo4>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(qo4 qo4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new l(this.this$0, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<jo4>>> fb7) {
            return ((l) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                this.label = 1;
                obj = a2.recommendedChallenges(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {55}, m = "getChallenge")
    public static final class m extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {55}, m = "invokeSuspend")
    public static final class n extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(qo4 qo4, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new n(this.this$0, this.$id, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((n) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$id;
                this.label = 1;
                obj = a2.getChallengeById(str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {43}, m = "getFocusedPlayers")
    public static final class o extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((List<String>) null, 0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getFocusedPlayers$response$1", f = "ChallengeRemoteDataSource.kt", l = {43}, m = "invokeSuspend")
    public static final class p extends zb7 implements gd7<fb7<? super fv7<ApiResponse<hn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $challengeIds;
        @DexIgnore
        public /* final */ /* synthetic */ int $near;
        @DexIgnore
        public /* final */ /* synthetic */ int $top;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(qo4 qo4, List list, int i, int i2, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$challengeIds = list;
            this.$top = i;
            this.$near = i2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new p(this.this$0, this.$challengeIds, this.$top, this.$near, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<hn4>>> fb7) {
            return ((p) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                List<String> list = this.$challengeIds;
                int i2 = this.$top;
                int i3 = this.$near;
                this.label = 1;
                obj = a2.getFocusedPlayers(list, i2, i3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {268}, m = "getSyncStatusData")
    public static final class q extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$getSyncStatusData$response$1", f = "ChallengeRemoteDataSource.kt", l = {268}, m = "invokeSuspend")
    public static final class r extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(qo4 qo4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new r(this.this$0, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((r) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                this.label = 1;
                obj = a2.getSyncStatusData(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {220}, m = "invitation")
    public static final class s extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String[]) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$invitation$response$1", f = "ChallengeRemoteDataSource.kt", l = {220}, m = "invokeSuspend")
    public static final class t extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new t(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((t) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.sendInvitation(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {255}, m = "joinChallenge")
    public static final class u extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, 0, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$joinChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {255}, m = "invokeSuspend")
    public static final class v extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new v(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((v) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.joinChallenge(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {141}, m = "leaveChallenge")
    public static final class w extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$leaveChallenge$response$1", f = "ChallengeRemoteDataSource.kt", l = {141}, m = "invokeSuspend")
    public static final class x extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new x(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((x) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.leaveChallenge(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource", f = "ChallengeRemoteDataSource.kt", l = {284}, m = "pushStepData")
    public static final class y extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(qo4 qo4, fb7 fb7) {
            super(fb7);
            this.this$0 = qo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((in4) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource$pushStepData$response$1", f = "ChallengeRemoteDataSource.kt", l = {284}, m = "invokeSuspend")
    public static final class z extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ qo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public z(qo4 qo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = qo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new z(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((z) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.pushStepData(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public qo4(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.qo4.w
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.qo4$w r0 = (com.fossil.qo4.w) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$w r0 = new com.fossil.qo4$w
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "challengeId"
            r10.a(r2, r9)
            com.fossil.qo4$x r2 = new com.fossil.qo4$x
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x008d
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r9 = r10.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0072
            goto L_0x0083
        L_0x0072:
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x007f }
            r0.<init>()     // Catch:{ oe4 -> 0x007f }
            java.lang.Class<com.fossil.mn4> r1 = com.fossil.mn4.class
            java.lang.Object r9 = r0.a(r9, r1)     // Catch:{ oe4 -> 0x007f }
            r4 = r9
            goto L_0x0083
        L_0x007f:
            r9 = move-exception
            r9.printStackTrace()
        L_0x0083:
            boolean r9 = r10.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00ab
        L_0x008d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00ac
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r10 = r9
        L_0x00ab:
            return r10
        L_0x00ac:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.b(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String[] r9, int r10, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.mn4>>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.fossil.qo4.e
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.qo4$e r0 = (com.fossil.qo4.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$e r0 = new com.fossil.qo4$e
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            int r9 = r0.I$0
            java.lang.Object r9 = r0.L$1
            java.lang.String[] r9 = (java.lang.String[]) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r11)
            goto L_0x0053
        L_0x0033:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003b:
            com.fossil.t87.a(r11)
            com.fossil.qo4$f r11 = new com.fossil.qo4$f
            r2 = 0
            r11.<init>(r8, r9, r10, r2)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.I$0 = r10
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r11, r0)
            if (r11 != r1) goto L_0x0053
            return r1
        L_0x0053:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0069
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r10 = r11.a()
            boolean r11 = r11.b()
            r9.<init>(r10, r11)
            goto L_0x0086
        L_0x0069:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0087
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r1 = r11.a()
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0086:
            return r9
        L_0x0087:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String[], int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.util.List<java.lang.String> r11, int r12, int r13, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.hn4>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.fossil.qo4.o
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.fossil.qo4$o r0 = (com.fossil.qo4.o) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$o r0 = new com.fossil.qo4$o
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            int r11 = r0.I$1
            int r11 = r0.I$0
            java.lang.Object r11 = r0.L$1
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.qo4 r11 = (com.fossil.qo4) r11
            com.fossil.t87.a(r14)
            goto L_0x005c
        L_0x0035:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003d:
            com.fossil.t87.a(r14)
            com.fossil.qo4$p r14 = new com.fossil.qo4$p
            r9 = 0
            r4 = r14
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.I$0 = r12
            r0.I$1 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.aj5.a(r14, r0)
            if (r14 != r1) goto L_0x005c
            return r1
        L_0x005c:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r11 = r14 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x0072
            com.fossil.bj5 r11 = new com.fossil.bj5
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            java.lang.Object r12 = r14.a()
            boolean r13 = r14.b()
            r11.<init>(r12, r13)
            goto L_0x008f
        L_0x0072:
            boolean r11 = r14 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0090
            com.fossil.yi5 r11 = new com.fossil.yi5
            com.fossil.yi5 r14 = (com.fossil.yi5) r14
            int r1 = r14.a()
            com.portfolio.platform.data.model.ServerError r2 = r14.c()
            java.lang.Throwable r3 = r14.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x008f:
            return r11
        L_0x0090:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.util.List, int, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.zi5<com.fossil.no4>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.fossil.qo4.q
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.qo4$q r0 = (com.fossil.qo4.q) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$q r0 = new com.fossil.qo4$q
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0036
            if (r2 != r3) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.qo4 r0 = (com.fossil.qo4) r0
            com.fossil.t87.a(r10)
            goto L_0x0049
        L_0x002e:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0036:
            com.fossil.t87.a(r10)
            com.fossil.qo4$r r10 = new com.fossil.qo4$r
            r10.<init>(r9, r4)
            r0.L$0 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0077
            com.fossil.fu4 r0 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            com.fossil.ie4 r0 = (com.fossil.ie4) r0
            if (r0 != 0) goto L_0x005c
            goto L_0x006d
        L_0x005c:
            com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x0069 }
            r1.<init>()     // Catch:{ oe4 -> 0x0069 }
            java.lang.Class<com.fossil.no4> r2 = com.fossil.no4.class
            java.lang.Object r0 = r1.a(r0, r2)     // Catch:{ oe4 -> 0x0069 }
            r4 = r0
            goto L_0x006d
        L_0x0069:
            r0 = move-exception
            r0.printStackTrace()
        L_0x006d:
            boolean r10 = r10.b()
            com.fossil.bj5 r0 = new com.fossil.bj5
            r0.<init>(r4, r10)
            goto L_0x0094
        L_0x0077:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0095
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r3 = r10.c()
            java.lang.Throwable r4 = r10.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0094:
            return r0
        L_0x0095:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.qo4.m
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.qo4$m r0 = (com.fossil.qo4.m) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$m r0 = new com.fossil.qo4$m
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0032:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            com.fossil.t87.a(r10)
            com.fossil.qo4$n r10 = new com.fossil.qo4$n
            r10.<init>(r8, r9, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x007d
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r9 = r10.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0062
            goto L_0x0073
        L_0x0062:
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x006f }
            r0.<init>()     // Catch:{ oe4 -> 0x006f }
            java.lang.Class<com.fossil.mn4> r1 = com.fossil.mn4.class
            java.lang.Object r9 = r0.a(r9, r1)     // Catch:{ oe4 -> 0x006f }
            r4 = r9
            goto L_0x0073
        L_0x006f:
            r9 = move-exception
            r9.printStackTrace()
        L_0x0073:
            boolean r9 = r10.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x009b
        L_0x007d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009c
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r10 = r9
        L_0x009b:
            return r10
        L_0x009c:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.qn4 r10, int r11, java.lang.String r12, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r13) {
        /*
            r9 = this;
            boolean r0 = r13 instanceof com.fossil.qo4.a
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.qo4$a r0 = (com.fossil.qo4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$a r0 = new com.fossil.qo4$a
            r0.<init>(r9, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0045
            if (r2 != r4) goto L_0x003d
            java.lang.Object r10 = r0.L$3
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            int r10 = r0.I$0
            java.lang.Object r10 = r0.L$1
            com.fossil.qn4 r10 = (com.fossil.qn4) r10
            java.lang.Object r10 = r0.L$0
            com.fossil.qo4 r10 = (com.fossil.qo4) r10
            com.fossil.t87.a(r13)
            goto L_0x0102
        L_0x003d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0045:
            com.fossil.t87.a(r13)
            com.fossil.ie4 r13 = new com.fossil.ie4
            r13.<init>()
            java.lang.String r2 = r10.j()
            java.lang.String r5 = "challengeType"
            r13.a(r5, r2)
            java.lang.String r2 = r10.e()
            java.lang.String r5 = "name"
            r13.a(r5, r2)
            int r2 = r10.b()
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            java.lang.String r5 = "duration"
            r13.a(r5, r2)
            int r2 = r10.i()
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            java.lang.String r5 = "target"
            r13.a(r5, r2)
            java.lang.String r2 = r10.f()
            java.lang.String r5 = "privacy"
            r13.a(r5, r2)
            java.lang.String r2 = r10.g()
            java.lang.String r5 = "startTime"
            r13.a(r5, r2)
            java.lang.Integer r2 = com.fossil.pb7.a(r11)
            java.lang.String r5 = "encryptMethod"
            r13.a(r5, r2)
            java.lang.String r2 = "serialNumber"
            r13.a(r2, r12)
            java.lang.String r2 = r10.a()
            r5 = 0
            if (r2 == 0) goto L_0x00b4
            int r2 = r2.length()
            if (r2 <= 0) goto L_0x00a8
            r2 = 1
            goto L_0x00a9
        L_0x00a8:
            r2 = 0
        L_0x00a9:
            if (r2 == 0) goto L_0x00b4
            java.lang.String r2 = r10.a()
            java.lang.String r6 = "description"
            r13.a(r6, r2)
        L_0x00b4:
            boolean r2 = r10.m()
            if (r2 == 0) goto L_0x00c8
            boolean r2 = r10.m()
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            java.lang.String r5 = "isInviteAllFriends"
            r13.a(r5, r2)
            goto L_0x00ea
        L_0x00c8:
            java.lang.String[] r2 = r10.d()
            int r6 = r2.length
            if (r6 != 0) goto L_0x00d1
            r6 = 1
            goto L_0x00d2
        L_0x00d1:
            r6 = 0
        L_0x00d2:
            r6 = r6 ^ r4
            if (r6 == 0) goto L_0x00ea
            com.fossil.de4 r6 = new com.fossil.de4
            r6.<init>()
            int r7 = r2.length
        L_0x00db:
            if (r5 >= r7) goto L_0x00e5
            r8 = r2[r5]
            r6.a(r8)
            int r5 = r5 + 1
            goto L_0x00db
        L_0x00e5:
            java.lang.String r2 = "invitePlayerIds"
            r13.a(r2, r6)
        L_0x00ea:
            com.fossil.qo4$b r2 = new com.fossil.qo4$b
            r2.<init>(r9, r13, r3)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.I$0 = r11
            r0.L$2 = r12
            r0.L$3 = r13
            r0.label = r4
            java.lang.Object r13 = com.fossil.aj5.a(r2, r0)
            if (r13 != r1) goto L_0x0102
            return r1
        L_0x0102:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r10 = r13 instanceof com.fossil.bj5
            if (r10 == 0) goto L_0x0130
            com.fossil.fu4 r10 = com.fossil.fu4.a
            com.fossil.bj5 r13 = (com.fossil.bj5) r13
            java.lang.Object r10 = r13.a()
            com.fossil.ie4 r10 = (com.fossil.ie4) r10
            if (r10 != 0) goto L_0x0115
            goto L_0x0126
        L_0x0115:
            com.google.gson.Gson r11 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x0122 }
            r11.<init>()     // Catch:{ oe4 -> 0x0122 }
            java.lang.Class<com.fossil.mn4> r12 = com.fossil.mn4.class
            java.lang.Object r10 = r11.a(r10, r12)     // Catch:{ oe4 -> 0x0122 }
            r3 = r10
            goto L_0x0126
        L_0x0122:
            r10 = move-exception
            r10.printStackTrace()
        L_0x0126:
            boolean r10 = r13.b()
            com.fossil.bj5 r11 = new com.fossil.bj5
            r11.<init>(r3, r10)
            goto L_0x015f
        L_0x0130:
            boolean r10 = r13 instanceof com.fossil.yi5
            if (r10 == 0) goto L_0x0160
            com.fossil.yi5 r11 = new com.fossil.yi5
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            com.portfolio.platform.data.model.ServerError r10 = r13.c()
            if (r10 == 0) goto L_0x0149
            java.lang.Integer r10 = r10.getCode()
            if (r10 == 0) goto L_0x0149
            int r10 = r10.intValue()
            goto L_0x014d
        L_0x0149:
            int r10 = r13.a()
        L_0x014d:
            r1 = r10
            com.portfolio.platform.data.model.ServerError r2 = r13.c()
            java.lang.Throwable r3 = r13.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x015f:
            return r11
        L_0x0160:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(com.fossil.qn4, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.Integer r12, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r13) {
        /*
            r8 = this;
            boolean r0 = r13 instanceof com.fossil.qo4.c
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.qo4$c r0 = (com.fossil.qo4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$c r0 = new com.fossil.qo4$c
            r0.<init>(r8, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 != r4) goto L_0x0042
            java.lang.Object r9 = r0.L$5
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$4
            java.lang.Integer r9 = (java.lang.Integer) r9
            java.lang.Object r9 = r0.L$3
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r13)
            goto L_0x0092
        L_0x0042:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x004a:
            com.fossil.t87.a(r13)
            com.fossil.ie4 r13 = new com.fossil.ie4
            r13.<init>()
            if (r10 == 0) goto L_0x0059
            java.lang.String r2 = "name"
            r13.a(r2, r10)
        L_0x0059:
            if (r11 == 0) goto L_0x0060
            java.lang.String r2 = "description"
            r13.a(r2, r11)
        L_0x0060:
            if (r12 == 0) goto L_0x006d
            int r2 = r12.intValue()
            if (r2 <= 0) goto L_0x006d
            java.lang.String r2 = "target"
            r13.a(r2, r12)
        L_0x006d:
            int r2 = r13.size()
            if (r2 == 0) goto L_0x0075
            r2 = 1
            goto L_0x0076
        L_0x0075:
            r2 = 0
        L_0x0076:
            if (r2 == 0) goto L_0x00f6
            com.fossil.qo4$d r2 = new com.fossil.qo4$d
            r2.<init>(r8, r9, r13, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.L$4 = r12
            r0.L$5 = r13
            r0.label = r4
            java.lang.Object r13 = com.fossil.aj5.a(r2, r0)
            if (r13 != r1) goto L_0x0092
            return r1
        L_0x0092:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r9 = r13 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00c0
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r13 = (com.fossil.bj5) r13
            java.lang.Object r9 = r13.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x00a5
            goto L_0x00b6
        L_0x00a5:
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x00b2 }
            r10.<init>()     // Catch:{ oe4 -> 0x00b2 }
            java.lang.Class<com.fossil.mn4> r11 = com.fossil.mn4.class
            java.lang.Object r9 = r10.a(r9, r11)     // Catch:{ oe4 -> 0x00b2 }
            r3 = r9
            goto L_0x00b6
        L_0x00b2:
            r9 = move-exception
            r9.printStackTrace()
        L_0x00b6:
            boolean r9 = r13.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r3, r9)
            goto L_0x00ef
        L_0x00c0:
            boolean r9 = r13 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00f0
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            com.portfolio.platform.data.model.ServerError r9 = r13.c()
            if (r9 == 0) goto L_0x00d9
            java.lang.Integer r9 = r9.getCode()
            if (r9 == 0) goto L_0x00d9
            int r9 = r9.intValue()
            goto L_0x00dd
        L_0x00d9:
            int r9 = r13.a()
        L_0x00dd:
            r1 = r9
            com.portfolio.platform.data.model.ServerError r2 = r13.c()
            java.lang.Throwable r3 = r13.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00ef:
            return r10
        L_0x00f0:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        L_0x00f6:
            java.lang.IllegalArgumentException r9 = new java.lang.IllegalArgumentException
            java.lang.String r10 = "Don't have input"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r11, java.lang.String[] r12, int r13, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.jn4>>> r14) {
        /*
            r10 = this;
            boolean r0 = r14 instanceof com.fossil.qo4.i
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.fossil.qo4$i r0 = (com.fossil.qo4.i) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$i r0 = new com.fossil.qo4$i
            r0.<init>(r10, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            int r11 = r0.I$0
            java.lang.Object r11 = r0.L$2
            java.lang.String[] r11 = (java.lang.String[]) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.qo4 r11 = (com.fossil.qo4) r11
            com.fossil.t87.a(r14)
            goto L_0x005e
        L_0x0037:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x003f:
            com.fossil.t87.a(r14)
            com.fossil.qo4$j r14 = new com.fossil.qo4$j
            r9 = 0
            r4 = r14
            r5 = r10
            r6 = r11
            r7 = r12
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.I$0 = r13
            r0.label = r3
            java.lang.Object r14 = com.fossil.aj5.a(r14, r0)
            if (r14 != r1) goto L_0x005e
            return r1
        L_0x005e:
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            boolean r11 = r14 instanceof com.fossil.bj5
            if (r11 == 0) goto L_0x0074
            com.fossil.bj5 r11 = new com.fossil.bj5
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            java.lang.Object r12 = r14.a()
            boolean r13 = r14.b()
            r11.<init>(r12, r13)
            goto L_0x0091
        L_0x0074:
            boolean r11 = r14 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0092
            com.fossil.yi5 r11 = new com.fossil.yi5
            com.fossil.yi5 r14 = (com.fossil.yi5) r14
            int r1 = r14.a()
            com.portfolio.platform.data.model.ServerError r2 = r14.c()
            java.lang.Throwable r3 = r14.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0091:
            return r11
        L_0x0092:
            com.fossil.p87 r11 = new com.fossil.p87
            r11.<init>()
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, java.lang.String[], int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, java.lang.String[] r10, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.fossil.qo4.s
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.qo4$s r0 = (com.fossil.qo4.s) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$s r0 = new com.fossil.qo4$s
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0046
            if (r2 != r4) goto L_0x003e
            java.lang.Object r9 = r0.L$4
            com.fossil.de4 r9 = (com.fossil.de4) r9
            java.lang.Object r9 = r0.L$3
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String[] r9 = (java.lang.String[]) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r11)
            goto L_0x0081
        L_0x003e:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0046:
            com.fossil.t87.a(r11)
            com.fossil.ie4 r11 = new com.fossil.ie4
            r11.<init>()
            com.fossil.de4 r2 = new com.fossil.de4
            r2.<init>()
            int r5 = r10.length
            r6 = 0
        L_0x0055:
            if (r6 >= r5) goto L_0x005f
            r7 = r10[r6]
            r2.a(r7)
            int r6 = r6 + 1
            goto L_0x0055
        L_0x005f:
            java.lang.String r5 = "playerIds"
            r11.a(r5, r2)
            java.lang.String r5 = "challengeId"
            r11.a(r5, r9)
            com.fossil.qo4$t r5 = new com.fossil.qo4$t
            r5.<init>(r8, r11, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.L$4 = r2
            r0.label = r4
            java.lang.Object r11 = com.fossil.aj5.a(r5, r0)
            if (r11 != r1) goto L_0x0081
            return r1
        L_0x0081:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00af
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r9 = r11.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0094
            goto L_0x00a5
        L_0x0094:
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x00a1 }
            r10.<init>()     // Catch:{ oe4 -> 0x00a1 }
            java.lang.Class<com.fossil.mn4> r0 = com.fossil.mn4.class
            java.lang.Object r9 = r10.a(r9, r0)     // Catch:{ oe4 -> 0x00a1 }
            r3 = r9
            goto L_0x00a5
        L_0x00a1:
            r9 = move-exception
            r9.printStackTrace()
        L_0x00a5:
            boolean r9 = r11.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r3, r9)
            goto L_0x00de
        L_0x00af:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00df
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r9 = r11.c()
            if (r9 == 0) goto L_0x00c8
            java.lang.Integer r9 = r9.getCode()
            if (r9 == 0) goto L_0x00c8
            int r9 = r9.intValue()
            goto L_0x00cc
        L_0x00c8:
            int r9 = r11.a()
        L_0x00cc:
            r1 = r9
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00de:
            return r10
        L_0x00df:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, java.lang.String[], com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, boolean r10, int r11, java.lang.String r12, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r13) {
        /*
            r8 = this;
            boolean r0 = r13 instanceof com.fossil.qo4.a0
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.qo4$a0 r0 = (com.fossil.qo4.a0) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$a0 r0 = new com.fossil.qo4$a0
            r0.<init>(r8, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0046
            if (r2 != r3) goto L_0x003e
            java.lang.Object r9 = r0.L$3
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            int r9 = r0.I$0
            boolean r9 = r0.Z$0
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r13)
            goto L_0x0084
        L_0x003e:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0046:
            com.fossil.t87.a(r13)
            com.fossil.ie4 r13 = new com.fossil.ie4
            r13.<init>()
            java.lang.Boolean r2 = com.fossil.pb7.a(r10)
            java.lang.String r5 = "confirmation"
            r13.a(r5, r2)
            java.lang.String r2 = "challengeId"
            r13.a(r2, r9)
            java.lang.Integer r2 = com.fossil.pb7.a(r11)
            java.lang.String r5 = "encryptMethod"
            r13.a(r5, r2)
            java.lang.String r2 = "serialNumber"
            r13.a(r2, r12)
            com.fossil.qo4$b0 r2 = new com.fossil.qo4$b0
            r2.<init>(r8, r13, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.Z$0 = r10
            r0.I$0 = r11
            r0.L$2 = r12
            r0.L$3 = r13
            r0.label = r3
            java.lang.Object r13 = com.fossil.aj5.a(r2, r0)
            if (r13 != r1) goto L_0x0084
            return r1
        L_0x0084:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r9 = r13 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00b2
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r13 = (com.fossil.bj5) r13
            java.lang.Object r9 = r13.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0097
            goto L_0x00a8
        L_0x0097:
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x00a4 }
            r10.<init>()     // Catch:{ oe4 -> 0x00a4 }
            java.lang.Class<com.fossil.mn4> r11 = com.fossil.mn4.class
            java.lang.Object r9 = r10.a(r9, r11)     // Catch:{ oe4 -> 0x00a4 }
            r4 = r9
            goto L_0x00a8
        L_0x00a4:
            r9 = move-exception
            r9.printStackTrace()
        L_0x00a8:
            boolean r9 = r13.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00e1
        L_0x00b2:
            boolean r9 = r13 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00e2
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            com.portfolio.platform.data.model.ServerError r9 = r13.c()
            if (r9 == 0) goto L_0x00cb
            java.lang.Integer r9 = r9.getCode()
            if (r9 == 0) goto L_0x00cb
            int r9 = r9.intValue()
            goto L_0x00cf
        L_0x00cb:
            int r9 = r13.a()
        L_0x00cf:
            r1 = r9
            com.portfolio.platform.data.model.ServerError r2 = r13.c()
            java.lang.Throwable r3 = r13.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00e1:
            return r10
        L_0x00e2:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, boolean, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, int r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.zi5<com.fossil.mn4>> r12) {
        /*
            r8 = this;
            boolean r0 = r12 instanceof com.fossil.qo4.u
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.qo4$u r0 = (com.fossil.qo4.u) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$u r0 = new com.fossil.qo4$u
            r0.<init>(r8, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0044
            if (r2 != r3) goto L_0x003c
            java.lang.Object r9 = r0.L$3
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            int r9 = r0.I$0
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r12)
            goto L_0x0077
        L_0x003c:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0044:
            com.fossil.t87.a(r12)
            com.fossil.ie4 r12 = new com.fossil.ie4
            r12.<init>()
            java.lang.String r2 = "challengeId"
            r12.a(r2, r9)
            java.lang.Integer r2 = com.fossil.pb7.a(r10)
            java.lang.String r5 = "encryptMethod"
            r12.a(r5, r2)
            java.lang.String r2 = "serialNumber"
            r12.a(r2, r11)
            com.fossil.qo4$v r2 = new com.fossil.qo4$v
            r2.<init>(r8, r12, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.I$0 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.label = r3
            java.lang.Object r12 = com.fossil.aj5.a(r2, r0)
            if (r12 != r1) goto L_0x0077
            return r1
        L_0x0077:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r9 = r12 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00a5
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r9 = r12.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x008a
            goto L_0x009b
        L_0x008a:
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x0097 }
            r10.<init>()     // Catch:{ oe4 -> 0x0097 }
            java.lang.Class<com.fossil.mn4> r11 = com.fossil.mn4.class
            java.lang.Object r9 = r10.a(r9, r11)     // Catch:{ oe4 -> 0x0097 }
            r4 = r9
            goto L_0x009b
        L_0x0097:
            r9 = move-exception
            r9.printStackTrace()
        L_0x009b:
            boolean r9 = r12.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00d4
        L_0x00a5:
            boolean r9 = r12 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00d5
            com.fossil.yi5 r10 = new com.fossil.yi5
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            com.portfolio.platform.data.model.ServerError r9 = r12.c()
            if (r9 == 0) goto L_0x00be
            java.lang.Integer r9 = r9.getCode()
            if (r9 == 0) goto L_0x00be
            int r9 = r9.intValue()
            goto L_0x00c2
        L_0x00be:
            int r9 = r12.a()
        L_0x00c2:
            r1 = r9
            com.portfolio.platform.data.model.ServerError r2 = r12.c()
            java.lang.Throwable r3 = r12.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00d4:
            return r10
        L_0x00d5:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(java.lang.String, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.in4 r9, com.fossil.fb7<? super com.fossil.zi5<com.fossil.lo4>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.qo4.y
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.qo4$y r0 = (com.fossil.qo4.y) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$y r0 = new com.fossil.qo4$y
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            com.fossil.in4 r9 = (com.fossil.in4) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r10)
            goto L_0x0063
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = r9.b()
            java.lang.String r5 = "encryptedDataBase64"
            r10.a(r5, r2)
            com.fossil.qo4$z r2 = new com.fossil.qo4$z
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x0063
            return r1
        L_0x0063:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0091
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r9 = r10.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0076
            goto L_0x0087
        L_0x0076:
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x0083 }
            r0.<init>()     // Catch:{ oe4 -> 0x0083 }
            java.lang.Class<com.fossil.lo4> r1 = com.fossil.lo4.class
            java.lang.Object r9 = r0.a(r9, r1)     // Catch:{ oe4 -> 0x0083 }
            r4 = r9
            goto L_0x0087
        L_0x0083:
            r9 = move-exception
            r9.printStackTrace()
        L_0x0087:
            boolean r9 = r10.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00af
        L_0x0091:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00b0
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r10 = r9
        L_0x00af:
            return r10
        L_0x00b0:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(com.fossil.in4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.jo4>>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.fossil.qo4.k
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.qo4$k r0 = (com.fossil.qo4.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$k r0 = new com.fossil.qo4$k
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.qo4 r0 = (com.fossil.qo4) r0
            com.fossil.t87.a(r10)
            goto L_0x0049
        L_0x002d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0035:
            com.fossil.t87.a(r10)
            com.fossil.qo4$l r10 = new com.fossil.qo4$l
            r2 = 0
            r10.<init>(r9, r2)
            r0.L$0 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x005f
            com.fossil.bj5 r0 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r1 = r10.a()
            boolean r10 = r10.b()
            r0.<init>(r1, r10)
            goto L_0x007c
        L_0x005f:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x007d
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r3 = r10.c()
            java.lang.Throwable r4 = r10.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x007c:
            return r0
        L_0x007d:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(int r9, int r10, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.yn4>>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.fossil.qo4.g
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.qo4$g r0 = (com.fossil.qo4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.qo4$g r0 = new com.fossil.qo4$g
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            int r9 = r0.I$1
            int r9 = r0.I$0
            java.lang.Object r9 = r0.L$0
            com.fossil.qo4 r9 = (com.fossil.qo4) r9
            com.fossil.t87.a(r11)
            goto L_0x0051
        L_0x0031:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0039:
            com.fossil.t87.a(r11)
            com.fossil.qo4$h r11 = new com.fossil.qo4$h
            r2 = 0
            r11.<init>(r8, r9, r10, r2)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.I$1 = r10
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r11, r0)
            if (r11 != r1) goto L_0x0051
            return r1
        L_0x0051:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0067
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r10 = r11.a()
            boolean r11 = r11.b()
            r9.<init>(r10, r11)
            goto L_0x0084
        L_0x0067:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0085
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            int r1 = r11.a()
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0084:
            return r9
        L_0x0085:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qo4.a(int, int, com.fossil.fb7):java.lang.Object");
    }
}
