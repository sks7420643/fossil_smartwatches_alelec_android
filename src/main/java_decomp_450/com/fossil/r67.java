package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.List;

public class r67 extends m0 {
    public ListView a;

    public class a implements g {
        public final /* synthetic */ Fragment a;

        public a(r67 r67, Fragment fragment) {
            this.a = fragment;
        }

        @Override // com.fossil.r67.g
        public void a(t67 t67) {
            t67.a(this.a);
        }

        @Override // com.fossil.r67.g
        public Context getContext() {
            return this.a.getContext();
        }
    }

    public class b implements g {
        public final /* synthetic */ FragmentActivity a;

        public b(r67 r67, FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @Override // com.fossil.r67.g
        public void a(t67 t67) {
            t67.a(this.a);
        }

        @Override // com.fossil.r67.g
        public Context getContext() {
            return this.a;
        }
    }

    public class c implements AdapterView.OnItemClickListener {
        public final /* synthetic */ g a;

        public c(g gVar) {
            this.a = gVar;
        }

        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (view.getTag() instanceof t67) {
                this.a.a((t67) view.getTag());
                r67.this.dismiss();
            }
        }
    }

    public static /* synthetic */ class d {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /*
        static {
            /*
                com.fossil.w67[] r0 = com.fossil.w67.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.r67.d.a = r0
                com.fossil.w67 r1 = com.fossil.w67.Camera     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.r67.d.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.w67 r1 = com.fossil.w67.Gallery     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.r67.d.<clinit>():void");
        }
        */
    }

    public class e extends ArrayAdapter<t67> {
        public Context a;

        public e(r67 r67, Context context, int i, List<t67> list) {
            super(context, i, list);
            this.a = context;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.a).inflate(b77.belvedere_dialog_row, viewGroup, false);
            }
            t67 t67 = (t67) getItem(i);
            f a2 = f.a(t67, this.a);
            ((ImageView) view.findViewById(a77.belvedere_dialog_row_image)).setImageDrawable(v6.c(this.a, a2.a()));
            ((TextView) view.findViewById(a77.belvedere_dialog_row_text)).setText(a2.b());
            view.setTag(t67);
            return view;
        }
    }

    public interface g {
        void a(t67 t67);

        Context getContext();
    }

    public static void a(FragmentManager fragmentManager, List<t67> list) {
        if (list != null && list.size() != 0) {
            r67 r67 = new r67();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("extra_intent", new ArrayList<>(list));
            r67.setArguments(bundle);
            r67.show(fragmentManager.b(), "BelvedereDialog");
        }
    }

    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("extra_intent");
        if (getParentFragment() != null) {
            a(new a(this, getParentFragment()), parcelableArrayList);
        } else if (getActivity() != null) {
            a(new b(this, getActivity()), parcelableArrayList);
        } else {
            Log.w("BelvedereDialog", "Not able to find a valid context for starting an BelvedereIntent");
            dismiss();
        }
    }

    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, getTheme());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(b77.belvedere_dialog, viewGroup, false);
        this.a = (ListView) inflate.findViewById(a77.belvedere_dialog_listview);
        return inflate;
    }

    public static class f {
        public final int a;
        public final String b;

        public f(int i, String str) {
            this.a = i;
            this.b = str;
        }

        public static f a(t67 t67, Context context) {
            int i = d.a[t67.a().ordinal()];
            if (i == 1) {
                return new f(z67.ic_camera, context.getString(c77.belvedere_dialog_camera));
            }
            if (i != 2) {
                return new f(-1, context.getString(c77.belvedere_dialog_unknown));
            }
            return new f(z67.ic_image, context.getString(c77.belvedere_dialog_gallery));
        }

        public String b() {
            return this.b;
        }

        public int a() {
            return this.a;
        }
    }

    public final void a(g gVar, List<t67> list) {
        this.a.setAdapter((ListAdapter) new e(this, gVar.getContext(), b77.belvedere_dialog_row, list));
        this.a.setOnItemClickListener(new c(gVar));
    }
}
