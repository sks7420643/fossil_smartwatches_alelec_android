package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b70 extends t60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<b70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final v60[] a(v60... v60Arr) {
            Object[] array = t97.e(v60Arr).toArray(new v60[0]);
            if (array != null) {
                return (v60[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public b70[] newArray(int i) {
            return new b70[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public b70 createFromParcel(Parcel parcel) {
            return (b70) t60.CREATOR.createFromParcel(parcel);
        }
    }

    @DexIgnore
    public b70(w60 w60, f70 f70, x60 x60) {
        super(qi0.REMINDER, CREATOR.a(w60, f70, x60));
    }
}
