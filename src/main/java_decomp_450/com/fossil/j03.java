package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j03 implements tr2<i03> {
    @DexIgnore
    public static j03 b; // = new j03();
    @DexIgnore
    public /* final */ tr2<i03> a;

    @DexIgnore
    public j03(tr2<i03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((i03) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((i03) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ i03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public j03() {
        this(sr2.a(new l03()));
    }
}
