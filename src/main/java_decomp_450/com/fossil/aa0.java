package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<aa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public aa0 createFromParcel(Parcel parcel) {
            return new aa0((short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public aa0[] newArray(int i) {
            return new aa0[i];
        }
    }

    @DexIgnore
    public aa0(short s, short s2, short s3, int i) throws IllegalArgumentException {
        this.a = s;
        this.b = s2;
        this.c = s3;
        this.d = i;
        boolean z = false;
        if (s == -2 || s == -1 || (s >= 0 && 359 >= s)) {
            short s4 = this.b;
            if (s4 == -2 || s4 == -1 || (s4 >= 0 && 359 >= s4)) {
                short s5 = this.c;
                if (s5 == -2 || s5 == -1 || (s5 >= 0 && 359 >= s5)) {
                    int i2 = this.d;
                    if (1000 <= i2 && 60000 >= i2) {
                        z = true;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(yh0.a(yh0.b("durationInMs ("), this.d, ") is out of range ", "[1000, 60000]."));
                    }
                    return;
                }
                StringBuilder b2 = yh0.b("subeyeDegree (");
                b2.append((int) this.c);
                b2.append(") must be equal to ");
                b2.append("-2 or -1 or in range");
                b2.append("[0, 359].");
                throw new IllegalArgumentException(b2.toString());
            }
            StringBuilder b3 = yh0.b("minuteDegree (");
            b3.append((int) this.b);
            b3.append(") must be equal to ");
            b3.append("-2 or -1 or in range");
            b3.append("[0, 359].");
            throw new IllegalArgumentException(b3.toString());
        }
        StringBuilder b4 = yh0.b("hourDegree (");
        b4.append((int) this.a);
        b4.append(") must be equal to ");
        b4.append("-2 or -1 or in range");
        b4.append("[0, 359].");
        throw new IllegalArgumentException(b4.toString());
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.v2, Short.valueOf(this.a)), r51.w2, Short.valueOf(this.b)), r51.x2, Short.valueOf(this.c)), r51.y2, Integer.valueOf(this.d));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort(this.a).putShort(this.b).putShort(this.c).putShort((short) this.d).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(aa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            aa0 aa0 = (aa0) obj;
            return this.a == aa0.a && this.b == aa0.b && this.c == aa0.c && this.d == aa0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig");
    }

    @DexIgnore
    public final int getDurationInMs() {
        return this.d;
    }

    @DexIgnore
    public final short getHourDegree() {
        return this.a;
    }

    @DexIgnore
    public final short getMinuteDegree() {
        return this.b;
    }

    @DexIgnore
    public final short getSubeyeDegree() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
