package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa1 {
    @DexIgnore
    public /* synthetic */ sa1(zd7 zd7) {
    }

    @DexIgnore
    public final mc1 a(byte b) {
        mc1 mc1;
        mc1[] values = mc1.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                mc1 = null;
                break;
            }
            mc1 = values[i];
            if (mc1.b == b) {
                break;
            }
            i++;
        }
        return mc1 != null ? mc1 : mc1.d;
    }
}
