package com.fossil;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg implements rg {
    @DexIgnore
    public static /* final */ rg a; // = new sg();

    @DexIgnore
    public static float a(RecyclerView recyclerView, View view) {
        int childCount = recyclerView.getChildCount();
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float l = da.l(childAt);
                if (l > f) {
                    f = l;
                }
            }
        }
        return f;
    }

    @DexIgnore
    @Override // com.fossil.rg
    public void a(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.rg
    public void b(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z && view.getTag(eg.item_touch_helper_previous_elevation) == null) {
            Float valueOf = Float.valueOf(da.l(view));
            da.a(view, a(recyclerView, view) + 1.0f);
            view.setTag(eg.item_touch_helper_previous_elevation, valueOf);
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }

    @DexIgnore
    @Override // com.fossil.rg
    public void b(View view) {
    }

    @DexIgnore
    @Override // com.fossil.rg
    public void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            Object tag = view.getTag(eg.item_touch_helper_previous_elevation);
            if (tag instanceof Float) {
                da.a(view, ((Float) tag).floatValue());
            }
            view.setTag(eg.item_touch_helper_previous_elevation, null);
        }
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }
}
