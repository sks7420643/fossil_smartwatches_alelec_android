package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl0 extends sh0 {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public nl0(ri1 ri1, byte[] bArr) {
        super(ri1, x81.EXCHANGE_PUBLIC_KEYS, qa1.I, 0, 8);
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 32) {
            byte[] a = s97.a(bArr, 0, 32);
            this.K = a;
            yz0.a(jSONObject, r51.q2, yz0.a(a, (String) null, 1));
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.a, null, null, 27);
        } else {
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.j, null, null, 27);
        }
        ((uh1) this).E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.p2, yz0.a(this.L, (String) null, 1));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.q2, yz0.a(this.K, (String) null, 1));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(this.L.length).order(ByteOrder.LITTLE_ENDIAN).put(this.L).array();
        ee7.a((Object) array, "ByteBuffer.allocate(phon\u2026\n                .array()");
        return array;
    }
}
