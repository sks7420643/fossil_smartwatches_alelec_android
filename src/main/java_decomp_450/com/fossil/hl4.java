package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.b87;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.RingStyleRepository_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FileRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaPresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesNotificationRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesRingStyleDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.QuickResponseRepository_Factory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.ThemeRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.UserSettingDao;
import com.portfolio.platform.data.source.UserSettingDatabase;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDataDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource_Factory;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl4 implements tj4 {
    @DexIgnore
    public Provider<UserRepository> A;
    @DexIgnore
    public Provider<xo4> A0;
    @DexIgnore
    public Provider<ThirdPartyRepository> A1;
    @DexIgnore
    public Provider<aq4> A2;
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> B;
    @DexIgnore
    public Provider<nn4> B0;
    @DexIgnore
    public Provider<PushPendingDataWorker.b> B1;
    @DexIgnore
    public Provider<ct4> B2;
    @DexIgnore
    public Provider<AlarmsRepository> C;
    @DexIgnore
    public Provider<po4> C0;
    @DexIgnore
    public Provider<nh5> C1;
    @DexIgnore
    public Provider<zq4> C2;
    @DexIgnore
    public Provider<pd5> D;
    @DexIgnore
    public Provider<qo4> D0;
    @DexIgnore
    public Provider<AddressDatabase> D1;
    @DexIgnore
    public Provider<tr4> D2;
    @DexIgnore
    public Provider<ge5> E;
    @DexIgnore
    public Provider<ro4> E0;
    @DexIgnore
    public Provider<AddressDao> E1;
    @DexIgnore
    public Provider<kp4> E2;
    @DexIgnore
    public Provider<SummariesRepository> F;
    @DexIgnore
    public Provider<co4> F0;
    @DexIgnore
    public Provider<LocationSource> F1;
    @DexIgnore
    public Provider<vp4> F2;
    @DexIgnore
    public Provider<SleepSummariesRepository> G;
    @DexIgnore
    public Provider<zo4> G0;
    @DexIgnore
    public Provider<wk5> G1;
    @DexIgnore
    public Provider<js4> G2;
    @DexIgnore
    public Provider<GuestApiService> H;
    @DexIgnore
    public Provider<ap4> H0;
    @DexIgnore
    public Provider<ri5> H1;
    @DexIgnore
    public Provider<us4> H2;
    @DexIgnore
    public Provider<pj4> I;
    @DexIgnore
    public Provider<bp4> I0;
    @DexIgnore
    public Provider<ui5> I1;
    @DexIgnore
    public Provider<os4> I2;
    @DexIgnore
    public Provider<NotificationsDataSource> J;
    @DexIgnore
    public Provider<FileDatabase> J0;
    @DexIgnore
    public Provider<vm4> J1;
    @DexIgnore
    public Provider<mr4> J2;
    @DexIgnore
    public Provider<NotificationsRepository> K;
    @DexIgnore
    public Provider<FileDao> K0;
    @DexIgnore
    public Provider<kw6> K1;
    @DexIgnore
    public Provider<sq4> K2;
    @DexIgnore
    public Provider<DeviceDatabase> L;
    @DexIgnore
    public Provider<qg5> L0;
    @DexIgnore
    public Provider<ek5> L1;
    @DexIgnore
    public Provider<gr4> L2;
    @DexIgnore
    public Provider<DeviceDao> M;
    @DexIgnore
    public Provider<FileRepository> M0;
    @DexIgnore
    public Provider<vg5> M1;
    @DexIgnore
    public Provider<gq4> M2;
    @DexIgnore
    public Provider<SkuDao> N;
    @DexIgnore
    public Provider<WatchFaceDao> N0;
    @DexIgnore
    public Provider<ff5> N1;
    @DexIgnore
    public Provider<kt4> N2;
    @DexIgnore
    public Provider<DeviceRemoteDataSource> O;
    @DexIgnore
    public Provider<RingStyleDao> O0;
    @DexIgnore
    public Provider<pk5> O1;
    @DexIgnore
    public Provider<dm5> O2;
    @DexIgnore
    public Provider<DeviceRepository> P;
    @DexIgnore
    public Provider<to4> P0;
    @DexIgnore
    public Provider<ih5> P1;
    @DexIgnore
    public Provider<fv5> P2;
    @DexIgnore
    public Provider<ContentResolver> Q;
    @DexIgnore
    public Provider<WatchAppDataDao> Q0;
    @DexIgnore
    public Provider<lh5> Q1;
    @DexIgnore
    public Provider<ym6> Q2;
    @DexIgnore
    public Provider<rl4> R;
    @DexIgnore
    public Provider<qd5> R0;
    @DexIgnore
    public Provider<kh5> R1;
    @DexIgnore
    public Provider<tl6> R2;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> S;
    @DexIgnore
    public Provider<CategoryDatabase> S0;
    @DexIgnore
    public Provider<MicroAppLastSettingRepository> S1;
    @DexIgnore
    public Provider<nl6> S2;
    @DexIgnore
    public Provider<HybridPresetDao> T;
    @DexIgnore
    public Provider<CategoryDao> T0;
    @DexIgnore
    public Provider<ll4> T1;
    @DexIgnore
    public Provider<mm6> T2;
    @DexIgnore
    public Provider<HybridPresetRemoteDataSource> U;
    @DexIgnore
    public Provider<CategoryRemoteDataSource> U0;
    @DexIgnore
    public Provider<FirmwareFileRepository> U1;
    @DexIgnore
    public Provider<hl6> U2;
    @DexIgnore
    public Provider<HybridPresetRepository> V;
    @DexIgnore
    public Provider<CategoryRepository> V0;
    @DexIgnore
    public Provider<gn5> V1;
    @DexIgnore
    public Provider<bl6> V2;
    @DexIgnore
    public Provider<ActivitiesRepository> W;
    @DexIgnore
    public Provider<WatchAppRepository> W0;
    @DexIgnore
    public Provider<InAppNotificationDatabase> W1;
    @DexIgnore
    public Provider<vk6> W2;
    @DexIgnore
    public Provider<ShortcutApiService> X;
    @DexIgnore
    public Provider<ComplicationRepository> X0;
    @DexIgnore
    public Provider<InAppNotificationDao> X1;
    @DexIgnore
    public Provider<am6> X2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Y;
    @DexIgnore
    public Provider<WatchFaceRemoteDataSource> Y0;
    @DexIgnore
    public Provider<InAppNotificationRepository> Y1;
    @DexIgnore
    public Provider<gm6> Y2;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> Z;
    @DexIgnore
    public Provider<WatchFaceRepository> Z0;
    @DexIgnore
    public Provider<co6> Z1;
    @DexIgnore
    public Provider<sm6> Z2;
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public Provider<MicroAppSettingRepository> a0;
    @DexIgnore
    public Provider<RingStyleRemoteDataSource> a1;
    @DexIgnore
    public Provider<GoogleApiService> a2;
    @DexIgnore
    public Provider<fn6> a3;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<SleepSessionsRepository> b0;
    @DexIgnore
    public Provider<RingStyleRepository> b1;
    @DexIgnore
    public Provider<qe> b2;
    @DexIgnore
    public Provider<bv5> b3;
    @DexIgnore
    public Provider<ch5> c;
    @DexIgnore
    public Provider<GoalTrackingRepository> c0;
    @DexIgnore
    public Provider<WatchLocalizationRepository> c1;
    @DexIgnore
    public Provider<df5> c2;
    @DexIgnore
    public Provider<bh6> c3;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<jh5> d0;
    @DexIgnore
    public Provider<gs6> d1;
    @DexIgnore
    public Provider<nj4> d2;
    @DexIgnore
    public Provider<am5> d3;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<WatchAppDao> e0;
    @DexIgnore
    public Provider<is6> e1;
    @DexIgnore
    public Provider<ComplicationLastSettingRepository> e2;
    @DexIgnore
    public Provider<ui6> e3;
    @DexIgnore
    public Provider<QuickResponseDatabase> f;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> f0;
    @DexIgnore
    public Provider<ul5> f1;
    @DexIgnore
    public Provider<WatchAppLastSettingRepository> f2;
    @DexIgnore
    public Provider<Map<Class<? extends he>, Provider<he>>> f3;
    @DexIgnore
    public Provider<QuickResponseMessageDao> g;
    @DexIgnore
    public Provider<ComplicationDao> g0;
    @DexIgnore
    public Provider<aw6> g1;
    @DexIgnore
    public Provider<a06> g2;
    @DexIgnore
    public Provider<rj4> g3;
    @DexIgnore
    public Provider<QuickResponseSenderDao> h;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> h0;
    @DexIgnore
    public Provider<nw6> h1;
    @DexIgnore
    public Provider<j56> h2;
    @DexIgnore
    public Provider<NotificationSettingsDao> h3;
    @DexIgnore
    public Provider<QuickResponseRepository> i;
    @DexIgnore
    public Provider<MicroAppDao> i0;
    @DexIgnore
    public Provider<pm5> i1;
    @DexIgnore
    public Provider<on5> i2;
    @DexIgnore
    public Provider<ph5> i3;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> j;
    @DexIgnore
    public Provider<MicroAppRemoteDataSource> j0;
    @DexIgnore
    public Provider<WorkoutSettingRemoteDataSource> j1;
    @DexIgnore
    public Provider<mn5> j2;
    @DexIgnore
    public Provider<ServerSettingDataSource> j3;
    @DexIgnore
    public Provider<mk5> k;
    @DexIgnore
    public Provider<MicroAppRepository> k0;
    @DexIgnore
    public Provider<WorkoutSettingRepository> k1;
    @DexIgnore
    public Provider<oi6> k2;
    @DexIgnore
    public Provider<ServerSettingDataSource> k3;
    @DexIgnore
    public Provider<DianaCustomizeDatabase> l;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> l0;
    @DexIgnore
    public Provider<rl5> l1;
    @DexIgnore
    public Provider<j36> l2;
    @DexIgnore
    public Provider<wx6> l3;
    @DexIgnore
    public Provider<DianaPresetDao> m;
    @DexIgnore
    public Provider<ComplicationLastSettingDao> m0;
    @DexIgnore
    public Provider<ad5> m1;
    @DexIgnore
    public Provider<fm5> m2;
    @DexIgnore
    public Provider<AuthApiGuestService> n;
    @DexIgnore
    public Provider<WatchAppLastSettingDao> n0;
    @DexIgnore
    public Provider<AppSettingsDatabase> n1;
    @DexIgnore
    public Provider<mm5> n2;
    @DexIgnore
    public Provider<hj5> o;
    @DexIgnore
    public Provider<HeartRateSampleRepository> o0;
    @DexIgnore
    public Provider<hm4> o1;
    @DexIgnore
    public Provider<hm5> o2;
    @DexIgnore
    public Provider<lj5> p;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> p0;
    @DexIgnore
    public Provider<jm4> p1;
    @DexIgnore
    public Provider<pu6> p2;
    @DexIgnore
    public Provider<ApiServiceV2> q;
    @DexIgnore
    public Provider<WorkoutSessionRepository> q0;
    @DexIgnore
    public Provider<km4> q1;
    @DexIgnore
    public Provider<e46> q2;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> r;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> r0;
    @DexIgnore
    public Provider<lm4> r1;
    @DexIgnore
    public Provider<b46> r2;
    @DexIgnore
    public Provider<DianaPresetRepository> s;
    @DexIgnore
    public Provider<BuddyChallengeDatabase> s0;
    @DexIgnore
    public Provider<uj5> s1;
    @DexIgnore
    public Provider<pk6> s2;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> t;
    @DexIgnore
    public Provider<go4> t0;
    @DexIgnore
    public Provider<ThemeDatabase> t1;
    @DexIgnore
    public Provider<sn6> t2;
    @DexIgnore
    public Provider<CustomizeRealDataDao> u;
    @DexIgnore
    public Provider<dp4> u0;
    @DexIgnore
    public Provider<ThemeDao> u1;
    @DexIgnore
    public Provider<zn6> u2;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> v;
    @DexIgnore
    public Provider<ep4> v0;
    @DexIgnore
    public Provider<ThemeRepository> v1;
    @DexIgnore
    public Provider<um5> v2;
    @DexIgnore
    public Provider<AuthApiUserService> w;
    @DexIgnore
    public Provider<fp4> w0;
    @DexIgnore
    public Provider<ApplicationEventListener> w1;
    @DexIgnore
    public Provider<rm5> w2;
    @DexIgnore
    public Provider<UserRemoteDataSource> x;
    @DexIgnore
    public Provider<vn4> x0;
    @DexIgnore
    public Provider<sj5> x1;
    @DexIgnore
    public Provider<to6> x2;
    @DexIgnore
    public Provider<UserSettingDatabase> y;
    @DexIgnore
    public Provider<vo4> y0;
    @DexIgnore
    public Provider<FitnessDataRepository> y1;
    @DexIgnore
    public Provider<yr4> y2;
    @DexIgnore
    public Provider<UserSettingDao> z;
    @DexIgnore
    public Provider<wo4> z0;
    @DexIgnore
    public Provider<ie5> z1;
    @DexIgnore
    public Provider<nq4> z2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements tu6 {
        @DexIgnore
        public /* final */ wu6 a;

        @DexIgnore
        public final yu6 a() {
            yu6 a2 = av6.a((PortfolioApp) hl4.this.d.get(), xu6.a(this.a), (UserRepository) hl4.this.A.get(), (qe) hl4.this.b2.get(), (df5) hl4.this.c2.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity b(CalibrationActivity calibrationActivity) {
            dl5.a(calibrationActivity, (UserRepository) hl4.this.A.get());
            dl5.a(calibrationActivity, (ch5) hl4.this.c.get());
            dl5.a(calibrationActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(calibrationActivity, (ll4) hl4.this.T1.get());
            dl5.a(calibrationActivity, new xm5());
            su6.a(calibrationActivity, a());
            return calibrationActivity;
        }

        @DexIgnore
        public a0(wu6 wu6) {
            this.a = wu6;
        }

        @DexIgnore
        @Override // com.fossil.tu6
        public void a(CalibrationActivity calibrationActivity) {
            b(calibrationActivity);
        }

        @DexIgnore
        public final yu6 a(yu6 yu6) {
            bv6.a(yu6);
            return yu6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements z26 {
        @DexIgnore
        @Override // com.fossil.z26
        public void a(a36 a36) {
            b(a36);
        }

        @DexIgnore
        public final a36 b(a36 a36) {
            b36.a(a36, (rj4) hl4.this.g3.get());
            return a36;
        }

        @DexIgnore
        public a1() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a2 implements lu5 {
        @DexIgnore
        public /* final */ qu5 a;

        @DexIgnore
        public final su5 a() {
            su5 a2 = tu5.a(ru5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        @Override // com.fossil.lu5
        public void a(gt5 gt5) {
        }

        @DexIgnore
        public final rt5 b(rt5 rt5) {
            st5.a(rt5, a());
            st5.a(rt5, (rj4) hl4.this.g3.get());
            return rt5;
        }

        @DexIgnore
        public a2(qu5 qu5) {
            this.a = qu5;
        }

        @DexIgnore
        @Override // com.fossil.lu5
        public void a(rt5 rt5) {
            b(rt5);
        }

        @DexIgnore
        @Override // com.fossil.lu5
        public void a(ou5 ou5) {
            b(ou5);
        }

        @DexIgnore
        public final ou5 b(ou5 ou5) {
            pu5.a(ou5, (rj4) hl4.this.g3.get());
            return ou5;
        }

        @DexIgnore
        public final su5 a(su5 su5) {
            uu5.a(su5);
            return su5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a3 implements q46 {
        @DexIgnore
        public /* final */ u46 a;

        @DexIgnore
        public final w46 a() {
            w46 a2 = x46.a(v46.a(this.a), hl4.this.F0(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity b(WatchAppSearchActivity watchAppSearchActivity) {
            dl5.a(watchAppSearchActivity, (UserRepository) hl4.this.A.get());
            dl5.a(watchAppSearchActivity, (ch5) hl4.this.c.get());
            dl5.a(watchAppSearchActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(watchAppSearchActivity, (ll4) hl4.this.T1.get());
            dl5.a(watchAppSearchActivity, new xm5());
            p46.a(watchAppSearchActivity, a());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public a3(u46 u46) {
            this.a = u46;
        }

        @DexIgnore
        @Override // com.fossil.q46
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            b(watchAppSearchActivity);
        }

        @DexIgnore
        public final w46 a(w46 w46) {
            y46.a(w46);
            return w46;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements sh6 {
        @DexIgnore
        public /* final */ wh6 a;

        @DexIgnore
        public final yh6 a() {
            yh6 a2 = zh6.a(xh6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity b(AboutActivity aboutActivity) {
            dl5.a(aboutActivity, (UserRepository) hl4.this.A.get());
            dl5.a(aboutActivity, (ch5) hl4.this.c.get());
            dl5.a(aboutActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(aboutActivity, (ll4) hl4.this.T1.get());
            dl5.a(aboutActivity, new xm5());
            rh6.a(aboutActivity, a());
            return aboutActivity;
        }

        @DexIgnore
        public b(wh6 wh6) {
            this.a = wh6;
        }

        @DexIgnore
        @Override // com.fossil.sh6
        public void a(AboutActivity aboutActivity) {
            b(aboutActivity);
        }

        @DexIgnore
        public final yh6 a(yh6 yh6) {
            ai6.a(yh6);
            return yh6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements ef6 {
        @DexIgnore
        public /* final */ if6 a;

        @DexIgnore
        public final kf6 a() {
            kf6 a2 = lf6.a(jf6.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (UserRepository) hl4.this.A.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (FileRepository) hl4.this.M0.get(), (pj4) hl4.this.I.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity b(CaloriesDetailActivity caloriesDetailActivity) {
            dl5.a(caloriesDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(caloriesDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(caloriesDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(caloriesDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(caloriesDetailActivity, new xm5());
            df6.a(caloriesDetailActivity, a());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public b0(if6 if6) {
            this.a = if6;
        }

        @DexIgnore
        @Override // com.fossil.ef6
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            b(caloriesDetailActivity);
        }

        @DexIgnore
        public final kf6 a(kf6 kf6) {
            mf6.a(kf6);
            return kf6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements ot6 {
        @DexIgnore
        public /* final */ pt6 a;

        @DexIgnore
        public final lt6 a() {
            lt6 a2 = mt6.a(qt6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final mw6 b() {
            return new mw6((UserRepository) hl4.this.A.get());
        }

        @DexIgnore
        public b1(pt6 pt6) {
            this.a = pt6;
        }

        @DexIgnore
        @Override // com.fossil.ot6
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            b(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final EmailOtpVerificationActivity b(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            dl5.a(emailOtpVerificationActivity, (UserRepository) hl4.this.A.get());
            dl5.a(emailOtpVerificationActivity, (ch5) hl4.this.c.get());
            dl5.a(emailOtpVerificationActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(emailOtpVerificationActivity, (ll4) hl4.this.T1.get());
            dl5.a(emailOtpVerificationActivity, new xm5());
            ht6.a(emailOtpVerificationActivity, a());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final lt6 a(lt6 lt6) {
            nt6.a(lt6, hl4.this.q0());
            nt6.a(lt6, b());
            nt6.a(lt6);
            return lt6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b2 implements ov5 {
        @DexIgnore
        public /* final */ tv5 a;

        @DexIgnore
        public final vv5 a() {
            vv5 a2 = wv5.a(uv5.a(this.a), (ch5) hl4.this.c.get(), (RemindersSettingsDatabase) hl4.this.r0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity b(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            dl5.a(notificationWatchRemindersActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationWatchRemindersActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationWatchRemindersActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationWatchRemindersActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationWatchRemindersActivity, new xm5());
            nv5.a(notificationWatchRemindersActivity, a());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public b2(tv5 tv5) {
            this.a = tv5;
        }

        @DexIgnore
        @Override // com.fossil.ov5
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            b(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final vv5 a(vv5 vv5) {
            xv5.a(vv5);
            return vv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b3 implements l36 {
        @DexIgnore
        @Override // com.fossil.l36
        public void a(o36 o36) {
            b(o36);
        }

        @DexIgnore
        public final o36 b(o36 o36) {
            p36.a(o36, (rj4) hl4.this.g3.get());
            return o36;
        }

        @DexIgnore
        public b3(q36 q36) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements he6 {
        @DexIgnore
        public /* final */ le6 a;

        @DexIgnore
        public final ne6 a() {
            ne6 a2 = oe6.a(me6.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (UserRepository) hl4.this.A.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (FileRepository) hl4.this.M0.get(), (pj4) hl4.this.I.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity b(ActiveTimeDetailActivity activeTimeDetailActivity) {
            dl5.a(activeTimeDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(activeTimeDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(activeTimeDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(activeTimeDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(activeTimeDetailActivity, new xm5());
            ge6.a(activeTimeDetailActivity, a());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public c(le6 le6) {
            this.a = le6;
        }

        @DexIgnore
        @Override // com.fossil.he6
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            b(activeTimeDetailActivity);
        }

        @DexIgnore
        public final ne6 a(ne6 ne6) {
            pe6.a(ne6);
            return ne6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements v96 {
        @DexIgnore
        public /* final */ da6 a;

        @DexIgnore
        public final z96 a() {
            z96 a2 = aa6.a(ea6.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ka6 b() {
            ka6 a2 = la6.a(fa6.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final qa6 c() {
            qa6 a2 = ra6.a(ga6.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public c0(da6 da6) {
            this.a = da6;
        }

        @DexIgnore
        @Override // com.fossil.v96
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            b(caloriesOverviewFragment);
        }

        @DexIgnore
        public final CaloriesOverviewFragment b(CaloriesOverviewFragment caloriesOverviewFragment) {
            ca6.a(caloriesOverviewFragment, a());
            ca6.a(caloriesOverviewFragment, c());
            ca6.a(caloriesOverviewFragment, b());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final z96 a(z96 z96) {
            ba6.a(z96);
            return z96;
        }

        @DexIgnore
        public final qa6 a(qa6 qa6) {
            sa6.a(qa6);
            return qa6;
        }

        @DexIgnore
        public final ka6 a(ka6 ka6) {
            ma6.a(ka6);
            return ka6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements wo6 {
        @DexIgnore
        public /* final */ zo6 a;

        @DexIgnore
        public final bp6 a() {
            bp6 a2 = cp6.a(ap6.a(this.a), hl4.this.N(), (DeviceRepository) hl4.this.P.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity b(ExploreWatchActivity exploreWatchActivity) {
            dl5.a(exploreWatchActivity, (UserRepository) hl4.this.A.get());
            dl5.a(exploreWatchActivity, (ch5) hl4.this.c.get());
            dl5.a(exploreWatchActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(exploreWatchActivity, (ll4) hl4.this.T1.get());
            dl5.a(exploreWatchActivity, new xm5());
            vo6.a(exploreWatchActivity, a());
            return exploreWatchActivity;
        }

        @DexIgnore
        public c1(zo6 zo6) {
            this.a = zo6;
        }

        @DexIgnore
        @Override // com.fossil.wo6
        public void a(ExploreWatchActivity exploreWatchActivity) {
            b(exploreWatchActivity);
        }

        @DexIgnore
        public final bp6 a(bp6 bp6) {
            dp6.a(bp6);
            return bp6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c2 implements op6 {
        @DexIgnore
        public /* final */ rp6 a;

        @DexIgnore
        public final ew6 a() {
            return new ew6((SummariesRepository) hl4.this.F.get(), (SleepSummariesRepository) hl4.this.G.get(), (SleepSessionsRepository) hl4.this.b0.get());
        }

        @DexIgnore
        public final tp6 b() {
            tp6 a2 = up6.a(sp6.a(this.a), (UserRepository) hl4.this.A.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public c2(rp6 rp6) {
            this.a = rp6;
        }

        @DexIgnore
        @Override // com.fossil.op6
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            b(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity b(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            dl5.a(onboardingHeightWeightActivity, (UserRepository) hl4.this.A.get());
            dl5.a(onboardingHeightWeightActivity, (ch5) hl4.this.c.get());
            dl5.a(onboardingHeightWeightActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(onboardingHeightWeightActivity, (ll4) hl4.this.T1.get());
            dl5.a(onboardingHeightWeightActivity, new xm5());
            np6.a(onboardingHeightWeightActivity, b());
            return onboardingHeightWeightActivity;
        }

        @DexIgnore
        public final tp6 a(tp6 tp6) {
            vp6.a(tp6);
            return tp6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c3 implements mu6 {
        @DexIgnore
        @Override // com.fossil.mu6
        public void a(nu6 nu6) {
            b(nu6);
        }

        @DexIgnore
        public final nu6 b(nu6 nu6) {
            ou6.a(nu6, (rj4) hl4.this.g3.get());
            return nu6;
        }

        @DexIgnore
        public c3() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements m76 {
        @DexIgnore
        public /* final */ u76 a;

        @DexIgnore
        public final q76 a() {
            q76 a2 = r76.a(v76.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final b86 b() {
            b86 a2 = c86.a(w76.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final h86 c() {
            h86 a2 = i86.a(x76.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d(u76 u76) {
            this.a = u76;
        }

        @DexIgnore
        @Override // com.fossil.m76
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            b(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            t76.a(activeTimeOverviewFragment, a());
            t76.a(activeTimeOverviewFragment, c());
            t76.a(activeTimeOverviewFragment, b());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final q76 a(q76 q76) {
            s76.a(q76);
            return q76;
        }

        @DexIgnore
        public final h86 a(h86 h86) {
            j86.a(h86);
            return h86;
        }

        @DexIgnore
        public final b86 a(b86 b86) {
            d86.a(b86);
            return b86;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements u06 {
        @DexIgnore
        public /* final */ g16 a;

        @DexIgnore
        public final i16 a() {
            i16 a2 = j16.a(h16.a(this.a), (ch5) hl4.this.c.get(), (UserRepository) hl4.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity b(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            dl5.a(commuteTimeSettingsActivity, (UserRepository) hl4.this.A.get());
            dl5.a(commuteTimeSettingsActivity, (ch5) hl4.this.c.get());
            dl5.a(commuteTimeSettingsActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(commuteTimeSettingsActivity, (ll4) hl4.this.T1.get());
            dl5.a(commuteTimeSettingsActivity, new xm5());
            t06.a(commuteTimeSettingsActivity, a());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public d0(g16 g16) {
            this.a = g16;
        }

        @DexIgnore
        @Override // com.fossil.u06
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            b(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final i16 a(i16 i16) {
            k16.a(i16);
            return i16;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements dv6 {
        @DexIgnore
        public /* final */ gv6 a;

        @DexIgnore
        public final iv6 a() {
            iv6 a2 = jv6.a((qe) hl4.this.b2.get(), (DeviceRepository) hl4.this.P.get(), (ch5) hl4.this.c.get(), hv6.a(this.a), new tm5(), c(), b(), new xm5(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final rm5 b() {
            return new rm5((GoogleApiService) hl4.this.a2.get());
        }

        @DexIgnore
        public final wm5 c() {
            return new wm5((nj4) hl4.this.d2.get());
        }

        @DexIgnore
        public d1(gv6 gv6) {
            this.a = gv6;
        }

        @DexIgnore
        @Override // com.fossil.dv6
        public void a(FindDeviceActivity findDeviceActivity) {
            b(findDeviceActivity);
        }

        @DexIgnore
        public final FindDeviceActivity b(FindDeviceActivity findDeviceActivity) {
            dl5.a(findDeviceActivity, (UserRepository) hl4.this.A.get());
            dl5.a(findDeviceActivity, (ch5) hl4.this.c.get());
            dl5.a(findDeviceActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(findDeviceActivity, (ll4) hl4.this.T1.get());
            dl5.a(findDeviceActivity, new xm5());
            cv6.a(findDeviceActivity, a());
            return findDeviceActivity;
        }

        @DexIgnore
        public final iv6 a(iv6 iv6) {
            kv6.a(iv6);
            return iv6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d2 implements or6 {
        @DexIgnore
        public /* final */ vr6 a;

        @DexIgnore
        public final tl5 a() {
            return new tl5((PortfolioApp) hl4.this.d.get(), (GuestApiService) hl4.this.H.get());
        }

        @DexIgnore
        public final LabelRemoteDataSource b() {
            return new LabelRemoteDataSource((ApiServiceV2) hl4.this.q.get());
        }

        @DexIgnore
        public final LabelRepository c() {
            return new LabelRepository((Context) hl4.this.b.get(), b());
        }

        @DexIgnore
        public final wl5 d() {
            return new wl5((DeviceRepository) hl4.this.P.get(), a(), (ch5) hl4.this.c.get(), (UserRepository) hl4.this.A.get(), hl4.this.L(), hl4.this.N(), c(), (FileRepository) hl4.this.M0.get());
        }

        @DexIgnore
        public final xr6 e() {
            xr6 a2 = zr6.a(wr6.a(this.a), d(), (DeviceRepository) hl4.this.P.get(), hl4.this.N(), (ek5) hl4.this.L1.get(), hl4.this.t0(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d2(vr6 vr6) {
            this.a = vr6;
        }

        @DexIgnore
        @Override // com.fossil.or6
        public void a(PairingActivity pairingActivity) {
            b(pairingActivity);
        }

        @DexIgnore
        public final PairingActivity b(PairingActivity pairingActivity) {
            dl5.a(pairingActivity, (UserRepository) hl4.this.A.get());
            dl5.a(pairingActivity, (ch5) hl4.this.c.get());
            dl5.a(pairingActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(pairingActivity, (ll4) hl4.this.T1.get());
            dl5.a(pairingActivity, new xm5());
            nr6.a(pairingActivity, e());
            return pairingActivity;
        }

        @DexIgnore
        public final xr6 a(xr6 xr6) {
            as6.a(xr6);
            return xr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d3 implements h46 {
        @DexIgnore
        public /* final */ k46 a;

        @DexIgnore
        public final m46 a() {
            m46 a2 = n46.a(l46.a(this.a), (GoogleApiService) hl4.this.a2.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity b(WeatherSettingActivity weatherSettingActivity) {
            dl5.a(weatherSettingActivity, (UserRepository) hl4.this.A.get());
            dl5.a(weatherSettingActivity, (ch5) hl4.this.c.get());
            dl5.a(weatherSettingActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(weatherSettingActivity, (ll4) hl4.this.T1.get());
            dl5.a(weatherSettingActivity, new xm5());
            g46.a(weatherSettingActivity, a());
            return weatherSettingActivity;
        }

        @DexIgnore
        public d3(k46 k46) {
            this.a = k46;
        }

        @DexIgnore
        @Override // com.fossil.h46
        public void a(WeatherSettingActivity weatherSettingActivity) {
            b(weatherSettingActivity);
        }

        @DexIgnore
        public final m46 a(m46 m46) {
            o46.a(m46);
            return m46;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements re6 {
        @DexIgnore
        public /* final */ ve6 a;

        @DexIgnore
        public final xe6 a() {
            xe6 a2 = ye6.a(we6.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (UserRepository) hl4.this.A.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (FileRepository) hl4.this.M0.get(), (pj4) hl4.this.I.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity b(ActivityDetailActivity activityDetailActivity) {
            dl5.a(activityDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(activityDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(activityDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(activityDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(activityDetailActivity, new xm5());
            qe6.a(activityDetailActivity, a());
            return activityDetailActivity;
        }

        @DexIgnore
        public e(ve6 ve6) {
            this.a = ve6;
        }

        @DexIgnore
        @Override // com.fossil.re6
        public void a(ActivityDetailActivity activityDetailActivity) {
            b(activityDetailActivity);
        }

        @DexIgnore
        public final xe6 a(xe6 xe6) {
            ze6.a(xe6);
            return xe6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements y06 {
        @DexIgnore
        public /* final */ b16 a;

        @DexIgnore
        public final d16 a() {
            d16 a2 = e16.a(c16.a(this.a), (ch5) hl4.this.c.get(), (UserRepository) hl4.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity b(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            dl5.a(commuteTimeSettingsDefaultAddressActivity, (UserRepository) hl4.this.A.get());
            dl5.a(commuteTimeSettingsDefaultAddressActivity, (ch5) hl4.this.c.get());
            dl5.a(commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(commuteTimeSettingsDefaultAddressActivity, (ll4) hl4.this.T1.get());
            dl5.a(commuteTimeSettingsDefaultAddressActivity, new xm5());
            x06.a(commuteTimeSettingsDefaultAddressActivity, a());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public e0(b16 b16) {
            this.a = b16;
        }

        @DexIgnore
        @Override // com.fossil.y06
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            b(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final d16 a(d16 d16) {
            f16.a(d16);
            return d16;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements fp6 {
        @DexIgnore
        public /* final */ ip6 a;

        @DexIgnore
        public final kp6 a() {
            kp6 a2 = lp6.a(jp6.a(this.a), b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final zn5 b() {
            return new zn5((AuthApiGuestService) hl4.this.n.get());
        }

        @DexIgnore
        public e1(ip6 ip6) {
            this.a = ip6;
        }

        @DexIgnore
        @Override // com.fossil.fp6
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            b(forgotPasswordActivity);
        }

        @DexIgnore
        public final ForgotPasswordActivity b(ForgotPasswordActivity forgotPasswordActivity) {
            dl5.a(forgotPasswordActivity, (UserRepository) hl4.this.A.get());
            dl5.a(forgotPasswordActivity, (ch5) hl4.this.c.get());
            dl5.a(forgotPasswordActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(forgotPasswordActivity, (ll4) hl4.this.T1.get());
            dl5.a(forgotPasswordActivity, new xm5());
            ep6.a(forgotPasswordActivity, a());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final kp6 a(kp6 kp6) {
            mp6.a(kp6);
            return kp6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e2 implements er6 {
        @DexIgnore
        public /* final */ hr6 a;

        @DexIgnore
        public final jr6 a() {
            jr6 a2 = kr6.a(ir6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity b(PairingInstructionsActivity pairingInstructionsActivity) {
            dl5.a(pairingInstructionsActivity, (UserRepository) hl4.this.A.get());
            dl5.a(pairingInstructionsActivity, (ch5) hl4.this.c.get());
            dl5.a(pairingInstructionsActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(pairingInstructionsActivity, (ll4) hl4.this.T1.get());
            dl5.a(pairingInstructionsActivity, new xm5());
            dr6.a(pairingInstructionsActivity, a());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public e2(hr6 hr6) {
            this.a = hr6;
        }

        @DexIgnore
        @Override // com.fossil.er6
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            b(pairingInstructionsActivity);
        }

        @DexIgnore
        public final jr6 a(jr6 jr6) {
            lr6.a(jr6);
            return jr6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e3 implements ov6 {
        @DexIgnore
        public /* final */ tv6 a;

        @DexIgnore
        public final sv6 a() {
            sv6 a2 = vv6.a(uv6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity b(WelcomeActivity welcomeActivity) {
            dl5.a(welcomeActivity, (UserRepository) hl4.this.A.get());
            dl5.a(welcomeActivity, (ch5) hl4.this.c.get());
            dl5.a(welcomeActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(welcomeActivity, (ll4) hl4.this.T1.get());
            dl5.a(welcomeActivity, new xm5());
            nv6.a(welcomeActivity, a());
            return welcomeActivity;
        }

        @DexIgnore
        public e3(tv6 tv6) {
            this.a = tv6;
        }

        @DexIgnore
        @Override // com.fossil.ov6
        public void a(WelcomeActivity welcomeActivity) {
            b(welcomeActivity);
        }

        @DexIgnore
        public final sv6 a(sv6 sv6) {
            wv6.a(sv6);
            return sv6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements q86 {
        @DexIgnore
        public /* final */ z86 a;

        @DexIgnore
        public final v86 a() {
            v86 a2 = w86.a(a96.a(this.a), (SummariesRepository) hl4.this.F.get(), (ActivitiesRepository) hl4.this.W.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final g96 b() {
            g96 a2 = h96.a(b96.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final m96 c() {
            m96 a2 = n96.a(c96.a(this.a), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public f(z86 z86) {
            this.a = z86;
        }

        @DexIgnore
        @Override // com.fossil.q86
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            b(activityOverviewFragment);
        }

        @DexIgnore
        public final ActivityOverviewFragment b(ActivityOverviewFragment activityOverviewFragment) {
            y86.a(activityOverviewFragment, a());
            y86.a(activityOverviewFragment, c());
            y86.a(activityOverviewFragment, b());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final v86 a(v86 v86) {
            x86.a(v86);
            return v86;
        }

        @DexIgnore
        public final m96 a(m96 m96) {
            o96.a(m96);
            return m96;
        }

        @DexIgnore
        public final g96 a(g96 g96) {
            i96.a(g96);
            return g96;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements a46 {
        @DexIgnore
        @Override // com.fossil.a46
        public void a(u36 u36) {
            b(u36);
        }

        @DexIgnore
        public final u36 b(u36 u36) {
            w36.a(u36, (rj4) hl4.this.g3.get());
            return u36;
        }

        @DexIgnore
        public f0() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements of6 {
        @DexIgnore
        public /* final */ sf6 a;

        @DexIgnore
        public final uf6 a() {
            uf6 a2 = vf6.a(tf6.a(this.a), (GoalTrackingRepository) hl4.this.c0.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity b(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            dl5.a(goalTrackingDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(goalTrackingDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(goalTrackingDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(goalTrackingDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(goalTrackingDetailActivity, new xm5());
            nf6.a(goalTrackingDetailActivity, a());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public f1(sf6 sf6) {
            this.a = sf6;
        }

        @DexIgnore
        @Override // com.fossil.of6
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            b(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final uf6 a(uf6 uf6) {
            wf6.a(uf6);
            return uf6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f2 implements ls6 {
        @DexIgnore
        public /* final */ qs6 a;

        @DexIgnore
        public final ts6 a() {
            ts6 a2 = us6.a(ss6.a(this.a), rs6.a(this.a), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity b(PermissionActivity permissionActivity) {
            dl5.a(permissionActivity, (UserRepository) hl4.this.A.get());
            dl5.a(permissionActivity, (ch5) hl4.this.c.get());
            dl5.a(permissionActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(permissionActivity, (ll4) hl4.this.T1.get());
            dl5.a(permissionActivity, new xm5());
            ks6.a(permissionActivity, a());
            return permissionActivity;
        }

        @DexIgnore
        public f2(qs6 qs6) {
            this.a = qs6;
        }

        @DexIgnore
        @Override // com.fossil.ls6
        public void a(PermissionActivity permissionActivity) {
            b(permissionActivity);
        }

        @DexIgnore
        public final ts6 a(ts6 ts6) {
            vs6.a(ts6);
            return ts6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f3 implements vg6 {
        @DexIgnore
        @Override // com.fossil.vg6
        public void a(wg6 wg6) {
            b(wg6);
        }

        @DexIgnore
        public final wg6 b(wg6 wg6) {
            yg6.a(wg6, (rj4) hl4.this.g3.get());
            return wg6;
        }

        @DexIgnore
        public f3() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements eq5 {
        @DexIgnore
        public /* final */ iq5 a;

        @DexIgnore
        public final mq5 a() {
            mq5 a2 = nq5.a(lq5.a(this.a), kq5.a(this.a), jq5.a(this.a), this.a.a(), c(), (pd5) hl4.this.D.get(), b(), (UserRepository) hl4.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final pq5 b() {
            return new pq5((AlarmsRepository) hl4.this.C.get());
        }

        @DexIgnore
        public final qq5 c() {
            return new qq5((PortfolioApp) hl4.this.d.get(), (AlarmsRepository) hl4.this.C.get());
        }

        @DexIgnore
        public g(iq5 iq5) {
            this.a = iq5;
        }

        @DexIgnore
        @Override // com.fossil.eq5
        public void a(AlarmActivity alarmActivity) {
            b(alarmActivity);
        }

        @DexIgnore
        public final AlarmActivity b(AlarmActivity alarmActivity) {
            dl5.a(alarmActivity, (UserRepository) hl4.this.A.get());
            dl5.a(alarmActivity, (ch5) hl4.this.c.get());
            dl5.a(alarmActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(alarmActivity, (ll4) hl4.this.T1.get());
            dl5.a(alarmActivity, new xm5());
            dq5.a(alarmActivity, a());
            return alarmActivity;
        }

        @DexIgnore
        public final mq5 a(mq5 mq5) {
            oq5.a(mq5);
            return mq5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements d46 {
        @DexIgnore
        @Override // com.fossil.d46
        public void a(x36 x36) {
            b(x36);
        }

        @DexIgnore
        public final x36 b(x36 x36) {
            y36.a(x36, (rj4) hl4.this.g3.get());
            return x36;
        }

        @DexIgnore
        public g0() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements za6 {
        @DexIgnore
        public /* final */ hb6 a;

        @DexIgnore
        public final db6 a() {
            db6 a2 = eb6.a(ib6.a(this.a), (ch5) hl4.this.c.get(), (GoalTrackingRepository) hl4.this.c0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ob6 b() {
            ob6 a2 = pb6.a(jb6.a(this.a), (UserRepository) hl4.this.A.get(), (ch5) hl4.this.c.get(), (GoalTrackingRepository) hl4.this.c0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ub6 c() {
            ub6 a2 = vb6.a(kb6.a(this.a), (UserRepository) hl4.this.A.get(), (ch5) hl4.this.c.get(), (GoalTrackingRepository) hl4.this.c0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public g1(hb6 hb6) {
            this.a = hb6;
        }

        @DexIgnore
        @Override // com.fossil.za6
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            b(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            gb6.a(goalTrackingOverviewFragment, a());
            gb6.a(goalTrackingOverviewFragment, c());
            gb6.a(goalTrackingOverviewFragment, b());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final db6 a(db6 db6) {
            fb6.a(db6);
            return db6;
        }

        @DexIgnore
        public final ub6 a(ub6 ub6) {
            wb6.a(ub6);
            return ub6;
        }

        @DexIgnore
        public final ob6 a(ob6 ob6) {
            qb6.a(ob6);
            return ob6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g2 implements in6 {
        @DexIgnore
        public /* final */ ln6 a;

        @DexIgnore
        public final nn6 a() {
            nn6 a2 = on6.a(mn6.a(this.a), (UserRepository) hl4.this.A.get(), hl4.this.A0());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity b(PreferredUnitActivity preferredUnitActivity) {
            dl5.a(preferredUnitActivity, (UserRepository) hl4.this.A.get());
            dl5.a(preferredUnitActivity, (ch5) hl4.this.c.get());
            dl5.a(preferredUnitActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(preferredUnitActivity, (ll4) hl4.this.T1.get());
            dl5.a(preferredUnitActivity, new xm5());
            hn6.a(preferredUnitActivity, a());
            return preferredUnitActivity;
        }

        @DexIgnore
        public g2(ln6 ln6) {
            this.a = ln6;
        }

        @DexIgnore
        @Override // com.fossil.in6
        public void a(PreferredUnitActivity preferredUnitActivity) {
            b(preferredUnitActivity);
        }

        @DexIgnore
        public final nn6 a(nn6 nn6) {
            pn6.a(nn6);
            return nn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g3 implements vn6 {
        @DexIgnore
        @Override // com.fossil.vn6
        public void a(wn6 wn6) {
            b(wn6);
        }

        @DexIgnore
        public final wn6 b(wn6 wn6) {
            yn6.a(wn6, (rj4) hl4.this.g3.get());
            return wn6;
        }

        @DexIgnore
        public g3() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h implements as4 {
        @DexIgnore
        @Override // com.fossil.as4
        public void a(bs4 bs4) {
            b(bs4);
        }

        @DexIgnore
        public final bs4 b(bs4 bs4) {
            cs4.a(bs4, (rj4) hl4.this.g3.get());
            return bs4;
        }

        @DexIgnore
        public h() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements f26 {
        @DexIgnore
        public /* final */ j26 a;

        @DexIgnore
        public final l26 a() {
            l26 a2 = m26.a(k26.a(this.a), hl4.this.H(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity b(ComplicationSearchActivity complicationSearchActivity) {
            dl5.a(complicationSearchActivity, (UserRepository) hl4.this.A.get());
            dl5.a(complicationSearchActivity, (ch5) hl4.this.c.get());
            dl5.a(complicationSearchActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(complicationSearchActivity, (ll4) hl4.this.T1.get());
            dl5.a(complicationSearchActivity, new xm5());
            e26.a(complicationSearchActivity, a());
            return complicationSearchActivity;
        }

        @DexIgnore
        public h0(j26 j26) {
            this.a = j26;
        }

        @DexIgnore
        @Override // com.fossil.f26
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            b(complicationSearchActivity);
        }

        @DexIgnore
        public final l26 a(l26 l26) {
            n26.a(l26);
            return l26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements yf6 {
        @DexIgnore
        public /* final */ cg6 a;

        @DexIgnore
        public final eg6 a() {
            eg6 a2 = fg6.a(dg6.a(this.a), (HeartRateSummaryRepository) hl4.this.p0.get(), (HeartRateSampleRepository) hl4.this.o0.get(), (UserRepository) hl4.this.A.get(), (WorkoutSessionRepository) hl4.this.q0.get(), (FileRepository) hl4.this.M0.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity b(HeartRateDetailActivity heartRateDetailActivity) {
            dl5.a(heartRateDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(heartRateDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(heartRateDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(heartRateDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(heartRateDetailActivity, new xm5());
            xf6.a(heartRateDetailActivity, a());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public h1(cg6 cg6) {
            this.a = cg6;
        }

        @DexIgnore
        @Override // com.fossil.yf6
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            b(heartRateDetailActivity);
        }

        @DexIgnore
        public final eg6 a(eg6 eg6) {
            gg6.a(eg6);
            return eg6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h2 implements g36 {
        @DexIgnore
        @Override // com.fossil.g36
        public void a(h36 h36) {
            b(h36);
        }

        @DexIgnore
        public final h36 b(h36 h36) {
            i36.a(h36, (rj4) hl4.this.g3.get());
            return h36;
        }

        @DexIgnore
        public h2() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements hp4 {
        @DexIgnore
        @Override // com.fossil.hp4
        public void a(ip4 ip4) {
            b(ip4);
        }

        @DexIgnore
        public final ip4 b(ip4 ip4) {
            jp4.a(ip4, (rj4) hl4.this.g3.get());
            return ip4;
        }

        @DexIgnore
        public i() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements gz5 {
        @DexIgnore
        public /* final */ hz5 a;

        @DexIgnore
        public final n06 a() {
            n06 a2 = o06.a(iz5.a(this.a), hl4.this.D());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final v26 b() {
            v26 a2 = w26.a(jz5.a(this.a), hl4.this.H0(), (DianaPresetRepository) hl4.this.s.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final r36 c() {
            r36 a2 = s36.a(kz5.a(this.a), hl4.this.D(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public i0(hz5 hz5) {
            this.a = hz5;
        }

        @DexIgnore
        @Override // com.fossil.gz5
        public void a(sz5 sz5) {
            b(sz5);
        }

        @DexIgnore
        public final sz5 b(sz5 sz5) {
            tz5.a(sz5, a());
            tz5.a(sz5, c());
            tz5.a(sz5, b());
            tz5.a(sz5, (rj4) hl4.this.g3.get());
            return sz5;
        }

        @DexIgnore
        public final n06 a(n06 n06) {
            p06.a(n06);
            return n06;
        }

        @DexIgnore
        public final r36 a(r36 r36) {
            t36.a(r36);
            return r36;
        }

        @DexIgnore
        public final v26 a(v26 v26) {
            x26.a(v26);
            return v26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements dc6 {
        @DexIgnore
        public /* final */ lc6 a;

        @DexIgnore
        public final hc6 a() {
            hc6 a2 = ic6.a(mc6.a(this.a), (HeartRateSampleRepository) hl4.this.o0.get(), (WorkoutSessionRepository) hl4.this.q0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final sc6 b() {
            sc6 a2 = tc6.a(nc6.a(this.a), (UserRepository) hl4.this.A.get(), (HeartRateSummaryRepository) hl4.this.p0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final yc6 c() {
            yc6 a2 = zc6.a(oc6.a(this.a), (UserRepository) hl4.this.A.get(), (HeartRateSummaryRepository) hl4.this.p0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public i1(lc6 lc6) {
            this.a = lc6;
        }

        @DexIgnore
        @Override // com.fossil.dc6
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            b(heartRateOverviewFragment);
        }

        @DexIgnore
        public final HeartRateOverviewFragment b(HeartRateOverviewFragment heartRateOverviewFragment) {
            kc6.a(heartRateOverviewFragment, a());
            kc6.a(heartRateOverviewFragment, c());
            kc6.a(heartRateOverviewFragment, b());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public final hc6 a(hc6 hc6) {
            jc6.a(hc6);
            return hc6;
        }

        @DexIgnore
        public final yc6 a(yc6 yc6) {
            ad6.a(yc6);
            return yc6;
        }

        @DexIgnore
        public final sc6 a(sc6 sc6) {
            uc6.a(sc6);
            return sc6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i2 implements ck6 {
        @DexIgnore
        public /* final */ gk6 a;

        @DexIgnore
        public final en5 a() {
            return new en5((AuthApiUserService) hl4.this.w.get());
        }

        @DexIgnore
        public final ik6 b() {
            ik6 a2 = jk6.a(hk6.a(this.a), a(), (rl4) hl4.this.R.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public i2(gk6 gk6) {
            this.a = gk6;
        }

        @DexIgnore
        @Override // com.fossil.ck6
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            b(profileChangePasswordActivity);
        }

        @DexIgnore
        public final ProfileChangePasswordActivity b(ProfileChangePasswordActivity profileChangePasswordActivity) {
            dl5.a(profileChangePasswordActivity, (UserRepository) hl4.this.A.get());
            dl5.a(profileChangePasswordActivity, (ch5) hl4.this.c.get());
            dl5.a(profileChangePasswordActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(profileChangePasswordActivity, (ll4) hl4.this.T1.get());
            dl5.a(profileChangePasswordActivity, new xm5());
            bk6.a(profileChangePasswordActivity, b());
            return profileChangePasswordActivity;
        }

        @DexIgnore
        public final ik6 a(ik6 ik6) {
            kk6.a(ik6);
            return ik6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements mp4 {
        @DexIgnore
        @Override // com.fossil.mp4
        public void a(np4 np4) {
            b(np4);
        }

        @DexIgnore
        public final np4 b(np4 np4) {
            op4.a(np4, (rj4) hl4.this.g3.get());
            return np4;
        }

        @DexIgnore
        public j() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements h06 {
        @DexIgnore
        @Override // com.fossil.h06
        public void a(k06 k06) {
            b(k06);
        }

        @DexIgnore
        public final k06 b(k06 k06) {
            l06.a(k06, (rj4) hl4.this.g3.get());
            return k06;
        }

        @DexIgnore
        public j0(m06 m06) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements zi6 {
        @DexIgnore
        public /* final */ dj6 a;

        @DexIgnore
        public final fn5 a() {
            return new fn5((UserRepository) hl4.this.A.get(), (DeviceRepository) hl4.this.P.get(), (ch5) hl4.this.c.get());
        }

        @DexIgnore
        public final fj6 b() {
            fj6 a2 = gj6.a(ej6.a(this.a), (DeviceRepository) hl4.this.P.get(), a(), (qd5) hl4.this.R0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public j1(dj6 dj6) {
            this.a = dj6;
        }

        @DexIgnore
        @Override // com.fossil.zi6
        public void a(HelpActivity helpActivity) {
            b(helpActivity);
        }

        @DexIgnore
        public final HelpActivity b(HelpActivity helpActivity) {
            dl5.a(helpActivity, (UserRepository) hl4.this.A.get());
            dl5.a(helpActivity, (ch5) hl4.this.c.get());
            dl5.a(helpActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(helpActivity, (ll4) hl4.this.T1.get());
            dl5.a(helpActivity, new xm5());
            yi6.a(helpActivity, b());
            return helpActivity;
        }

        @DexIgnore
        public final fj6 a(fj6 fj6) {
            hj6.a(fj6);
            return fj6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j2 implements qi6 {
        @DexIgnore
        @Override // com.fossil.qi6
        public void a(ri6 ri6) {
            b(ri6);
        }

        @DexIgnore
        public final ri6 b(ri6 ri6) {
            ti6.a(ri6, (rj4) hl4.this.g3.get());
            return ri6;
        }

        @DexIgnore
        public j2() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k implements fs4 {
        @DexIgnore
        @Override // com.fossil.fs4
        public void a(gs4 gs4) {
            b(gs4);
        }

        @DexIgnore
        public final gs4 b(gs4 gs4) {
            is4.a(gs4, (rj4) hl4.this.g3.get());
            return gs4;
        }

        @DexIgnore
        public k() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements sq5 {
        @DexIgnore
        public /* final */ vq5 a;

        @DexIgnore
        public final xq5 a() {
            xq5 a2 = yq5.a(wq5.a(this.a), (UserRepository) hl4.this.A.get(), (PortfolioApp) hl4.this.d.get(), hl4.this.g0());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity b(ConnectedAppsActivity connectedAppsActivity) {
            dl5.a(connectedAppsActivity, (UserRepository) hl4.this.A.get());
            dl5.a(connectedAppsActivity, (ch5) hl4.this.c.get());
            dl5.a(connectedAppsActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(connectedAppsActivity, (ll4) hl4.this.T1.get());
            dl5.a(connectedAppsActivity, new xm5());
            rq5.a(connectedAppsActivity, a());
            return connectedAppsActivity;
        }

        @DexIgnore
        public k0(vq5 vq5) {
            this.a = vq5;
        }

        @DexIgnore
        @Override // com.fossil.sq5
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            b(connectedAppsActivity);
        }

        @DexIgnore
        public final xq5 a(xq5 xq5) {
            zq5.a(xq5);
            return xq5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements qs5 {
        @DexIgnore
        public /* final */ vs5 a;

        @DexIgnore
        public final kw5 a() {
            kw5 a2 = lw5.a(ws5.a(this.a), (DNDSettingsDatabase) hl4.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ts5 b(ts5 ts5) {
            us5.a(ts5, a());
            return ts5;
        }

        @DexIgnore
        public k1(vs5 vs5) {
            this.a = vs5;
        }

        @DexIgnore
        @Override // com.fossil.qs5
        public void a(ts5 ts5) {
            b(ts5);
        }

        @DexIgnore
        public final kw5 a(kw5 kw5) {
            mw5.a(kw5);
            return kw5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k2 implements sj6 {
        @DexIgnore
        public /* final */ wj6 a;

        @DexIgnore
        public final yj6 a() {
            yj6 a2 = zj6.a(xj6.a(this.a), hl4.this.A0(), (UserRepository) hl4.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity b(ProfileOptInActivity profileOptInActivity) {
            dl5.a(profileOptInActivity, (UserRepository) hl4.this.A.get());
            dl5.a(profileOptInActivity, (ch5) hl4.this.c.get());
            dl5.a(profileOptInActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(profileOptInActivity, (ll4) hl4.this.T1.get());
            dl5.a(profileOptInActivity, new xm5());
            rj6.a(profileOptInActivity, a());
            return profileOptInActivity;
        }

        @DexIgnore
        public k2(wj6 wj6) {
            this.a = wj6;
        }

        @DexIgnore
        @Override // com.fossil.sj6
        public void a(ProfileOptInActivity profileOptInActivity) {
            b(profileOptInActivity);
        }

        @DexIgnore
        public final yj6 a(yj6 yj6) {
            ak6.a(yj6);
            return yj6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l implements cq4 {
        @DexIgnore
        @Override // com.fossil.cq4
        public void a(yp4 yp4) {
            b(yp4);
        }

        @DexIgnore
        public final yp4 b(yp4 yp4) {
            zp4.a(yp4, (rj4) hl4.this.g3.get());
            return yp4;
        }

        @DexIgnore
        public l() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements rk6 {
        @DexIgnore
        @Override // com.fossil.rk6
        public void a(sk6 sk6) {
            b(sk6);
        }

        @DexIgnore
        public final sk6 b(sk6 sk6) {
            tk6.a(sk6, (rj4) hl4.this.g3.get());
            return sk6;
        }

        @DexIgnore
        public l0(uk6 uk6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements pr5 {
        @DexIgnore
        public /* final */ zr5 a;

        @DexIgnore
        public final sn5 a() {
            return new sn5(d());
        }

        @DexIgnore
        public final fn5 b() {
            return new fn5((UserRepository) hl4.this.A.get(), (DeviceRepository) hl4.this.P.get(), (ch5) hl4.this.c.get());
        }

        @DexIgnore
        public final bs5 c() {
            bs5 a2 = cs5.a(as5.a(this.a), (ch5) hl4.this.c.get(), (DeviceRepository) hl4.this.P.get(), (PortfolioApp) hl4.this.d.get(), hl4.this.z0(), (rl4) hl4.this.R.get(), hl4.this.N(), a(), (wx6) hl4.this.l3.get(), d(), b(), (lm4) hl4.this.r1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) hl4.this.j3.get(), (ServerSettingDataSource) hl4.this.k3.get());
        }

        @DexIgnore
        public l1(zr5 zr5) {
            this.a = zr5;
        }

        @DexIgnore
        @Override // com.fossil.pr5
        public void a(HomeActivity homeActivity) {
            b(homeActivity);
        }

        @DexIgnore
        public final HomeActivity b(HomeActivity homeActivity) {
            dl5.a(homeActivity, (UserRepository) hl4.this.A.get());
            dl5.a(homeActivity, (ch5) hl4.this.c.get());
            dl5.a(homeActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(homeActivity, (ll4) hl4.this.T1.get());
            dl5.a(homeActivity, new xm5());
            or5.a(homeActivity, c());
            return homeActivity;
        }

        @DexIgnore
        public final bs5 a(bs5 bs5) {
            ds5.a(bs5);
            return bs5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l2 implements fq6 {
        @DexIgnore
        public /* final */ iq6 a;

        @DexIgnore
        public final ew6 a() {
            return new ew6((SummariesRepository) hl4.this.F.get(), (SleepSummariesRepository) hl4.this.G.get(), (SleepSessionsRepository) hl4.this.b0.get());
        }

        @DexIgnore
        public final kq6 b() {
            kq6 a2 = lq6.a(jq6.a(this.a), a(), (UserRepository) hl4.this.A.get(), (DeviceRepository) hl4.this.P.get(), c(), (lm4) hl4.this.r1.get(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository c() {
            return new ServerSettingRepository((ServerSettingDataSource) hl4.this.j3.get(), (ServerSettingDataSource) hl4.this.k3.get());
        }

        @DexIgnore
        public l2(iq6 iq6) {
            this.a = iq6;
        }

        @DexIgnore
        @Override // com.fossil.fq6
        public void a(ProfileSetupActivity profileSetupActivity) {
            b(profileSetupActivity);
        }

        @DexIgnore
        public final ProfileSetupActivity b(ProfileSetupActivity profileSetupActivity) {
            dl5.a(profileSetupActivity, (UserRepository) hl4.this.A.get());
            dl5.a(profileSetupActivity, (ch5) hl4.this.c.get());
            dl5.a(profileSetupActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(profileSetupActivity, (ll4) hl4.this.T1.get());
            dl5.a(profileSetupActivity, new xm5());
            eq6.a(profileSetupActivity, b());
            return profileSetupActivity;
        }

        @DexIgnore
        public final kq6 a(kq6 kq6) {
            mq6.a(kq6, hl4.this.v0());
            mq6.a(kq6, hl4.this.w0());
            mq6.a(kq6, hl4.this.e0());
            mq6.a(kq6, (qd5) hl4.this.R0.get());
            mq6.a(kq6);
            return kq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m implements ls4 {
        @DexIgnore
        @Override // com.fossil.ls4
        public void a(ms4 ms4) {
            b(ms4);
        }

        @DexIgnore
        public final ms4 b(ms4 ms4) {
            ns4.a(ms4, (rj4) hl4.this.g3.get());
            return ms4;
        }

        @DexIgnore
        public m() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements xk6 {
        @DexIgnore
        @Override // com.fossil.xk6
        public void a(yk6 yk6) {
            b(yk6);
        }

        @DexIgnore
        public final yk6 b(yk6 yk6) {
            zk6.a(yk6, (rj4) hl4.this.g3.get());
            return yk6;
        }

        @DexIgnore
        public m0(al6 al6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements fs5 {
        @DexIgnore
        public /* final */ gs5 a;

        @DexIgnore
        public final og5 a() {
            return new og5((PortfolioApp) hl4.this.d.get(), (DeviceRepository) hl4.this.P.get());
        }

        @DexIgnore
        public final sw5 b() {
            sw5 a2 = tw5.a(hs5.a(this.a), (pd5) hl4.this.D.get(), j(), (AlarmsRepository) hl4.this.C.get(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final xs5 c() {
            xs5 a2 = ys5.a(is5.a(this.a), (rl4) hl4.this.R.get(), (pd5) hl4.this.D.get(), new nw5(), new vu5(), i(), (NotificationSettingsDatabase) hl4.this.j.get(), j(), (AlarmsRepository) hl4.this.C.get(), (ch5) hl4.this.c.get(), (DNDSettingsDatabase) hl4.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final d76 d() {
            d76 a2 = e76.a(js5.a(this.a), (PortfolioApp) hl4.this.d.get(), (DeviceRepository) hl4.this.P.get(), hl4.this.N(), (SummariesRepository) hl4.this.F.get(), (GoalTrackingRepository) hl4.this.c0.get(), (SleepSummariesRepository) hl4.this.G.get(), (ch5) hl4.this.c.get(), (HeartRateSampleRepository) hl4.this.o0.get(), (ro4) hl4.this.E0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final e06 e() {
            e06 a2 = f06.a(ks5.a(this.a), hl4.this.F0(), hl4.this.H(), hl4.this.s0(), (DianaPresetRepository) hl4.this.s.get(), k(), a(), (CustomizeRealDataRepository) hl4.this.v.get(), (UserRepository) hl4.this.A.get(), hl4.this.H0(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final d56 f() {
            d56 a2 = e56.a((PortfolioApp) hl4.this.d.get(), ls5.a(this.a), (MicroAppRepository) hl4.this.k0.get(), (HybridPresetRepository) hl4.this.V.get(), l(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final oh6 g() {
            oh6 a2 = ph6.a(ms5.a(this.a), (PortfolioApp) hl4.this.d.get(), hl4.this.e0(), hl4.this.A0(), (DeviceRepository) hl4.this.P.get(), (UserRepository) hl4.this.A.get(), (SummariesRepository) hl4.this.F.get(), (SleepSummariesRepository) hl4.this.G.get(), hl4.this.M(), (fp4) hl4.this.w0.get(), (ro4) hl4.this.E0.get(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final hh6 h() {
            hh6 a2 = ih6.a(ns5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final mt5 i() {
            return new mt5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public final qq5 j() {
            return new qq5((PortfolioApp) hl4.this.d.get(), (AlarmsRepository) hl4.this.C.get());
        }

        @DexIgnore
        public final z46 k() {
            return new z46((DianaPresetRepository) hl4.this.s.get(), hl4.this.G(), hl4.this.E0(), hl4.this.H0());
        }

        @DexIgnore
        public final a56 l() {
            return new a56((DeviceRepository) hl4.this.P.get(), (HybridPresetRepository) hl4.this.V.get(), (MicroAppRepository) hl4.this.k0.get(), hl4.this.p0());
        }

        @DexIgnore
        public m1(gs5 gs5) {
            this.a = gs5;
        }

        @DexIgnore
        @Override // com.fossil.fs5
        public void a(wr5 wr5) {
            b(wr5);
        }

        @DexIgnore
        public final wr5 b(wr5 wr5) {
            xr5.a(wr5, d());
            xr5.a(wr5, e());
            xr5.a(wr5, f());
            xr5.a(wr5, g());
            xr5.a(wr5, c());
            xr5.a(wr5, b());
            xr5.a(wr5, h());
            return wr5;
        }

        @DexIgnore
        public final d76 a(d76 d76) {
            f76.a(d76);
            return d76;
        }

        @DexIgnore
        public final e06 a(e06 e06) {
            g06.a(e06);
            return e06;
        }

        @DexIgnore
        public final d56 a(d56 d56) {
            f56.a(d56);
            return d56;
        }

        @DexIgnore
        public final oh6 a(oh6 oh6) {
            qh6.a(oh6);
            return oh6;
        }

        @DexIgnore
        public final xs5 a(xs5 xs5) {
            zs5.a(xs5);
            return xs5;
        }

        @DexIgnore
        public final sw5 a(sw5 sw5) {
            uw5.a(sw5);
            return sw5;
        }

        @DexIgnore
        public final hh6 a(hh6 hh6) {
            jh6.a(hh6);
            return hh6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m2 implements av5 {
        @DexIgnore
        @Override // com.fossil.av5
        public void a(dv5 dv5) {
            b(dv5);
        }

        @DexIgnore
        public final dv5 b(dv5 dv5) {
            ev5.a(dv5, (rj4) hl4.this.g3.get());
            return dv5;
        }

        @DexIgnore
        public m2() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n implements pr4 {
        @DexIgnore
        @Override // com.fossil.pr4
        public void a(qr4 qr4) {
            b(qr4);
        }

        @DexIgnore
        public final qr4 b(qr4 qr4) {
            sr4.a(qr4, (rj4) hl4.this.g3.get());
            return qr4;
        }

        @DexIgnore
        public n() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements dl6 {
        @DexIgnore
        @Override // com.fossil.dl6
        public void a(el6 el6) {
            b(el6);
        }

        @DexIgnore
        public final el6 b(el6 el6) {
            fl6.a(el6, (rj4) hl4.this.g3.get());
            return el6;
        }

        @DexIgnore
        public n0(gl6 gl6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements o56 {
        @DexIgnore
        public /* final */ t56 a;

        @DexIgnore
        public final v56 a() {
            v56 a2 = w56.a(u56.a(this.a), b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final a56 b() {
            return new a56((DeviceRepository) hl4.this.P.get(), (HybridPresetRepository) hl4.this.V.get(), (MicroAppRepository) hl4.this.k0.get(), hl4.this.p0());
        }

        @DexIgnore
        public n1(t56 t56) {
            this.a = t56;
        }

        @DexIgnore
        @Override // com.fossil.o56
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            b(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final HybridCustomizeEditActivity b(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            dl5.a(hybridCustomizeEditActivity, (UserRepository) hl4.this.A.get());
            dl5.a(hybridCustomizeEditActivity, (ch5) hl4.this.c.get());
            dl5.a(hybridCustomizeEditActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(hybridCustomizeEditActivity, (ll4) hl4.this.T1.get());
            dl5.a(hybridCustomizeEditActivity, new xm5());
            n56.a(hybridCustomizeEditActivity, a());
            n56.a(hybridCustomizeEditActivity, (rj4) hl4.this.g3.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final v56 a(v56 v56) {
            x56.a(v56);
            return v56;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n2 implements ew5 {
        @DexIgnore
        public /* final */ fw5 a;

        @DexIgnore
        public final kv5 a() {
            kv5 a2 = lv5.a(gw5.a(this.a), (RemindersSettingsDatabase) hl4.this.r0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final bw5 b() {
            bw5 a2 = cw5.a(hw5.a(this.a), (RemindersSettingsDatabase) hl4.this.r0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public n2(fw5 fw5) {
            this.a = fw5;
        }

        @DexIgnore
        @Override // com.fossil.ew5
        public void a(rv5 rv5) {
            b(rv5);
        }

        @DexIgnore
        public final rv5 b(rv5 rv5) {
            sv5.a(rv5, a());
            sv5.a(rv5, b());
            return rv5;
        }

        @DexIgnore
        public final kv5 a(kv5 kv5) {
            mv5.a(kv5);
            return kv5;
        }

        @DexIgnore
        public final bw5 a(bw5 bw5) {
            dw5.a(bw5);
            return bw5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements et4 {
        @DexIgnore
        @Override // com.fossil.et4
        public void a(zs4 zs4) {
            b(zs4);
        }

        @DexIgnore
        public final zs4 b(zs4 zs4) {
            bt4.a(zs4, (rj4) hl4.this.g3.get());
            return zs4;
        }

        @DexIgnore
        public o() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements jl6 {
        @DexIgnore
        @Override // com.fossil.jl6
        public void a(kl6 kl6) {
            b(kl6);
        }

        @DexIgnore
        public final kl6 b(kl6 kl6) {
            ll6.a(kl6, (rj4) hl4.this.g3.get());
            return kl6;
        }

        @DexIgnore
        public o0(ml6 ml6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements g56 {
        @DexIgnore
        public /* final */ h56 a;

        @DexIgnore
        public final c66 a() {
            c66 a2 = d66.a(i56.a(this.a), hl4.this.D());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final r56 b(r56 r56) {
            s56.a(r56, a());
            s56.a(r56, (rj4) hl4.this.g3.get());
            return r56;
        }

        @DexIgnore
        public o1(h56 h56) {
            this.a = h56;
        }

        @DexIgnore
        @Override // com.fossil.g56
        public void a(r56 r56) {
            b(r56);
        }

        @DexIgnore
        public final c66 a(c66 c66) {
            e66.a(c66);
            return c66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o2 implements ci6 {
        @DexIgnore
        public /* final */ gi6 a;

        @DexIgnore
        public final ii6 a() {
            ii6 a2 = ji6.a(hi6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity b(ReplaceBatteryActivity replaceBatteryActivity) {
            dl5.a(replaceBatteryActivity, (UserRepository) hl4.this.A.get());
            dl5.a(replaceBatteryActivity, (ch5) hl4.this.c.get());
            dl5.a(replaceBatteryActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(replaceBatteryActivity, (ll4) hl4.this.T1.get());
            dl5.a(replaceBatteryActivity, new xm5());
            bi6.a(replaceBatteryActivity, a());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public o2(gi6 gi6) {
            this.a = gi6;
        }

        @DexIgnore
        @Override // com.fossil.ci6
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            b(replaceBatteryActivity);
        }

        @DexIgnore
        public final ii6 a(ii6 ii6) {
            ki6.a(ii6);
            return ii6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p implements ht4 {
        @DexIgnore
        @Override // com.fossil.ht4
        public void a(it4 it4) {
            b(it4);
        }

        @DexIgnore
        public final it4 b(it4 it4) {
            jt4.a(it4, (rj4) hl4.this.g3.get());
            return it4;
        }

        @DexIgnore
        public p() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements pl6 {
        @DexIgnore
        @Override // com.fossil.pl6
        public void a(ql6 ql6) {
            b(ql6);
        }

        @DexIgnore
        public final ql6 b(ql6 ql6) {
            rl6.a(ql6, (rj4) hl4.this.g3.get());
            return ql6;
        }

        @DexIgnore
        public p0(sl6 sl6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements go6 {
        @DexIgnore
        public /* final */ ko6 a;

        @DexIgnore
        public final no6 a() {
            no6 a2 = oo6.a(mo6.a(this.a), lo6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity b(LoginActivity loginActivity) {
            dl5.a(loginActivity, (UserRepository) hl4.this.A.get());
            dl5.a(loginActivity, (ch5) hl4.this.c.get());
            dl5.a(loginActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(loginActivity, (ll4) hl4.this.T1.get());
            dl5.a(loginActivity, new xm5());
            fo6.a(loginActivity, (ih5) hl4.this.P1.get());
            fo6.a(loginActivity, (jh5) hl4.this.d0.get());
            fo6.a(loginActivity, (lh5) hl4.this.Q1.get());
            fo6.a(loginActivity, (kh5) hl4.this.R1.get());
            fo6.a(loginActivity, a());
            return loginActivity;
        }

        @DexIgnore
        public p1(ko6 ko6) {
            this.a = ko6;
        }

        @DexIgnore
        @Override // com.fossil.go6
        public void a(LoginActivity loginActivity) {
            b(loginActivity);
        }

        @DexIgnore
        public final no6 a(no6 no6) {
            po6.a(no6, hl4.this.i0());
            po6.a(no6, hl4.this.l0());
            po6.a(no6, hl4.this.P());
            po6.a(no6, new yl5());
            po6.a(no6, hl4.this.N());
            po6.a(no6, (UserRepository) hl4.this.A.get());
            po6.a(no6, (DeviceRepository) hl4.this.P.get());
            po6.a(no6, (ch5) hl4.this.c.get());
            po6.a(no6, hl4.this.R());
            po6.a(no6, hl4.this.Y());
            po6.a(no6, (rl4) hl4.this.R.get());
            po6.a(no6, hl4.this.W());
            po6.a(no6, hl4.this.X());
            po6.a(no6, hl4.this.V());
            po6.a(no6, hl4.this.T());
            po6.a(no6, hl4.this.j0());
            po6.a(no6, (lh5) hl4.this.Q1.get());
            po6.a(no6, hl4.this.k0());
            po6.a(no6, hl4.this.n0());
            po6.a(no6, hl4.this.m0());
            po6.a(no6, hl4.this.F());
            po6.a(no6, (qd5) hl4.this.R0.get());
            po6.a(no6, (SummariesRepository) hl4.this.F.get());
            po6.a(no6, (SleepSummariesRepository) hl4.this.G.get());
            po6.a(no6, (GoalTrackingRepository) hl4.this.c0.get());
            po6.a(no6, hl4.this.S());
            po6.a(no6, hl4.this.U());
            po6.a(no6, hl4.this.d0());
            po6.a(no6, hl4.this.I0());
            po6.a(no6, (AlarmsRepository) hl4.this.C.get());
            po6.a(no6, (fp4) hl4.this.w0.get());
            po6.a(no6, (to4) hl4.this.P0.get());
            po6.a(no6, (lm4) hl4.this.r1.get());
            po6.a(no6, hl4.this.K0());
            po6.a(no6);
            return no6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p2 implements g66 {
        @DexIgnore
        public /* final */ k66 a;

        @DexIgnore
        public final m66 a() {
            m66 a2 = n66.a(l66.a(this.a), (MicroAppRepository) hl4.this.k0.get(), (ch5) hl4.this.c.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity b(SearchMicroAppActivity searchMicroAppActivity) {
            dl5.a(searchMicroAppActivity, (UserRepository) hl4.this.A.get());
            dl5.a(searchMicroAppActivity, (ch5) hl4.this.c.get());
            dl5.a(searchMicroAppActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(searchMicroAppActivity, (ll4) hl4.this.T1.get());
            dl5.a(searchMicroAppActivity, new xm5());
            f66.a(searchMicroAppActivity, a());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public p2(k66 k66) {
            this.a = k66;
        }

        @DexIgnore
        @Override // com.fossil.g66
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            b(searchMicroAppActivity);
        }

        @DexIgnore
        public final m66 a(m66 m66) {
            o66.a(m66);
            return m66;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements sp4 {
        @DexIgnore
        @Override // com.fossil.sp4
        public void a(tp4 tp4) {
            b(tp4);
        }

        @DexIgnore
        public final tp4 b(tp4 tp4) {
            up4.a(tp4, (rj4) hl4.this.g3.get());
            return tp4;
        }

        @DexIgnore
        public q() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements wl6 {
        @DexIgnore
        @Override // com.fossil.wl6
        public void a(xl6 xl6) {
            b(xl6);
        }

        @DexIgnore
        public final xl6 b(xl6 xl6) {
            yl6.a(xl6, (rj4) hl4.this.g3.get());
            return xl6;
        }

        @DexIgnore
        public q0(zl6 zl6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements qo6 {
        @DexIgnore
        @Override // com.fossil.qo6
        public void a(ro6 ro6) {
            b(ro6);
        }

        @DexIgnore
        public final ro6 b(ro6 ro6) {
            so6.a(ro6, (rj4) hl4.this.g3.get());
            return ro6;
        }

        @DexIgnore
        public q1() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q2 implements m16 {
        @DexIgnore
        public /* final */ q16 a;

        @DexIgnore
        public final s16 a() {
            s16 a2 = t16.a(r16.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity b(SearchRingPhoneActivity searchRingPhoneActivity) {
            dl5.a(searchRingPhoneActivity, (UserRepository) hl4.this.A.get());
            dl5.a(searchRingPhoneActivity, (ch5) hl4.this.c.get());
            dl5.a(searchRingPhoneActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(searchRingPhoneActivity, (ll4) hl4.this.T1.get());
            dl5.a(searchRingPhoneActivity, new xm5());
            l16.a(searchRingPhoneActivity, a());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public q2(q16 q16) {
            this.a = q16;
        }

        @DexIgnore
        @Override // com.fossil.m16
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            b(searchRingPhoneActivity);
        }

        @DexIgnore
        public final s16 a(s16 s16) {
            u16.a(s16);
            return s16;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r implements dq4 {
        @DexIgnore
        @Override // com.fossil.dq4
        public void a(eq4 eq4) {
            b(eq4);
        }

        @DexIgnore
        public final eq4 b(eq4 eq4) {
            fq4.a(eq4, (rj4) hl4.this.g3.get());
            return eq4;
        }

        @DexIgnore
        public r() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements cm6 {
        @DexIgnore
        @Override // com.fossil.cm6
        public void a(dm6 dm6) {
            b(dm6);
        }

        @DexIgnore
        public final dm6 b(dm6 dm6) {
            em6.a(dm6, (rj4) hl4.this.g3.get());
            return dm6;
        }

        @DexIgnore
        public r0(fm6 fm6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements y56 {
        @DexIgnore
        @Override // com.fossil.y56
        public void a(l56 l56) {
            b(l56);
        }

        @DexIgnore
        public final l56 b(l56 l56) {
            m56.a(l56, (rj4) hl4.this.g3.get());
            return l56;
        }

        @DexIgnore
        public r1(b66 b66) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r2 implements w16 {
        @DexIgnore
        public /* final */ z16 a;

        @DexIgnore
        public final b26 a() {
            b26 a2 = c26.a(a26.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity b(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            dl5.a(searchSecondTimezoneActivity, (UserRepository) hl4.this.A.get());
            dl5.a(searchSecondTimezoneActivity, (ch5) hl4.this.c.get());
            dl5.a(searchSecondTimezoneActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(searchSecondTimezoneActivity, (ll4) hl4.this.T1.get());
            dl5.a(searchSecondTimezoneActivity, new xm5());
            v16.a(searchSecondTimezoneActivity, a());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public r2(z16 z16) {
            this.a = z16;
        }

        @DexIgnore
        @Override // com.fossil.w16
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            b(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final b26 a(b26 b26) {
            d26.a(b26);
            return b26;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s implements kq4 {
        @DexIgnore
        @Override // com.fossil.kq4
        public void a(lq4 lq4) {
            b(lq4);
        }

        @DexIgnore
        public final lq4 b(lq4 lq4) {
            mq4.a(lq4, (rj4) hl4.this.g3.get());
            return lq4;
        }

        @DexIgnore
        public s() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements im6 {
        @DexIgnore
        @Override // com.fossil.im6
        public void a(jm6 jm6) {
            b(jm6);
        }

        @DexIgnore
        public final jm6 b(jm6 jm6) {
            km6.a(jm6, (rj4) hl4.this.g3.get());
            return jm6;
        }

        @DexIgnore
        public s0(lm6 lm6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements dt5 {
        @DexIgnore
        public /* final */ ht5 a;

        @DexIgnore
        public final jt5 a() {
            jt5 a2 = kt5.a(it5.a(this.a), (rl4) hl4.this.R.get(), new nw5(), new vu5(), b(), (ch5) hl4.this.c.get(), (NotificationSettingsDatabase) hl4.this.j.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final mt5 b() {
            return new mt5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public s1(ht5 ht5) {
            this.a = ht5;
        }

        @DexIgnore
        @Override // com.fossil.dt5
        public void a(NotificationAppsActivity notificationAppsActivity) {
            b(notificationAppsActivity);
        }

        @DexIgnore
        public final NotificationAppsActivity b(NotificationAppsActivity notificationAppsActivity) {
            dl5.a(notificationAppsActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationAppsActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationAppsActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationAppsActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationAppsActivity, new xm5());
            ct5.a(notificationAppsActivity, a());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final jt5 a(jt5 jt5) {
            lt5.a(jt5);
            return jt5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s2 implements xs6 {
        @DexIgnore
        public /* final */ bt6 a;

        @DexIgnore
        public final et6 a() {
            et6 a2 = ft6.a(dt6.a(this.a), ct6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity b(SignUpActivity signUpActivity) {
            dl5.a(signUpActivity, (UserRepository) hl4.this.A.get());
            dl5.a(signUpActivity, (ch5) hl4.this.c.get());
            dl5.a(signUpActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(signUpActivity, (ll4) hl4.this.T1.get());
            dl5.a(signUpActivity, new xm5());
            ws6.a(signUpActivity, a());
            ws6.a(signUpActivity, (ih5) hl4.this.P1.get());
            ws6.a(signUpActivity, (jh5) hl4.this.d0.get());
            ws6.a(signUpActivity, (lh5) hl4.this.Q1.get());
            ws6.a(signUpActivity, (kh5) hl4.this.R1.get());
            return signUpActivity;
        }

        @DexIgnore
        public s2(bt6 bt6) {
            this.a = bt6;
        }

        @DexIgnore
        @Override // com.fossil.xs6
        public void a(SignUpActivity signUpActivity) {
            b(signUpActivity);
        }

        @DexIgnore
        public final et6 a(et6 et6) {
            gt6.a(et6, hl4.this.j0());
            gt6.a(et6, (lh5) hl4.this.Q1.get());
            gt6.a(et6, hl4.this.k0());
            gt6.a(et6, hl4.this.n0());
            gt6.a(et6, hl4.this.m0());
            gt6.a(et6, hl4.this.l0());
            gt6.a(et6, (UserRepository) hl4.this.A.get());
            gt6.a(et6, (DeviceRepository) hl4.this.P.get());
            gt6.a(et6, (rl4) hl4.this.R.get());
            gt6.a(et6, hl4.this.W());
            gt6.a(et6, hl4.this.X());
            gt6.a(et6, hl4.this.R());
            gt6.a(et6, hl4.this.Y());
            gt6.a(et6, hl4.this.V());
            gt6.a(et6, hl4.this.T());
            gt6.a(et6, (AlarmsRepository) hl4.this.C.get());
            gt6.a(et6, new yl5());
            gt6.a(et6, hl4.this.N());
            gt6.a(et6, hl4.this.P());
            gt6.b(et6, (ch5) hl4.this.c.get());
            gt6.a(et6, hl4.this.E());
            gt6.a(et6, hl4.this.F());
            gt6.a(et6, (qd5) hl4.this.R0.get());
            gt6.a(et6, hl4.this.e0());
            gt6.a(et6, (SummariesRepository) hl4.this.F.get());
            gt6.a(et6, (SleepSummariesRepository) hl4.this.G.get());
            gt6.a(et6, (GoalTrackingRepository) hl4.this.c0.get());
            gt6.a(et6, hl4.this.S());
            gt6.a(et6, hl4.this.U());
            gt6.a(et6, hl4.this.q0());
            gt6.a(et6, hl4.this.d0());
            gt6.a(et6, hl4.this.I0());
            gt6.a(et6, (ch5) hl4.this.c.get());
            gt6.a(et6, (to4) hl4.this.P0.get());
            gt6.a(et6, (lm4) hl4.this.r1.get());
            gt6.a(et6, hl4.this.K0());
            gt6.a(et6);
            return et6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t implements pq4 {
        @DexIgnore
        @Override // com.fossil.pq4
        public void a(qq4 qq4) {
            b(qq4);
        }

        @DexIgnore
        public final qq4 b(qq4 qq4) {
            rq4.a(qq4, (rj4) hl4.this.g3.get());
            return qq4;
        }

        @DexIgnore
        public t() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements om6 {
        @DexIgnore
        @Override // com.fossil.om6
        public void a(pm6 pm6) {
            b(pm6);
        }

        @DexIgnore
        public final pm6 b(pm6 pm6) {
            qm6.a(pm6, (rj4) hl4.this.g3.get());
            return pm6;
        }

        @DexIgnore
        public t0(rm6 rm6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements ot5 {
        @DexIgnore
        public /* final */ tt5 a;

        @DexIgnore
        public final vt5 a() {
            vt5 a2 = wt5.a(ut5.a(this.a), (rl4) hl4.this.R.get(), new vu5(), b(), c(), new nw5(), (ch5) hl4.this.c.get(), (NotificationSettingsDao) hl4.this.h3.get(), hl4.this.u0(), (QuickResponseRepository) hl4.this.i.get(), (NotificationSettingsDatabase) hl4.this.j.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final xu5 b() {
            return new xu5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public final yu5 c() {
            return new yu5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public t1(tt5 tt5) {
            this.a = tt5;
        }

        @DexIgnore
        @Override // com.fossil.ot5
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            b(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity b(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            dl5.a(notificationCallsAndMessagesActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationCallsAndMessagesActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationCallsAndMessagesActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationCallsAndMessagesActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationCallsAndMessagesActivity, new xm5());
            nt5.a(notificationCallsAndMessagesActivity, a());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final vt5 a(vt5 vt5) {
            xt5.a(vt5);
            return vt5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t2 implements ig6 {
        @DexIgnore
        public /* final */ mg6 a;

        @DexIgnore
        public final og6 a() {
            og6 a2 = pg6.a(ng6.a(this.a), (SleepSummariesRepository) hl4.this.G.get(), (SleepSessionsRepository) hl4.this.b0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity b(SleepDetailActivity sleepDetailActivity) {
            dl5.a(sleepDetailActivity, (UserRepository) hl4.this.A.get());
            dl5.a(sleepDetailActivity, (ch5) hl4.this.c.get());
            dl5.a(sleepDetailActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(sleepDetailActivity, (ll4) hl4.this.T1.get());
            dl5.a(sleepDetailActivity, new xm5());
            hg6.a(sleepDetailActivity, a());
            return sleepDetailActivity;
        }

        @DexIgnore
        public t2(mg6 mg6) {
            this.a = mg6;
        }

        @DexIgnore
        @Override // com.fossil.ig6
        public void a(SleepDetailActivity sleepDetailActivity) {
            b(sleepDetailActivity);
        }

        @DexIgnore
        public final og6 a(og6 og6) {
            qg6.a(og6);
            return og6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements wq4 {
        @DexIgnore
        @Override // com.fossil.wq4
        public void a(xq4 xq4) {
            b(xq4);
        }

        @DexIgnore
        public final xq4 b(xq4 xq4) {
            yq4.a(xq4, (rj4) hl4.this.g3.get());
            return xq4;
        }

        @DexIgnore
        public u() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements um6 {
        @DexIgnore
        @Override // com.fossil.um6
        public void a(vm6 vm6) {
            b(vm6);
        }

        @DexIgnore
        public final vm6 b(vm6 vm6) {
            wm6.a(vm6, (rj4) hl4.this.g3.get());
            return vm6;
        }

        @DexIgnore
        public u0(xm6 xm6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements xw5 {
        @DexIgnore
        public /* final */ bx5 a;

        @DexIgnore
        public final dy5 a() {
            return new dy5((Context) hl4.this.b.get());
        }

        @DexIgnore
        public final ex5 b() {
            ex5 a2 = fx5.a(cx5.a(this.a), dx5.a(this.a), this.a.a(), (rl4) hl4.this.R.get(), new qy5(), a(), c(), d());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final cz5 c() {
            return new cz5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public final cm5 d() {
            return new cm5((NotificationsRepository) hl4.this.K.get(), (DeviceRepository) hl4.this.P.get());
        }

        @DexIgnore
        public u1(bx5 bx5) {
            this.a = bx5;
        }

        @DexIgnore
        @Override // com.fossil.xw5
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            b(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity b(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            dl5.a(notificationContactsAndAppsAssignedActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationContactsAndAppsAssignedActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationContactsAndAppsAssignedActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationContactsAndAppsAssignedActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationContactsAndAppsAssignedActivity, new xm5());
            ww5.a(notificationContactsAndAppsAssignedActivity, b());
            return notificationContactsAndAppsAssignedActivity;
        }

        @DexIgnore
        public final ex5 a(ex5 ex5) {
            gx5.a(ex5);
            return ex5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u2 implements hd6 {
        @DexIgnore
        public /* final */ pd6 a;

        @DexIgnore
        public final ld6 a() {
            ld6 a2 = md6.a(qd6.a(this.a), (SleepSummariesRepository) hl4.this.G.get(), (SleepSessionsRepository) hl4.this.b0.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final wd6 b() {
            wd6 a2 = xd6.a(rd6.a(this.a), (UserRepository) hl4.this.A.get(), (SleepSummariesRepository) hl4.this.G.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ce6 c() {
            ce6 a2 = de6.a(sd6.a(this.a), (UserRepository) hl4.this.A.get(), (SleepSummariesRepository) hl4.this.G.get(), (PortfolioApp) hl4.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public u2(pd6 pd6) {
            this.a = pd6;
        }

        @DexIgnore
        @Override // com.fossil.hd6
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            b(sleepOverviewFragment);
        }

        @DexIgnore
        public final SleepOverviewFragment b(SleepOverviewFragment sleepOverviewFragment) {
            od6.a(sleepOverviewFragment, a());
            od6.a(sleepOverviewFragment, c());
            od6.a(sleepOverviewFragment, b());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public final ld6 a(ld6 ld6) {
            nd6.a(ld6);
            return ld6;
        }

        @DexIgnore
        public final ce6 a(ce6 ce6) {
            ee6.a(ce6);
            return ce6;
        }

        @DexIgnore
        public final wd6 a(wd6 wd6) {
            yd6.a(wd6);
            return wd6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v implements cr4 {
        @DexIgnore
        @Override // com.fossil.cr4
        public void a(dr4 dr4) {
            b(dr4);
        }

        @DexIgnore
        public final dr4 b(dr4 dr4) {
            er4.a(dr4, (rj4) hl4.this.g3.get());
            return dr4;
        }

        @DexIgnore
        public v() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements p26 {
        @DexIgnore
        @Override // com.fossil.p26
        public void a(s26 s26) {
            b(s26);
        }

        @DexIgnore
        public final s26 b(s26 s26) {
            t26.a(s26, (rj4) hl4.this.g3.get());
            return s26;
        }

        @DexIgnore
        public v0(u26 u26) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements zt5 {
        @DexIgnore
        public /* final */ du5 a;

        @DexIgnore
        public final gu5 a() {
            gu5 a2 = hu5.a(fu5.a(this.a), (rl4) hl4.this.R.get(), new vu5(), b(), eu5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final yu5 b() {
            return new yu5((NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public v1(du5 du5) {
            this.a = du5;
        }

        @DexIgnore
        @Override // com.fossil.zt5
        public void a(NotificationContactsActivity notificationContactsActivity) {
            b(notificationContactsActivity);
        }

        @DexIgnore
        public final NotificationContactsActivity b(NotificationContactsActivity notificationContactsActivity) {
            dl5.a(notificationContactsActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationContactsActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationContactsActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationContactsActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationContactsActivity, new xm5());
            yt5.a(notificationContactsActivity, a());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final gu5 a(gu5 gu5) {
            iu5.a(gu5);
            return gu5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v2 implements rt6 {
        @DexIgnore
        public /* final */ ut6 a;

        @DexIgnore
        public final wt6 a() {
            wt6 a2 = xt6.a(vt6.a(this.a), (UserRepository) hl4.this.A.get(), (ph5) hl4.this.i3.get(), (ThemeRepository) hl4.this.v1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SplashScreenActivity b(SplashScreenActivity splashScreenActivity) {
            dl5.a(splashScreenActivity, (UserRepository) hl4.this.A.get());
            dl5.a(splashScreenActivity, (ch5) hl4.this.c.get());
            dl5.a(splashScreenActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(splashScreenActivity, (ll4) hl4.this.T1.get());
            dl5.a(splashScreenActivity, new xm5());
            zt6.a(splashScreenActivity, a());
            return splashScreenActivity;
        }

        @DexIgnore
        public v2(ut6 ut6) {
            this.a = ut6;
        }

        @DexIgnore
        @Override // com.fossil.rt6
        public void a(SplashScreenActivity splashScreenActivity) {
            b(splashScreenActivity);
        }

        @DexIgnore
        public final wt6 a(wt6 wt6) {
            yt6.a(wt6);
            return wt6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w implements rs4 {
        @DexIgnore
        @Override // com.fossil.rs4
        public void a(ss4 ss4) {
            b(ss4);
        }

        @DexIgnore
        public final ss4 b(ss4 ss4) {
            ts4.a(ss4, (rj4) hl4.this.g3.get());
            return ss4;
        }

        @DexIgnore
        public w() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements p66 {
        @DexIgnore
        @Override // com.fossil.p66
        public void a(mz5 mz5) {
            b(mz5);
        }

        @DexIgnore
        public final mz5 b(mz5 mz5) {
            nz5.a(mz5, (rj4) hl4.this.g3.get());
            return mz5;
        }

        @DexIgnore
        public w0() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements ix5 {
        @DexIgnore
        public /* final */ mx5 a;

        @DexIgnore
        public final px5 a() {
            px5 a2 = qx5.a(ox5.a(this.a), b(), nx5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader b() {
            return NotificationsLoader_Factory.newInstance((Context) hl4.this.b.get(), (NotificationsRepository) hl4.this.K.get());
        }

        @DexIgnore
        public w1(mx5 mx5) {
            this.a = mx5;
        }

        @DexIgnore
        @Override // com.fossil.ix5
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            b(notificationDialLandingActivity);
        }

        @DexIgnore
        public final NotificationDialLandingActivity b(NotificationDialLandingActivity notificationDialLandingActivity) {
            dl5.a(notificationDialLandingActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationDialLandingActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationDialLandingActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationDialLandingActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationDialLandingActivity, new xm5());
            hx5.a(notificationDialLandingActivity, a());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final px5 a(px5 px5) {
            rx5.a(px5);
            return px5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w2 implements lk6 {
        @DexIgnore
        @Override // com.fossil.lk6
        public void a(mk6 mk6) {
            b(mk6);
        }

        @DexIgnore
        public final mk6 b(mk6 mk6) {
            nk6.a(mk6, (rj4) hl4.this.g3.get());
            return mk6;
        }

        @DexIgnore
        public w2(ok6 ok6) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x implements ir4 {
        @DexIgnore
        @Override // com.fossil.ir4
        public void a(jr4 jr4) {
            b(jr4);
        }

        @DexIgnore
        public final jr4 b(jr4 jr4) {
            lr4.a(jr4, (rj4) hl4.this.g3.get());
            return jr4;
        }

        @DexIgnore
        public x() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements t66 {
        @DexIgnore
        public /* final */ u66 a;

        @DexIgnore
        public final j76 a() {
            j76 a2 = k76.a(v66.a(this.a), (SummariesRepository) hl4.this.F.get(), hl4.this.Z(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final n86 b() {
            n86 a2 = o86.a(w66.a(this.a), (SummariesRepository) hl4.this.F.get(), hl4.this.Z(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final s96 c() {
            s96 a2 = t96.a(x66.a(this.a), (SummariesRepository) hl4.this.F.get(), hl4.this.Z(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final wa6 d() {
            wa6 a2 = xa6.a(y66.a(this.a), (GoalTrackingRepository) hl4.this.c0.get(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ac6 e() {
            ac6 a2 = bc6.a(z66.a(this.a), (HeartRateSummaryRepository) hl4.this.p0.get(), hl4.this.Z(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ed6 f() {
            ed6 a2 = fd6.a(a76.a(this.a), (SleepSummariesRepository) hl4.this.G.get(), (SleepSessionsRepository) hl4.this.b0.get(), hl4.this.Z(), (UserRepository) hl4.this.A.get(), (pj4) hl4.this.I.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public x0(u66 u66) {
            this.a = u66;
        }

        @DexIgnore
        @Override // com.fossil.t66
        public void a(sr5 sr5) {
            b(sr5);
        }

        @DexIgnore
        public final sr5 b(sr5 sr5) {
            ur5.a(sr5, b());
            ur5.a(sr5, a());
            ur5.a(sr5, c());
            ur5.a(sr5, e());
            ur5.a(sr5, f());
            ur5.a(sr5, d());
            return sr5;
        }

        @DexIgnore
        public final n86 a(n86 n86) {
            p86.a(n86);
            return n86;
        }

        @DexIgnore
        public final j76 a(j76 j76) {
            l76.a(j76);
            return j76;
        }

        @DexIgnore
        public final s96 a(s96 s96) {
            u96.a(s96);
            return s96;
        }

        @DexIgnore
        public final ac6 a(ac6 ac6) {
            cc6.a(ac6);
            return ac6;
        }

        @DexIgnore
        public final ed6 a(ed6 ed6) {
            gd6.a(ed6);
            return ed6;
        }

        @DexIgnore
        public final wa6 a(wa6 wa6) {
            ya6.a(wa6);
            return wa6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements tx5 {
        @DexIgnore
        public /* final */ xx5 a;

        @DexIgnore
        public final dy5 a() {
            return new dy5((Context) hl4.this.b.get());
        }

        @DexIgnore
        public final ay5 b() {
            ay5 a2 = by5.a(zx5.a(this.a), this.a.a(), yx5.a(this.a), (rl4) hl4.this.R.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public x1(xx5 xx5) {
            this.a = xx5;
        }

        @DexIgnore
        @Override // com.fossil.tx5
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            b(notificationHybridAppActivity);
        }

        @DexIgnore
        public final NotificationHybridAppActivity b(NotificationHybridAppActivity notificationHybridAppActivity) {
            dl5.a(notificationHybridAppActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationHybridAppActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationHybridAppActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationHybridAppActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationHybridAppActivity, new xm5());
            sx5.a(notificationHybridAppActivity, b());
            return notificationHybridAppActivity;
        }

        @DexIgnore
        public final ay5 a(ay5 ay5) {
            cy5.a(ay5);
            return ay5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x2 implements bu6 {
        @DexIgnore
        public /* final */ fu6 a;

        @DexIgnore
        public final hu6 a() {
            hu6 a2 = iu6.a(gu6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity b(TroubleshootingActivity troubleshootingActivity) {
            dl5.a(troubleshootingActivity, (UserRepository) hl4.this.A.get());
            dl5.a(troubleshootingActivity, (ch5) hl4.this.c.get());
            dl5.a(troubleshootingActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(troubleshootingActivity, (ll4) hl4.this.T1.get());
            dl5.a(troubleshootingActivity, new xm5());
            au6.a(troubleshootingActivity, a());
            return troubleshootingActivity;
        }

        @DexIgnore
        public x2(fu6 fu6) {
            this.a = fu6;
        }

        @DexIgnore
        @Override // com.fossil.bu6
        public void a(TroubleshootingActivity troubleshootingActivity) {
            b(troubleshootingActivity);
        }

        @DexIgnore
        public final hu6 a(hu6 hu6) {
            ju6.a(hu6);
            return hu6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y implements nq6 {
        @DexIgnore
        public /* final */ qq6 a;

        @DexIgnore
        public final sq6 a() {
            sq6 a2 = tq6.a(rq6.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final oo5 b(oo5 oo5) {
            qo5.a(oo5, a());
            return oo5;
        }

        @DexIgnore
        public y(qq6 qq6) {
            this.a = qq6;
        }

        @DexIgnore
        @Override // com.fossil.nq6
        public void a(oo5 oo5) {
            b(oo5);
        }

        @DexIgnore
        public final li6 b(li6 li6) {
            ni6.a(li6, (rj4) hl4.this.g3.get());
            ni6.a(li6, a());
            return li6;
        }

        @DexIgnore
        @Override // com.fossil.nq6
        public void a(li6 li6) {
            b(li6);
        }

        @DexIgnore
        public final sq6 a(sq6 sq6) {
            uq6.a(sq6);
            return sq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements jj6 {
        @DexIgnore
        public /* final */ mj6 a;

        @DexIgnore
        public final oj6 a() {
            oj6 a2 = pj6.a(nj6.a(this.a), (DeviceRepository) hl4.this.P.get(), (qd5) hl4.this.R0.get(), (UserRepository) hl4.this.A.get(), b(), hl4.this.M());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final fn5 b() {
            return new fn5((UserRepository) hl4.this.A.get(), (DeviceRepository) hl4.this.P.get(), (ch5) hl4.this.c.get());
        }

        @DexIgnore
        public y0(mj6 mj6) {
            this.a = mj6;
        }

        @DexIgnore
        @Override // com.fossil.jj6
        public void a(DeleteAccountActivity deleteAccountActivity) {
            b(deleteAccountActivity);
        }

        @DexIgnore
        public final DeleteAccountActivity b(DeleteAccountActivity deleteAccountActivity) {
            dl5.a(deleteAccountActivity, (UserRepository) hl4.this.A.get());
            dl5.a(deleteAccountActivity, (ch5) hl4.this.c.get());
            dl5.a(deleteAccountActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(deleteAccountActivity, (ll4) hl4.this.T1.get());
            dl5.a(deleteAccountActivity, new xm5());
            ij6.a(deleteAccountActivity, a());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final oj6 a(oj6 oj6) {
            qj6.a(oj6);
            return oj6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements fy5 {
        @DexIgnore
        public /* final */ jy5 a;

        @DexIgnore
        public final ny5 a() {
            ny5 a2 = oy5.a(my5.a(this.a), this.a.b(), ky5.a(this.a), ly5.a(this.a), (rl4) hl4.this.R.get(), new qy5());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity b(NotificationHybridContactActivity notificationHybridContactActivity) {
            dl5.a(notificationHybridContactActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationHybridContactActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationHybridContactActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationHybridContactActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationHybridContactActivity, new xm5());
            ey5.a(notificationHybridContactActivity, a());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public y1(jy5 jy5) {
            this.a = jy5;
        }

        @DexIgnore
        @Override // com.fossil.fy5
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            b(notificationHybridContactActivity);
        }

        @DexIgnore
        public final ny5 a(ny5 ny5) {
            py5.a(ny5);
            return ny5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y2 implements wp6 {
        @DexIgnore
        public /* final */ zp6 a;

        @DexIgnore
        public final tl5 a() {
            return new tl5((PortfolioApp) hl4.this.d.get(), (GuestApiService) hl4.this.H.get());
        }

        @DexIgnore
        public final bq6 b() {
            bq6 a2 = cq6.a(aq6.a(this.a), (DeviceRepository) hl4.this.P.get(), (UserRepository) hl4.this.A.get(), hl4.this.N(), (ch5) hl4.this.c.get(), hl4.this.z0(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public y2(zp6 zp6) {
            this.a = zp6;
        }

        @DexIgnore
        @Override // com.fossil.wp6
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            b(updateFirmwareActivity);
        }

        @DexIgnore
        public final UpdateFirmwareActivity b(UpdateFirmwareActivity updateFirmwareActivity) {
            dl5.a(updateFirmwareActivity, (UserRepository) hl4.this.A.get());
            dl5.a(updateFirmwareActivity, (ch5) hl4.this.c.get());
            dl5.a(updateFirmwareActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(updateFirmwareActivity, (ll4) hl4.this.T1.get());
            dl5.a(updateFirmwareActivity, new xm5());
            vq6.a(updateFirmwareActivity, b());
            return updateFirmwareActivity;
        }

        @DexIgnore
        public final bq6 a(bq6 bq6) {
            dq6.a(bq6);
            return bq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z {
        @DexIgnore
        public wj4 a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public MicroAppSettingRepositoryModule c;
        @DexIgnore
        public RepositoriesModule d;
        @DexIgnore
        public NotificationsRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public z a(wj4 wj4) {
            c87.a(wj4);
            this.a = wj4;
            return this;
        }

        @DexIgnore
        public z() {
        }

        @DexIgnore
        public tj4 a() {
            c87.a(this.a, wj4.class);
            if (this.b == null) {
                this.b = new PortfolioDatabaseModule();
            }
            if (this.c == null) {
                this.c = new MicroAppSettingRepositoryModule();
            }
            if (this.d == null) {
                this.d = new RepositoriesModule();
            }
            if (this.e == null) {
                this.e = new NotificationsRepositoryModule();
            }
            if (this.f == null) {
                this.f = new UAppSystemVersionRepositoryModule();
            }
            return new hl4(this.a, this.b, this.c, this.d, this.e, this.f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements pz5 {
        @DexIgnore
        public /* final */ uz5 a;

        @DexIgnore
        public final og5 a() {
            return new og5((PortfolioApp) hl4.this.d.get(), (DeviceRepository) hl4.this.P.get());
        }

        @DexIgnore
        public final wz5 b() {
            wz5 a2 = xz5.a(vz5.a(this.a), (UserRepository) hl4.this.A.get(), this.a.a(), c(), a(), (ch5) hl4.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final z46 c() {
            return new z46((DianaPresetRepository) hl4.this.s.get(), hl4.this.G(), hl4.this.E0(), hl4.this.H0());
        }

        @DexIgnore
        public z0(uz5 uz5) {
            this.a = uz5;
        }

        @DexIgnore
        @Override // com.fossil.pz5
        public void a(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            b(dianaCustomizeEditActivity);
        }

        @DexIgnore
        public final DianaCustomizeEditActivity b(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            dl5.a(dianaCustomizeEditActivity, (UserRepository) hl4.this.A.get());
            dl5.a(dianaCustomizeEditActivity, (ch5) hl4.this.c.get());
            dl5.a(dianaCustomizeEditActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(dianaCustomizeEditActivity, (ll4) hl4.this.T1.get());
            dl5.a(dianaCustomizeEditActivity, new xm5());
            oz5.a(dianaCustomizeEditActivity, b());
            oz5.a(dianaCustomizeEditActivity, (rj4) hl4.this.g3.get());
            return dianaCustomizeEditActivity;
        }

        @DexIgnore
        public final wz5 a(wz5 wz5) {
            yz5.a(wz5);
            return wz5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z1 implements sy5 {
        @DexIgnore
        public /* final */ wy5 a;

        @DexIgnore
        public final zy5 a() {
            zy5 a2 = az5.a(yy5.a(this.a), this.a.b(), xy5.a(this.a), (rl4) hl4.this.R.get(), new qy5());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity b(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            dl5.a(notificationHybridEveryoneActivity, (UserRepository) hl4.this.A.get());
            dl5.a(notificationHybridEveryoneActivity, (ch5) hl4.this.c.get());
            dl5.a(notificationHybridEveryoneActivity, (DeviceRepository) hl4.this.P.get());
            dl5.a(notificationHybridEveryoneActivity, (ll4) hl4.this.T1.get());
            dl5.a(notificationHybridEveryoneActivity, new xm5());
            ry5.a(notificationHybridEveryoneActivity, a());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public z1(wy5 wy5) {
            this.a = wy5;
        }

        @DexIgnore
        @Override // com.fossil.sy5
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            b(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final zy5 a(zy5 zy5) {
            bz5.a(zy5);
            return zy5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z2 implements bn6 {
        @DexIgnore
        @Override // com.fossil.bn6
        public void a(cn6 cn6) {
            b(cn6);
        }

        @DexIgnore
        public final cn6 b(cn6 cn6) {
            dn6.a(cn6, (rj4) hl4.this.g3.get());
            return cn6;
        }

        @DexIgnore
        public z2(en6 en6) {
        }
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(ah5 ah5) {
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public hl4(wj4 wj4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.a = wj4;
        a(wj4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        b(wj4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
        c(wj4, portfolioDatabaseModule, microAppSettingRepositoryModule, repositoriesModule, notificationsRepositoryModule, uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static z L0() {
        return new z();
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ht4 A() {
        return new p();
    }

    @DexIgnore
    public final on5 A0() {
        return new on5(this.A.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public et4 B() {
        return new o();
    }

    @DexIgnore
    public final nw6 B0() {
        return new nw6(this.P.get(), this.A.get(), L(), this.d.get());
    }

    @DexIgnore
    public final CategoryRemoteDataSource C() {
        return new CategoryRemoteDataSource(this.q.get());
    }

    @DexIgnore
    public final WatchAppDataRemoteDataSource C0() {
        return new WatchAppDataRemoteDataSource(this.q.get());
    }

    @DexIgnore
    public final CategoryRepository D() {
        return new CategoryRepository(this.T0.get(), C());
    }

    @DexIgnore
    public final WatchAppDataRepository D0() {
        return new WatchAppDataRepository(this.Q0.get(), C0(), this.M0.get());
    }

    @DexIgnore
    public final xv6 E() {
        return new xv6(this.A.get());
    }

    @DexIgnore
    public final WatchAppLastSettingRepository E0() {
        return new WatchAppLastSettingRepository(this.n0.get());
    }

    @DexIgnore
    public final yv6 F() {
        return new yv6(this.A.get());
    }

    @DexIgnore
    public final WatchAppRepository F0() {
        return new WatchAppRepository(this.e0.get(), this.f0.get(), this.d.get());
    }

    @DexIgnore
    public final ComplicationLastSettingRepository G() {
        return new ComplicationLastSettingRepository(this.m0.get());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource G0() {
        return new WatchFaceRemoteDataSource(this.q.get());
    }

    @DexIgnore
    public final ComplicationRepository H() {
        return new ComplicationRepository(this.g0.get(), this.h0.get(), this.d.get());
    }

    @DexIgnore
    public final WatchFaceRepository H0() {
        return new WatchFaceRepository(this.b.get(), this.N0.get(), G0(), this.M0.get());
    }

    @DexIgnore
    public final uw6 I() {
        uw6 a4 = vw6.a(this.d.get(), this.K.get(), this.P.get(), this.j.get(), this.I.get(), this.Q.get(), x0(), this.c.get());
        a(a4);
        return a4;
    }

    @DexIgnore
    public final WatchLocalizationRepository I0() {
        return new WatchLocalizationRepository(this.q.get(), this.c.get());
    }

    @DexIgnore
    public final zv6 J() {
        return new zv6(this.s.get(), this.v.get());
    }

    @DexIgnore
    public final WorkoutSettingRemoteDataSource J0() {
        return new WorkoutSettingRemoteDataSource(this.q.get());
    }

    @DexIgnore
    public final zz6 K() {
        return new zz6(o0());
    }

    @DexIgnore
    public final WorkoutSettingRepository K0() {
        return new WorkoutSettingRepository(J0());
    }

    @DexIgnore
    public final aw6 L() {
        return new aw6(this.c.get());
    }

    @DexIgnore
    public final qn5 M() {
        return new qn5(this.A.get(), this.C.get(), this.c.get(), this.V.get(), this.W.get(), this.F.get(), this.a0.get(), this.K.get(), this.P.get(), this.b0.get(), this.c0.get(), this.d0.get(), g0(), this.G.get(), this.s.get(), F0(), H(), this.j.get(), this.e.get(), this.k0.get(), p0(), G(), E0(), Z(), this.o0.get(), this.p0.get(), this.q0.get(), this.r0.get(), this.w0.get(), this.A0.get(), this.E0.get(), this.I0.get(), this.M0.get(), this.i.get(), H0(), s0(), this.P0.get(), K0(), D0());
    }

    @DexIgnore
    public final ad5 N() {
        return new ad5(b0(), c0(), h0(), O());
    }

    @DexIgnore
    public final rl5 O() {
        return new rl5(this.s.get(), this.c.get(), new nw5(), this.d.get(), this.P.get(), this.j.get(), new vu5(), B0(), z0(), this.R0.get(), H0(), this.C.get(), K0(), this.s.get());
    }

    @DexIgnore
    public final rn5 P() {
        return new rn5(this.A.get());
    }

    @DexIgnore
    public final cw6 Q() {
        return new cw6(this.c.get());
    }

    @DexIgnore
    public final in5 R() {
        return new in5(this.W.get(), this.A.get(), Z());
    }

    @DexIgnore
    public final ym5 S() {
        return new ym5(this.c0.get(), this.A.get());
    }

    @DexIgnore
    public final an5 T() {
        return new an5(this.p0.get(), this.A.get(), Z());
    }

    @DexIgnore
    public final zm5 U() {
        return new zm5(this.c0.get(), this.A.get());
    }

    @DexIgnore
    public final bn5 V() {
        return new bn5(this.o0.get(), Z(), this.A.get());
    }

    @DexIgnore
    public final kn5 W() {
        return new kn5(this.b0.get(), this.A.get(), Z());
    }

    @DexIgnore
    public final ln5 X() {
        return new ln5(this.G.get(), this.A.get(), this.b0.get(), Z());
    }

    @DexIgnore
    public final jn5 Y() {
        return new jn5(this.F.get(), Z(), this.A.get(), this.W.get());
    }

    @DexIgnore
    public final FitnessDataRepository Z() {
        return new FitnessDataRepository(this.q.get());
    }

    @DexIgnore
    public final void a(wj4 wj4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        Provider<Context> a4 = z77.a(ak4.a(wj4));
        this.b = a4;
        this.c = z77.a(tk4.a(wj4, a4));
        Provider<PortfolioApp> a5 = z77.a(ck4.a(wj4));
        this.d = a5;
        this.e = z77.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(portfolioDatabaseModule, a5));
        Provider<QuickResponseDatabase> a6 = z77.a(PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.f = a6;
        this.g = z77.a(PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory.create(portfolioDatabaseModule, a6));
        Provider<QuickResponseSenderDao> a7 = z77.a(PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory.create(portfolioDatabaseModule, this.f));
        this.h = a7;
        this.i = z77.a(QuickResponseRepository_Factory.create(this.g, a7));
        Provider<NotificationSettingsDatabase> a8 = z77.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.j = a8;
        this.k = z77.a(ok5.a(this.c, this.e, this.i, a8));
        Provider<DianaCustomizeDatabase> a9 = z77.a(PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.l = a9;
        this.m = z77.a(PortfolioDatabaseModule_ProvideDianaPresetDaoFactory.create(portfolioDatabaseModule, a9));
        Provider<AuthApiGuestService> a10 = z77.a(dk4.a(wj4));
        this.n = a10;
        this.o = z77.a(ij5.a(this.d, a10, this.c));
        Provider<lj5> a11 = z77.a(mj5.a(this.n, this.c));
        this.p = a11;
        Provider<ApiServiceV2> a12 = z77.a(zj4.a(wj4, this.o, a11));
        this.q = a12;
        Provider<DianaPresetRemoteDataSource> a13 = z77.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(repositoriesModule, a12));
        this.r = a13;
        this.s = z77.a(DianaPresetRepository_Factory.create(this.m, a13));
        Provider<CustomizeRealDataDatabase> a14 = z77.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.t = a14;
        Provider<CustomizeRealDataDao> a15 = z77.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(portfolioDatabaseModule, a14));
        this.u = a15;
        this.v = z77.a(CustomizeRealDataRepository_Factory.create(a15));
        Provider<AuthApiUserService> a16 = z77.a(ek4.a(wj4, this.o, this.p));
        this.w = a16;
        this.x = UserRemoteDataSource_Factory.create(this.q, this.n, a16);
        Provider<UserSettingDatabase> a17 = z77.a(PortfolioDatabaseModule_ProvideUserSettingDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.y = a17;
        Provider<UserSettingDao> a18 = z77.a(PortfolioDatabaseModule_ProvideUserSettingDaoFactory.create(portfolioDatabaseModule, a17));
        this.z = a18;
        this.A = z77.a(UserRepository_Factory.create(this.x, a18, this.c));
        Provider<AlarmsRemoteDataSource> a19 = z77.a(AlarmsRemoteDataSource_Factory.create(this.q));
        this.B = a19;
        Provider<AlarmsRepository> a20 = z77.a(AlarmsRepository_Factory.create(a19));
        this.C = a20;
        this.D = z77.a(xj4.a(wj4, this.c, this.A, a20));
        Provider<ge5> a21 = z77.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(portfolioDatabaseModule, this.c));
        this.E = a21;
        this.F = z77.a(SummariesRepository_Factory.create(this.q, a21));
        this.G = z77.a(SleepSummariesRepository_Factory.create(this.q));
        this.H = z77.a(lk4.a(wj4));
        this.I = z77.a(qj4.a());
        Provider<NotificationsDataSource> a22 = z77.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(notificationsRepositoryModule));
        this.J = a22;
        this.K = z77.a(NotificationsRepository_Factory.create(a22));
        Provider<DeviceDatabase> a23 = z77.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.L = a23;
        this.M = z77.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(portfolioDatabaseModule, a23));
        this.N = z77.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(portfolioDatabaseModule, this.L));
        DeviceRemoteDataSource_Factory create = DeviceRemoteDataSource_Factory.create(this.q);
        this.O = create;
        this.P = z77.a(DeviceRepository_Factory.create(this.M, this.N, create));
        this.Q = z77.a(fk4.a(wj4));
        this.R = z77.a(vk4.a(wj4));
        Provider<HybridCustomizeDatabase> a24 = z77.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.S = a24;
        this.T = z77.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(portfolioDatabaseModule, a24));
        HybridPresetRemoteDataSource_Factory create2 = HybridPresetRemoteDataSource_Factory.create(this.q);
        this.U = create2;
        this.V = z77.a(HybridPresetRepository_Factory.create(this.T, create2));
        this.W = z77.a(ActivitiesRepository_Factory.create(this.q, this.A, this.E));
        Provider<ShortcutApiService> a25 = z77.a(uk4.a(wj4, this.o, this.p));
        this.X = a25;
        this.Y = z77.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(microAppSettingRepositoryModule, a25, this.I));
        Provider<MicroAppSettingDataSource> a26 = z77.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(microAppSettingRepositoryModule));
        this.Z = a26;
        this.a0 = z77.a(MicroAppSettingRepository_Factory.create(this.Y, a26, this.I));
        this.b0 = z77.a(SleepSessionsRepository_Factory.create(this.A, this.q));
        this.c0 = z77.a(GoalTrackingRepository_Factory.create(this.A, this.c, this.q));
        this.d0 = z77.a(pk4.a(wj4));
        this.e0 = z77.a(PortfolioDatabaseModule_ProvideWatchAppDaoFactory.create(portfolioDatabaseModule, this.l));
        this.f0 = z77.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(repositoriesModule, this.q));
        this.g0 = z77.a(PortfolioDatabaseModule_ProvideComplicationDaoFactory.create(portfolioDatabaseModule, this.l));
        this.h0 = z77.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(repositoriesModule, this.q));
        this.i0 = z77.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(portfolioDatabaseModule, this.S));
        MicroAppRemoteDataSource_Factory create3 = MicroAppRemoteDataSource_Factory.create(this.q);
        this.j0 = create3;
        this.k0 = z77.a(MicroAppRepository_Factory.create(this.i0, create3, this.d));
        this.l0 = z77.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(portfolioDatabaseModule, this.S));
        this.m0 = z77.a(PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory.create(portfolioDatabaseModule, this.l));
        this.n0 = z77.a(PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory.create(portfolioDatabaseModule, this.l));
        this.o0 = z77.a(HeartRateSampleRepository_Factory.create(this.q));
        this.p0 = z77.a(HeartRateSummaryRepository_Factory.create(this.q));
        this.q0 = z77.a(WorkoutSessionRepository_Factory.create(this.q));
        this.r0 = z77.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        Provider<BuddyChallengeDatabase> a27 = z77.a(PortfolioDatabaseModule_ProvidesBuddyChallengeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.s0 = a27;
        Provider<go4> a28 = z77.a(PortfolioDatabaseModule_ProvidesSocialProfileDaoFactory.create(portfolioDatabaseModule, a27));
        this.t0 = a28;
        this.u0 = z77.a(PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory.create(portfolioDatabaseModule, a28));
        Provider<ep4> a29 = z77.a(PortfolioDatabaseModule_ProvidesSocialProfileRemoteFactory.create(portfolioDatabaseModule, this.q));
        this.v0 = a29;
        this.w0 = z77.a(gp4.a(this.u0, a29, this.c));
        Provider<vn4> a30 = z77.a(PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory.create(portfolioDatabaseModule, this.s0));
        this.x0 = a30;
        this.y0 = z77.a(PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory.create(portfolioDatabaseModule, a30));
        Provider<wo4> a31 = z77.a(PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory.create(portfolioDatabaseModule, this.q));
        this.z0 = a31;
        this.A0 = z77.a(yo4.a(this.y0, a31, this.c));
        Provider<nn4> a32 = z77.a(PortfolioDatabaseModule_ProvidesChallengeDaoFactory.create(portfolioDatabaseModule, this.s0));
        this.B0 = a32;
        this.C0 = z77.a(PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory.create(portfolioDatabaseModule, a32));
        Provider<qo4> a33 = z77.a(PortfolioDatabaseModule_ProvidesChallengeRemoteDataSourceFactory.create(portfolioDatabaseModule, this.q));
        this.D0 = a33;
        this.E0 = z77.a(so4.a(this.C0, a33, this.c));
        Provider<co4> a34 = z77.a(PortfolioDatabaseModule_ProvidesNotificationDaoFactory.create(portfolioDatabaseModule, this.s0));
        this.F0 = a34;
        this.G0 = z77.a(PortfolioDatabaseModule_ProvidesNotificationLocalFactory.create(portfolioDatabaseModule, a34));
        Provider<ap4> a35 = z77.a(PortfolioDatabaseModule_ProvidesNotificationRemoteFactory.create(portfolioDatabaseModule, this.q));
        this.H0 = a35;
        this.I0 = z77.a(cp4.a(this.G0, a35));
        Provider<FileDatabase> a36 = z77.a(PortfolioDatabaseModule_ProvideFileDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.J0 = a36;
        this.K0 = z77.a(PortfolioDatabaseModule_ProvideFileDaoFactory.create(portfolioDatabaseModule, a36));
        Provider<qg5> a37 = z77.a(hk4.a(wj4));
        this.L0 = a37;
        this.M0 = z77.a(FileRepository_Factory.create(this.K0, a37, this.d));
        this.N0 = z77.a(PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory.create(portfolioDatabaseModule, this.l));
        this.O0 = z77.a(PortfolioDatabaseModule_ProvidesRingStyleDaoFactory.create(portfolioDatabaseModule, this.l));
        this.P0 = z77.a(uo4.a(this.c, this.q));
        this.Q0 = z77.a(PortfolioDatabaseModule_ProvidesWatchAppDataDaoFactory.create(portfolioDatabaseModule, this.l));
        this.R0 = z77.a(yj4.a(wj4));
        Provider<CategoryDatabase> a38 = z77.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.S0 = a38;
        this.T0 = z77.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(portfolioDatabaseModule, a38));
        CategoryRemoteDataSource_Factory create4 = CategoryRemoteDataSource_Factory.create(this.q);
        this.U0 = create4;
        this.V0 = CategoryRepository_Factory.create(this.T0, create4);
        this.W0 = WatchAppRepository_Factory.create(this.e0, this.f0, this.d);
    }

    @DexIgnore
    public final dw6 a0() {
        return new dw6(Q());
    }

    @DexIgnore
    public final void b(wj4 wj4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.X0 = ComplicationRepository_Factory.create(this.g0, this.h0, this.d);
        WatchFaceRemoteDataSource_Factory create = WatchFaceRemoteDataSource_Factory.create(this.q);
        this.Y0 = create;
        this.Z0 = WatchFaceRepository_Factory.create(this.b, this.N0, create, this.M0);
        RingStyleRemoteDataSource_Factory create2 = RingStyleRemoteDataSource_Factory.create(this.q);
        this.a1 = create2;
        this.b1 = RingStyleRepository_Factory.create(this.O0, create2, this.M0);
        this.c1 = WatchLocalizationRepository_Factory.create(this.q, this.c);
        this.d1 = hs6.a(this.W0, this.X0, this.s, this.V0, this.Z0, this.b1, ow5.a(), wu5.a(), this.j, this.c, this.c1, this.C);
        this.e1 = js6.a(this.V, this.k0, this.P, this.K, this.V0, this.C);
        this.f1 = vl5.a(this.k0, this.c, this.P, this.d, this.V, this.K, this.R0, this.C);
        bw6 a4 = bw6.a(this.c);
        this.g1 = a4;
        this.h1 = ow6.a(this.P, this.A, a4, this.d);
        this.i1 = qm5.a(this.P, this.c);
        WorkoutSettingRemoteDataSource_Factory create3 = WorkoutSettingRemoteDataSource_Factory.create(this.q);
        this.j1 = create3;
        this.k1 = WorkoutSettingRepository_Factory.create(create3);
        sl5 a5 = sl5.a(this.s, this.c, ow5.a(), this.d, this.P, this.j, wu5.a(), this.h1, this.i1, this.R0, this.Z0, this.C, this.k1, this.s);
        this.l1 = a5;
        this.m1 = bd5.a(this.d1, this.e1, this.f1, a5);
        Provider<AppSettingsDatabase> a6 = z77.a(PortfolioDatabaseModule_ProvideAppSettingsDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.n1 = a6;
        Provider<hm4> a7 = z77.a(PortfolioDatabaseModule_ProvidesFlagDaoFactory.create(portfolioDatabaseModule, a6));
        this.o1 = a7;
        this.p1 = z77.a(PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory.create(portfolioDatabaseModule, a7));
        Provider<km4> a8 = z77.a(PortfolioDatabaseModule_ProvidesFlagRemoteSourceFactory.create(portfolioDatabaseModule, this.q));
        this.q1 = a8;
        this.r1 = z77.a(mm4.a(this.p1, a8));
        this.s1 = z77.a(vj5.a(this.d, this.E0, this.A0, this.c));
        Provider<ThemeDatabase> a9 = z77.a(PortfolioDatabaseModule_ProvideThemeDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.t1 = a9;
        Provider<ThemeDao> a10 = z77.a(PortfolioDatabaseModule_ProvideThemeDaoFactory.create(portfolioDatabaseModule, a9));
        this.u1 = a10;
        Provider<ThemeRepository> a11 = z77.a(ThemeRepository_Factory.create(a10, this.d));
        this.v1 = a11;
        this.w1 = z77.a(bk4.a(wj4, this.d, this.c, this.V, this.V0, this.W0, this.X0, this.k0, this.s, this.P, this.A, this.C, this.m1, this.Z0, this.c1, this.b1, this.w0, this.A0, this.E0, this.M0, this.P0, this.k1, this.r1, this.s1, a11));
        this.x1 = z77.a(tj5.a(this.A));
        this.y1 = FitnessDataRepository_Factory.create(this.q);
        kk4 a12 = kk4.a(wj4, this.b, this.I, this.c);
        this.z1 = a12;
        Provider<ThirdPartyRepository> a13 = z77.a(ThirdPartyRepository_Factory.create(a12, this.W, this.d));
        this.A1 = a13;
        this.B1 = b07.a(this.W, this.F, this.b0, this.G, this.c0, this.o0, this.p0, this.y1, this.C, this.c, this.s, this.V, a13, this.E0, this.A0, this.M0, this.A, this.k1, this.P0, this.d);
        this.C1 = z77.a(oh5.a(this.s, this.c, this.Z0));
        Provider<AddressDatabase> a14 = z77.a(PortfolioDatabaseModule_ProvideAddressDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.D1 = a14;
        Provider<AddressDao> a15 = z77.a(PortfolioDatabaseModule_ProvideAddressDaoFactory.create(portfolioDatabaseModule, a14));
        this.E1 = a15;
        Provider<LocationSource> a16 = z77.a(LocationSource_Factory.create(a15));
        this.F1 = a16;
        this.G1 = z77.a(xk5.a(this.d, a16));
        this.H1 = z77.a(si5.a(this.k, this.c));
        this.I1 = z77.a(vi5.a(this.k, this.c));
        this.J1 = z77.a(wm4.a(this.R0, this.E0));
        this.K1 = lw6.a(ow5.a(), wu5.a(), this.j, this.K, this.P, this.c);
        Provider<ek5> a17 = z77.a(gk5.a());
        this.L1 = a17;
        this.M1 = z77.a(nk4.a(wj4, this.V, this.s1, this.i, this.C, this.c, this.K1, a17, this.G1));
        this.N1 = z77.a(yk4.a(wj4, this.P, this.d));
        this.O1 = z77.a(qk5.a());
        this.P1 = z77.a(gk4.a(wj4));
        this.Q1 = z77.a(al4.a(wj4));
        this.R1 = z77.a(zk4.a(wj4));
        z77.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(uAppSystemVersionRepositoryModule));
        MicroAppLastSettingRepository_Factory create4 = MicroAppLastSettingRepository_Factory.create(this.l0);
        this.S1 = create4;
        this.T1 = z77.a(sk4.a(wj4, this.c, this.A, this.e1, this.K, this.d, this.c0, this.M, this.S, create4, this.m1, this.r1));
        this.U1 = z77.a(ik4.a(wj4));
        this.V1 = z77.a(hn5.a());
        Provider<InAppNotificationDatabase> a18 = z77.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(portfolioDatabaseModule, this.d));
        this.W1 = a18;
        Provider<InAppNotificationDao> a19 = z77.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(portfolioDatabaseModule, a18));
        this.X1 = a19;
        Provider<InAppNotificationRepository> a20 = z77.a(InAppNotificationRepository_Factory.create(a19));
        this.Y1 = a20;
        this.Z1 = z77.a(mk4.a(wj4, a20));
        this.a2 = z77.a(jk4.a(wj4, this.o, this.p));
        this.b2 = z77.a(ok4.a(wj4));
        this.c2 = z77.a(xk4.a(wj4));
        this.d2 = z77.a(qk4.a(wj4));
        this.e2 = ComplicationLastSettingRepository_Factory.create(this.m0);
        WatchAppLastSettingRepository_Factory create5 = WatchAppLastSettingRepository_Factory.create(this.n0);
        this.f2 = create5;
        this.g2 = b06.a(this.s, this.X0, this.e2, this.W0, this.b1, create5, this.Z0, this.c);
        this.h2 = k56.a(this.V, this.S1, this.k0);
        this.i2 = pn5.a(this.A);
        nn5 a21 = nn5.a(this.A);
        this.j2 = a21;
        this.k2 = pi6.a(this.i2, a21);
        this.l2 = k36.a(this.Z0, this.b1);
        this.m2 = gm5.a(this.P, this.c);
        this.n2 = nm5.a(this.P, this.V, this.s, this.d, this.Z0);
        im5 a22 = im5.a(this.A, this.P, this.m1, this.d, this.c);
        this.o2 = a22;
        this.p2 = ru6.a(this.P, this.m2, this.n2, this.m1, this.c, a22, zl5.a(), this.E0, this.d);
        this.q2 = f46.a(this.c, this.A);
        this.r2 = c46.a(this.c, this.A);
        this.s2 = qk6.a(this.v1);
        tn6 a23 = tn6.a(this.k1);
        this.t2 = a23;
        this.u2 = ao6.a(a23, this.k1);
        this.v2 = vm5.a(this.F1, this.d);
        sm5 a24 = sm5.a(this.a2);
        this.w2 = a24;
        this.x2 = uo6.a(this.d, this.v2, a24);
        this.y2 = zr4.a(this.c, this.w0);
        this.z2 = oq4.a(this.w0);
        this.A2 = bq4.a(this.w0, this.P0, this.c);
        this.B2 = dt4.a(this.c, this.A0);
        this.C2 = ar4.a(this.I0);
        this.D2 = ur4.a(this.A0, this.E0);
        this.E2 = lp4.a(this.c, this.E0);
        this.F2 = wp4.a(this.A0, this.E0, this.c);
        this.G2 = ks4.a(this.c, this.E0, this.A0);
        this.H2 = vs4.a(this.E0);
        this.I2 = ps4.a(this.E0, this.c);
        this.J2 = or4.a(this.E0, this.A0, this.c);
        this.K2 = tq4.a(this.E0, this.A0);
        this.L2 = hr4.a(this.E0, this.c);
        this.M2 = hq4.a(this.E0);
        this.N2 = lt4.a(this.E0);
        em5 a25 = em5.a(this.i);
        this.O2 = a25;
        this.P2 = gv5.a(this.i, a25);
        this.Q2 = zm6.a(this.v1);
        this.R2 = ul6.a(this.v1);
    }

    @DexIgnore
    public final gs6 b0() {
        return new gs6(F0(), H(), this.s.get(), D(), H0(), s0(), new nw5(), new vu5(), this.j.get(), this.c.get(), I0(), this.C.get());
    }

    @DexIgnore
    public final void c(wj4 wj4, PortfolioDatabaseModule portfolioDatabaseModule, MicroAppSettingRepositoryModule microAppSettingRepositoryModule, RepositoriesModule repositoriesModule, NotificationsRepositoryModule notificationsRepositoryModule, UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.S2 = ol6.a(this.v1);
        this.T2 = nm6.a(this.v1);
        this.U2 = il6.a(this.v1);
        this.V2 = cl6.a(this.v1);
        this.W2 = wk6.a(this.v1);
        this.X2 = bm6.a(this.v1);
        this.Y2 = hm6.a(this.v1);
        this.Z2 = tm6.a(this.v1);
        this.a3 = gn6.a(this.v1);
        this.b3 = cv5.a(this.i);
        this.c3 = ch6.a(this.q0);
        bm5 a4 = bm5.a(this.d, this.A, this.z);
        this.d3 = a4;
        this.e3 = wi6.a(this.F, this.G, this.c0, this.A, a4);
        b87.b a5 = b87.a(46);
        a5.a((Object) a06.class, (Provider) this.g2);
        a5.a((Object) j56.class, (Provider) this.h2);
        a5.a((Object) oi6.class, (Provider) this.k2);
        a5.a((Object) c36.class, (Provider) d36.a());
        a5.a((Object) j36.class, (Provider) this.l2);
        a5.a((Object) pu6.class, (Provider) this.p2);
        a5.a((Object) e46.class, (Provider) this.q2);
        a5.a((Object) b46.class, (Provider) this.r2);
        a5.a((Object) q66.class, (Provider) r66.a());
        a5.a((Object) ju5.class, (Provider) ku5.a());
        a5.a((Object) pk6.class, (Provider) this.s2);
        a5.a((Object) zn6.class, (Provider) this.u2);
        a5.a((Object) to6.class, (Provider) this.x2);
        a5.a((Object) yr4.class, (Provider) this.y2);
        a5.a((Object) nq4.class, (Provider) this.z2);
        a5.a((Object) aq4.class, (Provider) this.A2);
        a5.a((Object) ds4.class, (Provider) es4.a());
        a5.a((Object) ct4.class, (Provider) this.B2);
        a5.a((Object) zq4.class, (Provider) this.C2);
        a5.a((Object) tr4.class, (Provider) this.D2);
        a5.a((Object) pp4.class, (Provider) qp4.a());
        a5.a((Object) kp4.class, (Provider) this.E2);
        a5.a((Object) vp4.class, (Provider) this.F2);
        a5.a((Object) js4.class, (Provider) this.G2);
        a5.a((Object) us4.class, (Provider) this.H2);
        a5.a((Object) os4.class, (Provider) this.I2);
        a5.a((Object) mr4.class, (Provider) this.J2);
        a5.a((Object) sq4.class, (Provider) this.K2);
        a5.a((Object) gr4.class, (Provider) this.L2);
        a5.a((Object) gq4.class, (Provider) this.M2);
        a5.a((Object) kt4.class, (Provider) this.N2);
        a5.a((Object) fv5.class, (Provider) this.P2);
        a5.a((Object) ym6.class, (Provider) this.Q2);
        a5.a((Object) tl6.class, (Provider) this.R2);
        a5.a((Object) nl6.class, (Provider) this.S2);
        a5.a((Object) mm6.class, (Provider) this.T2);
        a5.a((Object) hl6.class, (Provider) this.U2);
        a5.a((Object) bl6.class, (Provider) this.V2);
        a5.a((Object) vk6.class, (Provider) this.W2);
        a5.a((Object) am6.class, (Provider) this.X2);
        a5.a((Object) gm6.class, (Provider) this.Y2);
        a5.a((Object) sm6.class, (Provider) this.Z2);
        a5.a((Object) fn6.class, (Provider) this.a3);
        a5.a((Object) bv5.class, (Provider) this.b3);
        a5.a((Object) bh6.class, (Provider) this.c3);
        a5.a((Object) ui6.class, (Provider) this.e3);
        b87 a6 = a5.a();
        this.f3 = a6;
        this.g3 = z77.a(sj4.a(a6));
        this.h3 = z77.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(portfolioDatabaseModule, this.j));
        this.i3 = z77.a(rk4.a(wj4, this.c, this.T1));
        this.j3 = z77.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(repositoriesModule));
        this.k3 = z77.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(repositoriesModule, this.q));
        this.l3 = z77.a(wk4.a(wj4));
    }

    @DexIgnore
    public final is6 c0() {
        return new is6(this.V.get(), this.k0.get(), this.P.get(), this.K.get(), D(), this.C.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public vn6 d() {
        return new g3();
    }

    @DexIgnore
    public final fw6 d0() {
        return new fw6(Q(), this.P.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public kq4 e() {
        return new s();
    }

    @DexIgnore
    public final mn5 e0() {
        return new mn5(this.A.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public vg6 f() {
        return new f3();
    }

    @DexIgnore
    public final gw6 f0() {
        return new gw6(this.q.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public hp4 g() {
        return new i();
    }

    @DexIgnore
    public final ie5 g0() {
        return kk4.a(this.a, this.b.get(), this.I.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public sp4 h() {
        return new q();
    }

    @DexIgnore
    public final ul5 h0() {
        return new ul5(this.k0.get(), this.c.get(), this.P.get(), this.d.get(), this.V.get(), this.K.get(), this.R0.get(), this.C.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public cq4 i() {
        return new l();
    }

    @DexIgnore
    public final tn5 i0() {
        return new tn5(this.A.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public z26 j() {
        return new a1();
    }

    @DexIgnore
    public final un5 j0() {
        return new un5(this.P1.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public mp4 k() {
        return new j();
    }

    @DexIgnore
    public final vn5 k0() {
        return new vn5(this.d0.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public dq4 l() {
        return new r();
    }

    @DexIgnore
    public final wn5 l0() {
        return new wn5(this.A.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public cr4 m() {
        return new v();
    }

    @DexIgnore
    public final xn5 m0() {
        return new xn5(this.R1.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public mu6 n() {
        return new c3();
    }

    @DexIgnore
    public final yn5 n0() {
        return new yn5(this.Q1.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public p66 o() {
        return new w0();
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<a07<? extends ListenableWorker>>> o0() {
        return by3.of(PushPendingDataWorker.class, this.B1);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public wq4 p() {
        return new u();
    }

    @DexIgnore
    public final MicroAppLastSettingRepository p0() {
        return new MicroAppLastSettingRepository(this.l0.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ir4 q() {
        return new x();
    }

    @DexIgnore
    public final jw6 q0() {
        return new jw6(this.A.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public rs4 r() {
        return new w();
    }

    @DexIgnore
    public final RingStyleRemoteDataSource r0() {
        return new RingStyleRemoteDataSource(this.q.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public g36 s() {
        return new h2();
    }

    @DexIgnore
    public final RingStyleRepository s0() {
        return new RingStyleRepository(this.O0.get(), r0(), this.M0.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public as4 t() {
        return new h();
    }

    @DexIgnore
    public final kw6 t0() {
        return new kw6(new nw5(), new vu5(), this.j.get(), this.K.get(), this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public av5 u() {
        return new m2();
    }

    @DexIgnore
    public final dm5 u0() {
        return new dm5(this.i.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public fs4 v() {
        return new k();
    }

    @DexIgnore
    public final ao5 v0() {
        return new ao5(this.A.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public pq4 w() {
        return new t();
    }

    @DexIgnore
    public final bo5 w0() {
        return new bo5(this.A.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public qi6 x() {
        return new j2();
    }

    @DexIgnore
    public final cn5 x0() {
        return dn5.a(this.K.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public d46 y() {
        return new g0();
    }

    @DexIgnore
    public final om5 y0() {
        return new om5(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public a46 z() {
        return new f0();
    }

    @DexIgnore
    public final pm5 z0() {
        return new pm5(this.P.get(), this.c.get());
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public pr4 c() {
        return new n();
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(sh5 sh5) {
        b(sh5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ls4 b() {
        return new m();
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(PortfolioApp portfolioApp) {
        b(portfolioApp);
    }

    @DexIgnore
    public final sh5 b(sh5 sh5) {
        th5.a(sh5, this.c.get());
        th5.a(sh5, J());
        th5.a(sh5, this.D.get());
        return sh5;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(be5 be5) {
        b(be5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(df5 df5) {
        b(df5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(MFDeviceService mFDeviceService) {
        b(mFDeviceService);
    }

    @DexIgnore
    public final PortfolioApp b(PortfolioApp portfolioApp) {
        nl4.a(portfolioApp, this.c.get());
        nl4.a(portfolioApp, this.F.get());
        nl4.a(portfolioApp, this.G.get());
        nl4.a(portfolioApp, this.D.get());
        nl4.a(portfolioApp, this.H.get());
        nl4.a(portfolioApp, this.I.get());
        nl4.a(portfolioApp, this.q.get());
        nl4.a(portfolioApp, I());
        nl4.a(portfolioApp, this.R.get());
        nl4.a(portfolioApp, M());
        nl4.a(portfolioApp, this.R0.get());
        nl4.a(portfolioApp, this.w1.get());
        nl4.a(portfolioApp, this.P.get());
        nl4.a(portfolioApp, this.E.get());
        nl4.a(portfolioApp, this.x1.get());
        nl4.a(portfolioApp, this.s.get());
        nl4.a(portfolioApp, K());
        nl4.a(portfolioApp, I0());
        nl4.a(portfolioApp, this.C1.get());
        nl4.a(portfolioApp, this.A.get());
        nl4.a(portfolioApp, H0());
        nl4.a(portfolioApp, this.i.get());
        nl4.a(portfolioApp, K0());
        nl4.a(portfolioApp, D0());
        nl4.a(portfolioApp, this.G1.get());
        nl4.a(portfolioApp, this.H1.get());
        nl4.a(portfolioApp, this.I1.get());
        nl4.a(portfolioApp, this.v1.get());
        nl4.a(portfolioApp, L());
        nl4.a(portfolioApp, a0());
        nl4.a(portfolioApp, this.J1.get());
        return portfolioApp;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(FossilNotificationListenerService fossilNotificationListenerService) {
        b(fossilNotificationListenerService);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(AlarmReceiver alarmReceiver) {
        b(alarmReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(NetworkChangedReceiver networkChangedReceiver) {
        b(networkChangedReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(NotificationReceiver notificationReceiver) {
        b(notificationReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(no6 no6) {
        b(no6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(et6 et6) {
        b(et6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(kq6 kq6) {
        b(kq6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(BCNotificationActionReceiver bCNotificationActionReceiver) {
        b(bCNotificationActionReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(cl5 cl5) {
        b(cl5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(DebugActivity debugActivity) {
        b(debugActivity);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(WorkoutDetailActivity workoutDetailActivity) {
        b(workoutDetailActivity);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(CommuteTimeService commuteTimeService) {
        b(commuteTimeService);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(BootReceiver bootReceiver) {
        b(bootReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(URLRequestTaskHelper uRLRequestTaskHelper) {
        b(uRLRequestTaskHelper);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(ComplicationWeatherService complicationWeatherService) {
        b(complicationWeatherService);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(yw6 yw6) {
        b(yw6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(hw6 hw6) {
        b(hw6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(CloudImageHelper cloudImageHelper) {
        b(cloudImageHelper);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(wx6 wx6) {
        b(wx6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        b(fossilFirebaseMessagingService);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(tk5 tk5) {
        b(tk5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(rd5 rd5) {
        b(rd5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        b(appPackageRemoveReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(sg5 sg5) {
        b(sg5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(gh5 gh5) {
        b(gh5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(eh5 eh5) {
        b(eh5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(qg5 qg5) {
        b(qg5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(TimeChangeReceiver timeChangeReceiver) {
        b(timeChangeReceiver);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public void a(li5 li5) {
        b(li5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public tu6 a(wu6 wu6) {
        c87.a(wu6);
        return new a0(wu6);
    }

    @DexIgnore
    public final be5 b(be5 be5) {
        ce5.a(be5, this.c.get());
        ce5.a(be5, this.P.get());
        return be5;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public dv6 a(gv6 gv6) {
        c87.a(gv6);
        return new d1(gv6);
    }

    @DexIgnore
    public final df5 b(df5 df5) {
        ef5.a(df5, this.c.get());
        return df5;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public wp6 a(zp6 zp6) {
        c87.a(zp6);
        return new y2(zp6);
    }

    @DexIgnore
    public final MFDeviceService b(MFDeviceService mFDeviceService) {
        qj5.a(mFDeviceService, this.c.get());
        qj5.a(mFDeviceService, this.P.get());
        qj5.a(mFDeviceService, this.W.get());
        qj5.a(mFDeviceService, this.F.get());
        qj5.a(mFDeviceService, this.b0.get());
        qj5.a(mFDeviceService, this.G.get());
        qj5.a(mFDeviceService, this.A.get());
        qj5.a(mFDeviceService, this.V.get());
        qj5.a(mFDeviceService, this.k0.get());
        qj5.a(mFDeviceService, this.o0.get());
        qj5.a(mFDeviceService, this.p0.get());
        qj5.a(mFDeviceService, this.q0.get());
        qj5.a(mFDeviceService, Z());
        qj5.a(mFDeviceService, this.c0.get());
        qj5.a(mFDeviceService, this.I.get());
        qj5.a(mFDeviceService, this.R0.get());
        qj5.a(mFDeviceService, this.d.get());
        qj5.a(mFDeviceService, this.M1.get());
        qj5.a(mFDeviceService, this.A1.get());
        qj5.a(mFDeviceService, Q());
        qj5.a(mFDeviceService, B0());
        qj5.a(mFDeviceService, this.E.get());
        qj5.a(mFDeviceService, this.N1.get());
        qj5.a(mFDeviceService, this.s1.get());
        qj5.a(mFDeviceService, this.G1.get());
        qj5.a(mFDeviceService, this.L1.get());
        return mFDeviceService;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public pz5 a(uz5 uz5) {
        c87.a(uz5);
        return new z0(uz5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public f26 a(j26 j26) {
        c87.a(j26);
        return new h0(j26);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public q46 a(u46 u46) {
        c87.a(u46);
        return new a3(u46);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public re6 a(ve6 ve6) {
        c87.a(ve6);
        return new e(ve6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ef6 a(if6 if6) {
        c87.a(if6);
        return new b0(if6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public he6 a(le6 le6) {
        c87.a(le6);
        return new c(le6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ig6 a(mg6 mg6) {
        c87.a(mg6);
        return new t2(mg6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public of6 a(sf6 sf6) {
        c87.a(sf6);
        return new f1(sf6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public yf6 a(cg6 cg6) {
        c87.a(cg6);
        return new h1(cg6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public dt5 a(ht5 ht5) {
        c87.a(ht5);
        return new s1(ht5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ot5 a(tt5 tt5) {
        c87.a(tt5);
        return new t1(tt5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public lu5 a(qu5 qu5) {
        c87.a(qu5);
        return new a2(qu5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public zt5 a(du5 du5) {
        c87.a(du5);
        return new v1(du5);
    }

    @DexIgnore
    public final FossilNotificationListenerService b(FossilNotificationListenerService fossilNotificationListenerService) {
        oj5.a(fossilNotificationListenerService, this.L1.get());
        oj5.a(fossilNotificationListenerService, this.k.get());
        oj5.a(fossilNotificationListenerService, this.O1.get());
        oj5.a(fossilNotificationListenerService, this.I.get());
        oj5.a(fossilNotificationListenerService, this.c.get());
        oj5.a(fossilNotificationListenerService, this.A.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public qs5 a(vs5 vs5) {
        c87.a(vs5);
        return new k1(vs5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ov5 a(tv5 tv5) {
        c87.a(tv5);
        return new b2(tv5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ew5 a(fw5 fw5) {
        c87.a(fw5);
        return new n2(fw5);
    }

    @DexIgnore
    public final AlarmReceiver b(AlarmReceiver alarmReceiver) {
        ki5.a(alarmReceiver, this.A.get());
        ki5.a(alarmReceiver, this.c.get());
        ki5.a(alarmReceiver, this.P.get());
        ki5.a(alarmReceiver, this.D.get());
        ki5.a(alarmReceiver, this.C.get());
        return alarmReceiver;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public q86 a(z86 z86) {
        c87.a(z86);
        return new f(z86);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public hd6 a(pd6 pd6) {
        c87.a(pd6);
        return new u2(pd6);
    }

    @DexIgnore
    public final NetworkChangedReceiver b(NetworkChangedReceiver networkChangedReceiver) {
        qi5.a(networkChangedReceiver, this.A.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public za6 a(hb6 hb6) {
        c87.a(hb6);
        return new g1(hb6);
    }

    @DexIgnore
    public final NotificationReceiver b(NotificationReceiver notificationReceiver) {
        rh5.a(notificationReceiver, this.c.get());
        rh5.a(notificationReceiver, N());
        rh5.a(notificationReceiver, this.A.get());
        return notificationReceiver;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public v96 a(da6 da6) {
        c87.a(da6);
        return new c0(da6);
    }

    @DexIgnore
    public final no6 b(no6 no6) {
        po6.a(no6, i0());
        po6.a(no6, l0());
        po6.a(no6, P());
        po6.a(no6, new yl5());
        po6.a(no6, N());
        po6.a(no6, this.A.get());
        po6.a(no6, this.P.get());
        po6.a(no6, this.c.get());
        po6.a(no6, R());
        po6.a(no6, Y());
        po6.a(no6, this.R.get());
        po6.a(no6, W());
        po6.a(no6, X());
        po6.a(no6, V());
        po6.a(no6, T());
        po6.a(no6, j0());
        po6.a(no6, this.Q1.get());
        po6.a(no6, k0());
        po6.a(no6, n0());
        po6.a(no6, m0());
        po6.a(no6, F());
        po6.a(no6, this.R0.get());
        po6.a(no6, this.F.get());
        po6.a(no6, this.G.get());
        po6.a(no6, this.c0.get());
        po6.a(no6, S());
        po6.a(no6, U());
        po6.a(no6, d0());
        po6.a(no6, I0());
        po6.a(no6, this.C.get());
        po6.a(no6, this.w0.get());
        po6.a(no6, this.P0.get());
        po6.a(no6, this.r1.get());
        po6.a(no6, K0());
        po6.a(no6);
        return no6;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public m76 a(u76 u76) {
        c87.a(u76);
        return new d(u76);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public dc6 a(lc6 lc6) {
        c87.a(lc6);
        return new i1(lc6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ls6 a(qs6 qs6) {
        c87.a(qs6);
        return new f2(qs6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public qo6 a() {
        return new q1();
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public in6 a(ln6 ln6) {
        c87.a(ln6);
        return new g2(ln6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public zi6 a(dj6 dj6) {
        c87.a(dj6);
        return new j1(dj6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ci6 a(gi6 gi6) {
        c87.a(gi6);
        return new o2(gi6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public sh6 a(wh6 wh6) {
        c87.a(wh6);
        return new b(wh6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public lk6 a(ok6 ok6) {
        c87.a(ok6);
        return new w2(ok6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public um6 a(xm6 xm6) {
        c87.a(xm6);
        return new u0(xm6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public pl6 a(sl6 sl6) {
        c87.a(sl6);
        return new p0(sl6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public jl6 a(ml6 ml6) {
        c87.a(ml6);
        return new o0(ml6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public im6 a(lm6 lm6) {
        c87.a(lm6);
        return new s0(lm6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public dl6 a(gl6 gl6) {
        c87.a(gl6);
        return new n0(gl6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public xk6 a(al6 al6) {
        c87.a(al6);
        return new m0(al6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public rk6 a(uk6 uk6) {
        c87.a(uk6);
        return new l0(uk6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public wl6 a(zl6 zl6) {
        c87.a(zl6);
        return new q0(zl6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public cm6 a(fm6 fm6) {
        c87.a(fm6);
        return new r0(fm6);
    }

    @DexIgnore
    public final et6 b(et6 et6) {
        gt6.a(et6, j0());
        gt6.a(et6, this.Q1.get());
        gt6.a(et6, k0());
        gt6.a(et6, n0());
        gt6.a(et6, m0());
        gt6.a(et6, l0());
        gt6.a(et6, this.A.get());
        gt6.a(et6, this.P.get());
        gt6.a(et6, this.R.get());
        gt6.a(et6, W());
        gt6.a(et6, X());
        gt6.a(et6, R());
        gt6.a(et6, Y());
        gt6.a(et6, V());
        gt6.a(et6, T());
        gt6.a(et6, this.C.get());
        gt6.a(et6, new yl5());
        gt6.a(et6, N());
        gt6.a(et6, P());
        gt6.b(et6, this.c.get());
        gt6.a(et6, E());
        gt6.a(et6, F());
        gt6.a(et6, this.R0.get());
        gt6.a(et6, e0());
        gt6.a(et6, this.F.get());
        gt6.a(et6, this.G.get());
        gt6.a(et6, this.c0.get());
        gt6.a(et6, S());
        gt6.a(et6, U());
        gt6.a(et6, q0());
        gt6.a(et6, d0());
        gt6.a(et6, I0());
        gt6.a(et6, this.c.get());
        gt6.a(et6, this.P0.get());
        gt6.a(et6, this.r1.get());
        gt6.a(et6, K0());
        gt6.a(et6);
        return et6;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public om6 a(rm6 rm6) {
        c87.a(rm6);
        return new t0(rm6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public bn6 a(en6 en6) {
        c87.a(en6);
        return new z2(en6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public jj6 a(mj6 mj6) {
        c87.a(mj6);
        return new y0(mj6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public sj6 a(wj6 wj6) {
        c87.a(wj6);
        return new k2(wj6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ov6 a(tv6 tv6) {
        c87.a(tv6);
        return new e3(tv6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ix5 a(mx5 mx5) {
        c87.a(mx5);
        return new w1(mx5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public xw5 a(bx5 bx5) {
        c87.a(bx5);
        return new u1(bx5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public fy5 a(jy5 jy5) {
        c87.a(jy5);
        return new y1(jy5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public tx5 a(xx5 xx5) {
        c87.a(xx5);
        return new x1(xx5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public sy5 a(wy5 wy5) {
        c87.a(wy5);
        return new z1(wy5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public er6 a(hr6 hr6) {
        c87.a(hr6);
        return new e2(hr6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public or6 a(vr6 vr6) {
        c87.a(vr6);
        return new d2(vr6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public rt6 a(ut6 ut6) {
        c87.a(ut6);
        return new v2(ut6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public fs5 a(gs5 gs5) {
        c87.a(gs5);
        return new m1(gs5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public pr5 a(zr5 zr5) {
        c87.a(zr5);
        return new l1(zr5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public t66 a(u66 u66) {
        c87.a(u66);
        return new x0(u66);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public gz5 a(hz5 hz5) {
        c87.a(hz5);
        return new i0(hz5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public h06 a(m06 m06) {
        c87.a(m06);
        return new j0(m06);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public l36 a(q36 q36) {
        c87.a(q36);
        return new b3(q36);
    }

    @DexIgnore
    public final kq6 b(kq6 kq6) {
        mq6.a(kq6, v0());
        mq6.a(kq6, w0());
        mq6.a(kq6, e0());
        mq6.a(kq6, this.R0.get());
        mq6.a(kq6);
        return kq6;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public p26 a(u26 u26) {
        c87.a(u26);
        return new v0(u26);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public u06 a(g16 g16) {
        c87.a(g16);
        return new d0(g16);
    }

    @DexIgnore
    public final BCNotificationActionReceiver b(BCNotificationActionReceiver bCNotificationActionReceiver) {
        pt4.a(bCNotificationActionReceiver, this.A0.get());
        pt4.a(bCNotificationActionReceiver, this.E0.get());
        pt4.a(bCNotificationActionReceiver, this.c.get());
        return bCNotificationActionReceiver;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public y06 a(b16 b16) {
        c87.a(b16);
        return new e0(b16);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public w16 a(z16 z16) {
        c87.a(z16);
        return new r2(z16);
    }

    @DexIgnore
    public final cl5 b(cl5 cl5) {
        dl5.a(cl5, this.A.get());
        dl5.a(cl5, this.c.get());
        dl5.a(cl5, this.P.get());
        dl5.a(cl5, this.T1.get());
        dl5.a(cl5, new xm5());
        return cl5;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public h46 a(k46 k46) {
        c87.a(k46);
        return new d3(k46);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public eq5 a(iq5 iq5) {
        c87.a(iq5);
        return new g(iq5);
    }

    @DexIgnore
    public final DebugActivity b(DebugActivity debugActivity) {
        dl5.a(debugActivity, this.A.get());
        dl5.a(debugActivity, this.c.get());
        dl5.a(debugActivity, this.P.get());
        dl5.a(debugActivity, this.T1.get());
        dl5.a(debugActivity, new xm5());
        il5.a(debugActivity, this.c.get());
        il5.a(debugActivity, y0());
        il5.a(debugActivity, this.U1.get());
        il5.a(debugActivity, this.H.get());
        il5.a(debugActivity, this.s.get());
        il5.a(debugActivity, this.x1.get());
        il5.a(debugActivity, N());
        return debugActivity;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public bu6 a(fu6 fu6) {
        c87.a(fu6);
        return new x2(fu6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public xs6 a(bt6 bt6) {
        c87.a(bt6);
        return new s2(bt6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public go6 a(ko6 ko6) {
        c87.a(ko6);
        return new p1(ko6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public fp6 a(ip6 ip6) {
        c87.a(ip6);
        return new e1(ip6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public op6 a(rp6 rp6) {
        c87.a(rp6);
        return new c2(rp6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public fq6 a(iq6 iq6) {
        c87.a(iq6);
        return new l2(iq6);
    }

    @DexIgnore
    public final WorkoutDetailActivity b(WorkoutDetailActivity workoutDetailActivity) {
        ug6.a(workoutDetailActivity, this.q0.get());
        return workoutDetailActivity;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public nq6 a(qq6 qq6) {
        c87.a(qq6);
        return new y(qq6);
    }

    @DexIgnore
    public final CommuteTimeService b(CommuteTimeService commuteTimeService) {
        ck5.a(commuteTimeService, this.V1.get());
        return commuteTimeService;
    }

    @DexIgnore
    public final BootReceiver b(BootReceiver bootReceiver) {
        pi5.a(bootReceiver, this.D.get());
        return bootReceiver;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public wo6 a(zo6 zo6) {
        c87.a(zo6);
        return new c1(zo6);
    }

    @DexIgnore
    public final URLRequestTaskHelper b(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.q.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    public final ComplicationWeatherService b(ComplicationWeatherService complicationWeatherService) {
        xj5.a(complicationWeatherService, this.F1.get());
        zj5.a(complicationWeatherService, f0());
        zj5.a(complicationWeatherService, this.R.get());
        zj5.a(complicationWeatherService, this.c.get());
        zj5.a(complicationWeatherService, this.d.get());
        zj5.a(complicationWeatherService, this.v.get());
        zj5.a(complicationWeatherService, this.A.get());
        return complicationWeatherService;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public sq5 a(vq5 vq5) {
        c87.a(vq5);
        return new k0(vq5);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ck6 a(gk6 gk6) {
        c87.a(gk6);
        return new i2(gk6);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public o56 a(t56 t56) {
        c87.a(t56);
        return new n1(t56);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public y56 a(b66 b66) {
        c87.a(b66);
        return new r1(b66);
    }

    @DexIgnore
    public final yw6 b(yw6 yw6) {
        zw6.a(yw6, this.P.get());
        zw6.a(yw6, this.c.get());
        zw6.a(yw6, this.A.get());
        zw6.a(yw6, this.H.get());
        zw6.a(yw6, this.U1.get());
        return yw6;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public g56 a(h56 h56) {
        c87.a(h56);
        return new o1(h56);
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public g66 a(k66 k66) {
        c87.a(k66);
        return new p2(k66);
    }

    @DexIgnore
    public final hw6 b(hw6 hw6) {
        iw6.a(hw6, this.W.get());
        return hw6;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public m16 a(q16 q16) {
        c87.a(q16);
        return new q2(q16);
    }

    @DexIgnore
    public final CloudImageHelper b(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.I.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    @Override // com.fossil.tj4
    public ot6 a(pt6 pt6) {
        c87.a(pt6);
        return new b1(pt6);
    }

    @DexIgnore
    public final wx6 b(wx6 wx6) {
        xx6.a(wx6, this.c.get());
        return wx6;
    }

    @DexIgnore
    public final FossilFirebaseMessagingService b(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        ak5.a(fossilFirebaseMessagingService, this.Z1.get());
        ak5.a(fossilFirebaseMessagingService, this.s1.get());
        ak5.a(fossilFirebaseMessagingService, this.c.get());
        ak5.a(fossilFirebaseMessagingService, this.P0.get());
        ak5.a(fossilFirebaseMessagingService, this.r1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    public final uw6 a(uw6 uw6) {
        ww6.a(uw6, this.P.get());
        ww6.a(uw6, this.j.get());
        return uw6;
    }

    @DexIgnore
    public final tk5 b(tk5 tk5) {
        vk5.a(tk5, this.d.get());
        vk5.a(tk5, this.q.get());
        vk5.a(tk5, this.F1.get());
        vk5.a(tk5, this.A.get());
        vk5.a(tk5, this.c.get());
        vk5.a(tk5, this.s.get());
        return tk5;
    }

    @DexIgnore
    public final rd5 b(rd5 rd5) {
        sd5.a(rd5, this.c.get());
        sd5.a(rd5, this.P.get());
        sd5.a(rd5, this.A.get());
        sd5.a(rd5, L());
        return rd5;
    }

    @DexIgnore
    public final AppPackageRemoveReceiver b(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        ni5.a(appPackageRemoveReceiver, this.K.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    public final sg5 b(sg5 sg5) {
        ug5.a(sg5, this.P.get());
        ug5.a(sg5, this.c.get());
        return sg5;
    }

    @DexIgnore
    public final gh5 b(gh5 gh5) {
        hh5.a(gh5, this.d.get());
        hh5.a(gh5, this.q.get());
        hh5.a(gh5, this.F1.get());
        hh5.a(gh5, this.A.get());
        hh5.a(gh5, this.v.get());
        hh5.a(gh5, this.s.get());
        hh5.a(gh5, this.a2.get());
        return gh5;
    }

    @DexIgnore
    public final eh5 b(eh5 eh5) {
        fh5.a(eh5, this.v1.get());
        return eh5;
    }

    @DexIgnore
    public final qg5 b(qg5 qg5) {
        rg5.a(qg5, this.q.get());
        return qg5;
    }

    @DexIgnore
    public final TimeChangeReceiver b(TimeChangeReceiver timeChangeReceiver) {
        c07.a(timeChangeReceiver, this.D.get());
        c07.a(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }

    @DexIgnore
    public final li5 b(li5 li5) {
        mi5.a(li5, this.K.get());
        mi5.a(li5, this.c.get());
        mi5.a(li5, N());
        mi5.a(li5, new nw5());
        mi5.a(li5, new vu5());
        mi5.a(li5, this.j.get());
        return li5;
    }
}
