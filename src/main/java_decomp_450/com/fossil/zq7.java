package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zq7 extends qr7, WritableByteChannel {
    @DexIgnore
    long a(sr7 sr7) throws IOException;

    @DexIgnore
    zq7 a(br7 br7) throws IOException;

    @DexIgnore
    zq7 a(String str) throws IOException;

    @DexIgnore
    yq7 buffer();

    @DexIgnore
    zq7 c(long j) throws IOException;

    @DexIgnore
    @Override // com.fossil.qr7, java.io.Flushable
    void flush() throws IOException;

    @DexIgnore
    zq7 i(long j) throws IOException;

    @DexIgnore
    zq7 j() throws IOException;

    @DexIgnore
    OutputStream s();

    @DexIgnore
    zq7 write(byte[] bArr) throws IOException;

    @DexIgnore
    zq7 write(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    zq7 writeByte(int i) throws IOException;

    @DexIgnore
    zq7 writeInt(int i) throws IOException;

    @DexIgnore
    zq7 writeShort(int i) throws IOException;
}
