package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z34 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ v64 b;

    @DexIgnore
    public z34(String str, v64 v64) {
        this.a = str;
        this.b = v64;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            z24 a2 = z24.a();
            a2.b("Error creating marker: " + this.a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.b(), this.a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
