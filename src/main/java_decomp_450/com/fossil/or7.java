package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or7 {
    @DexIgnore
    public static nr7 a;
    @DexIgnore
    public static long b;
    @DexIgnore
    public static /* final */ or7 c; // = new or7();

    @DexIgnore
    public final void a(nr7 nr7) {
        ee7.b(nr7, "segment");
        if (!(nr7.f == null && nr7.g == null)) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!nr7.d) {
            synchronized (this) {
                long j = (long) 8192;
                if (b + j <= 65536) {
                    b += j;
                    nr7.f = a;
                    nr7.c = 0;
                    nr7.b = 0;
                    a = nr7;
                    i97 i97 = i97.a;
                }
            }
        }
    }

    @DexIgnore
    public final nr7 a() {
        synchronized (this) {
            nr7 nr7 = a;
            if (nr7 == null) {
                return new nr7();
            }
            a = nr7.f;
            nr7.f = null;
            b -= (long) 8192;
            return nr7;
        }
    }
}
