package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hv3 {
    @DexIgnore
    public static /* final */ zu3 m; // = new fv3(0.5f);
    @DexIgnore
    public av3 a;
    @DexIgnore
    public av3 b;
    @DexIgnore
    public av3 c;
    @DexIgnore
    public av3 d;
    @DexIgnore
    public zu3 e;
    @DexIgnore
    public zu3 f;
    @DexIgnore
    public zu3 g;
    @DexIgnore
    public zu3 h;
    @DexIgnore
    public cv3 i;
    @DexIgnore
    public cv3 j;
    @DexIgnore
    public cv3 k;
    @DexIgnore
    public cv3 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public av3 a; // = ev3.a();
        @DexIgnore
        public av3 b; // = ev3.a();
        @DexIgnore
        public av3 c; // = ev3.a();
        @DexIgnore
        public av3 d; // = ev3.a();
        @DexIgnore
        public zu3 e; // = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zu3 f; // = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zu3 g; // = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zu3 h; // = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public cv3 i; // = ev3.b();
        @DexIgnore
        public cv3 j; // = ev3.b();
        @DexIgnore
        public cv3 k; // = ev3.b();
        @DexIgnore
        public cv3 l; // = ev3.b();

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b a(float f2) {
            d(f2);
            e(f2);
            c(f2);
            b(f2);
            return this;
        }

        @DexIgnore
        public b b(zu3 zu3) {
            this.g = zu3;
            return this;
        }

        @DexIgnore
        public b c(zu3 zu3) {
            this.e = zu3;
            return this;
        }

        @DexIgnore
        public b d(float f2) {
            this.e = new xu3(f2);
            return this;
        }

        @DexIgnore
        public b e(float f2) {
            this.f = new xu3(f2);
            return this;
        }

        @DexIgnore
        public static float e(av3 av3) {
            if (av3 instanceof gv3) {
                return ((gv3) av3).a;
            }
            if (av3 instanceof bv3) {
                return ((bv3) av3).a;
            }
            return -1.0f;
        }

        @DexIgnore
        public b b(float f2) {
            this.h = new xu3(f2);
            return this;
        }

        @DexIgnore
        public b c(float f2) {
            this.g = new xu3(f2);
            return this;
        }

        @DexIgnore
        public b d(zu3 zu3) {
            this.f = zu3;
            return this;
        }

        @DexIgnore
        public b b(int i2, zu3 zu3) {
            b(ev3.a(i2));
            b(zu3);
            return this;
        }

        @DexIgnore
        public b c(int i2, zu3 zu3) {
            c(ev3.a(i2));
            c(zu3);
            return this;
        }

        @DexIgnore
        public b d(int i2, zu3 zu3) {
            d(ev3.a(i2));
            d(zu3);
            return this;
        }

        @DexIgnore
        public b a(zu3 zu3) {
            this.h = zu3;
            return this;
        }

        @DexIgnore
        public b b(av3 av3) {
            this.c = av3;
            float e2 = e(av3);
            if (e2 != -1.0f) {
                c(e2);
            }
            return this;
        }

        @DexIgnore
        public b c(av3 av3) {
            this.a = av3;
            float e2 = e(av3);
            if (e2 != -1.0f) {
                d(e2);
            }
            return this;
        }

        @DexIgnore
        public b d(av3 av3) {
            this.b = av3;
            float e2 = e(av3);
            if (e2 != -1.0f) {
                e(e2);
            }
            return this;
        }

        @DexIgnore
        public b a(int i2, zu3 zu3) {
            a(ev3.a(i2));
            a(zu3);
            return this;
        }

        @DexIgnore
        public b a(av3 av3) {
            this.d = av3;
            float e2 = e(av3);
            if (e2 != -1.0f) {
                b(e2);
            }
            return this;
        }

        @DexIgnore
        public b a(cv3 cv3) {
            this.i = cv3;
            return this;
        }

        @DexIgnore
        public hv3 a() {
            return new hv3(this);
        }

        @DexIgnore
        public b(hv3 hv3) {
            this.a = hv3.a;
            this.b = hv3.b;
            this.c = hv3.c;
            this.d = hv3.d;
            this.e = hv3.e;
            this.f = hv3.f;
            this.g = hv3.g;
            this.h = hv3.h;
            this.i = hv3.i;
            this.j = hv3.j;
            this.k = hv3.k;
            this.l = hv3.l;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        zu3 a(zu3 zu3);
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3) {
        return a(context, attributeSet, i2, i3, 0);
    }

    @DexIgnore
    public static b n() {
        return new b();
    }

    @DexIgnore
    public av3 b() {
        return this.d;
    }

    @DexIgnore
    public zu3 c() {
        return this.h;
    }

    @DexIgnore
    public av3 d() {
        return this.c;
    }

    @DexIgnore
    public zu3 e() {
        return this.g;
    }

    @DexIgnore
    public cv3 f() {
        return this.l;
    }

    @DexIgnore
    public cv3 g() {
        return this.j;
    }

    @DexIgnore
    public cv3 h() {
        return this.i;
    }

    @DexIgnore
    public av3 i() {
        return this.a;
    }

    @DexIgnore
    public zu3 j() {
        return this.e;
    }

    @DexIgnore
    public av3 k() {
        return this.b;
    }

    @DexIgnore
    public zu3 l() {
        return this.f;
    }

    @DexIgnore
    public b m() {
        return new b(this);
    }

    @DexIgnore
    public hv3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        return a(context, attributeSet, i2, i3, new xu3((float) i4));
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3, zu3 zu3) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, tr3.MaterialShape, i2, i3);
        int resourceId = obtainStyledAttributes.getResourceId(tr3.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(tr3.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return a(context, resourceId, resourceId2, zu3);
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3) {
        return a(context, i2, i3, 0);
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3, int i4) {
        return a(context, i2, i3, new xu3((float) i4));
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3, zu3 zu3) {
        if (i3 != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i2);
            i2 = i3;
            context = contextThemeWrapper;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, tr3.ShapeAppearance);
        try {
            int i4 = obtainStyledAttributes.getInt(tr3.ShapeAppearance_cornerFamily, 0);
            int i5 = obtainStyledAttributes.getInt(tr3.ShapeAppearance_cornerFamilyTopLeft, i4);
            int i6 = obtainStyledAttributes.getInt(tr3.ShapeAppearance_cornerFamilyTopRight, i4);
            int i7 = obtainStyledAttributes.getInt(tr3.ShapeAppearance_cornerFamilyBottomRight, i4);
            int i8 = obtainStyledAttributes.getInt(tr3.ShapeAppearance_cornerFamilyBottomLeft, i4);
            zu3 a2 = a(obtainStyledAttributes, tr3.ShapeAppearance_cornerSize, zu3);
            zu3 a3 = a(obtainStyledAttributes, tr3.ShapeAppearance_cornerSizeTopLeft, a2);
            zu3 a4 = a(obtainStyledAttributes, tr3.ShapeAppearance_cornerSizeTopRight, a2);
            zu3 a5 = a(obtainStyledAttributes, tr3.ShapeAppearance_cornerSizeBottomRight, a2);
            zu3 a6 = a(obtainStyledAttributes, tr3.ShapeAppearance_cornerSizeBottomLeft, a2);
            b bVar = new b();
            bVar.c(i5, a3);
            bVar.d(i6, a4);
            bVar.b(i7, a5);
            bVar.a(i8, a6);
            return bVar;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public hv3() {
        this.a = ev3.a();
        this.b = ev3.a();
        this.c = ev3.a();
        this.d = ev3.a();
        this.e = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = new xu3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = ev3.b();
        this.j = ev3.b();
        this.k = ev3.b();
        this.l = ev3.b();
    }

    @DexIgnore
    public static zu3 a(TypedArray typedArray, int i2, zu3 zu3) {
        TypedValue peekValue = typedArray.peekValue(i2);
        if (peekValue == null) {
            return zu3;
        }
        int i3 = peekValue.type;
        if (i3 == 5) {
            return new xu3((float) TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics()));
        }
        return i3 == 6 ? new fv3(peekValue.getFraction(1.0f, 1.0f)) : zu3;
    }

    @DexIgnore
    public cv3 a() {
        return this.k;
    }

    @DexIgnore
    public hv3 a(float f2) {
        b m2 = m();
        m2.a(f2);
        return m2.a();
    }

    @DexIgnore
    public hv3 a(c cVar) {
        b m2 = m();
        m2.c(cVar.a(j()));
        m2.d(cVar.a(l()));
        m2.a(cVar.a(c()));
        m2.b(cVar.a(e()));
        return m2.a();
    }

    @DexIgnore
    public boolean a(RectF rectF) {
        boolean z = this.l.getClass().equals(cv3.class) && this.j.getClass().equals(cv3.class) && this.i.getClass().equals(cv3.class) && this.k.getClass().equals(cv3.class);
        float a2 = this.e.a(rectF);
        boolean z2 = this.f.a(rectF) == a2 && this.h.a(rectF) == a2 && this.g.a(rectF) == a2;
        boolean z3 = (this.b instanceof gv3) && (this.a instanceof gv3) && (this.c instanceof gv3) && (this.d instanceof gv3);
        if (!z || !z2 || !z3) {
            return false;
        }
        return true;
    }
}
