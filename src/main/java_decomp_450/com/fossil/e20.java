package com.fossil;

import android.graphics.Bitmap;
import com.fossil.s10;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e20 implements cx<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ s10 a;
    @DexIgnore
    public /* final */ az b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements s10.b {
        @DexIgnore
        public /* final */ c20 a;
        @DexIgnore
        public /* final */ o50 b;

        @DexIgnore
        public a(c20 c20, o50 o50) {
            this.a = c20;
            this.b = o50;
        }

        @DexIgnore
        @Override // com.fossil.s10.b
        public void a() {
            this.a.a();
        }

        @DexIgnore
        @Override // com.fossil.s10.b
        public void a(dz dzVar, Bitmap bitmap) throws IOException {
            IOException a2 = this.b.a();
            if (a2 != null) {
                if (bitmap != null) {
                    dzVar.a(bitmap);
                }
                throw a2;
            }
        }
    }

    @DexIgnore
    public e20(s10 s10, az azVar) {
        this.a = s10;
        this.b = azVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, ax axVar) {
        return this.a.a(inputStream);
    }

    @DexIgnore
    public uy<Bitmap> a(InputStream inputStream, int i, int i2, ax axVar) throws IOException {
        c20 c20;
        boolean z;
        if (inputStream instanceof c20) {
            c20 = (c20) inputStream;
            z = false;
        } else {
            c20 = new c20(inputStream, this.b);
            z = true;
        }
        o50 b2 = o50.b(c20);
        try {
            return this.a.a(new s50(b2), i, i2, axVar, new a(c20, b2));
        } finally {
            b2.b();
            if (z) {
                c20.b();
            }
        }
    }
}
