package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pr<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <T> boolean a(pr<T> prVar, T t) {
            ee7.b(t, "data");
            return true;
        }
    }

    @DexIgnore
    Object a(rq rqVar, T t, rt rtVar, ir irVar, fb7<? super or> fb7);

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    String b(T t);
}
