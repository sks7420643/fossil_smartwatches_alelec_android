package com.fossil;

import android.database.Cursor;
import com.fossil.rx1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fx1 implements rx1.b {
    @DexIgnore
    public /* final */ Map a;

    @DexIgnore
    public fx1(Map map) {
        this.a = map;
    }

    @DexIgnore
    public static rx1.b a(Map map) {
        return new fx1(map);
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return rx1.a(this.a, (Cursor) obj);
    }
}
