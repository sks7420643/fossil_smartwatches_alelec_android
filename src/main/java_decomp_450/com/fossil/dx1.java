package com.fossil;

import android.database.Cursor;
import com.fossil.rx1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class dx1 implements rx1.b {
    @DexIgnore
    public /* final */ rx1 a;
    @DexIgnore
    public /* final */ List b;
    @DexIgnore
    public /* final */ pu1 c;

    @DexIgnore
    public dx1(rx1 rx1, List list, pu1 pu1) {
        this.a = rx1;
        this.b = list;
        this.c = pu1;
    }

    @DexIgnore
    public static rx1.b a(rx1 rx1, List list, pu1 pu1) {
        return new dx1(rx1, list, pu1);
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return rx1.a(this.a, this.b, this.c, (Cursor) obj);
    }
}
