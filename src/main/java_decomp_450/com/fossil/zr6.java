package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr6 implements Factory<xr6> {
    @DexIgnore
    public static xr6 a(ur6 ur6, wl5 wl5, DeviceRepository deviceRepository, ad5 ad5, ek5 ek5, kw6 kw6, ch5 ch5) {
        return new xr6(ur6, wl5, deviceRepository, ad5, ek5, kw6, ch5);
    }
}
