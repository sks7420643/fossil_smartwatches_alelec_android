package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cf implements af {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public cf(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cf)) {
            return false;
        }
        cf cfVar = (cf) obj;
        if (TextUtils.equals(this.a, cfVar.a) && this.b == cfVar.b && this.c == cfVar.c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return z8.a(this.a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
