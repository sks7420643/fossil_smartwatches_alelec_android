package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w76 implements Factory<z76> {
    @DexIgnore
    public static z76 a(u76 u76) {
        z76 b = u76.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
