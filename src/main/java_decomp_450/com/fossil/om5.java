package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ FirmwareData b;

        @DexIgnore
        public b(String str, FirmwareData firmwareData) {
            ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            ee7.b(firmwareData, "firmwareData");
            this.a = str;
            this.b = firmwareData;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final FirmwareData b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase", f = "UpdateDeviceToSpecificFirmwareUsecase.kt", l = {47}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ om5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(om5 om5, fb7 fb7) {
            super(fb7);
            this.this$0 = om5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = pm5.class.getSimpleName();
        ee7.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public om5(DeviceRepository deviceRepository, ch5 ch5) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.om5.b r9, com.fossil.fb7<java.lang.Object> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.om5.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.om5$e r0 = (com.fossil.om5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.om5$e r0 = new com.fossil.om5$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x004e
            if (r2 != r3) goto L_0x0046
            java.lang.Object r9 = r0.L$6
            com.misfit.frameworks.buttonservice.model.FirmwareData r9 = (com.misfit.frameworks.buttonservice.model.FirmwareData) r9
            java.lang.Object r1 = r0.L$5
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r0.L$4
            com.portfolio.platform.PortfolioApp r2 = (com.portfolio.platform.PortfolioApp) r2
            java.lang.Object r3 = r0.L$3
            com.misfit.frameworks.buttonservice.model.FirmwareData r3 = (com.misfit.frameworks.buttonservice.model.FirmwareData) r3
            java.lang.Object r3 = r0.L$2
            com.portfolio.platform.data.model.Device r3 = (com.portfolio.platform.data.model.Device) r3
            java.lang.Object r3 = r0.L$1
            com.fossil.om5$b r3 = (com.fossil.om5.b) r3
            java.lang.Object r0 = r0.L$0
            com.fossil.om5 r0 = (com.fossil.om5) r0
            com.fossil.t87.a(r10)     // Catch:{ Exception -> 0x014a }
            goto L_0x0138
        L_0x0046:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x004e:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.om5.f
            java.lang.String r4 = "running UseCase"
            r10.d(r2, r4)
            if (r9 != 0) goto L_0x0073
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.om5.f
            java.lang.String r0 = "Error when update firmware, requestValues is NULL"
            r9.e(r10, r0)
            com.fossil.om5$c r9 = new com.fossil.om5$c
            r9.<init>()
            return r9
        L_0x0073:
            com.portfolio.platform.data.source.DeviceRepository r10 = r8.d
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r2 = r2.c()
            com.portfolio.platform.data.model.Device r10 = r10.getDeviceBySerial(r2)
            if (r10 != 0) goto L_0x00ab
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r0 = com.fossil.om5.f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error when update firmware, no device exists in DB with serial="
            r1.append(r2)
            java.lang.String r9 = r9.a()
            r1.append(r9)
            java.lang.String r9 = r1.toString()
            r10.e(r0, r9)
            com.fossil.om5$c r9 = new com.fossil.om5$c
            r9.<init>()
            return r9
        L_0x00ab:
            com.misfit.frameworks.buttonservice.model.FirmwareData r2 = r9.b()
            java.lang.String r4 = r2.getDeviceModel()
            java.lang.String r5 = r10.getSku()
            if (r5 == 0) goto L_0x00ba
            goto L_0x00bc
        L_0x00ba:
            java.lang.String r5 = ""
        L_0x00bc:
            boolean r4 = com.portfolio.platform.data.MFDeviceModel.isSame(r4, r5)
            if (r4 != 0) goto L_0x00d5
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.om5.f
            java.lang.String r0 = "Firmware is not comparable with device model. Cannot OTA."
            r9.e(r10, r0)
            com.fossil.om5$c r9 = new com.fossil.om5$c
            r9.<init>()
            return r9
        L_0x00d5:
            com.fossil.ch5 r4 = r8.e
            java.lang.String r5 = r10.getDeviceId()
            java.lang.String r6 = r10.getFirmwareRevision()
            r4.a(r5, r6)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.om5.f
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Start update firmware with version="
            r6.append(r7)
            java.lang.String r7 = r2.getFirmwareVersion()
            r6.append(r7)
            java.lang.String r7 = ", currentVersion="
            r6.append(r7)
            java.lang.String r7 = r10.getFirmwareRevision()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.d(r5, r6)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            java.lang.String r5 = r9.a()
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r2
            r0.L$4 = r4
            r0.L$5 = r5
            r0.L$6 = r2
            r0.label = r3
            java.lang.Object r10 = r6.d(r0)
            if (r10 != r1) goto L_0x0135
            return r1
        L_0x0135:
            r9 = r2
            r2 = r4
            r1 = r5
        L_0x0138:
            if (r10 == 0) goto L_0x0145
            com.misfit.frameworks.buttonservice.model.UserProfile r10 = (com.misfit.frameworks.buttonservice.model.UserProfile) r10
            r2.a(r1, r9, r10)
            com.fossil.om5$d r9 = new com.fossil.om5$d
            r9.<init>()
            return r9
        L_0x0145:
            com.fossil.ee7.a()
            r9 = 0
            throw r9
        L_0x014a:
            r9 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r0 = com.fossil.om5.f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "running UseCase failed with exception="
            r1.append(r2)
            r1.append(r9)
            java.lang.String r1 = r1.toString()
            r10.d(r0, r1)
            r9.printStackTrace()
            com.fossil.om5$c r9 = new com.fossil.om5$c
            r9.<init>()
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.om5.a(com.fossil.om5$b, com.fossil.fb7):java.lang.Object");
    }
}
