package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or4 implements Factory<mr4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;
    @DexIgnore
    public /* final */ Provider<xo4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public or4(Provider<ro4> provider, Provider<xo4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static or4 a(Provider<ro4> provider, Provider<xo4> provider2, Provider<ch5> provider3) {
        return new or4(provider, provider2, provider3);
    }

    @DexIgnore
    public static mr4 a(ro4 ro4, xo4 xo4, ch5 ch5) {
        return new mr4(ro4, xo4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public mr4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
