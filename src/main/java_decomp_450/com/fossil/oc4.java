package com.fossil;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import com.fossil.o6;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.concurrent.atomic.AtomicInteger;
import net.sqlcipher.database.SQLiteDatabase;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc4 {
    @DexIgnore
    public static /* final */ AtomicInteger a; // = new AtomicInteger((int) SystemClock.elapsedRealtime());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ o6.e a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public a(o6.e eVar, String str, int i) {
            this.a = eVar;
            this.b = str;
            this.c = i;
        }
    }

    @DexIgnore
    public static a a(Context context, String str, dd4 dd4, String str2, Resources resources, PackageManager packageManager, Bundle bundle) {
        o6.e eVar = new o6.e(context, str2);
        String b = dd4.b(resources, str, "gcm.n.title");
        if (!TextUtils.isEmpty(b)) {
            eVar.b((CharSequence) b);
        }
        String b2 = dd4.b(resources, str, "gcm.n.body");
        if (!TextUtils.isEmpty(b2)) {
            eVar.a((CharSequence) b2);
            o6.c cVar = new o6.c();
            cVar.a(b2);
            eVar.a(cVar);
        }
        eVar.f(a(packageManager, resources, str, dd4.g("gcm.n.icon"), bundle));
        Uri a2 = a(str, dd4, resources);
        if (a2 != null) {
            eVar.a(a2);
        }
        eVar.a(a(context, dd4, str, packageManager));
        PendingIntent a3 = a(context, dd4);
        if (a3 != null) {
            eVar.b(a3);
        }
        Integer a4 = a(context, dd4.g("gcm.n.color"), bundle);
        if (a4 != null) {
            eVar.b(a4.intValue());
        }
        eVar.a(!dd4.a("gcm.n.sticky"));
        eVar.b(dd4.a("gcm.n.local_only"));
        String g = dd4.g("gcm.n.ticker");
        if (g != null) {
            eVar.c(g);
        }
        Integer e = dd4.e();
        if (e != null) {
            eVar.e(e.intValue());
        }
        Integer h = dd4.h();
        if (h != null) {
            eVar.g(h.intValue());
        }
        Integer d = dd4.d();
        if (d != null) {
            eVar.d(d.intValue());
        }
        Long f = dd4.f("gcm.n.event_time");
        if (f != null) {
            eVar.d(true);
            eVar.a(f.longValue());
        }
        long[] g2 = dd4.g();
        if (g2 != null) {
            eVar.a(g2);
        }
        int[] a5 = dd4.a();
        if (a5 != null) {
            eVar.a(a5[0], a5[1], a5[2]);
        }
        eVar.c(a(dd4));
        return new a(eVar, b(dd4), 0);
    }

    @DexIgnore
    public static a b(Context context, dd4 dd4) {
        Bundle a2 = a(context.getPackageManager(), context.getPackageName());
        return a(context, context.getPackageName(), dd4, b(context, dd4.c(), a2), context.getResources(), context.getPackageManager(), a2);
    }

    @DexIgnore
    public static boolean c(dd4 dd4) {
        return dd4.a("google.c.a.e");
    }

    @DexIgnore
    @TargetApi(26)
    public static String b(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 26) {
            return null;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion < 26) {
                return null;
            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NotificationManager.class);
            if (!TextUtils.isEmpty(str)) {
                if (notificationManager.getNotificationChannel(str) != null) {
                    return str;
                }
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 122);
                sb.append("Notification Channel requested (");
                sb.append(str);
                sb.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                Log.w("FirebaseMessaging", sb.toString());
            }
            String string = bundle.getString("com.google.firebase.messaging.default_notification_channel_id");
            if (TextUtils.isEmpty(string)) {
                Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
            } else if (notificationManager.getNotificationChannel(string) != null) {
                return string;
            } else {
                Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
            }
            if (notificationManager.getNotificationChannel("fcm_fallback_notification_channel") == null) {
                notificationManager.createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", context.getString(context.getResources().getIdentifier("fcm_fallback_notification_channel_label", LegacyTokenHelper.TYPE_STRING, context.getPackageName())), 3));
            }
            return "fcm_fallback_notification_channel";
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static String b(dd4 dd4) {
        String g = dd4.g("gcm.n.tag");
        if (!TextUtils.isEmpty(g)) {
            return g;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        StringBuilder sb = new StringBuilder(37);
        sb.append("FCM-Notification:");
        sb.append(uptimeMillis);
        return sb.toString();
    }

    @DexIgnore
    public static int a(dd4 dd4) {
        int i = dd4.a("gcm.n.default_sound") ? 1 : 0;
        if (dd4.a("gcm.n.default_vibrate_timings")) {
            i |= 2;
        }
        return dd4.a("gcm.n.default_light_settings") ? i | 4 : i;
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean a(Resources resources, int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (!(resources.getDrawable(i, null) instanceof AdaptiveIconDrawable)) {
                return true;
            }
            StringBuilder sb = new StringBuilder(77);
            sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
            sb.append(i);
            Log.e("FirebaseMessaging", sb.toString());
            return false;
        } catch (Resources.NotFoundException unused) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Couldn't find resource ");
            sb2.append(i);
            sb2.append(", treating it as an invalid icon");
            Log.e("FirebaseMessaging", sb2.toString());
            return false;
        }
    }

    @DexIgnore
    public static int a(PackageManager packageManager, Resources resources, String str, String str2, Bundle bundle) {
        if (!TextUtils.isEmpty(str2)) {
            int identifier = resources.getIdentifier(str2, ResourceManager.DRAWABLE, str);
            if (identifier != 0 && a(resources, identifier)) {
                return identifier;
            }
            int identifier2 = resources.getIdentifier(str2, "mipmap", str);
            if (identifier2 != 0 && a(resources, identifier2)) {
                return identifier2;
            }
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 61);
            sb.append("Icon resource ");
            sb.append(str2);
            sb.append(" not found. Notification will use default icon.");
            Log.w("FirebaseMessaging", sb.toString());
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_icon", 0);
        if (i == 0 || !a(resources, i)) {
            try {
                i = packageManager.getApplicationInfo(str, 0).icon;
            } catch (PackageManager.NameNotFoundException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 35);
                sb2.append("Couldn't get own application info: ");
                sb2.append(valueOf);
                Log.w("FirebaseMessaging", sb2.toString());
            }
        }
        if (i == 0 || !a(resources, i)) {
            return 17301651;
        }
        return i;
    }

    @DexIgnore
    public static Integer a(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException unused) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 56);
                sb.append("Color is invalid: ");
                sb.append(str);
                sb.append(". Notification will use default color.");
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i != 0) {
            try {
                return Integer.valueOf(v6.a(context, i));
            } catch (Resources.NotFoundException unused2) {
                Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            }
        }
        return null;
    }

    @DexIgnore
    public static Uri a(String str, dd4 dd4, Resources resources) {
        String f = dd4.f();
        if (TextUtils.isEmpty(f)) {
            return null;
        }
        if (Endpoints.DEFAULT_NAME.equals(f) || resources.getIdentifier(f, OrmLiteConfigUtil.RAW_DIR_NAME, str) == 0) {
            return RingtoneManager.getDefaultUri(2);
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 24 + String.valueOf(f).length());
        sb.append("android.resource://");
        sb.append(str);
        sb.append("/raw/");
        sb.append(f);
        return Uri.parse(sb.toString());
    }

    @DexIgnore
    public static PendingIntent a(Context context, dd4 dd4, String str, PackageManager packageManager) {
        Intent a2 = a(str, dd4, packageManager);
        if (a2 == null) {
            return null;
        }
        a2.addFlags(67108864);
        a2.putExtras(dd4.j());
        PendingIntent activity = PendingIntent.getActivity(context, a(), a2, 1073741824);
        return c(dd4) ? a(context, dd4, activity) : activity;
    }

    @DexIgnore
    public static Intent a(String str, dd4 dd4, PackageManager packageManager) {
        String g = dd4.g("gcm.n.click_action");
        if (!TextUtils.isEmpty(g)) {
            Intent intent = new Intent(g);
            intent.setPackage(str);
            intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
            return intent;
        }
        Uri b = dd4.b();
        if (b != null) {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setPackage(str);
            intent2.setData(b);
            return intent2;
        }
        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            Log.w("FirebaseMessaging", "No activity found to launch app");
        }
        return launchIntentForPackage;
    }

    @DexIgnore
    public static Bundle a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                return applicationInfo.metaData;
            }
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 35);
            sb.append("Couldn't get own application info: ");
            sb.append(valueOf);
            Log.w("FirebaseMessaging", sb.toString());
        }
        return Bundle.EMPTY;
    }

    @DexIgnore
    public static int a() {
        return a.incrementAndGet();
    }

    @DexIgnore
    public static PendingIntent a(Context context, dd4 dd4, PendingIntent pendingIntent) {
        return a(context, new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN").putExtras(dd4.i()).putExtra("pending_intent", pendingIntent));
    }

    @DexIgnore
    public static PendingIntent a(Context context, dd4 dd4) {
        if (!c(dd4)) {
            return null;
        }
        return a(context, new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(dd4.i()));
    }

    @DexIgnore
    public static PendingIntent a(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, a(), new Intent("com.google.firebase.MESSAGING_EVENT").setComponent(new ComponentName(context, "com.google.firebase.iid.FirebaseInstanceIdReceiver")).putExtra("wrapped_intent", intent), 1073741824);
    }
}
