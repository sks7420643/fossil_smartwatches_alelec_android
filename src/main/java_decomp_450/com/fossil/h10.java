package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h10 implements dx<BitmapDrawable> {
    @DexIgnore
    public /* final */ dz a;
    @DexIgnore
    public /* final */ dx<Bitmap> b;

    @DexIgnore
    public h10(dz dzVar, dx<Bitmap> dxVar) {
        this.a = dzVar;
        this.b = dxVar;
    }

    @DexIgnore
    public boolean a(uy<BitmapDrawable> uyVar, File file, ax axVar) {
        return this.b.a(new k10(uyVar.get().getBitmap(), this.a), file, axVar);
    }

    @DexIgnore
    @Override // com.fossil.dx
    public uw a(ax axVar) {
        return this.b.a(axVar);
    }
}
