package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad2 implements Parcelable.Creator<DataType> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataType createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        ArrayList arrayList = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str = j72.e(parcel, a);
            } else if (a2 == 2) {
                arrayList = j72.c(parcel, a, ic2.CREATOR);
            } else if (a2 == 3) {
                str2 = j72.e(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                str3 = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new DataType(str, arrayList, str2, str3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataType[] newArray(int i) {
        return new DataType[i];
    }
}
