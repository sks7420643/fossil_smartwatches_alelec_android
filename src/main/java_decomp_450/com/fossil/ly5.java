package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ly5 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(jy5 jy5) {
        LoaderManager c = jy5.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
