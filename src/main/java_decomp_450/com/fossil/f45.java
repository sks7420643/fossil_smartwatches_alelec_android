package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f45 extends e45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G;
    @DexIgnore
    public long E;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        G = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        G.put(2131362517, 2);
        G.put(2131362040, 3);
        G.put(2131361815, 4);
        G.put(2131362793, 5);
        G.put(2131362893, 6);
        G.put(2131362301, 7);
        G.put(2131362755, 8);
        G.put(2131362107, 9);
        G.put(2131361816, 10);
        G.put(2131362792, 11);
        G.put(2131362892, 12);
        G.put(2131362302, 13);
    }
    */

    @DexIgnore
    public f45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 14, F, G));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.E = 1;
        }
        g();
    }

    @DexIgnore
    public f45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleCheckBox) objArr[4], (FlexibleCheckBox) objArr[10], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (View) objArr[8], (ConstraintLayout) objArr[11], (ConstraintLayout) objArr[5], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[6], (ConstraintLayout) objArr[0]);
        this.E = -1;
        ((e45) this).D.setTag(null);
        a(view);
        f();
    }
}
