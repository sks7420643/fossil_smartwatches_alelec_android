package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f02 {
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_button; // = 2131887321;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_text; // = 2131887322;
    @DexIgnore
    public static /* final */ int common_google_play_services_enable_title; // = 2131887323;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_button; // = 2131887324;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_text; // = 2131887325;
    @DexIgnore
    public static /* final */ int common_google_play_services_install_title; // = 2131887326;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_channel_name; // = 2131887327;
    @DexIgnore
    public static /* final */ int common_google_play_services_notification_ticker; // = 2131887328;
    @DexIgnore
    public static /* final */ int common_google_play_services_unsupported_text; // = 2131887330;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_button; // = 2131887331;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_text; // = 2131887332;
    @DexIgnore
    public static /* final */ int common_google_play_services_update_title; // = 2131887333;
    @DexIgnore
    public static /* final */ int common_google_play_services_updating_text; // = 2131887334;
    @DexIgnore
    public static /* final */ int common_google_play_services_wear_update_text; // = 2131887335;
    @DexIgnore
    public static /* final */ int common_open_on_phone; // = 2131887336;
    @DexIgnore
    public static /* final */ int common_signin_button_text; // = 2131887337;
    @DexIgnore
    public static /* final */ int common_signin_button_text_long; // = 2131887338;
}
