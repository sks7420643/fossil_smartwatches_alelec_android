package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kc6 implements MembersInjector<HeartRateOverviewFragment> {
    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, hc6 hc6) {
        heartRateOverviewFragment.g = hc6;
    }

    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, yc6 yc6) {
        heartRateOverviewFragment.h = yc6;
    }

    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, sc6 sc6) {
        heartRateOverviewFragment.i = sc6;
    }
}
