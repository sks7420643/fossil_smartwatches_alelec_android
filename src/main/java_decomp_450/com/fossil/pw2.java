package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw2<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, nw2> a;

    @DexIgnore
    public pw2(Map.Entry<K, nw2> entry) {
        this.a = entry;
    }

    @DexIgnore
    public final nw2 a() {
        return this.a.getValue();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.a.getKey();
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.a.getValue() == null) {
            return null;
        }
        nw2.c();
        throw null;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof jx2) {
            return this.a.getValue().a((jx2) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
