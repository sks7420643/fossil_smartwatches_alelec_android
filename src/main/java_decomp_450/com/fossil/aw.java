package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.a00;
import com.fossil.a10;
import com.fossil.b10;
import com.fossil.c00;
import com.fossil.c10;
import com.fossil.d00;
import com.fossil.e00;
import com.fossil.i20;
import com.fossil.j00;
import com.fossil.jx;
import com.fossil.px;
import com.fossil.r00;
import com.fossil.rx;
import com.fossil.t00;
import com.fossil.u00;
import com.fossil.v00;
import com.fossil.w00;
import com.fossil.x00;
import com.fossil.y00;
import com.fossil.z00;
import com.fossil.zz;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aw implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile aw i;
    @DexIgnore
    public static volatile boolean j;
    @DexIgnore
    public /* final */ dz a;
    @DexIgnore
    public /* final */ uz b;
    @DexIgnore
    public /* final */ cw c;
    @DexIgnore
    public /* final */ gw d;
    @DexIgnore
    public /* final */ az e;
    @DexIgnore
    public /* final */ t30 f;
    @DexIgnore
    public /* final */ l30 g;
    @DexIgnore
    public /* final */ List<iw> h; // = new ArrayList();

    @DexIgnore
    public interface a {
        @DexIgnore
        r40 build();
    }

    @DexIgnore
    public aw(Context context, jy jyVar, uz uzVar, dz dzVar, az azVar, t30 t30, l30 l30, int i2, a aVar, Map<Class<?>, jw<?, ?>> map, List<q40<Object>> list, boolean z, boolean z2) {
        cx cxVar;
        cx cxVar2;
        Object obj;
        dw dwVar = dw.NORMAL;
        this.a = dzVar;
        this.e = azVar;
        this.b = uzVar;
        this.f = t30;
        this.g = l30;
        Resources resources = context.getResources();
        gw gwVar = new gw();
        this.d = gwVar;
        gwVar.a((ImageHeaderParser) new q10());
        if (Build.VERSION.SDK_INT >= 27) {
            this.d.a((ImageHeaderParser) new v10());
        }
        List<ImageHeaderParser> a2 = this.d.a();
        r20 r20 = new r20(context, a2, dzVar, azVar);
        cx<ParcelFileDescriptor, Bitmap> c2 = h20.c(dzVar);
        s10 s10 = new s10(this.d.a(), resources.getDisplayMetrics(), dzVar, azVar);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            cxVar = new m10(s10);
            cxVar2 = new e20(s10, azVar);
        } else {
            cxVar2 = new z10();
            cxVar = new n10();
        }
        n20 n20 = new n20(context);
        r00.c cVar = new r00.c(resources);
        r00.d dVar = new r00.d(resources);
        r00.b bVar = new r00.b(resources);
        r00.a aVar2 = new r00.a(resources);
        i10 i10 = new i10(azVar);
        b30 b30 = new b30();
        e30 e30 = new e30();
        ContentResolver contentResolver = context.getContentResolver();
        gw gwVar2 = this.d;
        gwVar2.a(ByteBuffer.class, new b00());
        gwVar2.a(InputStream.class, new s00(azVar));
        gwVar2.a("Bitmap", ByteBuffer.class, Bitmap.class, cxVar);
        gwVar2.a("Bitmap", InputStream.class, Bitmap.class, cxVar2);
        if (rx.c()) {
            obj = nw.class;
            this.d.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new b20(s10));
        } else {
            obj = nw.class;
        }
        gw gwVar3 = this.d;
        gwVar3.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, c2);
        gwVar3.a("Bitmap", AssetFileDescriptor.class, Bitmap.class, h20.a(dzVar));
        gwVar3.a(Bitmap.class, Bitmap.class, u00.a.a());
        gwVar3.a("Bitmap", Bitmap.class, Bitmap.class, new g20());
        gwVar3.a(Bitmap.class, (dx) i10);
        gwVar3.a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new g10(resources, cxVar));
        gwVar3.a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new g10(resources, cxVar2));
        gwVar3.a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new g10(resources, c2));
        gwVar3.a(BitmapDrawable.class, (dx) new h10(dzVar, i10));
        gwVar3.a("Gif", InputStream.class, t20.class, new a30(a2, r20, azVar));
        gwVar3.a("Gif", ByteBuffer.class, t20.class, r20);
        gwVar3.a(t20.class, (dx) new u20());
        gwVar3.a((Class) obj, (Class) obj, (n00) u00.a.a());
        gwVar3.a("Bitmap", obj, Bitmap.class, new y20(dzVar));
        gwVar3.a(Uri.class, Drawable.class, n20);
        gwVar3.a(Uri.class, Bitmap.class, new d20(n20, dzVar));
        gwVar3.a((jx.a<?>) new i20.a());
        gwVar3.a(File.class, ByteBuffer.class, new c00.b());
        gwVar3.a(File.class, InputStream.class, new e00.e());
        gwVar3.a(File.class, File.class, new p20());
        gwVar3.a(File.class, ParcelFileDescriptor.class, new e00.b());
        gwVar3.a(File.class, File.class, u00.a.a());
        gwVar3.a((jx.a<?>) new px.a(azVar));
        if (rx.c()) {
            this.d.a((jx.a<?>) new rx.a());
        }
        gw gwVar4 = this.d;
        gwVar4.a(Integer.TYPE, InputStream.class, cVar);
        gwVar4.a(Integer.TYPE, ParcelFileDescriptor.class, bVar);
        gwVar4.a(Integer.class, InputStream.class, cVar);
        gwVar4.a(Integer.class, ParcelFileDescriptor.class, bVar);
        gwVar4.a(Integer.class, Uri.class, dVar);
        gwVar4.a(Integer.TYPE, AssetFileDescriptor.class, aVar2);
        gwVar4.a(Integer.class, AssetFileDescriptor.class, aVar2);
        gwVar4.a(Integer.TYPE, Uri.class, dVar);
        gwVar4.a(String.class, InputStream.class, new d00.c());
        gwVar4.a(Uri.class, InputStream.class, new d00.c());
        gwVar4.a(String.class, InputStream.class, new t00.c());
        gwVar4.a(String.class, ParcelFileDescriptor.class, new t00.b());
        gwVar4.a(String.class, AssetFileDescriptor.class, new t00.a());
        gwVar4.a(Uri.class, InputStream.class, new y00.a());
        gwVar4.a(Uri.class, InputStream.class, new zz.c(context.getAssets()));
        gwVar4.a(Uri.class, ParcelFileDescriptor.class, new zz.b(context.getAssets()));
        gwVar4.a(Uri.class, InputStream.class, new z00.a(context));
        gwVar4.a(Uri.class, InputStream.class, new a10.a(context));
        if (Build.VERSION.SDK_INT >= 29) {
            this.d.a(Uri.class, InputStream.class, new b10.c(context));
            this.d.a(Uri.class, ParcelFileDescriptor.class, new b10.b(context));
        }
        gw gwVar5 = this.d;
        gwVar5.a(Uri.class, InputStream.class, new v00.d(contentResolver));
        gwVar5.a(Uri.class, ParcelFileDescriptor.class, new v00.b(contentResolver));
        gwVar5.a(Uri.class, AssetFileDescriptor.class, new v00.a(contentResolver));
        gwVar5.a(Uri.class, InputStream.class, new w00.a());
        gwVar5.a(URL.class, InputStream.class, new c10.a());
        gwVar5.a(Uri.class, File.class, new j00.a(context));
        gwVar5.a(f00.class, InputStream.class, new x00.a());
        gwVar5.a(byte[].class, ByteBuffer.class, new a00.a());
        gwVar5.a(byte[].class, InputStream.class, new a00.d());
        gwVar5.a(Uri.class, Uri.class, u00.a.a());
        gwVar5.a(Drawable.class, Drawable.class, u00.a.a());
        gwVar5.a(Drawable.class, Drawable.class, new o20());
        gwVar5.a(Bitmap.class, BitmapDrawable.class, new c30(resources));
        gwVar5.a(Bitmap.class, byte[].class, b30);
        gwVar5.a(Drawable.class, byte[].class, new d30(dzVar, b30, e30));
        gwVar5.a(t20.class, byte[].class, e30);
        if (Build.VERSION.SDK_INT >= 23) {
            cx<ByteBuffer, Bitmap> b2 = h20.b(dzVar);
            this.d.a(ByteBuffer.class, Bitmap.class, b2);
            this.d.a(ByteBuffer.class, BitmapDrawable.class, new g10(resources, b2));
        }
        this.c = new cw(context, azVar, this.d, new a50(), aVar, map, list, jyVar, z, i2);
    }

    @DexIgnore
    public static aw a(Context context) {
        if (i == null) {
            GeneratedAppGlideModule b2 = b(context.getApplicationContext());
            synchronized (aw.class) {
                if (i == null) {
                    a(context, b2);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public static void b(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        a(context, new bw(), generatedAppGlideModule);
    }

    @DexIgnore
    public dz c() {
        return this.a;
    }

    @DexIgnore
    public l30 d() {
        return this.g;
    }

    @DexIgnore
    public Context e() {
        return this.c.getBaseContext();
    }

    @DexIgnore
    public cw f() {
        return this.c;
    }

    @DexIgnore
    public gw g() {
        return this.d;
    }

    @DexIgnore
    public t30 h() {
        return this.f;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        a();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        a(i2);
    }

    @DexIgnore
    public static GeneratedAppGlideModule b(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(Context.class).newInstance(context.getApplicationContext());
        } catch (ClassNotFoundException unused) {
            if (!Log.isLoggable("Glide", 5)) {
                return null;
            }
            Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            return null;
        } catch (InstantiationException e2) {
            a(e2);
            throw null;
        } catch (IllegalAccessException e3) {
            a(e3);
            throw null;
        } catch (NoSuchMethodException e4) {
            a(e4);
            throw null;
        } catch (InvocationTargetException e5) {
            a(e5);
            throw null;
        }
    }

    @DexIgnore
    public static t30 c(Context context) {
        u50.a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).h();
    }

    @DexIgnore
    public static iw d(Context context) {
        return c(context).a(context);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!j) {
            j = true;
            b(context, generatedAppGlideModule);
            j = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public az b() {
        return this.e;
    }

    @DexIgnore
    public void b(iw iwVar) {
        synchronized (this.h) {
            if (this.h.contains(iwVar)) {
                this.h.remove(iwVar);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    @DexIgnore
    public static void a(Context context, bw bwVar, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<a40> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.a()) {
            emptyList = new c40(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.b().isEmpty()) {
            Set<Class<?>> b2 = generatedAppGlideModule.b();
            Iterator<a40> it = emptyList.iterator();
            while (it.hasNext()) {
                a40 next = it.next();
                if (b2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            Iterator<a40> it2 = emptyList.iterator();
            while (it2.hasNext()) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + it2.next().getClass());
            }
        }
        bwVar.a(generatedAppGlideModule != null ? generatedAppGlideModule.c() : null);
        for (a40 a40 : emptyList) {
            a40.a(applicationContext, bwVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, bwVar);
        }
        aw a2 = bwVar.a(applicationContext);
        for (a40 a402 : emptyList) {
            try {
                a402.a(applicationContext, a2, a2.d);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + a402.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, a2, a2.d);
        }
        applicationContext.registerComponentCallbacks(a2);
        i = a2;
    }

    @DexIgnore
    public static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public void a() {
        v50.b();
        this.b.a();
        this.a.a();
        this.e.a();
    }

    @DexIgnore
    public void a(int i2) {
        v50.b();
        for (iw iwVar : this.h) {
            iwVar.onTrimMemory(i2);
        }
        this.b.a(i2);
        this.a.a(i2);
        this.e.a(i2);
    }

    @DexIgnore
    public static iw a(Fragment fragment) {
        return c(fragment.getContext()).a(fragment);
    }

    @DexIgnore
    public static iw a(View view) {
        return c(view.getContext()).a(view);
    }

    @DexIgnore
    public boolean a(c50<?> c50) {
        synchronized (this.h) {
            for (iw iwVar : this.h) {
                if (iwVar.b(c50)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void a(iw iwVar) {
        synchronized (this.h) {
            if (!this.h.contains(iwVar)) {
                this.h.add(iwVar);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
