package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<ao4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ int b; // = v6.a(PortfolioApp.g0.c(), 2131099689);
    @DexIgnore
    public /* final */ int c; // = v6.a(PortfolioApp.g0.c(), 2131099677);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ oy6.b a;
        @DexIgnore
        public /* final */ st4 b; // = st4.d.a();
        @DexIgnore
        public /* final */ m85 c;
        @DexIgnore
        public /* final */ /* synthetic */ br4 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(br4 br4, m85 m85) {
            super(m85.d());
            ee7.b(m85, "binding");
            this.d = br4;
            this.c = m85;
            oy6.b b2 = oy6.a().b();
            ee7.a((Object) b2, "TextDrawable.builder().round()");
            this.a = b2;
        }

        @DexIgnore
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0100  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0104  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x010a  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0123  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.fossil.ao4 r25) {
            /*
                r24 = this;
                r0 = r24
                java.lang.String r1 = "item"
                r2 = r25
                com.fossil.ee7.b(r2, r1)
                com.fossil.m85 r1 = r0.c
                java.lang.String r3 = r25.i()
                int r4 = r3.hashCode()
                java.lang.String r5 = "activity_reach_goal"
                java.lang.String r6 = "activity_best_result"
                r8 = -1348781656(0xffffffffaf9b39a8, float:-2.8235303E-10)
                java.lang.String r9 = "Challenge"
                java.lang.String r10 = "arg"
                java.lang.String r11 = "abr"
                java.lang.String r12 = "-"
                java.lang.String r13 = "binding.root"
                java.lang.String r14 = "java.lang.String.format(format, *args)"
                java.lang.String r15 = "ivAvatar"
                r16 = 0
                r7 = 1
                r17 = 0
                switch(r4) {
                    case -1559094286: goto L_0x0473;
                    case -764698119: goto L_0x03c2;
                    case -473527954: goto L_0x0339;
                    case 238453777: goto L_0x02ec;
                    case 634854495: goto L_0x0263;
                    case 1433363166: goto L_0x01da;
                    case 1669546642: goto L_0x0132;
                    case 1898363949: goto L_0x0032;
                    default: goto L_0x0030;
                }
            L_0x0030:
                goto L_0x051b
            L_0x0032:
                java.lang.String r4 = "Title_Send_Invitation_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.fu4 r3 = com.fossil.fu4.a
                com.fossil.io4 r4 = r25.f()
                if (r4 == 0) goto L_0x0047
                java.lang.String r4 = r4.b()
                goto L_0x0049
            L_0x0047:
                r4 = r17
            L_0x0049:
                com.fossil.io4 r5 = r25.f()
                if (r5 == 0) goto L_0x0054
                java.lang.String r5 = r5.d()
                goto L_0x0056
            L_0x0054:
                r5 = r17
            L_0x0056:
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x0063
                java.lang.String r6 = r6.e()
                if (r6 == 0) goto L_0x0063
                goto L_0x0064
            L_0x0063:
                r6 = r12
            L_0x0064:
                java.lang.String r3 = r3.a(r4, r5, r6)
                java.lang.String r4 = r25.a()
                com.fossil.pn4 r5 = r25.b()
                if (r5 == 0) goto L_0x0079
                java.lang.String r5 = r5.b()
                if (r5 == 0) goto L_0x0079
                goto L_0x007a
            L_0x0079:
                r5 = r12
            L_0x007a:
                int r6 = r4.hashCode()
                r8 = -1500498103(0xffffffffa6903749, float:-1.0006992E-15)
                r9 = 2
                if (r6 == r8) goto L_0x00c8
                r8 = -1500497618(0xffffffffa690392e, float:-1.0007506E-15)
                if (r6 == r8) goto L_0x008a
                goto L_0x00f6
            L_0x008a:
                java.lang.String r6 = "Body_Send_Invitation_Challenge_ARG"
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x00f6
                com.fossil.we7 r4 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r6 = 2131886197(0x7f120075, float:1.9406966E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r6)
                java.lang.String r6 = "LanguageHelper.getString\u2026Invitation_Challenge_ARG)"
                com.fossil.ee7.a(r4, r6)
                r6 = 3
                java.lang.Object[] r8 = new java.lang.Object[r6]
                r8[r16] = r3
                r8[r7] = r5
                com.fossil.pn4 r5 = r25.b()
                if (r5 == 0) goto L_0x00b8
                java.lang.Integer r5 = r5.c()
                goto L_0x00ba
            L_0x00b8:
                r5 = r17
            L_0x00ba:
                r8[r9] = r5
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r8, r6)
                java.lang.String r4 = java.lang.String.format(r4, r5)
                com.fossil.ee7.a(r4, r14)
                goto L_0x00f8
            L_0x00c8:
                java.lang.String r6 = "Body_Send_Invitation_Challenge_ABR"
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x00f6
                com.fossil.we7 r4 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r6 = 2131886196(0x7f120074, float:1.9406964E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r6)
                java.lang.String r6 = "LanguageHelper.getString\u2026Invitation_Challenge_ABR)"
                com.fossil.ee7.a(r4, r6)
                java.lang.Object[] r6 = new java.lang.Object[r9]
                r6[r16] = r3
                r6[r7] = r5
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r6, r9)
                java.lang.String r4 = java.lang.String.format(r4, r5)
                com.fossil.ee7.a(r4, r14)
                goto L_0x00f8
            L_0x00f6:
                java.lang.String r4 = ""
            L_0x00f8:
                r20 = r4
                int r4 = r20.length()
                if (r4 != 0) goto L_0x0102
                r16 = 1
            L_0x0102:
                if (r16 == 0) goto L_0x010a
                android.text.SpannableString r4 = new android.text.SpannableString
                r4.<init>(r12)
                goto L_0x0118
            L_0x010a:
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r19 = r3
                android.text.Spannable r4 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
            L_0x0118:
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x0127
                java.lang.String r17 = r6.a()
            L_0x0127:
                r6 = r17
                com.fossil.oy6$b r7 = r0.a
                com.fossil.st4 r8 = r0.b
                com.fossil.rt4.a(r5, r6, r3, r7, r8)
                goto L_0x051d
            L_0x0132:
                java.lang.String r4 = "Title_Notification_Remind_End_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.pn4 r3 = r25.b()
                if (r3 == 0) goto L_0x0149
                java.lang.String r3 = r3.b()
                if (r3 == 0) goto L_0x0149
                r19 = r3
                goto L_0x014b
            L_0x0149:
                r19 = r9
            L_0x014b:
                com.fossil.we7 r3 = com.fossil.we7.a
                com.fossil.m85 r3 = r0.c
                android.view.View r3 = r3.d()
                com.fossil.ee7.a(r3, r13)
                android.content.Context r3 = r3.getContext()
                r4 = 2131886192(0x7f120070, float:1.9406956E38)
                java.lang.String r3 = com.fossil.ig5.a(r3, r4)
                java.lang.String r4 = "LanguageHelper.getString\u2026ion_Remind_End_Challenge)"
                com.fossil.ee7.a(r3, r4)
                java.lang.Object[] r4 = new java.lang.Object[r7]
                r4[r16] = r19
                java.lang.Object[] r4 = java.util.Arrays.copyOf(r4, r7)
                java.lang.String r3 = java.lang.String.format(r3, r4)
                com.fossil.ee7.a(r3, r14)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r20 = r3
                android.text.Spannable r3 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                com.fossil.pn4 r4 = r25.b()
                if (r4 == 0) goto L_0x018d
                java.lang.String r17 = r4.d()
            L_0x018d:
                r4 = r17
                if (r4 != 0) goto L_0x0192
                goto L_0x01b8
            L_0x0192:
                int r7 = r4.hashCode()
                if (r7 == r8) goto L_0x01ab
                r8 = -637042289(0xffffffffda07818f, float:-9.5353933E15)
                if (r7 == r8) goto L_0x019e
                goto L_0x01b8
            L_0x019e:
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x01b8
                com.fossil.br4 r4 = r0.d
                int r4 = r4.b
                goto L_0x01bf
            L_0x01ab:
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x01b8
                com.fossil.br4 r4 = r0.d
                int r4 = r4.c
                goto L_0x01be
            L_0x01b8:
                com.fossil.br4 r4 = r0.d
                int r4 = r4.c
            L_0x01be:
                r10 = r11
            L_0x01bf:
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.pq.a(r5)
                android.widget.ImageView r5 = r1.r
                com.fossil.oy6$d r6 = com.fossil.oy6.a()
                com.fossil.oy6$b r6 = r6.b()
                com.fossil.oy6 r4 = r6.b(r10, r4)
                r5.setImageDrawable(r4)
                goto L_0x0519
            L_0x01da:
                java.lang.String r4 = "Title_Send_Friend_Request"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.fu4 r3 = com.fossil.fu4.a
                com.fossil.io4 r4 = r25.f()
                if (r4 == 0) goto L_0x01ef
                java.lang.String r4 = r4.b()
                goto L_0x01f1
            L_0x01ef:
                r4 = r17
            L_0x01f1:
                com.fossil.io4 r5 = r25.f()
                if (r5 == 0) goto L_0x01fc
                java.lang.String r5 = r5.d()
                goto L_0x01fe
            L_0x01fc:
                r5 = r17
            L_0x01fe:
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x020b
                java.lang.String r6 = r6.e()
                if (r6 == 0) goto L_0x020b
                r12 = r6
            L_0x020b:
                java.lang.String r3 = r3.a(r4, r5, r12)
                com.fossil.we7 r4 = com.fossil.we7.a
                com.fossil.m85 r4 = r0.c
                android.view.View r4 = r4.d()
                com.fossil.ee7.a(r4, r13)
                android.content.Context r4 = r4.getContext()
                r5 = 2131886195(0x7f120073, float:1.9406962E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r5)
                java.lang.String r5 = "LanguageHelper.getString\u2026Body_Send_Friend_Request)"
                com.fossil.ee7.a(r4, r5)
                java.lang.Object[] r5 = new java.lang.Object[r7]
                r5[r16] = r3
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r5, r7)
                java.lang.String r4 = java.lang.String.format(r4, r5)
                com.fossil.ee7.a(r4, r14)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r19 = r3
                r20 = r4
                android.text.Spannable r4 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x0258
                java.lang.String r17 = r6.a()
            L_0x0258:
                r6 = r17
                com.fossil.oy6$b r7 = r0.a
                com.fossil.st4 r8 = r0.b
                com.fossil.rt4.a(r5, r6, r3, r7, r8)
                goto L_0x051d
            L_0x0263:
                java.lang.String r4 = "Title_Accepted_Friend_Request"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.fu4 r3 = com.fossil.fu4.a
                com.fossil.io4 r4 = r25.f()
                if (r4 == 0) goto L_0x0278
                java.lang.String r4 = r4.b()
                goto L_0x027a
            L_0x0278:
                r4 = r17
            L_0x027a:
                com.fossil.io4 r5 = r25.f()
                if (r5 == 0) goto L_0x0285
                java.lang.String r5 = r5.d()
                goto L_0x0287
            L_0x0285:
                r5 = r17
            L_0x0287:
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x0294
                java.lang.String r6 = r6.e()
                if (r6 == 0) goto L_0x0294
                r12 = r6
            L_0x0294:
                java.lang.String r3 = r3.a(r4, r5, r12)
                com.fossil.we7 r4 = com.fossil.we7.a
                com.fossil.m85 r4 = r0.c
                android.view.View r4 = r4.d()
                com.fossil.ee7.a(r4, r13)
                android.content.Context r4 = r4.getContext()
                r5 = 2131886189(0x7f12006d, float:1.940695E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r5)
                java.lang.String r5 = "LanguageHelper.getString\u2026_Accepted_Friend_Request)"
                com.fossil.ee7.a(r4, r5)
                java.lang.Object[] r5 = new java.lang.Object[r7]
                r5[r16] = r3
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r5, r7)
                java.lang.String r4 = java.lang.String.format(r4, r5)
                com.fossil.ee7.a(r4, r14)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r19 = r3
                r20 = r4
                android.text.Spannable r4 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x02e1
                java.lang.String r17 = r6.a()
            L_0x02e1:
                r6 = r17
                com.fossil.oy6$b r7 = r0.a
                com.fossil.st4 r8 = r0.b
                com.fossil.rt4.a(r5, r6, r3, r7, r8)
                goto L_0x051d
            L_0x02ec:
                java.lang.String r4 = "Title_Notification_Reached_Goal_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.xe5 r4 = com.fossil.xe5.b
                com.fossil.m85 r3 = r0.c
                android.view.View r3 = r3.d()
                com.fossil.ee7.a(r3, r13)
                android.content.Context r3 = r3.getContext()
                r5 = 2131886191(0x7f12006f, float:1.9406954E38)
                java.lang.String r6 = com.fossil.ig5.a(r3, r5)
                java.lang.String r3 = "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)"
                com.fossil.ee7.a(r6, r3)
                r7 = 0
                r8 = 4
                r9 = 0
                java.lang.String r5 = ""
                android.text.Spannable r17 = com.fossil.xe5.a(r4, r5, r6, r7, r8, r9)
                com.fossil.br4 r3 = r0.d
                int r3 = r3.b
                android.widget.ImageView r4 = r1.r
                com.fossil.ee7.a(r4, r15)
                com.fossil.pq.a(r4)
                android.widget.ImageView r4 = r1.r
                com.fossil.oy6$d r5 = com.fossil.oy6.a()
                com.fossil.oy6$b r5 = r5.b()
                com.fossil.oy6 r3 = r5.b(r10, r3)
                r4.setImageDrawable(r3)
                goto L_0x051b
            L_0x0339:
                java.lang.String r4 = "Title_Respond_Invitation_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.fu4 r3 = com.fossil.fu4.a
                com.fossil.io4 r4 = r25.f()
                if (r4 == 0) goto L_0x034e
                java.lang.String r4 = r4.b()
                goto L_0x0350
            L_0x034e:
                r4 = r17
            L_0x0350:
                com.fossil.io4 r5 = r25.f()
                if (r5 == 0) goto L_0x035b
                java.lang.String r5 = r5.d()
                goto L_0x035d
            L_0x035b:
                r5 = r17
            L_0x035d:
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x036a
                java.lang.String r6 = r6.e()
                if (r6 == 0) goto L_0x036a
                r12 = r6
            L_0x036a:
                java.lang.String r3 = r3.a(r4, r5, r12)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                com.fossil.we7 r4 = com.fossil.we7.a
                com.fossil.m85 r4 = r0.c
                android.view.View r4 = r4.d()
                com.fossil.ee7.a(r4, r13)
                android.content.Context r4 = r4.getContext()
                r5 = 2131886194(0x7f120072, float:1.940696E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r5)
                java.lang.String r5 = "LanguageHelper.getString\u2026ond_Invitation_Challenge)"
                com.fossil.ee7.a(r4, r5)
                java.lang.Object[] r5 = new java.lang.Object[r7]
                r5[r16] = r3
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r5, r7)
                java.lang.String r4 = java.lang.String.format(r4, r5)
                com.fossil.ee7.a(r4, r14)
                r21 = 0
                r22 = 4
                r23 = 0
                java.lang.String r19 = ""
                r20 = r4
                android.text.Spannable r4 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.io4 r6 = r25.f()
                if (r6 == 0) goto L_0x03b7
                java.lang.String r17 = r6.a()
            L_0x03b7:
                r6 = r17
                com.fossil.oy6$b r7 = r0.a
                com.fossil.st4 r8 = r0.b
                com.fossil.rt4.a(r5, r6, r3, r7, r8)
                goto L_0x051d
            L_0x03c2:
                java.lang.String r4 = "Title_Notification_Start_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.pn4 r3 = r25.b()
                if (r3 == 0) goto L_0x03d9
                java.lang.String r3 = r3.b()
                if (r3 == 0) goto L_0x03d9
                r19 = r3
                goto L_0x03db
            L_0x03d9:
                r19 = r9
            L_0x03db:
                com.fossil.we7 r3 = com.fossil.we7.a
                com.fossil.m85 r3 = r0.c
                android.view.View r3 = r3.d()
                com.fossil.ee7.a(r3, r13)
                android.content.Context r3 = r3.getContext()
                r4 = 2131886193(0x7f120071, float:1.9406958E38)
                java.lang.String r3 = com.fossil.ig5.a(r3, r4)
                java.lang.String r4 = "LanguageHelper.getString\u2026fication_Start_Challenge)"
                com.fossil.ee7.a(r3, r4)
                java.lang.Object[] r4 = new java.lang.Object[r7]
                r4[r16] = r19
                java.lang.Object[] r4 = java.util.Arrays.copyOf(r4, r7)
                java.lang.String r3 = java.lang.String.format(r3, r4)
                com.fossil.ee7.a(r3, r14)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r20 = r3
                android.text.Spannable r3 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                com.fossil.pn4 r4 = r25.b()
                if (r4 == 0) goto L_0x041d
                java.lang.String r17 = r4.d()
            L_0x041d:
                r4 = r17
                if (r4 != 0) goto L_0x0422
                goto L_0x0449
            L_0x0422:
                int r7 = r4.hashCode()
                if (r7 == r8) goto L_0x043b
                r8 = -637042289(0xffffffffda07818f, float:-9.5353933E15)
                if (r7 == r8) goto L_0x042e
                goto L_0x0449
            L_0x042e:
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x0449
                com.fossil.br4 r4 = r0.d
                int r4 = r4.b
                goto L_0x0458
            L_0x043b:
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x0449
                com.fossil.br4 r4 = r0.d
                int r4 = r4.c
                r10 = r11
                goto L_0x0458
            L_0x0449:
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r5 = 2131099972(0x7f060144, float:1.7812312E38)
                int r4 = com.fossil.v6.a(r4, r5)
                java.lang.String r10 = "bc"
            L_0x0458:
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.pq.a(r5)
                android.widget.ImageView r5 = r1.r
                com.fossil.oy6$d r6 = com.fossil.oy6.a()
                com.fossil.oy6$b r6 = r6.b()
                com.fossil.oy6 r4 = r6.b(r10, r4)
                r5.setImageDrawable(r4)
                goto L_0x0519
            L_0x0473:
                java.lang.String r4 = "Title_Notification_End_Challenge"
                boolean r3 = r3.equals(r4)
                if (r3 == 0) goto L_0x051b
                com.fossil.pn4 r3 = r25.b()
                if (r3 == 0) goto L_0x048a
                java.lang.String r3 = r3.b()
                if (r3 == 0) goto L_0x048a
                r19 = r3
                goto L_0x048c
            L_0x048a:
                r19 = r9
            L_0x048c:
                com.fossil.we7 r3 = com.fossil.we7.a
                com.fossil.m85 r3 = r0.c
                android.view.View r3 = r3.d()
                com.fossil.ee7.a(r3, r13)
                android.content.Context r3 = r3.getContext()
                r4 = 2131886190(0x7f12006e, float:1.9406952E38)
                java.lang.String r3 = com.fossil.ig5.a(r3, r4)
                java.lang.String r4 = "LanguageHelper.getString\u2026tification_End_Challenge)"
                com.fossil.ee7.a(r3, r4)
                java.lang.Object[] r4 = new java.lang.Object[r7]
                r4[r16] = r19
                java.lang.Object[] r4 = java.util.Arrays.copyOf(r4, r7)
                java.lang.String r3 = java.lang.String.format(r3, r4)
                com.fossil.ee7.a(r3, r14)
                com.fossil.xe5 r18 = com.fossil.xe5.b
                r21 = 0
                r22 = 4
                r23 = 0
                r20 = r3
                android.text.Spannable r3 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
                com.fossil.pn4 r4 = r25.b()
                if (r4 == 0) goto L_0x04ce
                java.lang.String r17 = r4.d()
            L_0x04ce:
                r4 = r17
                if (r4 != 0) goto L_0x04d3
                goto L_0x04f9
            L_0x04d3:
                int r7 = r4.hashCode()
                if (r7 == r8) goto L_0x04ec
                r8 = -637042289(0xffffffffda07818f, float:-9.5353933E15)
                if (r7 == r8) goto L_0x04df
                goto L_0x04f9
            L_0x04df:
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x04f9
                com.fossil.br4 r4 = r0.d
                int r4 = r4.b
                goto L_0x0500
            L_0x04ec:
                boolean r4 = r4.equals(r6)
                if (r4 == 0) goto L_0x04f9
                com.fossil.br4 r4 = r0.d
                int r4 = r4.c
                goto L_0x04ff
            L_0x04f9:
                com.fossil.br4 r4 = r0.d
                int r4 = r4.c
            L_0x04ff:
                r10 = r11
            L_0x0500:
                android.widget.ImageView r5 = r1.r
                com.fossil.ee7.a(r5, r15)
                com.fossil.pq.a(r5)
                android.widget.ImageView r5 = r1.r
                com.fossil.oy6$d r6 = com.fossil.oy6.a()
                com.fossil.oy6$b r6 = r6.b()
                com.fossil.oy6 r4 = r6.b(r10, r4)
                r5.setImageDrawable(r4)
            L_0x0519:
                r4 = r3
                goto L_0x051d
            L_0x051b:
                r4 = r17
            L_0x051d:
                com.portfolio.platform.view.FlexibleTextView r3 = r1.u
                java.lang.String r5 = "tvContent"
                com.fossil.ee7.a(r3, r5)
                r3.setText(r4)
                com.portfolio.platform.view.FlexibleTextView r1 = r1.t
                java.lang.String r3 = "tvAgo"
                com.fossil.ee7.a(r1, r3)
                java.util.Date r2 = r25.d()
                java.lang.String r2 = com.fossil.zd5.x(r2)
                r1.setText(r2)
                return
                switch-data {-1559094286->0x0473, -764698119->0x03c2, -473527954->0x0339, 238453777->0x02ec, 634854495->0x0263, 1433363166->0x01da, 1669546642->0x0132, 1898363949->0x0032, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.br4.a.a(com.fossil.ao4):void");
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        ((a) viewHolder).a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        m85 a2 = m85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemBcNotificationChalle\u2026tInflater, parent, false)");
        return new a(this, a2);
    }

    @DexIgnore
    public final void a(List<ao4> list) {
        ee7.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }
}
