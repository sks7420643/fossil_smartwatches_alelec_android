package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn5 implements Factory<on5> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public pn5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static pn5 a(Provider<UserRepository> provider) {
        return new pn5(provider);
    }

    @DexIgnore
    public static on5 a(UserRepository userRepository) {
        return new on5(userRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public on5 get() {
        return a(this.a.get());
    }
}
