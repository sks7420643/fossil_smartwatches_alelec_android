package com.fossil;

import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ve5 {
    @DexIgnore
    public static /* final */ ve5 a; // = new ve5();

    @DexIgnore
    public final List<xg5.a> a(HybridPreset hybridPreset) {
        ee7.b(hybridPreset, "hybridPreset");
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = buttons.iterator();
        while (it.hasNext()) {
            xg5.a b = a.b(it.next().getAppId());
            if (b != null) {
                arrayList.add(b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final xg5.a b(String str) {
        ee7.b(str, "microAppid");
        if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return xg5.a.SET_MICRO_APP_MUSIC;
        }
        if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return xg5.a.SET_MICRO_APP_COMMUTE_TIME;
        }
        return null;
    }

    @DexIgnore
    public final xg5.a c(String str) {
        ee7.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode != 104263205) {
                if (hashCode == 1223440372 && str.equals("weather")) {
                    return xg5.a.SET_COMPLICATION_WATCH_APP_WEATHER;
                }
            } else if (str.equals(Constants.MUSIC)) {
                return xg5.a.SET_WATCH_APP_MUSIC;
            }
        } else if (str.equals("commute-time")) {
            return xg5.a.SET_WATCH_APP_COMMUTE_TIME;
        }
        return null;
    }

    @DexIgnore
    public final List<xg5.a> a(DianaPreset dianaPreset) {
        ee7.b(dianaPreset, "dianaPreset");
        ArrayList arrayList = new ArrayList();
        ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = complications.iterator();
        while (it.hasNext()) {
            xg5.a a2 = a.a(it.next().getId());
            if (a2 != null) {
                arrayList2.add(a2);
            }
        }
        arrayList.addAll(arrayList2);
        ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
        ArrayList arrayList3 = new ArrayList();
        Iterator<T> it2 = watchapps.iterator();
        while (it2.hasNext()) {
            xg5.a c = a.c(it2.next().getId());
            if (c != null) {
                arrayList3.add(c);
            }
        }
        arrayList.addAll(arrayList3);
        return arrayList;
    }

    @DexIgnore
    public final xg5.a a(String str) {
        ee7.b(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -48173007) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                return xg5.a.SET_COMPLICATION_WATCH_APP_WEATHER;
            }
        } else if (str.equals("chance-of-rain")) {
            return xg5.a.SET_COMPLICATION_CHANCE_OF_RAIN;
        }
        return null;
    }
}
