package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il2 implements Parcelable.Creator<hl2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ hl2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 1) {
                j72.v(parcel, a);
            } else {
                status = (Status) j72.a(parcel, a, Status.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new hl2(status);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ hl2[] newArray(int i) {
        return new hl2[i];
    }
}
