package com.fossil;

import android.os.Handler;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qu implements bv {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ /* synthetic */ Handler a;

        @DexIgnore
        public a(qu quVar, Handler handler) {
            this.a = handler;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ yu a;
        @DexIgnore
        public /* final */ av b;
        @DexIgnore
        public /* final */ Runnable c;

        @DexIgnore
        public b(yu yuVar, av avVar, Runnable runnable) {
            this.a = yuVar;
            this.b = avVar;
            this.c = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.a.isCanceled()) {
                this.a.finish("canceled-at-delivery");
                return;
            }
            if (this.b.a()) {
                this.a.deliverResponse(this.b.a);
            } else {
                this.a.deliverError(this.b.c);
            }
            if (this.b.d) {
                this.a.addMarker("intermediate-response");
            } else {
                this.a.finish("done");
            }
            Runnable runnable = this.c;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public qu(Handler handler) {
        this.a = new a(this, handler);
    }

    @DexIgnore
    @Override // com.fossil.bv
    public void a(yu<?> yuVar, av<?> avVar) {
        a(yuVar, avVar, null);
    }

    @DexIgnore
    @Override // com.fossil.bv
    public void a(yu<?> yuVar, av<?> avVar, Runnable runnable) {
        yuVar.markDelivered();
        yuVar.addMarker("post-response");
        this.a.execute(new b(yuVar, avVar, runnable));
    }

    @DexIgnore
    @Override // com.fossil.bv
    public void a(yu<?> yuVar, fv fvVar) {
        yuVar.addMarker("post-error");
        this.a.execute(new b(yuVar, av.a(fvVar), null));
    }
}
