package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "androidx.lifecycle.CoroutineLiveDataKt$addDisposableSource$2", f = "CoroutineLiveData.kt", l = {}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super id>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $source;
        @DexIgnore
        public /* final */ /* synthetic */ xd $this_addDisposableSource;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ed$a$a")
        /* renamed from: com.fossil.ed$a$a  reason: collision with other inner class name */
        public static final class C0051a<T> implements zd<S> {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0051a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zd
            public final void onChanged(T t) {
                this.a.$this_addDisposableSource.b((Object) t);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xd xdVar, LiveData liveData, fb7 fb7) {
            super(2, fb7);
            this.$this_addDisposableSource = xdVar;
            this.$source = liveData;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.$this_addDisposableSource, this.$source, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super id> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.$this_addDisposableSource.a(this.$source, new C0051a(this));
                return new id(this.$source, this.$this_addDisposableSource);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public static final <T> Object a(xd<T> xdVar, LiveData<T> liveData, fb7<? super id> fb7) {
        return vh7.a(qj7.c().g(), new a(xdVar, liveData, null), fb7);
    }

    @DexIgnore
    public static /* synthetic */ LiveData a(ib7 ib7, long j, kd7 kd7, int i, Object obj) {
        if ((i & 1) != 0) {
            ib7 = jb7.INSTANCE;
        }
        if ((i & 2) != 0) {
            j = 5000;
        }
        return a(ib7, j, kd7);
    }

    @DexIgnore
    public static final <T> LiveData<T> a(ib7 ib7, long j, kd7<? super vd<T>, ? super fb7<? super i97>, ? extends Object> kd7) {
        ee7.b(ib7, "context");
        ee7.b(kd7, "block");
        return new dd(ib7, j, kd7);
    }
}
