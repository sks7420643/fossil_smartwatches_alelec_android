package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.FlexibleTabLayout;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p25 extends o25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131363162, 1);
        x.put(2131362599, 2);
        x.put(2131362596, 3);
        x.put(2131363005, 4);
    }
    */

    @DexIgnore
    public p25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 5, w, x));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public p25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[3], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (ViewPager2) objArr[4], (FlexibleTabLayout) objArr[1]);
        this.v = -1;
        ((o25) this).s.setTag(null);
        a(view);
        f();
    }
}
