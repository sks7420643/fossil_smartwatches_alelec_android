package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy0 extends i60 {
    @DexIgnore
    public /* final */ String[] a;

    @DexIgnore
    public hy0(String[] strArr) {
        this.a = strArr;
    }

    @DexIgnore
    @Override // com.fossil.i60
    public boolean a(km1 km1) {
        if (this.a.length == 0) {
            return true;
        }
        String serialNumber = km1.t.getSerialNumber();
        for (String str : this.a) {
            if (mh7.c(serialNumber, str, false, 2, null)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.F1, "serial_number_prefixes"), r51.G1, yz0.a(this.a));
    }
}
