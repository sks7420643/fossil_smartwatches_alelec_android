package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b10<DataT> implements m00<Uri, DataT> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ m00<File, DataT> b;
    @DexIgnore
    public /* final */ m00<Uri, DataT> c;
    @DexIgnore
    public /* final */ Class<DataT> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<DataT> implements n00<Uri, DataT> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Class<DataT> b;

        @DexIgnore
        public a(Context context, Class<DataT> cls) {
            this.a = context;
            this.b = cls;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public final m00<Uri, DataT> a(q00 q00) {
            return new b10(this.a, q00.a(File.class, this.b), q00.a(Uri.class, this.b), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends a<InputStream> {
        @DexIgnore
        public c(Context context) {
            super(context, InputStream.class);
        }
    }

    @DexIgnore
    public b10(Context context, m00<File, DataT> m00, m00<Uri, DataT> m002, Class<DataT> cls) {
        this.a = context.getApplicationContext();
        this.b = m00;
        this.c = m002;
        this.d = cls;
    }

    @DexIgnore
    public m00.a<DataT> a(Uri uri, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(uri), new d(this.a, this.b, this.c, uri, i, i2, axVar, this.d));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && vx.b(uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<DataT> implements ix<DataT> {
        @DexIgnore
        public static /* final */ String[] p; // = {"_data"};
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ m00<File, DataT> b;
        @DexIgnore
        public /* final */ m00<Uri, DataT> c;
        @DexIgnore
        public /* final */ Uri d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ ax g;
        @DexIgnore
        public /* final */ Class<DataT> h;
        @DexIgnore
        public volatile boolean i;
        @DexIgnore
        public volatile ix<DataT> j;

        @DexIgnore
        public d(Context context, m00<File, DataT> m00, m00<Uri, DataT> m002, Uri uri, int i2, int i3, ax axVar, Class<DataT> cls) {
            this.a = context.getApplicationContext();
            this.b = m00;
            this.c = m002;
            this.d = uri;
            this.e = i2;
            this.f = i3;
            this.g = axVar;
            this.h = cls;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super DataT> aVar) {
            try {
                ix<DataT> d2 = d();
                if (d2 == null) {
                    aVar.a((Exception) new IllegalArgumentException("Failed to build fetcher for: " + this.d));
                    return;
                }
                this.j = d2;
                if (this.i) {
                    cancel();
                } else {
                    d2.a(ewVar, aVar);
                }
            } catch (FileNotFoundException e2) {
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        public final m00.a<DataT> c() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.b.a(a(this.d), this.e, this.f, this.g);
            }
            return this.c.a(e() ? MediaStore.setRequireOriginal(this.d) : this.d, this.e, this.f, this.g);
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
            this.i = true;
            ix<DataT> ixVar = this.j;
            if (ixVar != null) {
                ixVar.cancel();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: com.fossil.ix<Data>, com.fossil.ix<DataT> */
        public final ix<DataT> d() throws FileNotFoundException {
            m00.a<DataT> c2 = c();
            if (c2 != null) {
                return (ix<Data>) c2.c;
            }
            return null;
        }

        @DexIgnore
        public final boolean e() {
            return this.a.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<DataT> getDataClass() {
            return this.h;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
            ix<DataT> ixVar = this.j;
            if (ixVar != null) {
                ixVar.a();
            }
        }

        @DexIgnore
        public final File a(Uri uri) throws FileNotFoundException {
            Cursor cursor = null;
            try {
                cursor = this.a.getContentResolver().query(uri, p, null, null, null);
                if (cursor == null || !cursor.moveToFirst()) {
                    throw new FileNotFoundException("Failed to media store entry for: " + uri);
                }
                String string = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
                if (!TextUtils.isEmpty(string)) {
                    return new File(string);
                }
                throw new FileNotFoundException("File path was empty in media store for: " + uri);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }
}
