package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v72 extends n62<w72> {
    @DexIgnore
    public v72(Context context, Looper looper, j62 j62, a12.b bVar, a12.c cVar) {
        super(context, looper, 39, j62, bVar, cVar);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        if (queryLocalInterface instanceof w72) {
            return (w72) queryLocalInterface;
        }
        return new y72(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.common.service.START";
    }
}
