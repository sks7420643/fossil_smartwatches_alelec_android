package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws4 {
    @DexIgnore
    public TimerViewObserver a;
    @DexIgnore
    public /* final */ oy6.b b;
    @DexIgnore
    public /* final */ st4 c; // = st4.d.a();
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ca5 a;
        @DexIgnore
        public /* final */ View b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ca5 ca5, View view) {
            super(view);
            ee7.b(ca5, "binding");
            ee7.b(view, "root");
            this.a = ca5;
            this.b = view;
        }

        @DexIgnore
        public final void a(jo4 jo4, TimerViewObserver timerViewObserver, oy6.b bVar, st4 st4) {
            String str;
            ee7.b(jo4, "recommended");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            ca5 ca5 = this.a;
            mn4 b2 = jo4.b();
            ca5.w.setEndingText(ig5.a(PortfolioApp.g0.c(), 2131886302));
            TimerTextView timerTextView = ca5.w;
            Date m = b2.m();
            timerTextView.setTime(m != null ? m.getTime() : 0);
            ca5.w.setObserver(timerViewObserver);
            FlexibleTextView flexibleTextView = ca5.r;
            ee7.a((Object) flexibleTextView, "ftvChallengeName");
            flexibleTextView.setText(b2.g());
            eo4 i = b2.i();
            if (i == null || (str = i.c()) == null) {
                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            }
            ImageView imageView = ca5.x;
            ee7.a((Object) imageView, "imgAvatar");
            eo4 i2 = b2.i();
            String str2 = null;
            rt4.a(imageView, i2 != null ? i2.e() : null, str, bVar, st4);
            String c = b2.c();
            if (c != null) {
                FlexibleTextView flexibleTextView2 = ca5.q;
                ee7.a((Object) flexibleTextView2, "ftvChallengeDes");
                flexibleTextView2.setText(c);
            } else {
                FlexibleTextView flexibleTextView3 = ca5.q;
                ee7.a((Object) flexibleTextView3, "ftvChallengeDes");
                flexibleTextView3.setText("");
            }
            FlexibleTextView flexibleTextView4 = ca5.t;
            ee7.a((Object) flexibleTextView4, "ftvDuration");
            Integer d = b2.d();
            flexibleTextView4.setText(d != null ? ye5.a(d.intValue()) : null);
            Integer h = b2.h();
            int intValue = h != null ? h.intValue() : 0;
            FlexibleTextView flexibleTextView5 = ca5.s;
            ee7.a((Object) flexibleTextView5, "ftvCountPlayer");
            flexibleTextView5.setText(String.valueOf(intValue));
            int a2 = v6.a(PortfolioApp.g0.c(), 2131099689);
            int a3 = v6.a(PortfolioApp.g0.c(), 2131099677);
            int i3 = 8;
            if (ee7.a((Object) b2.u(), (Object) "activity_best_result")) {
                ca5.r.setTextColor(a3);
                FlexibleTextView flexibleTextView6 = ca5.v;
                ee7.a((Object) flexibleTextView6, "ftvSteps");
                flexibleTextView6.setVisibility(8);
            } else {
                ca5.r.setTextColor(a2);
                FlexibleTextView flexibleTextView7 = ca5.v;
                ee7.a((Object) flexibleTextView7, "ftvSteps");
                flexibleTextView7.setVisibility(0);
                FlexibleTextView flexibleTextView8 = ca5.v;
                ee7.a((Object) flexibleTextView8, "ftvSteps");
                Integer t = b2.t();
                if (t != null) {
                    str2 = String.valueOf(t.intValue());
                }
                flexibleTextView8.setText(str2);
            }
            FlexibleTextView flexibleTextView9 = ca5.u;
            ee7.a((Object) flexibleTextView9, "ftvNew");
            if (jo4.c()) {
                i3 = 0;
            }
            flexibleTextView9.setVisibility(i3);
            String j = b2.j();
            if (j != null && j.hashCode() == -314497661 && j.equals("private")) {
                ca5.s.setCompoundDrawablesWithIntrinsicBounds(2131231024, 0, 0, 0);
            } else {
                ca5.s.setCompoundDrawablesWithIntrinsicBounds(2131231025, 0, 0, 0);
            }
        }
    }

    @DexIgnore
    public ws4(int i) {
        this.d = i;
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.b = b2;
    }

    @DexIgnore
    public final int a() {
        return this.d;
    }

    @DexIgnore
    public final void a(TimerViewObserver timerViewObserver) {
        this.a = timerViewObserver;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        Object obj = list.get(i);
        return (obj instanceof jo4) && ee7.a(((jo4) obj).a(), "pending-invitation");
    }

    @DexIgnore
    public a a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        ca5 a2 = ca5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemRecommendationChalle\u2026(inflater, parent, false)");
        View d2 = a2.d();
        ee7.a((Object) d2, "binding.root");
        return new a(a2, d2);
    }

    @DexIgnore
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof jo4)) {
            obj = null;
        }
        jo4 jo4 = (jo4) obj;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        if (jo4 != null && aVar != null) {
            aVar.a(jo4, this.a, this.b, this.c);
        }
    }
}
