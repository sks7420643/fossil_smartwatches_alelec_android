package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um3 extends vm3 {
    @DexIgnore
    public dp2 g;
    @DexIgnore
    public /* final */ /* synthetic */ om3 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public um3(om3 om3, String str, int i, dp2 dp2) {
        super(str, i);
        this.h = om3;
        this.g = dp2;
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final int a() {
        return this.g.p();
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final boolean b() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vm3
    public final boolean c() {
        return false;
    }

    @DexIgnore
    public final boolean a(Long l, Long l2, zp2 zp2, boolean z) {
        boolean z2 = b13.a() && this.h.l().d(((vm3) this).a, wb3.e0);
        boolean s = this.g.s();
        boolean t = this.g.t();
        boolean v = this.g.v();
        boolean z3 = s || t || v;
        Boolean bool = null;
        Integer num = null;
        bool = null;
        bool = null;
        bool = null;
        bool = null;
        if (!z || z3) {
            bp2 r = this.g.r();
            boolean t2 = r.t();
            if (zp2.t()) {
                if (!r.q()) {
                    this.h.e().w().a("No number filter for long property. property", this.h.i().c(zp2.q()));
                } else {
                    bool = vm3.a(vm3.a(zp2.u(), r.r()), t2);
                }
            } else if (zp2.v()) {
                if (!r.q()) {
                    this.h.e().w().a("No number filter for double property. property", this.h.i().c(zp2.q()));
                } else {
                    bool = vm3.a(vm3.a(zp2.w(), r.r()), t2);
                }
            } else if (!zp2.r()) {
                this.h.e().w().a("User property has no value, property", this.h.i().c(zp2.q()));
            } else if (r.zza()) {
                bool = vm3.a(vm3.a(zp2.s(), r.p(), this.h.e()), t2);
            } else if (!r.q()) {
                this.h.e().w().a("No string or number filter defined. property", this.h.i().c(zp2.q()));
            } else if (fm3.a(zp2.s())) {
                bool = vm3.a(vm3.a(zp2.s(), r.r()), t2);
            } else {
                this.h.e().w().a("Invalid user property value for Numeric number filter. property, value", this.h.i().c(zp2.q()), zp2.s());
            }
            this.h.e().B().a("Property filter result", bool == null ? "null" : bool);
            if (bool == null) {
                return false;
            }
            ((vm3) this).c = true;
            if (v && !bool.booleanValue()) {
                return true;
            }
            if (!z || this.g.s()) {
                ((vm3) this).d = bool;
            }
            if (bool.booleanValue() && z3 && zp2.zza()) {
                long p = zp2.p();
                if (l != null) {
                    p = l.longValue();
                }
                if (z2 && this.g.s() && !this.g.t() && l2 != null) {
                    p = l2.longValue();
                }
                if (this.g.t()) {
                    ((vm3) this).f = Long.valueOf(p);
                } else {
                    ((vm3) this).e = Long.valueOf(p);
                }
            }
            return true;
        }
        mg3 B = this.h.e().B();
        Integer valueOf = Integer.valueOf(((vm3) this).b);
        if (this.g.zza()) {
            num = Integer.valueOf(this.g.p());
        }
        B.a("Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", valueOf, num);
        return true;
    }
}
