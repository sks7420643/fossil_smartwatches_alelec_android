package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ff0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r60 c;
    @DexIgnore
    public /* final */ og0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ff0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ff0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(ac0.class.getClassLoader());
            if (readParcelable != null) {
                ac0 ac0 = (ac0) readParcelable;
                df0 df0 = (df0) parcel.readParcelable(df0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(r60.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new ff0(ac0, df0, (r60) readParcelable2, og0.values()[parcel.readInt()]);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ff0[] newArray(int i) {
            return new ff0[i];
        }
    }

    @DexIgnore
    public ff0(ac0 ac0, r60 r60, og0 og0) {
        super(ac0, null);
        this.d = og0;
        this.c = r60;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        try {
            qs1 qs1 = qs1.d;
            yb0 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return qs1.a(s, r60, new ww0(((ac0) deviceRequest).d(), new r60(this.c.getMajor(), this.c.getMinor()), this.d.a()).getData());
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (f41 e) {
            wl0.h.a(e);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(yz0.a(super.b(), r51.r3, this.c.toString()), r51.a4, yz0.a(this.d));
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ff0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ff0 ff0 = (ff0) obj;
            return !(ee7.a(this.c, ff0.c) ^ true) && this.d == ff0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.MicroAppErrorData");
    }

    @DexIgnore
    public final og0 getErrorType() {
        return this.d;
    }

    @DexIgnore
    public final r60 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }

    @DexIgnore
    public ff0(ac0 ac0, df0 df0, r60 r60, og0 og0) {
        super(ac0, df0);
        this.d = og0;
        this.c = r60;
    }
}
