package com.fossil;

import com.fossil.pu1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu1 extends pu1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ dt1 c;

    @DexIgnore
    @Override // com.fossil.pu1
    public String a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.pu1
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.pu1
    public dt1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof pu1)) {
            return false;
        }
        pu1 pu1 = (pu1) obj;
        if (this.a.equals(pu1.a())) {
            if (!Arrays.equals(this.b, pu1 instanceof fu1 ? ((fu1) pu1).b : pu1.b()) || !this.c.equals(pu1.c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b)) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends pu1.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public byte[] b;
        @DexIgnore
        public dt1 c;

        @DexIgnore
        @Override // com.fossil.pu1.a
        public pu1.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null backendName");
        }

        @DexIgnore
        @Override // com.fossil.pu1.a
        public pu1.a a(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.pu1.a
        public pu1.a a(dt1 dt1) {
            if (dt1 != null) {
                this.c = dt1;
                return this;
            }
            throw new NullPointerException("Null priority");
        }

        @DexIgnore
        @Override // com.fossil.pu1.a
        public pu1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " backendName";
            }
            if (this.c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new fu1(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public fu1(String str, byte[] bArr, dt1 dt1) {
        this.a = str;
        this.b = bArr;
        this.c = dt1;
    }
}
