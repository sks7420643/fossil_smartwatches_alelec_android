package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr7 {
    @DexIgnore
    public static final int a(int[] iArr, int i, int i2, int i3) {
        ee7.b(iArr, "$this$binarySearch");
        int i4 = i3 - 1;
        while (i2 <= i4) {
            int i5 = (i2 + i4) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i) {
                i2 = i5 + 1;
            } else if (i6 <= i) {
                return i5;
            } else {
                i4 = i5 - 1;
            }
        }
        return (-i2) - 1;
    }

    @DexIgnore
    public static final int a(pr7 pr7, int i) {
        ee7.b(pr7, "$this$segment");
        int a = a(pr7.getDirectory$okio(), i + 1, 0, pr7.getSegments$okio().length);
        return a >= 0 ? a : ~a;
    }
}
