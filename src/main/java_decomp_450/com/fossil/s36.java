package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s36 implements Factory<r36> {
    @DexIgnore
    public static r36 a(n36 n36, CategoryRepository categoryRepository, ch5 ch5) {
        return new r36(n36, categoryRepository, ch5);
    }
}
