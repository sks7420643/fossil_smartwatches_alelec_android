package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bp5;
import com.fossil.cy6;
import com.fossil.wo5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l56 extends go5 implements a66, cy6.g {
    @DexIgnore
    public qw6<k35> f;
    @DexIgnore
    public z56 g;
    @DexIgnore
    public wo5 h;
    @DexIgnore
    public bp5 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public j56 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements bp5.c {
        @DexIgnore
        public /* final */ /* synthetic */ l56 a;

        @DexIgnore
        public b(l56 l56) {
            this.a = l56;
        }

        @DexIgnore
        @Override // com.fossil.bp5.c
        public void a(MicroApp microApp) {
            ee7.b(microApp, "microApp");
            l56.a(this.a).a(microApp.getId());
        }

        @DexIgnore
        @Override // com.fossil.bp5.c
        public void b(MicroApp microApp) {
            ee7.b(microApp, "microApp");
            xg5.a(xg5.b, this.a.getContext(), ve5.a.b(microApp.getId()), false, false, false, (Integer) null, 60, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements wo5.c {
        @DexIgnore
        public /* final */ /* synthetic */ l56 a;

        @DexIgnore
        public c(l56 l56) {
            this.a = l56;
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a() {
            l56.a(this.a).h();
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a(Category category) {
            ee7.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppFragment", "onItemClicked category=" + category);
            l56.a(this.a).a(category);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ l56 a;

        @DexIgnore
        public d(l56 l56) {
            this.a = l56;
        }

        @DexIgnore
        public final void onClick(View view) {
            l56.a(this.a).i();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ z56 a(l56 l56) {
        z56 z56 = l56.g;
        if (z56 != null) {
            return z56;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void I(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void b(MicroApp microApp) {
        if (microApp != null) {
            bp5 bp5 = this.i;
            if (bp5 != null) {
                bp5.b(microApp.getId());
                c(microApp);
                return;
            }
            ee7.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(MicroApp microApp) {
        qw6<k35> qw6 = this.f;
        if (qw6 != null) {
            k35 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.y;
                ee7.a((Object) flexibleTextView, "binding.tvSelectedMicroApp");
                flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), microApp.getNameKey(), microApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.u;
                ee7.a((Object) flexibleTextView2, "binding.tvMicroAppDetail");
                flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), microApp.getDescriptionKey(), microApp.getDescription()));
                bp5 bp5 = this.i;
                if (bp5 != null) {
                    int a3 = bp5.a(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.t.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                ee7.d("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void d(String str) {
        ee7.b(str, "category");
        qw6<k35> qw6 = this.f;
        if (qw6 != null) {
            k35 a2 = qw6.a();
            if (a2 != null) {
                wo5 wo5 = this.h;
                if (wo5 != null) {
                    int a3 = wo5.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        wo5 wo52 = this.h;
                        if (wo52 != null) {
                            wo52.a(a3);
                            a2.s.smoothScrollToPosition(a3);
                            return;
                        }
                        ee7.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "MicroAppFragment";
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void e(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final void f1() {
        if (isActive()) {
            bp5 bp5 = this.i;
            if (bp5 != null) {
                bp5.c();
            } else {
                ee7.d("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            rj4 rj4 = this.j;
            if (rj4 != null) {
                he a2 = je.a(hybridCustomizeEditActivity, rj4).a(j56.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                j56 j56 = (j56) a2;
                this.p = j56;
                z56 z56 = this.g;
                if (z56 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (j56 != null) {
                    z56.a(j56);
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        Ringtone ringtone;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "onActivityResult requestCode " + i2 + " resultCode " + i3);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 != 104) {
                    if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                        z56 z56 = this.g;
                        if (z56 != null) {
                            z56.a(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), wc5.a(commuteTimeSetting));
                        } else {
                            ee7.d("mPresenter");
                            throw null;
                        }
                    }
                } else if (intent != null && i3 == -1 && (ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE")) != null) {
                    z56 z562 = this.g;
                    if (z562 != null) {
                        z562.a(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), wc5.a(ringtone));
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    z56 z563 = this.g;
                    if (z563 != null) {
                        ee7.a((Object) stringExtra, "selectedMicroAppId");
                        z563.a(stringExtra);
                        return;
                    }
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            z56 z564 = this.g;
            if (z564 != null) {
                z564.a(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), wc5.a(secondTimezoneSetting));
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        k35 k35 = (k35) qb.a(layoutInflater, 2131558584, viewGroup, false, a1());
        PortfolioApp.g0.c().f().a(new b66(this)).a(this);
        this.f = new qw6<>(this, k35);
        ee7.a((Object) k35, "binding");
        return k35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        z56 z56 = this.g;
        if (z56 != null) {
            z56.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        z56 z56 = this.g;
        if (z56 != null) {
            z56.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        bp5 bp5 = new bp5(null, null, 3, null);
        bp5.a(new b(this));
        this.i = bp5;
        wo5 wo5 = new wo5(null, null, 3, null);
        wo5.a(new c(this));
        this.h = wo5;
        qw6<k35> qw6 = this.f;
        if (qw6 != null) {
            k35 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.r.setBackgroundColor(Color.parseColor(b2));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                wo5 wo52 = this.h;
                if (wo52 != null) {
                    recyclerView.setAdapter(wo52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    bp5 bp52 = this.i;
                    if (bp52 != null) {
                        recyclerView2.setAdapter(bp52);
                        a2.w.setOnClickListener(new d(this));
                        return;
                    }
                    ee7.d("mMicroAppAdapter");
                    throw null;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void w(List<MicroApp> list) {
        ee7.b(list, "microApps");
        bp5 bp5 = this.i;
        if (bp5 != null) {
            bp5.a(list);
        } else {
            ee7.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void w(String str) {
        ee7.b(str, "content");
        qw6<k35> qw6 = this.f;
        if (qw6 != null) {
            k35 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.tvMicroAppDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(z56 z56) {
        ee7.b(z56, "presenter");
        this.g = z56;
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void a(List<Category> list) {
        ee7.b(list, "categories");
        wo5 wo5 = this.h;
        if (wo5 != null) {
            wo5.a(list);
        } else {
            ee7.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void a(boolean z, String str, String str2, String str3) {
        ee7.b(str, "microAppId");
        ee7.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        qw6<k35> qw6 = this.f;
        if (qw6 != null) {
            k35 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                ee7.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView2 = a2.w;
                    ee7.a((Object) flexibleTextView2, "it.tvMicroAppSetting");
                    flexibleTextView2.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView3 = a2.w;
                        ee7.a((Object) flexibleTextView3, "it.tvMicroAppSetting");
                        flexibleTextView3.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView4 = a2.w;
                    ee7.a((Object) flexibleTextView4, "it.tvMicroAppSetting");
                    flexibleTextView4.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView5 = a2.w;
                ee7.a((Object) flexibleTextView5, "it.tvMicroAppSetting");
                flexibleTextView5.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void c(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        ee7.b(str, "tag");
        if (ee7.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363307 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    ((cl5) activity3).k();
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (ee7.a((Object) str, (Object) bx6.c.a())) {
            if (i2 == 2131363307 && (activity2 = getActivity()) != null) {
                if (!ku7.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    px6.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    ((cl5) activity2).k();
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (ee7.a((Object) str, (Object) "LOCATION_PERMISSION_TAG")) {
            if (i2 != 2131363307 || (activity = getActivity()) == null) {
                return;
            }
            if (activity != null) {
                ((cl5) activity).k();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (ee7.a((Object) str, (Object) InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363307) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    @Override // com.fossil.a66
    public void a(String str, String str2, String str3) {
        ee7.b(str, "topMicroApp");
        ee7.b(str2, "middleMicroApp");
        ee7.b(str3, "bottomMicroApp");
        SearchMicroAppActivity.z.a(this, str, str2, str3);
    }
}
