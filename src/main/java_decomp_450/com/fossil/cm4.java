package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ ArrayList<Explore> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleTextView a;
        @DexIgnore
        public ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(cm4 cm4, View view) {
            super(view);
            ee7.b(view, "view");
            this.a = (FlexibleTextView) view.findViewById(2131362360);
            this.b = (ImageView) view.findViewById(2131362696);
        }

        @DexIgnore
        public final void a(Explore explore, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareTutorialAdapter", "build - position=" + i);
            if (explore != null) {
                FlexibleTextView flexibleTextView = this.a;
                ee7.a((Object) flexibleTextView, "ftvContent");
                flexibleTextView.setText(explore.getDescription());
                ImageView imageView = this.b;
                ee7.a((Object) imageView, "ivIcon");
                imageView.setBackground(v6.c(PortfolioApp.g0.c(), explore.getBackground()));
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public cm4(ArrayList<Explore> arrayList) {
        ee7.b(arrayList, "mData");
        this.a = arrayList;
    }

    @DexIgnore
    public final void a(List<? extends Explore> list) {
        ee7.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        if (getItemCount() > i && i != -1) {
            ((b) viewHolder).a(this.a.get(i), i);
        }
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.cm4$b' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558683, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026_tutorial, parent, false)");
        return new b(this, inflate);
    }
}
