package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dt3;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ot3 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ dt3<?> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public void onClick(View view) {
            ot3.this.a.a(ht3.a(this.a, ot3.this.a.d1().c));
            ot3.this.a.a(dt3.k.DAY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;

        @DexIgnore
        public b(TextView textView) {
            super(textView);
            this.a = textView;
        }
    }

    @DexIgnore
    public ot3(dt3<?> dt3) {
        this.a = dt3;
    }

    @DexIgnore
    public int b(int i) {
        return i - this.a.b1().e().d;
    }

    @DexIgnore
    public int c(int i) {
        return this.a.b1().e().d + i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.b1().f();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        int c = c(i);
        String string = bVar.a.getContext().getString(rr3.mtrl_picker_navigate_to_year_description);
        bVar.a.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(c)));
        bVar.a.setContentDescription(String.format(string, Integer.valueOf(c)));
        ys3 c1 = this.a.c1();
        Calendar b2 = nt3.b();
        xs3 xs3 = b2.get(1) == c ? c1.f : c1.d;
        for (Long l : this.a.e1().q()) {
            b2.setTimeInMillis(l.longValue());
            if (b2.get(1) == c) {
                xs3 = c1.e;
            }
        }
        xs3.a(bVar.a);
        bVar.a.setOnClickListener(a(c));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new b((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(pr3.mtrl_calendar_year, viewGroup, false));
    }

    @DexIgnore
    public final View.OnClickListener a(int i) {
        return new a(i);
    }
}
