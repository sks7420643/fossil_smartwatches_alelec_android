package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts1 extends zk1 {
    @DexIgnore
    public /* final */ ArrayList<lg1> U; // = new ArrayList<>();

    @DexIgnore
    public ts1(ri1 ri1, en0 en0) {
        super(ri1, en0, wm0.v0, gq0.b.a(ri1.u, pb1.UI_PACKAGE_FILE), oa7.b(w87.a(xf0.SKIP_ERASE, true), w87.a(xf0.SKIP_LIST, false), w87.a(xf0.ERASE_CACHE_FILE_BEFORE_GET, true)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 96);
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void a(ArrayList<bi1> arrayList) {
        byte[] bArr;
        bi1 bi1 = (bi1) ea7.e((List) arrayList);
        if (bi1 != null && (bArr = bi1.e) != null) {
            byte[] a = s97.a(bArr, 12, bArr.length - 4);
            int i = 0;
            while (i < a.length - 1) {
                int i2 = i + 2;
                int b = yz0.b(ByteBuffer.wrap(s97.a(a, i, i2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
                if (i >= (a.length - 1) - b) {
                    break;
                }
                cs1 a2 = cs1.e.a(a[i2]);
                byte[] a3 = s97.a(a, i2, i2 + b);
                if (a2 != null) {
                    try {
                        int i3 = vq1.a[a2.ordinal()];
                        if (i3 == 1) {
                            this.U.add(new xn0(a3));
                        } else if (i3 == 2) {
                            this.U.add(new ek0(a3));
                        }
                    } catch (Exception unused) {
                    }
                }
                i += b + 2;
            }
        }
        a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public Object d() {
        return this.U;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.L4;
        Object[] array = this.U.toArray(new lg1[0]);
        if (array != null) {
            return yz0.a(k, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    /* renamed from: d  reason: collision with other method in class */
    public ArrayList<lg1> m76d() {
        return this.U;
    }
}
