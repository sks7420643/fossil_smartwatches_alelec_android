package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ie3 extends ii3 implements ki3 {
    @DexIgnore
    public ie3(oh3 oh3) {
        super(oh3);
        a72.a(oh3);
    }

    @DexIgnore
    @Override // com.fossil.ii3
    public void a() {
        ((ii3) this).a.i();
    }

    @DexIgnore
    @Override // com.fossil.ii3
    public void d() {
        ((ii3) this).a.c().d();
    }

    @DexIgnore
    @Override // com.fossil.ii3
    public void g() {
        ((ii3) this).a.c().g();
    }

    @DexIgnore
    public void m() {
        ((ii3) this).a.j();
        throw null;
    }

    @DexIgnore
    public fb3 n() {
        return ((ii3) this).a.H();
    }

    @DexIgnore
    public ti3 o() {
        return ((ii3) this).a.u();
    }

    @DexIgnore
    public cg3 p() {
        return ((ii3) this).a.G();
    }

    @DexIgnore
    public ek3 q() {
        return ((ii3) this).a.E();
    }

    @DexIgnore
    public zj3 r() {
        return ((ii3) this).a.D();
    }

    @DexIgnore
    public fg3 s() {
        return ((ii3) this).a.x();
    }

    @DexIgnore
    public il3 t() {
        return ((ii3) this).a.r();
    }
}
