package com.fossil;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Bundle a;
        @DexIgnore
        public IconCompat b;
        @DexIgnore
        public /* final */ s6[] c;
        @DexIgnore
        public /* final */ s6[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ boolean h;
        @DexIgnore
        @Deprecated
        public int i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public PendingIntent k;

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2 != 0 ? IconCompat.a(null, "", i2) : null, charSequence, pendingIntent);
        }

        @DexIgnore
        public PendingIntent a() {
            return this.k;
        }

        @DexIgnore
        public boolean b() {
            return this.e;
        }

        @DexIgnore
        public s6[] c() {
            return this.d;
        }

        @DexIgnore
        public Bundle d() {
            return this.a;
        }

        @DexIgnore
        public IconCompat e() {
            int i2;
            if (this.b == null && (i2 = this.i) != 0) {
                this.b = IconCompat.a(null, "", i2);
            }
            return this.b;
        }

        @DexIgnore
        public s6[] f() {
            return this.c;
        }

        @DexIgnore
        public int g() {
            return this.g;
        }

        @DexIgnore
        public boolean h() {
            return this.f;
        }

        @DexIgnore
        public CharSequence i() {
            return this.j;
        }

        @DexIgnore
        public boolean j() {
            return this.h;
        }

        @DexIgnore
        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false);
        }

        @DexIgnore
        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, s6[] s6VarArr, s6[] s6VarArr2, boolean z, int i2, boolean z2, boolean z3) {
            this.f = true;
            this.b = iconCompat;
            if (iconCompat != null && iconCompat.c() == 2) {
                this.i = iconCompat.a();
            }
            this.j = e.d(charSequence);
            this.k = pendingIntent;
            this.a = bundle == null ? new Bundle() : bundle;
            this.c = s6VarArr;
            this.d = s6VarArr2;
            this.e = z;
            this.g = i2;
            this.f = z2;
            this.h = z3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f {
        @DexIgnore
        public CharSequence e;

        @DexIgnore
        public c a(CharSequence charSequence) {
            this.e = e.d(charSequence);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.o6.f
        public void a(n6 n6Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(n6Var.a()).setBigContentTitle(((f) this).b).bigText(this.e);
                if (((f) this).d) {
                    bigText.setSummaryText(((f) this).c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public static Notification.BubbleMetadata a(d dVar) {
            if (dVar == null) {
                return null;
            }
            new Notification.BubbleMetadata.Builder();
            dVar.a();
            throw null;
        }

        @DexIgnore
        public boolean a() {
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public String A;
        @DexIgnore
        public Bundle B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public Notification E;
        @DexIgnore
        public RemoteViews F;
        @DexIgnore
        public RemoteViews G;
        @DexIgnore
        public RemoteViews H;
        @DexIgnore
        public String I;
        @DexIgnore
        public int J;
        @DexIgnore
        public String K;
        @DexIgnore
        public long L;
        @DexIgnore
        public int M;
        @DexIgnore
        public boolean N;
        @DexIgnore
        public d O;
        @DexIgnore
        public Notification P;
        @DexIgnore
        public boolean Q;
        @DexIgnore
        @Deprecated
        public ArrayList<String> R;
        @DexIgnore
        public Context a;
        @DexIgnore
        public ArrayList<a> b;
        @DexIgnore
        public ArrayList<a> c;
        @DexIgnore
        public CharSequence d;
        @DexIgnore
        public CharSequence e;
        @DexIgnore
        public PendingIntent f;
        @DexIgnore
        public PendingIntent g;
        @DexIgnore
        public RemoteViews h;
        @DexIgnore
        public Bitmap i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public int k;
        @DexIgnore
        public int l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public f o;
        @DexIgnore
        public CharSequence p;
        @DexIgnore
        public CharSequence[] q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public String u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public String w;
        @DexIgnore
        public boolean x;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public boolean z;

        @DexIgnore
        public e(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            Notification notification = new Notification();
            this.P = notification;
            this.a = context;
            this.I = str;
            notification.when = System.currentTimeMillis();
            this.P.audioStreamType = -1;
            this.l = 0;
            this.R = new ArrayList<>();
            this.N = true;
        }

        @DexIgnore
        public e a(long j2) {
            this.P.when = j2;
            return this;
        }

        @DexIgnore
        public e b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        @DexIgnore
        public e c(CharSequence charSequence) {
            this.P.tickerText = d(charSequence);
            return this;
        }

        @DexIgnore
        public e d(boolean z2) {
            this.m = z2;
            return this;
        }

        @DexIgnore
        public e e(int i2) {
            this.l = i2;
            return this;
        }

        @DexIgnore
        public e f(int i2) {
            this.P.icon = i2;
            return this;
        }

        @DexIgnore
        public e g(int i2) {
            this.D = i2;
            return this;
        }

        @DexIgnore
        public e a(CharSequence charSequence) {
            this.e = d(charSequence);
            return this;
        }

        @DexIgnore
        public e b(PendingIntent pendingIntent) {
            this.P.deleteIntent = pendingIntent;
            return this;
        }

        @DexIgnore
        public e c(boolean z2) {
            a(2, z2);
            return this;
        }

        @DexIgnore
        public e d(int i2) {
            this.k = i2;
            return this;
        }

        @DexIgnore
        public static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        @DexIgnore
        public e a(PendingIntent pendingIntent) {
            this.f = pendingIntent;
            return this;
        }

        @DexIgnore
        public e b(Bitmap bitmap) {
            this.i = a(bitmap);
            return this;
        }

        @DexIgnore
        public e c(int i2) {
            Notification notification = this.P;
            notification.defaults = i2;
            if ((i2 & 4) != 0) {
                notification.flags |= 1;
            }
            return this;
        }

        @DexIgnore
        public final Bitmap a(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(d6.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(d6.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
        }

        @DexIgnore
        public e b(boolean z2) {
            this.x = z2;
            return this;
        }

        @DexIgnore
        public Bundle b() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        @DexIgnore
        public e b(int i2) {
            this.C = i2;
            return this;
        }

        @DexIgnore
        public e b(String str) {
            this.I = str;
            return this;
        }

        @DexIgnore
        public e a(Uri uri) {
            Notification notification = this.P;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        @DexIgnore
        @Deprecated
        public e(Context context) {
            this(context, null);
        }

        @DexIgnore
        public e a(long[] jArr) {
            this.P.vibrate = jArr;
            return this;
        }

        @DexIgnore
        public e a(int i2, int i3, int i4) {
            Notification notification = this.P;
            notification.ledARGB = i2;
            notification.ledOnMS = i3;
            notification.ledOffMS = i4;
            int i5 = (i3 == 0 || i4 == 0) ? 0 : 1;
            Notification notification2 = this.P;
            notification2.flags = i5 | (notification2.flags & -2);
            return this;
        }

        @DexIgnore
        public e a(boolean z2) {
            a(16, z2);
            return this;
        }

        @DexIgnore
        public e a(String str) {
            this.A = str;
            return this;
        }

        @DexIgnore
        public final void a(int i2, boolean z2) {
            if (z2) {
                Notification notification = this.P;
                notification.flags = i2 | notification.flags;
                return;
            }
            Notification notification2 = this.P;
            notification2.flags = (~i2) & notification2.flags;
        }

        @DexIgnore
        public e a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new a(i2, charSequence, pendingIntent));
            return this;
        }

        @DexIgnore
        public e a(a aVar) {
            this.b.add(aVar);
            return this;
        }

        @DexIgnore
        public e a(f fVar) {
            if (this.o != fVar) {
                this.o = fVar;
                if (fVar != null) {
                    fVar.a(this);
                }
            }
            return this;
        }

        @DexIgnore
        public e a(int i2) {
            this.J = i2;
            return this;
        }

        @DexIgnore
        public Notification a() {
            return new p6(this).b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {
        @DexIgnore
        public e a;
        @DexIgnore
        public CharSequence b;
        @DexIgnore
        public CharSequence c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public void a(Bundle bundle) {
        }

        @DexIgnore
        public abstract void a(n6 n6Var);

        @DexIgnore
        public void a(e eVar) {
            if (this.a != eVar) {
                this.a = eVar;
                if (eVar != null) {
                    eVar.a(this);
                }
            }
        }

        @DexIgnore
        public RemoteViews b(n6 n6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews c(n6 n6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews d(n6 n6Var) {
            return null;
        }
    }

    @DexIgnore
    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return q6.a(notification);
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f {
        @DexIgnore
        public Bitmap e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public b a(Bitmap bitmap) {
            this.f = bitmap;
            this.g = true;
            return this;
        }

        @DexIgnore
        public b b(Bitmap bitmap) {
            this.e = bitmap;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.o6.f
        public void a(n6 n6Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(n6Var.a()).setBigContentTitle(((f) this).b).bigPicture(this.e);
                if (this.g) {
                    bigPicture.bigLargeIcon(this.f);
                }
                if (((f) this).d) {
                    bigPicture.setSummaryText(((f) this).c);
                }
            }
        }
    }
}
