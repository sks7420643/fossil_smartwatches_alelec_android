package com.fossil;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gc2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gc2> CREATOR; // = new yc2();
    @DexIgnore
    public static /* final */ int[] i; // = new int[0];
    @DexIgnore
    public /* final */ DataType a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ hc2 d;
    @DexIgnore
    public /* final */ uc2 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int[] g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public DataType a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public String c;
        @DexIgnore
        public hc2 d;
        @DexIgnore
        public uc2 e;
        @DexIgnore
        public String f; // = "";
        @DexIgnore
        public int[] g;

        @DexIgnore
        public final a a(DataType dataType) {
            this.a = dataType;
            return this;
        }

        @DexIgnore
        @Deprecated
        public final a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        public final a a(int i) {
            this.b = i;
            return this;
        }

        @DexIgnore
        public final a a(hc2 hc2) {
            this.d = hc2;
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            this.e = uc2.b(str);
            return this;
        }

        @DexIgnore
        public final a a(Context context) {
            a(context.getPackageName());
            return this;
        }

        @DexIgnore
        public final gc2 a() {
            boolean z = true;
            a72.b(this.a != null, "Must set data type");
            if (this.b < 0) {
                z = false;
            }
            a72.b(z, "Must set data source type");
            return new gc2(this);
        }
    }

    @DexIgnore
    public gc2(DataType dataType, String str, int i2, hc2 hc2, uc2 uc2, String str2, int[] iArr) {
        this.a = dataType;
        this.c = i2;
        this.b = str;
        this.d = hc2;
        this.e = uc2;
        this.f = str2;
        this.h = D();
        this.g = iArr == null ? i : iArr;
    }

    @DexIgnore
    public final String A() {
        return this.c != 0 ? "derived" : OrmLiteConfigUtil.RAW_DIR_NAME;
    }

    @DexIgnore
    public final String B() {
        String str;
        String str2;
        int i2 = this.c;
        String str3 = i2 != 0 ? i2 != 1 ? "?" : "d" : "r";
        String x = this.a.x();
        uc2 uc2 = this.e;
        String str4 = "";
        if (uc2 == null) {
            str = str4;
        } else if (uc2.equals(uc2.b)) {
            str = ":gms";
        } else {
            String valueOf = String.valueOf(this.e.e());
            str = valueOf.length() != 0 ? ":".concat(valueOf) : new String(":");
        }
        hc2 hc2 = this.d;
        if (hc2 != null) {
            String g2 = hc2.g();
            String x2 = this.d.x();
            StringBuilder sb = new StringBuilder(String.valueOf(g2).length() + 2 + String.valueOf(x2).length());
            sb.append(":");
            sb.append(g2);
            sb.append(":");
            sb.append(x2);
            str2 = sb.toString();
        } else {
            str2 = str4;
        }
        String str5 = this.f;
        if (str5 != null) {
            String valueOf2 = String.valueOf(str5);
            str4 = valueOf2.length() != 0 ? ":".concat(valueOf2) : new String(":");
        }
        StringBuilder sb2 = new StringBuilder(str3.length() + 1 + String.valueOf(x).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str4).length());
        sb2.append(str3);
        sb2.append(":");
        sb2.append(x);
        sb2.append(str);
        sb2.append(str2);
        sb2.append(str4);
        return sb2.toString();
    }

    @DexIgnore
    public final uc2 C() {
        return this.e;
    }

    @DexIgnore
    public final String D() {
        StringBuilder sb = new StringBuilder();
        sb.append(A());
        sb.append(":");
        sb.append(this.a.g());
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e.e());
        }
        if (this.d != null) {
            sb.append(":");
            sb.append(this.d.v());
        }
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        return sb.toString();
    }

    @DexIgnore
    @Deprecated
    public int[] e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gc2)) {
            return false;
        }
        return this.h.equals(((gc2) obj).h);
    }

    @DexIgnore
    public DataType g() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("DataSource{");
        sb.append(A());
        if (this.b != null) {
            sb.append(":");
            sb.append(this.b);
        }
        if (this.e != null) {
            sb.append(":");
            sb.append(this.e);
        }
        if (this.d != null) {
            sb.append(":");
            sb.append(this.d);
        }
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        sb.append(":");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public hc2 v() {
        return this.d;
    }

    @DexIgnore
    @Deprecated
    public String w() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) g(), i2, false);
        k72.a(parcel, 2, w(), false);
        k72.a(parcel, 3, z());
        k72.a(parcel, 4, (Parcelable) v(), i2, false);
        k72.a(parcel, 5, (Parcelable) this.e, i2, false);
        k72.a(parcel, 6, y(), false);
        k72.a(parcel, 8, e(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public String x() {
        return this.h;
    }

    @DexIgnore
    public String y() {
        return this.f;
    }

    @DexIgnore
    public int z() {
        return this.c;
    }

    @DexIgnore
    public gc2(a aVar) {
        this.a = aVar.a;
        this.c = aVar.b;
        this.b = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.h = D();
        this.g = aVar.g;
    }
}
