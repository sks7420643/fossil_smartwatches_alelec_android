package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.sg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ld6 extends id6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public /* final */ MutableLiveData<Date> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ LiveData<qx6<List<MFSleepDay>>> j;
    @DexIgnore
    public /* final */ LiveData<qx6<List<MFSleepSession>>> k;
    @DexIgnore
    public /* final */ jd6 l;
    @DexIgnore
    public /* final */ SleepSummariesRepository m;
    @DexIgnore
    public /* final */ SleepSessionsRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ld6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSessions$1$1", f = "SleepOverviewDayPresenter.kt", l = {52, 52}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<MFSleepSession>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<MFSleepSession>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SleepSessionsRepository d = this.this$0.a.n;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = d.getSleepSessionList(date, date2, false, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public b(ld6 ld6) {
            this.a = ld6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<MFSleepSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSessions onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ld6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSummaries$1$1", f = "SleepOverviewDayPresenter.kt", l = {46, 46}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<MFSleepDay>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<MFSleepDay>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SleepSummariesRepository f = this.this$0.a.m;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = f.getSleepSummaries(date, date2, false, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(ld6 ld6) {
            this.a = ld6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<MFSleepDay>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSummaries onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1", f = "SleepOverviewDayPresenter.kt", l = {172}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ld6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends sg6.b>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(List list, fb7 fb7, d dVar) {
                super(2, fb7);
                this.$it = list;
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends sg6.b>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    ld6 ld6 = this.this$0.this$0;
                    Date b = ld6.f;
                    if (b != null) {
                        return ld6.a(b, this.$it);
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ld6 ld6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ld6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                qx6 qx6 = (qx6) this.this$0.k.a();
                if (qx6 == null || (list = (List) qx6.c()) == null) {
                    this.this$0.l.s(new ArrayList());
                    return i97.a;
                }
                ti7 a3 = this.this$0.b();
                a aVar = new a(list, null, this);
                this.L$0 = yi7;
                this.L$1 = list;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.l.s((List) obj);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<qx6<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ld6 a;

        @DexIgnore
        public e(ld6 ld6) {
            this.a = ld6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<MFSleepDay>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.h && this.a.i) {
                    ik7 unused = this.a.j();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<qx6<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ ld6 a;

        @DexIgnore
        public f(ld6 ld6) {
            this.a = ld6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<MFSleepSession>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSessions -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            sb.append('\"');
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                this.a.i = true;
                if (this.a.h && this.a.i) {
                    ik7 unused = this.a.j();
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ld6(jd6 jd6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        ee7.b(jd6, "mView");
        ee7.b(sleepSummariesRepository, "mSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSessionsRepository");
        ee7.b(portfolioApp, "mApp");
        this.l = jd6;
        this.m = sleepSummariesRepository;
        this.n = sleepSessionsRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<qx6<List<MFSleepDay>>> b2 = ge.b(mutableLiveData, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026 false) )\n        }\n    }");
        this.j = b2;
        LiveData<qx6<List<MFSleepSession>>> b3 = ge.b(this.g, new b(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026false)  )\n        }\n    }");
        this.k = b3;
    }

    @DexIgnore
    public final ik7 j() {
        return xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.f;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.h = false;
            this.i = false;
            Date date2 = new Date();
            this.f = date2;
            this.g.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewDayPresenter", "loadData - mDate=" + this.f);
        LiveData<qx6<List<MFSleepDay>>> liveData = this.j;
        jd6 jd6 = this.l;
        if (jd6 != null) {
            liveData.a((kd6) jd6, new e(this));
            this.k.a((LifecycleOwner) this.l, new f(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "stop");
        try {
            LiveData<qx6<List<MFSleepDay>>> liveData = this.j;
            jd6 jd6 = this.l;
            if (jd6 != null) {
                liveData.a((kd6) jd6);
                this.k.a((LifecycleOwner) this.l);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        this.l.a(this);
    }

    @DexIgnore
    public final List<sg6.b> a(Date date, List<MFSleepSession> list) {
        List<MFSleepDay> c2;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (zd5.a(Long.valueOf(date.getTime()), Long.valueOf(t.getDate()))) {
                arrayList.add(t);
            }
        }
        qx6<List<MFSleepDay>> a2 = this.j.a();
        MFSleepDay mFSleepDay = null;
        if (!(a2 == null || (c2 = a2.c()) == null)) {
            Iterator<T> it = c2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (zd5.d(date, next.getDate())) {
                    mFSleepDay = next;
                    break;
                }
            }
            mFSleepDay = mFSleepDay;
        }
        int a3 = ge5.c.a(mFSleepDay);
        ArrayList arrayList2 = new ArrayList();
        for (Iterator it2 = arrayList.iterator(); it2.hasNext(); it2 = it2) {
            MFSleepSession mFSleepSession = (MFSleepSession) it2.next();
            BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
            ArrayList arrayList3 = new ArrayList();
            SleepDistribution realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute();
            List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
            ArrayList arrayList4 = new ArrayList();
            int realStartTime = mFSleepSession.getRealStartTime();
            int totalMinuteBySleepDistribution = realSleepStateDistInMinute.getTotalMinuteBySleepDistribution();
            if (sleepStateChange != null) {
                for (T t2 : sleepStateChange) {
                    BarChart.b bVar = new BarChart.b(0, null, 0, 0, null, 31, null);
                    bVar.a((int) ((WrapperSleepStateChange) t2).index);
                    bVar.c(realStartTime);
                    bVar.b(totalMinuteBySleepDistribution);
                    int i2 = ((WrapperSleepStateChange) t2).state;
                    if (i2 == 0) {
                        bVar.a(BarChart.f.LOWEST);
                    } else if (i2 == 1) {
                        bVar.a(BarChart.f.DEFAULT);
                    } else if (i2 == 2) {
                        bVar.a(BarChart.f.HIGHEST);
                    }
                    arrayList4.add(bVar);
                    mFSleepSession = mFSleepSession;
                }
            }
            arrayList3.add(arrayList4);
            cVar.a().add(new BarChart.a(a3, arrayList3.size() != 0 ? arrayList3 : w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), 0, false, 12, null));
            cVar.b(a3);
            cVar.a(a3);
            int awake = realSleepStateDistInMinute.getAwake();
            int light = realSleepStateDistInMinute.getLight();
            int deep = realSleepStateDistInMinute.getDeep();
            if (totalMinuteBySleepDistribution > 0) {
                float f2 = (float) totalMinuteBySleepDistribution;
                float f3 = ((float) awake) / f2;
                float f4 = ((float) light) / f2;
                arrayList2.add(new sg6.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession.getTimezoneOffset()));
            }
        }
        return arrayList2;
    }
}
