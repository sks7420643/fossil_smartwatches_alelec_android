package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class r51 extends Enum<r51> {
    @DexIgnore
    public static /* final */ r51 A;
    @DexIgnore
    public static /* final */ r51 A0;
    @DexIgnore
    public static /* final */ r51 A1;
    @DexIgnore
    public static /* final */ r51 A2;
    @DexIgnore
    public static /* final */ r51 A3;
    @DexIgnore
    public static /* final */ r51 A4;
    @DexIgnore
    public static /* final */ r51 A5;
    @DexIgnore
    public static /* final */ r51 B;
    @DexIgnore
    public static /* final */ r51 B0;
    @DexIgnore
    public static /* final */ r51 B1;
    @DexIgnore
    public static /* final */ r51 B2;
    @DexIgnore
    public static /* final */ r51 B3;
    @DexIgnore
    public static /* final */ r51 B4;
    @DexIgnore
    public static /* final */ r51 B5;
    @DexIgnore
    public static /* final */ r51 C;
    @DexIgnore
    public static /* final */ r51 C0;
    @DexIgnore
    public static /* final */ r51 C1;
    @DexIgnore
    public static /* final */ r51 C2;
    @DexIgnore
    public static /* final */ r51 C3;
    @DexIgnore
    public static /* final */ r51 C4;
    @DexIgnore
    public static /* final */ r51 C5;
    @DexIgnore
    public static /* final */ r51 D;
    @DexIgnore
    public static /* final */ r51 D0;
    @DexIgnore
    public static /* final */ r51 D1;
    @DexIgnore
    public static /* final */ r51 D2;
    @DexIgnore
    public static /* final */ r51 D3;
    @DexIgnore
    public static /* final */ r51 D4;
    @DexIgnore
    public static /* final */ r51 D5;
    @DexIgnore
    public static /* final */ r51 E;
    @DexIgnore
    public static /* final */ r51 E0;
    @DexIgnore
    public static /* final */ r51 E1;
    @DexIgnore
    public static /* final */ r51 E2;
    @DexIgnore
    public static /* final */ r51 E3;
    @DexIgnore
    public static /* final */ r51 E4;
    @DexIgnore
    public static /* final */ r51 E5;
    @DexIgnore
    public static /* final */ r51 F;
    @DexIgnore
    public static /* final */ r51 F0;
    @DexIgnore
    public static /* final */ r51 F1;
    @DexIgnore
    public static /* final */ r51 F2;
    @DexIgnore
    public static /* final */ r51 F3;
    @DexIgnore
    public static /* final */ r51 F4;
    @DexIgnore
    public static /* final */ r51 F5;
    @DexIgnore
    public static /* final */ r51 G;
    @DexIgnore
    public static /* final */ r51 G0;
    @DexIgnore
    public static /* final */ r51 G1;
    @DexIgnore
    public static /* final */ r51 G2;
    @DexIgnore
    public static /* final */ r51 G3;
    @DexIgnore
    public static /* final */ r51 G4;
    @DexIgnore
    public static /* final */ r51 G5;
    @DexIgnore
    public static /* final */ r51 H;
    @DexIgnore
    public static /* final */ r51 H0;
    @DexIgnore
    public static /* final */ r51 H1;
    @DexIgnore
    public static /* final */ r51 H2;
    @DexIgnore
    public static /* final */ r51 H3;
    @DexIgnore
    public static /* final */ r51 H4;
    @DexIgnore
    public static /* final */ r51 H5;
    @DexIgnore
    public static /* final */ r51 I;
    @DexIgnore
    public static /* final */ r51 I0;
    @DexIgnore
    public static /* final */ r51 I1;
    @DexIgnore
    public static /* final */ r51 I2;
    @DexIgnore
    public static /* final */ r51 I3;
    @DexIgnore
    public static /* final */ r51 I4;
    @DexIgnore
    public static /* final */ r51 I5;
    @DexIgnore
    public static /* final */ r51 J;
    @DexIgnore
    public static /* final */ r51 J0;
    @DexIgnore
    public static /* final */ r51 J1;
    @DexIgnore
    public static /* final */ r51 J2;
    @DexIgnore
    public static /* final */ r51 J3;
    @DexIgnore
    public static /* final */ r51 J4;
    @DexIgnore
    public static /* final */ r51 J5;
    @DexIgnore
    public static /* final */ r51 K;
    @DexIgnore
    public static /* final */ r51 K0;
    @DexIgnore
    public static /* final */ r51 K1;
    @DexIgnore
    public static /* final */ r51 K2;
    @DexIgnore
    public static /* final */ r51 K3;
    @DexIgnore
    public static /* final */ r51 K4;
    @DexIgnore
    public static /* final */ r51 K5;
    @DexIgnore
    public static /* final */ r51 L;
    @DexIgnore
    public static /* final */ r51 L0;
    @DexIgnore
    public static /* final */ r51 L1;
    @DexIgnore
    public static /* final */ r51 L2;
    @DexIgnore
    public static /* final */ r51 L3;
    @DexIgnore
    public static /* final */ r51 L4;
    @DexIgnore
    public static /* final */ r51 L5;
    @DexIgnore
    public static /* final */ r51 M;
    @DexIgnore
    public static /* final */ r51 M0;
    @DexIgnore
    public static /* final */ r51 M1;
    @DexIgnore
    public static /* final */ r51 M2;
    @DexIgnore
    public static /* final */ r51 M3;
    @DexIgnore
    public static /* final */ r51 M4;
    @DexIgnore
    public static /* final */ r51 M5;
    @DexIgnore
    public static /* final */ r51 N;
    @DexIgnore
    public static /* final */ r51 N0;
    @DexIgnore
    public static /* final */ r51 N1;
    @DexIgnore
    public static /* final */ r51 N2;
    @DexIgnore
    public static /* final */ r51 N3;
    @DexIgnore
    public static /* final */ r51 N4;
    @DexIgnore
    public static /* final */ r51 N5;
    @DexIgnore
    public static /* final */ r51 O;
    @DexIgnore
    public static /* final */ r51 O0;
    @DexIgnore
    public static /* final */ r51 O1;
    @DexIgnore
    public static /* final */ r51 O2;
    @DexIgnore
    public static /* final */ r51 O3;
    @DexIgnore
    public static /* final */ r51 O4;
    @DexIgnore
    public static /* final */ r51 O5;
    @DexIgnore
    public static /* final */ r51 P;
    @DexIgnore
    public static /* final */ r51 P0;
    @DexIgnore
    public static /* final */ r51 P1;
    @DexIgnore
    public static /* final */ r51 P2;
    @DexIgnore
    public static /* final */ r51 P3;
    @DexIgnore
    public static /* final */ r51 P4;
    @DexIgnore
    public static /* final */ r51 P5;
    @DexIgnore
    public static /* final */ r51 Q;
    @DexIgnore
    public static /* final */ r51 Q0;
    @DexIgnore
    public static /* final */ r51 Q1;
    @DexIgnore
    public static /* final */ r51 Q2;
    @DexIgnore
    public static /* final */ r51 Q3;
    @DexIgnore
    public static /* final */ r51 Q4;
    @DexIgnore
    public static /* final */ r51 Q5;
    @DexIgnore
    public static /* final */ r51 R;
    @DexIgnore
    public static /* final */ r51 R0;
    @DexIgnore
    public static /* final */ r51 R1;
    @DexIgnore
    public static /* final */ r51 R2;
    @DexIgnore
    public static /* final */ r51 R3;
    @DexIgnore
    public static /* final */ r51 R4;
    @DexIgnore
    public static /* final */ r51 R5;
    @DexIgnore
    public static /* final */ r51 S;
    @DexIgnore
    public static /* final */ r51 S0;
    @DexIgnore
    public static /* final */ r51 S1;
    @DexIgnore
    public static /* final */ r51 S2;
    @DexIgnore
    public static /* final */ r51 S3;
    @DexIgnore
    public static /* final */ r51 S4;
    @DexIgnore
    public static /* final */ r51 S5;
    @DexIgnore
    public static /* final */ r51 T;
    @DexIgnore
    public static /* final */ r51 T0;
    @DexIgnore
    public static /* final */ r51 T1;
    @DexIgnore
    public static /* final */ r51 T2;
    @DexIgnore
    public static /* final */ r51 T3;
    @DexIgnore
    public static /* final */ r51 T4;
    @DexIgnore
    public static /* final */ /* synthetic */ r51[] T5;
    @DexIgnore
    public static /* final */ r51 U;
    @DexIgnore
    public static /* final */ r51 U0;
    @DexIgnore
    public static /* final */ r51 U1;
    @DexIgnore
    public static /* final */ r51 U2;
    @DexIgnore
    public static /* final */ r51 U3;
    @DexIgnore
    public static /* final */ r51 U4;
    @DexIgnore
    public static /* final */ r51 V;
    @DexIgnore
    public static /* final */ r51 V0;
    @DexIgnore
    public static /* final */ r51 V1;
    @DexIgnore
    public static /* final */ r51 V2;
    @DexIgnore
    public static /* final */ r51 V3;
    @DexIgnore
    public static /* final */ r51 V4;
    @DexIgnore
    public static /* final */ r51 W;
    @DexIgnore
    public static /* final */ r51 W0;
    @DexIgnore
    public static /* final */ r51 W1;
    @DexIgnore
    public static /* final */ r51 W2;
    @DexIgnore
    public static /* final */ r51 W3;
    @DexIgnore
    public static /* final */ r51 W4;
    @DexIgnore
    public static /* final */ r51 X;
    @DexIgnore
    public static /* final */ r51 X0;
    @DexIgnore
    public static /* final */ r51 X1;
    @DexIgnore
    public static /* final */ r51 X2;
    @DexIgnore
    public static /* final */ r51 X3;
    @DexIgnore
    public static /* final */ r51 X4;
    @DexIgnore
    public static /* final */ r51 Y;
    @DexIgnore
    public static /* final */ r51 Y0;
    @DexIgnore
    public static /* final */ r51 Y1;
    @DexIgnore
    public static /* final */ r51 Y2;
    @DexIgnore
    public static /* final */ r51 Y3;
    @DexIgnore
    public static /* final */ r51 Y4;
    @DexIgnore
    public static /* final */ r51 Z;
    @DexIgnore
    public static /* final */ r51 Z0;
    @DexIgnore
    public static /* final */ r51 Z1;
    @DexIgnore
    public static /* final */ r51 Z2;
    @DexIgnore
    public static /* final */ r51 Z3;
    @DexIgnore
    public static /* final */ r51 Z4;
    @DexIgnore
    public static /* final */ r51 a;
    @DexIgnore
    public static /* final */ r51 a0;
    @DexIgnore
    public static /* final */ r51 a1;
    @DexIgnore
    public static /* final */ r51 a2;
    @DexIgnore
    public static /* final */ r51 a3;
    @DexIgnore
    public static /* final */ r51 a4;
    @DexIgnore
    public static /* final */ r51 a5;
    @DexIgnore
    public static /* final */ r51 b;
    @DexIgnore
    public static /* final */ r51 b0;
    @DexIgnore
    public static /* final */ r51 b1;
    @DexIgnore
    public static /* final */ r51 b2;
    @DexIgnore
    public static /* final */ r51 b3;
    @DexIgnore
    public static /* final */ r51 b4;
    @DexIgnore
    public static /* final */ r51 b5;
    @DexIgnore
    public static /* final */ r51 c;
    @DexIgnore
    public static /* final */ r51 c0;
    @DexIgnore
    public static /* final */ r51 c1;
    @DexIgnore
    public static /* final */ r51 c2;
    @DexIgnore
    public static /* final */ r51 c3;
    @DexIgnore
    public static /* final */ r51 c4;
    @DexIgnore
    public static /* final */ r51 c5;
    @DexIgnore
    public static /* final */ r51 d;
    @DexIgnore
    public static /* final */ r51 d0;
    @DexIgnore
    public static /* final */ r51 d1;
    @DexIgnore
    public static /* final */ r51 d2;
    @DexIgnore
    public static /* final */ r51 d3;
    @DexIgnore
    public static /* final */ r51 d4;
    @DexIgnore
    public static /* final */ r51 d5;
    @DexIgnore
    public static /* final */ r51 e;
    @DexIgnore
    public static /* final */ r51 e0;
    @DexIgnore
    public static /* final */ r51 e1;
    @DexIgnore
    public static /* final */ r51 e2;
    @DexIgnore
    public static /* final */ r51 e3;
    @DexIgnore
    public static /* final */ r51 e4;
    @DexIgnore
    public static /* final */ r51 e5;
    @DexIgnore
    public static /* final */ r51 f;
    @DexIgnore
    public static /* final */ r51 f0;
    @DexIgnore
    public static /* final */ r51 f1;
    @DexIgnore
    public static /* final */ r51 f2;
    @DexIgnore
    public static /* final */ r51 f3;
    @DexIgnore
    public static /* final */ r51 f4;
    @DexIgnore
    public static /* final */ r51 f5;
    @DexIgnore
    public static /* final */ r51 g;
    @DexIgnore
    public static /* final */ r51 g0;
    @DexIgnore
    public static /* final */ r51 g1;
    @DexIgnore
    public static /* final */ r51 g2;
    @DexIgnore
    public static /* final */ r51 g3;
    @DexIgnore
    public static /* final */ r51 g4;
    @DexIgnore
    public static /* final */ r51 g5;
    @DexIgnore
    public static /* final */ r51 h;
    @DexIgnore
    public static /* final */ r51 h0;
    @DexIgnore
    public static /* final */ r51 h1;
    @DexIgnore
    public static /* final */ r51 h2;
    @DexIgnore
    public static /* final */ r51 h3;
    @DexIgnore
    public static /* final */ r51 h4;
    @DexIgnore
    public static /* final */ r51 h5;
    @DexIgnore
    public static /* final */ r51 i;
    @DexIgnore
    public static /* final */ r51 i0;
    @DexIgnore
    public static /* final */ r51 i1;
    @DexIgnore
    public static /* final */ r51 i2;
    @DexIgnore
    public static /* final */ r51 i3;
    @DexIgnore
    public static /* final */ r51 i4;
    @DexIgnore
    public static /* final */ r51 i5;
    @DexIgnore
    public static /* final */ r51 j;
    @DexIgnore
    public static /* final */ r51 j0;
    @DexIgnore
    public static /* final */ r51 j1;
    @DexIgnore
    public static /* final */ r51 j2;
    @DexIgnore
    public static /* final */ r51 j3;
    @DexIgnore
    public static /* final */ r51 j4;
    @DexIgnore
    public static /* final */ r51 j5;
    @DexIgnore
    public static /* final */ r51 k;
    @DexIgnore
    public static /* final */ r51 k0;
    @DexIgnore
    public static /* final */ r51 k1;
    @DexIgnore
    public static /* final */ r51 k2;
    @DexIgnore
    public static /* final */ r51 k3;
    @DexIgnore
    public static /* final */ r51 k4;
    @DexIgnore
    public static /* final */ r51 k5;
    @DexIgnore
    public static /* final */ r51 l;
    @DexIgnore
    public static /* final */ r51 l0;
    @DexIgnore
    public static /* final */ r51 l1;
    @DexIgnore
    public static /* final */ r51 l2;
    @DexIgnore
    public static /* final */ r51 l3;
    @DexIgnore
    public static /* final */ r51 l4;
    @DexIgnore
    public static /* final */ r51 l5;
    @DexIgnore
    public static /* final */ r51 m;
    @DexIgnore
    public static /* final */ r51 m0;
    @DexIgnore
    public static /* final */ r51 m1;
    @DexIgnore
    public static /* final */ r51 m2;
    @DexIgnore
    public static /* final */ r51 m3;
    @DexIgnore
    public static /* final */ r51 m4;
    @DexIgnore
    public static /* final */ r51 m5;
    @DexIgnore
    public static /* final */ r51 n;
    @DexIgnore
    public static /* final */ r51 n0;
    @DexIgnore
    public static /* final */ r51 n1;
    @DexIgnore
    public static /* final */ r51 n2;
    @DexIgnore
    public static /* final */ r51 n3;
    @DexIgnore
    public static /* final */ r51 n4;
    @DexIgnore
    public static /* final */ r51 n5;
    @DexIgnore
    public static /* final */ r51 o;
    @DexIgnore
    public static /* final */ r51 o0;
    @DexIgnore
    public static /* final */ r51 o1;
    @DexIgnore
    public static /* final */ r51 o2;
    @DexIgnore
    public static /* final */ r51 o3;
    @DexIgnore
    public static /* final */ r51 o4;
    @DexIgnore
    public static /* final */ r51 o5;
    @DexIgnore
    public static /* final */ r51 p;
    @DexIgnore
    public static /* final */ r51 p0;
    @DexIgnore
    public static /* final */ r51 p1;
    @DexIgnore
    public static /* final */ r51 p2;
    @DexIgnore
    public static /* final */ r51 p3;
    @DexIgnore
    public static /* final */ r51 p4;
    @DexIgnore
    public static /* final */ r51 p5;
    @DexIgnore
    public static /* final */ r51 q;
    @DexIgnore
    public static /* final */ r51 q0;
    @DexIgnore
    public static /* final */ r51 q1;
    @DexIgnore
    public static /* final */ r51 q2;
    @DexIgnore
    public static /* final */ r51 q3;
    @DexIgnore
    public static /* final */ r51 q4;
    @DexIgnore
    public static /* final */ r51 q5;
    @DexIgnore
    public static /* final */ r51 r;
    @DexIgnore
    public static /* final */ r51 r0;
    @DexIgnore
    public static /* final */ r51 r1;
    @DexIgnore
    public static /* final */ r51 r2;
    @DexIgnore
    public static /* final */ r51 r3;
    @DexIgnore
    public static /* final */ r51 r4;
    @DexIgnore
    public static /* final */ r51 r5;
    @DexIgnore
    public static /* final */ r51 s;
    @DexIgnore
    public static /* final */ r51 s0;
    @DexIgnore
    public static /* final */ r51 s1;
    @DexIgnore
    public static /* final */ r51 s2;
    @DexIgnore
    public static /* final */ r51 s3;
    @DexIgnore
    public static /* final */ r51 s4;
    @DexIgnore
    public static /* final */ r51 s5;
    @DexIgnore
    public static /* final */ r51 t;
    @DexIgnore
    public static /* final */ r51 t0;
    @DexIgnore
    public static /* final */ r51 t1;
    @DexIgnore
    public static /* final */ r51 t2;
    @DexIgnore
    public static /* final */ r51 t3;
    @DexIgnore
    public static /* final */ r51 t4;
    @DexIgnore
    public static /* final */ r51 t5;
    @DexIgnore
    public static /* final */ r51 u;
    @DexIgnore
    public static /* final */ r51 u0;
    @DexIgnore
    public static /* final */ r51 u1;
    @DexIgnore
    public static /* final */ r51 u2;
    @DexIgnore
    public static /* final */ r51 u3;
    @DexIgnore
    public static /* final */ r51 u4;
    @DexIgnore
    public static /* final */ r51 u5;
    @DexIgnore
    public static /* final */ r51 v;
    @DexIgnore
    public static /* final */ r51 v0;
    @DexIgnore
    public static /* final */ r51 v1;
    @DexIgnore
    public static /* final */ r51 v2;
    @DexIgnore
    public static /* final */ r51 v3;
    @DexIgnore
    public static /* final */ r51 v4;
    @DexIgnore
    public static /* final */ r51 v5;
    @DexIgnore
    public static /* final */ r51 w;
    @DexIgnore
    public static /* final */ r51 w0;
    @DexIgnore
    public static /* final */ r51 w1;
    @DexIgnore
    public static /* final */ r51 w2;
    @DexIgnore
    public static /* final */ r51 w3;
    @DexIgnore
    public static /* final */ r51 w4;
    @DexIgnore
    public static /* final */ r51 w5;
    @DexIgnore
    public static /* final */ r51 x;
    @DexIgnore
    public static /* final */ r51 x0;
    @DexIgnore
    public static /* final */ r51 x1;
    @DexIgnore
    public static /* final */ r51 x2;
    @DexIgnore
    public static /* final */ r51 x3;
    @DexIgnore
    public static /* final */ r51 x4;
    @DexIgnore
    public static /* final */ r51 x5;
    @DexIgnore
    public static /* final */ r51 y;
    @DexIgnore
    public static /* final */ r51 y0;
    @DexIgnore
    public static /* final */ r51 y1;
    @DexIgnore
    public static /* final */ r51 y2;
    @DexIgnore
    public static /* final */ r51 y3;
    @DexIgnore
    public static /* final */ r51 y4;
    @DexIgnore
    public static /* final */ r51 y5;
    @DexIgnore
    public static /* final */ r51 z;
    @DexIgnore
    public static /* final */ r51 z0;
    @DexIgnore
    public static /* final */ r51 z1;
    @DexIgnore
    public static /* final */ r51 z2;
    @DexIgnore
    public static /* final */ r51 z3;
    @DexIgnore
    public static /* final */ r51 z4;
    @DexIgnore
    public static /* final */ r51 z5;

    /*
    static {
        r51 r51 = new r51("DEVICE_INFO", 0);
        a = r51;
        r51 r512 = new r51("RSSI", 1);
        b = r512;
        r51 r513 = new r51("NOTIFICATION", 2);
        c = r513;
        r51 r514 = new r51("TYPE", 3);
        d = r514;
        r51 r515 = new r51("UID", 4);
        e = r515;
        r51 r516 = new r51("APP_NAME", 5);
        f = r516;
        r51 r517 = new r51(ShareConstants.TITLE, 6);
        g = r517;
        r51 r518 = new r51("SENDER", 7);
        h = r518;
        r51 r519 = new r51("SENDER_ID", 8);
        i = r519;
        r51 r5110 = new r51("MESSAGE", 9);
        j = r5110;
        r51 r5111 = new r51("FLAGS", 10);
        k = r5111;
        r51 r5112 = new r51("RECEIVED_TIMESTAMP_IN_SECOND", 11);
        l = r5112;
        r51 r5113 = new r51("REPLY_MESSAGES", 12);
        m = r5113;
        r51 r5114 = new r51("REQUEST", 14);
        n = r5114;
        r51 r5115 = new r51("RESPONSE", 15);
        o = r5115;
        r51 r5116 = new r51("REQUEST_ID", 16);
        p = r5116;
        r51 r5117 = new r51("CHANCE_OF_RAIN", 18);
        q = r5117;
        r51 r5118 = new r51("DESTINATION", 19);
        r = r5118;
        r51 r5119 = new r51("WEATHER_CONDITION", 21);
        s = r5119;
        r51 r5120 = new r51("TEMP_UNIT", 22);
        t = r5120;
        r51 r5121 = new r51("TRACK_INFO", 24);
        u = r5121;
        r51 r5122 = new r51("VOLUME", 25);
        v = r5122;
        r51 r5123 = new r51("ARTIST", 26);
        w = r5123;
        r51 r5124 = new r51("ALBUM", 27);
        x = r5124;
        r51 r5125 = new r51("ALARMS", 28);
        y = r5125;
        r51 r5126 = new r51("HOUR", 29);
        z = r5126;
        r51 r5127 = new r51("MINUTE", 30);
        A = r5127;
        r51 r5128 = new r51("DAYS", 31);
        B = r5128;
        r51 r5129 = new r51("REPEAT", 32);
        C = r5129;
        r51 r5130 = new r51("ALLOW_SNOOZE", 33);
        D = r5130;
        r51 r5131 = new r51("ENABLE", 34);
        E = r5131;
        r51 r5132 = new r51("BACKGROUND_IMAGE_CONFIG", 35);
        F = r5132;
        r51 r5133 = new r51("NAME", 46);
        G = r5133;
        r51 r5134 = new r51("FILE_SIZE", 47);
        H = r5134;
        r51 r5135 = new r51("FILE_CRC", 48);
        I = r5135;
        r51 r5136 = new r51("LOCATION", 50);
        J = r5136;
        r51 r5137 = new r51("INTERVAL", 53);
        K = r5137;
        r51 r5138 = new r51("MAX_INTERVAL", 54);
        L = r5138;
        r51 r5139 = new r51("MIN_INTERVAL", 55);
        M = r5139;
        r51 r5140 = new r51("LATENCY", 56);
        N = r5140;
        r51 r5141 = new r51("TIMEOUT", 57);
        O = r5141;
        r51 r5142 = new r51("PRIORITY", 58);
        P = r5142;
        r51 r5143 = new r51("CONFIGS", 59);
        Q = r5143;
        r51 r5144 = new r51("AFFECTED_CONFIGS", 60);
        R = r5144;
        r51 r5145 = new r51("GROUP_ID", 61);
        S = r5145;
        r51 r5146 = new r51("NOTIFICATION_FILTERS", 62);
        T = r5146;
        r51 r5147 = new r51("FITNESS_DATA", 64);
        U = r5147;
        r51 r5148 = new r51("START_TIME", 65);
        V = r5148;
        r51 r5149 = new r51("END_TIME", 66);
        W = r5149;
        r51 r5150 = new r51("TIMEZONE_OFFSET_IN_SECOND", 67);
        X = r5150;
        r51 r5151 = new r51("TOTAL_DISTANCE", 68);
        Y = r5151;
        r51 r5152 = new r51("TOTAL_CALORIES", 69);
        Z = r5152;
        r51 r5153 = new r51("TOTAL_STEPS", 70);
        a0 = r5153;
        r51 r5154 = new r51("TOTAL_ACTIVE_MINUTES", 71);
        b0 = r5154;
        r51 r5155 = new r51("SLEEP_SESSION", 72);
        c0 = r5155;
        r51 r5156 = new r51("WORKOUT_SESSION", 73);
        d0 = r5156;
        r51 r5157 = new r51("HEART_RATE_RECORD", 74);
        e0 = r5157;
        r51 r5158 = new r51("RESTING", 75);
        f0 = r5158;
        r51 r5159 = new r51("GOAL_TRACKING", 76);
        g0 = r5159;
        r51 r5160 = new r51("DATA_TYPE", 79);
        h0 = r5160;
        r51 r5161 = new r51("MAC_ADDRESS", 80);
        i0 = r5161;
        r51 r5162 = new r51("SERIAL_NUMBER", 81);
        j0 = r5162;
        r51 r5163 = new r51("HARDWARE_REVISION", 82);
        k0 = r5163;
        r51 r5164 = new r51("FIRMWARE_VERSION", 83);
        l0 = r5164;
        r51 r5165 = new r51("MODEL_NUMBER", 84);
        m0 = r5165;
        r51 r5166 = new r51("HEART_RATE_SERIAL_NUMBER", 85);
        n0 = r5166;
        r51 r5167 = new r51("BOOT_LOADER_VERSION", 86);
        o0 = r5167;
        r51 r5168 = new r51("WATCH_APP_VERSION", 87);
        p0 = r5168;
        r51 r5169 = new r51("FONT_VERSION", 88);
        q0 = r5169;
        r51 r5170 = new r51("SUPPORTED_DEVICE_CONFIG", 90);
        r0 = r5170;
        r51 r5171 = new r51("MUSIC_EVENT", 91);
        s0 = r5171;
        r51 r5172 = new r51(ShareConstants.ACTION, 92);
        t0 = r5172;
        r51 r5173 = new r51("STATUS", 93);
        u0 = r5173;
        r51 r5174 = new r51("SKIP_RESUME", 94);
        v0 = r5174;
        r51 r5175 = new r51("KEY", 95);
        w0 = r5175;
        r51 r5176 = new r51("VALUE", 96);
        x0 = r5176;
        r51 r5177 = new r51("FILE_HANDLE", 97);
        y0 = r5177;
        r51 r5178 = new r51("NEW_STATE", 98);
        z0 = r5178;
        r51 r5179 = new r51("PREV_STATE", 99);
        A0 = r5179;
        r51 r5180 = new r51("NEW_BOND_STATE", 100);
        B0 = r5180;
        r51 r5181 = new r51("RESPONSES", 101);
        C0 = r5181;
        r51 r5182 = new r51("DATA_SIZE", 102);
        D0 = r5182;
        r51 r5183 = new r51("SOCKET_ID", 103);
        E0 = r5183;
        r51 r5184 = new r51("PROCESSED_DATA_LENGTH", 104);
        F0 = r5184;
        r51 r5185 = new r51("WRITTEN_SIZE", 106);
        G0 = r5185;
        r51 r5186 = new r51("WRITTEN_DATA_CRC", 107);
        H0 = r5186;
        r51 r5187 = new r51("OPTIMAL_PAYLOAD", 108);
        I0 = r5187;
        r51 r5188 = new r51("VERIFIED_DATA_OFFSET", 109);
        J0 = r5188;
        r51 r5189 = new r51("VERIFIED_DATA_LENGTH", 110);
        K0 = r5189;
        r51 r5190 = new r51("VERIFIED_DATA_CRC", 111);
        L0 = r5190;
        r51 r5191 = new r51("RESULT_CODE", 112);
        M0 = r5191;
        r51 r5192 = new r51("ERROR_DETAIL", 113);
        N0 = r5192;
        r51 r5193 = new r51("TIMESTAMP", 114);
        O0 = r5193;
        r51 r5194 = new r51("CHANNEL_ID", 115);
        P0 = r5194;
        r51 r5195 = new r51("RAW_DATA", 116);
        Q0 = r5195;
        r51 r5196 = new r51("RAW_DATA_LENGTH", 117);
        R0 = r5196;
        r51 r5197 = new r51("DATA_CRC", 118);
        S0 = r5197;
        r51 r5198 = new r51("PACKAGE_COUNT", 119);
        T0 = r5198;
        r51 r5199 = new r51("MOVING_TYPE", 120);
        U0 = r5199;
        r51 r51100 = new r51("HAND_ID", 121);
        V0 = r51100;
        r51 r51101 = new r51("DEGREE", 122);
        W0 = r51101;
        r51 r51102 = new r51("DIRECTION", 123);
        X0 = r51102;
        r51 r51103 = new r51("SPEED", 124);
        Y0 = r51103;
        r51 r51104 = new r51("HAND_CONFIGS", 125);
        Z0 = r51104;
        r51 r51105 = new r51("OFFSET", 126);
        a1 = r51105;
        r51 r51106 = new r51("LENGTH", 127);
        b1 = r51106;
        r51 r51107 = new r51("TOTAL_LENGTH", 128);
        c1 = r51107;
        r51 r51108 = new r51("AUTO_CONNECT", 129);
        d1 = r51108;
        r51 r51109 = new r51("SERVICES", 130);
        e1 = r51109;
        r51 r51110 = new r51("CHARACTERISTICS", 131);
        f1 = r51110;
        r51 r51111 = new r51("PROPOSED_TIMEOUT", 132);
        g1 = r51111;
        r51 r51112 = new r51("REQUESTED_MTU", 133);
        h1 = r51112;
        r51 r51113 = new r51("EXCHANGED_MTU", 134);
        i1 = r51113;
        r51 r51114 = new r51("DEVICE_EVENT", 135);
        j1 = r51114;
        r51 r51115 = new r51("DEVICE", 136);
        k1 = r51115;
        r51 r51116 = new r51(cr6.v, 137);
        l1 = r51116;
        r51 r51117 = new r51("CURRENT_BOND_STATE", 138);
        m1 = r51117;
        r51 r51118 = new r51("CURRENT_HID_STATE", 139);
        n1 = r51118;
        r51 r51119 = new r51("NEW_HID_STATE", ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        o1 = r51119;
        r51 r51120 = new r51("IS_CACHED", 141);
        p1 = r51120;
        r51 r51121 = new r51("PERIPHERAL_CURRENT_STATE", 142);
        q1 = r51121;
        r51 r51122 = new r51("STARTED_AT", 143);
        r1 = r51122;
        r51 r51123 = new r51("COMPLETED_AT", 144);
        s1 = r51123;
        r51 r51124 = new r51("REQUEST_TIMEOUT_IN_MS", 145);
        t1 = r51124;
        r51 r51125 = new r51("FILE_LIST", 146);
        u1 = r51125;
        r51 r51126 = new r51("MESSAGE_LENGTH", 147);
        v1 = r51126;
        r51 r51127 = new r51("MESSAGE_CRC", 148);
        w1 = r51127;
        r51 r51128 = new r51("SEQUENCE", 149);
        x1 = r51128;
        r51 r51129 = new r51("HEARTBEAT_INTERVAL_IN_MS", 153);
        y1 = r51129;
        r51 r51130 = new r51("PHASE_RESULT", 155);
        z1 = r51130;
        r51 r51131 = new r51("CREATED_AT", 156);
        A1 = r51131;
        r51 r51132 = new r51("GATT_CONNECTED_DEVICES", 157);
        B1 = r51132;
        r51 r51133 = new r51("HID_CONNECTED_DEVICES", 158);
        C1 = r51133;
        r51 r51134 = new r51("SCAN_CALLBACK", 159);
        D1 = r51134;
        r51 r51135 = new r51("SCAN_ERROR", 160);
        E1 = r51135;
        r51 r51136 = new r51("FILTER_TYPE", 161);
        F1 = r51136;
        r51 r51137 = new r51("SERIAL_NUMBER_PREFIXES", 162);
        G1 = r51137;
        r51 r51138 = new r51("DEVICE_TYPES", 163);
        H1 = r51138;
        r51 r51139 = new r51("SERIAL_NUMBER_REGEX", 164);
        I1 = r51139;
        r51 r51140 = new r51("SCAN_FILTER", 165);
        J1 = r51140;
        r51 r51141 = new r51("DEVICE_FILTER", 166);
        K1 = r51141;
        r51 r51142 = new r51("FRAMES", 170);
        L1 = r51142;
        r51 r51143 = new r51("TOTAL_FRAMES", 171);
        M1 = r51143;
        r51 r51144 = new r51("TOTAL_SLEEP_IN_MINUTE", 172);
        N1 = r51144;
        r51 r51145 = new r51("AWAKE_IN_MINUTE", 173);
        O1 = r51145;
        r51 r51146 = new r51("LIGHT_SLEEP_IN_MINUTE", 174);
        P1 = r51146;
        r51 r51147 = new r51("DEEP_SLEEP_IN_MINUTE", 175);
        Q1 = r51147;
        r51 r51148 = new r51("TEMPERATURE", 176);
        R1 = r51148;
        r51 r51149 = new r51("CALORIES", 177);
        S1 = r51149;
        r51 r51150 = new r51("DISTANCE", 178);
        T1 = r51150;
        r51 r51151 = new r51("TIME", 179);
        U1 = r51151;
        r51 r51152 = new r51("DATE", 180);
        V1 = r51152;
        r51 r51153 = new r51("CURRENT_TEMPERATURE", 181);
        W1 = r51153;
        r51 r51154 = new r51("HIGH_TEMPERATURE", 182);
        X1 = r51154;
        r51 r51155 = new r51("LOW_TEMPERATURE", 183);
        Y1 = r51155;
        r51 r51156 = new r51("WEEKDAY", 184);
        Z1 = r51156;
        r51 r51157 = new r51("CURRENT_WEATHER_INFO", 185);
        a2 = r51157;
        r51 r51158 = new r51("HOURLY_FORECAST", 186);
        b2 = r51158;
        r51 r51159 = new r51("DAILY_FORECAST", 187);
        c2 = r51159;
        r51 r51160 = new r51("WEATHER_CONFIGS", 188);
        d2 = r51160;
        r51 r51161 = new r51("REQUEST_UUID", 191);
        e2 = r51161;
        r51 r51162 = new r51("CORRECT_OFFSET", 192);
        f2 = r51162;
        r51 r51163 = new r51("DEVICE_FILE", 193);
        g2 = r51163;
        r51 r51164 = new r51("FILE_VERSION", 194);
        h2 = r51164;
        r51 r51165 = new r51("CONNECT_DURATION", 195);
        i2 = r51165;
        r51 r51166 = new r51("CONNECT_COUNT", 196);
        j2 = r51166;
        r51 r51167 = new r51("DISCONNECT_COUNT", 197);
        k2 = r51167;
        r51 r51168 = new r51("MSL_STATUS_CODE", 198);
        l2 = r51168;
        r51 r51169 = new r51("PHONE_RANDOM_NUMBER", Action.Music.MUSIC_END_ACTION);
        m2 = r51169;
        r51 r51170 = new r51("DEVICE_RANDOM_NUMBER", 200);
        n2 = r51170;
        r51 r51171 = new r51("BOTH_SIDES_RANDOM_NUMBERS", 201);
        o2 = r51171;
        r51 r51172 = new r51("PHONE_PUBLIC_KEY", Action.Selfie.TAKE_BURST);
        p2 = r51172;
        r51 r51173 = new r51("DEVICE_PUBLIC_KEY", 203);
        q2 = r51173;
        r51 r51174 = new r51("SECRET_KEY_CRC", 204);
        r2 = r51174;
        r51 r51175 = new r51("AUTHENTICATION_KEY_TYPE", 206);
        s2 = r51175;
        r51 r51176 = new r51("DEVICE_SECURITY_VERSION", 207);
        t2 = r51176;
        r51 r51177 = new r51("TIMEZONE_OFFSET_IN_MINUTE", 208);
        u2 = r51177;
        r51 r51178 = new r51("HOUR_DEGREE", 209);
        v2 = r51178;
        r51 r51179 = new r51("MINUTE_DEGREE", 210);
        w2 = r51179;
        r51 r51180 = new r51("SUBEYE_DEGREE", 211);
        x2 = r51180;
        r51 r51181 = new r51("DURATION_IN_MS", 212);
        y2 = r51181;
        r51 r51182 = new r51("HAND_MOVING_CONFIG", 213);
        z2 = r51182;
        r51 r51183 = new r51("VIBE_PATTERN", 214);
        A2 = r51183;
        r51 r51184 = new r51("CHARACTERISTIC", 215);
        B2 = r51184;
        r51 r51185 = new r51("COMMAND_ID", 220);
        C2 = r51185;
        r51 r51186 = new r51("GATT_RESULT", 221);
        D2 = r51186;
        r51 r51187 = new r51("SECOND", 224);
        E2 = r51187;
        r51 r51188 = new r51("MILLISECOND", 225);
        F2 = r51188;
        r51 r51189 = new r51("SUPPORTED_FILES_VERSION", 226);
        G2 = r51189;
        r51 r51190 = new r51("FILE_TYPE", 227);
        H2 = r51190;
        r51 r51191 = new r51("VERSION", 228);
        I2 = r51191;
        r51 r51192 = new r51("DEVICE_TYPE", 229);
        J2 = r51192;
        r51 r51193 = new r51("EXPIRED_TIMESTAMP_IN_SECOND", 230);
        K2 = r51193;
        r51 r51194 = new r51("IS_COMPLETED", 231);
        L2 = r51194;
        r51 r51195 = new r51("EVENT_ID", 232);
        M2 = r51195;
        r51 r51196 = new r51("FILES", 233);
        N2 = r51196;
        r51 r51197 = new r51("SEGMENT_OFFSET", 235);
        O2 = r51197;
        r51 r51198 = new r51("SEGMENT_LENGTH", 236);
        P2 = r51198;
        r51 r51199 = new r51("TOTAL_FILE_LENGTH", 237);
        Q2 = r51199;
        r51 r51200 = new r51("SEGMENT_CRC", 238);
        R2 = r51200;
        r51 r51201 = new r51("PAGE_OFFSET", 239);
        S2 = r51201;
        r51 r51202 = new r51("NEW_SIZE_WRITTEN", 240);
        T2 = r51202;
        r51 r51203 = new r51("APP_BUNDLE_CRC", 242);
        U2 = r51203;
        r51 r51204 = new r51("APP_PACKAGE_NAME", 243);
        V2 = r51204;
        r51 r51205 = new r51("DATA", 244);
        W2 = r51205;
        r51 r51206 = new r51("DEVICE_DATA", 245);
        X2 = r51206;
        r51 r51207 = new r51("DEFAULT_ICON", 246);
        Y2 = r51207;
        r51 r51208 = new r51("ICON_CONFIG", 247);
        Z2 = r51208;
        r51 r51209 = new r51("NOTIFICATION_ICONS", 248);
        a3 = r51209;
        r51 r51210 = new r51("REQUEST_DATA", 249);
        b3 = r51210;
        r51 r51211 = new r51("LANGUAGE_CODE", 250);
        c3 = r51211;
        r51 r51212 = new r51("FILE_CRC_C", 251);
        d3 = r51212;
        r51 r51213 = new r51("PRESET", 252);
        e3 = r51213;
        r51 r51214 = new r51("NUMBER_OF_FILES", 253);
        f3 = r51214;
        r51 r51215 = new r51("TOTAL_FILE_SIZE", 254);
        g3 = r51215;
        r51 r51216 = new r51("SKIP_READ_ACTIVITY_FILES", 255);
        h3 = r51216;
        r51 r51217 = new r51("SKIP_ERASE_ACTIVITY_FILES", 256);
        i3 = r51217;
        r51 r51218 = new r51("RESPONSE_STATUS", 257);
        j3 = r51218;
        r51 r51219 = new r51("LOCALE", 258);
        k3 = r51219;
        r51 r51220 = new r51("LOCALIZATION_FILE", 259);
        l3 = r51220;
        r51 r51221 = new r51("ACTUAL_WRITTEN_SIZE", 260);
        m3 = r51221;
        r51 r51222 = new r51("HEADER_LENGTH", 261);
        n3 = r51222;
        r51 r51223 = new r51("TRANSFERRED_DATA_SIZE", 262);
        o3 = r51223;
        r51 r51224 = new r51("SIZE_WRITTEN", 263);
        p3 = r51224;
        r51 r51225 = new r51("BOND_REQUIRED", 264);
        q3 = r51225;
        r51 r51226 = new r51("MICRO_APP_VERSION", 266);
        r3 = r51226;
        r51 r51227 = new r51("FAST_PAIR_ID_HEX_STRING", 267);
        s3 = r51227;
        r51 r51228 = new r51("MICRO_APP_EVENT", 268);
        t3 = r51228;
        r51 r51229 = new r51("SHIP_HANDS_TO_TWELVE", 269);
        u3 = r51229;
        r51 r51230 = new r51("TRAVEL_TIME_IN_MINUTE", 270);
        v3 = r51230;
        r51 r51231 = new r51("CURRENT_HEART_RATE", 271);
        w3 = r51231;
        r51 r51232 = new r51("WATCH_PARAMETERS_FILE", 272);
        x3 = r51232;
        r51 r51233 = new r51("BACKGROUND_IMAGES", 273);
        y3 = r51233;
        r51 r51234 = new r51("IS_VALID_SECRET_KEY", 274);
        z3 = r51234;
        r51 r51235 = new r51("BUTTON", 275);
        A3 = r51235;
        r51 r51236 = new r51("DECLARATIONS", 276);
        B3 = r51236;
        r51 r51237 = new r51("TOTAL_DECLARATIONS", 277);
        C3 = r51237;
        r51 r51238 = new r51("MICRO_APP_ID", 279);
        D3 = r51238;
        r51 r51239 = new r51("MINOR_VERSION", 280);
        E3 = r51239;
        r51 r51240 = new r51("MAJOR_VERSION", 281);
        F3 = r51240;
        r51 r51241 = new r51("VARIATION_NUMBER", 282);
        G3 = r51241;
        r51 r51242 = new r51("VARIANT", 283);
        H3 = r51242;
        r51 r51243 = new r51("RUN_TIME", 284);
        I3 = r51243;
        r51 r51244 = new r51("HAS_CUSTOMIZATION", 285);
        J3 = r51244;
        r51 r51245 = new r51("CRC", 286);
        K3 = r51245;
        r51 r51246 = new r51("GOAL_ID", 287);
        L3 = r51246;
        r51 r51247 = new r51("CONTEXT_NUMBER", 288);
        M3 = r51247;
        r51 r51248 = new r51("ACTIVITY_ID", 289);
        N3 = r51248;
        r51 r51249 = new r51("INSTRUCTION_ID", 290);
        O3 = r51249;
        r51 r51250 = new r51("ROTATION", 291);
        P3 = r51250;
        r51 r51251 = new r51("ANIMATIONS", 292);
        Q3 = r51251;
        r51 r51252 = new r51("IS_RESUME", 293);
        R3 = r51252;
        r51 r51253 = new r51("TIMES", 294);
        S3 = r51253;
        r51 r51254 = new r51("STREAM_DATA", 295);
        T3 = r51254;
        r51 r51255 = new r51("DELAY_IN_SECOND", 296);
        U3 = r51255;
        r51 r51256 = new r51("HID_CODE", 297);
        V3 = r51256;
        r51 r51257 = new r51("IMMEDIATE_RELEASE", 298);
        W3 = r51257;
        r51 r51258 = new r51("SYSTEM_VERSION", Action.Selfie.SELFIE_END_ACTION);
        X3 = r51258;
        r51 r51259 = new r51("MICRO_APP_MAPPINGS", SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
        Y3 = r51259;
        r51 r51260 = new r51("TOTAL_MAPPINGS", Action.Presenter.NEXT);
        Z3 = r51260;
        r51 r51261 = new r51("MICRO_APP_ERROR_TYPE", Action.Presenter.PREVIOUS);
        a4 = r51261;
        r51 r51262 = new r51(Constants.NOTIFICATION_UID, Action.Presenter.BLACKOUT);
        b4 = r51262;
        r51 r51263 = new r51("APP_NOTIFICATION_EVENT", 304);
        c4 = r51263;
        r51 r51264 = new r51("SOFTWARE_REVISION", 305);
        d4 = r51264;
        r51 r51265 = new r51("FILE_HANDLE_DESCRIPTION", 306);
        e4 = r51265;
        r51 r51266 = new r51("ABSOLUTE_FILE_NUMBER", 307);
        f4 = r51266;
        r51 r51267 = new r51("PHASE_ID", 308);
        g4 = r51267;
        r51 r51268 = new r51("REQUEST_RESULT", 309);
        h4 = r51268;
        r51 r51269 = new r51("GATT_STATUS", 310);
        i4 = r51269;
        r51 r51270 = new r51("COMMAND_RESULT", 311);
        j4 = r51270;
        r51 r51271 = new r51("REQUESTED_CONNECTION_PARAMS", 313);
        k4 = r51271;
        r51 r51272 = new r51("ACCEPTED_CONNECTION_PARAMS", 314);
        l4 = r51272;
        r51 r51273 = new r51("ERASE_DURATION_IN_MS", 315);
        m4 = r51273;
        r51 r51274 = new r51("TRANSFER_DURATION_IN_MS", 316);
        n4 = r51274;
        r51 r51275 = new r51("RECONNECT_DURATION_IN_MS", 317);
        o4 = r51275;
        r51 r51276 = new r51("OLD_FIRMWARE", 318);
        p4 = r51276;
        r51 r51277 = new r51("NEW_FIRMWARE", 319);
        q4 = r51277;
        r51 r51278 = new r51("STACK_TRACE", 321);
        r4 = r51278;
        r51 r51279 = new r51("FILE_INDEX", 322);
        s4 = r51279;
        r51 r51280 = new r51("ERROR_CODE", 323);
        t4 = r51280;
        r51 r51281 = new r51("FILE", 324);
        u4 = r51281;
        r51 r51282 = new r51("ROW_ID", 325);
        v4 = r51282;
        r51 r51283 = new r51("NUMBER_OF_DELETED_ROW", 326);
        w4 = r51283;
        r51 r51284 = new r51("COMMUTE_TIME_IN_MINUTE", 328);
        x4 = r51284;
        r51 r51285 = new r51("COMMUTE_INFO", 329);
        y4 = r51285;
        r51 r51286 = new r51("TRAFFIC", 331);
        z4 = r51286;
        r51 r51287 = new r51("CURRENT_FILES_VERSION", 332);
        A4 = r51287;
        r51 r51288 = new r51("CURRENT_VERSION", 333);
        B4 = r51288;
        r51 r51289 = new r51("SUPPORTED_VERSION", 334);
        C4 = r51289;
        r51 r51290 = new r51("CHALLENGE_ID", 335);
        D4 = r51290;
        r51 r51291 = new r51("STEP", 337);
        E4 = r51291;
        r51 r51292 = new r51("USER", 340);
        F4 = r51292;
        r51 r51293 = new r51("RANK", FacebookRequestErrorClassification.EC_TOO_MANY_USER_ACTION_CALLS);
        G4 = r51293;
        r51 r51294 = new r51("PLAYER_NUM", 345);
        H4 = r51294;
        r51 r51295 = new r51("ID", 347);
        I4 = r51295;
        r51 r51296 = new r51("UI_PACKAGE", 349);
        J4 = r51296;
        r51 r51297 = new r51("TIMEOUT_IN_MS", 352);
        K4 = r51297;
        r51 r51298 = new r51("INSTALLED_UI_PACKAGES", 353);
        L4 = r51298;
        r51 r51299 = new r51("BUNDLE_ID", 355);
        M4 = r51299;
        r51 r51300 = new r51("AUTO_RUN", 356);
        N4 = r51300;
        r51 r51301 = new r51("RESERVED_DATA", 357);
        O4 = r51301;
        r51 r51302 = new r51("NODE_NAME", 359);
        P4 = r51302;
        r51 r51303 = new r51("UI_PACKAGE_INFO", 360);
        Q4 = r51303;
        r51 r51304 = new r51("UI_PACKAGE_CHILD_NODES", 362);
        R4 = r51304;
        r51 r51305 = new r51("UI_PACKAGE_IMAGES", 363);
        S4 = r51305;
        r51 r51306 = new r51("UI_PACKAGE_LAYOUTS", 364);
        T4 = r51306;
        r51 r51307 = new r51("UI_PACKAGE_LOCALES", 365);
        U4 = r51307;
        r51 r51308 = new r51("UI_PACKAGE_INFOS", 366);
        V4 = r51308;
        r51 r51309 = new r51("UI_PACKAGE_CONFIGS", 367);
        W4 = r51309;
        r51 r51310 = new r51("UI_PACKAGE_BUNDLE_ID", 368);
        X4 = r51310;
        r51 r51311 = new r51("UI_PACKAGE_ID", 369);
        Y4 = r51311;
        r51 r51312 = new r51("UI_PACKAGE_CRC", 370);
        Z4 = r51312;
        r51 r51313 = new r51("PERCENTAGE", 371);
        a5 = r51313;
        r51 r51314 = new r51("VOLTAGE", 372);
        b5 = r51314;
        r51 r51315 = new r51("MESS", 373);
        c5 = r51315;
        r51 r51316 = new r51("CHALLENGES", 374);
        d5 = r51316;
        r51 r51317 = new r51("BLUETOOTH_DEVICE_TYPE", 375);
        e5 = r51317;
        r51 r51318 = new r51("ICON", 377);
        f5 = r51318;
        r51 r51319 = new r51("REPLY_MESSAGE_GROUP", 378);
        g5 = r51319;
        r51 r51320 = new r51("TIMESTAMPS", 379);
        h5 = r51320;
        r51 r51321 = new r51("TARGET_FIRMWARE", 380);
        i5 = r51321;
        r51 r51322 = new r51("ENCRYPT_METHOD", 381);
        j5 = r51322;
        r51 r51323 = new r51("KEY_TYPE", 382);
        k5 = r51323;
        r51 r51324 = new r51("BATTERY_MODE", 383);
        l5 = r51324;
        r51 r51325 = new r51("NFC_STATE", 384);
        m5 = r51325;
        r51 r51326 = new r51("ALWAYS_ON_SCREEN_STATE", 385);
        n5 = r51326;
        r51 r51327 = new r51("TOUCH_TO_WAKE_STATE", 386);
        o5 = r51327;
        r51 r51328 = new r51("TILT_TO_WAKE_STATE", 387);
        p5 = r51328;
        r51 r51329 = new r51("LOCATION_STATE", 388);
        q5 = r51329;
        r51 r51330 = new r51("VIBRATION_STATE", 389);
        r5 = r51330;
        r51 r51331 = new r51("SPEAKER_STATE", 390);
        s5 = r51331;
        r51 r51332 = new r51("WIFI_STATE", 391);
        t5 = r51332;
        r51 r51333 = new r51("GOOGLE_DETECT_STATE", 392);
        u5 = r51333;
        r51 r51334 = new r51("BLE_FROM_MINUTE_OFFET", 393);
        v5 = r51334;
        r51 r51335 = new r51("BLE_TO_MINUTE_OFFET", 394);
        w5 = r51335;
        r51 r51336 = new r51("MINIMUM_STEP_THRESHOLD", 395);
        x5 = r51336;
        r51 r51337 = new r51("XOR_KEY_FIRST_OFFSET", 396);
        y5 = r51337;
        r51 r51338 = new r51("XOR_KEY_SECOND_OFFSET", 397);
        z5 = r51338;
        r51 r51339 = new r51("DIAMETER", 398);
        A5 = r51339;
        r51 r51340 = new r51("TIRE_SIZE", 399);
        B5 = r51340;
        r51 r51341 = new r51("CHAINRING", MFNetworkReturnCode.BAD_REQUEST);
        C5 = r51341;
        r51 r51342 = new r51("COG", 401);
        D5 = r51342;
        r51 r51343 = new r51("GPS", Action.ActivityTracker.TAG_ACTIVITY);
        E5 = r51343;
        r51 r51344 = new r51("ACTIVITY", MFNetworkReturnCode.WRONG_PASSWORD);
        F5 = r51344;
        r51 r51345 = new r51("SESSION_ID", 404);
        G5 = r51345;
        r51 r51346 = new r51("WATCH_APPS", 405);
        H5 = r51346;
        r51 r51347 = new r51("INSTALLED_WATCH_APPS", 406);
        I5 = r51347;
        r51 r51348 = new r51("ROUTE", 407);
        J5 = r51348;
        r51 r51349 = new r51("DURATION", MFNetworkReturnCode.CLIENT_TIMEOUT);
        K5 = r51349;
        r51 r51350 = new r51("TOTAL_DURATION", 410);
        L5 = r51350;
        r51 r51351 = new r51("ORIENTATION", 411);
        M5 = r51351;
        r51 r51352 = new r51("LAYOUT_TYPE", FacebookRequestErrorClassification.EC_APP_NOT_INSTALLED);
        N5 = r51352;
        r51 r51353 = new r51("GPS_FILE_COUNT", 413);
        O5 = r51353;
        r51 r51354 = new r51("MSL_DUR", 414);
        P5 = r51354;
        r51 r51355 = new r51("PAUSE_RUN_SEQ", MFNetworkReturnCode.CONTENT_TYPE_ERROR);
        Q5 = r51355;
        r51 r51356 = new r51("GPS_COUNT", 416);
        R5 = r51356;
        r51 r51357 = new r51("ELABEL_FILE", 417);
        S5 = r51357;
        T5 = new r51[]{r51, r512, r513, r514, r515, r516, r517, r518, r519, r5110, r5111, r5112, r5113, new r51("DEVICE_RESPONSE", 13), r5114, r5115, r5116, new r51("COMPLICATION_NAME", 17), r5117, r5118, new r51("MINUTES", 20), r5119, r5120, new r51("TEMP", 23), r5121, r5122, r5123, r5124, r5125, r5126, r5127, r5128, r5129, r5130, r5131, r5132, new r51("MAIN", 36), new r51("TOP_COMPLICATION", 37), new r51("RIGHT_COMPLICATION", 38), new r51("BOTTOM_COMPLICATION", 39), new r51("LEFT_COMPLICATION", 40), new r51("TOP", 41), new r51("RIGHT", 42), new r51("BOTTOM", 43), new r51("LEFT", 44), new r51("MIDDLE", 45), r5133, r5134, r5135, new r51("COMPLICATION_CONFIG", 49), r5136, new r51("UTC_OFFSET_IN_MINUTES", 51), new r51("CONFIG", 52), r5137, r5138, r5139, r5140, r5141, r5142, r5143, r5144, r5145, r5146, new r51("WATCH_APP_CONFIG", 63), r5147, r5148, r5149, r5150, r5151, r5152, r5153, r5154, r5155, r5156, r5157, r5158, r5159, new r51("CONNECTION_PARAMS", 77), new r51("EXCHANGED_CONNECTION_PARAMS", 78), r5160, r5161, r5162, r5163, r5164, r5165, r5166, r5167, r5168, r5169, new r51("LUTS_VERSION", 89), r5170, r5171, r5172, r5173, r5174, r5175, r5176, r5177, r5178, r5179, r5180, r5181, r5182, r5183, r5184, new r51("TRANSFERRED_DATA_CRC", 105), r5185, r5186, r5187, r5188, r5189, r5190, r5191, r5192, r5193, r5194, r5195, r5196, r5197, r5198, r5199, r51100, r51101, r51102, r51103, r51104, r51105, r51106, r51107, r51108, r51109, r51110, r51111, r51112, r51113, r51114, r51115, r51116, r51117, r51118, r51119, r51120, r51121, r51122, r51123, r51124, r51125, r51126, r51127, r51128, new r51("HEARTBEAT_STATISTIC", 150), new r51("HEARTBEATS_SENT", 151), new r51("HEARTBEATS_RECEIVED", 152), r51129, new r51("HEARTBEAT", 154), r51130, r51131, r51132, r51133, r51134, r51135, r51136, r51137, r51138, r51139, r51140, r51141, new r51("TOGGLE_RING_MY_PHONE", 167), new r51("MUSIC_CONTROL_NOTIFICATION", DateTimeConstants.HOURS_PER_WEEK), new r51("BACKGROUND_SYNC", 169), r51142, r51143, r51144, r51145, r51146, r51147, r51148, r51149, r51150, r51151, r51152, r51153, r51154, r51155, r51156, r51157, r51158, r51159, r51160, new r51("QUERY_STRING", 189), new r51("ERROR", FacebookRequestErrorClassification.EC_INVALID_TOKEN), r51161, r51162, r51163, r51164, r51165, r51166, r51167, r51168, r51169, r51170, r51171, r51172, r51173, r51174, new r51("SECRET_KEY", 205), r51175, r51176, r51177, r51178, r51179, r51180, r51181, r51182, r51183, r51184, new r51("DESCRIPTOR", 216), new r51("PREPARED_WRITE", 217), new r51("RESPONSE_NEEDED", 218), new r51("NEED_CONFIRM", 219), r51185, r51186, new r51("SERVICE", 222), new r51("EXECUTE", 223), r51187, r51188, r51189, r51190, r51191, r51192, r51193, r51194, r51195, r51196, new r51("ACTUAL_BYTE_WRITTEN", 234), r51197, r51198, r51199, r51200, r51201, r51202, new r51("OTA_ENTER_RESPONSE", 241), r51203, r51204, r51205, r51206, r51207, r51208, r51209, r51210, r51211, r51212, r51213, r51214, r51215, r51216, r51217, r51218, r51219, r51220, r51221, r51222, r51223, r51224, r51225, new r51("LOCALE_VERSION", 265), r51226, r51227, r51228, r51229, r51230, r51231, r51232, r51233, r51234, r51235, r51236, r51237, new r51("CUSTOMIZATION", 278), r51238, r51239, r51240, r51241, r51242, r51243, r51244, r51245, r51246, r51247, r51248, r51249, r51250, r51251, r51252, r51253, r51254, r51255, r51256, r51257, r51258, r51259, r51260, r51261, r51262, r51263, r51264, r51265, r51266, r51267, r51268, r51269, r51270, new r51("CURRENT_CONNECTION_PARAMS", 312), r51271, r51272, r51273, r51274, r51275, r51276, r51277, new r51("TOTAL_DATA_SIZE", 320), r51278, r51279, r51280, r51281, r51282, r51283, new r51("DEST", 327), r51284, r51285, new r51("COMMUTE", 330), r51286, r51287, r51288, r51289, r51290, new r51("REQUEST_TYPE", 336), r51291, new r51("STEP_OFFSET", 338), new r51("CALORIES_OFFSET", 339), r51292, r51293, new r51("DURATION_MINUTE", 342), new r51("REMAIN_MINUTE", 343), new r51("GOAL", 344), r51294, new r51("SESSION_STATE", 346), r51295, new r51("INSTALLED_VERSION", 348), r51296, new r51("HR_BPM", 350), new r51("CHALLENGE_METRIC", 351), r51297, r51298, new r51("PDK_CONFIG", 354), r51299, r51300, r51301, new r51("BYTE_CODE", 358), r51302, r51303, new r51("UI_PACKAGE_MAIN_NODE", 361), r51304, r51305, r51306, r51307, r51308, r51309, r51310, r51311, r51312, r51313, r51314, r51315, r51316, r51317, new r51("SUB_ENTRY", 376), r51318, r51319, r51320, r51321, r51322, r51323, r51324, r51325, r51326, r51327, r51328, r51329, r51330, r51331, r51332, r51333, r51334, r51335, r51336, r51337, r51338, r51339, r51340, r51341, r51342, r51343, r51344, r51345, r51346, r51347, r51348, r51349, new r51("APP_LIST", MFNetworkReturnCode.ITEM_NAME_IN_USED), r51350, r51351, r51352, r51353, r51354, r51355, r51356, r51357};
    }
    */

    @DexIgnore
    public r51(String str, int i6) {
    }

    @DexIgnore
    public static r51 valueOf(String str) {
        return (r51) Enum.valueOf(r51.class, str);
    }

    @DexIgnore
    public static r51[] values() {
        return (r51[]) T5.clone();
    }
}
