package com.fossil;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import com.fossil.ao5;
import com.fossil.bo5;
import com.fossil.ew6;
import com.fossil.fl4;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq6 extends gq6 {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public /* final */ DeviceRepository A;
    @DexIgnore
    public /* final */ ServerSettingRepository B;
    @DexIgnore
    public /* final */ lm4 C;
    @DexIgnore
    public /* final */ ch5 D;
    @DexIgnore
    public ao5 e;
    @DexIgnore
    public bo5 f;
    @DexIgnore
    public mn5 g;
    @DexIgnore
    public qd5 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Calendar u; // = Calendar.getInstance();
    @DexIgnore
    public SignUpSocialAuth v;
    @DexIgnore
    public SignUpEmailAuth w;
    @DexIgnore
    public /* final */ hq6 x;
    @DexIgnore
    public /* final */ ew6 y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ se7 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kq6$b$a$a")
            /* renamed from: com.fossil.kq6$b$a$a  reason: collision with other inner class name */
            public static final class C0099a implements ServerSettingDataSource.OnGetServerSettingList {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kq6$b$a$a$a")
                /* renamed from: com.fossil.kq6$b$a$a$a  reason: collision with other inner class name */
                public static final class C0100a implements fl4.e<bo5.c, bo5.b> {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0099a a;

                    @DexIgnore
                    public C0100a(C0099a aVar) {
                        this.a = aVar;
                    }

                    @DexIgnore
                    /* renamed from: a */
                    public void onSuccess(bo5.c cVar) {
                        ee7.b(cVar, "responseValue");
                        PortfolioApp.g0.c().f().a(this.a.a.this$0.this$0);
                        b bVar = this.a.a.this$0;
                        bVar.this$0.d(bVar.$it.getService());
                    }

                    @DexIgnore
                    public void a(bo5.b bVar) {
                        ee7.b(bVar, "errorValue");
                        this.a.a.this$0.this$0.x.f();
                        this.a.a.this$0.this$0.x.h(bVar.a(), bVar.b());
                    }
                }

                @DexIgnore
                public C0099a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String C = kq6.E;
                    local.e(C, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.a.this$0.this$0.x.f();
                    this.a.this$0.this$0.x.h(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(kq6.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (ee7.a((Object) t.getObjectId(), (Object) "dataLocationSharingPrivacyVersionLatest")) {
                                    this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (ee7.a((Object) t.getObjectId(), (Object) "privacyVersionLatest")) {
                                    this.a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (ee7.a((Object) t.getObjectId(), (Object) "tosVersionLatest")) {
                                    this.a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String C = kq6.E;
                    local.d(C, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.a.this$0.this$0.t) {
                        arrayList.add(this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.a.this$0.this$0.s) {
                        arrayList2.add(this.a.this$0.$privacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.a.this$0.this$0.r) {
                        arrayList3.add(this.a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.a.this$0.this$0.q().a(new bo5.a(this.a.this$0.$it), new C0100a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.B.getServerSettingList(new C0099a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(se7 se7, se7 se72, se7 se73, SignUpSocialAuth signUpSocialAuth, fb7 fb7, kq6 kq6) {
            super(2, fb7);
            this.$dataLocationSharingPrivacyVersionLatest = se7;
            this.$privacyVersionLatest = se72;
            this.$termOfUseVersionLatest = se73;
            this.$it = signUpSocialAuth;
            this.this$0 = kq6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, fb7, this.this$0);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ se7 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kq6$c$a$a")
            /* renamed from: com.fossil.kq6$c$a$a  reason: collision with other inner class name */
            public static final class C0101a implements ServerSettingDataSource.OnGetServerSettingList {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kq6$c$a$a$a")
                /* renamed from: com.fossil.kq6$c$a$a$a  reason: collision with other inner class name */
                public static final class C0102a implements fl4.e<ao5.c, ao5.b> {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0101a a;

                    @DexIgnore
                    public C0102a(C0101a aVar) {
                        this.a = aVar;
                    }

                    @DexIgnore
                    /* renamed from: a */
                    public void onSuccess(ao5.c cVar) {
                        ee7.b(cVar, "responseValue");
                        PortfolioApp.g0.c().f().a(this.a.a.this$0.this$0);
                        kq6 kq6 = this.a.a.this$0.this$0;
                        String lowerCase = "Email".toLowerCase();
                        ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        kq6.d(lowerCase);
                    }

                    @DexIgnore
                    public void a(ao5.b bVar) {
                        ee7.b(bVar, "errorValue");
                        this.a.a.this$0.this$0.x.f();
                        this.a.a.this$0.this$0.x.h(bVar.a(), bVar.b());
                    }
                }

                @DexIgnore
                public C0101a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String C = kq6.E;
                    local.e(C, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.a.this$0.this$0.x.f();
                    this.a.this$0.this$0.x.h(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(kq6.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (ee7.a((Object) t.getObjectId(), (Object) "dataLocationSharingPrivacyVersionLatest")) {
                                    this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (ee7.a((Object) t.getObjectId(), (Object) "privacyVersionLatest")) {
                                    this.a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (ee7.a((Object) t.getObjectId(), (Object) "tosVersionLatest")) {
                                    this.a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String C = kq6.E;
                    local.d(C, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.a.this$0.this$0.t) {
                        arrayList.add(this.a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.a.this$0.this$0.s) {
                        arrayList2.add(this.a.this$0.$privacyVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.a.this$0.this$0.r) {
                        arrayList3.add(this.a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.a.this$0.this$0.p().a(new ao5.a(this.a.this$0.$it), new C0102a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.B.getServerSettingList(new C0101a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(se7 se7, se7 se72, se7 se73, SignUpEmailAuth signUpEmailAuth, fb7 fb7, kq6 kq6) {
            super(2, fb7);
            this.$dataLocationSharingPrivacyVersionLatest = se7;
            this.$privacyVersionLatest = se72;
            this.$termOfUseVersionLatest = se73;
            this.$it = signUpEmailAuth;
            this.this$0 = kq6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, fb7, this.this$0);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1", f = "ProfileSetupPresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED, 419, 427}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $service;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$1", f = "ProfileSetupPresenter.kt", l = {410, 411, 416, 417}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<UserSettings>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<UserSettings>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            /* JADX WARNING: Removed duplicated region for block: B:21:0x00ed A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00ee  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0107 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0108 A[PHI: r13 
              PHI: (r13v1 java.lang.Object) = (r13v5 java.lang.Object), (r13v0 java.lang.Object) binds: [B:24:0x0105, B:6:0x0015] A[DONT_GENERATE, DONT_INLINE], RETURN] */
            @DexIgnore
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                    r12 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r12.label
                    r2 = 0
                    r3 = 4
                    r4 = 3
                    r5 = 2
                    r6 = 1
                    if (r1 == 0) goto L_0x004d
                    if (r1 == r6) goto L_0x0045
                    if (r1 == r5) goto L_0x0039
                    if (r1 == r4) goto L_0x002a
                    if (r1 != r3) goto L_0x0022
                    java.lang.Object r0 = r12.L$1
                    com.fossil.ko4 r0 = (com.fossil.ko4) r0
                    java.lang.Object r0 = r12.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r13)
                    goto L_0x0108
                L_0x0022:
                    java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r13.<init>(r0)
                    throw r13
                L_0x002a:
                    boolean r1 = r12.Z$0
                    java.lang.Object r2 = r12.L$1
                    com.fossil.ko4 r2 = (com.fossil.ko4) r2
                    java.lang.Object r4 = r12.L$0
                    com.fossil.yi7 r4 = (com.fossil.yi7) r4
                    com.fossil.t87.a(r13)
                    goto L_0x00f1
                L_0x0039:
                    java.lang.Object r1 = r12.L$1
                    com.fossil.ko4 r1 = (com.fossil.ko4) r1
                    java.lang.Object r5 = r12.L$0
                    com.fossil.yi7 r5 = (com.fossil.yi7) r5
                    com.fossil.t87.a(r13)
                    goto L_0x009a
                L_0x0045:
                    java.lang.Object r1 = r12.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r13)
                    goto L_0x007f
                L_0x004d:
                    com.fossil.t87.a(r13)
                    com.fossil.yi7 r1 = r12.p$
                    com.fossil.kq6$d r13 = r12.this$0
                    com.fossil.kq6 r13 = r13.this$0
                    com.fossil.lm4 r13 = r13.C
                    com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r7 = r7.c()
                    java.lang.String r7 = r7.c()
                    com.fossil.xe5 r8 = com.fossil.xe5.b
                    java.lang.String r8 = r8.a()
                    java.lang.String[] r9 = new java.lang.String[r6]
                    com.fossil.gm4 r10 = com.fossil.gm4.BUDDY_CHALLENGE
                    java.lang.String r10 = r10.getStrType()
                    r9[r2] = r10
                    r12.L$0 = r1
                    r12.label = r6
                    java.lang.Object r13 = r13.a(r7, r8, r9, r12)
                    if (r13 != r0) goto L_0x007f
                    return r0
                L_0x007f:
                    com.fossil.ko4 r13 = (com.fossil.ko4) r13
                    com.fossil.kq6$d r7 = r12.this$0
                    com.fossil.kq6 r7 = r7.this$0
                    com.fossil.lm4 r7 = r7.C
                    r12.L$0 = r1
                    r12.L$1 = r13
                    r12.label = r5
                    java.lang.Object r5 = r7.a(r12)
                    if (r5 != r0) goto L_0x0096
                    return r0
                L_0x0096:
                    r11 = r1
                    r1 = r13
                    r13 = r5
                    r5 = r11
                L_0x009a:
                    java.lang.Boolean r13 = (java.lang.Boolean) r13
                    boolean r13 = r13.booleanValue()
                    com.fossil.kq6$d r7 = r12.this$0
                    com.fossil.kq6 r7 = r7.this$0
                    com.fossil.ch5 r7 = r7.D
                    java.lang.Boolean r8 = com.fossil.pb7.a(r13)
                    r7.b(r8)
                    com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r7 = r7.c()
                    r7.a(r13)
                    com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                    java.lang.String r8 = com.fossil.kq6.E
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    r9.<init>()
                    java.lang.String r10 = "downloadRecommendedGoals - flag result: "
                    r9.append(r10)
                    r9.append(r1)
                    java.lang.String r9 = r9.toString()
                    r7.e(r8, r9)
                    com.fossil.kq6$d r7 = r12.this$0
                    com.fossil.kq6 r7 = r7.this$0
                    com.portfolio.platform.data.source.DeviceRepository r7 = r7.A
                    r8 = 0
                    r12.L$0 = r5
                    r12.L$1 = r1
                    r12.Z$0 = r13
                    r12.label = r4
                    java.lang.Object r2 = com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r7, r2, r12, r6, r8)
                    if (r2 != r0) goto L_0x00ee
                    return r0
                L_0x00ee:
                    r2 = r1
                    r4 = r5
                    r1 = r13
                L_0x00f1:
                    com.fossil.kq6$d r13 = r12.this$0
                    com.fossil.kq6 r13 = r13.this$0
                    com.portfolio.platform.data.source.UserRepository r13 = r13.z
                    r12.L$0 = r4
                    r12.L$1 = r2
                    r12.Z$0 = r1
                    r12.label = r3
                    java.lang.Object r13 = r13.getUserSettingFromServer(r12)
                    if (r13 != r0) goto L_0x0108
                    return r0
                L_0x0108:
                    return r13
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.kq6.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$currentUser$1", f = "ProfileSetupPresenter.kt", l = {419}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.z;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements fl4.e<ew6.d, ew6.b> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public c(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(ew6.d dVar) {
                ee7.b(dVar, "responseValue");
                d dVar2 = this.a;
                dVar2.this$0.e(dVar2.$service);
            }

            @DexIgnore
            public void a(ew6.b bVar) {
                ee7.b(bVar, "errorValue");
                d dVar = this.a;
                dVar.this$0.e(dVar.$service);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kq6 kq6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kq6;
            this.$service = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$service, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x003d
                if (r1 == r4) goto L_0x0035
                if (r1 == r3) goto L_0x002d
                if (r1 != r2) goto L_0x0025
                int r0 = r7.I$0
                java.lang.Object r1 = r7.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r2 = r7.L$1
                com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                java.lang.Object r2 = r7.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r8)
                goto L_0x00c2
            L_0x0025:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x002d:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x006f
            L_0x0035:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x0059
            L_0x003d:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r8 = r7.p$
                com.fossil.kq6 r1 = r7.this$0
                com.fossil.ti7 r1 = r1.c()
                com.fossil.kq6$d$a r6 = new com.fossil.kq6$d$a
                r6.<init>(r7, r5)
                r7.L$0 = r8
                r7.label = r4
                java.lang.Object r1 = com.fossil.vh7.a(r1, r6, r7)
                if (r1 != r0) goto L_0x0058
                return r0
            L_0x0058:
                r1 = r8
            L_0x0059:
                com.fossil.kq6 r8 = r7.this$0
                com.fossil.ti7 r8 = r8.c()
                com.fossil.kq6$d$b r4 = new com.fossil.kq6$d$b
                r4.<init>(r7, r5)
                r7.L$0 = r1
                r7.label = r3
                java.lang.Object r8 = com.fossil.vh7.a(r8, r4, r7)
                if (r8 != r0) goto L_0x006f
                return r0
            L_0x006f:
                com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
                if (r8 == 0) goto L_0x00ec
                java.lang.String r3 = r8.getBirthday()
                if (r3 == 0) goto L_0x00e8
                int r3 = r8.getAge(r3)
                com.fossil.kq6 r4 = r7.this$0
                com.fossil.qd5 r4 = r4.o()
                java.lang.String r5 = r8.getUserId()
                r4.b(r5)
                com.fossil.ah5$a r4 = com.fossil.ah5.p
                com.fossil.ah5 r4 = r4.a()
                java.lang.String r5 = r8.getUserId()
                r4.b(r5)
                com.fossil.pg5 r4 = com.fossil.pg5.i
                r4.c()
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                java.lang.String r5 = r8.getUserId()
                r4.q(r5)
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r7.L$0 = r1
                r7.L$1 = r8
                r7.L$2 = r8
                r7.I$0 = r3
                r7.label = r2
                java.lang.Object r1 = r4.l(r7)
                if (r1 != r0) goto L_0x00c0
                return r0
            L_0x00c0:
                r1 = r8
                r0 = r3
            L_0x00c2:
                com.fossil.kq6 r8 = r7.this$0
                com.fossil.ew6 r8 = r8.y
                com.fossil.ew6$c r2 = new com.fossil.ew6$c
                int r3 = r1.getHeightInCentimeters()
                int r4 = r1.getWeightInGrams()
                com.fossil.fb5$a r5 = com.fossil.fb5.Companion
                java.lang.String r1 = r1.getGender()
                com.fossil.fb5 r1 = r5.a(r1)
                r2.<init>(r0, r3, r4, r1)
                com.fossil.kq6$d$c r0 = new com.fossil.kq6$d$c
                r0.<init>(r7)
                r8.a(r2, r0)
                goto L_0x00ec
            L_0x00e8:
                com.fossil.ee7.a()
                throw r5
            L_0x00ec:
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.kq6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.z;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(kq6 kq6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kq6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kq6 kq6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kq6 kq62 = this.this$0;
                ti7 c = kq62.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = kq62;
                this.label = 1;
                obj = vh7.a(c, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                kq6 = kq62;
            } else if (i == 1) {
                kq6 = (kq6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kq6.j = (MFUser) obj;
            MFUser g = this.this$0.j;
            if (g != null) {
                this.this$0.x.a(g);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kq6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.z;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.updateUser(mFUser, true, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(kq6 kq6, MFUser mFUser, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kq6;
            this.$user = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$user, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.x.T0();
                this.this$0.x.g();
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            zi5 zi5 = (zi5) obj;
            if (zi5 instanceof bj5) {
                FLogger.INSTANCE.getLocal().d(kq6.E, "updateAccount successfully");
                PortfolioApp.g0.c().f().a(this.this$0);
                kq6 kq6 = this.this$0;
                String authType = this.$user.getAuthType();
                if (authType != null) {
                    kq6.d(authType);
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (zi5 instanceof yi5) {
                FLogger.INSTANCE.getLocal().d(kq6.E, "updateAccount failed");
                hq6 i2 = this.this$0.x;
                yi5 yi5 = (yi5) zi5;
                int a4 = yi5.a();
                ServerError c = yi5.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                i2.h(a4, str);
                this.this$0.x.f();
                this.this$0.x.r0();
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = kq6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileSetupPresenter::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    public kq6(hq6 hq6, ew6 ew6, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, lm4 lm4, ch5 ch5) {
        ee7.b(hq6, "mView");
        ee7.b(ew6, "mGetRecommendedGoalUseCase");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(serverSettingRepository, "mServerSettingRepository");
        ee7.b(lm4, "flagRepository");
        ee7.b(ch5, "shared");
        this.x = hq6;
        this.y = ew6;
        this.z = userRepository;
        this.A = deviceRepository;
        this.B = serverSettingRepository;
        this.C = lm4;
        this.D = ch5;
    }

    @DexIgnore
    public void A() {
        this.x.a(this);
    }

    @DexIgnore
    public final void B() {
        if (!w()) {
            this.x.T0();
        } else if (!x()) {
            this.x.T0();
        } else if (!y()) {
            this.x.T0();
        } else if (!v()) {
            this.x.T0();
        } else if (!tm4.a.a().a(m())) {
            this.x.T0();
        } else {
            this.x.r0();
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    public final String n() {
        String a2 = vx6.a(vx6.c.PRIVACY, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886908).toString();
        ee7.a((Object) a2, "privacyPolicyUrl");
        return mh7.a(str, "privacy_policy", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public final qd5 o() {
        qd5 qd5 = this.h;
        if (qd5 != null) {
            return qd5;
        }
        ee7.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final ao5 p() {
        ao5 ao5 = this.e;
        if (ao5 != null) {
            return ao5;
        }
        ee7.d("mSignUpEmailUseCase");
        throw null;
    }

    @DexIgnore
    public final bo5 q() {
        bo5 bo5 = this.f;
        if (bo5 != null) {
            return bo5;
        }
        ee7.d("mSignUpSocialUseCase");
        throw null;
    }

    @DexIgnore
    public final String r() {
        String a2 = vx6.a(vx6.c.PRIVACY, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886909).toString();
        ee7.a((Object) a2, "privacyPolicyUrl");
        return mh7.a(str, "privacy_policy", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public SignUpSocialAuth s() {
        return this.v;
    }

    @DexIgnore
    public final String t() {
        String a2 = vx6.a(vx6.c.TERMS, null);
        String a3 = vx6.a(vx6.c.PRIVACY, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886923).toString();
        ee7.a((Object) a2, "termOfUseUrl");
        String a4 = mh7.a(str, "term_of_use_url", a2, false, 4, (Object) null);
        ee7.a((Object) a3, "privacyPolicyUrl");
        return mh7.a(a4, "privacy_policy", a3, false, 4, (Object) null);
    }

    @DexIgnore
    public final String u() {
        String a2 = vx6.a(vx6.c.TERMS, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886910).toString();
        ee7.a((Object) a2, "termOfUseUrl");
        return mh7.a(str, "term_of_use_url", a2, false, 4, (Object) null);
    }

    @DexIgnore
    public final boolean v() {
        Calendar calendar = this.u;
        if (calendar == null) {
            return false;
        }
        if (calendar != null) {
            Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(calendar), LocalDate.now());
            ee7.a((Object) yearsBetween, "age");
            if (yearsBetween.getYears() >= 16) {
                return true;
            }
            return false;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final boolean w() {
        if (this.k.length() == 0) {
            this.x.a(false, false, "");
        } else if (!kx6.a(this.k)) {
            hq6 hq6 = this.x;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886955);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            hq6.a(false, true, a2);
        } else {
            this.x.a(true, false, "");
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean x() {
        String str = this.l;
        if (!(str == null || mh7.a(str))) {
            this.x.E(true);
            return true;
        }
        this.x.E(false);
        return false;
    }

    @DexIgnore
    public final boolean y() {
        String str = this.m;
        if (!(str == null || mh7.a(str))) {
            this.x.A(true);
            return true;
        }
        this.x.A(false);
        return false;
    }

    @DexIgnore
    public boolean z() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void b(String str) {
        ee7.b(str, Constants.PROFILE_KEY_FIRST_NAME);
        this.l = str;
        x();
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void c(String str) {
        ee7.b(str, Constants.PROFILE_KEY_LAST_NAME);
        this.m = str;
        y();
        B();
    }

    @DexIgnore
    public final ik7 d(String str) {
        ee7.b(str, Constants.SERVICE);
        return xh7.b(e(), null, null, new d(this, str, null), 3, null);
    }

    @DexIgnore
    public final void e(String str) {
        ee7.b(str, Constants.SERVICE);
        gf5 a2 = qd5.f.a("user_signup");
        String lowerCase = "Source".toLowerCase();
        ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        a2.a(lowerCase, str);
        a2.a();
        this.x.f();
        this.x.V();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        hq6 hq6 = this.x;
        Spanned fromHtml = Html.fromHtml(t());
        ee7.a((Object) fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        hq6.a(fromHtml);
        hq6 hq62 = this.x;
        Spanned fromHtml2 = Html.fromHtml(u());
        ee7.a((Object) fromHtml2, "Html.fromHtml(getTermsOfUseString())");
        hq62.d(fromHtml2);
        hq6 hq63 = this.x;
        Spanned fromHtml3 = Html.fromHtml(r());
        ee7.a((Object) fromHtml3, "Html.fromHtml(getPrivacyPolicyString())");
        hq63.b(fromHtml3);
        hq6 hq64 = this.x;
        Spanned fromHtml4 = Html.fromHtml(n());
        ee7.a((Object) fromHtml4, "Html.fromHtml(getLocationDataString())");
        hq64.c(fromHtml4);
        this.x.e();
        if (!this.i) {
            SignUpSocialAuth signUpSocialAuth = this.v;
            if (signUpSocialAuth != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = E;
                local.d(str, "start - mSocialAuth=" + signUpSocialAuth);
                this.x.a(signUpSocialAuth);
            }
            SignUpEmailAuth signUpEmailAuth = this.w;
            if (signUpEmailAuth != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = E;
                local2.d(str2, "start - mEmailAuth=" + signUpEmailAuth);
                this.x.c(signUpEmailAuth);
            }
        } else if (this.j == null) {
            ik7 unused = xh7.b(e(), null, null, new e(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void h() {
        if (this.i) {
            MFUser mFUser = this.j;
            if (mFUser != null) {
                mFUser.setEmail(this.k);
                String str = this.l;
                if (str != null) {
                    mFUser.setFirstName(str);
                    String str2 = this.m;
                    if (str2 != null) {
                        mFUser.setLastName(str2);
                        String str3 = this.o;
                        if (str3 != null) {
                            mFUser.setBirthday(str3);
                            mFUser.setDiagnosticEnabled(this.q);
                            String str4 = this.n;
                            if (str4 == null) {
                                str4 = fb5.OTHER.toString();
                            }
                            mFUser.setGender(str4);
                            a(mFUser);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = E;
        local.d(str5, "create account socialAuth=" + this.v + " emailAuth=" + this.w);
        this.x.g();
        SignUpSocialAuth signUpSocialAuth = this.v;
        if (signUpSocialAuth != null) {
            signUpSocialAuth.setEmail(this.k);
            String str6 = this.n;
            if (str6 == null) {
                str6 = fb5.OTHER.toString();
            }
            signUpSocialAuth.setGender(str6);
            String str7 = this.o;
            if (str7 != null) {
                signUpSocialAuth.setBirthday(str7);
                signUpSocialAuth.setClientId(rd5.g.a(""));
                String str8 = this.l;
                if (str8 != null) {
                    signUpSocialAuth.setFirstName(str8);
                    String str9 = this.m;
                    if (str9 != null) {
                        signUpSocialAuth.setLastName(str9);
                        signUpSocialAuth.setDiagnosticEnabled(this.q);
                        se7 se7 = new se7();
                        se7.element = "";
                        se7 se72 = new se7();
                        se72.element = "";
                        se7 se73 = new se7();
                        se73.element = "";
                        ik7 unused = xh7.b(e(), null, null, new b(se7, se72, se73, signUpSocialAuth, null, this), 3, null);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        SignUpEmailAuth signUpEmailAuth = this.w;
        if (signUpEmailAuth != null) {
            String str10 = this.n;
            if (str10 == null) {
                str10 = fb5.OTHER.toString();
            }
            signUpEmailAuth.setGender(str10);
            String str11 = this.o;
            if (str11 != null) {
                signUpEmailAuth.setBirthday(str11);
                signUpEmailAuth.setClientId(rd5.g.a(""));
                String str12 = this.l;
                if (str12 != null) {
                    signUpEmailAuth.setFirstName(str12);
                    String str13 = this.m;
                    if (str13 != null) {
                        signUpEmailAuth.setLastName(str13);
                        signUpEmailAuth.setDiagnosticEnabled(this.q);
                        se7 se74 = new se7();
                        se74.element = "";
                        se7 se75 = new se7();
                        se75.element = "";
                        se7 se76 = new se7();
                        se76.element = "";
                        ik7 unused2 = xh7.b(e(), null, null, new c(se74, se75, se76, signUpEmailAuth, null, this), 3, null);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public Calendar i() {
        Calendar calendar = this.u;
        ee7.a((Object) calendar, "mBirthdayCalendar");
        return calendar;
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void j() {
        Bundle bundle = new Bundle();
        bundle.putInt("DAY", this.u.get(5));
        bundle.putInt("MONTH", this.u.get(2) + 1);
        bundle.putInt("YEAR", this.u.get(1));
        this.x.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void k() {
        Bundle bundle = new Bundle();
        Calendar instance = Calendar.getInstance();
        bundle.putInt("DAY", instance.get(5));
        bundle.putInt("MONTH", instance.get(2) + 1);
        bundle.putInt("YEAR", instance.get(1) - 32);
        this.x.a(bundle);
    }

    @DexIgnore
    public SignUpEmailAuth l() {
        return this.w;
    }

    @DexIgnore
    public final List<String> m() {
        ArrayList arrayList = new ArrayList();
        if (this.p) {
            arrayList.add("Agree terms of use and privacy");
        }
        if (this.q) {
            arrayList.add("Allow Gather data usage");
        }
        if (this.r) {
            arrayList.add("Agree term of use");
        }
        if (this.s) {
            arrayList.add("Agree privacy");
        }
        if (this.t) {
            arrayList.add("Allow location data processing");
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void a(String str) {
        ee7.b(str, Constants.EMAIL);
        this.k = str;
        w();
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void d(boolean z2) {
        this.r = z2;
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void b(boolean z2) {
        this.t = z2;
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void c(boolean z2) {
        this.s = z2;
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void a(Date date, Calendar calendar) {
        ee7.b(date, "data");
        ee7.b(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d(E, "onBirthDayChanged");
        this.o = zd5.g(date);
        this.u = calendar;
        hq6 hq6 = this.x;
        String f2 = zd5.f(date);
        ee7.a((Object) f2, "DateHelper.formatLocalDateMonth(data)");
        hq6.P(f2);
        if (!v()) {
            hq6 hq62 = this.x;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886911);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026Text__YouCantUseTheAppIf)");
            hq62.b(false, a2);
        } else {
            this.x.b(true, "");
        }
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void e(boolean z2) {
        this.p = z2;
        B();
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void a(fb5 fb5) {
        ee7.b(fb5, "gender");
        this.n = fb5.toString();
        this.x.a(fb5);
    }

    @DexIgnore
    public void f(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    @Override // com.fossil.gq6
    public void a(boolean z2) {
        this.q = z2;
        B();
    }

    @DexIgnore
    public void a(SignUpEmailAuth signUpEmailAuth) {
        ee7.b(signUpEmailAuth, "auth");
        this.w = signUpEmailAuth;
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "auth");
        this.v = signUpSocialAuth;
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "updateAccount - userId = " + mFUser.getUserId());
        ik7 unused = xh7.b(e(), null, null, new f(this, mFUser, null), 3, null);
    }
}
