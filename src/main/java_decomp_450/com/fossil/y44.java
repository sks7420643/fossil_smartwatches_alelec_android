package com.fossil;

import com.facebook.internal.FileLruCache;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y44 implements Closeable {
    @DexIgnore
    public static /* final */ Logger g; // = Logger.getLogger(y44.class.getName());
    @DexIgnore
    public /* final */ RandomAccessFile a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public b d;
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ byte[] f; // = new byte[16];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements d {
        @DexIgnore
        public boolean a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder b;

        @DexIgnore
        public a(StringBuilder sb) {
            this.b = sb;
        }

        @DexIgnore
        @Override // com.fossil.y44.d
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.a) {
                this.a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ b c; // = new b(0, 0);
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.a + ", length = " + this.b + "]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends InputStream {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public /* synthetic */ c(y44 y44, b bVar, a aVar) {
            this(bVar);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            Object unused = y44.b(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = this.b;
            if (i3 <= 0) {
                return -1;
            }
            if (i2 > i3) {
                i2 = i3;
            }
            y44.this.a(this.a, bArr, i, i2);
            this.a = y44.this.d(this.a + i2);
            this.b -= i2;
            return i2;
        }

        @DexIgnore
        public c(b bVar) {
            this.a = y44.this.d(bVar.a + 4);
            this.b = bVar.b;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() throws IOException {
            if (this.b == 0) {
                return -1;
            }
            y44.this.a.seek((long) this.a);
            int read = y44.this.a.read();
            this.a = y44.this.d(this.a + 1);
            this.b--;
            return read;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(InputStream inputStream, int i) throws IOException;
    }

    @DexIgnore
    public y44(File file) throws IOException {
        if (!file.exists()) {
            a(file);
        }
        this.a = b(file);
        c();
    }

    @DexIgnore
    public static void b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    @DexIgnore
    public final void c() throws IOException {
        this.a.seek(0);
        this.a.readFully(this.f);
        int a2 = a(this.f, 0);
        this.b = a2;
        if (((long) a2) <= this.a.length()) {
            this.c = a(this.f, 4);
            int a3 = a(this.f, 8);
            int a4 = a(this.f, 12);
            this.d = b(a3);
            this.e = b(a4);
            return;
        }
        throw new IOException("File is truncated. Expected length: " + this.b + ", Actual length: " + this.a.length());
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public final int d(int i) {
        int i2 = this.b;
        return i < i2 ? i : (i + 16) - i2;
    }

    @DexIgnore
    public final int e() {
        return this.b - k();
    }

    @DexIgnore
    public synchronized void g() throws IOException {
        if (b()) {
            throw new NoSuchElementException();
        } else if (this.c == 1) {
            a();
        } else {
            int d2 = d(this.d.a + 4 + this.d.b);
            a(d2, this.f, 0, 4);
            int a2 = a(this.f, 0);
            a(this.b, this.c - 1, d2, this.e.a);
            this.c--;
            this.d = new b(d2, a2);
        }
    }

    @DexIgnore
    public int k() {
        if (this.c == 0) {
            return 16;
        }
        b bVar = this.e;
        int i = bVar.a;
        int i2 = this.d.a;
        if (i >= i2) {
            return (i - i2) + 4 + bVar.b + 16;
        }
        return (((i + 4) + bVar.b) + this.b) - i2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(y44.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.b);
        sb.append(", size=");
        sb.append(this.c);
        sb.append(", first=");
        sb.append(this.d);
        sb.append(", last=");
        sb.append(this.e);
        sb.append(", element lengths=[");
        try {
            a(new a(sb));
        } catch (IOException e2) {
            g.log(Level.WARNING, "read error", (Throwable) e2);
        }
        sb.append("]]");
        return sb.toString();
    }

    @DexIgnore
    public static void a(byte[] bArr, int... iArr) {
        int i = 0;
        for (int i2 : iArr) {
            b(bArr, i, i2);
            i += 4;
        }
    }

    @DexIgnore
    public final b b(int i) throws IOException {
        if (i == 0) {
            return b.c;
        }
        this.a.seek((long) i);
        return new b(i, this.a.readInt());
    }

    @DexIgnore
    public static int a(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    @DexIgnore
    public static RandomAccessFile b(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    @DexIgnore
    public final void a(int i, int i2, int i3, int i4) throws IOException {
        a(this.f, i, i2, i3, i4);
        this.a.seek(0);
        this.a.write(this.f);
    }

    @DexIgnore
    public final void b(int i, byte[] bArr, int i2, int i3) throws IOException {
        int d2 = d(i);
        int i4 = d2 + i3;
        int i5 = this.b;
        if (i4 <= i5) {
            this.a.seek((long) d2);
            this.a.write(bArr, i2, i3);
            return;
        }
        int i6 = i5 - d2;
        this.a.seek((long) d2);
        this.a.write(bArr, i2, i6);
        this.a.seek(16);
        this.a.write(bArr, i2 + i6, i3 - i6);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void a(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b2 = b(file2);
        try {
            b2.setLength(4096);
            b2.seek(0);
            byte[] bArr = new byte[16];
            a(bArr, 4096, 0, 0, 0);
            b2.write(bArr);
            b2.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }

    @DexIgnore
    public final void c(int i) throws IOException {
        this.a.setLength((long) i);
        this.a.getChannel().force(true);
    }

    @DexIgnore
    public synchronized boolean b() {
        return this.c == 0;
    }

    @DexIgnore
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public final void a(int i, byte[] bArr, int i2, int i3) throws IOException {
        int d2 = d(i);
        int i4 = d2 + i3;
        int i5 = this.b;
        if (i4 <= i5) {
            this.a.seek((long) d2);
            this.a.readFully(bArr, i2, i3);
            return;
        }
        int i6 = i5 - d2;
        this.a.seek((long) d2);
        this.a.readFully(bArr, i2, i6);
        this.a.seek(16);
        this.a.readFully(bArr, i2 + i6, i3 - i6);
    }

    @DexIgnore
    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized void a(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        b(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        if ((i | i2) < 0 || i2 > bArr.length - i) {
            throw new IndexOutOfBoundsException();
        }
        a(i2);
        boolean b2 = b();
        if (b2) {
            i3 = 16;
        } else {
            i3 = d(this.e.a + 4 + this.e.b);
        }
        b bVar = new b(i3, i2);
        b(this.f, 0, i2);
        b(bVar.a, this.f, 0, 4);
        b(bVar.a + 4, bArr, i, i2);
        a(this.b, this.c + 1, b2 ? bVar.a : this.d.a, bVar.a);
        this.e = bVar;
        this.c++;
        if (b2) {
            this.d = bVar;
        }
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        int i2 = i + 4;
        int e2 = e();
        if (e2 < i2) {
            int i3 = this.b;
            do {
                e2 += i3;
                i3 <<= 1;
            } while (e2 < i2);
            c(i3);
            b bVar = this.e;
            int d2 = d(bVar.a + 4 + bVar.b);
            if (d2 < this.d.a) {
                FileChannel channel = this.a.getChannel();
                channel.position((long) this.b);
                long j = (long) (d2 - 4);
                if (channel.transferTo(16, j, channel) != j) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i4 = this.e.a;
            int i5 = this.d.a;
            if (i4 < i5) {
                int i6 = (this.b + i4) - 16;
                a(i3, this.c, i5, i6);
                this.e = new b(i6, this.e.b);
            } else {
                a(i3, this.c, i5, i4);
            }
            this.b = i3;
        }
    }

    @DexIgnore
    public synchronized void a(d dVar) throws IOException {
        int i = this.d.a;
        for (int i2 = 0; i2 < this.c; i2++) {
            b b2 = b(i);
            dVar.a(new c(this, b2, null), b2.b);
            i = d(b2.a + 4 + b2.b);
        }
    }

    @DexIgnore
    public synchronized void a() throws IOException {
        a(4096, 0, 0, 0);
        this.c = 0;
        this.d = b.c;
        this.e = b.c;
        if (this.b > 4096) {
            c(4096);
        }
        this.b = 4096;
    }
}
