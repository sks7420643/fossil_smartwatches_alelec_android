package com.fossil;

import com.fossil.s3;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r3<K, V> extends s3<K, V> {
    @DexIgnore
    public HashMap<K, s3.c<K, V>> e; // = new HashMap<>();

    @DexIgnore
    @Override // com.fossil.s3
    public s3.c<K, V> a(K k) {
        return this.e.get(k);
    }

    @DexIgnore
    @Override // com.fossil.s3
    public V b(K k, V v) {
        s3.c<K, V> a = a(k);
        if (a != null) {
            return a.b;
        }
        this.e.put(k, a(k, v));
        return null;
    }

    @DexIgnore
    public boolean contains(K k) {
        return this.e.containsKey(k);
    }

    @DexIgnore
    @Override // com.fossil.s3
    public V remove(K k) {
        V v = (V) super.remove(k);
        this.e.remove(k);
        return v;
    }

    @DexIgnore
    public Map.Entry<K, V> b(K k) {
        if (contains(k)) {
            return this.e.get(k).d;
        }
        return null;
    }
}
