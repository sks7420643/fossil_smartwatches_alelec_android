package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* renamed from: com.fossil.do  reason: invalid class name */
public abstract class Cdo<T> extends eo<T> {
    @DexIgnore
    public static /* final */ String h; // = im.a("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.do$a")
    /* renamed from: com.fossil.do$a */
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                Cdo.this.a(context, intent);
            }
        }
    }

    @DexIgnore
    public Cdo(Context context, vp vpVar) {
        super(context, vpVar);
    }

    @DexIgnore
    public abstract void a(Context context, Intent intent);

    @DexIgnore
    @Override // com.fossil.eo
    public void b() {
        im.a().a(h, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        ((eo) this).b.registerReceiver(this.g, d());
    }

    @DexIgnore
    @Override // com.fossil.eo
    public void c() {
        im.a().a(h, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        ((eo) this).b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter d();
}
