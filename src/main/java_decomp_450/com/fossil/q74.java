package com.fossil;

import com.facebook.AccessToken;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q74 implements v74 {
    @DexIgnore
    public static z74 b(JSONObject jSONObject) {
        return new z74(jSONObject.optBoolean("collect_reports", true));
    }

    @DexIgnore
    public static a84 c(JSONObject jSONObject) {
        return new a84(jSONObject.optInt("max_custom_exception_events", 8), 4);
    }

    @DexIgnore
    @Override // com.fossil.v74
    public c84 a(d44 d44, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new c84(a(d44, (long) optInt2, jSONObject), a(jSONObject.getJSONObject("app")), c(jSONObject.getJSONObject(Constants.SESSION)), b(jSONObject.getJSONObject("features")), optInt, optInt2);
    }

    @DexIgnore
    public static b84 a(d44 d44) {
        JSONObject jSONObject = new JSONObject();
        return new c84(a(d44, 3600, jSONObject), null, c(jSONObject), b(jSONObject), 0, 3600);
    }

    @DexIgnore
    public static y74 a(JSONObject jSONObject) throws JSONException {
        return new y74(jSONObject.getString("status"), jSONObject.getString("url"), jSONObject.getString("reports_url"), jSONObject.getString("ndk_reports_url"), jSONObject.optBoolean("update_required", false));
    }

    @DexIgnore
    public static long a(d44 d44, long j, JSONObject jSONObject) {
        if (jSONObject.has(AccessToken.EXPIRES_AT_KEY)) {
            return jSONObject.optLong(AccessToken.EXPIRES_AT_KEY);
        }
        return d44.a() + (j * 1000);
    }
}
