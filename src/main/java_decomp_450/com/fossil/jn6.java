package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn6 extends go5 implements rn6, View.OnClickListener, cy6.g {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public qw6<a55> f;
    @DexIgnore
    public qn6 g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public GradientDrawable s; // = new GradientDrawable();
    @DexIgnore
    public GradientDrawable t; // = new GradientDrawable();
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jn6.v;
        }

        @DexIgnore
        public final jn6 b() {
            return new jn6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public b(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public c(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).b(ob5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public d(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).b(ob5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public e(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).d(ob5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public f(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).d(ob5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public g(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).a(ob5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public h(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).a(ob5.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public i(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).c(ob5.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jn6 a;

        @DexIgnore
        public j(jn6 jn6) {
            this.a = jn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            jn6.a(this.a).c(ob5.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = jn6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            v = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qn6 a(jn6 jn6) {
        qn6 qn6 = jn6.g;
        if (qn6 != null) {
            return qn6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.rn6
    public void c(ob5 ob5) {
        qw6<a55> qw6 = this.f;
        if (qw6 != null) {
            a55 a2 = qw6.a();
            if (a2 != null) {
                View childAt = a2.J.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ob5 != null) {
                        int i2 = kn6.a[ob5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.J.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.s);
                            View childAt3 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.t);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.J.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.s);
                            View childAt5 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.t);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.J.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    ee7.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.s);
                    View childAt7 = linearLayout.getChildAt(1);
                    ee7.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.t);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rn6
    public void d(ob5 ob5) {
        qw6<a55> qw6 = this.f;
        if (qw6 != null) {
            a55 a2 = qw6.a();
            if (a2 != null) {
                View childAt = a2.L.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ob5 != null) {
                        int i2 = kn6.b[ob5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.L.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.s);
                            View childAt3 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.t);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.L.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.s);
                            View childAt5 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.t);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.L.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    ee7.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.s);
                    View childAt7 = linearLayout.getChildAt(1);
                    ee7.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.t);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rn6
    public void e(ob5 ob5) {
        qw6<a55> qw6 = this.f;
        if (qw6 != null) {
            a55 a2 = qw6.a();
            if (a2 != null) {
                View childAt = a2.I.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ob5 != null) {
                        int i2 = kn6.c[ob5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.I.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.s);
                            View childAt3 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.t);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.I.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.s);
                            View childAt5 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.t);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.I.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    ee7.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.s);
                    View childAt7 = linearLayout.getChildAt(1);
                    ee7.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.t);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        ee7.b(view, "v");
        if (view.getId() == 2131361851) {
            v();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        v6.a(requireContext(), 2131100361);
        v6.a(requireContext(), 2131099829);
        this.h = Color.parseColor(eh5.l.a().b("primaryButton"));
        this.i = Color.parseColor(eh5.l.a().b("onPrimaryButton"));
        this.j = Color.parseColor(eh5.l.a().b("primaryButtonBorder"));
        this.p = Color.parseColor(eh5.l.a().b("secondaryButton"));
        this.q = Color.parseColor(eh5.l.a().b("onSecondaryButton"));
        this.r = Color.parseColor(eh5.l.a().b("secondaryButtonBorder"));
        a55 a55 = (a55) qb.a(LayoutInflater.from(getContext()), 2131558606, null, false, a1());
        a55.q.setOnClickListener(new b(this));
        View childAt = a55.J.getChildAt(0);
        if (childAt != null) {
            View childAt2 = ((LinearLayout) childAt).getChildAt(0);
            if (childAt2 != null) {
                childAt2.setOnClickListener(new c(this));
            }
            View childAt3 = a55.J.getChildAt(0);
            if (childAt3 != null) {
                View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                if (childAt4 != null) {
                    childAt4.setOnClickListener(new d(this));
                }
                View childAt5 = a55.L.getChildAt(0);
                if (childAt5 != null) {
                    View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                    if (childAt6 != null) {
                        childAt6.setOnClickListener(new e(this));
                    }
                    View childAt7 = a55.L.getChildAt(0);
                    if (childAt7 != null) {
                        View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                        if (childAt8 != null) {
                            childAt8.setOnClickListener(new f(this));
                        }
                        View childAt9 = a55.I.getChildAt(0);
                        if (childAt9 != null) {
                            View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                            if (childAt10 != null) {
                                childAt10.setOnClickListener(new g(this));
                            }
                            View childAt11 = a55.I.getChildAt(0);
                            if (childAt11 != null) {
                                View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                if (childAt12 != null) {
                                    childAt12.setOnClickListener(new h(this));
                                }
                                View childAt13 = a55.K.getChildAt(0);
                                if (childAt13 != null) {
                                    View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                    if (childAt14 != null) {
                                        childAt14.setOnClickListener(new i(this));
                                    }
                                    View childAt15 = a55.K.getChildAt(0);
                                    if (childAt15 != null) {
                                        View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                        if (childAt16 != null) {
                                            childAt16.setOnClickListener(new j(this));
                                        }
                                        a55.I.a(this.q, this.i);
                                        a55.K.a(this.q, this.i);
                                        a55.J.a(this.q, this.i);
                                        a55.L.a(this.q, this.i);
                                        this.f = new qw6<>(this, a55);
                                        this.s.setShape(0);
                                        this.s.setColor(this.h);
                                        this.s.setStroke(4, this.j);
                                        this.t.setShape(0);
                                        this.t.setColor(this.p);
                                        this.t.setStroke(4, this.r);
                                        ee7.a((Object) a55, "binding");
                                        return a55.d();
                                    }
                                    throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
        }
        throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        qn6 qn6 = this.g;
        if (qn6 != null) {
            qn6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qn6 qn6 = this.g;
        if (qn6 != null) {
            qn6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void a(qn6 qn6) {
        ee7.b(qn6, "presenter");
        jw3.a(qn6);
        ee7.a((Object) qn6, "checkNotNull(presenter)");
        this.g = qn6;
    }

    @DexIgnore
    @Override // com.fossil.rn6
    public void a(ob5 ob5) {
        qw6<a55> qw6 = this.f;
        if (qw6 != null) {
            a55 a2 = qw6.a();
            if (a2 != null) {
                View childAt = a2.K.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (ob5 != null) {
                        int i2 = kn6.d[ob5.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.K.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.s);
                            View childAt3 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.t);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.K.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            ee7.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.s);
                            View childAt5 = linearLayout.getChildAt(1);
                            ee7.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.t);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.K.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    ee7.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.s);
                    View childAt7 = linearLayout.getChildAt(1);
                    ee7.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.t);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rn6
    public void a(int i2, String str) {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = v;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0) && getActivity() != null && str.hashCode() == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).l();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
