package com.fossil;

import com.fossil.lc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic4 extends lc4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ mc4 d;
    @DexIgnore
    public /* final */ lc4.b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends lc4.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public mc4 d;
        @DexIgnore
        public lc4.b e;

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4.a a(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4.a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4.a c(String str) {
            this.a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4.a a(mc4 mc4) {
            this.d = mc4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4.a a(lc4.b bVar) {
            this.e = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.lc4.a
        public lc4 a() {
            return new ic4(this.a, this.b, this.c, this.d, this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.lc4
    public mc4 a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.lc4
    public String b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.lc4
    public String c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.lc4
    public lc4.b d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.lc4
    public String e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof lc4)) {
            return false;
        }
        lc4 lc4 = (lc4) obj;
        String str = this.a;
        if (str != null ? str.equals(lc4.e()) : lc4.e() == null) {
            String str2 = this.b;
            if (str2 != null ? str2.equals(lc4.b()) : lc4.b() == null) {
                String str3 = this.c;
                if (str3 != null ? str3.equals(lc4.c()) : lc4.c() == null) {
                    mc4 mc4 = this.d;
                    if (mc4 != null ? mc4.equals(lc4.a()) : lc4.a() == null) {
                        lc4.b bVar = this.e;
                        if (bVar == null) {
                            if (lc4.d() == null) {
                                return true;
                            }
                        } else if (bVar.equals(lc4.d())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = ((str == null ? 0 : str.hashCode()) ^ 1000003) * 1000003;
        String str2 = this.b;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.c;
        int hashCode3 = (hashCode2 ^ (str3 == null ? 0 : str3.hashCode())) * 1000003;
        mc4 mc4 = this.d;
        int hashCode4 = (hashCode3 ^ (mc4 == null ? 0 : mc4.hashCode())) * 1000003;
        lc4.b bVar = this.e;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return hashCode4 ^ i;
    }

    @DexIgnore
    public String toString() {
        return "InstallationResponse{uri=" + this.a + ", fid=" + this.b + ", refreshToken=" + this.c + ", authToken=" + this.d + ", responseCode=" + this.e + "}";
    }

    @DexIgnore
    public ic4(String str, String str2, String str3, mc4 mc4, lc4.b bVar) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = mc4;
        this.e = bVar;
    }
}
