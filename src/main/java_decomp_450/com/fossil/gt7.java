package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gt7 implements HttpEntity {
    @DexIgnore
    public static /* final */ char[] e; // = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    @DexIgnore
    public /* final */ ct7 a;
    @DexIgnore
    public /* final */ Header b;
    @DexIgnore
    public long c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public gt7(dt7 dt7, String str, Charset charset) {
        str = str == null ? a() : str;
        this.a = new ct7("form-data", charset, str, dt7 == null ? dt7.STRICT : dt7);
        this.b = new BasicHeader("Content-Type", a(str, charset));
        this.d = true;
    }

    @DexIgnore
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        sb.append(str);
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    @DexIgnore
    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    @DexIgnore
    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    @DexIgnore
    public Header getContentEncoding() {
        return null;
    }

    @DexIgnore
    public long getContentLength() {
        if (this.d) {
            this.c = this.a.c();
            this.d = false;
        }
        return this.c;
    }

    @DexIgnore
    public Header getContentType() {
        return this.b;
    }

    @DexIgnore
    public boolean isChunked() {
        return !isRepeatable();
    }

    @DexIgnore
    public boolean isRepeatable() {
        for (at7 at7 : this.a.a()) {
            if (at7.a().getContentLength() < 0) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isStreaming() {
        return !isRepeatable();
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        this.a.a(outputStream);
    }

    @DexIgnore
    public gt7() {
        this(dt7.STRICT, null, null);
    }

    @DexIgnore
    public String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            char[] cArr = e;
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    @DexIgnore
    public void a(at7 at7) {
        this.a.a(at7);
        this.d = true;
    }

    @DexIgnore
    public void a(String str, it7 it7) {
        a(new at7(str, it7));
    }
}
