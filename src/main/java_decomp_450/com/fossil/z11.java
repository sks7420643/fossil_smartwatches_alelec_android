package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z11 implements Parcelable.Creator<s31> {
    @DexIgnore
    public /* synthetic */ z11(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public s31 createFromParcel(Parcel parcel) {
        return new s31(parcel, (zd7) null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public s31[] newArray(int i) {
        return new s31[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public s31 m89createFromParcel(Parcel parcel) {
        return new s31(parcel, (zd7) null);
    }
}
