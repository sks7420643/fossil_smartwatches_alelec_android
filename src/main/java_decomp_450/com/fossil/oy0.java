package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy0 implements Parcelable.Creator<f01> {
    @DexIgnore
    public /* synthetic */ oy0(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public f01 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (ee7.a((Object) readString, (Object) jt0.class.getName())) {
            return jt0.CREATOR.createFromParcel(parcel);
        }
        if (ee7.a((Object) readString, (Object) s31.class.getName())) {
            return s31.CREATOR.createFromParcel(parcel);
        }
        if (ee7.a((Object) readString, (Object) ww0.class.getName())) {
            return ww0.CREATOR.createFromParcel(parcel);
        }
        return null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public f01[] newArray(int i) {
        return new f01[i];
    }
}
