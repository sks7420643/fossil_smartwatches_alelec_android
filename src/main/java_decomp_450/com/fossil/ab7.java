package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ab7 extends za7 {
    @DexIgnore
    public static final boolean a(int[] iArr, int[] iArr2) {
        ee7.b(iArr, "$this$contentEquals");
        ee7.b(iArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(iArr, iArr2);
    }

    @DexIgnore
    public static final boolean a(long[] jArr, long[] jArr2) {
        ee7.b(jArr, "$this$contentEquals");
        ee7.b(jArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(jArr, jArr2);
    }

    @DexIgnore
    public static final boolean a(byte[] bArr, byte[] bArr2) {
        ee7.b(bArr, "$this$contentEquals");
        ee7.b(bArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(bArr, bArr2);
    }

    @DexIgnore
    public static final boolean a(short[] sArr, short[] sArr2) {
        ee7.b(sArr, "$this$contentEquals");
        ee7.b(sArr2, FacebookRequestErrorClassification.KEY_OTHER);
        return Arrays.equals(sArr, sArr2);
    }
}
