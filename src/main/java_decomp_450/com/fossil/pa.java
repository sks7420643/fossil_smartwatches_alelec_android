package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pa {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ pa a;

        @DexIgnore
        public a(pa paVar) {
            this.a = paVar;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            oa a2 = this.a.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.A();
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<oa> a2 = this.a.a(str, i);
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(a2.get(i2).A());
            }
            return arrayList;
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.a.a(i, i2, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a {
        @DexIgnore
        public b(pa paVar) {
            super(paVar);
        }

        @DexIgnore
        public AccessibilityNodeInfo findFocus(int i) {
            oa b = ((a) this).a.b(i);
            if (b == null) {
                return null;
            }
            return b.A();
        }
    }

    @DexIgnore
    public pa() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.a = new b(this);
        } else if (i >= 16) {
            this.a = new a(this);
        } else {
            this.a = null;
        }
    }

    @DexIgnore
    public oa a(int i) {
        return null;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public List<oa> a(String str, int i) {
        return null;
    }

    @DexIgnore
    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    @DexIgnore
    public oa b(int i) {
        return null;
    }

    @DexIgnore
    public pa(Object obj) {
        this.a = obj;
    }
}
