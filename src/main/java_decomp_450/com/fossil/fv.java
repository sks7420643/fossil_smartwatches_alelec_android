package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fv extends Exception {
    @DexIgnore
    public /* final */ vu networkResponse;
    @DexIgnore
    public long networkTimeMs;

    @DexIgnore
    public fv() {
        this.networkResponse = null;
    }

    @DexIgnore
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }

    @DexIgnore
    public void setNetworkTimeMs(long j) {
        this.networkTimeMs = j;
    }

    @DexIgnore
    public fv(vu vuVar) {
        this.networkResponse = vuVar;
    }

    @DexIgnore
    public fv(String str) {
        super(str);
        this.networkResponse = null;
    }

    @DexIgnore
    public fv(String str, Throwable th) {
        super(str, th);
        this.networkResponse = null;
    }

    @DexIgnore
    public fv(Throwable th) {
        super(th);
        this.networkResponse = null;
    }
}
