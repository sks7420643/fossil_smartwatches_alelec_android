package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kk extends pk implements mk {
    @DexIgnore
    public kk(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static kk a(ViewGroup viewGroup) {
        return (kk) pk.c(viewGroup);
    }

    @DexIgnore
    @Override // com.fossil.mk
    public void b(View view) {
        ((pk) this).a.b(view);
    }

    @DexIgnore
    @Override // com.fossil.mk
    public void a(View view) {
        ((pk) this).a.a(view);
    }
}
