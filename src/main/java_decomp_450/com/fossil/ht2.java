package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht2 extends jt2<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ ht2 zza; // = new ht2();

    @DexIgnore
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        Comparable comparable = (Comparable) obj;
        Comparable comparable2 = (Comparable) obj2;
        or2.a(comparable);
        or2.a(comparable2);
        return comparable.compareTo(comparable2);
    }

    @DexIgnore
    public final String toString() {
        return "Ordering.natural()";
    }
}
