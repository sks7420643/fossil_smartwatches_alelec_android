package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gx1 implements rx1.d {
    @DexIgnore
    public /* final */ SQLiteDatabase a;

    @DexIgnore
    public gx1(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @DexIgnore
    public static rx1.d a(SQLiteDatabase sQLiteDatabase) {
        return new gx1(sQLiteDatabase);
    }

    @DexIgnore
    @Override // com.fossil.rx1.d
    public Object a() {
        return this.a.beginTransaction();
    }
}
