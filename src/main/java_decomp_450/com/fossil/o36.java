package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.em4;
import com.fossil.wo5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o36 extends go5 implements n36, cy6.g {
    @DexIgnore
    public qw6<a75> f;
    @DexIgnore
    public m36 g;
    @DexIgnore
    public wo5 h;
    @DexIgnore
    public em4 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public a06 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements em4.b {
        @DexIgnore
        public /* final */ /* synthetic */ o36 a;

        @DexIgnore
        public b(o36 o36) {
            this.a = o36;
        }

        @DexIgnore
        @Override // com.fossil.em4.b
        public void a(WatchApp watchApp) {
            ee7.b(watchApp, "watchApp");
            o36.a(this.a).a(watchApp.getWatchappId());
        }

        @DexIgnore
        @Override // com.fossil.em4.b
        public void b(WatchApp watchApp) {
            ee7.b(watchApp, "watchApp");
            xg5.a(xg5.b, this.a.getContext(), ve5.a.c(watchApp.getWatchappId()), false, false, false, (Integer) null, 60, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements wo5.c {
        @DexIgnore
        public /* final */ /* synthetic */ o36 a;

        @DexIgnore
        public c(o36 o36) {
            this.a = o36;
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a() {
            o36.a(this.a).h();
        }

        @DexIgnore
        @Override // com.fossil.wo5.c
        public void a(Category category) {
            ee7.b(category, "category");
            o36.a(this.a).a(category);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o36 a;

        @DexIgnore
        public d(o36 o36) {
            this.a = o36;
        }

        @DexIgnore
        public final void onClick(View view) {
            o36.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o36 a;

        @DexIgnore
        public e(o36 o36) {
            this.a = o36;
        }

        @DexIgnore
        public final void onClick(View view) {
            o36.a(this.a).j();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ m36 a(o36 o36) {
        m36 m36 = o36.g;
        if (m36 != null) {
            return m36;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void H(String str) {
        ee7.b(str, "content");
        qw6<a75> qw6 = this.f;
        if (qw6 != null) {
            a75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "it.tvWatchappsDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void R(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.z.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void b(WatchApp watchApp) {
        if (watchApp != null) {
            em4 em4 = this.i;
            if (em4 != null) {
                em4.b(watchApp.getWatchappId());
                c(watchApp);
                return;
            }
            ee7.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(WatchApp watchApp) {
        qw6<a75> qw6 = this.f;
        if (qw6 != null) {
            a75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                ee7.a((Object) flexibleTextView, "binding.tvSelectedWatchapps");
                flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), watchApp.getNameKey(), watchApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.w;
                ee7.a((Object) flexibleTextView2, "binding.tvWatchappsDetail");
                flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                em4 em4 = this.i;
                if (em4 != null) {
                    int a3 = em4.a(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.t.scrollToPosition(a3);
                        return;
                    }
                    return;
                }
                ee7.d("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void d(String str) {
        ee7.b(str, "category");
        qw6<a75> qw6 = this.f;
        if (qw6 != null) {
            a75 a2 = qw6.a();
            if (a2 != null) {
                wo5 wo5 = this.h;
                if (wo5 != null) {
                    int a3 = wo5.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        wo5 wo52 = this.h;
                        if (wo52 != null) {
                            wo52.a(a3);
                            a2.s.smoothScrollToPosition(a3);
                            return;
                        }
                        ee7.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void f(String str) {
        ee7.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.y;
            ee7.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public final void f1() {
        if (isActive()) {
            em4 em4 = this.i;
            if (em4 != null) {
                em4.c();
            } else {
                ee7.d("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void k(boolean z) {
        if (isActive()) {
            qw6<a75> qw6 = this.f;
            if (qw6 != null) {
                a75 a2 = qw6.a();
                if (a2 != null) {
                    a2.v.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? 2131231083 : 0, 0);
                    Drawable[] compoundDrawables = a2.v.getCompoundDrawables();
                    ee7.a((Object) compoundDrawables, "it.tvSelectedWatchapps.getCompoundDrawables()");
                    if (compoundDrawables[2] != null) {
                        Drawable drawable = compoundDrawables[2];
                        if (drawable != null) {
                            drawable.setColorFilter(getResources().getColor(2131099972), PorterDuff.Mode.MULTIPLY);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            rj4 rj4 = this.j;
            if (rj4 != null) {
                he a2 = je.a(dianaCustomizeEditActivity, rj4).a(a06.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                a06 a06 = (a06) a2;
                this.p = a06;
                m36 m36 = this.g;
                if (m36 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (a06 != null) {
                    m36.a(a06);
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 101) {
            if (i2 != 105) {
                if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING")) != null) {
                    m36 m36 = this.g;
                    if (m36 != null) {
                        m36.a("commute-time", commuteTimeWatchAppSetting);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                }
            } else if (i3 == -1 && intent != null && (weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING")) != null) {
                m36 m362 = this.g;
                if (m362 != null) {
                    m362.a("weather", weatherWatchAppSetting);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                m36 m363 = this.g;
                if (m363 != null) {
                    ee7.a((Object) stringExtra, "selectedWatchAppId");
                    m363.a(stringExtra);
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        a75 a75 = (a75) qb.a(layoutInflater, 2131558636, viewGroup, false, a1());
        PortfolioApp.g0.c().f().a(new q36(this)).a(this);
        this.f = new qw6<>(this, a75);
        ee7.a((Object) a75, "binding");
        return a75.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        m36 m36 = this.g;
        if (m36 != null) {
            m36.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        m36 m36 = this.g;
        if (m36 != null) {
            m36.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        em4 em4 = new em4(null, null, 3, null);
        em4.a(new b(this));
        this.i = em4;
        wo5 wo5 = new wo5(null, null, 3, null);
        wo5.a(new c(this));
        this.h = wo5;
        qw6<a75> qw6 = this.f;
        if (qw6 != null) {
            a75 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.r.setBackgroundColor(Color.parseColor(b2));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                wo5 wo52 = this.h;
                if (wo52 != null) {
                    recyclerView.setAdapter(wo52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    em4 em42 = this.i;
                    if (em42 != null) {
                        recyclerView2.setAdapter(em42);
                        a2.y.setOnClickListener(new d(this));
                        a2.v.setOnClickListener(new e(this));
                        return;
                    }
                    ee7.d("mWatchAppAdapter");
                    throw null;
                }
                ee7.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void v(List<WatchApp> list) {
        ee7.b(list, "watchApps");
        em4 em4 = this.i;
        if (em4 != null) {
            em4.a(list);
        } else {
            ee7.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(m36 m36) {
        ee7.b(m36, "presenter");
        this.g = m36;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void a(List<Category> list) {
        ee7.b(list, "categories");
        wo5 wo5 = this.h;
        if (wo5 != null) {
            wo5.a(list);
        } else {
            ee7.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void a(boolean z, String str, String str2, String str3) {
        ee7.b(str, "watchAppId");
        ee7.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        qw6<a75> qw6 = this.f;
        if (qw6 != null) {
            a75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.x;
                ee7.a((Object) flexibleTextView2, "it.tvWatchappsPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.y;
                    ee7.a((Object) flexibleTextView3, "it.tvWatchappsSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.y;
                        ee7.a((Object) flexibleTextView4, "it.tvWatchappsSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.y;
                    ee7.a((Object) flexibleTextView5, "it.tvWatchappsSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.y;
                ee7.a((Object) flexibleTextView6, "it.tvWatchappsSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void c(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.y.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.n36
    public void a(String str, String str2, String str3) {
        ee7.b(str, "topWatchApp");
        ee7.b(str2, "middleWatchApp");
        ee7.b(str3, "bottomWatchApp");
        WatchAppSearchActivity.z.a(this, str, str2, str3);
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        ee7.b(str, "tag");
        if (ee7.a((Object) str, (Object) "REQUEST_NOTIFICATION_ACCESS")) {
            if (i2 == 2131363229) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (ee7.a((Object) str, (Object) bx6.c.a())) {
            if (i2 == 2131363307 && (activity3 = getActivity()) != null) {
                if (!ku7.a(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    px6.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity3 != null) {
                    ((cl5) activity3).k();
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (ee7.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363307 && (activity2 = getActivity()) != null) {
                if (activity2 != null) {
                    ((cl5) activity2).k();
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (ee7.a((Object) str, (Object) "LOCATION_PERMISSION_TAG")) {
            if (i2 != 2131363307 || (activity = getActivity()) == null) {
                return;
            }
            if (activity != null) {
                ((cl5) activity).k();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (ee7.a((Object) str, (Object) InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363307) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }
}
