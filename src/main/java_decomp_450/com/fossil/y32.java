package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.v02;
import com.fossil.v02.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y32<O extends v02.d> extends m22 {
    @DexIgnore
    public /* final */ z02<O> c;

    @DexIgnore
    public y32(z02<O> z02) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = z02;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T a(T t) {
        this.c.a((r12) t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final void a(m42 m42) {
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final <A extends v02.b, T extends r12<? extends i12, A>> T b(T t) {
        this.c.b(t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final Context e() {
        return this.c.f();
    }

    @DexIgnore
    @Override // com.fossil.a12
    public final Looper f() {
        return this.c.h();
    }
}
