package com.fossil;

import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dq7 {
    @DexIgnore
    public static final dq7 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements dq7 {
        @DexIgnore
        @Override // com.fossil.dq7
        public void a(int i, tp7 tp7) {
        }

        @DexIgnore
        @Override // com.fossil.dq7
        public boolean a(int i, ar7 ar7, int i2, boolean z) throws IOException {
            ar7.skip((long) i2);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.dq7
        public boolean a(int i, List<up7> list) {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.dq7
        public boolean a(int i, List<up7> list, boolean z) {
            return true;
        }
    }

    @DexIgnore
    void a(int i, tp7 tp7);

    @DexIgnore
    boolean a(int i, ar7 ar7, int i2, boolean z) throws IOException;

    @DexIgnore
    boolean a(int i, List<up7> list);

    @DexIgnore
    boolean a(int i, List<up7> list, boolean z);
}
