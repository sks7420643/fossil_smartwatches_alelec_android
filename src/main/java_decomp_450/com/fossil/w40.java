package com.fossil;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w40 extends z40<Bitmap> {
    @DexIgnore
    public w40(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void c(Bitmap bitmap) {
        ((ImageView) ((d50) this).a).setImageBitmap(bitmap);
    }
}
