package com.fossil;

import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw5 implements Factory<kw5> {
    @DexIgnore
    public static kw5 a(jw5 jw5, DNDSettingsDatabase dNDSettingsDatabase) {
        return new kw5(jw5, dNDSettingsDatabase);
    }
}
