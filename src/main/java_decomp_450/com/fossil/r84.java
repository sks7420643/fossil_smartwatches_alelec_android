package com.fossil;

import java.io.IOException;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r84 {
    @DexIgnore
    String a(Object obj);

    @DexIgnore
    void a(Object obj, Writer writer) throws IOException;
}
