package com.fossil;

import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn3 implements v02.d.f {
    @DexIgnore
    public static /* final */ fn3 j; // = new fn3(false, false, null, false, null, null, false, null, null);
    @DexIgnore
    public /* final */ boolean a; // = false;
    @DexIgnore
    public /* final */ boolean b; // = false;
    @DexIgnore
    public /* final */ String c; // = null;
    @DexIgnore
    public /* final */ boolean d; // = false;
    @DexIgnore
    public /* final */ String e; // = null;
    @DexIgnore
    public /* final */ String f; // = null;
    @DexIgnore
    public /* final */ boolean g; // = false;
    @DexIgnore
    public /* final */ Long h; // = null;
    @DexIgnore
    public /* final */ Long i; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public fn3(boolean z, boolean z2, String str, boolean z3, String str2, String str3, boolean z4, Long l, Long l2) {
    }

    @DexIgnore
    public final Long a() {
        return this.h;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.f;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fn3)) {
            return false;
        }
        fn3 fn3 = (fn3) obj;
        return this.a == fn3.a && this.b == fn3.b && y62.a(this.c, fn3.c) && this.d == fn3.d && this.g == fn3.g && y62.a(this.e, fn3.e) && y62.a(this.f, fn3.f) && y62.a(this.h, fn3.h) && y62.a(this.i, fn3.i);
    }

    @DexIgnore
    public final Long f() {
        return this.i;
    }

    @DexIgnore
    public final String g() {
        return this.c;
    }

    @DexIgnore
    public final boolean h() {
        return this.d;
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(Boolean.valueOf(this.a), Boolean.valueOf(this.b), this.c, Boolean.valueOf(this.d), Boolean.valueOf(this.g), this.e, this.f, this.h, this.i);
    }

    @DexIgnore
    public final boolean i() {
        return this.b;
    }

    @DexIgnore
    public final boolean j() {
        return this.a;
    }

    @DexIgnore
    public final boolean k() {
        return this.g;
    }
}
