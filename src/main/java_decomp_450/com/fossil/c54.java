package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c54 extends v54.b {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    @Override // com.fossil.v54.b
    public String a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.b
    public String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.b)) {
            return false;
        }
        v54.b bVar = (v54.b) obj;
        if (!this.a.equals(bVar.a()) || !this.b.equals(bVar.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CustomAttribute{key=" + this.a + ", value=" + this.b + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.b.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.v54.b.a
        public v54.b.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null key");
        }

        @DexIgnore
        @Override // com.fossil.v54.b.a
        public v54.b.a b(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null value");
        }

        @DexIgnore
        @Override // com.fossil.v54.b.a
        public v54.b a() {
            String str = "";
            if (this.a == null) {
                str = str + " key";
            }
            if (this.b == null) {
                str = str + " value";
            }
            if (str.isEmpty()) {
                return new c54(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public c54(String str, String str2) {
        this.a = str;
        this.b = str2;
    }
}
