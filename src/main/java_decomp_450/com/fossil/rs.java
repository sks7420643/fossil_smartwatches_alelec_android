package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.lifecycle.LifecycleCoroutineDispatcher;
import com.fossil.tt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rs {
    @DexIgnore
    public static /* final */ Bitmap.Config[] b; // = (Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888});
    @DexIgnore
    public /* final */ is a; // = is.a.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ b c; // = new b(wr.a, qj7.c().g());
        @DexIgnore
        public static /* final */ a d; // = new a(null);
        @DexIgnore
        public /* final */ Lifecycle a;
        @DexIgnore
        public /* final */ ti7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final b a() {
                return b.c;
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public b(Lifecycle lifecycle, ti7 ti7) {
            ee7.b(lifecycle, "lifecycle");
            ee7.b(ti7, "mainDispatcher");
            this.a = lifecycle;
            this.b = ti7;
        }

        @DexIgnore
        public final Lifecycle a() {
            return this.a;
        }

        @DexIgnore
        public final ti7 b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b);
        }

        @DexIgnore
        public int hashCode() {
            Lifecycle lifecycle = this.a;
            int i = 0;
            int hashCode = (lifecycle != null ? lifecycle.hashCode() : 0) * 31;
            ti7 ti7 = this.b;
            if (ti7 != null) {
                i = ti7.hashCode();
            }
            return hashCode + i;
        }

        @DexIgnore
        public String toString() {
            return "LifecycleInfo(lifecycle=" + this.a + ", mainDispatcher=" + this.b + ")";
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final st a(it itVar, Context context) {
        ee7.b(itVar, "request");
        ee7.b(context, "context");
        st t = itVar.t();
        vt u = itVar.u();
        if (t != null) {
            return t;
        }
        if (u instanceof wt) {
            return tt.a.a(tt.a, ((wt) u).getView(), false, 2, null);
        }
        return new mt(context);
    }

    @DexIgnore
    public final boolean b(it itVar) {
        return itVar.v().isEmpty() || t97.a(b, itVar.d());
    }

    @DexIgnore
    public final b c(it itVar) {
        ee7.b(itVar, "request");
        if (itVar instanceof et) {
            Lifecycle a2 = a((et) itVar);
            if (a2 != null) {
                return new b(a2, LifecycleCoroutineDispatcher.e.a(qj7.c().g(), a2));
            }
            return b.d.a();
        }
        throw new p87();
    }

    @DexIgnore
    public final qt a(it itVar, st stVar) {
        ee7.b(itVar, "request");
        ee7.b(stVar, "sizeResolver");
        qt s = itVar.s();
        if (s != null) {
            return s;
        }
        if (stVar instanceof tt) {
            View view = ((tt) stVar).getView();
            if (view instanceof ImageView) {
                return iu.a((ImageView) view);
            }
        }
        vt u = itVar.u();
        if (u instanceof wt) {
            View view2 = ((wt) u).getView();
            if (view2 instanceof ImageView) {
                return iu.a((ImageView) view2);
            }
        }
        return qt.FILL;
    }

    @DexIgnore
    public final boolean a(it itVar) {
        ee7.b(itVar, "request");
        int i = ss.a[itVar.r().ordinal()];
        if (i == 1) {
            return false;
        }
        if (i == 2) {
            return true;
        }
        if (i == 3) {
            vt u = itVar.u();
            View view = null;
            if (!(u instanceof wt)) {
                u = null;
            }
            wt wtVar = (wt) u;
            if (wtVar != null) {
                view = wtVar.getView();
            }
            if (view instanceof ImageView) {
                return true;
            }
            return itVar.t() == null && !(itVar.u() instanceof wt);
        }
        throw new p87();
    }

    @DexIgnore
    public final ir a(it itVar, rt rtVar, qt qtVar, boolean z) {
        ee7.b(itVar, "request");
        ee7.b(rtVar, "size");
        ee7.b(qtVar, "scale");
        Bitmap.Config d = b(itVar) && a(itVar, rtVar) ? itVar.d() : Bitmap.Config.ARGB_8888;
        return new ir(d, itVar.e(), qtVar, a(itVar), itVar.c() && itVar.v().isEmpty() && d != Bitmap.Config.ALPHA_8, itVar.k(), itVar.p(), z ? itVar.o() : dt.DISABLED, itVar.g());
    }

    @DexIgnore
    public final boolean a(it itVar, Bitmap.Config config) {
        ee7.b(itVar, "request");
        ee7.b(config, "requestedConfig");
        if (!iu.b(config)) {
            return true;
        }
        if (!itVar.b()) {
            return false;
        }
        vt u = itVar.u();
        if (u instanceof wt) {
            View view = ((wt) u).getView();
            if (view.isAttachedToWindow() && !view.isHardwareAccelerated()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean a(it itVar, rt rtVar) {
        return a(itVar, itVar.d()) && this.a.a(rtVar);
    }

    @DexIgnore
    public final Lifecycle a(et etVar) {
        if (etVar.z() != null) {
            return etVar.z();
        }
        if (!(etVar.u() instanceof wt)) {
            return fu.a(etVar.x());
        }
        Context context = ((wt) etVar.u()).getView().getContext();
        ee7.a((Object) context, "target.view.context");
        return fu.a(context);
    }
}
