package com.fossil;

import android.os.Bundle;
import android.text.Spanned;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface hq6 extends dl4<gq6> {
    @DexIgnore
    void A(boolean z);

    @DexIgnore
    void E(boolean z);

    @DexIgnore
    void P(String str);

    @DexIgnore
    void T0();

    @DexIgnore
    void V();

    @DexIgnore
    void a(Bundle bundle);

    @DexIgnore
    void a(Spanned spanned);

    @DexIgnore
    void a(fb5 fb5);

    @DexIgnore
    void a(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    void a(MFUser mFUser);

    @DexIgnore
    void a(boolean z, boolean z2, String str);

    @DexIgnore
    void b(Spanned spanned);

    @DexIgnore
    void b(boolean z, String str);

    @DexIgnore
    void c(Spanned spanned);

    @DexIgnore
    void c(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void d(Spanned spanned);

    @DexIgnore
    void e();

    @DexIgnore
    void f();

    @DexIgnore
    void g();

    @DexIgnore
    void h(int i, String str);

    @DexIgnore
    void r0();
}
