package com.fossil;

import com.fossil.pf;
import com.fossil.qf;
import com.fossil.sf;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kf<K, V> extends qf<V> implements sf.a {
    @DexIgnore
    public pf.a<V> A; // = new a();
    @DexIgnore
    public /* final */ jf<K, V> t;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public int x; // = 0;
    @DexIgnore
    public boolean y; // = false;
    @DexIgnore
    public /* final */ boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pf.a<V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.pf.a
        public void a(int i, pf<V> pfVar) {
            if (pfVar.a()) {
                kf.this.c();
            } else if (!kf.this.h()) {
                List<T> list = pfVar.a;
                boolean z = true;
                if (i == 0) {
                    kf kfVar = kf.this;
                    ((qf) kfVar).e.a(pfVar.b, list, pfVar.c, pfVar.d, kfVar);
                    kf kfVar2 = kf.this;
                    if (((qf) kfVar2).f == -1) {
                        ((qf) kfVar2).f = pfVar.b + pfVar.d + (list.size() / 2);
                    }
                } else {
                    kf kfVar3 = kf.this;
                    boolean z2 = ((qf) kfVar3).f > ((qf) kfVar3).e.f();
                    kf kfVar4 = kf.this;
                    boolean z3 = kfVar4.z && ((qf) kfVar4).e.b(((qf) kfVar4).d.d, ((qf) kfVar4).h, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            kf kfVar5 = kf.this;
                            ((qf) kfVar5).e.a(list, kfVar5);
                        } else {
                            kf kfVar6 = kf.this;
                            kfVar6.x = 0;
                            kfVar6.v = 0;
                        }
                    } else if (i != 2) {
                        throw new IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        kf kfVar7 = kf.this;
                        ((qf) kfVar7).e.b(list, kfVar7);
                    } else {
                        kf kfVar8 = kf.this;
                        kfVar8.w = 0;
                        kfVar8.u = 0;
                    }
                    kf kfVar9 = kf.this;
                    if (kfVar9.z) {
                        if (z2) {
                            if (kfVar9.u != 1 && ((qf) kfVar9).e.b(kfVar9.y, ((qf) kfVar9).d.d, ((qf) kfVar9).h, kfVar9)) {
                                kf.this.u = 0;
                            }
                        } else if (kfVar9.v != 1 && ((qf) kfVar9).e.a(kfVar9.y, ((qf) kfVar9).d.d, ((qf) kfVar9).h, kfVar9)) {
                            kf.this.v = 0;
                        }
                    }
                }
                kf kfVar10 = kf.this;
                if (((qf) kfVar10).c != null) {
                    boolean z4 = ((qf) kfVar10).e.size() == 0;
                    boolean z5 = !z4 && i == 2 && pfVar.a.size() == 0;
                    if (!(!z4 && i == 1 && pfVar.a.size() == 0)) {
                        z = false;
                    }
                    kf.this.a(z4, z5, z);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public b(int i, Object obj) {
            this.a = i;
            this.b = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.jf<K, V> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            if (!kf.this.h()) {
                if (kf.this.t.isInvalid()) {
                    kf.this.c();
                    return;
                }
                kf kfVar = kf.this;
                kfVar.t.dispatchLoadBefore(this.a, this.b, ((qf) kfVar).d.a, ((qf) kfVar).a, kfVar.A);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public c(int i, Object obj) {
            this.a = i;
            this.b = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.jf<K, V> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            if (!kf.this.h()) {
                if (kf.this.t.isInvalid()) {
                    kf.this.c();
                    return;
                }
                kf kfVar = kf.this;
                kfVar.t.dispatchLoadAfter(this.a, this.b, ((qf) kfVar).d.a, ((qf) kfVar).a, kfVar.A);
            }
        }
    }

    @DexIgnore
    public kf(jf<K, V> jfVar, Executor executor, Executor executor2, qf.c<V> cVar, qf.f fVar, K k, int i) {
        super(new sf(), executor, executor2, cVar, fVar);
        boolean z2 = false;
        this.t = jfVar;
        ((qf) this).f = i;
        if (jfVar.isInvalid()) {
            c();
        } else {
            jf<K, V> jfVar2 = this.t;
            qf.f fVar2 = ((qf) this).d;
            jfVar2.dispatchLoadInitial(k, fVar2.e, fVar2.a, fVar2.c, ((qf) this).a, this.A);
        }
        if (this.t.supportsPageDropping() && ((qf) this).d.d != Integer.MAX_VALUE) {
            z2 = true;
        }
        this.z = z2;
    }

    @DexIgnore
    public static int c(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    public static int d(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void a(qf<V> qfVar, qf.e eVar) {
        sf<T> sfVar = qfVar.e;
        int g = ((qf) this).e.g() - sfVar.g();
        int h = ((qf) this).e.h() - sfVar.h();
        int l = sfVar.l();
        int e = sfVar.e();
        if (sfVar.isEmpty() || g < 0 || h < 0 || ((qf) this).e.l() != Math.max(l - g, 0) || ((qf) this).e.e() != Math.max(e - h, 0) || ((qf) this).e.k() != sfVar.k() + g + h) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (g != 0) {
            int min = Math.min(l, g);
            int i = g - min;
            int e2 = sfVar.e() + sfVar.k();
            if (min != 0) {
                eVar.a(e2, min);
            }
            if (i != 0) {
                eVar.b(e2 + min, i);
            }
        }
        if (h != 0) {
            int min2 = Math.min(e, h);
            int i2 = h - min2;
            if (min2 != 0) {
                eVar.a(e, min2);
            }
            if (i2 != 0) {
                eVar.b(0, i2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b() {
        this.u = 2;
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void c(int i, int i2) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void d(int i) {
        int d = d(((qf) this).d.b, i, ((qf) this).e.e());
        int c2 = c(((qf) this).d.b, i, ((qf) this).e.e() + ((qf) this).e.k());
        int max = Math.max(d, this.w);
        this.w = max;
        if (max > 0) {
            l();
        }
        int max2 = Math.max(c2, this.x);
        this.x = max2;
        if (max2 > 0) {
            k();
        }
    }

    @DexIgnore
    @Override // com.fossil.qf
    public Object e() {
        return this.t.getKey(((qf) this).f, ((qf) this).g);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public boolean g() {
        return true;
    }

    @DexIgnore
    public final void k() {
        if (this.v == 0) {
            this.v = 1;
            ((qf) this).b.execute(new c(((((qf) this).e.e() + ((qf) this).e.k()) - 1) + ((qf) this).e.j(), ((qf) this).e.d()));
        }
    }

    @DexIgnore
    public final void l() {
        if (this.u == 0) {
            this.u = 1;
            ((qf) this).b.execute(new b(((qf) this).e.e() + ((qf) this).e.j(), ((qf) this).e.c()));
        }
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i, int i2, int i3) {
        int i4 = (this.x - i2) - i3;
        this.x = i4;
        this.v = 0;
        if (i4 > 0) {
            k();
        }
        d(i, i2);
        e(i + i2, i3);
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.qf
    public lf<?, V> d() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i) {
        boolean z2 = false;
        e(0, i);
        if (((qf) this).e.e() > 0 || ((qf) this).e.l() > 0) {
            z2 = true;
        }
        this.y = z2;
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i, int i2, int i3) {
        int i4 = (this.w - i2) - i3;
        this.w = i4;
        this.u = 0;
        if (i4 > 0) {
            l();
        }
        d(i, i2);
        e(0, i3);
        e(i3);
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a() {
        this.v = 2;
    }

    @DexIgnore
    @Override // com.fossil.sf.a
    public void a(int i, int i2) {
        d(i, i2);
    }
}
