package com.fossil;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fi implements zi, yi {
    @DexIgnore
    public static /* final */ TreeMap<Integer, fi> i; // = new TreeMap<>();
    @DexIgnore
    public volatile String a;
    @DexIgnore
    public /* final */ long[] b;
    @DexIgnore
    public /* final */ double[] c;
    @DexIgnore
    public /* final */ String[] d;
    @DexIgnore
    public /* final */ byte[][] e;
    @DexIgnore
    public /* final */ int[] f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements yi {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public a(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // com.fossil.yi
        public void bindBlob(int i, byte[] bArr) {
            this.a.bindBlob(i, bArr);
        }

        @DexIgnore
        @Override // com.fossil.yi
        public void bindDouble(int i, double d) {
            this.a.bindDouble(i, d);
        }

        @DexIgnore
        @Override // com.fossil.yi
        public void bindLong(int i, long j) {
            this.a.bindLong(i, j);
        }

        @DexIgnore
        @Override // com.fossil.yi
        public void bindNull(int i) {
            this.a.bindNull(i);
        }

        @DexIgnore
        @Override // com.fossil.yi
        public void bindString(int i, String str) {
            this.a.bindString(i, str);
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }
    }

    @DexIgnore
    public fi(int i2) {
        this.g = i2;
        int i3 = i2 + 1;
        this.f = new int[i3];
        this.b = new long[i3];
        this.c = new double[i3];
        this.d = new String[i3];
        this.e = new byte[i3][];
    }

    @DexIgnore
    public static fi a(zi ziVar) {
        fi b2 = b(ziVar.b(), ziVar.a());
        ziVar.a(new a(b2));
        return b2;
    }

    @DexIgnore
    public static fi b(String str, int i2) {
        synchronized (i) {
            Map.Entry<Integer, fi> ceilingEntry = i.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                i.remove(ceilingEntry.getKey());
                fi value = ceilingEntry.getValue();
                value.a(str, i2);
                return value;
            }
            fi fiVar = new fi(i2);
            fiVar.a(str, i2);
            return fiVar;
        }
    }

    @DexIgnore
    public static void e() {
        if (i.size() > 15) {
            int size = i.size() - 10;
            Iterator<Integer> it = i.descendingKeySet().iterator();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    it.next();
                    it.remove();
                    size = i2;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindBlob(int i2, byte[] bArr) {
        this.f[i2] = 5;
        this.e[i2] = bArr;
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindDouble(int i2, double d2) {
        this.f[i2] = 3;
        this.c[i2] = d2;
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindLong(int i2, long j) {
        this.f[i2] = 2;
        this.b[i2] = j;
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindNull(int i2) {
        this.f[i2] = 1;
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindString(int i2, String str) {
        this.f[i2] = 4;
        this.d[i2] = str;
    }

    @DexIgnore
    public void c() {
        synchronized (i) {
            i.put(Integer.valueOf(this.g), this);
            e();
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @DexIgnore
    public void a(String str, int i2) {
        this.a = str;
        this.h = i2;
    }

    @DexIgnore
    @Override // com.fossil.zi
    public int a() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.zi
    public void a(yi yiVar) {
        for (int i2 = 1; i2 <= this.h; i2++) {
            int i3 = this.f[i2];
            if (i3 == 1) {
                yiVar.bindNull(i2);
            } else if (i3 == 2) {
                yiVar.bindLong(i2, this.b[i2]);
            } else if (i3 == 3) {
                yiVar.bindDouble(i2, this.c[i2]);
            } else if (i3 == 4) {
                yiVar.bindString(i2, this.d[i2]);
            } else if (i3 == 5) {
                yiVar.bindBlob(i2, this.e[i2]);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zi
    public String b() {
        return this.a;
    }

    @DexIgnore
    public void a(fi fiVar) {
        int a2 = fiVar.a() + 1;
        System.arraycopy(fiVar.f, 0, this.f, 0, a2);
        System.arraycopy(fiVar.b, 0, this.b, 0, a2);
        System.arraycopy(fiVar.d, 0, this.d, 0, a2);
        System.arraycopy(fiVar.e, 0, this.e, 0, a2);
        System.arraycopy(fiVar.c, 0, this.c, 0, a2);
    }
}
