package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d20 implements cx<Uri, Bitmap> {
    @DexIgnore
    public /* final */ n20 a;
    @DexIgnore
    public /* final */ dz b;

    @DexIgnore
    public d20(n20 n20, dz dzVar) {
        this.a = n20;
        this.b = dzVar;
    }

    @DexIgnore
    public boolean a(Uri uri, ax axVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @DexIgnore
    public uy<Bitmap> a(Uri uri, int i, int i2, ax axVar) {
        uy<Drawable> a2 = this.a.a(uri, i, i2, axVar);
        if (a2 == null) {
            return null;
        }
        return t10.a(this.b, a2.get(), i, i2);
    }
}
