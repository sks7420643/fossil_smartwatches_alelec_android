package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn5 extends fl4<fl4.b, a, fl4.a> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fl4.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public a(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.information.domain.usecase.GetUser", f = "GetUser.kt", l = {13}, m = "run")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ mn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(mn5 mn5, fb7 fb7) {
            super(fb7);
            this.this$0 = mn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((fl4.b) null, this);
        }
    }

    @DexIgnore
    public mn5(UserRepository userRepository) {
        ee7.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.fl4
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.fl4.b r5, com.fossil.fb7<java.lang.Object> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.fossil.mn5.b
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.mn5$b r0 = (com.fossil.mn5.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.mn5$b r0 = new com.fossil.mn5$b
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r5 = r0.L$1
            com.fossil.fl4$b r5 = (com.fossil.fl4.b) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.mn5 r5 = (com.fossil.mn5) r5
            com.fossil.t87.a(r6)
            goto L_0x004b
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r6)
            com.portfolio.platform.data.source.UserRepository r6 = r4.d
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r6.getCurrentUser(r0)
            if (r6 != r1) goto L_0x004b
            return r1
        L_0x004b:
            com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
            com.fossil.mn5$a r5 = new com.fossil.mn5$a
            r5.<init>(r6)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mn5.a(com.fossil.fl4$b, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetUser";
    }
}
