package com.fossil;

import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qv3 extends tv3 {
    @DexIgnore
    public qv3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.tv3
    public void a() {
        ((tv3) this).a.setEndIconOnClickListener(null);
        ((tv3) this).a.setEndIconOnLongClickListener(null);
    }
}
