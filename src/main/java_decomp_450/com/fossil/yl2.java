package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.a12;
import com.fossil.y12;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl2 extends km2 {
    @DexIgnore
    public /* final */ rl2 G;

    @DexIgnore
    public yl2(Context context, Looper looper, a12.b bVar, a12.c cVar, String str, j62 j62) {
        super(context, looper, bVar, cVar, str, j62);
        this.G = new rl2(context, ((km2) this).F);
    }

    @DexIgnore
    public final Location I() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.c();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final void a(bm2 bm2, y12<c53> y12, kl2 kl2) throws RemoteException {
        synchronized (this.G) {
            this.G.a(bm2, y12, kl2);
        }
    }

    @DexIgnore
    public final void a(f53 f53, s12<h53> s12, String str) throws RemoteException {
        s();
        boolean z = true;
        a72.a(f53 != null, "locationSettingsRequest can't be null nor empty.");
        if (s12 == null) {
            z = false;
        }
        a72.a(z, "listener can't be null.");
        ((nl2) A()).a(f53, new am2(s12), str);
    }

    @DexIgnore
    public final void a(y12.a<d53> aVar, kl2 kl2) throws RemoteException {
        this.G.a(aVar, kl2);
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, y12<d53> y12, kl2 kl2) throws RemoteException {
        synchronized (this.G) {
            this.G.a(locationRequest, y12, kl2);
        }
    }

    @DexIgnore
    public final void b(y12.a<c53> aVar, kl2 kl2) throws RemoteException {
        this.G.b(aVar, kl2);
    }
}
