package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v71 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;
    @DexIgnore
    public /* final */ /* synthetic */ x71 b;
    @DexIgnore
    public /* final */ /* synthetic */ qk1 c;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] d;

    @DexIgnore
    public v71(ri1 ri1, x71 x71, qk1 qk1, byte[] bArr) {
        this.a = ri1;
        this.b = x71;
        this.c = qk1;
        this.d = bArr;
    }

    @DexIgnore
    public final void run() {
        this.a.w.d.c();
        this.a.w.d.a(new mb1(this.b, this.c, this.d));
    }
}
