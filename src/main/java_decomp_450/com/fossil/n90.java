package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n90 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<n90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public n90 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                byte readByte = parcel.readByte();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        ee7.a((Object) readString3, "parcel.readString()!!");
                        String readString4 = parcel.readString();
                        if (readString4 != null) {
                            ee7.a((Object) readString4, "parcel.readString()!!");
                            return new n90(readString, readByte, readString2, readString3, readString4);
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public n90[] newArray(int i) {
            return new n90[i];
        }
    }

    @DexIgnore
    public n90(String str, byte b2, String str2, String str3, String str4) {
        this.a = str;
        this.b = b2;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.f, this.a), r51.v, Byte.valueOf(this.b)), r51.g, this.c), r51.w, this.d), r51.x, this.e);
    }

    @DexIgnore
    public final byte[] b() {
        ByteBuffer allocate = ByteBuffer.allocate(6);
        ee7.a((Object) allocate, "ByteBuffer.allocate(HEADER_LENGTH.toInt())");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        String str = this.a;
        Charset c2 = b21.x.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            byte min = (byte) Math.min(bytes.length + 1, 50);
            String str2 = this.c;
            Charset c3 = b21.x.c();
            if (str2 != null) {
                byte[] bytes2 = str2.getBytes(c3);
                ee7.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                byte min2 = (byte) Math.min(bytes2.length + 1, 50);
                String str3 = this.d;
                Charset c4 = b21.x.c();
                if (str3 != null) {
                    byte[] bytes3 = str3.getBytes(c4);
                    ee7.a((Object) bytes3, "(this as java.lang.String).getBytes(charset)");
                    byte min3 = (byte) Math.min(bytes3.length + 1, 50);
                    String str4 = this.e;
                    Charset c5 = b21.x.c();
                    if (str4 != null) {
                        byte[] bytes4 = str4.getBytes(c5);
                        ee7.a((Object) bytes4, "(this as java.lang.String).getBytes(charset)");
                        byte min4 = (byte) Math.min(bytes4.length + 1, 50);
                        short s = (short) (min + 7 + min2 + min3 + min4);
                        allocate.putShort(s);
                        allocate.put(min);
                        allocate.put(min2);
                        allocate.put(min3);
                        allocate.put(min4);
                        ByteBuffer allocate2 = ByteBuffer.allocate(s);
                        ee7.a((Object) allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                        allocate2.order(ByteOrder.LITTLE_ENDIAN);
                        allocate2.put(allocate.array());
                        ByteBuffer allocate3 = ByteBuffer.allocate(s - 6);
                        ee7.a((Object) allocate3, "ByteBuffer.allocate(totalLen - HEADER_LENGTH)");
                        allocate3.order(ByteOrder.LITTLE_ENDIAN);
                        allocate3.put(this.b);
                        String str5 = this.a;
                        Charset c6 = b21.x.c();
                        if (str5 != null) {
                            byte[] bytes5 = str5.getBytes(c6);
                            ee7.a((Object) bytes5, "(this as java.lang.String).getBytes(charset)");
                            allocate3.put(Arrays.copyOfRange(bytes5, 0, min - 1)).put((byte) 0);
                            String str6 = this.c;
                            Charset c7 = b21.x.c();
                            if (str6 != null) {
                                byte[] bytes6 = str6.getBytes(c7);
                                ee7.a((Object) bytes6, "(this as java.lang.String).getBytes(charset)");
                                allocate3.put(Arrays.copyOfRange(bytes6, 0, min2 - 1)).put((byte) 0);
                                String str7 = this.d;
                                Charset c8 = b21.x.c();
                                if (str7 != null) {
                                    byte[] bytes7 = str7.getBytes(c8);
                                    ee7.a((Object) bytes7, "(this as java.lang.String).getBytes(charset)");
                                    allocate3.put(Arrays.copyOfRange(bytes7, 0, min3 - 1)).put((byte) 0);
                                    String str8 = this.e;
                                    Charset c9 = b21.x.c();
                                    if (str8 != null) {
                                        byte[] bytes8 = str8.getBytes(c9);
                                        ee7.a((Object) bytes8, "(this as java.lang.String).getBytes(charset)");
                                        allocate3.put(Arrays.copyOfRange(bytes8, 0, min4 - 1)).put((byte) 0);
                                        allocate2.put(allocate3.array());
                                        byte[] array = allocate2.array();
                                        ee7.a((Object) array, "trackInfoData.array()");
                                        return array;
                                    }
                                    throw new x87("null cannot be cast to non-null type java.lang.String");
                                }
                                throw new x87("null cannot be cast to non-null type java.lang.String");
                            }
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(n90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            n90 n90 = (n90) obj;
            return !(ee7.a(this.a, n90.a) ^ true) && this.b == n90.b && !(ee7.a(this.c, n90.c) ^ true) && !(ee7.a(this.d, n90.d) ^ true) && !(ee7.a(this.e, n90.e) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.TrackInfo");
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.e;
    }

    @DexIgnore
    public final String getAppName() {
        return this.a;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.d;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.c;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        return this.e.hashCode() + ((hashCode2 + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
    }
}
