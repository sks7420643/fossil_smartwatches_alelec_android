package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt0<T> implements Comparator<zk0> {
    @DexIgnore
    public static /* final */ yt0 a; // = new yt0();

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public int compare(zk0 zk0, zk0 zk02) {
        return zk0.e().ordinal() - zk02.e().ordinal();
    }
}
