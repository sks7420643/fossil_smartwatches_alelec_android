package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.plugins.FilesDumperPlugin;
import com.fossil.v54;
import com.fossil.wearables.fsl.countdown.CountDown;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Explore;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a54 implements y84 {
    @DexIgnore
    public static /* final */ y84 a; // = new a54();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements u84<v54.b> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public void a(v54.b bVar, v84 v84) throws IOException {
            v84.a("key", bVar.a());
            v84.a("value", bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements u84<v54> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public void a(v54 v54, v84 v84) throws IOException {
            v84.a("sdkVersion", v54.g());
            v84.a("gmpAppId", v54.c());
            v84.a("platform", v54.f());
            v84.a("installationUuid", v54.d());
            v84.a("buildVersion", v54.a());
            v84.a("displayVersion", v54.b());
            v84.a(Constants.SESSION, v54.h());
            v84.a("ndkPayload", v54.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements u84<v54.c> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public void a(v54.c cVar, v84 v84) throws IOException {
            v84.a(FilesDumperPlugin.NAME, cVar.a());
            v84.a("orgId", cVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements u84<v54.c.b> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        public void a(v54.c.b bVar, v84 v84) throws IOException {
            v84.a("filename", bVar.b());
            v84.a("contents", bVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements u84<v54.d.a> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public void a(v54.d.a aVar, v84 v84) throws IOException {
            v84.a("identifier", aVar.b());
            v84.a("version", aVar.e());
            v84.a("displayVersion", aVar.a());
            v84.a("organization", aVar.d());
            v84.a("installationUuid", aVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements u84<v54.d.a.b> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public void a(v54.d.a.b bVar, v84 v84) throws IOException {
            v84.a("clsId", bVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements u84<v54.d.c> {
        @DexIgnore
        public static /* final */ g a; // = new g();

        @DexIgnore
        public void a(v54.d.c cVar, v84 v84) throws IOException {
            v84.a("arch", cVar.a());
            v84.a(DeviceRequestsHelper.DEVICE_INFO_MODEL, cVar.e());
            v84.a("cores", cVar.b());
            v84.a("ram", cVar.g());
            v84.a("diskSpace", cVar.c());
            v84.a("simulator", cVar.i());
            v84.a("state", cVar.h());
            v84.a("manufacturer", cVar.d());
            v84.a("modelClass", cVar.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements u84<v54.d> {
        @DexIgnore
        public static /* final */ h a; // = new h();

        @DexIgnore
        public void a(v54.d dVar, v84 v84) throws IOException {
            v84.a("generator", dVar.e());
            v84.a("identifier", dVar.h());
            v84.a("startedAt", dVar.j());
            v84.a(CountDown.COLUMN_ENDED_AT, dVar.c());
            v84.a("crashed", dVar.l());
            v84.a("app", dVar.a());
            v84.a("user", dVar.k());
            v84.a("os", dVar.i());
            v84.a("device", dVar.b());
            v84.a("events", dVar.d());
            v84.a("generatorType", dVar.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements u84<v54.d.AbstractC0206d.a> {
        @DexIgnore
        public static /* final */ i a; // = new i();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a aVar, v84 v84) throws IOException {
            v84.a("execution", aVar.c());
            v84.a("customAttributes", aVar.b());
            v84.a(Explore.COLUMN_BACKGROUND, aVar.a());
            v84.a("uiOrientation", aVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements u84<v54.d.AbstractC0206d.a.b.AbstractC0208a> {
        @DexIgnore
        public static /* final */ j a; // = new j();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b.AbstractC0208a aVar, v84 v84) throws IOException {
            v84.a("baseAddress", aVar.a());
            v84.a("size", aVar.c());
            v84.a("name", aVar.b());
            v84.a("uuid", aVar.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements u84<v54.d.AbstractC0206d.a.b> {
        @DexIgnore
        public static /* final */ k a; // = new k();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b bVar, v84 v84) throws IOException {
            v84.a("threads", bVar.d());
            v84.a("exception", bVar.b());
            v84.a("signal", bVar.c());
            v84.a("binaries", bVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements u84<v54.d.AbstractC0206d.a.b.c> {
        @DexIgnore
        public static /* final */ l a; // = new l();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b.c cVar, v84 v84) throws IOException {
            v84.a("type", cVar.e());
            v84.a("reason", cVar.d());
            v84.a("frames", cVar.b());
            v84.a("causedBy", cVar.a());
            v84.a("overflowCount", cVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements u84<v54.d.AbstractC0206d.a.b.AbstractC0212d> {
        @DexIgnore
        public static /* final */ m a; // = new m();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b.AbstractC0212d dVar, v84 v84) throws IOException {
            v84.a("name", dVar.c());
            v84.a("code", dVar.b());
            v84.a("address", dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements u84<v54.d.AbstractC0206d.a.b.e> {
        @DexIgnore
        public static /* final */ n a; // = new n();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b.e eVar, v84 v84) throws IOException {
            v84.a("name", eVar.c());
            v84.a("importance", eVar.b());
            v84.a("frames", eVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements u84<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> {
        @DexIgnore
        public static /* final */ o a; // = new o();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.a.b.e.AbstractC0215b bVar, v84 v84) throws IOException {
            v84.a("pc", bVar.d());
            v84.a("symbol", bVar.e());
            v84.a("file", bVar.a());
            v84.a(Constants.JSON_KEY_OFFSET, bVar.c());
            v84.a("importance", bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements u84<v54.d.AbstractC0206d.c> {
        @DexIgnore
        public static /* final */ p a; // = new p();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.c cVar, v84 v84) throws IOException {
            v84.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, cVar.a());
            v84.a("batteryVelocity", cVar.b());
            v84.a("proximityOn", cVar.f());
            v84.a("orientation", cVar.d());
            v84.a("ramUsed", cVar.e());
            v84.a("diskUsed", cVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements u84<v54.d.AbstractC0206d> {
        @DexIgnore
        public static /* final */ q a; // = new q();

        @DexIgnore
        public void a(v54.d.AbstractC0206d dVar, v84 v84) throws IOException {
            v84.a("timestamp", dVar.d());
            v84.a("type", dVar.e());
            v84.a("app", dVar.a());
            v84.a("device", dVar.b());
            v84.a("log", dVar.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements u84<v54.d.AbstractC0206d.AbstractC0217d> {
        @DexIgnore
        public static /* final */ r a; // = new r();

        @DexIgnore
        public void a(v54.d.AbstractC0206d.AbstractC0217d dVar, v84 v84) throws IOException {
            v84.a("content", dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements u84<v54.d.e> {
        @DexIgnore
        public static /* final */ s a; // = new s();

        @DexIgnore
        public void a(v54.d.e eVar, v84 v84) throws IOException {
            v84.a("platform", eVar.b());
            v84.a("version", eVar.c());
            v84.a("buildVersion", eVar.a());
            v84.a("jailbroken", eVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t implements u84<v54.d.f> {
        @DexIgnore
        public static /* final */ t a; // = new t();

        @DexIgnore
        public void a(v54.d.f fVar, v84 v84) throws IOException {
            v84.a("identifier", fVar.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.y84
    public void a(z84<?> z84) {
        z84.a(v54.class, b.a);
        z84.a(b54.class, b.a);
        z84.a(v54.d.class, h.a);
        z84.a(f54.class, h.a);
        z84.a(v54.d.a.class, e.a);
        z84.a(g54.class, e.a);
        z84.a(v54.d.a.b.class, f.a);
        z84.a(h54.class, f.a);
        z84.a(v54.d.f.class, t.a);
        z84.a(u54.class, t.a);
        z84.a(v54.d.e.class, s.a);
        z84.a(t54.class, s.a);
        z84.a(v54.d.c.class, g.a);
        z84.a(i54.class, g.a);
        z84.a(v54.d.AbstractC0206d.class, q.a);
        z84.a(j54.class, q.a);
        z84.a(v54.d.AbstractC0206d.a.class, i.a);
        z84.a(k54.class, i.a);
        z84.a(v54.d.AbstractC0206d.a.b.class, k.a);
        z84.a(l54.class, k.a);
        z84.a(v54.d.AbstractC0206d.a.b.e.class, n.a);
        z84.a(p54.class, n.a);
        z84.a(v54.d.AbstractC0206d.a.b.e.AbstractC0215b.class, o.a);
        z84.a(q54.class, o.a);
        z84.a(v54.d.AbstractC0206d.a.b.c.class, l.a);
        z84.a(n54.class, l.a);
        z84.a(v54.d.AbstractC0206d.a.b.AbstractC0212d.class, m.a);
        z84.a(o54.class, m.a);
        z84.a(v54.d.AbstractC0206d.a.b.AbstractC0208a.class, j.a);
        z84.a(m54.class, j.a);
        z84.a(v54.b.class, a.a);
        z84.a(c54.class, a.a);
        z84.a(v54.d.AbstractC0206d.c.class, p.a);
        z84.a(r54.class, p.a);
        z84.a(v54.d.AbstractC0206d.AbstractC0217d.class, r.a);
        z84.a(s54.class, r.a);
        z84.a(v54.c.class, c.a);
        z84.a(d54.class, c.a);
        z84.a(v54.c.b.class, d.a);
        z84.a(e54.class, d.a);
    }
}
