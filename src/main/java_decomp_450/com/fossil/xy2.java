package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xy2 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> a; // = this.b.a.iterator();
    @DexIgnore
    public /* final */ /* synthetic */ vy2 b;

    @DexIgnore
    public xy2(vy2 vy2) {
        this.b = vy2;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.a.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
