package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q40<R> {
    @DexIgnore
    boolean a(py pyVar, Object obj, c50<R> c50, boolean z);

    @DexIgnore
    boolean a(R r, Object obj, c50<R> c50, sw swVar, boolean z);
}
