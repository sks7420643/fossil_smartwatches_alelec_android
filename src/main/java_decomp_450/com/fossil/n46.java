package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n46 implements Factory<m46> {
    @DexIgnore
    public static m46 a(j46 j46, GoogleApiService googleApiService) {
        return new m46(j46, googleApiService);
    }
}
