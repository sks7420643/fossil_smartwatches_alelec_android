package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yf0 {
    DEFAULT((byte) 0),
    PRE_SHARED_KEY((byte) 1),
    SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final yf0 a(byte b) {
            yf0[] values = yf0.values();
            for (yf0 yf0 : values) {
                if (yf0.a() == b) {
                    return yf0;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public yf0(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
