package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pr2<T> implements Serializable {
    @DexIgnore
    public static <T> pr2<T> zza(T t) {
        or2.a(t);
        return new rr2(t);
    }

    @DexIgnore
    public static <T> pr2<T> zzc() {
        return lr2.zza;
    }

    @DexIgnore
    public abstract boolean zza();

    @DexIgnore
    public abstract T zzb();
}
