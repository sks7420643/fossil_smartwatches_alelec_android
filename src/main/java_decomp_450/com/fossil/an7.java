package com.fossil;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class an7 extends ak7 implements en7, Executor {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(an7.class, "inFlightTasks");
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<Runnable> b; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ ym7 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public volatile int inFlightTasks; // = 0;

    @DexIgnore
    public an7(ym7 ym7, int i, int i2) {
        this.c = ym7;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void a(ib7 ib7, Runnable runnable) {
        a(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void b(ib7 ib7, Runnable runnable) {
        a(runnable, true);
    }

    @DexIgnore
    @Override // com.fossil.en7
    public int c() {
        return this.e;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        a(runnable, false);
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        return super.toString() + "[dispatcher = " + this.c + ']';
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Runnable r3, boolean r4) {
        /*
            r2 = this;
        L_0x0000:
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r0 = com.fossil.an7.f
            int r0 = r0.incrementAndGet(r2)
            int r1 = r2.d
            if (r0 > r1) goto L_0x0010
            com.fossil.ym7 r0 = r2.c
            r0.a(r3, r2, r4)
            return
        L_0x0010:
            java.util.concurrent.ConcurrentLinkedQueue<java.lang.Runnable> r0 = r2.b
            r0.add(r3)
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r3 = com.fossil.an7.f
            int r3 = r3.decrementAndGet(r2)
            int r0 = r2.d
            if (r3 < r0) goto L_0x0020
            return
        L_0x0020:
            java.util.concurrent.ConcurrentLinkedQueue<java.lang.Runnable> r3 = r2.b
            java.lang.Object r3 = r3.poll()
            java.lang.Runnable r3 = (java.lang.Runnable) r3
            if (r3 == 0) goto L_0x002b
            goto L_0x0000
        L_0x002b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.an7.a(java.lang.Runnable, boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.en7
    public void a() {
        Runnable poll = this.b.poll();
        if (poll != null) {
            this.c.a(poll, this, true);
            return;
        }
        f.decrementAndGet(this);
        Runnable poll2 = this.b.poll();
        if (poll2 != null) {
            a(poll2, true);
        }
    }
}
