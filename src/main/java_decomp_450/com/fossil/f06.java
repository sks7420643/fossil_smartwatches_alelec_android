package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f06 implements Factory<e06> {
    @DexIgnore
    public static e06 a(d06 d06, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, RingStyleRepository ringStyleRepository, DianaPresetRepository dianaPresetRepository, z46 z46, og5 og5, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        return new e06(d06, watchAppRepository, complicationRepository, ringStyleRepository, dianaPresetRepository, z46, og5, customizeRealDataRepository, userRepository, watchFaceRepository, portfolioApp);
    }
}
