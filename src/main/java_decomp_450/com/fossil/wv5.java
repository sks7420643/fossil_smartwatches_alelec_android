package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv5 implements Factory<vv5> {
    @DexIgnore
    public static vv5 a(qv5 qv5, ch5 ch5, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new vv5(qv5, ch5, remindersSettingsDatabase);
    }
}
