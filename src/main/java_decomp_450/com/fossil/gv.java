package com.fossil;

import android.os.SystemClock;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gv {
    @DexIgnore
    public static String a; // = "Volley";
    @DexIgnore
    public static boolean b; // = Log.isLoggable("Volley", 2);
    @DexIgnore
    public static /* final */ String c; // = gv.class.getName();

    @DexIgnore
    public static void a(Throwable th, String str, Object... objArr) {
        Log.e(a, a(str, objArr), th);
    }

    @DexIgnore
    public static void b(String str, Object... objArr) {
        Log.d(a, a(str, objArr));
    }

    @DexIgnore
    public static void c(String str, Object... objArr) {
        Log.e(a, a(str, objArr));
    }

    @DexIgnore
    public static void d(String str, Object... objArr) {
        if (b) {
            Log.v(a, a(str, objArr));
        }
    }

    @DexIgnore
    public static void e(String str, Object... objArr) {
        Log.wtf(a, a(str, objArr));
    }

    @DexIgnore
    public static String a(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClassName().equals(c)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + CodelessMatcher.CURRENT_CLASS_NAME + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ boolean c; // = gv.b;
        @DexIgnore
        public /* final */ List<C0071a> a; // = new ArrayList();
        @DexIgnore
        public boolean b; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gv$a$a")
        /* renamed from: com.fossil.gv$a$a  reason: collision with other inner class name */
        public static class C0071a {
            @DexIgnore
            public /* final */ String a;
            @DexIgnore
            public /* final */ long b;
            @DexIgnore
            public /* final */ long c;

            @DexIgnore
            public C0071a(String str, long j, long j2) {
                this.a = str;
                this.b = j;
                this.c = j2;
            }
        }

        @DexIgnore
        public synchronized void a(String str, long j) {
            if (!this.b) {
                this.a.add(new C0071a(str, j, SystemClock.elapsedRealtime()));
            } else {
                throw new IllegalStateException("Marker added to finished log");
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            if (!this.b) {
                a("Request on the loose");
                gv.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        @DexIgnore
        public synchronized void a(String str) {
            this.b = true;
            long a2 = a();
            if (a2 > 0) {
                long j = this.a.get(0).c;
                gv.b("(%-4d ms) %s", Long.valueOf(a2), str);
                for (C0071a aVar : this.a) {
                    long j2 = aVar.c;
                    gv.b("(+%-4d) [%2d] %s", Long.valueOf(j2 - j), Long.valueOf(aVar.b), aVar.a);
                    j = j2;
                }
            }
        }

        @DexIgnore
        public final long a() {
            if (this.a.size() == 0) {
                return 0;
            }
            long j = this.a.get(0).c;
            List<C0071a> list = this.a;
            return list.get(list.size() - 1).c - j;
        }
    }
}
