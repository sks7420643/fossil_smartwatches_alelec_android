package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ci1 extends Enum<ci1> {
    @DexIgnore
    public static /* final */ ci1 a;
    @DexIgnore
    public static /* final */ ci1 b;
    @DexIgnore
    public static /* final */ ci1 c;
    @DexIgnore
    public static /* final */ ci1 d;
    @DexIgnore
    public static /* final */ ci1 e;
    @DexIgnore
    public static /* final */ ci1 f;
    @DexIgnore
    public static /* final */ ci1 g;
    @DexIgnore
    public static /* final */ ci1 h;
    @DexIgnore
    public static /* final */ ci1 i;
    @DexIgnore
    public static /* final */ ci1 j;
    @DexIgnore
    public static /* final */ /* synthetic */ ci1[] k;

    /*
    static {
        ci1 ci1 = new ci1("PHASE_INIT", 0);
        a = ci1;
        ci1 ci12 = new ci1("PHASE_START", 1);
        b = ci12;
        ci1 ci13 = new ci1("PHASE_END", 2);
        c = ci13;
        ci1 ci14 = new ci1("REQUEST", 3);
        d = ci14;
        ci1 ci15 = new ci1("RESPONSE", 4);
        e = ci15;
        ci1 ci16 = new ci1("SYSTEM_EVENT", 5);
        f = ci16;
        ci1 ci17 = new ci1("CENTRAL_EVENT", 6);
        g = ci17;
        ci1 ci18 = new ci1("DEVICE_EVENT", 7);
        h = ci18;
        ci1 ci19 = new ci1("EXCEPTION", 9);
        i = ci19;
        ci1 ci110 = new ci1("MSL", 11);
        j = ci110;
        k = new ci1[]{ci1, ci12, ci13, ci14, ci15, ci16, ci17, ci18, new ci1("GATT_SERVER_EVENT", 8), ci19, new ci1("DATABASE", 10), ci110};
    }
    */

    @DexIgnore
    public ci1(String str, int i2) {
    }

    @DexIgnore
    public static ci1 valueOf(String str) {
        return (ci1) Enum.valueOf(ci1.class, str);
    }

    @DexIgnore
    public static ci1[] values() {
        return (ci1[]) k.clone();
    }
}
