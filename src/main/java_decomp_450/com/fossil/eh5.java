package com.fossil;

import android.graphics.Typeface;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eh5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static eh5 k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ String a; // = "theme/fonts/soleil/";
    @DexIgnore
    public /* final */ String b; // = "theme/fonts/ginger/";
    @DexIgnore
    public /* final */ String c; // = "theme/fonts/sf/";
    @DexIgnore
    public /* final */ String d; // = ".otf";
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Style> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ s4<String, String> f; // = new s4<>(10);
    @DexIgnore
    public /* final */ s4<String, Typeface> g; // = new s4<>(10);
    @DexIgnore
    public /* final */ ArrayList<String> h; // = w97.a((Object[]) new String[]{this.a, this.b, this.c});
    @DexIgnore
    public ThemeRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final synchronized eh5 a() {
            eh5 b;
            if (eh5.k == null) {
                eh5.k = new eh5();
            }
            b = eh5.k;
            if (b == null) {
                ee7.a();
                throw null;
            }
            return b;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.ThemeManager$addNewTheme$1", f = "ThemeManager.kt", l = {128, 132}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ eh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(eh5 eh5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = eh5;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$id, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ThemeRepository a2 = this.this$0.a();
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.getThemeById(Endpoints.DEFAULT_NAME, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                Theme theme = (Theme) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Theme theme2 = (Theme) obj;
            if (theme2 != null) {
                theme2.setId(this.$id);
                theme2.setName("New theme");
                theme2.setType("modified");
                ThemeRepository a3 = this.this$0.a();
                this.L$0 = yi7;
                this.L$1 = theme2;
                this.label = 2;
                if (a3.upsertUserTheme(theme2, this) == a) {
                    return a;
                }
                return i97.a;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.ThemeManager", f = "ThemeManager.kt", l = {38}, m = "reloadStyleCache")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ eh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(eh5 eh5, fb7 fb7) {
            super(fb7);
            this.this$0 = eh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    /*
    static {
        String simpleName = eh5.class.getSimpleName();
        ee7.a((Object) simpleName, "ThemeManager::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public eh5() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final Typeface c(String str) {
        T t;
        ee7.b(str, "themeStyle");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getKey(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        s4<String, Typeface> s4Var = this.g;
        if (value != null) {
            Typeface b2 = s4Var.b(value);
            if (b2 == null) {
                Iterator<T> it2 = this.h.iterator();
                while (it2.hasNext()) {
                    try {
                        Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.g0.c().getAssets(), ((String) it2.next()) + value + this.d);
                        s4<String, Typeface> s4Var2 = this.g;
                        if (createFromAsset != null) {
                            s4Var2.a(value, createFromAsset);
                            return createFromAsset;
                        }
                        ee7.a();
                        throw null;
                    } catch (Exception unused) {
                        FLogger.INSTANCE.getLocal().d(j, "font file is not exists");
                    }
                }
            }
            return b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ThemeRepository a() {
        ThemeRepository themeRepository = this.i;
        if (themeRepository != null) {
            return themeRepository;
        }
        ee7.d("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final String b(String str) {
        T t;
        ee7.b(str, "themeStyle");
        String b2 = this.f.b(str);
        if (b2 == null) {
            Iterator<T> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) t.getKey(), (Object) str)) {
                    break;
                }
            }
            T t2 = t;
            b2 = t2 != null ? t2.getValue() : null;
            if (!TextUtils.isEmpty(b2)) {
                s4<String, String> s4Var = this.f;
                if (b2 != null) {
                    s4Var.a(str, b2);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        return b2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.eh5.c
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.eh5$c r0 = (com.fossil.eh5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.eh5$c r0 = new com.fossil.eh5$c
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.eh5 r0 = (com.fossil.eh5) r0
            com.fossil.t87.a(r7)
            goto L_0x0058
        L_0x002e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0036:
            com.fossil.t87.a(r7)
            com.fossil.s4<java.lang.String, java.lang.String> r7 = r6.f
            r7.a()
            com.fossil.s4<java.lang.String, android.graphics.Typeface> r7 = r6.g
            r7.a()
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r7 = r6.e
            r7.clear()
            com.portfolio.platform.data.source.ThemeRepository r7 = r6.i
            if (r7 == 0) goto L_0x0092
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r7 = r7.getCurrentTheme(r0)
            if (r7 != r1) goto L_0x0057
            return r1
        L_0x0057:
            r0 = r6
        L_0x0058:
            com.portfolio.platform.data.model.Theme r7 = (com.portfolio.platform.data.model.Theme) r7
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.eh5.j
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "reload style cache currentId "
            r4.append(r5)
            if (r7 == 0) goto L_0x0072
            java.lang.String r3 = r7.getId()
        L_0x0072:
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r1.d(r2, r3)
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r0 = r0.e
            if (r7 == 0) goto L_0x0087
            java.util.ArrayList r7 = r7.getStyles()
            if (r7 == 0) goto L_0x0087
            goto L_0x008c
        L_0x0087:
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
        L_0x008c:
            r0.addAll(r7)
            com.fossil.i97 r7 = com.fossil.i97.a
            return r7
        L_0x0092:
            java.lang.String r7 = "mThemeRepository"
            com.fossil.ee7.d(r7)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eh5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Typeface b(String str, List<Style> list) {
        T t;
        ee7.b(str, "themeStyle");
        ee7.b(list, "predefineStyles");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getKey(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        Iterator<T> it2 = this.h.iterator();
        while (it2.hasNext()) {
            try {
                Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.g0.c().getAssets(), ((String) it2.next()) + value + this.d);
                s4<String, Typeface> s4Var = this.g;
                if (createFromAsset != null) {
                    s4Var.a(str, createFromAsset);
                    return createFromAsset;
                }
                ee7.a();
                throw null;
            } catch (Exception unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String a(String str, List<Style> list) {
        T t;
        ee7.b(str, "themeStyle");
        ee7.b(list, "predefineStyles");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getKey(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return t2.getValue();
        }
        return null;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "addNewTheme id=" + str);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, str, null), 3, null);
    }
}
