package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um4 {
    @DexIgnore
    public static /* final */ um4 a; // = new um4();

    @DexIgnore
    public final void a(mn4 mn4, long j, String str, Context context) {
        ee7.b(mn4, "challenge");
        ee7.b(str, "startType");
        ee7.b(context, "context");
        Intent intent = new Intent("bc_create_challenge");
        intent.putExtra("challenge_extra", mn4);
        intent.putExtra("start_tracking_time_extra", j);
        intent.putExtra("start_type_extra", str);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Context context) {
        ee7.b(str, "type");
        ee7.b(str2, "currentId");
        ee7.b(str3, "friendId");
        ee7.b(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("friend_id_extra", str3);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void b(Context context) {
        ee7.b(context, "context");
        Intent intent = new Intent("bc_look_for_friend_challenge");
        intent.putExtra("current_user_id_extra", PortfolioApp.g0.c().w());
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, Context context) {
        ee7.b(str, "type");
        ee7.b(str2, "challengeId");
        ee7.b(str3, "profileId");
        ee7.b(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, int i, Context context) {
        ee7.b(str, "type");
        ee7.b(str2, "challengeId");
        ee7.b(str3, "profileId");
        ee7.b(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        intent.putExtra("current_steps_extra", i);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(String str, String str2, int i, int i2, boolean z, Context context) {
        ee7.b(str, "challengeId");
        ee7.b(str2, "currentId");
        ee7.b(context, "context");
        Intent intent = new Intent("bc_complete_challenge");
        intent.putExtra("challenge_id_extra", str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("current_steps_extra", i2);
        intent.putExtra("rank_extra", i);
        intent.putExtra("reach_goal_or_top", z);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(String str, Context context) {
        ee7.b(str, "socialProfileId");
        ee7.b(context, "context");
        Intent intent = new Intent("bc_create_social_profile");
        intent.putExtra("current_user_id_extra", str);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(String str, String str2, Context context) {
        ee7.b(str, "currentId");
        ee7.b(str2, "friendId");
        ee7.b(context, "context");
        Intent intent = new Intent("bc_send_friend_request");
        intent.putExtra("current_user_id_extra", str);
        intent.putExtra("friend_id_extra", str2);
        qe.a(context).a(intent);
    }

    @DexIgnore
    public final void a(Context context) {
        ee7.b(context, "context");
        Intent intent = new Intent("bc_look_for_friend");
        intent.putExtra("current_user_id_extra", PortfolioApp.g0.c().w());
        qe.a(context).a(intent);
    }
}
