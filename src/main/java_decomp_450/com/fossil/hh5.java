package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh5 implements MembersInjector<gh5> {
    @DexIgnore
    public static void a(gh5 gh5, PortfolioApp portfolioApp) {
        gh5.a = portfolioApp;
    }

    @DexIgnore
    public static void a(gh5 gh5, ApiServiceV2 apiServiceV2) {
        gh5.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(gh5 gh5, LocationSource locationSource) {
        gh5.c = locationSource;
    }

    @DexIgnore
    public static void a(gh5 gh5, UserRepository userRepository) {
        gh5.d = userRepository;
    }

    @DexIgnore
    public static void a(gh5 gh5, CustomizeRealDataRepository customizeRealDataRepository) {
        gh5.e = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(gh5 gh5, DianaPresetRepository dianaPresetRepository) {
        gh5.f = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(gh5 gh5, GoogleApiService googleApiService) {
        gh5.g = googleApiService;
    }
}
