package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap3<TResult> implements hp3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public ho3<TResult> c;

    @DexIgnore
    public ap3(Executor executor, ho3<TResult> ho3) {
        this.a = executor;
        this.c = ho3;
    }

    @DexIgnore
    @Override // com.fossil.hp3
    public final void a(no3<TResult> no3) {
        synchronized (this.b) {
            if (this.c != null) {
                this.a.execute(new zo3(this, no3));
            }
        }
    }
}
