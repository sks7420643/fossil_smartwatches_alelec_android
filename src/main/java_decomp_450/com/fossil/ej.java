package com.fossil;

import android.database.sqlite.SQLiteProgram;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ej implements yi {
    @DexIgnore
    public /* final */ SQLiteProgram a;

    @DexIgnore
    public ej(SQLiteProgram sQLiteProgram) {
        this.a = sQLiteProgram;
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindBlob(int i, byte[] bArr) {
        this.a.bindBlob(i, bArr);
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindDouble(int i, double d) {
        this.a.bindDouble(i, d);
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindLong(int i, long j) {
        this.a.bindLong(i, j);
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindNull(int i) {
        this.a.bindNull(i);
    }

    @DexIgnore
    @Override // com.fossil.yi
    public void bindString(int i, String str) {
        this.a.bindString(i, str);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }
}
