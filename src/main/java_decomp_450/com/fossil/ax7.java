package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax7 {
    @DexIgnore
    public c a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public long a;
        @DexIgnore
        public long b;

        @DexIgnore
        public final long a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final void a(long j) {
            this.b = j;
        }

        @DexIgnore
        public final void b(long j) {
            this.a = j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public final int a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.a;
        }

        @DexIgnore
        public final void a(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void c(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void d(int i) {
            this.a = i;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final void a(c cVar) {
        ee7.b(cVar, "<set-?>");
        this.a = cVar;
    }

    @DexIgnore
    public final void a(boolean z) {
    }

    @DexIgnore
    public final String b() {
        return "duration >=? AND duration <=?";
    }

    @DexIgnore
    public final String[] c() {
        Integer[] numArr = new Integer[4];
        c cVar = this.a;
        if (cVar != null) {
            numArr[0] = Integer.valueOf(cVar.d());
            c cVar2 = this.a;
            if (cVar2 != null) {
                numArr[1] = Integer.valueOf(cVar2.b());
                c cVar3 = this.a;
                if (cVar3 != null) {
                    numArr[2] = Integer.valueOf(cVar3.c());
                    c cVar4 = this.a;
                    if (cVar4 != null) {
                        numArr[3] = Integer.valueOf(cVar4.a());
                        List<Number> h = t97.h(numArr);
                        ArrayList arrayList = new ArrayList(x97.a(h, 10));
                        for (Number number : h) {
                            arrayList.add(String.valueOf(number.intValue()));
                        }
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            return (String[]) array;
                        }
                        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    ee7.d("sizeConstraint");
                    throw null;
                }
                ee7.d("sizeConstraint");
                throw null;
            }
            ee7.d("sizeConstraint");
            throw null;
        }
        ee7.d("sizeConstraint");
        throw null;
    }

    @DexIgnore
    public final String d() {
        return "width >= ? AND width <= ? AND height >= ? AND height <=?";
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "<set-?>");
        this.b = bVar;
    }

    @DexIgnore
    public final String[] a() {
        Long[] lArr = new Long[2];
        b bVar = this.b;
        if (bVar != null) {
            lArr[0] = Long.valueOf(bVar.b());
            b bVar2 = this.b;
            if (bVar2 != null) {
                lArr[1] = Long.valueOf(bVar2.a());
                List<Number> h = t97.h(lArr);
                ArrayList arrayList = new ArrayList(x97.a(h, 10));
                for (Number number : h) {
                    arrayList.add(String.valueOf(number.longValue()));
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    return (String[]) array;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            ee7.d("durationConstraint");
            throw null;
        }
        ee7.d("durationConstraint");
        throw null;
    }
}
