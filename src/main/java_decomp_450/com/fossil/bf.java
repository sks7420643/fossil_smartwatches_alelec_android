package com.fossil;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bf implements af {
    @DexIgnore
    public /* final */ MediaSessionManager.RemoteUserInfo a;

    @DexIgnore
    public bf(String str, int i, int i2) {
        this.a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bf)) {
            return false;
        }
        return this.a.equals(((bf) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return z8.a(this.a);
    }
}
