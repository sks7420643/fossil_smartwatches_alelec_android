package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kf7 extends ja7 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public kf7(int i, int i2, int i3) {
        this.d = i3;
        this.a = i2;
        boolean z = true;
        if (i3 <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.b = z;
        this.c = !z ? this.a : i;
    }

    @DexIgnore
    @Override // com.fossil.ja7
    public int a() {
        int i = this.c;
        if (i != this.a) {
            this.c = this.d + i;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.b;
    }
}
