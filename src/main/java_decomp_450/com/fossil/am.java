package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am {
    @DexIgnore
    public static /* final */ am i; // = new a().a();
    @DexIgnore
    public jm a; // = jm.NOT_REQUIRED;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f; // = -1;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public bm h; // = new bm();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public jm c; // = jm.NOT_REQUIRED;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public bm h; // = new bm();

        @DexIgnore
        public a a(jm jmVar) {
            this.c = jmVar;
            return this;
        }

        @DexIgnore
        public am a() {
            return new am(this);
        }
    }

    @DexIgnore
    public am() {
    }

    @DexIgnore
    public void a(jm jmVar) {
        this.a = jmVar;
    }

    @DexIgnore
    public jm b() {
        return this.a;
    }

    @DexIgnore
    public void c(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void d(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public boolean e() {
        return this.h.b() > 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || am.class != obj.getClass()) {
            return false;
        }
        am amVar = (am) obj;
        if (this.b == amVar.b && this.c == amVar.c && this.d == amVar.d && this.e == amVar.e && this.f == amVar.f && this.g == amVar.g && this.a == amVar.a) {
            return this.h.equals(amVar.h);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.b;
    }

    @DexIgnore
    public boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f;
        long j2 = this.g;
        return (((((((((((((this.a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void b(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public long c() {
        return this.f;
    }

    @DexIgnore
    public long d() {
        return this.g;
    }

    @DexIgnore
    public void a(long j) {
        this.f = j;
    }

    @DexIgnore
    public void b(long j) {
        this.g = j;
    }

    @DexIgnore
    public void a(bm bmVar) {
        this.h = bmVar;
    }

    @DexIgnore
    public bm a() {
        return this.h;
    }

    @DexIgnore
    public am(a aVar) {
        this.b = aVar.a;
        this.c = Build.VERSION.SDK_INT >= 23 && aVar.b;
        this.a = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.h = aVar.h;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    @DexIgnore
    public am(am amVar) {
        this.b = amVar.b;
        this.c = amVar.c;
        this.a = amVar.a;
        this.d = amVar.d;
        this.e = amVar.e;
        this.h = amVar.h;
    }
}
