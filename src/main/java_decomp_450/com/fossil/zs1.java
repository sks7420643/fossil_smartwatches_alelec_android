package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.fitness.AlgorithmManager;
import com.fossil.fitness.DataCaching;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs1 {
    @DexIgnore
    public static long a; // = 1800000;
    @DexIgnore
    public static /* final */ zs1 b; // = new zs1();

    @DexIgnore
    public final boolean a() {
        return (ee7.a(u31.g.f(), true) ^ true) || u31.g.a() != null;
    }

    @DexIgnore
    public final long b() {
        return a;
    }

    @DexIgnore
    public final String c() {
        return null;
    }

    @DexIgnore
    public final String d() {
        return null;
    }

    @DexIgnore
    public final String e() {
        return u31.g.e();
    }

    @DexIgnore
    public final long a(bg0 bg0) {
        int i = o71.a[bg0.ordinal()];
        if (i == 1) {
            return wl0.h.c();
        }
        if (i == 2) {
            return yl1.h.c();
        }
        if (i == 3) {
            return xn1.h.c();
        }
        if (i == 4) {
            return ak1.h.c();
        }
        throw new p87();
    }

    @DexIgnore
    public final void a(Context context, int i) throws IllegalArgumentException, IllegalStateException {
        Object obj;
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            u31 u31 = u31.g;
            try {
                obj = SoLibraryLoader.a().a(context, i);
                u31.g.a(context);
                t11 t11 = t11.a;
                zz0.d.a(context);
                f60.k.a(context);
                System.loadLibrary("FitnessAlgorithm");
                System.loadLibrary("EllipticCurveCrypto");
                System.loadLibrary("EInkImageFilter");
                File file = new File(context.getFilesDir(), "msl");
                file.mkdirs();
                if (file.exists()) {
                    AlgorithmManager.defaultManager().setDataCaching(new DataCaching(file.getAbsolutePath(), new byte[0]));
                }
                AlgorithmManager defaultManager = AlgorithmManager.defaultManager();
                ee7.a((Object) defaultManager, "AlgorithmManager.defaultManager()");
                defaultManager.setDebugEnabled(false);
                ee7.a(obj, "isValid");
            } catch (Exception unused) {
                obj = i97.a;
            }
            u31.a(obj);
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void a(dh0 dh0) {
        u31.g.a(dh0);
    }

    @DexIgnore
    public final void a(String str) {
        u31.g.a(str);
        yj0.e.a().c = str;
    }
}
