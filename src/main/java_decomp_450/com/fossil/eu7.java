package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eu7 implements tt7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public volatile tt7 b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public Method d;
    @DexIgnore
    public wt7 e;
    @DexIgnore
    public Queue<zt7> f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public eu7(String str, Queue<zt7> queue, boolean z) {
        this.a = str;
        this.f = queue;
        this.g = z;
    }

    @DexIgnore
    public tt7 a() {
        if (this.b != null) {
            return this.b;
        }
        if (this.g) {
            return bu7.NOP_LOGGER;
        }
        return b();
    }

    @DexIgnore
    public final tt7 b() {
        if (this.e == null) {
            this.e = new wt7(this, this.f);
        }
        return this.e;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public boolean d() {
        Boolean bool = this.c;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.d = this.b.getClass().getMethod("log", yt7.class);
            this.c = Boolean.TRUE;
        } catch (NoSuchMethodException unused) {
            this.c = Boolean.FALSE;
        }
        return this.c.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str) {
        a().debug(str);
    }

    @DexIgnore
    public boolean e() {
        return this.b instanceof bu7;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && eu7.class == obj.getClass() && this.a.equals(((eu7) obj).a);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str) {
        a().error(str);
    }

    @DexIgnore
    public boolean f() {
        return this.b == null;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str) {
        a().info(str);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isDebugEnabled() {
        return a().isDebugEnabled();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isErrorEnabled() {
        return a().isErrorEnabled();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isInfoEnabled() {
        return a().isInfoEnabled();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isTraceEnabled() {
        return a().isTraceEnabled();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isWarnEnabled() {
        return a().isWarnEnabled();
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str) {
        a().trace(str);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str) {
        a().warn(str);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object obj) {
        a().debug(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object obj) {
        a().error(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object obj) {
        a().info(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object obj) {
        a().trace(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object obj) {
        a().warn(str, obj);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object obj, Object obj2) {
        a().debug(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object obj, Object obj2) {
        a().error(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object obj, Object obj2) {
        a().info(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object obj, Object obj2) {
        a().trace(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object obj, Object obj2) {
        a().warn(str, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object... objArr) {
        a().debug(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object... objArr) {
        a().error(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object... objArr) {
        a().info(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object... objArr) {
        a().trace(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object... objArr) {
        a().warn(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Throwable th) {
        a().debug(str, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Throwable th) {
        a().error(str, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Throwable th) {
        a().info(str, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Throwable th) {
        a().trace(str, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Throwable th) {
        a().warn(str, th);
    }

    @DexIgnore
    public void a(tt7 tt7) {
        this.b = tt7;
    }

    @DexIgnore
    public void a(yt7 yt7) {
        if (d()) {
            try {
                this.d.invoke(this.b, yt7);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
    }
}
