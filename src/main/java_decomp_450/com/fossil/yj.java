package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362019;
    @DexIgnore
    public static /* final */ int forever; // = 2131362303;
    @DexIgnore
    public static /* final */ int ghost_view; // = 2131362536;
    @DexIgnore
    public static /* final */ int ghost_view_holder; // = 2131362537;
    @DexIgnore
    public static /* final */ int icon; // = 2131362579;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362580;
    @DexIgnore
    public static /* final */ int info; // = 2131362603;
    @DexIgnore
    public static /* final */ int italic; // = 2131362619;
    @DexIgnore
    public static /* final */ int line1; // = 2131362752;
    @DexIgnore
    public static /* final */ int line3; // = 2131362754;
    @DexIgnore
    public static /* final */ int normal; // = 2131362844;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362845;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362846;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362847;
    @DexIgnore
    public static /* final */ int parent_matrix; // = 2131362881;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362949;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362950;
    @DexIgnore
    public static /* final */ int save_non_transition_alpha; // = 2131363016;
    @DexIgnore
    public static /* final */ int save_overlay_view; // = 2131363017;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363124;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363125;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363126;
    @DexIgnore
    public static /* final */ int text; // = 2131363132;
    @DexIgnore
    public static /* final */ int text2; // = 2131363133;
    @DexIgnore
    public static /* final */ int time; // = 2131363154;
    @DexIgnore
    public static /* final */ int title; // = 2131363156;
    @DexIgnore
    public static /* final */ int transition_current_scene; // = 2131363169;
    @DexIgnore
    public static /* final */ int transition_layout_save; // = 2131363170;
    @DexIgnore
    public static /* final */ int transition_position; // = 2131363171;
    @DexIgnore
    public static /* final */ int transition_scene_layoutid_cache; // = 2131363172;
    @DexIgnore
    public static /* final */ int transition_transform; // = 2131363173;
}
