package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yn7 {
    @DexIgnore
    public static final yn7 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements yn7 {
        @DexIgnore
        @Override // com.fossil.yn7
        public List<xn7> a(go7 go7) {
            return Collections.emptyList();
        }

        @DexIgnore
        @Override // com.fossil.yn7
        public void a(go7 go7, List<xn7> list) {
        }
    }

    @DexIgnore
    List<xn7> a(go7 go7);

    @DexIgnore
    void a(go7 go7, List<xn7> list);
}
