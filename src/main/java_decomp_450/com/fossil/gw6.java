package com.fossil;

import com.fossil.ql4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw6 extends ql4<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gw6.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.a {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "errorMessage");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.b {
        @DexIgnore
        public /* final */ WeatherSettings.TEMP_UNIT a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public c(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            ee7.b(latLng, "latLng");
            ee7.b(temp_unit, "tempUnit");
            we4.a(temp_unit);
            ee7.a((Object) temp_unit, "checkNotNull(tempUnit)");
            this.a = temp_unit;
            we4.a(latLng);
            ee7.a((Object) latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ql4.c {
        @DexIgnore
        public /* final */ Weather a;

        @DexIgnore
        public d(Weather weather) {
            ee7.b(weather, "weather");
            this.a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ c $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gw6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
        public static final class a extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(1, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(fb7<?> fb7) {
                ee7.b(fb7, "completion");
                return new a(this.this$0, fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public final Object invoke(fb7<? super fv7<ie4>> fb7) {
                return ((a) create(fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    ApiServiceV2 a2 = this.this$0.this$0.d;
                    String valueOf = String.valueOf(this.this$0.$requestValues.a().a);
                    String valueOf2 = String.valueOf(this.this$0.$requestValues.a().b);
                    String value = this.this$0.$requestValues.b().getValue();
                    this.label = 1;
                    obj = a2.getWeather(valueOf, valueOf2, value, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gw6 gw6, c cVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gw6;
            this.$requestValues = cVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$requestValues, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = aj5.a(aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            zi5 zi5 = (zi5) obj;
            if (zi5 instanceof bj5) {
                fj5 fj5 = new fj5();
                fj5.a((ie4) ((bj5) zi5).a());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = gw6.f.a();
                local.d(a3, "onSuccess" + fj5.a());
                ql4.d b = this.this$0.a();
                Weather a4 = fj5.a();
                ee7.a((Object) a4, "mfWeatherResponse.weather");
                b.onSuccess(new d(a4));
            } else if (zi5 instanceof yi5) {
                this.this$0.a().a(new b(mb5.NETWORK_ERROR.name()));
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = gw6.class.getSimpleName();
        ee7.a((Object) simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public gw6(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    public void a(c cVar) {
        ee7.b(cVar, "requestValues");
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new e(this, cVar, null), 3, null);
    }
}
