package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z15 extends y15 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(8);
        z = iVar;
        iVar.a(1, new String[]{"item_heartrate_workout_day", "item_heartrate_workout_day"}, new int[]{2, 3}, new int[]{2131558693, 2131558693});
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131363144, 4);
        A.put(2131362796, 5);
        A.put(2131362330, 6);
        A.put(2131362625, 7);
    }
    */

    @DexIgnore
    public z15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
        ViewDataBinding.d(((y15) this).r);
        ViewDataBinding.d(((y15) this).s);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (((com.fossil.y15) r6).s.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.y15) r6).r.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.y     // Catch:{ all -> 0x0021 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0021 }
            com.fossil.s95 r0 = r6.r
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            com.fossil.s95 r0 = r6.s
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x001f
            return r4
        L_0x001f:
            r0 = 0
            return r0
        L_0x0021:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.z15.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 4;
        }
        ((y15) this).r.f();
        ((y15) this).s.f();
        g();
    }

    @DexIgnore
    public z15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 2, (FlexibleTextView) objArr[6], (s95) objArr[2], (s95) objArr[3], (ImageView) objArr[7], (LinearLayout) objArr[5], (LinearLayout) objArr[1], (TodayHeartRateChart) objArr[4]);
        this.y = -1;
        ((y15) this).v.setTag(null);
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.x = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
