package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static c<CommunicateMode, a> b; // = new c<>();
    @DexIgnore
    public static /* final */ HashMap<CommunicateMode, d<b>> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ nj5 d; // = new nj5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Intent a;

        @DexIgnore
        public a(CommunicateMode communicateMode, String str, Intent intent) {
            ee7.b(communicateMode, "mode");
            ee7.b(intent, "intent");
            this.a = intent;
        }

        @DexIgnore
        public final Intent a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CommunicateMode communicateMode, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1", f = "BleCommandResultManager.kt", l = {69}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $communicateModes;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ a $bleResult;
            @DexIgnore
            public /* final */ /* synthetic */ CommunicateMode $communicateMode;
            @DexIgnore
            public /* final */ /* synthetic */ d $observerList;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, CommunicateMode communicateMode, a aVar, fb7 fb7) {
                super(2, fb7);
                this.$observerList = dVar;
                this.$communicateMode = communicateMode;
                this.$bleResult = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$observerList, this.$communicateMode, this.$bleResult, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    for (b bVar : this.$observerList.a()) {
                        bVar.a(this.$communicateMode, this.$bleResult.a());
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(List list, fb7 fb7) {
            super(2, fb7);
            this.$communicateModes = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.$communicateModes, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0118  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0049  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r0 = r16
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 1
                if (r2 == 0) goto L_0x0035
                if (r2 != r3) goto L_0x002d
                java.lang.Object r2 = r0.L$6
                com.fossil.nj5$d r2 = (com.fossil.nj5.d) r2
                java.lang.Object r2 = r0.L$5
                com.fossil.nj5$a r2 = (com.fossil.nj5.a) r2
                java.lang.Object r4 = r0.L$4
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r4 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r4
                java.lang.Object r5 = r0.L$2
                java.util.Iterator r5 = (java.util.Iterator) r5
                java.lang.Object r6 = r0.L$1
                java.lang.Iterable r6 = (java.lang.Iterable) r6
                java.lang.Object r7 = r0.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r17)
                r3 = r0
                r8 = r4
                r4 = 1
                goto L_0x0109
            L_0x002d:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0035:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r2 = r0.p$
                java.util.List r4 = r0.$communicateModes
                java.util.Iterator r5 = r4.iterator()
                r7 = r2
                r6 = r4
                r2 = r0
            L_0x0043:
                boolean r4 = r5.hasNext()
                if (r4 == 0) goto L_0x0118
                java.lang.Object r4 = r5.next()
                r8 = r4
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r8 = (com.misfit.frameworks.buttonservice.communite.CommunicateMode) r8
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                com.fossil.nj5 r10 = com.fossil.nj5.d
                java.lang.String r10 = com.fossil.nj5.a
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "retrieve mode="
                r11.append(r12)
                r11.append(r8)
                java.lang.String r11 = r11.toString()
                r9.d(r10, r11)
                com.fossil.nj5 r9 = com.fossil.nj5.d
                com.fossil.nj5$c r9 = com.fossil.nj5.b
                java.lang.Object r9 = r9.a(r8)
                com.fossil.nj5$a r9 = (com.fossil.nj5.a) r9
                if (r9 == 0) goto L_0x0114
                com.fossil.nj5 r10 = com.fossil.nj5.d
                java.util.HashMap r10 = com.fossil.nj5.c
                java.lang.Object r10 = r10.get(r8)
                com.fossil.nj5$d r10 = (com.fossil.nj5.d) r10
                if (r10 == 0) goto L_0x0114
                int r11 = r10.b()
                if (r11 <= 0) goto L_0x0114
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r11 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
                if (r8 != r11) goto L_0x00c1
                com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
                com.fossil.nj5 r12 = com.fossil.nj5.d
                java.lang.String r12 = com.fossil.nj5.a
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "SyncMode="
                r13.append(r14)
                android.content.Intent r14 = r9.a()
                r15 = 3
                java.lang.String r3 = "sync_result"
                int r3 = r14.getIntExtra(r3, r15)
                r13.append(r3)
                java.lang.String r3 = r13.toString()
                r11.d(r12, r3)
            L_0x00c1:
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.nj5 r11 = com.fossil.nj5.d
                java.lang.String r11 = com.fossil.nj5.a
                java.lang.StringBuilder r12 = new java.lang.StringBuilder
                r12.<init>()
                java.lang.String r13 = "retrieve mode success="
                r12.append(r13)
                int r13 = r10.b()
                r12.append(r13)
                java.lang.String r12 = r12.toString()
                r3.d(r11, r12)
                com.fossil.tk7 r3 = com.fossil.qj7.c()
                com.fossil.nj5$e$a r11 = new com.fossil.nj5$e$a
                r12 = 0
                r11.<init>(r10, r8, r9, r12)
                r2.L$0 = r7
                r2.L$1 = r6
                r2.L$2 = r5
                r2.L$3 = r4
                r2.L$4 = r8
                r2.L$5 = r9
                r2.L$6 = r10
                r4 = 1
                r2.label = r4
                java.lang.Object r3 = com.fossil.vh7.a(r3, r11, r2)
                if (r3 != r1) goto L_0x0107
                return r1
            L_0x0107:
                r3 = r2
                r2 = r9
            L_0x0109:
                com.fossil.nj5 r9 = com.fossil.nj5.d
                com.fossil.nj5$c r9 = com.fossil.nj5.b
                r9.a(r8, r2)
                r2 = r3
                goto L_0x0115
            L_0x0114:
                r4 = 1
            L_0x0115:
                r3 = 1
                goto L_0x0043
            L_0x0118:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.nj5.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = nj5.class.getSimpleName();
        ee7.a((Object) simpleName, "BleCommandResultManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final synchronized void a(CommunicateMode communicateMode, a aVar) {
        ee7.b(communicateMode, "mode");
        ee7.b(aVar, "bleResult");
        b.b(communicateMode, aVar);
        a(communicateMode);
    }

    @DexIgnore
    public final void b(b bVar, CommunicateMode... communicateModeArr) {
        ee7.b(bVar, "observerReceiver");
        ee7.b(communicateModeArr, "communicateModes");
        b(bVar, w97.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> {
        @DexIgnore
        public /* final */ List<T> a; // = new ArrayList();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final List<T> a() {
            ArrayList arrayList;
            synchronized (this.b) {
                arrayList = new ArrayList();
                arrayList.addAll(this.a);
            }
            return arrayList;
        }

        @DexIgnore
        public final int b() {
            return this.a.size();
        }
    }

    @DexIgnore
    public final synchronized void b(b bVar, List<? extends CommunicateMode> list) {
        ee7.b(bVar, "observerReceiver");
        ee7.b(list, "communicateModes");
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            d<b> dVar = c.get(it.next());
            if (dVar != null) {
                dVar.b(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T, V> {
        @DexIgnore
        public /* final */ HashMap<T, V> a; // = new HashMap<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final boolean a(T t, V v) {
            boolean z;
            synchronized (this.b) {
                if (ee7.a((Object) this.a.get(t), (Object) v)) {
                    this.a.remove(t);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        @DexIgnore
        public final void b(T t, V v) {
            synchronized (this.b) {
                this.a.put(t, v);
                i97 i97 = i97.a;
            }
        }

        @DexIgnore
        public String toString() {
            String hashMap = this.a.toString();
            ee7.a((Object) hashMap, "mHashMap.toString()");
            return hashMap;
        }

        @DexIgnore
        public final V a(T t) {
            return this.a.get(t);
        }
    }

    @DexIgnore
    public final void a(b bVar, CommunicateMode... communicateModeArr) {
        ee7.b(bVar, "observerReceiver");
        ee7.b(communicateModeArr, "communicateModes");
        a(bVar, w97.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void a(CommunicateMode... communicateModeArr) {
        ee7.b(communicateModeArr, "communicateModes");
        a(w97.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final synchronized void a(List<? extends CommunicateMode> list) {
        ee7.b(list, "communicateModes");
        ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new e(list, null), 3, null);
    }

    @DexIgnore
    public final synchronized void a(b bVar, List<? extends CommunicateMode> list) {
        ee7.b(bVar, "observerReceiver");
        ee7.b(list, "communicateModes");
        for (T t : list) {
            d<b> dVar = c.get(t);
            if (dVar != null) {
                dVar.a(bVar);
            } else {
                HashMap<CommunicateMode, d<b>> hashMap = c;
                d<b> dVar2 = new d<>();
                dVar2.a(bVar);
                hashMap.put(t, dVar2);
            }
        }
    }
}
