package com.fossil;

import android.os.Build;
import androidx.fragment.app.Fragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nu7<T> {
    @DexIgnore
    public T a;

    @DexIgnore
    public nu7(T t) {
        this.a = t;
    }

    @DexIgnore
    public static nu7<Fragment> a(Fragment fragment) {
        if (Build.VERSION.SDK_INT < 23) {
            return new mu7(fragment);
        }
        return new ou7(fragment);
    }

    @DexIgnore
    public abstract boolean b(String str);

    @DexIgnore
    public boolean a(List<String> list) {
        for (String str : list) {
            if (a(str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(String str) {
        return !b(str);
    }

    @DexIgnore
    public T a() {
        return this.a;
    }
}
