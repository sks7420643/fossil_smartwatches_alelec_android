package com.fossil;

import com.fossil.fitness.FitnessData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh0 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ dn0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mh0(dn0 dn0) {
        super(1);
        this.a = dn0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        dn0 dn0 = this.a;
        FitnessData[] fitnessDataArr = ((b31) zk0).V;
        if (fitnessDataArr == null) {
            fitnessDataArr = new FitnessData[0];
        }
        dn0.D = fitnessDataArr;
        if (yp0.f.b(((zk0) this.a).x.a())) {
            dn0 dn02 = this.a;
            dn02.a(eu0.a(((zk0) dn02).v, null, is0.SUCCESS, null, 5));
        } else {
            dn0 dn03 = this.a;
            if (dn03.G > 0) {
                zk0.a(dn03, new wo1(((zk0) dn03).w, ((zk0) dn03).x, (HashMap) null, ((zk0) dn03).z, 4), new in1(dn03), new ip1(dn03), new hr1(dn03), (gd7) null, (gd7) null, 48, (Object) null);
            } else {
                dn03.a(eu0.a(((zk0) dn03).v, null, is0.SUCCESS, null, 5));
            }
        }
        return i97.a;
    }
}
