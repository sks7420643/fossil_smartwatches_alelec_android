package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nb {
    @DexIgnore
    public abstract ViewDataBinding a(pb pbVar, View view, int i);

    @DexIgnore
    public abstract ViewDataBinding a(pb pbVar, View[] viewArr, int i);

    @DexIgnore
    public List<nb> a() {
        return Collections.emptyList();
    }
}
