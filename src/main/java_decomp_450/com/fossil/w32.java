package com.fossil;

import android.util.Log;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w32 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ i02 a;
    @DexIgnore
    public /* final */ /* synthetic */ u12.c b;

    @DexIgnore
    public w32(u12.c cVar, i02 i02) {
        this.b = cVar;
        this.a = i02;
    }

    @DexIgnore
    public final void run() {
        u12.a aVar = (u12.a) u12.this.i.get(this.b.b);
        if (aVar != null) {
            if (this.a.x()) {
                boolean unused = this.b.e = true;
                if (this.b.a.n()) {
                    this.b.a();
                    return;
                }
                try {
                    this.b.a.a(null, this.b.a.e());
                } catch (SecurityException e) {
                    Log.e("GoogleApiManager", "Failed to get service from broker. ", e);
                    aVar.a(new i02(10));
                }
            } else {
                aVar.a(this.a);
            }
        }
    }
}
