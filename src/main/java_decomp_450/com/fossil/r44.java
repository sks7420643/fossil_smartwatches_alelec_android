package com.fossil;

import com.fossil.v54;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r44 implements Comparator {
    @DexIgnore
    public static /* final */ r44 a; // = new r44();

    @DexIgnore
    public static Comparator a() {
        return a;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((v54.b) obj).a().compareTo(((v54.b) obj2).a());
    }
}
