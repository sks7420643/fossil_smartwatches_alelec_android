package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gv2 {
    @DexIgnore
    public gv2() {
    }

    @DexIgnore
    public static int a(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    @DexIgnore
    public static long a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    @DexIgnore
    public static gv2 a(byte[] bArr, int i, int i2, boolean z) {
        hv2 hv2 = new hv2(bArr, 0, i2, false);
        try {
            hv2.b(i2);
            return hv2;
        } catch (iw2 e) {
            throw new IllegalArgumentException(e);
        }
    }
}
