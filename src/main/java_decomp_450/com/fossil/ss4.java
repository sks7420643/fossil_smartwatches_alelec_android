package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bu4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss4 extends go5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<o55> g;
    @DexIgnore
    public xs4 h;
    @DexIgnore
    public us4 i;
    @DexIgnore
    public TimerViewObserver j; // = new TimerViewObserver();
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ss4.q;
        }

        @DexIgnore
        public final ss4 b() {
            return new ss4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements InterceptSwipe.j {
        @DexIgnore
        public /* final */ /* synthetic */ o55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ss4 b;

        @DexIgnore
        public b(o55 o55, ss4 ss4) {
            this.a = o55;
            this.b = ss4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.a.q;
            ee7.a((Object) flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            ss4.d(this.b).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements bu4.b {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public c(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        @Override // com.fossil.bu4.b
        public void a(View view, int i) {
            ee7.b(view, "view");
            ss4.b(this.a).a(i);
            Object b = ss4.b(this.a).b(i);
            if (!(b instanceof jo4)) {
                b = null;
            }
            jo4 jo4 = (jo4) b;
            if (jo4 != null) {
                ss4.d(this.a).a(jo4, i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public d(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            o55 o55 = (o55) ss4.a(this.a).a();
            if (o55 != null) {
                InterceptSwipe interceptSwipe = o55.u;
                ee7.a((Object) interceptSwipe, "swipeRefresh");
                ee7.a((Object) bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public e(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ss4 ss4 = this.a;
            ee7.a((Object) bool, "it");
            ss4.T(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public f(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ss4.r.a();
            local.e(a2, "recommendedLive: " + list);
            this.a.j.a();
            xs4 b = ss4.b(this.a);
            ee7.a((Object) list, "it");
            b.a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends jo4, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public g(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<jo4, Integer> r87) {
            this.a.a(r87.getFirst().b(), r87.getFirst().a(), r87.getSecond().intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public h(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ss4.r.a();
            local.e(a2, "errorLive: - pair: " + r87);
            o55 o55 = (o55) ss4.a(this.a).a();
            if (o55 != null) {
                boolean booleanValue = r87.getFirst().booleanValue();
                ServerError serverError = (ServerError) r87.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = o55.r;
                    ee7.a((Object) flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = o55.r;
                ee7.a((Object) flexibleTextView2, "ftvError");
                String a3 = ig5.a(flexibleTextView2.getContext(), 2131886227);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, a3, 1).show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ ss4 a;

        @DexIgnore
        public i(ss4 ss4) {
            this.a = ss4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            if (ss4.b(this.a).getItemCount() == 2) {
                this.a.T(true);
            }
            xs4 b = ss4.b(this.a);
            ee7.a((Object) num, "it");
            b.c(num.intValue());
        }
    }

    /*
    static {
        String name = ss4.class.getName();
        ee7.a((Object) name, "BCRecommendationSubTabFragment::class.java.name");
        q = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(ss4 ss4) {
        qw6<o55> qw6 = ss4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xs4 b(ss4 ss4) {
        xs4 xs4 = ss4.h;
        if (xs4 != null) {
            return xs4;
        }
        ee7.d("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ us4 d(ss4 ss4) {
        us4 us4 = ss4.i;
        if (us4 != null) {
            return us4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<o55> qw6 = this.g;
        if (qw6 != null) {
            o55 a2 = qw6.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.u;
                ee7.a((Object) interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(false);
                if (z) {
                    FlexibleTextView flexibleTextView = a2.q;
                    ee7.a((Object) flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = a2.s;
                    ee7.a((Object) recyclerView, "rcvRecommended");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.q;
                ee7.a((Object) flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = a2.s;
                ee7.a((Object) recyclerView2, "rcvRecommended");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        xs4 xs4 = new xs4();
        this.h = xs4;
        if (xs4 != null) {
            xs4.a(this.j);
            qw6<o55> qw6 = this.g;
            if (qw6 != null) {
                o55 a2 = qw6.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.s;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    xs4 xs42 = this.h;
                    if (xs42 != null) {
                        recyclerView.setAdapter(xs42);
                        a2.u.setOnRefreshListener(new b(a2, this));
                        RecyclerView recyclerView2 = a2.s;
                        Context requireContext = requireContext();
                        ee7.a((Object) requireContext, "requireContext()");
                        recyclerView2.addOnItemTouchListener(new bu4(requireContext, new c(this)));
                        return;
                    }
                    ee7.d("recommendedAdapter");
                    throw null;
                }
                return;
            }
            ee7.d("binding");
            throw null;
        }
        ee7.d("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        us4 us4 = this.i;
        if (us4 != null) {
            us4.c().a(getViewLifecycleOwner(), new d(this));
            us4 us42 = this.i;
            if (us42 != null) {
                us42.a().a(getViewLifecycleOwner(), new e(this));
                us4 us43 = this.i;
                if (us43 != null) {
                    us43.d().a(getViewLifecycleOwner(), new f(this));
                    us4 us44 = this.i;
                    if (us44 != null) {
                        us44.e().a(getViewLifecycleOwner(), new g(this));
                        us4 us45 = this.i;
                        if (us45 != null) {
                            us45.b().a(getViewLifecycleOwner(), new h(this));
                            us4 us46 = this.i;
                            if (us46 != null) {
                                us46.f().a(getViewLifecycleOwner(), new i(this));
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void n(int i2) {
        us4 us4 = this.i;
        if (us4 != null) {
            us4.a(i2);
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().r().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(us4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.i = (us4) a2;
            getLifecycle().b(this.j);
            getLifecycle().a(this.j);
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        o55 o55 = (o55) qb.a(layoutInflater, 2131558613, viewGroup, false, a1());
        this.g = new qw6<>(this, o55);
        ee7.a((Object) o55, "bd");
        return o55.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().b(this.j);
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        us4 us4 = this.i;
        if (us4 != null) {
            us4.g();
            f1();
            g1();
            return;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void a(mn4 mn4, String str, int i2) {
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.z;
        Fragment requireParentFragment = requireParentFragment();
        ee7.a((Object) requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, mn4, str, i2, false);
    }
}
