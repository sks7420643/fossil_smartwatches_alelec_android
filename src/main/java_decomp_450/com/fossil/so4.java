package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so4 implements Factory<ro4> {
    @DexIgnore
    public /* final */ Provider<po4> a;
    @DexIgnore
    public /* final */ Provider<qo4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public so4(Provider<po4> provider, Provider<qo4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static so4 a(Provider<po4> provider, Provider<qo4> provider2, Provider<ch5> provider3) {
        return new so4(provider, provider2, provider3);
    }

    @DexIgnore
    public static ro4 a(po4 po4, qo4 qo4, ch5 ch5) {
        return new ro4(po4, qo4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ro4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
