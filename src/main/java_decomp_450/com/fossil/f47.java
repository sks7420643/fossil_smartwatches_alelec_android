package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class f47 extends Enum<f47> {
    @DexIgnore
    public static /* final */ f47 a; // = new f47("PAGE_VIEW", 0, 1);
    @DexIgnore
    public static /* final */ f47 b; // = new f47("SESSION_ENV", 1, 2);
    @DexIgnore
    public static /* final */ f47 c; // = new f47("ERROR", 2, 3);
    @DexIgnore
    public static /* final */ f47 d; // = new f47("CUSTOM", 3, 1000);
    @DexIgnore
    public static /* final */ f47 e; // = new f47("ADDITION", 4, 1001);
    @DexIgnore
    public static /* final */ f47 f; // = new f47("MONITOR_STAT", 5, 1002);
    @DexIgnore
    public static /* final */ f47 g; // = new f47("MTA_GAME_USER", 6, 1003);
    @DexIgnore
    public static /* final */ f47 h; // = new f47("NETWORK_MONITOR", 7, 1004);
    @DexIgnore
    public static /* final */ f47 i; // = new f47("NETWORK_DETECTOR", 8, 1005);
    @DexIgnore
    public int j;

    @DexIgnore
    public f47(String str, int i2, int i3) {
        this.j = i3;
    }

    @DexIgnore
    public final int a() {
        return this.j;
    }
}
