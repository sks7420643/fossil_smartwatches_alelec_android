package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq4 extends he {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public /* final */ LiveData<List<ao4>> e;
    @DexIgnore
    public /* final */ bp4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotification$1", f = "BCNotificationViewModel.kt", l = {57}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zq4 zq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.a.a(pb7.a(true));
                zq4 zq4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (zq4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel", f = "BCNotificationViewModel.kt", l = {68}, m = "fetchNotificationServer")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zq4 zq4, fb7 fb7) {
            super(fb7);
            this.this$0 = zq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotificationServer$result$1", f = "BCNotificationViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<List<ao4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zq4 zq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<List<ao4>>> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                bp4 b = this.this$0.f;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ zq4 a;

        @DexIgnore
        public e(zq4 zq4) {
            this.a = zq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<ao4>> apply(List<ao4> list) {
            MutableLiveData<List<ao4>> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.b(list);
            if (!list.isEmpty()) {
                this.a.b.a((Object) false);
                this.a.a.a((Object) false);
            } else if (!this.a.d) {
                this.a.b.a((Object) true);
            }
            if (this.a.d) {
                this.a.d = false;
                this.a.a();
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$refresh$1", f = "BCNotificationViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(zq4 zq4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = zq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                zq4 zq4 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                if (zq4.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = zq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCNotificationViewModel::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public zq4(bp4 bp4) {
        ee7.b(bp4, "notificationRepository");
        this.f = bp4;
        LiveData<List<ao4>> b2 = ge.b(au4.a(this.f.b()), new e(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.e = b2;
    }

    @DexIgnore
    public final void f() {
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<List<ao4>> e() {
        return this.e;
    }

    @DexIgnore
    public final void a() {
        ik7 unused = xh7.b(ie.a(this), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.zq4.c
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.zq4$c r0 = (com.fossil.zq4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.zq4$c r0 = new com.fossil.zq4$c
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x0036
            if (r2 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.zq4 r0 = (com.fossil.zq4) r0
            com.fossil.t87.a(r8)
            goto L_0x004e
        L_0x002e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0036:
            com.fossil.t87.a(r8)
            com.fossil.ti7 r8 = com.fossil.qj7.b()
            com.fossil.zq4$d r2 = new com.fossil.zq4$d
            r2.<init>(r7, r3)
            r0.L$0 = r7
            r0.label = r4
            java.lang.Object r8 = com.fossil.vh7.a(r8, r2, r0)
            if (r8 != r1) goto L_0x004d
            return r1
        L_0x004d:
            r0 = r7
        L_0x004e:
            com.fossil.ko4 r8 = (com.fossil.ko4) r8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.zq4.g
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "result: "
            r5.append(r6)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            r1.e(r2, r5)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r0.a
            r2 = 0
            java.lang.Boolean r5 = com.fossil.pb7.a(r2)
            r1.a(r5)
            java.lang.Object r1 = r8.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x008e
            boolean r8 = r1.isEmpty()
            if (r8 == 0) goto L_0x00ce
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r8 = r0.b
            java.lang.Boolean r0 = com.fossil.pb7.a(r4)
            r8.a(r0)
            goto L_0x00ce
        L_0x008e:
            androidx.lifecycle.LiveData<java.util.List<com.fossil.ao4>> r1 = r0.e
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x00bd
            androidx.lifecycle.LiveData<java.util.List<com.fossil.ao4>> r1 = r0.e
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x00b9
            java.util.List r1 = (java.util.List) r1
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x00a7
            goto L_0x00bd
        L_0x00a7:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r0.c
            java.lang.Boolean r1 = com.fossil.pb7.a(r2)
            com.portfolio.platform.data.model.ServerError r8 = r8.a()
            com.fossil.r87 r8 = com.fossil.w87.a(r1, r8)
            r0.a(r8)
            goto L_0x00ce
        L_0x00b9:
            com.fossil.ee7.a()
            throw r3
        L_0x00bd:
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r0 = r0.c
            java.lang.Boolean r1 = com.fossil.pb7.a(r4)
            com.portfolio.platform.data.model.ServerError r8 = r8.a()
            com.fossil.r87 r8 = com.fossil.w87.a(r1, r8)
            r0.a(r8)
        L_0x00ce:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zq4.a(com.fossil.fb7):java.lang.Object");
    }
}
