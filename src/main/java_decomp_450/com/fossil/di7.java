package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class di7 {
    @DexIgnore
    public static final <T> bi7<T> a(fb7<? super T> fb7) {
        if (!(fb7 instanceof lj7)) {
            return new bi7<>(fb7, 0);
        }
        bi7<T> c = ((lj7) fb7).c();
        if (c != null) {
            if (!c.l()) {
                c = null;
            }
            if (c != null) {
                return c;
            }
        }
        return new bi7<>(fb7, 0);
    }

    @DexIgnore
    public static final void a(ai7<?> ai7, bm7 bm7) {
        ai7.a(new yk7(bm7));
    }

    @DexIgnore
    public static final void a(ai7<?> ai7, rj7 rj7) {
        ai7.a(new sj7(rj7));
    }
}
