package com.fossil;

import com.fossil.ql4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sl4 {
    @DexIgnore
    <R extends ql4.c, E extends ql4.a> void a(E e, ql4.d<R, E> dVar);

    @DexIgnore
    <R extends ql4.c, E extends ql4.a> void a(R r, ql4.d<R, E> dVar);

    @DexIgnore
    void execute(Runnable runnable);
}
