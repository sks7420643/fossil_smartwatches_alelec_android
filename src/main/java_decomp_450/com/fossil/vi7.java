package com.fossil;

import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi7 {
    @DexIgnore
    public static final void a(ib7 ib7, Throwable th) {
        try {
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) ib7.get(CoroutineExceptionHandler.n);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(ib7, th);
            } else {
                ui7.a(ib7, th);
            }
        } catch (Throwable th2) {
            ui7.a(ib7, a(th, th2));
        }
    }

    @DexIgnore
    public static final Throwable a(Throwable th, Throwable th2) {
        if (th == th2) {
            return th;
        }
        RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
        i87.a(runtimeException, th);
        return runtimeException;
    }
}
