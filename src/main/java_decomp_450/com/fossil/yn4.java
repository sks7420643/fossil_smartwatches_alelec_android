package com.fossil;

import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn4 {
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("challengeType")
    public String b;
    @DexIgnore
    @te4("name")
    public String c;
    @DexIgnore
    @te4("description")
    public String d;
    @DexIgnore
    @te4("owner")
    public eo4 e;
    @DexIgnore
    @te4("numberOfPlayers")
    public Integer f;
    @DexIgnore
    @te4(SampleRaw.COLUMN_START_TIME)
    public Date g;
    @DexIgnore
    @te4(SampleRaw.COLUMN_END_TIME)
    public Date h;
    @DexIgnore
    @te4("target")
    public Integer i;
    @DexIgnore
    @te4("duration")
    public Integer j;
    @DexIgnore
    @te4(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String k;
    @DexIgnore
    @te4("version")
    public String l;
    @DexIgnore
    @te4("status")
    public String m;
    @DexIgnore
    @te4("players")
    public List<jn4> n;
    @DexIgnore
    public jn4 o;
    @DexIgnore
    public jn4 p;
    @DexIgnore
    @te4("createdAt")
    public Date q;
    @DexIgnore
    @te4("updatedAt")
    public Date r;
    @DexIgnore
    @te4("completedTime")
    public Date s;
    @DexIgnore
    public boolean t;

    @DexIgnore
    public yn4(String str, String str2, String str3, String str4, eo4 eo4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, List<jn4> list, jn4 jn4, jn4 jn42, Date date3, Date date4, Date date5, boolean z) {
        ee7.b(str, "id");
        ee7.b(list, "players");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = eo4;
        this.f = num;
        this.g = date;
        this.h = date2;
        this.i = num2;
        this.j = num3;
        this.k = str5;
        this.l = str6;
        this.m = str7;
        this.n = list;
        this.o = jn4;
        this.p = jn42;
        this.q = date3;
        this.r = date4;
        this.s = date5;
        this.t = z;
    }

    @DexIgnore
    public final void a(List<jn4> list) {
        ee7.b(list, "<set-?>");
        this.n = list;
    }

    @DexIgnore
    public final Date b() {
        return this.q;
    }

    @DexIgnore
    public final jn4 c() {
        return this.p;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final Integer e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yn4)) {
            return false;
        }
        yn4 yn4 = (yn4) obj;
        return ee7.a(this.a, yn4.a) && ee7.a(this.b, yn4.b) && ee7.a(this.c, yn4.c) && ee7.a(this.d, yn4.d) && ee7.a(this.e, yn4.e) && ee7.a(this.f, yn4.f) && ee7.a(this.g, yn4.g) && ee7.a(this.h, yn4.h) && ee7.a(this.i, yn4.i) && ee7.a(this.j, yn4.j) && ee7.a(this.k, yn4.k) && ee7.a(this.l, yn4.l) && ee7.a(this.m, yn4.m) && ee7.a(this.n, yn4.n) && ee7.a(this.o, yn4.o) && ee7.a(this.p, yn4.p) && ee7.a(this.q, yn4.q) && ee7.a(this.r, yn4.r) && ee7.a(this.s, yn4.s) && this.t == yn4.t;
    }

    @DexIgnore
    public final Date f() {
        return this.h;
    }

    @DexIgnore
    public final String g() {
        return this.a;
    }

    @DexIgnore
    public final String h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        eo4 eo4 = this.e;
        int hashCode5 = (hashCode4 + (eo4 != null ? eo4.hashCode() : 0)) * 31;
        Integer num = this.f;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        Date date = this.g;
        int hashCode7 = (hashCode6 + (date != null ? date.hashCode() : 0)) * 31;
        Date date2 = this.h;
        int hashCode8 = (hashCode7 + (date2 != null ? date2.hashCode() : 0)) * 31;
        Integer num2 = this.i;
        int hashCode9 = (hashCode8 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.j;
        int hashCode10 = (hashCode9 + (num3 != null ? num3.hashCode() : 0)) * 31;
        String str5 = this.k;
        int hashCode11 = (hashCode10 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.l;
        int hashCode12 = (hashCode11 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.m;
        int hashCode13 = (hashCode12 + (str7 != null ? str7.hashCode() : 0)) * 31;
        List<jn4> list = this.n;
        int hashCode14 = (hashCode13 + (list != null ? list.hashCode() : 0)) * 31;
        jn4 jn4 = this.o;
        int hashCode15 = (hashCode14 + (jn4 != null ? jn4.hashCode() : 0)) * 31;
        jn4 jn42 = this.p;
        int hashCode16 = (hashCode15 + (jn42 != null ? jn42.hashCode() : 0)) * 31;
        Date date3 = this.q;
        int hashCode17 = (hashCode16 + (date3 != null ? date3.hashCode() : 0)) * 31;
        Date date4 = this.r;
        int hashCode18 = (hashCode17 + (date4 != null ? date4.hashCode() : 0)) * 31;
        Date date5 = this.s;
        if (date5 != null) {
            i2 = date5.hashCode();
        }
        int i3 = (hashCode18 + i2) * 31;
        boolean z = this.t;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return i3 + i4;
    }

    @DexIgnore
    public final Integer i() {
        return this.f;
    }

    @DexIgnore
    public final eo4 j() {
        return this.e;
    }

    @DexIgnore
    public final List<jn4> k() {
        return this.n;
    }

    @DexIgnore
    public final String l() {
        return this.k;
    }

    @DexIgnore
    public final Date m() {
        return this.g;
    }

    @DexIgnore
    public final String n() {
        return this.m;
    }

    @DexIgnore
    public final Integer o() {
        return this.i;
    }

    @DexIgnore
    public final jn4 p() {
        return this.o;
    }

    @DexIgnore
    public final String q() {
        return this.b;
    }

    @DexIgnore
    public final Date r() {
        return this.r;
    }

    @DexIgnore
    public final String s() {
        return this.l;
    }

    @DexIgnore
    public final boolean t() {
        return this.t;
    }

    @DexIgnore
    public String toString() {
        return "HistoryChallenge(id=" + this.a + ", type=" + this.b + ", name=" + this.c + ", des=" + this.d + ", owner=" + this.e + ", numberOfPlayers=" + this.f + ", startTime=" + this.g + ", endTime=" + this.h + ", target=" + this.i + ", duration=" + this.j + ", privacy=" + this.k + ", version=" + this.l + ", status=" + this.m + ", players=" + this.n + ", topPlayer=" + this.o + ", currentPlayer=" + this.p + ", createdAt=" + this.q + ", updatedAt=" + this.r + ", completedAt=" + this.s + ", isFirst=" + this.t + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ yn4(String str, String str2, String str3, String str4, eo4 eo4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, List list, jn4 jn4, jn4 jn42, Date date3, Date date4, Date date5, boolean z, int i2, zd7 zd7) {
        this(str, str2, str3, str4, eo4, num, date, date2, num2, num3, str5, str6, str7, list, jn4, jn42, date3, date4, date5, (i2 & 524288) != 0 ? false : z);
    }

    @DexIgnore
    public final Date a() {
        return this.s;
    }
}
