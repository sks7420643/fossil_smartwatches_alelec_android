package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bh6;
import com.fossil.cy6;
import com.fossil.fm4;
import com.fossil.zg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg6 extends go5 implements cy6.g, zg6.b {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public bh6 f;
    @DexIgnore
    public qw6<i75> g;
    @DexIgnore
    public fm4 h;
    @DexIgnore
    public zg6 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wg6 a(String str) {
            ee7.b(str, "workoutSessionId");
            wg6 wg6 = new wg6();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WORKOUT_SESSION_ID", str);
            wg6.setArguments(bundle);
            return wg6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fm4.b {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public b(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        @Override // com.fossil.fm4.b
        public void a(cc5 cc5) {
            ee7.b(cc5, "workoutWrapperType");
            wg6.b(this.a).a(cc5);
            wg6.d(this.a).b(cc5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public c(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            zg6 c = this.a.i;
            if (c != null) {
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886682);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ctivity_Title__StartTime)");
                c.setTitle(a2);
            }
            zg6 c2 = this.a.i;
            if (c2 != null) {
                c2.a(vb5.START_TIME);
            }
            zg6 c3 = this.a.i;
            wb5 wb5 = null;
            if (c3 != null) {
                r87<wb5, wb5> b = wg6.b(this.a).b();
                c3.a(b != null ? b.getFirst() : null);
            }
            zg6 c4 = this.a.i;
            if (c4 != null) {
                r87<wb5, wb5> b2 = wg6.b(this.a).b();
                if (b2 != null) {
                    wb5 = b2.getSecond();
                }
                c4.b(wb5);
            }
            zg6 c5 = this.a.i;
            if (c5 != null) {
                c5.a(wg6.b(this.a).d());
            }
            zg6 c6 = this.a.i;
            if (c6 != null) {
                c6.b(wg6.b(this.a).e());
            }
            zg6 c7 = this.a.i;
            if (c7 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                c7.show(childFragmentManager, dh6.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public d(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            zg6 c = this.a.i;
            if (c != null) {
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886680);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026Activity_Label__Duration)");
                c.setTitle(a2);
            }
            zg6 c2 = this.a.i;
            if (c2 != null) {
                c2.a(vb5.DURATION);
            }
            zg6 c3 = this.a.i;
            wb5 wb5 = null;
            if (c3 != null) {
                r87<wb5, wb5> a3 = wg6.b(this.a).a();
                c3.a(a3 != null ? a3.getFirst() : null);
            }
            zg6 c4 = this.a.i;
            if (c4 != null) {
                r87<wb5, wb5> a4 = wg6.b(this.a).a();
                if (a4 != null) {
                    wb5 = a4.getSecond();
                }
                c4.b(wb5);
            }
            zg6 c5 = this.a.i;
            if (c5 != null) {
                c5.c(wg6.b(this.a).f());
            }
            zg6 c6 = this.a.i;
            if (c6 != null) {
                c6.a(wg6.b(this.a).d());
            }
            zg6 c7 = this.a.i;
            if (c7 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                c7.show(childFragmentManager, dh6.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public e(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public f(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<bh6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ wg6 a;

        @DexIgnore
        public g(wg6 wg6) {
            this.a = wg6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bh6.b bVar) {
            WorkoutSession d = bVar.d();
            if (d != null) {
                this.a.c(d);
            }
            Boolean f = bVar.f();
            if (f != null) {
                this.a.T(f.booleanValue());
            }
            if (bVar.a()) {
                this.a.j();
            } else {
                this.a.h();
            }
            Boolean c = bVar.c();
            if (c != null && c.booleanValue()) {
                this.a.v();
            }
            r87<Integer, String> b = bVar.b();
            if (b != null) {
                this.a.a(b.getFirst().intValue(), b.getSecond());
            }
            List<cc5> e = bVar.e();
            if (e != null) {
                wg6.d(this.a).a(e);
            }
        }
    }

    /*
    static {
        String simpleName = wg6.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutEditFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ bh6 b(wg6 wg6) {
        bh6 bh6 = wg6.f;
        if (bh6 != null) {
            return bh6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ fm4 d(wg6 wg6) {
        fm4 fm4 = wg6.h;
        if (fm4 != null) {
            return fm4;
        }
        ee7.d("mWorkoutTypeAdapter");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<i75> qw6 = this.g;
        if (qw6 != null) {
            i75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                ee7.a((Object) flexibleButton, "it.fbSave");
                flexibleButton.setEnabled(z);
                if (z) {
                    a2.s.a("flexible_button_primary");
                } else {
                    a2.s.a("flexible_button_disabled");
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        bh6 bh6 = this.f;
        if (bh6 == null) {
            ee7.d("mViewModel");
            throw null;
        } else if (bh6.g()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.V(childFragmentManager);
            return true;
        } else {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return true;
            }
            activity.finish();
            return true;
        }
    }

    @DexIgnore
    public final void f1() {
        bh6 bh6 = this.f;
        if (bh6 != null) {
            bh6.h();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void h() {
        a();
    }

    @DexIgnore
    public final void j() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        i75 i75 = (i75) qb.a(LayoutInflater.from(getContext()), 2131558640, null, false, a1());
        PortfolioApp.g0.c().f().f().a(this);
        rj4 rj4 = this.j;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(bh6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
            this.f = (bh6) a2;
            zg6 zg6 = (zg6) getChildFragmentManager().b(zg6.F.a());
            this.i = zg6;
            if (zg6 == null) {
                this.i = zg6.F.b();
            }
            zg6 zg62 = this.i;
            if (zg62 != null) {
                zg62.a(this);
                i75.A.setOnClickListener(new c(this));
                i75.y.setOnClickListener(new d(this));
                i75.s.setOnClickListener(new e(this));
                i75.t.setOnClickListener(new f(this));
                fm4 fm4 = new fm4(null, null, 3, null);
                fm4.a(new b(this));
                this.h = fm4;
                RecyclerView recyclerView = i75.v;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                fm4 fm42 = this.h;
                if (fm42 != null) {
                    recyclerView.setAdapter(fm42);
                    bh6 bh6 = this.f;
                    if (bh6 != null) {
                        bh6.c().a(getViewLifecycleOwner(), new g(this));
                        this.g = new qw6<>(this, i75);
                        ee7.a((Object) i75, "binding");
                        return i75.d();
                    }
                    ee7.d("mViewModel");
                    throw null;
                }
                ee7.d("mWorkoutTypeAdapter");
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        bh6 bh6 = this.f;
        if (bh6 != null) {
            Bundle arguments = getArguments();
            String string = arguments != null ? arguments.getString("EXTRA_WORKOUT_SESSION_ID") : null;
            if (string != null) {
                ee7.a((Object) string, "arguments?.getString(EXTRA_WORKOUT_SESSION_ID)!!");
                bh6.a(string);
                jf5 c1 = c1();
                if (c1 != null) {
                    c1.d();
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void c(WorkoutSession workoutSession) {
        Integer second;
        Integer second2;
        qw6<i75> qw6 = this.g;
        if (qw6 != null) {
            i75 a2 = qw6.a();
            if (a2 != null) {
                DateTime editedEndTime = workoutSession.getEditedEndTime();
                if (editedEndTime != null) {
                    long millis = editedEndTime.getMillis();
                    DateTime editedStartTime = workoutSession.getEditedStartTime();
                    if (editedStartTime != null) {
                        v87<Integer, Integer, Integer> d2 = zd5.d((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                        ee7.a((Object) d2, "DateHelper.getTimeValues(duration.toInt())");
                        cc5 a3 = cc5.Companion.a(workoutSession.getEditedType(), workoutSession.getEditedMode());
                        fm4 fm4 = this.h;
                        if (fm4 != null) {
                            fm4.b(a3);
                            FlexibleTextView flexibleTextView = a2.A;
                            ee7.a((Object) flexibleTextView, "binding.tvStartTimeValue");
                            DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                            flexibleTextView.setText(zd5.u(editedStartTime2 != null ? editedStartTime2.toDate() : null));
                            Integer first = d2.getFirst();
                            if (first != null && first.intValue() == 0 && ((second2 = d2.getSecond()) == null || second2.intValue() != 0)) {
                                FlexibleTextView flexibleTextView2 = a2.y;
                                ee7.a((Object) flexibleTextView2, "binding.tvDurationValue");
                                we7 we7 = we7.a;
                                String string = getResources().getString(2131886651);
                                ee7.a((Object) string, "resources.getString(R.st\u2026ilPage_Label__NumberMins)");
                                String format = String.format(string, Arrays.copyOf(new Object[]{d2.getSecond()}, 1));
                                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                flexibleTextView2.setText(format);
                            } else {
                                Integer first2 = d2.getFirst();
                                if ((first2 != null && first2.intValue() == 0) || (second = d2.getSecond()) == null || second.intValue() != 0) {
                                    FlexibleTextView flexibleTextView3 = a2.y;
                                    ee7.a((Object) flexibleTextView3, "binding.tvDurationValue");
                                    we7 we72 = we7.a;
                                    String string2 = getResources().getString(2131886609);
                                    ee7.a((Object) string2, "resources.getString(R.st\u2026bel__NumberHrsNumberMins)");
                                    String format2 = String.format(string2, Arrays.copyOf(new Object[]{d2.getFirst(), d2.getSecond()}, 2));
                                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                                    flexibleTextView3.setText(format2);
                                } else {
                                    FlexibleTextView flexibleTextView4 = a2.y;
                                    ee7.a((Object) flexibleTextView4, "binding.tvDurationValue");
                                    we7 we73 = we7.a;
                                    String string3 = getResources().getString(2131886203);
                                    ee7.a((Object) string3, "resources.getString(R.st\u2026enge_Label__StartInHours)");
                                    String format3 = String.format(string3, Arrays.copyOf(new Object[]{d2.getFirst()}, 1));
                                    ee7.a((Object) format3, "java.lang.String.format(format, *args)");
                                    flexibleTextView4.setText(format3);
                                }
                            }
                            fm4 fm42 = this.h;
                            if (fm42 != null) {
                                int a4 = fm42.a(a3);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str = q;
                                local.d(str, "receiveWorkoutSession scrollTo " + a4);
                                if (a4 >= 0) {
                                    a2.v.scrollToPosition(a4);
                                    return;
                                }
                                return;
                            }
                            ee7.d("mWorkoutTypeAdapter");
                            throw null;
                        }
                        ee7.d("mWorkoutTypeAdapter");
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ee7.b(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).a(str, i2, intent);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363229) {
            v();
        } else if (i2 == 2131363307) {
            f1();
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.zg6.b
    public void a(vb5 vb5, int i2, Integer num, float f2, Float f3) {
        ee7.b(vb5, "editMode");
        int i3 = xg6.a[vb5.ordinal()];
        int i4 = 0;
        if (i3 == 1) {
            bh6 bh6 = this.f;
            if (bh6 != null) {
                if (num != null) {
                    i4 = num.intValue();
                }
                bh6.b(i2, i4);
                return;
            }
            ee7.d("mViewModel");
            throw null;
        } else if (i3 == 2) {
            bh6 bh62 = this.f;
            if (bh62 != null) {
                if (num != null) {
                    i4 = num.intValue();
                }
                bh62.a(i2, i4);
                return;
            }
            ee7.d("mViewModel");
            throw null;
        } else if (i3 == 3) {
            bh6 bh63 = this.f;
            if (bh63 != null) {
                float f4 = (float) i2;
                if (num != null) {
                    i4 = num.intValue();
                }
                float f5 = (float) i4;
                if (f3 != null) {
                    bh63.a((double) re5.c(f4 + (f5 * f3.floatValue()), 1));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else if (i3 == 4) {
            bh6 bh64 = this.f;
            if (bh64 != null) {
                bh64.a((int) (((float) i2) * f2));
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }
}
