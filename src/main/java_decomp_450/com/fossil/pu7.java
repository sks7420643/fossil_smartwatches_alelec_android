package com.fossil;

import com.fossil.tu7;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu7 extends tu7.a {
    @DexIgnore
    public boolean a; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tu7<mo7, mo7> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public mo7 a(mo7 mo7) throws IOException {
            try {
                return jv7.a(mo7);
            } finally {
                mo7.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tu7<RequestBody, RequestBody> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.tu7
        public /* bridge */ /* synthetic */ RequestBody a(RequestBody requestBody) throws IOException {
            RequestBody requestBody2 = requestBody;
            a(requestBody2);
            return requestBody2;
        }

        @DexIgnore
        public RequestBody a(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tu7<mo7, mo7> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public mo7 a(mo7 mo7) {
            return mo7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.tu7
        public /* bridge */ /* synthetic */ mo7 a(mo7 mo7) throws IOException {
            mo7 mo72 = mo7;
            a(mo72);
            return mo72;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements tu7<Object, String> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        @Override // com.fossil.tu7
        public String a(Object obj) {
            return obj.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements tu7<mo7, i97> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public i97 a(mo7 mo7) {
            mo7.close();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements tu7<mo7, Void> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public Void a(mo7 mo7) {
            mo7.close();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.tu7.a
    public tu7<mo7, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == mo7.class) {
            if (jv7.a(annotationArr, (Class<? extends Annotation>) qw7.class)) {
                return c.a;
            }
            return a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (!this.a || type != i97.class) {
                return null;
            }
            try {
                return e.a;
            } catch (NoClassDefFoundError unused) {
                this.a = false;
                return null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.tu7.a
    public tu7<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(jv7.b(type))) {
            return b.a;
        }
        return null;
    }
}
