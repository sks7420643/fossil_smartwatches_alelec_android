package com.fossil;

import android.content.Context;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu4 implements RecyclerView.p {
    @DexIgnore
    public long a;
    @DexIgnore
    public GestureDetector b;
    @DexIgnore
    public /* final */ b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            ee7.b(motionEvent, "e");
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i);
    }

    @DexIgnore
    public bu4(Context context, b bVar) {
        ee7.b(context, "context");
        this.c = bVar;
        this.b = new GestureDetector(context, new a());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
        ee7.b(recyclerView, "view");
        ee7.b(motionEvent, "motionEvent");
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public void a(boolean z) {
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.p
    public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
        ee7.b(recyclerView, "view");
        ee7.b(motionEvent, "e");
        View findChildViewUnder = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
        if (findChildViewUnder == null || this.c == null || !this.b.onTouchEvent(motionEvent) || SystemClock.elapsedRealtime() - this.a < 1000) {
            return false;
        }
        this.a = SystemClock.elapsedRealtime();
        b bVar = this.c;
        View rootView = findChildViewUnder.getRootView();
        ee7.a((Object) rootView, "childView.rootView");
        bVar.a(rootView, recyclerView.getChildAdapterPosition(findChildViewUnder));
        return false;
    }
}
