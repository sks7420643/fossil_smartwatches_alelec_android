package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k36 implements Factory<j36> {
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> a;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> b;

    @DexIgnore
    public k36(Provider<WatchFaceRepository> provider, Provider<RingStyleRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static k36 a(Provider<WatchFaceRepository> provider, Provider<RingStyleRepository> provider2) {
        return new k36(provider, provider2);
    }

    @DexIgnore
    public static j36 a(WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository) {
        return new j36(watchFaceRepository, ringStyleRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public j36 get() {
        return a(this.a.get(), this.b.get());
    }
}
