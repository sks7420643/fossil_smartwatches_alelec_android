package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qx4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ RTLImageView G;
    @DexIgnore
    public /* final */ RTLImageView H;
    @DexIgnore
    public /* final */ View I;
    @DexIgnore
    public /* final */ LinearLayout J;
    @DexIgnore
    public /* final */ FlexibleProgressBar K;
    @DexIgnore
    public /* final */ ConstraintLayout L;
    @DexIgnore
    public /* final */ RecyclerView M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ View O;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ CoordinatorLayout v;
    @DexIgnore
    public /* final */ OverviewDayChart w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public qx4(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, CoordinatorLayout coordinatorLayout, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, View view2, LinearLayout linearLayout, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout5, RecyclerView recyclerView, FlexibleTextView flexibleTextView9, View view3) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = constraintLayout4;
        this.v = coordinatorLayout;
        this.w = overviewDayChart;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = flexibleTextView5;
        this.C = flexibleTextView6;
        this.D = flexibleTextView7;
        this.E = flexibleTextView8;
        this.F = rTLImageView;
        this.G = rTLImageView2;
        this.H = rTLImageView3;
        this.I = view2;
        this.J = linearLayout;
        this.K = flexibleProgressBar;
        this.L = constraintLayout5;
        this.M = recyclerView;
        this.N = flexibleTextView9;
        this.O = view3;
    }
}
