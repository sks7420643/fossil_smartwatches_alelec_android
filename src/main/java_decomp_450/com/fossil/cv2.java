package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv2 {
    @DexIgnore
    public /* final */ iv2 a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public cv2(int i) {
        byte[] bArr = new byte[i];
        this.b = bArr;
        this.a = iv2.a(bArr);
    }

    @DexIgnore
    public final tu2 a() {
        this.a.b();
        return new ev2(this.b);
    }

    @DexIgnore
    public final iv2 b() {
        return this.a;
    }

    @DexIgnore
    public /* synthetic */ cv2(int i, su2 su2) {
        this(i);
    }
}
