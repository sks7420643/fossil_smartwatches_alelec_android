package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ug3 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ xl3 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public ug3(xl3 xl3) {
        a72.a(xl3);
        this.a = xl3;
    }

    @DexIgnore
    public final void a() {
        this.a.q();
        this.a.c().g();
        if (!this.b) {
            this.a.f().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.c = this.a.j().t();
            this.a.e().B().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
            this.b = true;
        }
    }

    @DexIgnore
    public final void b() {
        this.a.q();
        this.a.c().g();
        this.a.c().g();
        if (this.b) {
            this.a.e().B().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.f().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.a.e().t().a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        this.a.q();
        String action = intent.getAction();
        this.a.e().B().a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean t = this.a.j().t();
            if (this.c != t) {
                this.c = t;
                this.a.c().a(new tg3(this, t));
                return;
            }
            return;
        }
        this.a.e().w().a("NetworkBroadcastReceiver received unknown action", action);
    }
}
