package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa3 implements Parcelable.Creator<p93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ p93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        ArrayList arrayList = null;
        b93 b93 = null;
        b93 b932 = null;
        ArrayList arrayList2 = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    arrayList = j72.c(parcel, a, LatLng.CREATOR);
                    break;
                case 3:
                    f = j72.n(parcel, a);
                    break;
                case 4:
                    i = j72.q(parcel, a);
                    break;
                case 5:
                    f2 = j72.n(parcel, a);
                    break;
                case 6:
                    z = j72.i(parcel, a);
                    break;
                case 7:
                    z2 = j72.i(parcel, a);
                    break;
                case 8:
                    z3 = j72.i(parcel, a);
                    break;
                case 9:
                    b93 = (b93) j72.a(parcel, a, b93.CREATOR);
                    break;
                case 10:
                    b932 = (b93) j72.a(parcel, a, b93.CREATOR);
                    break;
                case 11:
                    i2 = j72.q(parcel, a);
                    break;
                case 12:
                    arrayList2 = j72.c(parcel, a, l93.CREATOR);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new p93(arrayList, f, i, f2, z, z2, z3, b93, b932, i2, arrayList2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ p93[] newArray(int i) {
        return new p93[i];
    }
}
