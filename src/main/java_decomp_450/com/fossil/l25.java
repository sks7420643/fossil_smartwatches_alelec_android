package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l25 extends k25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i T;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(28);
        T = iVar;
        iVar.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558835});
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362335, 3);
        U.put(2131362334, 4);
        U.put(2131362747, 5);
        U.put(2131361960, 6);
        U.put(2131362977, 7);
        U.put(2131362470, 8);
        U.put(2131362768, 9);
        U.put(2131362343, 10);
        U.put(2131362736, 11);
        U.put(2131362344, 12);
        U.put(2131363390, 13);
        U.put(2131362388, 14);
        U.put(2131362490, 15);
        U.put(2131363110, 16);
        U.put(2131363391, 17);
        U.put(2131362097, 18);
        U.put(2131362493, 19);
        U.put(2131362788, 20);
        U.put(2131362494, 21);
        U.put(2131361909, 22);
        U.put(2131362491, 23);
        U.put(2131362787, 24);
        U.put(2131362492, 25);
        U.put(2131361910, 26);
        U.put(2131362461, 27);
    }
    */

    @DexIgnore
    public l25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 28, T, U));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.S = 0;
        }
        ViewDataBinding.d(((k25) this).H);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.k25) r6).H.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.S     // Catch:{ all -> 0x0018 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            com.fossil.ua5 r0 = r6.H
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            r0 = 0
            return r0
        L_0x0018:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.l25.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.S = 2;
        }
        ((k25) this).H.f();
        g();
    }

    @DexIgnore
    public l25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 1, (View) objArr[22], (View) objArr[26], (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[18], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[21], (ua5) objArr[2], (RTLImageView) objArr[11], (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[9], (LinearLayout) objArr[24], (LinearLayout) objArr[20], (ConstraintLayout) objArr[0], (RecyclerView) objArr[7], (FlexibleSwitchCompat) objArr[16], (View) objArr[13], (View) objArr[17]);
        this.S = -1;
        ((k25) this).t.setTag(null);
        ((k25) this).N.setTag(null);
        a(view);
        f();
    }
}
