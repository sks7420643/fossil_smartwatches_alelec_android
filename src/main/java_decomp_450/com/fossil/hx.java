package com.fossil;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx extends OutputStream {
    @DexIgnore
    public /* final */ OutputStream a;
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public az c;
    @DexIgnore
    public int d;

    @DexIgnore
    public hx(OutputStream outputStream, az azVar) {
        this(outputStream, azVar, 65536);
    }

    @DexIgnore
    public final void a() throws IOException {
        int i = this.d;
        if (i > 0) {
            this.a.write(this.b, 0, i);
            this.d = 0;
        }
    }

    @DexIgnore
    public final void b() throws IOException {
        if (this.d == this.b.length) {
            a();
        }
    }

    @DexIgnore
    public final void c() {
        byte[] bArr = this.b;
        if (bArr != null) {
            this.c.a(bArr);
            this.b = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        try {
            flush();
            this.a.close();
            c();
        } catch (Throwable th) {
            this.a.close();
            throw th;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() throws IOException {
        a();
        this.a.flush();
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(int i) throws IOException {
        byte[] bArr = this.b;
        int i2 = this.d;
        this.d = i2 + 1;
        bArr[i2] = (byte) i;
        b();
    }

    @DexIgnore
    public hx(OutputStream outputStream, az azVar, int i) {
        this.a = outputStream;
        this.c = azVar;
        this.b = (byte[]) azVar.b(i, byte[].class);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.d != 0 || i4 < this.b.length) {
                int min = Math.min(i4, this.b.length - this.d);
                System.arraycopy(bArr, i5, this.b, this.d, min);
                this.d += min;
                i3 += min;
                b();
            } else {
                this.a.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
