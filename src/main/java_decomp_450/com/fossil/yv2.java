package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yv2 implements kx2 {
    @DexIgnore
    public static /* final */ yv2 a; // = new yv2();

    @DexIgnore
    public static yv2 a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.kx2
    public final boolean zza(Class<?> cls) {
        return bw2.class.isAssignableFrom(cls);
    }

    @DexIgnore
    @Override // com.fossil.kx2
    public final hx2 zzb(Class<?> cls) {
        if (!bw2.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (hx2) bw2.a(cls.asSubclass(bw2.class)).a(bw2.f.c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
