package com.fossil;

import android.content.Context;
import android.os.PowerManager;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.foreground.SystemForegroundService;
import com.fossil.en;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xm implements vm, io {
    @DexIgnore
    public static /* final */ String q; // = im.a("Processor");
    @DexIgnore
    public PowerManager.WakeLock a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public zl c;
    @DexIgnore
    public vp d;
    @DexIgnore
    public WorkDatabase e;
    @DexIgnore
    public Map<String, en> f; // = new HashMap();
    @DexIgnore
    public Map<String, en> g; // = new HashMap();
    @DexIgnore
    public List<ym> h;
    @DexIgnore
    public Set<String> i;
    @DexIgnore
    public /* final */ List<vm> j;
    @DexIgnore
    public /* final */ Object p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public vm a;
        @DexIgnore
        public String b;
        @DexIgnore
        public i14<Boolean> c;

        @DexIgnore
        public a(vm vmVar, String str, i14<Boolean> i14) {
            this.a = vmVar;
            this.b = str;
            this.c = i14;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.c.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.a.a(this.b, z);
        }
    }

    @DexIgnore
    public xm(Context context, zl zlVar, vp vpVar, WorkDatabase workDatabase, List<ym> list) {
        this.b = context;
        this.c = zlVar;
        this.d = vpVar;
        this.e = workDatabase;
        this.h = list;
        this.i = new HashSet();
        this.j = new ArrayList();
        this.a = null;
        this.p = new Object();
    }

    @DexIgnore
    public boolean a(String str, WorkerParameters.a aVar) {
        synchronized (this.p) {
            if (c(str)) {
                im.a().a(q, String.format("Work %s is already enqueued for processing", str), new Throwable[0]);
                return false;
            }
            en.c cVar = new en.c(this.b, this.c, this.d, this, this.e, str);
            cVar.a(this.h);
            cVar.a(aVar);
            en a2 = cVar.a();
            i14<Boolean> a3 = a2.a();
            a3.a(new a(this, str, a3), this.d.a());
            this.g.put(str, a2);
            this.d.b().execute(a2);
            im.a().a(q, String.format("%s: processing %s", xm.class.getSimpleName(), str), new Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        boolean contains;
        synchronized (this.p) {
            contains = this.i.contains(str);
        }
        return contains;
    }

    @DexIgnore
    public boolean c(String str) {
        boolean z;
        synchronized (this.p) {
            if (!this.g.containsKey(str)) {
                if (!this.f.containsKey(str)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public boolean d(String str) {
        boolean containsKey;
        synchronized (this.p) {
            containsKey = this.f.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public boolean e(String str) {
        return a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public boolean f(String str) {
        boolean a2;
        synchronized (this.p) {
            boolean z = true;
            im.a().a(q, String.format("Processor cancelling %s", str), new Throwable[0]);
            this.i.add(str);
            en remove = this.f.remove(str);
            if (remove == null) {
                z = false;
            }
            if (remove == null) {
                remove = this.g.remove(str);
            }
            a2 = a(str, remove);
            if (z) {
                a();
            }
        }
        return a2;
    }

    @DexIgnore
    public boolean g(String str) {
        boolean a2;
        synchronized (this.p) {
            im.a().a(q, String.format("Processor stopping foreground work %s", str), new Throwable[0]);
            a2 = a(str, this.f.remove(str));
        }
        return a2;
    }

    @DexIgnore
    public boolean h(String str) {
        boolean a2;
        synchronized (this.p) {
            im.a().a(q, String.format("Processor stopping background work %s", str), new Throwable[0]);
            a2 = a(str, this.g.remove(str));
        }
        return a2;
    }

    @DexIgnore
    public void b(vm vmVar) {
        synchronized (this.p) {
            this.j.remove(vmVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.io
    public void a(String str) {
        synchronized (this.p) {
            this.f.remove(str);
            a();
        }
    }

    @DexIgnore
    public void a(vm vmVar) {
        synchronized (this.p) {
            this.j.add(vmVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        synchronized (this.p) {
            this.g.remove(str);
            im.a().a(q, String.format("%s %s executed; reschedule = %s", getClass().getSimpleName(), str, Boolean.valueOf(z)), new Throwable[0]);
            for (vm vmVar : this.j) {
                vmVar.a(str, z);
            }
        }
    }

    @DexIgnore
    public final void a() {
        synchronized (this.p) {
            if (!(!this.f.isEmpty())) {
                SystemForegroundService d2 = SystemForegroundService.d();
                if (d2 != null) {
                    im.a().a(q, "No more foreground work. Stopping SystemForegroundService", new Throwable[0]);
                    d2.c();
                } else {
                    im.a().a(q, "No more foreground work. SystemForegroundService is already stopped", new Throwable[0]);
                }
                if (this.a != null) {
                    this.a.release();
                    this.a = null;
                }
            }
        }
    }

    @DexIgnore
    public static boolean a(String str, en enVar) {
        if (enVar != null) {
            enVar.b();
            im.a().a(q, String.format("WorkerWrapper interrupted for %s", str), new Throwable[0]);
            return true;
        }
        im.a().a(q, String.format("WorkerWrapper could not be found for %s", str), new Throwable[0]);
        return false;
    }
}
