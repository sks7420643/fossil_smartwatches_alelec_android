package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj2 extends bj2 {
    @DexIgnore
    public /* final */ s12<Status> a;

    @DexIgnore
    public nj2(s12<Status> s12) {
        this.a = s12;
    }

    @DexIgnore
    @Override // com.fossil.zi2
    public final void c(Status status) {
        this.a.a(status);
    }
}
