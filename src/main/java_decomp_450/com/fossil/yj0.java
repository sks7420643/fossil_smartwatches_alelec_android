package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj0 {
    @DexIgnore
    public static /* final */ String a; // = yh0.a("UUID.randomUUID().toString()");
    @DexIgnore
    public static /* final */ Hashtable<String, m60> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
    @DexIgnore
    public static sn0 d; // = new sn0(u31.g.b(), u31.g.c(), u31.g.e(), u31.g.d(), "5.12.6-production-release", null, 32);
    @DexIgnore
    public static /* final */ yj0 e; // = new yj0();

    /*
    static {
        String string;
        SharedPreferences a2 = yz0.a(j91.b);
        if (a2 != null && (string = a2.getString("log.device.mapping", null)) != null) {
            ee7.a((Object) string, "getSharedPreferences(Sha\u2026_MAP_KEY, null) ?: return");
            String a3 = yn0.h.a(string);
            if (a3 != null) {
                try {
                    JSONObject jSONObject = new JSONObject(a3);
                    b.clear();
                    b.putAll(e.a(jSONObject));
                } catch (Exception e2) {
                    wl0.h.a(e2);
                }
            }
        }
    }
    */

    @DexIgnore
    public final sn0 a() {
        return d;
    }

    @DexIgnore
    public final String b(String str) {
        String str2 = c.get(str);
        return str2 != null ? str2 : a;
    }

    @DexIgnore
    public final m60 a(String str) {
        return b.get(str);
    }

    @DexIgnore
    public final void a(String str, m60 m60) {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putString;
        m60 m602 = b.get(str);
        Hashtable<String, m60> hashtable = b;
        if (m602 != null) {
            m60 = m60.t.a(m602, m60);
        }
        hashtable.put(str, m60);
        Hashtable<String, m60> hashtable2 = b;
        JSONObject jSONObject = new JSONObject();
        for (T t : hashtable2.keySet()) {
            m60 m603 = hashtable2.get(t);
            if (m603 != null) {
                jSONObject.put(t, m603.a());
            }
        }
        String jSONObject2 = jSONObject.toString();
        ee7.a((Object) jSONObject2, "convertDeviceInfoMapToJS\u2026formationMaps).toString()");
        String b2 = yn0.h.b(jSONObject2);
        SharedPreferences a2 = yz0.a(j91.b);
        if (a2 != null && (edit = a2.edit()) != null && (putString = edit.putString("log.device.mapping", b2)) != null) {
            putString.apply();
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        c.put(str, str2);
    }

    @DexIgnore
    public final HashMap<String, m60> a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        m60 a2;
        HashMap<String, m60> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        ee7.a((Object) keys, "jsonObject.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            if (!(!BluetoothAdapter.checkBluetoothAddress(next) || (optJSONObject = jSONObject.optJSONObject(next)) == null || (a2 = m60.t.a(optJSONObject)) == null)) {
                ee7.a((Object) next, "key");
                hashMap.put(next, a2);
            }
        }
        return hashMap;
    }
}
