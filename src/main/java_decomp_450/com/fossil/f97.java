package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f97 implements Comparable<f97> {
    @DexIgnore
    public /* final */ short a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public /* synthetic */ f97(short s) {
        this.a = s;
    }

    @DexIgnore
    public static boolean a(short s, Object obj) {
        return (obj instanceof f97) && s == ((f97) obj).a();
    }

    @DexIgnore
    public static final /* synthetic */ f97 b(short s) {
        return new f97(s);
    }

    @DexIgnore
    public static short c(short s) {
        return s;
    }

    @DexIgnore
    public static int d(short s) {
        return s;
    }

    @DexIgnore
    public static String e(short s) {
        return String.valueOf((int) (s & 65535));
    }

    @DexIgnore
    public final int a(short s) {
        throw null;
    }

    @DexIgnore
    public final /* synthetic */ short a() {
        return this.a;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(f97 f97) {
        a(f97.a());
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        short s = this.a;
        d(s);
        return s;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }
}
