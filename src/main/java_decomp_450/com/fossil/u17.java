package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u17 extends v17 {
    @DexIgnore
    @Override // com.fossil.a27
    public void error(String str, String str2, Object obj) {
        g().error(str, str2, obj);
    }

    @DexIgnore
    public abstract a27 g();

    @DexIgnore
    @Override // com.fossil.a27
    public void success(Object obj) {
        g().success(obj);
    }
}
