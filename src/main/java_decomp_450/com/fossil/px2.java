package com.fossil;

import com.fossil.bw2;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px2<T> implements cy2<T> {
    @DexIgnore
    public /* final */ jx2 a;
    @DexIgnore
    public /* final */ uy2<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ pv2<?> d;

    @DexIgnore
    public px2(uy2<?, ?> uy2, pv2<?> pv2, jx2 jx2) {
        this.b = uy2;
        this.c = pv2.a(jx2);
        this.d = pv2;
        this.a = jx2;
    }

    @DexIgnore
    public static <T> px2<T> a(uy2<?, ?> uy2, pv2<?> pv2, jx2 jx2) {
        return new px2<>(uy2, pv2, jx2);
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final T zza() {
        return (T) this.a.i().b();
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void zzb(T t, T t2) {
        ey2.a(this.b, t, t2);
        if (this.c) {
            ey2.a(this.d, t, t2);
        }
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void zzc(T t) {
        this.b.b(t);
        this.d.c(t);
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final boolean zzd(T t) {
        return this.d.a((Object) t).e();
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void a(T t, oz2 oz2) throws IOException {
        Iterator<Map.Entry<?, Object>> c2 = this.d.a((Object) t).c();
        while (c2.hasNext()) {
            Map.Entry<?, Object> next = c2.next();
            sv2 sv2 = (sv2) next.getKey();
            if (sv2.zzc() != pz2.MESSAGE || sv2.zzd() || sv2.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof pw2) {
                oz2.zza(sv2.zza(), ((pw2) next).a().b());
            } else {
                oz2.zza(sv2.zza(), next.getValue());
            }
        }
        uy2<?, ?> uy2 = this.b;
        uy2.b(uy2.a(t), oz2);
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final boolean zza(T t, T t2) {
        if (!this.b.a(t).equals(this.b.a(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final int zzb(T t) {
        uy2<?, ?> uy2 = this.b;
        int c2 = uy2.c(uy2.a(t)) + 0;
        return this.c ? c2 + this.d.a((Object) t).f() : c2;
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final int zza(T t) {
        int hashCode = this.b.a(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void a(T t, byte[] bArr, int i, int i2, ou2 ou2) throws IOException {
        T t2 = t;
        ty2 ty2 = ((bw2) t2).zzb;
        if (ty2 == ty2.d()) {
            ty2 = ty2.e();
            ((bw2) t2).zzb = ty2;
        }
        t.zza();
        bw2.d dVar = null;
        while (i < i2) {
            int a2 = pu2.a(bArr, i, ou2);
            int i3 = ou2.a;
            if (i3 == 11) {
                int i4 = 0;
                tu2 tu2 = null;
                while (a2 < i2) {
                    a2 = pu2.a(bArr, a2, ou2);
                    int i5 = ou2.a;
                    int i6 = i5 >>> 3;
                    int i7 = i5 & 7;
                    if (i6 != 2) {
                        if (i6 == 3) {
                            if (dVar != null) {
                                yx2.a();
                                throw new NoSuchMethodError();
                            } else if (i7 == 2) {
                                a2 = pu2.e(bArr, a2, ou2);
                                tu2 = (tu2) ou2.c;
                            }
                        }
                    } else if (i7 == 0) {
                        a2 = pu2.a(bArr, a2, ou2);
                        i4 = ou2.a;
                        dVar = (bw2.d) this.d.a(ou2.d, this.a, i4);
                    }
                    if (i5 == 12) {
                        break;
                    }
                    a2 = pu2.a(i5, bArr, a2, i2, ou2);
                }
                if (tu2 != null) {
                    ty2.a((i4 << 3) | 2, tu2);
                }
                i = a2;
            } else if ((i3 & 7) == 2) {
                dVar = (bw2.d) this.d.a(ou2.d, this.a, i3 >>> 3);
                if (dVar == null) {
                    i = pu2.a(i3, bArr, a2, i2, ty2, ou2);
                } else {
                    yx2.a();
                    throw new NoSuchMethodError();
                }
            } else {
                i = pu2.a(i3, bArr, a2, i2, ou2);
            }
        }
        if (i != i2) {
            throw iw2.zzg();
        }
    }
}
