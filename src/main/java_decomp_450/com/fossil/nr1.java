package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr1 extends v81 {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C; // = 15000;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ vc7<i97> E;
    @DexIgnore
    public /* final */ short F;
    @DexIgnore
    public /* final */ yx0 G;

    @DexIgnore
    public nr1(short s, yx0 yx0, ri1 ri1) {
        super(qa1.q, ri1, 0, 4);
        this.F = s;
        this.G = yx0;
        this.E = new op1(this, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void b(rk1 rk1) {
        if (rk1.a == qk1.FTC) {
            byte[] bArr = rk1.b;
            if (bArr.length >= 12) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.F != order.getShort(1)) {
                    return;
                }
                if (ln0.h.a() == b) {
                    i();
                    sz0 a = sz0.f.a(dr0.h.a(order.get(3)));
                    ((v81) this).v = sz0.a(((v81) this).v, null, null, a.c, null, a.e, 11);
                    this.A = yz0.b(order.getInt(4));
                    this.B = yz0.b(order.getInt(8));
                    ((v81) this).g.add(new zq0(0, rk1.a, rk1.b, yz0.a(yz0.a(new JSONObject(), r51.G0, Long.valueOf(this.A)), r51.H0, Long.valueOf(this.B)), 1));
                    a(((v81) this).v);
                    return;
                }
                ((v81) this).g.add(new zq0(0, rk1.a, rk1.b, null, 9));
                a(sz0.a(((v81) this).v, null, null, ay0.d, null, null, 27));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void c(eo0 eo0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        ((v81) this).v = sz0.a(((v81) this).v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        wr1 wr1 = ((v81) this).f;
        if (wr1 != null) {
            wr1.i = true;
        }
        wr1 wr12 = ((v81) this).f;
        if (!(wr12 == null || (jSONObject2 = wr12.m) == null)) {
            yz0.a(jSONObject2, r51.j, yz0.a(ay0.a));
        }
        ay0 ay0 = ((v81) this).v.c;
        ay0 ay02 = ay0.a;
        a(((v81) this).p);
        wr1 wr13 = ((v81) this).f;
        if (!(wr13 == null || (jSONObject = wr13.m) == null)) {
            yz0.a(jSONObject, r51.o3, Integer.valueOf(this.G.c()));
        }
        float min = Math.min((((float) this.G.c()) * 1.0f) / ((float) this.G.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.G.c() >= this.G.c) {
            this.D = min;
            a(min);
        }
        b();
    }

    @DexIgnore
    @Override // com.fossil.v81
    public eo0 d() {
        if (!(this.G.b.remaining() > 0)) {
            return null;
        }
        byte[] a = this.G.a();
        if (((v81) this).s) {
            a = ir0.b.b(((v81) this).y.u, this.G.f, a);
        }
        return new ze1(this.G.f, a, ((v81) this).y.w);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void f() {
        this.C = 30000;
        a(this.E);
        j();
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y0, yz0.a(this.F));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(super.h(), r51.G0, Long.valueOf(this.A)), r51.H0, Long.valueOf(this.B));
    }
}
