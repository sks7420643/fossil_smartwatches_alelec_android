package com.fossil;

import com.facebook.internal.ServerProtocol;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ks7 implements zr7 {
    @DexIgnore
    public yr7 a;
    @DexIgnore
    public is7 b;
    @DexIgnore
    public List c;

    @DexIgnore
    public void a(is7 is7) {
        this.b = is7;
        this.c = new ArrayList(is7.getRequiredOptions());
    }

    @DexIgnore
    public is7 b() {
        return this.b;
    }

    @DexIgnore
    public abstract String[] b(is7 is7, String[] strArr, boolean z);

    @DexIgnore
    public List c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.zr7
    public yr7 a(is7 is7, String[] strArr, boolean z) throws js7 {
        return a(is7, strArr, null, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
        if (r9 != false) goto L_0x004c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[LOOP:2: B:27:0x0085->B:39:0x0085, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0037 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.yr7 a(com.fossil.is7 r6, java.lang.String[] r7, java.util.Properties r8, boolean r9) throws com.fossil.js7 {
        /*
            r5 = this;
            java.util.List r0 = r6.helpOptions()
            java.util.Iterator r0 = r0.iterator()
        L_0x0008:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0018
            java.lang.Object r1 = r0.next()
            com.fossil.fs7 r1 = (com.fossil.fs7) r1
            r1.clearValues()
            goto L_0x0008
        L_0x0018:
            r5.a(r6)
            com.fossil.yr7 r6 = new com.fossil.yr7
            r6.<init>()
            r5.a = r6
            r6 = 0
            if (r7 != 0) goto L_0x0027
            java.lang.String[] r7 = new java.lang.String[r6]
        L_0x0027:
            com.fossil.is7 r0 = r5.b()
            java.lang.String[] r7 = r5.b(r0, r7, r9)
            java.util.List r7 = java.util.Arrays.asList(r7)
            java.util.ListIterator r7 = r7.listIterator()
        L_0x0037:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x009d
            java.lang.Object r0 = r7.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "--"
            boolean r2 = r1.equals(r0)
            r3 = 1
            if (r2 == 0) goto L_0x004e
        L_0x004c:
            r6 = 1
            goto L_0x0083
        L_0x004e:
            java.lang.String r2 = "-"
            boolean r4 = r2.equals(r0)
            if (r4 == 0) goto L_0x005f
            if (r9 == 0) goto L_0x0059
            goto L_0x004c
        L_0x0059:
            com.fossil.yr7 r2 = r5.a
            r2.addArg(r0)
            goto L_0x0083
        L_0x005f:
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x007b
            if (r9 == 0) goto L_0x0077
            com.fossil.is7 r2 = r5.b()
            boolean r2 = r2.hasOption(r0)
            if (r2 != 0) goto L_0x0077
            com.fossil.yr7 r6 = r5.a
            r6.addArg(r0)
            goto L_0x004c
        L_0x0077:
            r5.a(r0, r7)
            goto L_0x0083
        L_0x007b:
            com.fossil.yr7 r2 = r5.a
            r2.addArg(r0)
            if (r9 == 0) goto L_0x0083
            goto L_0x004c
        L_0x0083:
            if (r6 == 0) goto L_0x0037
        L_0x0085:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0037
            java.lang.Object r0 = r7.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = r1.equals(r0)
            if (r2 != 0) goto L_0x0085
            com.fossil.yr7 r2 = r5.a
            r2.addArg(r0)
            goto L_0x0085
        L_0x009d:
            r5.a(r8)
            r5.a()
            com.fossil.yr7 r6 = r5.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ks7.a(com.fossil.is7, java.lang.String[], java.util.Properties, boolean):com.fossil.yr7");
    }

    @DexIgnore
    public void a(Properties properties) {
        if (properties != null) {
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String obj = propertyNames.nextElement().toString();
                if (!this.a.hasOption(obj)) {
                    fs7 option = b().getOption(obj);
                    String property = properties.getProperty(obj);
                    if (option.hasArg()) {
                        if (option.getValues() == null || option.getValues().length == 0) {
                            try {
                                option.addValueForProcessing(property);
                            } catch (RuntimeException unused) {
                            }
                        }
                    } else if (!"yes".equalsIgnoreCase(property) && !ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equalsIgnoreCase(property) && !"1".equalsIgnoreCase(property)) {
                        return;
                    }
                    this.a.addOption(option);
                }
            }
        }
    }

    @DexIgnore
    public void a() throws es7 {
        if (!c().isEmpty()) {
            throw new es7(c());
        }
    }

    @DexIgnore
    public void a(fs7 fs7, ListIterator listIterator) throws js7 {
        while (true) {
            if (!listIterator.hasNext()) {
                break;
            }
            String str = (String) listIterator.next();
            if (b().hasOption(str) && str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
                listIterator.previous();
                break;
            } else {
                try {
                    fs7.addValueForProcessing(os7.a(str));
                } catch (RuntimeException unused) {
                    listIterator.previous();
                }
            }
        }
        if (fs7.getValues() == null && !fs7.hasOptionalArg()) {
            throw new ds7(fs7);
        }
    }

    @DexIgnore
    public void a(String str, ListIterator listIterator) throws js7 {
        if (b().hasOption(str)) {
            fs7 fs7 = (fs7) b().getOption(str).clone();
            if (fs7.isRequired()) {
                c().remove(fs7.getKey());
            }
            if (b().getOptionGroup(fs7) != null) {
                gs7 optionGroup = b().getOptionGroup(fs7);
                if (optionGroup.isRequired()) {
                    c().remove(optionGroup);
                }
                optionGroup.setSelected(fs7);
            }
            if (fs7.hasArg()) {
                a(fs7, listIterator);
            }
            this.a.addOption(fs7);
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Unrecognized option: ");
        stringBuffer.append(str);
        throw new ns7(stringBuffer.toString(), str);
    }
}
