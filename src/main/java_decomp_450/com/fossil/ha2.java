package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ha2 {
    @DexIgnore
    public static Context a;
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static synchronized boolean a(Context context) {
        synchronized (ha2.class) {
            Context applicationContext = context.getApplicationContext();
            if (a == null || b == null || a != applicationContext) {
                b = null;
                if (v92.j()) {
                    b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        b = true;
                    } catch (ClassNotFoundException unused) {
                        b = false;
                    }
                }
                a = applicationContext;
                return b.booleanValue();
            }
            return b.booleanValue();
        }
    }
}
