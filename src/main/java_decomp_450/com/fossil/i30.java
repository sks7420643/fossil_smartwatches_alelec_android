package com.fossil;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i30 implements p30 {
    @DexIgnore
    public /* final */ Set<q30> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    @Override // com.fossil.p30
    public void a(q30 q30) {
        this.a.add(q30);
        if (this.c) {
            q30.onDestroy();
        } else if (this.b) {
            q30.onStart();
        } else {
            q30.onStop();
        }
    }

    @DexIgnore
    @Override // com.fossil.p30
    public void b(q30 q30) {
        this.a.remove(q30);
    }

    @DexIgnore
    public void c() {
        this.b = false;
        for (q30 q30 : v50.a(this.a)) {
            q30.onStop();
        }
    }

    @DexIgnore
    public void b() {
        this.b = true;
        for (q30 q30 : v50.a(this.a)) {
            q30.onStart();
        }
    }

    @DexIgnore
    public void a() {
        this.c = true;
        for (q30 q30 : v50.a(this.a)) {
            q30.onDestroy();
        }
    }
}
