package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nd4 implements Runnable {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    public static Boolean g;
    @DexIgnore
    public static Boolean h;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ta4 b;
    @DexIgnore
    public /* final */ PowerManager.WakeLock c;
    @DexIgnore
    public /* final */ md4 d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public nd4 a;

        @DexIgnore
        public a(nd4 nd4) {
            this.a = nd4;
        }

        @DexIgnore
        public void a() {
            if (nd4.c()) {
                Log.d("FirebaseMessaging", "Connectivity change received registered");
            }
            nd4.this.a.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }

        @DexIgnore
        public synchronized void onReceive(Context context, Intent intent) {
            if (this.a != null) {
                if (this.a.a()) {
                    if (nd4.c()) {
                        Log.d("FirebaseMessaging", "Connectivity changed. Starting background sync.");
                    }
                    this.a.d.a(this.a, 0);
                    context.unregisterReceiver(this);
                    this.a = null;
                }
            }
        }
    }

    @DexIgnore
    public nd4(md4 md4, Context context, ta4 ta4, long j) {
        this.d = md4;
        this.a = context;
        this.e = j;
        this.b = ta4;
        this.c = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "wake:com.google.firebase.messaging");
    }

    @DexIgnore
    public static boolean b(Context context) {
        boolean z;
        boolean booleanValue;
        synchronized (f) {
            if (g == null) {
                z = a(context, "android.permission.WAKE_LOCK", g);
            } else {
                z = g.booleanValue();
            }
            Boolean valueOf = Boolean.valueOf(z);
            g = valueOf;
            booleanValue = valueOf.booleanValue();
        }
        return booleanValue;
    }

    @DexIgnore
    public static boolean c() {
        if (!Log.isLoggable("FirebaseMessaging", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3);
        }
        return true;
    }

    @DexIgnore
    public final synchronized boolean a() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.a.getSystemService("connectivity");
        activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public void run() {
        if (b(this.a)) {
            this.c.acquire(pc4.a);
        }
        try {
            this.d.a(true);
            if (!this.b.e()) {
                this.d.a(false);
                if (b(this.a)) {
                    try {
                        this.c.release();
                    } catch (RuntimeException unused) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            } else if (!a(this.a) || a()) {
                if (this.d.e()) {
                    this.d.a(false);
                } else {
                    this.d.a(this.e);
                }
                if (b(this.a)) {
                    try {
                        this.c.release();
                    } catch (RuntimeException unused2) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            } else {
                new a(this).a();
                if (b(this.a)) {
                    try {
                        this.c.release();
                    } catch (RuntimeException unused3) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            }
        } catch (IOException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            Log.e("FirebaseMessaging", valueOf.length() != 0 ? "Failed to sync topics. Won't retry sync. ".concat(valueOf) : new String("Failed to sync topics. Won't retry sync. "));
            this.d.a(false);
            if (b(this.a)) {
                try {
                    this.c.release();
                } catch (RuntimeException unused4) {
                    Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                }
            }
        } catch (Throwable th) {
            if (b(this.a)) {
                try {
                    this.c.release();
                } catch (RuntimeException unused5) {
                    Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public static boolean a(Context context) {
        boolean z;
        boolean booleanValue;
        synchronized (f) {
            if (h == null) {
                z = a(context, "android.permission.ACCESS_NETWORK_STATE", h);
            } else {
                z = h.booleanValue();
            }
            Boolean valueOf = Boolean.valueOf(z);
            h = valueOf;
            booleanValue = valueOf.booleanValue();
        }
        return booleanValue;
    }

    @DexIgnore
    public static boolean a(Context context, String str, Boolean bool) {
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z = context.checkCallingOrSelfPermission(str) == 0;
        if (!z && Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", a(str));
        }
        return z;
    }

    @DexIgnore
    public static String a(String str) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 142);
        sb.append("Missing Permission: ");
        sb.append(str);
        sb.append(". This permission should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        return sb.toString();
    }
}
