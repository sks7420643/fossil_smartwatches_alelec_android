package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eb3 {
    @DexIgnore
    public /* final */ sn2 a;

    @DexIgnore
    public interface a extends ri3 {
    }

    @DexIgnore
    public eb3(sn2 sn2) {
        this.a = sn2;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        this.a.a(str, str2, bundle);
    }

    @DexIgnore
    public void a(String str, String str2, Object obj) {
        this.a.a(str, str2, obj);
    }

    @DexIgnore
    public void a(a aVar) {
        this.a.a(aVar);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a.b(z);
    }
}
