package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ar7 extends sr7, ReadableByteChannel {
    @DexIgnore
    long a(qr7 qr7) throws IOException;

    @DexIgnore
    br7 a(long j) throws IOException;

    @DexIgnore
    String a(Charset charset) throws IOException;

    @DexIgnore
    boolean a(long j, br7 br7) throws IOException;

    @DexIgnore
    String b(long j) throws IOException;

    @DexIgnore
    yq7 buffer();

    @DexIgnore
    boolean d(long j) throws IOException;

    @DexIgnore
    byte[] f() throws IOException;

    @DexIgnore
    byte[] f(long j) throws IOException;

    @DexIgnore
    yq7 getBuffer();

    @DexIgnore
    void h(long j) throws IOException;

    @DexIgnore
    boolean h() throws IOException;

    @DexIgnore
    long m() throws IOException;

    @DexIgnore
    String p() throws IOException;

    @DexIgnore
    ar7 peek();

    @DexIgnore
    byte readByte() throws IOException;

    @DexIgnore
    void readFully(byte[] bArr) throws IOException;

    @DexIgnore
    int readInt() throws IOException;

    @DexIgnore
    short readShort() throws IOException;

    @DexIgnore
    void skip(long j) throws IOException;

    @DexIgnore
    long t() throws IOException;

    @DexIgnore
    InputStream u();
}
