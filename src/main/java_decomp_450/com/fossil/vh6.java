package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.cy6;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vh6 extends go5 implements uh6, cy6.g {
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public qw6<kw4> f;
    @DexIgnore
    public th6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final vh6 a() {
            return new vh6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vh6 a;

        @DexIgnore
        public b(vh6 vh6) {
            this.a = vh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vh6 a;

        @DexIgnore
        public c(vh6 vh6) {
            this.a = vh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = vx6.a(vx6.c.PRIVACY, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + a2);
            vh6 vh6 = this.a;
            ee7.a((Object) a2, "url");
            vh6.Y(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vh6 a;

        @DexIgnore
        public d(vh6 vh6) {
            this.a = vh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = vx6.a(vx6.c.TERMS, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + a2);
            vh6 vh6 = this.a;
            ee7.a((Object) a2, "url");
            vh6.Y(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vh6 a;

        @DexIgnore
        public e(vh6 vh6) {
            this.a = vh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = vx6.a(vx6.c.SOURCE_LICENSES, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + a2);
            vh6 vh6 = this.a;
            ee7.a((Object) a2, "url");
            vh6.Y(a2);
        }
    }

    @DexIgnore
    public final void Y(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), cj6.j.a());
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        kw4 kw4 = (kw4) qb.a(LayoutInflater.from(getContext()), 2131558490, null, false, a1());
        this.f = new qw6<>(this, kw4);
        ee7.a((Object) kw4, "binding");
        return kw4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        th6 th6 = this.g;
        if (th6 != null) {
            th6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        th6 th6 = this.g;
        if (th6 != null) {
            th6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<kw4> qw6 = this.f;
        if (qw6 != null) {
            kw4 a2 = qw6.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                if (!tm4.a.a().j()) {
                    RelativeLayout relativeLayout = a2.r;
                    ee7.a((Object) relativeLayout, "binding.btOpenSourceLicense");
                    relativeLayout.setVisibility(4);
                    View view2 = a2.C;
                    ee7.a((Object) view2, "binding.vLine2");
                    view2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void a(th6 th6) {
        ee7.b(th6, "presenter");
        jw3.a(th6);
        ee7.a((Object) th6, "Preconditions.checkNotNull(presenter)");
        this.g = th6;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof cl5)) {
            activity = null;
        }
        cl5 cl5 = (cl5) activity;
        if (cl5 != null) {
            cl5.a(str, i2, intent);
        }
    }
}
