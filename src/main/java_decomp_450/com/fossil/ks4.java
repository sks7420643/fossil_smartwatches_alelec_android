package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks4 implements Factory<js4> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;
    @DexIgnore
    public /* final */ Provider<xo4> c;

    @DexIgnore
    public ks4(Provider<ch5> provider, Provider<ro4> provider2, Provider<xo4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static ks4 a(Provider<ch5> provider, Provider<ro4> provider2, Provider<xo4> provider3) {
        return new ks4(provider, provider2, provider3);
    }

    @DexIgnore
    public static js4 a(ch5 ch5, ro4 ro4, xo4 xo4) {
        return new js4(ch5, ro4, xo4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public js4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
