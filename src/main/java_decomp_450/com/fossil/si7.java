package com.fossil;

public final class si7 {
    public static final boolean a;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r0.equals("on") != false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r0.equals("") != false) goto L_0x0033;
     */
    /*
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.scheduler"
            java.lang.String r0 = com.fossil.mm7.a(r0)
            if (r0 != 0) goto L_0x0009
            goto L_0x0033
        L_0x0009:
            int r1 = r0.hashCode()
            if (r1 == 0) goto L_0x002b
            r2 = 3551(0xddf, float:4.976E-42)
            if (r1 == r2) goto L_0x0022
            r2 = 109935(0x1ad6f, float:1.54052E-40)
            if (r1 != r2) goto L_0x0037
            java.lang.String r1 = "off"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0037
            r0 = 0
            goto L_0x0034
        L_0x0022:
            java.lang.String r1 = "on"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0037
            goto L_0x0033
        L_0x002b:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0037
        L_0x0033:
            r0 = 1
        L_0x0034:
            com.fossil.si7.a = r0
            return
        L_0x0037:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "System property 'kotlinx.coroutines.scheduler' has unrecognized value '"
            r1.append(r2)
            r1.append(r0)
            r0 = 39
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.si7.<clinit>():void");
    }
    */

    public static final ti7 a() {
        return a ? xm7.h : ji7.d;
    }

    public static final ib7 a(yi7 yi7, ib7 ib7) {
        ib7 plus = yi7.a().plus(ib7);
        ib7 plus2 = dj7.c() ? plus.plus(new wi7(dj7.b().incrementAndGet())) : plus;
        return (plus == qj7.a() || plus.get(gb7.m) != null) ? plus2 : plus2.plus(qj7.a());
    }

    public static final String a(ib7 ib7) {
        wi7 wi7;
        String str;
        if (!dj7.c() || (wi7 = (wi7) ib7.get(wi7.b)) == null) {
            return null;
        }
        xi7 xi7 = (xi7) ib7.get(xi7.b);
        if (xi7 == null || (str = xi7.g()) == null) {
            str = "coroutine";
        }
        return str + '#' + wi7.g();
    }
}
