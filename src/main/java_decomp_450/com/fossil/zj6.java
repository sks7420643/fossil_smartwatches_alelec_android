package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj6 implements Factory<yj6> {
    @DexIgnore
    public static yj6 a(uj6 uj6, on5 on5, UserRepository userRepository) {
        return new yj6(uj6, on5, userRepository);
    }
}
