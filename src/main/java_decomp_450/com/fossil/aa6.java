package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa6 implements Factory<z96> {
    @DexIgnore
    public static z96 a(x96 x96, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        return new z96(x96, summariesRepository, activitiesRepository, workoutSessionRepository, portfolioApp);
    }
}
