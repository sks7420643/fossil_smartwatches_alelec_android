package com.fossil;

import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt4 implements MembersInjector<BCNotificationActionReceiver> {
    @DexIgnore
    public static void a(BCNotificationActionReceiver bCNotificationActionReceiver, xo4 xo4) {
        bCNotificationActionReceiver.a = xo4;
    }

    @DexIgnore
    public static void a(BCNotificationActionReceiver bCNotificationActionReceiver, ro4 ro4) {
        bCNotificationActionReceiver.b = ro4;
    }

    @DexIgnore
    public static void a(BCNotificationActionReceiver bCNotificationActionReceiver, ch5 ch5) {
        bCNotificationActionReceiver.c = ch5;
    }
}
