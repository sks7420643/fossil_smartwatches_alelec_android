package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dq2 {
    @DexIgnore
    public static UserManager a;
    @DexIgnore
    public static volatile boolean b; // = (!a());

    @DexIgnore
    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean b(Context context) {
        boolean z;
        boolean z2 = true;
        int i = 1;
        while (true) {
            z = false;
            if (i > 2) {
                break;
            }
            if (a == null) {
                a = (UserManager) context.getSystemService(UserManager.class);
            }
            UserManager userManager = a;
            if (userManager == null) {
                return true;
            }
            try {
                if (!userManager.isUserUnlocked() && userManager.isUserRunning(Process.myUserHandle())) {
                    z2 = false;
                }
                z = z2;
            } catch (NullPointerException e) {
                Log.w("DirectBootUtils", "Failed to check if user is unlocked.", e);
                a = null;
                i++;
            }
        }
        if (z) {
            a = null;
        }
        return z;
    }

    @DexIgnore
    @TargetApi(24)
    public static boolean c(Context context) {
        if (b) {
            return true;
        }
        synchronized (dq2.class) {
            if (b) {
                return true;
            }
            boolean b2 = b(context);
            if (b2) {
                b = b2;
            }
            return b2;
        }
    }

    @DexIgnore
    public static boolean a(Context context) {
        return !a() || c(context);
    }
}
