package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g5 {
    @DexIgnore
    public i5 a;
    @DexIgnore
    public i5 b;
    @DexIgnore
    public i5 c;
    @DexIgnore
    public i5 d;
    @DexIgnore
    public i5 e;
    @DexIgnore
    public i5 f;
    @DexIgnore
    public i5 g;
    @DexIgnore
    public ArrayList<i5> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;

    @DexIgnore
    public g5(i5 i5Var, int i2, boolean z) {
        this.a = i5Var;
        this.l = i2;
        this.m = z;
    }

    @DexIgnore
    public static boolean a(i5 i5Var, int i2) {
        if (i5Var.s() != 8 && i5Var.C[i2] == i5.b.MATCH_CONSTRAINT) {
            int[] iArr = i5Var.g;
            if (iArr[i2] == 0 || iArr[i2] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.l * 2;
        i5 i5Var = this.a;
        boolean z = false;
        i5 i5Var2 = i5Var;
        boolean z2 = false;
        while (!z2) {
            this.i++;
            i5[] i5VarArr = i5Var.i0;
            int i3 = this.l;
            i5 i5Var3 = null;
            i5VarArr[i3] = null;
            i5Var.h0[i3] = null;
            if (i5Var.s() != 8) {
                if (this.b == null) {
                    this.b = i5Var;
                }
                this.d = i5Var;
                i5.b[] bVarArr = i5Var.C;
                int i4 = this.l;
                if (bVarArr[i4] == i5.b.MATCH_CONSTRAINT) {
                    int[] iArr = i5Var.g;
                    if (iArr[i4] == 0 || iArr[i4] == 3 || iArr[i4] == 2) {
                        this.j++;
                        float[] fArr = i5Var.g0;
                        int i5 = this.l;
                        float f2 = fArr[i5];
                        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            this.k += fArr[i5];
                        }
                        if (a(i5Var, this.l)) {
                            if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                this.n = true;
                            } else {
                                this.o = true;
                            }
                            if (this.h == null) {
                                this.h = new ArrayList<>();
                            }
                            this.h.add(i5Var);
                        }
                        if (this.f == null) {
                            this.f = i5Var;
                        }
                        i5 i5Var4 = this.g;
                        if (i5Var4 != null) {
                            i5Var4.h0[this.l] = i5Var;
                        }
                        this.g = i5Var;
                    }
                }
            }
            if (i5Var2 != i5Var) {
                i5Var2.i0[this.l] = i5Var;
            }
            h5 h5Var = i5Var.A[i2 + 1].d;
            if (h5Var != null) {
                i5 i5Var5 = h5Var.b;
                h5[] h5VarArr = i5Var5.A;
                if (h5VarArr[i2].d != null && h5VarArr[i2].d.b == i5Var) {
                    i5Var3 = i5Var5;
                }
            }
            if (i5Var3 == null) {
                i5Var3 = i5Var;
                z2 = true;
            }
            i5Var2 = i5Var;
            i5Var = i5Var3;
        }
        this.c = i5Var;
        if (this.l != 0 || !this.m) {
            this.e = this.a;
        } else {
            this.e = i5Var;
        }
        if (this.o && this.n) {
            z = true;
        }
        this.p = z;
    }

    @DexIgnore
    public void a() {
        if (!this.q) {
            b();
        }
        this.q = true;
    }
}
