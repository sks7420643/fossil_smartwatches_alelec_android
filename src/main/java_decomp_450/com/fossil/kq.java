package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq {
    @DexIgnore
    public /* final */ ti7 a;
    @DexIgnore
    public /* final */ zt b;
    @DexIgnore
    public /* final */ pt c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Drawable f;
    @DexIgnore
    public /* final */ Drawable g;
    @DexIgnore
    public /* final */ Drawable h;

    @DexIgnore
    public kq() {
        this(null, null, null, false, false, null, null, null, 255, null);
    }

    @DexIgnore
    public kq(ti7 ti7, zt ztVar, pt ptVar, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        ee7.b(ti7, "dispatcher");
        ee7.b(ptVar, "precision");
        this.a = ti7;
        this.b = ztVar;
        this.c = ptVar;
        this.d = z;
        this.e = z2;
        this.f = drawable;
        this.g = drawable2;
        this.h = drawable3;
    }

    @DexIgnore
    public final boolean a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final ti7 c() {
        return this.a;
    }

    @DexIgnore
    public final Drawable d() {
        return this.g;
    }

    @DexIgnore
    public final Drawable e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kq)) {
            return false;
        }
        kq kqVar = (kq) obj;
        return ee7.a(this.a, kqVar.a) && ee7.a(this.b, kqVar.b) && ee7.a(this.c, kqVar.c) && this.d == kqVar.d && this.e == kqVar.e && ee7.a(this.f, kqVar.f) && ee7.a(this.g, kqVar.g) && ee7.a(this.h, kqVar.h);
    }

    @DexIgnore
    public final Drawable f() {
        return this.f;
    }

    @DexIgnore
    public final pt g() {
        return this.c;
    }

    @DexIgnore
    public final zt h() {
        return this.b;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r2v6, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        ti7 ti7 = this.a;
        int i = 0;
        int hashCode = (ti7 != null ? ti7.hashCode() : 0) * 31;
        zt ztVar = this.b;
        int hashCode2 = (hashCode + (ztVar != null ? ztVar.hashCode() : 0)) * 31;
        pt ptVar = this.c;
        int hashCode3 = (hashCode2 + (ptVar != null ? ptVar.hashCode() : 0)) * 31;
        boolean z = this.d;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode3 + i3) * 31;
        boolean z2 = this.e;
        if (z2 == 0) {
            i2 = z2;
        }
        int i6 = (i5 + i2) * 31;
        Drawable drawable = this.f;
        int hashCode4 = (i6 + (drawable != null ? drawable.hashCode() : 0)) * 31;
        Drawable drawable2 = this.g;
        int hashCode5 = (hashCode4 + (drawable2 != null ? drawable2.hashCode() : 0)) * 31;
        Drawable drawable3 = this.h;
        if (drawable3 != null) {
            i = drawable3.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public String toString() {
        return "DefaultRequestOptions(dispatcher=" + this.a + ", transition=" + this.b + ", precision=" + this.c + ", allowHardware=" + this.d + ", allowRgb565=" + this.e + ", placeholder=" + this.f + ", error=" + this.g + ", fallback=" + this.h + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ kq(ti7 ti7, zt ztVar, pt ptVar, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3, int i, zd7 zd7) {
        this((i & 1) != 0 ? qj7.b() : ti7, (i & 2) != 0 ? null : ztVar, (i & 4) != 0 ? pt.AUTOMATIC : ptVar, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? null : drawable, (i & 64) != 0 ? null : drawable2, (i & 128) == 0 ? drawable3 : null);
    }
}
