package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se0 extends re0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<se0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public se0 createFromParcel(Parcel parcel) {
            return new se0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public se0[] newArray(int i) {
            return new se0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public se0 m70createFromParcel(Parcel parcel) {
            return new se0(parcel, null);
        }
    }

    @DexIgnore
    public /* synthetic */ se0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }

    @DexIgnore
    /* renamed from: isPercentageCircleEnable */
    public boolean b() {
        return super.b();
    }

    @DexIgnore
    public se0(boolean z) {
        super(kr0.PERCENTAGE_CIRCLE, z);
    }
}
