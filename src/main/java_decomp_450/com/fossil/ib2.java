package com.fossil;

import android.os.Bundle;
import com.fossil.za2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib2 implements za2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ za2 b;

    @DexIgnore
    public ib2(za2 za2, Bundle bundle) {
        this.b = za2;
        this.a = bundle;
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final void a(bb2 bb2) {
        this.b.a.onCreate(this.a);
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final int getState() {
        return 1;
    }
}
