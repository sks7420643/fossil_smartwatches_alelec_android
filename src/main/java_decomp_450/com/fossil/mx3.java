package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mx3<E> extends px3 implements Collection<E> {
    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean add(E e) {
        return delegate().add(e);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        return delegate().addAll(collection);
    }

    @DexIgnore
    public void clear() {
        delegate().clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return delegate().contains(obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<?> collection) {
        return delegate().containsAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.px3
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.px3
    public abstract Collection<E> delegate();

    @DexIgnore
    public boolean isEmpty() {
        return delegate().isEmpty();
    }

    @DexIgnore
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return delegate().iterator();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        return delegate().remove(obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean removeAll(Collection<?> collection) {
        return delegate().removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean retainAll(Collection<?> collection) {
        return delegate().retainAll(collection);
    }

    @DexIgnore
    public int size() {
        return delegate().size();
    }

    @DexIgnore
    public boolean standardAddAll(Collection<? extends E> collection) {
        return qy3.a(this, collection.iterator());
    }

    @DexIgnore
    public void standardClear() {
        qy3.a((Iterator<?>) iterator());
    }

    @DexIgnore
    public boolean standardContains(Object obj) {
        return qy3.a((Iterator<?>) iterator(), obj);
    }

    @DexIgnore
    public boolean standardContainsAll(Collection<?> collection) {
        return cx3.a((Collection<?>) this, collection);
    }

    @DexIgnore
    public boolean standardIsEmpty() {
        return !iterator().hasNext();
    }

    @DexIgnore
    public boolean standardRemove(Object obj) {
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (gw3.a(it.next(), obj)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean standardRemoveAll(Collection<?> collection) {
        return qy3.a((Iterator<?>) iterator(), collection);
    }

    @DexIgnore
    public boolean standardRetainAll(Collection<?> collection) {
        return qy3.b((Iterator<?>) iterator(), collection);
    }

    @DexIgnore
    public Object[] standardToArray() {
        return toArray(new Object[size()]);
    }

    @DexIgnore
    public String standardToString() {
        return cx3.a((Collection<?>) this);
    }

    @DexIgnore
    public Object[] toArray() {
        return delegate().toArray();
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public <T> T[] toArray(T[] tArr) {
        return (T[]) delegate().toArray(tArr);
    }

    @DexIgnore
    public <T> T[] standardToArray(T[] tArr) {
        return (T[]) iz3.a((Collection<?>) this, (Object[]) tArr);
    }
}
