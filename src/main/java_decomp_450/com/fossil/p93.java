package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<p93> CREATOR; // = new fa3();
    @DexIgnore
    public /* final */ List<LatLng> a;
    @DexIgnore
    public float b;
    @DexIgnore
    public int c;
    @DexIgnore
    public float d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public b93 h;
    @DexIgnore
    public b93 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public List<l93> p;

    @DexIgnore
    public p93() {
        this.b = 10.0f;
        this.c = -16777216;
        this.d = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = true;
        this.f = false;
        this.g = false;
        this.h = new a93();
        this.i = new a93();
        this.j = 0;
        this.p = null;
        this.a = new ArrayList();
    }

    @DexIgnore
    public final float A() {
        return this.d;
    }

    @DexIgnore
    public final boolean B() {
        return this.g;
    }

    @DexIgnore
    public final boolean C() {
        return this.f;
    }

    @DexIgnore
    public final boolean D() {
        return this.e;
    }

    @DexIgnore
    public final p93 a(float f2) {
        this.b = f2;
        return this;
    }

    @DexIgnore
    public final p93 b(b93 b93) {
        a72.a(b93, "startCap must not be null");
        this.h = b93;
        return this;
    }

    @DexIgnore
    public final p93 c(Iterable<LatLng> iterable) {
        for (LatLng latLng : iterable) {
            this.a.add(latLng);
        }
        return this;
    }

    @DexIgnore
    public final int e() {
        return this.c;
    }

    @DexIgnore
    public final b93 g() {
        return this.i;
    }

    @DexIgnore
    public final int v() {
        return this.j;
    }

    @DexIgnore
    public final List<l93> w() {
        return this.p;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.c(parcel, 2, x(), false);
        k72.a(parcel, 3, z());
        k72.a(parcel, 4, e());
        k72.a(parcel, 5, A());
        k72.a(parcel, 6, D());
        k72.a(parcel, 7, C());
        k72.a(parcel, 8, B());
        k72.a(parcel, 9, (Parcelable) y(), i2, false);
        k72.a(parcel, 10, (Parcelable) g(), i2, false);
        k72.a(parcel, 11, v());
        k72.c(parcel, 12, w(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final List<LatLng> x() {
        return this.a;
    }

    @DexIgnore
    public final b93 y() {
        return this.h;
    }

    @DexIgnore
    public final float z() {
        return this.b;
    }

    @DexIgnore
    public final p93 a(int i2) {
        this.c = i2;
        return this;
    }

    @DexIgnore
    public final p93 b(int i2) {
        this.j = i2;
        return this;
    }

    @DexIgnore
    public final p93 a(b93 b93) {
        a72.a(b93, "endCap must not be null");
        this.i = b93;
        return this;
    }

    @DexIgnore
    public final p93 b(float f2) {
        this.d = f2;
        return this;
    }

    @DexIgnore
    public final p93 c(boolean z) {
        this.e = z;
        return this;
    }

    @DexIgnore
    public final p93 a(List<l93> list) {
        this.p = list;
        return this;
    }

    @DexIgnore
    public final p93 b(boolean z) {
        this.f = z;
        return this;
    }

    @DexIgnore
    public final p93 a(boolean z) {
        this.g = z;
        return this;
    }

    @DexIgnore
    public p93(List list, float f2, int i2, float f3, boolean z, boolean z2, boolean z3, b93 b93, b93 b932, int i3, List<l93> list2) {
        this.b = 10.0f;
        this.c = -16777216;
        this.d = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = true;
        this.f = false;
        this.g = false;
        this.h = new a93();
        this.i = new a93();
        this.j = 0;
        this.p = null;
        this.a = list;
        this.b = f2;
        this.c = i2;
        this.d = f3;
        this.e = z;
        this.f = z2;
        this.g = z3;
        if (b93 != null) {
            this.h = b93;
        }
        if (b932 != null) {
            this.i = b932;
        }
        this.j = i3;
        this.p = list2;
    }
}
