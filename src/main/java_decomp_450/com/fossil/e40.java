package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e40 {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ vw<T> b;

        @DexIgnore
        public a(Class<T> cls, vw<T> vwVar) {
            this.a = cls;
            this.b = vwVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <T> vw<T> a(Class<T> cls) {
        for (a<?> aVar : this.a) {
            if (aVar.a(cls)) {
                return aVar.b;
            }
        }
        return null;
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, vw<T> vwVar) {
        this.a.add(new a<>(cls, vwVar));
    }
}
