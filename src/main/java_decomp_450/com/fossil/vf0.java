package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vf0 {
    NOT_ENCRYPTED((byte) 0),
    AES_CTR((byte) 1),
    AES_CBC_PKCS5((byte) 2),
    XOR((byte) 3);
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final vf0 a(byte b) {
            vf0[] values = vf0.values();
            for (vf0 vf0 : values) {
                if (vf0.a() == b) {
                    return vf0;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public vf0(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
