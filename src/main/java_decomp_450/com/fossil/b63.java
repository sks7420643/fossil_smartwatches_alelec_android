package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b63 extends ll2 {
    @DexIgnore
    public /* final */ /* synthetic */ oo3 a;

    @DexIgnore
    public b63(b53 b53, oo3 oo3) {
        this.a = oo3;
    }

    @DexIgnore
    @Override // com.fossil.kl2
    public final void a(hl2 hl2) throws RemoteException {
        Status a2 = hl2.a();
        if (a2 == null) {
            this.a.b((Exception) new w02(new Status(8, "Got null status from location service")));
        } else if (a2.g() == 0) {
            this.a.a((Object) true);
        } else {
            this.a.b((Exception) g62.a(a2));
        }
    }
}
