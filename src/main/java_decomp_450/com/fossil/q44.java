package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class q44 implements fo3 {
    @DexIgnore
    public /* final */ s44 a;

    @DexIgnore
    public q44(s44 s44) {
        this.a = s44;
    }

    @DexIgnore
    public static fo3 a(s44 s44) {
        return new q44(s44);
    }

    @DexIgnore
    @Override // com.fossil.fo3
    public Object then(no3 no3) {
        return Boolean.valueOf(this.a.a(no3));
    }
}
