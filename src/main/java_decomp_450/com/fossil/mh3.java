package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh3<V> extends FutureTask<V> implements Comparable<mh3<V>> {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ hh3 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mh3(hh3 hh3, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.d = hh3;
        a72.a((Object) str);
        long andIncrement = hh3.l.getAndIncrement();
        this.a = andIncrement;
        this.c = str;
        this.b = z;
        if (andIncrement == Long.MAX_VALUE) {
            hh3.e().t().a("Tasks index overflow");
        }
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(Object obj) {
        mh3 mh3 = (mh3) obj;
        boolean z = this.b;
        if (z != mh3.b) {
            return z ? -1 : 1;
        }
        long j = this.a;
        long j2 = mh3.a;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.d.e().u().a("Two tasks share the same index. index", Long.valueOf(this.a));
        return 0;
    }

    @DexIgnore
    public final void setException(Throwable th) {
        this.d.e().t().a(this.c, th);
        super.setException(th);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mh3(hh3 hh3, Runnable runnable, boolean z, String str) {
        super(runnable, null);
        this.d = hh3;
        a72.a((Object) str);
        long andIncrement = hh3.l.getAndIncrement();
        this.a = andIncrement;
        this.c = str;
        this.b = false;
        if (andIncrement == Long.MAX_VALUE) {
            hh3.e().t().a("Tasks index overflow");
        }
    }
}
