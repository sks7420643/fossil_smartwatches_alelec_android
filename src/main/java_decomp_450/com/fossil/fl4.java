package com.fossil;

import com.fossil.fl4.a;
import com.fossil.fl4.b;
import com.fossil.fl4.d;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fl4<Q extends b, R extends d, E extends a> {
    @DexIgnore
    public e<? super R, ? super E> a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ yi7 c; // = zi7.a(qj7.a());

    @DexIgnore
    public interface a extends c {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d extends c {
    }

    @DexIgnore
    public interface e<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ e $callBack;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fl4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(fl4 fl4, e eVar, b bVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fl4;
            this.$callBack = eVar;
            this.$requestValues = bVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$callBack, this.$requestValues, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.fl4 */
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: com.fossil.fl4 */
        /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: com.fossil.fl4 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.a = this.$callBack;
                fl4 fl4 = this.this$0;
                fl4.b = fl4.c();
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Start UseCase");
                fl4 fl42 = this.this$0;
                b bVar = this.$requestValues;
                this.L$0 = yi7;
                this.label = 1;
                obj = fl42.a(bVar, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (obj instanceof d) {
                this.this$0.a((d) obj);
            } else if (obj instanceof a) {
                this.this$0.a((a) obj);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.CoroutineUseCase$onError$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $errorValue;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fl4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(fl4 fl4, a aVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fl4;
            this.$errorValue = aVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$errorValue, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback failed");
                e a = this.this$0.a;
                if (a != null) {
                    a.a(this.$errorValue);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d $response;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fl4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(fl4 fl4, d dVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = fl4;
            this.$response = dVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$response, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback success");
                e a = this.this$0.a;
                if (a != null) {
                    a.onSuccess(this.$response);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public abstract Object a(Q q, fb7<Object> fb7);

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final yi7 b() {
        return this.c;
    }

    @DexIgnore
    public final ik7 a(Q q, e<? super R, ? super E> eVar) {
        return xh7.b(this.c, null, null, new f(this, eVar, q, null), 3, null);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.fl4$e<? super R extends com.fossil.fl4$d, ? super E extends com.fossil.fl4$a>, com.fossil.fl4$e<R extends com.fossil.fl4$d, E extends com.fossil.fl4$a> */
    public final e<R, E> a() {
        return (e<? super R, ? super E>) this.a;
    }

    @DexIgnore
    public final void a(e<? super R, ? super E> eVar) {
        ee7.b(eVar, Constants.CALLBACK);
        this.a = eVar;
    }

    @DexIgnore
    public final ik7 a(R r) {
        ee7.b(r, "response");
        return xh7.b(this.c, qj7.c(), null, new h(this, r, null), 2, null);
    }

    @DexIgnore
    public final ik7 a(E e2) {
        ee7.b(e2, "errorValue");
        return xh7.b(this.c, qj7.c(), null, new g(this, e2, null), 2, null);
    }
}
