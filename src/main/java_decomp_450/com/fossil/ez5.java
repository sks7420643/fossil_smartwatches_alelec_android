package com.fossil;

import com.fossil.xg5;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<fz5> c;
    @DexIgnore
    public List<? extends xg5.a> d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public ez5(String str, String str2, ArrayList<fz5> arrayList, List<? extends xg5.a> list, boolean z) {
        ee7.b(str, "mPresetId");
        ee7.b(str2, "mPresetName");
        ee7.b(arrayList, "mMicroApps");
        ee7.b(list, "mPermissionFeatures");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = list;
        this.e = z;
    }

    @DexIgnore
    public final ArrayList<fz5> a() {
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.xg5$a>, java.util.List<com.fossil.xg5$a> */
    public final List<xg5.a> b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.e;
    }
}
