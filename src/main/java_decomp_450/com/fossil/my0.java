package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my0 extends pe1 {
    @DexIgnore
    public static /* final */ uw0 CREATOR; // = new uw0(null);
    @DexIgnore
    public /* final */ double b;

    @DexIgnore
    public my0(double d) {
        super(zp1.DELAY);
        this.b = d;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        return yz0.a(super.a(), r51.U3, Double.valueOf(this.b));
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort((short) ((int) ((this.b * ((double) 1000)) / 100.0d)));
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(my0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((my0) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.DelayInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return (int) this.b;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.b);
        }
    }

    @DexIgnore
    public /* synthetic */ my0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readDouble();
    }
}
