package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj7 {
    @DexIgnore
    public static /* final */ lm7 a; // = new lm7("REMOVED_TASK");
    @DexIgnore
    public static /* final */ lm7 b; // = new lm7("CLOSED_EMPTY");

    @DexIgnore
    public static final long a(long j) {
        if (j <= 0) {
            return 0;
        }
        if (j >= 9223372036854L) {
            return Long.MAX_VALUE;
        }
        return 1000000 * j;
    }
}
