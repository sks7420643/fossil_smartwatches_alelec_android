package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class os2<E> extends ps2<E> implements List<E>, RandomAccess {
    @DexIgnore
    public static /* final */ xt2<Object> b; // = new rs2(mt2.zza, 0);

    @DexIgnore
    public static <E> os2<E> zza() {
        return (os2<E>) mt2.zza;
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final void add(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public boolean contains(@NullableDecl Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        or2.a(this);
        if (obj == this) {
            return true;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            int size = size();
            if (size == list.size()) {
                if (list instanceof RandomAccess) {
                    int i = 0;
                    while (i < size) {
                        if (mr2.a(get(i), list.get(i))) {
                            i++;
                        }
                    }
                    return true;
                }
                int size2 = size();
                Iterator<E> it = list.iterator();
                int i2 = 0;
                while (true) {
                    if (i2 < size2) {
                        if (!it.hasNext()) {
                            break;
                        }
                        E e = get(i2);
                        i2++;
                        if (!mr2.a(e, it.next())) {
                            break;
                        }
                    } else if (!it.hasNext()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = ~(~((i * 31) + get(i2).hashCode()));
        }
        return i;
    }

    @DexIgnore
    public int indexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        int size = size();
        int i = 0;
        if (obj == null) {
            while (i < size) {
                if (get(i) == null) {
                    return i;
                }
                i++;
            }
        } else {
            while (i < size) {
                if (obj.equals(get(i))) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(@NullableDecl Object obj) {
        if (obj == null) {
            return -1;
        }
        if (obj == null) {
            for (int size = size() - 1; size >= 0; size--) {
                if (get(size) == null) {
                    return size;
                }
            }
        } else {
            for (int size2 = size() - 1; size2 >= 0; size2--) {
                if (obj.equals(get(size2))) {
                    return size2;
                }
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator(int i) {
        or2.b(i, size());
        if (isEmpty()) {
            return b;
        }
        return new rs2(this, i);
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E remove(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.List
    @Deprecated
    public final E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<E> iterator() {
        return (xt2) listIterator();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final os2<E> zzc() {
        return this;
    }

    @DexIgnore
    public os2<E> zzd() {
        return size() <= 1 ? this : new qs2(this);
    }

    @DexIgnore
    public static <E> os2<E> zza(E e) {
        Object[] objArr = {e};
        for (int i = 0; i < 1; i++) {
            kt2.a(objArr[i], i);
        }
        return zza(objArr, 1);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public int zzb(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    @DexIgnore
    public static <E> os2<E> zza(Object[] objArr) {
        return zza(objArr, objArr.length);
    }

    @DexIgnore
    public static <E> os2<E> zza(Object[] objArr, int i) {
        return i == 0 ? (os2<E>) mt2.zza : new mt2(objArr, i);
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ ListIterator listIterator() {
        return (xt2) listIterator(0);
    }

    @DexIgnore
    /* renamed from: zza */
    public os2<E> subList(int i, int i2) {
        or2.a(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        return i3 == 0 ? (os2<E>) mt2.zza : new ts2(this, i, i3);
    }
}
