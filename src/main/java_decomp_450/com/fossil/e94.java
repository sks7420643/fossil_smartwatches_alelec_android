package com.fossil;

import android.util.Base64;
import android.util.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e94 implements v84, x84 {
    @DexIgnore
    public e94 a; // = null;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public /* final */ JsonWriter c;
    @DexIgnore
    public /* final */ Map<Class<?>, u84<?>> d;
    @DexIgnore
    public /* final */ Map<Class<?>, w84<?>> e;
    @DexIgnore
    public /* final */ u84<Object> f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public e94(Writer writer, Map<Class<?>, u84<?>> map, Map<Class<?>, w84<?>> map2, u84<Object> u84, boolean z) {
        this.c = new JsonWriter(writer);
        this.d = map;
        this.e = map2;
        this.f = u84;
        this.g = z;
    }

    @DexIgnore
    public final void b() throws IOException {
        if (this.b) {
            e94 e94 = this.a;
            if (e94 != null) {
                e94.b();
                this.a.b = false;
                this.a = null;
                this.c.endObject();
                return;
            }
            return;
        }
        throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
    }

    @DexIgnore
    public final e94 c(String str, Object obj) throws IOException, t84 {
        if (obj == null) {
            return this;
        }
        b();
        this.c.name(str);
        return a(obj, false);
    }

    @DexIgnore
    @Override // com.fossil.v84
    public e94 a(String str, Object obj) throws IOException {
        if (this.g) {
            return c(str, obj);
        }
        return b(str, obj);
    }

    @DexIgnore
    public final e94 b(String str, Object obj) throws IOException, t84 {
        b();
        this.c.name(str);
        if (obj != null) {
            return a(obj, false);
        }
        this.c.nullValue();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.v84
    public e94 a(String str, int i) throws IOException {
        b();
        this.c.name(str);
        a(i);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.v84
    public e94 a(String str, long j) throws IOException {
        b();
        this.c.name(str);
        a(j);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.v84
    public e94 a(String str, boolean z) throws IOException {
        b();
        this.c.name(str);
        a(z);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.x84
    public e94 a(String str) throws IOException {
        b();
        this.c.value(str);
        return this;
    }

    @DexIgnore
    public e94 a(int i) throws IOException {
        b();
        this.c.value((long) i);
        return this;
    }

    @DexIgnore
    public e94 a(long j) throws IOException {
        b();
        this.c.value(j);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.x84
    public e94 a(boolean z) throws IOException {
        b();
        this.c.value(z);
        return this;
    }

    @DexIgnore
    public e94 a(byte[] bArr) throws IOException {
        b();
        if (bArr == null) {
            this.c.nullValue();
        } else {
            this.c.value(Base64.encodeToString(bArr, 2));
        }
        return this;
    }

    @DexIgnore
    public e94 a(Object obj, boolean z) throws IOException {
        Class<?> cls;
        int i = 0;
        if (z && a(obj)) {
            Object[] objArr = new Object[1];
            if (obj == null) {
                cls = null;
            } else {
                cls = obj.getClass();
            }
            objArr[0] = cls;
            throw new t84(String.format("%s cannot be encoded inline", objArr));
        } else if (obj == null) {
            this.c.nullValue();
            return this;
        } else if (obj instanceof Number) {
            this.c.value((Number) obj);
            return this;
        } else if (obj.getClass().isArray()) {
            if (obj instanceof byte[]) {
                a((byte[]) obj);
                return this;
            }
            this.c.beginArray();
            if (obj instanceof int[]) {
                int[] iArr = (int[]) obj;
                int length = iArr.length;
                while (i < length) {
                    this.c.value((long) iArr[i]);
                    i++;
                }
            } else if (obj instanceof long[]) {
                long[] jArr = (long[]) obj;
                int length2 = jArr.length;
                while (i < length2) {
                    a(jArr[i]);
                    i++;
                }
            } else if (obj instanceof double[]) {
                double[] dArr = (double[]) obj;
                int length3 = dArr.length;
                while (i < length3) {
                    this.c.value(dArr[i]);
                    i++;
                }
            } else if (obj instanceof boolean[]) {
                boolean[] zArr = (boolean[]) obj;
                int length4 = zArr.length;
                while (i < length4) {
                    this.c.value(zArr[i]);
                    i++;
                }
            } else if (obj instanceof Number[]) {
                for (Number number : (Number[]) obj) {
                    a((Object) number, false);
                }
            } else {
                for (Object obj2 : (Object[]) obj) {
                    a(obj2, false);
                }
            }
            this.c.endArray();
            return this;
        } else if (obj instanceof Collection) {
            this.c.beginArray();
            for (Object obj3 : (Collection) obj) {
                a(obj3, false);
            }
            this.c.endArray();
            return this;
        } else if (obj instanceof Map) {
            this.c.beginObject();
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                Object key = entry.getKey();
                try {
                    a((String) key, entry.getValue());
                } catch (ClassCastException e2) {
                    throw new t84(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), e2);
                }
            }
            this.c.endObject();
            return this;
        } else {
            u84<?> u84 = this.d.get(obj.getClass());
            if (u84 != null) {
                a(u84, obj, z);
                return this;
            }
            w84<?> w84 = this.e.get(obj.getClass());
            if (w84 != null) {
                w84.a(obj, this);
                return this;
            } else if (obj instanceof Enum) {
                a(((Enum) obj).name());
                return this;
            } else {
                a(this.f, obj, z);
                return this;
            }
        }
    }

    @DexIgnore
    public e94 a(u84<Object> u84, Object obj, boolean z) throws IOException {
        if (!z) {
            this.c.beginObject();
        }
        u84.a(obj, this);
        if (!z) {
            this.c.endObject();
        }
        return this;
    }

    @DexIgnore
    public final boolean a(Object obj) {
        return obj == null || obj.getClass().isArray() || (obj instanceof Collection) || (obj instanceof Date) || (obj instanceof Enum) || (obj instanceof Number);
    }

    @DexIgnore
    public void a() throws IOException {
        b();
        this.c.flush();
    }
}
