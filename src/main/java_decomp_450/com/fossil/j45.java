package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j45 extends i45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i Z; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray a0;
    @DexIgnore
    public long Y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        a0 = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        a0.put(2131362517, 2);
        a0.put(2131363099, 3);
        a0.put(2131362429, 4);
        a0.put(2131363105, 5);
        a0.put(2131362065, 6);
        a0.put(2131362432, 7);
        a0.put(2131362779, 8);
        a0.put(2131362433, 9);
        a0.put(2131363393, 10);
        a0.put(2131362430, 11);
        a0.put(2131362778, 12);
        a0.put(2131362431, 13);
        a0.put(2131363394, 14);
        a0.put(2131362482, 15);
        a0.put(2131362786, 16);
        a0.put(2131362483, 17);
        a0.put(2131363395, 18);
        a0.put(2131362480, 19);
        a0.put(2131362092, 20);
        a0.put(2131362438, 21);
        a0.put(2131363106, 22);
        a0.put(2131362068, 23);
        a0.put(2131362428, 24);
        a0.put(2131362504, 25);
        a0.put(2131363111, 26);
        a0.put(2131362103, 27);
        a0.put(2131362481, 28);
        a0.put(2131362347, 29);
        a0.put(2131363103, 30);
        a0.put(2131362035, 31);
        a0.put(2131362500, 32);
        a0.put(2131362263, 33);
    }
    */

    @DexIgnore
    public j45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 34, Z, a0));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.Y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.Y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.Y = 1;
        }
        g();
    }

    @DexIgnore
    public j45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[31], (ConstraintLayout) objArr[6], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[27], (FlexibleButton) objArr[33], (FlexibleTextView) objArr[29], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[32], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (LinearLayout) objArr[12], (LinearLayout) objArr[8], (LinearLayout) objArr[16], (ConstraintLayout) objArr[0], (ScrollView) objArr[3], (FlexibleSwitchCompat) objArr[30], (FlexibleSwitchCompat) objArr[5], (FlexibleSwitchCompat) objArr[22], (FlexibleSwitchCompat) objArr[26], (View) objArr[10], (View) objArr[14], (View) objArr[18]);
        this.Y = -1;
        ((i45) this).P.setTag(null);
        a(view);
        f();
    }
}
