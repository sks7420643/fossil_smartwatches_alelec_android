package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok1 extends BluetoothGattCallback {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public ok1(ri1 ri1) {
        this.a = ri1;
    }

    @DexIgnore
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        ri1 ri1 = this.a;
        om1 om1 = no1.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            ee7.a((Object) uuid, "characteristic!!.uuid");
            qk1 a2 = om1.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            ri1.a(a2, value);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        ri1 ri1 = this.a;
        x71 a2 = x71.c.a(i);
        om1 om1 = no1.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            ee7.a((Object) uuid, "characteristic!!.uuid");
            qk1 a3 = om1.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            ri1.a.post(new v71(ri1, a2, a3, value));
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        ri1 ri1 = this.a;
        x71 a2 = x71.c.a(i);
        om1 om1 = no1.b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        ee7.a((Object) uuid, "characteristic.uuid");
        qk1 a3 = om1.a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        ri1.a.post(new ye1(ri1, a2, a3, value));
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        LinkedHashSet<Long> linkedHashSet;
        LinkedHashSet<Long> linkedHashSet2;
        BluetoothDevice device;
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        BluetoothDevice device2 = bluetoothGatt.getDevice();
        if ((device2 != null ? device2.getAddress() : null) != null) {
            BluetoothDevice device3 = bluetoothGatt.getDevice();
            String address = device3 != null ? device3.getAddress() : null;
            BluetoothGatt bluetoothGatt2 = this.a.b;
            if (ee7.a((Object) address, (Object) ((bluetoothGatt2 == null || (device = bluetoothGatt2.getDevice()) == null) ? null : device.getAddress()))) {
                le0 le0 = le0.DEBUG;
                if (i == 0) {
                    ri1 ri1 = this.a;
                    z31 z31 = ri1.c;
                    if (z31 != null) {
                        ri1.a(z31);
                    }
                    ri1 ri12 = this.a;
                    ri12.c = null;
                    ri12.a(new z31(i, i2, sa7.a((Object[]) new Long[]{Long.valueOf(System.currentTimeMillis())})));
                } else {
                    z31 z312 = this.a.c;
                    if (z312 != null && z312.a == i && z312.b == i2) {
                        if (((z312 == null || (linkedHashSet2 = z312.c) == null) ? 0 : linkedHashSet2.size()) < 1000) {
                            z31 z313 = this.a.c;
                            if (!(z313 == null || (linkedHashSet = z313.c) == null)) {
                                linkedHashSet.add(Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    }
                    ri1 ri13 = this.a;
                    z31 z314 = ri13.c;
                    if (z314 != null) {
                        ri13.a(z314);
                    }
                    this.a.c = new z31(i, i2, sa7.a((Object[]) new Long[]{Long.valueOf(System.currentTimeMillis())}));
                }
                this.a.a(i2, i);
            }
        }
    }

    @DexIgnore
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        le0 le0 = le0.DEBUG;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        ee7.a((Object) characteristic, "descriptor.characteristic");
        characteristic.getUuid();
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        if (uuid == null || uuid2 == null) {
            le0 le02 = le0.DEBUG;
            return;
        }
        ri1 ri1 = this.a;
        ri1.a.post(new ug1(ri1, x71.c.a(i), no1.b.a(uuid), fo0.d.a(uuid2), value));
    }

    @DexIgnore
    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.a.j = i - 3;
        }
        ri1 ri1 = this.a;
        ri1.a.post(new ed1(ri1, x71.c.a(i2), i));
    }

    @DexIgnore
    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        ri1 ri1 = this.a;
        ri1.a.post(new q91(ri1, x71.c.a(i2), i));
    }

    @DexIgnore
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        super.onServicesDiscovered(bluetoothGatt, i);
        List<BluetoothGattService> services = bluetoothGatt.getServices();
        ri1 ri1 = this.a;
        ee7.a((Object) services, "services");
        ri1.a(services);
        le0 le0 = le0.DEBUG;
        ea7.a(services, "\n", null, null, 0, null, qi1.a, 30, null);
        ri1 ri12 = this.a;
        x71 a2 = x71.c.a(i);
        ArrayList arrayList = new ArrayList(x97.a(services, 10));
        for (T t : services) {
            ee7.a((Object) t, "it");
            arrayList.add(t.getUuid());
        }
        Object[] array = arrayList.toArray(new UUID[0]);
        if (array != null) {
            UUID[] uuidArr = (UUID[]) array;
            Set<qk1> keySet = this.a.i.keySet();
            ee7.a((Object) keySet, "gattCharacteristicMap.keys");
            Object[] array2 = keySet.toArray(new qk1[0]);
            if (array2 != null) {
                ri12.a.post(new g21(ri12, a2, uuidArr, (qk1[]) array2));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
