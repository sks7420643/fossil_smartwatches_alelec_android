package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.fossil.i0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ju7 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ju7> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public Object h;
    @DexIgnore
    public Context i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ju7> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ju7 createFromParcel(Parcel parcel) {
            return new ju7(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ju7[] newArray(int i) {
            return new ju7[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ju7(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public static ju7 a(Intent intent, Activity activity) {
        ju7 ju7 = (ju7) intent.getParcelableExtra("extra_app_settings");
        ju7.a(activity);
        return ju7;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
    }

    @DexIgnore
    public ju7(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
    }

    @DexIgnore
    public final void a(Object obj) {
        this.h = obj;
        if (obj instanceof Activity) {
            this.i = (Activity) obj;
        } else if (obj instanceof Fragment) {
            this.i = ((Fragment) obj).getContext();
        } else {
            throw new IllegalStateException("Unknown object: " + obj);
        }
    }

    @DexIgnore
    public i0 a(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        i0.a aVar;
        int i2 = this.a;
        if (i2 > 0) {
            aVar = new i0.a(this.i, i2);
        } else {
            aVar = new i0.a(this.i);
        }
        aVar.a(false);
        aVar.b(this.c);
        aVar.a(this.b);
        aVar.b(this.d, onClickListener);
        aVar.a(this.e, onClickListener2);
        return aVar.c();
    }

    @DexIgnore
    public int a() {
        return this.g;
    }
}
