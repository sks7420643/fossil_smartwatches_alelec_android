package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z13 implements tr2<y13> {
    @DexIgnore
    public static z13 b; // = new z13();
    @DexIgnore
    public /* final */ tr2<y13> a;

    @DexIgnore
    public z13(tr2<y13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((y13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((y13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ y13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public z13() {
        this(sr2.a(new b23()));
    }
}
