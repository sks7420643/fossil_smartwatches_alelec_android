package com.fossil;

import android.content.Context;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr2 {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public dr2(Uri uri) {
        this(null, uri, "", "", false, false, false, false, null);
    }

    @DexIgnore
    public final tq2<Long> a(String str, long j) {
        return tq2.b(this, str, j, true);
    }

    @DexIgnore
    public dr2(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, nr2<Context, Boolean> nr2) {
        this.a = uri;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final tq2<Boolean> a(String str, boolean z) {
        return tq2.b(this, str, z, true);
    }

    @DexIgnore
    public final tq2<Double> a(String str, double d) {
        return tq2.b(this, str, -3.0d, true);
    }

    @DexIgnore
    public final tq2<String> a(String str, String str2) {
        return tq2.b(this, str, str2, true);
    }
}
