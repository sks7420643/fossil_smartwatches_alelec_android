package com.fossil;

import android.os.Bundle;
import com.fossil.fl4;
import com.fossil.zn5;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp6 extends gp6 {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public /* final */ hp6 f;
    @DexIgnore
    public /* final */ zn5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<zn5.d, zn5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ kp6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(kp6 kp6) {
            this.a = kp6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(zn5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.h().f();
            this.a.h().s0();
        }

        @DexIgnore
        public void a(zn5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.h().f();
            int a2 = cVar.a();
            if (a2 == 400005) {
                hp6 h = this.a.h();
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886931);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                h.K(a3);
                this.a.h().M(false);
            } else if (a2 != 404001) {
                hp6 h2 = this.a.h();
                int a4 = cVar.a();
                String b = cVar.b();
                if (b == null) {
                    b = "";
                }
                h2.a(a4, b);
                this.a.h().M(false);
            } else {
                hp6 h3 = this.a.h();
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886926);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                h3.K(a5);
                this.a.h().M(true);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public kp6(hp6 hp6, zn5 zn5) {
        ee7.b(hp6, "mView");
        ee7.b(zn5, "mResetPasswordUseCase");
        this.f = hp6;
        this.g = zn5;
    }

    @DexIgnore
    @Override // com.fossil.gp6
    public void a(String str) {
        ee7.b(str, Constants.EMAIL);
        this.e = str;
        if (str.length() == 0) {
            this.f.f0();
        } else {
            this.f.I();
        }
    }

    @DexIgnore
    @Override // com.fossil.gp6
    public void b(String str) {
        ee7.b(str, Constants.EMAIL);
        if (i()) {
            this.f.g();
            this.g.a(new zn5.b(str), new b(this));
        }
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, Constants.EMAIL);
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.f.T(this.e);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    public final hp6 h() {
        return this.f;
    }

    @DexIgnore
    public final boolean i() {
        if (this.e.length() == 0) {
            this.f.K("");
        } else if (!kx6.a(this.e)) {
            hp6 hp6 = this.f;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886931);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            hp6.K(a2);
        } else {
            this.f.K("");
            return true;
        }
        return false;
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        ee7.b(str, "key");
        ee7.b(bundle, "outState");
        bundle.putString(str, this.e);
    }
}
