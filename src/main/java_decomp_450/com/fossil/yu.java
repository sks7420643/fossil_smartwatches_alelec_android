package com.fossil;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.av;
import com.fossil.gv;
import com.fossil.mu;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yu<T> implements Comparable<yu<T>> {
    @DexIgnore
    public static /* final */ String DEFAULT_PARAMS_ENCODING; // = "UTF-8";
    @DexIgnore
    public mu.a mCacheEntry;
    @DexIgnore
    public boolean mCanceled;
    @DexIgnore
    public /* final */ int mDefaultTrafficStatsTag;
    @DexIgnore
    public av.a mErrorListener;
    @DexIgnore
    public /* final */ gv.a mEventLog;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ int mMethod;
    @DexIgnore
    public b mRequestCompleteListener;
    @DexIgnore
    public zu mRequestQueue;
    @DexIgnore
    public boolean mResponseDelivered;
    @DexIgnore
    public cv mRetryPolicy;
    @DexIgnore
    public Integer mSequence;
    @DexIgnore
    public boolean mShouldCache;
    @DexIgnore
    public boolean mShouldRetryServerErrors;
    @DexIgnore
    public Object mTag;
    @DexIgnore
    public /* final */ String mUrl;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public a(String str, long j) {
            this.a = str;
            this.b = j;
        }

        @DexIgnore
        public void run() {
            yu.this.mEventLog.a(this.a, this.b);
            yu.this.mEventLog.a(yu.this.toString());
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(yu<?> yuVar);

        @DexIgnore
        void a(yu<?> yuVar, av<?> avVar);
    }

    @DexIgnore
    public enum c {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    @DexIgnore
    @Deprecated
    public yu(String str, av.a aVar) {
        this(-1, str, aVar);
    }

    @DexIgnore
    private byte[] encodeParameters(Map<String, String> map, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getKey() == null || entry.getValue() == null) {
                    throw new IllegalArgumentException(String.format("Request#getParams() or Request#getPostParams() returned a map containing a null key or value: (%s, %s). All keys and values must be non-null.", entry.getKey(), entry.getValue()));
                }
                sb.append(URLEncoder.encode(entry.getKey(), str));
                sb.append('=');
                sb.append(URLEncoder.encode(entry.getValue(), str));
                sb.append('&');
            }
            return sb.toString().getBytes(str);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not supported: " + str, e);
        }
    }

    @DexIgnore
    public static int findDefaultTrafficStatsTag(String str) {
        Uri parse;
        String host;
        if (TextUtils.isEmpty(str) || (parse = Uri.parse(str)) == null || (host = parse.getHost()) == null) {
            return 0;
        }
        return host.hashCode();
    }

    @DexIgnore
    public void addMarker(String str) {
        if (gv.a.c) {
            this.mEventLog.a(str, Thread.currentThread().getId());
        }
    }

    @DexIgnore
    public void cancel() {
        synchronized (this.mLock) {
            this.mCanceled = true;
            this.mErrorListener = null;
        }
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return compareTo((yu) ((yu) obj));
    }

    @DexIgnore
    public void deliverError(fv fvVar) {
        av.a aVar;
        synchronized (this.mLock) {
            aVar = this.mErrorListener;
        }
        if (aVar != null) {
            aVar.onErrorResponse(fvVar);
        }
    }

    @DexIgnore
    public abstract void deliverResponse(T t);

    @DexIgnore
    public void finish(String str) {
        zu zuVar = this.mRequestQueue;
        if (zuVar != null) {
            zuVar.b(this);
        }
        if (gv.a.c) {
            long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new a(str, id));
                return;
            }
            this.mEventLog.a(str, id);
            this.mEventLog.a(toString());
        }
    }

    @DexIgnore
    public byte[] getBody() throws lu {
        Map<String, String> params = getParams();
        if (params == null || params.size() <= 0) {
            return null;
        }
        return encodeParameters(params, getParamsEncoding());
    }

    @DexIgnore
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=" + getParamsEncoding();
    }

    @DexIgnore
    public mu.a getCacheEntry() {
        return this.mCacheEntry;
    }

    @DexIgnore
    public String getCacheKey() {
        String url = getUrl();
        int method = getMethod();
        if (method == 0 || method == -1) {
            return url;
        }
        return Integer.toString(method) + '-' + url;
    }

    @DexIgnore
    public av.a getErrorListener() {
        av.a aVar;
        synchronized (this.mLock) {
            aVar = this.mErrorListener;
        }
        return aVar;
    }

    @DexIgnore
    public Map<String, String> getHeaders() throws lu {
        return Collections.emptyMap();
    }

    @DexIgnore
    public int getMethod() {
        return this.mMethod;
    }

    @DexIgnore
    public Map<String, String> getParams() throws lu {
        return null;
    }

    @DexIgnore
    public String getParamsEncoding() {
        return "UTF-8";
    }

    @DexIgnore
    @Deprecated
    public byte[] getPostBody() throws lu {
        Map<String, String> postParams = getPostParams();
        if (postParams == null || postParams.size() <= 0) {
            return null;
        }
        return encodeParameters(postParams, getPostParamsEncoding());
    }

    @DexIgnore
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    @Deprecated
    public Map<String, String> getPostParams() throws lu {
        return getParams();
    }

    @DexIgnore
    @Deprecated
    public String getPostParamsEncoding() {
        return getParamsEncoding();
    }

    @DexIgnore
    public c getPriority() {
        return c.NORMAL;
    }

    @DexIgnore
    public cv getRetryPolicy() {
        return this.mRetryPolicy;
    }

    @DexIgnore
    public final int getSequence() {
        Integer num = this.mSequence;
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("getSequence called before setSequence");
    }

    @DexIgnore
    public Object getTag() {
        return this.mTag;
    }

    @DexIgnore
    public final int getTimeoutMs() {
        return getRetryPolicy().a();
    }

    @DexIgnore
    public int getTrafficStatsTag() {
        return this.mDefaultTrafficStatsTag;
    }

    @DexIgnore
    public String getUrl() {
        return this.mUrl;
    }

    @DexIgnore
    public boolean hasHadResponseDelivered() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mResponseDelivered;
        }
        return z;
    }

    @DexIgnore
    public boolean isCanceled() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mCanceled;
        }
        return z;
    }

    @DexIgnore
    public void markDelivered() {
        synchronized (this.mLock) {
            this.mResponseDelivered = true;
        }
    }

    @DexIgnore
    public void notifyListenerResponseNotUsable() {
        b bVar;
        synchronized (this.mLock) {
            bVar = this.mRequestCompleteListener;
        }
        if (bVar != null) {
            bVar.a(this);
        }
    }

    @DexIgnore
    public void notifyListenerResponseReceived(av<?> avVar) {
        b bVar;
        synchronized (this.mLock) {
            bVar = this.mRequestCompleteListener;
        }
        if (bVar != null) {
            bVar.a(this, avVar);
        }
    }

    @DexIgnore
    public fv parseNetworkError(fv fvVar) {
        return fvVar;
    }

    @DexIgnore
    public abstract av<T> parseNetworkResponse(vu vuVar);

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public yu<?> setCacheEntry(mu.a aVar) {
        this.mCacheEntry = aVar;
        return this;
    }

    @DexIgnore
    public void setNetworkRequestCompleteListener(b bVar) {
        synchronized (this.mLock) {
            this.mRequestCompleteListener = bVar;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public yu<?> setRequestQueue(zu zuVar) {
        this.mRequestQueue = zuVar;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public yu<?> setRetryPolicy(cv cvVar) {
        this.mRetryPolicy = cvVar;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final yu<?> setSequence(int i) {
        this.mSequence = Integer.valueOf(i);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final yu<?> setShouldCache(boolean z) {
        this.mShouldCache = z;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final yu<?> setShouldRetryServerErrors(boolean z) {
        this.mShouldRetryServerErrors = z;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yu<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public yu<?> setTag(Object obj) {
        this.mTag = obj;
        return this;
    }

    @DexIgnore
    public final boolean shouldCache() {
        return this.mShouldCache;
    }

    @DexIgnore
    public final boolean shouldRetryServerErrors() {
        return this.mShouldRetryServerErrors;
    }

    @DexIgnore
    public String toString() {
        String str = "0x" + Integer.toHexString(getTrafficStatsTag());
        StringBuilder sb = new StringBuilder();
        sb.append(isCanceled() ? "[X] " : "[ ] ");
        sb.append(getUrl());
        sb.append(" ");
        sb.append(str);
        sb.append(" ");
        sb.append(getPriority());
        sb.append(" ");
        sb.append(this.mSequence);
        return sb.toString();
    }

    @DexIgnore
    public yu(int i, String str, av.a aVar) {
        this.mEventLog = gv.a.c ? new gv.a() : null;
        this.mLock = new Object();
        this.mShouldCache = true;
        this.mCanceled = false;
        this.mResponseDelivered = false;
        this.mShouldRetryServerErrors = false;
        this.mCacheEntry = null;
        this.mMethod = i;
        this.mUrl = str;
        this.mErrorListener = aVar;
        setRetryPolicy(new pu());
        this.mDefaultTrafficStatsTag = findDefaultTrafficStatsTag(str);
    }

    @DexIgnore
    public int compareTo(yu<T> yuVar) {
        c priority = getPriority();
        c priority2 = yuVar.getPriority();
        return priority == priority2 ? this.mSequence.intValue() - yuVar.mSequence.intValue() : priority2.ordinal() - priority.ordinal();
    }
}
