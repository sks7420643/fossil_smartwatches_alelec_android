package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft4 {
    @DexIgnore
    public /* final */ oy6.b a;
    @DexIgnore
    public /* final */ st4 b; // = st4.d.a();
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ e95 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(e95 e95) {
            super(e95.d());
            ee7.b(e95, "binding");
            this.a = e95;
        }

        @DexIgnore
        public final void a(un4 un4, oy6.b bVar, st4 st4) {
            ee7.b(un4, "friend");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            e95 e95 = this.a;
            String b = fu4.a.b(un4.b(), un4.e(), un4.i());
            FlexibleTextView flexibleTextView = e95.v;
            ee7.a((Object) flexibleTextView, "tvFullName");
            flexibleTextView.setText(b);
            ImageView imageView = e95.r;
            ee7.a((Object) imageView, "ivAvatar");
            rt4.a(imageView, un4.h(), b, bVar, st4);
            int c = un4.c();
            int i = 0;
            if (c == -1) {
                ImageView imageView2 = e95.s;
                ee7.a((Object) imageView2, "ivPending");
                imageView2.setVisibility(0);
                ImageView imageView3 = e95.s;
                ee7.a((Object) imageView3, "ivPending");
                imageView3.setImageDrawable(v6.c(imageView3.getContext(), 2131231015));
                ProgressBar progressBar = e95.u;
                ee7.a((Object) progressBar, "prg");
                progressBar.setVisibility(8);
            } else if (c == 0) {
                ImageView imageView4 = e95.s;
                ee7.a((Object) imageView4, "ivPending");
                imageView4.setVisibility(8);
                ProgressBar progressBar2 = e95.u;
                ee7.a((Object) progressBar2, "prg");
                if (un4.f() == 0) {
                    i = 8;
                }
                progressBar2.setVisibility(i);
            } else if (c == 1) {
                ImageView imageView5 = e95.s;
                ee7.a((Object) imageView5, "ivPending");
                imageView5.setVisibility(0);
                ImageView imageView6 = e95.s;
                ee7.a((Object) imageView6, "ivPending");
                imageView6.setImageDrawable(v6.c(imageView6.getContext(), 2131231117));
                ProgressBar progressBar3 = e95.u;
                ee7.a((Object) progressBar3, "prg");
                progressBar3.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public ft4(int i) {
        this.c = i;
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.a = b2;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        Object obj = list.get(i);
        if (obj instanceof un4) {
            un4 un4 = (un4) obj;
            if (un4.c() == 0 || un4.c() == 1 || un4.c() == -1) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        e95 a2 = e95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemFriendListBinding.in\u2026.context), parent, false)");
        return new a(a2);
    }

    @DexIgnore
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof un4)) {
            obj = null;
        }
        un4 un4 = (un4) obj;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        if (un4 != null && aVar != null) {
            aVar.a(un4, this.a, this.b);
        }
    }
}
