package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk4 implements Factory<df5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public xk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static xk4 a(wj4 wj4) {
        return new xk4(wj4);
    }

    @DexIgnore
    public static df5 b(wj4 wj4) {
        df5 o = wj4.o();
        c87.a(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public df5 get() {
        return b(this.a);
    }
}
