package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uq4 {
    @DexIgnore
    public /* final */ oy6.b a;
    @DexIgnore
    public /* final */ st4 b; // = st4.d.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ e95 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(e95 e95) {
            super(e95.d());
            ee7.b(e95, "binding");
            this.a = e95;
        }

        @DexIgnore
        public final void a(xn4 xn4, oy6.b bVar, st4 st4) {
            String str;
            ee7.b(xn4, "player");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            e95 e95 = this.a;
            if (ee7.a((Object) PortfolioApp.g0.c().w(), (Object) xn4.c())) {
                str = ig5.a(PortfolioApp.g0.c(), 2131886246);
            } else {
                str = fu4.a.b(xn4.b(), xn4.d(), xn4.e());
            }
            FlexibleTextView flexibleTextView = e95.v;
            ee7.a((Object) flexibleTextView, "tvFullName");
            flexibleTextView.setText(str);
            ImageView imageView = e95.r;
            ee7.a((Object) imageView, "ivAvatar");
            rt4.a(imageView, xn4.a(), xn4.b(), bVar, st4);
        }
    }

    @DexIgnore
    public uq4(int i) {
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.a = b2;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        return list.get(i) instanceof xn4;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        e95 a2 = e95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemFriendListBinding.in\u2026(inflater, parent, false)");
        return new a(a2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.xn4 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.xn4 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.xn4 */
    /* JADX WARN: Multi-variable type inference failed */
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        xn4 xn4 = null;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        Object obj = list.get(i);
        if (obj instanceof xn4) {
            xn4 = obj;
        }
        xn4 xn42 = xn4;
        if (aVar != null && xn42 != null) {
            aVar.a(xn42, this.a, this.b);
        }
    }
}
