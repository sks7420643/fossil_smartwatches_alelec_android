package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.uirenew.customview.TitleValueCell;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.zendesk.sdk.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x65 extends w65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i L; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray M;
    @DexIgnore
    public /* final */ ConstraintLayout J;
    @DexIgnore
    public long K;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        M = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        M.put(2131362636, 2);
        M.put(2131362597, 3);
        M.put(2131363113, 4);
        M.put(R.id.scroll, 5);
        M.put(2131362400, 6);
        M.put(2131362042, 7);
        M.put(2131362594, 8);
        M.put(2131362348, 9);
        M.put(2131362356, 10);
        M.put(2131362515, 11);
        M.put(2131362376, 12);
        M.put(2131362362, 13);
        M.put(2131362315, 14);
        M.put(2131362207, 15);
        M.put(2131363093, 16);
        M.put(2131362925, 17);
        M.put(2131363363, 18);
        M.put(2131361959, 19);
    }
    */

    @DexIgnore
    public x65(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 20, L, M));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.K = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.K != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.K = 1;
        }
        g();
    }

    @DexIgnore
    public x65(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[19], (ConstraintLayout) objArr[7], (TitleValueCell) objArr[15], (FriendsInView) objArr[14], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[6], (TimerTextView) objArr[11], (ImageView) objArr[8], (RTLImageView) objArr[3], (RTLImageView) objArr[2], (TitleValueCell) objArr[17], (NestedScrollView) objArr[5], (TitleValueCell) objArr[16], (SwipeRefreshLayout) objArr[4], (FlexibleTextView) objArr[1], (TitleValueCell) objArr[18]);
        this.K = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.J = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
