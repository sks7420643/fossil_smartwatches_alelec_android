package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fb7<T> {
    @DexIgnore
    ib7 getContext();

    @DexIgnore
    void resumeWith(Object obj);
}
