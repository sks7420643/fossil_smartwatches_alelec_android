package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import com.fossil.f60;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.WorkoutSessionManager;
import com.fossil.l60;
import com.fossil.pc0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km1 implements l60, pd0, cd0, hd0, jd0, mc0, ld0, rd0, pc0, qc0, rc0, tc0, ud0, uc0, vc0, xc0, ad0, bd0, ui0, rk0, om0, wc0, vd0, nc0, ed0, fd0, xd0, gd0, de0, ge0, zd0, zc0, sc0, kd0, nd0, od0, wd0, qd0, sd0, ee0, fe0, be0, yc0, ce0, oc0, dd0, kc0, id0, lc0, yd0, md0, ae0, td0 {
    @DexIgnore
    public static /* final */ gu0 CREATOR; // = new gu0(null);
    @DexIgnore
    public /* final */ en0 a; // = new en0(this);
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ ri1 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ u01 f;
    @DexIgnore
    public /* final */ vk1 g;
    @DexIgnore
    public /* final */ il0 h;
    @DexIgnore
    public /* final */ gd7<xp0, i97> i;
    @DexIgnore
    public /* final */ kd7<byte[], qk1, i97> j;
    @DexIgnore
    public String p;
    @DexIgnore
    public /* final */ Object q;
    @DexIgnore
    public pc0.a r;
    @DexIgnore
    public WorkoutSessionManager s;
    @DexIgnore
    public m60 t;
    @DexIgnore
    public l60.c u;
    @DexIgnore
    public l60.b v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ BluetoothDevice x;
    @DexIgnore
    public /* final */ String y;

    @DexIgnore
    public /* synthetic */ km1(BluetoothDevice bluetoothDevice, String str, zd7 zd7) {
        this.x = bluetoothDevice;
        this.y = str;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.c = w51.b.a(this.x);
            this.f = new u01(new wj0(new Hashtable(), null));
            this.g = new vk1();
            this.h = new il0(this);
            this.i = new on0(this);
            this.j = new uj0(this);
            this.p = yh0.a("UUID.randomUUID().toString()");
            this.q = new Object();
            this.t = new m60(this.c.d(), this.c.u, this.y, "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
            this.u = l60.c.DISCONNECTED;
            ri1 ri1 = this.c;
            il0 il0 = this.h;
            if (!ri1.h.contains(il0)) {
                ri1.h.add(il0);
            }
            m60 m60 = this.t;
            yj0.e.a(m60.getMacAddress(), m60);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ void a(km1 km1, le0 le0, String str, String str2, Object... objArr) {
    }

    @DexIgnore
    public l60.b A() {
        return this.v;
    }

    @DexIgnore
    public l60.d B() {
        return l60.d.f.a(this.c.e());
    }

    @DexIgnore
    public final void a(je0 je0) {
    }

    @DexIgnore
    @Override // com.fossil.tc0
    public he0<m60> c() {
        he0<m60> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        if (this.u == l60.c.CONNECTED) {
            he0<m60> he02 = new he0<>();
            he02.a(1.0f);
            he02.c(this.t);
            return he02;
        }
        aj1 aj1 = new aj1(this.c, this.a, yh0.a("UUID.randomUUID().toString()"));
        synchronized (this.q) {
            p60 a2 = a(aj1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                aj1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new eb1(this, aj1));
                og1 og1 = new og1(this, aj1);
                if (!((zk0) aj1).t) {
                    ((zk0) aj1).d.add(og1);
                }
                jk1 jk1 = new jk1(he0, this, aj1);
                if (!((zk0) aj1).t) {
                    ((zk0) aj1).h.add(jk1);
                }
                aj1.a(new n81(he0, this, aj1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                aj1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.mc0
    public he0<i97> cleanUp() {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        hq0 hq0 = new hq0(this.c, this.a);
        synchronized (this.q) {
            p60 a2 = a(hq0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                hq0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new rr0(this, hq0));
                nt0 nt0 = new nt0(this, hq0);
                if (!((zk0) hq0).t) {
                    ((zk0) hq0).d.add(nt0);
                }
                zw0 zw0 = new zw0(he0, this, hq0);
                if (!((zk0) hq0).t) {
                    ((zk0) hq0).h.add(zw0);
                }
                hq0.a(new pf1(he0, this, hq0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                hq0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.sc0
    public ie0<byte[]> d(byte[] bArr) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(bArr, (String) null, 1);
        g61 g61 = new g61(this.c, this.a, bArr);
        g61.c(new e11(this));
        synchronized (this.q) {
            p60 a2 = a(g61);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                g61.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new uc1(this, g61));
                oe1 oe1 = new oe1(this, g61);
                if (!((zk0) g61).t) {
                    ((zk0) g61).d.add(oe1);
                }
                fi1 fi1 = new fi1(he0, this, g61);
                if (!((zk0) g61).t) {
                    ((zk0) g61).h.add(fi1);
                }
                g61.a(new lz0(he0, this, g61));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                g61.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.id0
    public ie0<i97> e() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ie0<i97> ie0 = new ie0<>();
        tk0 tk0 = new tk0(this.c, this.a, yh0.a("UUID.randomUUID().toString()"));
        synchronized (this.q) {
            p60 a2 = a(tk0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                tk0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new sp0(this, tk0));
                or0 or0 = new or0(this, tk0);
                if (!((zk0) tk0).t) {
                    ((zk0) tk0).d.add(or0);
                }
                fv0 fv0 = new fv0(he0, this, tk0);
                if (!((zk0) tk0).t) {
                    ((zk0) tk0).h.add(fv0);
                }
                tk0.a(new mj0(he0, this, tk0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                tk0.l();
            }
        }
        he0.e((gd7) new kl0(ie0)).b((gd7<? super je0, i97>) new gn0(ie0));
        return ie0;
    }

    @DexIgnore
    @Override // com.fossil.zc0
    public byte[] f() {
        return mp0.b.a(this.c.u).a;
    }

    @DexIgnore
    @Override // com.fossil.rc0
    public boolean g() {
        le0 le0 = le0.DEBUG;
        if (!isActive()) {
            xm0.i.e(this);
        }
        wl0.h.a(new wr1(yz0.a(aw0.a), ci1.d, this.c.u, yz0.a(aw0.a), yh0.a("UUID.randomUUID().toString()"), true, this.p, null, null, null, 896));
        c(true);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.l60
    public l60.a getBondState() {
        return l60.a.b.a(this.c.getBondState());
    }

    @DexIgnore
    @Override // com.fossil.l60
    public l60.c getState() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.gd0
    public ie0<Integer> h() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ga1 ga1 = new ga1(this.c, this.a);
        synchronized (this.q) {
            p60 a2 = a(ga1);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ga1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new kk1(this, ga1));
                im1 im1 = new im1(this, ga1);
                if (!((zk0) ga1).t) {
                    ((zk0) ga1).d.add(im1);
                }
                hq1 hq1 = new hq1(he0, this, ga1);
                if (!((zk0) ga1).t) {
                    ((zk0) ga1).h.add(hq1);
                }
                ga1.a(new t81(he0, this, ga1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ga1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.hd0
    public ie0<i97> i() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ri1 ri1 = this.c;
        bn0 bn0 = new bn0(ri1, this.a, wm0.r, new h31(ri1));
        synchronized (this.q) {
            p60 a2 = a(bn0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                bn0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new sr0(this, bn0));
                ot0 ot0 = new ot0(this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).d.add(ot0);
                }
                ax0 ax0 = new ax0(he0, this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).h.add(ax0);
                }
                bn0.a(new vf1(he0, this, bn0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                bn0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.l60
    public boolean isActive() {
        return xm0.i.b(this);
    }

    @DexIgnore
    @Override // com.fossil.jd0
    public ie0<i97> j() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ri1 ri1 = this.c;
        bn0 bn0 = new bn0(ri1, this.a, wm0.q, new e51(ri1));
        synchronized (this.q) {
            p60 a2 = a(bn0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                bn0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new io1(this, bn0));
                iq1 iq1 = new iq1(this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).d.add(iq1);
                }
                ki0 ki0 = new ki0(he0, this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).h.add(ki0);
                }
                bn0.a(new ln1(he0, this, bn0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                bn0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.l60
    public ie0<i97> k() {
        le0 le0 = le0.DEBUG;
        wl0.h.a(new wr1(yz0.a(aw0.e), ci1.h, this.c.u, "", "", true, null, null, null, null, 960));
        ie0<i97> ie0 = new ie0<>();
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new lh1(this, ie0, null), 3, null);
        return ie0;
    }

    @DexIgnore
    @Override // com.fossil.vc0
    public he0<HashMap<o80, n80>> m() {
        he0<HashMap<o80, n80>> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ri1 ri1 = this.c;
        jq0 jq0 = new jq0(ri1, this.a, gq0.b.a(ri1.u, pb1.DEVICE_CONFIG));
        synchronized (this.q) {
            p60 a2 = a(jq0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                jq0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new zc1(this, jq0));
                te1 te1 = new te1(this, jq0);
                if (!((zk0) jq0).t) {
                    ((zk0) jq0).d.add(te1);
                }
                mi1 mi1 = new mi1(he0, this, jq0);
                if (!((zk0) jq0).t) {
                    ((zk0) jq0).h.add(mi1);
                }
                jq0.a(new gn1(he0, this, jq0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                jq0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.l60
    public m60 n() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.ad0
    public ie0<ab0> p() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        uk0 uk0 = new uk0(this.c, this.a);
        synchronized (this.q) {
            p60 a2 = a(uk0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                uk0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new hg1(this, uk0));
                di1 di1 = new di1(this, uk0);
                if (!((zk0) uk0).t) {
                    ((zk0) uk0).d.add(di1);
                }
                zl1 zl1 = new zl1(he0, this, uk0);
                if (!((zk0) uk0).t) {
                    ((zk0) uk0).h.add(zl1);
                }
                uk0.a(new gj0(he0, this, uk0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                uk0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.qd0
    public ie0<i97> s() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ri1 ri1 = this.c;
        bn0 bn0 = new bn0(ri1, this.a, wm0.t, new c71(ri1));
        synchronized (this.q) {
            p60 a2 = a(bn0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                bn0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new ty0(this, bn0));
                k01 k01 = new k01(this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).d.add(k01);
                }
                x31 x31 = new x31(he0, this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).h.add(x31);
                }
                bn0.a(new lc1(he0, this, bn0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                bn0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public final ie0<i97> t() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ri1 ri1 = this.c;
        um1 um1 = new um1(ri1, this.a, f31.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY, mp0.b.a(ri1.u).a(), yh0.a("UUID.randomUUID().toString()"));
        um1.c(new m81(this));
        synchronized (this.q) {
            p60 a2 = a(um1);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                um1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new tw0(this, um1));
                ly0 ly0 = new ly0(this, um1);
                if (!((zk0) um1).t) {
                    ((zk0) um1).d.add(ly0);
                }
                w11 w11 = new w11(he0, this, um1);
                if (!((zk0) um1).t) {
                    ((zk0) um1).h.add(w11);
                }
                um1.a(new o61(he0, this, um1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                um1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public final boolean u() {
        boolean z;
        synchronized (Boolean.valueOf(this.e)) {
            z = this.e;
        }
        return z;
    }

    @DexIgnore
    public final void v() {
        le0 le0 = le0.DEBUG;
        this.f.a(is0.INTERRUPTED, new wm0[]{wm0.f});
        if (this.u != l60.c.DISCONNECTED && !this.d) {
            es0 es0 = new es0(this.c, this.a);
            es0.a(new gl0(this));
            es0.l();
        }
    }

    @DexIgnore
    public final void w() {
        this.t = new m60(this.t.getName(), this.t.getMacAddress(), this.t.getSerialNumber(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.x, i2);
        }
        if (parcel != null) {
            parcel.writeString(this.y);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v2. Raw type applied. Possible types: com.fossil.kd7<byte[], com.fossil.qk1, com.fossil.i97>, com.fossil.kd7<? super byte[], ? super com.fossil.qk1, com.fossil.i97> */
    public final void x() {
        ec1 ec1 = new ec1(this.c, this.a);
        ec1.C = this.i;
        ec1.D = this.j;
        ec1.c(wj1.a);
        ec1.b(new ul1(this));
        ec1.l();
    }

    @DexIgnore
    public final ie0<i97> y() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        vq0 vq0 = new vq0(this.c, this.a, null, 4);
        synchronized (this.q) {
            p60 a2 = a(vq0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                vq0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new bd1(this, vq0));
                ke1 ke1 = new ke1(this, vq0);
                if (!((zk0) vq0).t) {
                    ((zk0) vq0).d.add(ke1);
                }
                vl0 vl0 = new vl0(he0, this, vq0);
                if (!((zk0) vq0).t) {
                    ((zk0) vq0).h.add(vl0);
                }
                vq0.a(new k31(he0, this, vq0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                vq0.l();
            }
        }
        he0.f(h51.a);
        return he0;
    }

    @DexIgnore
    public boolean z() {
        boolean z;
        synchronized (Boolean.valueOf(this.w)) {
            z = this.w;
        }
        return z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.km1 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.l60
    public <T> T a(Class<T> cls) {
        if (!ee7.a(u31.g.f(), (Object) true) || !t97.a((Object[]) this.t.getDeviceType().a(), (Object) cls)) {
            le0 le0 = le0.DEBUG;
            cls.getSimpleName();
            return null;
        }
        le0 le02 = le0.DEBUG;
        cls.getSimpleName();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.bd0
    public ie0<i97> b() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        so0 so0 = new so0(this.c, this.a);
        synchronized (this.q) {
            p60 a2 = a(so0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                so0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new ji0(this, so0));
                gk0 gk0 = new gk0(this, so0);
                if (!((zk0) so0).t) {
                    ((zk0) so0).d.add(gk0);
                }
                zn0 zn0 = new zn0(he0, this, so0);
                if (!((zk0) so0).t) {
                    ((zk0) so0).h.add(zn0);
                }
                so0.a(new mu0(he0, this, so0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                so0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.nc0
    public he0<i97> a(jg0[] jg0Arr) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(jg0Arr).toString(2);
        ds0 ds0 = new ds0(this.c, this.a, jg0Arr);
        synchronized (this.q) {
            p60 a2 = a(ds0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ds0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new dv0(this, ds0));
                vw0 vw0 = new vw0(this, ds0);
                if (!((zk0) ds0).t) {
                    ((zk0) ds0).d.add(vw0);
                }
                e01 e01 = new e01(he0, this, ds0);
                if (!((zk0) ds0).t) {
                    ((zk0) ds0).h.add(e01);
                }
                ds0.a(new dp1(he0, this, ds0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ds0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.lc0
    public he0<i97> b(long j2) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        String str = "setBuddyChallengeMinimumStepThreshold invoked: " + "minimumStepThreshold=" + j2 + '.';
        p61 p61 = new p61(this.c, this.a, j2, null, 8);
        synchronized (this.q) {
            p60 a2 = a(p61);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                p61.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new n71(this, p61));
                i91 i91 = new i91(this, p61);
                if (!((zk0) p61).t) {
                    ((zk0) p61).d.add(i91);
                }
                wc1 wc1 = new wc1(he0, this, p61);
                if (!((zk0) p61).t) {
                    ((zk0) p61).h.add(wc1);
                }
                p61.a(new nc1(he0, this, p61));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                p61.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ee0
    public ie0<i97> d() {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        h11 h11 = new h11(this.c, this.a, yh0.a("UUID.randomUUID().toString()"));
        synchronized (this.q) {
            p60 a2 = a(h11);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                h11.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new yp1(this, h11));
                xr1 xr1 = new xr1(this, h11);
                if (!((zk0) h11).t) {
                    ((zk0) h11).d.add(xr1);
                }
                zj0 zj0 = new zj0(he0, this, h11);
                if (!((zk0) h11).t) {
                    ((zk0) h11).h.add(zj0);
                }
                h11.a(new xh0(he0, this, h11));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                h11.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.zd0
    public void e(byte[] bArr) {
        le0 le0 = le0.DEBUG;
        yz0.a(bArr, (String) null, 1);
        wl0.h.a(new wr1(yz0.a(aw0.c), ci1.d, this.c.u, yz0.a(aw0.c), yh0.a("UUID.randomUUID().toString()"), true, this.p, null, null, yz0.a(new JSONObject(), r51.r2, Long.valueOf(ik1.a.a(bArr, ng1.CRC32))), 384));
        mp0.b.a(this.c.u, bArr);
    }

    @DexIgnore
    @Override // com.fossil.md0
    public he0<i97> c(byte[] bArr) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        String str = "sendEncryptedData invoked: " + "encryptedData=" + yz0.a(bArr, (String) null, 1) + '.';
        cp1 cp1 = new cp1(this.c, this.a, bArr, null, 8);
        synchronized (this.q) {
            p60 a2 = a(cp1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                cp1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new g01(this, cp1));
                a21 a21 = new a21(this, cp1);
                if (!((zk0) cp1).t) {
                    ((zk0) cp1).d.add(a21);
                }
                q51 q51 = new q51(he0, this, cp1);
                if (!((zk0) cp1).t) {
                    ((zk0) cp1).h.add(q51);
                }
                cp1.a(new oj0(he0, this, cp1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                cp1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.kc0
    public ie0<i97> a(long j2) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        f61 f61 = new f61(this.c, this.a, j2);
        synchronized (this.q) {
            p60 a2 = a(f61);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                f61.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new gi0(this, f61));
                dk0 dk0 = new dk0(this, f61);
                if (!((zk0) f61).t) {
                    ((zk0) f61).d.add(dk0);
                }
                wn0 wn0 = new wn0(he0, this, f61);
                if (!((zk0) f61).t) {
                    ((zk0) f61).h.add(wn0);
                }
                f61.a(new cl0(he0, this, f61));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                f61.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ge0
    public ie0<Boolean> b(byte[] bArr) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ik1.a.a(bArr, ng1.CRC32);
        gw0 gw0 = new gw0(this.c, this.a, bArr);
        synchronized (this.q) {
            p60 a2 = a(gw0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                gw0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new dk1(this, gw0));
                bm1 bm1 = new bm1(this, gw0);
                if (!((zk0) gw0).t) {
                    ((zk0) gw0).d.add(bm1);
                }
                aq1 aq1 = new aq1(he0, this, gw0);
                if (!((zk0) gw0).t) {
                    ((zk0) gw0).h.add(aq1);
                }
                gw0.a(new mk1(he0, this, gw0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                gw0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public void c(boolean z) {
        wl0.h.a(new wr1(yz0.a(aw0.f), ci1.h, this.c.u, "", "", true, null, null, null, yz0.a(new JSONObject(), r51.x0, Boolean.valueOf(z)), 448));
        synchronized (Boolean.valueOf(this.w)) {
            this.w = z;
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.qc0
    public boolean a() {
        le0 le0 = le0.DEBUG;
        if (isActive()) {
            xm0.i.f(this);
        }
        wl0.h.a(new wr1(yz0.a(aw0.b), ci1.d, this.c.u, yz0.a(aw0.b), yh0.a("UUID.randomUUID().toString()"), true, this.p, null, null, null, 896));
        v();
        z();
        c(false);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cd0
    public ie0<i97> a(m70 m70, j70[] j70Arr) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(j70Arr).toString(2);
        ri1 ri1 = this.c;
        bn0 bn0 = new bn0(ri1, this.a, wm0.s, new p11(ri1, m70, j70Arr));
        synchronized (this.q) {
            p60 a2 = a(bn0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                bn0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new hk0(this, bn0));
                em0 em0 = new em0(this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).d.add(em0);
                }
                wp0 wp0 = new wp0(he0, this, bn0);
                if (!((zk0) bn0).t) {
                    ((zk0) bn0).h.add(wp0);
                }
                bn0.a(new oz0(he0, this, bn0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                bn0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public final void b(boolean z) {
        synchronized (Boolean.valueOf(this.e)) {
            this.e = z;
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final q60<m60, eu0> b(HashMap<yo1, Object> hashMap) {
        le0 le0 = le0.DEBUG;
        se7 se7 = new se7();
        se7.element = null;
        q60<m60, eu0> q60 = new q60<>();
        q60.a(new ps0(se7));
        if (this.u == l60.c.CONNECTED) {
            q60.c(this.t);
        } else if (!this.d) {
            this.p = yh0.a("UUID.randomUUID().toString()");
            yj0.e.a(this.t.getMacAddress(), this.p);
            T t2 = (T) new s21(this.c, this.a, hashMap, yh0.a("UUID.randomUUID().toString()"));
            se7.element = t2;
            T t3 = t2;
            cn0 cn0 = new cn0(this);
            if (!((zk0) t3).t) {
                ((zk0) t3).d.add(cn0);
            }
            t3.c(new yo0(this, q60));
            t3.b(new uq0(this, q60));
            se7.element.l();
        } else {
            le0 le02 = le0.DEBUG;
            q60.b(new eu0(wm0.b, is0.NOT_ALLOW_TO_START, null, 4));
        }
        return q60;
    }

    @DexIgnore
    @Override // com.fossil.dd0
    public ie0<i97> a(w90 w90) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        w90.a(2);
        o41 o41 = new o41(this.c, this.a, w90);
        synchronized (this.q) {
            p60 a2 = a(o41);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                o41.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new hi1(this, o41));
                co1 co1 = new co1(this, o41);
                if (!((zk0) o41).t) {
                    ((zk0) o41).d.add(co1);
                }
                bs1 bs1 = new bs1(he0, this, o41);
                if (!((zk0) o41).t) {
                    ((zk0) o41).h.add(bs1);
                }
                o41.a(new u61(he0, this, o41));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                o41.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public static /* synthetic */ he0 b(km1 km1, boolean z, boolean z2, a61 a61, int i2) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                a61 = a61.LOW;
            } else {
                a61 = a61.NORMAL;
            }
        }
        return km1.b(z, z2, a61);
    }

    @DexIgnore
    public final he0<byte[][]> b(boolean z, boolean z2, a61 a61) {
        wo1 wo1;
        he0<byte[][]> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        if (z2) {
            wo1 = new xm1(this.c, this.a, oa7.b(w87.a(xf0.SKIP_ERASE, false), w87.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS, false)));
        } else {
            wo1 wo12 = new wo1(this.c, this.a, oa7.b(w87.a(xf0.SKIP_ERASE, Boolean.valueOf(z)), w87.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))), (String) null, 8);
            wo12.a(a61);
            wo1 = wo12;
        }
        synchronized (this.q) {
            p60 a2 = a(wo1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                wo1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new gs1(this, wo1));
                sy0 sy0 = new sy0(this, wo1);
                if (!((zk0) wo1).t) {
                    ((zk0) wo1).d.add(sy0);
                }
                jm1 jm1 = new jm1(he0, this, wo1);
                if (!((zk0) wo1).t) {
                    ((zk0) wo1).h.add(jm1);
                }
                wo1.a(new s61(he0, this, wo1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                wo1.l();
            }
        }
        he0.f(p81.a);
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ed0
    public ie0<i97> a(m90 m90) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        m90.a(2);
        l61 l61 = new l61(this.c, this.a, m90);
        synchronized (this.q) {
            p60 a2 = a(l61);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                l61.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new rn0(this, l61));
                np0 np0 = new np0(this, l61);
                if (!((zk0) l61).t) {
                    ((zk0) l61).d.add(np0);
                }
                et0 et0 = new et0(he0, this, l61);
                if (!((zk0) l61).t) {
                    ((zk0) l61).h.add(et0);
                }
                l61.a(new tf1(he0, this, l61));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                l61.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public final he0<i97> b(cf0 cf0) {
        pb1 pb1;
        he0<i97> he0;
        is0 is0;
        if ((cf0 instanceof ye0) || (cf0 instanceof nf0) || (cf0 instanceof jf0) || (cf0 instanceof of0) || (cf0 instanceof bf0) || (cf0 instanceof we0) || (cf0 instanceof xe0) || (cf0 instanceof ef0) || (cf0 instanceof kf0) || (cf0 instanceof hf0) || (cf0 instanceof gf0) || (cf0 instanceof lf0) || (cf0 instanceof mf0) || (cf0 instanceof pf0) || (cf0 instanceof qf0)) {
            pb1 = pb1.UI_SCRIPT;
        } else if (!(cf0 instanceof if0) && !(cf0 instanceof af0) && !(cf0 instanceof ze0) && !(cf0 instanceof ff0)) {
            he0<i97> he02 = new he0<>();
            he02.b(new o60(p60.INVALID_PARAMETERS, null, 2));
            return he02;
        } else {
            pb1 = pb1.MICRO_APP;
        }
        dn1 dn1 = new dn1(this.c, this.a, cf0, pb1, null, 16);
        synchronized (this.q) {
            p60 a2 = a(dn1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                dn1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new ue1(this, dn1));
                qg1 qg1 = new qg1(this, dn1);
                if (!((zk0) dn1).t) {
                    ((zk0) dn1).d.add(qg1);
                }
                lk1 lk1 = new lk1(he0, this, dn1);
                if (!((zk0) dn1).t) {
                    ((zk0) dn1).h.add(lk1);
                }
                dn1.a(new nn1(he0, this, dn1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                dn1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.fd0
    public he0<String> a(byte[] bArr, String str) {
        he0<String> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        int length = bArr.length;
        aj0 aj0 = new aj0(this.c, this.a, bArr, str);
        jp1 jp1 = new jp1(this);
        if (!((zk0) aj0).t) {
            ((zk0) aj0).d.add(jp1);
        }
        aj0.c(new ir1(this));
        aj0.b(new nh0(this));
        synchronized (this.q) {
            p60 a2 = a(aj0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                aj0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new hm1(this, aj0));
                go1 go1 = new go1(this, aj0);
                if (!((zk0) aj0).t) {
                    ((zk0) aj0).d.add(go1);
                }
                fs1 fs1 = new fs1(he0, this, aj0);
                if (!((zk0) aj0).t) {
                    ((zk0) aj0).h.add(fs1);
                }
                aj0.a(new jn1(he0, this, aj0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                aj0.l();
            }
        }
        he0.f(kj0.a);
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.kd0
    public he0<i97> a(o90 o90) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        o90.a(2);
        ac1 ac1 = new ac1(this.c, this.a, o90, 0, null, 24);
        synchronized (this.q) {
            p60 a2 = a(ac1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ac1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new ai1(this, ac1));
                yj1 yj1 = new yj1(this, ac1);
                if (!((zk0) ac1).t) {
                    ((zk0) ac1).d.add(yj1);
                }
                vn1 vn1 = new vn1(he0, this, ac1);
                if (!((zk0) ac1).t) {
                    ((zk0) ac1).h.add(vn1);
                }
                ac1.a(new ou0(he0, this, ac1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ac1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ld0
    public he0<i97> a(cf0 cf0) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        cf0.a(2);
        if (!(cf0 instanceof rf0)) {
            return b(cf0);
        }
        fr1 fr1 = new fr1(this.c, this.a, (rf0) cf0, null, 8);
        synchronized (this.q) {
            p60 a2 = a(fr1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                fr1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new r71(this, fr1));
                m91 m91 = new m91(this, fr1);
                if (!((zk0) fr1).t) {
                    ((zk0) fr1).d.add(m91);
                }
                ad1 ad1 = new ad1(he0, this, fr1);
                if (!((zk0) fr1).t) {
                    ((zk0) fr1).h.add(ad1);
                }
                fr1.a(new xf1(he0, this, fr1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                fr1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.nd0
    public he0<i97> a(n90 n90) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        n90.a(2);
        br1 br1 = new br1(this.c, this.a, n90, 0, null, 24);
        synchronized (this.q) {
            p60 a2 = a(br1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                br1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new vp1(this, br1));
                ur1 ur1 = new ur1(this, br1);
                if (!((zk0) br1).t) {
                    ((zk0) br1).d.add(ur1);
                }
                xj0 xj0 = new xj0(he0, this, br1);
                if (!((zk0) br1).t) {
                    ((zk0) br1).h.add(xj0);
                }
                br1.a(new ar0(he0, this, br1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                br1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.od0
    public he0<i97> a(u60[] u60Arr) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(u60Arr).toString(2);
        gh0 gh0 = new gh0(this.c, this.a, u60Arr, 0, null, 24);
        synchronized (this.q) {
            p60 a2 = a(gh0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                gh0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new u51(this, gh0));
                s71 s71 = new s71(this, gh0);
                if (!((zk0) gh0).t) {
                    ((zk0) gh0).d.add(s71);
                }
                hb1 hb1 = new hb1(he0, this, gh0);
                if (!((zk0) gh0).t) {
                    ((zk0) gh0).h.add(hb1);
                }
                gh0.a(new by0(he0, this, gh0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                gh0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.sd0
    public he0<o80[]> a(n80[] n80Arr) {
        he0<o80[]> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(n80Arr).toString(2);
        ArrayList arrayList = new ArrayList();
        for (n80 n80 : n80Arr) {
            if (t97.a(this.t.h(), n80.getKey())) {
                arrayList.add(n80);
            }
        }
        if (arrayList.isEmpty()) {
            he0<o80[]> he02 = new he0<>();
            he02.c(new o80[0]);
            return he02;
        }
        ri1 ri1 = this.c;
        en0 en0 = this.a;
        Object[] array = arrayList.toArray(new n80[0]);
        if (array != null) {
            jj1 jj1 = new jj1(ri1, en0, (n80[]) array, 0, null, 24);
            synchronized (this.q) {
                p60 a2 = a(jj1);
                he0 = new he0<>();
                if (a2 != null) {
                    if (kz0.d[a2.ordinal()] != 1) {
                        is0 = is0.NOT_ALLOW_TO_START;
                    } else {
                        is0 = is0.BLUETOOTH_OFF;
                    }
                    jj1.a(is0);
                    he0.b(new o60(a2, null, 2));
                } else {
                    he0.a((gd7<? super je0, i97>) new t51(this, jj1));
                    q71 q71 = new q71(this, jj1);
                    if (!((zk0) jj1).t) {
                        ((zk0) jj1).d.add(q71);
                    }
                    fb1 fb1 = new fb1(he0, this, jj1);
                    if (!((zk0) jj1).t) {
                        ((zk0) jj1).h.add(fb1);
                    }
                    jj1.a(new or1(he0, this, jj1));
                    if (this.u != l60.c.CONNECTED && !this.d) {
                        yp0.f.f();
                        b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                    }
                    jj1.l();
                }
            }
            return he0;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.td0
    public he0<r60> a(eg0 eg0) {
        he0<r60> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        eg0.a(2);
        oq0 oq0 = new oq0(this.c, this.a, eg0);
        oq0.c(new gp0(this));
        synchronized (this.q) {
            p60 a2 = a(oq0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                oq0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new v31(this, oq0));
                s51 s51 = new s51(this, oq0);
                if (!((zk0) oq0).t) {
                    ((zk0) oq0).d.add(s51);
                }
                k91 k91 = new k91(he0, this, oq0);
                if (!((zk0) oq0).t) {
                    ((zk0) oq0).h.add(k91);
                }
                oq0.a(new kn0(he0, this, oq0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                oq0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.l60
    public void a(l60.b bVar) {
        this.v = bVar;
    }

    @DexIgnore
    @Override // com.fossil.ud0
    public he0<i97> a(boolean z) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        zm0 zm0 = new zm0(this.c, this.a, z, null, 8);
        synchronized (this.q) {
            p60 a2 = a(zm0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                zm0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new zu0(this, zm0));
                rw0 rw0 = new rw0(this, zm0);
                if (!((zk0) zm0).t) {
                    ((zk0) zm0).d.add(rw0);
                }
                a01 a01 = new a01(he0, this, zm0);
                if (!((zk0) zm0).t) {
                    ((zk0) zm0).h.add(a01);
                }
                zm0.a(new lw0(he0, this, zm0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                zm0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.vd0
    public he0<String> a(fg0 fg0) {
        he0<String> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        fg0.a(2);
        js0 js0 = new js0(this.c, this.a, fg0);
        js0.c(new d51(this));
        synchronized (this.q) {
            p60 a2 = a(js0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                js0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new zr1(this, js0));
                ei0 ei0 = new ei0(this, js0);
                if (!((zk0) js0).t) {
                    ((zk0) js0).d.add(ei0);
                }
                yl0 yl0 = new yl0(he0, this, js0);
                if (!((zk0) js0).t) {
                    ((zk0) js0).h.add(yl0);
                }
                js0.a(new g31(he0, this, js0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                js0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.wd0
    public he0<i97> a(y90[] y90Arr) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(y90Arr).toString(2);
        ux0 ux0 = new ux0(this.c, this.a, y90Arr, null, 8);
        synchronized (this.q) {
            p60 a2 = a(ux0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ux0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new u11(this, ux0));
                n31 n31 = new n31(this, ux0);
                if (!((zk0) ux0).t) {
                    ((zk0) ux0).d.add(n31);
                }
                h71 h71 = new h71(he0, this, ux0);
                if (!((zk0) ux0).t) {
                    ((zk0) ux0).h.add(h71);
                }
                ux0.a(new uj1(he0, this, ux0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ux0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.xd0
    public he0<i97> a(wg0 wg0) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        wg0.a(2);
        dl0 dl0 = new dl0(this.c, this.a, wg0, null, 8);
        synchronized (this.q) {
            p60 a2 = a(dl0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                dl0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new un0(this, dl0));
                qp0 qp0 = new qp0(this, dl0);
                if (!((zk0) dl0).t) {
                    ((zk0) dl0).d.add(qp0);
                }
                it0 it0 = new it0(he0, this, dl0);
                if (!((zk0) dl0).t) {
                    ((zk0) dl0).h.add(it0);
                }
                dl0.a(new qr1(he0, this, dl0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                dl0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.yd0
    public he0<i97> a(vg0[] vg0Arr) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(vg0Arr).toString(2);
        ls0 ls0 = new ls0(this.c, this.a, vg0Arr, null, 8);
        synchronized (this.q) {
            p60 a2 = a(ls0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ls0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new qe1(this, ls0));
                mg1 mg1 = new mg1(this, ls0);
                if (!((zk0) ls0).t) {
                    ((zk0) ls0).d.add(mg1);
                }
                hk1 hk1 = new hk1(he0, this, ls0);
                if (!((zk0) ls0).t) {
                    ((zk0) ls0).h.add(hk1);
                }
                ls0.a(new mn0(he0, this, ls0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ls0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ae0
    public he0<hg0[]> a(hg0[] hg0Arr) {
        he0<hg0[]> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(hg0Arr).toString(2);
        rf1 rf1 = new rf1(this.c, this.a, hg0Arr);
        synchronized (this.q) {
            p60 a2 = a(rf1);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                rf1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new fm1(this, rf1));
                eo1 eo1 = new eo1(this, rf1);
                if (!((zk0) rf1).t) {
                    ((zk0) rf1).d.add(eo1);
                }
                ds1 ds1 = new ds1(he0, this, rf1);
                if (!((zk0) rf1).t) {
                    ((zk0) rf1).h.add(ds1);
                }
                rf1.a(new uu0(he0, this, rf1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                rf1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.ce0
    public he0<i97> a(ig0 ig0) {
        he0<i97> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        ig0.a(2);
        fu0 fu0 = new fu0(this.c, this.a, ig0);
        fu0.c(new va1(this));
        synchronized (this.q) {
            p60 a2 = a(fu0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                fu0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new r31(this, fu0));
                o51 o51 = new o51(this, fu0);
                if (!((zk0) fu0).t) {
                    ((zk0) fu0).d.add(o51);
                }
                h91 h91 = new h91(he0, this, fu0);
                if (!((zk0) fu0).t) {
                    ((zk0) fu0).h.add(h91);
                }
                fu0.a(new a91(he0, this, fu0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                fu0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.de0
    public ie0<byte[]> a(byte[] bArr) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        yz0.a(bArr, (String) null, 1);
        ju0 ju0 = new ju0(this.c, this.a, f31.PRE_SHARED_KEY, bArr);
        synchronized (this.q) {
            p60 a2 = a(ju0);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                ju0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new p31(this, ju0));
                m51 m51 = new m51(this, ju0);
                if (!((zk0) ju0).t) {
                    ((zk0) ju0).d.add(m51);
                }
                f91 f91 = new f91(he0, this, ju0);
                if (!((zk0) ju0).t) {
                    ((zk0) ju0).h.add(f91);
                }
                ju0.a(new yh1(he0, this, ju0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                ju0.l();
            }
        }
        return he0;
    }

    @DexIgnore
    @Override // com.fossil.fe0
    public he0<FitnessData[]> a(c80 c80) {
        he0<FitnessData[]> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        c80.a(2);
        dn0 dn0 = new dn0(this.c, this.a, c80, new HashMap());
        synchronized (this.q) {
            p60 a2 = a(dn0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                dn0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new ry0(this, dn0));
                i01 i01 = new i01(this, dn0);
                if (!((zk0) dn0).t) {
                    ((zk0) dn0).d.add(i01);
                }
                w31 w31 = new w31(he0, this, dn0);
                if (!((zk0) dn0).t) {
                    ((zk0) dn0).h.add(w31);
                }
                dn0.a(new wu0(he0, this, dn0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                dn0.l();
            }
        }
        he0.f(ow0.a);
        return he0;
    }

    @DexIgnore
    public final void a(WorkoutSessionManager workoutSessionManager) {
        this.s = workoutSessionManager;
    }

    @DexIgnore
    public final void a(l60.c cVar) {
        l60.c cVar2;
        synchronized (this.u) {
            cVar2 = this.u;
            this.u = cVar;
            i97 i97 = i97.a;
        }
        if (cVar2 != cVar) {
            wl0.h.a(new wr1(yz0.a(aw0.d), ci1.h, this.c.u, "", "", true, null, null, null, yz0.a(yz0.a(new JSONObject(), r51.A0, yz0.a(cVar2)), r51.z0, yz0.a(cVar)), 448));
            l60.c cVar3 = this.u;
            le0 le0 = le0.DEBUG;
            Intent intent = new Intent();
            intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE", cVar2);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE", cVar3);
            Context a2 = u31.g.a();
            if (a2 != null) {
                qe.a(a2).a(intent);
            }
            Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                myLooper = Looper.getMainLooper();
            }
            if (myLooper != null) {
                new Handler(myLooper).post(new r81(this, cVar2, cVar3));
                int i2 = kz0.c[cVar.ordinal()];
                if (i2 == 1) {
                    w();
                    this.f.b(is0.CONNECTION_DROPPED, new wm0[]{wm0.f});
                    this.f.a(new wj0(new Hashtable(), null));
                    this.d = false;
                } else if (i2 == 2) {
                    w();
                } else if (i2 == 3) {
                    m60 m60 = this.t;
                    yj0.e.a(m60.getMacAddress(), m60);
                    this.f.d();
                } else if (i2 == 5) {
                    w();
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(bb0 bb0) {
        l60.b bVar = this.v;
        if (bVar != null) {
            bVar.onEventReceived(this, bb0);
        }
    }

    @DexIgnore
    public static final /* synthetic */ boolean a(km1 km1) {
        T t2;
        boolean z;
        if (km1.c.t == dd1.CONNECTED) {
            Iterator<T> it = km1.f.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (((zk0) t2).y == wm0.f) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final ie0<i97> a(xp0 xp0) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        xp0.a(2);
        hj1 hj1 = new hj1(this.c, this.a, xp0);
        synchronized (this.q) {
            p60 a2 = a(hj1);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                hj1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new py0(this, hj1));
                yc1 yc1 = new yc1(this, hj1);
                if (!((zk0) hj1).t) {
                    ((zk0) hj1).d.add(yc1);
                }
                vp0 vp0 = new vp0(he0, this, hj1);
                if (!((zk0) hj1).t) {
                    ((zk0) hj1).h.add(vp0);
                }
                hj1.a(new l11(he0, this, hj1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                hj1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public static /* synthetic */ he0 a(km1 km1, boolean z, boolean z2, a61 a61, int i2) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            if (z2) {
                a61 = a61.LOW;
            } else {
                a61 = a61.NORMAL;
            }
        }
        return km1.a(z, z2, a61);
    }

    @DexIgnore
    public final he0<byte[][]> a(boolean z, boolean z2, a61 a61) {
        no0 no0;
        he0<byte[][]> he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        if (z2) {
            no0 = new rm0(this.c, this.a, oa7.b(w87.a(xf0.SKIP_ERASE, false), w87.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS, true)));
        } else {
            no0 no02 = new no0(this.c, this.a, wm0.o0, oa7.b(w87.a(xf0.SKIP_ERASE, Boolean.valueOf(z)), w87.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z))), yh0.a("UUID.randomUUID().toString()"));
            no02.a(a61);
            no0 = no02;
        }
        synchronized (this.q) {
            p60 a2 = a(no0);
            he0 = new he0<>();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                no0.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new yn1(this, no0));
                gt0 gt0 = new gt0(this, no0);
                if (!((zk0) no0).t) {
                    ((zk0) no0).d.add(gt0);
                }
                ab1 ab1 = new ab1(he0, this, no0);
                if (!((zk0) no0).t) {
                    ((zk0) no0).h.add(ab1);
                }
                no0.a(new sq0(he0, this, no0));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                no0.l();
            }
        }
        he0.f(ns0.a);
        return he0;
    }

    @DexIgnore
    public final ie0<i97> a(HashMap<d81, Object> hashMap) {
        he0 he0;
        is0 is0;
        le0 le0 = le0.DEBUG;
        uo1 uo1 = new uo1(this.c, this.a, hashMap);
        synchronized (this.q) {
            p60 a2 = a(uo1);
            he0 = new he0();
            if (a2 != null) {
                if (kz0.d[a2.ordinal()] != 1) {
                    is0 = is0.NOT_ALLOW_TO_START;
                } else {
                    is0 = is0.BLUETOOTH_OFF;
                }
                uo1.a(is0);
                he0.b(new o60(a2, null, 2));
            } else {
                he0.a((gd7<? super je0, i97>) new y11(this, uo1));
                fk1 fk1 = new fk1(this, uo1);
                if (!((zk0) uo1).t) {
                    ((zk0) uo1).d.add(fk1);
                }
                xw0 xw0 = new xw0(he0, this, uo1);
                if (!((zk0) uo1).t) {
                    ((zk0) uo1).h.add(xw0);
                }
                uo1.a(new ij0(he0, this, uo1));
                if (this.u != l60.c.CONNECTED && !this.d) {
                    yp0.f.f();
                    b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L)));
                }
                uo1.l();
            }
        }
        return he0;
    }

    @DexIgnore
    public final void a(l60.d dVar, l60.d dVar2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE", dVar);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE", dVar2);
        Context a2 = u31.g.a();
        if (a2 != null) {
            qe.a(a2).a(intent);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.eu0 r4) {
        /*
            r3 = this;
            com.fossil.is0 r0 = r4.b
            com.fossil.is0 r1 = com.fossil.is0.REQUEST_ERROR
            r2 = 1
            if (r0 != r1) goto L_0x0025
            com.fossil.sz0 r4 = r4.c
            com.fossil.ay0 r0 = r4.c
            com.fossil.ay0 r1 = com.fossil.ay0.c
            if (r0 != r1) goto L_0x0025
            com.fossil.lk0 r4 = r4.d
            com.fossil.oi0 r0 = r4.b
            com.fossil.oi0 r1 = com.fossil.oi0.c
            if (r0 != r1) goto L_0x0025
            com.fossil.x71 r4 = r4.c
            com.fossil.z51 r4 = r4.a
            com.fossil.z51 r0 = com.fossil.z51.GATT_NULL
            if (r4 == r0) goto L_0x0023
            com.fossil.z51 r0 = com.fossil.z51.START_FAIL
            if (r4 != r0) goto L_0x0025
        L_0x0023:
            r4 = 1
            goto L_0x0026
        L_0x0025:
            r4 = 0
        L_0x0026:
            if (r2 != r4) goto L_0x002b
            r3.v()
        L_0x002b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.km1.a(com.fossil.eu0):void");
    }

    @DexIgnore
    public final p60 a(zk0 zk0) {
        T t2;
        boolean z;
        if (f60.k.d() != f60.c.ENABLED) {
            return p60.BLUETOOTH_OFF;
        }
        int i2 = kz0.e[zk0.y.ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            return null;
        }
        if (i2 == 5) {
            ArrayList<zk0> a2 = this.f.a();
            if (a2.isEmpty()) {
                return null;
            }
            Iterator<T> it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                T t3 = t2;
                wm0 wm0 = ((zk0) t3).y;
                if (wm0 == wm0.f || wm0 == wm0.b || t3.e().compareTo((Enum) a61.LOW) <= 0) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t2 == null) {
                return null;
            }
            return p60.DEVICE_BUSY;
        } else if (this.u != l60.c.UPGRADING_FIRMWARE) {
            return null;
        } else {
            return p60.DEVICE_BUSY;
        }
    }
}
