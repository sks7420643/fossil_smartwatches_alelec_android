package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.do5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb6 extends go5 implements sb6 {
    @DexIgnore
    public xz6 f;
    @DexIgnore
    public qw6<s15> g;
    @DexIgnore
    public rb6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tb6 a;

        @DexIgnore
        public b(tb6 tb6, s15 s15) {
            this.a = tb6;
        }

        @DexIgnore
        public final void onClick(View view) {
            tb6.a(this.a).a().a((Integer) 2);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ xz6 a(tb6 tb6) {
        xz6 xz6 = tb6.f;
        if (xz6 != null) {
            return xz6;
        }
        ee7.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.sb6
    public void b(boolean z) {
        s15 a2;
        qw6<s15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                OverviewWeekChart overviewWeekChart = a2.v;
                ee7.a((Object) overviewWeekChart, "binding.weekChart");
                overviewWeekChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                ee7.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewWeekChart overviewWeekChart2 = a2.v;
            ee7.a((Object) overviewWeekChart2, "binding.weekChart");
            overviewWeekChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            ee7.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "GoalTrackingOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        s15 a2;
        OverviewWeekChart overviewWeekChart;
        qw6<s15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewWeekChart = a2.v) != null) {
            overviewWeekChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        s15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onCreateView");
        s15 s15 = (s15) qb.a(layoutInflater, 2131558561, viewGroup, false, a1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            he a3 = je.a(activity).a(xz6.class);
            ee7.a((Object) a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = (xz6) a3;
            s15.r.setOnClickListener(new b(this, s15));
        }
        this.g = new qw6<>(this, s15);
        f1();
        qw6<s15> qw6 = this.g;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onResume");
        f1();
        rb6 rb6 = this.h;
        if (rb6 != null) {
            rb6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onStop");
        rb6 rb6 = this.h;
        if (rb6 != null) {
            rb6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.sb6
    public void a(do5 do5) {
        s15 a2;
        OverviewWeekChart overviewWeekChart;
        ee7.b(do5, "baseModel");
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "showWeekDetails");
        qw6<s15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewWeekChart = a2.v) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            do5.a aVar = do5.a;
            ee7.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            ee7.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(do5);
        }
    }

    @DexIgnore
    public void a(rb6 rb6) {
        ee7.b(rb6, "presenter");
        this.h = rb6;
    }
}
