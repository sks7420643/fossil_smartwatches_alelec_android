package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ x90[] a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ug0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ug0 createFromParcel(Parcel parcel) {
            Object[] createTypedArray = parcel.createTypedArray(x90.CREATOR);
            if (createTypedArray != null) {
                ee7.a((Object) createTypedArray, "parcel.createTypedArray(\u2026tificationReplyMessage)!!");
                return new ug0((x90[]) createTypedArray);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ug0[] newArray(int i) {
            return new ug0[i];
        }
    }

    @DexIgnore
    public ug0(x90[] x90Arr) {
        Object[] copyOf = Arrays.copyOf(x90Arr, x90Arr.length);
        ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : copyOf) {
            if (hashSet.add(Byte.valueOf(((x90) obj).getMessageId()))) {
                arrayList.add(obj);
            }
        }
        Object[] array = ea7.a((Iterable) arrayList.subList(0, Math.min(10, arrayList.size())), (Comparator) new p51()).toArray(new x90[0]);
        if (array != null) {
            this.a = (x90[]) array;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (x90 x90 : this.a) {
                byteArrayOutputStream.write(x90.c());
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            ee7.a((Object) byteArray, "entriesData.toByteArray()");
            this.b = byteArray;
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(new JSONObject(), r51.m, yz0.a(this.a));
    }

    @DexIgnore
    public final byte[] b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ug0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.a, ((ug0) obj).a);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageGroup");
    }

    @DexIgnore
    public final x90[] getReplyMessages() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return q97.a(this.a);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.a, i);
    }
}
