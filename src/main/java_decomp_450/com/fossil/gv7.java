package com.fossil;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gv7<T> {
    @DexIgnore
    public static <T> gv7<T> a(Retrofit retrofit3, Method method) {
        ev7 a = ev7.a(retrofit3, method);
        Type genericReturnType = method.getGenericReturnType();
        if (jv7.c(genericReturnType)) {
            throw jv7.a(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
        } else if (genericReturnType != Void.TYPE) {
            return wu7.a(retrofit3, method, a);
        } else {
            throw jv7.a(method, "Service methods cannot return void.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract T a(Object[] objArr);
}
