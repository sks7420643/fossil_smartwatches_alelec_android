package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.we;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xe {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends we.b {
        @DexIgnore
        public a(Context context, b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((b) ((we.b) this).a).a(str, new we.c<>(result));
        }
    }

    @DexIgnore
    public interface b extends we.d {
        @DexIgnore
        void a(String str, we.c<Parcel> cVar);
    }

    @DexIgnore
    public static Object a(Context context, b bVar) {
        return new a(context, bVar);
    }
}
