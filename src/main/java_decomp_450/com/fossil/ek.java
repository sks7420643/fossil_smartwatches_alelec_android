package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ek {
    @DexIgnore
    public static Transition a; // = new AutoTransition();
    @DexIgnore
    public static ThreadLocal<WeakReference<n4<ViewGroup, ArrayList<Transition>>>> b; // = new ThreadLocal<>();
    @DexIgnore
    public static ArrayList<ViewGroup> c; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public Transition a;
        @DexIgnore
        public ViewGroup b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ek$a$a")
        /* renamed from: com.fossil.ek$a$a  reason: collision with other inner class name */
        public class C0057a extends dk {
            @DexIgnore
            public /* final */ /* synthetic */ n4 a;

            @DexIgnore
            public C0057a(n4 n4Var) {
                this.a = n4Var;
            }

            @DexIgnore
            @Override // androidx.transition.Transition.f
            public void c(Transition transition) {
                ((ArrayList) this.a.get(a.this.b)).remove(transition);
                transition.b(this);
            }
        }

        @DexIgnore
        public a(Transition transition, ViewGroup viewGroup) {
            this.a = transition;
            this.b = viewGroup;
        }

        @DexIgnore
        public final void a() {
            this.b.getViewTreeObserver().removeOnPreDrawListener(this);
            this.b.removeOnAttachStateChangeListener(this);
        }

        @DexIgnore
        public boolean onPreDraw() {
            a();
            if (!ek.c.remove(this.b)) {
                return true;
            }
            n4<ViewGroup, ArrayList<Transition>> a2 = ek.a();
            ArrayList<Transition> arrayList = a2.get(this.b);
            ArrayList arrayList2 = null;
            if (arrayList == null) {
                arrayList = new ArrayList<>();
                a2.put(this.b, arrayList);
            } else if (arrayList.size() > 0) {
                arrayList2 = new ArrayList(arrayList);
            }
            arrayList.add(this.a);
            this.a.a(new C0057a(a2));
            this.a.a(this.b, false);
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).e(this.b);
                }
            }
            this.a.a(this.b);
            return true;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            a();
            ek.c.remove(this.b);
            ArrayList<Transition> arrayList = ek.a().get(this.b);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator<Transition> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().e(this.b);
                }
            }
            this.a.a(true);
        }
    }

    @DexIgnore
    public static n4<ViewGroup, ArrayList<Transition>> a() {
        n4<ViewGroup, ArrayList<Transition>> n4Var;
        WeakReference<n4<ViewGroup, ArrayList<Transition>>> weakReference = b.get();
        if (weakReference != null && (n4Var = weakReference.get()) != null) {
            return n4Var;
        }
        n4<ViewGroup, ArrayList<Transition>> n4Var2 = new n4<>();
        b.set(new WeakReference<>(n4Var2));
        return n4Var2;
    }

    @DexIgnore
    public static void b(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            a aVar = new a(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, Transition transition) {
        ArrayList<Transition> arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<Transition> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().c(viewGroup);
            }
        }
        if (transition != null) {
            transition.a(viewGroup, true);
        }
        ak a2 = ak.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, Transition transition) {
        if (!c.contains(viewGroup) && da.G(viewGroup)) {
            c.add(viewGroup);
            if (transition == null) {
                transition = a;
            }
            Transition clone = transition.clone();
            c(viewGroup, clone);
            ak.a(viewGroup, null);
            b(viewGroup, clone);
        }
    }
}
