package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko1 implements Parcelable.Creator<kq1> {
    @DexIgnore
    public /* synthetic */ ko1(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public kq1 createFromParcel(Parcel parcel) {
        return new kq1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public kq1[] newArray(int i) {
        return new kq1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public kq1 m36createFromParcel(Parcel parcel) {
        return new kq1(parcel, null);
    }
}
