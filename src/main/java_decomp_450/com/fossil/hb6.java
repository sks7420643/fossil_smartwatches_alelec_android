package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb6 {
    @DexIgnore
    public /* final */ bb6 a;
    @DexIgnore
    public /* final */ sb6 b;
    @DexIgnore
    public /* final */ mb6 c;

    @DexIgnore
    public hb6(bb6 bb6, sb6 sb6, mb6 mb6) {
        ee7.b(bb6, "mGoalTrackingOverviewDayView");
        ee7.b(sb6, "mGoalTrackingOverviewWeekView");
        ee7.b(mb6, "mGoalTrackingOverviewMonthView");
        this.a = bb6;
        this.b = sb6;
        this.c = mb6;
    }

    @DexIgnore
    public final bb6 a() {
        return this.a;
    }

    @DexIgnore
    public final mb6 b() {
        return this.c;
    }

    @DexIgnore
    public final sb6 c() {
        return this.b;
    }
}
