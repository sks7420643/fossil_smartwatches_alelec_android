package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.SharedElementCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oc {
    @DexIgnore
    public static /* final */ int[] a; // = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    @DexIgnore
    public static /* final */ qc b; // = (Build.VERSION.SDK_INT >= 21 ? new pc() : null);
    @DexIgnore
    public static /* final */ qc c; // = a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ g a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ d8 c;

        @DexIgnore
        public a(g gVar, Fragment fragment, d8 d8Var) {
            this.a = gVar;
            this.b = fragment;
            this.c = d8Var;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList a;

        @DexIgnore
        public b(ArrayList arrayList) {
            this.a = arrayList;
        }

        @DexIgnore
        public void run() {
            oc.a(this.a, 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ g a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ d8 c;

        @DexIgnore
        public c(g gVar, Fragment fragment, d8 d8Var) {
            this.a = gVar;
            this.b = fragment;
            this.c = d8Var;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;
        @DexIgnore
        public /* final */ /* synthetic */ qc b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList g;
        @DexIgnore
        public /* final */ /* synthetic */ Object h;

        @DexIgnore
        public d(Object obj, qc qcVar, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.a = obj;
            this.b = qcVar;
            this.c = view;
            this.d = fragment;
            this.e = arrayList;
            this.f = arrayList2;
            this.g = arrayList3;
            this.h = obj2;
        }

        @DexIgnore
        public void run() {
            Object obj = this.a;
            if (obj != null) {
                this.b.b(obj, this.c);
                this.f.addAll(oc.a(this.b, this.a, this.d, this.e, this.c));
            }
            if (this.g != null) {
                if (this.h != null) {
                    ArrayList<View> arrayList = new ArrayList<>();
                    arrayList.add(this.c);
                    this.b.a(this.h, this.g, arrayList);
                }
                this.g.clear();
                this.g.add(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ n4 d;
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ qc f;
        @DexIgnore
        public /* final */ /* synthetic */ Rect g;

        @DexIgnore
        public e(Fragment fragment, Fragment fragment2, boolean z, n4 n4Var, View view, qc qcVar, Rect rect) {
            this.a = fragment;
            this.b = fragment2;
            this.c = z;
            this.d = n4Var;
            this.e = view;
            this.f = qcVar;
            this.g = rect;
        }

        @DexIgnore
        public void run() {
            oc.a(this.a, this.b, this.c, (n4<String, View>) this.d, false);
            View view = this.e;
            if (view != null) {
                this.f.a(view, this.g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ qc a;
        @DexIgnore
        public /* final */ /* synthetic */ n4 b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;
        @DexIgnore
        public /* final */ /* synthetic */ h d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ View f;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment g;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList j;
        @DexIgnore
        public /* final */ /* synthetic */ Object p;
        @DexIgnore
        public /* final */ /* synthetic */ Rect q;

        @DexIgnore
        public f(qc qcVar, n4 n4Var, Object obj, h hVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.a = qcVar;
            this.b = n4Var;
            this.c = obj;
            this.d = hVar;
            this.e = arrayList;
            this.f = view;
            this.g = fragment;
            this.h = fragment2;
            this.i = z;
            this.j = arrayList2;
            this.p = obj2;
            this.q = rect;
        }

        @DexIgnore
        public void run() {
            n4<String, View> a2 = oc.a(this.a, this.b, this.c, this.d);
            if (a2 != null) {
                this.e.addAll(a2.values());
                this.e.add(this.f);
            }
            oc.a(this.g, this.h, this.i, a2, false);
            Object obj = this.c;
            if (obj != null) {
                this.a.b(obj, this.j, this.e);
                View a3 = oc.a(a2, this.d, this.p, this.i);
                if (a3 != null) {
                    this.a.a(a3, this.q);
                }
            }
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(Fragment fragment, d8 d8Var);

        @DexIgnore
        void b(Fragment fragment, d8 d8Var);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public Fragment a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public yb c;
        @DexIgnore
        public Fragment d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public yb f;
    }

    @DexIgnore
    public static qc a() {
        try {
            return (qc) Class.forName("com.fossil.nj").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static void b(FragmentManager fragmentManager, int i, h hVar, View view, n4<String, String> n4Var, g gVar) {
        Fragment fragment;
        Fragment fragment2;
        qc a2;
        Object obj;
        ViewGroup viewGroup = fragmentManager.p.a() ? (ViewGroup) fragmentManager.p.a(i) : null;
        if (viewGroup != null && (a2 = a((fragment2 = hVar.d), (fragment = hVar.a))) != null) {
            boolean z = hVar.b;
            boolean z2 = hVar.e;
            ArrayList<View> arrayList = new ArrayList<>();
            ArrayList<View> arrayList2 = new ArrayList<>();
            Object a3 = a(a2, fragment, z);
            Object b2 = b(a2, fragment2, z2);
            Object b3 = b(a2, viewGroup, view, n4Var, hVar, arrayList2, arrayList, a3, b2);
            if (a3 == null && b3 == null) {
                obj = b2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = b2;
            }
            ArrayList<View> a4 = a(a2, obj, fragment2, arrayList2, view);
            ArrayList<View> a5 = a(a2, a3, fragment, arrayList, view);
            a(a5, 4);
            Object a6 = a(a2, a3, obj, b3, fragment, z);
            if (!(fragment2 == null || a4 == null || (a4.size() <= 0 && arrayList2.size() <= 0))) {
                d8 d8Var = new d8();
                gVar.b(fragment2, d8Var);
                a2.a(fragment2, a6, d8Var, new a(gVar, fragment2, d8Var));
            }
            if (a6 != null) {
                a(a2, obj, fragment2, a4);
                ArrayList<String> a7 = a2.a(arrayList);
                a2.a(a6, a3, a5, obj, a4, b3, arrayList);
                a2.a(viewGroup, a6);
                a2.a(viewGroup, arrayList2, arrayList, a7, n4Var);
                a(a5, 0);
                a2.b(b3, arrayList2, arrayList);
            }
        }
    }

    @DexIgnore
    public static void a(FragmentManager fragmentManager, ArrayList<yb> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z, g gVar) {
        if (fragmentManager.n >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                yb ybVar = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    b(ybVar, sparseArray, z);
                } else {
                    a(ybVar, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(fragmentManager.o.c());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    n4<String, String> a2 = a(keyAt, arrayList, arrayList2, i, i2);
                    h hVar = (h) sparseArray.valueAt(i4);
                    if (z) {
                        b(fragmentManager, keyAt, hVar, view, a2, gVar);
                    } else {
                        a(fragmentManager, keyAt, hVar, view, a2, gVar);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static n4<String, String> a(int i, ArrayList<yb> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        n4<String, String> n4Var = new n4<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            yb ybVar = arrayList.get(i4);
            if (ybVar.b(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = ((nc) ybVar).n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = ((nc) ybVar).n;
                        arrayList4 = ((nc) ybVar).o;
                    } else {
                        ArrayList<String> arrayList6 = ((nc) ybVar).n;
                        arrayList3 = ((nc) ybVar).o;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = n4Var.remove(str2);
                        if (remove != null) {
                            n4Var.put(str, remove);
                        } else {
                            n4Var.put(str, str2);
                        }
                    }
                }
            }
        }
        return n4Var;
    }

    @DexIgnore
    public static Object b(qc qcVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return qcVar.b(obj);
    }

    @DexIgnore
    public static Object b(qc qcVar, ViewGroup viewGroup, View view, n4<String, String> n4Var, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        Rect rect;
        View view2;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar.b;
        if (n4Var.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = a(qcVar, fragment, fragment2, z);
        }
        n4<String, View> b2 = b(qcVar, n4Var, obj3, hVar);
        n4<String, View> a2 = a(qcVar, n4Var, obj3, hVar);
        if (n4Var.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (a2 != null) {
                a2.clear();
            }
            obj4 = null;
        } else {
            a(arrayList, b2, n4Var.keySet());
            a(arrayList2, a2, n4Var.values());
            obj4 = obj3;
        }
        if (obj == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            arrayList2.add(view);
            qcVar.b(obj4, view, arrayList);
            a(qcVar, obj4, obj2, b2, hVar.e, hVar.f);
            Rect rect2 = new Rect();
            View a3 = a(a2, hVar, obj, z);
            if (a3 != null) {
                qcVar.a(obj, rect2);
            }
            rect = rect2;
            view2 = a3;
        } else {
            view2 = null;
            rect = null;
        }
        aa.a(viewGroup, new e(fragment, fragment2, z, a2, view2, qcVar, rect));
        return obj4;
    }

    @DexIgnore
    public static void a(qc qcVar, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            qcVar.a(obj, fragment.getView(), arrayList);
            aa.a(fragment.mContainer, new b(arrayList));
        }
    }

    @DexIgnore
    public static void a(FragmentManager fragmentManager, int i, h hVar, View view, n4<String, String> n4Var, g gVar) {
        Fragment fragment;
        Fragment fragment2;
        qc a2;
        Object obj;
        ViewGroup viewGroup = fragmentManager.p.a() ? (ViewGroup) fragmentManager.p.a(i) : null;
        if (viewGroup != null && (a2 = a((fragment2 = hVar.d), (fragment = hVar.a))) != null) {
            boolean z = hVar.b;
            boolean z2 = hVar.e;
            Object a3 = a(a2, fragment, z);
            Object b2 = b(a2, fragment2, z2);
            ArrayList arrayList = new ArrayList();
            ArrayList<View> arrayList2 = new ArrayList<>();
            Object a4 = a(a2, viewGroup, view, n4Var, hVar, arrayList, arrayList2, a3, b2);
            if (a3 == null && a4 == null) {
                obj = b2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = b2;
            }
            ArrayList<View> a5 = a(a2, obj, fragment2, arrayList, view);
            Object obj2 = (a5 == null || a5.isEmpty()) ? null : obj;
            a2.a(a3, view);
            Object a6 = a(a2, a3, obj2, a4, fragment, hVar.b);
            if (!(fragment2 == null || a5 == null || (a5.size() <= 0 && arrayList.size() <= 0))) {
                d8 d8Var = new d8();
                gVar.b(fragment2, d8Var);
                a2.a(fragment2, a6, d8Var, new c(gVar, fragment2, d8Var));
            }
            if (a6 != null) {
                ArrayList<View> arrayList3 = new ArrayList<>();
                a2.a(a6, a3, arrayList3, obj2, a5, a4, arrayList2);
                a(a2, viewGroup, fragment, view, arrayList2, a3, arrayList3, obj2, a5);
                a2.a((View) viewGroup, arrayList2, (Map<String, String>) n4Var);
                a2.a(viewGroup, a6);
                a2.a(viewGroup, arrayList2, (Map<String, String>) n4Var);
            }
        }
    }

    @DexIgnore
    public static n4<String, View> b(qc qcVar, n4<String, String> n4Var, Object obj, h hVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        if (n4Var.isEmpty() || obj == null) {
            n4Var.clear();
            return null;
        }
        Fragment fragment = hVar.d;
        n4<String, View> n4Var2 = new n4<>();
        qcVar.a((Map<String, View>) n4Var2, fragment.requireView());
        yb ybVar = hVar.f;
        if (hVar.e) {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = ((nc) ybVar).o;
        } else {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = ((nc) ybVar).n;
        }
        if (arrayList != null) {
            n4Var2.a(arrayList);
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a(arrayList, n4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = n4Var2.get(str);
                if (view == null) {
                    n4Var.remove(str);
                } else if (!str.equals(da.x(view))) {
                    n4Var.put(da.x(view), n4Var.remove(str));
                }
            }
        } else {
            n4Var.a(n4Var2.keySet());
        }
        return n4Var2;
    }

    @DexIgnore
    public static void a(qc qcVar, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        aa.a(viewGroup, new d(obj, qcVar, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }

    @DexIgnore
    public static qc a(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        qc qcVar = b;
        if (qcVar != null && a(qcVar, arrayList)) {
            return b;
        }
        qc qcVar2 = c;
        if (qcVar2 != null && a(qcVar2, arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    @DexIgnore
    public static void b(yb ybVar, SparseArray<h> sparseArray, boolean z) {
        if (ybVar.r.p.a()) {
            for (int size = ((nc) ybVar).a.size() - 1; size >= 0; size--) {
                a(ybVar, ((nc) ybVar).a.get(size), sparseArray, true, z);
            }
        }
    }

    @DexIgnore
    public static boolean a(qc qcVar, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!qcVar.a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static Object a(qc qcVar, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return qcVar.c(qcVar.b(obj));
    }

    @DexIgnore
    public static Object a(qc qcVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return qcVar.b(obj);
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, n4<String, View> n4Var, Collection<String> collection) {
        for (int size = n4Var.size() - 1; size >= 0; size--) {
            View e2 = n4Var.e(size);
            if (collection.contains(da.x(e2))) {
                arrayList.add(e2);
            }
        }
    }

    @DexIgnore
    public static Object a(qc qcVar, ViewGroup viewGroup, View view, n4<String, String> n4Var, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        n4<String, String> n4Var2;
        Object obj3;
        Object obj4;
        Rect rect;
        Fragment fragment = hVar.a;
        Fragment fragment2 = hVar.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar.b;
        if (n4Var.isEmpty()) {
            n4Var2 = n4Var;
            obj3 = null;
        } else {
            obj3 = a(qcVar, fragment, fragment2, z);
            n4Var2 = n4Var;
        }
        n4<String, View> b2 = b(qcVar, n4Var2, obj3, hVar);
        if (n4Var.isEmpty()) {
            obj4 = null;
        } else {
            arrayList.addAll(b2.values());
            obj4 = obj3;
        }
        if (obj == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            rect = new Rect();
            qcVar.b(obj4, view, arrayList);
            a(qcVar, obj4, obj2, b2, hVar.e, hVar.f);
            if (obj != null) {
                qcVar.a(obj, rect);
            }
        } else {
            rect = null;
        }
        aa.a(viewGroup, new f(qcVar, n4Var, obj4, hVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect));
        return obj4;
    }

    @DexIgnore
    public static n4<String, View> a(qc qcVar, n4<String, String> n4Var, Object obj, h hVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        String a2;
        Fragment fragment = hVar.a;
        View view = fragment.getView();
        if (n4Var.isEmpty() || obj == null || view == null) {
            n4Var.clear();
            return null;
        }
        n4<String, View> n4Var2 = new n4<>();
        qcVar.a((Map<String, View>) n4Var2, view);
        yb ybVar = hVar.c;
        if (hVar.b) {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = ((nc) ybVar).n;
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = ((nc) ybVar).o;
        }
        if (arrayList != null) {
            n4Var2.a(arrayList);
            n4Var2.a(n4Var.values());
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a(arrayList, n4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = n4Var2.get(str);
                if (view2 == null) {
                    String a3 = a(n4Var, str);
                    if (a3 != null) {
                        n4Var.remove(a3);
                    }
                } else if (!str.equals(da.x(view2)) && (a2 = a(n4Var, str)) != null) {
                    n4Var.put(a2, da.x(view2));
                }
            }
        } else {
            a(n4Var, n4Var2);
        }
        return n4Var2;
    }

    @DexIgnore
    public static String a(n4<String, String> n4Var, String str) {
        int size = n4Var.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(n4Var.e(i))) {
                return n4Var.c(i);
            }
        }
        return null;
    }

    @DexIgnore
    public static View a(n4<String, View> n4Var, h hVar, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        yb ybVar = hVar.c;
        if (obj == null || n4Var == null || (arrayList = ((nc) ybVar).n) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = ((nc) ybVar).n.get(0);
        } else {
            str = ((nc) ybVar).o.get(0);
        }
        return n4Var.get(str);
    }

    @DexIgnore
    public static void a(qc qcVar, Object obj, Object obj2, n4<String, View> n4Var, boolean z, yb ybVar) {
        String str;
        ArrayList<String> arrayList = ((nc) ybVar).n;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = ((nc) ybVar).o.get(0);
            } else {
                str = ((nc) ybVar).n.get(0);
            }
            View view = n4Var.get(str);
            qcVar.c(obj, view);
            if (obj2 != null) {
                qcVar.c(obj2, view);
            }
        }
    }

    @DexIgnore
    public static void a(n4<String, String> n4Var, n4<String, View> n4Var2) {
        for (int size = n4Var.size() - 1; size >= 0; size--) {
            if (!n4Var2.containsKey(n4Var.e(size))) {
                n4Var.d(size);
            }
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, Fragment fragment2, boolean z, n4<String, View> n4Var, boolean z2) {
        SharedElementCallback sharedElementCallback;
        int i;
        if (z) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (n4Var == null) {
                i = 0;
            } else {
                i = n4Var.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(n4Var.c(i2));
                arrayList.add(n4Var.e(i2));
            }
            if (z2) {
                sharedElementCallback.b(arrayList2, arrayList, null);
            } else {
                sharedElementCallback.a(arrayList2, arrayList, (List<View>) null);
            }
        }
    }

    @DexIgnore
    public static ArrayList<View> a(qc qcVar, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            qcVar.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        qcVar.a(obj, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    @DexIgnore
    public static Object a(qc qcVar, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return qcVar.b(obj2, obj, obj3);
        }
        return qcVar.a(obj2, obj, obj3);
    }

    @DexIgnore
    public static void a(yb ybVar, SparseArray<h> sparseArray, boolean z) {
        int size = ((nc) ybVar).a.size();
        for (int i = 0; i < size; i++) {
            a(ybVar, ((nc) ybVar).a.get(i), sparseArray, false, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        if (r0.mAdded != false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x006e, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x008a, code lost:
        if (r0.mHidden == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x008c, code lost:
        r9 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00d9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.fossil.yb r8, com.fossil.nc.a r9, android.util.SparseArray<com.fossil.oc.h> r10, boolean r11, boolean r12) {
        /*
            androidx.fragment.app.Fragment r0 = r9.b
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            int r1 = r0.mContainerId
            if (r1 != 0) goto L_0x000a
            return
        L_0x000a:
            if (r11 == 0) goto L_0x0013
            int[] r2 = com.fossil.oc.a
            int r9 = r9.a
            r9 = r2[r9]
            goto L_0x0015
        L_0x0013:
            int r9 = r9.a
        L_0x0015:
            r2 = 0
            r3 = 1
            if (r9 == r3) goto L_0x007f
            r4 = 3
            if (r9 == r4) goto L_0x0057
            r4 = 4
            if (r9 == r4) goto L_0x003f
            r4 = 5
            if (r9 == r4) goto L_0x002d
            r4 = 6
            if (r9 == r4) goto L_0x0057
            r4 = 7
            if (r9 == r4) goto L_0x007f
            r9 = 0
        L_0x0029:
            r4 = 0
            r5 = 0
            goto L_0x0092
        L_0x002d:
            if (r12 == 0) goto L_0x003c
            boolean r9 = r0.mHiddenChanged
            if (r9 == 0) goto L_0x008e
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x008e
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x008e
            goto L_0x008c
        L_0x003c:
            boolean r9 = r0.mHidden
            goto L_0x008f
        L_0x003f:
            if (r12 == 0) goto L_0x004e
            boolean r9 = r0.mHiddenChanged
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 == 0) goto L_0x0070
        L_0x004d:
            goto L_0x006e
        L_0x004e:
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x0070
            goto L_0x004d
        L_0x0057:
            if (r12 == 0) goto L_0x0072
            boolean r9 = r0.mAdded
            if (r9 != 0) goto L_0x0070
            android.view.View r9 = r0.mView
            if (r9 == 0) goto L_0x0070
            int r9 = r9.getVisibility()
            if (r9 != 0) goto L_0x0070
            float r9 = r0.mPostponedAlpha
            r4 = 0
            int r9 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r9 < 0) goto L_0x0070
        L_0x006e:
            r9 = 1
            goto L_0x007b
        L_0x0070:
            r9 = 0
            goto L_0x007b
        L_0x0072:
            boolean r9 = r0.mAdded
            if (r9 == 0) goto L_0x0070
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x0070
            goto L_0x006e
        L_0x007b:
            r5 = r9
            r9 = 0
            r4 = 1
            goto L_0x0092
        L_0x007f:
            if (r12 == 0) goto L_0x0084
            boolean r9 = r0.mIsNewlyAdded
            goto L_0x008f
        L_0x0084:
            boolean r9 = r0.mAdded
            if (r9 != 0) goto L_0x008e
            boolean r9 = r0.mHidden
            if (r9 != 0) goto L_0x008e
        L_0x008c:
            r9 = 1
            goto L_0x008f
        L_0x008e:
            r9 = 0
        L_0x008f:
            r2 = r9
            r9 = 1
            goto L_0x0029
        L_0x0092:
            java.lang.Object r6 = r10.get(r1)
            com.fossil.oc$h r6 = (com.fossil.oc.h) r6
            if (r2 == 0) goto L_0x00a4
            com.fossil.oc$h r6 = a(r6, r10, r1)
            r6.a = r0
            r6.b = r11
            r6.c = r8
        L_0x00a4:
            r2 = 0
            if (r12 != 0) goto L_0x00c5
            if (r9 == 0) goto L_0x00c5
            if (r6 == 0) goto L_0x00b1
            androidx.fragment.app.Fragment r9 = r6.d
            if (r9 != r0) goto L_0x00b1
            r6.d = r2
        L_0x00b1:
            androidx.fragment.app.FragmentManager r9 = r8.r
            int r7 = r0.mState
            if (r7 >= r3) goto L_0x00c5
            int r7 = r9.n
            if (r7 < r3) goto L_0x00c5
            boolean r7 = r8.p
            if (r7 != 0) goto L_0x00c5
            r9.p(r0)
            r9.a(r0, r3)
        L_0x00c5:
            if (r5 == 0) goto L_0x00d7
            if (r6 == 0) goto L_0x00cd
            androidx.fragment.app.Fragment r9 = r6.d
            if (r9 != 0) goto L_0x00d7
        L_0x00cd:
            com.fossil.oc$h r6 = a(r6, r10, r1)
            r6.d = r0
            r6.e = r11
            r6.f = r8
        L_0x00d7:
            if (r12 != 0) goto L_0x00e3
            if (r4 == 0) goto L_0x00e3
            if (r6 == 0) goto L_0x00e3
            androidx.fragment.app.Fragment r8 = r6.a
            if (r8 != r0) goto L_0x00e3
            r6.a = r2
        L_0x00e3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oc.a(com.fossil.yb, com.fossil.nc$a, android.util.SparseArray, boolean, boolean):void");
    }

    @DexIgnore
    public static h a(h hVar, SparseArray<h> sparseArray, int i) {
        if (hVar != null) {
            return hVar;
        }
        h hVar2 = new h();
        sparseArray.put(i, hVar2);
        return hVar2;
    }
}
