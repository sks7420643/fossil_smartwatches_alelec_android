package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up5 extends rf<DailyHeartRateSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public DailyHeartRateSummary h;
    @DexIgnore
    public /* final */ PortfolioApp i;
    @DexIgnore
    public /* final */ vp5 j;
    @DexIgnore
    public /* final */ FragmentManager k;
    @DexIgnore
    public /* final */ go5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public String f;
        @DexIgnore
        public int g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, String str, String str2, int i, String str3, int i2, String str4) {
            ee7.b(str, "mDayOfWeek");
            ee7.b(str2, "mDayOfMonth");
            ee7.b(str3, "mDailyRestingUnit");
            ee7.b(str4, "mDailyMaxUnit");
            this.a = date;
            this.b = z;
            this.c = str;
            this.d = str2;
            this.e = i;
            this.f = str3;
            this.g = i2;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void c(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void d(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final Date e() {
            return this.a;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final String g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ b(java.util.Date r11, boolean r12, java.lang.String r13, java.lang.String r14, int r15, java.lang.String r16, int r17, java.lang.String r18, int r19, com.fossil.zd7 r20) {
            /*
                r10 = this;
                r0 = r19
                r1 = r0 & 1
                if (r1 == 0) goto L_0x0008
                r1 = 0
                goto L_0x0009
            L_0x0008:
                r1 = r11
            L_0x0009:
                r2 = r0 & 2
                r3 = 0
                if (r2 == 0) goto L_0x0010
                r2 = 0
                goto L_0x0011
            L_0x0010:
                r2 = r12
            L_0x0011:
                r4 = r0 & 4
                java.lang.String r5 = ""
                if (r4 == 0) goto L_0x0019
                r4 = r5
                goto L_0x001a
            L_0x0019:
                r4 = r13
            L_0x001a:
                r6 = r0 & 8
                if (r6 == 0) goto L_0x0020
                r6 = r5
                goto L_0x0021
            L_0x0020:
                r6 = r14
            L_0x0021:
                r7 = r0 & 16
                if (r7 == 0) goto L_0x0027
                r7 = 0
                goto L_0x0028
            L_0x0027:
                r7 = r15
            L_0x0028:
                r8 = r0 & 32
                if (r8 == 0) goto L_0x002e
                r8 = r5
                goto L_0x0030
            L_0x002e:
                r8 = r16
            L_0x0030:
                r9 = r0 & 64
                if (r9 == 0) goto L_0x0035
                goto L_0x0037
            L_0x0035:
                r3 = r17
            L_0x0037:
                r0 = r0 & 128(0x80, float:1.794E-43)
                if (r0 == 0) goto L_0x003c
                goto L_0x003e
            L_0x003c:
                r5 = r18
            L_0x003e:
                r11 = r10
                r12 = r1
                r13 = r2
                r14 = r4
                r15 = r6
                r16 = r7
                r17 = r8
                r18 = r3
                r19 = r5
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.up5.b.<init>(java.util.Date, boolean, java.lang.String, java.lang.String, int, java.lang.String, int, java.lang.String, int, com.fossil.zd7):void");
        }

        @DexIgnore
        public final void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void b(int i) {
            this.e = i;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final int d() {
            return this.e;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final int b() {
            return this.g;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.g = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ o95 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ up5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.d.j.a(a2);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(up5 up5, o95 o95, View view) {
            super(view);
            ee7.b(o95, "binding");
            ee7.b(view, "root");
            this.d = up5;
            this.b = o95;
            this.c = view;
            o95.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            b a2 = this.d.a(dailyHeartRateSummary);
            this.a = a2.e();
            FlexibleTextView flexibleTextView = this.b.t;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.g());
            FlexibleTextView flexibleTextView2 = this.b.s;
            ee7.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.f());
            if (a2.b() == 0 && a2.d() == 0) {
                ConstraintLayout constraintLayout = this.b.q;
                ee7.a((Object) constraintLayout, "binding.clContainer");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.x;
                ee7.a((Object) flexibleTextView3, "binding.ftvNoRecord");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                ee7.a((Object) constraintLayout2, "binding.clContainer");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.x;
                ee7.a((Object) flexibleTextView4, "binding.ftvNoRecord");
                flexibleTextView4.setVisibility(8);
                FlexibleTextView flexibleTextView5 = this.b.z;
                ee7.a((Object) flexibleTextView5, "binding.ftvRestingValue");
                flexibleTextView5.setText(String.valueOf(a2.d()));
                FlexibleTextView flexibleTextView6 = this.b.y;
                ee7.a((Object) flexibleTextView6, "binding.ftvRestingUnit");
                flexibleTextView6.setText(a2.c());
                FlexibleTextView flexibleTextView7 = this.b.w;
                ee7.a((Object) flexibleTextView7, "binding.ftvMaxValue");
                flexibleTextView7.setText(String.valueOf(a2.b()));
                FlexibleTextView flexibleTextView8 = this.b.v;
                ee7.a((Object) flexibleTextView8, "binding.ftvMaxUnit");
                flexibleTextView8.setText(a2.a());
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            ee7.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!a2.h());
            if (a2.h()) {
                this.b.r.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.s.setBackgroundColor(this.d.e);
                this.b.t.setTextColor(this.d.f);
                this.b.s.setTextColor(this.d.d);
                return;
            }
            this.b.r.setBackgroundColor(this.d.g);
            this.b.t.setBackgroundColor(this.d.g);
            this.b.s.setBackgroundColor(this.d.g);
            this.b.t.setTextColor(this.d.f);
            this.b.s.setTextColor(this.d.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            ee7.b(str, "mWeekly");
            ee7.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ q95 g;
        @DexIgnore
        public /* final */ /* synthetic */ up5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.e != null && this.a.f != null) {
                    vp5 c = this.a.h.j;
                    Date b = this.a.e;
                    if (b != null) {
                        Date a2 = this.a.f;
                        if (a2 != null) {
                            c.b(b, a2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public e(com.fossil.up5 r4, com.fossil.q95 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.ee7.b(r5, r0)
                r3.h = r4
                com.fossil.o95 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.ee7.a(r0, r1)
                android.view.View r1 = r5.d()
                java.lang.String r2 = "binding.root"
                com.fossil.ee7.a(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r4 = r5.q
                com.fossil.up5$e$a r5 = new com.fossil.up5$e$a
                r5.<init>(r3)
                r4.setOnClickListener(r5)
                return
            L_0x0029:
                com.fossil.ee7.a()
                r4 = 0
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.up5.e.<init>(com.fossil.up5, com.fossil.q95):void");
        }

        @DexIgnore
        @Override // com.fossil.up5.c
        public void a(DailyHeartRateSummary dailyHeartRateSummary) {
            d b = this.h.b(dailyHeartRateSummary);
            this.f = b.a();
            this.e = b.b();
            FlexibleTextView flexibleTextView = this.g.s;
            ee7.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(dailyHeartRateSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ up5 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(up5 up5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = up5;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            ee7.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.l.getId() + ", isAdded=" + this.a.l.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.k.b(this.a.l.d1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                nc b3 = this.a.k.b();
                b3.a(view.getId(), this.a.l, this.a.l.d1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                nc b4 = this.a.k.b();
                b4.d(b2);
                b4.d();
                nc b5 = this.a.k.b();
                b5.a(view.getId(), this.a.l, this.a.l.d1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardHeartRatesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.l.getId() + ", isAdded2=" + this.a.l.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ee7.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public up5(tp5 tp5, PortfolioApp portfolioApp, vp5 vp5, FragmentManager fragmentManager, go5 go5) {
        super(tp5);
        ee7.b(tp5, "dailyHeartRateSummaryDifference");
        ee7.b(portfolioApp, "mApp");
        ee7.b(vp5, "mOnItemClick");
        ee7.b(fragmentManager, "mFragmentManager");
        ee7.b(go5, "mFragment");
        this.i = portfolioApp;
        this.j = vp5;
        this.k = fragmentManager;
        this.l = go5;
        String b2 = eh5.l.a().b("primaryText");
        String str = "#FFFFFF";
        this.d = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("nonBrandSurface");
        this.e = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("secondaryText");
        this.f = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.g = Color.parseColor(b5 != null ? b5 : str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.l.getId() == 0) {
            return 1010101;
        }
        return (long) this.l.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) getItem(i2);
        if (dailyHeartRateSummary == null) {
            return 1;
        }
        Calendar calendar = this.c;
        ee7.a((Object) calendar, "mCalendar");
        calendar.setTime(dailyHeartRateSummary.getDate());
        Calendar calendar2 = this.c;
        ee7.a((Object) calendar2, "mCalendar");
        Boolean w = zd5.w(calendar2.getTime());
        ee7.a((Object) w, "DateHelper.isToday(mCalendar.time)");
        if (w.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        ee7.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardHeartRatesAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            ee7.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            ee7.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardHeartRatesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            ee7.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((DailyHeartRateSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        ee7.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            o95 a2 = o95.a(from, viewGroup, false);
            ee7.a((Object) a2, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d2 = a2.d();
            ee7.a((Object) d2, "itemActivityDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            o95 a3 = o95.a(from, viewGroup, false);
            ee7.a((Object) a3, "ItemHeartRateDayBinding.\u2026tInflater, parent, false)");
            View d3 = a3.d();
            ee7.a((Object) d3, "itemActivityDayBinding.root");
            return new c(this, a3, d3);
        } else {
            q95 a4 = q95.a(from, viewGroup, false);
            ee7.a((Object) a4, "ItemHeartRateWeekBinding\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(com.fossil.qf<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary> r17) {
        /*
            r16 = this;
            if (r17 == 0) goto L_0x006a
            java.util.List r0 = r17.j()
            if (r0 == 0) goto L_0x006a
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            java.lang.String r2 = "summaries"
            com.fossil.ee7.a(r0, r2)
            boolean r2 = r0.isEmpty()
            r2 = r2 ^ 1
            java.lang.String r3 = "calendar"
            java.lang.String r4 = "calendar.time"
            if (r2 == 0) goto L_0x007d
            java.lang.Object r2 = com.fossil.ea7.d(r0)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r2 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r2
            com.fossil.ee7.a(r1, r3)
            java.util.Date r2 = r2.getDate()
            r1.setTime(r2)
            java.util.Date r2 = r1.getTime()
            java.lang.Boolean r2 = com.fossil.zd5.w(r2)
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x006d
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r1.setTime(r0)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r5 = new com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary
            r6 = 0
            java.util.Date r7 = r1.getTime()
            com.fossil.ee7.a(r7, r4)
            java.util.Date r0 = r1.getTime()
            com.fossil.ee7.a(r0, r4)
            long r8 = r0.getTime()
            java.util.Date r0 = r1.getTime()
            com.fossil.ee7.a(r0, r4)
            long r10 = r0.getTime()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r5.<init>(r6, r7, r8, r10, r12, r13, r14, r15)
        L_0x006a:
            r2 = r16
            goto L_0x00af
        L_0x006d:
            java.lang.Object r0 = com.fossil.ea7.d(r0)
            java.lang.String r1 = "summaries.first()"
            com.fossil.ee7.a(r0, r1)
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r0 = (com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary) r0
            r2 = r16
            r2.h = r0
            goto L_0x00af
        L_0x007d:
            r2 = r16
            com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary r0 = new com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary
            r5 = 0
            com.fossil.ee7.a(r1, r3)
            java.util.Date r6 = r1.getTime()
            com.fossil.ee7.a(r6, r4)
            java.util.Date r3 = r1.getTime()
            com.fossil.ee7.a(r3, r4)
            long r7 = r3.getTime()
            java.util.Date r1 = r1.getTime()
            com.fossil.ee7.a(r1, r4)
            long r9 = r1.getTime()
            r1 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r3 = r0
            r4 = r5
            r5 = r6
            r6 = r7
            r8 = r9
            r10 = r1
            r3.<init>(r4, r5, r6, r8, r10, r11, r12, r13)
        L_0x00af:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "updateList - size="
            r1.append(r3)
            if (r17 == 0) goto L_0x00ca
            int r3 = r17.size()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            goto L_0x00cb
        L_0x00ca:
            r3 = 0
        L_0x00cb:
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            java.lang.String r3 = "DashboardHeartRatesAdapter"
            r0.d(r3, r1)
            super.b(r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.up5.c(com.fossil.qf):void");
    }

    @DexIgnore
    public final b a(DailyHeartRateSummary dailyHeartRateSummary) {
        b bVar = new b(null, false, null, null, 0, null, 0, null, 255, null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int i2 = instance.get(7);
            Boolean w = zd5.w(instance.getTime());
            ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
            if (w.booleanValue()) {
                String a2 = ig5.a(this.i, 2131886598);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026artRateToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(xe5.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            Resting resting = dailyHeartRateSummary.getResting();
            boolean z = false;
            bVar.b(resting != null ? resting.getValue() : 0);
            String a3 = ig5.a(this.i, 2131886631);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
            if (a3 != null) {
                String lowerCase = a3.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                bVar.b(lowerCase);
                bVar.a(dailyHeartRateSummary.getMax());
                String a4 = ig5.a(this.i, 2131886629);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Label__Max)");
                if (a4 != null) {
                    String lowerCase2 = a4.toLowerCase();
                    ee7.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                    bVar.a(lowerCase2);
                    if (bVar.d() + bVar.b() == 0) {
                        z = true;
                    }
                    bVar.a(z);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(DailyHeartRateSummary dailyHeartRateSummary) {
        String str;
        String str2;
        d dVar = new d(null, null, null, null, 15, null);
        if (dailyHeartRateSummary != null) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            Boolean w = zd5.w(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String c2 = zd5.c(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String c3 = zd5.c(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            ee7.a((Object) w, "isToday");
            if (w.booleanValue()) {
                str = ig5.a(this.i, 2131886600);
                ee7.a((Object) str, "LanguageHelper.getString\u2026ateToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = c3 + ' ' + i5 + " - " + c3 + ' ' + i2;
            } else if (i7 == i4) {
                str = c3 + ' ' + i5 + " - " + c2 + ' ' + i2;
            } else {
                str = c3 + ' ' + i5 + ", " + i7 + " - " + c2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            if (dailyHeartRateSummary.getAvgRestingHeartRateOfWeek() == null) {
                str2 = "0";
            } else {
                str2 = String.valueOf(dailyHeartRateSummary.getAvgRestingHeartRateOfWeek());
            }
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886597);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026y_Text__NumberRestingBpm)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{str2}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }
}
