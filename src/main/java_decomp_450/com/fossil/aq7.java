package com.fossil;

import com.fossil.up7;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq7 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ yp7 d;
    @DexIgnore
    public /* final */ Deque<fo7> e; // = new ArrayDeque();
    @DexIgnore
    public up7.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ a i;
    @DexIgnore
    public /* final */ c j; // = new c();
    @DexIgnore
    public /* final */ c k; // = new c();
    @DexIgnore
    public tp7 l; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements sr7 {
        @DexIgnore
        public /* final */ yq7 a; // = new yq7();
        @DexIgnore
        public /* final */ yq7 b; // = new yq7();
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b(long j) {
            this.c = j;
        }

        @DexIgnore
        public void a(ar7 ar7, long j) throws IOException {
            boolean z;
            boolean z2;
            boolean z3;
            long j2;
            while (j > 0) {
                synchronized (aq7.this) {
                    z = this.e;
                    z2 = true;
                    z3 = this.b.x() + j > this.c;
                }
                if (z3) {
                    ar7.skip(j);
                    aq7.this.c(tp7.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    ar7.skip(j);
                    return;
                } else {
                    long b2 = ar7.b(this.a, j);
                    if (b2 != -1) {
                        j -= b2;
                        synchronized (aq7.this) {
                            if (this.d) {
                                j2 = this.a.x();
                                this.a.k();
                            } else {
                                if (this.b.x() != 0) {
                                    z2 = false;
                                }
                                this.b.a((sr7) this.a);
                                if (z2) {
                                    aq7.this.notifyAll();
                                }
                                j2 = 0;
                            }
                        }
                        if (j2 > 0) {
                            e(j2);
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            tp7 tp7;
            long j2;
            up7.a aVar;
            fo7 fo7;
            if (j >= 0) {
                while (true) {
                    synchronized (aq7.this) {
                        aq7.this.j.g();
                        try {
                            tp7 = aq7.this.l != null ? aq7.this.l : null;
                            if (!this.d) {
                                if (aq7.this.e.isEmpty() || aq7.this.f == null) {
                                    if (this.b.x() > 0) {
                                        j2 = this.b.b(yq7, Math.min(j, this.b.x()));
                                        aq7.this.a += j2;
                                        if (tp7 == null && aq7.this.a >= ((long) (aq7.this.d.x.c() / 2))) {
                                            aq7.this.d.a(aq7.this.c, aq7.this.a);
                                            aq7.this.a = 0;
                                        }
                                    } else if (this.e || tp7 != null) {
                                        j2 = -1;
                                    } else {
                                        aq7.this.k();
                                        aq7.this.j.m();
                                    }
                                    fo7 = null;
                                    aVar = null;
                                } else {
                                    fo7 = (fo7) aq7.this.e.removeFirst();
                                    aVar = aq7.this.f;
                                    j2 = -1;
                                }
                                if (fo7 != null && aVar != null) {
                                    aVar.a(fo7);
                                }
                            } else {
                                throw new IOException("stream closed");
                            }
                        } finally {
                            aq7.this.j.m();
                        }
                    }
                }
                if (j2 != -1) {
                    e(j2);
                    return j2;
                } else if (tp7 == null) {
                    return -1;
                } else {
                    throw new fq7(tp7);
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
            long x;
            up7.a aVar;
            ArrayList<fo7> arrayList;
            synchronized (aq7.this) {
                this.d = true;
                x = this.b.x();
                this.b.k();
                aVar = null;
                if (aq7.this.e.isEmpty() || aq7.this.f == null) {
                    arrayList = null;
                } else {
                    ArrayList arrayList2 = new ArrayList(aq7.this.e);
                    aq7.this.e.clear();
                    aVar = aq7.this.f;
                    arrayList = arrayList2;
                }
                aq7.this.notifyAll();
            }
            if (x > 0) {
                e(x);
            }
            aq7.this.a();
            if (aVar != null) {
                for (fo7 fo7 : arrayList) {
                    aVar.a(fo7);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public tr7 d() {
            return aq7.this.j;
        }

        @DexIgnore
        public final void e(long j) {
            aq7.this.d.g(j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends wq7 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.wq7
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        @Override // com.fossil.wq7
        public void i() {
            aq7.this.c(tp7.CANCEL);
            aq7.this.d.c();
        }

        @DexIgnore
        public void m() throws IOException {
            if (h()) {
                throw b((IOException) null);
            }
        }
    }

    @DexIgnore
    public aq7(int i2, yp7 yp7, boolean z, boolean z2, fo7 fo7) {
        if (yp7 != null) {
            this.c = i2;
            this.d = yp7;
            this.b = (long) yp7.y.c();
            this.h = new b((long) yp7.x.c());
            a aVar = new a();
            this.i = aVar;
            this.h.e = z2;
            aVar.c = z;
            if (fo7 != null) {
                this.e.add(fo7);
            }
            if (f() && fo7 != null) {
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet");
            } else if (!f() && fo7 == null) {
                throw new IllegalStateException("remotely-initiated streams should have headers");
            }
        } else {
            throw new NullPointerException("connection == null");
        }
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public qr7 d() {
        synchronized (this) {
            if (!this.g) {
                if (!f()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.i;
    }

    @DexIgnore
    public sr7 e() {
        return this.h;
    }

    @DexIgnore
    public boolean f() {
        if (this.d.a == ((this.c & 1) == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public synchronized boolean g() {
        if (this.l != null) {
            return false;
        }
        if ((this.h.e || this.h.d) && ((this.i.c || this.i.b) && this.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public tr7 h() {
        return this.j;
    }

    @DexIgnore
    public void i() {
        boolean g2;
        synchronized (this) {
            this.h.e = true;
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.c(this.c);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public synchronized fo7 j() throws IOException {
        this.j.g();
        while (this.e.isEmpty() && this.l == null) {
            try {
                k();
            } catch (Throwable th) {
                this.j.m();
                throw th;
            }
        }
        this.j.m();
        if (!this.e.isEmpty()) {
        } else {
            throw new fq7(this.l);
        }
        return this.e.removeFirst();
    }

    @DexIgnore
    public void k() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public tr7 l() {
        return this.k;
    }

    @DexIgnore
    public void a(tp7 tp7) throws IOException {
        if (b(tp7)) {
            this.d.b(this.c, tp7);
        }
    }

    @DexIgnore
    public final boolean b(tp7 tp7) {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            if (this.h.e && this.i.c) {
                return false;
            }
            this.l = tp7;
            notifyAll();
            this.d.c(this.c);
            return true;
        }
    }

    @DexIgnore
    public void c(tp7 tp7) {
        if (b(tp7)) {
            this.d.c(this.c, tp7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements qr7 {
        @DexIgnore
        public /* final */ yq7 a; // = new yq7();
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public void a(yq7 yq7, long j) throws IOException {
            this.a.a(yq7, j);
            while (this.a.x() >= 16384) {
                a(false);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
            if (r8.a.x() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
            if (r8.a.x() <= 0) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
            r0 = r8.d;
            r0.d.a(r0.c, true, (com.fossil.yq7) null, 0L);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
            r2 = r8.d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.b = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
            r8.d.d.flush();
            r8.d.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r8.d.i.c != false) goto L_0x003a;
         */
        @DexIgnore
        @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r8 = this;
                com.fossil.aq7 r0 = com.fossil.aq7.this
                monitor-enter(r0)
                boolean r1 = r8.b     // Catch:{ all -> 0x0050 }
                if (r1 == 0) goto L_0x0009
                monitor-exit(r0)     // Catch:{ all -> 0x0050 }
                return
            L_0x0009:
                monitor-exit(r0)     // Catch:{ all -> 0x0050 }
                com.fossil.aq7 r0 = com.fossil.aq7.this
                com.fossil.aq7$a r0 = r0.i
                boolean r0 = r0.c
                r1 = 1
                if (r0 != 0) goto L_0x003a
                com.fossil.yq7 r0 = r8.a
                long r2 = r0.x()
                r4 = 0
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x002d
            L_0x001f:
                com.fossil.yq7 r0 = r8.a
                long r2 = r0.x()
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x003a
                r8.a(r1)
                goto L_0x001f
            L_0x002d:
                com.fossil.aq7 r0 = com.fossil.aq7.this
                com.fossil.yp7 r2 = r0.d
                int r3 = r0.c
                r4 = 1
                r5 = 0
                r6 = 0
                r2.a(r3, r4, r5, r6)
            L_0x003a:
                com.fossil.aq7 r2 = com.fossil.aq7.this
                monitor-enter(r2)
                r8.b = r1     // Catch:{ all -> 0x004d }
                monitor-exit(r2)     // Catch:{ all -> 0x004d }
                com.fossil.aq7 r0 = com.fossil.aq7.this
                com.fossil.yp7 r0 = r0.d
                r0.flush()
                com.fossil.aq7 r0 = com.fossil.aq7.this
                r0.a()
                return
            L_0x004d:
                r0 = move-exception
                monitor-exit(r2)
                throw r0
            L_0x0050:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.aq7.a.close():void");
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public tr7 d() {
            return aq7.this.k;
        }

        @DexIgnore
        @Override // com.fossil.qr7, java.io.Flushable
        public void flush() throws IOException {
            synchronized (aq7.this) {
                aq7.this.b();
            }
            while (this.a.x() > 0) {
                a(false);
                aq7.this.d.flush();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (aq7.this) {
                aq7.this.k.g();
                while (aq7.this.b <= 0 && !this.c && !this.b && aq7.this.l == null) {
                    try {
                        aq7.this.k();
                    } catch (Throwable th) {
                        aq7.this.k.m();
                        throw th;
                    }
                }
                aq7.this.k.m();
                aq7.this.b();
                min = Math.min(aq7.this.b, this.a.x());
                aq7.this.b -= min;
            }
            aq7.this.k.g();
            try {
                aq7.this.d.a(aq7.this.c, z && min == this.a.x(), this.a, min);
            } finally {
                aq7.this.k.m();
            }
        }
    }

    @DexIgnore
    public void a(List<up7> list) {
        boolean g2;
        synchronized (this) {
            this.g = true;
            this.e.add(ro7.b(list));
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.c(this.c);
        }
    }

    @DexIgnore
    public synchronized void d(tp7 tp7) {
        if (this.l == null) {
            this.l = tp7;
            notifyAll();
        }
    }

    @DexIgnore
    public void a(ar7 ar7, int i2) throws IOException {
        this.h.a(ar7, (long) i2);
    }

    @DexIgnore
    public void b() throws IOException {
        a aVar = this.i;
        if (aVar.b) {
            throw new IOException("stream closed");
        } else if (aVar.c) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new fq7(this.l);
        }
    }

    @DexIgnore
    public void a() throws IOException {
        boolean z;
        boolean g2;
        synchronized (this) {
            z = !this.h.e && this.h.d && (this.i.c || this.i.b);
            g2 = g();
        }
        if (z) {
            a(tp7.CANCEL);
        } else if (!g2) {
            this.d.c(this.c);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }
}
