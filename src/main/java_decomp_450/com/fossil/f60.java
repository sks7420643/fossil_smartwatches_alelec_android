package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.PlaceManager;
import com.fossil.l60;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTimeFieldType;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f60 {
    @DexIgnore
    public static /* final */ Handler a;
    @DexIgnore
    public static /* final */ BluetoothAdapter b; // = BluetoothAdapter.getDefaultAdapter();
    @DexIgnore
    public static f c;
    @DexIgnore
    public static /* final */ Hashtable<String, q60<m60, eu0>> d; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ScanCallback e; // = new i();
    @DexIgnore
    public static /* final */ LinkedHashMap<b, j60> f; // = new LinkedHashMap<>();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new g();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new h();
    @DexIgnore
    public static boolean i;
    @DexIgnore
    public static a j;
    @DexIgnore
    public static /* final */ f60 k; // = new f60();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(f60 f60, c cVar, c cVar2);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceFound(l60 l60, int i);

        @DexIgnore
        void onStartScanFailed(g60 g60);
    }

    @DexIgnore
    public enum c {
        DISABLED(10),
        ENABLING(11),
        ENABLED(12),
        DISABLING(13);
        
        @DexIgnore
        public static /* final */ a c; // = new a(null);
        @DexIgnore
        public /* final */ int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
            }

            @DexIgnore
            public final c a(int i) {
                c cVar;
                c[] values = c.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        cVar = null;
                        break;
                    }
                    cVar = values[i2];
                    if (cVar.a() == i) {
                        break;
                    }
                    i2++;
                }
                return cVar != null ? cVar : c.DISABLED;
            }
        }

        @DexIgnore
        public c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class d extends Enum<d> {
        @DexIgnore
        public static /* final */ d b;
        @DexIgnore
        public static /* final */ d c;
        @DexIgnore
        public static /* final */ d d;
        @DexIgnore
        public static /* final */ d e;
        @DexIgnore
        public static /* final */ d f;
        @DexIgnore
        public static /* final */ d g;
        @DexIgnore
        public static /* final */ d h;
        @DexIgnore
        public static /* final */ d i;
        @DexIgnore
        public static /* final */ d j;
        @DexIgnore
        public static /* final */ /* synthetic */ d[] k;
        @DexIgnore
        public static /* final */ vj0 l; // = new vj0(null);
        @DexIgnore
        public /* final */ byte a;

        /*
        static {
            d dVar = new d("INCOMPLETE_16_BIT_SERVICE_CLASS_UUID", 1, (byte) 2);
            b = dVar;
            d dVar2 = new d("COMPLETE_16_BIT_SERVICE_CLASS_UUID", 2, (byte) 3);
            c = dVar2;
            d dVar3 = new d("INCOMPLETE_32_BIT_SERVICE_CLASS_UUID", 3, (byte) 4);
            d = dVar3;
            d dVar4 = new d("COMPLETE_32_BIT_SERVICE_CLASS_UUID", 4, (byte) 5);
            e = dVar4;
            d dVar5 = new d("INCOMPLETE_128_BIT_SERVICE_CLASS_UUID", 5, (byte) 6);
            f = dVar5;
            d dVar6 = new d("COMPLETE_128_BIT_SERVICE_CLASS_UUID", 6, (byte) 7);
            g = dVar6;
            d dVar7 = new d("COMPLETE_LOCAL_NAME", 7, (byte) 9);
            h = dVar7;
            d dVar8 = new d("SERVICE_DATA_16_BIT_SERVICE_CLASS_UUID", 8, DateTimeFieldType.MILLIS_OF_DAY);
            i = dVar8;
            d dVar9 = new d("MANUFACTURER_SPECIFIC_DATA", 9, (byte) 255);
            j = dVar9;
            k = new d[]{new d("FLAG", 0, (byte) 1), dVar, dVar2, dVar3, dVar4, dVar5, dVar6, dVar7, dVar8, dVar9};
        }
        */

        @DexIgnore
        public d(String str, int i2, byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public static d valueOf(String str) {
            return (d) Enum.valueOf(d.class, str);
        }

        @DexIgnore
        public static d[] values() {
            return (d[]) k.clone();
        }
    }

    @DexIgnore
    public enum e {
        START_SCAN,
        START_SCAN_FAILED,
        STOP_SCAN,
        DEVICE_RETRIEVED,
        DEVICE_FOUND,
        GET_GATT_CONNECTED_DEVICES,
        GET_HID_CONNECTED_DEVICES,
        BOND_STATE_CHANGED,
        BLUETOOTH_STATE_CHANGED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public final JSONArray a(List<BluetoothDevice> list) {
            JSONArray jSONArray = new JSONArray();
            for (T t : list) {
                JSONObject jSONObject = new JSONObject();
                r51 r51 = r51.G;
                Object name = t.getName();
                if (name == null) {
                    name = JSONObject.NULL;
                }
                jSONArray.put(yz0.a(yz0.a(yz0.a(jSONObject, r51, name), r51.i0, t.getAddress()), r51.m1, m01.e.a(t.getBondState())));
            }
            return jSONArray;
        }

        @DexIgnore
        public void run() {
            if (!this.a) {
                t11 t11 = t11.a;
                List<BluetoothDevice> a2 = f60.k.a();
                t11 t112 = t11.a;
                ea7.a(a2, null, null, null, 0, null, tl0.a, 31, null);
                wl0.h.a(new wr1(yz0.a(e.GET_GATT_CONNECTED_DEVICES), ci1.g, "", "", "", true, null, null, null, yz0.a(new JSONObject(), r51.B1, a(a2)), 448));
                List<BluetoothDevice> b = f60.k.b();
                t11 t113 = t11.a;
                ea7.a(b, null, null, null, 0, null, pn0.a, 31, null);
                wl0.h.a(new wr1(yz0.a(e.GET_HID_CONNECTED_DEVICES), ci1.g, "", "", "", true, null, null, null, yz0.a(new JSONObject(), r51.C1, a(b)), 448));
                ArrayList arrayList = new ArrayList(a2);
                arrayList.addAll(b);
                for (BluetoothDevice bluetoothDevice : ea7.c((Iterable) arrayList)) {
                    if (bluetoothDevice.getType() != 1) {
                        sx0 sx0 = sx0.c;
                        String address = bluetoothDevice.getAddress();
                        ee7.a((Object) address, "bluetoothDevice.address");
                        String b2 = sx0.b(address);
                        if (b2 != null) {
                            km1 a3 = sx0.c.a(bluetoothDevice, b2);
                            f60 f60 = f60.k;
                            if (f60.b(a3)) {
                                f60.a(a3, 0);
                            }
                        } else {
                            f60 f602 = f60.k;
                            if (f60.d.get(bluetoothDevice.getAddress()) == null) {
                                km1 a4 = sx0.c.a(bluetoothDevice, "");
                                yo1 yo1 = yo1.AUTO_CONNECT;
                                yp0.f.f();
                                HashMap<yo1, Object> b3 = oa7.b(w87.a(yo1, false), w87.a(yo1.CONNECTION_TIME_OUT, 30000L));
                                t11 t114 = t11.a;
                                q60<m60, eu0> b4 = a4.b(b3);
                                f60 f603 = f60.k;
                                f60.d.put(bluetoothDevice.getAddress(), b4);
                                b4.c(new lp0(bluetoothDevice, a4)).b(new hr0(bluetoothDevice));
                            }
                        }
                    }
                }
                f60 f604 = f60.k;
                f60.a.postDelayed(this, 5000);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String action;
            if (context != null && intent != null && (action = intent.getAction()) != null && action.hashCode() == -1530327060 && action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                f60.k.a(c.c.a(intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", RecyclerView.UNDEFINED_DURATION)), c.c.a(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1);
                if (bluetoothDevice != null) {
                    f60.k.a(bluetoothDevice, intExtra, intExtra2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends ScanCallback {
        @DexIgnore
        public void onScanFailed(int i) {
            f60.k.a(new g60(h60.e.a(i)));
        }

        @DexIgnore
        public void onScanResult(int i, ScanResult scanResult) {
            ScanRecord scanRecord;
            byte[] bytes = (scanResult == null || (scanRecord = scanResult.getScanRecord()) == null) ? null : scanRecord.getBytes();
            BluetoothDevice device = scanResult != null ? scanResult.getDevice() : null;
            if (bytes != null && device != null) {
                if (f60.k.d(bytes)) {
                    String f = f60.k.f(bytes);
                    if (m60.t.a(f)) {
                        km1 a = sx0.c.a(device, f);
                        if (f60.k.b(a)) {
                            f60.k.a(a, scanResult.getRssi());
                        }
                    }
                } else if (f60.k.e(bytes)) {
                    km1 a2 = sx0.c.a(device, "");
                    m60 a3 = m60.a(a2.t, f60.k.a(bytes), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, yz0.a(f60.k.b(bytes), (String) null, 1), 131070);
                    a2.t = a3;
                    a3.a(n60.WEAR_OS);
                    f60.k.a(a2, scanResult.getRssi());
                }
            }
        }
    }

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            a = new Handler(myLooper);
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final void a(List<ScanFilter> list, ScanSettings scanSettings) {
        BluetoothAdapter bluetoothAdapter = b;
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter != null ? bluetoothAdapter.getBluetoothLeScanner() : null;
        if (bluetoothLeScanner == null) {
            a(new g60(h60.UNKNOWN_ERROR));
        } else {
            bluetoothLeScanner.startScan(list, scanSettings, e);
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> b() {
        return zz0.d.a();
    }

    @DexIgnore
    public final void c() {
        int i2;
        b[] bVarArr;
        synchronized (f) {
            Set<b> keySet = f.keySet();
            ee7.a((Object) keySet, "activeLeScanCallbacks.keys");
            Object[] array = keySet.toArray(new b[0]);
            if (array != null) {
                bVarArr = (b[]) array;
                i97 i97 = i97.a;
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        for (b bVar : bVarArr) {
            k.a(bVar);
        }
    }

    @DexIgnore
    public final boolean d(byte[] bArr) {
        HashMap<d, String[]> c2 = c(bArr);
        String[] strArr = c2.get(d.f);
        if (strArr != null && t97.a(strArr, b21.x.u())) {
            return true;
        }
        String[] strArr2 = c2.get(d.g);
        if (strArr2 == null || !t97.a(strArr2, b21.x.u())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean e(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = yz0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (d.l.a(bArr[i3]) == d.j && b2 >= 3) {
                ByteBuffer order = ByteBuffer.wrap(s97.a(s97.a(bArr, i3, i3 + b2), 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == 224) {
                    return true;
                }
            }
            i2 = b2 + i3;
        }
        return false;
    }

    @DexIgnore
    public final String f(byte[] bArr) {
        byte[] bArr2;
        byte b2;
        if (bArr == null) {
            return new String();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= bArr.length) {
                break;
            }
            int i3 = i2 + 1;
            short b3 = yz0.b(bArr[i2]);
            if (b3 != 0 && (b2 = bArr[i3]) != ((byte) 0)) {
                if (b2 == -1 && b3 >= 13) {
                    int i4 = i3 + 1 + 2;
                    bArr2 = Arrays.copyOfRange(bArr, i4, i4 + 10);
                    ee7.a((Object) bArr2, "Arrays.copyOfRange(adver\u2026RER_SERIAL_NUMBER_LENGTH)");
                    break;
                }
                i2 = b3 + i3;
            } else {
                break;
            }
        }
        bArr2 = new byte[0];
        return new String(bArr2, sg7.a);
    }

    @DexIgnore
    public final void b(b bVar) {
        wl0.h.a(new wr1(yz0.a(e.STOP_SCAN), ci1.g, "", "", "", true, null, null, null, yz0.a(new JSONObject(), r51.D1, yz0.a(bVar.hashCode())), 448));
        a(bVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.l60 a(java.lang.String r18, java.lang.String r19) throws java.lang.IllegalArgumentException {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            com.fossil.t11 r3 = com.fossil.t11.a
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            com.fossil.r51 r4 = com.fossil.r51.j0
            org.json.JSONObject r3 = com.fossil.yz0.a(r3, r4, r1)
            com.fossil.r51 r4 = com.fossil.r51.i0
            org.json.JSONObject r15 = com.fossil.yz0.a(r3, r4, r2)
            com.fossil.zs1 r3 = com.fossil.zs1.b
            boolean r3 = r3.a()
            if (r3 == 0) goto L_0x0127
            com.fossil.u31 r3 = com.fossil.u31.g
            java.lang.Object r3 = r3.f()
            r4 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r4)
            boolean r3 = com.fossil.ee7.a(r3, r5)
            r3 = r3 ^ r4
            r5 = 0
            if (r3 == 0) goto L_0x0035
            return r5
        L_0x0035:
            com.fossil.m60$c r3 = com.fossil.m60.t
            boolean r3 = r3.a(r1)
            if (r3 == 0) goto L_0x00e0
            com.fossil.n60$a r3 = com.fossil.n60.d
            com.fossil.n60 r3 = r3.a(r1)
            com.fossil.n60 r6 = com.fossil.n60.UNKNOWN
            r7 = 0
            if (r3 == r6) goto L_0x008c
            com.fossil.sx0 r3 = com.fossil.sx0.c
            java.lang.String r3 = r3.a(r1)
            com.fossil.t11 r6 = com.fossil.t11.a
            boolean r6 = android.bluetooth.BluetoothAdapter.checkBluetoothAddress(r3)
            if (r6 == 0) goto L_0x006a
            android.bluetooth.BluetoothAdapter r6 = com.fossil.f60.b
            if (r6 == 0) goto L_0x005f
            android.bluetooth.BluetoothDevice r3 = r6.getRemoteDevice(r3)
            goto L_0x0060
        L_0x005f:
            r3 = r5
        L_0x0060:
            if (r3 == 0) goto L_0x006a
            com.fossil.sx0 r6 = com.fossil.sx0.c
            com.fossil.km1 r3 = r6.a(r3, r1)
            r6 = 1
            goto L_0x006c
        L_0x006a:
            r3 = r5
            r6 = 0
        L_0x006c:
            if (r3 != 0) goto L_0x008e
            com.fossil.t11 r8 = com.fossil.t11.a
            boolean r8 = android.bluetooth.BluetoothAdapter.checkBluetoothAddress(r19)
            if (r8 == 0) goto L_0x0089
            android.bluetooth.BluetoothAdapter r8 = com.fossil.f60.b
            if (r8 == 0) goto L_0x007f
            android.bluetooth.BluetoothDevice r2 = r8.getRemoteDevice(r2)
            goto L_0x0080
        L_0x007f:
            r2 = r5
        L_0x0080:
            if (r2 == 0) goto L_0x008e
            com.fossil.sx0 r3 = com.fossil.sx0.c
            com.fossil.km1 r3 = r3.a(r2, r1)
            goto L_0x008e
        L_0x0089:
            com.fossil.t11 r1 = com.fossil.t11.a
            goto L_0x008e
        L_0x008c:
            r3 = r5
            r6 = 0
        L_0x008e:
            if (r3 == 0) goto L_0x009c
            boolean r1 = r0.b(r3)
            if (r1 != 0) goto L_0x009c
            com.fossil.t11 r1 = com.fossil.t11.a
            r0.a(r3)
            r3 = r5
        L_0x009c:
            com.fossil.r51 r1 = com.fossil.r51.p1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)
            com.fossil.yz0.a(r15, r1, r2)
            com.fossil.r51 r1 = com.fossil.r51.k1
            if (r3 == 0) goto L_0x00ae
            org.json.JSONObject r2 = r0.a(r3)
            goto L_0x00b0
        L_0x00ae:
            java.lang.Object r2 = org.json.JSONObject.NULL
        L_0x00b0:
            com.fossil.yz0.a(r15, r1, r2)
            com.fossil.wl0 r1 = com.fossil.wl0.h
            com.fossil.wr1 r2 = new com.fossil.wr1
            com.fossil.f60$e r5 = com.fossil.f60.e.DEVICE_RETRIEVED
            java.lang.String r6 = com.fossil.yz0.a(r5)
            com.fossil.ci1 r8 = com.fossil.ci1.g
            if (r3 == 0) goto L_0x00c3
            r11 = 1
            goto L_0x00c4
        L_0x00c3:
            r11 = 0
        L_0x00c4:
            r12 = 0
            r13 = 0
            r14 = 0
            r16 = 448(0x1c0, float:6.28E-43)
            java.lang.String r4 = ""
            java.lang.String r9 = ""
            java.lang.String r10 = ""
            r5 = r2
            r7 = r8
            r8 = r4
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            r1.a(r2)
            com.fossil.t11 r1 = com.fossil.t11.a
            if (r3 == 0) goto L_0x00df
            r0.a(r3)
        L_0x00df:
            return r3
        L_0x00e0:
            com.fossil.wl0 r2 = com.fossil.wl0.h
            com.fossil.wr1 r14 = new com.fossil.wr1
            com.fossil.f60$e r3 = com.fossil.f60.e.DEVICE_RETRIEVED
            java.lang.String r4 = com.fossil.yz0.a(r3)
            com.fossil.ci1 r5 = com.fossil.ci1.g
            com.fossil.r51 r3 = com.fossil.r51.N0
            java.lang.String r6 = "Invalid serial number."
            org.json.JSONObject r13 = com.fossil.yz0.a(r15, r3, r6)
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r15 = 448(0x1c0, float:6.28E-43)
            java.lang.String r6 = ""
            java.lang.String r7 = ""
            java.lang.String r8 = ""
            r3 = r14
            r0 = r14
            r14 = r15
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r2.a(r0)
            com.fossil.t11 r0 = com.fossil.t11.a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "serialNumber("
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = ") is invalid."
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            r0.<init>(r1)
            throw r0
        L_0x0127:
            com.fossil.wl0 r0 = com.fossil.wl0.h
            com.fossil.wr1 r13 = new com.fossil.wr1
            com.fossil.f60$e r1 = com.fossil.f60.e.DEVICE_RETRIEVED
            java.lang.String r2 = com.fossil.yz0.a(r1)
            com.fossil.ci1 r3 = com.fossil.ci1.g
            com.fossil.r51 r1 = com.fossil.r51.N0
            java.lang.String r14 = "SdkLogManager.init must be call with application context first."
            org.json.JSONObject r11 = com.fossil.yz0.a(r15, r1, r14)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r12 = 448(0x1c0, float:6.28E-43)
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            java.lang.String r6 = ""
            r1 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r0.a(r13)
            com.fossil.t11 r0 = com.fossil.t11.a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r14)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.f60.a(java.lang.String, java.lang.String):com.fossil.l60");
    }

    @DexIgnore
    public final c d() {
        if (b == null) {
            return c.DISABLED;
        }
        return c.c.a(b.getState());
    }

    @DexIgnore
    public final boolean b(km1 km1) {
        return i || km1.t.getDeviceType() != n60.UNKNOWN;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.HashMap<com.fossil.f60$d, java.lang.String[]> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v6, resolved type: java.lang.Object[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0071 A[LOOP:1: B:21:0x006f->B:22:0x0071, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.HashMap<com.fossil.f60.d, java.lang.String[]> c(byte[] r14) {
        /*
            r13 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r1 = 0
            r2 = 0
        L_0x0007:
            int r3 = r14.length
            if (r2 >= r3) goto L_0x009d
            int r3 = r2 + 1
            byte r2 = r14[r2]
            r4 = 255(0xff, float:3.57E-43)
            byte r4 = (byte) r4
            r2 = r2 & r4
            byte r2 = (byte) r2
            short r2 = com.fossil.yz0.b(r2)
            if (r2 == 0) goto L_0x009d
            int r4 = r3 + 1
            int r5 = r14.length
            if (r4 <= r5) goto L_0x0020
            goto L_0x009d
        L_0x0020:
            com.fossil.vj0 r5 = com.fossil.f60.d.l
            byte r6 = r14[r3]
            com.fossil.f60$d r5 = r5.a(r6)
            if (r5 != 0) goto L_0x002b
            goto L_0x003e
        L_0x002b:
            int[] r6 = com.fossil.ct0.a
            int r7 = r5.ordinal()
            r6 = r6[r7]
            switch(r6) {
                case 1: goto L_0x003c;
                case 2: goto L_0x003c;
                case 3: goto L_0x003a;
                case 4: goto L_0x003a;
                case 5: goto L_0x0037;
                case 6: goto L_0x0037;
                default: goto L_0x0036;
            }
        L_0x0036:
            goto L_0x003e
        L_0x0037:
            r6 = 16
            goto L_0x003f
        L_0x003a:
            r6 = 4
            goto L_0x003f
        L_0x003c:
            r6 = 2
            goto L_0x003f
        L_0x003e:
            r6 = 0
        L_0x003f:
            if (r5 == 0) goto L_0x009a
            if (r6 <= 0) goto L_0x009a
            int r7 = r3 + r2
            byte[] r4 = java.util.Arrays.copyOfRange(r14, r4, r7)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.Object r8 = r0.get(r5)
            java.lang.String[] r8 = (java.lang.String[]) r8
            if (r8 == 0) goto L_0x005e
            java.lang.String r9 = "currentServiceUUIDs"
            com.fossil.ee7.a(r8, r9)
            com.fossil.ba7.a(r7, r8)
        L_0x005e:
            java.lang.String r8 = "serviceUUIDRaw"
            com.fossil.ee7.a(r4, r8)
            byte[][] r4 = com.fossil.yz0.a(r4, r6)
            java.util.ArrayList r6 = new java.util.ArrayList
            int r8 = r4.length
            r6.<init>(r8)
            int r8 = r4.length
            r9 = 0
        L_0x006f:
            if (r9 >= r8) goto L_0x0083
            r10 = r4[r9]
            byte[] r10 = com.fossil.t97.c(r10)
            r11 = 0
            r12 = 1
            java.lang.String r10 = com.fossil.yz0.a(r10, r11, r12)
            r6.add(r10)
            int r9 = r9 + 1
            goto L_0x006f
        L_0x0083:
            r7.addAll(r6)
            java.lang.String[] r4 = new java.lang.String[r1]
            java.lang.Object[] r4 = r7.toArray(r4)
            if (r4 == 0) goto L_0x0092
            r0.put(r5, r4)
            goto L_0x009a
        L_0x0092:
            com.fossil.x87 r14 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
            r14.<init>(r0)
            throw r14
        L_0x009a:
            int r2 = r2 + r3
            goto L_0x0007
        L_0x009d:
            return r0
            switch-data {1->0x003c, 2->0x003c, 3->0x003a, 4->0x003a, 5->0x0037, 6->0x0037, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.f60.c(byte[]):java.util.HashMap");
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = yz0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (d.l.a(bArr[i3]) == d.i && b2 >= 3) {
                byte[] a2 = s97.a(bArr, i3, i3 + b2);
                ByteBuffer order = ByteBuffer.wrap(s97.a(a2, 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == -468) {
                    return s97.a(a2, 3, b2);
                }
            }
            i2 = b2 + i3;
        }
        return new byte[0];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0178, code lost:
        if (r0 == false) goto L_0x017b;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x017e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.j60 r22, com.fossil.f60.b r23) throws java.lang.IllegalStateException {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            r3 = r23
            com.fossil.wl0 r0 = com.fossil.wl0.h
            com.fossil.wr1 r15 = new com.fossil.wr1
            com.fossil.f60$e r4 = com.fossil.f60.e.START_SCAN
            java.lang.String r5 = com.fossil.yz0.a(r4)
            com.fossil.ci1 r6 = com.fossil.ci1.g
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            com.fossil.r51 r7 = com.fossil.r51.J1
            org.json.JSONObject r8 = r22.a()
            org.json.JSONObject r4 = com.fossil.yz0.a(r4, r7, r8)
            com.fossil.r51 r7 = com.fossil.r51.D1
            int r8 = r23.hashCode()
            java.lang.String r8 = com.fossil.yz0.a(r8)
            org.json.JSONObject r14 = com.fossil.yz0.a(r4, r7, r8)
            java.lang.String r7 = ""
            java.lang.String r8 = ""
            java.lang.String r9 = ""
            r10 = 1
            r11 = 0
            r12 = 0
            r13 = 0
            r16 = 448(0x1c0, float:6.28E-43)
            r4 = r15
            r1 = r15
            r15 = r16
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r0.a(r1)
            com.fossil.u31 r0 = com.fossil.u31.g
            android.content.Context r0 = r0.a()
            r1 = 1
            r4 = 0
            if (r0 != 0) goto L_0x0109
            com.fossil.u31 r0 = com.fossil.u31.g
            java.lang.Object r0 = r0.f()
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r1)
            boolean r0 = com.fossil.ee7.a(r0, r5)
            if (r0 != 0) goto L_0x00a2
            com.fossil.wl0 r0 = com.fossil.wl0.h
            com.fossil.wr1 r1 = new com.fossil.wr1
            com.fossil.f60$e r4 = com.fossil.f60.e.START_SCAN
            java.lang.String r5 = com.fossil.yz0.a(r4)
            com.fossil.ci1 r6 = com.fossil.ci1.g
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            com.fossil.r51 r7 = com.fossil.r51.J1
            org.json.JSONObject r8 = r22.a()
            org.json.JSONObject r4 = com.fossil.yz0.a(r4, r7, r8)
            com.fossil.r51 r7 = com.fossil.r51.D1
            int r8 = r23.hashCode()
            java.lang.String r8 = com.fossil.yz0.a(r8)
            org.json.JSONObject r14 = com.fossil.yz0.a(r4, r7, r8)
            r10 = 1
            r11 = 0
            r12 = 0
            r13 = 0
            r15 = 448(0x1c0, float:6.28E-43)
            java.lang.String r7 = ""
            java.lang.String r8 = ""
            java.lang.String r9 = ""
            r4 = r1
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            r0.a(r1)
            r5 = r21
            r5.a(r3, r2)
            goto L_0x01c8
        L_0x00a2:
            r5 = r21
            com.fossil.wl0 r0 = com.fossil.wl0.h
            com.fossil.wr1 r15 = new com.fossil.wr1
            com.fossil.f60$e r6 = com.fossil.f60.e.START_SCAN_FAILED
            java.lang.String r7 = com.fossil.yz0.a(r6)
            com.fossil.ci1 r8 = com.fossil.ci1.g
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
            com.fossil.r51 r9 = com.fossil.r51.J1
            org.json.JSONObject r10 = r22.a()
            org.json.JSONObject r6 = com.fossil.yz0.a(r6, r9, r10)
            com.fossil.r51 r9 = com.fossil.r51.D1
            int r10 = r23.hashCode()
            java.lang.String r10 = com.fossil.yz0.a(r10)
            org.json.JSONObject r6 = com.fossil.yz0.a(r6, r9, r10)
            com.fossil.r51 r9 = com.fossil.r51.N0
            java.lang.String r14 = "SdkLogManager.init must be call with application context first."
            org.json.JSONObject r16 = com.fossil.yz0.a(r6, r9, r14)
            r12 = 0
            r13 = 0
            r17 = 0
            r18 = 0
            r19 = 448(0x1c0, float:6.28E-43)
            java.lang.String r9 = ""
            java.lang.String r10 = ""
            java.lang.String r11 = ""
            r6 = r15
            r20 = r14
            r14 = r17
            r1 = r15
            r15 = r18
            r17 = r19
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r0.a(r1)
            com.fossil.t11 r0 = com.fossil.t11.a
            r0 = 0
            r1 = 1
            com.fossil.k60.a(r2, r4, r1, r0)
            int r0 = r23.hashCode()
            com.fossil.yz0.a(r0)
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r1 = r20
            r0.<init>(r1)
            throw r0
        L_0x0109:
            r5 = r21
            int r6 = android.os.Build.VERSION.SDK_INT
            r7 = 23
            if (r6 < r7) goto L_0x013f
            com.fossil.ii0 r6 = com.fossil.ii0.a
            java.lang.String r8 = "android.permission.ACCESS_COARSE_LOCATION"
            java.lang.String[] r8 = new java.lang.String[]{r8}
            boolean r6 = r6.a(r0, r8)
            if (r6 != 0) goto L_0x0130
            com.fossil.ii0 r6 = com.fossil.ii0.a
            java.lang.String r8 = "android.permission.ACCESS_FINE_LOCATION"
            java.lang.String[] r8 = new java.lang.String[]{r8}
            boolean r6 = r6.a(r0, r8)
            if (r6 == 0) goto L_0x012e
            goto L_0x0130
        L_0x012e:
            r6 = 0
            goto L_0x0131
        L_0x0130:
            r6 = 1
        L_0x0131:
            if (r6 != 0) goto L_0x013f
            com.fossil.g60 r0 = new com.fossil.g60
            com.fossil.h60 r1 = com.fossil.h60.LOCATION_PERMISSION_NOT_GRANTED
            r0.<init>(r1)
            r5.a(r3, r0)
            goto L_0x01c8
        L_0x013f:
            int r6 = android.os.Build.VERSION.SDK_INT
            if (r6 < r7) goto L_0x0189
            java.lang.String r6 = "location"
            java.lang.Object r0 = r0.getSystemService(r6)
            r6 = r0
            android.location.LocationManager r6 = (android.location.LocationManager) r6
            if (r6 == 0) goto L_0x017b
            int r0 = android.os.Build.VERSION.SDK_INT
            r7 = 28
            if (r0 < r7) goto L_0x0159
            boolean r1 = r6.isLocationEnabled()
            goto L_0x017c
        L_0x0159:
            java.lang.String r0 = "gps"
            boolean r0 = r6.isProviderEnabled(r0)     // Catch:{ SecurityException -> 0x0161 }
            r7 = r0
            goto L_0x0168
        L_0x0161:
            r0 = move-exception
            com.fossil.wl0 r7 = com.fossil.wl0.h
            r7.a(r0)
            r7 = 0
        L_0x0168:
            java.lang.String r0 = "network"
            boolean r0 = r6.isProviderEnabled(r0)     // Catch:{ SecurityException -> 0x016f }
            goto L_0x0176
        L_0x016f:
            r0 = move-exception
            com.fossil.wl0 r6 = com.fossil.wl0.h
            r6.a(r0)
            r0 = 0
        L_0x0176:
            if (r7 != 0) goto L_0x017c
            if (r0 == 0) goto L_0x017b
            goto L_0x017c
        L_0x017b:
            r1 = 0
        L_0x017c:
            if (r1 != 0) goto L_0x0189
            com.fossil.g60 r0 = new com.fossil.g60
            com.fossil.h60 r1 = com.fossil.h60.LOCATION_SERVICE_NOT_ENABLED
            r0.<init>(r1)
            r5.a(r3, r0)
            goto L_0x01c8
        L_0x0189:
            com.fossil.wl0 r0 = com.fossil.wl0.h
            com.fossil.wr1 r1 = new com.fossil.wr1
            com.fossil.f60$e r4 = com.fossil.f60.e.START_SCAN
            java.lang.String r7 = com.fossil.yz0.a(r4)
            com.fossil.ci1 r8 = com.fossil.ci1.g
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            com.fossil.r51 r6 = com.fossil.r51.J1
            org.json.JSONObject r9 = r22.a()
            org.json.JSONObject r4 = com.fossil.yz0.a(r4, r6, r9)
            com.fossil.r51 r6 = com.fossil.r51.D1
            int r9 = r23.hashCode()
            java.lang.String r9 = com.fossil.yz0.a(r9)
            org.json.JSONObject r16 = com.fossil.yz0.a(r4, r6, r9)
            r12 = 1
            r13 = 0
            r14 = 0
            r15 = 0
            r17 = 448(0x1c0, float:6.28E-43)
            java.lang.String r9 = ""
            java.lang.String r10 = ""
            java.lang.String r11 = ""
            r6 = r1
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r0.a(r1)
            r5.a(r3, r2)
        L_0x01c8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.f60.a(com.fossil.j60, com.fossil.f60$b):void");
    }

    @DexIgnore
    public final void a(b bVar, j60 j60) {
        t11 t11 = t11.a;
        boolean z = false;
        k60.a(j60, 0, 1, null);
        yz0.a(bVar.hashCode());
        if (d() != c.ENABLED) {
            a(bVar, new g60(h60.BLUETOOTH_OFF));
            return;
        }
        synchronized (f) {
            f.put(bVar, j60);
            if (f.size() == 1) {
                z = true;
            }
            i97 i97 = i97.a;
        }
        if (z && ee7.a(u31.g.f(), (Object) true)) {
            ArrayList arrayList = new ArrayList();
            ScanSettings build = new ScanSettings.Builder().setScanMode(2).build();
            ee7.a((Object) build, "ScanSettings.Builder()\n \u2026MODE_LOW_LATENCY).build()");
            a(arrayList, build);
            f fVar = new f();
            c = fVar;
            if (fVar != null) {
                fVar.run();
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        int i2;
        boolean z;
        BluetoothLeScanner bluetoothLeScanner;
        t11 t11 = t11.a;
        yz0.a(bVar.hashCode());
        synchronized (f) {
            f.remove(bVar);
            z = f.size() == 0;
            i97 i97 = i97.a;
        }
        if (z) {
            f fVar = c;
            if (fVar != null) {
                fVar.a = true;
            }
            f fVar2 = c;
            if (fVar2 != null) {
                a.removeCallbacks(fVar2);
            }
            c = null;
            Collection<q60<m60, eu0>> values = d.values();
            ee7.a((Object) values, "deviceInFetchingInformation.values");
            Object[] array = values.toArray(new q60[0]);
            if (array != null) {
                for (Object obj : array) {
                    ((q60) obj).a(new eu0(null, is0.INTERRUPTED, null, 5));
                }
                BluetoothAdapter bluetoothAdapter = b;
                if (!(bluetoothAdapter == null || (bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner()) == null)) {
                    bluetoothLeScanner.stopScan(e);
                }
                sx0.c.a();
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> a() {
        ArrayList arrayList = new ArrayList();
        Context a2 = u31.g.a();
        List<BluetoothDevice> list = null;
        BluetoothManager bluetoothManager = (BluetoothManager) (a2 != null ? a2.getSystemService(PlaceManager.PARAM_BLUETOOTH) : null);
        if (bluetoothManager != null) {
            list = bluetoothManager.getConnectedDevices(7);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @DexIgnore
    public static final /* synthetic */ void a(f60 f60, km1 km1) {
        if (f60.b(km1)) {
            f60.a(km1, 0);
        }
    }

    @DexIgnore
    public final String a(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = yz0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                return "";
            }
            if (d.l.a(bArr[i3]) == d.h) {
                return new String(s97.a(s97.a(bArr, i3, i3 + b2), 1, b2), b21.x.c());
            }
            i2 = b2 + i3;
        }
        return "";
    }

    @DexIgnore
    public final void a(Context context) {
        context.registerReceiver(g, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        context.registerReceiver(h, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    @DexIgnore
    public final void a(c cVar, c cVar2) {
        t11 t11 = t11.a;
        wl0.h.a(new wr1(yz0.a(e.BLUETOOTH_STATE_CHANGED), ci1.f, "", "", "", true, null, null, null, yz0.a(yz0.a(new JSONObject(), r51.A0, yz0.a(cVar)), r51.z0, yz0.a(cVar2)), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE", cVar);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE", cVar2);
        Context a2 = u31.g.a();
        if (a2 != null) {
            qe.a(a2).a(intent);
        }
        int i2 = ct0.b[cVar2.ordinal()];
        if (i2 == 2 || i2 == 3 || i2 == 4) {
            c();
            t11 t112 = t11.a;
            f.size();
        }
        a aVar = j;
        if (aVar != null) {
            aVar.a(this, cVar, cVar2);
        }
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i2, int i3) {
        t11 t11 = t11.a;
        bluetoothDevice.getAddress();
        switch (i2) {
            case 10:
            case 11:
            case 12:
            default:
                switch (i3) {
                    case 10:
                    case 11:
                    case 12:
                    default:
                        wl0 wl0 = wl0.h;
                        String a2 = yz0.a(e.BOND_STATE_CHANGED);
                        ci1 ci1 = ci1.g;
                        String address = bluetoothDevice.getAddress();
                        String str = "";
                        String str2 = address != null ? address : str;
                        JSONObject jSONObject = new JSONObject();
                        r51 r51 = r51.i0;
                        String address2 = bluetoothDevice.getAddress();
                        if (address2 != null) {
                            str = address2;
                        }
                        wl0.a(new wr1(a2, ci1, str2, "", "", true, null, null, null, yz0.a(yz0.a(yz0.a(jSONObject, r51, str), r51.A0, yz0.a(l60.a.b.a(i2))), r51.z0, yz0.a(l60.a.b.a(i3))), 448));
                        Intent intent = new Intent();
                        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
                        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE", bluetoothDevice);
                        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", i2);
                        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", i3);
                        Context a3 = u31.g.a();
                        if (a3 != null) {
                            qe.a(a3).a(intent);
                            return;
                        }
                        return;
                }
        }
    }

    @DexIgnore
    public final void a(km1 km1, int i2) {
        Set<Map.Entry<b, j60>> entrySet;
        synchronized (f) {
            entrySet = f.entrySet();
            ee7.a((Object) entrySet, "activeLeScanCallbacks.entries");
            i97 i97 = i97.a;
        }
        for (T t : entrySet) {
            if (((j60) t.getValue()).a(km1)) {
                k.a((b) t.getKey(), km1, i2);
            }
        }
    }

    @DexIgnore
    public final void a(b bVar, km1 km1, int i2) {
        t11 t11 = t11.a;
        km1.t.getMacAddress();
        km1.t.getSerialNumber();
        wl0.h.a(new wr1(yz0.a(e.DEVICE_FOUND), ci1.g, "", "", "", true, null, null, null, yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D1, yz0.a(bVar.hashCode())), r51.k1, a(km1)), r51.b, Integer.valueOf(i2)), 448));
        bVar.onDeviceFound(km1, i2);
    }

    @DexIgnore
    public final void a(g60 g60) {
        Set<b> keySet;
        synchronized (f) {
            keySet = f.keySet();
            ee7.a((Object) keySet, "activeLeScanCallbacks.keys");
            i97 i97 = i97.a;
        }
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            k.a(it.next(), g60);
        }
    }

    @DexIgnore
    public final void a(b bVar, g60 g60) {
        wl0.h.a(new wr1(yz0.a(e.START_SCAN_FAILED), ci1.g, "", "", "", false, null, null, null, yz0.a(yz0.a(new JSONObject(), r51.D1, String.valueOf(bVar.hashCode())), r51.E1, g60.a()), 448));
        t11 t11 = t11.a;
        yz0.a(bVar.hashCode());
        g60.a(2);
        bVar.onStartScanFailed(g60);
    }

    @DexIgnore
    public final JSONObject a(km1 km1) {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.j0, km1.t.getSerialNumber()), r51.l1, yz0.a(km1.u)), r51.i0, km1.t.getMacAddress()), r51.m1, yz0.a(l60.a.b.a(km1.c.getBondState())));
    }
}
