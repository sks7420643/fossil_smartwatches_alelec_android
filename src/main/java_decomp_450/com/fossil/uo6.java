package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo6 implements Factory<to6> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<um5> b;
    @DexIgnore
    public /* final */ Provider<rm5> c;

    @DexIgnore
    public uo6(Provider<PortfolioApp> provider, Provider<um5> provider2, Provider<rm5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static uo6 a(Provider<PortfolioApp> provider, Provider<um5> provider2, Provider<rm5> provider3) {
        return new uo6(provider, provider2, provider3);
    }

    @DexIgnore
    public static to6 a(PortfolioApp portfolioApp, um5 um5, rm5 rm5) {
        return new to6(portfolioApp, um5, rm5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public to6 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
