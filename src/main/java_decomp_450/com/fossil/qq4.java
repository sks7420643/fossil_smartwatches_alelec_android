package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq4 extends go5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<i35> g;
    @DexIgnore
    public sq4 h;
    @DexIgnore
    public String i;
    @DexIgnore
    public boolean j; // = true;
    @DexIgnore
    public vq4 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qq4.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final qq4 a(String str, boolean z) {
            qq4 qq4 = new qq4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putBoolean("challenge_status_extra", z);
            qq4.setArguments(bundle);
            return qq4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qq4 a;

        @DexIgnore
        public b(qq4 qq4) {
            this.a = qq4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ qq4 a;

        @DexIgnore
        public c(qq4 qq4) {
            this.a = qq4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            i35 i35 = (i35) qq4.a(this.a).a();
            if (!(i35 == null || (flexibleTextView = i35.q) == null)) {
                flexibleTextView.setVisibility(8);
            }
            qq4.d(this.a).a(this.a.i, this.a.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ qq4 a;

        @DexIgnore
        public d(qq4 qq4) {
            this.a = qq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            i35 i35 = (i35) qq4.a(this.a).a();
            if (i35 != null) {
                SwipeRefreshLayout swipeRefreshLayout = i35.u;
                ee7.a((Object) swipeRefreshLayout, "swipe");
                ee7.a((Object) bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ qq4 a;

        @DexIgnore
        public e(qq4 qq4) {
            this.a = qq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            vq4 c = qq4.c(this.a);
            ee7.a((Object) list, "it");
            c.a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ qq4 a;

        @DexIgnore
        public f(qq4 qq4) {
            this.a = qq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            if (r87.getFirst().booleanValue()) {
                i35 i35 = (i35) qq4.a(this.a).a();
                if (i35 != null) {
                    FlexibleTextView flexibleTextView = i35.q;
                    ee7.a((Object) flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, ig5.a(activity, 2131886227), 1).show();
            }
        }
    }

    /*
    static {
        String simpleName = qq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCMemberInChallengeFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(qq4 qq4) {
        qw6<i35> qw6 = qq4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ vq4 c(qq4 qq4) {
        vq4 vq4 = qq4.p;
        if (vq4 != null) {
            return vq4;
        }
        ee7.d("memberAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ sq4 d(qq4 qq4) {
        sq4 sq4 = qq4.h;
        if (sq4 != null) {
            return sq4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        vq4 vq4 = new vq4();
        this.p = vq4;
        if (vq4 != null) {
            vq4.setHasStableIds(true);
            qw6<i35> qw6 = this.g;
            if (qw6 != null) {
                i35 a2 = qw6.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setHasFixedSize(true);
                    vq4 vq42 = this.p;
                    if (vq42 != null) {
                        recyclerView.setAdapter(vq42);
                        a2.r.setOnClickListener(new b(this));
                        a2.u.setOnRefreshListener(new c(this));
                        return;
                    }
                    ee7.d("memberAdapter");
                    throw null;
                }
                return;
            }
            ee7.d("binding");
            throw null;
        }
        ee7.d("memberAdapter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        sq4 sq4 = this.h;
        if (sq4 != null) {
            sq4.b().a(getViewLifecycleOwner(), new d(this));
            sq4 sq42 = this.h;
            if (sq42 != null) {
                sq42.c().a(getViewLifecycleOwner(), new e(this));
                sq4 sq43 = this.h;
                if (sq43 != null) {
                    sq43.a().a(getViewLifecycleOwner(), new f(this));
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().w().a(this);
        rj4 rj4 = this.f;
        String str = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(sq4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ngeViewModel::class.java)");
            this.h = (sq4) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                str = arguments.getString("challenge_id_extra");
            }
            this.i = str;
            Bundle arguments2 = getArguments();
            this.j = arguments2 != null ? arguments2.getBoolean("challenge_status_extra") : true;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        i35 i35 = (i35) qb.a(layoutInflater, 2131558583, viewGroup, false, a1());
        this.g = new qw6<>(this, i35);
        ee7.a((Object) i35, "binding");
        return i35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        sq4 sq4 = this.h;
        if (sq4 != null) {
            sq4.a(this.i, this.j);
            f1();
            g1();
            return;
        }
        ee7.d("viewModel");
        throw null;
    }
}
