package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r36 extends m36 {
    @DexIgnore
    public a06 e;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<String, Boolean, Parcelable>> i; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ zd<String> l;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> m;
    @DexIgnore
    public /* final */ zd<List<WatchApp>> n;
    @DexIgnore
    public /* final */ zd<v87<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ n36 p;
    @DexIgnore
    public /* final */ CategoryRepository q;
    @DexIgnore
    public /* final */ ch5 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1", f = "WatchAppsPresenter.kt", l = {239}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ r36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return r36.f(this.this$0.this$0).g(this.this$0.$id);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(r36 r36, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = r36;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$id, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            boolean z2;
            Object a2 = nb7.a();
            int i = this.label;
            Parcelable parcelable = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                z = cf5.c.f(this.$id);
                if (z) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = null;
                    this.Z$0 = z;
                    this.label = 1;
                    obj = vh7.a(a3, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                    z2 = z;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.i.a(new v87(this.$id, pb7.a(z), parcelable));
                return i97.a;
            } else if (i == 1) {
                z2 = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) obj;
            z = z2;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.i.a(new v87(this.$id, pb7.a(z), parcelable));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore
        public c(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.p.d(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1", f = "WatchAppsPresenter.kt", l = {83}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ r36 $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.r36$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.r36$d$a$a  reason: collision with other inner class name */
            public static final class C0160a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0160a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0160a aVar = new C0160a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                    return ((C0160a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(r36 r36, fb7 fb7) {
                super(2, fb7);
                this.$this_run = r36;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$this_run, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = qj7.b();
                    C0160a aVar = new C0160a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (!list.isEmpty()) {
                    this.$this_run.g.a((Object) ((Category) list.get(0)).getId());
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(WatchApp watchApp) {
            if (watchApp != null) {
                ik7 unused = this.a.b(watchApp.getWatchappId());
            }
            String str = (String) this.a.g.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transform from selected WatchApp to category");
            sb.append(" currentCategory=");
            sb.append(str);
            sb.append(" watchAppCategories=");
            sb.append(watchApp != null ? watchApp.getCategories() : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (watchApp != null) {
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.g.a((Object) categories.get(0));
                } else {
                    this.a.g.a((Object) str);
                }
            } else {
                r36 r36 = this.a;
                ik7 unused2 = xh7.b(r36.e(), null, null, new a(r36, null), 3, null);
            }
            return this.a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<v87<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore
        public e(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<String, Boolean, ? extends Parcelable> v87) {
            String str;
            String str2;
            String str3;
            if (v87 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "onLiveDataChanged setting of " + v87.getFirst() + " isSettingRequired " + v87.getSecond().booleanValue() + " setting " + ((Parcelable) v87.getThird()) + ' ');
                String str4 = "";
                if (v87.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) v87.getThird();
                    if (parcelable == null) {
                        str = str4;
                        str4 = cf5.c.a(v87.getFirst());
                    } else {
                        try {
                            String first = v87.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    str2 = ig5.a(PortfolioApp.g0.c(), 2131886357);
                                    ee7.a((Object) str2, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = str4;
                            } else if (hashCode != 1223440372) {
                                str2 = str4;
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    Iterator<WeatherLocationWrapper> it = locations.iterator();
                                    str = str4;
                                    while (it.hasNext()) {
                                        try {
                                            WeatherLocationWrapper next = it.next();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < w97.a((List) locations)) {
                                                StringBuilder sb2 = new StringBuilder();
                                                sb2.append(next != null ? next.getName() : null);
                                                sb2.append("; ");
                                                str3 = sb2.toString();
                                            } else {
                                                str3 = next != null ? next.getName() : null;
                                            }
                                            sb.append(str3);
                                            str = sb.toString();
                                        } catch (Exception e) {
                                            e = e;
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            local2.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.a.p.a(true, v87.getFirst(), str4, str);
                                            return;
                                        }
                                    }
                                }
                                str = str4;
                            }
                            str = str2;
                        } catch (Exception e2) {
                            e = e2;
                            str = str4;
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            local22.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.a.p.a(true, v87.getFirst(), str4, str);
                            return;
                        }
                    }
                    this.a.p.a(true, v87.getFirst(), str4, str);
                    return;
                }
                this.a.p.a(false, v87.getFirst(), str4, null);
                WatchApp a2 = r36.f(this.a).h().a();
                if (a2 != null) {
                    n36 i = this.a.p;
                    String a3 = ig5.a(PortfolioApp.g0.c(), a2.getDescriptionKey(), a2.getDescription());
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    i.H(a3);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<List<? extends WatchApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore
        public f(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.a.p.v(list);
                WatchApp a2 = r36.f(this.a).h().a();
                if (a2 != null) {
                    this.a.p.b(a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore
        public g(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<WatchApp>> apply(String str) {
            T t;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            a06 f = r36.f(this.a);
            ee7.a((Object) str, "category");
            List<WatchApp> c = f.c(str);
            ArrayList arrayList = new ArrayList();
            DianaPreset a2 = r36.f(this.a).a().a();
            WatchApp a3 = r36.f(this.a).h().a();
            String watchappId = a3 != null ? a3.getWatchappId() : null;
            if (a2 != null) {
                for (WatchApp watchApp : c) {
                    Iterator<T> it = a2.getWatchapps().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        T t2 = t;
                        boolean z = true;
                        if (!ee7.a((Object) t2.getId(), (Object) watchApp.getWatchappId()) || !(!ee7.a((Object) t2.getId(), (Object) watchappId))) {
                            z = false;
                            continue;
                        }
                        if (z) {
                            break;
                        }
                    }
                    if (t == null || ee7.a((Object) watchApp.getWatchappId(), (Object) "empty")) {
                        arrayList.add(watchApp);
                    }
                }
            }
            this.a.h.a(arrayList);
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1", f = "WatchAppsPresenter.kt", l = {196}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ r36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Category>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(r36 r36, fb7 fb7) {
            super(2, fb7);
            this.this$0 = r36;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) obj) {
                if (!r36.f(this.this$0).c(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.j.clear();
            this.this$0.j.addAll(arrayList);
            this.this$0.p.a((List<Category>) this.this$0.j);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<WatchApp> {
        @DexIgnore
        public /* final */ /* synthetic */ r36 a;

        @DexIgnore
        public i(r36 r36) {
            this.a = r36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            this.a.f.a(watchApp);
            if (watchApp != null) {
                this.a.p.k(cf5.c.e(watchApp.getWatchappId()));
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public r36(n36 n36, CategoryRepository categoryRepository, ch5 ch5) {
        ee7.b(n36, "mView");
        ee7.b(categoryRepository, "mCategoryRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.p = n36;
        this.q = categoryRepository;
        this.r = ch5;
        new Gson();
        LiveData<String> b2 = ge.b(this.f, new d(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.k = b2;
        this.l = new c(this);
        LiveData<List<WatchApp>> b3 = ge.b(this.k, new g(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = b3;
        this.n = new f(this);
        this.o = new e(this);
    }

    @DexIgnore
    public static final /* synthetic */ a06 f(r36 r36) {
        a06 a06 = r36.e;
        if (a06 != null) {
            return a06;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final ik7 b(String str) {
        return xh7.b(e(), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        this.k.a(this.l);
        this.m.a(this.n);
        this.i.a(this.o);
        if (this.j.isEmpty()) {
            ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
        } else {
            this.p.a((List<Category>) this.j);
        }
        a06 a06 = this.e;
        if (a06 != null) {
            LiveData<WatchApp> h2 = a06.h();
            n36 n36 = this.p;
            if (n36 != null) {
                h2.a((o36) n36, new i(this));
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        a06 a06 = this.e;
        if (a06 != null) {
            LiveData<WatchApp> h2 = a06.h();
            n36 n36 = this.p;
            if (n36 != null) {
                h2.a((o36) n36);
                this.h.b(this.n);
                this.g.b(this.l);
                this.i.b(this.o);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void h() {
        T t;
        String str;
        T t2;
        String str2;
        String id;
        a06 a06 = this.e;
        T t3 = null;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            String str3 = "empty";
            if (a2 != null) {
                Iterator<T> it = a2.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                        break;
                    }
                }
                T t4 = t;
                if (t4 == null || (str = t4.getId()) == null) {
                    str = str3;
                }
                Iterator<T> it2 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    if (ee7.a((Object) t2.getPosition(), (Object) "middle")) {
                        break;
                    }
                }
                T t5 = t2;
                if (t5 == null || (str2 = t5.getId()) == null) {
                    str2 = str3;
                }
                Iterator<T> it3 = a2.getWatchapps().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    T next = it3.next();
                    if (ee7.a((Object) next.getPosition(), (Object) "bottom")) {
                        t3 = next;
                        break;
                    }
                }
                T t6 = t3;
                if (!(t6 == null || (id = t6.getId()) == null)) {
                    str3 = id;
                }
                this.p.a(str, str2, str3);
                return;
            }
            this.p.a(str3, str3, str3);
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0051, code lost:
        if (r1 != null) goto L_0x0056;
     */
    @DexIgnore
    @Override // com.fossil.m36
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r6 = this;
            com.fossil.a06 r0 = r6.e
            java.lang.String r1 = "mDianaCustomizeViewModel"
            r2 = 0
            if (r0 == 0) goto L_0x008a
            androidx.lifecycle.LiveData r0 = r0.h()
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.model.diana.WatchApp r0 = (com.portfolio.platform.data.model.diana.WatchApp) r0
            if (r0 == 0) goto L_0x0089
            java.lang.String r3 = r0.component1()
            com.fossil.a06 r4 = r6.e
            if (r4 == 0) goto L_0x0085
            androidx.lifecycle.MutableLiveData r1 = r4.a()
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r1 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r1
            if (r1 == 0) goto L_0x0054
            java.util.ArrayList r1 = r1.getWatchapps()
            if (r1 == 0) goto L_0x0054
            java.util.Iterator r1 = r1.iterator()
        L_0x0031:
            boolean r4 = r1.hasNext()
            if (r4 == 0) goto L_0x0049
            java.lang.Object r4 = r1.next()
            r5 = r4
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r5 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r5
            java.lang.String r5 = r5.getId()
            boolean r5 = com.fossil.ee7.a(r3, r5)
            if (r5 == 0) goto L_0x0031
            r2 = r4
        L_0x0049:
            com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting r2 = (com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting) r2
            if (r2 == 0) goto L_0x0054
            java.lang.String r1 = r2.getSettings()
            if (r1 == 0) goto L_0x0054
            goto L_0x0056
        L_0x0054:
            java.lang.String r1 = ""
        L_0x0056:
            java.lang.String r0 = r0.getWatchappId()
            int r2 = r0.hashCode()
            r3 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
            if (r2 == r3) goto L_0x0077
            r3 = 1223440372(0x48ec37f4, float:483775.62)
            if (r2 == r3) goto L_0x0069
            goto L_0x0089
        L_0x0069:
            java.lang.String r2 = "weather"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0089
            com.fossil.n36 r0 = r6.p
            r0.R(r1)
            goto L_0x0089
        L_0x0077:
            java.lang.String r2 = "commute-time"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0089
            com.fossil.n36 r0 = r6.p
            r0.c(r1)
            goto L_0x0089
        L_0x0085:
            com.fossil.ee7.d(r1)
            throw r2
        L_0x0089:
            return
        L_0x008a:
            com.fossil.ee7.d(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r36.i():void");
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void j() {
        a06 a06 = this.e;
        if (a06 != null) {
            WatchApp a2 = a06.h().a();
            if (a2 != null) {
                String component1 = a2.component1();
                if (cf5.c.e(component1)) {
                    this.p.f(component1);
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void k() {
        this.p.a(this);
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void a(a06 a06) {
        ee7.b(a06, "viewModel");
        this.e = a06;
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void a(Category category) {
        ee7.b(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.g.a(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void a(String str) {
        T t;
        Object obj;
        ee7.b(str, "watchappId");
        a06 a06 = this.e;
        if (a06 != null) {
            WatchApp d2 = a06.d(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + d2);
            if (d2 != null) {
                a06 a062 = this.e;
                if (a062 != null) {
                    DianaPreset a2 = a062.a().a();
                    if (a2 != null) {
                        DianaPreset clone = a2.clone();
                        ArrayList arrayList = new ArrayList();
                        a06 a063 = this.e;
                        if (a063 != null) {
                            String a3 = a063.i().a();
                            if (a3 != null) {
                                ee7.a((Object) a3, "mDianaCustomizeViewModel\u2026ctedWatchAppPos().value!!");
                                String str2 = a3;
                                a06 a064 = this.e;
                                if (a064 != null) {
                                    if (!a064.i(str)) {
                                        Iterator<DianaPresetWatchAppSetting> it = clone.getWatchapps().iterator();
                                        while (it.hasNext()) {
                                            DianaPresetWatchAppSetting next = it.next();
                                            if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                                                arrayList.add(new DianaPresetWatchAppSetting(str2, str, next.getLocalUpdateAt()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        Iterator it2 = arrayList.iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                obj = null;
                                                break;
                                            }
                                            obj = it2.next();
                                            if (ee7.a((Object) ((DianaPresetWatchAppSetting) obj).getPosition(), (Object) str2)) {
                                                break;
                                            }
                                        }
                                        if (obj == null) {
                                            SimpleDateFormat simpleDateFormat = zd5.m.get();
                                            if (simpleDateFormat != null) {
                                                String format = simpleDateFormat.format(new Date());
                                                ee7.a((Object) format, "DateHelper.LOCAL_DATE_SP\u2026AT.get()!!.format(Date())");
                                                arrayList.add(new DianaPresetWatchAppSetting(str2, str, format));
                                            } else {
                                                ee7.a();
                                                throw null;
                                            }
                                        }
                                        clone.getWatchapps().clear();
                                        clone.getWatchapps().addAll(arrayList);
                                        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditPresenter", "Update current preset=" + clone);
                                        a06 a065 = this.e;
                                        if (a065 != null) {
                                            a065.a(clone);
                                        } else {
                                            ee7.d("mDianaCustomizeViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it3 = a2.getWatchapps().iterator();
                                        while (true) {
                                            if (!it3.hasNext()) {
                                                t = null;
                                                break;
                                            }
                                            t = it3.next();
                                            if (ee7.a((Object) t.getId(), (Object) str)) {
                                                break;
                                            }
                                        }
                                        T t2 = t;
                                        if (t2 != null) {
                                            a06 a066 = this.e;
                                            if (a066 != null) {
                                                a066.k(t2.getPosition());
                                            } else {
                                                ee7.d("mDianaCustomizeViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = d2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.r.l()) {
                                        this.r.o(true);
                                        this.p.f("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                ee7.d("mDianaCustomizeViewModel");
                                throw null;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.d("mDianaCustomizeViewModel");
                        throw null;
                    }
                    return;
                }
                ee7.d("mDianaCustomizeViewModel");
                throw null;
            }
            return;
        }
        ee7.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.m36
    public void a(String str, Parcelable parcelable) {
        T t;
        ee7.b(str, "watchAppId");
        ee7.b(parcelable, MicroAppSetting.SETTING);
        a06 a06 = this.e;
        if (a06 != null) {
            DianaPreset a2 = a06.a().a();
            if (a2 != null) {
                DianaPreset clone = a2.clone();
                Iterator<T> it = clone.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.getId(), (Object) str)) {
                        break;
                    }
                }
                T t2 = t;
                be4 be4 = new be4();
                be4.b(new od5());
                Gson a3 = be4.a();
                if (t2 != null) {
                    t2.setSettings(a3.a(parcelable));
                }
                FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "update current preset with new setting " + parcelable + " of " + str);
                a06 a062 = this.e;
                if (a062 != null) {
                    a062.a(clone);
                } else {
                    ee7.d("mDianaCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            ee7.d("mDianaCustomizeViewModel");
            throw null;
        }
    }
}
