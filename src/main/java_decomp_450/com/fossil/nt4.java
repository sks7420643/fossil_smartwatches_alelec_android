package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt4 {
    @DexIgnore
    public static final List<dn4> a(List<? extends Date> list, Date date) {
        String str;
        ee7.b(list, "$this$toBottomDialogModels");
        ee7.b(date, "currentDate");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            boolean d = zd5.d(t, date);
            String u = zd5.u(t);
            long time = (t.getTime() - date.getTime()) / ((long) 60000);
            if (d) {
                we7 we7 = we7.a;
                String string = PortfolioApp.g0.c().getResources().getString(2131886339);
                ee7.a((Object) string, "PortfolioApp.instance.re\u2026l__AtTimeInNumberMinutes)");
                str = String.format(string, Arrays.copyOf(new Object[]{u, Long.valueOf(time)}, 2));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            } else {
                we7 we72 = we7.a;
                String string2 = PortfolioApp.g0.c().getResources().getString(2131886340);
                ee7.a((Object) string2, "PortfolioApp.instance.re\u2026orrowTimeInNumberMinutes)");
                str = String.format(string2, Arrays.copyOf(new Object[]{u, Long.valueOf(time)}, 2));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            }
            arrayList.add(new dn4(str, t, false));
        }
        if (!arrayList.isEmpty()) {
            ((dn4) arrayList.get(0)).a(true);
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<xn4> b(List<un4> list, List<jn4> list2) {
        T t;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        ee7.b(list, "$this$toFriendsIn");
        ee7.b(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        if (list.size() == list2.size() - 1) {
            for (T t2 : list) {
                String d = t2.d();
                String b = t2.b();
                if (b != null) {
                    str4 = b;
                } else {
                    str4 = "";
                }
                String e = t2.e();
                if (e != null) {
                    str5 = e;
                } else {
                    str5 = "";
                }
                String i = t2.i();
                String h = t2.h();
                if (h != null) {
                    str6 = h;
                } else {
                    str6 = "";
                }
                arrayList.add(new xn4(d, str4, str5, i, str6));
            }
        } else {
            for (T t3 : list) {
                Iterator<T> it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) t.d(), (Object) t3.d())) {
                        break;
                    }
                }
                if (t != null) {
                    String d2 = t3.d();
                    String b2 = t3.b();
                    if (b2 != null) {
                        str = b2;
                    } else {
                        str = "";
                    }
                    String e2 = t3.e();
                    if (e2 != null) {
                        str2 = e2;
                    } else {
                        str2 = "";
                    }
                    String i2 = t3.i();
                    String h2 = t3.h();
                    if (h2 != null) {
                        str3 = h2;
                    } else {
                        str3 = "";
                    }
                    arrayList.add(new xn4(d2, str, str2, i2, str3));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<Object> c(List<jo4> list) {
        ee7.b(list, "$this$toRecommendedChallenges");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        int i = 0;
        boolean z = false;
        for (T t : list) {
            int i2 = i + 1;
            if (i >= 0) {
                T t2 = t;
                if (i != 0) {
                    if (!z && ee7.a((Object) t2.a(), (Object) "available-challenge")) {
                        String a = ig5.a(PortfolioApp.g0.c(), 2131886240);
                        ee7.a((Object) a, "LanguageHelper.getString\u2026tle__AvailableChallenges)");
                        arrayList.add(a);
                    }
                    arrayList.add(t2);
                    i = i2;
                } else if (ee7.a((Object) t2.a(), (Object) "pending-invitation")) {
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886241);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026Title__PendingInvitation)");
                    arrayList.add(a2);
                    arrayList.add(t2);
                    i = i2;
                } else {
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886240);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026tle__AvailableChallenges)");
                    arrayList.add(a3);
                }
                z = true;
                arrayList.add(t2);
                i = i2;
            } else {
                w97.c();
                throw null;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<mo4> d(List<un4> list, List<jn4> list2) {
        T t;
        ee7.b(list, "$this$toUnInvitedFriends");
        ee7.b(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) t.d(), (Object) t2.d())) {
                    break;
                }
            }
            if (t == null) {
                arrayList.add(new mo4(t2.d(), t2.i(), t2.b(), t2.e(), t2.h(), false, 32, null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<oo4> d(List<yn4> list) {
        T t;
        T t2;
        ee7.b(list, "$this$toUIHistories");
        ArrayList arrayList = new ArrayList();
        for (T t3 : list) {
            Iterator<T> it = t3.k().iterator();
            while (true) {
                t = null;
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                Integer h = t2.h();
                boolean z = true;
                if (h == null || 1 != h.intValue()) {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            T t4 = t2;
            Iterator<T> it2 = t3.k().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                T next = it2.next();
                if (ee7.a((Object) PortfolioApp.g0.c().w(), (Object) next.d())) {
                    t = next;
                    break;
                }
            }
            arrayList.add(new oo4(new mn4(t3.g(), t3.q(), t3.h(), t3.d(), t3.j(), t3.i(), t3.m(), t3.f(), t3.o(), t3.e(), t3.l(), t3.s(), t3.n(), null, t3.b(), t3.r(), null, 65536, null), t4, t, t3.t()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<xn4> b(List<jn4> list) {
        ee7.b(list, "$this$toFriendUI");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            String d = t.d();
            String c = t.c();
            String str = c != null ? c : "";
            String e = t.e();
            String str2 = e != null ? e : "";
            String i = t.i();
            String g = t.g();
            if (g == null) {
                g = "";
            }
            arrayList.add(new xn4(d, str, str2, i, g));
        }
        return arrayList;
    }

    @DexIgnore
    public static final qn4 b(mn4 mn4) {
        ee7.b(mn4, "$this$toDraft");
        String f = mn4.f();
        String g = mn4.g();
        if (g == null) {
            g = "";
        }
        String c = mn4.c();
        String u = mn4.u();
        if (u == null) {
            u = "activity_reach_goal";
        }
        String j = mn4.j();
        if (j == null) {
            j = "public_with_friend";
        }
        Integer t = mn4.t();
        int intValue = t != null ? t.intValue() : 15000;
        Integer d = mn4.d();
        return new qn4(f, g, c, u, j, intValue, d != null ? d.intValue() : 259200, null, null, false, null, 1920, null);
    }

    @DexIgnore
    public static final r87<List<xn4>, List<xn4>> c(List<un4> list, List<jn4> list2) {
        T t;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        ee7.b(list, "$this$toPairFriendInAndMember");
        ee7.b(list2, "joinedPlayers");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : list2) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) t.d(), (Object) t2.d())) {
                    break;
                }
            }
            if (t == null) {
                String d = t2.d();
                String c = t2.c();
                if (c != null) {
                    str4 = c;
                } else {
                    str4 = "";
                }
                String e = t2.e();
                if (e != null) {
                    str5 = e;
                } else {
                    str5 = "";
                }
                String i = t2.i();
                String g = t2.g();
                if (g != null) {
                    str6 = g;
                } else {
                    str6 = "";
                }
                arrayList2.add(new xn4(d, str4, str5, i, str6));
            } else {
                String d2 = t2.d();
                String c2 = t2.c();
                if (c2 != null) {
                    str = c2;
                } else {
                    str = "";
                }
                String e2 = t2.e();
                if (e2 != null) {
                    str2 = e2;
                } else {
                    str2 = "";
                }
                String i2 = t2.i();
                String g2 = t2.g();
                if (g2 != null) {
                    str3 = g2;
                } else {
                    str3 = "";
                }
                arrayList.add(new xn4(d2, str, str2, i2, str3));
            }
        }
        return w87.a(arrayList, arrayList2);
    }

    @DexIgnore
    public static final r87<String[], Long[]> a(List<? extends Date> list) {
        String str;
        ee7.b(list, "$this$toDateStringArray");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (T t : list) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(t);
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            Date time = instance.getTime();
            ee7.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (zd5.d(t, new Date())) {
                str = ig5.a(PortfolioApp.g0.c(), 2131886619);
            } else {
                str = zd5.b((Date) t);
            }
            ee7.a((Object) str, LegacyTokenHelper.TYPE_STRING);
            arrayList.add(str);
            arrayList2.add(Long.valueOf(time2));
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Object[] array2 = arrayList2.toArray(new Long[0]);
            if (array2 != null) {
                return new r87<>(array, array2);
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public static final List<Object> a(List<jn4> list, List<jn4> list2) {
        ee7.b(list, "$this$toAllPlayerWithHeader");
        ArrayList arrayList = new ArrayList();
        if (list2 == null || list2.isEmpty()) {
            arrayList.addAll(list);
        } else {
            String a = ig5.a(PortfolioApp.g0.c(), 2131886314);
            ee7.a((Object) a, "LanguageHelper.getString\u2026rd_Subtitle__Top3Players)");
            arrayList.add(a);
            arrayList.addAll(list);
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886313);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026btitle__CloseCompetitors)");
            arrayList.add(a2);
            arrayList.addAll(list2);
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(qn4 qn4, qn4 qn42) {
        ee7.b(qn4, "$this$isEditableFieldTheSame");
        ee7.b(qn42, "draft");
        if (!ee7.a((Object) qn4.e(), (Object) qn42.e())) {
            return false;
        }
        String a = qn4.a();
        String str = "";
        if (a == null) {
            a = str;
        }
        String a2 = qn42.a();
        if (a2 != null) {
            str = a2;
        }
        return !(ee7.a(a, str) ^ true) && qn4.i() == qn42.i() && qn4.b() == qn42.b();
    }

    @DexIgnore
    public static final mn4 a(mn4 mn4) {
        ee7.b(mn4, "$this$clone");
        return new mn4(mn4.f(), mn4.u(), mn4.g(), mn4.c(), mn4.i(), mn4.h(), mn4.m(), mn4.e(), mn4.t(), mn4.d(), mn4.j(), mn4.w(), mn4.p(), mn4.s(), mn4.b(), mn4.v(), mn4.a());
    }

    @DexIgnore
    public static final yn4 a(yn4 yn4, List<jn4> list) {
        ee7.b(yn4, "$this$clone");
        ee7.b(list, "list");
        return new yn4(yn4.g(), yn4.q(), yn4.h(), yn4.d(), yn4.j(), yn4.i(), yn4.m(), yn4.f(), yn4.o(), yn4.e(), yn4.l(), yn4.s(), yn4.n(), list, yn4.p(), yn4.c(), yn4.b(), yn4.r(), yn4.a(), false, 524288, null);
    }
}
