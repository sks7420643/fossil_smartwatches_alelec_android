package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l81 extends zk1 {
    @DexIgnore
    public m60 U;

    @DexIgnore
    public l81(ri1 ri1, en0 en0, short s, String str) {
        super(ri1, en0, wm0.c, s, oa7.b(w87.a(xf0.SKIP_ERASE, true), w87.a(xf0.NUMBER_OF_FILE_REQUIRED, 1), w87.a(xf0.ERASE_CACHE_FILE_BEFORE_GET, true)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
        this.U = new m60(ri1.d(), ri1.u, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void a(ArrayList<bi1> arrayList) {
        a(((zk0) this).v);
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void c(bi1 bi1) {
        is0 is0;
        super.c(bi1);
        try {
            this.U = m60.a((m60) az0.d.a(bi1.e), this.U.getName(), this.U.getMacAddress(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262140);
            is0 = is0.SUCCESS;
        } catch (f41 e) {
            wl0.h.a(e);
            is0 = is0.UNSUPPORTED_FORMAT;
        }
        ((zk0) this).v = eu0.a(((zk0) this).v, null, is0, null, 5);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public Object d() {
        return this.U;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public JSONObject k() {
        return yz0.a(super.k(), r51.a, this.U.a());
    }
}
