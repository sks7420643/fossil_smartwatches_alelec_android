package com.fossil;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy6 extends MetricAffectingSpan {
    @DexIgnore
    public Typeface a;

    @DexIgnore
    public iy6(Typeface typeface) {
        ee7.b(typeface, "typeface");
        this.a = typeface;
    }

    @DexIgnore
    public final void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
    }

    @DexIgnore
    public void updateDrawState(TextPaint textPaint) {
        if (textPaint != null) {
            Typeface typeface = this.a;
            if (typeface != null) {
                a(textPaint, typeface);
            } else {
                ee7.d("typeface");
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void updateMeasureState(TextPaint textPaint) {
        ee7.b(textPaint, "textPaint");
        Typeface typeface = this.a;
        if (typeface != null) {
            a(textPaint, typeface);
        } else {
            ee7.d("typeface");
            throw null;
        }
    }
}
