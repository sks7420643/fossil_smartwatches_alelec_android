package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum r90 {
    SUCCESS((byte) 0),
    FAILED((byte) 1),
    NOT_FOUND((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public r90(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
