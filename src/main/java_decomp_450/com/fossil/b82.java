package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b82 extends l62 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ x12 b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public b82(Intent intent, x12 x12, int i) {
        this.a = intent;
        this.b = x12;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.l62
    public final void a() {
        Intent intent = this.a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
