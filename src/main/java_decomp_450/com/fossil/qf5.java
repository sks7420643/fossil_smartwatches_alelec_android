package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qf5 implements Cloneable {
    @DexIgnore
    public int a;
    @DexIgnore
    public byte[] b;

    @DexIgnore
    public qf5(int i, byte[] bArr) {
        if (bArr != null) {
            this.a = i;
            byte[] bArr2 = new byte[bArr.length];
            this.b = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    public void b(byte[] bArr) {
        if (bArr != null) {
            byte[] bArr2 = new byte[bArr.length];
            this.b = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    @Override // java.lang.Object
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        byte[] bArr = this.b;
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        try {
            return new qf5(this.a, bArr2);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        return new java.lang.String(r3.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        return new java.lang.String(r3.b, "iso-8859-1");
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0018 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String d() {
        /*
            r3 = this;
            int r0 = r3.a
            if (r0 != 0) goto L_0x000c
            java.lang.String r0 = new java.lang.String
            byte[] r1 = r3.b
            r0.<init>(r1)
            return r0
        L_0x000c:
            java.lang.String r0 = com.fossil.of5.a(r0)     // Catch:{ Exception -> 0x0018 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0018 }
            byte[] r2 = r3.b     // Catch:{ Exception -> 0x0018 }
            r1.<init>(r2, r0)     // Catch:{ Exception -> 0x0018 }
            return r1
        L_0x0018:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0022 }
            byte[] r1 = r3.b     // Catch:{ Exception -> 0x0022 }
            java.lang.String r2 = "iso-8859-1"
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0022 }
            return r0
        L_0x0022:
            java.lang.String r0 = new java.lang.String
            byte[] r1 = r3.b
            r0.<init>(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qf5.d():java.lang.String");
    }

    @DexIgnore
    public byte[] f() {
        byte[] bArr = this.b;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public qf5(byte[] bArr) {
        this(106, bArr);
    }

    @DexIgnore
    public qf5(String str) {
        try {
            this.b = str.getBytes(uv.PROTOCOL_CHARSET);
            this.a = 106;
        } catch (UnsupportedEncodingException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "Default encoding must be supported=" + e);
        }
    }
}
