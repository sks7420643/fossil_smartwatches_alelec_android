package com.fossil;

import android.graphics.Matrix;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xk extends wk {
    @DexIgnore
    @Override // com.fossil.tk, com.fossil.yk
    public void a(View view, float f) {
        view.setTransitionAlpha(f);
    }

    @DexIgnore
    @Override // com.fossil.tk, com.fossil.yk
    public float b(View view) {
        return view.getTransitionAlpha();
    }

    @DexIgnore
    @Override // com.fossil.uk, com.fossil.yk
    public void c(View view, Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }

    @DexIgnore
    @Override // com.fossil.wk, com.fossil.yk
    public void a(View view, int i) {
        view.setTransitionVisibility(i);
    }

    @DexIgnore
    @Override // com.fossil.uk, com.fossil.yk
    public void b(View view, Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }

    @DexIgnore
    @Override // com.fossil.vk, com.fossil.yk
    public void a(View view, int i, int i2, int i3, int i4) {
        view.setLeftTopRightBottom(i, i2, i3, i4);
    }

    @DexIgnore
    @Override // com.fossil.uk, com.fossil.yk
    public void a(View view, Matrix matrix) {
        view.setAnimationMatrix(matrix);
    }
}
