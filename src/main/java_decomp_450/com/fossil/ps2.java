package com.fossil;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ps2<E> extends AbstractCollection<E> implements Serializable {
    @DexIgnore
    public static /* final */ Object[] a; // = new Object[0];

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean contains(@NullableDecl Object obj);

    @DexIgnore
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object[] toArray() {
        return toArray(a);
    }

    @DexIgnore
    public int zzb(Object[] objArr, int i) {
        yt2 yt2 = (yt2) iterator();
        while (yt2.hasNext()) {
            objArr[i] = yt2.next();
            i++;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: zzb */
    public abstract yt2<E> iterator();

    @DexIgnore
    public os2<E> zzc() {
        return isEmpty() ? os2.zza() : os2.zza(toArray());
    }

    @DexIgnore
    @NullableDecl
    public Object[] zze() {
        return null;
    }

    @DexIgnore
    public int zzf() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int zzg() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean zzh();

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    public final <T> T[] toArray(T[] tArr) {
        T[] tArr2;
        or2.a(tArr);
        int size = size();
        if (tArr.length < size) {
            Object[] zze = zze();
            if (zze != null) {
                return (T[]) Arrays.copyOfRange(zze, zzf(), zzg(), tArr.getClass());
            }
            tArr = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), size));
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        zzb(tArr2, 0);
        return tArr2;
    }
}
