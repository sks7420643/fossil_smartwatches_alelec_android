package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em4 extends RecyclerView.g<c> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<WatchApp> c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(WatchApp watchApp);

        @DexIgnore
        void b(WatchApp watchApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public WatchApp e;
        @DexIgnore
        public /* final */ /* synthetic */ em4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    d.a((WatchApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    d.b((WatchApp) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.em4$c$c")
        /* renamed from: com.fossil.em4$c$c  reason: collision with other inner class name */
        public static final class C0059c implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public C0059c(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                ee7.b(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(em4 em4, View view) {
            super(view);
            ee7.b(view, "view");
            this.f = em4;
            this.c = view.findViewById(2131362698);
            this.d = view.findViewById(2131362736);
            View findViewById = view.findViewById(2131363464);
            ee7.a((Object) findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363356);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            String str;
            ee7.b(watchApp, "watchApp");
            this.e = watchApp;
            if (watchApp != null) {
                this.a.d(watchApp.getWatchappId());
                this.b.setText(ig5.a(PortfolioApp.g0.c(), watchApp.getNameKey(), watchApp.getName()));
            }
            this.a.setSelectedWc(ee7.a((Object) watchApp.getWatchappId(), (Object) this.f.b));
            View view = this.d;
            ee7.a((Object) view, "ivWarning");
            view.setVisibility(!cf5.c.d(watchApp.getWatchappId()) ? 0 : 8);
            if (ee7.a((Object) watchApp.getWatchappId(), (Object) this.f.b)) {
                View view2 = this.c;
                ee7.a((Object) view2, "ivIndicator");
                view2.setBackground(v6.c(PortfolioApp.g0.c(), 2131230953));
            } else {
                View view3 = this.c;
                ee7.a((Object) view3, "ivIndicator");
                view3.setBackground(v6.c(PortfolioApp.g0.c(), 2131230954));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            WatchApp watchApp2 = this.e;
            if (watchApp2 == null || (str = watchApp2.getWatchappId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            ee7.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "WATCH_APP", putExtra, null, new C0059c(this), 4, null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ em4(ArrayList arrayList, b bVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("WatchAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final b d() {
        return this.d;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "watchAppId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558717, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public em4(ArrayList<WatchApp> arrayList, b bVar) {
        ee7.b(arrayList, "mData");
        this.c = arrayList;
        this.d = bVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<WatchApp> list) {
        ee7.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        ee7.b(str, "watchAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getWatchappId(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchApp watchApp = this.c.get(i);
            ee7.a((Object) watchApp, "mData[position]");
            cVar.a(watchApp);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.d = bVar;
    }
}
