package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.v02;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c02 extends v02.a<dz1, GoogleSignInOptions> {
    @DexIgnore
    /* Return type fixed from 'com.fossil.v02$f' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.content.Context, android.os.Looper, com.fossil.j62, java.lang.Object, com.fossil.a12$b, com.fossil.a12$c] */
    @Override // com.fossil.v02.a
    public final /* synthetic */ dz1 a(Context context, Looper looper, j62 j62, GoogleSignInOptions googleSignInOptions, a12.b bVar, a12.c cVar) {
        return new dz1(context, looper, j62, googleSignInOptions, bVar, cVar);
    }

    @DexIgnore
    @Override // com.fossil.v02.e
    public final /* synthetic */ List a(Object obj) {
        GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
        if (googleSignInOptions == null) {
            return Collections.emptyList();
        }
        return googleSignInOptions.v();
    }
}
