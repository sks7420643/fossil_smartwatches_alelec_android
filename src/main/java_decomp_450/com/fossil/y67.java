package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y67 implements u67 {
    @DexIgnore
    public boolean a; // = false;

    @DexIgnore
    @Override // com.fossil.u67
    public void d(String str, String str2) {
        if (this.a) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.u67
    public void e(String str, String str2) {
        if (this.a) {
            Log.e(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.u67
    public void setLoggable(boolean z) {
        this.a = z;
    }

    @DexIgnore
    @Override // com.fossil.u67
    public void w(String str, String str2) {
        if (this.a) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.u67
    public void e(String str, String str2, Throwable th) {
        if (this.a) {
            Log.e(str, str2, th);
        }
    }
}
