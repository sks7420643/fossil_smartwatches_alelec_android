package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq4 extends go5 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<gy4> f;
    @DexIgnore
    public zq4 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public br4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xq4.p;
        }

        @DexIgnore
        public final xq4 b() {
            return new xq4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xq4 a;

        @DexIgnore
        public b(xq4 xq4) {
            this.a = xq4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ gy4 a;
        @DexIgnore
        public /* final */ /* synthetic */ xq4 b;

        @DexIgnore
        public c(gy4 gy4, xq4 xq4) {
            this.a = gy4;
            this.b = xq4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            xq4.c(this.b).f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ xq4 a;

        @DexIgnore
        public d(xq4 xq4) {
            this.a = xq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            gy4 gy4 = (gy4) xq4.a(this.a).a();
            if (gy4 != null) {
                SwipeRefreshLayout swipeRefreshLayout = gy4.w;
                ee7.a((Object) swipeRefreshLayout, "swipe");
                ee7.a((Object) bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ xq4 a;

        @DexIgnore
        public e(xq4 xq4) {
            this.a = xq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            gy4 gy4 = (gy4) xq4.a(this.a).a();
            if (gy4 != null) {
                ee7.a((Object) bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = gy4.q;
                    ee7.a((Object) flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = gy4.u;
                    ee7.a((Object) recyclerView, "rcvNotification");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = gy4.q;
                ee7.a((Object) flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = gy4.u;
                ee7.a((Object) recyclerView2, "rcvNotification");
                recyclerView2.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<List<? extends ao4>> {
        @DexIgnore
        public /* final */ /* synthetic */ xq4 a;

        @DexIgnore
        public f(xq4 xq4) {
            this.a = xq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<ao4> list) {
            br4 b = this.a.i;
            if (b != null) {
                ee7.a((Object) list, "it");
                b.a(list);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ xq4 a;

        @DexIgnore
        public g(xq4 xq4) {
            this.a = xq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            boolean booleanValue = r87.getFirst().booleanValue();
            ServerError serverError = (ServerError) r87.getSecond();
            gy4 gy4 = (gy4) xq4.a(this.a).a();
            if (gy4 != null) {
                FlexibleTextView flexibleTextView = gy4.q;
                ee7.a((Object) flexibleTextView, "ftvEmpty");
                flexibleTextView.setVisibility(8);
                if (booleanValue) {
                    FlexibleTextView flexibleTextView2 = gy4.r;
                    ee7.a((Object) flexibleTextView2, "ftvError");
                    flexibleTextView2.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView3 = gy4.r;
                ee7.a((Object) flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(8);
                FlexibleTextView flexibleTextView4 = gy4.r;
                ee7.a((Object) flexibleTextView4, "ftvError");
                String a2 = ig5.a(flexibleTextView4.getContext(), 2131886227);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, a2, 1).show();
                }
            }
        }
    }

    /*
    static {
        String simpleName = xq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCNotificationFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(xq4 xq4) {
        qw6<gy4> qw6 = xq4.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zq4 c(xq4 xq4) {
        zq4 zq4 = xq4.g;
        if (zq4 != null) {
            return zq4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<gy4> qw6 = this.f;
        if (qw6 != null) {
            gy4 a2 = qw6.a();
            if (a2 != null) {
                this.i = new br4();
                a2.s.setOnClickListener(new b(this));
                a2.w.setOnRefreshListener(new c(a2, this));
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.hasFixedSize();
                recyclerView.setAdapter(this.i);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        zq4 zq4 = this.g;
        if (zq4 != null) {
            zq4.d().a(getViewLifecycleOwner(), new d(this));
            zq4 zq42 = this.g;
            if (zq42 != null) {
                zq42.b().a(getViewLifecycleOwner(), new e(this));
                zq4 zq43 = this.g;
                if (zq43 != null) {
                    zq43.e().a(getViewLifecycleOwner(), new f(this));
                    zq4 zq44 = this.g;
                    if (zq44 != null) {
                        zq44.c().a(getViewLifecycleOwner(), new g(this));
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().p().a(this);
        rj4 rj4 = this.h;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(zq4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.g = (zq4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        gy4 gy4 = (gy4) qb.a(layoutInflater, 2131558514, viewGroup, false, a1());
        this.f = new qw6<>(this, gy4);
        ee7.a((Object) gy4, "binding");
        return gy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        g1();
    }
}
