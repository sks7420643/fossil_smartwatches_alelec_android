package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to3<TResult, TContinuationResult> implements hp3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ fo3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ lp3<TContinuationResult> c;

    @DexIgnore
    public to3(Executor executor, fo3<TResult, TContinuationResult> fo3, lp3<TContinuationResult> lp3) {
        this.a = executor;
        this.b = fo3;
        this.c = lp3;
    }

    @DexIgnore
    @Override // com.fossil.hp3
    public final void a(no3<TResult> no3) {
        this.a.execute(new vo3(this, no3));
    }
}
