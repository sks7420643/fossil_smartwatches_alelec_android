package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class as implements yr<Integer, Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public as(Context context) {
        ee7.b(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.yr
    public /* bridge */ /* synthetic */ boolean a(Integer num) {
        return a(num.intValue());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.yr
    public /* bridge */ /* synthetic */ Uri b(Integer num) {
        return b(num.intValue());
    }

    @DexIgnore
    public boolean a(int i) {
        try {
            return this.a.getResources().getResourceEntryName(i) != null;
        } catch (Resources.NotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public Uri b(int i) {
        Uri parse = Uri.parse("android.resource://" + this.a.getPackageName() + '/' + i);
        ee7.a((Object) parse, "Uri.parse(this)");
        return parse;
    }
}
