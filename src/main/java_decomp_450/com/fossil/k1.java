package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.v1;
import com.fossil.w1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k1 implements v1 {
    @DexIgnore
    public Context a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public p1 c;
    @DexIgnore
    public LayoutInflater d;
    @DexIgnore
    public v1.a e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public w1 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public k1(Context context, int i2, int i3) {
        this.a = context;
        this.d = LayoutInflater.from(context);
        this.f = i2;
        this.g = i3;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Context context, p1 p1Var) {
        this.b = context;
        LayoutInflater.from(context);
        this.c = p1Var;
    }

    @DexIgnore
    public abstract void a(r1 r1Var, w1.a aVar);

    @DexIgnore
    public abstract boolean a(int i2, r1 r1Var);

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    public w1 b(ViewGroup viewGroup) {
        if (this.h == null) {
            w1 w1Var = (w1) this.d.inflate(this.f, viewGroup, false);
            this.h = w1Var;
            w1Var.a(this.c);
            a(true);
        }
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public int getId() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup != null) {
            p1 p1Var = this.c;
            int i2 = 0;
            if (p1Var != null) {
                p1Var.b();
                ArrayList<r1> n = this.c.n();
                int size = n.size();
                int i3 = 0;
                for (int i4 = 0; i4 < size; i4++) {
                    r1 r1Var = n.get(i4);
                    if (a(i3, r1Var)) {
                        View childAt = viewGroup.getChildAt(i3);
                        r1 itemData = childAt instanceof w1.a ? ((w1.a) childAt).getItemData() : null;
                        View a2 = a(r1Var, childAt, viewGroup);
                        if (r1Var != itemData) {
                            a2.setPressed(false);
                            a2.jumpDrawablesToCurrentState();
                        }
                        if (a2 != childAt) {
                            a(a2, i3);
                        }
                        i3++;
                    }
                }
                i2 = i3;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.h).addView(view, i2);
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public v1.a a() {
        return this.e;
    }

    @DexIgnore
    public w1.a a(ViewGroup viewGroup) {
        return (w1.a) this.d.inflate(this.g, viewGroup, false);
    }

    @DexIgnore
    public View a(r1 r1Var, View view, ViewGroup viewGroup) {
        w1.a aVar;
        if (view instanceof w1.a) {
            aVar = (w1.a) view;
        } else {
            aVar = a(viewGroup);
        }
        a(r1Var, aVar);
        return (View) aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z) {
        v1.a aVar = this.e;
        if (aVar != null) {
            aVar.a(p1Var, z);
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4, types: [com.fossil.p1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.v1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.fossil.a2 r2) {
        /*
            r1 = this;
            com.fossil.v1$a r0 = r1.e
            if (r0 == 0) goto L_0x000e
            if (r2 == 0) goto L_0x0007
            goto L_0x0009
        L_0x0007:
            com.fossil.p1 r2 = r1.c
        L_0x0009:
            boolean r2 = r0.a(r2)
            return r2
        L_0x000e:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k1.a(com.fossil.a2):boolean");
    }

    @DexIgnore
    public void a(int i2) {
        this.i = i2;
    }
}
