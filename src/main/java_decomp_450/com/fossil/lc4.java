package com.fossil;

import com.fossil.ic4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lc4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(b bVar);

        @DexIgnore
        public abstract a a(mc4 mc4);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract lc4 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(String str);
    }

    @DexIgnore
    public enum b {
        OK,
        BAD_CONFIG
    }

    @DexIgnore
    public static a f() {
        return new ic4.b();
    }

    @DexIgnore
    public abstract mc4 a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract b d();

    @DexIgnore
    public abstract String e();
}
