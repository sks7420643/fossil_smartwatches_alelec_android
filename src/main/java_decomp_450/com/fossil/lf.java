package com.fossil;

import com.fossil.pf;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lf<Key, Value> {
    @DexIgnore
    public AtomicBoolean mInvalid; // = new AtomicBoolean(false);
    @DexIgnore
    public CopyOnWriteArrayList<c> mOnInvalidatedCallbacks; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements t3<List<X>, List<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ t3 a;

        @DexIgnore
        public a(t3 t3Var) {
            this.a = t3Var;
        }

        @DexIgnore
        /* renamed from: a */
        public List<Y> apply(List<X> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.a.apply(list.get(i)));
            }
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<Key, Value> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends b<Key, ToValue> {
            @DexIgnore
            public /* final */ /* synthetic */ t3 a;

            @DexIgnore
            public a(t3 t3Var) {
                this.a = t3Var;
            }

            @DexIgnore
            @Override // com.fossil.lf.b
            public lf<Key, ToValue> create() {
                return b.this.create().mapByPage(this.a);
            }
        }

        @DexIgnore
        public abstract lf<Key, Value> create();

        @DexIgnore
        public <ToValue> b<Key, ToValue> map(t3<Value, ToValue> t3Var) {
            return mapByPage(lf.createListFunction(t3Var));
        }

        @DexIgnore
        public <ToValue> b<Key, ToValue> mapByPage(t3<List<Value>, List<ToValue>> t3Var) {
            return new a(t3Var);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public static <A, B> List<B> convert(t3<List<A>, List<B>> t3Var, List<A> list) {
        List<B> apply = t3Var.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new IllegalStateException("Invalid Function " + t3Var + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> t3<List<X>, List<Y>> createListFunction(t3<X, Y> t3Var) {
        return new a(t3Var);
    }

    @DexIgnore
    public void addInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.add(cVar);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            Iterator<c> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> lf<Key, ToValue> map(t3<Value, ToValue> t3Var);

    @DexIgnore
    public abstract <ToValue> lf<Key, ToValue> mapByPage(t3<List<Value>, List<ToValue>> t3Var);

    @DexIgnore
    public void removeInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.remove(cVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ lf b;
        @DexIgnore
        public /* final */ pf.a<T> c;
        @DexIgnore
        public /* final */ Object d; // = new Object();
        @DexIgnore
        public Executor e; // = null;
        @DexIgnore
        public boolean f; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ pf a;

            @DexIgnore
            public a(pf pfVar) {
                this.a = pfVar;
            }

            @DexIgnore
            public void run() {
                d dVar = d.this;
                dVar.c.a(dVar.a, this.a);
            }
        }

        @DexIgnore
        public d(lf lfVar, int i, Executor executor, pf.a<T> aVar) {
            this.b = lfVar;
            this.a = i;
            this.e = executor;
            this.c = aVar;
        }

        @DexIgnore
        public static void a(List<?> list, int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        public void a(Executor executor) {
            synchronized (this.d) {
                this.e = executor;
            }
        }

        @DexIgnore
        public boolean a() {
            if (!this.b.isInvalid()) {
                return false;
            }
            a(pf.c());
            return true;
        }

        @DexIgnore
        public void a(pf<T> pfVar) {
            Executor executor;
            synchronized (this.d) {
                if (!this.f) {
                    this.f = true;
                    executor = this.e;
                } else {
                    throw new IllegalStateException("callback.onResult already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new a(pfVar));
            } else {
                this.c.a(this.a, pfVar);
            }
        }
    }
}
