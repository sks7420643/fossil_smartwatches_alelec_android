package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dg3 extends ln2 implements bg3 {
    @DexIgnore
    public dg3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(ub3 ub3, nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ub3);
        jo2.a(E, nm3);
        b(1, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void b(nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nm3);
        b(18, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void c(nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nm3);
        b(4, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void d(nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nm3);
        b(6, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(em3 em3, nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, em3);
        jo2.a(E, nm3);
        b(2, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(ub3 ub3, String str, String str2) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ub3);
        E.writeString(str);
        E.writeString(str2);
        b(5, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(nm3 nm3, boolean z) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nm3);
        jo2.a(E, z);
        Parcel a = a(7, E);
        ArrayList createTypedArrayList = a.createTypedArrayList(em3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final byte[] a(ub3 ub3, String str) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ub3);
        E.writeString(str);
        Parcel a = a(9, E);
        byte[] createByteArray = a.createByteArray();
        a.recycle();
        return createByteArray;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel E = E();
        E.writeLong(j);
        E.writeString(str);
        E.writeString(str2);
        E.writeString(str3);
        b(10, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final String a(nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nm3);
        Parcel a = a(11, E);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(wm3 wm3, nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, wm3);
        jo2.a(E, nm3);
        b(12, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(wm3 wm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, wm3);
        b(13, E);
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(String str, String str2, boolean z, nm3 nm3) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, z);
        jo2.a(E, nm3);
        Parcel a = a(14, E);
        ArrayList createTypedArrayList = a.createTypedArrayList(em3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<em3> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        E.writeString(str3);
        jo2.a(E, z);
        Parcel a = a(15, E);
        ArrayList createTypedArrayList = a.createTypedArrayList(em3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<wm3> a(String str, String str2, nm3 nm3) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, nm3);
        Parcel a = a(16, E);
        ArrayList createTypedArrayList = a.createTypedArrayList(wm3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final List<wm3> a(String str, String str2, String str3) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        E.writeString(str3);
        Parcel a = a(17, E);
        ArrayList createTypedArrayList = a.createTypedArrayList(wm3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.bg3
    public final void a(Bundle bundle, nm3 nm3) throws RemoteException {
        Parcel E = E();
        jo2.a(E, bundle);
        jo2.a(E, nm3);
        b(19, E);
    }
}
