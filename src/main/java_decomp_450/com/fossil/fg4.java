package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fg4 extends jg4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public fg4() {
        a();
    }

    @DexIgnore
    public fg4 a() {
        this.a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jg4
    public fg4 a(gg4 gg4) throws IOException {
        while (true) {
            int j = gg4.j();
            if (j == 0) {
                return this;
            }
            if (j == 18) {
                this.a = gg4.i();
            } else if (j == 26) {
                this.b = gg4.i();
            } else if (j == 50) {
                gg4.i();
            } else if (!lg4.b(gg4, j)) {
                return this;
            }
        }
    }
}
