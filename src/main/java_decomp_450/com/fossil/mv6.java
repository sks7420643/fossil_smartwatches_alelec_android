package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv6 extends go5 implements tt6 {
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public st6 f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final mv6 a() {
            return new mv6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.tt6
    public void i() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.tt6
    public void n0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558627, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(2131362701);
        ImageView imageView2 = (ImageView) inflate.findViewById(2131362642);
        if (tm4.a.a().k()) {
            ee7.a((Object) imageView, "ivLogo");
            imageView.setVisibility(0);
            ee7.a((Object) imageView2, "ivBackground");
            imageView2.setVisibility(4);
        }
        return inflate;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        st6 st6 = this.f;
        if (st6 != null) {
            st6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        st6 st6 = this.f;
        if (st6 != null) {
            st6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(st6 st6) {
        ee7.b(st6, "presenter");
        this.f = st6;
    }
}
