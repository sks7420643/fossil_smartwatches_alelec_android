package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n04 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    @CanIgnoreReturnValue
    public <T> T a(t04<T> t04) throws IOException {
        jw3.a(t04);
        q04 a = q04.a();
        try {
            Reader a2 = a();
            a.a(a2);
            T t = (T) o04.a(a2, t04);
            a.close();
            return t;
        } catch (Throwable th) {
            a.close();
            throw th;
        }
    }
}
