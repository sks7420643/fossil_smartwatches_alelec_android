package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jh4 implements ph4 {
    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ph4
    public void a(qh4 qh4) {
        if (sh4.a(qh4.d(), qh4.f) >= 2) {
            qh4.a(a(qh4.d().charAt(qh4.f), qh4.d().charAt(qh4.f + 1)));
            qh4.f += 2;
            return;
        }
        char c = qh4.c();
        int a = sh4.a(qh4.d(), qh4.f, a());
        if (a != a()) {
            if (a == 1) {
                qh4.a('\u00e6');
                qh4.b(1);
            } else if (a == 2) {
                qh4.a('\u00ef');
                qh4.b(2);
            } else if (a == 3) {
                qh4.a('\u00ee');
                qh4.b(3);
            } else if (a == 4) {
                qh4.a('\u00f0');
                qh4.b(4);
            } else if (a == 5) {
                qh4.a('\u00e7');
                qh4.b(5);
            } else {
                throw new IllegalStateException("Illegal mode: " + a);
            }
        } else if (sh4.c(c)) {
            qh4.a('\u00eb');
            qh4.a((char) ((c - '\u0080') + 1));
            qh4.f++;
        } else {
            qh4.a((char) (c + 1));
            qh4.f++;
        }
    }

    @DexIgnore
    public static char a(char c, char c2) {
        if (sh4.b(c) && sh4.b(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + 130);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }
}
