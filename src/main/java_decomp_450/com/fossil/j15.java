package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j15 extends i15 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362927, 1);
        A.put(2131363342, 2);
        A.put(2131362682, 3);
        A.put(2131362396, 4);
        A.put(2131362395, 5);
        A.put(2131362399, 6);
        A.put(2131361937, 7);
    }
    */

    @DexIgnore
    public j15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public j15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (RTLImageView) objArr[3], (DashBar) objArr[1], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2]);
        this.y = -1;
        ((i15) this).w.setTag(null);
        a(view);
        f();
    }
}
