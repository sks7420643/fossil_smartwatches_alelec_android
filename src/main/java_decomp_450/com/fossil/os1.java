package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os1 extends y71<vg0[]> {
    @DexIgnore
    public static /* final */ os1 b; // = new os1();

    @DexIgnore
    public os1() {
        super(new r60((byte) 2, (byte) 0));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [short, java.lang.Object] */
    @Override // com.fossil.y71
    public byte[] a(short s, vg0[] vg0Arr) {
        vg0[] vg0Arr2 = vg0Arr;
        byte[] a = a(vg0Arr2);
        byte[] array = ByteBuffer.allocate(a.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put(((y71) this).a.getMajor()).put(((y71) this).a.getMinor()).putShort(0).putShort((short) b(vg0Arr2)).putInt(a.length).put(a).putInt((int) ik1.a.a(a, ng1.CRC32C)).array();
        ee7.a((Object) array, "result.array()");
        return array;
    }

    @DexIgnore
    public final int b(vg0[] vg0Arr) {
        ArrayList arrayList = new ArrayList(vg0Arr.length);
        for (vg0 vg0 : vg0Arr) {
            arrayList.add(vg0.getReplyMessageGroup());
        }
        return (ea7.c((Iterable) arrayList).size() * 11) + 2;
    }

    @DexIgnore
    public byte[] a(vg0[] vg0Arr) {
        int b2 = b(vg0Arr);
        ArrayList arrayList = new ArrayList(vg0Arr.length);
        for (vg0 vg0 : vg0Arr) {
            arrayList.add(vg0.getReplyMessageGroup());
        }
        List<ug0> c = ea7.c((Iterable) arrayList);
        ByteBuffer order = ByteBuffer.allocate(b(vg0Arr)).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) 2).put((byte) 11);
        int i = 0;
        for (ug0 ug0 : c) {
            ArrayList<vg0> arrayList2 = new ArrayList();
            for (vg0 vg02 : vg0Arr) {
                if (ee7.a(vg02.getReplyMessageGroup(), ug0)) {
                    arrayList2.add(vg02);
                }
            }
            int i2 = 0;
            for (vg0 vg03 : arrayList2) {
                i2 |= vg03.getNotificationType().a();
            }
            order.putShort((short) i2).put((byte) ug0.getReplyMessages().length).putInt(b2 + 12 + i).putInt(ug0.b().length);
            i += ug0.b().length;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(order.array());
        for (ug0 ug02 : c) {
            byteArrayOutputStream.write(ug02.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "entriesDataInByte.toByteArray()");
        return byteArray;
    }
}
