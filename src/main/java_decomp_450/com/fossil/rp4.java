package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp4 extends RecyclerView.g<b> {
    @DexIgnore
    public List<sn4> a; // = new ArrayList();
    @DexIgnore
    public a b;
    @DexIgnore
    public /* final */ int c; // = v6.a(PortfolioApp.g0.c(), 2131099689);
    @DexIgnore
    public /* final */ int d; // = v6.a(PortfolioApp.g0.c(), 2131099677);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(sn4 sn4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ u85 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ rp4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends fe7 implements gd7<View, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ sn4 $item$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, sn4 sn4) {
                super(1);
                this.this$0 = bVar;
                this.$item$inlined = sn4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(View view) {
                invoke(view);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                a c;
                if (this.this$0.getAdapterPosition() != -1 && (c = this.this$0.c.b) != null) {
                    c.a(this.$item$inlined);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(rp4 rp4, u85 u85, View view) {
            super(view);
            ee7.b(u85, "binding");
            ee7.b(view, "root");
            this.c = rp4;
            this.a = u85;
            this.b = view;
        }

        @DexIgnore
        public void a(sn4 sn4) {
            ee7.b(sn4, "item");
            u85 u85 = this.a;
            View view = u85.v;
            ee7.a((Object) view, "topDivider");
            int i = 8;
            view.setVisibility(getAdapterPosition() == 0 ? 8 : 0);
            View view2 = u85.q;
            ee7.a((Object) view2, "bottomDivider");
            if (getAdapterPosition() != this.c.a.size() - 1) {
                i = 0;
            }
            view2.setVisibility(i);
            FlexibleTextView flexibleTextView = u85.x;
            ee7.a((Object) flexibleTextView, "tvName");
            flexibleTextView.setText(sn4.c());
            FlexibleTextView flexibleTextView2 = u85.w;
            ee7.a((Object) flexibleTextView2, "tvDesc");
            flexibleTextView2.setText(sn4.a());
            ImageView imageView = u85.u;
            ee7.a((Object) imageView, "ivThumbnail");
            imageView.setImageDrawable(v6.c(imageView.getContext(), sn4.f()));
            String g = sn4.g();
            int hashCode = g.hashCode();
            if (hashCode != -1348781656) {
                if (hashCode == -637042289 && g.equals("activity_reach_goal")) {
                    u85.x.setTextColor(this.c.c);
                }
            } else if (g.equals("activity_best_result")) {
                u85.x.setTextColor(this.c.d);
            }
            FlexibleButton flexibleButton = u85.t;
            ee7.a((Object) flexibleButton, "fbCreate");
            du4.a(flexibleButton, new a(this, sn4));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        bVar.a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        u85 a2 = u85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemChallengeTemplateLis\u2026tInflater, parent, false)");
        View d2 = a2.d();
        ee7.a((Object) d2, "itemTemplateListBinding.root");
        return new b(this, a2, d2);
    }

    @DexIgnore
    public final void a(List<sn4> list) {
        ee7.b(list, "newList");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(a aVar) {
        ee7.b(aVar, "listener");
        this.b = aVar;
    }
}
