package com.fossil;

import com.fossil.sv2;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pv2<T extends sv2<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract qv2<T> a(Object obj);

    @DexIgnore
    public abstract Object a(nv2 nv2, jx2 jx2, int i);

    @DexIgnore
    public abstract void a(oz2 oz2, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean a(jx2 jx2);

    @DexIgnore
    public abstract qv2<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
