package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.dp5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os6 extends go5 implements ns6 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public boolean f;
    @DexIgnore
    public qw6<y45> g;
    @DexIgnore
    public ms6 h;
    @DexIgnore
    public dp5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return os6.p;
        }

        @DexIgnore
        public final os6 b() {
            Bundle bundle = new Bundle();
            os6 os6 = new os6();
            os6.setArguments(bundle);
            return os6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final os6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("PMS_SKIP_ABLE", z);
            os6 os6 = new os6();
            os6.setArguments(bundle);
            return os6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ os6 a;

        @DexIgnore
        public b(os6 os6) {
            this.a = os6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.n(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ os6 a;

        @DexIgnore
        public c(os6 os6) {
            this.a = os6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.n(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements dp5.a {
        @DexIgnore
        public /* final */ /* synthetic */ os6 a;

        @DexIgnore
        public d(os6 os6) {
            this.a = os6;
        }

        @DexIgnore
        @Override // com.fossil.dp5.a
        public void a(PermissionData permissionData) {
            ee7.b(permissionData, "permissionData");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = os6.q.a();
            local.d(a2, "requestPermission " + permissionData);
            if (!permissionData.getAndroidPermissionSet().isEmpty()) {
                os6 os6 = this.a;
                Object[] array = permissionData.getAndroidPermissionSet().toArray(new String[0]);
                if (array != null) {
                    os6.requestPermissions((String[]) array, 111);
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            xg5.c settingPermissionId = permissionData.getSettingPermissionId();
            if (settingPermissionId != null) {
                int i = ps6.a[settingPermissionId.ordinal()];
                if (i == 1) {
                    this.a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
                } else if (i == 2) {
                    this.a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                } else if (i == 3) {
                    this.a.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                }
            }
        }
    }

    /*
    static {
        String simpleName = os6.class.getSimpleName();
        ee7.a((Object) simpleName, "PermissionFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ns6
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        n(2);
        return true;
    }

    @DexIgnore
    public final void f1() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        StringBuilder sb = new StringBuilder();
        sb.append("package:");
        FragmentActivity activity = getActivity();
        sb.append(activity != null ? activity.getPackageName() : null);
        intent.setData(Uri.parse(sb.toString()));
        startActivity(intent);
    }

    @DexIgnore
    public void n(int i2) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(i2);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        qw6<y45> qw6 = new qw6<>(this, (y45) qb.a(layoutInflater, 2131558605, viewGroup, false, a1()));
        this.g = qw6;
        if (qw6 != null) {
            y45 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ms6 ms6 = this.h;
        if (ms6 != null) {
            ms6.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.g6.b, com.fossil.go5, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        ee7.b(strArr, "permissions");
        ee7.b(iArr, "grantResults");
        super.onRequestPermissionsResult(i2, strArr, iArr);
        Integer[] b2 = s97.b(iArr);
        int length = b2.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            int i5 = i4 + 1;
            if (b2[i3].intValue() == -1) {
                String str = strArr[i4];
                boolean shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale(str);
                ms6 ms6 = this.h;
                if (ms6 != null) {
                    boolean a2 = ms6.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = p;
                    local.d(str2, "permission " + str + " is denied, isDenyOnly " + shouldShowRequestPermissionRationale + " isFirstTime " + a2 + ' ');
                    if (shouldShowRequestPermissionRationale) {
                        return;
                    }
                    if (!a2) {
                        f1();
                        return;
                    }
                    ms6 ms62 = this.h;
                    if (ms62 != null) {
                        ms62.a(str, false);
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else {
                i3++;
                i4 = i5;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(p, "onResume");
        ms6 ms6 = this.h;
        if (ms6 != null) {
            ms6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ImageView imageView;
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            boolean z = arguments.getBoolean("PMS_SKIP_ABLE", false);
            this.f = z;
            if (z) {
                qw6<y45> qw6 = this.g;
                if (qw6 != null) {
                    y45 a2 = qw6.a();
                    if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                        flexibleButton2.setVisibility(0);
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            }
        }
        qw6<y45> qw62 = this.g;
        if (qw62 != null) {
            y45 a3 = qw62.a();
            if (!(a3 == null || (flexibleButton = a3.q) == null)) {
                flexibleButton.setOnClickListener(new b(this));
            }
            qw6<y45> qw63 = this.g;
            if (qw63 != null) {
                y45 a4 = qw63.a();
                if (a4 != null && (imageView = a4.s) != null) {
                    imageView.setOnClickListener(new c(this));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ns6
    public void t(List<PermissionData> list) {
        FlexibleTextView flexibleTextView;
        ee7.b(list, "listPermissionModel");
        if (this.i == null) {
            qw6<y45> qw6 = this.g;
            if (qw6 != null) {
                y45 a2 = qw6.a();
                if (!(a2 == null || (flexibleTextView = a2.r) == null)) {
                    flexibleTextView.setVisibility(0);
                }
                dp5 dp5 = new dp5();
                dp5.a(new d(this));
                this.i = dp5;
                qw6<y45> qw62 = this.g;
                if (qw62 != null) {
                    y45 a3 = qw62.a();
                    RecyclerView recyclerView = a3 != null ? a3.u : null;
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                        recyclerView.setAdapter(this.i);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
        dp5 dp52 = this.i;
        if (dp52 != null) {
            dp52.a(list);
        }
    }

    @DexIgnore
    public void a(ms6 ms6) {
        ee7.b(ms6, "presenter");
        this.h = ms6;
    }
}
