package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bj implements wi {
    @DexIgnore
    public static /* final */ String[] b; // = new String[0];
    @DexIgnore
    public /* final */ SQLiteDatabase a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ zi a;

        @DexIgnore
        public a(zi ziVar) {
            this.a = ziVar;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.a(new ej(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ zi a;

        @DexIgnore
        public b(zi ziVar) {
            this.a = ziVar;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.a(new ej(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    @DexIgnore
    public bj(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @DexIgnore
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.a == sQLiteDatabase;
    }

    @DexIgnore
    @Override // com.fossil.wi
    public void beginTransaction() {
        this.a.beginTransaction();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public aj compileStatement(String str) {
        return new fj(this.a.compileStatement(str));
    }

    @DexIgnore
    @Override // com.fossil.wi
    public void endTransaction() {
        this.a.endTransaction();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public void execSQL(String str) throws SQLException {
        this.a.execSQL(str);
    }

    @DexIgnore
    @Override // com.fossil.wi
    public List<Pair<String, String>> getAttachedDbs() {
        return this.a.getAttachedDbs();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public String getPath() {
        return this.a.getPath();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public boolean inTransaction() {
        return this.a.inTransaction();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public boolean isOpen() {
        return this.a.isOpen();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public Cursor query(String str) {
        return query(new vi(str));
    }

    @DexIgnore
    @Override // com.fossil.wi
    public void setTransactionSuccessful() {
        this.a.setTransactionSuccessful();
    }

    @DexIgnore
    @Override // com.fossil.wi
    public void execSQL(String str, Object[] objArr) throws SQLException {
        this.a.execSQL(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.wi
    public Cursor query(zi ziVar) {
        return this.a.rawQueryWithFactory(new a(ziVar), ziVar.b(), b, null);
    }

    @DexIgnore
    @Override // com.fossil.wi
    public Cursor query(zi ziVar, CancellationSignal cancellationSignal) {
        return this.a.rawQueryWithFactory(new b(ziVar), ziVar.b(), b, null, cancellationSignal);
    }
}
