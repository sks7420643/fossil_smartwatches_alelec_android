package com.fossil;

import android.content.Context;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cg3 extends kh3 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public List<String> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;

    @DexIgnore
    public cg3(oh3 oh3, long j2) {
        super(oh3);
        this.h = j2;
    }

    @DexIgnore
    public final String A() {
        w();
        return this.c;
    }

    @DexIgnore
    public final String B() {
        w();
        return this.k;
    }

    @DexIgnore
    public final String C() {
        w();
        return this.l;
    }

    @DexIgnore
    public final String D() {
        w();
        return this.m;
    }

    @DexIgnore
    public final int E() {
        w();
        return this.e;
    }

    @DexIgnore
    public final int F() {
        w();
        return this.j;
    }

    @DexIgnore
    public final List<String> G() {
        return this.i;
    }

    @DexIgnore
    public final String H() {
        if (!c43.a() || !l().a(wb3.r0)) {
            try {
                Class<?> loadClass = f().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                if (loadClass == null) {
                    return null;
                }
                try {
                    Object invoke = loadClass.getDeclaredMethod("getInstance", Context.class).invoke(null, f());
                    if (invoke == null) {
                        return null;
                    }
                    try {
                        return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                    } catch (Exception unused) {
                        e().y().a("Failed to retrieve Firebase Instance Id");
                        return null;
                    }
                } catch (Exception unused2) {
                    e().x().a("Failed to obtain Firebase Analytics instance");
                    return null;
                }
            } catch (ClassNotFoundException unused3) {
                return null;
            }
        } else {
            e().B().a("Disabled IID for tests.");
            return null;
        }
    }

    @DexIgnore
    public final nm3 a(String str) {
        String str2;
        long j2;
        boolean z;
        Boolean bool;
        g();
        a();
        String A = A();
        String B = B();
        w();
        String str3 = this.d;
        long E = (long) E();
        w();
        String str4 = this.f;
        long n = l().n();
        w();
        g();
        if (this.g == 0) {
            this.g = ((ii3) this).a.v().a(f(), f().getPackageName());
        }
        long j3 = this.g;
        boolean g2 = ((ii3) this).a.g();
        boolean z2 = !k().v;
        g();
        a();
        if (!((ii3) this).a.g()) {
            str2 = null;
        } else {
            str2 = H();
        }
        oh3 oh3 = ((ii3) this).a;
        Long valueOf = Long.valueOf(oh3.p().j.a());
        if (valueOf.longValue() == 0) {
            j2 = oh3.F;
            z = g2;
        } else {
            z = g2;
            j2 = Math.min(oh3.F, valueOf.longValue());
        }
        int F = F();
        boolean booleanValue = l().q().booleanValue();
        ym3 l2 = l();
        l2.a();
        Boolean e2 = l2.e("google_analytics_ssaid_collection_enabled");
        boolean booleanValue2 = Boolean.valueOf(e2 == null || e2.booleanValue()).booleanValue();
        wg3 k2 = k();
        k2.g();
        boolean z3 = k2.s().getBoolean("deferred_analytics_collection", false);
        String C = C();
        Boolean e3 = l().e("google_analytics_default_allow_ad_personalization_signals");
        if (e3 == null) {
            bool = null;
        } else {
            bool = Boolean.valueOf(!e3.booleanValue());
        }
        return new nm3(A, B, str3, E, str4, n, j3, str, z, z2, str2, 0, j2, F, booleanValue, booleanValue2, z3, C, bool, this.h, l().a(wb3.c0) ? this.i : null, (!f23.a() || !l().a(wb3.o0)) ? null : D());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x02c2  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01ac A[Catch:{ IllegalStateException -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01cd A[Catch:{ IllegalStateException -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01cf A[Catch:{ IllegalStateException -> 0x0246 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0267  */
    @Override // com.fossil.kh3
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void u() {
        /*
            r11 = this;
            android.content.Context r0 = r11.f()
            java.lang.String r0 = r0.getPackageName()
            android.content.Context r1 = r11.f()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.String r2 = "Unknown"
            java.lang.String r3 = ""
            r4 = 0
            java.lang.String r5 = "unknown"
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != 0) goto L_0x002d
            com.fossil.jg3 r7 = r11.e()
            com.fossil.mg3 r7 = r7.t()
            java.lang.Object r8 = com.fossil.jg3.a(r0)
            java.lang.String r9 = "PackageManager is null, app identity information might be inaccurate. appId"
            r7.a(r9, r8)
            goto L_0x008c
        L_0x002d:
            java.lang.String r5 = r1.getInstallerPackageName(r0)     // Catch:{ IllegalArgumentException -> 0x0032 }
            goto L_0x0043
        L_0x0032:
            com.fossil.jg3 r7 = r11.e()
            com.fossil.mg3 r7 = r7.t()
            java.lang.Object r8 = com.fossil.jg3.a(r0)
            java.lang.String r9 = "Error retrieving app installer package name. appId"
            r7.a(r9, r8)
        L_0x0043:
            if (r5 != 0) goto L_0x0048
            java.lang.String r5 = "manual_install"
            goto L_0x0051
        L_0x0048:
            java.lang.String r7 = "com.android.vending"
            boolean r7 = r7.equals(r5)
            if (r7 == 0) goto L_0x0051
            r5 = r3
        L_0x0051:
            android.content.Context r7 = r11.f()     // Catch:{ NameNotFoundException -> 0x0079 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x0079 }
            android.content.pm.PackageInfo r7 = r1.getPackageInfo(r7, r4)     // Catch:{ NameNotFoundException -> 0x0079 }
            if (r7 == 0) goto L_0x008c
            android.content.pm.ApplicationInfo r8 = r7.applicationInfo     // Catch:{ NameNotFoundException -> 0x0079 }
            java.lang.CharSequence r8 = r1.getApplicationLabel(r8)     // Catch:{ NameNotFoundException -> 0x0079 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ NameNotFoundException -> 0x0079 }
            if (r9 != 0) goto L_0x0070
            java.lang.String r8 = r8.toString()     // Catch:{ NameNotFoundException -> 0x0079 }
            goto L_0x0071
        L_0x0070:
            r8 = r2
        L_0x0071:
            java.lang.String r2 = r7.versionName     // Catch:{ NameNotFoundException -> 0x0076 }
            int r6 = r7.versionCode     // Catch:{ NameNotFoundException -> 0x0076 }
            goto L_0x008c
        L_0x0076:
            r7 = r2
            r2 = r8
            goto L_0x007a
        L_0x0079:
            r7 = r2
        L_0x007a:
            com.fossil.jg3 r8 = r11.e()
            com.fossil.mg3 r8 = r8.t()
            java.lang.Object r9 = com.fossil.jg3.a(r0)
            java.lang.String r10 = "Error retrieving package info. appId, appName"
            r8.a(r10, r9, r2)
            r2 = r7
        L_0x008c:
            r11.c = r0
            r11.f = r5
            r11.d = r2
            r11.e = r6
            r5 = 0
            r11.g = r5
            r11.b()
            android.content.Context r2 = r11.f()
            com.google.android.gms.common.api.Status r2 = com.fossil.v12.a(r2)
            r5 = 1
            if (r2 == 0) goto L_0x00ae
            boolean r6 = r2.y()
            if (r6 == 0) goto L_0x00ae
            r6 = 1
            goto L_0x00af
        L_0x00ae:
            r6 = 0
        L_0x00af:
            com.fossil.oh3 r7 = r11.a
            java.lang.String r7 = r7.z()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x00cb
            com.fossil.oh3 r7 = r11.a
            java.lang.String r7 = r7.A()
            java.lang.String r8 = "am"
            boolean r7 = r8.equals(r7)
            if (r7 == 0) goto L_0x00cb
            r7 = 1
            goto L_0x00cc
        L_0x00cb:
            r7 = 0
        L_0x00cc:
            r6 = r6 | r7
            if (r6 != 0) goto L_0x00f8
            if (r2 != 0) goto L_0x00df
            com.fossil.jg3 r2 = r11.e()
            com.fossil.mg3 r2 = r2.u()
            java.lang.String r8 = "GoogleService failed to initialize (no status)"
            r2.a(r8)
            goto L_0x00f8
        L_0x00df:
            com.fossil.jg3 r8 = r11.e()
            com.fossil.mg3 r8 = r8.u()
            int r9 = r2.g()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            java.lang.String r2 = r2.v()
            java.lang.String r10 = "GoogleService failed to initialize, status"
            r8.a(r10, r9, r2)
        L_0x00f8:
            if (r6 == 0) goto L_0x0192
            com.fossil.oh3 r2 = r11.a
            int r2 = r2.h()
            switch(r2) {
                case 0: goto L_0x0181;
                case 1: goto L_0x0173;
                case 2: goto L_0x0165;
                case 3: goto L_0x0157;
                case 4: goto L_0x0149;
                case 5: goto L_0x013b;
                case 6: goto L_0x012d;
                case 7: goto L_0x011f;
                default: goto L_0x0103;
            }
        L_0x0103:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.z()
            java.lang.String r8 = "App measurement disabled"
            r6.a(r8)
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.u()
            java.lang.String r8 = "Invalid scion state in identity"
            r6.a(r8)
            goto L_0x018e
        L_0x011f:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.z()
            java.lang.String r8 = "App measurement disabled via the global data collection setting"
            r6.a(r8)
            goto L_0x018e
        L_0x012d:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.y()
            java.lang.String r8 = "App measurement deactivated via resources. This method is being deprecated. Please refer to https://firebase.google.com/support/guides/disable-analytics"
            r6.a(r8)
            goto L_0x018e
        L_0x013b:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.B()
            java.lang.String r8 = "App measurement disabled via the init parameters"
            r6.a(r8)
            goto L_0x018e
        L_0x0149:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.z()
            java.lang.String r8 = "App measurement disabled via the manifest"
            r6.a(r8)
            goto L_0x018e
        L_0x0157:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.z()
            java.lang.String r8 = "App measurement disabled by setAnalyticsCollectionEnabled(false)"
            r6.a(r8)
            goto L_0x018e
        L_0x0165:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.B()
            java.lang.String r8 = "App measurement deactivated via the init parameters"
            r6.a(r8)
            goto L_0x018e
        L_0x0173:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.z()
            java.lang.String r8 = "App measurement deactivated via the manifest"
            r6.a(r8)
            goto L_0x018e
        L_0x0181:
            com.fossil.jg3 r6 = r11.e()
            com.fossil.mg3 r6 = r6.B()
            java.lang.String r8 = "App measurement collection enabled"
            r6.a(r8)
        L_0x018e:
            if (r2 != 0) goto L_0x0192
            r2 = 1
            goto L_0x0193
        L_0x0192:
            r2 = 0
        L_0x0193:
            r11.k = r3
            r11.l = r3
            r11.m = r3
            r11.b()
            if (r7 == 0) goto L_0x01a6
            com.fossil.oh3 r6 = r11.a
            java.lang.String r6 = r6.z()
            r11.l = r6
        L_0x01a6:
            boolean r6 = com.fossil.v33.a()     // Catch:{ IllegalStateException -> 0x0246 }
            if (r6 == 0) goto L_0x01c3
            com.fossil.ym3 r6 = r11.l()     // Catch:{ IllegalStateException -> 0x0246 }
            com.fossil.yf3<java.lang.Boolean> r7 = com.fossil.wb3.Q0     // Catch:{ IllegalStateException -> 0x0246 }
            boolean r6 = r6.a(r7)     // Catch:{ IllegalStateException -> 0x0246 }
            if (r6 == 0) goto L_0x01c3
            android.content.Context r6 = r11.f()     // Catch:{ IllegalStateException -> 0x0246 }
            java.lang.String r7 = "google_app_id"
            java.lang.String r6 = com.fossil.xj3.a(r6, r7)     // Catch:{ IllegalStateException -> 0x0246 }
            goto L_0x01c7
        L_0x01c3:
            java.lang.String r6 = com.fossil.v12.a()     // Catch:{ IllegalStateException -> 0x0246 }
        L_0x01c7:
            boolean r7 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x0246 }
            if (r7 == 0) goto L_0x01cf
            r7 = r3
            goto L_0x01d0
        L_0x01cf:
            r7 = r6
        L_0x01d0:
            r11.k = r7     // Catch:{ IllegalStateException -> 0x0246 }
            boolean r7 = com.fossil.f23.a()     // Catch:{ IllegalStateException -> 0x0246 }
            java.lang.String r8 = "admob_app_id"
            if (r7 == 0) goto L_0x0212
            com.fossil.ym3 r7 = r11.l()
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.o0
            boolean r7 = r7.a(r9)
            if (r7 == 0) goto L_0x0212
            com.fossil.g72 r7 = new com.fossil.g72
            android.content.Context r9 = r11.f()
            r7.<init>(r9)
            java.lang.String r9 = "ga_app_id"
            java.lang.String r9 = r7.a(r9)
            boolean r10 = android.text.TextUtils.isEmpty(r9)
            if (r10 == 0) goto L_0x01fc
            goto L_0x01fd
        L_0x01fc:
            r3 = r9
        L_0x01fd:
            r11.m = r3
            boolean r3 = android.text.TextUtils.isEmpty(r6)
            if (r3 == 0) goto L_0x020b
            boolean r3 = android.text.TextUtils.isEmpty(r9)
            if (r3 != 0) goto L_0x0227
        L_0x020b:
            java.lang.String r3 = r7.a(r8)
            r11.l = r3
            goto L_0x0227
        L_0x0212:
            boolean r3 = android.text.TextUtils.isEmpty(r6)
            if (r3 != 0) goto L_0x0227
            com.fossil.g72 r3 = new com.fossil.g72
            android.content.Context r6 = r11.f()
            r3.<init>(r6)
            java.lang.String r3 = r3.a(r8)
            r11.l = r3
        L_0x0227:
            if (r2 == 0) goto L_0x0258
            com.fossil.jg3 r2 = r11.e()
            com.fossil.mg3 r2 = r2.B()
            java.lang.String r3 = "App measurement enabled for app package, google app id"
            java.lang.String r6 = r11.c
            java.lang.String r7 = r11.k
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0240
            java.lang.String r7 = r11.l
            goto L_0x0242
        L_0x0240:
            java.lang.String r7 = r11.k
        L_0x0242:
            r2.a(r3, r6, r7)
            goto L_0x0258
        L_0x0246:
            r2 = move-exception
            com.fossil.jg3 r3 = r11.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.Object r0 = com.fossil.jg3.a(r0)
            java.lang.String r6 = "Fetching Google App Id failed with exception. appId"
            r3.a(r6, r0, r2)
        L_0x0258:
            r0 = 0
            r11.i = r0
            com.fossil.ym3 r0 = r11.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.c0
            boolean r0 = r0.a(r2)
            if (r0 == 0) goto L_0x02ac
            r11.b()
            com.fossil.ym3 r0 = r11.l()
            java.lang.String r2 = "analytics.safelisted_events"
            java.util.List r0 = r0.f(r2)
            if (r0 == 0) goto L_0x02a8
            int r2 = r0.size()
            if (r2 != 0) goto L_0x028b
            com.fossil.jg3 r2 = r11.e()
            com.fossil.mg3 r2 = r2.y()
            java.lang.String r3 = "Safelisted event list is empty. Ignoring"
            r2.a(r3)
        L_0x0289:
            r5 = 0
            goto L_0x02a8
        L_0x028b:
            java.util.Iterator r2 = r0.iterator()
        L_0x028f:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x02a8
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            com.fossil.jm3 r6 = r11.j()
            java.lang.String r7 = "safelisted event"
            boolean r3 = r6.b(r7, r3)
            if (r3 != 0) goto L_0x028f
            goto L_0x0289
        L_0x02a8:
            if (r5 == 0) goto L_0x02ac
            r11.i = r0
        L_0x02ac:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 16
            if (r0 < r2) goto L_0x02c2
            if (r1 == 0) goto L_0x02bf
            android.content.Context r0 = r11.f()
            boolean r0 = com.fossil.ha2.a(r0)
            r11.j = r0
            return
        L_0x02bf:
            r11.j = r4
            return
        L_0x02c2:
            r11.j = r4
            return
            switch-data {0->0x0181, 1->0x0173, 2->0x0165, 3->0x0157, 4->0x0149, 5->0x013b, 6->0x012d, 7->0x011f, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cg3.u():void");
    }

    @DexIgnore
    @Override // com.fossil.kh3
    public final boolean z() {
        return true;
    }
}
