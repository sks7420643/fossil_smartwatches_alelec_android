package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i53 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<i53> CREATOR; // = new t53();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore
    public i53(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.a = z;
        this.b = z2;
        this.c = z3;
        this.d = z4;
        this.e = z5;
        this.f = z6;
    }

    @DexIgnore
    public final boolean e() {
        return this.f;
    }

    @DexIgnore
    public final boolean g() {
        return this.c;
    }

    @DexIgnore
    public final boolean v() {
        return this.d;
    }

    @DexIgnore
    public final boolean w() {
        return this.a;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, w());
        k72.a(parcel, 2, y());
        k72.a(parcel, 3, g());
        k72.a(parcel, 4, v());
        k72.a(parcel, 5, x());
        k72.a(parcel, 6, e());
        k72.a(parcel, a2);
    }

    @DexIgnore
    public final boolean x() {
        return this.e;
    }

    @DexIgnore
    public final boolean y() {
        return this.b;
    }
}
