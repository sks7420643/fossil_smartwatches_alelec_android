package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc6 {
    @DexIgnore
    public /* final */ fc6 a;
    @DexIgnore
    public /* final */ wc6 b;
    @DexIgnore
    public /* final */ qc6 c;

    @DexIgnore
    public lc6(fc6 fc6, wc6 wc6, qc6 qc6) {
        ee7.b(fc6, "mHeartRateOverviewDayView");
        ee7.b(wc6, "mHeartRateOverviewWeekView");
        ee7.b(qc6, "mHeartRateOverviewMonthView");
        this.a = fc6;
        this.b = wc6;
        this.c = qc6;
    }

    @DexIgnore
    public final fc6 a() {
        return this.a;
    }

    @DexIgnore
    public final qc6 b() {
        return this.c;
    }

    @DexIgnore
    public final wc6 c() {
        return this.b;
    }
}
