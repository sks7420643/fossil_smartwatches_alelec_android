package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c93 {
    @DexIgnore
    public /* final */ bn2 a;

    @DexIgnore
    public c93(bn2 bn2) {
        a72.a(bn2);
        this.a = bn2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof c93)) {
            return false;
        }
        try {
            return this.a.b(((c93) obj).a);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(LatLng latLng) {
        try {
            this.a.setCenter(latLng);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(int i) {
        try {
            this.a.setStrokeColor(i);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(double d) {
        try {
            this.a.setRadius(d);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(float f) {
        try {
            this.a.setStrokeWidth(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(int i) {
        try {
            this.a.setFillColor(i);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.a(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
