package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z3 {
    @DexIgnore
    public /* final */ e a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends v3 {
        @DexIgnore
        public a(z3 z3Var) {
        }
    }

    @DexIgnore
    public z3(e eVar) {
        this.a = eVar;
        new a(this);
    }

    @DexIgnore
    public IBinder a() {
        return this.a.asBinder();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof z3)) {
            return false;
        }
        return ((z3) obj).a().equals(this.a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }
}
