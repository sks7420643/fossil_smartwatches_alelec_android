package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import androidx.collection.SimpleArrayMap;
import androidx.collection.SparseArrayCompat;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w2 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static w2 i;
    @DexIgnore
    public static /* final */ c j; // = new c(6);
    @DexIgnore
    public WeakHashMap<Context, SparseArrayCompat<ColorStateList>> a;
    @DexIgnore
    public SimpleArrayMap<String, d> b;
    @DexIgnore
    public SparseArrayCompat<String> c;
    @DexIgnore
    public /* final */ WeakHashMap<Context, r4<WeakReference<Drawable.ConstantState>>> d; // = new WeakHashMap<>(0);
    @DexIgnore
    public TypedValue e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public e g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements d {
        @DexIgnore
        @Override // com.fossil.w2.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return u0.e(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements d {
        @DexIgnore
        @Override // com.fossil.w2.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return fl.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends s4<Integer, PorterDuffColorFilter> {
        @DexIgnore
        public c(int i) {
            super(i);
        }

        @DexIgnore
        public static int b(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) b(Integer.valueOf(b(i, mode)));
        }

        @DexIgnore
        public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        ColorStateList a(Context context, int i);

        @DexIgnore
        PorterDuff.Mode a(int i);

        @DexIgnore
        Drawable a(w2 w2Var, Context context, int i);

        @DexIgnore
        boolean a(Context context, int i, Drawable drawable);

        @DexIgnore
        boolean b(Context context, int i, Drawable drawable);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements d {
        @DexIgnore
        @Override // com.fossil.w2.d
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return ll.createFromXmlInner(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    @DexIgnore
    public static synchronized w2 a() {
        w2 w2Var;
        synchronized (w2.class) {
            if (i == null) {
                w2 w2Var2 = new w2();
                i = w2Var2;
                a(w2Var2);
            }
            w2Var = i;
        }
        return w2Var;
    }

    @DexIgnore
    public synchronized Drawable b(Context context, int i2) {
        return a(context, i2, false);
    }

    @DexIgnore
    public synchronized ColorStateList c(Context context, int i2) {
        ColorStateList d2;
        d2 = d(context, i2);
        if (d2 == null) {
            d2 = this.g == null ? null : this.g.a(context, i2);
            if (d2 != null) {
                a(context, i2, d2);
            }
        }
        return d2;
    }

    @DexIgnore
    public final ColorStateList d(Context context, int i2) {
        SparseArrayCompat<ColorStateList> sparseArrayCompat;
        WeakHashMap<Context, SparseArrayCompat<ColorStateList>> weakHashMap = this.a;
        if (weakHashMap == null || (sparseArrayCompat = weakHashMap.get(context)) == null) {
            return null;
        }
        return sparseArrayCompat.a(i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073 A[Catch:{ Exception -> 0x00a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[Catch:{ Exception -> 0x00a2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable e(android.content.Context r11, int r12) {
        /*
            r10 = this;
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.w2$d> r0 = r10.b
            r1 = 0
            if (r0 == 0) goto L_0x00b2
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b2
            androidx.collection.SparseArrayCompat<java.lang.String> r0 = r10.c
            java.lang.String r2 = "appcompat_skip_skip"
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r0.a(r12)
            java.lang.String r0 = (java.lang.String) r0
            boolean r3 = r2.equals(r0)
            if (r3 != 0) goto L_0x0027
            if (r0 == 0) goto L_0x002f
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.w2$d> r3 = r10.b
            java.lang.Object r0 = r3.get(r0)
            if (r0 != 0) goto L_0x002f
        L_0x0027:
            return r1
        L_0x0028:
            androidx.collection.SparseArrayCompat r0 = new androidx.collection.SparseArrayCompat
            r0.<init>()
            r10.c = r0
        L_0x002f:
            android.util.TypedValue r0 = r10.e
            if (r0 != 0) goto L_0x003a
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r10.e = r0
        L_0x003a:
            android.util.TypedValue r0 = r10.e
            android.content.res.Resources r1 = r11.getResources()
            r3 = 1
            r1.getValue(r12, r0, r3)
            long r4 = a(r0)
            android.graphics.drawable.Drawable r6 = r10.a(r11, r4)
            if (r6 == 0) goto L_0x004f
            return r6
        L_0x004f:
            java.lang.CharSequence r7 = r0.string
            if (r7 == 0) goto L_0x00aa
            java.lang.String r7 = r7.toString()
            java.lang.String r8 = ".xml"
            boolean r7 = r7.endsWith(r8)
            if (r7 == 0) goto L_0x00aa
            android.content.res.XmlResourceParser r1 = r1.getXml(r12)     // Catch:{ Exception -> 0x00a2 }
            android.util.AttributeSet r7 = android.util.Xml.asAttributeSet(r1)     // Catch:{ Exception -> 0x00a2 }
        L_0x0067:
            int r8 = r1.next()     // Catch:{ Exception -> 0x00a2 }
            r9 = 2
            if (r8 == r9) goto L_0x0071
            if (r8 == r3) goto L_0x0071
            goto L_0x0067
        L_0x0071:
            if (r8 != r9) goto L_0x009a
            java.lang.String r3 = r1.getName()     // Catch:{ Exception -> 0x00a2 }
            androidx.collection.SparseArrayCompat<java.lang.String> r8 = r10.c     // Catch:{ Exception -> 0x00a2 }
            r8.a(r12, r3)     // Catch:{ Exception -> 0x00a2 }
            androidx.collection.SimpleArrayMap<java.lang.String, com.fossil.w2$d> r8 = r10.b     // Catch:{ Exception -> 0x00a2 }
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Exception -> 0x00a2 }
            com.fossil.w2$d r3 = (com.fossil.w2.d) r3     // Catch:{ Exception -> 0x00a2 }
            if (r3 == 0) goto L_0x008f
            android.content.res.Resources$Theme r8 = r11.getTheme()     // Catch:{ Exception -> 0x00a2 }
            android.graphics.drawable.Drawable r1 = r3.a(r11, r1, r7, r8)     // Catch:{ Exception -> 0x00a2 }
            r6 = r1
        L_0x008f:
            if (r6 == 0) goto L_0x00aa
            int r0 = r0.changingConfigurations     // Catch:{ Exception -> 0x00a2 }
            r6.setChangingConfigurations(r0)     // Catch:{ Exception -> 0x00a2 }
            r10.a(r11, r4, r6)     // Catch:{ Exception -> 0x00a2 }
            goto L_0x00aa
        L_0x009a:
            org.xmlpull.v1.XmlPullParserException r11 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r0 = "No start tag found"
            r11.<init>(r0)     // Catch:{ Exception -> 0x00a2 }
            throw r11     // Catch:{ Exception -> 0x00a2 }
        L_0x00a2:
            r11 = move-exception
            java.lang.String r0 = "ResourceManagerInternal"
            java.lang.String r1 = "Exception while inflating drawable"
            android.util.Log.e(r0, r1, r11)
        L_0x00aa:
            if (r6 != 0) goto L_0x00b1
            androidx.collection.SparseArrayCompat<java.lang.String> r11 = r10.c
            r11.a(r12, r2)
        L_0x00b1:
            return r6
        L_0x00b2:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w2.e(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    @DexIgnore
    public synchronized void b(Context context) {
        r4<WeakReference<Drawable.ConstantState>> r4Var = this.d.get(context);
        if (r4Var != null) {
            r4Var.d();
        }
    }

    @DexIgnore
    public static void a(w2 w2Var) {
        if (Build.VERSION.SDK_INT < 24) {
            w2Var.a("vector", new f());
            w2Var.a("animated-vector", new b());
            w2Var.a("animated-selector", new a());
        }
    }

    @DexIgnore
    public synchronized void a(e eVar) {
        this.g = eVar;
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i2, boolean z) {
        Drawable e2;
        a(context);
        e2 = e(context, i2);
        if (e2 == null) {
            e2 = a(context, i2);
        }
        if (e2 == null) {
            e2 = v6.c(context, i2);
        }
        if (e2 != null) {
            e2 = a(context, i2, z, e2);
        }
        if (e2 != null) {
            q2.b(e2);
        }
        return e2;
    }

    @DexIgnore
    public static long a(TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    @DexIgnore
    public final Drawable a(Context context, int i2) {
        Drawable drawable;
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        context.getResources().getValue(i2, typedValue, true);
        long a2 = a(typedValue);
        Drawable a3 = a(context, a2);
        if (a3 != null) {
            return a3;
        }
        e eVar = this.g;
        if (eVar == null) {
            drawable = null;
        } else {
            drawable = eVar.a(this, context, i2);
        }
        if (drawable != null) {
            drawable.setChangingConfigurations(typedValue.changingConfigurations);
            a(context, a2, drawable);
        }
        return drawable;
    }

    @DexIgnore
    public final Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList c2 = c(context, i2);
        if (c2 != null) {
            if (q2.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable i3 = p7.i(drawable);
            p7.a(i3, c2);
            PorterDuff.Mode a2 = a(i2);
            if (a2 == null) {
                return i3;
            }
            p7.a(i3, a2);
            return i3;
        }
        e eVar = this.g;
        if ((eVar == null || !eVar.b(context, i2, drawable)) && !a(context, i2, drawable) && z) {
            return null;
        }
        return drawable;
    }

    @DexIgnore
    public final synchronized Drawable a(Context context, long j2) {
        r4<WeakReference<Drawable.ConstantState>> r4Var = this.d.get(context);
        if (r4Var == null) {
            return null;
        }
        WeakReference<Drawable.ConstantState> b2 = r4Var.b(j2);
        if (b2 != null) {
            Drawable.ConstantState constantState = b2.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            r4Var.d(j2);
        }
        return null;
    }

    @DexIgnore
    public final synchronized boolean a(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        r4<WeakReference<Drawable.ConstantState>> r4Var = this.d.get(context);
        if (r4Var == null) {
            r4Var = new r4<>();
            this.d.put(context, r4Var);
        }
        r4Var.c(j2, new WeakReference<>(constantState));
        return true;
    }

    @DexIgnore
    public synchronized Drawable a(Context context, l3 l3Var, int i2) {
        Drawable e2 = e(context, i2);
        if (e2 == null) {
            e2 = l3Var.a(i2);
        }
        if (e2 == null) {
            return null;
        }
        return a(context, i2, false, e2);
    }

    @DexIgnore
    public boolean a(Context context, int i2, Drawable drawable) {
        e eVar = this.g;
        return eVar != null && eVar.a(context, i2, drawable);
    }

    @DexIgnore
    public final void a(String str, d dVar) {
        if (this.b == null) {
            this.b = new SimpleArrayMap<>();
        }
        this.b.put(str, dVar);
    }

    @DexIgnore
    public PorterDuff.Mode a(int i2) {
        e eVar = this.g;
        if (eVar == null) {
            return null;
        }
        return eVar.a(i2);
    }

    @DexIgnore
    public final void a(Context context, int i2, ColorStateList colorStateList) {
        if (this.a == null) {
            this.a = new WeakHashMap<>();
        }
        SparseArrayCompat<ColorStateList> sparseArrayCompat = this.a.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new SparseArrayCompat<>();
            this.a.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.a(i2, colorStateList);
    }

    @DexIgnore
    public static void a(Drawable drawable, e3 e3Var, int[] iArr) {
        if (!q2.a(drawable) || drawable.mutate() == drawable) {
            if (e3Var.d || e3Var.c) {
                drawable.setColorFilter(a(e3Var.d ? e3Var.a : null, e3Var.c ? e3Var.b : h, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("ResourceManagerInternal", "Mutated drawable is not the same instance as the input.");
    }

    @DexIgnore
    public static PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return a(colorStateList.getColorForState(iArr, 0), mode);
    }

    @DexIgnore
    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (w2.class) {
            a2 = j.a(i2, mode);
            if (a2 == null) {
                a2 = new PorterDuffColorFilter(i2, mode);
                j.a(i2, mode, a2);
            }
        }
        return a2;
    }

    @DexIgnore
    public final void a(Context context) {
        if (!this.f) {
            this.f = true;
            Drawable b2 = b(context, y0.abc_vector_test);
            if (b2 == null || !a(b2)) {
                this.f = false;
                throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    @DexIgnore
    public static boolean a(Drawable drawable) {
        return (drawable instanceof ll) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }
}
