package com.fossil;

import android.text.TextUtils;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc5 {
    @DexIgnore
    public static final boolean a(String str, Gson gson, String str2) {
        CommuteTimeSetting commuteTimeSetting;
        CommuteTimeSetting commuteTimeSetting2;
        ee7.b(gson, "gson");
        try {
            commuteTimeSetting = (CommuteTimeSetting) gson.a(str2, CommuteTimeSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse commute time");
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        if (commuteTimeSetting == null) {
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        try {
            commuteTimeSetting2 = (CommuteTimeSetting) gson.a(str, CommuteTimeSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other commute time");
            commuteTimeSetting2 = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        if (commuteTimeSetting2 == null) {
            commuteTimeSetting2 = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        return ee7.a(commuteTimeSetting.getAddress(), commuteTimeSetting2.getAddress()) && commuteTimeSetting.getAvoidTolls() == commuteTimeSetting2.getAvoidTolls() && ee7.a(commuteTimeSetting.getFormat(), commuteTimeSetting2.getFormat());
    }

    @DexIgnore
    public static final boolean b(List<WeatherLocationWrapper> list, List<WeatherLocationWrapper> list2) {
        String str;
        ee7.b(list, "$this$isWeatherLocationWrapperListEquals");
        ee7.b(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        Iterator<WeatherLocationWrapper> it = list.iterator();
        while (it.hasNext()) {
            WeatherLocationWrapper next = it.next();
            Iterator<T> it2 = list2.iterator();
            while (true) {
                str = null;
                if (!it2.hasNext()) {
                    break;
                }
                T next2 = it2.next();
                T t = next2;
                String id = next != null ? next.getId() : null;
                if (t != null) {
                    str = t.getId();
                }
                if (ee7.a((Object) id, (Object) str)) {
                    str = next2;
                    break;
                }
            }
            if (((WeatherLocationWrapper) str) == null) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final boolean c(String str, Gson gson, String str2) {
        Ringtone ringtone;
        Ringtone ringtone2;
        ee7.b(gson, "gson");
        try {
            ringtone = (Ringtone) gson.a(str, Ringtone.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse ringPhoneSetting");
            ringtone = new Ringtone(null, null, 3, null);
        }
        if (ringtone == null) {
            ringtone = new Ringtone(null, null, 3, null);
        }
        try {
            ringtone2 = (Ringtone) gson.a(str2, Ringtone.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherRingPhoneSetting");
            ringtone2 = new Ringtone(null, null, 3, null);
        }
        if (ringtone2 == null) {
            ringtone2 = new Ringtone(null, null, 3, null);
        }
        return ee7.a((Object) ringtone2.getRingtoneName(), (Object) ringtone.getRingtoneName());
    }

    @DexIgnore
    public static final boolean d(String str, Gson gson, String str2) {
        SecondTimezoneSetting secondTimezoneSetting;
        SecondTimezoneSetting secondTimezoneSetting2;
        ee7.b(gson, "gson");
        try {
            secondTimezoneSetting = (SecondTimezoneSetting) gson.a(str2, SecondTimezoneSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherSecondTzSetting");
            secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        if (secondTimezoneSetting == null) {
            secondTimezoneSetting = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        try {
            secondTimezoneSetting2 = (SecondTimezoneSetting) gson.a(str, SecondTimezoneSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse secondTzSetting");
            secondTimezoneSetting2 = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        if (secondTimezoneSetting2 == null) {
            secondTimezoneSetting2 = new SecondTimezoneSetting(null, null, 0, null, 15, null);
        }
        return ee7.a(secondTimezoneSetting, secondTimezoneSetting2);
    }

    @DexIgnore
    public static final boolean e(String str, Gson gson, String str2) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        WeatherWatchAppSetting weatherWatchAppSetting2;
        ee7.b(gson, "gson");
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) gson.a(str2, WeatherWatchAppSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other weather watch app");
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        if (weatherWatchAppSetting == null) {
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        try {
            weatherWatchAppSetting2 = (WeatherWatchAppSetting) gson.a(str, WeatherWatchAppSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse weather watch app");
            weatherWatchAppSetting2 = new WeatherWatchAppSetting();
        }
        if (weatherWatchAppSetting2 == null) {
            weatherWatchAppSetting2 = new WeatherWatchAppSetting();
        }
        return b(weatherWatchAppSetting2.getLocations(), weatherWatchAppSetting.getLocations());
    }

    @DexIgnore
    public static final boolean b(String str, Gson gson, String str2) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2;
        ee7.b(gson, "gson");
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) gson.a(str2, CommuteTimeWatchAppSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse commute time");
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        if (commuteTimeWatchAppSetting == null) {
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        try {
            commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) gson.a(str, CommuteTimeWatchAppSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other commute time");
            commuteTimeWatchAppSetting2 = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        if (commuteTimeWatchAppSetting2 == null) {
            commuteTimeWatchAppSetting2 = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        return a(commuteTimeWatchAppSetting.getAddresses(), commuteTimeWatchAppSetting2.getAddresses());
    }

    @DexIgnore
    public static final boolean a(List<AddressWrapper> list, List<AddressWrapper> list2) {
        T t;
        boolean z;
        ee7.b(list, "$this$isDestinationListEquals");
        ee7.b(list2, FacebookRequestErrorClassification.KEY_OTHER);
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z2 = true;
        for (AddressWrapper addressWrapper : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                T t2 = t;
                if (!ee7.a((Object) addressWrapper.getAddress(), (Object) t2.getAddress()) || !ee7.a((Object) addressWrapper.getName(), (Object) t2.getName()) || addressWrapper.getAvoidTolls() != t2.getAvoidTolls()) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null) {
                z2 = false;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final boolean a(String str) {
        return TextUtils.isEmpty(str) || ee7.a(str, "{}");
    }
}
