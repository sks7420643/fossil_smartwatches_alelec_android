package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;
    @DexIgnore
    public /* final */ /* synthetic */ Object d;
    @DexIgnore
    public /* final */ /* synthetic */ Object e;
    @DexIgnore
    public /* final */ /* synthetic */ jg3 f;

    @DexIgnore
    public ig3(jg3 jg3, int i, String str, Object obj, Object obj2, Object obj3) {
        this.f = jg3;
        this.a = i;
        this.b = str;
        this.c = obj;
        this.d = obj2;
        this.e = obj3;
    }

    @DexIgnore
    public final void run() {
        wg3 p = ((ii3) this.f).a.p();
        if (p.r()) {
            if (this.f.c == 0) {
                if (this.f.l().o()) {
                    jg3 jg3 = this.f;
                    jg3.b();
                    char unused = jg3.c = 'C';
                } else {
                    jg3 jg32 = this.f;
                    jg32.b();
                    char unused2 = jg32.c = 'c';
                }
            }
            if (this.f.d < 0) {
                jg3 jg33 = this.f;
                long unused3 = jg33.d = jg33.l().n();
            }
            char charAt = "01VDIWEA?".charAt(this.a);
            char a2 = this.f.c;
            long b2 = this.f.d;
            String a3 = jg3.a(true, this.b, this.c, this.d, this.e);
            StringBuilder sb = new StringBuilder(String.valueOf(a3).length() + 24);
            sb.append("2");
            sb.append(charAt);
            sb.append(a2);
            sb.append(b2);
            sb.append(":");
            sb.append(a3);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.b.substring(0, 1024);
            }
            p.d.a(sb2, 1);
            return;
        }
        this.f.a(6, "Persisted config not initialized. Not logging error/warn");
    }
}
