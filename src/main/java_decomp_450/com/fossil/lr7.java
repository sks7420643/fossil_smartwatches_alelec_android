package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr7 implements zq7 {
    @DexIgnore
    public /* final */ yq7 a; // = new yq7();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ qr7 c;

    @DexIgnore
    public lr7(qr7 qr7) {
        ee7.b(qr7, "sink");
        this.c = qr7;
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public void a(yq7 yq7, long j) {
        ee7.b(yq7, "source");
        if (!this.b) {
            this.a.a(yq7, j);
            j();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public yq7 buffer() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 c(long j) {
        if (!this.b) {
            this.a.c(j);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (!this.b) {
            Throwable th = null;
            try {
                if (this.a.x() > 0) {
                    this.c.a(this.a, this.a.x());
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.c.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.b = true;
            if (th != null) {
                throw th;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public tr7 d() {
        return this.c.d();
    }

    @DexIgnore
    @Override // com.fossil.zq7, com.fossil.qr7, java.io.Flushable
    public void flush() {
        if (!this.b) {
            if (this.a.x() > 0) {
                qr7 qr7 = this.c;
                yq7 yq7 = this.a;
                qr7.a(yq7, yq7.x());
            }
            this.c.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 i(long j) {
        if (!this.b) {
            this.a.i(j);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.b;
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 j() {
        if (!this.b) {
            long l = this.a.l();
            if (l > 0) {
                this.c.a(this.a, l);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public OutputStream s() {
        return new a(this);
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.c + ')';
    }

    @DexIgnore
    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) {
        ee7.b(byteBuffer, "source");
        if (!this.b) {
            int write = this.a.write(byteBuffer);
            j();
            return write;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 writeByte(int i) {
        if (!this.b) {
            this.a.writeByte(i);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 writeInt(int i) {
        if (!this.b) {
            this.a.writeInt(i);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 writeShort(int i) {
        if (!this.b) {
            this.a.writeShort(i);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends OutputStream {
        @DexIgnore
        public /* final */ /* synthetic */ lr7 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(lr7 lr7) {
            this.a = lr7;
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.a.close();
        }

        @DexIgnore
        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
            lr7 lr7 = this.a;
            if (!lr7.b) {
                lr7.flush();
            }
        }

        @DexIgnore
        public String toString() {
            return this.a + ".outputStream()";
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(int i) {
            lr7 lr7 = this.a;
            if (!lr7.b) {
                lr7.a.writeByte((int) ((byte) i));
                this.a.j();
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "data");
            lr7 lr7 = this.a;
            if (!lr7.b) {
                lr7.a.write(bArr, i, i2);
                this.a.j();
                return;
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 a(br7 br7) {
        ee7.b(br7, "byteString");
        if (!this.b) {
            this.a.a(br7);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 write(byte[] bArr) {
        ee7.b(bArr, "source");
        if (!this.b) {
            this.a.write(bArr);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 a(String str) {
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        if (!this.b) {
            this.a.a(str);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public zq7 write(byte[] bArr, int i, int i2) {
        ee7.b(bArr, "source");
        if (!this.b) {
            this.a.write(bArr, i, i2);
            j();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    @Override // com.fossil.zq7
    public long a(sr7 sr7) {
        ee7.b(sr7, "source");
        long j = 0;
        while (true) {
            long b2 = sr7.b(this.a, (long) 8192);
            if (b2 == -1) {
                return j;
            }
            j += b2;
            j();
        }
    }
}
