package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu5 implements Factory<vu5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ wu5 a; // = new wu5();
    }

    @DexIgnore
    public static wu5 a() {
        return a.a;
    }

    @DexIgnore
    public static vu5 b() {
        return new vu5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vu5 get() {
        return b();
    }
}
