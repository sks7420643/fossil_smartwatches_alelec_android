package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class er7 extends tr7 {
    @DexIgnore
    public tr7 e;

    @DexIgnore
    public er7(tr7 tr7) {
        ee7.b(tr7, "delegate");
        this.e = tr7;
    }

    @DexIgnore
    public final er7 a(tr7 tr7) {
        ee7.b(tr7, "delegate");
        this.e = tr7;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public tr7 b() {
        return this.e.b();
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public boolean d() {
        return this.e.d();
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public void e() throws IOException {
        this.e.e();
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public long f() {
        return this.e.f();
    }

    @DexIgnore
    public final tr7 g() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public tr7 a(long j, TimeUnit timeUnit) {
        ee7.b(timeUnit, Constants.PROFILE_KEY_UNIT);
        return this.e.a(j, timeUnit);
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public tr7 a(long j) {
        return this.e.a(j);
    }

    @DexIgnore
    @Override // com.fossil.tr7
    public tr7 a() {
        return this.e.a();
    }
}
