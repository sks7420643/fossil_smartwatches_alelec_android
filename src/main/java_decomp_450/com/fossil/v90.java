package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v90 extends q90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<v90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public v90 createFromParcel(Parcel parcel) {
            return new v90(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public v90[] newArray(int i) {
            return new v90[i];
        }
    }

    @DexIgnore
    public v90(byte b2, int i) {
        super(s90.REPLY_MESSAGE);
        this.b = b2;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.q90
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.j, Byte.valueOf(this.b)), r51.i, Integer.valueOf(this.c));
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.b;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.q90
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(((q90) this).a.ordinal());
        parcel.writeByte(this.b);
        parcel.writeInt(this.c);
    }

    @DexIgnore
    public /* synthetic */ v90(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readInt();
    }
}
