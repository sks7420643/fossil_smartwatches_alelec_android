package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutType d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ec0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ec0 createFromParcel(Parcel parcel) {
            return new ec0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ec0[] newArray(int i) {
            return new ec0[i];
        }
    }

    @DexIgnore
    public ec0(byte b, int i, WorkoutType workoutType, long j, boolean z) {
        super(cb0.WORKOUT_START, b, i);
        this.d = workoutType;
        this.e = j;
        this.f = z;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(super.a(), r51.F5, yz0.a(this.d)), r51.G5, Long.valueOf(this.e)), r51.E5, Boolean.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(ec0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            ec0 ec0 = (ec0) obj;
            return this.d == ec0.d && this.e == ec0.e && this.f == ec0.f;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.StartWorkoutRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = Long.valueOf(this.e).hashCode();
        return Boolean.valueOf(this.f).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.d.getValue());
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ ec0(Parcel parcel, zd7 zd7) {
        super(parcel);
        WorkoutType c = yz0.c(parcel.readByte());
        this.d = c == null ? WorkoutType.UNKNOWN : c;
        this.e = parcel.readLong();
        this.f = parcel.readInt() != 1 ? false : true;
    }
}
