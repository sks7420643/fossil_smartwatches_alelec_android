package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz1 extends n62<pz1> {
    @DexIgnore
    public /* final */ GoogleSignInOptions E;

    @DexIgnore
    public dz1(Context context, Looper looper, j62 j62, GoogleSignInOptions googleSignInOptions, a12.b bVar, a12.c cVar) {
        super(context, looper, 91, j62, bVar, cVar);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.a().a() : googleSignInOptions;
        if (!j62.d().isEmpty()) {
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(googleSignInOptions);
            for (Scope scope : j62.d()) {
                aVar.a(scope, new Scope[0]);
            }
            googleSignInOptions = aVar.a();
        }
        this.E = googleSignInOptions;
    }

    @DexIgnore
    public final GoogleSignInOptions I() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        if (queryLocalInterface instanceof pz1) {
            return (pz1) queryLocalInterface;
        }
        return new qz1(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public final boolean d() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public final int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public final Intent m() {
        return ez1.a(w(), this.E);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
}
