package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.HorizontalScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public final class tz6 extends HorizontalScrollView {
    @DexIgnore
    public long a; // = -1;
    @DexIgnore
    public b b;
    @DexIgnore
    public Runnable c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            tz6 tz6 = tz6.this;
            if (currentTimeMillis - tz6.a > 100) {
                tz6.a = -1;
                tz6.b.a();
                return;
            }
            tz6.postDelayed(this, 100);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void onScrollChanged();
    }

    @DexIgnore
    public tz6(Context context, b bVar) {
        super(context);
        this.b = bVar;
    }

    @DexIgnore
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        b bVar = this.b;
        if (bVar != null) {
            bVar.onScrollChanged();
            if (this.a == -1) {
                postDelayed(this.c, 100);
            }
            this.a = System.currentTimeMillis();
        }
    }
}
