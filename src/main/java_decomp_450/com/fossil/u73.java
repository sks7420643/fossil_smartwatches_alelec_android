package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u73 extends tm2 implements v63 {
    @DexIgnore
    public u73(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(float f, float f2) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        zza.writeFloat(f2);
        Parcel a = a(3, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 e(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        Parcel a = a(4, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 n() throws RemoteException {
        Parcel a = a(1, zza());
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 z() throws RemoteException {
        Parcel a = a(2, zza());
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        Parcel a = a(5, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(float f, int i, int i2) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        zza.writeInt(i);
        zza.writeInt(i2);
        Parcel a = a(6, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(CameraPosition cameraPosition) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, cameraPosition);
        Parcel a = a(7, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(LatLng latLng) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLng);
        Parcel a = a(8, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(LatLng latLng, float f) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLng);
        zza.writeFloat(f);
        Parcel a = a(9, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.v63
    public final ab2 a(LatLngBounds latLngBounds, int i) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLngBounds);
        zza.writeInt(i);
        Parcel a = a(10, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
