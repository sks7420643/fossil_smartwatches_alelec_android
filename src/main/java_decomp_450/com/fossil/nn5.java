package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn5 implements Factory<mn5> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public nn5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static nn5 a(Provider<UserRepository> provider) {
        return new nn5(provider);
    }

    @DexIgnore
    public static mn5 a(UserRepository userRepository) {
        return new mn5(userRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public mn5 get() {
        return a(this.a.get());
    }
}
