package com.fossil;

import com.fossil.fl4;
import com.fossil.jw6;
import com.fossil.mw6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt6 extends it6 {
    @DexIgnore
    public jw6 e;
    @DexIgnore
    public mw6 f;
    @DexIgnore
    public SignUpEmailAuth g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ jt6 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<jw6.d, jw6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ lt6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(lt6 lt6) {
            this.a = lt6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(jw6.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.i.f();
            this.a.i.L0();
        }

        @DexIgnore
        public void a(jw6.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.i.f();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "resendOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.i.f(cVar.a(), cVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<mw6.d, mw6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ lt6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(lt6 lt6) {
            this.a = lt6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mw6.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.i.f();
            jt6 a2 = this.a.i;
            SignUpEmailAuth l = this.a.l();
            if (l != null) {
                a2.a(l.getEmail(), 10, 20);
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public void a(mw6.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.i.f();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = et6.U.a();
            local.d(a2, "verifyOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.i.P(cVar.a() == 401);
            this.a.i.f(cVar.a(), cVar.b());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public lt6(jt6 jt6) {
        ee7.b(jt6, "mView");
        this.i = jt6;
    }

    @DexIgnore
    public final void b(String[] strArr) {
        this.h = "";
        int length = strArr.length;
        int i2 = 0;
        while (i2 < length) {
            String str = strArr[i2];
            String str2 = this.h;
            if (str != null) {
                this.h = ee7.a(str2, (Object) nh7.d((CharSequence) str).toString());
                i2++;
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            this.i.h(signUpEmailAuth.getEmail());
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.it6
    public void h() {
        this.i.X();
    }

    @DexIgnore
    @Override // com.fossil.it6
    public void i() {
        this.i.P(false);
        this.i.g();
        jw6 jw6 = this.e;
        if (jw6 != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            String email = signUpEmailAuth != null ? signUpEmailAuth.getEmail() : null;
            if (email != null) {
                jw6.a(new jw6.b(email), new b(this));
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.it6
    public void j() {
        jt6 jt6 = this.i;
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            jt6.a(signUpEmailAuth);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.it6
    public void k() {
        this.i.P(false);
        this.i.g();
        mw6 mw6 = this.f;
        if (mw6 != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            if (signUpEmailAuth != null) {
                String email = signUpEmailAuth.getEmail();
                String str = this.h;
                if (str != null) {
                    mw6.a(new mw6.b(email, str), new c(this));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mVerifyEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public final SignUpEmailAuth l() {
        return this.g;
    }

    @DexIgnore
    public void m() {
        this.i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.it6
    public void a(String[] strArr) {
        String str;
        ee7.b(strArr, "codes");
        b(strArr);
        String str2 = this.h;
        boolean z = false;
        if (!(str2 == null || mh7.a(str2)) && (str = this.h) != null && str.length() == 4) {
            z = true;
        }
        this.i.n(z);
    }

    @DexIgnore
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        ee7.b(signUpEmailAuth, "emailAuth");
        this.g = signUpEmailAuth;
    }
}
