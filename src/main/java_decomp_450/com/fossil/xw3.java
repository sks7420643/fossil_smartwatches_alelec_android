package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xw3<K, V> extends sw3<K, V> implements xz3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7431625294878419160L;

    @DexIgnore
    public xw3(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    @Override // com.fossil.sw3
    public abstract Set<V> createCollection();

    @DexIgnore
    @Override // com.fossil.vw3
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.vw3, com.fossil.zy3
    @CanIgnoreReturnValue
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    @Override // com.fossil.sw3
    public Set<V> createUnmodifiableEmptyCollection() {
        return iy3.of();
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.vw3, com.fossil.zy3
    public Set<Map.Entry<K, V>> entries() {
        return (Set) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.zy3
    public Set<V> get(K k) {
        return (Set) super.get((Object) k);
    }

    @DexIgnore
    @Override // com.fossil.sw3
    @CanIgnoreReturnValue
    public Set<V> removeAll(Object obj) {
        return (Set) super.removeAll(obj);
    }

    @DexIgnore
    @Override // com.fossil.sw3, com.fossil.vw3
    @CanIgnoreReturnValue
    public Set<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (Set) super.replaceValues((Object) k, (Iterable) iterable);
    }
}
