package com.fossil;

import android.database.Cursor;
import android.os.Build;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ti {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, a> b;
    @DexIgnore
    public /* final */ Set<b> c;
    @DexIgnore
    public /* final */ Set<d> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ List<String> d;
        @DexIgnore
        public /* final */ List<String> e;

        @DexIgnore
        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.equals(bVar.a) && this.b.equals(bVar.b) && this.c.equals(bVar.c) && this.d.equals(bVar.d)) {
                return this.e.equals(bVar.e);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ForeignKey{referenceTable='" + this.a + '\'' + ", onDelete='" + this.b + '\'' + ", onUpdate='" + this.c + '\'' + ", columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Comparable<c> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public c(int i, int i2, String str, String str2) {
            this.a = i;
            this.b = i2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = this.a - cVar.a;
            return i == 0 ? this.b - cVar.b : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ List<String> c;

        @DexIgnore
        public d(String str, boolean z, List<String> list) {
            this.a = str;
            this.b = z;
            this.c = list;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.b != dVar.b || !this.c.equals(dVar.c)) {
                return false;
            }
            if (this.a.startsWith("index_")) {
                return dVar.a.startsWith("index_");
            }
            return this.a.equals(dVar.a);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.a.startsWith("index_")) {
                i = -1184239155;
            } else {
                i = this.a.hashCode();
            }
            return (((i * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Index{name='" + this.a + '\'' + ", unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    @DexIgnore
    public ti(String str, Map<String, a> map, Set<b> set, Set<d> set2) {
        Set<d> set3;
        this.a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = Collections.unmodifiableSet(set2);
        }
        this.d = set3;
    }

    @DexIgnore
    public static ti a(wi wiVar, String str) {
        return new ti(str, b(wiVar, str), c(wiVar, str), d(wiVar, str));
    }

    @DexIgnore
    public static Map<String, a> b(wi wiVar, String str) {
        Cursor query = wiVar.query("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (query.getColumnCount() > 0) {
                int columnIndex = query.getColumnIndex("name");
                int columnIndex2 = query.getColumnIndex("type");
                int columnIndex3 = query.getColumnIndex("notnull");
                int columnIndex4 = query.getColumnIndex("pk");
                int columnIndex5 = query.getColumnIndex("dflt_value");
                while (query.moveToNext()) {
                    String string = query.getString(columnIndex);
                    hashMap.put(string, new a(string, query.getString(columnIndex2), query.getInt(columnIndex3) != 0, query.getInt(columnIndex4), query.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static Set<b> c(wi wiVar, String str) {
        HashSet hashSet = new HashSet();
        Cursor query = wiVar.query("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("id");
            int columnIndex2 = query.getColumnIndex("seq");
            int columnIndex3 = query.getColumnIndex("table");
            int columnIndex4 = query.getColumnIndex("on_delete");
            int columnIndex5 = query.getColumnIndex("on_update");
            List<c> a2 = a(query);
            int count = query.getCount();
            for (int i = 0; i < count; i++) {
                query.moveToPosition(i);
                if (query.getInt(columnIndex2) == 0) {
                    int i2 = query.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (c cVar : a2) {
                        if (cVar.a == i2) {
                            arrayList.add(cVar.c);
                            arrayList2.add(cVar.d);
                        }
                    }
                    hashSet.add(new b(query.getString(columnIndex3), query.getString(columnIndex4), query.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static Set<d> d(wi wiVar, String str) {
        Cursor query = wiVar.query("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("name");
            int columnIndex2 = query.getColumnIndex("origin");
            int columnIndex3 = query.getColumnIndex(DatabaseFieldConfigLoader.FIELD_NAME_UNIQUE);
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    HashSet hashSet = new HashSet();
                    while (query.moveToNext()) {
                        if ("c".equals(query.getString(columnIndex2))) {
                            String string = query.getString(columnIndex);
                            boolean z = true;
                            if (query.getInt(columnIndex3) != 1) {
                                z = false;
                            }
                            d a2 = a(wiVar, string, z);
                            if (a2 == null) {
                                query.close();
                                return null;
                            }
                            hashSet.add(a2);
                        }
                    }
                    query.close();
                    return hashSet;
                }
            }
            return null;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Set<d> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || ti.class != obj.getClass()) {
            return false;
        }
        ti tiVar = (ti) obj;
        String str = this.a;
        if (str == null ? tiVar.a != null : !str.equals(tiVar.a)) {
            return false;
        }
        Map<String, a> map = this.b;
        if (map == null ? tiVar.b != null : !map.equals(tiVar.b)) {
            return false;
        }
        Set<b> set2 = this.c;
        if (set2 == null ? tiVar.c != null : !set2.equals(tiVar.c)) {
            return false;
        }
        Set<d> set3 = this.d;
        if (set3 == null || (set = tiVar.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, a> map = this.b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<b> set = this.c;
        if (set != null) {
            i = set.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "TableInfo{name='" + this.a + '\'' + ", columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }

    @DexIgnore
    public static List<c> a(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ int g;

        @DexIgnore
        public a(String str, String str2, boolean z, int i, String str3, int i2) {
            this.a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = a(str2);
            this.f = str3;
            this.g = i2;
        }

        @DexIgnore
        public static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            String str;
            String str2;
            String str3;
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.e != aVar.e) {
                    return false;
                }
            } else if (a() != aVar.a()) {
                return false;
            }
            if (!this.a.equals(aVar.a) || this.d != aVar.d) {
                return false;
            }
            if (this.g == 1 && aVar.g == 2 && (str3 = this.f) != null && !str3.equals(aVar.f)) {
                return false;
            }
            if (this.g == 2 && aVar.g == 1 && (str2 = aVar.f) != null && !str2.equals(this.f)) {
                return false;
            }
            int i = this.g;
            if ((i == 0 || i != aVar.g || ((str = this.f) == null ? aVar.f == null : str.equals(aVar.f))) && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.c) * 31) + (this.d ? 1231 : 1237)) * 31) + this.e;
        }

        @DexIgnore
        public String toString() {
            return "Column{name='" + this.a + '\'' + ", type='" + this.b + '\'' + ", affinity='" + this.c + '\'' + ", notNull=" + this.d + ", primaryKeyPosition=" + this.e + ", defaultValue='" + this.f + '\'' + '}';
        }

        @DexIgnore
        public boolean a() {
            return this.e > 0;
        }
    }

    @DexIgnore
    public static d a(wi wiVar, String str, boolean z) {
        Cursor query = wiVar.query("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("seqno");
            int columnIndex2 = query.getColumnIndex("cid");
            int columnIndex3 = query.getColumnIndex("name");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    TreeMap treeMap = new TreeMap();
                    while (query.moveToNext()) {
                        if (query.getInt(columnIndex2) >= 0) {
                            int i = query.getInt(columnIndex);
                            treeMap.put(Integer.valueOf(i), query.getString(columnIndex3));
                        }
                    }
                    ArrayList arrayList = new ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    d dVar = new d(str, z, arrayList);
                    query.close();
                    return dVar;
                }
            }
            return null;
        } finally {
            query.close();
        }
    }
}
