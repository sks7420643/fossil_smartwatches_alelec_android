package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class is {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final is a() {
            if (Build.VERSION.SDK_INT < 26 || hs.b.a()) {
                return new js(false);
            }
            int i = Build.VERSION.SDK_INT;
            if (i == 26 || i == 27) {
                return ms.e;
            }
            return new js(true);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public is() {
    }

    @DexIgnore
    public abstract boolean a(rt rtVar);

    @DexIgnore
    public /* synthetic */ is(zd7 zd7) {
        this();
    }
}
