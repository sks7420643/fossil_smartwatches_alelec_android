package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ln6 {
    @DexIgnore
    public /* final */ rn6 a;

    @DexIgnore
    public ln6(rn6 rn6) {
        ee7.b(rn6, "view");
        jw3.a(rn6, "view cannot be null!", new Object[0]);
        ee7.a((Object) rn6, "checkNotNull(view, \"view cannot be null!\")");
        this.a = rn6;
    }

    @DexIgnore
    public final rn6 a() {
        return this.a;
    }
}
