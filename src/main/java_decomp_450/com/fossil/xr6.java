package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.fl4;
import com.fossil.kw6;
import com.fossil.wl5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr6 extends rr6 {
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public ShineDevice e;
    @DexIgnore
    public /* final */ List<r87<ShineDevice, String>> f; // = new ArrayList();
    @DexIgnore
    public /* final */ HashMap<String, List<Integer>> g; // = new HashMap<>();
    @DexIgnore
    public Handler h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ ArrayList<Device> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<SKUModel> l; // = new ArrayList<>();
    @DexIgnore
    public if5 m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ d q; // = new d(this);
    @DexIgnore
    public /* final */ e r; // = new e(this);
    @DexIgnore
    public /* final */ ur6 s;
    @DexIgnore
    public /* final */ wl5 t;
    @DexIgnore
    public /* final */ DeviceRepository u;
    @DexIgnore
    public /* final */ ad5 v;
    @DexIgnore
    public /* final */ ek5 w;
    @DexIgnore
    public /* final */ kw6 x;
    @DexIgnore
    public /* final */ ch5 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = xr6.class.getSimpleName();
            ee7.a((Object) simpleName, "PairingPresenter::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public void run() {
            xr6.this.c(true);
            if (xr6.this.s().isEmpty()) {
                xr6.this.s.B0();
                return;
            }
            ur6 h = xr6.this.s;
            xr6 xr6 = xr6.this;
            h.q(xr6.a(xr6.s()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1", f = "PairingPresenter.kt", l = {425}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ShineDevice $shineDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xr6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1$deviceName$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    DeviceRepository c = this.this$0.this$0.u;
                    String serial = this.this$0.$shineDevice.getSerial();
                    ee7.a((Object) serial, "shineDevice.serial");
                    return c.getDeviceNameBySerial(serial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xr6 xr6, ShineDevice shineDevice, fb7 fb7) {
            super(2, fb7);
            this.this$0 = xr6;
            this.$shineDevice = shineDevice;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$shineDevice, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            T t = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) obj;
            Iterator<T> it = this.this$0.s().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (pb7.a(ee7.a((Object) ((ShineDevice) next.getFirst()).getSerial(), (Object) this.$shineDevice.getSerial())).booleanValue()) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 == null) {
                this.this$0.s().add(new r87<>(this.$shineDevice, str));
            } else {
                ((ShineDevice) t2.getFirst()).updateRssi(this.$shineDevice.getRssi());
            }
            ur6 h = this.this$0.s;
            xr6 xr6 = this.this$0;
            h.l(xr6.a(xr6.s()));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<wl5.j, wl5.i> {
        @DexIgnore
        public /* final */ /* synthetic */ xr6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1", f = "PairingPresenter.kt", l = {312}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ wl5.i $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xr6$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xr6$d$a$a  reason: collision with other inner class name */
            public static final class C0246a extends zb7 implements kd7<yi7, fb7<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0246a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0246a aVar = new C0246a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super HashMap<String, String>> fb7) {
                    return ((C0246a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        a aVar = this.this$0;
                        return aVar.this$0.a.d(aVar.$errorValue.a());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, wl5.i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$errorValue = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$errorValue, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = xr6.z.a();
                    local.d(a2, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
                    ti7 a3 = this.this$0.a.b();
                    C0246a aVar = new C0246a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a3, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                HashMap hashMap = (HashMap) obj;
                if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
                    this.this$0.a.a("pair_fail", hashMap);
                }
                this.this$0.a.s.a();
                wl5.i iVar = this.$errorValue;
                if (iVar instanceof wl5.d) {
                    if (this.this$0.a.p) {
                        ur6 h = this.this$0.a.s;
                        ShineDevice g = this.this$0.a.e;
                        if (g != null) {
                            String serial = g.getSerial();
                            ee7.a((Object) serial, "mPairingDevice!!.serial");
                            h.q(serial);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ur6 h2 = this.this$0.a.s;
                        ShineDevice g2 = this.this$0.a.e;
                        if (g2 != null) {
                            String serial2 = g2.getSerial();
                            ee7.a((Object) serial2, "mPairingDevice!!.serial");
                            h2.A(serial2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else if (iVar instanceof wl5.k) {
                    this.this$0.a.s.a(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
                } else if (iVar instanceof wl5.e) {
                    this.this$0.a.s.B();
                }
                String valueOf = String.valueOf(this.$errorValue.b());
                gf5 a4 = qd5.f.a("setup_device_session_optional_error");
                a4.a("error_code", valueOf);
                if5 p = this.this$0.a.p();
                if (p != null) {
                    p.a(a4);
                }
                if5 p2 = this.this$0.a.p();
                if (p2 != null) {
                    p2.a(valueOf);
                }
                qd5.f.e("setup_device_session");
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1", f = "PairingPresenter.kt", l = {283, 289, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $devicePairingSerial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    String str;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        String deviceNameBySerial = this.this$0.this$0.a.u.getDeviceNameBySerial(this.this$0.$devicePairingSerial);
                        MisfitDeviceProfile a = be5.o.e().a(this.this$0.$devicePairingSerial);
                        if (a == null || (str = a.getFirmwareVersion()) == null) {
                            str = "";
                        }
                        qd5.f.c().a(be5.o.b(this.this$0.$devicePairingSerial), deviceNameBySerial, str);
                        return this.this$0.this$0.a.x.a(new kw6.b(this.this$0.$devicePairingSerial), (fl4.e) null);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xr6$d$b$b")
            @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onSuccess$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xr6$d$b$b  reason: collision with other inner class name */
            public static final class C0247b extends zb7 implements kd7<yi7, fb7<? super HashMap<String, String>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0247b(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0247b bVar = new C0247b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super HashMap<String, String>> fb7) {
                    return ((C0247b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        b bVar = this.this$0;
                        return bVar.this$0.a.d(bVar.$devicePairingSerial);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$devicePairingSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$devicePairingSerial, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00a3  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3 A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r8.label
                    r2 = 0
                    r3 = 3
                    r4 = 2
                    r5 = 1
                    if (r1 == 0) goto L_0x003b
                    if (r1 == r5) goto L_0x0033
                    if (r1 == r4) goto L_0x0027
                    if (r1 != r3) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    java.util.HashMap r0 = (java.util.HashMap) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r9)
                    goto L_0x00c4
                L_0x001f:
                    java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r9.<init>(r0)
                    throw r9
                L_0x0027:
                    java.lang.Object r1 = r8.L$1
                    java.util.HashMap r1 = (java.util.HashMap) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r9)
                    goto L_0x0099
                L_0x0033:
                    java.lang.Object r1 = r8.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r9)
                    goto L_0x005b
                L_0x003b:
                    com.fossil.t87.a(r9)
                    com.fossil.yi7 r9 = r8.p$
                    com.fossil.xr6$d r1 = r8.this$0
                    com.fossil.xr6 r1 = r1.a
                    com.fossil.ti7 r1 = r1.b()
                    com.fossil.xr6$d$b$b r6 = new com.fossil.xr6$d$b$b
                    r6.<init>(r8, r2)
                    r8.L$0 = r9
                    r8.label = r5
                    java.lang.Object r1 = com.fossil.vh7.a(r1, r6, r8)
                    if (r1 != r0) goto L_0x0058
                    return r0
                L_0x0058:
                    r7 = r1
                    r1 = r9
                    r9 = r7
                L_0x005b:
                    java.util.HashMap r9 = (java.util.HashMap) r9
                    java.lang.String r5 = "Style_Number"
                    boolean r5 = r9.containsKey(r5)
                    if (r5 == 0) goto L_0x007d
                    java.lang.String r5 = "Device_Name"
                    boolean r5 = r9.containsKey(r5)
                    if (r5 == 0) goto L_0x007d
                    com.fossil.xr6$d r5 = r8.this$0
                    com.fossil.xr6 r5 = r5.a
                    java.lang.String r6 = "pair_success"
                    r5.a(r6, r9)
                    com.fossil.xr6$d r5 = r8.this$0
                    com.fossil.xr6 r5 = r5.a
                    r5.a(r9)
                L_0x007d:
                    com.fossil.xr6$d r5 = r8.this$0
                    com.fossil.xr6 r5 = r5.a
                    com.fossil.ti7 r5 = r5.c()
                    com.fossil.xr6$d$b$a r6 = new com.fossil.xr6$d$b$a
                    r6.<init>(r8, r2)
                    r8.L$0 = r1
                    r8.L$1 = r9
                    r8.label = r4
                    java.lang.Object r2 = com.fossil.vh7.a(r5, r6, r8)
                    if (r2 != r0) goto L_0x0097
                    return r0
                L_0x0097:
                    r2 = r1
                    r1 = r9
                L_0x0099:
                    com.fossil.xr6$d r9 = r8.this$0
                    com.fossil.xr6 r9 = r9.a
                    com.fossil.if5 r9 = r9.p()
                    if (r9 == 0) goto L_0x00a8
                    java.lang.String r4 = ""
                    r9.a(r4)
                L_0x00a8:
                    com.fossil.qd5$a r9 = com.fossil.qd5.f
                    java.lang.String r4 = "setup_device_session"
                    r9.e(r4)
                    com.fossil.xr6$d r9 = r8.this$0
                    com.fossil.xr6 r9 = r9.a
                    com.fossil.ek5 r9 = r9.w
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r3
                    java.lang.Object r9 = r9.b(r8)
                    if (r9 != r0) goto L_0x00c4
                    return r0
                L_0x00c4:
                    com.fossil.xr6$d r9 = r8.this$0
                    com.fossil.xr6 r9 = r9.a
                    r9.x()
                    com.fossil.i97 r9 = com.fossil.i97.a
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.xr6.d.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(xr6 xr6) {
            this.a = xr6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(wl5.j jVar) {
            ee7.b(jVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xr6.z.a();
            local.d(a2, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.a.y.a0() + ", response=" + jVar.getClass().getSimpleName());
            if (jVar instanceof wl5.a) {
                this.a.s.a();
                this.a.s.a(((wl5.a) jVar).a(), this.a.q());
            } else if (jVar instanceof wl5.f) {
                this.a.s.a();
                this.a.s.b(((wl5.f) jVar).a(), this.a.q());
            } else if (jVar instanceof wl5.m) {
                wl5.m mVar = (wl5.m) jVar;
                this.a.s.a(mVar.a(), this.a.q(), mVar.b());
            } else if (jVar instanceof wl5.b) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = xr6.z.a();
                StringBuilder sb = new StringBuilder();
                sb.append("authorizeDeviceResponse, isStartTimer=");
                wl5.b bVar = (wl5.b) jVar;
                sb.append(bVar.a());
                local2.d(a3, sb.toString());
                if (bVar.a()) {
                    this.a.s.F(true);
                    return;
                }
                this.a.p = false;
                this.a.s.b();
                this.a.s.F(false);
            } else if (jVar instanceof wl5.l) {
                String deviceId = ((wl5.l) jVar).a().getDeviceId();
                PortfolioApp.g0.c().a(this.a.v, false, 13);
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, deviceId, xr6.z.a(), "Pair Success");
                ik7 unused = xh7.b(this.a.e(), null, null, new b(this, deviceId, null), 3, null);
            }
        }

        @DexIgnore
        public void a(wl5.i iVar) {
            ee7.b(iVar, "errorValue");
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, iVar, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ xr6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(xr6 xr6) {
            this.a = xr6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            ShineDevice shineDevice = (ShineDevice) intent.getParcelableExtra("device");
            if (shineDevice == null) {
                FLogger.INSTANCE.getLocal().d(xr6.z.a(), "scanReceiver - ShineDevice is NULL!!!");
                return;
            }
            String serial = shineDevice.getSerial();
            if (TextUtils.isEmpty(serial)) {
                FLogger.INSTANCE.getLocal().d(xr6.z.a(), "scanReceiver - ShineDeviceSerial is NULL => wearOS device=" + shineDevice);
                this.a.e(shineDevice);
                return;
            }
            FLogger.INSTANCE.getLocal().d(xr6.z.a(), "scanReceiver - receive device serial=" + serial + " allSkuModelSize " + this.a.o().size());
            be5 e = be5.o.e();
            T t2 = null;
            if (serial == null) {
                ee7.a();
                throw null;
            } else if (e.a(serial, this.a.o())) {
                int rssi = shineDevice.getRssi();
                this.a.a(serial, rssi);
                Iterator<T> it = this.a.s().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (ee7.a((Object) ((ShineDevice) t.getFirst()).getSerial(), (Object) serial)) {
                        break;
                    }
                }
                T t3 = t;
                Iterator<T> it2 = this.a.t().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (ee7.a((Object) next.getDeviceId(), (Object) serial)) {
                        t2 = next;
                        break;
                    }
                }
                T t4 = t2;
                if (t4 == null && t3 == null) {
                    FLogger.INSTANCE.getLocal().d(xr6.z.a(), "Add device " + serial + " to list");
                    this.a.d(shineDevice);
                } else if (t4 != null || t3 == null || this.a.r()) {
                    FLogger.INSTANCE.getLocal().d(xr6.z.a(), "Device already in list, ignore it " + serial);
                } else {
                    FLogger.INSTANCE.getLocal().d(xr6.z.a(), "Pre-scan is not complete, update RSSI for scanned devices");
                    int c = this.a.c(serial);
                    if (c != Integer.MIN_VALUE) {
                        rssi = c;
                    }
                    ((ShineDevice) t3.getFirst()).updateRssi(rssi);
                    ur6 h = this.a.s;
                    xr6 xr6 = this.a;
                    h.l(xr6.a(xr6.s()));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1", f = "PairingPresenter.kt", l = {99, 101}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xr6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Device>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.u.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1$2", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends SKUModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends SKUModel>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.u.getSupportedSku();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(xr6 xr6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = xr6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00c7  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00f9  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0116  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 2
                r3 = 0
                r4 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r4) goto L_0x0023
                if (r1 != r2) goto L_0x001b
                java.lang.Object r0 = r12.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r12.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r13)
                goto L_0x0089
            L_0x001b:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0023:
                java.lang.Object r1 = r12.L$1
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                java.lang.Object r5 = r12.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r13)
                goto L_0x005b
            L_0x002f:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r5 = r12.p$
                com.fossil.xr6 r13 = r12.this$0
                java.util.ArrayList r13 = r13.t()
                r13.clear()
                com.fossil.xr6 r13 = r12.this$0
                java.util.ArrayList r1 = r13.t()
                com.fossil.xr6 r13 = r12.this$0
                com.fossil.ti7 r13 = r13.c()
                com.fossil.xr6$f$a r6 = new com.fossil.xr6$f$a
                r6.<init>(r12, r3)
                r12.L$0 = r5
                r12.L$1 = r1
                r12.label = r4
                java.lang.Object r13 = com.fossil.vh7.a(r13, r6, r12)
                if (r13 != r0) goto L_0x005b
                return r0
            L_0x005b:
                java.util.Collection r13 = (java.util.Collection) r13
                r1.addAll(r13)
                com.fossil.xr6 r13 = r12.this$0
                java.util.ArrayList r13 = r13.o()
                r13.clear()
                com.fossil.xr6 r13 = r12.this$0
                java.util.ArrayList r13 = r13.o()
                com.fossil.xr6 r1 = r12.this$0
                com.fossil.ti7 r1 = r1.c()
                com.fossil.xr6$f$b r6 = new com.fossil.xr6$f$b
                r6.<init>(r12, r3)
                r12.L$0 = r5
                r12.L$1 = r13
                r12.label = r2
                java.lang.Object r1 = com.fossil.vh7.a(r1, r6, r12)
                if (r1 != r0) goto L_0x0087
                return r0
            L_0x0087:
                r0 = r13
                r13 = r1
            L_0x0089:
                java.util.Collection r13 = (java.util.Collection) r13
                r0.addAll(r13)
                com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r13 = r13.c()
                com.fossil.qe r13 = com.fossil.qe.a(r13)
                com.fossil.xr6 r0 = r12.this$0
                com.fossil.xr6$e r0 = r0.r
                android.content.IntentFilter r1 = new android.content.IntentFilter
                java.lang.String r2 = "SCAN_DEVICE_FOUND"
                r1.<init>(r2)
                r13.a(r0, r1)
                com.fossil.xr6 r13 = r12.this$0
                com.fossil.wl5 r13 = r13.t
                r13.k()
                com.fossil.xr6 r13 = r12.this$0
                android.os.Handler r0 = new android.os.Handler
                android.os.Looper r1 = android.os.Looper.getMainLooper()
                r0.<init>(r1)
                r13.h = r0
                com.fossil.xr6 r13 = r12.this$0
                com.misfit.frameworks.buttonservice.model.ShineDevice r13 = r13.e
                if (r13 == 0) goto L_0x00ef
                com.fossil.xr6 r13 = r12.this$0
                com.fossil.wl5 r13 = r13.t
                com.fossil.xr6 r0 = r12.this$0
                com.misfit.frameworks.buttonservice.model.ShineDevice r0 = r0.e
                if (r0 == 0) goto L_0x00eb
                com.fossil.xr6 r1 = r12.this$0
                com.fossil.xr6$d r1 = r1.q
                r13.a(r0, r1)
                com.fossil.nj5 r13 = com.fossil.nj5.d
                com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r0 = new com.misfit.frameworks.buttonservice.communite.CommunicateMode[r4]
                r1 = 0
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK
                r0[r1] = r2
                r13.a(r0)
                goto L_0x00ef
            L_0x00eb:
                com.fossil.ee7.a()
                throw r3
            L_0x00ef:
                com.fossil.xg5 r3 = com.fossil.xg5.b
                com.fossil.xr6 r13 = r12.this$0
                com.fossil.ur6 r13 = r13.s
                if (r13 == 0) goto L_0x0116
                com.fossil.zq6 r13 = (com.fossil.zq6) r13
                android.content.Context r4 = r13.getContext()
                com.fossil.xg5$a r5 = com.fossil.xg5.a.PAIR_DEVICE
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 56
                r11 = 0
                boolean r13 = com.fossil.xg5.a(r3, r4, r5, r6, r7, r8, r9, r10, r11)
                if (r13 == 0) goto L_0x0113
                com.fossil.xr6 r13 = r12.this$0
                r13.w()
            L_0x0113:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x0116:
                com.fossil.x87 r13 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment"
                r13.<init>(r0)
                throw r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xr6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<lm5, jm5> {
        @DexIgnore
        public /* final */ /* synthetic */ xr6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public g(xr6 xr6, String str) {
            this.a = xr6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(lm5 lm5) {
            ee7.b(lm5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xr6.z.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            this.a.m();
        }

        @DexIgnore
        public void a(jm5 jm5) {
            ee7.b(jm5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xr6.z.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + jm5.a());
            this.a.s.a();
            int i = yr6.a[jm5.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    this.a.s.U(this.b);
                } else if (i == 3) {
                    this.a.s.x(this.b);
                } else if (i != 4) {
                    this.a.s.d(jm5.a().ordinal(), this.b);
                } else {
                    FLogger.INSTANCE.getLocal().d(xr6.z.a(), "User deny stopping workout");
                }
            } else if (jm5.b() != null) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(new ArrayList(jm5.b()));
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                ur6 h = this.a.s;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    h.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore
    public xr6(ur6 ur6, wl5 wl5, DeviceRepository deviceRepository, ad5 ad5, ek5 ek5, kw6 kw6, ch5 ch5) {
        ee7.b(ur6, "mPairingView");
        ee7.b(wl5, "mLinkDeviceUseCase");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(ek5, "mMusicControlComponent");
        ee7.b(kw6, "mSetNotificationUseCase");
        ee7.b(ch5, "mSharePrefs");
        this.s = ur6;
        this.t = wl5;
        this.u = deviceRepository;
        this.v = ad5;
        this.w = ek5;
        this.x = kw6;
        this.y = ch5;
    }

    @DexIgnore
    public final void A() {
        try {
            IButtonConnectivity b2 = PortfolioApp.g0.b();
            if (b2 != null) {
                b2.deviceStopScan();
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final ArrayList<SKUModel> o() {
        return this.l;
    }

    @DexIgnore
    public final if5 p() {
        return this.m;
    }

    @DexIgnore
    public final boolean q() {
        return this.j;
    }

    @DexIgnore
    public final boolean r() {
        return this.n;
    }

    @DexIgnore
    public final List<r87<ShineDevice, String>> s() {
        return this.f;
    }

    @DexIgnore
    public final ArrayList<Device> t() {
        return this.k;
    }

    @DexIgnore
    public final boolean u() {
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale.getLanguage())) {
            return false;
        }
        Locale locale2 = Locale.getDefault();
        ee7.a((Object) locale2, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale2.getCountry())) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        Locale locale3 = Locale.getDefault();
        ee7.a((Object) locale3, "Locale.getDefault()");
        sb.append(locale3.getLanguage());
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        Locale locale4 = Locale.getDefault();
        ee7.a((Object) locale4, "Locale.getDefault()");
        sb.append(locale4.getCountry());
        String sb2 = sb.toString();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "language: " + sb2);
        if (mh7.b(sb2, "zh_CN", true) || mh7.b(sb2, "zh_SG", true)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean v() {
        return this.i;
    }

    @DexIgnore
    public void w() {
        if (!this.i && this.o) {
            if (this.f.isEmpty()) {
                this.s.K();
                this.n = false;
                Handler handler = this.h;
                if (handler != null) {
                    handler.postDelayed(new b(), (long) 15000);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                this.s.q(a(this.f));
            }
            z();
        }
    }

    @DexIgnore
    public final void x() {
        FLogger.INSTANCE.getLocal().d(z.a(), "onUserContinueToNextStep");
        this.s.a();
        if (be5.o.f(PortfolioApp.g0.c().c())) {
            this.s.u();
        } else {
            this.s.i();
        }
    }

    @DexIgnore
    public void y() {
        this.s.a(this);
    }

    @DexIgnore
    public void z() {
        try {
            IButtonConnectivity b2 = PortfolioApp.g0.b();
            if (b2 != null) {
                b2.deviceStartScan();
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void b(boolean z2) {
        this.j = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    public final HashMap<String, String> d(String str) {
        ee7.b(str, "serial");
        HashMap<String, String> hashMap = new HashMap<>();
        SKUModel skuModelBySerialPrefix = this.u.getSkuModelBySerialPrefix(be5.o.b(str));
        if (skuModelBySerialPrefix != null) {
            String sku = skuModelBySerialPrefix.getSku();
            if (sku != null) {
                hashMap.put("Style_Number", sku);
                String deviceName = skuModelBySerialPrefix.getDeviceName();
                if (deviceName != null) {
                    hashMap.put("Device_Name", deviceName);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void e(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "syncDevice - serial=" + str);
        this.v.b(str).a(new km5(!be5.o.a(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new g(this, str));
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        Handler handler = this.h;
        if (handler != null) {
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            } else {
                ee7.a();
                throw null;
            }
        }
        A();
        this.t.n();
        qe.a(PortfolioApp.g0.c()).a(this.r);
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void h() {
        FLogger.INSTANCE.getLocal().d(z.a(), "cancelPairDevice()");
        PortfolioApp c2 = PortfolioApp.g0.c();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            ee7.a((Object) serial, "mPairingDevice!!.serial");
            c2.a(serial);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public ShineDevice i() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public boolean j() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void k() {
        this.s.B0();
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void l() {
        List<String> d2 = w97.d(ig5.a(PortfolioApp.g0.c(), 2131886839), ig5.a(PortfolioApp.g0.c(), 2131886840));
        this.p = true;
        this.s.o(d2);
        this.t.a(30000);
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void m() {
        this.s.b();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = z.a();
            local.d(a2, "pairDevice - serial=" + shineDevice.getSerial());
            String serial = shineDevice.getSerial();
            ee7.a((Object) serial, "it.serial");
            String macAddress = shineDevice.getMacAddress();
            ee7.a((Object) macAddress, "it.macAddress");
            wl5.h hVar = new wl5.h(serial, macAddress);
            if5 b2 = qd5.f.b("setup_device_session");
            this.m = b2;
            qd5.f.a("setup_device_session", b2);
            PortfolioApp c2 = PortfolioApp.g0.c();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            c2.a(communicateMode, "", communicateMode, hVar.a());
            this.t.a(hVar, this.q);
            if5 if5 = this.m;
            if (if5 != null) {
                if5.d();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void n() {
        this.t.m();
        ur6 ur6 = this.s;
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            ee7.a((Object) serial, "mPairingDevice!!.serial");
            ur6.a(serial, this.j);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void b(ShineDevice shineDevice) {
        ee7.b(shineDevice, "pairingDevice");
        this.e = shineDevice;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void c(ShineDevice shineDevice) {
        ee7.b(shineDevice, "device");
        xg5 xg5 = xg5.b;
        ur6 ur6 = this.s;
        if (ur6 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
        } else if (xg5.a(xg5, ((zq6) ur6).getContext(), xg5.a.PAIR_DEVICE, false, false, false, (Integer) null, 60, (Object) null)) {
            this.s.b();
            this.o = false;
            A();
            this.e = shineDevice;
            String c2 = PortfolioApp.g0.c().c();
            if (!mh7.a((CharSequence) c2)) {
                e(c2);
            } else {
                m();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void b(String str) {
        ee7.b(str, "serial");
        this.s.b();
        PortfolioApp.g0.c().a(str, false);
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void a(boolean z2) {
        this.o = z2;
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void a(int i2) {
        this.s.v();
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void a(ShineDevice shineDevice) {
        ee7.b(shineDevice, "device");
        this.s.J(u());
    }

    @DexIgnore
    public final void d(ShineDevice shineDevice) {
        ee7.b(shineDevice, "shineDevice");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = z.a();
        local.d(a2, "addDevice serial=" + shineDevice.getSerial());
        String serial = shineDevice.getSerial();
        ee7.a((Object) serial, "shineDevice.serial");
        int c2 = c(serial);
        if (c2 == Integer.MIN_VALUE) {
            c2 = shineDevice.getRssi();
        }
        shineDevice.updateRssi(c2);
        ik7 unused = xh7.b(e(), null, null, new c(this, shineDevice, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.rr6
    public void a(String str) {
        ee7.b(str, "serial");
        this.s.b();
        PortfolioApp.g0.c().a(str, true);
    }

    @DexIgnore
    public final void e(ShineDevice shineDevice) {
        T t2;
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (ee7.a((Object) ((ShineDevice) t2.getFirst()).getMacAddress(), (Object) shineDevice.getMacAddress())) {
                break;
            }
        }
        if (t2 == null) {
            ArrayList<SKUModel> arrayList = this.l;
            ArrayList<SKUModel> arrayList2 = new ArrayList();
            for (T t3 : arrayList) {
                if (ee7.a((Object) t3.getGroupName(), (Object) "WearOS")) {
                    arrayList2.add(t3);
                }
            }
            if (!arrayList2.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = z.a();
                local.d(a2, "wearOS devices in DB=" + arrayList2);
                for (SKUModel sKUModel : arrayList2) {
                    String name = shineDevice.getName();
                    ee7.a((Object) name, "shineDevice.name");
                    if (nh7.a((CharSequence) name, (CharSequence) String.valueOf(sKUModel.getDeviceName()), true)) {
                        FLogger.INSTANCE.getLocal().d(z.a(), "Detected wearOS device is acceptable");
                        this.f.add(new r87<>(shineDevice, shineDevice.getName()));
                        this.s.l(a(this.f));
                        return;
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a(String str, int i2) {
        ee7.b(str, "serial");
        int i3 = (i2 == 0 || i2 == -999999) ? 0 : i2;
        if (this.g.containsKey(str)) {
            List<Integer> list = this.g.get(str);
            if (list != null && !list.contains(0)) {
                if (list.size() < 5) {
                    list.add(Integer.valueOf(i2));
                    return;
                }
                list.remove(0);
                list.add(Integer.valueOf(i2));
                return;
            }
            return;
        }
        this.g.put(str, w97.d(Integer.valueOf(i3)));
    }

    @DexIgnore
    public void d(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    public final int c(String str) {
        ee7.b(str, "serial");
        if (!this.g.containsKey(str)) {
            return RecyclerView.UNDEFINED_DURATION;
        }
        double d2 = 0.0d;
        List<Integer> list = this.g.get(str);
        if (list == null) {
            return RecyclerView.UNDEFINED_DURATION;
        }
        for (Integer num : list) {
            d2 += (double) num.intValue();
        }
        int size = list.size();
        if (size <= 0) {
            size = 1;
        }
        return (int) (d2 / ((double) size));
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        ee7.b(str, Constants.EVENT);
        ee7.b(map, "values");
        qd5.f.c().a(str, map);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        ee7.b(map, "properties");
        qd5.f.c().a(map);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0043, code lost:
        if ((r2.length() == 0) != false) goto L_0x0045;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.r87<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> a(java.util.List<com.fossil.r87<com.misfit.frameworks.buttonservice.model.ShineDevice, java.lang.String>> r8) {
        /*
            r7 = this;
            java.lang.String r0 = "devices"
            com.fossil.ee7.b(r8, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r8 = r8.iterator()
        L_0x000e:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x004c
            java.lang.Object r1 = r8.next()
            r2 = r1
            com.fossil.r87 r2 = (com.fossil.r87) r2
            java.lang.Object r3 = r2.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r3 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r3
            int r3 = r3.getRssi()
            r4 = -150(0xffffffffffffff6a, float:NaN)
            r5 = 0
            r6 = 1
            if (r3 > r4) goto L_0x0045
            java.lang.Object r2 = r2.getFirst()
            com.misfit.frameworks.buttonservice.model.ShineDevice r2 = (com.misfit.frameworks.buttonservice.model.ShineDevice) r2
            java.lang.String r2 = r2.getSerial()
            java.lang.String r3 = "it.first.serial"
            com.fossil.ee7.a(r2, r3)
            int r2 = r2.length()
            if (r2 != 0) goto L_0x0042
            r2 = 1
            goto L_0x0043
        L_0x0042:
            r2 = 0
        L_0x0043:
            if (r2 == 0) goto L_0x0046
        L_0x0045:
            r5 = 1
        L_0x0046:
            if (r5 == 0) goto L_0x000e
            r0.add(r1)
            goto L_0x000e
        L_0x004c:
            java.util.List r8 = com.fossil.ea7.d(r0)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xr6.a(java.util.List):java.util.List");
    }
}
