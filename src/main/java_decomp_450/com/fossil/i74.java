package com.fossil;

public class i74 implements j74 {
    public final k74 a;
    public final l74 b;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /*
        static {
            /*
                com.fossil.g74$a[] r0 = com.fossil.g74.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.i74.a.a = r0
                com.fossil.g74$a r1 = com.fossil.g74.a.JAVA     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.i74.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.g74$a r1 = com.fossil.g74.a.NATIVE     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.i74.a.<clinit>():void");
        }
        */
    }

    public i74(k74 k74, l74 l74) {
        this.a = k74;
        this.b = l74;
    }

    @Override // com.fossil.j74
    public boolean a(e74 e74, boolean z) {
        int i = a.a[e74.c.getType().ordinal()];
        if (i == 1) {
            this.a.a(e74, z);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.b.a(e74, z);
            return true;
        }
    }
}
