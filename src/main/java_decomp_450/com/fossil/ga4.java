package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.facebook.internal.Utility;
import com.fossil.l94;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ga4 {
    @DexIgnore
    public /* final */ l14 a;
    @DexIgnore
    public /* final */ ta4 b;
    @DexIgnore
    public /* final */ db4 c;
    @DexIgnore
    public /* final */ vd4 d;
    @DexIgnore
    public /* final */ l94 e;
    @DexIgnore
    public /* final */ wb4 f;

    @DexIgnore
    public ga4(l14 l14, ta4 ta4, vd4 vd4, l94 l94, wb4 wb4) {
        this(l14, ta4, new db4(l14.b(), ta4), vd4, l94, wb4);
    }

    @DexIgnore
    public no3<String> a(String str, String str2, String str3) {
        return a(b(str, str2, str3, new Bundle()));
    }

    @DexIgnore
    public no3<?> b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return a(b(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }

    @DexIgnore
    public no3<?> c(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        return a(b(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }

    @DexIgnore
    public static String a(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    @DexIgnore
    public ga4(l14 l14, ta4 ta4, db4 db4, vd4 vd4, l94 l94, wb4 wb4) {
        this.a = l14;
        this.b = ta4;
        this.c = db4;
        this.d = vd4;
        this.e = l94;
        this.f = wb4;
    }

    @DexIgnore
    public final String a() {
        try {
            return a(MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1).digest(this.a.c().getBytes()));
        } catch (NoSuchAlgorithmException unused) {
            return "[HASH-ERROR]";
        }
    }

    @DexIgnore
    public final Bundle a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString(RemoteFLogger.MESSAGE_SENDER_KEY, str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.a.d().b());
        bundle.putString("gmsv", Integer.toString(this.b.c()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.a());
        bundle.putString("app_ver_name", this.b.b());
        bundle.putString("firebase-app-name-hash", a());
        try {
            String a2 = ((ac4) qo3.a((no3) this.f.a(false))).a();
            if (!TextUtils.isEmpty(a2)) {
                bundle.putString("Goog-Firebase-Installations-Auth", a2);
            } else {
                Log.w("FirebaseInstanceId", "FIS auth token is empty");
            }
        } catch (InterruptedException | ExecutionException e2) {
            Log.e("FirebaseInstanceId", "Failed to get FIS auth token", e2);
        }
        bundle.putString("cliv", "20.2.3".length() != 0 ? "fiid-".concat("20.2.3") : new String("fiid-"));
        l94.a a3 = this.e.a("fire-iid");
        if (a3 != l94.a.NONE) {
            bundle.putString("Firebase-Client-Log-Type", Integer.toString(a3.getCode()));
            bundle.putString("Firebase-Client", this.d.a());
        }
        return bundle;
    }

    @DexIgnore
    public final no3<Bundle> b(String str, String str2, String str3, Bundle bundle) {
        a(str, str2, str3, bundle);
        return this.c.a(bundle);
    }

    @DexIgnore
    public final /* synthetic */ String b(no3 no3) throws Exception {
        return a((Bundle) no3.a(IOException.class));
    }

    @DexIgnore
    public final String a(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null) {
                return string;
            }
            String string2 = bundle.getString("unregistered");
            if (string2 != null) {
                return string2;
            }
            String string3 = bundle.getString("error");
            if ("RST".equals(string3)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string3 != null) {
                throw new IOException(string3);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    @DexIgnore
    public final no3<String> a(no3<Bundle> no3) {
        return no3.a(u94.a(), new fa4(this));
    }

    @DexIgnore
    public static boolean a(String str) {
        return "SERVICE_NOT_AVAILABLE".equals(str) || "INTERNAL_SERVER_ERROR".equals(str) || "InternalServerError".equals(str);
    }
}
