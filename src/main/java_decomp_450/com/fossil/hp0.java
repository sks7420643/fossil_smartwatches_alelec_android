package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp0 {
    @DexIgnore
    public /* synthetic */ hp0(zd7 zd7) {
    }

    @DexIgnore
    public final dr0 a(byte b) {
        dr0 dr0;
        dr0[] values = dr0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                dr0 = null;
                break;
            }
            dr0 = values[i];
            if (dr0.b == b) {
                break;
            }
            i++;
        }
        return dr0 != null ? dr0 : dr0.e;
    }
}
