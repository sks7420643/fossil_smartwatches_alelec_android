package com.fossil;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ou3 {
    @DexIgnore
    public static TypedValue a(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            return typedValue;
        }
        return null;
    }

    @DexIgnore
    public static int a(Context context, int i, String str) {
        TypedValue a = a(context, i);
        if (a != null) {
            return a.data;
        }
        throw new IllegalArgumentException(String.format("%1$s requires a value for the %2$s attribute to be set in your app theme. You can either set the attribute in your theme or update your theme to inherit from Theme.MaterialComponents (or a descendant).", str, context.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static int a(View view, int i) {
        return a(view.getContext(), i, view.getClass().getCanonicalName());
    }

    @DexIgnore
    public static boolean a(Context context, int i, boolean z) {
        TypedValue a = a(context, i);
        if (a == null || a.type != 18) {
            return z;
        }
        return a.data != 0;
    }
}
