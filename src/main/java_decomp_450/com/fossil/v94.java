package com.fossil;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.ha4;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v94 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<v94> CREATOR; // = new a();
    @DexIgnore
    public Messenger a;
    @DexIgnore
    public ha4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Parcelable.Creator<v94> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public v94 createFromParcel(Parcel parcel) {
            IBinder readStrongBinder = parcel.readStrongBinder();
            if (readStrongBinder != null) {
                return new v94(readStrongBinder);
            }
            return null;
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public v94[] newArray(int i) {
            return new v94[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ClassLoader {
        @DexIgnore
        @Override // java.lang.ClassLoader
        public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (!FirebaseInstanceId.q()) {
                return v94.class;
            }
            Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            return v94.class;
        }
    }

    @DexIgnore
    public v94(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a = new Messenger(iBinder);
        } else {
            this.b = new ha4.a(iBinder);
        }
    }

    @DexIgnore
    public void a(Message message) throws RemoteException {
        Messenger messenger = this.a;
        if (messenger != null) {
            messenger.send(message);
        } else {
            this.b.a(message);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a().equals(((v94) obj).a());
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Messenger messenger = this.a;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.b.asBinder());
        }
    }

    @DexIgnore
    public IBinder a() {
        Messenger messenger = this.a;
        return messenger != null ? messenger.getBinder() : this.b.asBinder();
    }
}
