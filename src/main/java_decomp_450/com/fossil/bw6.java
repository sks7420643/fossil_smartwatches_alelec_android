package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw6 implements Factory<aw6> {
    @DexIgnore
    public /* final */ Provider<ch5> a;

    @DexIgnore
    public bw6(Provider<ch5> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static bw6 a(Provider<ch5> provider) {
        return new bw6(provider);
    }

    @DexIgnore
    public static aw6 a(ch5 ch5) {
        return new aw6(ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public aw6 get() {
        return a(this.a.get());
    }
}
