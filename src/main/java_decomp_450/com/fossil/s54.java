package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s54 extends v54.d.AbstractC0206d.AbstractC0217d {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.AbstractC0217d
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof v54.d.AbstractC0206d.AbstractC0217d) {
            return this.a.equals(((v54.d.AbstractC0206d.AbstractC0217d) obj).a());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Log{content=" + this.a + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.AbstractC0217d.a {
        @DexIgnore
        public String a;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.AbstractC0217d.a
        public v54.d.AbstractC0206d.AbstractC0217d.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null content");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.AbstractC0217d.a
        public v54.d.AbstractC0206d.AbstractC0217d a() {
            String str = "";
            if (this.a == null) {
                str = str + " content";
            }
            if (str.isEmpty()) {
                return new s54(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public s54(String str) {
        this.a = str;
    }
}
