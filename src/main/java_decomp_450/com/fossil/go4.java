package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface go4 {
    @DexIgnore
    long a(fo4 fo4);

    @DexIgnore
    void a();

    @DexIgnore
    LiveData<fo4> b();

    @DexIgnore
    fo4 c();
}
