package com.fossil;

import com.google.gson.Gson;
import com.portfolio.platform.data.model.ServerError;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jj5<T> implements ru7<T> {
    @DexIgnore
    public abstract void a(Call<T> call, fv7<T> fv7);

    @DexIgnore
    public abstract void a(Call<T> call, ServerError serverError);

    @DexIgnore
    public abstract void a(Call<T> call, Throwable th);

    @DexIgnore
    @Override // com.fossil.ru7
    public void onFailure(Call<T> call, Throwable th) {
        ee7.b(call, "call");
        ee7.b(th, "t");
        a(call, th);
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onResponse(Call<T> call, fv7<T> fv7) {
        String str;
        String str2;
        String str3;
        ee7.b(call, "call");
        ee7.b(fv7, "response");
        if (fv7.d()) {
            a(call, fv7);
            return;
        }
        int b = fv7.b();
        if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            mo7 c = fv7.c();
            if (c == null || (str = c.string()) == null) {
                str = fv7.e();
            }
            serverError.setMessage(str);
            a(call, serverError);
            return;
        }
        try {
            Gson gson = new Gson();
            mo7 c2 = fv7.c();
            if (c2 == null || (str3 = c2.string()) == null) {
                str3 = fv7.e();
            }
            ServerError serverError2 = (ServerError) gson.a(str3, (Class) ServerError.class);
            ee7.a((Object) serverError2, "serverError");
            a(call, serverError2);
        } catch (Exception unused) {
            ServerError serverError3 = new ServerError();
            serverError3.setCode(Integer.valueOf(b));
            mo7 c3 = fv7.c();
            if (c3 == null || (str2 = c3.string()) == null) {
                str2 = fv7.e();
            }
            serverError3.setMessage(str2);
            a(call, serverError3);
        }
    }
}
