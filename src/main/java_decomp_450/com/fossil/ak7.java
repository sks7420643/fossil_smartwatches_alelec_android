package com.fossil;

import com.fossil.ib7;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ak7 extends ti7 implements Closeable {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends db7<ti7, ak7> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ak7$a$a")
        /* renamed from: com.fossil.ak7$a$a  reason: collision with other inner class name */
        public static final class C0012a extends fe7 implements gd7<ib7.b, ak7> {
            @DexIgnore
            public static /* final */ C0012a INSTANCE; // = new C0012a();

            @DexIgnore
            public C0012a() {
                super(1);
            }

            @DexIgnore
            public final ak7 invoke(ib7.b bVar) {
                if (!(bVar instanceof ak7)) {
                    bVar = null;
                }
                return (ak7) bVar;
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public a() {
            super(ti7.a, C0012a.INSTANCE);
        }
    }

    /*
    static {
        new a(null);
    }
    */
}
