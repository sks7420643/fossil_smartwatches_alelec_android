package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String b;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String c;
    @DexIgnore
    @te4("socialId")
    public String d;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<eo4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public eo4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new eo4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public eo4[] newArray(int i) {
            return new eo4[i];
        }
    }

    @DexIgnore
    public eo4(String str, String str2, String str3, String str4, String str5) {
        ee7.b(str, "id");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof eo4)) {
            return false;
        }
        eo4 eo4 = (eo4) obj;
        return ee7.a(this.a, eo4.a) && ee7.a(this.b, eo4.b) && ee7.a(this.c, eo4.c) && ee7.a(this.d, eo4.d) && ee7.a(this.e, eo4.e);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "Owner(id=" + this.a + ", firstName=" + this.b + ", lastName=" + this.c + ", socialId=" + this.d + ", url=" + this.e + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public eo4(android.os.Parcel r8) {
        /*
            r7 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r8, r0)
            java.lang.String r2 = r8.readString()
            if (r2 == 0) goto L_0x0025
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.ee7.a(r2, r0)
            java.lang.String r3 = r8.readString()
            java.lang.String r4 = r8.readString()
            java.lang.String r5 = r8.readString()
            java.lang.String r6 = r8.readString()
            r1 = r7
            r1.<init>(r2, r3, r4, r5, r6)
            return
        L_0x0025:
            com.fossil.ee7.a()
            r8 = 0
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.eo4.<init>(android.os.Parcel):void");
    }
}
