package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm5 extends fl4<b, fl4.d, fl4.a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ GoalTrackingRepository d;
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            ee7.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData", f = "FetchGoalTrackingData.kt", l = {26, 44}, m = "run")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zm5 zm5, fb7 fb7) {
            super(fb7);
            this.this$0 = zm5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = zm5.class.getSimpleName();
        ee7.a((Object) simpleName, "FetchGoalTrackingData::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public zm5(GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(userRepository, "mUserRepository");
        this.d = goalTrackingRepository;
        this.e = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.zm5.b r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.fossil.zm5.c
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.zm5$c r0 = (com.fossil.zm5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.zm5$c r0 = new com.fossil.zm5$c
            r0.<init>(r13, r15)
        L_0x0018:
            r6 = r0
            java.lang.Object r15 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L_0x0062
            if (r1 == r3) goto L_0x004e
            if (r1 != r2) goto L_0x0046
            java.lang.Object r14 = r6.L$5
            java.util.Date r14 = (java.util.Date) r14
            java.lang.Object r14 = r6.L$4
            java.util.Date r14 = (java.util.Date) r14
            java.lang.Object r14 = r6.L$3
            com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
            java.lang.Object r14 = r6.L$2
            java.util.Date r14 = (java.util.Date) r14
            java.lang.Object r14 = r6.L$1
            com.fossil.zm5$b r14 = (com.fossil.zm5.b) r14
            java.lang.Object r14 = r6.L$0
            com.fossil.zm5 r14 = (com.fossil.zm5) r14
            com.fossil.t87.a(r15)
            goto L_0x012e
        L_0x0046:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x004e:
            java.lang.Object r14 = r6.L$2
            java.util.Date r14 = (java.util.Date) r14
            java.lang.Object r1 = r6.L$1
            com.fossil.zm5$b r1 = (com.fossil.zm5.b) r1
            java.lang.Object r3 = r6.L$0
            com.fossil.zm5 r3 = (com.fossil.zm5) r3
            com.fossil.t87.a(r15)
            r12 = r3
            r3 = r14
            r14 = r1
            r1 = r12
            goto L_0x00a2
        L_0x0062:
            com.fossil.t87.a(r15)
            if (r14 != 0) goto L_0x006a
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x006a:
            java.util.Date r15 = r14.a()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.fossil.zm5.f
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "executeUseCase - date="
            r5.append(r7)
            java.lang.String r7 = com.fossil.qy6.a(r15)
            r5.append(r7)
            java.lang.String r5 = r5.toString()
            r1.d(r4, r5)
            com.portfolio.platform.data.source.UserRepository r1 = r13.e
            r6.L$0 = r13
            r6.L$1 = r14
            r6.L$2 = r15
            r6.label = r3
            java.lang.Object r1 = r1.getCurrentUser(r6)
            if (r1 != r0) goto L_0x009f
            return r0
        L_0x009f:
            r3 = r15
            r15 = r1
            r1 = r13
        L_0x00a2:
            com.portfolio.platform.data.model.MFUser r15 = (com.portfolio.platform.data.model.MFUser) r15
            if (r15 == 0) goto L_0x0139
            java.lang.String r4 = r15.getCreatedAt()
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 == 0) goto L_0x00b2
            goto L_0x0139
        L_0x00b2:
            java.lang.String r4 = r15.getCreatedAt()
            if (r4 == 0) goto L_0x0134
            java.util.Date r4 = com.fossil.zd5.d(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r7 = com.fossil.zm5.f
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "executeUseCase - createdDate="
            r8.append(r9)
            java.lang.String r9 = "createdDate"
            com.fossil.ee7.a(r4, r9)
            java.lang.String r9 = com.fossil.qy6.a(r4)
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r5.d(r7, r8)
            boolean r5 = com.fossil.zd5.b(r4, r3)
            if (r5 != 0) goto L_0x0131
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            boolean r5 = com.fossil.zd5.b(r3, r5)
            if (r5 == 0) goto L_0x00f3
            goto L_0x0131
        L_0x00f3:
            java.util.Calendar r5 = com.fossil.zd5.s(r3)
            java.lang.String r7 = "DateHelper.getStartOfWeek(date)"
            com.fossil.ee7.a(r5, r7)
            java.util.Date r5 = r5.getTime()
            boolean r7 = com.fossil.zd5.c(r4, r5)
            if (r7 == 0) goto L_0x0107
            r5 = r4
        L_0x0107:
            com.portfolio.platform.data.source.GoalTrackingRepository r7 = r1.d
            java.lang.String r8 = "startDate"
            com.fossil.ee7.a(r5, r8)
            r8 = 0
            r9 = 0
            r10 = 12
            r11 = 0
            r6.L$0 = r1
            r6.L$1 = r14
            r6.L$2 = r3
            r6.L$3 = r15
            r6.L$4 = r4
            r6.L$5 = r5
            r6.label = r2
            r1 = r7
            r2 = r5
            r4 = r8
            r5 = r9
            r7 = r10
            r8 = r11
            java.lang.Object r14 = com.portfolio.platform.data.source.GoalTrackingRepository.loadGoalTrackingDataList$default(r1, r2, r3, r4, r5, r6, r7, r8)
            if (r14 != r0) goto L_0x012e
            return r0
        L_0x012e:
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x0131:
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x0134:
            com.fossil.ee7.a()
            r14 = 0
            throw r14
        L_0x0139:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = com.fossil.zm5.f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "executeUseCase - FAILED!!! with user="
            r1.append(r2)
            r1.append(r15)
            java.lang.String r15 = r1.toString()
            r14.d(r0, r15)
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm5.a(com.fossil.zm5$b, com.fossil.fb7):java.lang.Object");
    }
}
