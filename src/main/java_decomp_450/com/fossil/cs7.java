package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cs7 {
    @DexIgnore
    public int a; // = 74;
    @DexIgnore
    public int b; // = 1;
    @DexIgnore
    public int c; // = 3;
    @DexIgnore
    public String d; // = System.getProperty("line.separator");
    @DexIgnore
    public String e; // = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
    @DexIgnore
    public String f; // = "--";
    @DexIgnore
    public Comparator g; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // java.util.Comparator
        public int compare(Object obj, Object obj2) {
            return ((fs7) obj).getKey().compareToIgnoreCase(((fs7) obj2).getKey());
        }
    }

    @DexIgnore
    public int a() {
        return this.c;
    }

    @DexIgnore
    public int b() {
        return this.b;
    }

    @DexIgnore
    public Comparator c() {
        return this.g;
    }

    @DexIgnore
    public int d() {
        return this.a;
    }

    @DexIgnore
    public void a(PrintWriter printWriter, int i, is7 is7, int i2, int i3) {
        StringBuffer stringBuffer = new StringBuffer();
        a(stringBuffer, i, is7, i2, i3);
        printWriter.println(stringBuffer.toString());
    }

    @DexIgnore
    public StringBuffer a(StringBuffer stringBuffer, int i, is7 is7, int i2, int i3) {
        String a2 = a(i2);
        String a3 = a(i3);
        ArrayList arrayList = new ArrayList();
        List<fs7> helpOptions = is7.helpOptions();
        Collections.sort(helpOptions, c());
        int i4 = 0;
        int i5 = 0;
        for (fs7 fs7 : helpOptions) {
            StringBuffer stringBuffer2 = new StringBuffer(8);
            if (fs7.getOpt() == null) {
                stringBuffer2.append(a2);
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("   ");
                stringBuffer3.append(this.f);
                stringBuffer2.append(stringBuffer3.toString());
                stringBuffer2.append(fs7.getLongOpt());
            } else {
                stringBuffer2.append(a2);
                stringBuffer2.append(this.e);
                stringBuffer2.append(fs7.getOpt());
                if (fs7.hasLongOpt()) {
                    stringBuffer2.append(',');
                    stringBuffer2.append(this.f);
                    stringBuffer2.append(fs7.getLongOpt());
                }
            }
            if (fs7.hasArg()) {
                if (fs7.hasArgName()) {
                    stringBuffer2.append(" <");
                    stringBuffer2.append(fs7.getArgName());
                    stringBuffer2.append(SimpleComparison.GREATER_THAN_OPERATION);
                } else {
                    stringBuffer2.append(' ');
                }
            }
            arrayList.add(stringBuffer2);
            if (stringBuffer2.length() > i5) {
                i5 = stringBuffer2.length();
            }
        }
        Iterator it = helpOptions.iterator();
        while (it.hasNext()) {
            fs7 fs72 = (fs7) it.next();
            int i6 = i4 + 1;
            StringBuffer stringBuffer4 = new StringBuffer(arrayList.get(i4).toString());
            if (stringBuffer4.length() < i5) {
                stringBuffer4.append(a(i5 - stringBuffer4.length()));
            }
            stringBuffer4.append(a3);
            int i7 = i5 + i3;
            if (fs72.getDescription() != null) {
                stringBuffer4.append(fs72.getDescription());
            }
            a(stringBuffer, i, i7, stringBuffer4.toString());
            if (it.hasNext()) {
                stringBuffer.append(this.d);
            }
            i4 = i6;
        }
        return stringBuffer;
    }

    @DexIgnore
    public StringBuffer a(StringBuffer stringBuffer, int i, int i2, String str) {
        int a2 = a(str, i, 0);
        if (a2 == -1) {
            stringBuffer.append(a(str));
            return stringBuffer;
        }
        stringBuffer.append(a(str.substring(0, a2)));
        stringBuffer.append(this.d);
        if (i2 >= i) {
            i2 = 1;
        }
        String a3 = a(i2);
        while (true) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(a3);
            stringBuffer2.append(str.substring(a2).trim());
            str = stringBuffer2.toString();
            a2 = a(str, i, 0);
            if (a2 == -1) {
                stringBuffer.append(str);
                return stringBuffer;
            }
            if (str.length() > i && a2 == i2 - 1) {
                a2 = i;
            }
            stringBuffer.append(a(str.substring(0, a2)));
            stringBuffer.append(this.d);
        }
    }

    @DexIgnore
    public int a(String str, int i, int i2) {
        int indexOf = str.indexOf(10, i2);
        if ((indexOf != -1 && indexOf <= i) || ((indexOf = str.indexOf(9, i2)) != -1 && indexOf <= i)) {
            return indexOf + 1;
        }
        int i3 = i + i2;
        if (i3 >= str.length()) {
            return -1;
        }
        int i4 = i3;
        while (i4 >= i2 && (r5 = str.charAt(i4)) != ' ' && r5 != '\n' && r5 != '\r') {
            i4--;
        }
        if (i4 > i2) {
            return i4;
        }
        while (i3 <= str.length() && (r9 = str.charAt(i3)) != ' ' && r9 != '\n' && r9 != '\r') {
            i3++;
        }
        if (i3 == str.length()) {
            return -1;
        }
        return i3;
    }

    @DexIgnore
    public String a(int i) {
        StringBuffer stringBuffer = new StringBuffer(i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(' ');
        }
        return stringBuffer.toString();
    }

    @DexIgnore
    public String a(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        int length = str.length();
        while (length > 0 && Character.isWhitespace(str.charAt(length - 1))) {
            length--;
        }
        return str.substring(0, length);
    }
}
