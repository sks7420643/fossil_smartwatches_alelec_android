package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m34 {
    @DexIgnore
    public static /* final */ Pattern e; // = Pattern.compile("http(s?)://[^\\/]+", 2);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ m64 b;
    @DexIgnore
    public /* final */ k64 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public m34(String str, String str2, m64 m64, k64 k64) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (m64 != null) {
            this.d = str;
            this.a = a(str2);
            this.b = m64;
            this.c = k64;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    @DexIgnore
    public l64 a() {
        return a(Collections.emptyMap());
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public l64 a(Map<String, String> map) {
        l64 a2 = this.b.a(this.c, b(), map);
        a2.a("User-Agent", "Crashlytics Android SDK/" + y34.e());
        a2.a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return a2;
    }

    @DexIgnore
    public final String a(String str) {
        return !t34.b(this.d) ? e.matcher(str).replaceFirst(this.d) : str;
    }
}
