package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.be5;
import com.fossil.cy6;
import com.fossil.pu6;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lu6 extends ho5 implements fv6, cy6.g {
    @DexIgnore
    public static /* final */ a v; // = new a(null);
    @DexIgnore
    public ev6 g;
    @DexIgnore
    public qw6<e15> h;
    @DexIgnore
    public n63 i;
    @DexIgnore
    public View j;
    @DexIgnore
    public Bitmap p;
    @DexIgnore
    public iw q;
    @DexIgnore
    public /* final */ String r; // = eh5.l.a().b("primaryText");
    @DexIgnore
    public /* final */ String s; // = eh5.l.a().b("success");
    @DexIgnore
    public /* final */ String t; // = eh5.l.a().b("secondaryText");
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final lu6 a() {
            return new lu6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ lu6 a;

        @DexIgnore
        public b(lu6 lu6) {
            this.a = lu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.requireActivity().finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements p63 {
        @DexIgnore
        public /* final */ /* synthetic */ lu6 a;

        @DexIgnore
        public c(lu6 lu6) {
            this.a = lu6;
        }

        @DexIgnore
        @Override // com.fossil.p63
        public final void onMapReady(n63 n63) {
            this.a.i = n63;
            ee7.a((Object) n63, "googleMap");
            u63 f = n63.f();
            ee7.a((Object) f, "googleMap.uiSettings");
            f.b(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ lu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ e15 b;

        @DexIgnore
        public d(lu6 lu6, e15 e15) {
            this.a = lu6;
            this.b = e15;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            yx6.a(this.b.C);
            ConstraintLayout constraintLayout = this.b.r;
            ee7.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            lu6.a(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ e15 a;
        @DexIgnore
        public /* final */ /* synthetic */ lu6 b;

        @DexIgnore
        public e(e15 e15, lu6 lu6, pu6.d dVar) {
            this.a = e15;
            this.b = lu6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            lu6.b(this.b).a(str2).a(this.a.A);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ lu6 b;

        @DexIgnore
        public f(View view, String str, lu6 lu6, pu6.d dVar) {
            this.a = view;
            this.b = lu6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.a.findViewById(2131362587);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(je5.a(str2));
                    int dimensionPixelSize = PortfolioApp.g0.c().getResources().getDimensionPixelSize(2131165441);
                    this.a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.a.getMeasuredHeight());
                    this.b.p = je5.a(this.a);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ev6 a(lu6 lu6) {
        ev6 ev6 = lu6.g;
        if (ev6 != null) {
            return ev6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ iw b(lu6 lu6) {
        iw iwVar = lu6.q;
        if (iwVar != null) {
            return iwVar;
        }
        ee7.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void M() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        e15 f1 = f1();
        if (f1 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = f1.C;
            ee7.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = f1.r;
            ee7.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            ev6 ev6 = this.g;
            if (ev6 != null) {
                ev6.b(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void X0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        e15 f1 = f1();
        if (f1 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = f1.C;
            ee7.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setEnabled(false);
            FlexibleSwitchCompat flexibleSwitchCompat2 = f1.C;
            ee7.a((Object) flexibleSwitchCompat2, "binding.swLocate");
            flexibleSwitchCompat2.setChecked(false);
            String str = this.t;
            if (str != null) {
                f1.y.setTextColor(Color.parseColor(str));
            }
            ConstraintLayout constraintLayout = f1.r;
            ee7.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            FlexibleTextView flexibleTextView = f1.t;
            ee7.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(0);
            ev6 ev6 = this.g;
            if (ev6 != null) {
                ev6.b(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final e15 f1() {
        qw6<e15> qw6 = this.h;
        if (qw6 != null) {
            return qw6.a();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void g(int i2) {
        qw6<e15> qw6 = this.h;
        if (qw6 != null) {
            e15 a2 = qw6.a();
            if (a2 != null) {
                float b2 = be5.o.b(i2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i2 + ", distanceInFt: " + b2);
                if (b2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && b2 <= 5.0f) {
                    FlexibleTextView flexibleTextView = a2.E;
                    ee7.a((Object) flexibleTextView, "binding.tvLocationStatus");
                    flexibleTextView.setText(ig5.a(getContext(), 2131887101));
                    FlexibleTextView flexibleTextView2 = a2.E;
                    ee7.a((Object) flexibleTextView2, "binding.tvLocationStatus");
                    flexibleTextView2.setEnabled(true);
                    String str = this.s;
                    if (str != null) {
                        a2.E.setTextColor(Color.parseColor(str));
                        a2.E.setBackgroundColor(Color.parseColor(str));
                        FlexibleTextView flexibleTextView3 = a2.E;
                        ee7.a((Object) flexibleTextView3, "binding.tvLocationStatus");
                        Drawable background = flexibleTextView3.getBackground();
                        ee7.a((Object) background, "binding.tvLocationStatus.background");
                        background.setAlpha(128);
                    }
                } else if (b2 >= 5.0f && b2 <= 10.0f) {
                    FlexibleTextView flexibleTextView4 = a2.E;
                    ee7.a((Object) flexibleTextView4, "binding.tvLocationStatus");
                    flexibleTextView4.setText(ig5.a(getContext(), 2131887099));
                    FlexibleTextView flexibleTextView5 = a2.E;
                    ee7.a((Object) flexibleTextView5, "binding.tvLocationStatus");
                    flexibleTextView5.setEnabled(true);
                    String str2 = this.s;
                    if (str2 != null) {
                        a2.E.setTextColor(Color.parseColor(str2));
                        a2.E.setBackgroundColor(Color.parseColor(str2));
                        FlexibleTextView flexibleTextView6 = a2.E;
                        ee7.a((Object) flexibleTextView6, "binding.tvLocationStatus");
                        Drawable background2 = flexibleTextView6.getBackground();
                        ee7.a((Object) background2, "binding.tvLocationStatus.background");
                        background2.setAlpha(128);
                    }
                } else if (b2 < 10.0f || b2 > 30.0f) {
                    FlexibleTextView flexibleTextView7 = a2.E;
                    ee7.a((Object) flexibleTextView7, "binding.tvLocationStatus");
                    flexibleTextView7.setText(ig5.a(getContext(), 2131887098));
                    FlexibleTextView flexibleTextView8 = a2.E;
                    ee7.a((Object) flexibleTextView8, "binding.tvLocationStatus");
                    flexibleTextView8.setEnabled(false);
                    String str3 = this.t;
                    if (str3 != null) {
                        a2.E.setTextColor(Color.parseColor(str3));
                        a2.E.setBackgroundColor(Color.parseColor(str3));
                        FlexibleTextView flexibleTextView9 = a2.E;
                        ee7.a((Object) flexibleTextView9, "binding.tvLocationStatus");
                        Drawable background3 = flexibleTextView9.getBackground();
                        ee7.a((Object) background3, "binding.tvLocationStatus.background");
                        background3.setAlpha(128);
                    }
                } else {
                    FlexibleTextView flexibleTextView10 = a2.E;
                    ee7.a((Object) flexibleTextView10, "binding.tvLocationStatus");
                    flexibleTextView10.setText(ig5.a(getContext(), 2131887100));
                    FlexibleTextView flexibleTextView11 = a2.E;
                    ee7.a((Object) flexibleTextView11, "binding.tvLocationStatus");
                    flexibleTextView11.setEnabled(true);
                    String str4 = this.s;
                    if (str4 != null) {
                        a2.E.setTextColor(Color.parseColor(str4));
                        a2.E.setBackgroundColor(Color.parseColor(str4));
                        FlexibleTextView flexibleTextView12 = a2.E;
                        ee7.a((Object) flexibleTextView12, "binding.tvLocationStatus");
                        Drawable background4 = flexibleTextView12.getBackground();
                        ee7.a((Object) background4, "binding.tvLocationStatus.background");
                        background4.setAlpha(128);
                    }
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iw a2 = aw.a(this);
        ee7.a((Object) a2, "Glide.with(this)");
        this.q = a2;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String b2;
        ee7.b(layoutInflater, "inflater");
        e15 e15 = (e15) qb.a(layoutInflater, 2131558554, viewGroup, false, a1());
        View inflate = layoutInflater.inflate(2131558729, (ViewGroup) null);
        this.j = inflate;
        if (inflate != null) {
            int dimensionPixelSize = PortfolioApp.g0.c().getResources().getDimensionPixelSize(2131165441);
            inflate.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
            this.p = je5.a(inflate);
        }
        e15.z.setOnClickListener(new b(this));
        Fragment b3 = getChildFragmentManager().b(2131362241);
        if (b3 != null) {
            ((SupportMapFragment) b3).a(new c(this));
            e15.C.setOnCheckedChangeListener(new d(this, e15));
            ConstraintLayout constraintLayout = e15.q;
            if (!(constraintLayout == null || (b2 = eh5.l.a().b("nonBrandSurface")) == null)) {
                constraintLayout.setBackgroundColor(Color.parseColor(b2));
            }
            this.h = new qw6<>(this, e15);
            ee7.a((Object) e15, "binding");
            return e15.d();
        }
        throw new x87("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        super.onPause();
        ev6 ev6 = this.g;
        if (ev6 != null) {
            ev6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        super.onResume();
        ev6 ev6 = this.g;
        if (ev6 != null) {
            ev6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void v(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        qw6<e15> qw6 = this.h;
        if (qw6 != null) {
            e15 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void a(pu6.d dVar) {
        ee7.b(dVar, "watchSetting");
        if (isActive()) {
            qw6<e15> qw6 = this.h;
            if (qw6 != null) {
                e15 a2 = qw6.a();
                if (a2 != null) {
                    String deviceId = dVar.a().getDeviceId();
                    FlexibleTextView flexibleTextView = a2.D;
                    ee7.a((Object) flexibleTextView, "it.tvDeviceName");
                    flexibleTextView.setText(dVar.b());
                    a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    FlexibleTextView flexibleTextView2 = a2.t;
                    ee7.a((Object) flexibleTextView2, "it.ftvError");
                    we7 we7 = we7.a;
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131887104);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    String format = String.format(a3, Arrays.copyOf(new Object[]{dVar.b()}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView2.setText(format);
                    boolean d2 = dVar.d();
                    if (d2) {
                        if (dVar.e()) {
                            String str = this.s;
                            if (str != null) {
                                a2.D.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView3 = a2.D;
                            ee7.a((Object) flexibleTextView3, "it.tvDeviceName");
                            flexibleTextView3.setAlpha(1.0f);
                            a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, v6.c(PortfolioApp.g0.c(), 2131231071), (Drawable) null);
                        } else {
                            String str2 = this.r;
                            if (str2 != null) {
                                a2.D.setTextColor(Color.parseColor(str2));
                            }
                            FlexibleTextView flexibleTextView4 = a2.D;
                            ee7.a((Object) flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView5 = a2.E;
                            ee7.a((Object) flexibleTextView5, "it.tvLocationStatus");
                            flexibleTextView5.setText(ig5.a(getContext(), 2131887098));
                            FlexibleTextView flexibleTextView6 = a2.E;
                            ee7.a((Object) flexibleTextView6, "it.tvLocationStatus");
                            flexibleTextView6.setEnabled(false);
                            String str3 = this.t;
                            if (str3 != null) {
                                a2.E.setTextColor(Color.parseColor(str3));
                                a2.E.setBackgroundColor(Color.parseColor(str3));
                                FlexibleTextView flexibleTextView7 = a2.E;
                                ee7.a((Object) flexibleTextView7, "it.tvLocationStatus");
                                Drawable background = flexibleTextView7.getBackground();
                                ee7.a((Object) background, "it.tvLocationStatus.background");
                                background.setAlpha(128);
                            }
                        }
                    } else if (!d2) {
                        String str4 = this.r;
                        if (str4 != null) {
                            a2.D.setTextColor(Color.parseColor(str4));
                        }
                        FlexibleTextView flexibleTextView8 = a2.D;
                        ee7.a((Object) flexibleTextView8, "it.tvDeviceName");
                        flexibleTextView8.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView9 = a2.E;
                        ee7.a((Object) flexibleTextView9, "it.tvLocationStatus");
                        flexibleTextView9.setText(ig5.a(getContext(), 2131887098));
                        FlexibleTextView flexibleTextView10 = a2.E;
                        ee7.a((Object) flexibleTextView10, "it.tvLocationStatus");
                        flexibleTextView10.setEnabled(false);
                        String str5 = this.t;
                        if (str5 != null) {
                            a2.E.setTextColor(Color.parseColor(str5));
                            a2.E.setBackgroundColor(Color.parseColor(str5));
                            FlexibleTextView flexibleTextView11 = a2.E;
                            ee7.a((Object) flexibleTextView11, "it.tvLocationStatus");
                            Drawable background2 = flexibleTextView11.getBackground();
                            ee7.a((Object) background2, "it.tvLocationStatus.background");
                            background2.setAlpha(128);
                        }
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(be5.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.A;
                    ee7.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, be5.o.b(deviceId, be5.b.SMALL)).setImageCallback(new e(a2, this, dVar)).download();
                    View view = this.j;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(be5.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new f(view, deviceId, this, dVar)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ev6 ev6) {
        ee7.b(ev6, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + ev6);
        this.g = ev6;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        e15 f1 = f1();
        if (f1 != null) {
            FlexibleTextView flexibleTextView = f1.t;
            ee7.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(8);
            if (d2 != null && d3 != null) {
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                n63 n63 = this.i;
                if (n63 != null) {
                    n63.b(m63.a(latLng, 13.0f));
                    n63.a(m63.b(18.0f));
                    n63.a();
                    Bitmap bitmap = this.p;
                    if (bitmap != null) {
                        k93 k93 = new k93();
                        k93.b(ig5.a(PortfolioApp.g0.c(), 2131887097));
                        k93.a(z83.a(bitmap));
                        k93.a(latLng);
                        n63.a(k93);
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void a(long j2) {
        FlexibleTextView flexibleTextView;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j2);
        qw6<e15> qw6 = this.h;
        if (qw6 != null) {
            e15 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.x) != null) {
                flexibleTextView.setText(DateUtils.getRelativeTimeSpanString(j2));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void a(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        qw6<e15> qw6 = this.h;
        if (qw6 != null) {
            e15 a2 = qw6.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.C;
                ee7.a((Object) flexibleSwitchCompat, "binding.swLocate");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fv6
    public void a(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
        e15 f1 = f1();
        if (f1 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = f1.C;
            ee7.a((Object) flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = f1.r;
            ee7.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            ev6 ev6 = this.g;
            if (ev6 != null) {
                ev6.b(false);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }
}
