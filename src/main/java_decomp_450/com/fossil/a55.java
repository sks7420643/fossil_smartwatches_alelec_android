package com.fossil;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TabItem A;
    @DexIgnore
    public /* final */ TabItem B;
    @DexIgnore
    public /* final */ TabItem C;
    @DexIgnore
    public /* final */ TabItem D;
    @DexIgnore
    public /* final */ TabItem E;
    @DexIgnore
    public /* final */ TabItem F;
    @DexIgnore
    public /* final */ TabItem G;
    @DexIgnore
    public /* final */ TabItem H;
    @DexIgnore
    public /* final */ TabLayout I;
    @DexIgnore
    public /* final */ TabLayout J;
    @DexIgnore
    public /* final */ TabLayout K;
    @DexIgnore
    public /* final */ TabLayout L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ ConstraintLayout N;
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ RelativeLayout y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    public a55(Object obj, View view, int i, RTLImageView rTLImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ConstraintLayout constraintLayout3, RelativeLayout relativeLayout, ConstraintLayout constraintLayout4, TabItem tabItem, TabItem tabItem2, TabItem tabItem3, TabItem tabItem4, TabItem tabItem5, TabItem tabItem6, TabItem tabItem7, TabItem tabItem8, TabLayout tabLayout, TabLayout tabLayout2, TabLayout tabLayout3, TabLayout tabLayout4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout5) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView3;
        this.w = flexibleTextView4;
        this.x = constraintLayout3;
        this.y = relativeLayout;
        this.z = constraintLayout4;
        this.A = tabItem;
        this.B = tabItem2;
        this.C = tabItem3;
        this.D = tabItem4;
        this.E = tabItem5;
        this.F = tabItem6;
        this.G = tabItem7;
        this.H = tabItem8;
        this.I = tabLayout;
        this.J = tabLayout2;
        this.K = tabLayout3;
        this.L = tabLayout4;
        this.M = flexibleTextView5;
        this.N = constraintLayout5;
    }
}
