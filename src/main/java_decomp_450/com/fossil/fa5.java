package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fa5 extends ea5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = null;
    @DexIgnore
    public long r;

    @DexIgnore
    public fa5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 1, s, t));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    public fa5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[0]);
        this.r = -1;
        ((ea5) this).q.setTag(null);
        a(view);
        f();
    }
}
