package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tv7 implements tu7<mo7, Long> {
    @DexIgnore
    public static /* final */ tv7 a; // = new tv7();

    @DexIgnore
    public Long a(mo7 mo7) throws IOException {
        return Long.valueOf(mo7.string());
    }
}
