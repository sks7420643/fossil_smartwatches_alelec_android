package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.f60;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri1 implements Parcelable {
    @DexIgnore
    public static /* final */ f21 CREATOR; // = new f21(null);
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public BluetoothGatt b;
    @DexIgnore
    public z31 c;
    @DexIgnore
    public /* final */ BluetoothGattCallback d;
    @DexIgnore
    public kv0 e;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<b51> f;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<y61> g;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<jb1> h;
    @DexIgnore
    public /* final */ HashMap<qk1, no1> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int p;
    @DexIgnore
    public /* final */ BroadcastReceiver q;
    @DexIgnore
    public /* final */ BroadcastReceiver r;
    @DexIgnore
    public /* final */ BroadcastReceiver s;
    @DexIgnore
    public dd1 t;
    @DexIgnore
    public /* final */ String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public /* final */ cx0 w;
    @DexIgnore
    public /* final */ BluetoothDevice x;

    @DexIgnore
    public /* synthetic */ ri1(BluetoothDevice bluetoothDevice, zd7 zd7) {
        this.x = bluetoothDevice;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.a = new Handler(myLooper);
            this.d = new ok1(this);
            this.e = new kv0(this);
            this.f = new CopyOnWriteArraySet<>();
            this.g = new CopyOnWriteArraySet<>();
            this.h = new CopyOnWriteArraySet<>();
            this.i = new HashMap<>();
            this.j = 20;
            this.p = 20;
            this.q = new mm1(this);
            this.r = new lo1(this);
            this.s = new kk0(this);
            this.t = dd1.DISCONNECTED;
            String address = this.x.getAddress();
            ee7.a((Object) address, "bluetoothDevice.address");
            this.u = address;
            this.w = new cx0();
            BroadcastReceiver broadcastReceiver = this.q;
            String[] strArr = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"};
            Context a2 = u31.g.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(strArr[i2]);
                }
                qe.a(a2).a(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = this.r;
            String[] strArr2 = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED"};
            Context a3 = u31.g.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(strArr2[i3]);
                }
                qe.a(a3).a(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = this.s;
            String[] strArr3 = {"com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED"};
            Context a4 = u31.g.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(strArr3[i4]);
                }
                qe.a(a4).a(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.v = z;
    }

    @DexIgnore
    public final void c() {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new g21(this, new x71(z51.BLUETOOTH_OFF, 0, 2), new UUID[0], new qk1[0]));
            return;
        }
        BluetoothGatt bluetoothGatt = this.b;
        if (bluetoothGatt == null) {
            this.a.post(new g21(this, new x71(z51.GATT_NULL, 0, 2), new UUID[0], new qk1[0]));
        } else if (true != bluetoothGatt.discoverServices()) {
            this.a.post(new g21(this, new x71(z51.START_FAIL, 0, 2), new UUID[0], new qk1[0]));
        }
    }

    @DexIgnore
    public final String d() {
        String name = this.x.getName();
        return name != null ? name : "";
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final p91 e() {
        return p91.f.a(zz0.d.c(this.x));
    }

    @DexIgnore
    public final boolean f() {
        if (!yp0.f.a()) {
            return false;
        }
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            return false;
        }
        try {
            le0 le02 = le0.DEBUG;
            Method method = this.x.getClass().getMethod("removeBond", new Class[0]);
            ee7.a((Object) method, "bluetoothDevice.javaClass.getMethod(\"removeBond\")");
            Object invoke = method.invoke(this.x, new Object[0]);
            if (invoke != null) {
                return ((Boolean) invoke).booleanValue();
            }
            throw new x87("null cannot be cast to non-null type kotlin.Boolean");
        } catch (Exception e2) {
            wl0.h.a(e2);
            return false;
        }
    }

    @DexIgnore
    public final void g() {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new q91(this, new x71(z51.BLUETOOTH_OFF, 0, 2), 0));
            return;
        }
        x71 x71 = new x71(z51.SUCCESS, 0, 2);
        BluetoothGatt bluetoothGatt = this.b;
        if (bluetoothGatt == null) {
            x71 = new x71(z51.GATT_NULL, 0, 2);
        } else if (true != bluetoothGatt.readRemoteRssi()) {
            x71 = new x71(z51.START_FAIL, 0, 2);
        }
        if (x71.a != z51.SUCCESS) {
            this.a.post(new q91(this, x71, 0));
        }
    }

    @DexIgnore
    public final m01 getBondState() {
        return m01.e.a(this.x.getBondState());
    }

    @DexIgnore
    public final void h() {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new kb1(this, new x71(z51.BLUETOOTH_OFF, 0, 2), getBondState(), getBondState()));
        } else if (m01.BOND_NONE == getBondState()) {
            this.a.post(new kb1(this, new x71(z51.SUCCESS, 0, 2), getBondState(), getBondState()));
        } else if (!f()) {
            this.a.post(new kb1(this, new x71(z51.START_FAIL, 0, 2), getBondState(), getBondState()));
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.x, i2);
        }
    }

    @DexIgnore
    public final dd1 b(int i2) {
        if (i2 == 0) {
            return dd1.DISCONNECTED;
        }
        if (i2 == 1) {
            return dd1.CONNECTING;
        }
        if (i2 == 2) {
            return dd1.CONNECTED;
        }
        if (i2 != 3) {
            return dd1.DISCONNECTED;
        }
        return dd1.DISCONNECTING;
    }

    @DexIgnore
    public final void a(z31 z31) {
        wr1 wr1 = new wr1("connection_state_changed", ci1.g, this.u, "", "", z31.a == 0, null, null, null, yz0.a(yz0.a(new JSONObject(), r51.u0, Integer.valueOf(z31.a)), r51.z0, yz0.a(b(z31.b))), 448);
        Long l = (Long) ea7.e(z31.c);
        if (l != null) {
            long longValue = l.longValue();
            wr1.a = longValue;
            yz0.a(wr1.m, r51.V, Double.valueOf(yz0.a(longValue)));
            JSONObject jSONObject = wr1.m;
            r51 r51 = r51.W;
            Long l2 = (Long) ea7.g(z31.c);
            if (l2 != null) {
                longValue = l2.longValue();
            }
            yz0.a(jSONObject, r51, Double.valueOf(yz0.a(longValue)));
            JSONArray jSONArray = new JSONArray();
            Iterator<T> it = z31.c.iterator();
            while (it.hasNext()) {
                jSONArray.put(yz0.a(it.next().longValue()));
            }
            yz0.a(wr1.m, r51.h5, jSONArray);
            wl0.h.a(wr1);
        }
    }

    @DexIgnore
    public final void b(qk1 qk1, byte[] bArr) {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new ye1(this, new x71(z51.BLUETOOTH_OFF, 0, 2), qk1, new byte[0]));
            return;
        }
        x71 x71 = new x71(z51.SUCCESS, 0, 2);
        if (this.b == null) {
            x71 = new x71(z51.GATT_NULL, 0, 2);
        } else {
            no1 no1 = this.i.get(qk1);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = no1 != null ? no1.a : null;
            if (bluetoothGattCharacteristic != null) {
                bluetoothGattCharacteristic.setValue(bArr);
            }
            if (bluetoothGattCharacteristic == null) {
                x71 = new x71(z51.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.b;
                if (bluetoothGatt == null || true != bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                    x71 = new x71(z51.START_FAIL, 0, 2);
                }
            }
        }
        le0 le02 = le0.DEBUG;
        int length = bArr.length;
        yz0.a(bArr, (String) null, 1);
        if (x71.a != z51.SUCCESS) {
            this.a.post(new ye1(this, x71, qk1, new byte[0]));
        }
    }

    @DexIgnore
    public final void b() {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new dx0(this, new x71(z51.BLUETOOTH_OFF, 0, 2), getBondState(), getBondState()));
        } else if (m01.BONDED == getBondState()) {
            this.a.post(new dx0(this, new x71(z51.SUCCESS, 0, 2), getBondState(), getBondState()));
        } else if (!this.x.createBond()) {
            this.a.post(new dx0(this, new x71(z51.START_FAIL, 0, 2), getBondState(), getBondState()));
        }
    }

    @DexIgnore
    public final void a(int i2, dd1 dd1) {
        Iterator<y61> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new lv0(it.next(), dd1, i2));
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(qk1 qk1, byte[] bArr) {
        le0 le0 = le0.DEBUG;
        qk1.name();
        yz0.a(bArr, (String) null, 1);
        Iterator<y61> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new hm0(it.next(), qk1, bArr));
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0016, code lost:
        if (r6 != 3) goto L_0x00df;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r6, int r7) {
        /*
            r5 = this;
            com.fossil.dd1 r0 = r5.t
            monitor-enter(r0)
            com.fossil.dd1 r1 = r5.b(r6)     // Catch:{ all -> 0x00e3 }
            com.fossil.dd1 r2 = r5.t     // Catch:{ all -> 0x00e3 }
            if (r1 != r2) goto L_0x000d
            monitor-exit(r0)
            return
        L_0x000d:
            if (r6 == 0) goto L_0x0085
            r2 = 1
            if (r6 == r2) goto L_0x0052
            r2 = 2
            if (r6 == r2) goto L_0x001a
            r2 = 3
            if (r6 == r2) goto L_0x0052
            goto L_0x00df
        L_0x001a:
            android.bluetooth.BluetoothGatt r6 = r5.b
            if (r6 == 0) goto L_0x00df
            r5.t = r1
            com.fossil.le0 r6 = com.fossil.le0.DEBUG
            com.fossil.i21 r6 = com.fossil.x71.c
            com.fossil.x71 r6 = r6.a(r7)
            com.fossil.dd1 r1 = r5.t
            android.os.Handler r2 = r5.a
            com.fossil.wy0 r3 = new com.fossil.wy0
            r3.<init>(r5, r6, r1)
            r2.post(r3)
            com.fossil.i21 r6 = com.fossil.x71.c
            com.fossil.x71 r6 = r6.a(r7)
            com.fossil.dd1 r1 = r5.t
            android.os.Handler r2 = r5.a
            com.fossil.vr0 r3 = new com.fossil.vr0
            r3.<init>(r5, r6, r1)
            r2.post(r3)
            com.fossil.dd1 r6 = r5.t
            r5.a(r7, r6)
            com.fossil.dd1 r6 = r5.t
            r5.a(r6)
            goto L_0x00df
        L_0x0052:
            r5.t = r1
            com.fossil.le0 r6 = com.fossil.le0.DEBUG
            com.fossil.i21 r6 = com.fossil.x71.c
            com.fossil.x71 r6 = r6.a(r7)
            com.fossil.dd1 r1 = r5.t
            android.os.Handler r2 = r5.a
            com.fossil.wy0 r3 = new com.fossil.wy0
            r3.<init>(r5, r6, r1)
            r2.post(r3)
            com.fossil.i21 r6 = com.fossil.x71.c
            com.fossil.x71 r6 = r6.a(r7)
            com.fossil.dd1 r1 = r5.t
            android.os.Handler r2 = r5.a
            com.fossil.vr0 r3 = new com.fossil.vr0
            r3.<init>(r5, r6, r1)
            r2.post(r3)
            com.fossil.dd1 r6 = r5.t
            r5.a(r7, r6)
            com.fossil.dd1 r6 = r5.t
            r5.a(r6)
            goto L_0x00df
        L_0x0085:
            r5.t = r1
            com.fossil.le0 r6 = com.fossil.le0.DEBUG
            com.fossil.kv0 r6 = r5.e
            java.util.HashMap<com.fossil.qk1, com.fossil.no1> r1 = r5.i
            r1.clear()
            r1 = 20
            r5.j = r1
            r5.p = r1
            com.fossil.kv0 r1 = new com.fossil.kv0
            r1.<init>(r5)
            r5.e = r1
            com.fossil.st0 r1 = new com.fossil.st0
            com.fossil.cx0 r2 = r5.w
            r1.<init>(r2)
            r5.a(r1)
            com.fossil.i21 r1 = com.fossil.x71.c
            com.fossil.x71 r1 = r1.a(r7)
            com.fossil.dd1 r2 = r5.t
            android.os.Handler r3 = r5.a
            com.fossil.wy0 r4 = new com.fossil.wy0
            r4.<init>(r5, r1, r2)
            r3.post(r4)
            com.fossil.i21 r1 = com.fossil.x71.c
            com.fossil.x71 r1 = r1.a(r7)
            com.fossil.dd1 r2 = r5.t
            android.os.Handler r3 = r5.a
            com.fossil.vr0 r4 = new com.fossil.vr0
            r4.<init>(r5, r1, r2)
            r3.post(r4)
            android.os.Handler r1 = r5.a
            com.fossil.lq1 r2 = new com.fossil.lq1
            r2.<init>(r6)
            r1.post(r2)
            com.fossil.dd1 r6 = r5.t
            r5.a(r7, r6)
            com.fossil.dd1 r6 = r5.t
            r5.a(r6)
        L_0x00df:
            com.fossil.i97 r6 = com.fossil.i97.a
            monitor-exit(r0)
            return
        L_0x00e3:
            r6 = move-exception
            monitor-exit(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ri1.a(int, int):void");
    }

    @DexIgnore
    public final void a(dd1 dd1) {
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new x51(it.next(), this, dd1));
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(p91 p91, p91 p912) {
        this.a.post(new n01(this, new x71(z51.SUCCESS, 0, 2), p91, p912));
        this.a.post(new rt0(this, new x71(z51.SUCCESS, 0, 2), p91, p912));
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            try {
                this.a.post(new a41(it.next(), this, p91, p912));
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(f60.c cVar) {
        if (cVar != f60.c.ENABLED) {
            a(0, xr0.j.a);
        }
    }

    @DexIgnore
    public final void a(List<? extends BluetoothGattService> list) {
        for (BluetoothGattService bluetoothGattService : list) {
            for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()) {
                om1 om1 = no1.b;
                UUID uuid = bluetoothGattCharacteristic.getUuid();
                ee7.a((Object) uuid, "characteristic.uuid");
                qk1 a2 = om1.a(uuid);
                if (a2 != qk1.UNKNOWN) {
                    this.i.put(a2, new no1(bluetoothGattCharacteristic));
                }
            }
        }
    }

    @DexIgnore
    public final void a(eo0 eo0) {
        eo0.e = new ks1(this, eo0);
        eo0.f = new ni0(this, eo0);
        this.e.a(eo0);
    }

    @DexIgnore
    public final void a(eo0 eo0, oi0 oi0) {
        if (eo0 != null) {
            kv0 kv0 = this.e;
            if (!ee7.a(eo0, kv0.b)) {
                eo0.a((vc7<i97>) qt0.a);
                kv0.a.remove(eo0);
            }
            eo0.a(oi0);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        BluetoothGatt bluetoothGatt;
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new vr0(this, new x71(z51.BLUETOOTH_OFF, 0, 2), dd1.DISCONNECTED));
            return;
        }
        int i2 = xe1.a[this.t.ordinal()];
        if (i2 == 1) {
            a(1, 0);
            BluetoothGatt bluetoothGatt2 = this.b;
            if (bluetoothGatt2 == null) {
                le0 le02 = le0.DEBUG;
                if (Build.VERSION.SDK_INT >= 23) {
                    bluetoothGatt = this.x.connectGatt(u31.g.a(), z, this.d, 2);
                } else {
                    bluetoothGatt = this.x.connectGatt(u31.g.a(), z, this.d);
                }
                this.b = bluetoothGatt;
                if (bluetoothGatt == null) {
                    le0 le03 = le0.DEBUG;
                    a(0, xr0.e.a);
                    return;
                }
                le0 le04 = le0.DEBUG;
            } else if (true != bluetoothGatt2.connect()) {
                le0 le05 = le0.DEBUG;
                a(0, xr0.e.a);
            } else {
                le0 le06 = le0.DEBUG;
            }
        } else if (i2 == 3) {
            if (this.b != null) {
                this.a.post(new vr0(this, new x71(z51.SUCCESS, 0, 2), dd1.CONNECTED));
                return;
            }
            a(0, xr0.b.a);
        }
    }

    @DexIgnore
    public final void a() {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            x71 x71 = new x71(z51.BLUETOOTH_OFF, 0, 2);
            p91 p91 = p91.DISCONNECTED;
            this.a.post(new rt0(this, x71, p91, p91));
            return;
        }
        int i2 = xe1.c[e().ordinal()];
        if (i2 == 1) {
            x71 a2 = x71.c.a(zz0.d.a(this.x));
            if (a2.a != z51.SUCCESS) {
                p91 p912 = p91.DISCONNECTED;
                this.a.post(new rt0(this, a2, p912, p912));
            }
        } else if (i2 == 3) {
            x71 x712 = new x71(z51.SUCCESS, 0, 2);
            p91 p913 = p91.CONNECTED;
            this.a.post(new rt0(this, x712, p913, p913));
        }
    }

    @DexIgnore
    public final boolean a(BluetoothGatt bluetoothGatt) {
        boolean z = false;
        String str = null;
        if (yp0.f.a()) {
            if (f60.k.d() != f60.c.ENABLED) {
                le0 le0 = le0.DEBUG;
                str = "Peripheral.refreshDeviceCache: Bluetooth Off.";
            } else {
                try {
                    le0 le02 = le0.DEBUG;
                    Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                    ee7.a((Object) method, "gatt.javaClass.getMethod(\"refresh\")");
                    Object invoke = method.invoke(bluetoothGatt, new Object[0]);
                    if (invoke != null) {
                        z = ((Boolean) invoke).booleanValue();
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.Boolean");
                    }
                } catch (Exception e2) {
                    str = e2.getLocalizedMessage();
                    if (str == null) {
                        str = e2.toString();
                    }
                    wl0.h.a(e2);
                }
            }
        }
        ci1 ci1 = ci1.d;
        String str2 = this.u;
        String a2 = yh0.a("UUID.randomUUID().toString()");
        JSONObject jSONObject = new JSONObject();
        r51 r51 = r51.N0;
        if (str == null) {
            str = JSONObject.NULL;
        }
        wl0.h.a(new wr1("refresh_device_cache", ci1, str2, "", a2, z, null, null, null, yz0.a(jSONObject, r51, str), 448));
        return z;
    }

    @DexIgnore
    public final void a(int i2) {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new ed1(this, new x71(z51.BLUETOOTH_OFF, 0, 2), 0));
        } else if (i2 == this.j) {
            this.a.post(new ed1(this, new x71(z51.SUCCESS, 0, 2), this.j));
        } else {
            BluetoothGatt bluetoothGatt = this.b;
            if (bluetoothGatt == null) {
                this.a.post(new ed1(this, new x71(z51.GATT_NULL, 0, 2), this.j));
            } else if (true != bluetoothGatt.requestMtu(i2)) {
                this.a.post(new ed1(this, new x71(z51.START_FAIL, 0, 2), this.j));
            }
        }
    }

    @DexIgnore
    public final void a(qk1 qk1) {
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
            this.a.post(new v71(this, new x71(z51.BLUETOOTH_OFF, 0, 2), qk1, new byte[0]));
            return;
        }
        x71 x71 = new x71(z51.SUCCESS, 0, 2);
        if (this.b == null) {
            x71 = new x71(z51.GATT_NULL, 0, 2);
        } else {
            no1 no1 = this.i.get(qk1);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = no1 != null ? no1.a : null;
            if (bluetoothGattCharacteristic == null) {
                x71 = new x71(z51.CHARACTERISTIC_NOT_FOUND, 0, 2);
            } else {
                BluetoothGatt bluetoothGatt = this.b;
                if (bluetoothGatt == null || true != bluetoothGatt.readCharacteristic(bluetoothGattCharacteristic)) {
                    x71 = new x71(z51.START_FAIL, 0, 2);
                }
            }
        }
        if (x71.a != z51.SUCCESS) {
            this.a.post(new v71(this, x71, qk1, new byte[0]));
        }
    }

    @DexIgnore
    public final boolean a(qk1 qk1, boolean z) {
        BluetoothGatt bluetoothGatt;
        if (f60.k.d() != f60.c.ENABLED) {
            le0 le0 = le0.DEBUG;
        } else {
            no1 no1 = this.i.get(qk1);
            BluetoothGattCharacteristic bluetoothGattCharacteristic = no1 != null ? no1.a : null;
            if (bluetoothGattCharacteristic == null || (bluetoothGatt = this.b) == null || true != bluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, z)) {
                return false;
            }
            return true;
        }
        return false;
    }
}
