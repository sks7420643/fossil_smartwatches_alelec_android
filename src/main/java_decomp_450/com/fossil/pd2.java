package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.y62;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pd2 extends i72 implements i12 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<pd2> CREATOR; // = new qd2();
    @DexIgnore
    public /* final */ List<gc2> a;
    @DexIgnore
    public /* final */ Status b;

    @DexIgnore
    public pd2(List<gc2> list, Status status) {
        this.a = Collections.unmodifiableList(list);
        this.b = status;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public Status a() {
        return this.b;
    }

    @DexIgnore
    public List<gc2> e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof pd2) {
                pd2 pd2 = (pd2) obj;
                if (this.b.equals(pd2.b) && y62.a(this.a, pd2.a)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(this.b, this.a);
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a("status", this.b);
        a2.a("dataSources", this.a);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.c(parcel, 1, e(), false);
        k72.a(parcel, 2, (Parcelable) a(), i, false);
        k72.a(parcel, a2);
    }
}
