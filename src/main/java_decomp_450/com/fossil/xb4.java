package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xb4 extends m14 {
    @DexIgnore
    public /* final */ a status;

    @DexIgnore
    public enum a {
        BAD_CONFIG
    }

    @DexIgnore
    public xb4(a aVar) {
        this.status = aVar;
    }

    @DexIgnore
    public a getStatus() {
        return this.status;
    }

    @DexIgnore
    public xb4(String str, a aVar) {
        super(str);
        this.status = aVar;
    }

    @DexIgnore
    public xb4(String str, a aVar, Throwable th) {
        super(str, th);
        this.status = aVar;
    }
}
