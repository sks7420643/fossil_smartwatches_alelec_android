package com.fossil;

import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class th7<T> extends rh7<T> {
    @DexIgnore
    public /* final */ Thread d;
    @DexIgnore
    public /* final */ vj7 e;

    @DexIgnore
    public th7(ib7 ib7, Thread thread, vj7 vj7) {
        super(ib7, true);
        this.d = thread;
        this.e = vj7;
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public void a(Object obj) {
        if (!ee7.a(Thread.currentThread(), this.d)) {
            LockSupport.unpark(this.d);
        }
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public boolean j() {
        return true;
    }

    @DexIgnore
    public final T p() {
        gl7 a = hl7.a();
        if (a != null) {
            a.b();
        }
        try {
            vj7 vj7 = this.e;
            li7 li7 = null;
            if (vj7 != null) {
                vj7.b(vj7, false, 1, li7);
            }
            while (!Thread.interrupted()) {
                try {
                    vj7 vj72 = this.e;
                    long n = vj72 != null ? vj72.n() : Long.MAX_VALUE;
                    if (i()) {
                        T t = (T) qk7.b(h());
                        if (t instanceof li7) {
                            li7 = t;
                        }
                        li7 li72 = li7;
                        if (li72 == null) {
                            return t;
                        }
                        throw li72.a;
                    }
                    gl7 a2 = hl7.a();
                    if (a2 != null) {
                        a2.a(this, n);
                    } else {
                        LockSupport.parkNanos(this, n);
                    }
                } finally {
                    vj7 vj73 = this.e;
                    if (vj73 != null) {
                        vj7.a(vj73, false, 1, li7);
                    }
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            a((Throwable) interruptedException);
            throw interruptedException;
        } finally {
            gl7 a3 = hl7.a();
            if (a3 != null) {
                a3.d();
            }
        }
    }
}
