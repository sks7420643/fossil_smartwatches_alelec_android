package com.fossil;

import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uc5 {
    @DexIgnore
    public static final List<mo4> a(List<un4> list) {
        ee7.b(list, "$this$toSuggestedFriends");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            arrayList.add(new mo4(t.d(), t.i(), t.b(), t.e(), t.h(), false, 32, null));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<Object> b(List<un4> list) {
        int i;
        int i2;
        ee7.b(list, "$this$unSectionedFriendToSectionedFriends");
        ArrayList arrayList = new ArrayList();
        int i3 = 0;
        if (((un4) ea7.d((List) list)).c() == 2) {
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                i2 = 0;
                while (it.hasNext()) {
                    if ((it.next().c() == 2) && (i2 = i2 + 1) < 0) {
                        w97.b();
                        throw null;
                    }
                }
            } else {
                i2 = 0;
            }
            we7 we7 = we7.a;
            String a = ig5.a(PortfolioApp.g0.c(), 2131886294);
            ee7.a((Object) a, "LanguageHelper.getString\u2026t_Title__PendingRequests)");
            String format = String.format(a, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            arrayList.add(format);
        }
        if (((un4) ea7.f((List) list)).c() == 0 || ((un4) ea7.f((List) list)).c() == 1 || ((un4) ea7.f((List) list)).c() == -1) {
            if (!(list instanceof Collection) || !list.isEmpty()) {
                i = 0;
                for (T t : list) {
                    if ((t.c() == 0 || t.c() == 1 || t.c() == -1) && (i = i + 1) < 0) {
                        w97.b();
                        throw null;
                    }
                }
            } else {
                i = 0;
            }
            we7 we72 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886293);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ends_List_Title__Friends)");
            String format2 = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format2, "java.lang.String.format(format, *args)");
            arrayList.add(format2);
        }
        for (T t2 : list) {
            if (t2.c() == 2) {
                i3++;
                arrayList.add(i3, t2);
            } else {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final un4 a(un4 un4) {
        ee7.b(un4, "$this$clone");
        return new un4(un4.d(), un4.i(), un4.b(), un4.e(), un4.g(), un4.h(), un4.a(), un4.f(), un4.c());
    }
}
