package com.fossil;

import com.fossil.ag7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class le7 extends ne7 implements ag7 {
    @DexIgnore
    public le7() {
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public sf7 computeReflected() {
        te7.a(this);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ag7
    public Object getDelegate() {
        return ((ag7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.vc7
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public le7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.ag7
    public ag7.a getGetter() {
        return ((ag7) getReflected()).getGetter();
    }
}
