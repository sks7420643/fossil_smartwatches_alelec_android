package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af6;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg6 extends go5 implements ag6, View.OnClickListener, af6.b {
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public qw6<u15> f;
    @DexIgnore
    public zf6 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public af6 i;
    @DexIgnore
    public /* final */ Calendar j; // = Calendar.getInstance();
    @DexIgnore
    public String p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final bg6 a(Date date) {
            ee7.b(date, "date");
            bg6 bg6 = new bg6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            bg6.setArguments(bundle);
            return bg6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ qf b;

        @DexIgnore
        public b(bg6 bg6, boolean z, qf qfVar, ob5 ob5) {
            this.a = z;
            this.b = qfVar;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            ee7.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public bg6() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.q = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("backgroundDashboard");
        this.r = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("secondaryText");
        this.s = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("primaryText");
        this.t = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("nonBrandNonReachGoal");
        this.u = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.v = Color.parseColor(b7 != null ? b7 : str);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void b(WorkoutSession workoutSession) {
        ee7.b(workoutSession, "workoutSession");
    }

    @DexIgnore
    @Override // com.fossil.ag6
    public void c(int i2, int i3) {
        qw6<u15> qw6 = this.f;
        if (qw6 != null) {
            u15 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (i2 == 0 && i3 == 0) {
                FlexibleTextView flexibleTextView = a2.A;
                ee7.a((Object) flexibleTextView, "it.ftvNoRecord");
                flexibleTextView.setVisibility(0);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "it.clContainer");
                constraintLayout.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.A;
            ee7.a((Object) flexibleTextView2, "it.ftvNoRecord");
            flexibleTextView2.setVisibility(8);
            ConstraintLayout constraintLayout2 = a2.s;
            ee7.a((Object) constraintLayout2, "it.clContainer");
            constraintLayout2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = a2.D;
            ee7.a((Object) flexibleTextView3, "it.ftvRestingValue");
            flexibleTextView3.setText(String.valueOf(i2));
            FlexibleTextView flexibleTextView4 = a2.z;
            ee7.a((Object) flexibleTextView4, "it.ftvMaxValue");
            flexibleTextView4.setText(String.valueOf(i3));
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HeartRateDetailFragment";
    }

    @DexIgnore
    public final void f1() {
        TodayHeartRateChart todayHeartRateChart;
        qw6<u15> qw6 = this.f;
        if (qw6 != null) {
            u15 a2 = qw6.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.a("maxHeartRate", "lowestHeartRate");
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        MFLogger.d("HeartRateDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362636:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362637:
                    zf6 zf6 = this.g;
                    if (zf6 != null) {
                        zf6.l();
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                case 2131362705:
                    zf6 zf62 = this.g;
                    if (zf62 != null) {
                        zf62.k();
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        u15 u15 = (u15) qb.a(layoutInflater, 2131558562, viewGroup, false, a1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.h = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        ee7.a((Object) u15, "binding");
        a(u15);
        zf6 zf6 = this.g;
        if (zf6 != null) {
            zf6.a(this.h);
            this.f = new qw6<>(this, u15);
            f1();
            qw6<u15> qw6 = this.f;
            if (qw6 != null) {
                u15 a2 = qw6.a();
                if (a2 != null) {
                    return a2.d();
                }
                return null;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        zf6 zf6 = this.g;
        if (zf6 != null) {
            zf6.j();
            Lifecycle lifecycle = getLifecycle();
            zf6 zf62 = this.g;
            if (zf62 != null) {
                lifecycle.b(zf62.i());
                super.onDestroyView();
                Z0();
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        zf6 zf6 = this.g;
        if (zf6 != null) {
            zf6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
        zf6 zf6 = this.g;
        if (zf6 != null) {
            zf6.b(this.h);
            zf6 zf62 = this.g;
            if (zf62 != null) {
                zf62.f();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        zf6 zf6 = this.g;
        if (zf6 != null) {
            zf6.a(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void a(u15 u15) {
        u15.F.setOnClickListener(this);
        u15.G.setOnClickListener(this);
        u15.H.setOnClickListener(this);
        this.p = eh5.l.a().b("dianaHeartRateTab");
        af6 af6 = new af6(af6.d.HEART_RATE, ob5.IMPERIAL, new WorkoutSessionDifference(), this.p);
        this.i = af6;
        if (af6 != null) {
            af6.a(this);
        }
        RecyclerView recyclerView = u15.L;
        ee7.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.i);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = v6.c(recyclerView.getContext(), 2131230856);
        if (c != null) {
            fe6 fe6 = new fe6(linearLayoutManager.Q(), false, false, 6, null);
            ee7.a((Object) c, ResourceManager.DRAWABLE);
            fe6.a(c);
            recyclerView.addItemDecoration(fe6);
        }
        u15.J.setBackgroundColor(this.r);
        u15.r.setBackgroundColor(this.q);
        u15.D.setTextColor(this.t);
        u15.C.setTextColor(this.u);
        u15.z.setTextColor(this.t);
        u15.y.setTextColor(this.u);
        u15.x.setTextColor(this.s);
        u15.w.setTextColor(this.t);
        u15.t.setBackgroundColor(this.v);
        u15.v.setBackgroundColor(this.r);
    }

    @DexIgnore
    public void a(zf6 zf6) {
        ee7.b(zf6, "presenter");
        this.g = zf6;
        Lifecycle lifecycle = getLifecycle();
        zf6 zf62 = this.g;
        if (zf62 == null) {
            ee7.d("mPresenter");
            throw null;
        } else if (zf62 != null) {
            lifecycle.a(zf62.i());
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ag6
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        ee7.b(date, "date");
        this.h = date;
        Calendar calendar = this.j;
        ee7.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.j.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.j);
        qw6<u15> qw6 = this.f;
        if (qw6 != null) {
            u15 a2 = qw6.a();
            if (a2 != null) {
                a2.q.a(true, true);
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.j.get(5)));
                if (z) {
                    ImageView imageView = a2.G;
                    ee7.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.G;
                    ee7.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.H;
                    ee7.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.x;
                        ee7.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(ig5.a(getContext(), 2131886616));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.x;
                    ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(xe5.b.b(i2));
                    return;
                }
                ImageView imageView4 = a2.H;
                ee7.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.x;
                ee7.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(xe5.b.b(i2));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ag6
    public void a(int i2, List<fz6> list, List<v87<Integer, r87<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        ee7.b(list, "listTodayHeartRateModel");
        ee7.b(list2, "listTimeZoneChange");
        qw6<u15> qw6 = this.f;
        if (qw6 != null) {
            u15 a2 = qw6.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.a(list);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ag6
    public void a(boolean z, ob5 ob5, qf<WorkoutSession> qfVar) {
        ee7.b(ob5, "distanceUnit");
        ee7.b(qfVar, "workoutSessions");
        qw6<u15> qw6 = this.f;
        if (qw6 != null) {
            u15 a2 = qw6.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.J;
                    ee7.a((Object) linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!qfVar.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.B;
                        ee7.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.L;
                        ee7.a((Object) recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        af6 af6 = this.i;
                        if (af6 != null) {
                            af6.a(ob5, qfVar);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.B;
                        ee7.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.L;
                        ee7.a((Object) recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        af6 af62 = this.i;
                        if (af62 != null) {
                            af62.a(ob5, qfVar);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.J;
                    ee7.a((Object) linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                ee7.a((Object) appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new b(this, z, qfVar, ob5));
                    eVar.a(behavior);
                    return;
                }
                throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.af6.b
    public void a(WorkoutSession workoutSession) {
        String str;
        ob5 h2;
        ee7.b(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            zf6 zf6 = this.g;
            if (zf6 != null) {
                if (zf6 == null || (h2 = zf6.h()) == null || (str = h2.name()) == null) {
                    str = ob5.METRIC.name();
                }
                WorkoutDetailActivity.a aVar = WorkoutDetailActivity.e;
                ee7.a((Object) activity, "it");
                aVar.a(activity, workoutSession.getId(), str);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }
}
