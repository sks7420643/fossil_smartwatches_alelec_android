package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.rp2;
import com.fossil.tp2;
import com.fossil.up2;
import com.fossil.vp2;
import com.fossil.zp2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xl3 implements ki3 {
    @DexIgnore
    public static volatile xl3 x;
    @DexIgnore
    public ih3 a;
    @DexIgnore
    public ng3 b;
    @DexIgnore
    public jb3 c;
    @DexIgnore
    public ug3 d;
    @DexIgnore
    public tl3 e;
    @DexIgnore
    public om3 f;
    @DexIgnore
    public /* final */ fm3 g;
    @DexIgnore
    public uj3 h;
    @DexIgnore
    public /* final */ oh3 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public long l;
    @DexIgnore
    public List<Runnable> m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public FileLock s;
    @DexIgnore
    public FileChannel t;
    @DexIgnore
    public List<Long> u;
    @DexIgnore
    public List<Long> v;
    @DexIgnore
    public long w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements lb3 {
        @DexIgnore
        public vp2 a;
        @DexIgnore
        public List<Long> b;
        @DexIgnore
        public List<rp2> c;
        @DexIgnore
        public long d;

        @DexIgnore
        public a(xl3 xl3) {
        }

        @DexIgnore
        @Override // com.fossil.lb3
        public final void a(vp2 vp2) {
            a72.a(vp2);
            this.a = vp2;
        }

        @DexIgnore
        public /* synthetic */ a(xl3 xl3, am3 am3) {
            this(xl3);
        }

        @DexIgnore
        @Override // com.fossil.lb3
        public final boolean a(long j, rp2 rp2) {
            a72.a(rp2);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && a(this.c.get(0)) != a(rp2)) {
                return false;
            }
            long k = this.d + ((long) rp2.k());
            if (k >= ((long) Math.max(0, wb3.i.a(null).intValue()))) {
                return false;
            }
            this.d = k;
            this.c.add(rp2);
            this.b.add(Long.valueOf(j));
            if (this.c.size() >= Math.max(1, wb3.j.a(null).intValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public static long a(rp2 rp2) {
            return ((rp2.s() / 1000) / 60) / 60;
        }
    }

    @DexIgnore
    public xl3(dm3 dm3) {
        this(dm3, null);
    }

    @DexIgnore
    public static xl3 a(Context context) {
        a72.a(context);
        a72.a(context.getApplicationContext());
        if (x == null) {
            synchronized (xl3.class) {
                if (x == null) {
                    x = new xl3(new dm3(context));
                }
            }
        }
        return x;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:48:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A() {
        /*
            r21 = this;
            r0 = r21
            r21.x()
            r21.q()
            long r1 = r0.l
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x004d
            com.fossil.oh3 r1 = r0.i
            com.fossil.n92 r1 = r1.zzm()
            long r1 = r1.c()
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r0.l
            long r1 = r1 - r7
            long r1 = java.lang.Math.abs(r1)
            long r5 = r5 - r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x004b
            com.fossil.oh3 r1 = r0.i
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            java.lang.String r3 = "Upload has been suspended. Will update scheduling later in approximately ms"
            r1.a(r3, r2)
            com.fossil.ug3 r1 = r21.v()
            r1.b()
            com.fossil.tl3 r1 = r21.w()
            r1.t()
            return
        L_0x004b:
            r0.l = r3
        L_0x004d:
            com.fossil.oh3 r1 = r0.i
            boolean r1 = r1.l()
            if (r1 == 0) goto L_0x0255
            boolean r1 = r21.z()
            if (r1 != 0) goto L_0x005d
            goto L_0x0255
        L_0x005d:
            com.fossil.oh3 r1 = r0.i
            com.fossil.n92 r1 = r1.zzm()
            long r1 = r1.b()
            com.fossil.yf3<java.lang.Long> r5 = com.fossil.wb3.A
            r6 = 0
            java.lang.Object r5 = r5.a(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r7 = r5.longValue()
            long r7 = java.lang.Math.max(r3, r7)
            com.fossil.jb3 r5 = r21.k()
            boolean r5 = r5.F()
            if (r5 != 0) goto L_0x008f
            com.fossil.jb3 r5 = r21.k()
            boolean r5 = r5.A()
            if (r5 == 0) goto L_0x008d
            goto L_0x008f
        L_0x008d:
            r5 = 0
            goto L_0x0090
        L_0x008f:
            r5 = 1
        L_0x0090:
            if (r5 == 0) goto L_0x00cc
            com.fossil.oh3 r10 = r0.i
            com.fossil.ym3 r10 = r10.o()
            java.lang.String r10 = r10.s()
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x00bb
            java.lang.String r11 = ".none."
            boolean r10 = r11.equals(r10)
            if (r10 != 0) goto L_0x00bb
            com.fossil.yf3<java.lang.Long> r10 = com.fossil.wb3.v
            java.lang.Object r10 = r10.a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00dc
        L_0x00bb:
            com.fossil.yf3<java.lang.Long> r10 = com.fossil.wb3.u
            java.lang.Object r10 = r10.a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00dc
        L_0x00cc:
            com.fossil.yf3<java.lang.Long> r10 = com.fossil.wb3.t
            java.lang.Object r10 = r10.a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
        L_0x00dc:
            com.fossil.oh3 r12 = r0.i
            com.fossil.wg3 r12 = r12.p()
            com.fossil.ah3 r12 = r12.e
            long r12 = r12.a()
            com.fossil.oh3 r14 = r0.i
            com.fossil.wg3 r14 = r14.p()
            com.fossil.ah3 r14 = r14.f
            long r14 = r14.a()
            com.fossil.jb3 r16 = r21.k()
            r17 = r10
            long r9 = r16.C()
            com.fossil.jb3 r11 = r21.k()
            r19 = r7
            long r6 = r11.D()
            long r6 = java.lang.Math.max(r9, r6)
            int r8 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0113
        L_0x0110:
            r10 = r3
            goto L_0x0188
        L_0x0113:
            long r6 = r6 - r1
            long r6 = java.lang.Math.abs(r6)
            long r6 = r1 - r6
            long r12 = r12 - r1
            long r8 = java.lang.Math.abs(r12)
            long r8 = r1 - r8
            long r14 = r14 - r1
            long r10 = java.lang.Math.abs(r14)
            long r1 = r1 - r10
            long r8 = java.lang.Math.max(r8, r1)
            long r10 = r6 + r19
            if (r5 == 0) goto L_0x0139
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0139
            long r10 = java.lang.Math.min(r6, r8)
            long r10 = r10 + r17
        L_0x0139:
            com.fossil.fm3 r5 = r21.n()
            r12 = r17
            boolean r5 = r5.a(r8, r12)
            if (r5 != 0) goto L_0x0147
            long r10 = r8 + r12
        L_0x0147:
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0188
            int r5 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x0188
            r5 = 0
        L_0x0150:
            r6 = 20
            com.fossil.yf3<java.lang.Integer> r7 = com.fossil.wb3.C
            r8 = 0
            java.lang.Object r7 = r7.a(r8)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            r9 = 0
            int r7 = java.lang.Math.max(r9, r7)
            int r6 = java.lang.Math.min(r6, r7)
            if (r5 >= r6) goto L_0x0110
            r6 = 1
            long r6 = r6 << r5
            com.fossil.yf3<java.lang.Long> r12 = com.fossil.wb3.B
            java.lang.Object r12 = r12.a(r8)
            java.lang.Long r12 = (java.lang.Long) r12
            long r12 = r12.longValue()
            long r12 = java.lang.Math.max(r3, r12)
            long r12 = r12 * r6
            long r10 = r10 + r12
            int r6 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x0185
            goto L_0x0188
        L_0x0185:
            int r5 = r5 + 1
            goto L_0x0150
        L_0x0188:
            int r1 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x01aa
            com.fossil.oh3 r1 = r0.i
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.String r2 = "Next upload time is 0"
            r1.a(r2)
            com.fossil.ug3 r1 = r21.v()
            r1.b()
            com.fossil.tl3 r1 = r21.w()
            r1.t()
            return
        L_0x01aa:
            com.fossil.ng3 r1 = r21.j()
            boolean r1 = r1.t()
            if (r1 != 0) goto L_0x01d2
            com.fossil.oh3 r1 = r0.i
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.String r2 = "No network"
            r1.a(r2)
            com.fossil.ug3 r1 = r21.v()
            r1.a()
            com.fossil.tl3 r1 = r21.w()
            r1.t()
            return
        L_0x01d2:
            com.fossil.oh3 r1 = r0.i
            com.fossil.wg3 r1 = r1.p()
            com.fossil.ah3 r1 = r1.g
            long r1 = r1.a()
            com.fossil.yf3<java.lang.Long> r5 = com.fossil.wb3.r
            r6 = 0
            java.lang.Object r5 = r5.a(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.fossil.fm3 r7 = r21.n()
            boolean r7 = r7.a(r1, r5)
            if (r7 != 0) goto L_0x01fe
            long r1 = r1 + r5
            long r10 = java.lang.Math.max(r10, r1)
        L_0x01fe:
            com.fossil.ug3 r1 = r21.v()
            r1.b()
            com.fossil.oh3 r1 = r0.i
            com.fossil.n92 r1 = r1.zzm()
            long r1 = r1.b()
            long r10 = r10 - r1
            int r1 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x023a
            com.fossil.yf3<java.lang.Long> r1 = com.fossil.wb3.w
            r2 = 0
            java.lang.Object r1 = r1.a(r2)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r10 = java.lang.Math.max(r3, r1)
            com.fossil.oh3 r1 = r0.i
            com.fossil.wg3 r1 = r1.p()
            com.fossil.ah3 r1 = r1.e
            com.fossil.oh3 r2 = r0.i
            com.fossil.n92 r2 = r2.zzm()
            long r2 = r2.b()
            r1.a(r2)
        L_0x023a:
            com.fossil.oh3 r1 = r0.i
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.Long r2 = java.lang.Long.valueOf(r10)
            java.lang.String r3 = "Upload scheduled in approximately ms"
            r1.a(r3, r2)
            com.fossil.tl3 r1 = r21.w()
            r1.a(r10)
            return
        L_0x0255:
            com.fossil.oh3 r1 = r0.i
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.a(r2)
            com.fossil.ug3 r1 = r21.v()
            r1.b()
            com.fossil.tl3 r1 = r21.w()
            r1.t()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.A():void");
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final xm3 b() {
        return this.i.b();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final hh3 c() {
        return this.i.c();
    }

    @DexIgnore
    public final void d() {
        x();
        if (this.p || this.q || this.r) {
            this.i.e().B().a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.p), Boolean.valueOf(this.q), Boolean.valueOf(this.r));
            return;
        }
        this.i.e().B().a("Stopping uploading service(s)");
        List<Runnable> list = this.m;
        if (list != null) {
            for (Runnable runnable : list) {
                runnable.run();
            }
            this.m.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final jg3 e() {
        return this.i.e();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final Context f() {
        return this.i.f();
    }

    @DexIgnore
    public final boolean g() {
        FileLock fileLock;
        x();
        if (!this.i.o().a(wb3.m0) || (fileLock = this.s) == null || !fileLock.isValid()) {
            try {
                FileChannel channel = new RandomAccessFile(new File(this.i.f().getFilesDir(), "google_app_measurement.db"), rw.u).getChannel();
                this.t = channel;
                FileLock tryLock = channel.tryLock();
                this.s = tryLock;
                if (tryLock != null) {
                    this.i.e().B().a("Storage concurrent access okay");
                    return true;
                }
                this.i.e().t().a("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e2) {
                this.i.e().t().a("Failed to acquire storage lock", e2);
                return false;
            } catch (IOException e3) {
                this.i.e().t().a("Failed to access storage lock file", e3);
                return false;
            } catch (OverlappingFileLockException e4) {
                this.i.e().w().a("Storage lock already acquired", e4);
                return false;
            }
        } else {
            this.i.e().B().a("Storage concurrent access okay");
            return true;
        }
    }

    @DexIgnore
    public final ym3 h() {
        return this.i.o();
    }

    @DexIgnore
    public final ih3 i() {
        b(this.a);
        return this.a;
    }

    @DexIgnore
    public final ng3 j() {
        b(this.b);
        return this.b;
    }

    @DexIgnore
    public final jb3 k() {
        b(this.c);
        return this.c;
    }

    @DexIgnore
    public final om3 l() {
        b(this.f);
        return this.f;
    }

    @DexIgnore
    public final uj3 m() {
        b(this.h);
        return this.h;
    }

    @DexIgnore
    public final fm3 n() {
        b(this.g);
        return this.g;
    }

    @DexIgnore
    public final hg3 o() {
        return this.i.w();
    }

    @DexIgnore
    public final jm3 p() {
        return this.i.v();
    }

    @DexIgnore
    public final void q() {
        if (!this.j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    @DexIgnore
    public final void r() {
        kg3 b2;
        String str;
        x();
        q();
        this.r = true;
        try {
            this.i.b();
            Boolean F = this.i.E().F();
            if (F == null) {
                this.i.e().w().a("Upload data called on the client side before use of service was decided");
            } else if (F.booleanValue()) {
                this.i.e().t().a("Upload called in the client side when service should be used");
                this.r = false;
                d();
            } else if (this.l > 0) {
                A();
                this.r = false;
                d();
            } else {
                x();
                if (this.u != null) {
                    this.i.e().B().a("Uploading requested multiple times");
                    this.r = false;
                    d();
                } else if (!j().t()) {
                    this.i.e().B().a("Network not connected, ignoring upload request");
                    A();
                    this.r = false;
                    d();
                } else {
                    long b3 = this.i.zzm().b();
                    int b4 = this.i.o().b(null, wb3.Q);
                    long x2 = b3 - ym3.x();
                    for (int i2 = 0; i2 < b4 && a((String) null, x2); i2++) {
                    }
                    long a2 = this.i.p().e.a();
                    if (a2 != 0) {
                        this.i.e().A().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(b3 - a2)));
                    }
                    String v2 = k().v();
                    if (!TextUtils.isEmpty(v2)) {
                        if (this.w == -1) {
                            this.w = k().w();
                        }
                        List<Pair<vp2, Long>> a3 = k().a(v2, this.i.o().b(v2, wb3.g), Math.max(0, this.i.o().b(v2, wb3.h)));
                        if (!a3.isEmpty()) {
                            Iterator<Pair<vp2, Long>> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                vp2 vp2 = (vp2) it.next().first;
                                if (!TextUtils.isEmpty(vp2.s())) {
                                    str = vp2.s();
                                    break;
                                }
                            }
                            if (str != null) {
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= a3.size()) {
                                        break;
                                    }
                                    vp2 vp22 = (vp2) a3.get(i3).first;
                                    if (!TextUtils.isEmpty(vp22.s()) && !vp22.s().equals(str)) {
                                        a3 = a3.subList(0, i3);
                                        break;
                                    }
                                    i3++;
                                }
                            }
                            up2.a p2 = up2.p();
                            int size = a3.size();
                            ArrayList arrayList = new ArrayList(a3.size());
                            boolean g2 = this.i.o().g(v2);
                            for (int i4 = 0; i4 < size; i4++) {
                                vp2.a aVar = (vp2.a) ((vp2) a3.get(i4).first).l();
                                arrayList.add((Long) a3.get(i4).second);
                                aVar.g(this.i.o().n());
                                aVar.a(b3);
                                this.i.b();
                                aVar.b(false);
                                if (!g2) {
                                    aVar.y();
                                }
                                if (this.i.o().e(v2, wb3.Z)) {
                                    aVar.l(n().a(((vp2) ((bw2) aVar.g())).a()));
                                }
                                p2.a(aVar);
                            }
                            String a4 = this.i.e().a(2) ? n().a((up2) ((bw2) p2.g())) : null;
                            n();
                            byte[] a5 = ((up2) ((bw2) p2.g())).a();
                            String a6 = wb3.q.a(null);
                            try {
                                URL url = new URL(a6);
                                a72.a(!arrayList.isEmpty());
                                if (this.u != null) {
                                    this.i.e().t().a("Set uploading progress before finishing the previous upload");
                                } else {
                                    this.u = new ArrayList(arrayList);
                                }
                                this.i.p().f.a(b3);
                                String str2 = "?";
                                if (size > 0) {
                                    str2 = p2.a(0).w0();
                                }
                                this.i.e().B().a("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(a5.length), a4);
                                this.q = true;
                                ng3 j2 = j();
                                zl3 zl3 = new zl3(this, v2);
                                j2.g();
                                j2.q();
                                a72.a(url);
                                a72.a(a5);
                                a72.a(zl3);
                                j2.c().b(new rg3(j2, v2, url, a5, null, zl3));
                            } catch (MalformedURLException unused) {
                                this.i.e().t().a("Failed to parse upload URL. Not uploading. appId", jg3.a(v2), a6);
                            }
                        }
                    } else {
                        this.w = -1;
                        String a7 = k().a(b3 - ym3.x());
                        if (!TextUtils.isEmpty(a7) && (b2 = k().b(a7)) != null) {
                            a(b2);
                        }
                    }
                    this.r = false;
                    d();
                }
            }
        } finally {
            this.r = false;
            d();
        }
    }

    @DexIgnore
    public final void s() {
        x();
        q();
        if (!this.k) {
            this.k = true;
            if (g()) {
                int a2 = a(this.t);
                int E = this.i.G().E();
                x();
                if (a2 > E) {
                    this.i.e().t().a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                } else if (a2 >= E) {
                } else {
                    if (a(E, this.t)) {
                        this.i.e().B().a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    } else {
                        this.i.e().t().a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(E));
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void t() {
        this.o++;
    }

    @DexIgnore
    public final oh3 u() {
        return this.i;
    }

    @DexIgnore
    public final ug3 v() {
        ug3 ug3 = this.d;
        if (ug3 != null) {
            return ug3;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    @DexIgnore
    public final tl3 w() {
        b(this.e);
        return this.e;
    }

    @DexIgnore
    public final void x() {
        this.i.c().g();
    }

    @DexIgnore
    public final long y() {
        long b2 = this.i.zzm().b();
        wg3 p2 = this.i.p();
        p2.n();
        p2.g();
        long a2 = p2.i.a();
        if (a2 == 0) {
            a2 = 1 + ((long) p2.j().t().nextInt(DateTimeConstants.MILLIS_PER_DAY));
            p2.i.a(a2);
        }
        return ((((b2 + a2) / 1000) / 60) / 60) / 24;
    }

    @DexIgnore
    public final boolean z() {
        x();
        q();
        return k().E() || !TextUtils.isEmpty(k().v());
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final n92 zzm() {
        return this.i.zzm();
    }

    @DexIgnore
    public xl3(dm3 dm3, oh3 oh3) {
        this.j = false;
        a72.a(dm3);
        this.i = oh3.a(dm3.a, null, null);
        this.w = -1;
        fm3 fm3 = new fm3(this);
        fm3.r();
        this.g = fm3;
        ng3 ng3 = new ng3(this);
        ng3.r();
        this.b = ng3;
        ih3 ih3 = new ih3(this);
        ih3.r();
        this.a = ih3;
        this.i.c().a(new am3(this, dm3));
    }

    @DexIgnore
    public static void b(yl3 yl3) {
        if (yl3 == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!yl3.p()) {
            String valueOf = String.valueOf(yl3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0318  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(com.fossil.ub3 r27, com.fossil.nm3 r28) {
        /*
            r26 = this;
            r1 = r26
            r2 = r27
            r3 = r28
            java.lang.String r4 = "_sno"
            com.fossil.a72.a(r28)
            java.lang.String r5 = r3.a
            com.fossil.a72.b(r5)
            long r5 = java.lang.System.nanoTime()
            r26.x()
            r26.q()
            java.lang.String r15 = r3.a
            r26.n()
            boolean r7 = com.fossil.fm3.a(r27, r28)
            if (r7 != 0) goto L_0x0026
            return
        L_0x0026:
            boolean r7 = r3.h
            if (r7 != 0) goto L_0x002e
            r1.c(r3)
            return
        L_0x002e:
            com.fossil.ih3 r7 = r26.i()
            java.lang.String r8 = r2.a
            boolean r7 = r7.b(r15, r8)
            java.lang.String r14 = "_err"
            r13 = 0
            if (r7 == 0) goto L_0x00d9
            com.fossil.oh3 r3 = r1.i
            com.fossil.jg3 r3 = r3.e()
            com.fossil.mg3 r3 = r3.w()
            java.lang.Object r4 = com.fossil.jg3.a(r15)
            com.fossil.oh3 r5 = r1.i
            com.fossil.hg3 r5 = r5.w()
            java.lang.String r6 = r2.a
            java.lang.String r5 = r5.a(r6)
            java.lang.String r6 = "Dropping blacklisted event. appId"
            r3.a(r6, r4, r5)
            com.fossil.ih3 r3 = r26.i()
            boolean r3 = r3.g(r15)
            if (r3 != 0) goto L_0x0073
            com.fossil.ih3 r3 = r26.i()
            boolean r3 = r3.h(r15)
            if (r3 == 0) goto L_0x0071
            goto L_0x0073
        L_0x0071:
            r3 = 0
            goto L_0x0074
        L_0x0073:
            r3 = 1
        L_0x0074:
            if (r3 != 0) goto L_0x008f
            java.lang.String r4 = r2.a
            boolean r4 = r14.equals(r4)
            if (r4 != 0) goto L_0x008f
            com.fossil.oh3 r4 = r1.i
            com.fossil.jm3 r7 = r4.v()
            r9 = 11
            java.lang.String r11 = r2.a
            r12 = 0
            java.lang.String r10 = "_ev"
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)
        L_0x008f:
            if (r3 == 0) goto L_0x00d8
            com.fossil.jb3 r2 = r26.k()
            com.fossil.kg3 r2 = r2.b(r15)
            if (r2 == 0) goto L_0x00d8
            long r3 = r2.D()
            long r5 = r2.C()
            long r3 = java.lang.Math.max(r3, r5)
            com.fossil.oh3 r5 = r1.i
            com.fossil.n92 r5 = r5.zzm()
            long r5 = r5.b()
            long r5 = r5 - r3
            long r3 = java.lang.Math.abs(r5)
            com.fossil.yf3<java.lang.Long> r5 = com.fossil.wb3.z
            java.lang.Object r5 = r5.a(r13)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00d8
            com.fossil.oh3 r3 = r1.i
            com.fossil.jg3 r3 = r3.e()
            com.fossil.mg3 r3 = r3.A()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.a(r4)
            r1.a(r2)
        L_0x00d8:
            return
        L_0x00d9:
            boolean r7 = com.fossil.e03.a()
            if (r7 == 0) goto L_0x0108
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            com.fossil.yf3<java.lang.Boolean> r8 = com.fossil.wb3.K0
            boolean r7 = r7.a(r8)
            if (r7 == 0) goto L_0x0108
            com.fossil.og3 r2 = com.fossil.og3.a(r27)
            com.fossil.oh3 r7 = r1.i
            com.fossil.jm3 r7 = r7.v()
            com.fossil.oh3 r8 = r1.i
            com.fossil.ym3 r8 = r8.o()
            int r8 = r8.a(r15)
            r7.a(r2, r8)
            com.fossil.ub3 r2 = r2.a()
        L_0x0108:
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            r8 = 2
            boolean r7 = r7.a(r8)
            if (r7 == 0) goto L_0x012e
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.B()
            com.fossil.oh3 r9 = r1.i
            com.fossil.hg3 r9 = r9.w()
            java.lang.String r9 = r9.a(r2)
            java.lang.String r10 = "Logging event"
            r7.a(r10, r9)
        L_0x012e:
            com.fossil.jb3 r7 = r26.k()
            r7.y()
            r1.c(r3)     // Catch:{ all -> 0x093d }
            boolean r7 = com.fossil.p03.a()     // Catch:{ all -> 0x093d }
            if (r7 == 0) goto L_0x014e
            com.fossil.oh3 r7 = r1.i     // Catch:{ all -> 0x093d }
            com.fossil.ym3 r7 = r7.o()     // Catch:{ all -> 0x093d }
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.J0     // Catch:{ all -> 0x093d }
            boolean r7 = r7.a(r9)     // Catch:{ all -> 0x093d }
            if (r7 == 0) goto L_0x014e
            r7 = 1
            goto L_0x014f
        L_0x014e:
            r7 = 0
        L_0x014f:
            java.lang.String r9 = "ecommerce_purchase"
            java.lang.String r10 = r2.a     // Catch:{ all -> 0x093d }
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x093d }
            java.lang.String r10 = "refund"
            if (r9 != 0) goto L_0x0172
            if (r7 == 0) goto L_0x0170
            java.lang.String r7 = "purchase"
            java.lang.String r9 = r2.a
            boolean r7 = r7.equals(r9)
            if (r7 != 0) goto L_0x0172
            java.lang.String r7 = r2.a
            boolean r7 = r10.equals(r7)
            if (r7 == 0) goto L_0x0170
            goto L_0x0172
        L_0x0170:
            r7 = 0
            goto L_0x0173
        L_0x0172:
            r7 = 1
        L_0x0173:
            java.lang.String r9 = "_iap"
            java.lang.String r11 = r2.a
            boolean r9 = r9.equals(r11)
            if (r9 != 0) goto L_0x0182
            if (r7 == 0) goto L_0x0180
            goto L_0x0182
        L_0x0180:
            r9 = 0
            goto L_0x0183
        L_0x0182:
            r9 = 1
        L_0x0183:
            if (r9 == 0) goto L_0x0327
            com.fossil.tb3 r9 = r2.b
            java.lang.String r11 = "currency"
            java.lang.String r9 = r9.e(r11)
            java.lang.String r11 = "value"
            if (r7 == 0) goto L_0x0200
            com.fossil.tb3 r7 = r2.b
            java.lang.Double r7 = r7.d(r11)
            double r17 = r7.doubleValue()
            r19 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r17 = r17 * r19
            r21 = 0
            int r7 = (r17 > r21 ? 1 : (r17 == r21 ? 0 : -1))
            if (r7 != 0) goto L_0x01b5
            com.fossil.tb3 r7 = r2.b
            java.lang.Long r7 = r7.c(r11)
            long r12 = r7.longValue()
            double r11 = (double) r12
            double r17 = r11 * r19
        L_0x01b5:
            r11 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r7 = (r17 > r11 ? 1 : (r17 == r11 ? 0 : -1))
            if (r7 > 0) goto L_0x01e3
            r11 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r7 = (r17 > r11 ? 1 : (r17 == r11 ? 0 : -1))
            if (r7 < 0) goto L_0x01e3
            long r11 = java.lang.Math.round(r17)
            boolean r7 = com.fossil.p03.a()
            if (r7 == 0) goto L_0x020a
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            com.fossil.yf3<java.lang.Boolean> r13 = com.fossil.wb3.J0
            boolean r7 = r7.a(r13)
            if (r7 == 0) goto L_0x020a
            java.lang.String r7 = r2.a
            boolean r7 = r10.equals(r7)
            if (r7 == 0) goto L_0x020a
            long r11 = -r11
            goto L_0x020a
        L_0x01e3:
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.w()
            java.lang.String r8 = "Data lost. Currency value is too big. appId"
            java.lang.Object r9 = com.fossil.jg3.a(r15)
            java.lang.Double r10 = java.lang.Double.valueOf(r17)
            r7.a(r8, r9, r10)
            r23 = r5
            r5 = 0
            r11 = 0
            goto L_0x0316
        L_0x0200:
            com.fossil.tb3 r7 = r2.b
            java.lang.Long r7 = r7.c(r11)
            long r11 = r7.longValue()
        L_0x020a:
            boolean r7 = android.text.TextUtils.isEmpty(r9)
            if (r7 != 0) goto L_0x0312
            java.util.Locale r7 = java.util.Locale.US
            java.lang.String r7 = r9.toUpperCase(r7)
            java.lang.String r9 = "[A-Z]{3}"
            boolean r9 = r7.matches(r9)
            if (r9 == 0) goto L_0x0312
            java.lang.String r9 = "_ltv_"
            java.lang.String r7 = java.lang.String.valueOf(r7)
            int r10 = r7.length()
            if (r10 == 0) goto L_0x022f
            java.lang.String r7 = r9.concat(r7)
            goto L_0x0234
        L_0x022f:
            java.lang.String r7 = new java.lang.String
            r7.<init>(r9)
        L_0x0234:
            r10 = r7
            com.fossil.jb3 r7 = r26.k()
            com.fossil.gm3 r7 = r7.c(r15, r10)
            if (r7 == 0) goto L_0x0270
            java.lang.Object r9 = r7.e
            boolean r9 = r9 instanceof java.lang.Long
            if (r9 != 0) goto L_0x0246
            goto L_0x0270
        L_0x0246:
            java.lang.Object r7 = r7.e
            java.lang.Long r7 = (java.lang.Long) r7
            long r7 = r7.longValue()
            com.fossil.gm3 r17 = new com.fossil.gm3
            java.lang.String r9 = r2.c
            com.fossil.oh3 r13 = r1.i
            com.fossil.n92 r13 = r13.zzm()
            long r18 = r13.b()
            long r7 = r7 + r11
            java.lang.Long r13 = java.lang.Long.valueOf(r7)
            r7 = r17
            r8 = r15
            r23 = r5
            r5 = 0
            r6 = 1
            r11 = r18
            r7.<init>(r8, r9, r10, r11, r13)
        L_0x026d:
            r6 = r17
            goto L_0x02d7
        L_0x0270:
            r23 = r5
            r5 = 0
            r6 = 1
            com.fossil.jb3 r7 = r26.k()
            com.fossil.oh3 r9 = r1.i
            com.fossil.ym3 r9 = r9.o()
            com.fossil.yf3<java.lang.Integer> r13 = com.fossil.wb3.E
            int r9 = r9.b(r15, r13)
            int r9 = r9 - r6
            com.fossil.a72.b(r15)
            r7.g()
            r7.q()
            android.database.sqlite.SQLiteDatabase r13 = r7.u()     // Catch:{ SQLiteException -> 0x02a9 }
            java.lang.String r8 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x02a9 }
            r6[r5] = r15     // Catch:{ SQLiteException -> 0x02a9 }
            r16 = 1
            r6[r16] = r15     // Catch:{ SQLiteException -> 0x02a9 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ SQLiteException -> 0x02a9 }
            r16 = 2
            r6[r16] = r9     // Catch:{ SQLiteException -> 0x02a9 }
            r13.execSQL(r8, r6)     // Catch:{ SQLiteException -> 0x02a9 }
            goto L_0x02bc
        L_0x02a9:
            r0 = move-exception
            r6 = r0
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.t()
            java.lang.String r8 = "Error pruning currencies. appId"
            java.lang.Object r9 = com.fossil.jg3.a(r15)
            r7.a(r8, r9, r6)
        L_0x02bc:
            com.fossil.gm3 r17 = new com.fossil.gm3
            java.lang.String r9 = r2.c
            com.fossil.oh3 r6 = r1.i
            com.fossil.n92 r6 = r6.zzm()
            long r18 = r6.b()
            java.lang.Long r13 = java.lang.Long.valueOf(r11)
            r7 = r17
            r8 = r15
            r11 = r18
            r7.<init>(r8, r9, r10, r11, r13)
            goto L_0x026d
        L_0x02d7:
            com.fossil.jb3 r7 = r26.k()
            boolean r7 = r7.a(r6)
            if (r7 != 0) goto L_0x0315
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.t()
            java.lang.String r8 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r9 = com.fossil.jg3.a(r15)
            com.fossil.oh3 r10 = r1.i
            com.fossil.hg3 r10 = r10.w()
            java.lang.String r11 = r6.c
            java.lang.String r10 = r10.c(r11)
            java.lang.Object r6 = r6.e
            r7.a(r8, r9, r10, r6)
            com.fossil.oh3 r6 = r1.i
            com.fossil.jm3 r7 = r6.v()
            r9 = 9
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)
            goto L_0x0315
        L_0x0312:
            r23 = r5
            r5 = 0
        L_0x0315:
            r11 = 1
        L_0x0316:
            if (r11 != 0) goto L_0x032a
            com.fossil.jb3 r2 = r26.k()
            r2.t()
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            return
        L_0x0327:
            r23 = r5
            r5 = 0
        L_0x032a:
            java.lang.String r6 = r2.a
            boolean r6 = com.fossil.jm3.h(r6)
            java.lang.String r7 = r2.a
            boolean r18 = r14.equals(r7)
            boolean r7 = com.fossil.p03.a()
            r19 = 1
            if (r7 == 0) goto L_0x035d
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            java.lang.String r8 = r3.a
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.F0
            boolean r7 = r7.e(r8, r9)
            if (r7 == 0) goto L_0x035d
            com.fossil.oh3 r7 = r1.i
            r7.v()
            com.fossil.tb3 r7 = r2.b
            long r7 = com.fossil.jm3.a(r7)
            long r7 = r7 + r19
            r11 = r7
            goto L_0x035f
        L_0x035d:
            r11 = r19
        L_0x035f:
            com.fossil.jb3 r7 = r26.k()
            long r8 = r26.y()
            r13 = 1
            r16 = 0
            r17 = 0
            r10 = r15
            r14 = r6
            r27 = r15
            r15 = r16
            r16 = r18
            com.fossil.ib3 r7 = r7.a(r8, r10, r11, r13, r14, r15, r16, r17)
            long r8 = r7.b
            com.fossil.yf3<java.lang.Integer> r10 = com.fossil.wb3.k
            r14 = 0
            java.lang.Object r10 = r10.a(r14)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r10 = r10.intValue()
            long r10 = (long) r10
            long r8 = r8 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            r12 = 0
            int r15 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r15 <= 0) goto L_0x03be
            long r8 = r8 % r10
            int r2 = (r8 > r19 ? 1 : (r8 == r19 ? 0 : -1))
            if (r2 != 0) goto L_0x03af
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r4 = com.fossil.jg3.a(r27)
            long r5 = r7.b
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r2.a(r3, r4, r5)
        L_0x03af:
            com.fossil.jb3 r2 = r26.k()
            r2.t()
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            return
        L_0x03be:
            if (r6 == 0) goto L_0x0413
            long r8 = r7.a
            com.fossil.yf3<java.lang.Integer> r15 = com.fossil.wb3.m
            java.lang.Object r15 = r15.a(r14)
            java.lang.Integer r15 = (java.lang.Integer) r15
            int r15 = r15.intValue()
            long r14 = (long) r15
            long r8 = r8 - r14
            int r14 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r14 <= 0) goto L_0x0413
            long r8 = r8 % r10
            int r3 = (r8 > r19 ? 1 : (r8 == r19 ? 0 : -1))
            if (r3 != 0) goto L_0x03f2
            com.fossil.oh3 r3 = r1.i
            com.fossil.jg3 r3 = r3.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r4 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r5 = com.fossil.jg3.a(r27)
            long r6 = r7.a
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r3.a(r4, r5, r6)
        L_0x03f2:
            com.fossil.oh3 r3 = r1.i
            com.fossil.jm3 r7 = r3.v()
            r9 = 16
            java.lang.String r10 = "_ev"
            java.lang.String r11 = r2.a
            r12 = 0
            r8 = r27
            r7.a(r8, r9, r10, r11, r12)
            com.fossil.jb3 r2 = r26.k()
            r2.t()
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            return
        L_0x0413:
            if (r18 == 0) goto L_0x0462
            long r8 = r7.d
            com.fossil.oh3 r10 = r1.i
            com.fossil.ym3 r10 = r10.o()
            java.lang.String r11 = r3.a
            com.fossil.yf3<java.lang.Integer> r14 = com.fossil.wb3.l
            int r10 = r10.b(r11, r14)
            r11 = 1000000(0xf4240, float:1.401298E-39)
            int r10 = java.lang.Math.min(r11, r10)
            int r10 = java.lang.Math.max(r5, r10)
            long r10 = (long) r10
            long r8 = r8 - r10
            int r10 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r10 <= 0) goto L_0x0462
            int r2 = (r8 > r19 ? 1 : (r8 == r19 ? 0 : -1))
            if (r2 != 0) goto L_0x0453
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Too many error events logged. appId, count"
            java.lang.Object r4 = com.fossil.jg3.a(r27)
            long r5 = r7.d
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r2.a(r3, r4, r5)
        L_0x0453:
            com.fossil.jb3 r2 = r26.k()
            r2.t()
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            return
        L_0x0462:
            com.fossil.tb3 r7 = r2.b
            android.os.Bundle r14 = r7.zzb()
            com.fossil.oh3 r7 = r1.i
            com.fossil.jm3 r7 = r7.v()
            java.lang.String r8 = "_o"
            java.lang.String r9 = r2.c
            r7.a(r14, r8, r9)
            com.fossil.oh3 r7 = r1.i
            com.fossil.jm3 r7 = r7.v()
            r15 = r27
            boolean r7 = r7.d(r15)
            java.lang.String r11 = "_r"
            if (r7 == 0) goto L_0x04a1
            com.fossil.oh3 r7 = r1.i
            com.fossil.jm3 r7 = r7.v()
            java.lang.String r8 = "_dbg"
            java.lang.Long r9 = java.lang.Long.valueOf(r19)
            r7.a(r14, r8, r9)
            com.fossil.oh3 r7 = r1.i
            com.fossil.jm3 r7 = r7.v()
            java.lang.Long r8 = java.lang.Long.valueOf(r19)
            r7.a(r14, r11, r8)
        L_0x04a1:
            java.lang.String r7 = "_s"
            java.lang.String r8 = r2.a
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x04c8
            com.fossil.jb3 r7 = r26.k()
            java.lang.String r8 = r3.a
            com.fossil.gm3 r7 = r7.c(r8, r4)
            if (r7 == 0) goto L_0x04c8
            java.lang.Object r8 = r7.e
            boolean r8 = r8 instanceof java.lang.Long
            if (r8 == 0) goto L_0x04c8
            com.fossil.oh3 r8 = r1.i
            com.fossil.jm3 r8 = r8.v()
            java.lang.Object r7 = r7.e
            r8.a(r14, r4, r7)
        L_0x04c8:
            com.fossil.jb3 r4 = r26.k()
            long r7 = r4.c(r15)
            int r4 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r4 <= 0) goto L_0x04eb
            com.fossil.oh3 r4 = r1.i
            com.fossil.jg3 r4 = r4.e()
            com.fossil.mg3 r4 = r4.w()
            java.lang.String r9 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r10 = com.fossil.jg3.a(r15)
            java.lang.Long r7 = java.lang.Long.valueOf(r7)
            r4.a(r9, r10, r7)
        L_0x04eb:
            com.fossil.rb3 r4 = new com.fossil.rb3
            com.fossil.oh3 r8 = r1.i
            java.lang.String r9 = r2.c
            java.lang.String r10 = r2.a
            long r12 = r2.d
            r18 = 0
            r7 = r4
            r2 = r10
            r10 = r15
            r5 = r11
            r11 = r2
            r16 = r14
            r2 = r15
            r25 = 0
            r14 = r18
            r7.<init>(r8, r9, r10, r11, r12, r14, r16)
            com.fossil.jb3 r7 = r26.k()
            java.lang.String r8 = r4.b
            com.fossil.qb3 r7 = r7.a(r2, r8)
            if (r7 != 0) goto L_0x0589
            com.fossil.jb3 r7 = r26.k()
            long r7 = r7.h(r2)
            com.fossil.oh3 r9 = r1.i
            com.fossil.ym3 r9 = r9.o()
            int r9 = r9.b(r2)
            long r9 = (long) r9
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 < 0) goto L_0x056f
            if (r6 == 0) goto L_0x056f
            com.fossil.oh3 r3 = r1.i
            com.fossil.jg3 r3 = r3.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r5 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r6 = com.fossil.jg3.a(r2)
            com.fossil.oh3 r7 = r1.i
            com.fossil.hg3 r7 = r7.w()
            java.lang.String r4 = r4.b
            java.lang.String r4 = r7.a(r4)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            int r7 = r7.b(r2)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r3.a(r5, r6, r4, r7)
            com.fossil.oh3 r3 = r1.i
            com.fossil.jm3 r7 = r3.v()
            r9 = 8
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r2
            r7.a(r8, r9, r10, r11, r12)
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            return
        L_0x056f:
            com.fossil.qb3 r6 = new com.fossil.qb3
            java.lang.String r9 = r4.b
            r10 = 0
            r12 = 0
            long r14 = r4.d
            r16 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r7 = r6
            r8 = r2
            r7.<init>(r8, r9, r10, r12, r14, r16, r18, r19, r20, r21)
            goto L_0x0597
        L_0x0589:
            com.fossil.oh3 r2 = r1.i
            long r8 = r7.f
            com.fossil.rb3 r4 = r4.a(r2, r8)
            long r8 = r4.d
            com.fossil.qb3 r6 = r7.a(r8)
        L_0x0597:
            com.fossil.jb3 r2 = r26.k()
            r2.a(r6)
            r26.x()
            r26.q()
            com.fossil.a72.a(r4)
            com.fossil.a72.a(r28)
            java.lang.String r2 = r4.a
            com.fossil.a72.b(r2)
            java.lang.String r2 = r4.a
            java.lang.String r6 = r3.a
            boolean r2 = r2.equals(r6)
            com.fossil.a72.a(r2)
            com.fossil.vp2$a r2 = com.fossil.vp2.z0()
            r6 = 1
            r2.a(r6)
            java.lang.String r7 = "android"
            r2.a(r7)
            java.lang.String r7 = r3.a
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x05d4
            java.lang.String r7 = r3.a
            r2.f(r7)
        L_0x05d4:
            java.lang.String r7 = r3.d
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x05e1
            java.lang.String r7 = r3.d
            r2.e(r7)
        L_0x05e1:
            java.lang.String r7 = r3.c
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x05ee
            java.lang.String r7 = r3.c
            r2.g(r7)
        L_0x05ee:
            long r7 = r3.j
            r9 = -2147483648(0xffffffff80000000, double:NaN)
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 == 0) goto L_0x05fd
            long r7 = r3.j
            int r8 = (int) r7
            r2.h(r8)
        L_0x05fd:
            long r7 = r3.e
            r2.f(r7)
            java.lang.String r7 = r3.b
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x060f
            java.lang.String r7 = r3.b
            r2.k(r7)
        L_0x060f:
            boolean r7 = com.fossil.f23.a()
            if (r7 == 0) goto L_0x065e
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            java.lang.String r8 = r3.a
            com.fossil.yf3<java.lang.Boolean> r9 = com.fossil.wb3.o0
            boolean r7 = r7.e(r8, r9)
            if (r7 == 0) goto L_0x065e
            java.lang.String r7 = r2.w()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x063c
            java.lang.String r7 = r3.A
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x063c
            java.lang.String r7 = r3.A
            r2.p(r7)
        L_0x063c:
            java.lang.String r7 = r2.w()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0675
            java.lang.String r7 = r2.z()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0675
            java.lang.String r7 = r3.w
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x0675
            java.lang.String r7 = r3.w
            r2.o(r7)
            goto L_0x0675
        L_0x065e:
            java.lang.String r7 = r2.w()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0675
            java.lang.String r7 = r3.w
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x0675
            java.lang.String r7 = r3.w
            r2.o(r7)
        L_0x0675:
            long r7 = r3.f
            r9 = 0
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 == 0) goto L_0x0682
            long r7 = r3.f
            r2.h(r7)
        L_0x0682:
            long r7 = r3.y
            r2.k(r7)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            java.lang.String r8 = r3.a
            com.fossil.yf3<java.lang.Boolean> r11 = com.fossil.wb3.X
            boolean r7 = r7.e(r8, r11)
            if (r7 == 0) goto L_0x06a4
            com.fossil.fm3 r7 = r26.n()
            java.util.List r7 = r7.t()
            if (r7 == 0) goto L_0x06a4
            r2.c(r7)
        L_0x06a4:
            com.fossil.oh3 r7 = r1.i
            com.fossil.wg3 r7 = r7.p()
            java.lang.String r8 = r3.a
            android.util.Pair r7 = r7.a(r8)
            if (r7 == 0) goto L_0x06d7
            java.lang.Object r8 = r7.first
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x06d7
            boolean r8 = r3.t
            if (r8 == 0) goto L_0x0739
            java.lang.Object r8 = r7.first
            java.lang.String r8 = (java.lang.String) r8
            r2.h(r8)
            java.lang.Object r8 = r7.second
            if (r8 == 0) goto L_0x0739
            java.lang.Object r7 = r7.second
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            r2.a(r7)
            goto L_0x0739
        L_0x06d7:
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            com.fossil.oh3 r8 = r1.i
            android.content.Context r8 = r8.f()
            boolean r7 = r7.a(r8)
            if (r7 != 0) goto L_0x0739
            boolean r7 = r3.u
            if (r7 == 0) goto L_0x0739
            com.fossil.oh3 r7 = r1.i
            android.content.Context r7 = r7.f()
            android.content.ContentResolver r7 = r7.getContentResolver()
            java.lang.String r8 = "android_id"
            java.lang.String r7 = android.provider.Settings.Secure.getString(r7, r8)
            if (r7 != 0) goto L_0x0719
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.w()
            java.lang.String r8 = "null secure ID. appId"
            java.lang.String r11 = r2.u()
            java.lang.Object r11 = com.fossil.jg3.a(r11)
            r7.a(r8, r11)
            java.lang.String r7 = "null"
            goto L_0x0736
        L_0x0719:
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L_0x0736
            com.fossil.oh3 r8 = r1.i
            com.fossil.jg3 r8 = r8.e()
            com.fossil.mg3 r8 = r8.w()
            java.lang.String r11 = "empty secure ID. appId"
            java.lang.String r12 = r2.u()
            java.lang.Object r12 = com.fossil.jg3.a(r12)
            r8.a(r11, r12)
        L_0x0736:
            r2.m(r7)
        L_0x0739:
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            r7.n()
            java.lang.String r7 = android.os.Build.MODEL
            r2.c(r7)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            r7.n()
            java.lang.String r7 = android.os.Build.VERSION.RELEASE
            r2.b(r7)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            long r7 = r7.s()
            int r8 = (int) r7
            r2.f(r8)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            java.lang.String r7 = r7.t()
            r2.d(r7)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            com.fossil.yf3<java.lang.Boolean> r8 = com.fossil.wb3.M0
            boolean r7 = r7.a(r8)
            if (r7 != 0) goto L_0x0783
            long r7 = r3.q
            r2.j(r7)
        L_0x0783:
            com.fossil.oh3 r7 = r1.i
            boolean r7 = r7.g()
            if (r7 == 0) goto L_0x0799
            r2.u()
            boolean r7 = android.text.TextUtils.isEmpty(r25)
            if (r7 != 0) goto L_0x0799
            r7 = r25
            r2.n(r7)
        L_0x0799:
            com.fossil.jb3 r7 = r26.k()
            java.lang.String r8 = r3.a
            com.fossil.kg3 r7 = r7.b(r8)
            if (r7 != 0) goto L_0x081a
            com.fossil.kg3 r7 = new com.fossil.kg3
            com.fossil.oh3 r8 = r1.i
            java.lang.String r11 = r3.a
            r7.<init>(r8, r11)
            com.fossil.oh3 r8 = r1.i
            com.fossil.jm3 r8 = r8.v()
            java.lang.String r8 = r8.v()
            r7.a(r8)
            java.lang.String r8 = r3.p
            r7.f(r8)
            java.lang.String r8 = r3.b
            r7.b(r8)
            com.fossil.oh3 r8 = r1.i
            com.fossil.wg3 r8 = r8.p()
            java.lang.String r11 = r3.a
            java.lang.String r8 = r8.b(r11)
            r7.e(r8)
            r7.g(r9)
            r7.a(r9)
            r7.b(r9)
            java.lang.String r8 = r3.c
            r7.g(r8)
            long r11 = r3.j
            r7.c(r11)
            java.lang.String r8 = r3.d
            r7.h(r8)
            long r11 = r3.e
            r7.d(r11)
            long r11 = r3.f
            r7.e(r11)
            boolean r8 = r3.h
            r7.a(r8)
            com.fossil.oh3 r8 = r1.i
            com.fossil.ym3 r8 = r8.o()
            com.fossil.yf3<java.lang.Boolean> r11 = com.fossil.wb3.M0
            boolean r8 = r8.a(r11)
            if (r8 != 0) goto L_0x080e
            long r11 = r3.q
            r7.p(r11)
        L_0x080e:
            long r11 = r3.y
            r7.f(r11)
            com.fossil.jb3 r8 = r26.k()
            r8.a(r7)
        L_0x081a:
            java.lang.String r8 = r7.m()
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x082b
            java.lang.String r8 = r7.m()
            r2.i(r8)
        L_0x082b:
            java.lang.String r8 = r7.r()
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x083c
            java.lang.String r7 = r7.r()
            r2.l(r7)
        L_0x083c:
            com.fossil.jb3 r7 = r26.k()
            java.lang.String r3 = r3.a
            java.util.List r3 = r7.a(r3)
            r11 = 0
        L_0x0847:
            int r7 = r3.size()
            if (r11 >= r7) goto L_0x087c
            com.fossil.zp2$a r7 = com.fossil.zp2.A()
            java.lang.Object r8 = r3.get(r11)
            com.fossil.gm3 r8 = (com.fossil.gm3) r8
            java.lang.String r8 = r8.c
            r7.a(r8)
            java.lang.Object r8 = r3.get(r11)
            com.fossil.gm3 r8 = (com.fossil.gm3) r8
            long r12 = r8.d
            r7.a(r12)
            com.fossil.fm3 r8 = r26.n()
            java.lang.Object r12 = r3.get(r11)
            com.fossil.gm3 r12 = (com.fossil.gm3) r12
            java.lang.Object r12 = r12.e
            r8.a(r7, r12)
            r2.a(r7)
            int r11 = r11 + 1
            goto L_0x0847
        L_0x087c:
            com.fossil.jb3 r3 = r26.k()     // Catch:{ IOException -> 0x08f1 }
            com.fossil.jx2 r7 = r2.g()     // Catch:{ IOException -> 0x08f1 }
            com.fossil.bw2 r7 = (com.fossil.bw2) r7     // Catch:{ IOException -> 0x08f1 }
            com.fossil.vp2 r7 = (com.fossil.vp2) r7     // Catch:{ IOException -> 0x08f1 }
            long r2 = r3.a(r7)     // Catch:{ IOException -> 0x08f1 }
            com.fossil.jb3 r7 = r26.k()
            com.fossil.tb3 r8 = r4.f
            if (r8 == 0) goto L_0x08e7
            com.fossil.tb3 r8 = r4.f
            java.util.Iterator r8 = r8.iterator()
        L_0x089a:
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x08ae
            java.lang.Object r11 = r8.next()
            java.lang.String r11 = (java.lang.String) r11
            boolean r11 = r5.equals(r11)
            if (r11 == 0) goto L_0x089a
        L_0x08ac:
            r11 = 1
            goto L_0x08e8
        L_0x08ae:
            com.fossil.ih3 r5 = r26.i()
            java.lang.String r8 = r4.a
            java.lang.String r11 = r4.b
            boolean r5 = r5.c(r8, r11)
            com.fossil.jb3 r11 = r26.k()
            long r12 = r26.y()
            java.lang.String r14 = r4.a
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            com.fossil.ib3 r8 = r11.a(r12, r14, r15, r16, r17, r18, r19)
            if (r5 == 0) goto L_0x08e7
            long r11 = r8.e
            com.fossil.oh3 r5 = r1.i
            com.fossil.ym3 r5 = r5.o()
            java.lang.String r8 = r4.a
            int r5 = r5.c(r8)
            long r13 = (long) r5
            int r5 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r5 >= 0) goto L_0x08e7
            goto L_0x08ac
        L_0x08e7:
            r11 = 0
        L_0x08e8:
            boolean r2 = r7.a(r4, r2, r11)
            if (r2 == 0) goto L_0x090a
            r1.l = r9
            goto L_0x090a
        L_0x08f1:
            r0 = move-exception
            r3 = r0
            com.fossil.oh3 r4 = r1.i
            com.fossil.jg3 r4 = r4.e()
            com.fossil.mg3 r4 = r4.t()
            java.lang.String r5 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r2 = r2.u()
            java.lang.Object r2 = com.fossil.jg3.a(r2)
            r4.a(r5, r2, r3)
        L_0x090a:
            com.fossil.jb3 r2 = r26.k()
            r2.t()
            com.fossil.jb3 r2 = r26.k()
            r2.z()
            r26.A()
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.B()
            long r3 = java.lang.System.nanoTime()
            long r3 = r3 - r23
            r5 = 500000(0x7a120, double:2.47033E-318)
            long r3 = r3 + r5
            r5 = 1000000(0xf4240, double:4.940656E-318)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.lang.String r4 = "Background event processing time, ms"
            r2.a(r4, r3)
            return
        L_0x093d:
            r0 = move-exception
            r2 = r0
            com.fossil.jb3 r3 = r26.k()
            r3.z()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.c(com.fossil.ub3, com.fossil.nm3):void");
    }

    @DexIgnore
    public final boolean e(nm3 nm3) {
        return (!f23.a() || !this.i.o().e(nm3.a, wb3.o0)) ? !TextUtils.isEmpty(nm3.b) || !TextUtils.isEmpty(nm3.w) : !TextUtils.isEmpty(nm3.b) || !TextUtils.isEmpty(nm3.A) || !TextUtils.isEmpty(nm3.w);
    }

    @DexIgnore
    public final void b(ub3 ub3, nm3 nm3) {
        if (g23.a() && this.i.o().a(wb3.O0)) {
            og3 a2 = og3.a(ub3);
            this.i.v().a(a2.d, k().i(nm3.a));
            this.i.v().a(a2, this.i.o().a(nm3.a));
            ub3 = a2.a();
        }
        a(ub3, nm3);
    }

    @DexIgnore
    public final void a(dm3 dm3) {
        this.i.c().g();
        jb3 jb3 = new jb3(this);
        jb3.r();
        this.c = jb3;
        this.i.o().a(this.a);
        om3 om3 = new om3(this);
        om3.r();
        this.f = om3;
        uj3 uj3 = new uj3(this);
        uj3.r();
        this.h = uj3;
        tl3 tl3 = new tl3(this);
        tl3.r();
        this.e = tl3;
        this.d = new ug3(this);
        if (this.n != this.o) {
            this.i.e().t().a("Not all upload components initialized", Integer.valueOf(this.n), Integer.valueOf(this.o));
        }
        this.j = true;
    }

    @DexIgnore
    public final String d(nm3 nm3) {
        try {
            return (String) this.i.c().a(new bm3(this, nm3)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.i.e().t().a("Failed to get app instance id. appId", jg3.a(nm3.a), e2);
            return null;
        }
    }

    @DexIgnore
    public final void b(rp2.a aVar, rp2.a aVar2) {
        a72.a("_e".equals(aVar.q()));
        n();
        tp2 b2 = fm3.b((rp2) ((bw2) aVar.g()), "_et");
        if (b2.s() && b2.t() > 0) {
            long t2 = b2.t();
            n();
            tp2 b3 = fm3.b((rp2) ((bw2) aVar2.g()), "_et");
            if (b3 != null && b3.t() > 0) {
                t2 += b3.t();
            }
            n().a(aVar2, "_et", Long.valueOf(t2));
            n().a(aVar, "_fr", (Object) 1L);
        }
    }

    @DexIgnore
    public final Boolean b(kg3 kg3) {
        try {
            if (kg3.v() != -2147483648L) {
                if (kg3.v() == ((long) ja2.b(this.i.f()).b(kg3.l(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = ja2.b(this.i.f()).b(kg3.l(), 0).versionName;
                if (kg3.u() != null && kg3.u().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public final void a() {
        this.i.c().g();
        k().B();
        if (this.i.p().e.a() == 0) {
            this.i.p().e.a(this.i.zzm().b());
        }
        A();
    }

    @DexIgnore
    public final void b(em3 em3, nm3 nm3) {
        x();
        q();
        if (e(nm3)) {
            if (!nm3.h) {
                c(nm3);
            } else if (!"_npa".equals(em3.b) || nm3.x == null) {
                this.i.e().A().a("Removing user property", this.i.w().c(em3.b));
                k().y();
                try {
                    c(nm3);
                    k().b(nm3.a, em3.b);
                    k().t();
                    this.i.e().A().a("User property removed", this.i.w().c(em3.b));
                } finally {
                    k().z();
                }
            } else {
                this.i.e().A().a("Falling back to manifest metadata value for ad personalization");
                a(new em3("_npa", this.i.zzm().b(), Long.valueOf(nm3.x.booleanValue() ? 1 : 0), "auto"), nm3);
            }
        }
    }

    @DexIgnore
    public final void a(ub3 ub3, String str) {
        kg3 b2 = k().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.u())) {
            this.i.e().A().a("No app data available; dropping event", str);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(ub3.a)) {
                this.i.e().w().a("Could not find package. appId", jg3.a(str));
            }
        } else if (!b3.booleanValue()) {
            this.i.e().t().a("App version does not match; dropping event. appId", jg3.a(str));
            return;
        }
        b(ub3, new nm3(str, b2.n(), b2.u(), b2.v(), b2.w(), b2.x(), b2.y(), (String) null, b2.A(), false, b2.r(), b2.f(), 0L, 0, b2.g(), b2.h(), false, b2.o(), b2.i(), b2.z(), b2.j(), (!f23.a() || !this.i.o().e(b2.l(), wb3.o0)) ? null : b2.p()));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:153:0x048c  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011b A[Catch:{ all -> 0x04b8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0205  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x023b  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x024e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.fossil.nm3 r22) {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            java.lang.String r3 = "_sysu"
            java.lang.String r4 = "_sys"
            java.lang.String r5 = "_pfo"
            java.lang.String r6 = "_uwa"
            java.lang.String r0 = "app_id=?"
            r21.x()
            r21.q()
            com.fossil.a72.a(r22)
            java.lang.String r7 = r2.a
            com.fossil.a72.b(r7)
            boolean r7 = r21.e(r22)
            if (r7 != 0) goto L_0x0023
            return
        L_0x0023:
            com.fossil.jb3 r7 = r21.k()
            java.lang.String r8 = r2.a
            com.fossil.kg3 r7 = r7.b(r8)
            r8 = 0
            if (r7 == 0) goto L_0x0056
            java.lang.String r10 = r7.n()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 == 0) goto L_0x0056
            java.lang.String r10 = r2.b
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x0056
            r7.h(r8)
            com.fossil.jb3 r10 = r21.k()
            r10.a(r7)
            com.fossil.ih3 r7 = r21.i()
            java.lang.String r10 = r2.a
            r7.d(r10)
        L_0x0056:
            boolean r7 = r2.h
            if (r7 != 0) goto L_0x005e
            r21.c(r22)
            return
        L_0x005e:
            long r10 = r2.r
            int r7 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x006e
            com.fossil.oh3 r7 = r1.i
            com.fossil.n92 r7 = r7.zzm()
            long r10 = r7.b()
        L_0x006e:
            com.fossil.oh3 r7 = r1.i
            com.fossil.ob3 r7 = r7.F()
            r7.v()
            int r7 = r2.s
            r15 = 1
            if (r7 == 0) goto L_0x0098
            if (r7 == r15) goto L_0x0098
            com.fossil.oh3 r12 = r1.i
            com.fossil.jg3 r12 = r12.e()
            com.fossil.mg3 r12 = r12.w()
            java.lang.String r13 = r2.a
            java.lang.Object r13 = com.fossil.jg3.a(r13)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r14 = "Incorrect app type, assuming installed app. appId, appType"
            r12.a(r14, r13, r7)
            r7 = 0
        L_0x0098:
            com.fossil.jb3 r12 = r21.k()
            r12.y()
            com.fossil.jb3 r12 = r21.k()     // Catch:{ all -> 0x04b8 }
            java.lang.String r13 = r2.a     // Catch:{ all -> 0x04b8 }
            java.lang.String r14 = "_npa"
            com.fossil.gm3 r14 = r12.c(r13, r14)     // Catch:{ all -> 0x04b8 }
            if (r14 == 0) goto L_0x00bc
            java.lang.String r12 = "auto"
            java.lang.String r13 = r14.b     // Catch:{ all -> 0x04b8 }
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x04b8 }
            if (r12 == 0) goto L_0x00b8
            goto L_0x00bc
        L_0x00b8:
            r18 = r3
            r3 = 1
            goto L_0x010f
        L_0x00bc:
            java.lang.Boolean r12 = r2.x     // Catch:{ all -> 0x04b8 }
            if (r12 == 0) goto L_0x00f9
            com.fossil.em3 r13 = new com.fossil.em3     // Catch:{ all -> 0x04b8 }
            java.lang.String r18 = "_npa"
            java.lang.Boolean r12 = r2.x     // Catch:{ all -> 0x04b8 }
            boolean r12 = r12.booleanValue()     // Catch:{ all -> 0x04b8 }
            if (r12 == 0) goto L_0x00cf
            r19 = 1
            goto L_0x00d1
        L_0x00cf:
            r19 = r8
        L_0x00d1:
            java.lang.Long r19 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x04b8 }
            java.lang.String r20 = "auto"
            r8 = 1
            r12 = r13
            r8 = r13
            r13 = r18
            r18 = r3
            r9 = r14
            r3 = 1
            r14 = r10
            r16 = r19
            r17 = r20
            r12.<init>(r13, r14, r16, r17)     // Catch:{ all -> 0x04b8 }
            if (r9 == 0) goto L_0x00f5
            java.lang.Object r9 = r9.e     // Catch:{ all -> 0x04b8 }
            java.lang.Long r12 = r8.d     // Catch:{ all -> 0x04b8 }
            boolean r9 = r9.equals(r12)     // Catch:{ all -> 0x04b8 }
            if (r9 != 0) goto L_0x010f
        L_0x00f5:
            r1.a(r8, r2)     // Catch:{ all -> 0x04b8 }
            goto L_0x010f
        L_0x00f9:
            r18 = r3
            r9 = r14
            r3 = 1
            if (r9 == 0) goto L_0x010f
            com.fossil.em3 r8 = new com.fossil.em3     // Catch:{ all -> 0x04b8 }
            java.lang.String r13 = "_npa"
            r16 = 0
            java.lang.String r17 = "auto"
            r12 = r8
            r14 = r10
            r12.<init>(r13, r14, r16, r17)     // Catch:{ all -> 0x04b8 }
            r1.b(r8, r2)     // Catch:{ all -> 0x04b8 }
        L_0x010f:
            com.fossil.jb3 r8 = r21.k()     // Catch:{ all -> 0x04b8 }
            java.lang.String r9 = r2.a     // Catch:{ all -> 0x04b8 }
            com.fossil.kg3 r8 = r8.b(r9)     // Catch:{ all -> 0x04b8 }
            if (r8 == 0) goto L_0x01c9
            com.fossil.oh3 r12 = r1.i     // Catch:{ all -> 0x04b8 }
            r12.v()     // Catch:{ all -> 0x04b8 }
            java.lang.String r12 = r2.b     // Catch:{ all -> 0x04b8 }
            java.lang.String r13 = r8.n()     // Catch:{ all -> 0x04b8 }
            java.lang.String r14 = r2.w     // Catch:{ all -> 0x04b8 }
            java.lang.String r15 = r8.o()     // Catch:{ all -> 0x04b8 }
            boolean r12 = com.fossil.jm3.a(r12, r13, r14, r15)     // Catch:{ all -> 0x04b8 }
            if (r12 == 0) goto L_0x01c9
            com.fossil.oh3 r12 = r1.i     // Catch:{ all -> 0x04b8 }
            com.fossil.jg3 r12 = r12.e()     // Catch:{ all -> 0x04b8 }
            com.fossil.mg3 r12 = r12.w()     // Catch:{ all -> 0x04b8 }
            java.lang.String r13 = "New GMP App Id passed in. Removing cached database data. appId"
            java.lang.String r14 = r8.l()     // Catch:{ all -> 0x04b8 }
            java.lang.Object r14 = com.fossil.jg3.a(r14)     // Catch:{ all -> 0x04b8 }
            r12.a(r13, r14)     // Catch:{ all -> 0x04b8 }
            com.fossil.jb3 r12 = r21.k()     // Catch:{ all -> 0x04b8 }
            java.lang.String r8 = r8.l()     // Catch:{ all -> 0x04b8 }
            r12.q()     // Catch:{ all -> 0x04b8 }
            r12.g()     // Catch:{ all -> 0x04b8 }
            com.fossil.a72.b(r8)     // Catch:{ all -> 0x04b8 }
            android.database.sqlite.SQLiteDatabase r13 = r12.u()     // Catch:{ SQLiteException -> 0x01b6 }
            java.lang.String[] r14 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x01b6 }
            r15 = 0
            r14[r15] = r8     // Catch:{ SQLiteException -> 0x01b6 }
            java.lang.String r9 = "events"
            int r9 = r13.delete(r9, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "user_attributes"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "conditional_properties"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "apps"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "raw_events"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "raw_events_metadata"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "event_filters"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "property_filters"
            int r15 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r15
            java.lang.String r15 = "audience_filter_values"
            int r0 = r13.delete(r15, r0, r14)     // Catch:{ SQLiteException -> 0x01b6 }
            int r9 = r9 + r0
            if (r9 <= 0) goto L_0x01c8
            com.fossil.jg3 r0 = r12.e()     // Catch:{ SQLiteException -> 0x01b6 }
            com.fossil.mg3 r0 = r0.B()     // Catch:{ SQLiteException -> 0x01b6 }
            java.lang.String r13 = "Deleted application data. app, records"
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ SQLiteException -> 0x01b6 }
            r0.a(r13, r8, r9)     // Catch:{ SQLiteException -> 0x01b6 }
            goto L_0x01c8
        L_0x01b6:
            r0 = move-exception
            com.fossil.jg3 r9 = r12.e()
            com.fossil.mg3 r9 = r9.t()
            java.lang.String r12 = "Error deleting application data. appId, error"
            java.lang.Object r8 = com.fossil.jg3.a(r8)
            r9.a(r12, r8, r0)
        L_0x01c8:
            r8 = 0
        L_0x01c9:
            if (r8 == 0) goto L_0x0228
            long r12 = r8.v()
            r14 = -2147483648(0xffffffff80000000, double:NaN)
            int r0 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r0 == 0) goto L_0x01e3
            long r12 = r8.v()
            r9 = r4
            long r3 = r2.j
            int r0 = (r12 > r3 ? 1 : (r12 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x01e4
            r0 = 1
            goto L_0x01e5
        L_0x01e3:
            r9 = r4
        L_0x01e4:
            r0 = 0
        L_0x01e5:
            long r3 = r8.v()
            int r12 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r12 != 0) goto L_0x0201
            java.lang.String r3 = r8.u()
            if (r3 == 0) goto L_0x0201
            java.lang.String r3 = r8.u()
            java.lang.String r4 = r2.c
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0201
            r14 = 1
            goto L_0x0202
        L_0x0201:
            r14 = 0
        L_0x0202:
            r0 = r0 | r14
            if (r0 == 0) goto L_0x0229
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            java.lang.String r3 = "_pv"
            java.lang.String r4 = r8.u()
            r0.putString(r3, r4)
            com.fossil.ub3 r3 = new com.fossil.ub3
            java.lang.String r13 = "_au"
            com.fossil.tb3 r14 = new com.fossil.tb3
            r14.<init>(r0)
            java.lang.String r15 = "auto"
            r12 = r3
            r16 = r10
            r12.<init>(r13, r14, r15, r16)
            r1.a(r3, r2)
            goto L_0x0229
        L_0x0228:
            r9 = r4
        L_0x0229:
            r21.c(r22)
            if (r7 != 0) goto L_0x023b
            com.fossil.jb3 r0 = r21.k()
            java.lang.String r3 = r2.a
            java.lang.String r4 = "_f"
            com.fossil.qb3 r0 = r0.a(r3, r4)
            goto L_0x024c
        L_0x023b:
            r3 = 1
            if (r7 != r3) goto L_0x024b
            com.fossil.jb3 r0 = r21.k()
            java.lang.String r3 = r2.a
            java.lang.String r4 = "_v"
            com.fossil.qb3 r0 = r0.a(r3, r4)
            goto L_0x024c
        L_0x024b:
            r0 = 0
        L_0x024c:
            if (r0 != 0) goto L_0x048c
            r3 = 3600000(0x36ee80, double:1.7786363E-317)
            long r12 = r10 / r3
            r14 = 1
            long r12 = r12 + r14
            long r12 = r12 * r3
            java.lang.String r0 = "_dac"
            java.lang.String r3 = "_r"
            java.lang.String r4 = "_c"
            java.lang.String r8 = "_et"
            if (r7 != 0) goto L_0x03ea
            com.fossil.em3 r7 = new com.fossil.em3
            java.lang.String r14 = "_fot"
            java.lang.Long r16 = java.lang.Long.valueOf(r12)
            java.lang.String r17 = "auto"
            r12 = r7
            r13 = r14
            r14 = r10
            r12.<init>(r13, r14, r16, r17)
            r1.a(r7, r2)
            com.fossil.oh3 r7 = r1.i
            com.fossil.ym3 r7 = r7.o()
            java.lang.String r12 = r2.b
            com.fossil.yf3<java.lang.Boolean> r13 = com.fossil.wb3.S
            boolean r7 = r7.e(r12, r13)
            if (r7 == 0) goto L_0x0293
            r21.x()
            com.fossil.oh3 r7 = r1.i
            com.fossil.bh3 r7 = r7.s()
            java.lang.String r12 = r2.a
            r7.a(r12)
        L_0x0293:
            r21.x()
            r21.q()
            android.os.Bundle r7 = new android.os.Bundle
            r7.<init>()
            r12 = 1
            r7.putLong(r4, r12)
            r7.putLong(r3, r12)
            r3 = 0
            r7.putLong(r6, r3)
            r7.putLong(r5, r3)
            r7.putLong(r9, r3)
            r14 = r18
            r7.putLong(r14, r3)
            com.fossil.oh3 r3 = r1.i
            com.fossil.ym3 r3 = r3.o()
            java.lang.String r4 = r2.a
            com.fossil.yf3<java.lang.Boolean> r12 = com.fossil.wb3.U
            boolean r3 = r3.e(r4, r12)
            if (r3 == 0) goto L_0x02cc
            r3 = 1
            r7.putLong(r8, r3)
            goto L_0x02ce
        L_0x02cc:
            r3 = 1
        L_0x02ce:
            boolean r12 = r2.v
            if (r12 == 0) goto L_0x02d5
            r7.putLong(r0, r3)
        L_0x02d5:
            com.fossil.jb3 r0 = r21.k()
            java.lang.String r3 = r2.a
            com.fossil.a72.b(r3)
            r0.g()
            r0.q()
            java.lang.String r4 = "first_open_count"
            long r3 = r0.h(r3, r4)
            com.fossil.oh3 r0 = r1.i
            android.content.Context r0 = r0.f()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            if (r0 != 0) goto L_0x030f
            com.fossil.oh3 r0 = r1.i
            com.fossil.jg3 r0 = r0.e()
            com.fossil.mg3 r0 = r0.t()
            java.lang.String r6 = "PackageManager is null, first open report might be inaccurate. appId"
            java.lang.String r9 = r2.a
            java.lang.Object r9 = com.fossil.jg3.a(r9)
            r0.a(r6, r9)
        L_0x030b:
            r12 = 0
            goto L_0x03ce
        L_0x030f:
            com.fossil.oh3 r0 = r1.i     // Catch:{ NameNotFoundException -> 0x0321 }
            android.content.Context r0 = r0.f()     // Catch:{ NameNotFoundException -> 0x0321 }
            com.fossil.ia2 r0 = com.fossil.ja2.b(r0)     // Catch:{ NameNotFoundException -> 0x0321 }
            java.lang.String r12 = r2.a     // Catch:{ NameNotFoundException -> 0x0321 }
            r13 = 0
            android.content.pm.PackageInfo r0 = r0.b(r12, r13)     // Catch:{ NameNotFoundException -> 0x0321 }
            goto L_0x0338
        L_0x0321:
            r0 = move-exception
            com.fossil.oh3 r12 = r1.i
            com.fossil.jg3 r12 = r12.e()
            com.fossil.mg3 r12 = r12.t()
            java.lang.String r13 = "Package info is null, first open report might be inaccurate. appId"
            java.lang.String r15 = r2.a
            java.lang.Object r15 = com.fossil.jg3.a(r15)
            r12.a(r13, r15, r0)
            r0 = 0
        L_0x0338:
            if (r0 == 0) goto L_0x038a
            long r12 = r0.firstInstallTime
            r15 = 0
            int r17 = (r12 > r15 ? 1 : (r12 == r15 ? 0 : -1))
            if (r17 == 0) goto L_0x038a
            long r12 = r0.firstInstallTime
            r18 = r14
            long r14 = r0.lastUpdateTime
            int r0 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r0 == 0) goto L_0x036d
            com.fossil.oh3 r0 = r1.i
            com.fossil.ym3 r0 = r0.o()
            com.fossil.yf3<java.lang.Boolean> r12 = com.fossil.wb3.t0
            boolean r0 = r0.a(r12)
            if (r0 == 0) goto L_0x0366
            r12 = 0
            int r0 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x036b
            r12 = 1
            r7.putLong(r6, r12)
            goto L_0x036b
        L_0x0366:
            r12 = 1
            r7.putLong(r6, r12)
        L_0x036b:
            r14 = 0
            goto L_0x036e
        L_0x036d:
            r14 = 1
        L_0x036e:
            com.fossil.em3 r0 = new com.fossil.em3
            java.lang.String r13 = "_fi"
            if (r14 == 0) goto L_0x0377
            r14 = 1
            goto L_0x0379
        L_0x0377:
            r14 = 0
        L_0x0379:
            java.lang.Long r16 = java.lang.Long.valueOf(r14)
            java.lang.String r17 = "auto"
            r12 = r0
            r6 = r18
            r14 = r10
            r12.<init>(r13, r14, r16, r17)
            r1.a(r0, r2)
            goto L_0x038b
        L_0x038a:
            r6 = r14
        L_0x038b:
            com.fossil.oh3 r0 = r1.i     // Catch:{ NameNotFoundException -> 0x039d }
            android.content.Context r0 = r0.f()     // Catch:{ NameNotFoundException -> 0x039d }
            com.fossil.ia2 r0 = com.fossil.ja2.b(r0)     // Catch:{ NameNotFoundException -> 0x039d }
            java.lang.String r12 = r2.a     // Catch:{ NameNotFoundException -> 0x039d }
            r13 = 0
            android.content.pm.ApplicationInfo r0 = r0.a(r12, r13)     // Catch:{ NameNotFoundException -> 0x039d }
            goto L_0x03b4
        L_0x039d:
            r0 = move-exception
            com.fossil.oh3 r12 = r1.i
            com.fossil.jg3 r12 = r12.e()
            com.fossil.mg3 r12 = r12.t()
            java.lang.String r13 = "Application info is null, first open report might be inaccurate. appId"
            java.lang.String r14 = r2.a
            java.lang.Object r14 = com.fossil.jg3.a(r14)
            r12.a(r13, r14, r0)
            r0 = 0
        L_0x03b4:
            if (r0 == 0) goto L_0x030b
            int r12 = r0.flags
            r13 = 1
            r12 = r12 & r13
            if (r12 == 0) goto L_0x03c1
            r12 = 1
            r7.putLong(r9, r12)
        L_0x03c1:
            int r0 = r0.flags
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x030b
            r12 = 1
            r7.putLong(r6, r12)
            goto L_0x030b
        L_0x03ce:
            int r0 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r0 < 0) goto L_0x03d5
            r7.putLong(r5, r3)
        L_0x03d5:
            com.fossil.ub3 r0 = new com.fossil.ub3
            java.lang.String r13 = "_f"
            com.fossil.tb3 r14 = new com.fossil.tb3
            r14.<init>(r7)
            java.lang.String r15 = "auto"
            r12 = r0
            r16 = r10
            r12.<init>(r13, r14, r15, r16)
            r1.b(r0, r2)
            goto L_0x0446
        L_0x03ea:
            r5 = 1
            if (r7 != r5) goto L_0x0446
            com.fossil.em3 r5 = new com.fossil.em3
            java.lang.String r6 = "_fvt"
            java.lang.Long r16 = java.lang.Long.valueOf(r12)
            java.lang.String r17 = "auto"
            r12 = r5
            r13 = r6
            r14 = r10
            r12.<init>(r13, r14, r16, r17)
            r1.a(r5, r2)
            r21.x()
            r21.q()
            android.os.Bundle r5 = new android.os.Bundle
            r5.<init>()
            r6 = 1
            r5.putLong(r4, r6)
            r5.putLong(r3, r6)
            com.fossil.oh3 r3 = r1.i
            com.fossil.ym3 r3 = r3.o()
            java.lang.String r4 = r2.a
            com.fossil.yf3<java.lang.Boolean> r6 = com.fossil.wb3.U
            boolean r3 = r3.e(r4, r6)
            if (r3 == 0) goto L_0x0429
            r3 = 1
            r5.putLong(r8, r3)
            goto L_0x042b
        L_0x0429:
            r3 = 1
        L_0x042b:
            boolean r6 = r2.v
            if (r6 == 0) goto L_0x0432
            r5.putLong(r0, r3)
        L_0x0432:
            com.fossil.ub3 r0 = new com.fossil.ub3
            java.lang.String r13 = "_v"
            com.fossil.tb3 r14 = new com.fossil.tb3
            r14.<init>(r5)
            java.lang.String r15 = "auto"
            r12 = r0
            r16 = r10
            r12.<init>(r13, r14, r15, r16)
            r1.b(r0, r2)
        L_0x0446:
            com.fossil.oh3 r0 = r1.i
            com.fossil.ym3 r0 = r0.o()
            java.lang.String r3 = r2.a
            com.fossil.yf3<java.lang.Boolean> r4 = com.fossil.wb3.V
            boolean r0 = r0.e(r3, r4)
            if (r0 != 0) goto L_0x04a9
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            r3 = 1
            r0.putLong(r8, r3)
            com.fossil.oh3 r3 = r1.i
            com.fossil.ym3 r3 = r3.o()
            java.lang.String r4 = r2.a
            com.fossil.yf3<java.lang.Boolean> r5 = com.fossil.wb3.U
            boolean r3 = r3.e(r4, r5)
            if (r3 == 0) goto L_0x0477
            java.lang.String r3 = "_fr"
            r4 = 1
            r0.putLong(r3, r4)
        L_0x0477:
            com.fossil.ub3 r3 = new com.fossil.ub3
            java.lang.String r13 = "_e"
            com.fossil.tb3 r14 = new com.fossil.tb3
            r14.<init>(r0)
            java.lang.String r15 = "auto"
            r12 = r3
            r16 = r10
            r12.<init>(r13, r14, r15, r16)
            r1.b(r3, r2)
            goto L_0x04a9
        L_0x048c:
            boolean r0 = r2.i
            if (r0 == 0) goto L_0x04a9
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
            com.fossil.ub3 r3 = new com.fossil.ub3
            java.lang.String r13 = "_cd"
            com.fossil.tb3 r14 = new com.fossil.tb3
            r14.<init>(r0)
            java.lang.String r15 = "auto"
            r12 = r3
            r16 = r10
            r12.<init>(r13, r14, r15, r16)
            r1.b(r3, r2)
        L_0x04a9:
            com.fossil.jb3 r0 = r21.k()
            r0.t()
            com.fossil.jb3 r0 = r21.k()
            r0.z()
            return
        L_0x04b8:
            r0 = move-exception
            com.fossil.jb3 r2 = r21.k()
            r2.z()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.b(com.fossil.nm3):void");
    }

    @DexIgnore
    public final void a(ub3 ub3, nm3 nm3) {
        List<wm3> list;
        List<wm3> list2;
        List<wm3> list3;
        List<String> list4;
        ub3 ub32 = ub3;
        a72.a(nm3);
        a72.b(nm3.a);
        x();
        q();
        String str = nm3.a;
        long j2 = ub32.d;
        n();
        if (fm3.a(ub3, nm3)) {
            if (!nm3.h) {
                c(nm3);
                return;
            }
            if (this.i.o().e(str, wb3.c0) && (list4 = nm3.z) != null) {
                if (list4.contains(ub32.a)) {
                    Bundle zzb = ub32.b.zzb();
                    zzb.putLong("ga_safelisted", 1);
                    ub32 = new ub3(ub32.a, new tb3(zzb), ub32.c, ub32.d);
                } else {
                    this.i.e().A().a("Dropping non-safelisted event. appId, event name, origin", str, ub32.a, ub32.c);
                    return;
                }
            }
            k().y();
            try {
                jb3 k2 = k();
                a72.b(str);
                k2.g();
                k2.q();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    k2.e().w().a("Invalid time querying timed out conditional properties", jg3.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = k2.a("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (wm3 wm3 : list) {
                    if (wm3 != null) {
                        this.i.e().B().a("User property timed out", wm3.a, this.i.w().c(wm3.c.b), wm3.c.zza());
                        if (wm3.g != null) {
                            c(new ub3(wm3.g, j2), nm3);
                        }
                        k().e(str, wm3.c.b);
                    }
                }
                jb3 k3 = k();
                a72.b(str);
                k3.g();
                k3.q();
                if (i2 < 0) {
                    k3.e().w().a("Invalid time querying expired conditional properties", jg3.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = k3.a("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (wm3 wm32 : list2) {
                    if (wm32 != null) {
                        this.i.e().B().a("User property expired", wm32.a, this.i.w().c(wm32.c.b), wm32.c.zza());
                        k().b(str, wm32.c.b);
                        if (wm32.p != null) {
                            arrayList.add(wm32.p);
                        }
                        k().e(str, wm32.c.b);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    c(new ub3((ub3) obj, j2), nm3);
                }
                jb3 k4 = k();
                String str2 = ub32.a;
                a72.b(str);
                a72.b(str2);
                k4.g();
                k4.q();
                if (i2 < 0) {
                    k4.e().w().a("Invalid time querying triggered conditional properties", jg3.a(str), k4.i().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = k4.a("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (wm3 wm33 : list3) {
                    if (wm33 != null) {
                        em3 em3 = wm33.c;
                        gm3 gm3 = new gm3(wm33.a, wm33.b, em3.b, j2, em3.zza());
                        if (k().a(gm3)) {
                            this.i.e().B().a("User property triggered", wm33.a, this.i.w().c(gm3.c), gm3.e);
                        } else {
                            this.i.e().t().a("Too many active user properties, ignoring", jg3.a(wm33.a), this.i.w().c(gm3.c), gm3.e);
                        }
                        if (wm33.i != null) {
                            arrayList2.add(wm33.i);
                        }
                        wm33.c = new em3(gm3);
                        wm33.e = true;
                        k().a(wm33);
                    }
                }
                c(ub32, nm3);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    c(new ub3((ub3) obj2, j2), nm3);
                }
                k().t();
            } finally {
                k().z();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:130:0x039e  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x03aa  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0598  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x066f  */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x080a  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x0826  */
    /* JADX WARNING: Removed duplicated region for block: B:279:0x0840  */
    /* JADX WARNING: Removed duplicated region for block: B:506:0x0f22  */
    /* JADX WARNING: Removed duplicated region for block: B:511:0x0f36  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0262  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r44, long r45) {
        /*
            r43 = this;
            r1 = r43
            java.lang.String r2 = "_npa"
            java.lang.String r3 = ""
            com.fossil.jb3 r4 = r43.k()
            r4.y()
            com.fossil.xl3$a r4 = new com.fossil.xl3$a     // Catch:{ all -> 0x0f3a }
            r5 = 0
            r4.<init>(r1, r5)     // Catch:{ all -> 0x0f3a }
            com.fossil.jb3 r6 = r43.k()     // Catch:{ all -> 0x0f3a }
            long r7 = r1.w     // Catch:{ all -> 0x0f3a }
            com.fossil.a72.a(r4)     // Catch:{ all -> 0x0f3a }
            r6.g()     // Catch:{ all -> 0x0f3a }
            r6.q()     // Catch:{ all -> 0x0f3a }
            r10 = -1
            r12 = 2
            r13 = 0
            r14 = 1
            android.database.sqlite.SQLiteDatabase r15 = r6.u()     // Catch:{ SQLiteException -> 0x0236, all -> 0x0231 }
            boolean r16 = android.text.TextUtils.isEmpty(r5)     // Catch:{ SQLiteException -> 0x0236, all -> 0x0231 }
            if (r16 == 0) goto L_0x0095
            int r16 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r16 == 0) goto L_0x0049
            java.lang.String[] r9 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0044 }
            java.lang.String r17 = java.lang.String.valueOf(r7)     // Catch:{ SQLiteException -> 0x0044 }
            r9[r13] = r17     // Catch:{ SQLiteException -> 0x0044 }
            java.lang.String r17 = java.lang.String.valueOf(r45)     // Catch:{ SQLiteException -> 0x0044 }
            r9[r14] = r17     // Catch:{ SQLiteException -> 0x0044 }
            goto L_0x0051
        L_0x0044:
            r0 = move-exception
            r7 = r0
            r9 = r5
            goto L_0x023a
        L_0x0049:
            java.lang.String[] r9 = new java.lang.String[r14]
            java.lang.String r17 = java.lang.String.valueOf(r45)
            r9[r13] = r17
        L_0x0051:
            if (r16 == 0) goto L_0x0058
            java.lang.String r16 = "rowid <= ? and "
            r45 = r16
            goto L_0x005a
        L_0x0058:
            r45 = r3
        L_0x005a:
            int r5 = r45.length()
            int r5 = r5 + 148
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>(r5)
            java.lang.String r5 = "select app_id, metadata_fingerprint from raw_events where "
            r12.append(r5)
            r5 = r45
            r12.append(r5)
            java.lang.String r5 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r12.append(r5)
            java.lang.String r5 = r12.toString()
            android.database.Cursor r5 = r15.rawQuery(r5, r9)
            boolean r9 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x022e }
            if (r9 != 0) goto L_0x0089
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x0089:
            java.lang.String r9 = r5.getString(r13)
            java.lang.String r12 = r5.getString(r14)     // Catch:{ SQLiteException -> 0x022b }
            r5.close()     // Catch:{ SQLiteException -> 0x022b }
            goto L_0x00e6
        L_0x0095:
            int r5 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r5 == 0) goto L_0x00a6
            r9 = 2
            java.lang.String[] r12 = new java.lang.String[r9]
            r9 = 0
            r12[r13] = r9
            java.lang.String r9 = java.lang.String.valueOf(r7)
            r12[r14] = r9
            goto L_0x00ab
        L_0x00a6:
            r9 = 0
            java.lang.String[] r12 = new java.lang.String[]{r9}
        L_0x00ab:
            if (r5 == 0) goto L_0x00b0
            java.lang.String r5 = " and rowid <= ?"
            goto L_0x00b1
        L_0x00b0:
            r5 = r3
        L_0x00b1:
            int r9 = r5.length()
            int r9 = r9 + 84
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>(r9)
            java.lang.String r9 = "select metadata_fingerprint from raw_events where app_id = ?"
            r10.append(r9)
            r10.append(r5)
            java.lang.String r5 = " order by rowid limit 1;"
            r10.append(r5)
            java.lang.String r5 = r10.toString()
            android.database.Cursor r5 = r15.rawQuery(r5, r12)
            boolean r9 = r5.moveToFirst()
            if (r9 != 0) goto L_0x00de
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x00de:
            java.lang.String r12 = r5.getString(r13)
            r5.close()
            r9 = 0
        L_0x00e6:
            java.lang.String r16 = "raw_events_metadata"
            java.lang.String r10 = "metadata"
            java.lang.String[] r17 = new java.lang.String[]{r10}
            java.lang.String r18 = "app_id = ? and metadata_fingerprint = ?"
            r10 = 2
            java.lang.String[] r11 = new java.lang.String[r10]
            r11[r13] = r9
            r11[r14] = r12
            r20 = 0
            r21 = 0
            java.lang.String r22 = "rowid"
            java.lang.String r23 = "2"
            r10 = r15
            r15 = r10
            r19 = r11
            android.database.Cursor r5 = r15.query(r16, r17, r18, r19, r20, r21, r22, r23)
            boolean r11 = r5.moveToFirst()
            if (r11 != 0) goto L_0x0125
            com.fossil.jg3 r7 = r6.e()
            com.fossil.mg3 r7 = r7.t()
            java.lang.String r8 = "Raw event metadata record is missing. appId"
            java.lang.Object r10 = com.fossil.jg3.a(r9)
            r7.a(r8, r10)
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x0125:
            byte[] r11 = r5.getBlob(r13)
            com.fossil.vp2$a r15 = com.fossil.vp2.z0()     // Catch:{ IOException -> 0x0212 }
            com.fossil.fm3.a(r15, r11)     // Catch:{ IOException -> 0x0212 }
            com.fossil.vp2$a r15 = (com.fossil.vp2.a) r15     // Catch:{ IOException -> 0x0212 }
            com.fossil.jx2 r11 = r15.g()     // Catch:{ IOException -> 0x0212 }
            com.fossil.bw2 r11 = (com.fossil.bw2) r11     // Catch:{ IOException -> 0x0212 }
            com.fossil.vp2 r11 = (com.fossil.vp2) r11     // Catch:{ IOException -> 0x0212 }
            boolean r15 = r5.moveToNext()
            if (r15 == 0) goto L_0x0151
            com.fossil.jg3 r15 = r6.e()
            com.fossil.mg3 r15 = r15.w()
            java.lang.String r14 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r13 = com.fossil.jg3.a(r9)
            r15.a(r14, r13)
        L_0x0151:
            r5.close()
            r4.a(r11)
            r13 = -1
            int r11 = (r7 > r13 ? 1 : (r7 == r13 ? 0 : -1))
            if (r11 == 0) goto L_0x0174
            java.lang.String r11 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]
            r13 = 0
            r14[r13] = r9
            r13 = 1
            r14[r13] = r12
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r8 = 2
            r14[r8] = r7
            r18 = r11
            r19 = r14
            goto L_0x0183
        L_0x0174:
            java.lang.String r7 = "app_id = ? and metadata_fingerprint = ?"
            r8 = 2
            java.lang.String[] r11 = new java.lang.String[r8]
            r8 = 0
            r11[r8] = r9
            r8 = 1
            r11[r8] = r12
            r18 = r7
            r19 = r11
        L_0x0183:
            java.lang.String r16 = "raw_events"
            java.lang.String r7 = "rowid"
            java.lang.String r8 = "name"
            java.lang.String r11 = "timestamp"
            java.lang.String r12 = "data"
            java.lang.String[] r17 = new java.lang.String[]{r7, r8, r11, r12}
            r20 = 0
            r21 = 0
            java.lang.String r22 = "rowid"
            r23 = 0
            r15 = r10
            android.database.Cursor r5 = r15.query(r16, r17, r18, r19, r20, r21, r22, r23)
            boolean r7 = r5.moveToFirst()
            if (r7 != 0) goto L_0x01bc
            com.fossil.jg3 r7 = r6.e()
            com.fossil.mg3 r7 = r7.w()
            java.lang.String r8 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r10 = com.fossil.jg3.a(r9)
            r7.a(r8, r10)
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x01bc:
            r7 = 0
            long r10 = r5.getLong(r7)
            r7 = 3
            byte[] r8 = r5.getBlob(r7)
            com.fossil.rp2$a r7 = com.fossil.rp2.z()     // Catch:{ IOException -> 0x01f3 }
            com.fossil.fm3.a(r7, r8)     // Catch:{ IOException -> 0x01f3 }
            com.fossil.rp2$a r7 = (com.fossil.rp2.a) r7     // Catch:{ IOException -> 0x01f3 }
            r8 = 1
            java.lang.String r12 = r5.getString(r8)
            r7.a(r12)
            r8 = 2
            long r12 = r5.getLong(r8)
            r7.a(r12)
            com.fossil.jx2 r7 = r7.g()
            com.fossil.bw2 r7 = (com.fossil.bw2) r7
            com.fossil.rp2 r7 = (com.fossil.rp2) r7
            boolean r7 = r4.a(r10, r7)
            if (r7 != 0) goto L_0x0206
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x01f3:
            r0 = move-exception
            r7 = r0
            com.fossil.jg3 r8 = r6.e()
            com.fossil.mg3 r8 = r8.t()
            java.lang.String r10 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r11 = com.fossil.jg3.a(r9)
            r8.a(r10, r11, r7)
        L_0x0206:
            boolean r7 = r5.moveToNext()
            if (r7 != 0) goto L_0x01bc
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x0212:
            r0 = move-exception
            r7 = r0
            com.fossil.jg3 r8 = r6.e()
            com.fossil.mg3 r8 = r8.t()
            java.lang.String r10 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r11 = com.fossil.jg3.a(r9)
            r8.a(r10, r11, r7)
            if (r5 == 0) goto L_0x0250
            r5.close()
            goto L_0x0250
        L_0x022b:
            r0 = move-exception
            r7 = r0
            goto L_0x023a
        L_0x022e:
            r0 = move-exception
            r7 = r0
            goto L_0x0239
        L_0x0231:
            r0 = move-exception
            r2 = r0
            r5 = 0
            goto L_0x0f34
        L_0x0236:
            r0 = move-exception
            r7 = r0
            r5 = 0
        L_0x0239:
            r9 = 0
        L_0x023a:
            com.fossil.jg3 r6 = r6.e()     // Catch:{ all -> 0x0f32 }
            com.fossil.mg3 r6 = r6.t()     // Catch:{ all -> 0x0f32 }
            java.lang.String r8 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r9 = com.fossil.jg3.a(r9)     // Catch:{ all -> 0x0f32 }
            r6.a(r8, r9, r7)     // Catch:{ all -> 0x0f32 }
            if (r5 == 0) goto L_0x0250
            r5.close()
        L_0x0250:
            java.util.List<com.fossil.rp2> r5 = r4.c
            if (r5 == 0) goto L_0x025f
            java.util.List<com.fossil.rp2> r5 = r4.c
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x025d
            goto L_0x025f
        L_0x025d:
            r5 = 0
            goto L_0x0260
        L_0x025f:
            r5 = 1
        L_0x0260:
            if (r5 != 0) goto L_0x0f22
            com.fossil.vp2 r5 = r4.a
            com.fossil.bw2$a r5 = r5.l()
            com.fossil.vp2$a r5 = (com.fossil.vp2.a) r5
            r5.p()
            com.fossil.oh3 r6 = r1.i
            com.fossil.ym3 r6 = r6.o()
            com.fossil.vp2 r7 = r4.a
            java.lang.String r7 = r7.w0()
            com.fossil.yf3<java.lang.Boolean> r8 = com.fossil.wb3.V
            boolean r6 = r6.e(r7, r8)
            r7 = -1
            r8 = -1
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
        L_0x0288:
            java.util.List<com.fossil.rp2> r9 = r4.c
            int r9 = r9.size()
            r18 = r3
            java.lang.String r3 = "_fr"
            r19 = r13
            java.lang.String r13 = "_et"
            r20 = r2
            java.lang.String r2 = "_e"
            r21 = r14
            r22 = r15
            if (r12 >= r9) goto L_0x089c
            java.util.List<com.fossil.rp2> r9 = r4.c
            java.lang.Object r9 = r9.get(r12)
            com.fossil.rp2 r9 = (com.fossil.rp2) r9
            com.fossil.bw2$a r9 = r9.l()
            com.fossil.rp2$a r9 = (com.fossil.rp2.a) r9
            com.fossil.ih3 r14 = r43.i()
            com.fossil.vp2 r15 = r4.a
            java.lang.String r15 = r15.w0()
            r16 = r12
            java.lang.String r12 = r9.q()
            boolean r12 = r14.b(r15, r12)
            java.lang.String r14 = "_err"
            if (r12 == 0) goto L_0x0343
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.w()
            java.lang.String r3 = "Dropping blacklisted raw event. appId"
            com.fossil.vp2 r12 = r4.a
            java.lang.String r12 = r12.w0()
            java.lang.Object r12 = com.fossil.jg3.a(r12)
            com.fossil.oh3 r13 = r1.i
            com.fossil.hg3 r13 = r13.w()
            java.lang.String r15 = r9.q()
            java.lang.String r13 = r13.a(r15)
            r2.a(r3, r12, r13)
            com.fossil.ih3 r2 = r43.i()
            com.fossil.vp2 r3 = r4.a
            java.lang.String r3 = r3.w0()
            boolean r2 = r2.g(r3)
            if (r2 != 0) goto L_0x0310
            com.fossil.ih3 r2 = r43.i()
            com.fossil.vp2 r3 = r4.a
            java.lang.String r3 = r3.w0()
            boolean r2 = r2.h(r3)
            if (r2 == 0) goto L_0x030e
            goto L_0x0310
        L_0x030e:
            r2 = 0
            goto L_0x0311
        L_0x0310:
            r2 = 1
        L_0x0311:
            if (r2 != 0) goto L_0x0336
            java.lang.String r2 = r9.q()
            boolean r2 = r14.equals(r2)
            if (r2 != 0) goto L_0x0336
            com.fossil.oh3 r2 = r1.i
            com.fossil.jm3 r24 = r2.v()
            com.fossil.vp2 r2 = r4.a
            java.lang.String r25 = r2.w0()
            r26 = 11
            java.lang.String r27 = "_ev"
            java.lang.String r28 = r9.q()
            r29 = 0
            r24.a(r25, r26, r27, r28, r29)
        L_0x0336:
            r26 = r6
            r12 = r16
            r13 = r19
            r14 = r21
            r15 = r22
            r6 = r5
            goto L_0x0891
        L_0x0343:
            com.fossil.ih3 r12 = r43.i()
            com.fossil.vp2 r15 = r4.a
            java.lang.String r15 = r15.w0()
            r26 = r6
            java.lang.String r6 = r9.q()
            boolean r6 = r12.c(r15, r6)
            java.lang.String r12 = "_c"
            if (r6 != 0) goto L_0x03b5
            r43.n()
            java.lang.String r15 = r9.q()
            com.fossil.a72.b(r15)
            r27 = r7
            int r7 = r15.hashCode()
            r28 = r10
            r10 = 94660(0x171c4, float:1.32647E-40)
            if (r7 == r10) goto L_0x0391
            r10 = 95025(0x17331, float:1.33158E-40)
            if (r7 == r10) goto L_0x0387
            r10 = 95027(0x17333, float:1.33161E-40)
            if (r7 == r10) goto L_0x037d
            goto L_0x039b
        L_0x037d:
            java.lang.String r7 = "_ui"
            boolean r7 = r15.equals(r7)
            if (r7 == 0) goto L_0x039b
            r7 = 1
            goto L_0x039c
        L_0x0387:
            java.lang.String r7 = "_ug"
            boolean r7 = r15.equals(r7)
            if (r7 == 0) goto L_0x039b
            r7 = 2
            goto L_0x039c
        L_0x0391:
            java.lang.String r7 = "_in"
            boolean r7 = r15.equals(r7)
            if (r7 == 0) goto L_0x039b
            r7 = 0
            goto L_0x039c
        L_0x039b:
            r7 = -1
        L_0x039c:
            if (r7 == 0) goto L_0x03a6
            r10 = 1
            if (r7 == r10) goto L_0x03a6
            r10 = 2
            if (r7 == r10) goto L_0x03a6
            r7 = 0
            goto L_0x03a7
        L_0x03a6:
            r7 = 1
        L_0x03a7:
            if (r7 == 0) goto L_0x03aa
            goto L_0x03b9
        L_0x03aa:
            r30 = r5
            r31 = r8
            r10 = r11
            r29 = r13
            r13 = r2
            r11 = r3
            goto L_0x0596
        L_0x03b5:
            r27 = r7
            r28 = r10
        L_0x03b9:
            r29 = r13
            r7 = 0
            r10 = 0
            r15 = 0
        L_0x03be:
            int r13 = r9.o()
            r30 = r5
            java.lang.String r5 = "_r"
            if (r15 >= r13) goto L_0x0428
            com.fossil.tp2 r13 = r9.a(r15)
            java.lang.String r13 = r13.p()
            boolean r13 = r12.equals(r13)
            if (r13 == 0) goto L_0x03f4
            com.fossil.tp2 r5 = r9.a(r15)
            com.fossil.bw2$a r5 = r5.l()
            com.fossil.tp2$a r5 = (com.fossil.tp2.a) r5
            r13 = r8
            r7 = 1
            r5.a(r7)
            com.fossil.jx2 r5 = r5.g()
            com.fossil.bw2 r5 = (com.fossil.bw2) r5
            com.fossil.tp2 r5 = (com.fossil.tp2) r5
            r9.a(r15, r5)
            r8 = r11
            r7 = 1
            goto L_0x0421
        L_0x03f4:
            r13 = r8
            com.fossil.tp2 r8 = r9.a(r15)
            java.lang.String r8 = r8.p()
            boolean r5 = r5.equals(r8)
            if (r5 == 0) goto L_0x0420
            com.fossil.tp2 r5 = r9.a(r15)
            com.fossil.bw2$a r5 = r5.l()
            com.fossil.tp2$a r5 = (com.fossil.tp2.a) r5
            r8 = r11
            r10 = 1
            r5.a(r10)
            com.fossil.jx2 r5 = r5.g()
            com.fossil.bw2 r5 = (com.fossil.bw2) r5
            com.fossil.tp2 r5 = (com.fossil.tp2) r5
            r9.a(r15, r5)
            r10 = 1
            goto L_0x0421
        L_0x0420:
            r8 = r11
        L_0x0421:
            int r15 = r15 + 1
            r11 = r8
            r8 = r13
            r5 = r30
            goto L_0x03be
        L_0x0428:
            r13 = r8
            r8 = r11
            if (r7 != 0) goto L_0x045f
            if (r6 == 0) goto L_0x045f
            com.fossil.oh3 r7 = r1.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.B()
            java.lang.String r11 = "Marking event as conversion"
            com.fossil.oh3 r15 = r1.i
            com.fossil.hg3 r15 = r15.w()
            r31 = r13
            java.lang.String r13 = r9.q()
            java.lang.String r13 = r15.a(r13)
            r7.a(r11, r13)
            com.fossil.tp2$a r7 = com.fossil.tp2.F()
            r7.a(r12)
            r13 = r2
            r11 = r3
            r2 = 1
            r7.a(r2)
            r9.a(r7)
            goto L_0x0463
        L_0x045f:
            r11 = r3
            r31 = r13
            r13 = r2
        L_0x0463:
            if (r10 != 0) goto L_0x0493
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.B()
            java.lang.String r3 = "Marking event as real-time"
            com.fossil.oh3 r7 = r1.i
            com.fossil.hg3 r7 = r7.w()
            java.lang.String r10 = r9.q()
            java.lang.String r7 = r7.a(r10)
            r2.a(r3, r7)
            com.fossil.tp2$a r2 = com.fossil.tp2.F()
            r2.a(r5)
            r3 = r8
            r7 = 1
            r2.a(r7)
            r9.a(r2)
            goto L_0x0494
        L_0x0493:
            r3 = r8
        L_0x0494:
            com.fossil.jb3 r32 = r43.k()
            long r33 = r43.y()
            com.fossil.vp2 r2 = r4.a
            java.lang.String r35 = r2.w0()
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 0
            r40 = 1
            com.fossil.ib3 r2 = r32.a(r33, r35, r36, r37, r38, r39, r40)
            long r7 = r2.e
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            com.fossil.vp2 r10 = r4.a
            java.lang.String r10 = r10.w0()
            int r2 = r2.c(r10)
            r10 = r3
            long r2 = (long) r2
            int r15 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r15 <= 0) goto L_0x04cc
            a(r9, r5)
            goto L_0x04ce
        L_0x04cc:
            r19 = 1
        L_0x04ce:
            java.lang.String r2 = r9.q()
            boolean r2 = com.fossil.jm3.h(r2)
            if (r2 == 0) goto L_0x0596
            if (r6 == 0) goto L_0x0596
            com.fossil.jb3 r32 = r43.k()
            long r33 = r43.y()
            com.fossil.vp2 r2 = r4.a
            java.lang.String r35 = r2.w0()
            r36 = 0
            r37 = 0
            r38 = 1
            r39 = 0
            r40 = 0
            com.fossil.ib3 r2 = r32.a(r33, r35, r36, r37, r38, r39, r40)
            long r2 = r2.c
            com.fossil.oh3 r5 = r1.i
            com.fossil.ym3 r5 = r5.o()
            com.fossil.vp2 r7 = r4.a
            java.lang.String r7 = r7.w0()
            com.fossil.yf3<java.lang.Integer> r8 = com.fossil.wb3.n
            int r5 = r5.b(r7, r8)
            long r7 = (long) r5
            int r5 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r5 <= 0) goto L_0x0596
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.w()
            java.lang.String r3 = "Too many conversions. Not logging as conversion. appId"
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            java.lang.Object r5 = com.fossil.jg3.a(r5)
            r2.a(r3, r5)
            r2 = -1
            r3 = 0
            r5 = 0
            r7 = 0
        L_0x052c:
            int r8 = r9.o()
            if (r7 >= r8) goto L_0x0557
            com.fossil.tp2 r8 = r9.a(r7)
            java.lang.String r15 = r8.p()
            boolean r15 = r12.equals(r15)
            if (r15 == 0) goto L_0x0549
            com.fossil.bw2$a r2 = r8.l()
            com.fossil.tp2$a r2 = (com.fossil.tp2.a) r2
            r3 = r2
            r2 = r7
            goto L_0x0554
        L_0x0549:
            java.lang.String r8 = r8.p()
            boolean r8 = r14.equals(r8)
            if (r8 == 0) goto L_0x0554
            r5 = 1
        L_0x0554:
            int r7 = r7 + 1
            goto L_0x052c
        L_0x0557:
            if (r5 == 0) goto L_0x055f
            if (r3 == 0) goto L_0x055f
            r9.b(r2)
            goto L_0x0596
        L_0x055f:
            if (r3 == 0) goto L_0x057d
            java.lang.Object r3 = r3.clone()
            com.fossil.bw2$a r3 = (com.fossil.bw2.a) r3
            com.fossil.tp2$a r3 = (com.fossil.tp2.a) r3
            r3.a(r14)
            r7 = 10
            r3.a(r7)
            com.fossil.jx2 r3 = r3.g()
            com.fossil.bw2 r3 = (com.fossil.bw2) r3
            com.fossil.tp2 r3 = (com.fossil.tp2) r3
            r9.a(r2, r3)
            goto L_0x0596
        L_0x057d:
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Did not find conversion parameter. appId"
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            java.lang.Object r5 = com.fossil.jg3.a(r5)
            r2.a(r3, r5)
        L_0x0596:
            if (r6 == 0) goto L_0x0659
            java.util.ArrayList r2 = new java.util.ArrayList
            java.util.List r3 = r9.zza()
            r2.<init>(r3)
            r3 = 0
            r5 = -1
            r6 = -1
        L_0x05a4:
            int r7 = r2.size()
            java.lang.String r8 = "currency"
            java.lang.String r14 = "value"
            if (r3 >= r7) goto L_0x05d4
            java.lang.Object r7 = r2.get(r3)
            com.fossil.tp2 r7 = (com.fossil.tp2) r7
            java.lang.String r7 = r7.p()
            boolean r7 = r14.equals(r7)
            if (r7 == 0) goto L_0x05c0
            r5 = r3
            goto L_0x05d1
        L_0x05c0:
            java.lang.Object r7 = r2.get(r3)
            com.fossil.tp2 r7 = (com.fossil.tp2) r7
            java.lang.String r7 = r7.p()
            boolean r7 = r8.equals(r7)
            if (r7 == 0) goto L_0x05d1
            r6 = r3
        L_0x05d1:
            int r3 = r3 + 1
            goto L_0x05a4
        L_0x05d4:
            r3 = -1
            if (r5 == r3) goto L_0x065a
            java.lang.Object r3 = r2.get(r5)
            com.fossil.tp2 r3 = (com.fossil.tp2) r3
            boolean r3 = r3.s()
            if (r3 != 0) goto L_0x060a
            java.lang.Object r3 = r2.get(r5)
            com.fossil.tp2 r3 = (com.fossil.tp2) r3
            boolean r3 = r3.w()
            if (r3 != 0) goto L_0x060a
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.y()
            java.lang.String r3 = "Value must be specified with a numeric type."
            r2.a(r3)
            r9.b(r5)
            a(r9, r12)
            r2 = 18
            a(r9, r2, r14)
            goto L_0x0659
        L_0x060a:
            r3 = -1
            if (r6 != r3) goto L_0x0610
            r2 = 1
            r7 = 3
            goto L_0x063c
        L_0x0610:
            java.lang.Object r2 = r2.get(r6)
            com.fossil.tp2 r2 = (com.fossil.tp2) r2
            java.lang.String r2 = r2.r()
            int r6 = r2.length()
            r7 = 3
            if (r6 == r7) goto L_0x0623
        L_0x0621:
            r2 = 1
            goto L_0x063c
        L_0x0623:
            r6 = 0
        L_0x0624:
            int r14 = r2.length()
            if (r6 >= r14) goto L_0x063b
            int r14 = r2.codePointAt(r6)
            boolean r15 = java.lang.Character.isLetter(r14)
            if (r15 != 0) goto L_0x0635
            goto L_0x0621
        L_0x0635:
            int r14 = java.lang.Character.charCount(r14)
            int r6 = r6 + r14
            goto L_0x0624
        L_0x063b:
            r2 = 0
        L_0x063c:
            if (r2 == 0) goto L_0x065b
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.y()
            java.lang.String r6 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r2.a(r6)
            r9.b(r5)
            a(r9, r12)
            r2 = 19
            a(r9, r2, r8)
            goto L_0x065b
        L_0x0659:
            r3 = -1
        L_0x065a:
            r7 = 3
        L_0x065b:
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            com.fossil.yf3<java.lang.Boolean> r6 = com.fossil.wb3.U
            boolean r2 = r2.e(r5, r6)
            if (r2 == 0) goto L_0x080a
            java.lang.String r2 = r9.q()
            r5 = r13
            boolean r2 = r5.equals(r2)
            r12 = 1000(0x3e8, double:4.94E-321)
            if (r2 == 0) goto L_0x06d5
            r43.n()
            com.fossil.jx2 r2 = r9.g()
            com.fossil.bw2 r2 = (com.fossil.bw2) r2
            com.fossil.rp2 r2 = (com.fossil.rp2) r2
            com.fossil.tp2 r2 = com.fossil.fm3.b(r2, r11)
            if (r2 != 0) goto L_0x06cb
            if (r10 == 0) goto L_0x06bf
            long r14 = r10.zzf()
            long r24 = r9.zzf()
            long r14 = r14 - r24
            long r14 = java.lang.Math.abs(r14)
            int r2 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x06bf
            java.lang.Object r2 = r10.clone()
            com.fossil.bw2$a r2 = (com.fossil.bw2.a) r2
            com.fossil.rp2$a r2 = (com.fossil.rp2.a) r2
            boolean r6 = r1.a(r9, r2)
            if (r6 == 0) goto L_0x06bf
            r6 = r30
            r8 = r31
            r6.a(r8, r2)
            r7 = r27
            r14 = r29
        L_0x06ba:
            r10 = 0
        L_0x06bb:
            r28 = 0
            goto L_0x0814
        L_0x06bf:
            r6 = r30
            r8 = r31
            r28 = r9
            r7 = r21
            r14 = r29
            goto L_0x0814
        L_0x06cb:
            r6 = r30
            r8 = r31
            r11 = r27
            r14 = r29
            goto L_0x0813
        L_0x06d5:
            r6 = r30
            r8 = r31
            java.lang.String r2 = "_vs"
            java.lang.String r11 = r9.q()
            boolean r2 = r2.equals(r11)
            if (r2 == 0) goto L_0x072d
            r43.n()
            com.fossil.jx2 r2 = r9.g()
            com.fossil.bw2 r2 = (com.fossil.bw2) r2
            com.fossil.rp2 r2 = (com.fossil.rp2) r2
            r14 = r29
            com.fossil.tp2 r2 = com.fossil.fm3.b(r2, r14)
            if (r2 != 0) goto L_0x0729
            if (r28 == 0) goto L_0x0721
            long r10 = r28.zzf()
            long r24 = r9.zzf()
            long r10 = r10 - r24
            long r10 = java.lang.Math.abs(r10)
            int r2 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x0721
            java.lang.Object r2 = r28.clone()
            com.fossil.bw2$a r2 = (com.fossil.bw2.a) r2
            com.fossil.rp2$a r2 = (com.fossil.rp2.a) r2
            boolean r10 = r1.a(r2, r9)
            if (r10 == 0) goto L_0x0721
            r11 = r27
            r6.a(r11, r2)
            r7 = r11
            goto L_0x06ba
        L_0x0721:
            r11 = r27
            r10 = r9
            r7 = r11
            r8 = r21
            goto L_0x0814
        L_0x0729:
            r11 = r27
            goto L_0x0813
        L_0x072d:
            r11 = r27
            r14 = r29
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            com.fossil.vp2 r12 = r4.a
            java.lang.String r12 = r12.w0()
            com.fossil.yf3<java.lang.Boolean> r13 = com.fossil.wb3.s0
            boolean r2 = r2.e(r12, r13)
            if (r2 == 0) goto L_0x0813
            java.lang.String r2 = "_ab"
            java.lang.String r12 = r9.q()
            boolean r2 = r2.equals(r12)
            if (r2 == 0) goto L_0x0813
            r43.n()
            com.fossil.jx2 r2 = r9.g()
            com.fossil.bw2 r2 = (com.fossil.bw2) r2
            com.fossil.rp2 r2 = (com.fossil.rp2) r2
            com.fossil.tp2 r2 = com.fossil.fm3.b(r2, r14)
            if (r2 != 0) goto L_0x0813
            if (r28 == 0) goto L_0x0813
            long r12 = r28.zzf()
            long r24 = r9.zzf()
            long r12 = r12 - r24
            long r12 = java.lang.Math.abs(r12)
            r24 = 4000(0xfa0, double:1.9763E-320)
            int r2 = (r12 > r24 ? 1 : (r12 == r24 ? 0 : -1))
            if (r2 > 0) goto L_0x0813
            java.lang.Object r2 = r28.clone()
            com.fossil.bw2$a r2 = (com.fossil.bw2.a) r2
            com.fossil.rp2$a r2 = (com.fossil.rp2.a) r2
            r1.b(r2, r9)
            java.lang.String r12 = r2.q()
            boolean r12 = r5.equals(r12)
            com.fossil.a72.a(r12)
            r43.n()
            com.fossil.jx2 r12 = r2.g()
            com.fossil.bw2 r12 = (com.fossil.bw2) r12
            com.fossil.rp2 r12 = (com.fossil.rp2) r12
            java.lang.String r13 = "_sn"
            com.fossil.tp2 r12 = com.fossil.fm3.b(r12, r13)
            r43.n()
            com.fossil.jx2 r13 = r2.g()
            com.fossil.bw2 r13 = (com.fossil.bw2) r13
            com.fossil.rp2 r13 = (com.fossil.rp2) r13
            java.lang.String r15 = "_sc"
            com.fossil.tp2 r13 = com.fossil.fm3.b(r13, r15)
            r43.n()
            com.fossil.jx2 r15 = r2.g()
            com.fossil.bw2 r15 = (com.fossil.bw2) r15
            com.fossil.rp2 r15 = (com.fossil.rp2) r15
            java.lang.String r3 = "_si"
            com.fossil.tp2 r3 = com.fossil.fm3.b(r15, r3)
            if (r12 == 0) goto L_0x07c8
            java.lang.String r12 = r12.r()
            goto L_0x07ca
        L_0x07c8:
            r12 = r18
        L_0x07ca:
            boolean r15 = android.text.TextUtils.isEmpty(r12)
            if (r15 != 0) goto L_0x07d9
            com.fossil.fm3 r15 = r43.n()
            java.lang.String r7 = "_sn"
            r15.a(r9, r7, r12)
        L_0x07d9:
            if (r13 == 0) goto L_0x07e0
            java.lang.String r7 = r13.r()
            goto L_0x07e2
        L_0x07e0:
            r7 = r18
        L_0x07e2:
            boolean r12 = android.text.TextUtils.isEmpty(r7)
            if (r12 != 0) goto L_0x07f1
            com.fossil.fm3 r12 = r43.n()
            java.lang.String r13 = "_sc"
            r12.a(r9, r13, r7)
        L_0x07f1:
            if (r3 == 0) goto L_0x0804
            com.fossil.fm3 r7 = r43.n()
            java.lang.String r12 = "_si"
            long r24 = r3.t()
            java.lang.Long r3 = java.lang.Long.valueOf(r24)
            r7.a(r9, r12, r3)
        L_0x0804:
            r6.a(r11, r2)
            r7 = r11
            goto L_0x06bb
        L_0x080a:
            r5 = r13
            r11 = r27
            r14 = r29
            r6 = r30
            r8 = r31
        L_0x0813:
            r7 = r11
        L_0x0814:
            if (r26 != 0) goto L_0x0875
            java.lang.String r2 = r9.q()
            boolean r2 = r5.equals(r2)
            if (r2 == 0) goto L_0x0875
            int r2 = r9.o()
            if (r2 != 0) goto L_0x0840
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.w()
            java.lang.String r3 = "Engagement event does not contain any parameters. appId"
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            java.lang.Object r5 = com.fossil.jg3.a(r5)
            r2.a(r3, r5)
            goto L_0x0875
        L_0x0840:
            com.fossil.fm3 r2 = r43.n()
            com.fossil.jx2 r3 = r9.g()
            com.fossil.bw2 r3 = (com.fossil.bw2) r3
            com.fossil.rp2 r3 = (com.fossil.rp2) r3
            java.lang.Object r2 = r2.a(r3, r14)
            java.lang.Long r2 = (java.lang.Long) r2
            if (r2 != 0) goto L_0x086e
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.w()
            java.lang.String r3 = "Engagement event does not include duration. appId"
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            java.lang.Object r5 = com.fossil.jg3.a(r5)
            r2.a(r3, r5)
            goto L_0x0875
        L_0x086e:
            long r2 = r2.longValue()
            long r2 = r22 + r2
            goto L_0x0877
        L_0x0875:
            r2 = r22
        L_0x0877:
            java.util.List<com.fossil.rp2> r5 = r4.c
            com.fossil.jx2 r11 = r9.g()
            com.fossil.bw2 r11 = (com.fossil.bw2) r11
            com.fossil.rp2 r11 = (com.fossil.rp2) r11
            r12 = r16
            r5.set(r12, r11)
            int r14 = r21 + 1
            r6.a(r9)
            r15 = r2
            r11 = r10
            r13 = r19
            r10 = r28
        L_0x0891:
            int r12 = r12 + 1
            r5 = r6
            r3 = r18
            r2 = r20
            r6 = r26
            goto L_0x0288
        L_0x089c:
            r11 = r3
            r26 = r6
            r14 = r13
            r6 = r5
            r5 = r2
            if (r26 == 0) goto L_0x08f9
            r2 = r21
            r15 = r22
            r3 = 0
        L_0x08a9:
            if (r3 >= r2) goto L_0x08f7
            com.fossil.rp2 r7 = r6.b(r3)
            java.lang.String r8 = r7.q()
            boolean r8 = r5.equals(r8)
            if (r8 == 0) goto L_0x08ca
            r43.n()
            com.fossil.tp2 r8 = com.fossil.fm3.b(r7, r11)
            if (r8 == 0) goto L_0x08ca
            r6.c(r3)
            int r2 = r2 + -1
            int r3 = r3 + -1
            goto L_0x08f4
        L_0x08ca:
            r43.n()
            com.fossil.tp2 r7 = com.fossil.fm3.b(r7, r14)
            if (r7 == 0) goto L_0x08f4
            boolean r8 = r7.s()
            if (r8 == 0) goto L_0x08e2
            long r7 = r7.t()
            java.lang.Long r7 = java.lang.Long.valueOf(r7)
            goto L_0x08e3
        L_0x08e2:
            r7 = 0
        L_0x08e3:
            if (r7 == 0) goto L_0x08f4
            long r8 = r7.longValue()
            r12 = 0
            int r10 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r10 <= 0) goto L_0x08f4
            long r7 = r7.longValue()
            long r15 = r15 + r7
        L_0x08f4:
            r7 = 1
            int r3 = r3 + r7
            goto L_0x08a9
        L_0x08f7:
            r2 = r15
            goto L_0x08fb
        L_0x08f9:
            r2 = r22
        L_0x08fb:
            r5 = 0
            r1.a(r6, r2, r5)
            java.util.List r5 = r6.zza()
            java.util.Iterator r5 = r5.iterator()
        L_0x0907:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x0921
            java.lang.Object r7 = r5.next()
            com.fossil.rp2 r7 = (com.fossil.rp2) r7
            java.lang.String r8 = "_s"
            java.lang.String r7 = r7.q()
            boolean r7 = r8.equals(r7)
            if (r7 == 0) goto L_0x0907
            r5 = 1
            goto L_0x0922
        L_0x0921:
            r5 = 0
        L_0x0922:
            java.lang.String r7 = "_se"
            if (r5 == 0) goto L_0x0931
            com.fossil.jb3 r5 = r43.k()
            java.lang.String r8 = r6.u()
            r5.b(r8, r7)
        L_0x0931:
            java.lang.String r5 = "_sid"
            int r5 = com.fossil.fm3.a(r6, r5)
            if (r5 < 0) goto L_0x093b
            r5 = 1
            goto L_0x093c
        L_0x093b:
            r5 = 0
        L_0x093c:
            if (r5 == 0) goto L_0x0943
            r5 = 1
            r1.a(r6, r2, r5)
            goto L_0x0965
        L_0x0943:
            int r2 = com.fossil.fm3.a(r6, r7)
            if (r2 < 0) goto L_0x0965
            r6.e(r2)
            com.fossil.oh3 r2 = r1.i
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Session engagement user property is in the bundle without session ID. appId"
            com.fossil.vp2 r5 = r4.a
            java.lang.String r5 = r5.w0()
            java.lang.Object r5 = com.fossil.jg3.a(r5)
            r2.a(r3, r5)
        L_0x0965:
            com.fossil.fm3 r2 = r43.n()
            com.fossil.jg3 r3 = r2.e()
            com.fossil.mg3 r3 = r3.B()
            java.lang.String r5 = "Checking account type status for ad personalization signals"
            r3.a(r5)
            com.fossil.ih3 r3 = r2.o()
            java.lang.String r5 = r6.u()
            boolean r3 = r3.e(r5)
            if (r3 == 0) goto L_0x09f3
            com.fossil.jb3 r3 = r2.n()
            java.lang.String r5 = r6.u()
            com.fossil.kg3 r3 = r3.b(r5)
            if (r3 == 0) goto L_0x09f3
            boolean r3 = r3.g()
            if (r3 == 0) goto L_0x09f3
            com.fossil.ob3 r3 = r2.h()
            boolean r3 = r3.w()
            if (r3 == 0) goto L_0x09f3
            com.fossil.jg3 r3 = r2.e()
            com.fossil.mg3 r3 = r3.A()
            java.lang.String r5 = "Turning off ad personalization due to account type"
            r3.a(r5)
            com.fossil.zp2$a r3 = com.fossil.zp2.A()
            r5 = r20
            r3.a(r5)
            com.fossil.ob3 r2 = r2.h()
            long r7 = r2.u()
            r3.a(r7)
            r7 = 1
            r3.b(r7)
            com.fossil.jx2 r2 = r3.g()
            com.fossil.bw2 r2 = (com.fossil.bw2) r2
            com.fossil.zp2 r2 = (com.fossil.zp2) r2
            r3 = 0
        L_0x09d1:
            int r7 = r6.zze()
            if (r3 >= r7) goto L_0x09ed
            com.fossil.zp2 r7 = r6.d(r3)
            java.lang.String r7 = r7.q()
            boolean r7 = r5.equals(r7)
            if (r7 == 0) goto L_0x09ea
            r6.a(r3, r2)
            r3 = 1
            goto L_0x09ee
        L_0x09ea:
            int r3 = r3 + 1
            goto L_0x09d1
        L_0x09ed:
            r3 = 0
        L_0x09ee:
            if (r3 != 0) goto L_0x09f3
            r6.a(r2)
        L_0x09f3:
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            java.lang.String r3 = r6.u()
            com.fossil.yf3<java.lang.Boolean> r5 = com.fossil.wb3.n0
            boolean r2 = r2.e(r3, r5)
            if (r2 == 0) goto L_0x0a08
            a(r6)
        L_0x0a08:
            r6.x()
            com.fossil.om3 r7 = r43.l()
            java.lang.String r8 = r6.u()
            java.util.List r9 = r6.zza()
            java.util.List r10 = r6.q()
            long r2 = r6.zzf()
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            long r2 = r6.r()
            java.lang.Long r12 = java.lang.Long.valueOf(r2)
            java.util.List r2 = r7.a(r8, r9, r10, r11, r12)
            r6.b(r2)
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            com.fossil.vp2 r3 = r4.a
            java.lang.String r3 = r3.w0()
            boolean r2 = r2.h(r3)
            if (r2 == 0) goto L_0x0d7a
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x0d75 }
            r2.<init>()     // Catch:{ all -> 0x0d75 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0d75 }
            r3.<init>()     // Catch:{ all -> 0x0d75 }
            com.fossil.oh3 r5 = r1.i     // Catch:{ all -> 0x0d75 }
            com.fossil.jm3 r5 = r5.v()     // Catch:{ all -> 0x0d75 }
            java.security.SecureRandom r5 = r5.t()     // Catch:{ all -> 0x0d75 }
            r7 = 0
        L_0x0a59:
            int r8 = r6.o()     // Catch:{ all -> 0x0d75 }
            if (r7 >= r8) goto L_0x0d40
            com.fossil.rp2 r8 = r6.b(r7)     // Catch:{ all -> 0x0d75 }
            com.fossil.bw2$a r8 = r8.l()     // Catch:{ all -> 0x0d75 }
            com.fossil.rp2$a r8 = (com.fossil.rp2.a) r8     // Catch:{ all -> 0x0d75 }
            java.lang.String r9 = r8.q()     // Catch:{ all -> 0x0d75 }
            java.lang.String r10 = "_ep"
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x0d75 }
            java.lang.String r10 = "_sr"
            if (r9 == 0) goto L_0x0aed
            com.fossil.fm3 r9 = r43.n()
            com.fossil.jx2 r11 = r8.g()
            com.fossil.bw2 r11 = (com.fossil.bw2) r11
            com.fossil.rp2 r11 = (com.fossil.rp2) r11
            java.lang.String r12 = "_en"
            java.lang.Object r9 = r9.a(r11, r12)
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r11 = r2.get(r9)
            com.fossil.qb3 r11 = (com.fossil.qb3) r11
            if (r11 != 0) goto L_0x0aa4
            com.fossil.jb3 r11 = r43.k()
            com.fossil.vp2 r12 = r4.a
            java.lang.String r12 = r12.w0()
            com.fossil.qb3 r11 = r11.a(r12, r9)
            r2.put(r9, r11)
        L_0x0aa4:
            java.lang.Long r9 = r11.i
            if (r9 != 0) goto L_0x0ae3
            java.lang.Long r9 = r11.j
            long r12 = r9.longValue()
            r14 = 1
            int r9 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r9 <= 0) goto L_0x0abd
            com.fossil.fm3 r9 = r43.n()
            java.lang.Long r12 = r11.j
            r9.a(r8, r10, r12)
        L_0x0abd:
            java.lang.Boolean r9 = r11.k
            if (r9 == 0) goto L_0x0ad8
            java.lang.Boolean r9 = r11.k
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0ad8
            com.fossil.fm3 r9 = r43.n()
            java.lang.String r10 = "_efs"
            r11 = 1
            java.lang.Long r13 = java.lang.Long.valueOf(r11)
            r9.a(r8, r10, r13)
        L_0x0ad8:
            com.fossil.jx2 r9 = r8.g()
            com.fossil.bw2 r9 = (com.fossil.bw2) r9
            com.fossil.rp2 r9 = (com.fossil.rp2) r9
            r3.add(r9)
        L_0x0ae3:
            r6.a(r7, r8)
        L_0x0ae6:
            r44 = r4
            r15 = r5
            r5 = r6
            r1 = r7
            goto L_0x0d36
        L_0x0aed:
            com.fossil.ih3 r9 = r43.i()
            com.fossil.vp2 r11 = r4.a
            java.lang.String r11 = r11.w0()
            long r11 = r9.f(r11)
            com.fossil.oh3 r9 = r1.i
            r9.v()
            long r13 = r8.zzf()
            long r13 = com.fossil.jm3.a(r13, r11)
            com.fossil.jx2 r9 = r8.g()
            com.fossil.bw2 r9 = (com.fossil.bw2) r9
            com.fossil.rp2 r9 = (com.fossil.rp2) r9
            java.lang.String r15 = "_dbg"
            r20 = r11
            r16 = 1
            java.lang.Long r11 = java.lang.Long.valueOf(r16)
            boolean r12 = android.text.TextUtils.isEmpty(r15)
            if (r12 != 0) goto L_0x0b7a
            if (r11 != 0) goto L_0x0b23
            goto L_0x0b7a
        L_0x0b23:
            java.util.List r9 = r9.zza()
            java.util.Iterator r9 = r9.iterator()
        L_0x0b2b:
            boolean r12 = r9.hasNext()
            if (r12 == 0) goto L_0x0b7a
            java.lang.Object r12 = r9.next()
            com.fossil.tp2 r12 = (com.fossil.tp2) r12
            r44 = r9
            java.lang.String r9 = r12.p()
            boolean r9 = r15.equals(r9)
            if (r9 == 0) goto L_0x0b77
            boolean r9 = r11 instanceof java.lang.Long
            if (r9 == 0) goto L_0x0b55
            long r15 = r12.t()
            java.lang.Long r9 = java.lang.Long.valueOf(r15)
            boolean r9 = r11.equals(r9)
            if (r9 != 0) goto L_0x0b75
        L_0x0b55:
            boolean r9 = r11 instanceof java.lang.String
            if (r9 == 0) goto L_0x0b63
            java.lang.String r9 = r12.r()
            boolean r9 = r11.equals(r9)
            if (r9 != 0) goto L_0x0b75
        L_0x0b63:
            boolean r9 = r11 instanceof java.lang.Double
            if (r9 == 0) goto L_0x0b7a
            double r15 = r12.x()
            java.lang.Double r9 = java.lang.Double.valueOf(r15)
            boolean r9 = r11.equals(r9)
            if (r9 == 0) goto L_0x0b7a
        L_0x0b75:
            r9 = 1
            goto L_0x0b7b
        L_0x0b77:
            r9 = r44
            goto L_0x0b2b
        L_0x0b7a:
            r9 = 0
        L_0x0b7b:
            if (r9 != 0) goto L_0x0b90
            com.fossil.ih3 r9 = r43.i()
            com.fossil.vp2 r11 = r4.a
            java.lang.String r11 = r11.w0()
            java.lang.String r12 = r8.q()
            int r9 = r9.d(r11, r12)
            goto L_0x0b91
        L_0x0b90:
            r9 = 1
        L_0x0b91:
            if (r9 > 0) goto L_0x0bba
            com.fossil.oh3 r10 = r1.i
            com.fossil.jg3 r10 = r10.e()
            com.fossil.mg3 r10 = r10.w()
            java.lang.String r11 = "Sample rate must be positive. event, rate"
            java.lang.String r12 = r8.q()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r10.a(r11, r12, r9)
            com.fossil.jx2 r9 = r8.g()
            com.fossil.bw2 r9 = (com.fossil.bw2) r9
            com.fossil.rp2 r9 = (com.fossil.rp2) r9
            r3.add(r9)
            r6.a(r7, r8)
            goto L_0x0ae6
        L_0x0bba:
            java.lang.String r11 = r8.q()
            java.lang.Object r11 = r2.get(r11)
            com.fossil.qb3 r11 = (com.fossil.qb3) r11
            if (r11 != 0) goto L_0x0c18
            com.fossil.jb3 r11 = r43.k()
            com.fossil.vp2 r12 = r4.a
            java.lang.String r12 = r12.w0()
            java.lang.String r15 = r8.q()
            com.fossil.qb3 r11 = r11.a(r12, r15)
            if (r11 != 0) goto L_0x0c18
            com.fossil.oh3 r11 = r1.i
            com.fossil.jg3 r11 = r11.e()
            com.fossil.mg3 r11 = r11.w()
            java.lang.String r12 = "Event being bundled has no eventAggregate. appId, eventName"
            com.fossil.vp2 r15 = r4.a
            java.lang.String r15 = r15.w0()
            java.lang.String r1 = r8.q()
            r11.a(r12, r15, r1)
            com.fossil.qb3 r11 = new com.fossil.qb3
            com.fossil.vp2 r1 = r4.a
            java.lang.String r27 = r1.w0()
            java.lang.String r28 = r8.q()
            r29 = 1
            r31 = 1
            r33 = 1
            long r35 = r8.zzf()
            r37 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r26 = r11
            r26.<init>(r27, r28, r29, r31, r33, r35, r37, r39, r40, r41, r42)
        L_0x0c18:
            com.fossil.fm3 r1 = r43.n()
            com.fossil.jx2 r12 = r8.g()
            com.fossil.bw2 r12 = (com.fossil.bw2) r12
            com.fossil.rp2 r12 = (com.fossil.rp2) r12
            java.lang.String r15 = "_eid"
            java.lang.Object r1 = r1.a(r12, r15)
            java.lang.Long r1 = (java.lang.Long) r1
            if (r1 == 0) goto L_0x0c30
            r12 = 1
            goto L_0x0c31
        L_0x0c30:
            r12 = 0
        L_0x0c31:
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)
            r15 = 1
            if (r9 != r15) goto L_0x0c66
            com.fossil.jx2 r1 = r8.g()
            com.fossil.bw2 r1 = (com.fossil.bw2) r1
            com.fossil.rp2 r1 = (com.fossil.rp2) r1
            r3.add(r1)
            boolean r1 = r12.booleanValue()
            if (r1 == 0) goto L_0x0c61
            java.lang.Long r1 = r11.i
            if (r1 != 0) goto L_0x0c55
            java.lang.Long r1 = r11.j
            if (r1 != 0) goto L_0x0c55
            java.lang.Boolean r1 = r11.k
            if (r1 == 0) goto L_0x0c61
        L_0x0c55:
            r1 = 0
            com.fossil.qb3 r9 = r11.a(r1, r1, r1)
            java.lang.String r1 = r8.q()
            r2.put(r1, r9)
        L_0x0c61:
            r6.a(r7, r8)
            goto L_0x0ae6
        L_0x0c66:
            int r15 = r5.nextInt(r9)
            if (r15 != 0) goto L_0x0ca8
            com.fossil.fm3 r1 = r43.n()
            r44 = r4
            r15 = r5
            long r4 = (long) r9
            java.lang.Long r9 = java.lang.Long.valueOf(r4)
            r1.a(r8, r10, r9)
            com.fossil.jx2 r1 = r8.g()
            com.fossil.bw2 r1 = (com.fossil.bw2) r1
            com.fossil.rp2 r1 = (com.fossil.rp2) r1
            r3.add(r1)
            boolean r1 = r12.booleanValue()
            if (r1 == 0) goto L_0x0c95
            java.lang.Long r1 = java.lang.Long.valueOf(r4)
            r4 = 0
            com.fossil.qb3 r11 = r11.a(r4, r1, r4)
        L_0x0c95:
            java.lang.String r1 = r8.q()
            long r4 = r8.zzf()
            com.fossil.qb3 r4 = r11.a(r4, r13)
            r2.put(r1, r4)
            r5 = r6
            r1 = r7
            goto L_0x0d33
        L_0x0ca8:
            r44 = r4
            r15 = r5
            java.lang.Long r4 = r11.h
            if (r4 == 0) goto L_0x0cba
            java.lang.Long r4 = r11.h
            long r4 = r4.longValue()
            r30 = r6
            r16 = r7
            goto L_0x0ccf
        L_0x0cba:
            r4 = r43
            com.fossil.oh3 r5 = r4.i
            r5.v()
            long r4 = r8.r()
            r30 = r6
            r16 = r7
            r6 = r20
            long r4 = com.fossil.jm3.a(r4, r6)
        L_0x0ccf:
            int r6 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r6 == 0) goto L_0x0d1d
            com.fossil.fm3 r1 = r43.n()
            java.lang.String r4 = "_efs"
            r5 = 1
            java.lang.Long r7 = java.lang.Long.valueOf(r5)
            r1.a(r8, r4, r7)
            com.fossil.fm3 r1 = r43.n()
            long r5 = (long) r9
            java.lang.Long r4 = java.lang.Long.valueOf(r5)
            r1.a(r8, r10, r4)
            com.fossil.jx2 r1 = r8.g()
            com.fossil.bw2 r1 = (com.fossil.bw2) r1
            com.fossil.rp2 r1 = (com.fossil.rp2) r1
            r3.add(r1)
            boolean r1 = r12.booleanValue()
            if (r1 == 0) goto L_0x0d0d
            java.lang.Long r1 = java.lang.Long.valueOf(r5)
            r4 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r4)
            r4 = 0
            com.fossil.qb3 r11 = r11.a(r4, r1, r5)
        L_0x0d0d:
            java.lang.String r1 = r8.q()
            long r4 = r8.zzf()
            com.fossil.qb3 r4 = r11.a(r4, r13)
            r2.put(r1, r4)
            goto L_0x0d2f
        L_0x0d1d:
            boolean r4 = r12.booleanValue()
            if (r4 == 0) goto L_0x0d2f
            java.lang.String r4 = r8.q()
            r5 = 0
            com.fossil.qb3 r1 = r11.a(r1, r5, r5)
            r2.put(r4, r1)
        L_0x0d2f:
            r1 = r16
            r5 = r30
        L_0x0d33:
            r5.a(r1, r8)
        L_0x0d36:
            int r7 = r1 + 1
            r1 = r43
            r4 = r44
            r6 = r5
            r5 = r15
            goto L_0x0a59
        L_0x0d40:
            r44 = r4
            r5 = r6
            int r1 = r3.size()
            int r4 = r5.o()
            if (r1 >= r4) goto L_0x0d53
            r5.p()
            r5.a(r3)
        L_0x0d53:
            java.util.Set r1 = r2.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x0d5b:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0d7d
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            com.fossil.jb3 r3 = r43.k()
            java.lang.Object r2 = r2.getValue()
            com.fossil.qb3 r2 = (com.fossil.qb3) r2
            r3.a(r2)
            goto L_0x0d5b
        L_0x0d75:
            r0 = move-exception
            r1 = r43
            goto L_0x0f3b
        L_0x0d7a:
            r44 = r4
            r5 = r6
        L_0x0d7d:
            r1 = r43
            com.fossil.oh3 r2 = r1.i
            com.fossil.ym3 r2 = r2.o()
            java.lang.String r3 = r5.u()
            com.fossil.yf3<java.lang.Boolean> r4 = com.fossil.wb3.n0
            boolean r2 = r2.e(r3, r4)
            if (r2 != 0) goto L_0x0d94
            a(r5)
        L_0x0d94:
            r2 = r44
            com.fossil.vp2 r3 = r2.a
            java.lang.String r3 = r3.w0()
            com.fossil.jb3 r4 = r43.k()
            com.fossil.kg3 r4 = r4.b(r3)
            if (r4 != 0) goto L_0x0dc0
            com.fossil.oh3 r4 = r1.i
            com.fossil.jg3 r4 = r4.e()
            com.fossil.mg3 r4 = r4.t()
            java.lang.String r6 = "Bundling raw events w/o app info. appId"
            com.fossil.vp2 r7 = r2.a
            java.lang.String r7 = r7.w0()
            java.lang.Object r7 = com.fossil.jg3.a(r7)
            r4.a(r6, r7)
            goto L_0x0e1b
        L_0x0dc0:
            int r6 = r5.o()
            if (r6 <= 0) goto L_0x0e1b
            long r6 = r4.t()
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0dd4
            r5.e(r6)
            goto L_0x0dd7
        L_0x0dd4:
            r5.t()
        L_0x0dd7:
            long r8 = r4.s()
            r10 = 0
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0de2
            goto L_0x0de3
        L_0x0de2:
            r6 = r8
        L_0x0de3:
            int r8 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x0deb
            r5.d(r6)
            goto L_0x0dee
        L_0x0deb:
            r5.s()
        L_0x0dee:
            r4.E()
            long r6 = r4.B()
            int r7 = (int) r6
            r5.g(r7)
            long r6 = r5.zzf()
            r4.a(r6)
            long r6 = r5.r()
            r4.b(r6)
            java.lang.String r6 = r4.e()
            if (r6 == 0) goto L_0x0e11
            r5.j(r6)
            goto L_0x0e14
        L_0x0e11:
            r5.v()
        L_0x0e14:
            com.fossil.jb3 r6 = r43.k()
            r6.a(r4)
        L_0x0e1b:
            int r4 = r5.o()
            if (r4 <= 0) goto L_0x0e81
            com.fossil.oh3 r4 = r1.i
            r4.b()
            com.fossil.ih3 r4 = r43.i()
            com.fossil.vp2 r6 = r2.a
            java.lang.String r6 = r6.w0()
            com.fossil.mp2 r4 = r4.a(r6)
            if (r4 == 0) goto L_0x0e45
            boolean r6 = r4.zza()
            if (r6 != 0) goto L_0x0e3d
            goto L_0x0e45
        L_0x0e3d:
            long r6 = r4.p()
            r5.i(r6)
            goto L_0x0e70
        L_0x0e45:
            com.fossil.vp2 r4 = r2.a
            java.lang.String r4 = r4.B()
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 == 0) goto L_0x0e57
            r6 = -1
            r5.i(r6)
            goto L_0x0e70
        L_0x0e57:
            com.fossil.oh3 r4 = r1.i
            com.fossil.jg3 r4 = r4.e()
            com.fossil.mg3 r4 = r4.w()
            java.lang.String r6 = "Did not find measurement config or missing version info. appId"
            com.fossil.vp2 r7 = r2.a
            java.lang.String r7 = r7.w0()
            java.lang.Object r7 = com.fossil.jg3.a(r7)
            r4.a(r6, r7)
        L_0x0e70:
            com.fossil.jb3 r4 = r43.k()
            com.fossil.jx2 r5 = r5.g()
            com.fossil.bw2 r5 = (com.fossil.bw2) r5
            com.fossil.vp2 r5 = (com.fossil.vp2) r5
            r13 = r19
            r4.a(r5, r13)
        L_0x0e81:
            com.fossil.jb3 r4 = r43.k()
            java.util.List<java.lang.Long> r2 = r2.b
            com.fossil.a72.a(r2)
            r4.g()
            r4.q()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "rowid in ("
            r5.<init>(r6)
            r6 = 0
        L_0x0e98:
            int r7 = r2.size()
            if (r6 >= r7) goto L_0x0eb5
            if (r6 == 0) goto L_0x0ea5
            java.lang.String r7 = ","
            r5.append(r7)
        L_0x0ea5:
            java.lang.Object r7 = r2.get(r6)
            java.lang.Long r7 = (java.lang.Long) r7
            long r7 = r7.longValue()
            r5.append(r7)
            int r6 = r6 + 1
            goto L_0x0e98
        L_0x0eb5:
            java.lang.String r6 = ")"
            r5.append(r6)
            android.database.sqlite.SQLiteDatabase r6 = r4.u()
            java.lang.String r7 = "raw_events"
            java.lang.String r5 = r5.toString()
            r8 = 0
            int r5 = r6.delete(r7, r5, r8)
            int r6 = r2.size()
            if (r5 == r6) goto L_0x0ee8
            com.fossil.jg3 r4 = r4.e()
            com.fossil.mg3 r4 = r4.t()
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            int r2 = r2.size()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r4.a(r6, r5, r2)
        L_0x0ee8:
            com.fossil.jb3 r2 = r43.k()
            android.database.sqlite.SQLiteDatabase r4 = r2.u()
            java.lang.String r5 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0eff }
            r7 = 0
            r6[r7] = r3     // Catch:{ SQLiteException -> 0x0eff }
            r7 = 1
            r6[r7] = r3     // Catch:{ SQLiteException -> 0x0eff }
            r4.execSQL(r5, r6)     // Catch:{ SQLiteException -> 0x0eff }
            goto L_0x0f12
        L_0x0eff:
            r0 = move-exception
            r4 = r0
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r5 = "Failed to remove unused event metadata. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r3)
            r2.a(r5, r3, r4)
        L_0x0f12:
            com.fossil.jb3 r2 = r43.k()
            r2.t()
            com.fossil.jb3 r2 = r43.k()
            r2.z()
            r2 = 1
            return r2
        L_0x0f22:
            com.fossil.jb3 r2 = r43.k()
            r2.t()
            com.fossil.jb3 r2 = r43.k()
            r2.z()
            r2 = 0
            return r2
        L_0x0f32:
            r0 = move-exception
            r2 = r0
        L_0x0f34:
            if (r5 == 0) goto L_0x0f39
            r5.close()
        L_0x0f39:
            throw r2
        L_0x0f3a:
            r0 = move-exception
        L_0x0f3b:
            r2 = r0
            com.fossil.jb3 r3 = r43.k()
            r3.z()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.a(java.lang.String, long):boolean");
    }

    @DexIgnore
    public final void b(wm3 wm3) {
        nm3 a2 = a(wm3.a);
        if (a2 != null) {
            b(wm3, a2);
        }
    }

    @DexIgnore
    public final void b(wm3 wm3, nm3 nm3) {
        a72.a(wm3);
        a72.b(wm3.a);
        a72.a(wm3.c);
        a72.b(wm3.c.b);
        x();
        q();
        if (e(nm3)) {
            if (!nm3.h) {
                c(nm3);
                return;
            }
            k().y();
            try {
                c(nm3);
                wm3 d2 = k().d(wm3.a, wm3.c.b);
                if (d2 != null) {
                    this.i.e().A().a("Removing conditional user property", wm3.a, this.i.w().c(wm3.c.b));
                    k().e(wm3.a, wm3.c.b);
                    if (d2.e) {
                        k().b(wm3.a, wm3.c.b);
                    }
                    if (wm3.p != null) {
                        Bundle bundle = null;
                        if (wm3.p.b != null) {
                            bundle = wm3.p.b.zzb();
                        }
                        c(this.i.v().a(wm3.a, wm3.p.a, bundle, d2.b, wm3.p.d, true, false), nm3);
                    }
                } else {
                    this.i.e().w().a("Conditional user property doesn't exist", jg3.a(wm3.a), this.i.w().c(wm3.c.b));
                }
                k().t();
            } finally {
                k().z();
            }
        }
    }

    @DexIgnore
    public final kg3 c(nm3 nm3) {
        x();
        q();
        a72.a(nm3);
        a72.b(nm3.a);
        kg3 b2 = k().b(nm3.a);
        String b3 = this.i.p().b(nm3.a);
        if (!o13.a() || !this.i.o().a(wb3.u0)) {
            return a(nm3, b2, b3);
        }
        if (b2 == null) {
            b2 = new kg3(this.i, nm3.a);
            b2.a(this.i.v().v());
            b2.e(b3);
        } else if (!b3.equals(b2.q())) {
            b2.e(b3);
            b2.a(this.i.v().v());
        }
        b2.b(nm3.b);
        b2.c(nm3.w);
        if (f23.a() && this.i.o().e(b2.l(), wb3.o0)) {
            b2.d(nm3.A);
        }
        if (!TextUtils.isEmpty(nm3.p)) {
            b2.f(nm3.p);
        }
        long j2 = nm3.e;
        if (j2 != 0) {
            b2.d(j2);
        }
        if (!TextUtils.isEmpty(nm3.c)) {
            b2.g(nm3.c);
        }
        b2.c(nm3.j);
        String str = nm3.d;
        if (str != null) {
            b2.h(str);
        }
        b2.e(nm3.f);
        b2.a(nm3.h);
        if (!TextUtils.isEmpty(nm3.g)) {
            b2.i(nm3.g);
        }
        if (!this.i.o().a(wb3.M0)) {
            b2.p(nm3.q);
        }
        b2.b(nm3.t);
        b2.c(nm3.u);
        b2.a(nm3.x);
        b2.f(nm3.y);
        if (b2.a()) {
            k().a(b2);
        }
        return b2;
    }

    @DexIgnore
    public static void a(vp2.a aVar) {
        aVar.b(Long.MAX_VALUE);
        aVar.c(Long.MIN_VALUE);
        for (int i2 = 0; i2 < aVar.o(); i2++) {
            rp2 b2 = aVar.b(i2);
            if (b2.s() < aVar.zzf()) {
                aVar.b(b2.s());
            }
            if (b2.s() > aVar.r()) {
                aVar.c(b2.s());
            }
        }
    }

    @DexIgnore
    public final void a(vp2.a aVar, long j2, boolean z) {
        gm3 gm3;
        String str = z ? "_se" : "_lte";
        gm3 c2 = k().c(aVar.u(), str);
        if (c2 == null || c2.e == null) {
            gm3 = new gm3(aVar.u(), "auto", str, this.i.zzm().b(), Long.valueOf(j2));
        } else {
            gm3 = new gm3(aVar.u(), "auto", str, this.i.zzm().b(), Long.valueOf(((Long) c2.e).longValue() + j2));
        }
        zp2.a A = zp2.A();
        A.a(str);
        A.a(this.i.zzm().b());
        A.b(((Long) gm3.e).longValue());
        zp2 zp2 = (zp2) ((bw2) A.g());
        boolean z2 = false;
        int a2 = fm3.a(aVar, str);
        if (a2 >= 0) {
            aVar.a(a2, zp2);
            z2 = true;
        }
        if (!z2) {
            aVar.a(zp2);
        }
        if (j2 > 0) {
            k().a(gm3);
            this.i.e().B().a("Updated engagement user property. scope, value", z ? "session-scoped" : "lifetime", gm3.e);
        }
    }

    @DexIgnore
    public final boolean a(rp2.a aVar, rp2.a aVar2) {
        String str;
        a72.a("_e".equals(aVar.q()));
        n();
        tp2 b2 = fm3.b((rp2) ((bw2) aVar.g()), "_sc");
        String str2 = null;
        if (b2 == null) {
            str = null;
        } else {
            str = b2.r();
        }
        n();
        tp2 b3 = fm3.b((rp2) ((bw2) aVar2.g()), "_pc");
        if (b3 != null) {
            str2 = b3.r();
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        b(aVar, aVar2);
        return true;
    }

    @DexIgnore
    public static void a(rp2.a aVar, String str) {
        List<tp2> zza = aVar.zza();
        for (int i2 = 0; i2 < zza.size(); i2++) {
            if (str.equals(zza.get(i2).p())) {
                aVar.b(i2);
                return;
            }
        }
    }

    @DexIgnore
    public static void a(rp2.a aVar, int i2, String str) {
        List<tp2> zza = aVar.zza();
        int i3 = 0;
        while (i3 < zza.size()) {
            if (!"_err".equals(zza.get(i3).p())) {
                i3++;
            } else {
                return;
            }
        }
        tp2.a F = tp2.F();
        F.a("_err");
        F.a(Long.valueOf((long) i2).longValue());
        tp2.a F2 = tp2.F();
        F2.a("_ev");
        F2.b(str);
        aVar.a((tp2) ((bw2) F.g()));
        aVar.a((tp2) ((bw2) F2.g()));
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        x();
        q();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.q = false;
                d();
                throw th2;
            }
        }
        List<Long> list = this.u;
        this.u = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.i.p().e.a(this.i.zzm().b());
                this.i.p().f.a(0);
                A();
                this.i.e().B().a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                k().y();
                try {
                    for (Long l2 : list) {
                        try {
                            jb3 k2 = k();
                            long longValue = l2.longValue();
                            k2.g();
                            k2.q();
                            try {
                                if (k2.u().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                    throw new SQLiteException("Deleted fewer rows from queue than expected");
                                }
                            } catch (SQLiteException e2) {
                                k2.e().t().a("Failed to delete a bundle in a queue table", e2);
                                throw e2;
                            }
                        } catch (SQLiteException e3) {
                            if (this.v == null || !this.v.contains(l2)) {
                                throw e3;
                            }
                        }
                    }
                    k().t();
                    k().z();
                    this.v = null;
                    if (!j().t() || !z()) {
                        this.w = -1;
                        A();
                    } else {
                        r();
                    }
                    this.l = 0;
                } catch (Throwable th3) {
                    k().z();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.i.e().t().a("Database error while trying to delete uploaded bundles", e4);
                this.l = this.i.zzm().c();
                this.i.e().B().a("Disable upload, time", Long.valueOf(this.l));
            }
        } else {
            this.i.e().B().a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.i.p().f.a(this.i.zzm().b());
            if (!(i2 == 503 || i2 == 429)) {
                z = false;
            }
            if (z) {
                this.i.p().g.a(this.i.zzm().b());
            }
            k().a(list);
            A();
        }
        this.q = false;
        d();
    }

    @DexIgnore
    public final void a(kg3 kg3) {
        n4 n4Var;
        x();
        if (!f23.a() || !this.i.o().e(kg3.l(), wb3.o0)) {
            if (TextUtils.isEmpty(kg3.n()) && TextUtils.isEmpty(kg3.o())) {
                a(kg3.l(), 204, null, null, null);
                return;
            }
        } else if (TextUtils.isEmpty(kg3.n()) && TextUtils.isEmpty(kg3.p()) && TextUtils.isEmpty(kg3.o())) {
            a(kg3.l(), 204, null, null, null);
            return;
        }
        String a2 = this.i.o().a(kg3);
        try {
            URL url = new URL(a2);
            this.i.e().B().a("Fetching remote configuration", kg3.l());
            mp2 a3 = i().a(kg3.l());
            String b2 = i().b(kg3.l());
            if (a3 == null || TextUtils.isEmpty(b2)) {
                n4Var = null;
            } else {
                n4 n4Var2 = new n4();
                n4Var2.put("If-Modified-Since", b2);
                n4Var = n4Var2;
            }
            this.p = true;
            ng3 j2 = j();
            String l2 = kg3.l();
            cm3 cm3 = new cm3(this);
            j2.g();
            j2.q();
            a72.a(url);
            a72.a(cm3);
            j2.c().b(new rg3(j2, l2, url, null, n4Var, cm3));
        } catch (MalformedURLException unused) {
            this.i.e().t().a("Failed to parse config URL. Not fetching. appId", jg3.a(kg3.l()), a2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x014a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r7, int r8, java.lang.Throwable r9, byte[] r10, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r11) {
        /*
            r6 = this;
            r6.x()
            r6.q()
            com.fossil.a72.b(r7)
            r0 = 0
            if (r10 != 0) goto L_0x000e
            byte[] r10 = new byte[r0]     // Catch:{ all -> 0x0196 }
        L_0x000e:
            com.fossil.oh3 r1 = r6.i     // Catch:{ all -> 0x0196 }
            com.fossil.jg3 r1 = r1.e()     // Catch:{ all -> 0x0196 }
            com.fossil.mg3 r1 = r1.B()     // Catch:{ all -> 0x0196 }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r10.length     // Catch:{ all -> 0x0196 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0196 }
            r1.a(r2, r3)     // Catch:{ all -> 0x0196 }
            com.fossil.jb3 r1 = r6.k()     // Catch:{ all -> 0x0196 }
            r1.y()     // Catch:{ all -> 0x0196 }
            com.fossil.jb3 r1 = r6.k()     // Catch:{ all -> 0x018d }
            com.fossil.kg3 r1 = r1.b(r7)     // Catch:{ all -> 0x018d }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 304(0x130, float:4.26E-43)
            r4 = 1
            if (r8 == r2) goto L_0x003e
            r2 = 204(0xcc, float:2.86E-43)
            if (r8 == r2) goto L_0x003e
            if (r8 != r3) goto L_0x0042
        L_0x003e:
            if (r9 != 0) goto L_0x0042
            r2 = 1
            goto L_0x0043
        L_0x0042:
            r2 = 0
        L_0x0043:
            if (r1 != 0) goto L_0x005a
            com.fossil.oh3 r8 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.jg3 r8 = r8.e()     // Catch:{ all -> 0x018d }
            com.fossil.mg3 r8 = r8.w()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r7 = com.fossil.jg3.a(r7)     // Catch:{ all -> 0x018d }
            r8.a(r9, r7)     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x005a:
            r5 = 404(0x194, float:5.66E-43)
            if (r2 != 0) goto L_0x00ca
            if (r8 != r5) goto L_0x0061
            goto L_0x00ca
        L_0x0061:
            com.fossil.oh3 r10 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.n92 r10 = r10.zzm()     // Catch:{ all -> 0x018d }
            long r10 = r10.b()     // Catch:{ all -> 0x018d }
            r1.i(r10)     // Catch:{ all -> 0x018d }
            com.fossil.jb3 r10 = r6.k()     // Catch:{ all -> 0x018d }
            r10.a(r1)     // Catch:{ all -> 0x018d }
            com.fossil.oh3 r10 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.jg3 r10 = r10.e()     // Catch:{ all -> 0x018d }
            com.fossil.mg3 r10 = r10.B()     // Catch:{ all -> 0x018d }
            java.lang.String r11 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x018d }
            r10.a(r11, r1, r9)     // Catch:{ all -> 0x018d }
            com.fossil.ih3 r9 = r6.i()     // Catch:{ all -> 0x018d }
            r9.c(r7)     // Catch:{ all -> 0x018d }
            com.fossil.oh3 r7 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.wg3 r7 = r7.p()     // Catch:{ all -> 0x018d }
            com.fossil.ah3 r7 = r7.f     // Catch:{ all -> 0x018d }
            com.fossil.oh3 r9 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.n92 r9 = r9.zzm()     // Catch:{ all -> 0x018d }
            long r9 = r9.b()     // Catch:{ all -> 0x018d }
            r7.a(r9)     // Catch:{ all -> 0x018d }
            r7 = 503(0x1f7, float:7.05E-43)
            if (r8 == r7) goto L_0x00ae
            r7 = 429(0x1ad, float:6.01E-43)
            if (r8 != r7) goto L_0x00ad
            goto L_0x00ae
        L_0x00ad:
            r4 = 0
        L_0x00ae:
            if (r4 == 0) goto L_0x00c5
            com.fossil.oh3 r7 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.wg3 r7 = r7.p()     // Catch:{ all -> 0x018d }
            com.fossil.ah3 r7 = r7.g     // Catch:{ all -> 0x018d }
            com.fossil.oh3 r8 = r6.i     // Catch:{ all -> 0x018d }
            com.fossil.n92 r8 = r8.zzm()     // Catch:{ all -> 0x018d }
            long r8 = r8.b()     // Catch:{ all -> 0x018d }
            r7.a(r8)     // Catch:{ all -> 0x018d }
        L_0x00c5:
            r6.A()     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x00ca:
            r9 = 0
            if (r11 == 0) goto L_0x00d6
            java.lang.String r2 = "Last-Modified"
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x018d }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x018d }
            goto L_0x00d7
        L_0x00d6:
            r11 = r9
        L_0x00d7:
            if (r11 == 0) goto L_0x00e6
            int r2 = r11.size()     // Catch:{ all -> 0x018d }
            if (r2 <= 0) goto L_0x00e6
            java.lang.Object r11 = r11.get(r0)     // Catch:{ all -> 0x018d }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x018d }
            goto L_0x00e7
        L_0x00e6:
            r11 = r9
        L_0x00e7:
            if (r8 == r5) goto L_0x0103
            if (r8 != r3) goto L_0x00ec
            goto L_0x0103
        L_0x00ec:
            com.fossil.ih3 r9 = r6.i()     // Catch:{ all -> 0x018d }
            boolean r9 = r9.a(r7, r10, r11)     // Catch:{ all -> 0x018d }
            if (r9 != 0) goto L_0x0124
            com.fossil.jb3 r7 = r6.k()
            r7.z()
            r6.p = r0
            r6.d()
            return
        L_0x0103:
            com.fossil.ih3 r11 = r6.i()
            com.fossil.mp2 r11 = r11.a(r7)
            if (r11 != 0) goto L_0x0124
            com.fossil.ih3 r11 = r6.i()
            boolean r9 = r11.a(r7, r9, r9)
            if (r9 != 0) goto L_0x0124
            com.fossil.jb3 r7 = r6.k()
            r7.z()
            r6.p = r0
            r6.d()
            return
        L_0x0124:
            com.fossil.oh3 r9 = r6.i
            com.fossil.n92 r9 = r9.zzm()
            long r2 = r9.b()
            r1.h(r2)
            com.fossil.jb3 r9 = r6.k()
            r9.a(r1)
            if (r8 != r5) goto L_0x014a
            com.fossil.oh3 r8 = r6.i
            com.fossil.jg3 r8 = r8.e()
            com.fossil.mg3 r8 = r8.y()
            java.lang.String r9 = "Config not found. Using empty config. appId"
            r8.a(r9, r7)
            goto L_0x0162
        L_0x014a:
            com.fossil.oh3 r7 = r6.i
            com.fossil.jg3 r7 = r7.e()
            com.fossil.mg3 r7 = r7.B()
            java.lang.String r9 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            int r10 = r10.length
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            r7.a(r9, r8, r10)
        L_0x0162:
            com.fossil.ng3 r7 = r6.j()
            boolean r7 = r7.t()
            if (r7 == 0) goto L_0x0176
            boolean r7 = r6.z()
            if (r7 == 0) goto L_0x0176
            r6.r()
            goto L_0x0179
        L_0x0176:
            r6.A()
        L_0x0179:
            com.fossil.jb3 r7 = r6.k()
            r7.t()
            com.fossil.jb3 r7 = r6.k()
            r7.z()
            r6.p = r0
            r6.d()
            return
        L_0x018d:
            r7 = move-exception
            com.fossil.jb3 r8 = r6.k()
            r8.z()
            throw r7
        L_0x0196:
            r7 = move-exception
            r6.p = r0
            r6.d()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.a(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        x();
        if (this.m == null) {
            this.m = new ArrayList();
        }
        this.m.add(runnable);
    }

    @DexIgnore
    public final int a(FileChannel fileChannel) {
        x();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.e().t().a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.i.e().w().a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e2) {
            this.i.e().t().a("Failed to read from channel", e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean a(int i2, FileChannel fileChannel) {
        x();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.e().t().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            if (this.i.o().a(wb3.z0) && Build.VERSION.SDK_INT <= 19) {
                fileChannel.position(0L);
            }
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.i.e().t().a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e2) {
            this.i.e().t().a("Failed to write to channel", e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(nm3 nm3) {
        if (this.u != null) {
            ArrayList arrayList = new ArrayList();
            this.v = arrayList;
            arrayList.addAll(this.u);
        }
        jb3 k2 = k();
        String str = nm3.a;
        a72.b(str);
        k2.g();
        k2.q();
        try {
            SQLiteDatabase u2 = k2.u();
            String[] strArr = {str};
            int delete = u2.delete("apps", "app_id=?", strArr) + 0 + u2.delete("events", "app_id=?", strArr) + u2.delete("user_attributes", "app_id=?", strArr) + u2.delete("conditional_properties", "app_id=?", strArr) + u2.delete("raw_events", "app_id=?", strArr) + u2.delete("raw_events_metadata", "app_id=?", strArr) + u2.delete("queue", "app_id=?", strArr) + u2.delete("audience_filter_values", "app_id=?", strArr) + u2.delete("main_event_params", "app_id=?", strArr) + u2.delete("default_event_params", "app_id=?", strArr);
            if (delete > 0) {
                k2.e().B().a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            k2.e().t().a("Error resetting analytics data. appId, error", jg3.a(str), e2);
        }
        if (nm3.h) {
            b(nm3);
        }
    }

    @DexIgnore
    public final void a(em3 em3, nm3 nm3) {
        x();
        q();
        if (e(nm3)) {
            if (!nm3.h) {
                c(nm3);
                return;
            }
            int b2 = this.i.v().b(em3.b);
            if (b2 != 0) {
                this.i.v();
                String a2 = jm3.a(em3.b, 24, true);
                String str = em3.b;
                this.i.v().a(nm3.a, b2, "_ev", a2, str != null ? str.length() : 0);
                return;
            }
            int b3 = this.i.v().b(em3.b, em3.zza());
            if (b3 != 0) {
                this.i.v();
                String a3 = jm3.a(em3.b, 24, true);
                Object zza = em3.zza();
                this.i.v().a(nm3.a, b3, "_ev", a3, (zza == null || (!(zza instanceof String) && !(zza instanceof CharSequence))) ? 0 : String.valueOf(zza).length());
                return;
            }
            Object c2 = this.i.v().c(em3.b, em3.zza());
            if (c2 != null) {
                if ("_sid".equals(em3.b)) {
                    long j2 = em3.c;
                    String str2 = em3.f;
                    long j3 = 0;
                    gm3 c3 = k().c(nm3.a, "_sno");
                    if (c3 != null) {
                        Object obj = c3.e;
                        if (obj instanceof Long) {
                            j3 = ((Long) obj).longValue();
                            a(new em3("_sno", j2, Long.valueOf(j3 + 1), str2), nm3);
                        }
                    }
                    if (c3 != null) {
                        this.i.e().w().a("Retrieved last session number from database does not contain a valid (long) value", c3.e);
                    }
                    qb3 a4 = k().a(nm3.a, "_s");
                    if (a4 != null) {
                        j3 = a4.c;
                        this.i.e().B().a("Backfill the session number. Last used session number", Long.valueOf(j3));
                    }
                    a(new em3("_sno", j2, Long.valueOf(j3 + 1), str2), nm3);
                }
                gm3 gm3 = new gm3(nm3.a, em3.f, em3.b, em3.c, c2);
                this.i.e().B().a("Setting user property", this.i.w().c(gm3.c), c2);
                k().y();
                try {
                    c(nm3);
                    boolean a5 = k().a(gm3);
                    k().t();
                    if (!a5) {
                        this.i.e().t().a("Too many unique user properties are set. Ignoring user property", this.i.w().c(gm3.c), gm3.e);
                        this.i.v().a(nm3.a, 9, (String) null, (String) null, 0);
                    }
                } finally {
                    k().z();
                }
            }
        }
    }

    @DexIgnore
    public final void a(yl3 yl3) {
        this.n++;
    }

    @DexIgnore
    public final nm3 a(String str) {
        kg3 b2 = k().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.u())) {
            this.i.e().A().a("No app data available; dropping", str);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            return new nm3(str, b2.n(), b2.u(), b2.v(), b2.w(), b2.x(), b2.y(), (String) null, b2.A(), false, b2.r(), b2.f(), 0L, 0, b2.g(), b2.h(), false, b2.o(), b2.i(), b2.z(), b2.j(), (!f23.a() || !this.i.o().e(str, wb3.o0)) ? null : b2.p());
        }
        this.i.e().t().a("App version does not match; dropping. appId", jg3.a(str));
        return null;
    }

    @DexIgnore
    public final void a(wm3 wm3) {
        nm3 a2 = a(wm3.a);
        if (a2 != null) {
            a(wm3, a2);
        }
    }

    @DexIgnore
    public final void a(wm3 wm3, nm3 nm3) {
        a72.a(wm3);
        a72.b(wm3.a);
        a72.a((Object) wm3.b);
        a72.a(wm3.c);
        a72.b(wm3.c.b);
        x();
        q();
        if (e(nm3)) {
            if (!nm3.h) {
                c(nm3);
                return;
            }
            wm3 wm32 = new wm3(wm3);
            boolean z = false;
            wm32.e = false;
            k().y();
            try {
                wm3 d2 = k().d(wm32.a, wm32.c.b);
                if (d2 != null && !d2.b.equals(wm32.b)) {
                    this.i.e().w().a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.i.w().c(wm32.c.b), wm32.b, d2.b);
                }
                if (d2 != null && d2.e) {
                    wm32.b = d2.b;
                    wm32.d = d2.d;
                    wm32.h = d2.h;
                    wm32.f = d2.f;
                    wm32.i = d2.i;
                    wm32.e = d2.e;
                    wm32.c = new em3(wm32.c.b, d2.c.c, wm32.c.zza(), d2.c.f);
                } else if (TextUtils.isEmpty(wm32.f)) {
                    wm32.c = new em3(wm32.c.b, wm32.d, wm32.c.zza(), wm32.c.f);
                    wm32.e = true;
                    z = true;
                }
                if (wm32.e) {
                    em3 em3 = wm32.c;
                    gm3 gm3 = new gm3(wm32.a, wm32.b, em3.b, em3.c, em3.zza());
                    if (k().a(gm3)) {
                        this.i.e().A().a("User property updated immediately", wm32.a, this.i.w().c(gm3.c), gm3.e);
                    } else {
                        this.i.e().t().a("(2)Too many active user properties, ignoring", jg3.a(wm32.a), this.i.w().c(gm3.c), gm3.e);
                    }
                    if (z && wm32.i != null) {
                        c(new ub3(wm32.i, wm32.d), nm3);
                    }
                }
                if (k().a(wm32)) {
                    this.i.e().A().a("Conditional property added", wm32.a, this.i.w().c(wm32.c.b), wm32.c.zza());
                } else {
                    this.i.e().t().a("Too many conditional properties, ignoring", jg3.a(wm32.a), this.i.w().c(wm32.c.b), wm32.c.zza());
                }
                k().t();
            } finally {
                k().z();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.kg3 a(com.fossil.nm3 r9, com.fossil.kg3 r10, java.lang.String r11) {
        /*
            r8 = this;
            r0 = 1
            if (r10 != 0) goto L_0x001e
            com.fossil.kg3 r10 = new com.fossil.kg3
            com.fossil.oh3 r1 = r8.i
            java.lang.String r2 = r9.a
            r10.<init>(r1, r2)
            com.fossil.oh3 r1 = r8.i
            com.fossil.jm3 r1 = r1.v()
            java.lang.String r1 = r1.v()
            r10.a(r1)
            r10.e(r11)
        L_0x001c:
            r11 = 1
            goto L_0x003a
        L_0x001e:
            java.lang.String r1 = r10.q()
            boolean r1 = r11.equals(r1)
            if (r1 != 0) goto L_0x0039
            r10.e(r11)
            com.fossil.oh3 r11 = r8.i
            com.fossil.jm3 r11 = r11.v()
            java.lang.String r11 = r11.v()
            r10.a(r11)
            goto L_0x001c
        L_0x0039:
            r11 = 0
        L_0x003a:
            java.lang.String r1 = r9.b
            java.lang.String r2 = r10.n()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x004c
            java.lang.String r11 = r9.b
            r10.b(r11)
            r11 = 1
        L_0x004c:
            java.lang.String r1 = r9.w
            java.lang.String r2 = r10.o()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x005e
            java.lang.String r11 = r9.w
            r10.c(r11)
            r11 = 1
        L_0x005e:
            boolean r1 = com.fossil.f23.a()
            if (r1 == 0) goto L_0x0088
            com.fossil.oh3 r1 = r8.i
            com.fossil.ym3 r1 = r1.o()
            java.lang.String r2 = r10.l()
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.o0
            boolean r1 = r1.e(r2, r3)
            if (r1 == 0) goto L_0x0088
            java.lang.String r1 = r9.A
            java.lang.String r2 = r10.p()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x0088
            java.lang.String r11 = r9.A
            r10.d(r11)
            r11 = 1
        L_0x0088:
            java.lang.String r1 = r9.p
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r1 = r9.p
            java.lang.String r2 = r10.r()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r11 = r9.p
            r10.f(r11)
            r11 = 1
        L_0x00a2:
            long r1 = r9.e
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00b8
            long r5 = r10.x()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00b8
            long r1 = r9.e
            r10.d(r1)
            r11 = 1
        L_0x00b8:
            java.lang.String r1 = r9.c
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00d2
            java.lang.String r1 = r9.c
            java.lang.String r2 = r10.u()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00d2
            java.lang.String r11 = r9.c
            r10.g(r11)
            r11 = 1
        L_0x00d2:
            long r1 = r9.j
            long r5 = r10.v()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00e2
            long r1 = r9.j
            r10.c(r1)
            r11 = 1
        L_0x00e2:
            java.lang.String r1 = r9.d
            if (r1 == 0) goto L_0x00f6
            java.lang.String r2 = r10.w()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00f6
            java.lang.String r11 = r9.d
            r10.h(r11)
            r11 = 1
        L_0x00f6:
            long r1 = r9.f
            long r5 = r10.y()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0106
            long r1 = r9.f
            r10.e(r1)
            r11 = 1
        L_0x0106:
            boolean r1 = r9.h
            boolean r2 = r10.A()
            if (r1 == r2) goto L_0x0114
            boolean r11 = r9.h
            r10.a(r11)
            r11 = 1
        L_0x0114:
            java.lang.String r1 = r9.g
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x012e
            java.lang.String r1 = r9.g
            java.lang.String r2 = r10.d()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x012e
            java.lang.String r11 = r9.g
            r10.i(r11)
            r11 = 1
        L_0x012e:
            com.fossil.oh3 r1 = r8.i
            com.fossil.ym3 r1 = r1.o()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.M0
            boolean r1 = r1.a(r2)
            if (r1 != 0) goto L_0x014c
            long r1 = r9.q
            long r5 = r10.f()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x014c
            long r1 = r9.q
            r10.p(r1)
            r11 = 1
        L_0x014c:
            boolean r1 = r9.t
            boolean r2 = r10.g()
            if (r1 == r2) goto L_0x015a
            boolean r11 = r9.t
            r10.b(r11)
            r11 = 1
        L_0x015a:
            boolean r1 = r9.u
            boolean r2 = r10.h()
            if (r1 == r2) goto L_0x0168
            boolean r11 = r9.u
            r10.c(r11)
            r11 = 1
        L_0x0168:
            java.lang.Boolean r1 = r9.x
            java.lang.Boolean r2 = r10.i()
            if (r1 == r2) goto L_0x0176
            java.lang.Boolean r11 = r9.x
            r10.a(r11)
            r11 = 1
        L_0x0176:
            long r1 = r9.y
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x018a
            long r3 = r10.z()
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x018a
            long r1 = r9.y
            r10.f(r1)
            goto L_0x018b
        L_0x018a:
            r0 = r11
        L_0x018b:
            if (r0 == 0) goto L_0x0194
            com.fossil.jb3 r9 = r8.k()
            r9.a(r10)
        L_0x0194:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl3.a(com.fossil.nm3, com.fossil.kg3, java.lang.String):com.fossil.kg3");
    }

    @DexIgnore
    public final void a(boolean z) {
        A();
    }
}
