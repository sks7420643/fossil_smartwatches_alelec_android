package com.fossil;

import com.fossil.cv1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu1 extends cv1 {
    @DexIgnore
    public /* final */ Iterable<ku1> a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    @Override // com.fossil.cv1
    public Iterable<ku1> a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.cv1
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cv1)) {
            return false;
        }
        cv1 cv1 = (cv1) obj;
        if (this.a.equals(cv1.a())) {
            if (Arrays.equals(this.b, cv1 instanceof xu1 ? ((xu1) cv1).b : cv1.b())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "BackendRequest{events=" + this.a + ", extras=" + Arrays.toString(this.b) + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cv1.a {
        @DexIgnore
        public Iterable<ku1> a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.cv1.a
        public cv1.a a(Iterable<ku1> iterable) {
            if (iterable != null) {
                this.a = iterable;
                return this;
            }
            throw new NullPointerException("Null events");
        }

        @DexIgnore
        @Override // com.fossil.cv1.a
        public cv1.a a(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.cv1.a
        public cv1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " events";
            }
            if (str.isEmpty()) {
                return new xu1(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public xu1(Iterable<ku1> iterable, byte[] bArr) {
        this.a = iterable;
        this.b = bArr;
    }
}
