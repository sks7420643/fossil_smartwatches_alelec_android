package com.fossil;

import com.fossil.ac4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zb4 implements cc4 {
    @DexIgnore
    public /* final */ dc4 a;
    @DexIgnore
    public /* final */ oo3<ac4> b;

    @DexIgnore
    public zb4(dc4 dc4, oo3<ac4> oo3) {
        this.a = dc4;
        this.b = oo3;
    }

    @DexIgnore
    @Override // com.fossil.cc4
    public boolean a(hc4 hc4) {
        if (!hc4.j() || this.a.a(hc4)) {
            return false;
        }
        oo3<ac4> oo3 = this.b;
        ac4.a d = ac4.d();
        d.a(hc4.a());
        d.b(hc4.b());
        d.a(hc4.g());
        oo3.a(d.a());
        return true;
    }

    @DexIgnore
    @Override // com.fossil.cc4
    public boolean a(hc4 hc4, Exception exc) {
        if (!hc4.h() && !hc4.i() && !hc4.k()) {
            return false;
        }
        this.b.b(exc);
        return true;
    }
}
