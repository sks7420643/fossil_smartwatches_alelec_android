package com.fossil;

import android.database.Cursor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po implements oo {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<no> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<no> {
        @DexIgnore
        public a(po poVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, no noVar) {
            String str = noVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            Long l = noVar.b;
            if (l == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindLong(2, l.longValue());
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public po(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.oo
    public void a(no noVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(noVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.oo
    public Long a(String str) {
        fi b2 = fi.b("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Long l = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                if (!a2.isNull(0)) {
                    l = Long.valueOf(a2.getLong(0));
                }
            }
            return l;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
