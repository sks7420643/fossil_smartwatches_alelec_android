package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g03 implements h03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.service.configurable_service_limits", true);
        b = dr2.a("measurement.client.configurable_service_limits", true);
        dr2.a("measurement.id.service.configurable_service_limits", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.h03
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.h03
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.h03
    public final boolean zzc() {
        return b.b().booleanValue();
    }
}
