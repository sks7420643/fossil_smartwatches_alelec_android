package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi2 extends gi2 implements yi2 {
    @DexIgnore
    public xi2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    @Override // com.fossil.yi2
    public final void a(id2 id2) throws RemoteException {
        Parcel zza = zza();
        ej2.a(zza, id2);
        a(3, zza);
    }
}
