package com.fossil;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr implements pr<Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public lr(Context context) {
        ee7.b(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, uri, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    /* renamed from: c */
    public String b(Uri uri) {
        ee7.b(uri, "data");
        String uri2 = uri.toString();
        ee7.a((Object) uri2, "data.toString()");
        return uri2;
    }

    @DexIgnore
    public final boolean b(Uri uri) {
        ee7.b(uri, "data");
        return ee7.a(uri.getAuthority(), "com.android.contacts") && ee7.a(uri.getLastPathSegment(), "display_photo");
    }

    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        return ee7.a((Object) uri.getScheme(), (Object) "content");
    }

    @DexIgnore
    public Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7<? super or> fb7) {
        InputStream inputStream;
        if (b(uri)) {
            AssetFileDescriptor openAssetFileDescriptor = this.a.getContentResolver().openAssetFileDescriptor(uri, "r");
            inputStream = openAssetFileDescriptor != null ? openAssetFileDescriptor.createInputStream() : null;
            if (inputStream == null) {
                throw new IllegalStateException(("Unable to find a contact photo associated with '" + uri + "'.").toString());
            }
        } else {
            inputStream = this.a.getContentResolver().openInputStream(uri);
            if (inputStream == null) {
                throw new IllegalStateException(("Unable to open '" + uri + "'.").toString());
            }
        }
        return new vr(ir7.a(ir7.a(inputStream)), this.a.getContentResolver().getType(uri), br.DISK);
    }
}
