package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx2<K, V> {
    @DexIgnore
    public static <K, V> void a(iv2 iv2, ex2<K, V> ex2, K k, V v) throws IOException {
        qv2.a(iv2, ex2.a, 1, k);
        qv2.a(iv2, ex2.c, 2, v);
    }

    @DexIgnore
    public static <K, V> int a(ex2<K, V> ex2, K k, V v) {
        return qv2.a(ex2.a, 1, k) + qv2.a(ex2.c, 2, v);
    }
}
