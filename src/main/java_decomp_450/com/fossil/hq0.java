package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.SdkDatabase;
import com.fossil.c80;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq0 extends zk0 {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ c80 E; // = new c80((byte) 26, c80.a.MALE, 170, 65, c80.b.LEFT_WRIST);
    @DexIgnore
    public /* final */ ArrayList<ul0> F; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.DEVICE_CONFIG, ul0.FILE_CONFIG}));

    @DexIgnore
    public hq0(ri1 ri1, en0 en0) {
        super(ri1, en0, wm0.K, null, false, 24);
    }

    @DexIgnore
    public static final /* synthetic */ void a(hq0 hq0) {
        int i = hq0.D;
        if (i < 2) {
            hq0.D = i + 1;
            if (yp0.f.b(((zk0) hq0).x.a())) {
                zk0.a(hq0, new zm1(((zk0) hq0).w, ((zk0) hq0).x, hq0.E, true, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) hq0).z, 32), new rs1(hq0), new vi0(hq0), new sk0(hq0), (gd7) null, (gd7) null, 48, (Object) null);
            } else {
                zk0.a(hq0, new sp1(oo1.b.a, ((zk0) hq0).w, 0, 4), new pm0(hq0), new lo0(hq0), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
            }
        } else {
            hq0.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.F;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new he1(((zk0) this).w), new to1(this), new tq1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public final void m() {
        rc1 a;
        String str = ((zk0) this).w.u;
        SdkDatabase a2 = SdkDatabase.e.a();
        Integer valueOf = (a2 == null || (a = a2.a()) == null) ? null : Integer.valueOf(a.a(str));
        yz0.a(new JSONObject(), r51.w4, valueOf != null ? valueOf : -1);
        s11 s11 = s11.DELETE_ALL;
        t11 t11 = t11.a;
        if (valueOf != null) {
            valueOf.intValue();
        }
        a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.C;
    }
}
