package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u81 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ x61 CREATOR; // = new x61(null);
    @DexIgnore
    public /* final */ qk1 a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    public u81(qk1 qk1, long j, byte[] bArr) {
        this.a = qk1;
        this.b = j;
        this.c = bArr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.B2, this.a.a), r51.O0, Double.valueOf(yz0.a(this.b))), r51.W2, yz0.a(this.c, (String) null, 1));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeLong(this.b);
        parcel.writeByteArray(this.c);
    }
}
