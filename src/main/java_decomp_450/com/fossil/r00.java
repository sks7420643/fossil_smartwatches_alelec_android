package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.m00;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r00<Data> implements m00<Integer, Data> {
    @DexIgnore
    public /* final */ m00<Uri, Data> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements n00<Integer, AssetFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public a(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Integer, AssetFileDescriptor> a(q00 q00) {
            return new r00(this.a, q00.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements n00<Integer, ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public b(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Integer, ParcelFileDescriptor> a(q00 q00) {
            return new r00(this.a, q00.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements n00<Integer, InputStream> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public c(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Integer, InputStream> a(q00 q00) {
            return new r00(this.a, q00.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements n00<Integer, Uri> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public d(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Integer, Uri> a(q00 q00) {
            return new r00(this.a, u00.a());
        }
    }

    @DexIgnore
    public r00(Resources resources, m00<Uri, Data> m00) {
        this.b = resources;
        this.a = m00;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean a(Integer num) {
        return true;
    }

    @DexIgnore
    public m00.a<Data> a(Integer num, int i, int i2, ax axVar) {
        Uri a2 = a(num);
        if (a2 == null) {
            return null;
        }
        return this.a.a(a2, i, i2, axVar);
    }

    @DexIgnore
    public final Uri a(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }
}
