package com.fossil;

import com.fossil.gb7;
import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ti7 extends cb7 implements gb7 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends db7<gb7, ti7> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ti7$a$a")
        /* renamed from: com.fossil.ti7$a$a  reason: collision with other inner class name */
        public static final class C0185a extends fe7 implements gd7<ib7.b, ti7> {
            @DexIgnore
            public static /* final */ C0185a INSTANCE; // = new C0185a();

            @DexIgnore
            public C0185a() {
                super(1);
            }

            @DexIgnore
            public final ti7 invoke(ib7.b bVar) {
                if (!(bVar instanceof ti7)) {
                    bVar = null;
                }
                return (ti7) bVar;
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public a() {
            super(gb7.m, C0185a.INSTANCE);
        }
    }

    @DexIgnore
    public ti7() {
        super(gb7.m);
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public void a(fb7<?> fb7) {
        if (fb7 != null) {
            bi7<?> d = ((lj7) fb7).d();
            if (d != null) {
                d.d();
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<*>");
    }

    @DexIgnore
    public abstract void a(ib7 ib7, Runnable runnable);

    @DexIgnore
    public void b(ib7 ib7, Runnable runnable) {
        a(ib7, runnable);
    }

    @DexIgnore
    public boolean b(ib7 ib7) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.ib7.b, com.fossil.cb7
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        return (E) gb7.a.a(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.cb7
    public ib7 minusKey(ib7.c<?> cVar) {
        return gb7.a.b(this, cVar);
    }

    @DexIgnore
    public String toString() {
        return ej7.a(this) + '@' + ej7.b(this);
    }

    @DexIgnore
    @Override // com.fossil.gb7
    public final <T> fb7<T> b(fb7<? super T> fb7) {
        return new lj7(this, fb7);
    }
}
