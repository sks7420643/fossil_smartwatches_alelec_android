package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.s62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c72 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<c72> CREATOR; // = new k82();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public IBinder b;
    @DexIgnore
    public i02 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public c72(int i, IBinder iBinder, i02 i02, boolean z, boolean z2) {
        this.a = i;
        this.b = iBinder;
        this.c = i02;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public s62 e() {
        return s62.a.a(this.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c72)) {
            return false;
        }
        c72 c72 = (c72) obj;
        return this.c.equals(c72.c) && e().equals(c72.e());
    }

    @DexIgnore
    public i02 g() {
        return this.c;
    }

    @DexIgnore
    public boolean v() {
        return this.d;
    }

    @DexIgnore
    public boolean w() {
        return this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b, false);
        k72.a(parcel, 3, (Parcelable) g(), i, false);
        k72.a(parcel, 4, v());
        k72.a(parcel, 5, w());
        k72.a(parcel, a2);
    }
}
