package com.fossil;

import android.view.View;
import android.view.WindowId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bl implements cl {
    @DexIgnore
    public /* final */ WindowId a;

    @DexIgnore
    public bl(View view) {
        this.a = view.getWindowId();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof bl) && ((bl) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
