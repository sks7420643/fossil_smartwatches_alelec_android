package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rc1 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<bi1> b;
    @DexIgnore
    public /* final */ ji c;
    @DexIgnore
    public /* final */ ji d;
    @DexIgnore
    public /* final */ ji e;

    @DexIgnore
    public rc1(ci ciVar) {
        this.a = ciVar;
        this.b = new i51(this, ciVar);
        this.c = new f71(this, ciVar);
        this.d = new c91(this, ciVar);
        this.e = new xa1(this, ciVar);
    }

    @DexIgnore
    public long a(bi1 bi1) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(bi1);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public int a(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.e.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<bi1> a() {
        fi b2 = fi.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "deviceMacAddress");
            int b5 = oi.b(a2, "fileType");
            int b6 = oi.b(a2, "fileIndex");
            int b7 = oi.b(a2, "rawData");
            int b8 = oi.b(a2, "fileLength");
            int b9 = oi.b(a2, "fileCrc");
            int b10 = oi.b(a2, "createdTimeStamp");
            int b11 = oi.b(a2, "isCompleted");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                bi1 bi1 = new bi1(a2.getString(b4), (byte) a2.getShort(b5), (byte) a2.getShort(b6), a2.getBlob(b7), a2.getLong(b8), a2.getLong(b9), a2.getLong(b10), a2.getInt(b11) != 0);
                bi1.a = a2.getInt(b3);
                arrayList.add(bi1);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
