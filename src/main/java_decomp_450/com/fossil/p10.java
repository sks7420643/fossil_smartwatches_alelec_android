package com.fossil;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p10 extends l10 {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(yw.a);

    @DexIgnore
    @Override // com.fossil.l10
    public Bitmap a(dz dzVar, Bitmap bitmap, int i, int i2) {
        return f20.b(dzVar, bitmap, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        return obj instanceof p10;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return -670243078;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
