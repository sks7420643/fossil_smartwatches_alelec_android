package com.fossil;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r26 extends dl4<q26> {
    @DexIgnore
    void G0();

    @DexIgnore
    void L();

    @DexIgnore
    void M0();

    @DexIgnore
    void O0();

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, qb5 qb5);

    @DexIgnore
    void a(List<WatchFaceWrapper> list, qb5 qb5);

    @DexIgnore
    void b();

    @DexIgnore
    void b(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    void g0();

    @DexIgnore
    void v(boolean z);

    @DexIgnore
    void z0();
}
