package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq4 implements Factory<sq4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;
    @DexIgnore
    public /* final */ Provider<xo4> b;

    @DexIgnore
    public tq4(Provider<ro4> provider, Provider<xo4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static tq4 a(Provider<ro4> provider, Provider<xo4> provider2) {
        return new tq4(provider, provider2);
    }

    @DexIgnore
    public static sq4 a(ro4 ro4, xo4 xo4) {
        return new sq4(ro4, xo4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public sq4 get() {
        return a(this.a.get(), this.b.get());
    }
}
