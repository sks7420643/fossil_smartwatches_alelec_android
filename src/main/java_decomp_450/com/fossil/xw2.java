package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw2 extends sw2 {
    @DexIgnore
    public xw2() {
        super();
    }

    @DexIgnore
    public static <E> jw2<E> b(Object obj, long j) {
        return (jw2) bz2.f(obj, j);
    }

    @DexIgnore
    @Override // com.fossil.sw2
    public final void a(Object obj, long j) {
        b(obj, j).zzb();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // com.fossil.sw2
    public final <E> void a(Object obj, Object obj2, long j) {
        jw2<E> b = b(obj, j);
        jw2<E> b2 = b(obj2, j);
        int size = b.size();
        int size2 = b2.size();
        jw2<E> jw2 = b;
        jw2 = b;
        if (size > 0 && size2 > 0) {
            boolean zza = b.zza();
            jw2<E> jw22 = b;
            if (!zza) {
                jw22 = b.zza(size2 + size);
            }
            jw22.addAll(b2);
            jw2 = jw22;
        }
        if (size > 0) {
            b2 = jw2;
        }
        bz2.a(obj, j, b2);
    }
}
