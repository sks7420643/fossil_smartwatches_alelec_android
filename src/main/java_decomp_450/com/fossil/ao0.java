package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wp0 a;
    @DexIgnore
    public /* final */ /* synthetic */ zk0 b;
    @DexIgnore
    public /* final */ /* synthetic */ float c;

    @DexIgnore
    public ao0(wp0 wp0, zk0 zk0, float f) {
        this.a = wp0;
        this.b = zk0;
        this.c = f;
    }

    @DexIgnore
    public final void run() {
        km1.a(this.a.b, le0.DEBUG, yz0.a(this.b.y), "Progress: %.4f.", Float.valueOf(this.c));
        this.a.a.a(this.c);
    }
}
