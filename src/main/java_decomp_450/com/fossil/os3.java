package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import com.google.android.material.button.MaterialButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class os3 {
    @DexIgnore
    public static /* final */ boolean s; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ MaterialButton a;
    @DexIgnore
    public hv3 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public PorterDuff.Mode i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public LayerDrawable r;

    @DexIgnore
    public os3(MaterialButton materialButton, hv3 hv3) {
        this.a = materialButton;
        this.b = hv3;
    }

    @DexIgnore
    public void a(TypedArray typedArray) {
        this.c = typedArray.getDimensionPixelOffset(tr3.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(tr3.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(tr3.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(tr3.MaterialButton_android_insetBottom, 0);
        if (typedArray.hasValue(tr3.MaterialButton_cornerRadius)) {
            int dimensionPixelSize = typedArray.getDimensionPixelSize(tr3.MaterialButton_cornerRadius, -1);
            this.g = dimensionPixelSize;
            a(this.b.a((float) dimensionPixelSize));
            this.p = true;
        }
        this.h = typedArray.getDimensionPixelSize(tr3.MaterialButton_strokeWidth, 0);
        this.i = lu3.a(typedArray.getInt(tr3.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = pu3.a(this.a.getContext(), typedArray, tr3.MaterialButton_backgroundTint);
        this.k = pu3.a(this.a.getContext(), typedArray, tr3.MaterialButton_strokeColor);
        this.l = pu3.a(this.a.getContext(), typedArray, tr3.MaterialButton_rippleColor);
        this.q = typedArray.getBoolean(tr3.MaterialButton_android_checkable, false);
        int dimensionPixelSize2 = typedArray.getDimensionPixelSize(tr3.MaterialButton_elevation, 0);
        int u = da.u(this.a);
        int paddingTop = this.a.getPaddingTop();
        int t = da.t(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        this.a.setInternalBackground(a());
        dv3 d2 = d();
        if (d2 != null) {
            d2.b((float) dimensionPixelSize2);
        }
        da.b(this.a, u + this.c, paddingTop + this.e, t + this.d, paddingBottom + this.f);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            o();
        }
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (d() != null) {
                p7.a(d(), this.j);
            }
        }
    }

    @DexIgnore
    public dv3 d() {
        return a(false);
    }

    @DexIgnore
    public ColorStateList e() {
        return this.l;
    }

    @DexIgnore
    public hv3 f() {
        return this.b;
    }

    @DexIgnore
    public ColorStateList g() {
        return this.k;
    }

    @DexIgnore
    public int h() {
        return this.h;
    }

    @DexIgnore
    public ColorStateList i() {
        return this.j;
    }

    @DexIgnore
    public PorterDuff.Mode j() {
        return this.i;
    }

    @DexIgnore
    public final dv3 k() {
        return a(true);
    }

    @DexIgnore
    public boolean l() {
        return this.o;
    }

    @DexIgnore
    public boolean m() {
        return this.q;
    }

    @DexIgnore
    public void n() {
        this.o = true;
        this.a.setSupportBackgroundTintList(this.j);
        this.a.setSupportBackgroundTintMode(this.i);
    }

    @DexIgnore
    public final void o() {
        dv3 d2 = d();
        dv3 k2 = k();
        if (d2 != null) {
            d2.a((float) this.h, this.k);
            if (k2 != null) {
                k2.a((float) this.h, this.n ? vs3.a(this.a, jr3.colorSurface) : 0);
            }
        }
    }

    @DexIgnore
    public void b(int i2) {
        if (!this.p || this.g != i2) {
            this.g = i2;
            this.p = true;
            a(this.b.a((float) i2));
        }
    }

    @DexIgnore
    public void c(boolean z) {
        this.n = z;
        o();
    }

    @DexIgnore
    public void c(int i2) {
        if (this.h != i2) {
            this.h = i2;
            o();
        }
    }

    @DexIgnore
    public int b() {
        return this.g;
    }

    @DexIgnore
    public void b(boolean z) {
        this.q = z;
    }

    @DexIgnore
    public final void b(hv3 hv3) {
        if (d() != null) {
            d().setShapeAppearanceModel(hv3);
        }
        if (k() != null) {
            k().setShapeAppearanceModel(hv3);
        }
        if (c() != null) {
            c().setShapeAppearanceModel(hv3);
        }
    }

    @DexIgnore
    public kv3 c() {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        if (this.r.getNumberOfLayers() > 2) {
            return (kv3) this.r.getDrawable(2);
        }
        return (kv3) this.r.getDrawable(1);
    }

    @DexIgnore
    public final InsetDrawable a(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (d() != null && this.i != null) {
                p7.a(d(), this.i);
            }
        }
    }

    @DexIgnore
    public final Drawable a() {
        dv3 dv3 = new dv3(this.b);
        dv3.a(this.a.getContext());
        p7.a(dv3, this.j);
        PorterDuff.Mode mode = this.i;
        if (mode != null) {
            p7.a(dv3, mode);
        }
        dv3.a((float) this.h, this.k);
        dv3 dv32 = new dv3(this.b);
        dv32.setTint(0);
        dv32.a((float) this.h, this.n ? vs3.a(this.a, jr3.colorSurface) : 0);
        if (s) {
            dv3 dv33 = new dv3(this.b);
            this.m = dv33;
            p7.b(dv33, -1);
            RippleDrawable rippleDrawable = new RippleDrawable(uu3.b(this.l), a(new LayerDrawable(new Drawable[]{dv32, dv3})), this.m);
            this.r = rippleDrawable;
            return rippleDrawable;
        }
        tu3 tu3 = new tu3(this.b);
        this.m = tu3;
        p7.a(tu3, uu3.b(this.l));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{dv32, dv3, this.m});
        this.r = layerDrawable;
        return a(layerDrawable);
    }

    @DexIgnore
    public void a(int i2, int i3) {
        Drawable drawable = this.m;
        if (drawable != null) {
            drawable.setBounds(this.c, this.e, i3 - this.d, i2 - this.f);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (d() != null) {
            d().setTint(i2);
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            if (s && (this.a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.a.getBackground()).setColor(uu3.b(colorStateList));
            } else if (!s && (this.a.getBackground() instanceof tu3)) {
                ((tu3) this.a.getBackground()).setTintList(uu3.b(colorStateList));
            }
        }
    }

    @DexIgnore
    public final dv3 a(boolean z) {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        if (s) {
            return (dv3) ((LayerDrawable) ((InsetDrawable) this.r.getDrawable(0)).getDrawable()).getDrawable(!z);
        }
        return (dv3) this.r.getDrawable(!z);
    }

    @DexIgnore
    public void a(hv3 hv3) {
        this.b = hv3;
        b(hv3);
    }
}
