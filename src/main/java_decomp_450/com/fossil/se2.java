package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se2 {
    @DexIgnore
    public SharedPreferences a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public /* final */ Map<String, Object> c;

    @DexIgnore
    public se2(Context context) {
        this(context, new af2());
    }

    @DexIgnore
    public final boolean a() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public final synchronized void b() {
        this.c.clear();
        af2.a(this.b);
        this.a.edit().clear().commit();
    }

    @DexIgnore
    public se2(Context context, af2 af2) {
        this.c = new n4();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        File file = new File(v6.c(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    le2.a(this.b, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("InstanceID/Store", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final synchronized void a(String str) {
        SharedPreferences.Editor edit = this.a.edit();
        for (String str2 : this.a.getAll().keySet()) {
            if (str2.startsWith(str)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this) {
            this.c.remove(str);
        }
        af2.a(this.b, str);
        a(String.valueOf(str).concat("|"));
    }
}
