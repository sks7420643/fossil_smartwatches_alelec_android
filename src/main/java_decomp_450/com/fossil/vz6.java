package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.fossil.be5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz6 extends LinearLayout implements CustomSwipeRefreshLayout.c {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public ImageView b;
    @DexIgnore
    public FlexibleTextView c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleTextView e;
    @DexIgnore
    public FlexibleTextView f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ String i; // = eh5.l.a().b("nonBrandSurface");
    @DexIgnore
    public iw j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ vz6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(vz6 vz6) {
            this.a = vz6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            hw<Drawable> a2 = this.a.getMRequestManager$app_fossilRelease().a(str2);
            ImageView ivDevice$app_fossilRelease = this.a.getIvDevice$app_fossilRelease();
            if (ivDevice$app_fossilRelease != null) {
                a2.a(ivDevice$app_fossilRelease);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vz6(Context context) {
        super(context);
        ee7.b(context, "context");
        iw d2 = aw.d(PortfolioApp.g0.c());
        ee7.a((Object) d2, "Glide.with(PortfolioApp.instance)");
        this.j = d2;
        setWillNotDraw(false);
        a();
    }

    @DexIgnore
    public final void a() {
        View inflate = LayoutInflater.from(getContext()).inflate(2131558834, (ViewGroup) null);
        if (inflate != null) {
            this.a = (ViewGroup) inflate;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            ViewGroup viewGroup = this.a;
            if (viewGroup != null) {
                this.c = (FlexibleTextView) viewGroup.findViewById(2131362318);
                this.d = (FlexibleTextView) viewGroup.findViewById(2131362326);
                this.e = (FlexibleTextView) viewGroup.findViewById(2131362321);
                this.f = (FlexibleTextView) viewGroup.findViewById(2131362323);
                this.b = (ImageView) viewGroup.findViewById(2131362623);
                String str = this.i;
                if (str != null) {
                    viewGroup.setBackgroundColor(Color.parseColor(str));
                }
                addView(viewGroup, layoutParams);
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @DexIgnore
    public final ImageView getIvDevice$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public final iw getMRequestManager$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    public final void setIvDevice$app_fossilRelease(ImageView imageView) {
        this.b = imageView;
    }

    @DexIgnore
    public final void setMRequestManager$app_fossilRelease(iw iwVar) {
        ee7.b(iwVar, "<set-?>");
        this.j = iwVar;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
    public void a(CustomSwipeRefreshLayout.e eVar, CustomSwipeRefreshLayout.e eVar2) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        ee7.b(eVar, "currentState");
        ee7.b(eVar2, "lastState");
        int a2 = eVar.a();
        int a3 = eVar2.a();
        if (a2 != a3) {
            if (a2 == 0) {
                FlexibleTextView flexibleTextView3 = this.d;
                if (flexibleTextView3 != null) {
                    flexibleTextView3.setText(ig5.a(getContext(), 2131886756));
                }
            } else if (a2 != 1) {
                if (a2 == 2) {
                    FlexibleTextView flexibleTextView4 = this.d;
                    if (flexibleTextView4 != null) {
                        flexibleTextView4.setText(ig5.a(getContext(), 2131886759));
                    }
                } else if (a2 == 3 && (flexibleTextView2 = this.d) != null) {
                    flexibleTextView2.setText(ig5.a(getContext(), 2131886731));
                }
            } else if (!(a3 == 1 || (flexibleTextView = this.d) == null)) {
                flexibleTextView.setText(ig5.a(getContext(), 2131886757));
            }
            a(this.g, this.h);
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        String str3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeadViewCard", "updateStaticInfo - deviceSerial=" + str);
        this.g = str;
        this.h = str2;
        PortfolioApp c2 = PortfolioApp.g0.c();
        if (TextUtils.isEmpty(str)) {
            FlexibleTextView flexibleTextView = this.f;
            if (flexibleTextView != null) {
                flexibleTextView.setVisibility(0);
                return;
            }
            return;
        }
        FlexibleTextView flexibleTextView2 = this.f;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setVisibility(8);
        }
        FlexibleTextView flexibleTextView3 = this.c;
        if (flexibleTextView3 != null) {
            flexibleTextView3.setText(str2);
        }
        long e2 = new ch5(c2).e(c2.c());
        if (e2 <= 0) {
            str3 = ig5.a(c2, 2131887357);
            ee7.a((Object) str3, "LanguageHelper.getString\u2026 R.string.empty_hour_min)");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTimeInMillis(e2);
            Boolean w = zd5.w(instance.getTime());
            ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
            if (w.booleanValue()) {
                we7 we7 = we7.a;
                String a2 = ig5.a(c2, 2131886645);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ay_Title__TodayMonthDate)");
                str3 = String.format(a2, Arrays.copyOf(new Object[]{zd5.i(instance.getTime())}, 1));
                ee7.a((Object) str3, "java.lang.String.format(format, *args)");
            } else {
                Calendar instance2 = Calendar.getInstance();
                ee7.a((Object) instance2, "Calendar.getInstance()");
                long timeInMillis = (instance2.getTimeInMillis() - e2) / 3600000;
                long j2 = (long) 24;
                if (timeInMillis < j2) {
                    we7 we72 = we7.a;
                    String a3 = ig5.a(c2, 2131887515);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    str3 = String.format(a3, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis)}, 1));
                    ee7.a((Object) str3, "java.lang.String.format(format, *args)");
                } else {
                    we7 we73 = we7.a;
                    String a4 = ig5.a(c2, 2131887515);
                    ee7.a((Object) a4, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    str3 = String.format(a4, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis / j2)}, 1));
                    ee7.a((Object) str3, "java.lang.String.format(format, *args)");
                }
            }
        }
        FlexibleTextView flexibleTextView4 = this.e;
        if (flexibleTextView4 != null) {
            we7 we74 = we7.a;
            String a5 = ig5.a(c2, 2131887126);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
            String format = String.format(a5, Arrays.copyOf(new Object[]{str3}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView4.setText(format);
        }
        FlexibleTextView flexibleTextView5 = this.e;
        if (flexibleTextView5 != null) {
            flexibleTextView5.setSelected(true);
        }
        CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
        if (str != null) {
            CloudImageHelper.ItemImage type = with.setSerialNumber(str).setSerialPrefix(be5.o.b(str)).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.b;
            if (imageView != null) {
                type.setPlaceHolder(imageView, be5.o.b(str, be5.b.SMALL)).setImageCallback(new b(this)).download();
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
