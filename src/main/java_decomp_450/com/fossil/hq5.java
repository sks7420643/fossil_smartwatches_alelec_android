package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq5 extends ho5 implements gq5, cy6.g {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<ix4> g;
    @DexIgnore
    public fq5 h;
    @DexIgnore
    public /* final */ s i; // = new s(this);
    @DexIgnore
    public /* final */ p j; // = new p(this);
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hq5.q;
        }

        @DexIgnore
        public final hq5 b() {
            return new hq5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public b(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.r;
            ee7.a((Object) flexibleTextView, "binding.dayFriday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(6, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public c(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.t;
            ee7.a((Object) flexibleTextView, "binding.daySaturday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(7, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public d(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            fq5 b2 = hq5.b(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.J;
            ee7.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.I;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public e(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            fq5 b2 = hq5.b(this.a);
            NumberPicker numberPicker2 = this.b.H;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.I;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public f(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            fq5 b2 = hq5.b(this.a);
            NumberPicker numberPicker2 = this.b.H;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.J;
            ee7.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;

        @DexIgnore
        public g(hq5 hq5) {
            this.a = hq5;
        }

        @DexIgnore
        public final void onClick(View view) {
            hq5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;

        @DexIgnore
        public h(hq5 hq5) {
            this.a = hq5;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.k(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public i(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            hq5.b(this.a).a(z);
            ConstraintLayout constraintLayout = this.b.q;
            ee7.a((Object) constraintLayout, "binding.clDaysRepeat");
            constraintLayout.setVisibility(z ? 0 : 8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;

        @DexIgnore
        public j(hq5 hq5) {
            this.a = hq5;
        }

        @DexIgnore
        public final void onClick(View view) {
            hq5.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public k(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.u;
            ee7.a((Object) flexibleTextView, "binding.daySunday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(1, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public l(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.s;
            ee7.a((Object) flexibleTextView, "binding.dayMonday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public m(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.w;
            ee7.a((Object) flexibleTextView, "binding.dayTuesday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(3, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public n(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.x;
            ee7.a((Object) flexibleTextView, "binding.dayWednesday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(4, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ix4 b;

        @DexIgnore
        public o(hq5 hq5, ix4 ix4) {
            this.a = hq5;
            this.b = ix4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ee7.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            hq5 hq5 = this.a;
            FlexibleTextView flexibleTextView = this.b.v;
            ee7.a((Object) flexibleTextView, "binding.dayThursday");
            hq5.a((TextView) flexibleTextView);
            this.a.b(5, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public p(hq5 hq5) {
            this.a = hq5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            ix4 ix4 = (ix4) hq5.a(this.a).a();
            if (!(ix4 == null || (flexibleEditText = ix4.z) == null)) {
                flexibleEditText.requestLayout();
            }
            hq5.b(this.a).a(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 50";
                ix4 ix42 = (ix4) hq5.a(this.a).a();
                if (ix42 != null && (flexibleTextView = ix42.B) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText a;
        @DexIgnore
        public /* final */ /* synthetic */ hq5 b;

        @DexIgnore
        public q(FlexibleEditText flexibleEditText, hq5 hq5) {
            this.a = flexibleEditText;
            this.b = hq5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View d;
            int i;
            ix4 ix4 = (ix4) hq5.a(this.b).a();
            if (ix4 != null && (flexibleTextView = ix4.D) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.a;
                    ee7.a((Object) flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        i = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.a;
                        ee7.a((Object) flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            i = text2.length();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    ee7.a((Object) flexibleTextView, "counterTextView");
                    flexibleTextView.setText(i + " / 15");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                ix4 ix42 = (ix4) hq5.a(this.b).a();
                if (!(ix42 == null || (d = ix42.d()) == null)) {
                    d.requestFocus();
                }
                ee7.a((Object) flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText a;
        @DexIgnore
        public /* final */ /* synthetic */ hq5 b;

        @DexIgnore
        public r(FlexibleEditText flexibleEditText, hq5 hq5) {
            this.a = flexibleEditText;
            this.b = hq5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View d;
            int i;
            ix4 ix4 = (ix4) hq5.a(this.b).a();
            if (ix4 != null && (flexibleTextView = ix4.B) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.a;
                    ee7.a((Object) flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        i = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.a;
                        ee7.a((Object) flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            i = text2.length();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    ee7.a((Object) flexibleTextView, "counterTextView");
                    flexibleTextView.setText(i + " / 50");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                ix4 ix42 = (ix4) hq5.a(this.b).a();
                if (!(ix42 == null || (d = ix42.d()) == null)) {
                    d.requestFocus();
                }
                ee7.a((Object) flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hq5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public s(hq5 hq5) {
            this.a = hq5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            ix4 ix4 = (ix4) hq5.a(this.a).a();
            if (!(ix4 == null || (flexibleEditText = ix4.A) == null)) {
                flexibleEditText.requestLayout();
            }
            hq5.b(this.a).b(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 15";
                ix4 ix42 = (ix4) hq5.a(this.a).a();
                if (ix42 != null && (flexibleTextView = ix42.D) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /*
    static {
        String simpleName = hq5.class.getSimpleName();
        ee7.a((Object) simpleName, "AlarmFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(hq5 hq5) {
        qw6<ix4> qw6 = hq5.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ fq5 b(hq5 hq5) {
        fq5 fq5 = hq5.h;
        if (fq5 != null) {
            return fq5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void A() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void A0() {
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.V(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void J0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.S(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void O(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "startFireBaseTracer(), isAddNewAlarm = " + z);
        T(z);
    }

    @DexIgnore
    public final void T(boolean z) {
        if (!z) {
            V("add_alarm_view");
        } else {
            V("edit_alarm_view");
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void c(int i2, boolean z) {
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (a2 != null) {
                switch (i2) {
                    case 1:
                        FlexibleTextView flexibleTextView = a2.u;
                        ee7.a((Object) flexibleTextView, "binding.daySunday");
                        flexibleTextView.setSelected(z);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        ee7.a((Object) flexibleTextView2, "binding.daySunday");
                        a((TextView) flexibleTextView2);
                        return;
                    case 2:
                        FlexibleTextView flexibleTextView3 = a2.s;
                        ee7.a((Object) flexibleTextView3, "binding.dayMonday");
                        flexibleTextView3.setSelected(z);
                        FlexibleTextView flexibleTextView4 = a2.s;
                        ee7.a((Object) flexibleTextView4, "binding.dayMonday");
                        a((TextView) flexibleTextView4);
                        return;
                    case 3:
                        FlexibleTextView flexibleTextView5 = a2.w;
                        ee7.a((Object) flexibleTextView5, "binding.dayTuesday");
                        flexibleTextView5.setSelected(z);
                        FlexibleTextView flexibleTextView6 = a2.w;
                        ee7.a((Object) flexibleTextView6, "binding.dayTuesday");
                        a((TextView) flexibleTextView6);
                        return;
                    case 4:
                        FlexibleTextView flexibleTextView7 = a2.x;
                        ee7.a((Object) flexibleTextView7, "binding.dayWednesday");
                        flexibleTextView7.setSelected(z);
                        FlexibleTextView flexibleTextView8 = a2.x;
                        ee7.a((Object) flexibleTextView8, "binding.dayWednesday");
                        a((TextView) flexibleTextView8);
                        return;
                    case 5:
                        FlexibleTextView flexibleTextView9 = a2.v;
                        ee7.a((Object) flexibleTextView9, "binding.dayThursday");
                        flexibleTextView9.setSelected(z);
                        FlexibleTextView flexibleTextView10 = a2.v;
                        ee7.a((Object) flexibleTextView10, "binding.dayThursday");
                        a((TextView) flexibleTextView10);
                        return;
                    case 6:
                        FlexibleTextView flexibleTextView11 = a2.r;
                        ee7.a((Object) flexibleTextView11, "binding.dayFriday");
                        flexibleTextView11.setSelected(z);
                        FlexibleTextView flexibleTextView12 = a2.r;
                        ee7.a((Object) flexibleTextView12, "binding.dayFriday");
                        a((TextView) flexibleTextView12);
                        return;
                    case 7:
                        FlexibleTextView flexibleTextView13 = a2.t;
                        ee7.a((Object) flexibleTextView13, "binding.daySaturday");
                        flexibleTextView13.setSelected(z);
                        FlexibleTextView flexibleTextView14 = a2.t;
                        ee7.a((Object) flexibleTextView14, "binding.daySaturday");
                        a((TextView) flexibleTextView14);
                        return;
                    default:
                        return;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return q;
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void e(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        FlexibleButton flexibleButton3;
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (!(a2 == null || (flexibleButton3 = a2.y) == null)) {
                flexibleButton3.setEnabled(z);
            }
            if (z) {
                qw6<ix4> qw62 = this.g;
                if (qw62 != null) {
                    ix4 a3 = qw62.a();
                    if (a3 != null && (flexibleButton2 = a3.y) != null) {
                        flexibleButton2.a("flexible_button_primary");
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            qw6<ix4> qw63 = this.g;
            if (qw63 != null) {
                ix4 a4 = qw63.a();
                if (a4 != null && (flexibleButton = a4.y) != null) {
                    flexibleButton.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        fq5 fq5 = this.h;
        if (fq5 != null) {
            fq5.i();
            return false;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void l0() {
        RTLImageView rTLImageView;
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (a2 != null && (rTLImageView = a2.F) != null) {
                rTLImageView.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FlexibleEditText flexibleEditText;
        FlexibleEditText flexibleEditText2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ix4 ix4 = (ix4) qb.a(layoutInflater, 2131558502, viewGroup, false, a1());
        ee7.a((Object) ix4, "binding");
        a(ix4);
        qw6<ix4> qw6 = new qw6<>(this, ix4);
        this.g = qw6;
        ix4 a2 = qw6.a();
        if (!(a2 == null || (flexibleEditText2 = a2.A) == null)) {
            flexibleEditText2.addTextChangedListener(this.i);
            flexibleEditText2.setOnFocusChangeListener(new q(flexibleEditText2, this));
        }
        qw6<ix4> qw62 = this.g;
        if (qw62 != null) {
            ix4 a3 = qw62.a();
            if (!(a3 == null || (flexibleEditText = a3.z) == null)) {
                flexibleEditText.addTextChangedListener(this.j);
                flexibleEditText.setOnFocusChangeListener(new r(flexibleEditText, this));
            }
            qw6<ix4> qw63 = this.g;
            if (qw63 != null) {
                ix4 a4 = qw63.a();
                if (a4 != null) {
                    ee7.a((Object) a4, "mBinding.get()!!");
                    return a4.d();
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleEditText flexibleEditText = a2.A;
                if (flexibleEditText != null) {
                    flexibleEditText.removeTextChangedListener(this.i);
                }
                a2.z.removeTextChangedListener(this.j);
            }
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        fq5 fq5 = this.h;
        if (fq5 != null) {
            fq5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        fq5 fq5 = this.h;
        if (fq5 != null) {
            fq5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void s(String str) {
        FlexibleEditText flexibleEditText;
        ee7.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.d(str2, "showMessage, value = " + str);
        if (!mh7.a((CharSequence) str)) {
            qw6<ix4> qw6 = this.g;
            if (qw6 != null) {
                ix4 a2 = qw6.a();
                if (a2 != null && (flexibleEditText = a2.z) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void t(boolean z) {
        ConstraintLayout constraintLayout;
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (!(a2 == null || (flexibleSwitchCompat = a2.L) == null)) {
                flexibleSwitchCompat.setChecked(z);
            }
            qw6<ix4> qw62 = this.g;
            if (qw62 != null) {
                ix4 a3 = qw62.a();
                if (a3 != null && (constraintLayout = a3.q) != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(int i2, boolean z) {
        fq5 fq5 = this.h;
        if (fq5 != null) {
            fq5.a(z, i2);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(fq5 fq5) {
        ee7.b(fq5, "presenter");
        this.h = fq5;
    }

    @DexIgnore
    public final void a(ix4 ix4) {
        FlexibleTextView flexibleTextView = ix4.u;
        ee7.a((Object) flexibleTextView, "binding.daySunday");
        if (!flexibleTextView.isSelected()) {
            ix4.u.a("flexible_tv_selected");
        } else {
            ix4.u.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView2 = ix4.s;
        ee7.a((Object) flexibleTextView2, "binding.dayMonday");
        if (!flexibleTextView2.isSelected()) {
            ix4.s.a("flexible_tv_selected");
        } else {
            ix4.s.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView3 = ix4.w;
        ee7.a((Object) flexibleTextView3, "binding.dayTuesday");
        if (!flexibleTextView3.isSelected()) {
            ix4.w.a("flexible_tv_selected");
        } else {
            ix4.w.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView4 = ix4.x;
        ee7.a((Object) flexibleTextView4, "binding.dayWednesday");
        if (!flexibleTextView4.isSelected()) {
            ix4.x.a("flexible_tv_selected");
        } else {
            ix4.x.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView5 = ix4.v;
        ee7.a((Object) flexibleTextView5, "binding.dayThursday");
        if (!flexibleTextView5.isSelected()) {
            ix4.v.a("flexible_tv_selected");
        } else {
            ix4.v.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView6 = ix4.r;
        ee7.a((Object) flexibleTextView6, "binding.dayFriday");
        if (!flexibleTextView6.isSelected()) {
            ix4.r.a("flexible_tv_selected");
        } else {
            ix4.r.a("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView7 = ix4.t;
        ee7.a((Object) flexibleTextView7, "binding.daySaturday");
        if (!flexibleTextView7.isSelected()) {
            ix4.t.a("flexible_tv_selected");
        } else {
            ix4.t.a("flexible_tv_unselected");
        }
        ix4.E.setOnClickListener(new g(this));
        ix4.F.setOnClickListener(new h(this));
        ix4.L.setOnCheckedChangeListener(new i(this, ix4));
        ix4.y.setOnClickListener(new j(this));
        ix4.u.setOnClickListener(new k(this, ix4));
        ix4.s.setOnClickListener(new l(this, ix4));
        ix4.w.setOnClickListener(new m(this, ix4));
        ix4.x.setOnClickListener(new n(this, ix4));
        ix4.v.setOnClickListener(new o(this, ix4));
        ix4.r.setOnClickListener(new b(this, ix4));
        ix4.t.setOnClickListener(new c(this, ix4));
        NumberPicker numberPicker = ix4.H;
        ee7.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = ix4.H;
        ee7.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        ix4.H.setOnValueChangedListener(new d(this, ix4));
        NumberPicker numberPicker3 = ix4.J;
        ee7.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = ix4.J;
        ee7.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        ix4.J.setOnValueChangedListener(new e(this, ix4));
        String[] strArr = new String[2];
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886102);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
        if (a2 != null) {
            String upperCase = a2.toUpperCase();
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            strArr[0] = upperCase;
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886104);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            if (a3 != null) {
                String upperCase2 = a3.toUpperCase();
                ee7.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                strArr[1] = upperCase2;
                NumberPicker numberPicker5 = ix4.I;
                ee7.a((Object) numberPicker5, "binding.numberPickerThree");
                numberPicker5.setMinValue(0);
                NumberPicker numberPicker6 = ix4.I;
                ee7.a((Object) numberPicker6, "binding.numberPickerThree");
                numberPicker6.setMaxValue(1);
                ix4.I.setDisplayedValues(strArr);
                ix4.I.setOnValueChangedListener(new f(this, ix4));
                return;
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public final void a(TextView textView) {
        ee7.b(textView, "textView");
        FlexibleTextView flexibleTextView = (FlexibleTextView) textView;
        if (flexibleTextView.isSelected()) {
            flexibleTextView.a("flexible_tv_selected");
        } else {
            flexibleTextView.a("flexible_tv_unselected");
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void a(String str) {
        FlexibleEditText flexibleEditText;
        ee7.b(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.d(str2, "showTitle, value = " + str);
        if (!mh7.a((CharSequence) str)) {
            qw6<ix4> qw6 = this.g;
            if (qw6 != null) {
                ix4 a2 = qw6.a();
                if (a2 != null && (flexibleEditText = a2.A) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void a(int i2) {
        int i3;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        int i6 = 12;
        if (i4 >= 12) {
            i3 = 1;
            i4 -= 12;
        } else {
            i3 = 0;
        }
        if (i4 != 0) {
            i6 = i4;
        }
        qw6<ix4> qw6 = this.g;
        if (qw6 != null) {
            ix4 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.H;
                ee7.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i6);
                NumberPicker numberPicker2 = a2.J;
                ee7.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i5);
                NumberPicker numberPicker3 = a2.I;
                ee7.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i3);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void a(SparseIntArray sparseIntArray) {
        ee7.b(sparseIntArray, "daysRepeat");
        int size = sparseIntArray.size();
        for (int i2 = 1; i2 <= 7; i2++) {
            boolean z = false;
            if (size == 0) {
                c(i2, false);
            } else {
                if (sparseIntArray.get(i2) == i2) {
                    z = true;
                }
                c(i2, z);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        ee7.b(str, "tag");
        super.a(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != 1038249436) {
                if (hashCode == 1185284775 && str.equals("CONFIRM_SET_ALARM_FAILED") && i2 == 2131362260 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            } else if (str.equals("CONFIRM_DELETE_ALARM") && i2 == 2131363307) {
                fq5 fq5 = this.h;
                if (fq5 != null) {
                    fq5.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 != 2131363307) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                    return;
                }
                return;
            }
            fq5 fq52 = this.h;
            if (fq52 != null) {
                fq52.j();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gq5
    public void a(Alarm alarm, boolean z) {
        ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "showSetAlarmComplete: alarm = " + alarm + ", isSuccess = " + z);
        if (z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.R(childFragmentManager);
        }
    }
}
