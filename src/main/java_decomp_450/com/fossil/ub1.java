package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ub1(zk1 zk1) {
        super(1);
        this.a = zk1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        int i;
        CopyOnWriteArrayList b = this.a.G;
        ArrayList<bi1> arrayList = ((jp0) v81).X;
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it = arrayList.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((bi1) next).f > 0) {
                i = 1;
            }
            if (i != 0) {
                arrayList2.add(next);
            }
        }
        b.addAll(arrayList2);
        zk1 zk1 = this.a;
        for (bi1 bi1 : zk1.G) {
            i += (int) bi1.f;
        }
        zk1.I = yz0.b(i);
        this.a.q();
        this.a.p();
        return i97.a;
    }
}
