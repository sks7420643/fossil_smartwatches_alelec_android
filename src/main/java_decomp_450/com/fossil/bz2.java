package com.fossil;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bz2 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(bz2.class.getName());
    @DexIgnore
    public static /* final */ Unsafe b; // = c();
    @DexIgnore
    public static /* final */ Class<?> c; // = mu2.b();
    @DexIgnore
    public static /* final */ boolean d; // = d(Long.TYPE);
    @DexIgnore
    public static /* final */ boolean e; // = d(Integer.TYPE);
    @DexIgnore
    public static /* final */ c f;
    @DexIgnore
    public static /* final */ boolean g; // = e();
    @DexIgnore
    public static /* final */ boolean h; // = d();
    @DexIgnore
    public static /* final */ long i; // = ((long) b(byte[].class));
    @DexIgnore
    public static /* final */ boolean j; // = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public Unsafe a;

        @DexIgnore
        public c(Unsafe unsafe) {
            this.a = unsafe;
        }

        @DexIgnore
        public abstract byte a(Object obj, long j);

        @DexIgnore
        public abstract void a(Object obj, long j, byte b);

        @DexIgnore
        public abstract void a(Object obj, long j, double d);

        @DexIgnore
        public abstract void a(Object obj, long j, float f);

        @DexIgnore
        public final void a(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        @DexIgnore
        public abstract void a(Object obj, long j, boolean z);

        @DexIgnore
        public abstract boolean b(Object obj, long j);

        @DexIgnore
        public abstract float c(Object obj, long j);

        @DexIgnore
        public abstract double d(Object obj, long j);

        @DexIgnore
        public final int e(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        @DexIgnore
        public final long f(Object obj, long j) {
            return this.a.getLong(obj, j);
        }

        @DexIgnore
        public final void a(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends c {
        @DexIgnore
        public d(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final byte a(Object obj, long j) {
            return ((c) this).a.getByte(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final boolean b(Object obj, long j) {
            return ((c) this).a.getBoolean(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final float c(Object obj, long j) {
            return ((c) this).a.getFloat(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final double d(Object obj, long j) {
            return ((c) this).a.getDouble(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, byte b) {
            ((c) this).a.putByte(obj, j, b);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, boolean z) {
            ((c) this).a.putBoolean(obj, j, z);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, float f) {
            ((c) this).a.putFloat(obj, j, f);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, double d) {
            ((c) this).a.putDouble(obj, j, d);
        }
    }

    /*
    static {
        c cVar;
        c cVar2 = null;
        if (b != null) {
            if (!mu2.a()) {
                cVar2 = new d(b);
            } else if (d) {
                cVar2 = new a(b);
            } else if (e) {
                cVar2 = new b(b);
            }
        }
        f = cVar2;
        b(boolean[].class);
        c(boolean[].class);
        b(int[].class);
        c(int[].class);
        b(long[].class);
        c(long[].class);
        b(float[].class);
        c(float[].class);
        b(double[].class);
        c(double[].class);
        b(Object[].class);
        c(Object[].class);
        Field f2 = f();
        if (!(f2 == null || (cVar = f) == null)) {
            cVar.a.objectFieldOffset(f2);
        }
    }
    */

    @DexIgnore
    public static boolean a() {
        return h;
    }

    @DexIgnore
    public static boolean b() {
        return g;
    }

    @DexIgnore
    public static int c(Class<?> cls) {
        if (h) {
            return f.a.arrayIndexScale(cls);
        }
        return -1;
    }

    @DexIgnore
    public static float d(Object obj, long j2) {
        return f.c(obj, j2);
    }

    @DexIgnore
    public static double e(Object obj, long j2) {
        return f.d(obj, j2);
    }

    @DexIgnore
    public static Object f(Object obj, long j2) {
        return f.a.getObject(obj, j2);
    }

    @DexIgnore
    public static byte k(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) (((~j2) & 3) << 3)));
    }

    @DexIgnore
    public static byte l(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) ((j2 & 3) << 3)));
    }

    @DexIgnore
    public static boolean m(Object obj, long j2) {
        return k(obj, j2) != 0;
    }

    @DexIgnore
    public static boolean n(Object obj, long j2) {
        return l(obj, j2) != 0;
    }

    @DexIgnore
    public static <T> T a(Class<T> cls) {
        try {
            return (T) b.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public static int b(Class<?> cls) {
        if (h) {
            return f.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    @DexIgnore
    public static boolean d() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (mu2.a()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static boolean e() {
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (f() == null) {
                return false;
            }
            if (mu2.a()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends c {
        @DexIgnore
        public a(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final byte a(Object obj, long j) {
            if (bz2.j) {
                return bz2.k(obj, j);
            }
            return bz2.l(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final boolean b(Object obj, long j) {
            if (bz2.j) {
                return bz2.m(obj, j);
            }
            return bz2.n(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final float c(Object obj, long j) {
            return Float.intBitsToFloat(e(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final double d(Object obj, long j) {
            return Double.longBitsToDouble(f(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, byte b) {
            if (bz2.j) {
                bz2.c(obj, j, b);
            } else {
                bz2.d(obj, j, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, boolean z) {
            if (bz2.j) {
                bz2.d(obj, j, z);
            } else {
                bz2.e(obj, j, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final byte a(Object obj, long j) {
            if (bz2.j) {
                return bz2.k(obj, j);
            }
            return bz2.l(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final boolean b(Object obj, long j) {
            if (bz2.j) {
                return bz2.m(obj, j);
            }
            return bz2.n(obj, j);
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final float c(Object obj, long j) {
            return Float.intBitsToFloat(e(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final double d(Object obj, long j) {
            return Double.longBitsToDouble(f(obj, j));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, byte b) {
            if (bz2.j) {
                bz2.c(obj, j, b);
            } else {
                bz2.d(obj, j, b);
            }
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, boolean z) {
            if (bz2.j) {
                bz2.d(obj, j, z);
            } else {
                bz2.e(obj, j, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        @Override // com.fossil.bz2.c
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }
    }

    @DexIgnore
    public static boolean c(Object obj, long j2) {
        return f.b(obj, j2);
    }

    @DexIgnore
    public static Field f() {
        Field a2;
        if (mu2.a() && (a2 = a(Buffer.class, "effectiveDirectAddress")) != null) {
            return a2;
        }
        Field a3 = a(Buffer.class, "address");
        if (a3 == null || a3.getType() != Long.TYPE) {
            return null;
        }
        return a3;
    }

    @DexIgnore
    public static int a(Object obj, long j2) {
        return f.e(obj, j2);
    }

    @DexIgnore
    public static long b(Object obj, long j2) {
        return f.f(obj, j2);
    }

    @DexIgnore
    public static Unsafe c() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zy2());
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void a(Object obj, long j2, int i2) {
        f.a(obj, j2, i2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, long j3) {
        f.a(obj, j2, j3);
    }

    @DexIgnore
    public static void c(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int a2 = a(obj, j3);
        int i2 = ((~((int) j2)) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a2 & (~(255 << i2))));
    }

    @DexIgnore
    public static void a(Object obj, long j2, boolean z) {
        f.a(obj, j2, z);
    }

    @DexIgnore
    public static void a(Object obj, long j2, float f2) {
        f.a(obj, j2, f2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, double d2) {
        f.a(obj, j2, d2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, Object obj2) {
        f.a.putObject(obj, j2, obj2);
    }

    @DexIgnore
    public static byte a(byte[] bArr, long j2) {
        return f.a(bArr, i + j2);
    }

    @DexIgnore
    public static void a(byte[] bArr, long j2, byte b2) {
        f.a((Object) bArr, i + j2, b2);
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void e(Object obj, long j2, boolean z) {
        d(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static boolean d(Class<?> cls) {
        if (!mu2.a()) {
            return false;
        }
        try {
            Class<?> cls2 = c;
            cls2.getMethod("peekLong", cls, Boolean.TYPE);
            cls2.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls2.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls2.getMethod("peekInt", cls, Boolean.TYPE);
            cls2.getMethod("pokeByte", cls, Byte.TYPE);
            cls2.getMethod("peekByte", cls);
            cls2.getMethod("pokeByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            cls2.getMethod("peekByteArray", cls, byte[].class, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public static void d(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a(obj, j3) & (~(255 << i2))));
    }

    @DexIgnore
    public static void d(Object obj, long j2, boolean z) {
        c(obj, j2, z ? (byte) 1 : 0);
    }
}
