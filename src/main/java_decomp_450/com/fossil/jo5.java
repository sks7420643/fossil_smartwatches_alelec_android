package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jo5 extends dy6 implements pq6 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public oq6 j;
    @DexIgnore
    public qw6<kx4> p;
    @DexIgnore
    public yz6 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jo5.s;
        }

        @DexIgnore
        public final jo5 b() {
            return new jo5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ jo5 a;

        @DexIgnore
        public b(jo5 jo5) {
            this.a = jo5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jo5.t.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            jo5.a(this.a).b(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ jo5 a;

        @DexIgnore
        public c(jo5 jo5) {
            this.a = jo5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jo5.t.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            jo5.a(this.a).a(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ jo5 a;

        @DexIgnore
        public d(jo5 jo5) {
            this.a = jo5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jo5.t.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            jo5.a(this.a).c(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo5 a;

        @DexIgnore
        public e(jo5 jo5) {
            this.a = jo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            jo5.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo5 a;

        @DexIgnore
        public f(jo5 jo5) {
            this.a = jo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismiss();
        }
    }

    /*
    static {
        String simpleName = jo5.class.getSimpleName();
        ee7.a((Object) simpleName, "BirthdayFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ oq6 a(jo5 jo5) {
        oq6 oq6 = jo5.j;
        if (oq6 != null) {
            return oq6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final kx4 b1() {
        qw6<kx4> qw6 = this.p;
        if (qw6 != null) {
            return qw6.a();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.pq6
    public void c(Date date) {
        ee7.b(date, Constants.PROFILE_KEY_BIRTHDAY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        yz6 yz6 = this.q;
        if (yz6 != null) {
            yz6.a().a(date);
            dismiss();
            return;
        }
        ee7.d("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951897);
    }

    @DexIgnore
    @Override // com.fossil.ns3, com.fossil.m0, com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(s, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558503, null);
        ee7.a((Object) inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        ms3 ms3 = new ms3(requireContext(), getTheme());
        ms3.setContentView(inflate);
        ms3.setCanceledOnTouchOutside(true);
        return ms3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ViewDataBinding a2 = qb.a(layoutInflater, 2131558503, viewGroup, false);
        ee7.a((Object) a2, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        kx4 kx4 = (kx4) a2;
        this.p = new qw6<>(this, kx4);
        return kx4.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        oq6 oq6 = this.j;
        if (oq6 != null) {
            oq6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onStop() {
        super.onStop();
        oq6 oq6 = this.j;
        if (oq6 != null) {
            oq6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        he a2 = je.a(requireActivity()).a(yz6.class);
        ee7.a((Object) a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.q = (yz6) a2;
        kx4 b1 = b1();
        if (!(b1 == null || (numberPicker3 = b1.v) == null)) {
            DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
            ee7.a((Object) instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
            numberPicker3.setDisplayedValues(instance.getShortMonths());
            numberPicker3.setOnValueChangedListener(new b(this));
        }
        kx4 b12 = b1();
        if (!(b12 == null || (numberPicker2 = b12.u) == null)) {
            numberPicker2.setOnValueChangedListener(new c(this));
        }
        kx4 b13 = b1();
        if (!(b13 == null || (numberPicker = b13.w) == null)) {
            numberPicker.setOnValueChangedListener(new d(this));
        }
        kx4 b14 = b1();
        if (!(b14 == null || (flexibleButton2 = b14.s) == null)) {
            flexibleButton2.setOnClickListener(new e(this));
        }
        kx4 b15 = b1();
        if (b15 != null && (flexibleButton = b15.r) != null) {
            flexibleButton.setOnClickListener(new f(this));
        }
    }

    @DexIgnore
    public void a(oq6 oq6) {
        ee7.b(oq6, "presenter");
        this.j = oq6;
    }

    @DexIgnore
    @Override // com.fossil.pq6
    public void a(r87<Integer, Integer> r87, r87<Integer, Integer> r872, r87<Integer, Integer> r873) {
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        ee7.b(r87, "dayRange");
        ee7.b(r872, "monthRange");
        ee7.b(r873, "yearRange");
        kx4 b1 = b1();
        if (!(b1 == null || (numberPicker6 = b1.w) == null)) {
            ee7.a((Object) numberPicker6, "npYear");
            numberPicker6.setMinValue(r873.getFirst().intValue());
            numberPicker6.setMaxValue(r873.getSecond().intValue());
        }
        kx4 b12 = b1();
        if (!(b12 == null || (numberPicker5 = b12.v) == null)) {
            ee7.a((Object) numberPicker5, "npMonth");
            numberPicker5.setMinValue(r872.getFirst().intValue());
            numberPicker5.setMaxValue(r872.getSecond().intValue());
        }
        kx4 b13 = b1();
        if (!(b13 == null || (numberPicker4 = b13.u) == null)) {
            ee7.a((Object) numberPicker4, "npDay");
            numberPicker4.setMinValue(r87.getFirst().intValue());
            numberPicker4.setMaxValue(r87.getSecond().intValue());
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            kx4 b14 = b1();
            if (!(b14 == null || (numberPicker3 = b14.u) == null)) {
                numberPicker3.setValue(i);
            }
            kx4 b15 = b1();
            if (!(b15 == null || (numberPicker2 = b15.v) == null)) {
                numberPicker2.setValue(i2);
            }
            kx4 b16 = b1();
            if (!(b16 == null || (numberPicker = b16.w) == null)) {
                numberPicker.setValue(i3);
            }
            oq6 oq6 = this.j;
            if (oq6 != null) {
                oq6.a(i);
                oq6 oq62 = this.j;
                if (oq62 != null) {
                    oq62.b(i2);
                    oq6 oq63 = this.j;
                    if (oq63 != null) {
                        oq63.c(i3);
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pq6
    public void a(int i, int i2) {
        NumberPicker numberPicker;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "Update day range: from " + i + " to " + i2);
        kx4 b1 = b1();
        if (b1 != null && (numberPicker = b1.u) != null) {
            ee7.a((Object) numberPicker, "npDay");
            numberPicker.setMinValue(i);
            numberPicker.setMaxValue(i2);
        }
    }
}
