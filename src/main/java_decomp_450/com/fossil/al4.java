package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al4 implements Factory<lh5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public al4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static al4 a(wj4 wj4) {
        return new al4(wj4);
    }

    @DexIgnore
    public static lh5 b(wj4 wj4) {
        lh5 q = wj4.q();
        c87.a(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public lh5 get() {
        return b(this.a);
    }
}
