package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutType;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutType d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bc0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bc0 createFromParcel(Parcel parcel) {
            return new bc0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bc0[] newArray(int i) {
            return new bc0[i];
        }
    }

    @DexIgnore
    public bc0(byte b, int i, WorkoutType workoutType, long j, boolean z) {
        super(cb0.WORKOUT_RESUME, b, i);
        this.d = workoutType;
        this.e = j;
        this.f = z;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(super.a(), r51.F5, yz0.a(this.d)), r51.G5, Long.valueOf(this.e)), r51.E5, Boolean.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(bc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            bc0 bc0 = (bc0) obj;
            return this.d == bc0.d && this.e == bc0.e && this.f == bc0.f;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.ResumeWorkoutRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = Long.valueOf(this.e).hashCode();
        return Boolean.valueOf(this.f).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.d.getValue());
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ bc0(Parcel parcel, zd7 zd7) {
        super(parcel);
        WorkoutType c = yz0.c(parcel.readByte());
        this.d = c == null ? WorkoutType.UNKNOWN : c;
        this.e = parcel.readLong();
        this.f = parcel.readInt() != 1 ? false : true;
    }
}
