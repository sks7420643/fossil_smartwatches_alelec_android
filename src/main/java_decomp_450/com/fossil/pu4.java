package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu4 {
    @DexIgnore
    public final DateTime a(String str) {
        try {
            return zd5.a(DateTimeZone.UTC, str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeUTCStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final String a(DateTime dateTime) {
        String a = zd5.a(DateTimeZone.UTC, dateTime);
        ee7.a((Object) a, "DateHelper.printServerDa\u2026t(DateTimeZone.UTC, date)");
        return mh7.a(a, "Z", "+0000", true);
    }
}
