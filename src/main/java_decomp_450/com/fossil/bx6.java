package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.fy6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.NumberPickerLarge;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx6 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ String b; // = b;
    @DexIgnore
    public static /* final */ bx6 c; // = new bx6();

    /*
    static {
        String simpleName = bx6.class.getSimpleName();
        ee7.a((Object) simpleName, "DialogUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void A(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c().getApplicationContext(), 2131886462));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886778));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "");
    }

    @DexIgnore
    public final void B(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886845);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886874);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void C(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886859);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886755);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void D(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886831));
        fVar.a(2131363307);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(false);
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void E(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, Constants.ACTIVITY);
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886941));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "WRONG_FORMAT_PASSWORD");
    }

    @DexIgnore
    public final void F(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c().getApplicationContext(), 2131886457);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026t__UpToNumberPhotosCanBe)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{20}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, format);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886778));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "");
    }

    @DexIgnore
    public final void G(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886847));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "PROCESS_IMAGE_ERROR");
    }

    @DexIgnore
    public final void H(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886126));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886827));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "QUICK_RESPONSE_WARNING");
    }

    @DexIgnore
    public final void I(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886083));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886386));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "MAX_NUMBER_OF_ALARMS");
    }

    @DexIgnore
    public final void J(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887056));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886152));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886151));
        fVar.a(fragmentManager, "CONFIRM_REMOVE_WATCH_FACE");
    }

    @DexIgnore
    public final void K(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887279));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887543));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887021));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(fragmentManager, "REQUEST_CONTACT_PHONE_SMS_PERMISSION");
    }

    @DexIgnore
    public final void L(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886777);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026ourSmartwatchsFindDevice)");
        c(fragmentManager, a2);
    }

    @DexIgnore
    public final void M(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886873);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886874);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        c(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void N(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886873);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886755);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        c(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void O(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886927));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "RESET_PASS_SUCCESS");
    }

    @DexIgnore
    public final void P(FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886828));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886829));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886827));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void Q(FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886933));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void R(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragment");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887526));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "CONFIRM_SET_ALARM_FAILED");
    }

    @DexIgnore
    public final void S(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558629);
        fVar.a(2131362656);
        fVar.a(2131362656);
        fVar.a(fragmentManager, "DEVICE_SET_DATA_FAILED");
    }

    @DexIgnore
    public final void T(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886129));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886134));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886130));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886898));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "SET TO WATCH FAIL");
    }

    @DexIgnore
    public final void U(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886831));
        fVar.a(2131363307);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void V(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886174));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886173));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886172));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void W(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886740));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886739));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886736));
        fVar.a(false);
        fVar.a(2131363307);
        fVar.a(fragmentManager, "FIRMWARE_UPDATE_FAIL");
    }

    @DexIgnore
    public final void X(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558483);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886332));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886330));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886331));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "FINDING_FRIEND");
    }

    @DexIgnore
    public final String a() {
        return b;
    }

    @DexIgnore
    public final void b(String str, FragmentManager fragmentManager) {
        ee7.b(str, "serial");
        ee7.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886771));
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886772);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026AnActiveDeviceIsDisabled)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{str}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363255, format);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886769));
        fVar.a(2131363307);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886767));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "REMOVE_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void c(String str, FragmentManager fragmentManager) {
        ee7.b(str, "serial");
        ee7.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886765));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886764));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886763));
        fVar.a(2131363307);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886762));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "REMOVE_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void d(String str, FragmentManager fragmentManager) {
        ee7.b(str, "serial");
        ee7.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886765));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886764));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886763));
        fVar.a(2131363307);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886762));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "SWITCH_DEVICE_ERASE_FAIL", bundle);
    }

    @DexIgnore
    public final void e(String str, FragmentManager fragmentManager) {
        ee7.b(str, "serial");
        ee7.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886770));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886774));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886769));
        fVar.a(2131363307);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886767));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "SWITCH_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void f(String str, FragmentManager fragmentManager) {
        ee7.b(str, "serial");
        ee7.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886765));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886764));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886763));
        fVar.a(2131363307);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886762));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "SWITCH_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void g(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, Constants.ACTIVITY);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363229);
        fVar.a(2131363307);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886844));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886875));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887021));
        fVar.a(false);
        fVar.a(fragmentManager, "BLUETOOTH_OFF");
    }

    @DexIgnore
    public final void h(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887352));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887458));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887280));
        fVar.a(fragmentManager, "DELETE_THEME");
    }

    @DexIgnore
    public final void i(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887353));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887458));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887280));
        fVar.a(fragmentManager, "APPLY_NEW_COLOR_THEME");
    }

    @DexIgnore
    public final void j(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887042));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887041));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887037));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887036));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ACCOUNT");
    }

    @DexIgnore
    public final void k(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886101));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886100));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886099));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ALARM");
    }

    @DexIgnore
    public final void l(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886782));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886781));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886780));
        fVar.a(fragmentManager, "DOWNLOAD");
    }

    @DexIgnore
    public final void m(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886796));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886795));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "SEARCH_ON_STORE");
    }

    @DexIgnore
    public final void n(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886830));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887182));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "FAIL_DUE_TO_DEVICE_DISCONNECTED");
    }

    @DexIgnore
    public final void o(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886826));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886825));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886824));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886823));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "FEEDBACK_CONFIRM");
    }

    @DexIgnore
    public final void p(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886684));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886683));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD_FUTURE_ERROR");
    }

    @DexIgnore
    public final void q(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886847));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "GENERAL_ERROR");
    }

    @DexIgnore
    public final void r(FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886779));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886742));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void s(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886822));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886819));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886818));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886816));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "HAPPINESS_CONFIRM");
    }

    @DexIgnore
    public final void t(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886511));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886359));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "COMMUTE_TIME_LOAD_LOCATION_FAIL");
    }

    @DexIgnore
    public final void u(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886387));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886386));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "LOCATION_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void v(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886361));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886359));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void w(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886360));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886359));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void x(FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558488);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886743));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886742));
        fVar.a(2131363307);
        fVar.a(false);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void y(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887056));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887055));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887054));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_LOGOUT_ACCOUNT");
    }

    @DexIgnore
    public final void z(FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363307);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886743));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886745));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886746));
        fVar.a(2131363229);
        fVar.a(fragmentManager, "NO_INTERNET_CONNECTION");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i) {
        ee7.b(fragmentManager, "fragmentManager");
        fy6.k e1 = fy6.e1();
        e1.c(0);
        e1.a(false);
        e1.b(i);
        e1.a(-65536);
        e1.b(false);
        e1.a().show(fragmentManager, "COLOR_PICKER_DIALOG");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, String str) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "emailAddress");
        DashbarData dashbarData = new DashbarData(2131362927, i, i2);
        cy6.f fVar = new cy6.f(2131558556);
        fVar.a(2131362395, str);
        fVar.a(dashbarData);
        fVar.a(2131362656);
        fVar.a(2131361937);
        fVar.b(true);
        fVar.a(false);
        fVar.b(R.color.transparent);
        ee7.a((Object) fVar, "AlertDialogFragment.Buil\u2026olor(R.color.transparent)");
        cy6 a2 = fVar.a("EMAIL_OTP_VERIFICATION");
        ee7.a((Object) a2, "builder.create(EMAIL_OTP_VERIFICATION)");
        a2.setStyle(0, 2131951629);
        a2.show(fragmentManager, "EMAIL_OTP_VERIFICATION");
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager, String str, String str2) {
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363342, str);
        fVar.a(2131363255, str2);
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887021));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(fragmentManager, "REQUEST_OPEN_LOCATION_SERVICE");
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager, String str, String str2) {
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, str2);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886747));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886748));
        fVar.a(fragmentManager, "REQUEST_LOCATION_SERVICE_PERMISSION");
    }

    @DexIgnore
    public final void d(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886316));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886321));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886320));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886319));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "LEAVE_CHALLENGE");
    }

    @DexIgnore
    public final void e(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887358));
        fVar.a(2131363255, "Sorry, you are required to log out as you are using unauthorized app");
        fVar.a(false);
        fVar.a(fragmentManager, "AA_WARNING");
    }

    @DexIgnore
    public final void f(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886821));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886820));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886817));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886815));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "APP_RATING_CONFIRM");
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager, String str) {
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363342, str);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886751));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886752));
        fVar.a(fragmentManager, b);
    }

    @DexIgnore
    public final void d(FragmentManager fragmentManager, String str) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "description");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
        fVar.a(2131363255, str);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "LIMIT_WARNING");
    }

    @DexIgnore
    public final void a(int i, String str, FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
            return;
        }
        switch (i) {
            case 401:
                return;
            case MFNetworkReturnCode.CLIENT_TIMEOUT /*{ENCODED_INT: 408}*/:
                r(fragmentManager);
                return;
            case MFNetworkReturnCode.RATE_LIMIT_EXEEDED /*{ENCODED_INT: 429}*/:
                Q(fragmentManager);
                return;
            case 500:
            case 503:
            case 504:
                P(fragmentManager);
                return;
            case 601:
                z(fragmentManager);
                return;
            case 400002:
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886228);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026tTimeShouldBeGreaterThan)");
                a(fragmentManager, a2, a3);
                return;
            case 400605:
            case 400611:
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886224);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026eIsNotAvailableOrAlready)");
                a(fragmentManager, a4, a5);
                return;
            case 400609:
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886226);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026ax100PlayersPerChallenge)");
                a(fragmentManager, a6, a7);
                return;
            default:
                if (TextUtils.isEmpty(str)) {
                    r(fragmentManager);
                    return;
                } else if (str != null) {
                    a(str, fragmentManager);
                    return;
                } else {
                    ee7.a();
                    throw null;
                }
        }
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager, String str) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "deviceName");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887130));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131887129));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887128));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887127));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_DEVICE");
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886775));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886773));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886768));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886766));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(false);
        cy6 a2 = fVar.a(fragmentManager, "ASK_TO_CANCEL_WORKOUT");
        ee7.a((Object) a2, "AlertDialogFragment.Buil\u2026r, ASK_TO_CANCEL_WORKOUT)");
        a2.setCancelable(false);
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558482);
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886174));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886173));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886172));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, String str) {
        ee7.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                r(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        x(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            r(fragmentManager);
                            return;
                        } else if (str != null) {
                            a(str, fragmentManager);
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                }
                P(fragmentManager);
            } else {
                Q(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void a(String str, FragmentManager fragmentManager) {
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
        fVar.a(2131363255, str);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886742));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "SERVER_ERROR");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, ContactGroup contactGroup) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(contactGroup, "contactGroup");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE", contactGroup);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131887504));
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887505);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026move_contact_description)");
        Contact contact = contactGroup.getContacts().get(0);
        ee7.a((Object) contact, "contactGroup.contacts[0]");
        String format = String.format(a2, Arrays.copyOf(new Object[]{contact.getDisplayName()}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363255, format);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887128));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886539));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_CONTACT", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "description");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, "");
        fVar.a(2131363255, str);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886114));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "DND_SCHEDULED_TIME_ERROR");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, bt5 bt5, int i, int i2) {
        String str;
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(bt5, "contactWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER", bt5);
        Contact contact = bt5.getContact();
        if (contact == null || contact.getContactId() != -100) {
            Contact contact2 = bt5.getContact();
            if (contact2 == null || contact2.getContactId() != -200) {
                Contact contact3 = bt5.getContact();
                if (contact3 != null) {
                    str = contact3.getDisplayName();
                    ee7.a((Object) str, "contactWrapper.contact!!.displayName");
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                str = ig5.a(PortfolioApp.g0.c(), 2131886155);
                ee7.a((Object) str, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
            }
        } else {
            str = ig5.a(PortfolioApp.g0.c(), 2131886154);
            ee7.a((Object) str, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
        }
        cy6.f fVar = new cy6.f(2131558482);
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886153);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{str, Integer.valueOf(i), Integer.valueOf(i2)}, 3));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363255, format);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886152));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886151));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_CONTACT", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str, int i, int i2, at5 at5) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "name");
        ee7.b(at5, "appWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_APPWRAPPER", at5);
        cy6.f fVar = new cy6.f(2131558482);
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886153);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{str, String.valueOf(i), String.valueOf(i2)}, 3));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363255, format);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886152));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886151));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_APP", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, int i3, String[] strArr) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(strArr, "displayedValues");
        cy6.f fVar = new cy6.f(2131558478);
        fVar.a(2131362243);
        fVar.a(2131362961);
        fVar.a(2131362853, 1, 12, i);
        fVar.a(2131362854, 0, 59, i2);
        fVar.a(2131362857, 0, 1, i3, NumberPickerLarge.getTwoDigitFormatter(), strArr);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, GoalTrackingData goalTrackingData) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(goalTrackingData, "goalTrackingData");
        Bundle bundle = new Bundle();
        bundle.putSerializable("GOAL_TRACKING_DELETE_BUNDLE", goalTrackingData);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886688));
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886687));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886686));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886685));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "GOAL_TRACKING_DELETE", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363307);
        fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886159));
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886158));
        fVar.a(fragmentManager, "NOTIFICATION_WARNING");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, gb5 gb5, int i) {
        String str;
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(gb5, "type");
        int i2 = ax6.a[gb5.ordinal()];
        if (i2 == 1) {
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131887066);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            str = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 2) {
            we7 we72 = we7.a;
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131887057);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            str = String.format(a3, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 3) {
            we7 we73 = we7.a;
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131887059);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            str = String.format(a4, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 4) {
            we7 we74 = we7.a;
            String a5 = ig5.a(PortfolioApp.g0.c(), 2131887061);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026t__PleaseSetASleepGoalOf)");
            str = String.format(a5, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 != 5) {
            str = "";
        } else {
            we7 we75 = we7.a;
            String a6 = ig5.a(PortfolioApp.g0.c(), 2131887074);
            ee7.a((Object) a6, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            str = String.format(a6, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) str, "java.lang.String.format(format, *args)");
        }
        cy6.f fVar = new cy6.f(2131558480);
        fVar.a(2131363307);
        fVar.a(2131363255, str);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131887065));
        fVar.a(fragmentManager, "GOAL_EXCEED_VALUE");
    }

    @DexIgnore
    public final void a(Integer num, String str, FragmentManager fragmentManager) {
        ee7.b(fragmentManager, "fragmentManager");
        if (num == null) {
            q(fragmentManager);
        } else {
            a(num.intValue(), str, fragmentManager);
        }
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str, String str2) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "title");
        ee7.b(str2, "description");
        cy6.f fVar = new cy6.f(2131558481);
        fVar.a(2131363342, str);
        fVar.a(2131363255, str2);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886741));
        fVar.a(2131363307);
        fVar.a(fragmentManager, "GENERAL_WARNING");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str, String str2, mn4 mn4) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(str, "title");
        ee7.b(str2, "description");
        Bundle bundle = new Bundle();
        bundle.putParcelable("CHALLENGE", mn4);
        cy6.f fVar = new cy6.f(2131558484);
        fVar.a(2131363342, str);
        fVar.a(2131363255, str2);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886320));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131887054));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, "LEAVE_CHALLENGE", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, Bundle bundle, String str, String str2) {
        ee7.b(fragmentManager, "fragmentManager");
        ee7.b(bundle, "bundle");
        ee7.b(str, "type");
        ee7.b(str2, "message");
        cy6.f fVar = new cy6.f(2131558483);
        fVar.a(2131363255, str2);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886320));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886816));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(fragmentManager, str, bundle);
    }
}
