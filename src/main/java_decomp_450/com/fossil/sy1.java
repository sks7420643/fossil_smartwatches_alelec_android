package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.fossil.v02;
import com.fossil.z62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sy1 extends z02<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements z62.a<uy1, GoogleSignInAccount> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.i12] */
        @Override // com.fossil.z62.a
        public final /* synthetic */ GoogleSignInAccount a(uy1 uy1) {
            return uy1.b();
        }

        @DexIgnore
        public /* synthetic */ a(yz1 yz1) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class b {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {1, 2, 3, 4};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public sy1(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, my1.e, googleSignInOptions, new o12());
    }

    @DexIgnore
    public Intent i() {
        Context f = f();
        int i = yz1.a[k() - 1];
        if (i == 1) {
            return ez1.b(f, (GoogleSignInOptions) e());
        }
        if (i != 2) {
            return ez1.c(f, (GoogleSignInOptions) e());
        }
        return ez1.a(f, (GoogleSignInOptions) e());
    }

    @DexIgnore
    public no3<Void> j() {
        return z62.a(ez1.a(b(), f(), k() == b.c));
    }

    @DexIgnore
    public final synchronized int k() {
        if (j == b.a) {
            Context f = f();
            l02 a2 = l02.a();
            int a3 = a2.a(f, q02.a);
            if (a3 == 0) {
                j = b.d;
            } else if (a2.a(f, a3, (String) null) != null || DynamiteModule.a(f, "com.google.android.gms.auth.api.fallback") == 0) {
                j = b.b;
            } else {
                j = b.c;
            }
        }
        return j;
    }

    @DexIgnore
    public sy1(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, (v02) my1.e, (v02.d) googleSignInOptions, (d22) new o12());
    }
}
