package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.device.DeviceImplementation$streamingEventListener$1$1", f = "DeviceImplementation.kt", l = {}, m = "invokeSuspend")
public final class sl0 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ /* synthetic */ on0 c;
    @DexIgnore
    public /* final */ /* synthetic */ xp0 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sl0(on0 on0, xp0 xp0, fb7 fb7) {
        super(2, fb7);
        this.c = on0;
        this.d = xp0;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        sl0 sl0 = new sl0(this.c, this.d, fb7);
        sl0.a = (yi7) obj;
        return sl0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        sl0 sl0 = new sl0(this.c, this.d, fb7);
        sl0.a = yi7;
        return sl0.invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            km1 km1 = this.c.a;
            km1.g.a(km1, this.d);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
