package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l3 extends Resources {
    @DexIgnore
    public static boolean b;
    @DexIgnore
    public /* final */ WeakReference<Context> a;

    @DexIgnore
    public l3(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.a = new WeakReference<>(context);
    }

    @DexIgnore
    public static boolean b() {
        return a() && Build.VERSION.SDK_INT <= 20;
    }

    @DexIgnore
    public final Drawable a(int i) {
        return super.getDrawable(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Context context = this.a.get();
        if (context != null) {
            return w2.a().a(context, this, i);
        }
        return super.getDrawable(i);
    }

    @DexIgnore
    public static boolean a() {
        return b;
    }
}
