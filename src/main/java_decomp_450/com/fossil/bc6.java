package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bc6 implements Factory<ac6> {
    @DexIgnore
    public static ac6 a(yb6 yb6, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        return new ac6(yb6, heartRateSummaryRepository, fitnessDataRepository, userRepository, pj4);
    }
}
