package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public bx7(String str, String str2, int i, int i2, boolean z) {
        ee7.b(str, "id");
        ee7.b(str2, "name");
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = i2;
        this.e = z;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof bx7) {
                bx7 bx7 = (bx7) obj;
                if (ee7.a((Object) this.a, (Object) bx7.a) && ee7.a((Object) this.b, (Object) bx7.b)) {
                    if (this.c == bx7.c) {
                        if (this.d == bx7.d) {
                            if (this.e == bx7.e) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i2 = (((((hashCode + i) * 31) + this.c) * 31) + this.d) * 31;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public String toString() {
        return "GalleryEntity(id=" + this.a + ", name=" + this.b + ", length=" + this.c + ", typeInt=" + this.d + ", isAll=" + this.e + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bx7(String str, String str2, int i, int i2, boolean z, int i3, zd7 zd7) {
        this(str, str2, i, i2, (i3 & 16) != 0 ? false : z);
    }
}
