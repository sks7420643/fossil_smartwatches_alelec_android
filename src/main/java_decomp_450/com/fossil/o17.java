package com.fossil;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Transformation;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o17 {
    @DexIgnore
    public static /* final */ StringBuilder a; // = new StringBuilder();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            sendMessageDelayed(obtainMessage(), 1000);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(11)
    public static class b {
        @DexIgnore
        public static int a(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(12)
    public static class c {
        @DexIgnore
        public static int a(Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public static Downloader a(Context context) {
            return new d17(context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends Thread {
        @DexIgnore
        public e(Runnable runnable) {
            super(runnable);
        }

        @DexIgnore
        public void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements ThreadFactory {
        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new e(runnable);
        }
    }

    @DexIgnore
    public static int a(Bitmap bitmap) {
        int i;
        if (Build.VERSION.SDK_INT >= 12) {
            i = c.a(bitmap);
        } else {
            i = bitmap.getRowBytes() * bitmap.getHeight();
        }
        if (i >= 0) {
            return i;
        }
        throw new IllegalStateException("Negative size: " + bitmap);
    }

    @DexIgnore
    public static boolean b() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    @DexIgnore
    public static Downloader c(Context context) {
        try {
            Class.forName("com.squareup.okhttp.OkHttpClient");
            return d.a(context);
        } catch (ClassNotFoundException unused) {
            return new n17(context);
        }
    }

    @DexIgnore
    public static boolean d(Context context) {
        try {
            if (Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0) {
                return true;
            }
            return false;
        } catch (NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static File b(Context context) {
        File file = new File(context.getApplicationContext().getCacheDir(), "picasso-cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    @DexIgnore
    public static byte[] c(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    @DexIgnore
    public static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static boolean b(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    @DexIgnore
    public static void a() {
        if (!b()) {
            throw new IllegalStateException("Method call should happen from the main thread.");
        }
    }

    @DexIgnore
    public static boolean b(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[12];
        if (inputStream.read(bArr, 0, 12) != 12 || !"RIFF".equals(new String(bArr, 0, 4, "US-ASCII")) || !"WEBP".equals(new String(bArr, 8, 4, "US-ASCII"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static String a(o07 o07) {
        return a(o07, "");
    }

    @DexIgnore
    public static String a(o07 o07, String str) {
        StringBuilder sb = new StringBuilder(str);
        m07 c2 = o07.c();
        if (c2 != null) {
            sb.append(c2.b.d());
        }
        List<m07> d2 = o07.d();
        if (d2 != null) {
            int size = d2.size();
            for (int i = 0; i < size; i++) {
                if (i > 0 || c2 != null) {
                    sb.append(", ");
                }
                sb.append(d2.get(i).b.d());
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static void a(String str, String str2, String str3) {
        a(str, str2, str3, "");
    }

    @DexIgnore
    public static void a(String str, String str2, String str3, String str4) {
        Log.d("Picasso", String.format("%1$-11s %2$-12s %3$s %4$s", str, str2, str3, str4));
    }

    @DexIgnore
    public static String a(g17 g17) {
        String a2 = a(g17, a);
        a.setLength(0);
        return a2;
    }

    @DexIgnore
    public static String a(g17 g17, StringBuilder sb) {
        String str = g17.f;
        if (str != null) {
            sb.ensureCapacity(str.length() + 50);
            sb.append(g17.f);
        } else {
            Uri uri = g17.d;
            if (uri != null) {
                String uri2 = uri.toString();
                sb.ensureCapacity(uri2.length() + 50);
                sb.append(uri2);
            } else {
                sb.ensureCapacity(50);
                sb.append(g17.e);
            }
        }
        sb.append('\n');
        if (g17.m != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            sb.append("rotation:");
            sb.append(g17.m);
            if (g17.p) {
                sb.append('@');
                sb.append(g17.n);
                sb.append('x');
                sb.append(g17.o);
            }
            sb.append('\n');
        }
        if (g17.c()) {
            sb.append("resize:");
            sb.append(g17.h);
            sb.append('x');
            sb.append(g17.i);
            sb.append('\n');
        }
        if (g17.j) {
            sb.append("centerCrop");
            sb.append('\n');
        } else if (g17.k) {
            sb.append("centerInside");
            sb.append('\n');
        }
        List<Transformation> list = g17.g;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                sb.append(g17.g.get(i).key());
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        String[] split = str.split(" ", 2);
        if ("CACHE".equals(split[0])) {
            return true;
        }
        if (split.length == 1) {
            return false;
        }
        try {
            if (!"CONDITIONAL_CACHE".equals(split[0]) || Integer.parseInt(split[1]) != 304) {
                return false;
            }
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    @DexIgnore
    public static long a(File file) {
        long j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j = (((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException unused) {
            j = 5242880;
        }
        return Math.max(Math.min(j, 52428800L), 5242880L);
    }

    @DexIgnore
    public static int a(Context context) {
        ActivityManager activityManager = (ActivityManager) a(context, Constants.ACTIVITY);
        boolean z = (context.getApplicationInfo().flags & 1048576) != 0;
        int memoryClass = activityManager.getMemoryClass();
        if (z && Build.VERSION.SDK_INT >= 11) {
            memoryClass = b.a(activityManager);
        }
        return (memoryClass * 1048576) / 7;
    }

    @DexIgnore
    public static <T> T a(Context context, String str) {
        return (T) context.getSystemService(str);
    }

    @DexIgnore
    public static int a(Resources resources, g17 g17) throws FileNotFoundException {
        Uri uri;
        if (g17.e != 0 || (uri = g17.d) == null) {
            return g17.e;
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            List<String> pathSegments = g17.d.getPathSegments();
            if (pathSegments == null || pathSegments.isEmpty()) {
                throw new FileNotFoundException("No path segments: " + g17.d);
            } else if (pathSegments.size() == 1) {
                try {
                    return Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException unused) {
                    throw new FileNotFoundException("Last path segment is not a resource ID: " + g17.d);
                }
            } else if (pathSegments.size() == 2) {
                return resources.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                throw new FileNotFoundException("More than two path segments: " + g17.d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + g17.d);
        }
    }

    @DexIgnore
    public static Resources a(Context context, g17 g17) throws FileNotFoundException {
        Uri uri;
        if (g17.e != 0 || (uri = g17.d) == null) {
            return context.getResources();
        }
        String authority = uri.getAuthority();
        if (authority != null) {
            try {
                return context.getPackageManager().getResourcesForApplication(authority);
            } catch (PackageManager.NameNotFoundException unused) {
                throw new FileNotFoundException("Unable to obtain resources for package: " + g17.d);
            }
        } else {
            throw new FileNotFoundException("No package provided: " + g17.d);
        }
    }

    @DexIgnore
    public static void a(Looper looper) {
        a aVar = new a(looper);
        aVar.sendMessageDelayed(aVar.obtainMessage(), 1000);
    }
}
