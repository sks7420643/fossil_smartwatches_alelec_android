package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static String b;
    @DexIgnore
    public static /* final */ ke5 c; // = new ke5();

    /*
    static {
        String simpleName = ke5.class.getSimpleName();
        ee7.a((Object) simpleName, "InstallationUUID::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final synchronized String a(Context context) {
        String str;
        ee7.b(context, "context");
        if (b == null) {
            File file = new File(context.getFilesDir(), "INSTALLATION");
            try {
                if (!file.exists()) {
                    b(file);
                }
                b = a(file);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a;
                local.d(str2, ".id(), error=" + e);
            }
        }
        str = b;
        if (str == null) {
            str = "";
        }
        return str;
    }

    @DexIgnore
    public final void b(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        String uuid = UUID.randomUUID().toString();
        ee7.a((Object) uuid, "UUID.randomUUID().toString()");
        Charset charset = sg7.a;
        if (uuid != null) {
            byte[] bytes = uuid.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String a(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        byte[] bArr = new byte[((int) randomAccessFile.length())];
        randomAccessFile.readFully(bArr);
        randomAccessFile.close();
        return new String(bArr, sg7.a);
    }
}
