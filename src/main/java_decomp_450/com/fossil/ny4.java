package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ny4 extends my4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362562, 1);
        H.put(2131361971, 2);
        H.put(2131362276, 3);
        H.put(2131363401, 4);
        H.put(2131361902, 5);
        H.put(2131362743, 6);
        H.put(2131362125, 7);
        H.put(2131362755, 8);
        H.put(2131362711, 9);
        H.put(2131362751, 10);
        H.put(2131362333, 11);
        H.put(2131361917, 12);
        H.put(2131362769, 13);
        H.put(2131363018, 14);
    }
    */

    @DexIgnore
    public ny4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 15, G, H));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public ny4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleAutoCompleteTextView) objArr[5], (View) objArr[12], (FlexibleButton) objArr[2], (ImageView) objArr[7], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[11], (RTLImageView) objArr[1], (ImageView) objArr[9], (ConstraintLayout) objArr[6], (View) objArr[10], (ImageView) objArr[8], (LinearLayout) objArr[13], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[14], (View) objArr[4]);
        this.F = -1;
        ((my4) this).C.setTag(null);
        a(view);
        f();
    }
}
