package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.za2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb2 implements za2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity a;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ za2 d;

    @DexIgnore
    public fb2(za2 za2, Activity activity, Bundle bundle, Bundle bundle2) {
        this.d = za2;
        this.a = activity;
        this.b = bundle;
        this.c = bundle2;
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final void a(bb2 bb2) {
        this.d.a.a(this.a, this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final int getState() {
        return 0;
    }
}
