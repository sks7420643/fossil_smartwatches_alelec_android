package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy3 {
    @DexIgnore
    public static /* final */ k04<Object> a; // = new c();
    @DexIgnore
    public static /* final */ Iterator<Object> b; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pw3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Object[] c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Object[] objArr, int i3) {
            super(i, i2);
            this.c = objArr;
            this.d = i3;
        }

        @DexIgnore
        @Override // com.fossil.pw3
        public T a(int i) {
            return (T) this.c[this.d + i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends j04<T> {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public b(Object obj) {
            this.b = obj;
        }

        @DexIgnore
        public boolean hasNext() {
            return !this.a;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.a) {
                this.a = true;
                return (T) this.b;
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends k04<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public boolean hasPrevious() {
            return false;
        }

        @DexIgnore
        @Override // java.util.Iterator, java.util.ListIterator
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int nextIndex() {
            return 0;
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public Object previous() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Iterator<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            bx3.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends j04<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public e(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            return (T) this.a.next();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends qw3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator c;
        @DexIgnore
        public /* final */ /* synthetic */ kw3 d;

        @DexIgnore
        public f(Iterator it, kw3 kw3) {
            this.c = it;
            this.d = kw3;
        }

        @DexIgnore
        @Override // com.fossil.qw3
        public T a() {
            while (this.c.hasNext()) {
                T t = (T) this.c.next();
                if (this.d.apply(t)) {
                    return t;
                }
            }
            return (T) b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends h04<F, T> {
        @DexIgnore
        public /* final */ /* synthetic */ cw3 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(Iterator it, cw3 cw3) {
            super(it);
            this.b = cw3;
        }

        @DexIgnore
        @Override // com.fossil.h04
        public T a(F f) {
            return (T) this.b.apply(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h<E> implements kz3<E> {
        @DexIgnore
        public /* final */ Iterator<? extends E> a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public E c;

        @DexIgnore
        public h(Iterator<? extends E> it) {
            jw3.a(it);
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b || this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator, com.fossil.kz3
        public E next() {
            if (!this.b) {
                return (E) this.a.next();
            }
            E e = this.c;
            this.b = false;
            this.c = null;
            return e;
        }

        @DexIgnore
        @Override // com.fossil.kz3
        public E peek() {
            if (!this.b) {
                this.c = (E) this.a.next();
                this.b = true;
            }
            return this.c;
        }

        @DexIgnore
        public void remove() {
            jw3.b(!this.b, "Can't remove after you've peeked at next");
            this.a.remove();
        }
    }

    @DexIgnore
    public static <T> j04<T> a() {
        return b();
    }

    @DexIgnore
    public static <T> k04<T> b() {
        return (k04<T>) a;
    }

    @DexIgnore
    public static <T> Iterator<T> c() {
        return (Iterator<T>) b;
    }

    @DexIgnore
    public static String d(Iterator<?> it) {
        ew3 ew3 = cx3.a;
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        ew3.a(sb, it);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public static <T> j04<T> e(Iterator<? extends T> it) {
        jw3.a(it);
        if (it instanceof j04) {
            return (j04) it;
        }
        return new e(it);
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Object obj) {
        return b((Iterator) it, lw3.a(obj));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static boolean b(Iterator<?> it, Collection<?> collection) {
        return e(it, lw3.a(lw3.a((Collection) collection)));
    }

    @DexIgnore
    public static <T> j04<T> c(Iterator<T> it, kw3<? super T> kw3) {
        jw3.a(it);
        jw3.a(kw3);
        return new f(it, kw3);
    }

    @DexIgnore
    public static <T> int d(Iterator<T> it, kw3<? super T> kw3) {
        jw3.a(kw3, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (kw3.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static boolean a(Iterator<?> it, Collection<?> collection) {
        return e(it, lw3.a((Collection) collection));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T b(Iterator<T> it) {
        T next = it.next();
        if (!it.hasNext()) {
            return next;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("expected one element but was: <");
        sb.append((Object) next);
        for (int i = 0; i < 4 && it.hasNext(); i++) {
            sb.append(", ");
            sb.append((Object) it.next());
        }
        if (it.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.util.Iterator<?> r3, java.util.Iterator<?> r4) {
        /*
        L_0x0000:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x001d
            boolean r0 = r4.hasNext()
            r1 = 0
            if (r0 != 0) goto L_0x000e
            return r1
        L_0x000e:
            java.lang.Object r0 = r3.next()
            java.lang.Object r2 = r4.next()
            boolean r0 = com.fossil.gw3.a(r0, r2)
            if (r0 != 0) goto L_0x0000
            return r1
        L_0x001d:
            boolean r3 = r4.hasNext()
            r3 = r3 ^ 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qy3.a(java.util.Iterator, java.util.Iterator):boolean");
    }

    @DexIgnore
    public static <T> kz3<T> c(Iterator<? extends T> it) {
        if (it instanceof h) {
            return (h) it;
        }
        return new h(it);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> boolean e(Iterator<T> it, kw3<? super T> kw3) {
        jw3.a(kw3);
        boolean z = false;
        while (it.hasNext()) {
            if (kw3.apply(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Collection<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it) {
        jw3.a(collection);
        jw3.a(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    @DexIgnore
    public static <T> boolean b(Iterator<T> it, kw3<? super T> kw3) {
        return d(it, kw3) != -1;
    }

    @DexIgnore
    public static <T> T b(Iterator<? extends T> it, T t) {
        return it.hasNext() ? (T) it.next() : t;
    }

    @DexIgnore
    public static <T> boolean a(Iterator<T> it, kw3<? super T> kw3) {
        jw3.a(kw3);
        while (it.hasNext()) {
            if (!kw3.apply(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <F, T> Iterator<T> a(Iterator<F> it, cw3<? super F, ? extends T> cw3) {
        jw3.a(cw3);
        return new g(it, cw3);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int a(Iterator<?> it, int i) {
        jw3.a(it);
        int i2 = 0;
        jw3.a(i >= 0, "numberToAdvance must be nonnegative");
        while (i2 < i && it.hasNext()) {
            it.next();
            i2++;
        }
        return i2;
    }

    @DexIgnore
    public static void a(Iterator<?> it) {
        jw3.a(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @DexIgnore
    @SafeVarargs
    public static <T> j04<T> a(T... tArr) {
        return a(tArr, 0, tArr.length, 0);
    }

    @DexIgnore
    public static <T> k04<T> a(T[] tArr, int i, int i2, int i3) {
        jw3.a(i2 >= 0);
        jw3.b(i, i + i2, tArr.length);
        jw3.b(i3, i2);
        if (i2 == 0) {
            return b();
        }
        return new a(i2, i3, tArr, i);
    }

    @DexIgnore
    public static <T> j04<T> a(T t) {
        return new b(t);
    }
}
