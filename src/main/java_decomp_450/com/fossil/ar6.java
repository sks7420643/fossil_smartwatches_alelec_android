package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.gx6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar6 extends ho5 implements gr6 {
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<u45> g;
    @DexIgnore
    public fr6 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ar6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            ar6 ar6 = new ar6();
            ar6.setArguments(bundle);
            return ar6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ar6 a;

        @DexIgnore
        public b(ar6 ar6, String str) {
            this.a = ar6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ar6.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ar6 a;

        @DexIgnore
        public c(ar6 ar6, String str) {
            this.a = ar6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                if (this.a.i) {
                    HomeActivity.a aVar = HomeActivity.z;
                    ee7.a((Object) activity, "fragmentActivity");
                    HomeActivity.a.a(aVar, activity, null, 2, null);
                }
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ar6 a;

        @DexIgnore
        public d(ar6 ar6, String str) {
            this.a = ar6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore
    public static final /* synthetic */ fr6 b(ar6 ar6) {
        fr6 fr6 = ar6.h;
        if (fr6 != null) {
            return fr6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gr6
    public void N() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.A;
            ee7.a((Object) activity, "fragmentActivity");
            fr6 fr6 = this.h;
            if (fr6 != null) {
                aVar.a(activity, fr6.i());
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.gr6
    public void e() {
        DashBar dashBar;
        qw6<u45> qw6 = this.g;
        if (qw6 != null) {
            u45 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.e(dashBar, this.i, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentActivity activity;
        fr6 fr6 = this.h;
        if (fr6 == null) {
            ee7.d("mPresenter");
            throw null;
        } else if (fr6.i() || (activity = getActivity()) == null) {
            return false;
        } else {
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.gr6
    public void i(boolean z) {
        ImageView imageView;
        if (isActive()) {
            qw6<u45> qw6 = this.g;
            if (qw6 != null) {
                u45 a2 = qw6.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    ee7.a((Object) imageView, "it");
                    imageView.setVisibility(!z ? 4 : 0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        u45 u45 = (u45) qb.a(layoutInflater, 2131558603, viewGroup, false, a1());
        this.g = new qw6<>(this, u45);
        ee7.a((Object) u45, "binding");
        return u45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        fr6 fr6 = this.h;
        if (fr6 != null) {
            fr6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        fr6 fr6 = this.h;
        if (fr6 != null) {
            fr6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886894);
        qw6<u45> qw6 = this.g;
        if (qw6 != null) {
            u45 a3 = qw6.a();
            if (a3 != null) {
                a3.r.setOnClickListener(new b(this, a2));
                a3.s.setOnClickListener(new c(this, a2));
                a3.w.setOnClickListener(new d(this, a2));
                FlexibleTextView flexibleTextView = a3.v;
                ee7.a((Object) flexibleTextView, "binding.ftvWearOs");
                flexibleTextView.setText(a2);
                FlexibleTextView flexibleTextView2 = a3.v;
                ee7.a((Object) flexibleTextView2, "binding.ftvWearOs");
                flexibleTextView2.setTypeface(flexibleTextView2.getTypeface(), 1);
                if (!tm4.a.a().d()) {
                    RelativeLayout relativeLayout = a3.A;
                    ee7.a((Object) relativeLayout, "binding.rlWearOsGroup");
                    relativeLayout.setVisibility(8);
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.i = z;
                fr6 fr6 = this.h;
                if (fr6 != null) {
                    fr6.a(z);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(fr6 fr6) {
        ee7.b(fr6, "presenter");
        this.h = fr6;
    }
}
