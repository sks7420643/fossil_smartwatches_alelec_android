package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r73 extends wm2 implements q73 {
    @DexIgnore
    public r73() {
        super("com.google.android.gms.maps.internal.IOnMarkerClickListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        boolean a = a(fn2.a(parcel.readStrongBinder()));
        parcel2.writeNoException();
        xm2.a(parcel2, a);
        return true;
    }
}
