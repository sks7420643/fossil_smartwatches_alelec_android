package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj1 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.ASYNC}));
    @DexIgnore
    public /* final */ xp0 D;

    @DexIgnore
    public hj1(ri1 ri1, en0 en0, xp0 xp0) {
        super(ri1, en0, wm0.L, null, false, 24);
        this.D = xp0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new ee1(((zk0) this).w, this.D), ud1.a, of1.a, (kd7) null, new kh1(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.j1, this.D.a());
    }
}
