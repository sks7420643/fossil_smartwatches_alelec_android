package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn7 {
    @DexIgnore
    public static /* final */ lm7 a; // = new lm7("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ lm7 b; // = new lm7("LOCKED");
    @DexIgnore
    public static /* final */ lm7 c; // = new lm7("UNLOCKED");
    @DexIgnore
    public static /* final */ jn7 d; // = new jn7(b);
    @DexIgnore
    public static /* final */ jn7 e; // = new jn7(c);

    /*
    static {
        new lm7("LOCK_FAIL");
        new lm7("ENQUEUE_FAIL");
        new lm7("SELECT_SUCCESS");
    }
    */

    @DexIgnore
    public static /* synthetic */ kn7 a(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }

    @DexIgnore
    public static final kn7 a(boolean z) {
        return new ln7(z);
    }
}
