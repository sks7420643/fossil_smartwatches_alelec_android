package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wy6 {
    @DexIgnore
    public static /* final */ ExecutorService f; // = Executors.newCachedThreadPool();
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ vy6 c;
    @DexIgnore
    public /* final */ Bitmap d;
    @DexIgnore
    public /* final */ b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wy6$a$a")
        /* renamed from: com.fossil.wy6$a$a  reason: collision with other inner class name */
        public class RunnableC0236a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ BitmapDrawable a;

            @DexIgnore
            public RunnableC0236a(BitmapDrawable bitmapDrawable) {
                this.a = bitmapDrawable;
            }

            @DexIgnore
            public void run() {
                wy6.this.e.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            wy6 wy6 = wy6.this;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(wy6.a, ry6.a(wy6.this.b.get(), wy6.d, wy6.c));
            if (wy6.this.e != null) {
                new Handler(Looper.getMainLooper()).post(new RunnableC0236a(bitmapDrawable));
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(BitmapDrawable bitmapDrawable);
    }

    @DexIgnore
    public wy6(Context context, Bitmap bitmap, vy6 vy6, b bVar) {
        this.a = context.getResources();
        this.c = vy6;
        this.e = bVar;
        this.b = new WeakReference<>(context);
        this.d = bitmap;
    }

    @DexIgnore
    public void a() {
        f.execute(new a());
    }
}
