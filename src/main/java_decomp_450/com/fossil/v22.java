package com.fossil;

import com.fossil.v02;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v22 extends a32 {
    @DexIgnore
    public /* final */ Map<v02.f, s22> b;
    @DexIgnore
    public /* final */ /* synthetic */ q22 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v22(q22 q22, Map<v02.f, s22> map) {
        super(q22, null);
        this.c = q22;
        this.b = map;
    }

    @DexIgnore
    @Override // com.fossil.a32
    public final void a() {
        r62 r62 = new r62(this.c.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (v02.f fVar : this.b.keySet()) {
            if (!fVar.j() || this.b.get(fVar).c) {
                arrayList2.add(fVar);
            } else {
                arrayList.add(fVar);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = r62.a(this.c.c, (v02.f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = r62.a(this.c.c, (v02.f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.c.a.a(new u22(this, this.c, new i02(i, null)));
            return;
        }
        if (this.c.m && this.c.k != null) {
            this.c.k.b();
        }
        for (v02.f fVar2 : this.b.keySet()) {
            s22 s22 = this.b.get(fVar2);
            if (!fVar2.j() || r62.a(this.c.c, fVar2) == 0) {
                fVar2.a(s22);
            } else {
                this.c.a.a(new x22(this, this.c, s22));
            }
        }
    }
}
