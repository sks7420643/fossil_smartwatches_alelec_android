package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f54 extends v54.d {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ Long d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ v54.d.a f;
    @DexIgnore
    public /* final */ v54.d.f g;
    @DexIgnore
    public /* final */ v54.d.e h;
    @DexIgnore
    public /* final */ v54.d.c i;
    @DexIgnore
    public /* final */ w54<v54.d.AbstractC0206d> j;
    @DexIgnore
    public /* final */ int k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.b {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public v54.d.a f;
        @DexIgnore
        public v54.d.f g;
        @DexIgnore
        public v54.d.e h;
        @DexIgnore
        public v54.d.c i;
        @DexIgnore
        public w54<v54.d.AbstractC0206d> j;
        @DexIgnore
        public Integer k;

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null generator");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b b(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b(v54.d dVar) {
            this.a = dVar.e();
            this.b = dVar.g();
            this.c = Long.valueOf(dVar.j());
            this.d = dVar.c();
            this.e = Boolean.valueOf(dVar.l());
            this.f = dVar.a();
            this.g = dVar.k();
            this.h = dVar.i();
            this.i = dVar.b();
            this.j = dVar.d();
            this.k = Integer.valueOf(dVar.f());
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(long j2) {
            this.c = Long.valueOf(j2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(Long l) {
            this.d = l;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(v54.d.a aVar) {
            if (aVar != null) {
                this.f = aVar;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(v54.d.f fVar) {
            this.g = fVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(v54.d.e eVar) {
            this.h = eVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(v54.d.c cVar) {
            this.i = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(w54<v54.d.AbstractC0206d> w54) {
            this.j = w54;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d.b a(int i2) {
            this.k = Integer.valueOf(i2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.b
        public v54.d a() {
            String str = "";
            if (this.a == null) {
                str = str + " generator";
            }
            if (this.b == null) {
                str = str + " identifier";
            }
            if (this.c == null) {
                str = str + " startedAt";
            }
            if (this.e == null) {
                str = str + " crashed";
            }
            if (this.f == null) {
                str = str + " app";
            }
            if (this.k == null) {
                str = str + " generatorType";
            }
            if (str.isEmpty()) {
                return new f54(this.a, this.b, this.c.longValue(), this.d, this.e.booleanValue(), this.f, this.g, this.h, this.i, this.j, this.k.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public v54.d.a a() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public v54.d.c b() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public Long c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public w54<v54.d.AbstractC0206d> d() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public String e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Long l;
        v54.d.f fVar;
        v54.d.e eVar;
        v54.d.c cVar;
        w54<v54.d.AbstractC0206d> w54;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d)) {
            return false;
        }
        v54.d dVar = (v54.d) obj;
        if (!this.a.equals(dVar.e()) || !this.b.equals(dVar.g()) || this.c != dVar.j() || ((l = this.d) != null ? !l.equals(dVar.c()) : dVar.c() != null) || this.e != dVar.l() || !this.f.equals(dVar.a()) || ((fVar = this.g) != null ? !fVar.equals(dVar.k()) : dVar.k() != null) || ((eVar = this.h) != null ? !eVar.equals(dVar.i()) : dVar.i() != null) || ((cVar = this.i) != null ? !cVar.equals(dVar.b()) : dVar.b() != null) || ((w54 = this.j) != null ? !w54.equals(dVar.d()) : dVar.d() != null) || this.k != dVar.f()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public int f() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public String g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        long j2 = this.c;
        int hashCode = (((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003;
        Long l = this.d;
        int i2 = 0;
        int hashCode2 = (((((hashCode ^ (l == null ? 0 : l.hashCode())) * 1000003) ^ (this.e ? 1231 : 1237)) * 1000003) ^ this.f.hashCode()) * 1000003;
        v54.d.f fVar = this.g;
        int hashCode3 = (hashCode2 ^ (fVar == null ? 0 : fVar.hashCode())) * 1000003;
        v54.d.e eVar = this.h;
        int hashCode4 = (hashCode3 ^ (eVar == null ? 0 : eVar.hashCode())) * 1000003;
        v54.d.c cVar = this.i;
        int hashCode5 = (hashCode4 ^ (cVar == null ? 0 : cVar.hashCode())) * 1000003;
        w54<v54.d.AbstractC0206d> w54 = this.j;
        if (w54 != null) {
            i2 = w54.hashCode();
        }
        return ((hashCode5 ^ i2) * 1000003) ^ this.k;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public v54.d.e i() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public long j() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public v54.d.f k() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public boolean l() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d
    public v54.d.b m() {
        return new b(this);
    }

    @DexIgnore
    public String toString() {
        return "Session{generator=" + this.a + ", identifier=" + this.b + ", startedAt=" + this.c + ", endedAt=" + this.d + ", crashed=" + this.e + ", app=" + this.f + ", user=" + this.g + ", os=" + this.h + ", device=" + this.i + ", events=" + this.j + ", generatorType=" + this.k + "}";
    }

    @DexIgnore
    public f54(String str, String str2, long j2, Long l, boolean z, v54.d.a aVar, v54.d.f fVar, v54.d.e eVar, v54.d.c cVar, w54<v54.d.AbstractC0206d> w54, int i2) {
        this.a = str;
        this.b = str2;
        this.c = j2;
        this.d = l;
        this.e = z;
        this.f = aVar;
        this.g = fVar;
        this.h = eVar;
        this.i = cVar;
        this.j = w54;
        this.k = i2;
    }
}
