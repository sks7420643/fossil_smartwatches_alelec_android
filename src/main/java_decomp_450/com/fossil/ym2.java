package com.fossil;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ym2 extends IInterface {
    @DexIgnore
    ab2 d() throws RemoteException;

    @DexIgnore
    ab2 d(float f) throws RemoteException;

    @DexIgnore
    ab2 zza(Bitmap bitmap) throws RemoteException;

    @DexIgnore
    ab2 zza(String str) throws RemoteException;
}
