package com.fossil;

import android.os.Bundle;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pf6 extends cl4 {
    @DexIgnore
    public abstract void a(Bundle bundle);

    @DexIgnore
    public abstract void a(GoalTrackingData goalTrackingData);

    @DexIgnore
    public abstract void a(Date date);

    @DexIgnore
    public abstract void b(Date date);

    @DexIgnore
    public abstract void c(Date date);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
