package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw6 extends fl4<b, Object, c> {
    @DexIgnore
    public /* final */ nw5 d;
    @DexIgnore
    public /* final */ vu5 e;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase f;
    @DexIgnore
    public /* final */ NotificationsRepository g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ ch5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public c(String str) {
            ee7.b(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.SetNotificationUseCase", f = "SetNotificationUseCase.kt", l = {38}, m = "run")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ kw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kw6 kw6, fb7 fb7) {
            super(fb7);
            this.this$0 = kw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public kw6(nw5 nw5, vu5 vu5, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, ch5 ch5) {
        ee7.b(nw5, "mGetApp");
        ee7.b(vu5, "mGetAllContactGroups");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        ee7.b(notificationsRepository, "mNotificationsRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharePref");
        this.d = nw5;
        this.e = vu5;
        this.f = notificationSettingsDatabase;
        this.g = notificationsRepository;
        this.h = deviceRepository;
        this.i = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetNotificationUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.kw6.b r11, com.fossil.fb7<java.lang.Object> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.kw6.d
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.kw6$d r0 = (com.fossil.kw6.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.kw6$d r0 = new com.fossil.kw6$d
            r0.<init>(r10, r12)
        L_0x0018:
            r6 = r0
            java.lang.Object r12 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            java.lang.String r7 = "SetNotificationUseCase"
            r2 = 1
            if (r1 == 0) goto L_0x0040
            if (r1 != r2) goto L_0x0038
            java.lang.Object r11 = r6.L$2
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r0 = r6.L$1
            com.fossil.kw6$b r0 = (com.fossil.kw6.b) r0
            java.lang.Object r0 = r6.L$0
            com.fossil.kw6 r0 = (com.fossil.kw6) r0
            com.fossil.t87.a(r12)
            goto L_0x007b
        L_0x0038:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0040:
            com.fossil.t87.a(r12)
            if (r11 == 0) goto L_0x00e2
            java.lang.String r12 = r11.a()
            boolean r12 = com.fossil.mh7.a(r12)
            if (r12 == 0) goto L_0x0051
            goto L_0x00e2
        L_0x0051:
            java.lang.String r12 = r11.a()
            boolean r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r12)
            if (r1 == 0) goto L_0x0088
            com.fossil.nx6 r1 = com.fossil.nx6.b
            com.fossil.nw5 r3 = r10.d
            com.fossil.vu5 r4 = r10.e
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r5 = r10.f
            com.fossil.ch5 r8 = r10.i
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.label = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r8
            java.lang.Object r11 = r1.a(r2, r3, r4, r5, r6)
            if (r11 != r0) goto L_0x0078
            return r0
        L_0x0078:
            r9 = r12
            r12 = r11
            r11 = r9
        L_0x007b:
            java.util.List r12 = (java.util.List) r12
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r0 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            long r1 = java.lang.System.currentTimeMillis()
            r0.<init>(r12, r1)
            r12 = r11
            goto L_0x00ac
        L_0x0088:
            com.portfolio.platform.data.source.NotificationsRepository r11 = r10.g
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r0 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            int r0 = r0.getValue()
            android.util.SparseArray r11 = r11.getAllNotificationsByHour(r12, r0)
            com.portfolio.platform.data.source.DeviceRepository r0 = r10.h
            com.fossil.be5$a r1 = com.fossil.be5.o
            java.lang.String r1 = r1.b(r12)
            com.portfolio.platform.data.model.SKUModel r0 = r0.getSkuModelBySerialPrefix(r1)
            com.fossil.nx6 r1 = com.fossil.nx6.b
            boolean r0 = r1.a(r0, r12)
            com.fossil.nx6 r1 = com.fossil.nx6.b
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r0 = r1.a(r11, r0)
        L_0x00ac:
            com.portfolio.platform.PortfolioApp$a r11 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r11 = r11.c()
            r11.a(r0, r12)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r1 = "saveNotificationSettingToDevice, total: "
            r12.append(r1)
            java.util.List r0 = r0.getNotificationFilters()
            int r0 = r0.size()
            r12.append(r0)
            java.lang.String r0 = " items"
            r12.append(r0)
            java.lang.String r12 = r12.toString()
            r11.d(r7, r12)
            java.lang.Object r11 = new java.lang.Object
            r11.<init>()
            return r11
        L_0x00e2:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid request: Request = "
            r0.append(r1)
            r0.append(r11)
            java.lang.String r11 = r0.toString()
            r12.d(r7, r11)
            com.fossil.kw6$c r11 = new com.fossil.kw6$c
            java.lang.String r12 = "Invalid request"
            r11.<init>(r12)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kw6.a(com.fossil.kw6$b, com.fossil.fb7):java.lang.Object");
    }
}
