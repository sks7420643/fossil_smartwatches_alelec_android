package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt1 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public bt1(String str) {
        if (str != null) {
            this.a = str;
            return;
        }
        throw new NullPointerException("name is null");
    }

    @DexIgnore
    public static bt1 a(String str) {
        return new bt1(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bt1)) {
            return false;
        }
        return this.a.equals(((bt1) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Encoding{name=\"" + this.a + "\"}";
    }

    @DexIgnore
    public String a() {
        return this.a;
    }
}
