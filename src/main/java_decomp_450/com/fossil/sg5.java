package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.HybridNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppFilterHistory;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.MessageComparator;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.data.model.NotificationPriority;
import com.portfolio.platform.data.source.DeviceRepository;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sg5 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static sg5 h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public ch5 b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<LightAndHaptics> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ Runnable f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(sg5 sg5) {
            sg5.h = sg5;
        }

        @DexIgnore
        public final sg5 b() {
            return sg5.h;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final sg5 a() {
            if (b() == null) {
                a(new sg5(null));
            }
            sg5 b = b();
            if (b != null) {
                return b;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sg5 sg5, NotificationInfo notificationInfo, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sg5;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$info, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                sg5 sg5 = this.this$0;
                String packageName = this.$info.getPackageName();
                ee7.a((Object) packageName, "info.packageName");
                LightAndHaptics a = sg5.a(packageName, this.$info.getBody());
                if (a != null) {
                    a.setNotificationType(NotificationType.APP_FILTER);
                    this.this$0.a(a);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstEmail$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sg5 sg5, NotificationInfo notificationInfo, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sg5;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$info, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = sg5.g;
                local.e(e, "Check again email with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_EMAIL.name(), this.$info.getBody());
                if (a != null) {
                    this.this$0.a(a);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstPhone$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(sg5 sg5, NotificationInfo notificationInfo, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sg5;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$info, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = sg5.g;
                local.e(e, "Check again phone with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_CALLS.name(), this.$info.getSenderInfo());
                if (a != null) {
                    a.setNotificationType(NotificationType.CALL);
                    this.this$0.a(a);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(sg5 sg5, NotificationInfo notificationInfo, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sg5;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$info, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e = sg5.g;
                local.e(e, "Check again sms with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics a = this.this$0.a(AppType.ALL_SMS.name(), this.$info.getBody());
                if (a != null) {
                    a.setNotificationType(NotificationType.SMS);
                    this.this$0.a(a);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ sg5 a;

        @DexIgnore
        public f(sg5 sg5) {
            this.a = sg5;
        }

        @DexIgnore
        public final void run() {
            this.a.b();
        }
    }

    /*
    static {
        String simpleName = sg5.class.getSimpleName();
        ee7.a((Object) simpleName, "LightAndHapticsManager::class.java.simpleName");
        g = simpleName;
        ee7.a((Object) Arrays.asList(CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR), "Arrays.asList(Calibratio\u2026brationEnums.HandId.HOUR)");
        ee7.a((Object) Arrays.asList(CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR, CalibrationEnums.HandId.SUB_EYE), "Arrays.asList(Calibratio\u2026tionEnums.HandId.SUB_EYE)");
    }
    */

    @DexIgnore
    public sg5() {
        PortfolioApp.g0.c().f().a(this);
        this.c = new PriorityBlockingQueue<>(5, new MessageComparator());
        this.e = new Handler(Looper.getMainLooper());
        this.f = new f(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0095 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0178  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x017c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(com.portfolio.platform.data.model.NotificationInfo r12) {
        /*
            r11 = this;
            com.portfolio.platform.data.NotificationSource r0 = r12.getSource()
            com.portfolio.platform.data.NotificationSource r1 = com.portfolio.platform.data.NotificationSource.TEXT
            java.lang.String r2 = "-1234"
            java.lang.String r3 = "-5678"
            r4 = 0
            if (r0 != r1) goto L_0x0038
            com.fossil.ox6 r0 = com.fossil.ox6.a()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r1 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            java.util.List r0 = r0.b(r3, r1)
            java.lang.String r1 = r12.getSenderInfo()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0036
            com.fossil.ox6 r1 = com.fossil.ox6.a()
            java.lang.String r5 = r12.getSenderInfo()
            if (r5 == 0) goto L_0x0032
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r6 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            java.util.List r1 = r1.b(r5, r6)
            goto L_0x006b
        L_0x0032:
            com.fossil.ee7.a()
            throw r4
        L_0x0036:
            r1 = r4
            goto L_0x006b
        L_0x0038:
            com.portfolio.platform.data.NotificationSource r0 = r12.getSource()
            com.portfolio.platform.data.NotificationSource r1 = com.portfolio.platform.data.NotificationSource.CALL
            if (r0 != r1) goto L_0x0069
            com.fossil.ox6 r0 = com.fossil.ox6.a()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r1 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            java.util.List r0 = r0.a(r2, r1)
            java.lang.String r1 = r12.getSenderInfo()
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0036
            com.fossil.ox6 r1 = com.fossil.ox6.a()
            java.lang.String r5 = r12.getSenderInfo()
            if (r5 == 0) goto L_0x0065
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r6 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            java.util.List r1 = r1.a(r5, r6)
            goto L_0x006b
        L_0x0065:
            com.fossil.ee7.a()
            throw r4
        L_0x0069:
            r0 = r4
            r1 = r0
        L_0x006b:
            r5 = 0
            if (r0 == 0) goto L_0x0093
            if (r1 == 0) goto L_0x0093
            boolean r6 = r1.isEmpty()
            if (r6 == 0) goto L_0x0093
            boolean r6 = r0.isEmpty()
            if (r6 == 0) goto L_0x0093
            boolean r0 = r11.a()
            if (r0 == 0) goto L_0x0092
            r11.b(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = com.fossil.sg5.g
            java.lang.String r1 = ".Inside checkAgainstContacts, NO assigned contact with this phone number, NO ALL-TEXT, let Message fire"
            r12.d(r0, r1)
        L_0x0092:
            return r5
        L_0x0093:
            if (r1 != 0) goto L_0x0098
            if (r0 != 0) goto L_0x0098
            return r5
        L_0x0098:
            java.lang.String r6 = "info.source"
            java.lang.String r7 = " matchcontact is "
            java.lang.String r8 = " body "
            r9 = 1
            if (r1 == 0) goto L_0x00b9
            boolean r10 = r1.isEmpty()
            r10 = r10 ^ r9
            if (r10 == 0) goto L_0x00b9
            java.lang.String r0 = r12.getSenderInfo()
            com.portfolio.platform.data.NotificationSource r2 = r12.getSource()
            com.fossil.ee7.a(r2, r6)
            com.portfolio.platform.data.model.LightAndHaptics r4 = r11.a(r0, r1, r2)
            goto L_0x0146
        L_0x00b9:
            com.portfolio.platform.data.NotificationSource r1 = r12.getSource()
            if (r1 != 0) goto L_0x00c1
            goto L_0x0146
        L_0x00c1:
            int[] r10 = com.fossil.tg5.a
            int r1 = r1.ordinal()
            r1 = r10[r1]
            if (r1 == r9) goto L_0x010b
            r3 = 2
            if (r1 == r3) goto L_0x00cf
            goto L_0x0146
        L_0x00cf:
            com.portfolio.platform.data.NotificationSource r1 = r12.getSource()
            com.fossil.ee7.a(r1, r6)
            com.portfolio.platform.data.model.LightAndHaptics r4 = r11.a(r2, r0, r1)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sg5.g
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ALL CALL with "
            r2.append(r3)
            java.lang.String r3 = r12.getSenderInfo()
            r2.append(r3)
            r2.append(r8)
            java.lang.String r3 = r12.getBody()
            r2.append(r3)
            r2.append(r7)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            goto L_0x0146
        L_0x010b:
            com.portfolio.platform.data.NotificationSource r1 = r12.getSource()
            com.fossil.ee7.a(r1, r6)
            com.portfolio.platform.data.model.LightAndHaptics r4 = r11.a(r3, r0, r1)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sg5.g
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ALL TEXT with "
            r2.append(r3)
            java.lang.String r3 = r12.getSenderInfo()
            r2.append(r3)
            r2.append(r8)
            java.lang.String r3 = r12.getBody()
            r2.append(r3)
            r2.append(r7)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
        L_0x0146:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sg5.g
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "checkAgainstContacts with "
            r2.append(r3)
            java.lang.String r3 = r12.getSenderInfo()
            r2.append(r3)
            r2.append(r8)
            java.lang.String r12 = r12.getBody()
            r2.append(r12)
            r2.append(r7)
            r2.append(r4)
            java.lang.String r12 = r2.toString()
            r0.d(r1, r12)
            if (r4 == 0) goto L_0x017c
            r11.a(r4)
            return r9
        L_0x017c:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sg5.c(com.portfolio.platform.data.model.NotificationInfo):boolean");
    }

    @DexIgnore
    public final ik7 f(NotificationInfo notificationInfo) {
        return xh7.b(zi7.a(qj7.a()), null, null, new e(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final void b(NotificationInfo notificationInfo) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        if (!TextUtils.isEmpty(notificationInfo.getPackageName())) {
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new b(this, notificationInfo, null), 3, null);
        }
    }

    @DexIgnore
    public final ik7 d(NotificationInfo notificationInfo) {
        return xh7.b(zi7.a(qj7.a()), null, null, new c(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final ik7 e(NotificationInfo notificationInfo) {
        return xh7.b(zi7.a(qj7.a()), null, null, new d(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo) {
        ee7.b(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "notification info : " + notificationInfo.getSource());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local2.d(str2, "notification body : " + notificationInfo.getBody());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local3.d(str3, "notification type : " + notificationInfo.getSource());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local4.e(str4, "notification - info=" + notificationInfo);
        String packageName = notificationInfo.getPackageName();
        ee7.a((Object) packageName, "info.packageName");
        if (!a(packageName)) {
            if (notificationInfo.getSource() == NotificationSource.CALL || notificationInfo.getSource() == NotificationSource.TEXT || notificationInfo.getSource() == NotificationSource.MAIL) {
                if (!c(notificationInfo)) {
                    if (notificationInfo.getSource() == NotificationSource.CALL) {
                        e(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.TEXT) {
                        f(notificationInfo);
                    } else if (notificationInfo.getSource() == NotificationSource.MAIL) {
                        d(notificationInfo);
                    }
                }
            } else if (notificationInfo.getSource() == NotificationSource.OS) {
                b(notificationInfo);
            }
        }
    }

    @DexIgnore
    public final LightAndHaptics b(String str) {
        Contact a2 = ox6.a().a(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (a2 == null || !a2.isUseSms()) {
            return null;
        }
        NotificationType notificationType = NotificationType.SMS;
        String displayName = a2.getDisplayName();
        ContactGroup contactGroup = a2.getContactGroup();
        ee7.a((Object) contactGroup, "contact.contactGroup");
        return new LightAndHaptics("", notificationType, displayName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public /* synthetic */ sg5(zd7 zd7) {
        this();
    }

    @DexIgnore
    public final void b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Check the  : " + this.c.size());
        if (!this.c.isEmpty()) {
            this.d = true;
            c();
            return;
        }
        this.d = false;
    }

    @DexIgnore
    public final void b(LightAndHaptics lightAndHaptics) {
        FLogger.INSTANCE.getLocal().d(g, "doPlayHandsNotification");
        NotificationType type = lightAndHaptics.getType();
        if (type != null) {
            int i2 = tg5.c[type.ordinal()];
            if (i2 == 1) {
                PortfolioApp c2 = PortfolioApp.g0.c();
                String c3 = PortfolioApp.g0.c().c();
                NotificationBaseObj.ANotificationType aNotificationType = NotificationBaseObj.ANotificationType.INCOMING_CALL;
                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                String senderName = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName, "item.senderName");
                String senderName2 = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName2, "item.senderName");
                String senderName3 = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName3, "item.senderName");
                c2.b(c3, new DianaNotificationObj(0, aNotificationType, phone_incoming_call, senderName, senderName2, -1, senderName3, w97.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), null, null, null, 1792, null));
            } else if (i2 == 2) {
                PortfolioApp c4 = PortfolioApp.g0.c();
                String c5 = PortfolioApp.g0.c().c();
                NotificationBaseObj.ANotificationType aNotificationType2 = NotificationBaseObj.ANotificationType.TEXT;
                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                String senderName4 = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName4, "item.senderName");
                String senderName5 = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName5, "item.senderName");
                String senderName6 = lightAndHaptics.getSenderName();
                ee7.a((Object) senderName6, "item.senderName");
                c4.b(c5, new DianaNotificationObj(0, aNotificationType2, messages, senderName4, senderName5, -1, senderName6, w97.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), null, null, null, 1792, null));
            } else if (i2 == 3) {
                String senderName7 = lightAndHaptics.getSenderName();
                String packageName = senderName7 == null || mh7.a(senderName7) ? lightAndHaptics.getPackageName() : lightAndHaptics.getSenderName();
                CRC32 crc32 = new CRC32();
                String packageName2 = lightAndHaptics.getPackageName();
                ee7.a((Object) packageName2, "item.packageName");
                Charset charset = sg7.a;
                if (packageName2 != null) {
                    byte[] bytes = packageName2.getBytes(charset);
                    ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    crc32.update(bytes);
                    crc32.getValue();
                    ee7.a((Object) packageName, "appName");
                    String packageName3 = lightAndHaptics.getPackageName();
                    ee7.a((Object) packageName3, "item.packageName");
                    FNotification fNotification = new FNotification(packageName, packageName3, "", NotificationBaseObj.ANotificationType.NOTIFICATION);
                    PortfolioApp c6 = PortfolioApp.g0.c();
                    String c7 = PortfolioApp.g0.c().c();
                    NotificationBaseObj.ANotificationType aNotificationType3 = NotificationBaseObj.ANotificationType.NOTIFICATION;
                    String packageName4 = lightAndHaptics.getPackageName();
                    ee7.a((Object) packageName4, "item.packageName");
                    String packageName5 = lightAndHaptics.getPackageName();
                    ee7.a((Object) packageName5, "item.packageName");
                    String packageName6 = lightAndHaptics.getPackageName();
                    ee7.a((Object) packageName6, "item.packageName");
                    c6.b(c7, new HybridNotificationObj(0, aNotificationType3, fNotification, packageName4, packageName5, -1, packageName6, w97.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore
    public final void a(NotificationInfo notificationInfo, boolean z) {
        ee7.b(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        LightAndHaptics b2 = b(notificationInfo.getSenderInfo());
        if (b2 == null && z) {
            String packageName = notificationInfo.getPackageName();
            ee7.a((Object) packageName, "info.packageName");
            if (a(packageName)) {
                String packageName2 = notificationInfo.getPackageName();
                ee7.a((Object) packageName2, "info.packageName");
                b2 = a(packageName2, notificationInfo.getBody());
            }
            if (b2 == null) {
                List<ContactGroup> b3 = ox6.a().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
                NotificationSource source = notificationInfo.getSource();
                ee7.a((Object) source, "info.source");
                b2 = a("-5678", b3, source);
            }
        }
        if (b2 != null) {
            a(b2);
        }
    }

    @DexIgnore
    public final void a(LightAndHaptics lightAndHaptics) {
        if (this.c.size() < 5) {
            this.c.add(lightAndHaptics);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "addEventToQueue : " + lightAndHaptics.getType());
            if (!this.d) {
                this.d = true;
                c();
            }
        }
    }

    @DexIgnore
    public final void c() {
        if (!this.c.isEmpty()) {
            try {
                LightAndHaptics remove = this.c.remove();
                ee7.a((Object) remove, "nextItem");
                b(remove);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Total duration: " + 500L);
                this.e.postDelayed(this.f, 500);
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "exception when startQueue " + e2);
            }
        } else {
            this.d = false;
        }
    }

    @DexIgnore
    public final LightAndHaptics a(String str, List<? extends ContactGroup> list, NotificationSource notificationSource) {
        ContactGroup contactGroup;
        Contact contactWithPhoneNumber;
        if (list == null || !(!list.isEmpty()) || (contactWithPhoneNumber = (contactGroup = (ContactGroup) list.get(0)).getContactWithPhoneNumber(str)) == null) {
            return null;
        }
        String firstName = contactWithPhoneNumber.getFirstName();
        if (!TextUtils.isEmpty(contactWithPhoneNumber.getLastName()) && !mh7.b(contactWithPhoneNumber.getLastName(), "null", true)) {
            firstName = firstName + " " + contactWithPhoneNumber.getLastName();
        }
        int i2 = tg5.b[notificationSource.ordinal()];
        if (i2 == 1) {
            return new LightAndHaptics("", NotificationType.CALL, firstName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_CALL);
        }
        if (i2 != 2) {
            return null;
        }
        return new LightAndHaptics("", NotificationType.SMS, firstName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.equals(Telephony.Sms.getDefaultSmsPackage(PortfolioApp.g0.c()), str)) {
            ch5 ch5 = this.b;
            if (ch5 == null) {
                ee7.d("mSharePref");
                throw null;
            } else if (ch5.S()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final LightAndHaptics a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "Attepting to match app " + str);
        AppFilter a2 = nx6.b.a(str, MFDeviceFamily.DEVICE_FAMILY_SAM);
        if (a2 == null) {
            return null;
        }
        AppFilterHistory appFilterHistory = new AppFilterHistory("");
        appFilterHistory.setColor(a2.getColor());
        appFilterHistory.setHaptic(a2.getHaptic());
        appFilterHistory.setTitle(a2.getName());
        appFilterHistory.setSubTitle(str2);
        appFilterHistory.setType(a2.getType());
        NotificationType notificationType = NotificationType.APP_FILTER;
        if (ee7.a((Object) str, (Object) AppType.ALL_CALLS.name())) {
            notificationType = NotificationType.CALL;
        } else if (ee7.a((Object) str, (Object) AppType.ALL_SMS.name())) {
            notificationType = NotificationType.SMS;
        } else if (ee7.a((Object) str, (Object) AppType.ALL_EMAIL.name())) {
            notificationType = NotificationType.EMAIL;
        }
        return new LightAndHaptics(str, notificationType, "", a2.getHour(), NotificationPriority.APP_FILTER);
    }

    @DexIgnore
    public final boolean a() {
        for (AppFilter appFilter : ah5.p.a().a().getAllAppFilters()) {
            ee7.a((Object) appFilter, "item");
            if (mh7.b(appFilter.getType(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.g0.c()), true)) {
                FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return true");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return false");
        return false;
    }
}
