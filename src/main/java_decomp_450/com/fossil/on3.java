package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on3 extends uf2 implements ln3 {
    @DexIgnore
    public on3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final void a(s62 s62, int i, boolean z) throws RemoteException {
        Parcel E = E();
        vf2.a(E, s62);
        E.writeInt(i);
        vf2.a(E, z);
        b(9, E);
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final void b(int i) throws RemoteException {
        Parcel E = E();
        E.writeInt(i);
        b(7, E);
    }

    @DexIgnore
    @Override // com.fossil.ln3
    public final void a(rn3 rn3, jn3 jn3) throws RemoteException {
        Parcel E = E();
        vf2.a(E, rn3);
        vf2.a(E, jn3);
        b(12, E);
    }
}
