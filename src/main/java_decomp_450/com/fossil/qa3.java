package com.fossil;

import com.fossil.n63;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa3 extends t73 {
    @DexIgnore
    public /* final */ /* synthetic */ n63.k a;

    @DexIgnore
    public qa3(n63 n63, n63.k kVar) {
        this.a = kVar;
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final void c(en2 en2) {
        this.a.onMarkerDragStart(new j93(en2));
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final void d(en2 en2) {
        this.a.onMarkerDragEnd(new j93(en2));
    }

    @DexIgnore
    @Override // com.fossil.s73
    public final void e(en2 en2) {
        this.a.onMarkerDrag(new j93(en2));
    }
}
