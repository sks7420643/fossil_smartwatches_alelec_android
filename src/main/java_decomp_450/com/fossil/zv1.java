package com.fossil;

import com.fossil.dw1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv1 extends dw1 {
    @DexIgnore
    public /* final */ by1 a;
    @DexIgnore
    public /* final */ Map<dt1, dw1.b> b;

    @DexIgnore
    public zv1(by1 by1, Map<dt1, dw1.b> map) {
        if (by1 != null) {
            this.a = by1;
            if (map != null) {
                this.b = map;
                return;
            }
            throw new NullPointerException("Null values");
        }
        throw new NullPointerException("Null clock");
    }

    @DexIgnore
    @Override // com.fossil.dw1
    public by1 a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.dw1
    public Map<dt1, dw1.b> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dw1)) {
            return false;
        }
        dw1 dw1 = (dw1) obj;
        if (!this.a.equals(dw1.a()) || !this.b.equals(dw1.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SchedulerConfig{clock=" + this.a + ", values=" + this.b + "}";
    }
}
