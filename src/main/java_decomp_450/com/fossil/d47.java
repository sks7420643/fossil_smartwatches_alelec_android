package com.fossil;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d47 extends e47 {
    @DexIgnore
    public String m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Thread o; // = null;

    @DexIgnore
    public d47(Context context, int i, int i2, Throwable th, a47 a47) {
        super(context, i, a47);
        a(i2, th);
    }

    @DexIgnore
    public d47(Context context, int i, int i2, Throwable th, Thread thread, a47 a47) {
        super(context, i, a47);
        a(i2, th);
        this.o = thread;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public f47 a() {
        return f47.c;
    }

    @DexIgnore
    public final void a(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.m = stringWriter.toString();
            this.n = i;
            printWriter.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.e47
    public boolean a(JSONObject jSONObject) {
        a67.a(jSONObject, "er", this.m);
        jSONObject.put("ea", this.n);
        int i = this.n;
        if (i != 2 && i != 3) {
            return true;
        }
        new m57(((e47) this).j).a(jSONObject, this.o);
        return true;
    }
}
