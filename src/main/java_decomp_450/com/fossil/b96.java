package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b96 implements Factory<e96> {
    @DexIgnore
    public static e96 a(z86 z86) {
        e96 b = z86.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
