package com.fossil;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import com.fossil.oe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class le extends LoaderManager {
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public /* final */ LifecycleOwner a;
    @DexIgnore
    public /* final */ c b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<D> implements zd<D> {
        @DexIgnore
        public /* final */ oe<D> a;
        @DexIgnore
        public /* final */ LoaderManager.a<D> b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(oe<D> oeVar, LoaderManager.a<D> aVar) {
            this.a = oeVar;
            this.b = aVar;
        }

        @DexIgnore
        public boolean a() {
            return this.c;
        }

        @DexIgnore
        public void b() {
            if (this.c) {
                if (le.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.a);
                }
                this.b.a(this.a);
            }
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(D d) {
            if (le.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.a + ": " + this.a.dataToString(d));
            }
            this.b.a(this.a, d);
            this.c = true;
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }

        @DexIgnore
        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends he {
        @DexIgnore
        public static /* final */ ViewModelProvider.Factory c; // = new a();
        @DexIgnore
        public SparseArrayCompat<a> a; // = new SparseArrayCompat<>();
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements ViewModelProvider.Factory {
            @DexIgnore
            @Override // androidx.lifecycle.ViewModelProvider.Factory
            public <T extends he> T create(Class<T> cls) {
                return new c();
            }
        }

        @DexIgnore
        public static c a(ViewModelStore viewModelStore) {
            return (c) new ViewModelProvider(viewModelStore, c).a(c.class);
        }

        @DexIgnore
        public boolean b() {
            return this.b;
        }

        @DexIgnore
        public void c() {
            int h = this.a.h();
            for (int i = 0; i < h; i++) {
                this.a.e(i).g();
            }
        }

        @DexIgnore
        public void d() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.he
        public void onCleared() {
            super.onCleared();
            int h = this.a.h();
            for (int i = 0; i < h; i++) {
                this.a.e(i).a(true);
            }
            this.a.d();
        }

        @DexIgnore
        public void a() {
            this.b = false;
        }

        @DexIgnore
        public void b(int i) {
            this.a.d(i);
        }

        @DexIgnore
        public void a(int i, a aVar) {
            this.a.c(i, aVar);
        }

        @DexIgnore
        public <D> a<D> a(int i) {
            return this.a.a(i);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.a.h() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.a.h(); i++) {
                    a e = this.a.e(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.a.c(i));
                    printWriter.print(": ");
                    printWriter.println(e.toString());
                    e.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    @DexIgnore
    public le(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.a = lifecycleOwner;
        this.b = c.a(viewModelStore);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final <D> oe<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar, oe<D> oeVar) {
        try {
            this.b.d();
            oe<D> a2 = aVar.a(i, bundle);
            if (a2 != null) {
                if (a2.getClass().isMemberClass()) {
                    if (!Modifier.isStatic(a2.getClass().getModifiers())) {
                        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + a2);
                    }
                }
                a aVar2 = new a(i, bundle, a2, oeVar);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.b.a(i, aVar2);
                this.b.a();
                return aVar2.a(this.a, aVar);
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.b.a();
            throw th;
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> oe<D> b(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            a<D> a2 = this.b.a(i);
            oe<D> oeVar = null;
            if (a2 != null) {
                oeVar = a2.a(false);
            }
            return a(i, bundle, aVar, oeVar);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        x8.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<D> extends MutableLiveData<D> implements oe.c<D> {
        @DexIgnore
        public /* final */ int k;
        @DexIgnore
        public /* final */ Bundle l;
        @DexIgnore
        public /* final */ oe<D> m;
        @DexIgnore
        public LifecycleOwner n;
        @DexIgnore
        public b<D> o;
        @DexIgnore
        public oe<D> p;

        @DexIgnore
        public a(int i, Bundle bundle, oe<D> oeVar, oe<D> oeVar2) {
            this.k = i;
            this.l = bundle;
            this.m = oeVar;
            this.p = oeVar2;
            oeVar.registerListener(i, this);
        }

        @DexIgnore
        public oe<D> a(LifecycleOwner lifecycleOwner, LoaderManager.a<D> aVar) {
            b<D> bVar = new b<>(this.m, aVar);
            a(lifecycleOwner, bVar);
            b<D> bVar2 = this.o;
            if (bVar2 != null) {
                b((zd) bVar2);
            }
            this.n = lifecycleOwner;
            this.o = bVar;
            return this.m;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.zd<? super D> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // androidx.lifecycle.LiveData
        public void b(zd<? super D> zdVar) {
            super.b((zd) zdVar);
            this.n = null;
            this.o = null;
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void d() {
            if (le.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void e() {
            if (le.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        @DexIgnore
        public oe<D> f() {
            return this.m;
        }

        @DexIgnore
        public void g() {
            LifecycleOwner lifecycleOwner = this.n;
            b<D> bVar = this.o;
            if (lifecycleOwner != null && bVar != null) {
                super.b((zd) bVar);
                a(lifecycleOwner, bVar);
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            x8.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }

        @DexIgnore
        @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
        public void b(D d) {
            super.b((Object) d);
            oe<D> oeVar = this.p;
            if (oeVar != null) {
                oeVar.reset();
                this.p = null;
            }
        }

        @DexIgnore
        public oe<D> a(boolean z) {
            if (le.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            b<D> bVar = this.o;
            if (bVar != null) {
                b((zd) bVar);
                if (z) {
                    bVar.b();
                }
            }
            this.m.unregisterListener(this);
            if ((bVar == null || bVar.a()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        @DexIgnore
        @Override // com.fossil.oe.c
        public void a(oe<D> oeVar, D d) {
            if (le.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                b((Object) d);
                return;
            }
            if (le.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            a((Object) d);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            oe<D> oeVar = this.m;
            oeVar.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                b<D> bVar = this.o;
                bVar.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(f().dataToString((D) a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(c());
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> oe<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            a<D> a2 = this.b.a(i);
            if (c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (a2 == null) {
                return a(i, bundle, aVar, (oe) null);
            }
            if (c) {
                Log.v("LoaderManager", "  Re-using existing loader " + a2);
            }
            return a2.a(this.a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void a(int i) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            a a2 = this.b.a(i);
            if (a2 != null) {
                a2.a(true);
                this.b.b(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void a() {
        this.b.c();
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }
}
