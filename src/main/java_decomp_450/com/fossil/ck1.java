package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ck1 extends Enum<ck1> {
    @DexIgnore
    public static /* final */ ck1 b;
    @DexIgnore
    public static /* final */ ck1 c;
    @DexIgnore
    public static /* final */ /* synthetic */ ck1[] d;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        ck1 ck1 = new ck1("CONFIGURATION_FILE", 0, (byte) 0);
        b = ck1;
        ck1 ck12 = new ck1("REMOTE_ACTIVITY", 8, (byte) 8);
        c = ck12;
        d = new ck1[]{ck1, new ck1("ELEMENT_CONFIGURATION_FILE", 1, (byte) 1), new ck1("LAUNCH_FILE", 2, (byte) 2), new ck1("LAUNCH_ELEMENT_FILE", 3, (byte) 3), new ck1("DECLARATION_FILE", 4, (byte) 4), new ck1("DECLARATION_ELEMENT_FILE", 5, (byte) 5), new ck1("CUSTOMIZATION_FILE", 6, (byte) 6), new ck1("CUSTOMIZATION_ELEMENT_FAME", 7, (byte) 7), ck12};
    }
    */

    @DexIgnore
    public ck1(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static ck1 valueOf(String str) {
        return (ck1) Enum.valueOf(ck1.class, str);
    }

    @DexIgnore
    public static ck1[] values() {
        return (ck1[]) d.clone();
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
