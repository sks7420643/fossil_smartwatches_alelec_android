package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcelable;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class je5 {
    @DexIgnore
    public static /* final */ String a; // = "je5";

    @DexIgnore
    public static Uri a(Context context) {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir != null) {
            return Uri.fromFile(new File(externalCacheDir.getPath(), "pickerImage.jpg"));
        }
        return null;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Uri a2 = a(context);
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
            Intent intent2 = new Intent(intent);
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            intent2.setComponent(new ComponentName(activityInfo.packageName, activityInfo.name));
            intent2.setPackage(resolveInfo.activityInfo.packageName);
            if (a2 != null) {
                intent2.putExtra("output", a2);
            }
            arrayList.add(intent2);
        }
        Intent intent3 = new Intent("android.intent.action.GET_CONTENT");
        intent3.setType("image/*");
        for (ResolveInfo resolveInfo2 : packageManager.queryIntentActivities(intent3, 0)) {
            Intent intent4 = new Intent(intent3);
            ActivityInfo activityInfo2 = resolveInfo2.activityInfo;
            intent4.setComponent(new ComponentName(activityInfo2.packageName, activityInfo2.name));
            intent4.setPackage(resolveInfo2.activityInfo.packageName);
            arrayList.add(intent4);
        }
        Intent intent5 = (Intent) arrayList.get(arrayList.size() - 1);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Intent intent6 = (Intent) it.next();
            if (intent6.getComponent() != null && "com.android.documentsui.DocumentsActivity".equalsIgnoreCase(intent6.getComponent().getClassName())) {
                intent5 = intent6;
                break;
            }
        }
        arrayList.remove(intent5);
        Intent createChooser = Intent.createChooser(intent5, context.getResources().getString(2131887293));
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        return createChooser;
    }

    @DexIgnore
    public static Bitmap a(View view) {
        view.setDrawingCacheEnabled(true);
        view.destroyDrawingCache();
        view.buildDrawingCache();
        return view.getDrawingCache();
    }

    @DexIgnore
    public static Intent a(Context context, PackageManager packageManager) {
        ArrayList arrayList = new ArrayList();
        Uri a2 = a(context);
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
            Intent intent2 = new Intent(intent);
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            intent2.setComponent(new ComponentName(activityInfo.packageName, activityInfo.name));
            intent2.setPackage(resolveInfo.activityInfo.packageName);
            if (a2 != null) {
                intent2.putExtra("output", a2);
            }
            arrayList.add(intent2);
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Intent intent3 = (Intent) arrayList.get(arrayList.size() - 1);
        arrayList.remove(arrayList.size() - 1);
        Intent createChooser = Intent.createChooser(intent3, "");
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        intent3.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        return createChooser;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003d, code lost:
        if (r2.equalsIgnoreCase("inline-data") == false) goto L_0x0040;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.net.Uri a(android.content.Intent r7, android.content.Context r8) {
        /*
            r0 = 0
            r1 = 1
            if (r7 != 0) goto L_0x0006
        L_0x0004:
            r0 = 1
            goto L_0x0040
        L_0x0006:
            android.net.Uri r2 = r7.getData()
            if (r2 == 0) goto L_0x000d
            goto L_0x0040
        L_0x000d:
            java.lang.String r2 = r7.getAction()
            if (r2 == 0) goto L_0x0004
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.fossil.je5.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Action is "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            java.lang.String r3 = "android.media.action.IMAGE_CAPTURE"
            boolean r3 = r2.equals(r3)
            if (r3 != 0) goto L_0x0004
            java.lang.String r3 = "inline-data"
            boolean r2 = r2.equalsIgnoreCase(r3)
            if (r2 == 0) goto L_0x0040
            goto L_0x0004
        L_0x0040:
            if (r0 == 0) goto L_0x0047
            android.net.Uri r7 = a(r8)
            goto L_0x004b
        L_0x0047:
            android.net.Uri r7 = r7.getData()
        L_0x004b:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.je5.a(android.content.Intent, android.content.Context):android.net.Uri");
    }

    @DexIgnore
    public static Bitmap a(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(str, options);
    }

    @DexIgnore
    public static int a(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        int i5 = 1;
        if (i3 > i2 || i4 > i) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i6 / i5 >= i2 && i7 / i5 >= i) {
                i5 *= 2;
            }
        }
        return i5;
    }

    @DexIgnore
    public static Bitmap a(InputStream inputStream, int i, int i2) {
        FLogger.INSTANCE.getLocal().d(a, "fastReadingBitmapFromAssets");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    options.inSampleSize = a(options, i, i2);
                    options.inJustDecodeBounds = false;
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    try {
                        byteArrayOutputStream.close();
                        return decodeByteArray;
                    } catch (Exception e) {
                        e.printStackTrace();
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = a;
                        local.e(str, "fastReadingBitmapFromAssets - os close with e=" + e);
                        return decodeByteArray;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local2.e(str2, "fastReadingBitmapFromAssets - e=" + e2);
            try {
                byteArrayOutputStream.close();
            } catch (Exception e3) {
                e3.printStackTrace();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local3.e(str3, "fastReadingBitmapFromAssets - os close with e=" + e3);
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = a;
                local4.e(str4, "fastReadingBitmapFromAssets - os close with e=" + e4);
            }
            throw th;
        }
    }
}
