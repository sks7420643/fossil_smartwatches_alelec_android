package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bm7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(bm7.class, Object.class, "_next");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(bm7.class, Object.class, "_prev");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater c; // = AtomicReferenceFieldUpdater.newUpdater(bm7.class, Object.class, "_removedRef");
    @DexIgnore
    public volatile Object _next; // = this;
    @DexIgnore
    public volatile Object _prev; // = this;
    @DexIgnore
    public volatile Object _removedRef; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends ul7<bm7> {
        @DexIgnore
        public bm7 b;
        @DexIgnore
        public /* final */ bm7 c;

        @DexIgnore
        public a(bm7 bm7) {
            this.c = bm7;
        }

        @DexIgnore
        public void a(bm7 bm7, Object obj) {
            boolean z = obj == null;
            bm7 bm72 = z ? this.c : this.b;
            if (bm72 != null && bm7.a.compareAndSet(bm7, this, bm72) && z) {
                bm7 bm73 = this.c;
                bm7 bm74 = this.b;
                if (bm74 != null) {
                    bm73.c(bm74);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final bm7 b(bm7 bm7) {
        while (bm7.f()) {
            bm7 = (bm7) bm7._prev;
        }
        return bm7;
    }

    @DexIgnore
    public final bm7 c() {
        return am7.a(b());
    }

    @DexIgnore
    public final bm7 d() {
        bm7 a2 = a((hm7) null);
        return a2 != null ? a2 : b((bm7) this._prev);
    }

    @DexIgnore
    public final void e() {
        Object b2 = b();
        if (b2 != null) {
            ((im7) b2).a.a((hm7) null);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
    }

    @DexIgnore
    public boolean f() {
        return b() instanceof im7;
    }

    @DexIgnore
    public boolean g() {
        return i() == null;
    }

    @DexIgnore
    public final bm7 h() {
        while (true) {
            Object b2 = b();
            if (b2 != null) {
                bm7 bm7 = (bm7) b2;
                if (bm7 == this) {
                    return null;
                }
                if (bm7.g()) {
                    return bm7;
                }
                bm7.e();
            } else {
                throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final bm7 i() {
        Object b2;
        bm7 bm7;
        do {
            b2 = b();
            if (b2 instanceof im7) {
                return ((im7) b2).a;
            }
            if (b2 == this) {
                return (bm7) b2;
            }
            if (b2 != null) {
                bm7 = (bm7) b2;
            } else {
                throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!a.compareAndSet(this, b2, bm7.j()));
        bm7.a((hm7) null);
        return null;
    }

    @DexIgnore
    public final im7 j() {
        im7 im7 = (im7) this._removedRef;
        if (im7 != null) {
            return im7;
        }
        im7 im72 = new im7(this);
        c.lazySet(this, im72);
        return im72;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(System.identityHashCode(this));
    }

    @DexIgnore
    public final boolean a(bm7 bm7) {
        b.lazySet(bm7, this);
        a.lazySet(bm7, this);
        while (b() == this) {
            if (a.compareAndSet(this, this, bm7)) {
                bm7.c(this);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void c(bm7 bm7) {
        bm7 bm72;
        do {
            bm72 = (bm7) bm7._prev;
            if (b() != bm7) {
                return;
            }
        } while (!b.compareAndSet(bm7, bm72, this));
        if (f()) {
            bm7.a((hm7) null);
        }
    }

    @DexIgnore
    public final Object b() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof hm7)) {
                return obj;
            }
            ((hm7) obj).a(this);
        }
    }

    @DexIgnore
    public final int a(bm7 bm7, bm7 bm72, a aVar) {
        b.lazySet(bm7, this);
        a.lazySet(bm7, bm72);
        aVar.b = bm72;
        if (!a.compareAndSet(this, bm72, aVar)) {
            return 0;
        }
        return aVar.a(this) == null ? 1 : 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (com.fossil.bm7.a.compareAndSet(r3, r2, ((com.fossil.im7) r4).a) != false) goto L_0x004b;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.bm7 a(com.fossil.hm7 r7) {
        /*
            r6 = this;
        L_0x0000:
            java.lang.Object r0 = r6._prev
            com.fossil.bm7 r0 = (com.fossil.bm7) r0
            r1 = 0
            r2 = r0
        L_0x0006:
            r3 = r1
        L_0x0007:
            java.lang.Object r4 = r2._next
            if (r4 != r6) goto L_0x0018
            if (r0 != r2) goto L_0x000e
            return r2
        L_0x000e:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = com.fossil.bm7.b
            boolean r0 = r1.compareAndSet(r6, r0, r2)
            if (r0 != 0) goto L_0x0017
            goto L_0x0000
        L_0x0017:
            return r2
        L_0x0018:
            boolean r5 = r6.f()
            if (r5 == 0) goto L_0x001f
            return r1
        L_0x001f:
            if (r4 != r7) goto L_0x0022
            return r2
        L_0x0022:
            boolean r5 = r4 instanceof com.fossil.hm7
            if (r5 == 0) goto L_0x0038
            if (r7 == 0) goto L_0x0032
            r0 = r4
            com.fossil.hm7 r0 = (com.fossil.hm7) r0
            boolean r0 = r7.a(r0)
            if (r0 == 0) goto L_0x0032
            return r1
        L_0x0032:
            com.fossil.hm7 r4 = (com.fossil.hm7) r4
            r4.a(r2)
            goto L_0x0000
        L_0x0038:
            boolean r5 = r4 instanceof com.fossil.im7
            if (r5 == 0) goto L_0x0052
            if (r3 == 0) goto L_0x004d
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r5 = com.fossil.bm7.a
            com.fossil.im7 r4 = (com.fossil.im7) r4
            com.fossil.bm7 r4 = r4.a
            boolean r2 = r5.compareAndSet(r3, r2, r4)
            if (r2 != 0) goto L_0x004b
            goto L_0x0000
        L_0x004b:
            r2 = r3
            goto L_0x0006
        L_0x004d:
            java.lang.Object r2 = r2._prev
            com.fossil.bm7 r2 = (com.fossil.bm7) r2
            goto L_0x0007
        L_0x0052:
            if (r4 == 0) goto L_0x0059
            com.fossil.bm7 r4 = (com.fossil.bm7) r4
            r3 = r2
            r2 = r4
            goto L_0x0007
        L_0x0059:
            com.fossil.x87 r7 = new com.fossil.x87
        */
        //  java.lang.String r0 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
        /*
            r7.<init>(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bm7.a(com.fossil.hm7):com.fossil.bm7");
    }
}
