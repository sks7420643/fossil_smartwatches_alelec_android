package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c56 extends io5<b56> {
    @DexIgnore
    void M(String str);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b(int i);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void k();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void n(List<ez5> list);

    @DexIgnore
    void s(boolean z);

    @DexIgnore
    void w();
}
