package com.fossil;

import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw7 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(PluginRegistry.Registrar registrar) {
            ee7.b(registrar, "registrar");
            new MethodChannel(registrar.messenger(), "top.kikt/photo_manager").setMethodCallHandler(new vw7(registrar));
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new ix7(null, 1, null);
    }
    */

    @DexIgnore
    public static final void a(PluginRegistry.Registrar registrar) {
        a.a(registrar);
    }
}
