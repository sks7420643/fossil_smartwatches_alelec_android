package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j84 implements m84 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ m84[] b;
    @DexIgnore
    public /* final */ k84 c;

    @DexIgnore
    public j84(int i, m84... m84Arr) {
        this.a = i;
        this.b = m84Arr;
        this.c = new k84(i);
    }

    @DexIgnore
    @Override // com.fossil.m84
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        m84[] m84Arr = this.b;
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (m84 m84 : m84Arr) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = m84.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.a ? this.c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
