package com.fossil;

import com.fossil.lf;
import com.fossil.mf;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xf<K, A, B> extends mf<K, B> {
    @DexIgnore
    public /* final */ mf<K, A> a;
    @DexIgnore
    public /* final */ t3<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf.c<A> {
        @DexIgnore
        public /* final */ /* synthetic */ mf.c a;

        @DexIgnore
        public a(mf.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.mf.a
        public void a(List<A> list) {
            this.a.a(xf.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends mf.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ mf.a a;

        @DexIgnore
        public b(mf.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.mf.a
        public void a(List<A> list) {
            this.a.a(xf.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends mf.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ mf.a a;

        @DexIgnore
        public c(mf.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.mf.a
        public void a(List<A> list) {
            this.a.a(xf.this.a(list));
        }
    }

    @DexIgnore
    public xf(mf<K, A> mfVar, t3<List<A>, List<B>> t3Var) {
        this.a = mfVar;
        this.b = t3Var;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = lf.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void addInvalidatedCallback(lf.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.mf
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadAfter(mf.f<K> fVar, mf.a<B> aVar) {
        this.a.loadAfter(fVar, new b(aVar));
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadBefore(mf.f<K> fVar, mf.a<B> aVar) {
        this.a.loadBefore(fVar, new c(aVar));
    }

    @DexIgnore
    @Override // com.fossil.mf
    public void loadInitial(mf.e<K> eVar, mf.c<B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void removeInvalidatedCallback(lf.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
