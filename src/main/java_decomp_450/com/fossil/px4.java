package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class px4 extends ox4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362109, 1);
        E.put(2131361851, 2);
        E.put(2131362927, 3);
        E.put(2131362517, 4);
        E.put(2131362460, 5);
        E.put(2131362378, 6);
        E.put(2131362070, 7);
        E.put(2131361854, 8);
        E.put(2131362095, 9);
        E.put(2131361855, 10);
        E.put(2131361852, 11);
    }
    */

    @DexIgnore
    public px4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public px4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[2], (AppCompatImageView) objArr[11], (FlexibleImageButton) objArr[8], (FlexibleImageButton) objArr[10], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[6], (FlexibleButton) objArr[5], (FlexibleTextView) objArr[4], (DashBar) objArr[3], (ConstraintLayout) objArr[0]);
        this.C = -1;
        ((ox4) this).B.setTag(null);
        a(view);
        f();
    }
}
