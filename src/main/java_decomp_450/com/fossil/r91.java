package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r91 extends eo0 {
    @DexIgnore
    public m01 j; // = m01.BOND_NONE;

    @DexIgnore
    public r91(cx0 cx0) {
        super(aq0.k, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.h();
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        m01 m01;
        return (s91 instanceof fx0) && ((m01 = ((fx0) s91).b) == m01.BONDED || m01 == m01.BOND_NONE);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        c(s91);
        x71 x71 = s91.a;
        if (x71.a != z51.SUCCESS) {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        } else if (this.j == m01.BOND_NONE) {
            lk0 = lk0.a(((eo0) this).d, null, oi0.a, null, 5);
        } else {
            lk0 = lk0.a(((eo0) this).d, null, oi0.d, null, 5);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.j = ((fx0) s91).b;
    }
}
