package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class do7 extends RequestBody {
    @DexIgnore
    public static /* final */ ho7 c; // = ho7.a("application/x-www-form-urlencoded");
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public a() {
            this(null);
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(go7.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(go7.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(go7.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(go7.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a(Charset charset) {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public do7 a() {
            return new do7(this.a, this.b);
        }
    }

    @DexIgnore
    public do7(List<String> list, List<String> list2) {
        this.a = ro7.a(list);
        this.b = ro7.a(list2);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() {
        return a((zq7) null, true);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public ho7 b() {
        return c;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void a(zq7 zq7) throws IOException {
        a(zq7, false);
    }

    @DexIgnore
    public final long a(zq7 zq7, boolean z) {
        yq7 yq7;
        if (z) {
            yq7 = new yq7();
        } else {
            yq7 = zq7.buffer();
        }
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                yq7.writeByte(38);
            }
            yq7.a(this.a.get(i));
            yq7.writeByte(61);
            yq7.a(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long x = yq7.x();
        yq7.k();
        return x;
    }
}
