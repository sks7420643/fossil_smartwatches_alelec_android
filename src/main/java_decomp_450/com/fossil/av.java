package com.fossil;

import com.fossil.mu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class av<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ mu.a b;
    @DexIgnore
    public /* final */ fv c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onErrorResponse(fv fvVar);
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public av(T t, mu.a aVar) {
        this.d = false;
        this.a = t;
        this.b = aVar;
        this.c = null;
    }

    @DexIgnore
    public static <T> av<T> a(T t, mu.a aVar) {
        return new av<>(t, aVar);
    }

    @DexIgnore
    public static <T> av<T> a(fv fvVar) {
        return new av<>(fvVar);
    }

    @DexIgnore
    public boolean a() {
        return this.c == null;
    }

    @DexIgnore
    public av(fv fvVar) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = fvVar;
    }
}
