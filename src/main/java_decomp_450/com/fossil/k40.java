package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.k40;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k40<T extends k40<T>> implements Cloneable {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D; // = true;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public int a;
    @DexIgnore
    public float b; // = 1.0f;
    @DexIgnore
    public iy c; // = iy.d;
    @DexIgnore
    public ew d; // = ew.NORMAL;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public yw q; // = j50.a();
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s; // = true;
    @DexIgnore
    public Drawable t;
    @DexIgnore
    public int u;
    @DexIgnore
    public ax v; // = new ax();
    @DexIgnore
    public Map<Class<?>, ex<?>> w; // = new m50();
    @DexIgnore
    public Class<?> x; // = Object.class;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public Resources.Theme z;

    @DexIgnore
    public static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @DexIgnore
    public final boolean A() {
        return this.B;
    }

    @DexIgnore
    public final boolean B() {
        return this.i;
    }

    @DexIgnore
    public final boolean C() {
        return a(8);
    }

    @DexIgnore
    public boolean D() {
        return this.D;
    }

    @DexIgnore
    public final boolean E() {
        return this.s;
    }

    @DexIgnore
    public final boolean F() {
        return this.r;
    }

    @DexIgnore
    public final boolean G() {
        return a(2048);
    }

    @DexIgnore
    public final boolean H() {
        return v50.b(this.p, this.j);
    }

    @DexIgnore
    public T I() {
        this.y = true;
        M();
        return this;
    }

    @DexIgnore
    public T J() {
        return b(r10.c, new o10());
    }

    @DexIgnore
    public T K() {
        return a(r10.b, new p10());
    }

    @DexIgnore
    public T L() {
        return a(r10.a, new w10());
    }

    @DexIgnore
    public final T M() {
        return this;
    }

    @DexIgnore
    public final T N() {
        if (!this.y) {
            M();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    public T a(float f2) {
        if (this.A) {
            return (T) clone().a(f2);
        }
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.b = f2;
        this.a |= 2;
        N();
        return this;
    }

    @DexIgnore
    public T b(boolean z2) {
        if (this.A) {
            return (T) clone().b(z2);
        }
        this.E = z2;
        this.a |= 1048576;
        N();
        return this;
    }

    @DexIgnore
    public T c(int i2) {
        return a((zw<Y>) x00.b, Integer.valueOf(i2));
    }

    @DexIgnore
    public final T d(r10 r10, ex<Bitmap> exVar) {
        if (this.A) {
            return (T) clone().d(r10, exVar);
        }
        a(r10);
        return a(exVar);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof k40)) {
            return false;
        }
        k40 k40 = (k40) obj;
        if (Float.compare(k40.b, this.b) == 0 && this.f == k40.f && v50.b(this.e, k40.e) && this.h == k40.h && v50.b(this.g, k40.g) && this.u == k40.u && v50.b(this.t, k40.t) && this.i == k40.i && this.j == k40.j && this.p == k40.p && this.r == k40.r && this.s == k40.s && this.B == k40.B && this.C == k40.C && this.c.equals(k40.c) && this.d == k40.d && this.v.equals(k40.v) && this.w.equals(k40.w) && this.x.equals(k40.x) && v50.b(this.q, k40.q) && v50.b(this.z, k40.z)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public T f() {
        return c(r10.b, new p10());
    }

    @DexIgnore
    public T h() {
        return a((zw) ((zw<Y>) z20.b), (Object) true);
    }

    @DexIgnore
    public int hashCode() {
        return v50.a(this.z, v50.a(this.q, v50.a(this.x, v50.a(this.w, v50.a(this.v, v50.a(this.d, v50.a(this.c, v50.a(this.C, v50.a(this.B, v50.a(this.s, v50.a(this.r, v50.a(this.p, v50.a(this.j, v50.a(this.i, v50.a(this.t, v50.a(this.u, v50.a(this.g, v50.a(this.h, v50.a(this.e, v50.a(this.f, v50.a(this.b)))))))))))))))))))));
    }

    @DexIgnore
    public final iy i() {
        return this.c;
    }

    @DexIgnore
    public final int j() {
        return this.f;
    }

    @DexIgnore
    public final Drawable k() {
        return this.e;
    }

    @DexIgnore
    public final Drawable l() {
        return this.t;
    }

    @DexIgnore
    public final int m() {
        return this.u;
    }

    @DexIgnore
    public final boolean n() {
        return this.C;
    }

    @DexIgnore
    public final ax o() {
        return this.v;
    }

    @DexIgnore
    public final int p() {
        return this.j;
    }

    @DexIgnore
    public final int q() {
        return this.p;
    }

    @DexIgnore
    public final Drawable r() {
        return this.g;
    }

    @DexIgnore
    public final int s() {
        return this.h;
    }

    @DexIgnore
    public final ew t() {
        return this.d;
    }

    @DexIgnore
    public final Class<?> u() {
        return this.x;
    }

    @DexIgnore
    public final yw v() {
        return this.q;
    }

    @DexIgnore
    public final float w() {
        return this.b;
    }

    @DexIgnore
    public final Resources.Theme x() {
        return this.z;
    }

    @DexIgnore
    public final Map<Class<?>, ex<?>> y() {
        return this.w;
    }

    @DexIgnore
    public final boolean z() {
        return this.E;
    }

    @DexIgnore
    public final T c(r10 r10, ex<Bitmap> exVar) {
        return a(r10, exVar, true);
    }

    @DexIgnore
    @Override // java.lang.Object
    public T clone() {
        try {
            T t2 = (T) ((k40) super.clone());
            ax axVar = new ax();
            ((k40) t2).v = axVar;
            axVar.a(this.v);
            m50 m50 = new m50();
            ((k40) t2).w = m50;
            m50.putAll(this.w);
            ((k40) t2).y = false;
            ((k40) t2).A = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public T d() {
        if (!this.y || this.A) {
            this.A = true;
            return I();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    public T b(int i2) {
        if (this.A) {
            return (T) clone().b(i2);
        }
        this.h = i2;
        int i3 = this.a | 128;
        this.a = i3;
        this.g = null;
        this.a = i3 & -65;
        N();
        return this;
    }

    @DexIgnore
    public T a(iy iyVar) {
        if (this.A) {
            return (T) clone().a(iyVar);
        }
        u50.a(iyVar);
        this.c = iyVar;
        this.a |= 4;
        N();
        return this;
    }

    @DexIgnore
    public T a(ew ewVar) {
        if (this.A) {
            return (T) clone().a(ewVar);
        }
        u50.a(ewVar);
        this.d = ewVar;
        this.a |= 8;
        N();
        return this;
    }

    @DexIgnore
    public final T b(r10 r10, ex<Bitmap> exVar) {
        if (this.A) {
            return (T) clone().b(r10, exVar);
        }
        a(r10);
        return a(exVar, false);
    }

    @DexIgnore
    public T a(boolean z2) {
        if (this.A) {
            return (T) clone().a(true);
        }
        this.i = !z2;
        this.a |= 256;
        N();
        return this;
    }

    @DexIgnore
    public T a(int i2, int i3) {
        if (this.A) {
            return (T) clone().a(i2, i3);
        }
        this.p = i2;
        this.j = i3;
        this.a |= 512;
        N();
        return this;
    }

    @DexIgnore
    public T a(yw ywVar) {
        if (this.A) {
            return (T) clone().a(ywVar);
        }
        u50.a(ywVar);
        this.q = ywVar;
        this.a |= 1024;
        N();
        return this;
    }

    @DexIgnore
    public <Y> T a(zw<Y> zwVar, Y y2) {
        if (this.A) {
            return (T) clone().a(zwVar, y2);
        }
        u50.a(zwVar);
        u50.a((Object) y2);
        this.v.a(zwVar, y2);
        N();
        return this;
    }

    @DexIgnore
    public T a(Class<?> cls) {
        if (this.A) {
            return (T) clone().a(cls);
        }
        u50.a(cls);
        this.x = cls;
        this.a |= 4096;
        N();
        return this;
    }

    @DexIgnore
    public T a(r10 r10) {
        u50.a(r10);
        return a((zw<Y>) r10.f, r10);
    }

    @DexIgnore
    public final T a(r10 r10, ex<Bitmap> exVar) {
        return a(r10, exVar, false);
    }

    @DexIgnore
    public final T a(r10 r10, ex<Bitmap> exVar, boolean z2) {
        T t2;
        if (z2) {
            t2 = d(r10, exVar);
        } else {
            t2 = b(r10, exVar);
        }
        ((k40) t2).D = true;
        return t2;
    }

    @DexIgnore
    public T a(ex<Bitmap> exVar) {
        return a(exVar, true);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ex<android.graphics.Bitmap> */
    /* JADX WARN: Multi-variable type inference failed */
    public T a(ex<Bitmap> exVar, boolean z2) {
        if (this.A) {
            return (T) clone().a(exVar, z2);
        }
        u10 u10 = new u10(exVar, z2);
        a(Bitmap.class, exVar, z2);
        a(Drawable.class, u10, z2);
        u10.a();
        a(BitmapDrawable.class, u10, z2);
        a(t20.class, new w20(exVar), z2);
        N();
        return this;
    }

    @DexIgnore
    public <Y> T a(Class<Y> cls, ex<Y> exVar, boolean z2) {
        if (this.A) {
            return (T) clone().a(cls, exVar, z2);
        }
        u50.a(cls);
        u50.a(exVar);
        this.w.put(cls, exVar);
        int i2 = this.a | 2048;
        this.a = i2;
        this.s = true;
        int i3 = i2 | 65536;
        this.a = i3;
        this.D = false;
        if (z2) {
            this.a = i3 | 131072;
            this.r = true;
        }
        N();
        return this;
    }

    @DexIgnore
    public T a(k40<?> k40) {
        if (this.A) {
            return (T) clone().a(k40);
        }
        if (b(k40.a, 2)) {
            this.b = k40.b;
        }
        if (b(k40.a, 262144)) {
            this.B = k40.B;
        }
        if (b(k40.a, 1048576)) {
            this.E = k40.E;
        }
        if (b(k40.a, 4)) {
            this.c = k40.c;
        }
        if (b(k40.a, 8)) {
            this.d = k40.d;
        }
        if (b(k40.a, 16)) {
            this.e = k40.e;
            this.f = 0;
            this.a &= -33;
        }
        if (b(k40.a, 32)) {
            this.f = k40.f;
            this.e = null;
            this.a &= -17;
        }
        if (b(k40.a, 64)) {
            this.g = k40.g;
            this.h = 0;
            this.a &= -129;
        }
        if (b(k40.a, 128)) {
            this.h = k40.h;
            this.g = null;
            this.a &= -65;
        }
        if (b(k40.a, 256)) {
            this.i = k40.i;
        }
        if (b(k40.a, 512)) {
            this.p = k40.p;
            this.j = k40.j;
        }
        if (b(k40.a, 1024)) {
            this.q = k40.q;
        }
        if (b(k40.a, 4096)) {
            this.x = k40.x;
        }
        if (b(k40.a, 8192)) {
            this.t = k40.t;
            this.u = 0;
            this.a &= -16385;
        }
        if (b(k40.a, 16384)) {
            this.u = k40.u;
            this.t = null;
            this.a &= -8193;
        }
        if (b(k40.a, 32768)) {
            this.z = k40.z;
        }
        if (b(k40.a, 65536)) {
            this.s = k40.s;
        }
        if (b(k40.a, 131072)) {
            this.r = k40.r;
        }
        if (b(k40.a, 2048)) {
            this.w.putAll(k40.w);
            this.D = k40.D;
        }
        if (b(k40.a, 524288)) {
            this.C = k40.C;
        }
        if (!this.s) {
            this.w.clear();
            int i2 = this.a & -2049;
            this.a = i2;
            this.r = false;
            this.a = i2 & -131073;
            this.D = true;
        }
        this.a |= k40.a;
        this.v.a(k40.v);
        N();
        return this;
    }

    @DexIgnore
    public final boolean a(int i2) {
        return b(this.a, i2);
    }
}
