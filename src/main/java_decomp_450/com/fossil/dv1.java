package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dv1 {

    @DexIgnore
    public enum a {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    @DexIgnore
    public static dv1 a(long j) {
        return new yu1(a.OK, j);
    }

    @DexIgnore
    public static dv1 c() {
        return new yu1(a.FATAL_ERROR, -1);
    }

    @DexIgnore
    public static dv1 d() {
        return new yu1(a.TRANSIENT_ERROR, -1);
    }

    @DexIgnore
    public abstract long a();

    @DexIgnore
    public abstract a b();
}
