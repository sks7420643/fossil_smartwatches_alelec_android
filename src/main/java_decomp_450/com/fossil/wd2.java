package com.fossil;

import android.content.Context;
import com.fossil.v02;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd2 implements v02.d.b {
    @DexIgnore
    public /* final */ GoogleSignInAccount a;

    @DexIgnore
    public wd2(Context context, GoogleSignInAccount googleSignInAccount) {
        if ("<<default account>>".equals(googleSignInAccount.g())) {
            if (v92.h() && context.getPackageManager().hasSystemFeature("cn.google")) {
                this.a = null;
                return;
            }
        }
        this.a = googleSignInAccount;
    }

    @DexIgnore
    @Override // com.fossil.v02.d.b
    public final GoogleSignInAccount b() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof wd2) && y62.a(((wd2) obj).a, this.a);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        GoogleSignInAccount googleSignInAccount = this.a;
        if (googleSignInAccount != null) {
            return googleSignInAccount.hashCode();
        }
        return 0;
    }
}
