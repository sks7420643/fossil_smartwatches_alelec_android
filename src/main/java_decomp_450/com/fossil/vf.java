package com.fossil;

import com.fossil.qf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vf<T> extends qf<T> {
    @DexIgnore
    public /* final */ boolean t;
    @DexIgnore
    public /* final */ Object u;
    @DexIgnore
    public /* final */ lf<?, T> v;

    @DexIgnore
    public vf(qf<T> qfVar) {
        super(qfVar.e.n(), qfVar.a, qfVar.b, null, qfVar.d);
        this.v = qfVar.d();
        this.t = qfVar.g();
        ((qf) this).f = qfVar.f;
        this.u = qfVar.e();
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void a(qf<T> qfVar, qf.e eVar) {
    }

    @DexIgnore
    @Override // com.fossil.qf
    public lf<?, T> d() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.qf
    public void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.qf
    public Object e() {
        return this.u;
    }

    @DexIgnore
    @Override // com.fossil.qf
    public boolean g() {
        return this.t;
    }

    @DexIgnore
    @Override // com.fossil.qf
    public boolean h() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.qf
    public boolean i() {
        return true;
    }
}
