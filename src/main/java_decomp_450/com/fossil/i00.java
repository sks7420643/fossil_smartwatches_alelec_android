package com.fossil;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i00 implements g00 {
    @DexIgnore
    public /* final */ Map<String, List<h00>> b;
    @DexIgnore
    public volatile Map<String, String> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ String b; // = b();
        @DexIgnore
        public static /* final */ Map<String, List<h00>> c;
        @DexIgnore
        public Map<String, List<h00>> a; // = c;

        /*
        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(b)) {
                hashMap.put("User-Agent", Collections.singletonList(new b(b)));
            }
            c = Collections.unmodifiableMap(hashMap);
        }
        */

        @DexIgnore
        public static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == '\t') && charAt < '\u007f') {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        @DexIgnore
        public i00 a() {
            return new i00(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements h00 {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            this.a = str;
        }

        @DexIgnore
        @Override // com.fossil.h00
        public String a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.a.equals(((b) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "StringHeaderFactory{value='" + this.a + '\'' + '}';
        }
    }

    @DexIgnore
    public i00(Map<String, List<h00>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    @Override // com.fossil.g00
    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.c;
    }

    @DexIgnore
    public final Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, List<h00>> entry : this.b.entrySet()) {
            String a2 = a(entry.getValue());
            if (!TextUtils.isEmpty(a2)) {
                hashMap.put(entry.getKey(), a2);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof i00) {
            return this.b.equals(((i00) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "LazyHeaders{headers=" + this.b + '}';
    }

    @DexIgnore
    public final String a(List<h00> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a2 = list.get(i).a();
            if (!TextUtils.isEmpty(a2)) {
                sb.append(a2);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
}
