package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l55 extends k55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i X; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Y;
    @DexIgnore
    public long W;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Y = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        Y.put(2131362927, 2);
        Y.put(2131363342, 3);
        Y.put(2131363099, 4);
        Y.put(2131362608, 5);
        Y.put(2131362223, 6);
        Y.put(2131362649, 7);
        Y.put(2131362609, 8);
        Y.put(2131362225, 9);
        Y.put(2131362650, 10);
        Y.put(2131362610, 11);
        Y.put(2131362227, 12);
        Y.put(2131362651, 13);
        Y.put(2131362605, 14);
        Y.put(2131362219, 15);
        Y.put(2131362648, 16);
        Y.put(2131362410, 17);
        Y.put(2131362259, 18);
        Y.put(2131362254, 19);
        Y.put(2131362261, 20);
        Y.put(2131361999, 21);
        Y.put(2131362471, 22);
        Y.put(2131362002, 23);
        Y.put(2131362521, 24);
        Y.put(2131362001, 25);
        Y.put(2131362513, 26);
        Y.put(2131361993, 27);
        Y.put(2131362409, 28);
        Y.put(2131361992, 29);
        Y.put(2131362408, 30);
        Y.put(2131362251, 31);
    }
    */

    @DexIgnore
    public l55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 32, X, Y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.W = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.W != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.W = 1;
        }
        g();
    }

    @DexIgnore
    public l55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleCheckBox) objArr[29], (FlexibleCheckBox) objArr[27], (FlexibleCheckBox) objArr[21], (FlexibleCheckBox) objArr[25], (FlexibleCheckBox) objArr[23], (FlexibleTextInputEditText) objArr[15], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[9], (FlexibleTextInputEditText) objArr[12], (FlexibleButton) objArr[31], (FlexibleButton) objArr[19], (FlexibleButton) objArr[18], (FlexibleButton) objArr[20], (FlexibleTextView) objArr[30], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[24], (FlexibleTextInputLayout) objArr[14], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[8], (FlexibleTextInputLayout) objArr[11], (RTLImageView) objArr[16], (RTLImageView) objArr[7], (RTLImageView) objArr[10], (RTLImageView) objArr[13], (ImageView) objArr[1], (DashBar) objArr[2], (ConstraintLayout) objArr[0], (ScrollView) objArr[4], (FlexibleTextView) objArr[3]);
        this.W = -1;
        ((k55) this).T.setTag(null);
        a(view);
        f();
    }
}
