package com.fossil;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz1 implements Parcelable.Creator<GoogleSignInAccount> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInAccount createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        String str6 = null;
        ArrayList arrayList = null;
        String str7 = null;
        String str8 = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    str = j72.e(parcel, a);
                    break;
                case 3:
                    str2 = j72.e(parcel, a);
                    break;
                case 4:
                    str3 = j72.e(parcel, a);
                    break;
                case 5:
                    str4 = j72.e(parcel, a);
                    break;
                case 6:
                    uri = (Uri) j72.a(parcel, a, Uri.CREATOR);
                    break;
                case 7:
                    str5 = j72.e(parcel, a);
                    break;
                case 8:
                    j = j72.s(parcel, a);
                    break;
                case 9:
                    str6 = j72.e(parcel, a);
                    break;
                case 10:
                    arrayList = j72.c(parcel, a, Scope.CREATOR);
                    break;
                case 11:
                    str7 = j72.e(parcel, a);
                    break;
                case 12:
                    str8 = j72.e(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new GoogleSignInAccount(i, str, str2, str3, str4, uri, str5, j, str6, arrayList, str7, str8);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleSignInAccount[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
