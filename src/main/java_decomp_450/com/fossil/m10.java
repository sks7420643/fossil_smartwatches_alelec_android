package com.fossil;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m10 implements cx<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ s10 a;

    @DexIgnore
    public m10(s10 s10) {
        this.a = s10;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, ax axVar) {
        return this.a.a(byteBuffer);
    }

    @DexIgnore
    public uy<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, ax axVar) throws IOException {
        return this.a.a(l50.c(byteBuffer), i, i2, axVar);
    }
}
