package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.WindowInsets;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class la {
    @DexIgnore
    public static /* final */ la b; // = new a().a().a().b().c();
    @DexIgnore
    public /* final */ i a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ d a;

        @DexIgnore
        public a() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.a = new c();
            } else if (i >= 20) {
                this.a = new b();
            } else {
                this.a = new d();
            }
        }

        @DexIgnore
        public a a(f7 f7Var) {
            this.a.a(f7Var);
            return this;
        }

        @DexIgnore
        public a b(f7 f7Var) {
            this.a.b(f7Var);
            return this;
        }

        @DexIgnore
        public la a() {
            return this.a.a();
        }

        @DexIgnore
        public a(la laVar) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.a = new c(laVar);
            } else if (i >= 20) {
                this.a = new b(laVar);
            } else {
                this.a = new d(laVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends d {
        @DexIgnore
        public /* final */ WindowInsets.Builder b;

        @DexIgnore
        public c() {
            this.b = new WindowInsets.Builder();
        }

        @DexIgnore
        @Override // com.fossil.la.d
        public void a(f7 f7Var) {
            this.b.setStableInsets(f7Var.a());
        }

        @DexIgnore
        @Override // com.fossil.la.d
        public void b(f7 f7Var) {
            this.b.setSystemWindowInsets(f7Var.a());
        }

        @DexIgnore
        @Override // com.fossil.la.d
        public la a() {
            return la.a(this.b.build());
        }

        @DexIgnore
        public c(la laVar) {
            WindowInsets.Builder builder;
            WindowInsets k = laVar.k();
            if (k != null) {
                builder = new WindowInsets.Builder(k);
            } else {
                builder = new WindowInsets.Builder();
            }
            this.b = builder;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ la a;

        @DexIgnore
        public d() {
            this(new la((la) null));
        }

        @DexIgnore
        public la a() {
            return this.a;
        }

        @DexIgnore
        public void a(f7 f7Var) {
        }

        @DexIgnore
        public void b(f7 f7Var) {
        }

        @DexIgnore
        public d(la laVar) {
            this.a = laVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends f {
        @DexIgnore
        public g(la laVar, WindowInsets windowInsets) {
            super(laVar, windowInsets);
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public la a() {
            return la.a(((e) this).b.consumeDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public i9 d() {
            return i9.a(((e) this).b.getDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof g)) {
                return false;
            }
            return Objects.equals(((e) this).b, ((e) ((g) obj)).b);
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public int hashCode() {
            return ((e) this).b.hashCode();
        }

        @DexIgnore
        public g(la laVar, g gVar) {
            super(laVar, gVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends g {
        @DexIgnore
        public h(la laVar, WindowInsets windowInsets) {
            super(laVar, windowInsets);
        }

        @DexIgnore
        @Override // com.fossil.la.e, com.fossil.la.i
        public la a(int i, int i2, int i3, int i4) {
            return la.a(((e) this).b.inset(i, i2, i3, i4));
        }

        @DexIgnore
        public h(la laVar, h hVar) {
            super(laVar, hVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i {
        @DexIgnore
        public /* final */ la a;

        @DexIgnore
        public i(la laVar) {
            this.a = laVar;
        }

        @DexIgnore
        public la a() {
            return this.a;
        }

        @DexIgnore
        public la b() {
            return this.a;
        }

        @DexIgnore
        public la c() {
            return this.a;
        }

        @DexIgnore
        public i9 d() {
            return null;
        }

        @DexIgnore
        public f7 e() {
            return f7.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof i)) {
                return false;
            }
            i iVar = (i) obj;
            if (h() != iVar.h() || g() != iVar.g() || !z8.a(f(), iVar.f()) || !z8.a(e(), iVar.e()) || !z8.a(d(), iVar.d())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public f7 f() {
            return f7.e;
        }

        @DexIgnore
        public boolean g() {
            return false;
        }

        @DexIgnore
        public boolean h() {
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return z8.a(Boolean.valueOf(h()), Boolean.valueOf(g()), f(), e(), d());
        }

        @DexIgnore
        public la a(int i, int i2, int i3, int i4) {
            return la.b;
        }
    }

    @DexIgnore
    public la(WindowInsets windowInsets) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 29) {
            this.a = new h(this, windowInsets);
        } else if (i2 >= 28) {
            this.a = new g(this, windowInsets);
        } else if (i2 >= 21) {
            this.a = new f(this, windowInsets);
        } else if (i2 >= 20) {
            this.a = new e(this, windowInsets);
        } else {
            this.a = new i(this);
        }
    }

    @DexIgnore
    public static la a(WindowInsets windowInsets) {
        e9.a(windowInsets);
        return new la(windowInsets);
    }

    @DexIgnore
    @Deprecated
    public la b(int i2, int i3, int i4, int i5) {
        a aVar = new a(this);
        aVar.b(f7.a(i2, i3, i4, i5));
        return aVar.a();
    }

    @DexIgnore
    public la c() {
        return this.a.c();
    }

    @DexIgnore
    public int d() {
        return h().d;
    }

    @DexIgnore
    public int e() {
        return h().a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof la)) {
            return false;
        }
        return z8.a(this.a, ((la) obj).a);
    }

    @DexIgnore
    public int f() {
        return h().c;
    }

    @DexIgnore
    public int g() {
        return h().b;
    }

    @DexIgnore
    public f7 h() {
        return this.a.f();
    }

    @DexIgnore
    public int hashCode() {
        i iVar = this.a;
        if (iVar == null) {
            return 0;
        }
        return iVar.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return !h().equals(f7.e);
    }

    @DexIgnore
    public boolean j() {
        return this.a.g();
    }

    @DexIgnore
    public WindowInsets k() {
        i iVar = this.a;
        if (iVar instanceof e) {
            return ((e) iVar).b;
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends d {
        @DexIgnore
        public static Field c;
        @DexIgnore
        public static boolean d;
        @DexIgnore
        public static Constructor<WindowInsets> e;
        @DexIgnore
        public static boolean f;
        @DexIgnore
        public WindowInsets b;

        @DexIgnore
        public b() {
            this.b = b();
        }

        @DexIgnore
        @Override // com.fossil.la.d
        public la a() {
            return la.a(this.b);
        }

        @DexIgnore
        @Override // com.fossil.la.d
        public void b(f7 f7Var) {
            WindowInsets windowInsets = this.b;
            if (windowInsets != null) {
                this.b = windowInsets.replaceSystemWindowInsets(f7Var.a, f7Var.b, f7Var.c, f7Var.d);
            }
        }

        @DexIgnore
        public b(la laVar) {
            this.b = laVar.k();
        }

        @DexIgnore
        public static WindowInsets b() {
            if (!d) {
                try {
                    c = WindowInsets.class.getDeclaredField("CONSUMED");
                } catch (ReflectiveOperationException e2) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", e2);
                }
                d = true;
            }
            Field field = c;
            if (field != null) {
                try {
                    WindowInsets windowInsets = (WindowInsets) field.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                } catch (ReflectiveOperationException e3) {
                    Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", e3);
                }
            }
            if (!f) {
                try {
                    e = WindowInsets.class.getConstructor(Rect.class);
                } catch (ReflectiveOperationException e4) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", e4);
                }
                f = true;
            }
            Constructor<WindowInsets> constructor = e;
            if (constructor != null) {
                try {
                    return constructor.newInstance(new Rect());
                } catch (ReflectiveOperationException e5) {
                    Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", e5);
                }
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends e {
        @DexIgnore
        public f7 d; // = null;

        @DexIgnore
        public f(la laVar, WindowInsets windowInsets) {
            super(laVar, windowInsets);
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public la b() {
            return la.a(((e) this).b.consumeStableInsets());
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public la c() {
            return la.a(((e) this).b.consumeSystemWindowInsets());
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public final f7 e() {
            if (this.d == null) {
                this.d = f7.a(((e) this).b.getStableInsetLeft(), ((e) this).b.getStableInsetTop(), ((e) this).b.getStableInsetRight(), ((e) this).b.getStableInsetBottom());
            }
            return this.d;
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public boolean g() {
            return ((e) this).b.isConsumed();
        }

        @DexIgnore
        public f(la laVar, f fVar) {
            super(laVar, fVar);
        }
    }

    @DexIgnore
    public la a() {
        return this.a.a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends i {
        @DexIgnore
        public /* final */ WindowInsets b;
        @DexIgnore
        public f7 c;

        @DexIgnore
        public e(la laVar, WindowInsets windowInsets) {
            super(laVar);
            this.c = null;
            this.b = windowInsets;
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public la a(int i, int i2, int i3, int i4) {
            a aVar = new a(la.a(this.b));
            aVar.b(la.a(f(), i, i2, i3, i4));
            aVar.a(la.a(e(), i, i2, i3, i4));
            return aVar.a();
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public final f7 f() {
            if (this.c == null) {
                this.c = f7.a(this.b.getSystemWindowInsetLeft(), this.b.getSystemWindowInsetTop(), this.b.getSystemWindowInsetRight(), this.b.getSystemWindowInsetBottom());
            }
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.la.i
        public boolean h() {
            return this.b.isRound();
        }

        @DexIgnore
        public e(la laVar, e eVar) {
            this(laVar, new WindowInsets(eVar.b));
        }
    }

    @DexIgnore
    public la a(int i2, int i3, int i4, int i5) {
        return this.a.a(i2, i3, i4, i5);
    }

    @DexIgnore
    public static f7 a(f7 f7Var, int i2, int i3, int i4, int i5) {
        int max = Math.max(0, f7Var.a - i2);
        int max2 = Math.max(0, f7Var.b - i3);
        int max3 = Math.max(0, f7Var.c - i4);
        int max4 = Math.max(0, f7Var.d - i5);
        if (max == i2 && max2 == i3 && max3 == i4 && max4 == i5) {
            return f7Var;
        }
        return f7.a(max, max2, max3, max4);
    }

    @DexIgnore
    public la b() {
        return this.a.b();
    }

    @DexIgnore
    public la(la laVar) {
        if (laVar != null) {
            i iVar = laVar.a;
            if (Build.VERSION.SDK_INT >= 29 && (iVar instanceof h)) {
                this.a = new h(this, (h) iVar);
            } else if (Build.VERSION.SDK_INT >= 28 && (iVar instanceof g)) {
                this.a = new g(this, (g) iVar);
            } else if (Build.VERSION.SDK_INT >= 21 && (iVar instanceof f)) {
                this.a = new f(this, (f) iVar);
            } else if (Build.VERSION.SDK_INT < 20 || !(iVar instanceof e)) {
                this.a = new i(this);
            } else {
                this.a = new e(this, (e) iVar);
            }
        } else {
            this.a = new i(this);
        }
    }
}
