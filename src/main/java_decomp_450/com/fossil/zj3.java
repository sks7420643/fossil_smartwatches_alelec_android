package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj3 extends kh3 {
    @DexIgnore
    public volatile wj3 c;
    @DexIgnore
    public wj3 d;
    @DexIgnore
    public wj3 e;
    @DexIgnore
    public /* final */ Map<Activity, wj3> f; // = new ConcurrentHashMap();
    @DexIgnore
    public Activity g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public volatile wj3 i;
    @DexIgnore
    public wj3 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ Object l; // = new Object();
    @DexIgnore
    public String m;

    @DexIgnore
    public zj3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public final wj3 A() {
        a();
        return this.c;
    }

    @DexIgnore
    public final wj3 a(boolean z) {
        w();
        g();
        if (!l().a(wb3.D0) || !z) {
            return this.e;
        }
        wj3 wj3 = this.e;
        return wj3 != null ? wj3 : this.j;
    }

    @DexIgnore
    public final void b(Activity activity) {
        if (l().a(wb3.D0)) {
            synchronized (this.l) {
                this.k = false;
                this.h = true;
            }
        }
        long c2 = zzm().c();
        if (!l().a(wb3.C0) || l().r().booleanValue()) {
            wj3 d2 = d(activity);
            this.d = this.c;
            this.c = null;
            c().a(new ck3(this, d2, c2));
            return;
        }
        this.c = null;
        c().a(new dk3(this, c2));
    }

    @DexIgnore
    public final void c(Activity activity) {
        synchronized (this.l) {
            if (activity == this.g) {
                this.g = null;
            }
        }
        if (l().r().booleanValue()) {
            this.f.remove(activity);
        }
    }

    @DexIgnore
    public final wj3 d(Activity activity) {
        a72.a(activity);
        wj3 wj3 = this.f.get(activity);
        if (wj3 == null) {
            wj3 wj32 = new wj3(null, a(activity.getClass().getCanonicalName()), j().s());
            this.f.put(activity, wj32);
            wj3 = wj32;
        }
        return (l().a(wb3.D0) && this.i != null) ? this.i : wj3;
    }

    @DexIgnore
    @Override // com.fossil.kh3
    public final boolean z() {
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00da, code lost:
        r1 = e().B();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e2, code lost:
        if (r10 != null) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e4, code lost:
        r2 = "null";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e7, code lost:
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e8, code lost:
        if (r2 != null) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ea, code lost:
        r3 = "null";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ed, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ee, code lost:
        r1.a("Logging screen view with name, class", r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00f5, code lost:
        if (r17.c != null) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00f7, code lost:
        r1 = r17.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00fa, code lost:
        r1 = r17.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00fc, code lost:
        r4 = new com.fossil.wj3(r10, r2, j().s(), true, r19);
        r17.c = r4;
        r17.d = r1;
        r17.i = r4;
        c().a(new com.fossil.yj3(r17, r18, r4, r1, zzm().c()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x012d, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.os.Bundle r18, long r19) {
        /*
            r17 = this;
            r8 = r17
            r0 = r18
            com.fossil.ym3 r1 = r17.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.D0
            boolean r1 = r1.a(r2)
            if (r1 != 0) goto L_0x001e
            com.fossil.jg3 r0 = r17.e()
            com.fossil.mg3 r0 = r0.y()
            java.lang.String r1 = "Manual screen reporting is disabled."
            r0.a(r1)
            return
        L_0x001e:
            java.lang.Object r1 = r8.l
            monitor-enter(r1)
            boolean r2 = r8.k     // Catch:{ all -> 0x012e }
            if (r2 != 0) goto L_0x0034
            com.fossil.jg3 r0 = r17.e()     // Catch:{ all -> 0x012e }
            com.fossil.mg3 r0 = r0.y()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "Cannot log screen view event when the app is in the background."
            r0.a(r2)     // Catch:{ all -> 0x012e }
            monitor-exit(r1)     // Catch:{ all -> 0x012e }
            return
        L_0x0034:
            r2 = 0
            if (r0 == 0) goto L_0x0092
            java.lang.String r2 = "screen_name"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x012e }
            r3 = 100
            if (r2 == 0) goto L_0x0064
            int r4 = r2.length()     // Catch:{ all -> 0x012e }
            if (r4 <= 0) goto L_0x004d
            int r4 = r2.length()     // Catch:{ all -> 0x012e }
            if (r4 <= r3) goto L_0x0064
        L_0x004d:
            com.fossil.jg3 r0 = r17.e()     // Catch:{ all -> 0x012e }
            com.fossil.mg3 r0 = r0.y()     // Catch:{ all -> 0x012e }
            java.lang.String r3 = "Invalid screen name length for screen view. Length"
            int r2 = r2.length()     // Catch:{ all -> 0x012e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x012e }
            r0.a(r3, r2)     // Catch:{ all -> 0x012e }
            monitor-exit(r1)     // Catch:{ all -> 0x012e }
            return
        L_0x0064:
            java.lang.String r4 = "screen_class"
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x012e }
            if (r4 == 0) goto L_0x008f
            int r5 = r4.length()     // Catch:{ all -> 0x012e }
            if (r5 <= 0) goto L_0x0078
            int r5 = r4.length()     // Catch:{ all -> 0x012e }
            if (r5 <= r3) goto L_0x008f
        L_0x0078:
            com.fossil.jg3 r0 = r17.e()     // Catch:{ all -> 0x012e }
            com.fossil.mg3 r0 = r0.y()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "Invalid screen class length for screen view. Length"
            int r3 = r4.length()     // Catch:{ all -> 0x012e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x012e }
            r0.a(r2, r3)     // Catch:{ all -> 0x012e }
            monitor-exit(r1)     // Catch:{ all -> 0x012e }
            return
        L_0x008f:
            r10 = r2
            r2 = r4
            goto L_0x0093
        L_0x0092:
            r10 = r2
        L_0x0093:
            if (r2 != 0) goto L_0x00aa
            android.app.Activity r2 = r8.g     // Catch:{ all -> 0x012e }
            if (r2 == 0) goto L_0x00a8
            android.app.Activity r2 = r8.g     // Catch:{ all -> 0x012e }
            java.lang.Class r2 = r2.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = r2.getCanonicalName()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = a(r2)     // Catch:{ all -> 0x012e }
            goto L_0x00aa
        L_0x00a8:
            java.lang.String r2 = "Activity"
        L_0x00aa:
            r11 = r2
            boolean r2 = r8.h     // Catch:{ all -> 0x012e }
            if (r2 == 0) goto L_0x00d9
            com.fossil.wj3 r2 = r8.c     // Catch:{ all -> 0x012e }
            if (r2 == 0) goto L_0x00d9
            r2 = 0
            r8.h = r2     // Catch:{ all -> 0x012e }
            com.fossil.wj3 r2 = r8.c     // Catch:{ all -> 0x012e }
            java.lang.String r2 = r2.b     // Catch:{ all -> 0x012e }
            boolean r2 = com.fossil.jm3.c(r2, r11)     // Catch:{ all -> 0x012e }
            com.fossil.wj3 r3 = r8.c     // Catch:{ all -> 0x012e }
            java.lang.String r3 = r3.a     // Catch:{ all -> 0x012e }
            boolean r3 = com.fossil.jm3.c(r3, r10)     // Catch:{ all -> 0x012e }
            if (r2 == 0) goto L_0x00d9
            if (r3 == 0) goto L_0x00d9
            com.fossil.jg3 r0 = r17.e()     // Catch:{ all -> 0x012e }
            com.fossil.mg3 r0 = r0.y()     // Catch:{ all -> 0x012e }
            java.lang.String r2 = "Ignoring call to log screen view event with duplicate parameters."
            r0.a(r2)     // Catch:{ all -> 0x012e }
            monitor-exit(r1)     // Catch:{ all -> 0x012e }
            return
        L_0x00d9:
            monitor-exit(r1)     // Catch:{ all -> 0x012e }
            com.fossil.jg3 r1 = r17.e()
            com.fossil.mg3 r1 = r1.B()
            if (r10 != 0) goto L_0x00e7
            java.lang.String r2 = "null"
            goto L_0x00e8
        L_0x00e7:
            r2 = r10
        L_0x00e8:
            if (r11 != 0) goto L_0x00ed
            java.lang.String r3 = "null"
            goto L_0x00ee
        L_0x00ed:
            r3 = r11
        L_0x00ee:
            java.lang.String r4 = "Logging screen view with name, class"
            r1.a(r4, r2, r3)
            com.fossil.wj3 r1 = r8.c
            if (r1 != 0) goto L_0x00fa
            com.fossil.wj3 r1 = r8.d
            goto L_0x00fc
        L_0x00fa:
            com.fossil.wj3 r1 = r8.c
        L_0x00fc:
            r5 = r1
            com.fossil.wj3 r4 = new com.fossil.wj3
            com.fossil.jm3 r1 = r17.j()
            long r12 = r1.s()
            r14 = 1
            r9 = r4
            r15 = r19
            r9.<init>(r10, r11, r12, r14, r15)
            r8.c = r4
            r8.d = r5
            r8.i = r4
            com.fossil.n92 r1 = r17.zzm()
            long r6 = r1.c()
            com.fossil.hh3 r9 = r17.c()
            com.fossil.yj3 r10 = new com.fossil.yj3
            r1 = r10
            r2 = r17
            r3 = r18
            r1.<init>(r2, r3, r4, r5, r6)
            r9.a(r10)
            return
        L_0x012e:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zj3.a(android.os.Bundle, long):void");
    }

    @DexIgnore
    public final void b(Activity activity, Bundle bundle) {
        wj3 wj3;
        if (l().r().booleanValue() && bundle != null && (wj3 = this.f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", wj3.c);
            bundle2.putString("name", wj3.a);
            bundle2.putString("referrer_name", wj3.b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, wj3 wj3, wj3 wj32, long j2) {
        if (bundle != null) {
            bundle.remove("screen_name");
            bundle.remove("screen_class");
        }
        a(wj3, wj32, j2, true, j().a((String) null, "screen_view", bundle, (List<String>) null, true, true));
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        if (!l().r().booleanValue()) {
            e().y().a("setCurrentScreen cannot be called while screen reporting is disabled.");
        } else if (this.c == null) {
            e().y().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f.get(activity) == null) {
            e().y().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean c2 = jm3.c(this.c.b, str2);
            boolean c3 = jm3.c(this.c.a, str);
            if (c2 && c3) {
                e().y().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                e().y().a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                e().B().a("Setting current screen to name, class", str == null ? "null" : str, str2);
                wj3 wj3 = new wj3(str, str2, j().s());
                this.f.put(activity, wj3);
                a(activity, wj3, true);
            } else {
                e().y().a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @DexIgnore
    public final void a(Activity activity, wj3 wj3, boolean z) {
        wj3 wj32;
        wj3 wj33 = this.c == null ? this.d : this.c;
        if (wj3.b == null) {
            wj32 = new wj3(wj3.a, activity != null ? a(activity.getClass().getCanonicalName()) : null, wj3.c, wj3.e, wj3.f);
        } else {
            wj32 = wj3;
        }
        this.d = this.c;
        this.c = wj32;
        c().a(new bk3(this, wj32, wj33, zzm().c(), z));
    }

    @DexIgnore
    public final void a(wj3 wj3, wj3 wj32, long j2, boolean z, Bundle bundle) {
        boolean z2;
        long j3;
        long j4;
        wj3 wj33;
        g();
        boolean z3 = false;
        if (l().a(wb3.U)) {
            z2 = z && this.e != null;
            if (z2) {
                a(this.e, true, j2);
            }
        } else {
            if (z && (wj33 = this.e) != null) {
                a(wj33, true, j2);
            }
            z2 = false;
        }
        if (wj32 == null || wj32.c != wj3.c || !jm3.c(wj32.b, wj3.b) || !jm3.c(wj32.a, wj3.a)) {
            z3 = true;
        }
        if (z3) {
            Bundle bundle2 = new Bundle();
            if (l().a(wb3.D0)) {
                bundle2 = bundle != null ? new Bundle(bundle) : new Bundle();
            }
            a(wj3, bundle2, true);
            if (wj32 != null) {
                String str = wj32.a;
                if (str != null) {
                    bundle2.putString("_pn", str);
                }
                String str2 = wj32.b;
                if (str2 != null) {
                    bundle2.putString("_pc", str2);
                }
                bundle2.putLong("_pi", wj32.c);
            }
            if (l().a(wb3.U) && z2) {
                if (!u13.a() || !l().a(wb3.W) || !i13.a() || !l().a(wb3.A0)) {
                    j4 = t().e.b();
                } else {
                    j4 = t().a(j2);
                }
                if (j4 > 0) {
                    j().a(bundle2, j4);
                }
            }
            String str3 = "auto";
            if (l().a(wb3.D0)) {
                if (!l().r().booleanValue()) {
                    bundle2.putLong("_mt", 1);
                }
                if (wj3.e) {
                    str3 = "app";
                }
            }
            if (l().a(wb3.D0)) {
                long b = zzm().b();
                if (wj3.e) {
                    long j5 = wj3.f;
                    if (j5 != 0) {
                        j3 = j5;
                        o().a(str3, "_vs", j3, bundle2);
                    }
                }
                j3 = b;
                o().a(str3, "_vs", j3, bundle2);
            } else {
                o().b(str3, "_vs", bundle2);
            }
        }
        this.e = wj3;
        if (l().a(wb3.D0) && wj3.e) {
            this.j = wj3;
        }
        q().a(wj3);
    }

    @DexIgnore
    public final void a(wj3 wj3, boolean z, long j2) {
        n().a(zzm().c());
        if (t().a(wj3 != null && wj3.d, z, j2) && wj3 != null) {
            wj3.d = false;
        }
    }

    @DexIgnore
    public static void a(wj3 wj3, Bundle bundle, boolean z) {
        if (bundle != null && wj3 != null && (!bundle.containsKey("_sc") || z)) {
            String str = wj3.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            String str2 = wj3.b;
            if (str2 != null) {
                bundle.putString("_sc", str2);
            } else {
                bundle.remove("_sc");
            }
            bundle.putLong("_si", wj3.c);
        } else if (bundle != null && wj3 == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @DexIgnore
    public final void a(String str, wj3 wj3) {
        g();
        synchronized (this) {
            if (this.m == null || this.m.equals(str) || wj3 != null) {
                this.m = str;
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @DexIgnore
    public final void a(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (l().r().booleanValue() && bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f.put(activity, new wj3(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        if (l().a(wb3.D0)) {
            synchronized (this.l) {
                this.k = true;
                if (activity != this.g) {
                    synchronized (this.l) {
                        this.g = activity;
                        this.h = false;
                    }
                    if (l().a(wb3.C0) && l().r().booleanValue()) {
                        this.i = null;
                        c().a(new fk3(this));
                    }
                }
            }
        }
        if (!l().a(wb3.C0) || l().r().booleanValue()) {
            a(activity, d(activity), false);
            fb3 n = n();
            n.c().a(new jf3(n, n.zzm().c()));
            return;
        }
        this.c = this.i;
        c().a(new ak3(this));
    }
}
