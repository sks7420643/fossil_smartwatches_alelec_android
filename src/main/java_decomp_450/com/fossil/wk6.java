package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk6 implements Factory<vk6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public wk6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static wk6 a(Provider<ThemeRepository> provider) {
        return new wk6(provider);
    }

    @DexIgnore
    public static vk6 a(ThemeRepository themeRepository) {
        return new vk6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vk6 get() {
        return a(this.a.get());
    }
}
