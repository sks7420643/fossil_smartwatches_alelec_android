package com.fossil;

import android.os.RemoteException;
import com.fossil.e53;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm2 extends e53.a<h53> {
    @DexIgnore
    public /* final */ /* synthetic */ f53 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hm2(gm2 gm2, a12 a12, f53 f53, String str) {
        super(a12);
        this.s = f53;
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ i12 a(Status status) {
        return new h53(status);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b] */
    @Override // com.fossil.r12
    public final /* synthetic */ void a(yl2 yl2) throws RemoteException {
        yl2.a(this.s, this, this.t);
    }
}
