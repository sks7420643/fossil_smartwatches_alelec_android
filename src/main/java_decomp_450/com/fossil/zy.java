package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zy<T> {
    @DexIgnore
    int a();

    @DexIgnore
    int a(T t);

    @DexIgnore
    String getTag();

    @DexIgnore
    T newArray(int i);
}
