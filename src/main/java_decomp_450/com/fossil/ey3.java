package com.fossil;

import com.fossil.iy3;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey3<K, V> extends iy3.b<K> {
    @DexIgnore
    @Weak
    public /* final */ by3<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ by3<K, ?> map;

        @DexIgnore
        public a(by3<K, ?> by3) {
            this.map = by3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.keySet();
        }
    }

    @DexIgnore
    public ey3(by3<K, V> by3) {
        this.map = by3;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.iy3.b
    public K get(int i) {
        return this.map.entrySet().asList().get(i).getKey();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.iy3
    public Object writeReplace() {
        return new a(this.map);
    }

    @DexIgnore
    @Override // com.fossil.iy3.b, com.fossil.iy3.b, java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
    public j04<K> iterator() {
        return this.map.keyIterator();
    }
}
