package com.fossil;

import android.os.Handler;
import android.os.Looper;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eo0 implements h01<s91> {
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public /* final */ nm1 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public lk0 d;
    @DexIgnore
    public gd7<? super eo0, i97> e;
    @DexIgnore
    public gd7<? super eo0, i97> f;
    @DexIgnore
    public vc7<i97> g;
    @DexIgnore
    public /* final */ aq0 h;
    @DexIgnore
    public /* final */ cx0 i;

    @DexIgnore
    public eo0(aq0 aq0, cx0 cx0) {
        this.h = aq0;
        this.i = cx0;
        yz0.a(aq0);
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.a = new Handler(myLooper);
            this.b = nm1.NORMAL;
            this.d = new lk0(this.h, oi0.b, null, 4);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public abstract void a(ri1 ri1);

    @DexIgnore
    public void a(Object obj) {
        s91 s91 = (s91) obj;
        if (b(s91)) {
            oi0 oi0 = this.d.b;
            if (oi0 == oi0.l || oi0 == oi0.e) {
                this.d = lk0.a(this.d, null, null, s91.a, 3);
            } else {
                a(s91);
            }
            a();
        }
    }

    @DexIgnore
    public nm1 b() {
        return this.b;
    }

    @DexIgnore
    public abstract boolean b(s91 s91);

    @DexIgnore
    public abstract qy0<s91> c();

    @DexIgnore
    public void c(s91 s91) {
    }

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public final eo0 a(vc7<i97> vc7) {
        this.g = vc7;
        return this;
    }

    @DexIgnore
    public final void a(oi0 oi0) {
        if (!this.c) {
            t11 t11 = t11.a;
            this.d = lk0.a(this.d, null, oi0, null, 5);
            if (d()) {
                this.a.postDelayed(new im0(this), 5000);
            } else {
                a();
            }
        }
    }

    @DexIgnore
    public void a(s91 s91) {
        c(s91);
        lk0 a2 = lk0.d.a(s91.a);
        this.d = lk0.a(this.d, null, a2.b, a2.c, 1);
    }

    @DexIgnore
    public final void a() {
        if (!this.c) {
            this.c = true;
            qy0<s91> c2 = c();
            if (c2 != null) {
                c2.b(this);
            }
            oi0 oi0 = oi0.a;
            lk0 lk0 = this.d;
            if (oi0 == lk0.b) {
                t11 t11 = t11.a;
                k60.a(lk0, 0, 1, null);
                gd7<? super eo0, i97> gd7 = this.e;
                if (gd7 != null) {
                    gd7.invoke(this);
                }
            } else {
                t11 t112 = t11.a;
                k60.a(lk0, 0, 1, null);
                gd7<? super eo0, i97> gd72 = this.f;
                if (gd72 != null) {
                    gd72.invoke(this);
                }
            }
            vc7<i97> vc7 = this.g;
            if (vc7 != null) {
                vc7.invoke();
            }
        }
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return new JSONObject();
    }
}
