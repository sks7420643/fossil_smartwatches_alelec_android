package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t50 {
    @DexIgnore
    public Class<?> a;
    @DexIgnore
    public Class<?> b;
    @DexIgnore
    public Class<?> c;

    @DexIgnore
    public t50() {
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || t50.class != obj.getClass()) {
            return false;
        }
        t50 t50 = (t50) obj;
        return this.a.equals(t50.a) && this.b.equals(t50.b) && v50.b(this.c, t50.c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.a.hashCode() * 31) + this.b.hashCode()) * 31;
        Class<?> cls = this.c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    @DexIgnore
    public String toString() {
        return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
    }

    @DexIgnore
    public t50(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }
}
