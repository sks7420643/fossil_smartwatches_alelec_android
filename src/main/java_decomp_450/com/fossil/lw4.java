package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lw4 extends kw4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362109, 1);
        F.put(2131361851, 2);
        F.put(2131363342, 3);
        F.put(2131362798, 4);
        F.put(2131361954, 5);
        F.put(2131362575, 6);
        F.put(2131363393, 7);
        F.put(2131361947, 8);
        F.put(2131362573, 9);
        F.put(2131363394, 10);
        F.put(2131361945, 11);
        F.put(2131362572, 12);
    }
    */

    @DexIgnore
    public lw4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 13, E, F));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public lw4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[2], (RelativeLayout) objArr[11], (RelativeLayout) objArr[8], (RelativeLayout) objArr[5], (ConstraintLayout) objArr[1], (RTLImageView) objArr[12], (RTLImageView) objArr[9], (RTLImageView) objArr[6], (LinearLayout) objArr[4], (RelativeLayout) objArr[0], (FlexibleTextView) objArr[3], (View) objArr[7], (View) objArr[10]);
        this.D = -1;
        ((kw4) this).z.setTag(null);
        a(view);
        f();
    }
}
