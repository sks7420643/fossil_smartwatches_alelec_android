package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt0 extends wi1<n80[], HashMap<o80, n80>> {
    @DexIgnore
    public static /* final */ vt0[] b;
    @DexIgnore
    public static /* final */ vt0[] c;
    @DexIgnore
    public static /* final */ y71<n80[]>[] d; // = {new mm0(), new io0()};
    @DexIgnore
    public static /* final */ uk1<HashMap<o80, n80>>[] e; // = {new eq0(pb1.DEVICE_CONFIG), new as0(pb1.DEVICE_CONFIG)};
    @DexIgnore
    public static /* final */ wt0 f; // = new wt0();

    /*
    static {
        vt0[] vt0Arr = {new vt0(o80.BIOMETRIC_PROFILE, new u91(c80.CREATOR)), new vt0(o80.DAILY_STEP, new ob1(k80.CREATOR)), new vt0(o80.DAILY_STEP_GOAL, new id1(l80.CREATOR)), new vt0(o80.DAILY_CALORIE, new cf1(g80.CREATOR)), new vt0(o80.DAILY_CALORIE_GOAL, new yg1(h80.CREATOR)), new vt0(o80.DAILY_TOTAL_ACTIVE_MINUTE, new vi1(m80.CREATOR)), new vt0(o80.DAILY_ACTIVE_MINUTE_GOAL, new tk1(f80.CREATOR)), new vt0(o80.DAILY_DISTANCE, new rm1(i80.CREATOR)), new vt0(o80.INACTIVE_NUDGE, new qo1(t80.CREATOR)), new vt0(o80.VIBE_STRENGTH, new pv0(w80.CREATOR)), new vt0(o80.DO_NOT_DISTURB_SCHEDULE, new hx0(q80.CREATOR)), new vt0(o80.TIME, new zy0(v80.CREATOR)), new vt0(o80.BATTERY, new r01(b80.CREATOR)), new vt0(o80.HEART_RATE_MODE, new k21(r80.CREATOR)), new vt0(o80.DAILY_SLEEP, new e41(j80.CREATOR)), new vt0(o80.DISPLAY_UNIT, new b61(p80.CREATOR)), new vt0(o80.SECOND_TIMEZONE_OFFSET, new z71(u80.CREATOR))};
        b = vt0Arr;
        c = (vt0[]) s97.b(vt0Arr, new vt0[]{new vt0(o80.CURRENT_HEART_RATE, new qq1(d80.CREATOR)), new vt0(o80.HELLAS_BATTERY, new ps1(s80.CREATOR)), new vt0(o80.AUTO_WORKOUT_DETECTION, new si0(y80.CREATOR)), new vt0(o80.CYCLING_CADENCE, new pk0(e80.CREATOR))});
    }
    */

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<HashMap<o80, n80>>[] b() {
        return e;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<n80[]>[] a() {
        return d;
    }

    @DexIgnore
    public final byte[] a(n80[] n80Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (n80 n80 : n80Arr) {
            byteArrayOutputStream.write(n80.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "entryData.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final HashMap<o80, n80> a(byte[] bArr, vt0[] vt0Arr) {
        n80 n80;
        vt0 vt0;
        int length = (bArr.length - 12) - 4;
        byte[] a = s97.a(bArr, 12, length + 12);
        ByteBuffer order = ByteBuffer.wrap(a).order(ByteOrder.LITTLE_ENDIAN);
        HashMap<o80, n80> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i + 3;
            if (i2 > length) {
                return hashMap;
            }
            short s = order.getShort(i);
            byte b2 = order.get(i + 2);
            o80 a2 = o80.d.a(s);
            try {
                byte[] a3 = s97.a(a, i2, i2 + b2);
                int length2 = vt0Arr.length;
                int i3 = 0;
                while (true) {
                    n80 = null;
                    if (i3 >= length2) {
                        vt0 = null;
                        break;
                    }
                    vt0 = vt0Arr[i3];
                    if (vt0.a == a2) {
                        break;
                    }
                    i3++;
                }
                if (vt0 != null) {
                    n80 = vt0.b.invoke(a3);
                }
                if (!(a2 == null || n80 == null)) {
                    hashMap.put(a2, n80);
                }
            } catch (Exception e2) {
                wl0.h.a(e2);
            }
            i += b2 + 3;
        }
    }
}
