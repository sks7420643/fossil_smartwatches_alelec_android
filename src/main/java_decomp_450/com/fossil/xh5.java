package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "hourNotification")
public class xh5 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xh5> CREATOR; // = new a();
    @DexIgnore
    @DatabaseField(columnName = AppFilter.COLUMN_HOUR)
    public int a;
    @DexIgnore
    @DatabaseField(columnName = AppFilter.COLUMN_IS_VIBRATION_ONLY)
    public boolean b;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long c;
    @DexIgnore
    @DatabaseField(columnName = "extraId")
    public String d;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String e;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<xh5> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public xh5 createFromParcel(Parcel parcel) {
            return new xh5(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public xh5[] newArray(int i) {
            return new xh5[i];
        }
    }

    @DexIgnore
    public xh5() {
    }

    @DexIgnore
    public void a(String str) {
        this.f = str;
    }

    @DexIgnore
    public String b() {
        return this.f;
    }

    @DexIgnore
    public void c(String str) {
        this.e = str;
    }

    @DexIgnore
    public int d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String e() {
        return this.e;
    }

    @DexIgnore
    public boolean f() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeByte(this.b ? (byte) 1 : 0);
        parcel.writeLong(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
    }

    @DexIgnore
    public xh5(int i, boolean z, String str, String str2) {
        this.a = i;
        this.b = z;
        this.d = str;
        this.f = str2;
        this.e = str + str2;
    }

    @DexIgnore
    public void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public void b(String str) {
        this.d = str;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public long a() {
        return this.c;
    }

    @DexIgnore
    public void a(long j) {
        this.c = j;
    }

    @DexIgnore
    public xh5(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readByte() != 0;
        this.c = parcel.readLong();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
    }
}
