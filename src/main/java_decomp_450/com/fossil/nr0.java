package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr0 implements Parcelable.Creator<jt0> {
    @DexIgnore
    public /* synthetic */ nr0(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public jt0 createFromParcel(Parcel parcel) {
        return new jt0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public jt0[] newArray(int i) {
        return new jt0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public jt0 m43createFromParcel(Parcel parcel) {
        return new jt0(parcel, null);
    }
}
