package com.fossil;

import java.util.Comparator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e04 {

    @DexIgnore
    public enum b {
        NEXT_LOWER {
            @DexIgnore
            @Override // com.fossil.e04.b
            public int resultIndex(int i) {
                return i - 1;
            }
        },
        NEXT_HIGHER {
            @DexIgnore
            @Override // com.fossil.e04.b
            public int resultIndex(int i) {
                return i;
            }
        },
        INVERTED_INSERTION_INDEX {
            @DexIgnore
            @Override // com.fossil.e04.b
            public int resultIndex(int i) {
                return ~i;
            }
        };

        @DexIgnore
        public abstract int resultIndex(int i);
    }

    @DexIgnore
    public enum c {
        ANY_PRESENT {
            @DexIgnore
            @Override // com.fossil.e04.c
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return i;
            }
        },
        LAST_PRESENT {
            @DexIgnore
            @Override // com.fossil.e04.c
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int size = list.size() - 1;
                while (i < size) {
                    int i2 = ((i + size) + 1) >>> 1;
                    if (comparator.compare((Object) list.get(i2), e) > 0) {
                        size = i2 - 1;
                    } else {
                        i = i2;
                    }
                }
                return i;
            }
        },
        FIRST_PRESENT {
            @DexIgnore
            @Override // com.fossil.e04.c
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int i2 = 0;
                while (i2 < i) {
                    int i3 = (i2 + i) >>> 1;
                    if (comparator.compare((Object) list.get(i3), e) < 0) {
                        i2 = i3 + 1;
                    } else {
                        i = i3;
                    }
                }
                return i2;
            }
        },
        FIRST_AFTER {
            @DexIgnore
            @Override // com.fossil.e04.c
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return c.LAST_PRESENT.resultIndex(comparator, e, list, i) + 1;
            }
        },
        LAST_BEFORE {
            @DexIgnore
            @Override // com.fossil.e04.c
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return c.FIRST_PRESENT.resultIndex(comparator, e, list, i) - 1;
            }
        };

        @DexIgnore
        public abstract <E> int resultIndex(Comparator<? super E> comparator, E e2, List<? extends E> list, int i);
    }

    @DexIgnore
    public static <E> int a(List<? extends E> list, E e, Comparator<? super E> comparator, c cVar, b bVar) {
        jw3.a(comparator);
        jw3.a(list);
        jw3.a(cVar);
        jw3.a(bVar);
        if (!(list instanceof RandomAccess)) {
            list = uy3.a(list);
        }
        int i = 0;
        int size = list.size() - 1;
        while (i <= size) {
            int i2 = (i + size) >>> 1;
            int compare = comparator.compare(e, (Object) list.get(i2));
            if (compare < 0) {
                size = i2 - 1;
            } else if (compare <= 0) {
                return i + cVar.resultIndex(comparator, e, list.subList(i, size + 1), i2 - i);
            } else {
                i = i2 + 1;
            }
        }
        return bVar.resultIndex(i);
    }
}
