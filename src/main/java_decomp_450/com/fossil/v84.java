package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v84 {
    @DexIgnore
    v84 a(String str, int i) throws IOException;

    @DexIgnore
    v84 a(String str, long j) throws IOException;

    @DexIgnore
    v84 a(String str, Object obj) throws IOException;

    @DexIgnore
    v84 a(String str, boolean z) throws IOException;
}
