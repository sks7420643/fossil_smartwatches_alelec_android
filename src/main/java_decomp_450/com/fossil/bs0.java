package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs0 extends wi1<n90, i97> {
    @DexIgnore
    public static /* final */ y71<n90>[] b; // = {new jo0(), new fq0()};
    @DexIgnore
    public static /* final */ uk1<i97>[] c; // = new uk1[0];
    @DexIgnore
    public static /* final */ bs0 d; // = new bs0();

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<i97>[] b() {
        return c;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<n90>[] a() {
        return b;
    }

    @DexIgnore
    public final byte[] a(n90 n90) {
        return n90.b();
    }
}
