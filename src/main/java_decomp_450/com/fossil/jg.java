package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.ng;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jg<T> {
    @DexIgnore
    public static /* final */ Executor h; // = new c();
    @DexIgnore
    public /* final */ xg a;
    @DexIgnore
    public /* final */ ig<T> b;
    @DexIgnore
    public Executor c;
    @DexIgnore
    public /* final */ List<b<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<T> e;
    @DexIgnore
    public List<T> f; // = Collections.emptyList();
    @DexIgnore
    public int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jg$a$a")
        /* renamed from: com.fossil.jg$a$a  reason: collision with other inner class name */
        public class C0085a extends ng.b {
            @DexIgnore
            public C0085a() {
            }

            @DexIgnore
            @Override // com.fossil.ng.b
            public int a() {
                return a.this.b.size();
            }

            @DexIgnore
            @Override // com.fossil.ng.b
            public int b() {
                return a.this.a.size();
            }

            @DexIgnore
            @Override // com.fossil.ng.b
            public Object c(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.b.get(i2);
                if (obj != null && obj2 != null) {
                    return jg.this.b.b().getChangePayload(obj, obj2);
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.fossil.ng.b
            public boolean a(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.b.get(i2);
                if (obj != null && obj2 != null) {
                    return jg.this.b.b().areContentsTheSame(obj, obj2);
                }
                if (obj == null && obj2 == null) {
                    return true;
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.fossil.ng.b
            public boolean b(int i, int i2) {
                Object obj = a.this.a.get(i);
                Object obj2 = a.this.b.get(i2);
                if (obj == null || obj2 == null) {
                    return obj == null && obj2 == null;
                }
                return jg.this.b.b().areItemsTheSame(obj, obj2);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ ng.c a;

            @DexIgnore
            public b(ng.c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void run() {
                a aVar = a.this;
                jg jgVar = jg.this;
                if (jgVar.g == aVar.c) {
                    jgVar.a(aVar.b, this.a, aVar.d);
                }
            }
        }

        @DexIgnore
        public a(List list, List list2, int i, Runnable runnable) {
            this.a = list;
            this.b = list2;
            this.c = i;
            this.d = runnable;
        }

        @DexIgnore
        public void run() {
            jg.this.c.execute(new b(ng.a(new C0085a())));
        }
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void a(List<T> list, List<T> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }

    @DexIgnore
    public jg(xg xgVar, ig<T> igVar) {
        this.a = xgVar;
        this.b = igVar;
        if (igVar.c() != null) {
            this.c = igVar.c();
        } else {
            this.c = h;
        }
    }

    @DexIgnore
    public List<T> a() {
        return this.f;
    }

    @DexIgnore
    public void b(List<T> list, Runnable runnable) {
        int i = this.g + 1;
        this.g = i;
        List<T> list2 = this.e;
        if (list != list2) {
            List<T> list3 = this.f;
            if (list == null) {
                int size = list2.size();
                this.e = null;
                this.f = Collections.emptyList();
                this.a.c(0, size);
                a(list3, runnable);
            } else if (list2 == null) {
                this.e = list;
                this.f = Collections.unmodifiableList(list);
                this.a.b(0, list.size());
                a(list3, runnable);
            } else {
                this.b.a().execute(new a(list2, list, i, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(List<T> list) {
        b(list, null);
    }

    @DexIgnore
    public void a(List<T> list, ng.c cVar, Runnable runnable) {
        List<T> list2 = this.f;
        this.e = list;
        this.f = Collections.unmodifiableList(list);
        cVar.a(this.a);
        a(list2, runnable);
    }

    @DexIgnore
    public final void a(List<T> list, Runnable runnable) {
        for (b<T> bVar : this.d) {
            bVar.a(list, this.f);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(b<T> bVar) {
        this.d.add(bVar);
    }
}
