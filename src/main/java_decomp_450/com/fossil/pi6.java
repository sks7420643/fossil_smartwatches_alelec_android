package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pi6 implements Factory<oi6> {
    @DexIgnore
    public /* final */ Provider<on5> a;
    @DexIgnore
    public /* final */ Provider<mn5> b;

    @DexIgnore
    public pi6(Provider<on5> provider, Provider<mn5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static pi6 a(Provider<on5> provider, Provider<mn5> provider2) {
        return new pi6(provider, provider2);
    }

    @DexIgnore
    public static oi6 a(on5 on5, mn5 mn5) {
        return new oi6(on5, mn5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public oi6 get() {
        return a(this.a.get(), this.b.get());
    }
}
