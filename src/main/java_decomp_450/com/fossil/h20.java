package com.fossil;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.zw;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h20<T> implements cx<T, Bitmap> {
    @DexIgnore
    public static /* final */ zw<Long> d; // = zw.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new a());
    @DexIgnore
    public static /* final */ zw<Integer> e; // = zw.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new b());
    @DexIgnore
    public static /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ f<T> a;
    @DexIgnore
    public /* final */ dz b;
    @DexIgnore
    public /* final */ e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements zw.b<Long> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(8);

        @DexIgnore
        public void a(byte[] bArr, Long l, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.a) {
                this.a.position(0);
                messageDigest.update(this.a.putLong(l.longValue()).array());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements zw.b<Integer> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(4);

        @DexIgnore
        public void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.a) {
                    this.a.position(0);
                    messageDigest.update(this.a.putInt(num.intValue()).array());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements f<AssetFileDescriptor> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(a aVar) {
            this();
        }

        @DexIgnore
        public void a(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements f<ByteBuffer> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends MediaDataSource {
            @DexIgnore
            public /* final */ /* synthetic */ ByteBuffer a;

            @DexIgnore
            public a(d dVar, ByteBuffer byteBuffer) {
                this.a = byteBuffer;
            }

            @DexIgnore
            @Override // java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public long getSize() {
                return (long) this.a.limit();
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public int readAt(long j, byte[] bArr, int i, int i2) {
                if (j >= ((long) this.a.limit())) {
                    return -1;
                }
                this.a.position((int) j);
                int min = Math.min(i2, this.a.remaining());
                this.a.get(bArr, i, min);
                return min;
            }
        }

        @DexIgnore
        public void a(MediaMetadataRetriever mediaMetadataRetriever, ByteBuffer byteBuffer) {
            mediaMetadataRetriever.setDataSource(new a(this, byteBuffer));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }

    @DexIgnore
    public interface f<T> {
        @DexIgnore
        void a(MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements f<ParcelFileDescriptor> {
        @DexIgnore
        public void a(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    @DexIgnore
    public h20(dz dzVar, f<T> fVar) {
        this(dzVar, fVar, f);
    }

    @DexIgnore
    public static cx<AssetFileDescriptor, Bitmap> a(dz dzVar) {
        return new h20(dzVar, new c(null));
    }

    @DexIgnore
    public static cx<ByteBuffer, Bitmap> b(dz dzVar) {
        return new h20(dzVar, new d());
    }

    @DexIgnore
    public static cx<ParcelFileDescriptor, Bitmap> c(dz dzVar) {
        return new h20(dzVar, new g());
    }

    @DexIgnore
    @Override // com.fossil.cx
    public boolean a(T t, ax axVar) {
        return true;
    }

    @DexIgnore
    public h20(dz dzVar, f<T> fVar, e eVar) {
        this.b = dzVar;
        this.a = fVar;
        this.c = eVar;
    }

    @DexIgnore
    @TargetApi(27)
    public static Bitmap b(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, r10 r10) {
        try {
            int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                parseInt2 = parseInt;
                parseInt = parseInt2;
            }
            float b2 = r10.b(parseInt, parseInt2, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, Math.round(((float) parseInt) * b2), Math.round(b2 * ((float) parseInt2)));
        } catch (Throwable th) {
            if (!Log.isLoggable("VideoDecoder", 3)) {
                return null;
            }
            Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: com.fossil.ax */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.cx
    public uy<Bitmap> a(T t, int i, int i2, ax axVar) throws IOException {
        long longValue = ((Long) axVar.a(d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) axVar.a(e);
            if (num == null) {
                num = 2;
            }
            r10 r10 = (r10) axVar.a(r10.f);
            if (r10 == null) {
                r10 = r10.e;
            }
            MediaMetadataRetriever a2 = this.c.a();
            try {
                this.a.a(a2, t);
                Bitmap a3 = a(a2, longValue, num.intValue(), i, i2, r10);
                a2.release();
                return k10.a(a3, this.b);
            } catch (RuntimeException e2) {
                throw new IOException(e2);
            } catch (Throwable th) {
                a2.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    @DexIgnore
    public static Bitmap a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, r10 r10) {
        Bitmap b2 = (Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || r10 == r10.d) ? null : b(mediaMetadataRetriever, j, i, i2, i3, r10);
        return b2 == null ? a(mediaMetadataRetriever, j, i) : b2;
    }

    @DexIgnore
    public static Bitmap a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }
}
