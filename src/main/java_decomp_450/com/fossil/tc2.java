package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc2 implements Parcelable.Creator<uc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uc2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 1) {
                j72.v(parcel, a);
            } else {
                str = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new uc2(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uc2[] newArray(int i) {
        return new uc2[i];
    }
}
