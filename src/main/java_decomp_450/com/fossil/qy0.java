package com.fossil;

import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy0<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Vector<h01<T>> b; // = new Vector<>();

    @DexIgnore
    public final synchronized void a(h01<T> h01) {
        if (!this.b.contains(h01)) {
            this.b.addElement(h01);
        }
    }

    @DexIgnore
    public final synchronized void b(h01<T> h01) {
        this.b.removeElement(h01);
    }

    @DexIgnore
    public final synchronized void c() {
        this.a = true;
    }

    @DexIgnore
    public final synchronized boolean b() {
        return this.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r1 = ((com.fossil.h01[]) r0.element).length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        r1 = r1 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r1 < 0) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        ((com.fossil.eo0) ((com.fossil.h01[]) r0.element)[r1]).a((java.lang.Object) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r4) {
        /*
            r3 = this;
            com.fossil.se7 r0 = new com.fossil.se7
            r0.<init>()
            monitor-enter(r3)
            boolean r1 = r3.b()     // Catch:{ all -> 0x0043 }
            if (r1 != 0) goto L_0x000e
            monitor-exit(r3)
            return
        L_0x000e:
            java.util.Vector<com.fossil.h01<T>> r1 = r3.b
            r2 = 0
            com.fossil.h01[] r2 = new com.fossil.h01[r2]
            java.lang.Object[] r1 = r1.toArray(r2)
            if (r1 == 0) goto L_0x003b
            com.fossil.h01[] r1 = (com.fossil.h01[]) r1
            r0.element = r1
            r3.a()
            com.fossil.i97 r1 = com.fossil.i97.a
            monitor-exit(r3)
            if (r4 == 0) goto L_0x003a
            T r1 = r0.element
            com.fossil.h01[] r1 = (com.fossil.h01[]) r1
            int r1 = r1.length
        L_0x002a:
            int r1 = r1 + -1
            if (r1 < 0) goto L_0x003a
            T r2 = r0.element
            com.fossil.h01[] r2 = (com.fossil.h01[]) r2
            r2 = r2[r1]
            com.fossil.eo0 r2 = (com.fossil.eo0) r2
            r2.a(r4)
            goto L_0x002a
        L_0x003a:
            return
        L_0x003b:
            com.fossil.x87 r4 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
            r4.<init>(r0)
            throw r4
        L_0x0043:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qy0.a(java.lang.Object):void");
    }

    @DexIgnore
    public final synchronized void a() {
        this.a = false;
    }
}
