package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qh0 extends qj1 {
    @DexIgnore
    public int A;

    @DexIgnore
    public qh0(ri1 ri1) {
        super(qa1.h, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.A = ((w71) eo0).j;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.b, Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.b, Integer.valueOf(this.A));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new w71(((v81) this).y.w);
    }
}
