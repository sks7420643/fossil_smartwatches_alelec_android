package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.be5;
import com.fossil.cc5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p76 extends go5 implements o76 {
    @DexIgnore
    public qw6<qw4> f;
    @DexIgnore
    public n76 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.o76
    public void b(do5 do5, ArrayList<String> arrayList) {
        qw4 a2;
        OverviewDayChart overviewDayChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + do5);
        qw6<qw4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayChart.a(do5);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        qw4 a2;
        OverviewDayChart overviewDayChart;
        qw6<qw4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            be5.a aVar = be5.o;
            n76 n76 = this.g;
            if (aVar.a(n76 != null ? n76.h() : null)) {
                overviewDayChart.a("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        qw4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        qw4 qw4 = (qw4) qb.a(layoutInflater, 2131558493, viewGroup, false, a1());
        qw4.r.setOnClickListener(b.a);
        this.f = new qw6<>(this, qw4);
        f1();
        qw6<qw4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        f1();
        n76 n76 = this.g;
        if (n76 != null) {
            n76.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        n76 n76 = this.g;
        if (n76 != null) {
            n76.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.o76
    public void a(boolean z, List<WorkoutSession> list) {
        qw4 a2;
        View d;
        View d2;
        ee7.b(list, "workoutSessions");
        qw6<qw4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    w75 w75 = a2.t;
                    if (w75 != null && (d2 = w75.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                w75 w752 = a2.t;
                if (!(w752 == null || (d = w752.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            ee7.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(n76 n76) {
        ee7.b(n76, "presenter");
        this.g = n76;
    }

    @DexIgnore
    public final void a(w75 w75, WorkoutSession workoutSession) {
        String str;
        String str2;
        AppCompatImageView appCompatImageView;
        if (w75 != null) {
            View d = w75.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            cc5.a aVar = cc5.Companion;
            r87<Integer, Integer> a2 = aVar.a(aVar.a(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String a3 = ig5.a(context, a2.getSecond().intValue());
            w75.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = w75.r;
            ee7.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            FossilDeviceSerialPatternUtil.DEVICE device = null;
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    v87<Integer, Integer, Integer> d2 = zd5.d((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                    ee7.a((Object) d2, "DateHelper.getTimeValues(duration.toInt())");
                    FlexibleTextView flexibleTextView2 = w75.s;
                    ee7.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
                    if (ee7.a(d2.getFirst().intValue(), 0) > 0) {
                        we7 we7 = we7.a;
                        String a4 = ig5.a(context, 2131886652);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                        str = String.format(a4, Arrays.copyOf(new Object[]{d2.getFirst(), d2.getSecond(), d2.getThird()}, 3));
                        ee7.a((Object) str, "java.lang.String.format(format, *args)");
                    } else if (ee7.a(d2.getSecond().intValue(), 0) > 0) {
                        we7 we72 = we7.a;
                        String a5 = ig5.a(context, 2131886653);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                        str = String.format(a5, Arrays.copyOf(new Object[]{d2.getSecond(), d2.getThird()}, 2));
                        ee7.a((Object) str, "java.lang.String.format(format, *args)");
                    } else {
                        we7 we73 = we7.a;
                        String a6 = ig5.a(context, 2131886654);
                        ee7.a((Object) a6, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                        str = String.format(a6, Arrays.copyOf(new Object[]{d2.getThird()}, 1));
                        ee7.a((Object) str, "java.lang.String.format(format, *args)");
                    }
                    flexibleTextView2.setText(str);
                    FlexibleTextView flexibleTextView3 = w75.q;
                    ee7.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
                    flexibleTextView3.setText(zd5.a(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
                    be5.a aVar2 = be5.o;
                    n76 n76 = this.g;
                    if (n76 != null) {
                        device = n76.h();
                    }
                    if (aVar2.a(device)) {
                        str2 = eh5.l.a().b("dianaActiveMinutesTab");
                    } else {
                        str2 = eh5.l.a().b("hybridActiveMinutesTab");
                    }
                    if (str2 != null && (appCompatImageView = w75.t) != null) {
                        appCompatImageView.setColorFilter(Color.parseColor(str2));
                        return;
                    }
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }
}
