package com.fossil;

import android.view.View;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface d06 extends io5<c06> {
    @DexIgnore
    void a(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2);

    @DexIgnore
    void a(List<dz5> list, DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b(int i);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void d(boolean z, boolean z2);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void k();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void w();
}
