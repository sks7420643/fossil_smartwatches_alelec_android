package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk0 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public kk0(ri1 ri1) {
        this.a = ri1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == -1099894406 && action.equals("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED")) {
            p91 a2 = p91.f.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", 0));
            p91 a3 = p91.f.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", 0));
            if (ee7.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE"), this.a.x)) {
                wl0.h.a(new wr1("hid_connection_state_changed", ci1.g, this.a.u, "", "", true, null, null, null, yz0.a(yz0.a(new JSONObject(), r51.A0, yz0.a(a2)), r51.z0, yz0.a(a3)), 448));
                this.a.a(a2, a3);
            }
        }
    }
}
