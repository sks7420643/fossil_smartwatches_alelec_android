package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b45 extends a45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        z.put(2131362517, 2);
        z.put(2131362281, 3);
        z.put(2131362653, 4);
        z.put(2131362751, 5);
        z.put(2131362978, 6);
    }
    */

    @DexIgnore
    public b45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 7, y, z));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public b45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[2], (ImageView) objArr[1], (ImageView) objArr[4], (View) objArr[5], (ConstraintLayout) objArr[0], (RecyclerView) objArr[6]);
        this.x = -1;
        ((a45) this).v.setTag(null);
        a(view);
        f();
    }
}
