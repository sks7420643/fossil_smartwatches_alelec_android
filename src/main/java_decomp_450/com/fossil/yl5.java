package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl5 extends fl4<c, e, d> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ b f; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return yl5.g;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = yl5.h.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + yl5.this.d() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.FORCE_CONNECT && yl5.this.d()) {
                boolean z = false;
                yl5.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    yl5.this.a(new e());
                    return;
                }
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                yl5.this.a(new d(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            ee7.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public d(int i, int i2, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase", f = "ReconnectDeviceUseCase.kt", l = {58}, m = "run")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(yl5 yl5, fb7 fb7) {
            super(fb7);
            this.this$0 = yl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        String simpleName = yl5.class.getSimpleName();
        ee7.a((Object) simpleName, "ReconnectDeviceUseCase::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return g;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final void e() {
        nj5.d.a(this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void f() {
        nj5.d.b(this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.yl5.c r6, com.fossil.fb7<java.lang.Object> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.yl5.f
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.yl5$f r0 = (com.fossil.yl5.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.yl5$f r0 = new com.fossil.yl5$f
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            java.lang.Object r6 = r0.L$1
            com.fossil.yl5$c r6 = (com.fossil.yl5.c) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.yl5 r6 = (com.fossil.yl5) r6
            com.fossil.t87.a(r7)     // Catch:{ Exception -> 0x0031 }
            goto L_0x008c
        L_0x0031:
            r6 = move-exception
            goto L_0x0066
        L_0x0033:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003b:
            com.fossil.t87.a(r7)
            r5.e = r3
            r7 = 0
            if (r6 == 0) goto L_0x0048
            java.lang.String r2 = r6.a()
            goto L_0x0049
        L_0x0048:
            r2 = r7
        L_0x0049:
            r5.d = r2
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r4 = r5.d
            if (r4 == 0) goto L_0x0062
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r6 = r2.b(r4, r0)
            if (r6 != r1) goto L_0x008c
            return r1
        L_0x0062:
            com.fossil.ee7.a()
            throw r7
        L_0x0066:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = com.fossil.yl5.g
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error inside "
            r1.append(r2)
            java.lang.String r2 = com.fossil.yl5.g
            r1.append(r2)
            java.lang.String r2 = ".connectDevice - e="
            r1.append(r2)
            r1.append(r6)
            java.lang.String r6 = r1.toString()
            r7.e(r0, r6)
        L_0x008c:
            java.lang.Object r6 = new java.lang.Object
            r6.<init>()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yl5.a(com.fossil.yl5$c, com.fossil.fb7):java.lang.Object");
    }
}
