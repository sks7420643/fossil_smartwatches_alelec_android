package com.fossil;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de4 extends JsonElement implements Iterable<JsonElement> {
    @DexIgnore
    public /* final */ List<JsonElement> a; // = new ArrayList();

    @DexIgnore
    public void a(String str) {
        this.a.add(str == null ? he4.a : new le4(str));
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public int b() {
        if (this.a.size() == 1) {
            return this.a.get(0).b();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof de4) && ((de4) obj).a.equals(this.a));
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public String f() {
        if (this.a.size() == 1) {
            return this.a.get(0).f();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public JsonElement get(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<JsonElement> iterator() {
        return this.a.iterator();
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public void a(JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = he4.a;
        }
        this.a.add(jsonElement);
    }

    @DexIgnore
    @Override // com.google.gson.JsonElement
    public boolean a() {
        if (this.a.size() == 1) {
            return this.a.get(0).a();
        }
        throw new IllegalStateException();
    }
}
