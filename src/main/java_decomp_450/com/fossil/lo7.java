package com.fossil;

import com.fossil.fo7;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo7 {
    @DexIgnore
    public /* final */ go7 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ fo7 c;
    @DexIgnore
    public /* final */ RequestBody d;
    @DexIgnore
    public /* final */ Map<Class<?>, Object> e;
    @DexIgnore
    public volatile pn7 f;

    @DexIgnore
    public lo7(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c.a();
        this.d = aVar.d;
        this.e = ro7.a(aVar.e);
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }

    @DexIgnore
    public List<String> b(String str) {
        return this.c.b(str);
    }

    @DexIgnore
    public fo7 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.h();
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public a f() {
        return new a(this);
    }

    @DexIgnore
    public go7 g() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.a + ", tags=" + this.e + '}';
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public go7 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public fo7.a c;
        @DexIgnore
        public RequestBody d;
        @DexIgnore
        public Map<Class<?>, Object> e;

        @DexIgnore
        public a() {
            this.e = Collections.emptyMap();
            this.b = "GET";
            this.c = new fo7.a();
        }

        @DexIgnore
        public a a(go7 go7) {
            if (go7 != null) {
                this.a = go7;
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                a(go7.d(str));
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.c.c(str);
            return this;
        }

        @DexIgnore
        public a(lo7 lo7) {
            Map<Class<?>, Object> map;
            this.e = Collections.emptyMap();
            this.a = lo7.a;
            this.b = lo7.b;
            this.d = lo7.d;
            if (lo7.e.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = new LinkedHashMap<>(lo7.e);
            }
            this.e = map;
            this.c = lo7.c.a();
        }

        @DexIgnore
        public a a(fo7 fo7) {
            this.c = fo7.a();
            return this;
        }

        @DexIgnore
        public a a(pn7 pn7) {
            String pn72 = pn7.toString();
            if (pn72.isEmpty()) {
                a(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
                return this;
            }
            b(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, pn72);
            return this;
        }

        @DexIgnore
        public a b(String str, String str2) {
            this.c.d(str, str2);
            return this;
        }

        @DexIgnore
        public a b() {
            a("GET", (RequestBody) null);
            return this;
        }

        @DexIgnore
        public a a(RequestBody requestBody) {
            a("POST", requestBody);
            return this;
        }

        @DexIgnore
        public a a(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !lp7.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !lp7.e(str)) {
                this.b = str;
                this.d = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public <T> a a(Class<? super T> cls, T t) {
            if (cls != null) {
                if (t == null) {
                    this.e.remove(cls);
                } else {
                    if (this.e.isEmpty()) {
                        this.e = new LinkedHashMap();
                    }
                    this.e.put(cls, cls.cast(t));
                }
                return this;
            }
            throw new NullPointerException("type == null");
        }

        @DexIgnore
        public lo7 a() {
            if (this.a != null) {
                return new lo7(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    @DexIgnore
    public RequestBody a() {
        return this.d;
    }

    @DexIgnore
    public pn7 b() {
        pn7 pn7 = this.f;
        if (pn7 != null) {
            return pn7;
        }
        pn7 a2 = pn7.a(this.c);
        this.f = a2;
        return a2;
    }

    @DexIgnore
    public <T> T a(Class<? extends T> cls) {
        return (T) cls.cast(this.e.get(cls));
    }
}
