package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public mk5 a;
    @DexIgnore
    public ch5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ se7 $notificationInfo;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(se7 se7, fb7 fb7) {
            super(2, fb7);
            this.$notificationInfo = se7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$notificationInfo, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.g0.c());
                if (defaultSmsPackage == null || nx6.b.a(defaultSmsPackage)) {
                    FLogger.INSTANCE.getLocal().d(ui5.c, "onReceive() - SMS app filter is assigned - ignore");
                } else {
                    sg5.i.a().a((NotificationInfo) this.$notificationInfo.element);
                    FLogger.INSTANCE.getLocal().d(ui5.c, "onReceive) - SMS app filter is not assigned - add to Queue");
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ui5.class.getSimpleName();
        ee7.a((Object) simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        rf5 a2;
        ee7.b(context, "context");
        ee7.b(intent, "intent");
        se7 se7 = new se7();
        se7.element = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        StringBuilder sb = new StringBuilder();
        sb.append("SmsMmsReceiver : ");
        String action = intent.getAction();
        if (action != null) {
            sb.append(action);
            local.d(str, sb.toString());
            ch5 ch5 = this.b;
            if (ch5 == null) {
                ee7.d("mSharePref");
                throw null;
            } else if (ch5.S()) {
                FLogger.INSTANCE.getLocal().d(c, "SmsMmsReceiver user new solution");
            } else {
                String str2 = "";
                if (TextUtils.isEmpty(intent.getAction()) || !ee7.a((Object) intent.getAction(), (Object) "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
                    try {
                        SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        if (messagesFromIntent != null) {
                            if (!(messagesFromIntent.length == 0)) {
                                for (SmsMessage smsMessage : messagesFromIntent) {
                                    ee7.a((Object) smsMessage, "message");
                                    String messageBody = smsMessage.getMessageBody();
                                    String originatingAddress = smsMessage.getOriginatingAddress();
                                    se7.element = (T) new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, str2);
                                    if (!TextUtils.isEmpty(messageBody) && !TextUtils.isEmpty(originatingAddress)) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
                    }
                } else {
                    try {
                        if (intent.hasExtra("data") && (a2 = new yf5(intent.getByteArrayExtra("data")).a()) != null && a2.a() != null) {
                            qf5 a3 = a2.a();
                            ee7.a((Object) a3, "pdu.from");
                            String d = a3.d();
                            FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + d);
                            se7.element = (T) new NotificationInfo(NotificationSource.TEXT, d, str2, str2);
                        }
                    } catch (Exception e2) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
                    }
                }
                if (se7.element != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onReceive() - SMSMMS - sender info ");
                    String senderInfo = se7.element.getSenderInfo();
                    if (senderInfo != null) {
                        sb2.append(senderInfo);
                        local2.d(str3, sb2.toString());
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.g0.c().c())) {
                            ch5 ch52 = this.b;
                            if (ch52 == null) {
                                ee7.d("mSharePref");
                                throw null;
                            } else if (ch52.I()) {
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
                            } else {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str4 = c;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("onReceive() - SMS-MMS - sender info ");
                                String senderInfo2 = se7.element.getSenderInfo();
                                if (senderInfo2 != null) {
                                    sb3.append(senderInfo2);
                                    local3.d(str4, sb3.toString());
                                    ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new b(se7, null), 3, null);
                                    return;
                                }
                                ee7.a();
                                throw null;
                            }
                        } else {
                            if (se7.element.getSenderInfo() != null) {
                                str2 = se7.element.getSenderInfo();
                            }
                            mk5 mk5 = this.a;
                            if (mk5 == null) {
                                ee7.d("mDianaNotificationComponent");
                                throw null;
                            } else if (str2 != null) {
                                String body = se7.element.getBody();
                                ee7.a((Object) body, "notificationInfo.body");
                                Calendar instance = Calendar.getInstance();
                                ee7.a((Object) instance, "Calendar.getInstance()");
                                mk5.a(str2, body, instance.getTimeInMillis());
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
