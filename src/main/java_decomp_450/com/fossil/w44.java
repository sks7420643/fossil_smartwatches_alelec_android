package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w44 {
    @DexIgnore
    void a();

    @DexIgnore
    void a(long j, String str);

    @DexIgnore
    String b();

    @DexIgnore
    byte[] c();

    @DexIgnore
    void d();
}
