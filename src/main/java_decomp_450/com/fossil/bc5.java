package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bc5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;

    /*
    static {
        int[] iArr = new int[ub5.values().length];
        a = iArr;
        iArr[ub5.INDOOR.ordinal()] = 1;
        a[ub5.OUTDOOR.ordinal()] = 2;
        int[] iArr2 = new int[ac5.values().length];
        b = iArr2;
        iArr2[ac5.UNKNOWN.ordinal()] = 1;
        b[ac5.RUNNING.ordinal()] = 2;
        b[ac5.CYCLING.ordinal()] = 3;
        b[ac5.SPINNING.ordinal()] = 4;
        b[ac5.TREADMILL.ordinal()] = 5;
        b[ac5.ELLIPTICAL.ordinal()] = 6;
        b[ac5.WEIGHTS.ordinal()] = 7;
        b[ac5.WORKOUT.ordinal()] = 8;
        b[ac5.YOGA.ordinal()] = 9;
        b[ac5.WALKING.ordinal()] = 10;
        b[ac5.ROWING.ordinal()] = 11;
        b[ac5.SWIMMING.ordinal()] = 12;
        b[ac5.AEROBIC.ordinal()] = 13;
        b[ac5.HIKING.ordinal()] = 14;
        int[] iArr3 = new int[cc5.values().length];
        c = iArr3;
        iArr3[cc5.UNKNOWN.ordinal()] = 1;
        c[cc5.RUNNING.ordinal()] = 2;
        c[cc5.SPINNING.ordinal()] = 3;
        c[cc5.OUTDOOR_CYCLING.ordinal()] = 4;
        c[cc5.TREADMILL.ordinal()] = 5;
        c[cc5.ELLIPTICAL.ordinal()] = 6;
        c[cc5.WEIGHTS.ordinal()] = 7;
        c[cc5.WORKOUT.ordinal()] = 8;
        c[cc5.WALK.ordinal()] = 9;
        c[cc5.ROW_MACHINE.ordinal()] = 10;
        c[cc5.HIKING.ordinal()] = 11;
        int[] iArr4 = new int[cc5.values().length];
        d = iArr4;
        iArr4[cc5.UNKNOWN.ordinal()] = 1;
        d[cc5.SPINNING.ordinal()] = 2;
        d[cc5.OUTDOOR_CYCLING.ordinal()] = 3;
        d[cc5.TREADMILL.ordinal()] = 4;
        d[cc5.ELLIPTICAL.ordinal()] = 5;
        d[cc5.WEIGHTS.ordinal()] = 6;
        d[cc5.WORKOUT.ordinal()] = 7;
        d[cc5.ROW_MACHINE.ordinal()] = 8;
        int[] iArr5 = new int[cc5.values().length];
        e = iArr5;
        iArr5[cc5.RUNNING.ordinal()] = 1;
        e[cc5.SPINNING.ordinal()] = 2;
        e[cc5.OUTDOOR_CYCLING.ordinal()] = 3;
        e[cc5.TREADMILL.ordinal()] = 4;
        e[cc5.ELLIPTICAL.ordinal()] = 5;
        e[cc5.WEIGHTS.ordinal()] = 6;
        e[cc5.WORKOUT.ordinal()] = 7;
        e[cc5.WALK.ordinal()] = 8;
        e[cc5.ROW_MACHINE.ordinal()] = 9;
        e[cc5.HIKING.ordinal()] = 10;
        e[cc5.UNKNOWN.ordinal()] = 11;
    }
    */
}
