package com.fossil;

import java.io.File;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc1 implements ru7<String> {
    @DexIgnore
    public /* final */ /* synthetic */ gg1 a;
    @DexIgnore
    public /* final */ /* synthetic */ File b;

    @DexIgnore
    public sc1(gg1 gg1, File file) {
        this.a = gg1;
        this.b = file;
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onFailure(Call<String> call, Throwable th) {
        t11 t11 = t11.a;
        th.getMessage();
        this.a.b();
    }

    @DexIgnore
    @Override // com.fossil.ru7
    public void onResponse(Call<String> call, fv7<String> fv7) {
        t11 t11 = t11.a;
        fv7.e();
        fv7.b();
        fv7.a();
        mo7 c = fv7.c();
        if (c != null) {
            c.string();
        }
        int b2 = fv7.b();
        if ((200 <= b2 && 299 >= b2) || b2 == 400 || b2 == 413) {
            this.a.c.remove(this.b);
            this.b.delete();
            this.a.c();
            return;
        }
        this.a.b();
    }
}
