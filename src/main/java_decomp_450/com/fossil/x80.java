package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x80 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ short e; // = 0;
    @DexIgnore
    public static /* final */ short f; // = 255;
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public x80 createFromParcel(Parcel parcel) {
            x80 x80 = new x80(yz0.b(parcel.readByte()), yz0.b(parcel.readByte()), yz0.b(parcel.readByte()), yz0.b(parcel.readByte()));
            x80.b();
            return x80;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public x80[] newArray(int i) {
            return new x80[i];
        }
    }

    /*
    static {
        ud7 ud7 = ud7.a;
    }
    */

    @DexIgnore
    public x80(short s, short s2, short s3, short s4) throws IllegalArgumentException {
        this.a = s;
        this.b = s2;
        this.c = s3;
        this.d = s4;
        b();
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("start_latency", Short.valueOf(this.a)).put("pause_latency", Short.valueOf(this.b)).put("resume_latency", Short.valueOf(this.c)).put("stop_latency", Short.valueOf(this.d));
        ee7.a((Object) put, "JSONObject()\n           \u2026cy\", stopLatencyInMinute)");
        return put;
    }

    @DexIgnore
    public final void b() throws IllegalArgumentException {
        short s = e;
        short s2 = f;
        short s3 = this.a;
        boolean z = true;
        if (s <= s3 && s2 >= s3) {
            short s4 = e;
            short s5 = f;
            short s6 = this.b;
            if (s4 <= s6 && s5 >= s6) {
                short s7 = e;
                short s8 = f;
                short s9 = this.c;
                if (s7 <= s9 && s8 >= s9) {
                    short s10 = e;
                    short s11 = f;
                    short s12 = this.d;
                    if (s10 > s12 || s11 < s12) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder b2 = yh0.b("stop latency (");
                        b2.append((int) this.a);
                        b2.append(") is out of range ");
                        b2.append('[');
                        b2.append((int) e);
                        b2.append(", ");
                        throw new IllegalArgumentException(yh0.a(b2, f, "]."));
                    }
                    return;
                }
                StringBuilder b3 = yh0.b("resume latency (");
                b3.append((int) this.a);
                b3.append(") is out of range ");
                b3.append('[');
                b3.append((int) e);
                b3.append(", ");
                throw new IllegalArgumentException(yh0.a(b3, f, "]."));
            }
            StringBuilder b4 = yh0.b("pause latency (");
            b4.append((int) this.a);
            b4.append(") is out of range ");
            b4.append('[');
            b4.append((int) e);
            b4.append(", ");
            throw new IllegalArgumentException(yh0.a(b4, f, "]."));
        }
        StringBuilder b5 = yh0.b("start latency (");
        b5.append((int) this.a);
        b5.append(") is out of range ");
        b5.append('[');
        b5.append((int) e);
        b5.append(", ");
        throw new IllegalArgumentException(yh0.a(b5, f, "]."));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(x80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x80 x80 = (x80) obj;
            return this.a == x80.a && this.b == x80.b && this.c == x80.c && this.d == x80.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.ActivityDetectionLatency");
    }

    @DexIgnore
    public final short getPauseLatencyInMinute() {
        return this.b;
    }

    @DexIgnore
    public final short getResumeLatencyInMinute() {
        return this.c;
    }

    @DexIgnore
    public final short getStartLatencyInMinute() {
        return this.a;
    }

    @DexIgnore
    public final short getStopLatencyInMinute() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte((byte) this.a);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.b);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.c);
        }
        if (parcel != null) {
            parcel.writeByte((byte) this.d);
        }
    }
}
