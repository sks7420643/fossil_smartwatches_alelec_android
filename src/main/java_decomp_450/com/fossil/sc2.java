package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc2 implements Parcelable.Creator<mc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ mc2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        Bundle bundle = null;
        int[] iArr = null;
        float[] fArr = null;
        byte[] bArr = null;
        int i = 0;
        boolean z = false;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    z = j72.i(parcel, a);
                    break;
                case 3:
                    f = j72.n(parcel, a);
                    break;
                case 4:
                    str = j72.e(parcel, a);
                    break;
                case 5:
                    bundle = j72.a(parcel, a);
                    break;
                case 6:
                    iArr = j72.d(parcel, a);
                    break;
                case 7:
                    fArr = j72.c(parcel, a);
                    break;
                case 8:
                    bArr = j72.b(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new mc2(i, z, f, str, bundle, iArr, fArr, bArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ mc2[] newArray(int i) {
        return new mc2[i];
    }
}
