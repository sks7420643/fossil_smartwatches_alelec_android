package com.fossil;

import com.fossil.ib7;
import com.fossil.ib7.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class db7<B extends ib7.b, E extends B> implements ib7.c<E> {
    @DexIgnore
    public /* final */ ib7.c<?> a;
    @DexIgnore
    public /* final */ gd7<ib7.b, E> b;

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: com.fossil.ib7$c<?> */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.fossil.gd7<? super com.fossil.ib7$b, ? extends E extends B>, com.fossil.gd7<com.fossil.ib7$b, E extends B>, java.lang.Object] */
    public db7(ib7.c<B> cVar, gd7<? super ib7.b, ? extends E> gd7) {
        ee7.b(cVar, "baseKey");
        ee7.b(gd7, "safeCast");
        this.b = gd7;
        this.a = cVar instanceof db7 ? (ib7.c<B>) ((db7) cVar).a : cVar;
    }

    @DexIgnore
    public final E a(ib7.b bVar) {
        ee7.b(bVar, "element");
        return this.b.invoke(bVar);
    }

    @DexIgnore
    public final boolean a(ib7.c<?> cVar) {
        ee7.b(cVar, "key");
        return cVar == this || this.a == cVar;
    }
}
