package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ String a; // = eh5.l.a().b("onPrimaryButton");
    @DexIgnore
    public ArrayList<dz5> b;
    @DexIgnore
    public DianaComplicationRingStyle c;
    @DexIgnore
    public d d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ zz5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zz5 zz5, View view) {
            super(view);
            ee7.b(view, "view");
            this.c = zz5;
            View findViewById = view.findViewById(2131361960);
            if (findViewById != null) {
                this.a = (FlexibleButton) findViewById;
                View findViewById2 = view.findViewById(2131363239);
                ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
                this.b = (TextView) findViewById2;
                if (!TextUtils.isEmpty(zz5.c())) {
                    int parseColor = Color.parseColor(zz5.c());
                    Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230982);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        this.a.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                bf5.a(this.a, this);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleButton");
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.a.a("flexible_button_disabled");
                this.a.setClickable(false);
                this.b.setText(PortfolioApp.g0.c().getString(2131886548));
                return;
            }
            this.a.a("flexible_button_primary");
            this.a.setClickable(true);
            this.b.setText(PortfolioApp.g0.c().getString(2131886535));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == 2131361960) {
                this.c.e().y();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void a(String str, String str2);

        @DexIgnore
        void b(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2);

        @DexIgnore
        void b(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void x();

        @DexIgnore
        void y();
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public zz5(ArrayList<dz5> arrayList, DianaComplicationRingStyle dianaComplicationRingStyle, d dVar) {
        ee7.b(arrayList, "mData");
        ee7.b(dVar, "mListener");
        this.b = arrayList;
        this.c = dianaComplicationRingStyle;
        this.d = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final DianaComplicationRingStyle d() {
        return this.c;
    }

    @DexIgnore
    public final d e() {
        return this.d;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) this.b.get(i).hashCode();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (i == this.b.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizePresetDetailAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            dz5 dz5 = this.b.get(i);
            ee7.a((Object) dz5, "mData[position]");
            dz5 dz52 = dz5;
            if (i != 0) {
                z = false;
            }
            bVar.a(dz52, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.b.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558680, viewGroup, false);
            ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558672, viewGroup, false);
        ee7.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(List<dz5> list, DianaComplicationRingStyle dianaComplicationRingStyle) {
        ee7.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizePresetDetailAdapter", "setData - data=" + list);
        this.c = dianaComplicationRingStyle;
        this.b.clear();
        this.b.addAll(list);
        this.b.add(new dz5("", "", new ArrayList(), new ArrayList(), new ArrayList(), false, null, 64, null));
        notifyDataSetChanged();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public dz5 A;
        @DexIgnore
        public /* final */ /* synthetic */ zz5 B;
        @DexIgnore
        public TextView a;
        @DexIgnore
        public CustomizeWidget b;
        @DexIgnore
        public CustomizeWidget c;
        @DexIgnore
        public CustomizeWidget d;
        @DexIgnore
        public CustomizeWidget e;
        @DexIgnore
        public View f;
        @DexIgnore
        public View g;
        @DexIgnore
        public View h;
        @DexIgnore
        public ImageView i;
        @DexIgnore
        public ImageView j;
        @DexIgnore
        public CustomizeWidget p;
        @DexIgnore
        public CustomizeWidget q;
        @DexIgnore
        public CustomizeWidget r;
        @DexIgnore
        public ImageButton s;
        @DexIgnore
        public FlexibleTextView t;
        @DexIgnore
        public View u;
        @DexIgnore
        public FlexibleButton v;
        @DexIgnore
        public View w;
        @DexIgnore
        public View x;
        @DexIgnore
        public View y;
        @DexIgnore
        public View z;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zz5 zz5, View view) {
            super(view);
            ee7.b(view, "view");
            this.B = zz5;
            View findViewById = view.findViewById(2131363310);
            ee7.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            TextView textView = (TextView) findViewById;
            this.a = textView;
            textView.setOnClickListener(this);
            View findViewById2 = view.findViewById(2131362736);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.iv_warning)");
            this.j = (ImageView) findViewById2;
            View findViewById3 = view.findViewById(2131362047);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.cl_complications)");
            this.f = findViewById3;
            View findViewById4 = view.findViewById(2131362087);
            ee7.a((Object) findViewById4, "view.findViewById(R.id.cl_preset_buttons)");
            this.g = findViewById4;
            View findViewById5 = view.findViewById(2131362737);
            ee7.a((Object) findViewById5, "view.findViewById(R.id.iv_watch_hands_background)");
            this.h = findViewById5;
            View findViewById6 = view.findViewById(2131362738);
            ee7.a((Object) findViewById6, "view.findViewById(R.id.iv_watch_theme_background)");
            this.i = (ImageView) findViewById6;
            View findViewById7 = view.findViewById(2131363463);
            ee7.a((Object) findViewById7, "view.findViewById(R.id.wc_top)");
            this.b = (CustomizeWidget) findViewById7;
            View findViewById8 = view.findViewById(2131363455);
            ee7.a((Object) findViewById8, "view.findViewById(R.id.wc_bottom)");
            this.c = (CustomizeWidget) findViewById8;
            View findViewById9 = view.findViewById(2131363462);
            ee7.a((Object) findViewById9, "view.findViewById(R.id.wc_start)");
            this.d = (CustomizeWidget) findViewById9;
            View findViewById10 = view.findViewById(2131363459);
            ee7.a((Object) findViewById10, "view.findViewById(R.id.wc_end)");
            this.e = (CustomizeWidget) findViewById10;
            View findViewById11 = view.findViewById(2131362564);
            ee7.a((Object) findViewById11, "view.findViewById(R.id.ib_edit_themes)");
            this.s = (ImageButton) findViewById11;
            View findViewById12 = view.findViewById(2131363401);
            ee7.a((Object) findViewById12, "view.findViewById(R.id.v_underline)");
            this.z = findViewById12;
            View findViewById13 = view.findViewById(2131363454);
            ee7.a((Object) findViewById13, "view.findViewById(R.id.wa_top)");
            this.p = (CustomizeWidget) findViewById13;
            View findViewById14 = view.findViewById(2131363453);
            ee7.a((Object) findViewById14, "view.findViewById(R.id.wa_middle)");
            this.q = (CustomizeWidget) findViewById14;
            View findViewById15 = view.findViewById(2131363452);
            ee7.a((Object) findViewById15, "view.findViewById(R.id.wa_bottom)");
            this.r = (CustomizeWidget) findViewById15;
            View findViewById16 = view.findViewById(2131362757);
            ee7.a((Object) findViewById16, "view.findViewById(R.id.line_bottom)");
            this.y = findViewById16;
            View findViewById17 = view.findViewById(2131362758);
            ee7.a((Object) findViewById17, "view.findViewById(R.id.line_center)");
            this.x = findViewById17;
            View findViewById18 = view.findViewById(2131362759);
            ee7.a((Object) findViewById18, "view.findViewById(R.id.line_top)");
            this.w = findViewById18;
            View findViewById19 = view.findViewById(2131362748);
            ee7.a((Object) findViewById19, "view.findViewById(R.id.layout_right)");
            this.u = findViewById19;
            View findViewById20 = view.findViewById(2131361971);
            ee7.a((Object) findViewById20, "view.findViewById(R.id.btn_right)");
            this.v = (FlexibleButton) findViewById20;
            View findViewById21 = view.findViewById(2131363283);
            ee7.a((Object) findViewById21, "view.findViewById(R.id.tv_left)");
            this.t = (FlexibleTextView) findViewById21;
            this.v.setOnClickListener(this);
            this.u.setOnClickListener(this);
            this.t.setOnClickListener(this);
            this.b.setOnClickListener(this);
            this.c.setOnClickListener(this);
            this.d.setOnClickListener(this);
            this.e.setOnClickListener(this);
            this.s.setOnClickListener(this);
            this.p.setOnClickListener(this);
            this.q.setOnClickListener(this);
            this.r.setOnClickListener(this);
            this.j.setOnClickListener(this);
        }

        @DexIgnore
        public final List<a9<View, String>> a() {
            a9[] a9VarArr = new a9[7];
            TextView textView = this.a;
            a9VarArr[0] = new a9(textView, textView.getTransitionName());
            View view = this.h;
            a9VarArr[1] = new a9(view, view.getTransitionName());
            ImageView imageView = this.i;
            if (imageView != null) {
                a9VarArr[2] = new a9(imageView, imageView.getTransitionName());
                View view2 = this.z;
                a9VarArr[3] = new a9(view2, view2.getTransitionName());
                View view3 = this.y;
                a9VarArr[4] = new a9(view3, view3.getTransitionName());
                View view4 = this.x;
                a9VarArr[5] = new a9(view4, view4.getTransitionName());
                View view5 = this.w;
                a9VarArr[6] = new a9(view5, view5.getTransitionName());
                return w97.c(a9VarArr);
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<a9<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.b;
            CustomizeWidget customizeWidget2 = this.c;
            CustomizeWidget customizeWidget3 = this.e;
            CustomizeWidget customizeWidget4 = this.d;
            CustomizeWidget customizeWidget5 = this.p;
            CustomizeWidget customizeWidget6 = this.q;
            CustomizeWidget customizeWidget7 = this.r;
            return w97.c(new a9(customizeWidget, customizeWidget.getTransitionName()), new a9(customizeWidget2, customizeWidget2.getTransitionName()), new a9(customizeWidget3, customizeWidget3.getTransitionName()), new a9(customizeWidget4, customizeWidget4.getTransitionName()), new a9(customizeWidget5, customizeWidget5.getTransitionName()), new a9(customizeWidget6, customizeWidget6.getTransitionName()), new a9(customizeWidget7, customizeWidget7.getTransitionName()));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.dz5 */
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.fossil.dz5 */
        /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: com.fossil.dz5 */
        /* JADX WARN: Multi-variable type inference failed */
        public void onClick(View view) {
            String str;
            String d2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizePresetDetailAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.A == null) && view != null) {
                Object obj = null;
                switch (view.getId()) {
                    case 2131361971:
                        this.B.e().x();
                        return;
                    case 2131362564:
                        this.B.e().b(this.A, a(), b());
                        return;
                    case 2131362736:
                        Iterator it = this.B.b.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Object next = it.next();
                                if (((dz5) next).b()) {
                                    obj = next;
                                }
                            }
                        }
                        dz5 dz5 = (dz5) obj;
                        if (dz5 != null) {
                            xg5 xg5 = xg5.b;
                            d e2 = this.B.e();
                            if (e2 != null) {
                                xg5.a(xg5, ((vr5) e2).getContext(), (List) dz5.c(), false, false, false, (Integer) null, 52, (Object) null);
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
                        }
                        return;
                    case 2131363283:
                        if (this.B.b.size() > 2) {
                            Object obj2 = this.B.b.get(1);
                            ee7.a(obj2, "mData[1]");
                            dz5 dz52 = (dz5) obj2;
                            d e3 = this.B.e();
                            dz5 dz53 = this.A;
                            if (dz53 != null) {
                                boolean b2 = dz53.b();
                                dz5 dz54 = this.A;
                                if (dz54 != null) {
                                    e3.b(b2, dz54.e(), dz52.e(), dz52.d());
                                    return;
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case 2131363310:
                        d e4 = this.B.e();
                        dz5 dz55 = this.A;
                        String str2 = "";
                        if (dz55 == null || (str = dz55.e()) == null) {
                            str = str2;
                        }
                        dz5 dz56 = this.A;
                        if (!(dz56 == null || (d2 = dz56.d()) == null)) {
                            str2 = d2;
                        }
                        e4.a(str, str2);
                        return;
                    case 2131363452:
                        this.B.e().a(this.A, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363453:
                        this.B.e().a(this.A, a(), b(), "middle", getAdapterPosition());
                        return;
                    case 2131363454:
                        this.B.e().a(this.A, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    case 2131363455:
                        this.B.e().b(this.A, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case 2131363459:
                        this.B.e().b(this.A, a(), b(), "right", getAdapterPosition());
                        return;
                    case 2131363462:
                        this.B.e().b(this.A, a(), b(), ViewHierarchy.DIMENSION_LEFT_KEY, getAdapterPosition());
                        return;
                    case 2131363463:
                        this.B.e().b(this.A, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:253:0x0146, code lost:
            continue;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(com.fossil.dz5 r14, boolean r15) {
            /*
                r13 = this;
                java.lang.String r0 = "preset"
                com.fossil.ee7.b(r14, r0)
                r0 = 1
                java.lang.String r1 = "DianaCustomizePresetDetailAdapter"
                r2 = 0
                r3 = 0
                if (r15 == 0) goto L_0x00c4
                com.fossil.xg5 r4 = com.fossil.xg5.b
                com.fossil.zz5 r15 = r13.B
                com.fossil.zz5$d r15 = r15.e()
                if (r15 == 0) goto L_0x00bc
                com.fossil.vr5 r15 = (com.fossil.vr5) r15
                android.content.Context r5 = r15.getContext()
                java.util.List r6 = r14.c()
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 0
                r11 = 56
                r12 = 0
                boolean r15 = com.fossil.xg5.a(r4, r5, r6, r7, r8, r9, r10, r11, r12)
                if (r15 == 0) goto L_0x0040
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r4 = "warning gone"
                r15.d(r1, r4)
                android.widget.ImageView r15 = r13.j
                r4 = 8
                r15.setVisibility(r4)
                goto L_0x0050
            L_0x0040:
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r4 = "warning visible"
                r15.d(r1, r4)
                android.widget.ImageView r15 = r13.j
                r15.setVisibility(r2)
            L_0x0050:
                android.view.View r15 = r13.u
                r15.setSelected(r0)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r4 = 2131886544(0x7f1201d0, float:1.940767E38)
                java.lang.String r0 = r0.getString(r4)
                r15.setText(r0)
                com.fossil.zz5 r15 = r13.B
                java.lang.String r15 = r15.c()
                boolean r15 = android.text.TextUtils.isEmpty(r15)
                if (r15 != 0) goto L_0x0096
                com.fossil.zz5 r15 = r13.B
                java.lang.String r15 = r15.c()
                int r15 = android.graphics.Color.parseColor(r15)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r4 = 2131231051(0x7f08014b, float:1.8078172E38)
                android.graphics.drawable.Drawable r0 = r0.getDrawable(r4)
                if (r0 == 0) goto L_0x0096
                r0.setTint(r15)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                r15.setCompoundDrawablesWithIntrinsicBounds(r0, r3, r3, r3)
                com.fossil.i97 r15 = com.fossil.i97.a
            L_0x0096:
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                java.lang.String r0 = "flexible_button_right_applied"
                r15.a(r0)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                r15.setClickable(r2)
                com.fossil.zz5 r15 = r13.B
                java.util.ArrayList r15 = r15.b
                int r15 = r15.size()
                r0 = 2
                if (r15 != r0) goto L_0x00b6
                com.portfolio.platform.view.FlexibleTextView r15 = r13.t
                r0 = 4
                r15.setVisibility(r0)
                goto L_0x00f1
            L_0x00b6:
                com.portfolio.platform.view.FlexibleTextView r15 = r13.t
                r15.setVisibility(r2)
                goto L_0x00f1
            L_0x00bc:
                com.fossil.x87 r14 = new com.fossil.x87
                java.lang.String r15 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment"
                r14.<init>(r15)
                throw r14
            L_0x00c4:
                android.view.View r15 = r13.u
                r15.setSelected(r2)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r5 = 2131886536(0x7f1201c8, float:1.9407654E38)
                java.lang.String r4 = r4.getString(r5)
                r15.setText(r4)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                r15.setCompoundDrawablesWithIntrinsicBounds(r2, r2, r2, r2)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                java.lang.String r4 = "flexible_button_right_apply"
                r15.a(r4)
                com.portfolio.platform.view.FlexibleButton r15 = r13.v
                r15.setClickable(r0)
                com.portfolio.platform.view.FlexibleTextView r15 = r13.t
                r15.setVisibility(r2)
            L_0x00f1:
                r13.A = r14
                android.widget.TextView r15 = r13.a
                java.lang.String r0 = r14.e()
                r15.setText(r0)
                java.util.ArrayList r15 = new java.util.ArrayList
                r15.<init>()
                java.util.ArrayList r0 = r14.a()
                r15.addAll(r0)
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                java.util.ArrayList r2 = r14.f()
                r0.addAll(r2)
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "build with complications="
                r4.append(r5)
                r4.append(r15)
                java.lang.String r5 = " watchapps="
                r4.append(r5)
                r4.append(r0)
                java.lang.String r5 = " mWatchFaceWrapper="
                r4.append(r5)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r5 = r14.g()
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r1, r4)
                java.util.Iterator r15 = r15.iterator()
            L_0x0146:
                boolean r1 = r15.hasNext()
                java.lang.String r2 = "top"
                java.lang.String r4 = "bottom"
                if (r1 == 0) goto L_0x0470
                java.lang.Object r1 = r15.next()
                com.fossil.fz5 r1 = (com.fossil.fz5) r1
                java.lang.String r5 = r1.c()
                if (r5 != 0) goto L_0x015d
                goto L_0x0146
            L_0x015d:
                int r6 = r5.hashCode()
                r7 = 2131230873(0x7f080099, float:1.8077811E38)
                switch(r6) {
                    case -1383228885: goto L_0x03af;
                    case 115029: goto L_0x02ee;
                    case 3317767: goto L_0x022b;
                    case 108511772: goto L_0x0168;
                    default: goto L_0x0167;
                }
            L_0x0167:
                goto L_0x0146
            L_0x0168:
                java.lang.String r2 = "right"
                boolean r2 = r5.equals(r2)
                if (r2 == 0) goto L_0x0146
                com.portfolio.platform.view.CustomizeWidget r2 = r13.e
                java.lang.String r4 = r1.a()
                r2.b(r4)
                com.portfolio.platform.view.CustomizeWidget r2 = r13.e
                java.lang.String r1 = r1.d()
                r2.setBottomContent(r1)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x0196
                android.graphics.drawable.Drawable r1 = r1.getRightComplication()
                if (r1 == 0) goto L_0x0196
                com.portfolio.platform.view.CustomizeWidget r2 = r13.e
                r2.setBackgroundDrawableCus(r1)
                com.fossil.i97 r1 = com.fossil.i97.a
                goto L_0x01a1
            L_0x0196:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.e
                java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
                r1.setBackgroundRes(r2)
                com.fossil.i97 r1 = com.fossil.i97.a
            L_0x01a1:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x01ac
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r1 = r1.getRightMetaData()
                goto L_0x01ad
            L_0x01ac:
                r1 = r3
            L_0x01ad:
                if (r1 == 0) goto L_0x01cd
                com.portfolio.platform.view.CustomizeWidget r1 = r13.e
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r2 = r14.g()
                if (r2 == 0) goto L_0x01c9
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r2 = r2.getTopMetaData()
                if (r2 == 0) goto L_0x01c5
                java.lang.Integer r2 = r2.getUnselectedForegroundColor()
                r1.a(r3, r3, r2, r3)
                goto L_0x0224
            L_0x01c5:
                com.fossil.ee7.a()
                throw r3
            L_0x01c9:
                com.fossil.ee7.a()
                throw r3
            L_0x01cd:
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x01da
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                goto L_0x01db
            L_0x01da:
                r1 = r3
            L_0x01db:
                if (r1 == 0) goto L_0x021f
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x021b
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                java.lang.String r1 = r1.getUnselectedForegroundColor()
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x021f
                com.portfolio.platform.view.CustomizeWidget r1 = r13.e
                com.fossil.zz5 r2 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r2 = r2.d()
                if (r2 == 0) goto L_0x0217
                com.portfolio.platform.data.model.diana.preset.MetaData r2 = r2.getMetaData()
                java.lang.String r2 = r2.getUnselectedForegroundColor()
                if (r2 == 0) goto L_0x0213
                int r2 = android.graphics.Color.parseColor(r2)
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r1.a(r3, r3, r2, r3)
                goto L_0x0224
            L_0x0213:
                com.fossil.ee7.a()
                throw r3
            L_0x0217:
                com.fossil.ee7.a()
                throw r3
            L_0x021b:
                com.fossil.ee7.a()
                throw r3
            L_0x021f:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.e
                r1.g()
            L_0x0224:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.e
                r1.h()
                goto L_0x0146
            L_0x022b:
                java.lang.String r2 = "left"
                boolean r2 = r5.equals(r2)
                if (r2 == 0) goto L_0x0146
                com.portfolio.platform.view.CustomizeWidget r2 = r13.d
                java.lang.String r4 = r1.a()
                r2.b(r4)
                com.portfolio.platform.view.CustomizeWidget r2 = r13.d
                java.lang.String r1 = r1.d()
                r2.setBottomContent(r1)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x0259
                android.graphics.drawable.Drawable r1 = r1.getLeftComplication()
                if (r1 == 0) goto L_0x0259
                com.portfolio.platform.view.CustomizeWidget r2 = r13.d
                r2.setBackgroundDrawableCus(r1)
                com.fossil.i97 r1 = com.fossil.i97.a
                goto L_0x0264
            L_0x0259:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.d
                java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
                r1.setBackgroundRes(r2)
                com.fossil.i97 r1 = com.fossil.i97.a
            L_0x0264:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x026f
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r1 = r1.getLeftMetaData()
                goto L_0x0270
            L_0x026f:
                r1 = r3
            L_0x0270:
                if (r1 == 0) goto L_0x0290
                com.portfolio.platform.view.CustomizeWidget r1 = r13.d
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r2 = r14.g()
                if (r2 == 0) goto L_0x028c
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r2 = r2.getTopMetaData()
                if (r2 == 0) goto L_0x0288
                java.lang.Integer r2 = r2.getUnselectedForegroundColor()
                r1.a(r3, r3, r2, r3)
                goto L_0x02e7
            L_0x0288:
                com.fossil.ee7.a()
                throw r3
            L_0x028c:
                com.fossil.ee7.a()
                throw r3
            L_0x0290:
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x029d
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                goto L_0x029e
            L_0x029d:
                r1 = r3
            L_0x029e:
                if (r1 == 0) goto L_0x02e2
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x02de
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                java.lang.String r1 = r1.getUnselectedForegroundColor()
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x02e2
                com.portfolio.platform.view.CustomizeWidget r1 = r13.d
                com.fossil.zz5 r2 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r2 = r2.d()
                if (r2 == 0) goto L_0x02da
                com.portfolio.platform.data.model.diana.preset.MetaData r2 = r2.getMetaData()
                java.lang.String r2 = r2.getUnselectedForegroundColor()
                if (r2 == 0) goto L_0x02d6
                int r2 = android.graphics.Color.parseColor(r2)
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r1.a(r3, r3, r2, r3)
                goto L_0x02e7
            L_0x02d6:
                com.fossil.ee7.a()
                throw r3
            L_0x02da:
                com.fossil.ee7.a()
                throw r3
            L_0x02de:
                com.fossil.ee7.a()
                throw r3
            L_0x02e2:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.d
                r1.g()
            L_0x02e7:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.d
                r1.h()
                goto L_0x0146
            L_0x02ee:
                boolean r2 = r5.equals(r2)
                if (r2 == 0) goto L_0x0146
                com.portfolio.platform.view.CustomizeWidget r2 = r13.b
                java.lang.String r4 = r1.a()
                r2.b(r4)
                com.portfolio.platform.view.CustomizeWidget r2 = r13.b
                java.lang.String r1 = r1.d()
                r2.setBottomContent(r1)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x031a
                android.graphics.drawable.Drawable r1 = r1.getTopComplication()
                if (r1 == 0) goto L_0x031a
                com.portfolio.platform.view.CustomizeWidget r2 = r13.b
                r2.setBackgroundDrawableCus(r1)
                com.fossil.i97 r1 = com.fossil.i97.a
                goto L_0x0325
            L_0x031a:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.b
                java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
                r1.setBackgroundRes(r2)
                com.fossil.i97 r1 = com.fossil.i97.a
            L_0x0325:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x0330
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r1 = r1.getTopMetaData()
                goto L_0x0331
            L_0x0330:
                r1 = r3
            L_0x0331:
                if (r1 == 0) goto L_0x0351
                com.portfolio.platform.view.CustomizeWidget r1 = r13.b
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r2 = r14.g()
                if (r2 == 0) goto L_0x034d
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r2 = r2.getTopMetaData()
                if (r2 == 0) goto L_0x0349
                java.lang.Integer r2 = r2.getUnselectedForegroundColor()
                r1.a(r3, r3, r2, r3)
                goto L_0x03a8
            L_0x0349:
                com.fossil.ee7.a()
                throw r3
            L_0x034d:
                com.fossil.ee7.a()
                throw r3
            L_0x0351:
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x035e
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                goto L_0x035f
            L_0x035e:
                r1 = r3
            L_0x035f:
                if (r1 == 0) goto L_0x03a3
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x039f
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                java.lang.String r1 = r1.getUnselectedForegroundColor()
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x03a3
                com.portfolio.platform.view.CustomizeWidget r1 = r13.b
                com.fossil.zz5 r2 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r2 = r2.d()
                if (r2 == 0) goto L_0x039b
                com.portfolio.platform.data.model.diana.preset.MetaData r2 = r2.getMetaData()
                java.lang.String r2 = r2.getUnselectedForegroundColor()
                if (r2 == 0) goto L_0x0397
                int r2 = android.graphics.Color.parseColor(r2)
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r1.a(r3, r3, r2, r3)
                goto L_0x03a8
            L_0x0397:
                com.fossil.ee7.a()
                throw r3
            L_0x039b:
                com.fossil.ee7.a()
                throw r3
            L_0x039f:
                com.fossil.ee7.a()
                throw r3
            L_0x03a3:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.b
                r1.g()
            L_0x03a8:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.b
                r1.h()
                goto L_0x0146
            L_0x03af:
                boolean r2 = r5.equals(r4)
                if (r2 == 0) goto L_0x0146
                com.portfolio.platform.view.CustomizeWidget r2 = r13.c
                java.lang.String r4 = r1.a()
                r2.b(r4)
                com.portfolio.platform.view.CustomizeWidget r2 = r13.c
                java.lang.String r1 = r1.d()
                r2.setBottomContent(r1)
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x03db
                android.graphics.drawable.Drawable r1 = r1.getTopComplication()
                if (r1 == 0) goto L_0x03db
                com.portfolio.platform.view.CustomizeWidget r2 = r13.c
                r2.setBackgroundDrawableCus(r1)
                com.fossil.i97 r1 = com.fossil.i97.a
                goto L_0x03e6
            L_0x03db:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.c
                java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
                r1.setBackgroundRes(r2)
                com.fossil.i97 r1 = com.fossil.i97.a
            L_0x03e6:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r1 = r14.g()
                if (r1 == 0) goto L_0x03f1
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r1 = r1.getBottomMetaData()
                goto L_0x03f2
            L_0x03f1:
                r1 = r3
            L_0x03f2:
                if (r1 == 0) goto L_0x0412
                com.portfolio.platform.view.CustomizeWidget r1 = r13.c
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r2 = r14.g()
                if (r2 == 0) goto L_0x040e
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper$MetaData r2 = r2.getTopMetaData()
                if (r2 == 0) goto L_0x040a
                java.lang.Integer r2 = r2.getUnselectedForegroundColor()
                r1.a(r3, r3, r2, r3)
                goto L_0x0469
            L_0x040a:
                com.fossil.ee7.a()
                throw r3
            L_0x040e:
                com.fossil.ee7.a()
                throw r3
            L_0x0412:
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x041f
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                goto L_0x0420
            L_0x041f:
                r1 = r3
            L_0x0420:
                if (r1 == 0) goto L_0x0464
                com.fossil.zz5 r1 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r1 = r1.d()
                if (r1 == 0) goto L_0x0460
                com.portfolio.platform.data.model.diana.preset.MetaData r1 = r1.getMetaData()
                java.lang.String r1 = r1.getUnselectedForegroundColor()
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x0464
                com.portfolio.platform.view.CustomizeWidget r1 = r13.c
                com.fossil.zz5 r2 = r13.B
                com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r2 = r2.d()
                if (r2 == 0) goto L_0x045c
                com.portfolio.platform.data.model.diana.preset.MetaData r2 = r2.getMetaData()
                java.lang.String r2 = r2.getUnselectedForegroundColor()
                if (r2 == 0) goto L_0x0458
                int r2 = android.graphics.Color.parseColor(r2)
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r1.a(r3, r3, r2, r3)
                goto L_0x0469
            L_0x0458:
                com.fossil.ee7.a()
                throw r3
            L_0x045c:
                com.fossil.ee7.a()
                throw r3
            L_0x0460:
                com.fossil.ee7.a()
                throw r3
            L_0x0464:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.c
                r1.g()
            L_0x0469:
                com.portfolio.platform.view.CustomizeWidget r1 = r13.c
                r1.h()
                goto L_0x0146
            L_0x0470:
                com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper r14 = r14.g()
                if (r14 == 0) goto L_0x0484
                android.graphics.drawable.Drawable r14 = r14.getBackground()
                if (r14 == 0) goto L_0x0484
                android.widget.ImageView r15 = r13.i
                r15.setImageDrawable(r14)
                com.fossil.i97 r14 = com.fossil.i97.a
                goto L_0x0498
            L_0x0484:
                android.widget.ImageView r14 = r13.i
                com.portfolio.platform.PortfolioApp$a r15 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r15 = r15.c()
                r1 = 2131231251(0x7f080213, float:1.8078578E38)
                android.graphics.drawable.Drawable r15 = r15.getDrawable(r1)
                r14.setImageDrawable(r15)
                com.fossil.i97 r14 = com.fossil.i97.a
            L_0x0498:
                java.util.Iterator r14 = r0.iterator()
            L_0x049c:
                boolean r15 = r14.hasNext()
                if (r15 == 0) goto L_0x0504
                java.lang.Object r15 = r14.next()
                com.fossil.fz5 r15 = (com.fossil.fz5) r15
                java.lang.String r0 = r15.c()
                if (r0 != 0) goto L_0x04af
                goto L_0x049c
            L_0x04af:
                int r1 = r0.hashCode()
                r3 = -1383228885(0xffffffffad8d9a2b, float:-1.6098308E-11)
                if (r1 == r3) goto L_0x04ef
                r3 = -1074341483(0xffffffffbff6d995, float:-1.9285151)
                if (r1 == r3) goto L_0x04d8
                r3 = 115029(0x1c155, float:1.6119E-40)
                if (r1 == r3) goto L_0x04c3
                goto L_0x049c
            L_0x04c3:
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x049c
                com.portfolio.platform.view.CustomizeWidget r0 = r13.p
                java.lang.String r15 = r15.a()
                r0.d(r15)
                com.portfolio.platform.view.CustomizeWidget r15 = r13.p
                r15.h()
                goto L_0x049c
            L_0x04d8:
                java.lang.String r1 = "middle"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x049c
                com.portfolio.platform.view.CustomizeWidget r0 = r13.q
                java.lang.String r15 = r15.a()
                r0.d(r15)
                com.portfolio.platform.view.CustomizeWidget r15 = r13.q
                r15.h()
                goto L_0x049c
            L_0x04ef:
                boolean r0 = r0.equals(r4)
                if (r0 == 0) goto L_0x049c
                com.portfolio.platform.view.CustomizeWidget r0 = r13.r
                java.lang.String r15 = r15.a()
                r0.d(r15)
                com.portfolio.platform.view.CustomizeWidget r15 = r13.r
                r15.h()
                goto L_0x049c
            L_0x0504:
                return
                switch-data {-1383228885->0x03af, 115029->0x02ee, 3317767->0x022b, 108511772->0x0168, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zz5.b.a(com.fossil.dz5, boolean):void");
        }
    }
}
