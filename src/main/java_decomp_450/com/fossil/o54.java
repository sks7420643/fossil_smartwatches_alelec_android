package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o54 extends v54.d.AbstractC0206d.a.b.AbstractC0212d {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d
    public long a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d
    public String b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d
    public String c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b.AbstractC0212d)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b.AbstractC0212d dVar = (v54.d.AbstractC0206d.a.b.AbstractC0212d) obj;
        if (!this.a.equals(dVar.c()) || !this.b.equals(dVar.b()) || this.c != dVar.a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Signal{name=" + this.a + ", code=" + this.b + ", address=" + this.c + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a
        public v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a a(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null code");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a
        public v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a
        public v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a a(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a
        public v54.d.AbstractC0206d.a.b.AbstractC0212d a() {
            String str = "";
            if (this.a == null) {
                str = str + " name";
            }
            if (this.b == null) {
                str = str + " code";
            }
            if (this.c == null) {
                str = str + " address";
            }
            if (str.isEmpty()) {
                return new o54(this.a, this.b, this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public o54(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }
}
