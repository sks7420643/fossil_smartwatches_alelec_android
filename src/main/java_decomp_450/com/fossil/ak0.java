package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ak0 {
    DISTANCE((byte) 0),
    POSITION((byte) 1);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public ak0(byte b) {
        this.a = b;
    }
}
