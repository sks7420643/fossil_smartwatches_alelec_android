package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ci4 extends li4 {
    @DexIgnore
    @Override // com.fossil.sg4, com.fossil.li4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (mg4 == mg4.CODE_39) {
            return super.a(str, mg4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_39, but got " + mg4);
    }

    @DexIgnore
    @Override // com.fossil.li4
    public boolean[] a(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            int i = length + 25;
            int i2 = 0;
            while (i2 < length) {
                int indexOf = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i2));
                if (indexOf >= 0) {
                    a(bi4.a[indexOf], iArr);
                    for (int i3 = 0; i3 < 9; i3++) {
                        i += iArr[i3];
                    }
                    i2++;
                } else {
                    throw new IllegalArgumentException("Bad contents: " + str);
                }
            }
            boolean[] zArr = new boolean[i];
            a(bi4.b, iArr);
            int a = li4.a(zArr, 0, iArr, true);
            int[] iArr2 = {1};
            int a2 = a + li4.a(zArr, a, iArr2, false);
            for (int i4 = 0; i4 < length; i4++) {
                a(bi4.a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%".indexOf(str.charAt(i4))], iArr);
                int a3 = a2 + li4.a(zArr, a2, iArr, true);
                a2 = a3 + li4.a(zArr, a3, iArr2, false);
            }
            a(bi4.b, iArr);
            li4.a(zArr, a2, iArr, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }

    @DexIgnore
    public static void a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) != 0) {
                i3 = 2;
            }
            iArr[i2] = i3;
        }
    }
}
