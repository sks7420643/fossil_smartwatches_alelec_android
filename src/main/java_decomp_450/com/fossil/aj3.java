package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public aj3(ti3 ti3, long j) {
        this.b = ti3;
        this.a = j;
    }

    @DexIgnore
    public final void run() {
        ti3 ti3 = this.b;
        long j = this.a;
        ti3.g();
        ti3.a();
        ti3.w();
        ti3.e().A().a("Resetting analytics data (FE)");
        il3 t = ti3.t();
        t.g();
        t.e.a();
        boolean g = ((ii3) ti3).a.g();
        wg3 k = ti3.k();
        k.j.a(j);
        if (!TextUtils.isEmpty(k.k().z.a())) {
            k.z.a(null);
        }
        if (t13.a() && k.l().a(wb3.w0)) {
            k.u.a(0);
        }
        if (!k.l().p()) {
            k.c(!g);
        }
        k.A.a(null);
        k.B.a(0);
        k.C.a(null);
        ti3.q().C();
        if (t13.a() && ti3.l().a(wb3.w0)) {
            ti3.t().d.a();
        }
        ti3.i = !g;
        this.b.q().a(new AtomicReference<>());
    }
}
