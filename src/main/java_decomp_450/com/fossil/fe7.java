package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fe7<R> implements be7<R>, Serializable {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public fe7(int i) {
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.be7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public String toString() {
        String a = te7.a((fe7) this);
        ee7.a((Object) a, "Reflection.renderLambdaToString(this)");
        return a;
    }
}
