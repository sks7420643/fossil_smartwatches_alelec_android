package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class af5 {
    @DexIgnore
    public static /* final */ af5 a; // = new af5();

    @DexIgnore
    public final String a(Float f) {
        String b = re5.b(f != null ? f.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        ee7.a((Object) b, "NumberHelper.decimalForm\u2026Number(calories ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String b(Integer num) {
        String b = re5.b(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2);
        ee7.a((Object) b, "NumberHelper.decimalForm\u2026teps?.toFloat() ?: 0F, 2)");
        return b;
    }

    @DexIgnore
    public final String a(Integer num) {
        String b = re5.b(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        ee7.a((Object) b, "NumberHelper.decimalForm\u2026Time?.toFloat() ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String a(Float f, ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ob5 ob52 = ob5.IMPERIAL;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (ob5 == ob52) {
            if (f != null) {
                f2 = f.floatValue();
            }
            String b = re5.b(xd5.j(f2), 1);
            ee7.a((Object) b, "NumberHelper.decimalForm\u2026Miles(distance ?: 0F), 1)");
            return b;
        }
        if (f != null) {
            f2 = f.floatValue();
        }
        String b2 = re5.b(xd5.i(f2), 1);
        ee7.a((Object) b2, "NumberHelper.decimalForm\u2026eters(distance ?: 0F), 1)");
        return b2;
    }
}
