package com.fossil;

import com.facebook.internal.NativeProtocol;
import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class xr0 extends Enum<xr0> {
    @DexIgnore
    public static /* final */ xr0 b;
    @DexIgnore
    public static /* final */ xr0 c;
    @DexIgnore
    public static /* final */ xr0 d;
    @DexIgnore
    public static /* final */ xr0 e;
    @DexIgnore
    public static /* final */ xr0 f;
    @DexIgnore
    public static /* final */ xr0 g;
    @DexIgnore
    public static /* final */ xr0 h;
    @DexIgnore
    public static /* final */ xr0 i;
    @DexIgnore
    public static /* final */ xr0 j;
    @DexIgnore
    public static /* final */ xr0 k;
    @DexIgnore
    public static /* final */ /* synthetic */ xr0[] l;
    @DexIgnore
    public static /* final */ bq0 m; // = new bq0(null);
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        xr0 xr0 = new xr0("SUCCESS", 0, 0);
        b = xr0;
        xr0 xr02 = new xr0("UNSUPPORTED_FILE_HANDLE", 1, 1);
        c = xr02;
        xr0 xr03 = new xr0("REQUEST_NOT_SUPPORTED", 6, 6);
        d = xr03;
        xr0 xr04 = new xr0("START_FAIL", 47, 65536);
        e = xr04;
        xr0 xr05 = new xr0("HID_PROXY_NOT_CONNECTED", 48, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY);
        f = xr05;
        xr0 xr06 = new xr0("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 49, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REQUEST);
        g = xr06;
        xr0 xr07 = new xr0("HID_INPUT_DEVICE_DISABLED", 50, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REPLY);
        h = xr07;
        xr0 xr08 = new xr0("HID_UNKNOWN_ERROR", 51, NativeProtocol.MESSAGE_GET_INSTALL_DATA_REQUEST);
        i = xr08;
        xr0 xr09 = new xr0("BLUETOOTH_OFF", 52, 16777215);
        j = xr09;
        xr0 xr010 = new xr0("UNKNOWN", 53, 16777215);
        k = xr010;
        l = new xr0[]{xr0, xr02, new xr0("READ_NOT_PERMITTED", 2, 2), new xr0("WRITE_NOT_PERMITTED", 3, 3), new xr0("INVALID_PDU", 4, 4), new xr0("INSUFFICIENT_AUTHENTICATION", 5, 5), xr03, new xr0("INVALID_OFFSET", 7, 7), new xr0("INSUFFICIENT_AUTHORIZATION", 8, 8), new xr0("PREPARE_QUEUE_FULL", 9, 9), new xr0("ATTRIBUTE_NOT_FOUND", 10, 10), new xr0("ATTRIBUTE_NOT_LONG", 11, 11), new xr0("INSUFFICIENT_ENCRYPTION_KEY_SIZE", 12, 12), new xr0("INVALID_ATTRIBUTE_VALUE_LEN", 13, 13), new xr0("OTHER_UNLIKELY_ERROR", 14, 14), new xr0("INSUFFICIENT_ENCRYPTION", 15, 15), new xr0("UNSUPPORTED_GROUP_TYPE", 16, 16), new xr0("INSUFFICIENT_RESOURCES", 17, 17), new xr0("OUT_OF_MEMORY", 18, 112), new xr0("TRANSACTION_TIMEOUT", 19, 113), new xr0("TRANSACTION_OVERFLOW", 20, 114), new xr0("INVALID_RESPONSE_PDU", 21, 115), new xr0("REQUEST_CANCELLED", 22, 116), new xr0("OTHER_UNDEFINED_ERROR", 23, 117), new xr0("REQUIRED_CHARACTERISTIC_NOT_FOUND", 24, 118), new xr0("ATTRIBUTE_PDU_LENGTH_EXCEEDED_MTU_SIZE", 25, 119), new xr0("PROCEDURE_CONTINUING", 26, 120), new xr0("NO_RESOURCES", 27, 128), new xr0("INTERNAL_ERROR", 28, 129), new xr0("WRONG_STATE", 29, 130), new xr0("DB_FULL", 30, 131), new xr0("BUSY", 31, 132), new xr0("ERROR", 32, 133), new xr0("CMD_STARTED", 33, 134), new xr0("ILLEGAL_PARAMETER", 34, 135), new xr0("PENDING", 35, 136), new xr0("AUTH_FAIL", 36, 137), new xr0("MORE", 37, 138), new xr0("INVALID_CFG", 38, 139), new xr0("SERVICE_STARTED", 39, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL), new xr0("ENCRYPED_NO_MITM", 40, 141), new xr0("NOT_ENCRYPTED", 41, 142), new xr0("CONGESTED", 42, 143), new xr0("CCCD_IMPROPERLY_CONFIGURED", 43, 253), new xr0("PROCEDURE_ALREADY_IN_PROGRESS", 44, 254), new xr0("VALUE_OUT_OF_RANGE", 45, 255), new xr0("GATT_FAILURE", 46, 257), xr04, xr05, xr06, xr07, xr08, xr09, xr010};
    }
    */

    @DexIgnore
    public xr0(String str, int i2, int i3) {
        this.a = i3;
    }

    @DexIgnore
    public static xr0 valueOf(String str) {
        return (xr0) Enum.valueOf(xr0.class, str);
    }

    @DexIgnore
    public static xr0[] values() {
        return (xr0[]) l.clone();
    }
}
