package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw3<F, T> extends aw3<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ cw3<F, ? extends T> function;
    @DexIgnore
    public /* final */ aw3<T> resultEquivalence;

    @DexIgnore
    public dw3(cw3<F, ? extends T> cw3, aw3<T> aw3) {
        jw3.a(cw3);
        this.function = cw3;
        jw3.a(aw3);
        this.resultEquivalence = aw3;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.aw3<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.aw3
    public boolean doEquivalent(F f, F f2) {
        return this.resultEquivalence.equivalent(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.aw3<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.aw3
    public int doHash(F f) {
        return this.resultEquivalence.hash(this.function.apply(f));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dw3)) {
            return false;
        }
        dw3 dw3 = (dw3) obj;
        if (!this.function.equals(dw3.function) || !this.resultEquivalence.equals(dw3.resultEquivalence)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return gw3.a(this.function, this.resultEquivalence);
    }

    @DexIgnore
    public String toString() {
        return this.resultEquivalence + ".onResultOf(" + this.function + ")";
    }
}
