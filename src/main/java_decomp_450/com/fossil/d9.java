package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d9<T> extends c9<T> {
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public d9(int i) {
        super(i);
    }

    @DexIgnore
    @Override // com.fossil.b9, com.fossil.c9
    public T a() {
        T t;
        synchronized (this.c) {
            t = (T) super.a();
        }
        return t;
    }

    @DexIgnore
    @Override // com.fossil.b9, com.fossil.c9
    public boolean a(T t) {
        boolean a;
        synchronized (this.c) {
            a = super.a(t);
        }
        return a;
    }
}
