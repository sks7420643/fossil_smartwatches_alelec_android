package com.fossil;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ot7 implements mt7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public nt7 b;

    @DexIgnore
    public ot7(String str) {
        a(str);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(mt7 mt7) {
        if (mt7 instanceof ot7) {
            return this.b.compareTo(((ot7) mt7).b);
        }
        return compareTo(new ot7(mt7.toString()));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof mt7) && compareTo((mt7) obj) == 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + 11;
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }

    @DexIgnore
    public final void a(String str) {
        String str2;
        String str3;
        this.b = new nt7(str);
        int indexOf = str.indexOf(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        boolean z = false;
        boolean z2 = true;
        if (indexOf < 0) {
            str3 = null;
            str2 = str;
        } else {
            str2 = str.substring(0, indexOf);
            str3 = str.substring(indexOf + 1);
        }
        if (str3 != null) {
            try {
                if (str3.length() != 1) {
                    if (str3.startsWith("0")) {
                        this.a = str3;
                    }
                }
                Integer.valueOf(str3);
            } catch (NumberFormatException unused) {
                this.a = str3;
            }
        }
        if (str2.contains(CodelessMatcher.CURRENT_CLASS_NAME) || str2.startsWith("0")) {
            StringTokenizer stringTokenizer = new StringTokenizer(str2, CodelessMatcher.CURRENT_CLASS_NAME);
            try {
                a(stringTokenizer);
                if (stringTokenizer.hasMoreTokens()) {
                    a(stringTokenizer);
                }
                if (stringTokenizer.hasMoreTokens()) {
                    a(stringTokenizer);
                }
                if (stringTokenizer.hasMoreTokens()) {
                    this.a = stringTokenizer.nextToken();
                    z = Pattern.compile("\\d+").matcher(this.a).matches();
                }
                if (!str2.contains("..") && !str2.startsWith(CodelessMatcher.CURRENT_CLASS_NAME) && !str2.endsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                    z2 = z;
                }
            } catch (NumberFormatException unused2) {
            }
            if (z2) {
                this.a = str;
                return;
            }
            return;
        }
        try {
            Integer.valueOf(str2);
        } catch (NumberFormatException unused3) {
            this.a = str;
        }
    }

    @DexIgnore
    public static Integer a(StringTokenizer stringTokenizer) {
        try {
            String nextToken = stringTokenizer.nextToken();
            if (nextToken.length() > 1) {
                if (nextToken.startsWith("0")) {
                    throw new NumberFormatException("Number part has a leading 0: '" + nextToken + "'");
                }
            }
            return Integer.valueOf(nextToken);
        } catch (NoSuchElementException unused) {
            throw new NumberFormatException("Number is invalid");
        }
    }
}
