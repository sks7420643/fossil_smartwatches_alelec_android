package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hm7 {
    @DexIgnore
    public abstract ul7<?> a();

    @DexIgnore
    public abstract Object a(Object obj);

    @DexIgnore
    public final boolean a(hm7 hm7) {
        ul7<?> a;
        ul7<?> a2 = a();
        if (a2 == null || (a = hm7.a()) == null || a2.b() >= a.b()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public String toString() {
        return ej7.a(this) + '@' + ej7.b(this);
    }
}
