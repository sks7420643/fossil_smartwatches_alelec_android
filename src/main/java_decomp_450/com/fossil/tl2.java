package com.fossil;

import com.fossil.y12;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tl2 implements y12.b<c53> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationResult a;

    @DexIgnore
    public tl2(sl2 sl2, LocationResult locationResult) {
        this.a = locationResult;
    }

    @DexIgnore
    @Override // com.fossil.y12.b
    public final void a() {
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.y12.b
    public final /* synthetic */ void a(c53 c53) {
        c53.onLocationResult(this.a);
    }
}
