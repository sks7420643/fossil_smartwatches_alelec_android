package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentContainerView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f15 extends e15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J;
    @DexIgnore
    public long H;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        J = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        J.put(2131363342, 2);
        J.put(2131362661, 3);
        J.put(2131363257, 4);
        J.put(2131363315, 5);
        J.put(2131363291, 6);
        J.put(2131362449, 7);
        J.put(2131363107, 8);
        J.put(2131362400, 9);
        J.put(2131362073, 10);
        J.put(2131362241, 11);
        J.put(2131362069, 12);
        J.put(2131362442, 13);
        J.put(2131362443, 14);
        J.put(2131362440, 15);
        J.put(2131362441, 16);
    }
    */

    @DexIgnore
    public f15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 17, I, J));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public f15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[12], (ConstraintLayout) objArr[10], (FragmentContainerView) objArr[11], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[7], (RTLImageView) objArr[1], (ImageView) objArr[3], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[8], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[2]);
        this.H = -1;
        ((e15) this).B.setTag(null);
        a(view);
        f();
    }
}
