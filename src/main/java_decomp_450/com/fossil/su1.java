package com.fossil;

import com.fossil.ou1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su1<T> implements ft1<T> {
    @DexIgnore
    public /* final */ pu1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ bt1 c;
    @DexIgnore
    public /* final */ et1<T, byte[]> d;
    @DexIgnore
    public /* final */ tu1 e;

    @DexIgnore
    public su1(pu1 pu1, String str, bt1 bt1, et1<T, byte[]> et1, tu1 tu1) {
        this.a = pu1;
        this.b = str;
        this.c = bt1;
        this.d = et1;
        this.e = tu1;
    }

    @DexIgnore
    public static /* synthetic */ void a(Exception exc) {
    }

    @DexIgnore
    @Override // com.fossil.ft1
    public void a(ct1<T> ct1) {
        a(ct1, ru1.a());
    }

    @DexIgnore
    @Override // com.fossil.ft1
    public void a(ct1<T> ct1, ht1 ht1) {
        tu1 tu1 = this.e;
        ou1.a g = ou1.g();
        g.a(this.a);
        g.a((ct1<?>) ct1);
        g.a(this.b);
        g.a((et1<?, byte[]>) this.d);
        g.a(this.c);
        tu1.a(g.a(), ht1);
    }
}
