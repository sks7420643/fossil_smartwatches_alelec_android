package com.fossil;

import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class uk5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[AddressWrapper.AddressType.values().length];
        a = iArr;
        iArr[AddressWrapper.AddressType.HOME.ordinal()] = 1;
        a[AddressWrapper.AddressType.WORK.ordinal()] = 2;
        a[AddressWrapper.AddressType.OTHER.ordinal()] = 3;
        int[] iArr2 = new int[LocationSource.ErrorState.values().length];
        b = iArr2;
        iArr2[LocationSource.ErrorState.LOCATION_PERMISSION_OFF.ordinal()] = 1;
        b[LocationSource.ErrorState.LOCATION_SERVICE_OFF.ordinal()] = 2;
    }
    */
}
