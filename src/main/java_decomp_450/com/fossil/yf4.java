package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.xf4;
import com.fossil.zf4;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class yf4 {
    public static final tf4 g = new a();
    public static final Logger h = Logger.getLogger(yf4.class.getName());
    public static final Map<Character, Character> i;
    public static final Map<Character, Character> j;
    public static final String k;
    public static final Pattern l = Pattern.compile("[+\uff0b]+");
    public static final Pattern m = Pattern.compile("[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+");
    public static final Pattern n = Pattern.compile("(\\p{Nd})");
    public static final Pattern o = Pattern.compile("[+\uff0b\\p{Nd}]");
    public static final Pattern p = Pattern.compile("[\\\\/] *x");
    public static final Pattern q = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    public static final Pattern r = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    public static final String s;
    public static final String t = c("x\uff58#\uff03~\uff5e".length() != 0 ? ",".concat("x\uff58#\uff03~\uff5e") : new String(","));
    public static final Pattern u;
    public static final Pattern v;
    public static final Pattern w = Pattern.compile("(\\$\\d)");
    public static final Pattern x = Pattern.compile("\\$CC");
    public static yf4 y = null;
    public final vf4 a;
    public final Map<Integer, List<String>> b;
    public final Set<String> c = new HashSet(35);
    public final ag4 d = new ag4(100);
    public final Set<String> e = new HashSet(320);
    public final Set<Integer> f = new HashSet();

    public static class a implements tf4 {
        @Override // com.fossil.tf4
        public InputStream a(String str) {
            return yf4.class.getResourceAsStream(str);
        }
    }

    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;
        public static final /* synthetic */ int[] c;

        /* JADX WARNING: Can't wrap try/catch for region: R(39:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|(2:27|28)|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|(3:47|48|50)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(40:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|(2:27|28)|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|(3:47|48|50)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(43:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Can't wrap try/catch for region: R(44:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Can't wrap try/catch for region: R(45:0|1|2|3|5|6|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0033 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0095 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00a9 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00c4 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00ce */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00d8 */
        /*
        static {
            /*
                com.fossil.yf4$d[] r0 = com.fossil.yf4.d.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.yf4.b.c = r0
                r1 = 1
                com.fossil.yf4$d r2 = com.fossil.yf4.d.PREMIUM_RATE     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.yf4$d r3 = com.fossil.yf4.d.TOLL_FREE     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                r2 = 3
                int[] r3 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.yf4$d r4 = com.fossil.yf4.d.MOBILE     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                r3 = 4
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.FIXED_LINE     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.FIXED_LINE_OR_MOBILE     // Catch:{ NoSuchFieldError -> 0x003e }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r6 = 5
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.SHARED_COST     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r6 = 6
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.VOIP     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r6 = 7
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.PERSONAL_NUMBER     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r6 = 8
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x006c }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.PAGER     // Catch:{ NoSuchFieldError -> 0x006c }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r6 = 9
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.UAN     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r6 = 10
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r4 = com.fossil.yf4.b.c     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.fossil.yf4$d r5 = com.fossil.yf4.d.VOICEMAIL     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r6 = 11
                r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                com.fossil.yf4$c[] r4 = com.fossil.yf4.c.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.fossil.yf4.b.b = r4
                com.fossil.yf4$c r5 = com.fossil.yf4.c.E164     // Catch:{ NoSuchFieldError -> 0x0095 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0095 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0095 }
            L_0x0095:
                int[] r4 = com.fossil.yf4.b.b     // Catch:{ NoSuchFieldError -> 0x009f }
                com.fossil.yf4$c r5 = com.fossil.yf4.c.INTERNATIONAL     // Catch:{ NoSuchFieldError -> 0x009f }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x009f }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x009f }
            L_0x009f:
                int[] r4 = com.fossil.yf4.b.b     // Catch:{ NoSuchFieldError -> 0x00a9 }
                com.fossil.yf4$c r5 = com.fossil.yf4.c.RFC3966     // Catch:{ NoSuchFieldError -> 0x00a9 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a9 }
                r4[r5] = r2     // Catch:{ NoSuchFieldError -> 0x00a9 }
            L_0x00a9:
                int[] r4 = com.fossil.yf4.b.b     // Catch:{ NoSuchFieldError -> 0x00b3 }
                com.fossil.yf4$c r5 = com.fossil.yf4.c.NATIONAL     // Catch:{ NoSuchFieldError -> 0x00b3 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b3 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x00b3 }
            L_0x00b3:
                com.fossil.zf4$a[] r4 = com.fossil.zf4.a.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.fossil.yf4.b.a = r4
                com.fossil.zf4$a r5 = com.fossil.zf4.a.FROM_NUMBER_WITH_PLUS_SIGN     // Catch:{ NoSuchFieldError -> 0x00c4 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c4 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x00c4 }
            L_0x00c4:
                int[] r1 = com.fossil.yf4.b.a     // Catch:{ NoSuchFieldError -> 0x00ce }
                com.fossil.zf4$a r4 = com.fossil.zf4.a.FROM_NUMBER_WITH_IDD     // Catch:{ NoSuchFieldError -> 0x00ce }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ce }
                r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x00ce }
            L_0x00ce:
                int[] r0 = com.fossil.yf4.b.a     // Catch:{ NoSuchFieldError -> 0x00d8 }
                com.fossil.zf4$a r1 = com.fossil.zf4.a.FROM_NUMBER_WITHOUT_PLUS_SIGN     // Catch:{ NoSuchFieldError -> 0x00d8 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00d8 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00d8 }
            L_0x00d8:
                int[] r0 = com.fossil.yf4.b.a     // Catch:{ NoSuchFieldError -> 0x00e2 }
                com.fossil.zf4$a r1 = com.fossil.zf4.a.FROM_DEFAULT_COUNTRY     // Catch:{ NoSuchFieldError -> 0x00e2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e2 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x00e2 }
            L_0x00e2:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.yf4.b.<clinit>():void");
        }
        */
    }

    public enum c {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    public enum d {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }

    public enum e {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    /*
    static {
        HashMap hashMap = new HashMap();
        hashMap.put(52, "1");
        hashMap.put(54, CrashDumperPlugin.OPTION_KILL_DEFAULT);
        Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.add(52);
        hashSet.add(54);
        hashSet.add(55);
        Collections.unmodifiableSet(hashSet);
        HashMap hashMap2 = new HashMap();
        hashMap2.put('0', '0');
        hashMap2.put('1', '1');
        hashMap2.put('2', '2');
        hashMap2.put('3', '3');
        hashMap2.put('4', '4');
        hashMap2.put('5', '5');
        hashMap2.put('6', '6');
        hashMap2.put('7', '7');
        hashMap2.put('8', '8');
        hashMap2.put('9', '9');
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        i = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(i);
        hashMap4.putAll(hashMap2);
        j = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put('+', '+');
        hashMap5.put('*', '*');
        Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character ch : i.keySet()) {
            char charValue = ch.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue)), Character.valueOf(charValue));
            hashMap6.put(Character.valueOf(charValue), Character.valueOf(charValue));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put('-', '-');
        hashMap6.put('\uff0d', '-');
        hashMap6.put('\u2010', '-');
        hashMap6.put('\u2011', '-');
        hashMap6.put('\u2012', '-');
        hashMap6.put('\u2013', '-');
        hashMap6.put('\u2014', '-');
        hashMap6.put('\u2015', '-');
        hashMap6.put('\u2212', '-');
        hashMap6.put('/', '/');
        hashMap6.put('\uff0f', '/');
        hashMap6.put(' ', ' ');
        hashMap6.put('\u3000', ' ');
        hashMap6.put('\u2060', ' ');
        hashMap6.put('.', '.');
        hashMap6.put('\uff0e', '.');
        Collections.unmodifiableMap(hashMap6);
        Pattern.compile("[\\d]+(?:[~\u2053\u223c\uff5e][\\d]+)?");
        String valueOf = String.valueOf(Arrays.toString(i.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(i.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        k = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(String.valueOf(k));
        StringBuilder sb = new StringBuilder("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*".length() + 2 + valueOf3.length() + "\\p{Nd}".length());
        sb.append("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*");
        sb.append(valueOf3);
        sb.append("\\p{Nd}");
        sb.append("]*");
        s = sb.toString();
        c("x\uff58#\uff03~\uff5e");
        String valueOf4 = String.valueOf(String.valueOf(t));
        StringBuilder sb2 = new StringBuilder(valueOf4.length() + 5);
        sb2.append("(?:");
        sb2.append(valueOf4);
        sb2.append(")$");
        u = Pattern.compile(sb2.toString(), 66);
        String valueOf5 = String.valueOf(String.valueOf(s));
        String valueOf6 = String.valueOf(String.valueOf(t));
        StringBuilder sb3 = new StringBuilder(valueOf5.length() + 5 + valueOf6.length());
        sb3.append(valueOf5);
        sb3.append("(?:");
        sb3.append(valueOf6);
        sb3.append(")?");
        v = Pattern.compile(sb3.toString(), 66);
        Pattern.compile("(\\D+)");
        Pattern.compile("\\$NP");
        Pattern.compile("\\$FG");
        Pattern.compile("\\(?\\$1\\)?");
    }
    */

    public yf4(vf4 vf4, Map<Integer, List<String>> map) {
        this.a = vf4;
        this.b = map;
        for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
            List<String> value = entry.getValue();
            if (value.size() != 1 || !"001".equals(value.get(0))) {
                this.e.addAll(value);
            } else {
                this.f.add(entry.getKey());
            }
        }
        if (this.e.remove("001")) {
            h.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.c.addAll(map.get(1));
    }

    public static StringBuilder a(String str, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        char[] charArray = str.toCharArray();
        for (char c2 : charArray) {
            int digit = Character.digit(c2, 10);
            if (digit != -1) {
                sb.append(digit);
            } else if (z) {
                sb.append(c2);
            }
        }
        return sb;
    }

    public static void b(StringBuilder sb) {
        sb.replace(0, sb.length(), f(sb.toString()));
    }

    public static String c(String str) {
        String valueOf = String.valueOf(String.valueOf(str));
        StringBuilder sb = new StringBuilder(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[".length() + 48 + valueOf.length() + "(\\p{Nd}{1,7})".length() + "\\p{Nd}".length());
        sb.append(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[");
        sb.append(valueOf);
        sb.append("]|int|anexo|\uff49\uff4e\uff54)");
        sb.append("[:\\.\uff0e]?[ \u00a0\\t,-]*");
        sb.append("(\\p{Nd}{1,7})");
        sb.append("#?|");
        sb.append("[- ]+(");
        sb.append("\\p{Nd}");
        sb.append("{1,5})#");
        return sb.toString();
    }

    public static String d(String str) {
        Matcher matcher = o.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = q.matcher(substring);
        if (matcher2.find()) {
            substring = substring.substring(0, matcher2.start());
            Logger logger = h;
            Level level = Level.FINER;
            String valueOf = String.valueOf(substring);
            logger.log(level, valueOf.length() != 0 ? "Stripped trailing characters: ".concat(valueOf) : new String("Stripped trailing characters: "));
        }
        Matcher matcher3 = p.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    public static boolean e(String str) {
        if (str.length() < 2) {
            return false;
        }
        return v.matcher(str).matches();
    }

    public static String f(String str) {
        if (r.matcher(str).matches()) {
            return a(str, j, true);
        }
        return g(str);
    }

    public static String g(String str) {
        return a(str, false).toString();
    }

    public final boolean c(int i2) {
        return this.b.containsKey(Integer.valueOf(i2));
    }

    public final boolean b(String str) {
        return str != null && this.e.contains(str);
    }

    public String b(int i2) {
        List<String> list = this.b.get(Integer.valueOf(i2));
        if (list == null) {
            return "ZZ";
        }
        return list.get(0);
    }

    public static String a(String str, Map<Character, Character> map, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            Character ch = map.get(Character.valueOf(Character.toUpperCase(charAt)));
            if (ch != null) {
                sb.append(ch);
            } else if (!z) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    public zf4 b(String str, String str2) throws xf4 {
        zf4 zf4 = new zf4();
        a(str, str2, zf4);
        return zf4;
    }

    public static synchronized void a(yf4 yf4) {
        synchronized (yf4.class) {
            y = yf4;
        }
    }

    public static synchronized yf4 a() {
        yf4 yf4;
        synchronized (yf4.class) {
            if (y == null) {
                a(a(g));
            }
            yf4 = y;
        }
        return yf4;
    }

    public static yf4 a(vf4 vf4) {
        if (vf4 != null) {
            return new yf4(vf4, sf4.a());
        }
        throw new IllegalArgumentException("metadataSource could not be null.");
    }

    public static yf4 a(tf4 tf4) {
        if (tf4 != null) {
            return a(new wf4(tf4));
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    public String a(zf4 zf4, c cVar) {
        if (zf4.getNationalNumber() == 0 && zf4.hasRawInput()) {
            String rawInput = zf4.getRawInput();
            if (rawInput.length() > 0) {
                return rawInput;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        a(zf4, cVar, sb);
        return sb.toString();
    }

    public void a(zf4 zf4, c cVar, StringBuilder sb) {
        sb.setLength(0);
        int countryCode = zf4.getCountryCode();
        String a2 = a(zf4);
        if (cVar == c.E164) {
            sb.append(a2);
            a(countryCode, c.E164, sb);
        } else if (!c(countryCode)) {
            sb.append(a2);
        } else {
            dg4 a3 = a(countryCode, b(countryCode));
            sb.append(a(a2, a3, cVar));
            a(zf4, a3, cVar, sb);
            a(countryCode, cVar, sb);
        }
    }

    public final dg4 a(int i2, String str) {
        return "001".equals(str) ? a(i2) : a(str);
    }

    public String a(zf4 zf4) {
        StringBuilder sb = new StringBuilder();
        if (zf4.isItalianLeadingZero()) {
            char[] cArr = new char[zf4.getNumberOfLeadingZeros()];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(zf4.getNationalNumber());
        return sb.toString();
    }

    public final void a(int i2, c cVar, StringBuilder sb) {
        int i3 = b.b[cVar.ordinal()];
        if (i3 == 1) {
            sb.insert(0, i2).insert(0, '+');
        } else if (i3 == 2) {
            sb.insert(0, " ").insert(0, i2).insert(0, '+');
        } else if (i3 == 3) {
            sb.insert(0, ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).insert(0, i2).insert(0, '+').insert(0, "tel:");
        }
    }

    public final String a(String str, dg4 dg4, c cVar) {
        return a(str, dg4, cVar, (String) null);
    }

    public final String a(String str, dg4 dg4, c cVar, String str2) {
        cg4[] cg4Arr = dg4.w;
        if (cg4Arr.length == 0 || cVar == c.NATIONAL) {
            cg4Arr = dg4.v;
        }
        cg4 a2 = a(cg4Arr, str);
        return a2 == null ? str : a(str, a2, cVar, str2);
    }

    public cg4 a(cg4[] cg4Arr, String str) {
        for (cg4 cg4 : cg4Arr) {
            String[] strArr = cg4.c;
            int length = strArr.length;
            if ((length == 0 || this.d.a(strArr[length - 1]).matcher(str).lookingAt()) && this.d.a(cg4.a).matcher(str).matches()) {
                return cg4;
            }
        }
        return null;
    }

    public final String a(String str, cg4 cg4, c cVar, String str2) {
        String str3;
        String str4 = cg4.b;
        Matcher matcher = this.d.a(cg4.a).matcher(str);
        if (cVar != c.NATIONAL || str2 == null || str2.length() <= 0 || cg4.e.length() <= 0) {
            String str5 = cg4.d;
            if (cVar != c.NATIONAL || str5 == null || str5.length() <= 0) {
                str3 = matcher.replaceAll(str4);
            } else {
                str3 = matcher.replaceAll(w.matcher(str4).replaceFirst(str5));
            }
        } else {
            str3 = matcher.replaceAll(w.matcher(str4).replaceFirst(x.matcher(cg4.e).replaceFirst(str2)));
        }
        if (cVar != c.RFC3966) {
            return str3;
        }
        Matcher matcher2 = m.matcher(str3);
        if (matcher2.lookingAt()) {
            str3 = matcher2.replaceFirst("");
        }
        return matcher2.reset(str3).replaceAll(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
    }

    public final void a(zf4 zf4, dg4 dg4, c cVar, StringBuilder sb) {
        if (zf4.hasExtension() && zf4.getExtension().length() > 0) {
            if (cVar == c.RFC3966) {
                sb.append(";ext=");
                sb.append(zf4.getExtension());
            } else if (!dg4.s.equals("")) {
                sb.append(dg4.s);
                sb.append(zf4.getExtension());
            } else {
                sb.append(" ext. ");
                sb.append(zf4.getExtension());
            }
        }
    }

    public dg4 a(String str) {
        if (!b(str)) {
            return null;
        }
        return this.a.a(str);
    }

    public dg4 a(int i2) {
        if (!this.b.containsKey(Integer.valueOf(i2))) {
            return null;
        }
        return this.a.a(i2);
    }

    public final e a(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            return e.IS_POSSIBLE;
        }
        if (matcher.lookingAt()) {
            return e.TOO_LONG;
        }
        return e.TOO_SHORT;
    }

    public final boolean a(dg4 dg4, String str) {
        return a(this.d.a(dg4.a.b), str) == e.TOO_SHORT;
    }

    public int a(StringBuilder sb, StringBuilder sb2) {
        if (!(sb.length() == 0 || sb.charAt(0) == '0')) {
            int length = sb.length();
            int i2 = 1;
            while (i2 <= 3 && i2 <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i2));
                if (this.b.containsKey(Integer.valueOf(parseInt))) {
                    sb2.append(sb.substring(i2));
                    return parseInt;
                }
                i2++;
            }
        }
        return 0;
    }

    public int a(String str, dg4 dg4, StringBuilder sb, boolean z, zf4 zf4) throws xf4 {
        if (str.length() == 0) {
            return 0;
        }
        StringBuilder sb2 = new StringBuilder(str);
        zf4.a a2 = a(sb2, dg4 != null ? dg4.r : "NonMatch");
        if (z) {
            zf4.setCountryCodeSource(a2);
        }
        if (a2 == zf4.a.FROM_DEFAULT_COUNTRY) {
            if (dg4 != null) {
                int i2 = dg4.q;
                String valueOf = String.valueOf(i2);
                String sb3 = sb2.toString();
                if (sb3.startsWith(valueOf)) {
                    StringBuilder sb4 = new StringBuilder(sb3.substring(valueOf.length()));
                    fg4 fg4 = dg4.a;
                    Pattern a3 = this.d.a(fg4.a);
                    a(sb4, dg4, (StringBuilder) null);
                    Pattern a4 = this.d.a(fg4.b);
                    if ((!a3.matcher(sb2).matches() && a3.matcher(sb4).matches()) || a(a4, sb2.toString()) == e.TOO_LONG) {
                        sb.append((CharSequence) sb4);
                        if (z) {
                            zf4.setCountryCodeSource(zf4.a.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        zf4.setCountryCode(i2);
                        return i2;
                    }
                }
            }
            zf4.setCountryCode(0);
            return 0;
        } else if (sb2.length() > 2) {
            int a5 = a(sb2, sb);
            if (a5 != 0) {
                zf4.setCountryCode(a5);
                return a5;
            }
            throw new xf4(xf4.a.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        } else {
            throw new xf4(xf4.a.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        }
    }

    public final boolean a(Pattern pattern, StringBuilder sb) {
        Matcher matcher = pattern.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = n.matcher(sb.substring(end));
        if (matcher2.find() && g(matcher2.group(1)).equals("0")) {
            return false;
        }
        sb.delete(0, end);
        return true;
    }

    public zf4.a a(StringBuilder sb, String str) {
        if (sb.length() == 0) {
            return zf4.a.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = l.matcher(sb);
        if (matcher.lookingAt()) {
            sb.delete(0, matcher.end());
            b(sb);
            return zf4.a.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern a2 = this.d.a(str);
        b(sb);
        return a(a2, sb) ? zf4.a.FROM_NUMBER_WITH_IDD : zf4.a.FROM_DEFAULT_COUNTRY;
    }

    public boolean a(StringBuilder sb, dg4 dg4, StringBuilder sb2) {
        int length = sb.length();
        String str = dg4.t;
        if (!(length == 0 || str.length() == 0)) {
            Matcher matcher = this.d.a(str).matcher(sb);
            if (matcher.lookingAt()) {
                Pattern a2 = this.d.a(dg4.a.a);
                boolean matches = a2.matcher(sb).matches();
                int groupCount = matcher.groupCount();
                String str2 = dg4.u;
                if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
                    StringBuilder sb3 = new StringBuilder(sb);
                    sb3.replace(0, length, matcher.replaceFirst(str2));
                    if (matches && !a2.matcher(sb3.toString()).matches()) {
                        return false;
                    }
                    if (sb2 != null && groupCount > 1) {
                        sb2.append(matcher.group(1));
                    }
                    sb.replace(0, sb.length(), sb3.toString());
                    return true;
                } else if (matches && !a2.matcher(sb.substring(matcher.end())).matches()) {
                    return false;
                } else {
                    if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                        sb2.append(matcher.group(1));
                    }
                    sb.delete(0, matcher.end());
                    return true;
                }
            }
        }
        return false;
    }

    public String a(StringBuilder sb) {
        Matcher matcher = u.matcher(sb);
        if (!matcher.find() || !e(sb.substring(0, matcher.start()))) {
            return "";
        }
        int groupCount = matcher.groupCount();
        for (int i2 = 1; i2 <= groupCount; i2++) {
            if (matcher.group(i2) != null) {
                String group = matcher.group(i2);
                sb.delete(matcher.start(), sb.length());
                return group;
            }
        }
        return "";
    }

    public final boolean a(String str, String str2) {
        if (!b(str2)) {
            return (str == null || str.length() == 0 || !l.matcher(str).lookingAt()) ? false : true;
        }
        return true;
    }

    public void a(String str, String str2, zf4 zf4) throws xf4 {
        a(str, str2, false, true, zf4);
    }

    public static void a(String str, zf4 zf4) {
        if (str.length() > 1 && str.charAt(0) == '0') {
            zf4.setItalianLeadingZero(true);
            int i2 = 1;
            while (i2 < str.length() - 1 && str.charAt(i2) == '0') {
                i2++;
            }
            if (i2 != 1) {
                zf4.setNumberOfLeadingZeros(i2);
            }
        }
    }

    public final void a(String str, String str2, boolean z, boolean z2, zf4 zf4) throws xf4 {
        int i2;
        if (str == null) {
            throw new xf4(xf4.a.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (str.length() <= 250) {
            StringBuilder sb = new StringBuilder();
            a(str, sb);
            if (!e(sb.toString())) {
                throw new xf4(xf4.a.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (!z2 || a(sb.toString(), str2)) {
                if (z) {
                    zf4.setRawInput(str);
                }
                String a2 = a(sb);
                if (a2.length() > 0) {
                    zf4.setExtension(a2);
                }
                dg4 a3 = a(str2);
                StringBuilder sb2 = new StringBuilder();
                try {
                    i2 = a(sb.toString(), a3, sb2, z, zf4);
                } catch (xf4 e2) {
                    Matcher matcher = l.matcher(sb.toString());
                    if (e2.getErrorType() != xf4.a.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new xf4(e2.getErrorType(), e2.getMessage());
                    }
                    i2 = a(sb.substring(matcher.end()), a3, sb2, z, zf4);
                    if (i2 == 0) {
                        throw new xf4(xf4.a.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (i2 != 0) {
                    String b2 = b(i2);
                    if (!b2.equals(str2)) {
                        a3 = a(i2, b2);
                    }
                } else {
                    b(sb);
                    sb2.append((CharSequence) sb);
                    if (str2 != null) {
                        zf4.setCountryCode(a3.q);
                    } else if (z) {
                        zf4.clearCountryCodeSource();
                    }
                }
                if (sb2.length() >= 2) {
                    if (a3 != null) {
                        StringBuilder sb3 = new StringBuilder();
                        StringBuilder sb4 = new StringBuilder(sb2);
                        a(sb4, a3, sb3);
                        if (!a(a3, sb4.toString())) {
                            if (z) {
                                zf4.setPreferredDomesticCarrierCode(sb3.toString());
                            }
                            sb2 = sb4;
                        }
                    }
                    int length = sb2.length();
                    if (length < 2) {
                        throw new xf4(xf4.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                    } else if (length <= 17) {
                        a(sb2.toString(), zf4);
                        zf4.setNationalNumber(Long.parseLong(sb2.toString()));
                    } else {
                        throw new xf4(xf4.a.TOO_LONG, "The string supplied is too long to be a phone number.");
                    }
                } else {
                    throw new xf4(xf4.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
            } else {
                throw new xf4(xf4.a.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        } else {
            throw new xf4(xf4.a.TOO_LONG, "The string supplied was too long to parse.");
        }
    }

    public final void a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf > 0) {
            int i2 = indexOf + 15;
            if (str.charAt(i2) == '+') {
                int indexOf2 = str.indexOf(59, i2);
                if (indexOf2 > 0) {
                    sb.append(str.substring(i2, indexOf2));
                } else {
                    sb.append(str.substring(i2));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + 4 : 0, indexOf));
        } else {
            sb.append(d(str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }
}
