package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class co6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public co6(InAppNotificationRepository inAppNotificationRepository) {
        ee7.b(inAppNotificationRepository, "repository");
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(tn4 tn4, Context context) {
        String str;
        String str2;
        int i;
        String str3;
        String str4;
        int i2;
        String a2;
        String b;
        String a3;
        String b2;
        String a4;
        String e;
        String a5;
        String c;
        String e2;
        String a6;
        String b3;
        ee7.b(tn4, "notification");
        ee7.b(context, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("InAppNotificationManager", "notificationId: " + tn4.c());
        String e3 = tn4.e();
        int hashCode = e3.hashCode();
        if (hashCode == 1433363166 ? !e3.equals("Title_Send_Friend_Request") : hashCode != 1898363949 || !e3.equals("Title_Send_Invitation_Challenge")) {
            String e4 = tn4.e();
            String e5 = tn4.e();
            int hashCode2 = e5.hashCode();
            String str5 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            String str6 = "Challenge";
            String str7 = null;
            int i3 = 99;
            switch (hashCode2) {
                case -1559094286:
                    if (e5.equals("Title_Notification_End_Challenge")) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887169);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        we7 we7 = we7.a;
                        String a7 = ig5.a(PortfolioApp.g0.c(), 2131886190);
                        ee7.a((Object) a7, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        Object[] objArr = new Object[1];
                        pn4 b4 = tn4.b();
                        if (!(b4 == null || (b = b4.b()) == null)) {
                            str6 = b;
                        }
                        objArr[0] = str6;
                        str4 = String.format(a7, Arrays.copyOf(objArr, 1));
                        ee7.a((Object) str4, "java.lang.String.format(format, *args)");
                        pn4 b5 = tn4.b();
                        if (!(b5 == null || (a2 = b5.a()) == null)) {
                            i2 = a2.hashCode();
                            i3 = i2;
                        }
                        str2 = str3;
                        str = str4;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                case -764698119:
                    if (e5.equals("Title_Notification_Start_Challenge")) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887172);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        we7 we72 = we7.a;
                        String a8 = ig5.a(PortfolioApp.g0.c(), 2131886193);
                        ee7.a((Object) a8, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        Object[] objArr2 = new Object[1];
                        pn4 b6 = tn4.b();
                        if (!(b6 == null || (b2 = b6.b()) == null)) {
                            str6 = b2;
                        }
                        objArr2[0] = str6;
                        str4 = String.format(a8, Arrays.copyOf(objArr2, 1));
                        ee7.a((Object) str4, "java.lang.String.format(format, *args)");
                        pn4 b7 = tn4.b();
                        if (!(b7 == null || (a3 = b7.a()) == null)) {
                            i2 = a3.hashCode();
                            i3 = i2;
                        }
                        str2 = str3;
                        str = str4;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                case -473527954:
                    if (e5.equals("Title_Respond_Invitation_Challenge")) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887173);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        fu4 fu4 = fu4.a;
                        io4 d = tn4.d();
                        String b8 = d != null ? d.b() : null;
                        io4 d2 = tn4.d();
                        if (d2 != null) {
                            str7 = d2.d();
                        }
                        io4 d3 = tn4.d();
                        if (!(d3 == null || (e = d3.e()) == null)) {
                            str5 = e;
                        }
                        String a9 = fu4.a(b8, str7, str5);
                        we7 we73 = we7.a;
                        String a10 = ig5.a(PortfolioApp.g0.c(), 2131886194);
                        ee7.a((Object) a10, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        str4 = String.format(a10, Arrays.copyOf(new Object[]{a9}, 1));
                        ee7.a((Object) str4, "java.lang.String.format(format, *args)");
                        pn4 b9 = tn4.b();
                        if (!(b9 == null || (a4 = b9.a()) == null)) {
                            i2 = a4.hashCode();
                            i3 = i2;
                        }
                        str2 = str3;
                        str = str4;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                case 238453777:
                    if (e5.equals("Title_Notification_Reached_Goal_Challenge")) {
                        String a11 = ig5.a(PortfolioApp.g0.c(), 2131887170);
                        ee7.a((Object) a11, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        String a12 = ig5.a(PortfolioApp.g0.c(), 2131886191);
                        ee7.a((Object) a12, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        pn4 b10 = tn4.b();
                        if (!(b10 == null || (a5 = b10.a()) == null)) {
                            i3 = a5.hashCode();
                        }
                        str2 = a11;
                        str = a12;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                case 634854495:
                    if (e5.equals("Title_Accepted_Friend_Request")) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887168);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        fu4 fu42 = fu4.a;
                        io4 d4 = tn4.d();
                        String b11 = d4 != null ? d4.b() : null;
                        io4 d5 = tn4.d();
                        if (d5 != null) {
                            str7 = d5.d();
                        }
                        io4 d6 = tn4.d();
                        if (!(d6 == null || (e2 = d6.e()) == null)) {
                            str5 = e2;
                        }
                        String a13 = fu42.a(b11, str7, str5);
                        we7 we74 = we7.a;
                        String a14 = ig5.a(PortfolioApp.g0.c(), 2131886189);
                        ee7.a((Object) a14, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        str4 = String.format(a14, Arrays.copyOf(new Object[]{a13}, 1));
                        ee7.a((Object) str4, "java.lang.String.format(format, *args)");
                        io4 d7 = tn4.d();
                        if (!(d7 == null || (c = d7.c()) == null)) {
                            i2 = c.hashCode();
                            i3 = i2;
                        }
                        str2 = str3;
                        str = str4;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                case 1669546642:
                    if (e5.equals("Title_Notification_Remind_End_Challenge")) {
                        str3 = ig5.a(PortfolioApp.g0.c(), 2131887169);
                        ee7.a((Object) str3, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        we7 we75 = we7.a;
                        String a15 = ig5.a(PortfolioApp.g0.c(), 2131886192);
                        ee7.a((Object) a15, "LanguageHelper.getString\u2026ion_Remind_End_Challenge)");
                        Object[] objArr3 = new Object[1];
                        pn4 b12 = tn4.b();
                        if (!(b12 == null || (b3 = b12.b()) == null)) {
                            str6 = b3;
                        }
                        objArr3[0] = str6;
                        str4 = String.format(a15, Arrays.copyOf(objArr3, 1));
                        ee7.a((Object) str4, "java.lang.String.format(format, *args)");
                        pn4 b13 = tn4.b();
                        if (!(b13 == null || (a6 = b13.a()) == null)) {
                            i2 = a6.hashCode();
                            i3 = i2;
                        }
                        str2 = str3;
                        str = str4;
                        i = i3;
                        break;
                    }
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
                default:
                    str2 = "";
                    str = e4;
                    i = 99;
                    break;
            }
            qe5.c.a(PortfolioApp.g0.c(), i, str2, str, null, null);
            return;
        }
        b(tn4, context);
    }

    @DexIgnore
    public final void b(tn4 tn4, Context context) {
        qe5.c.b(tn4, context);
    }
}
