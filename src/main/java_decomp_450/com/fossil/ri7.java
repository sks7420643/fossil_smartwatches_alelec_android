package com.fossil;

import com.fossil.ri7;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ri7<T extends Throwable & ri7<T>> {
    @DexIgnore
    T createCopy();
}
