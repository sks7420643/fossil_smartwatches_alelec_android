package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv5 implements Factory<kv5> {
    @DexIgnore
    public static kv5 a(iv5 iv5, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new kv5(iv5, remindersSettingsDatabase);
    }
}
