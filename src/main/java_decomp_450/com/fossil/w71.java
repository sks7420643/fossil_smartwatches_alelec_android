package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w71 extends eo0 {
    @DexIgnore
    public int j;

    @DexIgnore
    public w71(cx0 cx0) {
        super(aq0.i, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.g();
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return s91 instanceof gd1;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.h;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.j = ((gd1) s91).b;
    }
}
