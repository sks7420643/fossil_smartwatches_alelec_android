package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no1 {
    @DexIgnore
    public static /* final */ om1 b; // = new om1(null);
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic a;

    @DexIgnore
    public no1(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        this.a = bluetoothGattCharacteristic;
        om1 om1 = b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        ee7.a((Object) uuid, "this.bluetoothGattCharacteristic.uuid");
        om1.a(uuid);
    }
}
