package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw7 {
    @DexIgnore
    public Double a;
    @DexIgnore
    public Double b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ long k;

    @DexIgnore
    public zw7(String str, String str2, long j2, long j3, int i2, int i3, int i4, String str3, long j4) {
        ee7.b(str, "id");
        ee7.b(str2, "path");
        ee7.b(str3, "displayName");
        this.c = str;
        this.d = str2;
        this.e = j2;
        this.f = j3;
        this.g = i2;
        this.h = i3;
        this.i = i4;
        this.j = str3;
        this.k = j4;
    }

    @DexIgnore
    public final long a() {
        return this.f;
    }

    @DexIgnore
    public final String b() {
        return this.j;
    }

    @DexIgnore
    public final long c() {
        return this.e;
    }

    @DexIgnore
    public final int d() {
        return this.h;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zw7) {
                zw7 zw7 = (zw7) obj;
                if (ee7.a((Object) this.c, (Object) zw7.c) && ee7.a((Object) this.d, (Object) zw7.d)) {
                    if (this.e == zw7.e) {
                        if (this.f == zw7.f) {
                            if (this.g == zw7.g) {
                                if (this.h == zw7.h) {
                                    if ((this.i == zw7.i) && ee7.a((Object) this.j, (Object) zw7.j)) {
                                        if (this.k == zw7.k) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Double f() {
        return this.a;
    }

    @DexIgnore
    public final Double g() {
        return this.b;
    }

    @DexIgnore
    public final long h() {
        return this.k;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.c;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.d;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        long j2 = this.e;
        long j3 = this.f;
        int i3 = (((((((((((hashCode + hashCode2) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.g) * 31) + this.h) * 31) + this.i) * 31;
        String str3 = this.j;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        long j4 = this.k;
        return ((i3 + i2) * 31) + ((int) (j4 ^ (j4 >>> 32)));
    }

    @DexIgnore
    public final String i() {
        return this.d;
    }

    @DexIgnore
    public final int j() {
        return this.i;
    }

    @DexIgnore
    public final int k() {
        return this.g;
    }

    @DexIgnore
    public String toString() {
        return "AssetEntity(id=" + this.c + ", path=" + this.d + ", duration=" + this.e + ", createDt=" + this.f + ", width=" + this.g + ", height=" + this.h + ", type=" + this.i + ", displayName=" + this.j + ", modifiedDate=" + this.k + ")";
    }

    @DexIgnore
    public final void a(Double d2) {
        this.a = d2;
    }

    @DexIgnore
    public final void b(Double d2) {
        this.b = d2;
    }
}
