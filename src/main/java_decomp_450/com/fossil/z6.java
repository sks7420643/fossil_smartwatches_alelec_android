package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.Base64;
import android.util.TypedValue;
import android.util.Xml;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z6 {

    @DexIgnore
    public interface a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements a {
        @DexIgnore
        public /* final */ c[] a;

        @DexIgnore
        public b(c[] cVarArr) {
            this.a = cVarArr;
        }

        @DexIgnore
        public c[] a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public c(String str, int i, boolean z, String str2, int i2, int i3) {
            this.a = str;
            this.b = i;
            this.c = z;
            this.d = str2;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public String a() {
            return this.a;
        }

        @DexIgnore
        public int b() {
            return this.f;
        }

        @DexIgnore
        public int c() {
            return this.e;
        }

        @DexIgnore
        public String d() {
            return this.d;
        }

        @DexIgnore
        public int e() {
            return this.b;
        }

        @DexIgnore
        public boolean f() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements a {
        @DexIgnore
        public /* final */ n8 a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(n8 n8Var, int i, int i2) {
            this.a = n8Var;
            this.c = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public n8 b() {
            return this.a;
        }

        @DexIgnore
        public int c() {
            return this.b;
        }
    }

    @DexIgnore
    public static a a(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        int next;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return b(xmlPullParser, resources);
        }
        throw new XmlPullParserException("No start tag found");
    }

    @DexIgnore
    public static a b(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return c(xmlPullParser, resources);
        }
        a(xmlPullParser);
        return null;
    }

    @DexIgnore
    public static a c(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), f6.FontFamily);
        String string = obtainAttributes.getString(f6.FontFamily_fontProviderAuthority);
        String string2 = obtainAttributes.getString(f6.FontFamily_fontProviderPackage);
        String string3 = obtainAttributes.getString(f6.FontFamily_fontProviderQuery);
        int resourceId = obtainAttributes.getResourceId(f6.FontFamily_fontProviderCerts, 0);
        int integer = obtainAttributes.getInteger(f6.FontFamily_fontProviderFetchStrategy, 1);
        int integer2 = obtainAttributes.getInteger(f6.FontFamily_fontProviderFetchTimeout, 500);
        obtainAttributes.recycle();
        if (string == null || string2 == null || string3 == null) {
            ArrayList arrayList = new ArrayList();
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (xmlPullParser.getName().equals("font")) {
                        arrayList.add(d(xmlPullParser, resources));
                    } else {
                        a(xmlPullParser);
                    }
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            return new b((c[]) arrayList.toArray(new c[arrayList.size()]));
        }
        while (xmlPullParser.next() != 3) {
            a(xmlPullParser);
        }
        return new d(new n8(string, string2, string3, a(resources, resourceId)), integer, integer2);
    }

    @DexIgnore
    public static c d(XmlPullParser xmlPullParser, Resources resources) throws XmlPullParserException, IOException {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), f6.FontFamilyFont);
        if (obtainAttributes.hasValue(f6.FontFamilyFont_fontWeight)) {
            i = f6.FontFamilyFont_fontWeight;
        } else {
            i = f6.FontFamilyFont_android_fontWeight;
        }
        int i6 = obtainAttributes.getInt(i, MFNetworkReturnCode.BAD_REQUEST);
        if (obtainAttributes.hasValue(f6.FontFamilyFont_fontStyle)) {
            i2 = f6.FontFamilyFont_fontStyle;
        } else {
            i2 = f6.FontFamilyFont_android_fontStyle;
        }
        boolean z = 1 == obtainAttributes.getInt(i2, 0);
        if (obtainAttributes.hasValue(f6.FontFamilyFont_ttcIndex)) {
            i3 = f6.FontFamilyFont_ttcIndex;
        } else {
            i3 = f6.FontFamilyFont_android_ttcIndex;
        }
        if (obtainAttributes.hasValue(f6.FontFamilyFont_fontVariationSettings)) {
            i4 = f6.FontFamilyFont_fontVariationSettings;
        } else {
            i4 = f6.FontFamilyFont_android_fontVariationSettings;
        }
        String string = obtainAttributes.getString(i4);
        int i7 = obtainAttributes.getInt(i3, 0);
        if (obtainAttributes.hasValue(f6.FontFamilyFont_font)) {
            i5 = f6.FontFamilyFont_font;
        } else {
            i5 = f6.FontFamilyFont_android_font;
        }
        int resourceId = obtainAttributes.getResourceId(i5, 0);
        String string2 = obtainAttributes.getString(i5);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            a(xmlPullParser);
        }
        return new c(string2, i6, z, string, i7, resourceId);
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return typedArray.getType(i);
        }
        TypedValue typedValue = new TypedValue();
        typedArray.getValue(i, typedValue);
        return typedValue.type;
    }

    @DexIgnore
    public static List<List<byte[]>> a(Resources resources, int i) {
        if (i == 0) {
            return Collections.emptyList();
        }
        TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            if (a(obtainTypedArray, 0) == 1) {
                for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                    int resourceId = obtainTypedArray.getResourceId(i2, 0);
                    if (resourceId != 0) {
                        arrayList.add(a(resources.getStringArray(resourceId)));
                    }
                }
            } else {
                arrayList.add(a(resources.getStringArray(i)));
            }
            obtainTypedArray.recycle();
            return arrayList;
        } finally {
            obtainTypedArray.recycle();
        }
    }

    @DexIgnore
    public static List<byte[]> a(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            arrayList.add(Base64.decode(str, 0));
        }
        return arrayList;
    }

    @DexIgnore
    public static void a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            }
        }
    }
}
