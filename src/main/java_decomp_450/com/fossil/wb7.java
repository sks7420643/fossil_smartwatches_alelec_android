package com.fossil;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb7 {
    @DexIgnore
    public static /* final */ a a; // = new a(null, null, null);
    @DexIgnore
    public static a b;
    @DexIgnore
    public static /* final */ wb7 c; // = new wb7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public a(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    @DexIgnore
    public final a a(ob7 ob7) {
        try {
            a aVar = new a(Class.class.getDeclaredMethod("getModule", new Class[0]), ob7.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), ob7.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", new Class[0]));
            b = aVar;
            return aVar;
        } catch (Exception unused) {
            a aVar2 = a;
            b = aVar2;
            return aVar2;
        }
    }

    @DexIgnore
    public final String b(ob7 ob7) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        ee7.b(ob7, "continuation");
        a aVar = b;
        if (aVar == null) {
            aVar = a(ob7);
        }
        String str = null;
        if (aVar == a || (method = aVar.a) == null || (invoke = method.invoke(ob7.getClass(), new Object[0])) == null || (method2 = aVar.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = aVar.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        if (invoke3 instanceof String) {
            str = invoke3;
        }
        return str;
    }
}
