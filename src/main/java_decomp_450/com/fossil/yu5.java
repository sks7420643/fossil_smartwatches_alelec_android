package com.fossil;

import com.fossil.ql4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu5 extends ql4<b, c, ql4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.b {
        @DexIgnore
        public /* final */ List<bt5> a;
        @DexIgnore
        public /* final */ List<bt5> b;

        @DexIgnore
        public b(List<bt5> list, List<bt5> list2) {
            ee7.b(list, "contactWrapperListRemoved");
            ee7.b(list2, "contactWrapperListAdded");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<bt5> a() {
            return this.b;
        }

        @DexIgnore
        public final List<bt5> b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a(null);
        String simpleName = yu5.class.getSimpleName();
        ee7.a((Object) simpleName, "SaveContactGroupsNotific\u2026on::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public yu5(NotificationsRepository notificationsRepository) {
        ee7.b(notificationsRepository, "notificationsRepository");
        jw3.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        ee7.a((Object) notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0273 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(java.util.List<com.fossil.bt5> r18) {
        /*
            r17 = this;
            r1 = r17
            boolean r0 = r18.isEmpty()
            if (r0 == 0) goto L_0x0009
            return
        L_0x0009:
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r2 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_DIANA
            int r2 = r2.getValue()
            java.util.List r2 = r0.getAllContactGroups(r2)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            com.fossil.ah5$a r0 = com.fossil.ah5.p
            com.fossil.ah5 r0 = r0.a()
            com.fossil.wearables.fsl.contact.ContactProvider r7 = r0.b()
            java.util.Iterator r8 = r18.iterator()
        L_0x0037:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0278
            java.lang.Object r0 = r8.next()
            com.fossil.bt5 r0 = (com.fossil.bt5) r0
            if (r2 == 0) goto L_0x015f
            java.util.Iterator r11 = r2.iterator()
            r12 = 0
        L_0x004a:
            boolean r13 = r11.hasNext()
            if (r13 == 0) goto L_0x015f
            java.lang.Object r13 = r11.next()
            com.fossil.wearables.fsl.contact.ContactGroup r13 = (com.fossil.wearables.fsl.contact.ContactGroup) r13
            java.lang.String r14 = "contactGroup"
            com.fossil.ee7.a(r13, r14)
            java.util.List r14 = r13.getContacts()
            java.util.Iterator r14 = r14.iterator()
        L_0x0063:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto L_0x0156
            java.lang.Object r15 = r14.next()
            com.fossil.wearables.fsl.contact.Contact r15 = (com.fossil.wearables.fsl.contact.Contact) r15
            com.fossil.wearables.fsl.contact.Contact r16 = r0.getContact()
            if (r16 == 0) goto L_0x0150
            if (r15 == 0) goto L_0x0150
            int r9 = r15.getContactId()
            com.fossil.wearables.fsl.contact.Contact r16 = r0.getContact()
            if (r16 == 0) goto L_0x0150
            int r10 = r16.getContactId()
            if (r9 != r10) goto L_0x0150
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x0096
            boolean r9 = r9.isUseCall()
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            goto L_0x0097
        L_0x0096:
            r9 = 0
        L_0x0097:
            if (r9 == 0) goto L_0x014b
            boolean r9 = r9.booleanValue()
            r15.setUseCall(r9)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x00af
            boolean r9 = r9.isUseSms()
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)
            goto L_0x00b0
        L_0x00af:
            r9 = 0
        L_0x00b0:
            if (r9 == 0) goto L_0x0146
            boolean r9 = r9.booleanValue()
            r15.setUseSms(r9)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x00c4
            java.lang.String r9 = r9.getFirstName()
            goto L_0x00c5
        L_0x00c4:
            r9 = 0
        L_0x00c5:
            r15.setFirstName(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.yu5.e
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r14 = "Contact Id = "
            r12.append(r14)
            com.fossil.wearables.fsl.contact.Contact r14 = r0.getContact()
            if (r14 == 0) goto L_0x00e9
            int r14 = r14.getContactId()
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            goto L_0x00ea
        L_0x00e9:
            r14 = 0
        L_0x00ea:
            r12.append(r14)
            java.lang.String r14 = ", "
            r12.append(r14)
            r16 = r8
            java.lang.String r8 = "Contact name = "
            r12.append(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x0104
            java.lang.String r8 = r8.getFirstName()
            goto L_0x0105
        L_0x0104:
            r8 = 0
        L_0x0105:
            r12.append(r8)
            r8 = 44
            r12.append(r8)
            java.lang.String r8 = "Contact db row = "
            r12.append(r8)
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x0121
            int r8 = r8.getDbRowId()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            goto L_0x0122
        L_0x0121:
            r8 = 0
        L_0x0122:
            r12.append(r8)
            r12.append(r14)
            java.lang.String r8 = "Contact phone = "
            r12.append(r8)
            java.lang.String r8 = r0.getPhoneNumber()
            r12.append(r8)
            java.lang.String r8 = r12.toString()
            r9.d(r10, r8)
            r0.setContact(r15)
            r7.removeContactGroup(r13)
            r2.remove(r13)
            r12 = 1
            goto L_0x0158
        L_0x0146:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x014b:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        L_0x0150:
            r16 = r8
            r8 = r16
            goto L_0x0063
        L_0x0156:
            r16 = r8
        L_0x0158:
            if (r12 == 0) goto L_0x015b
            goto L_0x0161
        L_0x015b:
            r8 = r16
            goto L_0x004a
        L_0x015f:
            r16 = r8
        L_0x0161:
            com.fossil.wearables.fsl.contact.ContactGroup r8 = new com.fossil.wearables.fsl.contact.ContactGroup
            r8.<init>()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r9 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_DIANA
            int r9 = r9.getValue()
            r8.setDeviceFamily(r9)
            int r9 = r0.getCurrentHandGroup()
            r8.setHour(r9)
            r4.add(r8)
            com.fossil.wearables.fsl.contact.Contact r9 = r0.getContact()
            if (r9 == 0) goto L_0x0182
            r9.setContactGroup(r8)
        L_0x0182:
            if (r9 == 0) goto L_0x01a3
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x0193
            boolean r8 = r8.isUseCall()
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)
            goto L_0x0194
        L_0x0193:
            r8 = 0
        L_0x0194:
            if (r8 == 0) goto L_0x019e
            boolean r8 = r8.booleanValue()
            r9.setUseCall(r8)
            goto L_0x01a3
        L_0x019e:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x01a3:
            if (r9 == 0) goto L_0x01c4
            com.fossil.wearables.fsl.contact.Contact r8 = r0.getContact()
            if (r8 == 0) goto L_0x01b4
            boolean r8 = r8.isUseSms()
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)
            goto L_0x01b5
        L_0x01b4:
            r8 = 0
        L_0x01b5:
            if (r8 == 0) goto L_0x01bf
            boolean r8 = r8.booleanValue()
            r9.setUseSms(r8)
            goto L_0x01c4
        L_0x01bf:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x01c4:
            if (r9 == 0) goto L_0x01ca
            r8 = 0
            r9.setUseEmail(r8)
        L_0x01ca:
            if (r9 == 0) goto L_0x0273
            r3.add(r9)
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            android.content.ContentResolver r10 = r8.getContentResolver()
            boolean r8 = r0.hasPhoneNumber()
            if (r8 == 0) goto L_0x026f
            android.net.Uri r11 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            java.lang.String r8 = "data1"
            java.lang.String[] r12 = new java.lang.String[]{r8}
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "contact_id="
            r13.append(r14)
            int r14 = r9.getContactId()
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r14 = 0
            r15 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15)
            if (r10 == 0) goto L_0x026f
        L_0x0204:
            boolean r11 = r10.moveToNext()     // Catch:{ Exception -> 0x0243 }
            if (r11 == 0) goto L_0x023d
            com.fossil.wearables.fsl.contact.PhoneNumber r11 = new com.fossil.wearables.fsl.contact.PhoneNumber     // Catch:{ Exception -> 0x0243 }
            r11.<init>()     // Catch:{ Exception -> 0x0243 }
            int r12 = r10.getColumnIndex(r8)     // Catch:{ Exception -> 0x0243 }
            java.lang.String r12 = r10.getString(r12)     // Catch:{ Exception -> 0x0243 }
            r11.setNumber(r12)     // Catch:{ Exception -> 0x0243 }
            r11.setContact(r9)     // Catch:{ Exception -> 0x0243 }
            java.lang.Boolean r12 = com.fossil.yx6.a(r5, r11)     // Catch:{ Exception -> 0x0243 }
            boolean r12 = r12.booleanValue()     // Catch:{ Exception -> 0x0243 }
            if (r12 != 0) goto L_0x022a
            r5.add(r11)     // Catch:{ Exception -> 0x0243 }
        L_0x022a:
            boolean r12 = r0.isFavorites()     // Catch:{ Exception -> 0x0243 }
            if (r12 == 0) goto L_0x0204
            com.portfolio.platform.data.model.PhoneFavoritesContact r12 = new com.portfolio.platform.data.model.PhoneFavoritesContact     // Catch:{ Exception -> 0x0243 }
            java.lang.String r11 = r11.getNumber()     // Catch:{ Exception -> 0x0243 }
            r12.<init>(r11)     // Catch:{ Exception -> 0x0243 }
            r6.add(r12)     // Catch:{ Exception -> 0x0243 }
            goto L_0x0204
        L_0x023d:
            r10.close()
            goto L_0x026f
        L_0x0241:
            r0 = move-exception
            goto L_0x026b
        L_0x0243:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0241 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ all -> 0x0241 }
            java.lang.String r9 = com.fossil.yu5.e     // Catch:{ all -> 0x0241 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0241 }
            r11.<init>()     // Catch:{ all -> 0x0241 }
            java.lang.String r12 = "Error Inside "
            r11.append(r12)     // Catch:{ all -> 0x0241 }
            java.lang.String r12 = com.fossil.yu5.e     // Catch:{ all -> 0x0241 }
            r11.append(r12)     // Catch:{ all -> 0x0241 }
            java.lang.String r12 = ".saveContactToFSL - ex="
            r11.append(r12)     // Catch:{ all -> 0x0241 }
            r11.append(r0)     // Catch:{ all -> 0x0241 }
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x0241 }
            r8.e(r9, r0)     // Catch:{ all -> 0x0241 }
            goto L_0x023d
        L_0x026b:
            r10.close()
            throw r0
        L_0x026f:
            r8 = r16
            goto L_0x0037
        L_0x0273:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0278:
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveContactGroupList(r4)
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveListContact(r3)
            com.portfolio.platform.data.source.NotificationsRepository r0 = r1.d
            r0.saveListPhoneNumber(r5)
            r1.d(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yu5.c(java.util.List):void");
    }

    @DexIgnore
    public final void d(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        ee7.b(bVar, "requestValues");
        a(bVar.b());
        c(bVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveContactGroupsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<bt5> list) {
        if (!list.isEmpty()) {
            ContactProvider b2 = ah5.p.a().b();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (bt5 bt5 : list) {
                Contact contact = bt5.getContact();
                if (contact != null) {
                    arrayList2.add(contact);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    local.d(str, "Removed contact = " + contact.getFirstName() + " row id = " + contact.getDbRowId());
                    for (ContactGroup contactGroup : b2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue())) {
                        ee7.a((Object) contactGroup, "contactGroupItem");
                        Iterator<Contact> it = contactGroup.getContacts().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Contact next = it.next();
                            int contactId = contact.getContactId();
                            ee7.a((Object) next, "contactItem");
                            if (contactId == next.getContactId()) {
                                contact.setDbRowId(next.getDbRowId());
                                arrayList.add(contactGroup);
                                break;
                            }
                        }
                    }
                    for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
                        ee7.a((Object) phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                        arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            b(arrayList3);
        }
    }
}
