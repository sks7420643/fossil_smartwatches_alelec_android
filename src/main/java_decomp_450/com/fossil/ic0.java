package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutRunningHistory;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ int[] f;
    @DexIgnore
    public /* final */ ArrayList<WorkoutRunningHistory> g; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ic0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ic0 createFromParcel(Parcel parcel) {
            return new ic0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ic0[] newArray(int i) {
            return new ic0[i];
        }
    }

    @DexIgnore
    public ic0(byte b, int i, long j, long j2, int[] iArr) {
        super(cb0.WORKOUT_PAUSE_RUN_SEQUENCE, b, i);
        this.d = j;
        this.f = iArr;
        this.e = j2;
        jf7 a2 = qf7.a(t97.c(iArr), 2);
        int first = a2.getFirst();
        int last = a2.getLast();
        int a3 = a2.a();
        if (a3 >= 0) {
            if (first > last) {
                return;
            }
        } else if (first < last) {
            return;
        }
        while (true) {
            this.g.add(new WorkoutRunningHistory(iArr[first], iArr[first + 1]));
            if (first != last) {
                first += a3;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        JSONObject a2 = yz0.a(super.a(), r51.G5, Long.valueOf(this.d));
        r51 r51 = r51.Q5;
        int[] iArr = this.f;
        JSONArray jSONArray = new JSONArray();
        for (int i : iArr) {
            jSONArray.put(i);
        }
        return yz0.a(yz0.a(a2, r51, jSONArray), r51.L5, Long.valueOf(this.e));
    }

    @DexIgnore
    public final long d() {
        return this.e;
    }

    @DexIgnore
    public final ArrayList<WorkoutRunningHistory> e() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(ic0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            ic0 ic0 = (ic0) obj;
            return this.d == ic0.d && Arrays.equals(this.f, ic0.f) && this.e == ic0.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutPauseRunSequenceRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        int hashCode = Long.valueOf(this.d).hashCode();
        int hashCode2 = this.f.hashCode();
        return Long.valueOf(this.e).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeIntArray(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
    }

    @DexIgnore
    public /* synthetic */ ic0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = parcel.readLong();
        int[] createIntArray = parcel.createIntArray();
        if (createIntArray != null) {
            this.f = createIntArray;
            this.e = parcel.readLong();
            return;
        }
        ee7.a();
        throw null;
    }
}
