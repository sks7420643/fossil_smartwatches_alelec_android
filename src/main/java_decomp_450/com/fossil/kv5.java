package com.fossil;

import android.text.format.DateFormat;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv5 extends hv5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public InactivityNudgeTimeModel g;
    @DexIgnore
    public InactivityNudgeTimeModel h;
    @DexIgnore
    public /* final */ iv5 i;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1", f = "InactivityNudgeTimePresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$1$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kv5$b$a$a")
            /* renamed from: com.fossil.kv5$b$a$a  reason: collision with other inner class name */
            public static final class RunnableC0104a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                public RunnableC0104a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.a.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel c = this.a.this$0.this$0.g;
                    if (c != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(c);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.j.runInTransaction(new RunnableC0104a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kv5 kv5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kv5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2", f = "InactivityNudgeTimePresenter.kt", l = {115}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kv5$c$a$a")
            /* renamed from: com.fossil.kv5$c$a$a  reason: collision with other inner class name */
            public static final class RunnableC0105a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                public RunnableC0105a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                public final void run() {
                    InactivityNudgeTimeDao inactivityNudgeTimeDao = this.a.this$0.this$0.j.getInactivityNudgeTimeDao();
                    InactivityNudgeTimeModel b = this.a.this$0.this$0.h;
                    if (b != null) {
                        inactivityNudgeTimeDao.upsertInactivityNudgeTime(b);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.j.runInTransaction(new RunnableC0105a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kv5 kv5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kv5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1", f = "InactivityNudgeTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1$listInactivityNudgeTimeModel$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends InactivityNudgeTimeModel>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.j.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kv5 kv5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kv5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel : (List) obj) {
                if (inactivityNudgeTimeModel.getNudgeTimeType() == this.this$0.f) {
                    boolean is24HourFormat = DateFormat.is24HourFormat(PortfolioApp.g0.c());
                    this.this$0.i.Q(is24HourFormat);
                    iv5 f = this.this$0.i;
                    kv5 kv5 = this.this$0;
                    f.a(kv5.b(kv5.f));
                    this.this$0.i.a(inactivityNudgeTimeModel.getMinutes(), is24HourFormat);
                }
                if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                    this.this$0.g = inactivityNudgeTimeModel;
                } else {
                    this.this$0.h = inactivityNudgeTimeModel;
                }
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = kv5.class.getSimpleName();
        ee7.a((Object) simpleName, "InactivityNudgeTimePrese\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public kv5(iv5 iv5, RemindersSettingsDatabase remindersSettingsDatabase) {
        ee7.b(iv5, "mView");
        ee7.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.i = iv5;
        this.j = remindersSettingsDatabase;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.hv5
    public void h() {
        if (this.f == 0) {
            int i2 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel = this.h;
            if (inactivityNudgeTimeModel == null) {
                ee7.a();
                throw null;
            } else if (i2 > inactivityNudgeTimeModel.getMinutes()) {
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886115);
                iv5 iv5 = this.i;
                ee7.a((Object) a2, "des");
                iv5.i(a2);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = this.g;
                if (inactivityNudgeTimeModel2 != null) {
                    inactivityNudgeTimeModel2.setMinutes(this.e);
                    ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
                    return;
                }
                ee7.a();
                throw null;
            }
        } else {
            int i3 = this.e;
            InactivityNudgeTimeModel inactivityNudgeTimeModel3 = this.g;
            if (inactivityNudgeTimeModel3 == null) {
                ee7.a();
                throw null;
            } else if (i3 < inactivityNudgeTimeModel3.getMinutes()) {
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886115);
                iv5 iv52 = this.i;
                ee7.a((Object) a3, "des");
                iv52.i(a3);
            } else {
                InactivityNudgeTimeModel inactivityNudgeTimeModel4 = this.h;
                if (inactivityNudgeTimeModel4 != null) {
                    inactivityNudgeTimeModel4.setMinutes(this.e);
                    ik7 unused2 = xh7.b(e(), null, null, new c(this, null), 3, null);
                    return;
                }
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final String b(int i2) {
        if (i2 == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886122);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return a2;
        }
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886120);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return a3;
    }

    @DexIgnore
    @Override // com.fossil.hv5
    public void a(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0085  */
    @Override // com.fossil.hv5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r6, java.lang.String r7, boolean r8) {
        /*
            r5 = this;
            java.lang.String r0 = "hourValue"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r0 = "minuteValue"
            com.fossil.ee7.b(r7, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.kv5.k
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "updateTime: hourValue = "
            r2.append(r3)
            r2.append(r6)
            java.lang.String r3 = ", minuteValue = "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r3 = ", isPM = "
            r2.append(r3)
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            r0 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r1 = "Integer.valueOf(hourValue)"
            com.fossil.ee7.a(r6, r1)     // Catch:{ Exception -> 0x0054 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0054 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0052 }
            java.lang.String r1 = "Integer.valueOf(minuteValue)"
            com.fossil.ee7.a(r7, r1)     // Catch:{ Exception -> 0x0052 }
            int r7 = r7.intValue()     // Catch:{ Exception -> 0x0052 }
            goto L_0x0073
        L_0x0052:
            r7 = move-exception
            goto L_0x0056
        L_0x0054:
            r7 = move-exception
            r6 = 0
        L_0x0056:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.kv5.k
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Exception when parse time e="
            r3.append(r4)
            r3.append(r7)
            java.lang.String r7 = r3.toString()
            r1.e(r2, r7)
            r7 = 0
        L_0x0073:
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            boolean r1 = android.text.format.DateFormat.is24HourFormat(r1)
            if (r1 == 0) goto L_0x0085
            int r6 = r6 * 60
            int r6 = r6 + r7
            r5.e = r6
            goto L_0x009a
        L_0x0085:
            r1 = 12
            if (r8 == 0) goto L_0x0091
            if (r6 != r1) goto L_0x008e
            r0 = 12
            goto L_0x0095
        L_0x008e:
            int r0 = r6 + 12
            goto L_0x0095
        L_0x0091:
            if (r6 != r1) goto L_0x0094
            goto L_0x0095
        L_0x0094:
            r0 = r6
        L_0x0095:
            int r0 = r0 * 60
            int r0 = r0 + r7
            r5.e = r0
        L_0x009a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kv5.a(java.lang.String, java.lang.String, boolean):void");
    }
}
