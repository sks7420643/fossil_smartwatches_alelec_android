package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg4 extends rg4 {
    @DexIgnore
    public static /* final */ pg4 a;

    /*
    static {
        pg4 pg4 = new pg4();
        a = pg4;
        pg4.setStackTrace(rg4.NO_TRACE);
    }
    */

    @DexIgnore
    public pg4() {
    }

    @DexIgnore
    public static pg4 getFormatInstance() {
        return rg4.isStackTrace ? new pg4() : a;
    }

    @DexIgnore
    public pg4(Throwable th) {
        super(th);
    }

    @DexIgnore
    public static pg4 getFormatInstance(Throwable th) {
        return rg4.isStackTrace ? new pg4(th) : a;
    }
}
