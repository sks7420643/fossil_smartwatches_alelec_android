package com.fossil.blesdk.database;

import com.fossil.ci;
import com.fossil.ei;
import com.fossil.pi;
import com.fossil.rc1;
import com.fossil.th;
import com.fossil.ti;
import com.fossil.wi;
import com.fossil.xi;
import com.fossil.zh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SdkDatabase_Impl extends SdkDatabase {
    @DexIgnore
    public volatile rc1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ei.a {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void createAllTables(wi wiVar) {
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS `DeviceFile` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `deviceMacAddress` TEXT NOT NULL, `fileType` INTEGER NOT NULL, `fileIndex` INTEGER NOT NULL, `rawData` BLOB NOT NULL, `fileLength` INTEGER NOT NULL, `fileCrc` INTEGER NOT NULL, `createdTimeStamp` INTEGER NOT NULL, `isCompleted` INTEGER NOT NULL)");
            wiVar.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            wiVar.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f0501e661379ccab31d0b3858cee8e83')");
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void dropAllTables(wi wiVar) {
            wiVar.execSQL("DROP TABLE IF EXISTS `DeviceFile`");
            if (((ci) SdkDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SdkDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SdkDatabase_Impl.this).mCallbacks.get(i)).onDestructiveMigration(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onCreate(wi wiVar) {
            if (((ci) SdkDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SdkDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SdkDatabase_Impl.this).mCallbacks.get(i)).onCreate(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onOpen(wi wiVar) {
            wi unused = ((ci) SdkDatabase_Impl.this).mDatabase = wiVar;
            SdkDatabase_Impl.this.internalInitInvalidationTracker(wiVar);
            if (((ci) SdkDatabase_Impl.this).mCallbacks != null) {
                int size = ((ci) SdkDatabase_Impl.this).mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((ci.b) ((ci) SdkDatabase_Impl.this).mCallbacks.get(i)).onOpen(wiVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPostMigrate(wi wiVar) {
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public void onPreMigrate(wi wiVar) {
            pi.a(wiVar);
        }

        @DexIgnore
        @Override // com.fossil.ei.a
        public ei.b onValidateSchema(wi wiVar) {
            HashMap hashMap = new HashMap(9);
            hashMap.put("id", new ti.a("id", "INTEGER", true, 1, null, 1));
            hashMap.put("deviceMacAddress", new ti.a("deviceMacAddress", "TEXT", true, 0, null, 1));
            hashMap.put("fileType", new ti.a("fileType", "INTEGER", true, 0, null, 1));
            hashMap.put("fileIndex", new ti.a("fileIndex", "INTEGER", true, 0, null, 1));
            hashMap.put("rawData", new ti.a("rawData", "BLOB", true, 0, null, 1));
            hashMap.put("fileLength", new ti.a("fileLength", "INTEGER", true, 0, null, 1));
            hashMap.put("fileCrc", new ti.a("fileCrc", "INTEGER", true, 0, null, 1));
            hashMap.put("createdTimeStamp", new ti.a("createdTimeStamp", "INTEGER", true, 0, null, 1));
            hashMap.put("isCompleted", new ti.a("isCompleted", "INTEGER", true, 0, null, 1));
            ti tiVar = new ti("DeviceFile", hashMap, new HashSet(0), new HashSet(0));
            ti a2 = ti.a(wiVar, "DeviceFile");
            if (tiVar.equals(a2)) {
                return new ei.b(true, null);
            }
            return new ei.b(false, "DeviceFile(com.fossil.blesdk.database.entity.DeviceFile).\n Expected:\n" + tiVar + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public void clearAllTables() {
        super.assertNotMainThread();
        wi writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `DeviceFile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ci
    public zh createInvalidationTracker() {
        return new zh(this, new HashMap(0), new HashMap(0), "DeviceFile");
    }

    @DexIgnore
    @Override // com.fossil.ci
    public xi createOpenHelper(th thVar) {
        ei eiVar = new ei(thVar, new a(4), "f0501e661379ccab31d0b3858cee8e83", "05778a001d8750bf160c2cf361f4e16f");
        xi.b.a a2 = xi.b.a(thVar.b);
        a2.a(thVar.c);
        a2.a(eiVar);
        return thVar.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.fossil.blesdk.database.SdkDatabase
    public rc1 a() {
        rc1 rc1;
        if (this.f != null) {
            return this.f;
        }
        synchronized (this) {
            if (this.f == null) {
                this.f = new rc1(this);
            }
            rc1 = this.f;
        }
        return rc1;
    }
}
