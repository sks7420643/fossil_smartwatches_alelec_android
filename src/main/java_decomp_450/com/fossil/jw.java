package com.fossil;

import com.fossil.jw;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jw<CHILD extends jw<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public g50<? super TranscodeType> a; // = e50.a();

    @DexIgnore
    public final g50<? super TranscodeType> d() {
        return this.a;
    }

    @DexIgnore
    @Override // java.lang.Object
    public final CHILD clone() {
        try {
            return (CHILD) ((jw) super.clone());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
