package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ja2 {
    @DexIgnore
    public static ja2 b; // = new ja2();
    @DexIgnore
    public ia2 a; // = null;

    @DexIgnore
    public static ia2 b(Context context) {
        return b.a(context);
    }

    @DexIgnore
    public final synchronized ia2 a(Context context) {
        if (this.a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.a = new ia2(context);
        }
        return this.a;
    }
}
