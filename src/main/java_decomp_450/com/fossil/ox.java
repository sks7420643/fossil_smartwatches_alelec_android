package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import com.fossil.ix;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ox implements ix<InputStream> {
    @DexIgnore
    public static /* final */ b g; // = new a();
    @DexIgnore
    public /* final */ f00 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public HttpURLConnection d;
    @DexIgnore
    public InputStream e;
    @DexIgnore
    public volatile boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements b {
        @DexIgnore
        @Override // com.fossil.ox.b
        public HttpURLConnection a(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        HttpURLConnection a(URL url) throws IOException;
    }

    @DexIgnore
    public ox(f00 f00, int i) {
        this(f00, i, g);
    }

    @DexIgnore
    public static boolean b(int i) {
        return i / 100 == 3;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a(ew ewVar, ix.a<? super InputStream> aVar) {
        StringBuilder sb;
        long a2 = q50.a();
        try {
            aVar.a(a(this.a.f(), 0, null, this.a.c()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(q50.a(a2));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e2) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e2);
            }
            aVar.a((Exception) e2);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + q50.a(a2));
            }
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void cancel() {
        this.f = true;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public ox(f00 f00, int i, b bVar) {
        this.a = f00;
        this.b = i;
        this.c = bVar;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public sw b() {
        return sw.REMOTE;
    }

    @DexIgnore
    public final InputStream a(URL url, int i, URL url2, Map<String, String> map) throws IOException {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new ww("In re-direct loop");
                    }
                } catch (URISyntaxException unused) {
                }
            }
            this.d = this.c.a(url);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                this.d.addRequestProperty(entry.getKey(), entry.getValue());
            }
            this.d.setConnectTimeout(this.b);
            this.d.setReadTimeout(this.b);
            this.d.setUseCaches(false);
            this.d.setDoInput(true);
            this.d.setInstanceFollowRedirects(false);
            this.d.connect();
            this.e = this.d.getInputStream();
            if (this.f) {
                return null;
            }
            int responseCode = this.d.getResponseCode();
            if (a(responseCode)) {
                return a(this.d);
            }
            if (b(responseCode)) {
                String headerField = this.d.getHeaderField("Location");
                if (!TextUtils.isEmpty(headerField)) {
                    URL url3 = new URL(url, headerField);
                    a();
                    return a(url3, i + 1, url, map);
                }
                throw new ww("Received empty or null redirect url");
            } else if (responseCode == -1) {
                throw new ww(responseCode);
            } else {
                throw new ww(this.d.getResponseMessage(), responseCode);
            }
        } else {
            throw new ww("Too many (> 5) redirects!");
        }
    }

    @DexIgnore
    public static boolean a(int i) {
        return i / 100 == 2;
    }

    @DexIgnore
    public final InputStream a(HttpURLConnection httpURLConnection) throws IOException {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.e = n50.a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.e = httpURLConnection.getInputStream();
        }
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a() {
        InputStream inputStream = this.e;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.d;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.d = null;
    }
}
