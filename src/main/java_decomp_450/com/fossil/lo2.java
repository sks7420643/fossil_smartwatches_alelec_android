package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ o43 h;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lo2(sn2 sn2, String str, String str2, boolean z, o43 o43) {
        super(sn2);
        this.i = sn2;
        this.e = str;
        this.f = str2;
        this.g = z;
        this.h = o43;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.i.h.getUserProperties(this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void b() {
        this.h.a(null);
    }
}
