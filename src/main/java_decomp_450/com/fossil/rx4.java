package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx4 extends qx4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        R.put(2131362636, 2);
        R.put(2131363342, 3);
        R.put(2131362083, 4);
        R.put(2131362637, 5);
        R.put(2131362374, 6);
        R.put(2131362373, 7);
        R.put(2131362751, 8);
        R.put(2131362371, 9);
        R.put(2131362369, 10);
        R.put(2131362401, 11);
        R.put(2131362705, 12);
        R.put(2131362152, 13);
        R.put(2131361889, 14);
        R.put(2131362044, 15);
        R.put(2131362085, 16);
        R.put(2131362886, 17);
        R.put(2131362476, 18);
        R.put(2131362417, 19);
        R.put(2131362180, 20);
        R.put(2131362764, 21);
        R.put(2131363386, 22);
        R.put(2131362465, 23);
        R.put(2131362974, 24);
    }
    */

    @DexIgnore
    public rx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 25, Q, R));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.P != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.P = 1;
        }
        g();
    }

    @DexIgnore
    public rx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (AppBarLayout) objArr[14], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[16], (ConstraintLayout) objArr[1], (CoordinatorLayout) objArr[13], (OverviewDayChart) objArr[20], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[18], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (View) objArr[8], (LinearLayout) objArr[21], (FlexibleProgressBar) objArr[17], (ConstraintLayout) objArr[0], (RecyclerView) objArr[24], (FlexibleTextView) objArr[3], (View) objArr[22]);
        this.P = -1;
        ((qx4) this).L.setTag(null);
        a(view);
        f();
    }
}
