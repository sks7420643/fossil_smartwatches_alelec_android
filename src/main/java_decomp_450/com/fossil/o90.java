package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o90 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ba0 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ z90[] i;
    @DexIgnore
    public /* final */ long j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<o90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public o90 createFromParcel(Parcel parcel) {
            return new o90(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public o90[] newArray(int i) {
            return new o90[i];
        }
    }

    @DexIgnore
    public o90(ba0 ba0, int i2, String str, String str2, String str3, int i3, String str4, z90[] z90Arr, long j2) {
        this.a = ba0;
        this.b = i2;
        this.d = str;
        ik1 ik1 = ik1.a;
        byte[] bytes = str.getBytes(b21.x.c());
        ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        this.c = ik1.a(s97.a(bytes, (byte) 0), ng1.CRC32);
        this.e = yz0.a(str2, 99, null, null, 6);
        this.f = yz0.a(str3, 97, null, null, 6);
        this.g = i3;
        this.h = yz0.a(str4, 249, null, null, 6);
        this.i = z90Arr;
        this.j = j2;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        String str = this.h;
        Charset c2 = b21.x.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            JSONObject a2 = yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.d, yz0.a(this.a)), r51.e, Integer.valueOf(this.b)), r51.V2, this.d), r51.U2, yz0.a((int) this.c)), r51.g, this.e), r51.h, this.f), r51.i, Integer.valueOf(this.g)), r51.v1, Integer.valueOf(this.h.length())), r51.w1, yz0.a((int) ik1.a.a(bytes, ng1.CRC32)));
            r51 r51 = r51.k;
            z90[] z90Arr = this.i;
            JSONArray jSONArray = new JSONArray();
            for (z90 z90 : z90Arr) {
                jSONArray.put(yz0.a(z90));
            }
            return yz0.a(yz0.a(a2, r51, jSONArray), r51.l, Float.valueOf(((float) this.j) / 1000.0f));
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] b() {
        byte b2 = (byte) 12;
        byte b3 = this.a.b();
        byte b4 = 0;
        for (z90 z90 : this.i) {
            b4 = (byte) (b4 | z90.a());
        }
        byte b5 = (byte) 4;
        String a2 = yz0.a(this.e);
        Charset c2 = b21.x.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            String a3 = yz0.a(this.f);
            Charset c3 = b21.x.c();
            if (a3 != null) {
                byte[] bytes2 = a3.getBytes(c3);
                ee7.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                String a4 = yz0.a(this.h);
                Charset c4 = b21.x.c();
                if (a4 != null) {
                    byte[] bytes3 = a4.getBytes(c4);
                    ee7.a((Object) bytes3, "(this as java.lang.String).getBytes(charset)");
                    short length = (short) (b2 + b5 + b5 + bytes.length + bytes2.length + b5 + bytes3.length + b5);
                    ByteBuffer allocate = ByteBuffer.allocate(length);
                    ee7.a((Object) allocate, "ByteBuffer.allocate(totalLen.toInt())");
                    allocate.order(ByteOrder.LITTLE_ENDIAN);
                    allocate.putShort(length);
                    allocate.put(b2);
                    allocate.put(b3);
                    allocate.put(b4);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.put((byte) bytes.length);
                    allocate.put((byte) bytes2.length);
                    allocate.put((byte) bytes3.length);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.putInt(this.b);
                    allocate.putInt((int) this.c);
                    allocate.put(bytes);
                    allocate.put(bytes2);
                    allocate.put(bytes3);
                    allocate.putInt(this.g);
                    allocate.putInt((int) (this.j / ((long) 1000)));
                    byte[] array = allocate.array();
                    ee7.a((Object) array, "notificationData.array()");
                    return array;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(o90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            o90 o90 = (o90) obj;
            return this.a == o90.a && this.b == o90.b && this.c == o90.c && !(ee7.a(this.e, o90.e) ^ true) && !(ee7.a(this.f, o90.f) ^ true) && this.g == o90.g && !(ee7.a(this.h, o90.h) ^ true) && Arrays.equals(this.i, o90.i) && this.j == o90.j;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotification");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.c;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.d;
    }

    @DexIgnore
    public final z90[] getFlags() {
        return this.i;
    }

    @DexIgnore
    public final String getMessage() {
        return this.h;
    }

    @DexIgnore
    public final long getReceivedTimestampInMilliSecond() {
        return this.j;
    }

    @DexIgnore
    public final String getSender() {
        return this.f;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.g;
    }

    @DexIgnore
    public final String getTitle() {
        return this.e;
    }

    @DexIgnore
    public final ba0 getType() {
        return this.a;
    }

    @DexIgnore
    public final int getUid() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Long.valueOf(this.c).hashCode();
        int hashCode2 = this.e.hashCode();
        int hashCode3 = this.f.hashCode();
        int hashCode4 = Integer.valueOf(this.g).hashCode();
        int hashCode5 = this.h.hashCode();
        return Long.valueOf(this.j).hashCode() + ((((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.i)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeSerializable(this.a);
        parcel.writeInt(this.b);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        z90[] z90Arr = this.i;
        ArrayList arrayList = new ArrayList(z90Arr.length);
        for (z90 z90 : z90Arr) {
            arrayList.add(z90.name());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            parcel.writeStringArray((String[]) array);
            parcel.writeLong(this.j);
            parcel.writeLong(this.c);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ o90(android.os.Parcel r12, com.fossil.zd7 r13) {
        /*
            r11 = this;
            java.io.Serializable r13 = r12.readSerializable()
            if (r13 == 0) goto L_0x006c
            r1 = r13
            com.fossil.ba0 r1 = (com.fossil.ba0) r1
            int r2 = r12.readInt()
            java.lang.String r3 = r12.readString()
            r13 = 0
            if (r3 == 0) goto L_0x0068
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.ee7.a(r3, r0)
            java.lang.String r4 = r12.readString()
            if (r4 == 0) goto L_0x0064
            com.fossil.ee7.a(r4, r0)
            java.lang.String r5 = r12.readString()
            if (r5 == 0) goto L_0x0060
            com.fossil.ee7.a(r5, r0)
            int r6 = r12.readInt()
            java.lang.String r7 = r12.readString()
            if (r7 == 0) goto L_0x005c
            com.fossil.ee7.a(r7, r0)
            com.fossil.z90$a r0 = com.fossil.z90.c
            java.lang.String[] r8 = r12.createStringArray()
            if (r8 == 0) goto L_0x0058
            java.lang.String r13 = "parcel.createStringArray()!!"
            com.fossil.ee7.a(r8, r13)
            com.fossil.z90[] r8 = r0.a(r8)
            long r9 = r12.readLong()
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            long r12 = r12.readLong()
            r11.c = r12
            return
        L_0x0058:
            com.fossil.ee7.a()
            throw r13
        L_0x005c:
            com.fossil.ee7.a()
            throw r13
        L_0x0060:
            com.fossil.ee7.a()
            throw r13
        L_0x0064:
            com.fossil.ee7.a()
            throw r13
        L_0x0068:
            com.fossil.ee7.a()
            throw r13
        L_0x006c:
            com.fossil.x87 r12 = new com.fossil.x87
            java.lang.String r13 = "null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationType"
            r12.<init>(r13)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.o90.<init>(android.os.Parcel, com.fossil.zd7):void");
    }
}
