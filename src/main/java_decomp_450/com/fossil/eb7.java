package com.fossil;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.ib7;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eb7 implements ib7, Serializable {
    @DexIgnore
    public /* final */ ib7.b element;
    @DexIgnore
    public /* final */ ib7 left;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Serializable {
        @DexIgnore
        public static /* final */ C0050a Companion; // = new C0050a(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ ib7[] elements;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.eb7$a$a")
        /* renamed from: com.fossil.eb7$a$a  reason: collision with other inner class name */
        public static final class C0050a {
            @DexIgnore
            public C0050a() {
            }

            @DexIgnore
            public /* synthetic */ C0050a(zd7 zd7) {
                this();
            }
        }

        @DexIgnore
        public a(ib7[] ib7Arr) {
            ee7.b(ib7Arr, MessengerShareContentUtility.ELEMENTS);
            this.elements = ib7Arr;
        }

        @DexIgnore
        private final Object readResolve() {
            ib7[] ib7Arr = this.elements;
            ib7 ib7 = jb7.INSTANCE;
            for (ib7 ib72 : ib7Arr) {
                ib7 = ib7.plus(ib72);
            }
            return ib7;
        }

        @DexIgnore
        public final ib7[] getElements() {
            return this.elements;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements kd7<String, ib7.b, String> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final String invoke(String str, ib7.b bVar) {
            ee7.b(str, "acc");
            ee7.b(bVar, "element");
            if (str.length() == 0) {
                return bVar.toString();
            }
            return str + ", " + bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements kd7<i97, ib7.b, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ ib7[] $elements;
        @DexIgnore
        public /* final */ /* synthetic */ qe7 $index;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ib7[] ib7Arr, qe7 qe7) {
            super(2);
            this.$elements = ib7Arr;
            this.$index = qe7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public /* bridge */ /* synthetic */ i97 invoke(i97 i97, ib7.b bVar) {
            invoke(i97, bVar);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(i97 i97, ib7.b bVar) {
            ee7.b(i97, "<anonymous parameter 0>");
            ee7.b(bVar, "element");
            ib7[] ib7Arr = this.$elements;
            qe7 qe7 = this.$index;
            int i = qe7.element;
            qe7.element = i + 1;
            ib7Arr[i] = bVar;
        }
    }

    @DexIgnore
    public eb7(ib7 ib7, ib7.b bVar) {
        ee7.b(ib7, ViewHierarchy.DIMENSION_LEFT_KEY);
        ee7.b(bVar, "element");
        this.left = ib7;
        this.element = bVar;
    }

    @DexIgnore
    private final Object writeReplace() {
        int a2 = a();
        ib7[] ib7Arr = new ib7[a2];
        qe7 qe7 = new qe7();
        boolean z = false;
        qe7.element = 0;
        fold(i97.a, new c(ib7Arr, qe7));
        if (qe7.element == a2) {
            z = true;
        }
        if (z) {
            return new a(ib7Arr);
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final int a() {
        int i = 2;
        eb7 eb7 = this;
        while (true) {
            ib7 ib7 = eb7.left;
            if (!(ib7 instanceof eb7)) {
                ib7 = null;
            }
            eb7 = (eb7) ib7;
            if (eb7 == null) {
                return i;
            }
            i++;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof eb7) {
                eb7 eb7 = (eb7) obj;
                if (eb7.a() != a() || !eb7.a(this)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <R> R fold(R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
        ee7.b(kd7, "operation");
        return (R) kd7.invoke((Object) this.left.fold(r, kd7), this.element);
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        ee7.b(cVar, "key");
        eb7 eb7 = this;
        while (true) {
            E e = (E) eb7.element.get(cVar);
            if (e != null) {
                return e;
            }
            ib7 ib7 = eb7.left;
            if (!(ib7 instanceof eb7)) {
                return (E) ib7.get(cVar);
            }
            eb7 = (eb7) ib7;
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.left.hashCode() + this.element.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 minusKey(ib7.c<?> cVar) {
        ee7.b(cVar, "key");
        if (this.element.get(cVar) != null) {
            return this.left;
        }
        ib7 minusKey = this.left.minusKey(cVar);
        if (minusKey == this.left) {
            return this;
        }
        if (minusKey == jb7.INSTANCE) {
            return this.element;
        }
        return new eb7(minusKey, this.element);
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 plus(ib7 ib7) {
        ee7.b(ib7, "context");
        return ib7.a.a(this, ib7);
    }

    @DexIgnore
    public String toString() {
        return "[" + ((String) fold("", b.INSTANCE)) + "]";
    }

    @DexIgnore
    public final boolean a(ib7.b bVar) {
        return ee7.a(get(bVar.getKey()), bVar);
    }

    @DexIgnore
    public final boolean a(eb7 eb7) {
        while (a(eb7.element)) {
            ib7 ib7 = eb7.left;
            if (ib7 instanceof eb7) {
                eb7 = (eb7) ib7;
            } else if (ib7 != null) {
                return a((ib7.b) ib7);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
            }
        }
        return false;
    }
}
