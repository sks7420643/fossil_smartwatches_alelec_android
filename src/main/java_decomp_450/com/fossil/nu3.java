package com.fossil;

import android.graphics.Typeface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu3 extends su3 {
    @DexIgnore
    public /* final */ Typeface a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Typeface typeface);
    }

    @DexIgnore
    public nu3(a aVar, Typeface typeface) {
        this.a = typeface;
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.su3
    public void a(Typeface typeface, boolean z) {
        a(typeface);
    }

    @DexIgnore
    @Override // com.fossil.su3
    public void a(int i) {
        a(this.a);
    }

    @DexIgnore
    public void a() {
        this.c = true;
    }

    @DexIgnore
    public final void a(Typeface typeface) {
        if (!this.c) {
            this.b.a(typeface);
        }
    }
}
