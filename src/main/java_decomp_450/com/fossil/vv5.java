package com.fossil;

import android.text.SpannableString;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv5 extends pv5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> m;
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> n; // = this.q.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ qv5 o;
    @DexIgnore
    public /* final */ ch5 p;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1", f = "NotificationWatchRemindersPresenter.kt", l = {151, 160}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vv5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends InactivityNudgeTimeModel>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.q.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vv5$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$remindMinutes$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.vv5$b$b  reason: collision with other inner class name */
        public static final class C0223b extends zb7 implements kd7<yi7, fb7<? super RemindTimeModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0223b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0223b bVar = new C0223b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super RemindTimeModel> fb7) {
                return ((C0223b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.q.getRemindTimeDao().getRemindTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vv5 vv5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vv5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            qe7 qe7;
            Object obj2;
            qe7 qe72;
            Integer a2;
            Short a3;
            Object obj3;
            yi7 yi7;
            qe7 qe73;
            boolean z2;
            Object a4 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                qe7 qe74 = new qe7();
                qe74.element = 0;
                qe73 = new qe7();
                qe73.element = 0;
                boolean c = this.this$0.i;
                ti7 a5 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = qe74;
                this.L$2 = qe73;
                this.Z$0 = c;
                this.label = 1;
                obj3 = vh7.a(a5, aVar, this);
                if (obj3 == a4) {
                    return a4;
                }
                qe7 = qe74;
                z2 = c;
            } else if (i == 1) {
                z2 = this.Z$0;
                qe73 = (qe7) this.L$2;
                qe7 = (qe7) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
                obj3 = obj;
            } else if (i == 2) {
                List list = (List) this.L$3;
                boolean z3 = this.Z$0;
                qe72 = (qe7) this.L$2;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                z = z3;
                qe7 = (qe7) this.L$1;
                obj2 = obj;
                RemindTimeModel remindTimeModel = (RemindTimeModel) obj2;
                PortfolioApp c2 = PortfolioApp.g0.c();
                String c3 = PortfolioApp.g0.c().c();
                int i2 = qe7.element;
                byte b = (byte) (i2 / 60);
                byte b2 = (byte) (i2 % 60);
                int i3 = qe72.element;
                c2.a(c3, new InactiveNudgeData(b, b2, (byte) (i3 / 60), (byte) (i3 % 60), (remindTimeModel != null || (a2 = pb7.a(remindTimeModel.getMinutes())) == null || (a3 = pb7.a((short) a2.intValue())) == null) ? 0 : a3.shortValue(), z));
                this.this$0.p.s(this.this$0.i);
                this.this$0.p.t(this.this$0.j);
                this.this$0.p.u(this.this$0.k);
                this.this$0.p.r(this.this$0.l);
                this.this$0.o.close();
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<InactivityNudgeTimeModel> list2 = (List) obj3;
            for (InactivityNudgeTimeModel inactivityNudgeTimeModel : list2) {
                if (inactivityNudgeTimeModel.getNudgeTimeType() == 0) {
                    qe7.element = inactivityNudgeTimeModel.getMinutes();
                } else if (inactivityNudgeTimeModel.getNudgeTimeType() == 1) {
                    qe73.element = inactivityNudgeTimeModel.getMinutes();
                }
            }
            ti7 a6 = this.this$0.c();
            C0223b bVar = new C0223b(this, null);
            this.L$0 = yi7;
            this.L$1 = qe7;
            this.L$2 = qe73;
            this.Z$0 = z2;
            this.L$3 = list2;
            this.label = 2;
            obj2 = vh7.a(a6, bVar, this);
            if (obj2 == a4) {
                return a4;
            }
            z = z2;
            qe72 = qe73;
            RemindTimeModel remindTimeModel2 = (RemindTimeModel) obj2;
            PortfolioApp c22 = PortfolioApp.g0.c();
            String c32 = PortfolioApp.g0.c().c();
            int i22 = qe7.element;
            byte b3 = (byte) (i22 / 60);
            byte b22 = (byte) (i22 % 60);
            int i32 = qe72.element;
            c22.a(c32, new InactiveNudgeData(b3, b22, (byte) (i32 / 60), (byte) (i32 % 60), (remindTimeModel2 != null || (a2 = pb7.a(remindTimeModel2.getMinutes())) == null || (a3 = pb7.a((short) a2.intValue())) == null) ? 0 : a3.shortValue(), z));
            this.this$0.p.s(this.this$0.i);
            this.this$0.p.t(this.this$0.j);
            this.this$0.p.u(this.this$0.k);
            this.this$0.p.r(this.this$0.l);
            this.this$0.o.close();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {60}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $lInActivityNudgeTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vv5$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.vv5$c$a$a  reason: collision with other inner class name */
            public static final class C0224a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0224a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0224a aVar = new C0224a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0224a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.a.q.getInactivityNudgeTimeDao().upsertListInactivityNudgeTime(this.this$0.$lInActivityNudgeTimeModel);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$lInActivityNudgeTimeModel = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$lInActivityNudgeTimeModel, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = this.this$0.a.c();
                    C0224a aVar = new C0224a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(a2, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            if (list == null || list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
                arrayList.add(inactivityNudgeTimeModel);
                arrayList.add(inactivityNudgeTimeModel2);
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, arrayList, null), 3, null);
                return;
            }
            for (T t : list) {
                if (t.getNudgeTimeType() == 0) {
                    qv5 h = this.a.o;
                    SpannableString e = ze5.e(t.getMinutes());
                    ee7.a((Object) e, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    h.c(e);
                } else {
                    qv5 h2 = this.a.o;
                    SpannableString e2 = ze5.e(t.getMinutes());
                    ee7.a((Object) e2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    h2.d(e2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1", f = "NotificationWatchRemindersPresenter.kt", l = {79}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ RemindTimeModel $tempRemindTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vv5$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.vv5$d$a$a  reason: collision with other inner class name */
            public static final class C0225a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0225a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0225a aVar = new C0225a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0225a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.a.q.getRemindTimeDao().upsertRemindTimeModel(this.this$0.$tempRemindTimeModel);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, RemindTimeModel remindTimeModel, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$tempRemindTimeModel = remindTimeModel;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$tempRemindTimeModel, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = this.this$0.a.c();
                    C0225a aVar = new C0225a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(a2, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            if (remindTimeModel == null) {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, new RemindTimeModel("RemindTime", 20), null), 3, null);
                return;
            }
            qv5 h = this.a.o;
            String d = ze5.d(remindTimeModel.getMinutes());
            ee7.a((Object) d, "TimeUtils.getRemindTimeS\u2026(remindTimeModel.minutes)");
            h.Q(d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 a;

        @DexIgnore
        public e(vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<InactivityNudgeTimeModel> list) {
            qv5 unused = this.a.o;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ vv5 a;

        @DexIgnore
        public f(vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(RemindTimeModel remindTimeModel) {
            qv5 unused = this.a.o;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = vv5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public vv5(qv5 qv5, ch5 ch5, RemindersSettingsDatabase remindersSettingsDatabase) {
        ee7.b(qv5, "mView");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.o = qv5;
        this.p = ch5;
        this.q = remindersSettingsDatabase;
        this.m = remindersSettingsDatabase.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
        boolean W = this.p.W();
        this.e = W;
        this.i = W;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void j() {
        this.j = !this.j;
        if (m()) {
            this.o.e(true);
        } else {
            this.o.e(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void k() {
        this.k = !this.k;
        if (m()) {
            this.o.e(true);
        } else {
            this.o.e(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void l() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final boolean m() {
        return (this.i == this.e && this.j == this.f && this.k == this.g && this.l == this.h) ? false : true;
    }

    @DexIgnore
    public void n() {
        this.o.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.m;
        qv5 qv5 = this.o;
        if (qv5 != null) {
            liveData.a((rv5) qv5, new c(this));
            this.n.a((LifecycleOwner) this.o, new d(this));
            boolean X = this.p.X();
            this.f = X;
            this.j = X;
            boolean Y = this.p.Y();
            this.g = Y;
            this.k = Y;
            boolean V = this.p.V();
            this.h = V;
            this.l = V;
            this.o.I(this.i);
            this.o.p(this.j);
            this.o.H(this.k);
            this.o.l(this.l);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.m.b(new e(this));
        this.n.b(new f(this));
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void h() {
        this.l = !this.l;
        if (m()) {
            this.o.e(true);
        } else {
            this.o.e(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void i() {
        this.i = !this.i;
        if (m()) {
            this.o.e(true);
        } else {
            this.o.e(false);
        }
        this.o.I(this.i);
    }
}
