package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nh5 {
    @DexIgnore
    public /* final */ DianaPresetRepository a;
    @DexIgnore
    public /* final */ ch5 b;
    @DexIgnore
    public /* final */ WatchFaceRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.validation.DataValidationManager", f = "DataValidationManager.kt", l = {37}, m = "validateDianaPreset")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ nh5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(nh5 nh5, fb7 fb7) {
            super(fb7);
            this.this$0 = nh5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public nh5(DianaPresetRepository dianaPresetRepository, ch5 ch5, WatchFaceRepository watchFaceRepository) {
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(ch5, "mSharePreferencesManager");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        this.a = dianaPresetRepository;
        this.b = ch5;
        this.c = watchFaceRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r13, com.fossil.fb7<? super com.fossil.i97> r14) {
        /*
            r12 = this;
            boolean r0 = r14 instanceof com.fossil.nh5.b
            if (r0 == 0) goto L_0x0013
            r0 = r14
            com.fossil.nh5$b r0 = (com.fossil.nh5.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.nh5$b r0 = new com.fossil.nh5$b
            r0.<init>(r12, r14)
        L_0x0018:
            java.lang.Object r14 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0047
            if (r2 != r3) goto L_0x003f
            java.lang.Object r13 = r0.L$4
            com.fossil.oe7 r13 = (com.fossil.oe7) r13
            java.lang.Object r13 = r0.L$3
            java.util.List r13 = (java.util.List) r13
            java.lang.Object r13 = r0.L$2
            java.util.ArrayList r13 = (java.util.ArrayList) r13
            java.lang.Object r13 = r0.L$1
            java.lang.String r13 = (java.lang.String) r13
            java.lang.Object r0 = r0.L$0
            com.fossil.nh5 r0 = (com.fossil.nh5) r0
            com.fossil.t87.a(r14)
            goto L_0x012a
        L_0x003f:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r14 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r14)
            throw r13
        L_0x0047:
            com.fossil.t87.a(r14)
            com.portfolio.platform.data.source.DianaPresetRepository r14 = r12.a
            java.util.ArrayList r14 = r14.getPresetList(r13)
            com.portfolio.platform.data.source.WatchFaceRepository r2 = r12.c
            com.fossil.qb5 r5 = com.fossil.qb5.BACKGROUND
            java.util.List r2 = r2.getWatchFacesWithType(r13, r5)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "invalidateDianaPreset serial "
            r6.append(r7)
            r6.append(r13)
            java.lang.String r7 = " presetList size "
            r6.append(r7)
            int r7 = r14.size()
            r6.append(r7)
            java.lang.String r7 = " watchFaceListSize "
            r6.append(r7)
            int r7 = r2.size()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = "DataValidationManager"
            r5.d(r7, r6)
            boolean r5 = r14.isEmpty()
            if (r5 != 0) goto L_0x0134
            boolean r5 = r2.isEmpty()
            if (r5 == 0) goto L_0x009a
            goto L_0x0134
        L_0x009a:
            com.fossil.oe7 r5 = new com.fossil.oe7
            r5.<init>()
            r5.element = r4
            java.util.Iterator r6 = r14.iterator()
        L_0x00a5:
            boolean r8 = r6.hasNext()
            if (r8 == 0) goto L_0x0105
            java.lang.Object r8 = r6.next()
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
            com.portfolio.platform.data.source.WatchFaceRepository r9 = r12.c
            java.lang.String r10 = r8.getWatchFaceId()
            com.portfolio.platform.data.model.diana.preset.WatchFace r9 = r9.getWatchFaceWithId(r10)
            if (r9 != 0) goto L_0x00a5
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "invalidateDianaPreset watchface "
            r10.append(r11)
            java.lang.String r11 = r8.getWatchFaceId()
            r10.append(r11)
            java.lang.String r11 = " is not exist, choose first watch face "
            r10.append(r11)
            java.lang.Object r11 = r2.get(r4)
            com.portfolio.platform.data.model.diana.preset.WatchFace r11 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r11
            r10.append(r11)
            java.lang.String r11 = " for preset "
            r10.append(r11)
            java.lang.String r11 = r8.getName()
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r9.d(r7, r10)
            java.lang.Object r9 = r2.get(r4)
            com.portfolio.platform.data.model.diana.preset.WatchFace r9 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r9
            java.lang.String r9 = r9.getId()
            r8.setWatchFaceId(r9)
            r5.element = r3
            goto L_0x00a5
        L_0x0105:
            boolean r6 = r5.element
            if (r6 == 0) goto L_0x0131
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r8 = "invalidateDianaPreset preset is outdated, force sync to device"
            r6.d(r7, r8)
            com.portfolio.platform.data.source.DianaPresetRepository r6 = r12.a
            r0.L$0 = r12
            r0.L$1 = r13
            r0.L$2 = r14
            r0.L$3 = r2
            r0.L$4 = r5
            r0.label = r3
            java.lang.Object r14 = r6.upsertPresetList(r14, r0)
            if (r14 != r1) goto L_0x0129
            return r1
        L_0x0129:
            r0 = r12
        L_0x012a:
            com.fossil.ch5 r14 = r0.b
            r0 = 0
            r14.a(r13, r0, r4)
        L_0x0131:
            com.fossil.i97 r13 = com.fossil.i97.a
            return r13
        L_0x0134:
            com.fossil.i97 r13 = com.fossil.i97.a
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nh5.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
