package com.fossil;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym3 extends ii3 {
    @DexIgnore
    public Boolean b;
    @DexIgnore
    public gb3 c; // = hb3.a;
    @DexIgnore
    public Boolean d;

    @DexIgnore
    public ym3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public static long w() {
        return wb3.D.a(null).longValue();
    }

    @DexIgnore
    public static long x() {
        return wb3.d.a(null).longValue();
    }

    @DexIgnore
    public final void a(gb3 gb3) {
        this.c = gb3;
    }

    @DexIgnore
    public final int b(String str) {
        if (!e03.a() || !d(null, wb3.K0)) {
            return 500;
        }
        return a(str, wb3.H, 500, 2000);
    }

    @DexIgnore
    public final int c(String str) {
        return b(str, wb3.o);
    }

    @DexIgnore
    public final int d(String str) {
        if (!e03.a() || !d(null, wb3.K0)) {
            return 25;
        }
        return a(str, wb3.G, 25, 100);
    }

    @DexIgnore
    public final boolean e(String str, yf3<Boolean> yf3) {
        return d(str, yf3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b A[SYNTHETIC, Splitter:B:9:0x002b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> f(java.lang.String r4) {
        /*
            r3 = this;
            com.fossil.a72.b(r4)
            android.os.Bundle r0 = r3.v()
            r1 = 0
            if (r0 != 0) goto L_0x0019
            com.fossil.jg3 r4 = r3.e()
            com.fossil.mg3 r4 = r4.t()
            java.lang.String r0 = "Failed to load metadata: Metadata bundle is null"
            r4.a(r0)
        L_0x0017:
            r4 = r1
            goto L_0x0028
        L_0x0019:
            boolean r2 = r0.containsKey(r4)
            if (r2 != 0) goto L_0x0020
            goto L_0x0017
        L_0x0020:
            int r4 = r0.getInt(r4)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
        L_0x0028:
            if (r4 != 0) goto L_0x002b
            return r1
        L_0x002b:
            android.content.Context r0 = r3.f()     // Catch:{ NotFoundException -> 0x0043 }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ NotFoundException -> 0x0043 }
            int r4 = r4.intValue()     // Catch:{ NotFoundException -> 0x0043 }
            java.lang.String[] r4 = r0.getStringArray(r4)     // Catch:{ NotFoundException -> 0x0043 }
            if (r4 != 0) goto L_0x003e
            return r1
        L_0x003e:
            java.util.List r4 = java.util.Arrays.asList(r4)     // Catch:{ NotFoundException -> 0x0043 }
            return r4
        L_0x0043:
            r4 = move-exception
            com.fossil.jg3 r0 = r3.e()
            com.fossil.mg3 r0 = r0.t()
            java.lang.String r2 = "Failed to load string array from metadata: resource not found"
            r0.a(r2, r4)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ym3.f(java.lang.String):java.util.List");
    }

    @DexIgnore
    public final boolean g(String str) {
        return "1".equals(this.c.a(str, "gaia_collection_enabled"));
    }

    @DexIgnore
    public final boolean h(String str) {
        return "1".equals(this.c.a(str, "measurement.event_sampling_enabled"));
    }

    @DexIgnore
    public final boolean i(String str) {
        return d(str, wb3.K);
    }

    @DexIgnore
    public final String j(String str) {
        yf3<String> yf3 = wb3.L;
        if (str == null) {
            return yf3.a(null);
        }
        return yf3.a(this.c.a(str, yf3.a()));
    }

    @DexIgnore
    public final int m() {
        if (!e03.a() || !l().d(null, wb3.L0) || j().u() < 201500) {
            return 25;
        }
        return 100;
    }

    @DexIgnore
    public final long n() {
        b();
        return 31000;
    }

    @DexIgnore
    public final boolean o() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = f().getApplicationInfo();
                    String a = w92.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.d = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        e().t().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    @DexIgnore
    public final boolean p() {
        b();
        Boolean e = e("firebase_analytics_collection_deactivated");
        return e != null && e.booleanValue();
    }

    @DexIgnore
    public final Boolean q() {
        a();
        Boolean e = e("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(e == null || e.booleanValue());
    }

    @DexIgnore
    public final Boolean r() {
        a();
        boolean z = true;
        if (!s23.a() || !a(wb3.C0)) {
            return true;
        }
        Boolean e = e("google_analytics_automatic_screen_reporting_enabled");
        if (e != null && !e.booleanValue()) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final String s() {
        return a("debug.firebase.analytics.app", "");
    }

    @DexIgnore
    public final String t() {
        return a("debug.deferred.deeplink", "");
    }

    @DexIgnore
    public final boolean u() {
        if (this.b == null) {
            Boolean e = e("app_measurement_lite");
            this.b = e;
            if (e == null) {
                this.b = false;
            }
        }
        if (this.b.booleanValue() || !((ii3) this).a.C()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final Bundle v() {
        try {
            if (f().getPackageManager() == null) {
                e().t().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = ja2.b(f()).a(f().getPackageName(), 128);
            if (a != null) {
                return a.metaData;
            }
            e().t().a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            e().t().a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    @DexIgnore
    public final int a(String str) {
        return a(str, wb3.I, 25, 100);
    }

    @DexIgnore
    public final double c(String str, yf3<Double> yf3) {
        if (str == null) {
            return yf3.a(null).doubleValue();
        }
        String a = this.c.a(str, yf3.a());
        if (TextUtils.isEmpty(a)) {
            return yf3.a(null).doubleValue();
        }
        try {
            return yf3.a(Double.valueOf(Double.parseDouble(a))).doubleValue();
        } catch (NumberFormatException unused) {
            return yf3.a(null).doubleValue();
        }
    }

    @DexIgnore
    public final Boolean e(String str) {
        a72.b(str);
        Bundle v = v();
        if (v == null) {
            e().t().a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (!v.containsKey(str)) {
            return null;
        } else {
            return Boolean.valueOf(v.getBoolean(str));
        }
    }

    @DexIgnore
    public final long a(String str, yf3<Long> yf3) {
        if (str == null) {
            return yf3.a(null).longValue();
        }
        String a = this.c.a(str, yf3.a());
        if (TextUtils.isEmpty(a)) {
            return yf3.a(null).longValue();
        }
        try {
            return yf3.a(Long.valueOf(Long.parseLong(a))).longValue();
        } catch (NumberFormatException unused) {
            return yf3.a(null).longValue();
        }
    }

    @DexIgnore
    public final int b(String str, yf3<Integer> yf3) {
        if (str == null) {
            return yf3.a(null).intValue();
        }
        String a = this.c.a(str, yf3.a());
        if (TextUtils.isEmpty(a)) {
            return yf3.a(null).intValue();
        }
        try {
            return yf3.a(Integer.valueOf(Integer.parseInt(a))).intValue();
        } catch (NumberFormatException unused) {
            return yf3.a(null).intValue();
        }
    }

    @DexIgnore
    public final boolean d(String str, yf3<Boolean> yf3) {
        if (str == null) {
            return yf3.a(null).booleanValue();
        }
        String a = this.c.a(str, yf3.a());
        if (TextUtils.isEmpty(a)) {
            return yf3.a(null).booleanValue();
        }
        return yf3.a(Boolean.valueOf(Boolean.parseBoolean(a))).booleanValue();
    }

    @DexIgnore
    public final int a(String str, yf3<Integer> yf3, int i, int i2) {
        return Math.max(Math.min(b(str, yf3), i2), i);
    }

    @DexIgnore
    public final boolean a(yf3<Boolean> yf3) {
        return d(null, yf3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002d, code lost:
        if (android.text.TextUtils.isEmpty(r1) != false) goto L_0x002f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(com.fossil.kg3 r6) {
        /*
            r5 = this;
            android.net.Uri$Builder r0 = new android.net.Uri$Builder
            r0.<init>()
            java.lang.String r1 = r6.n()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0033
            boolean r1 = com.fossil.f23.a()
            if (r1 == 0) goto L_0x002f
            com.fossil.ym3 r1 = r5.l()
            java.lang.String r2 = r6.l()
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.o0
            boolean r1 = r1.d(r2, r3)
            if (r1 == 0) goto L_0x002f
            java.lang.String r1 = r6.p()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0033
        L_0x002f:
            java.lang.String r1 = r6.o()
        L_0x0033:
            com.fossil.yf3<java.lang.String> r2 = com.fossil.wb3.e
            r3 = 0
            java.lang.Object r2 = r2.a(r3)
            java.lang.String r2 = (java.lang.String) r2
            android.net.Uri$Builder r2 = r0.scheme(r2)
            com.fossil.yf3<java.lang.String> r4 = com.fossil.wb3.f
            java.lang.Object r3 = r4.a(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri$Builder r2 = r2.encodedAuthority(r3)
            java.lang.String r3 = "config/app/"
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r4 = r1.length()
            if (r4 == 0) goto L_0x005d
            java.lang.String r1 = r3.concat(r1)
            goto L_0x0062
        L_0x005d:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r3)
        L_0x0062:
            android.net.Uri$Builder r1 = r2.path(r1)
            java.lang.String r6 = r6.m()
            java.lang.String r2 = "app_instance_id"
            android.net.Uri$Builder r6 = r1.appendQueryParameter(r2, r6)
            java.lang.String r1 = "platform"
            java.lang.String r2 = "android"
            android.net.Uri$Builder r6 = r6.appendQueryParameter(r1, r2)
            long r1 = r5.n()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = "gmp_version"
            r6.appendQueryParameter(r2, r1)
            android.net.Uri r6 = r0.build()
            java.lang.String r6 = r6.toString()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ym3.a(com.fossil.kg3):java.lang.String");
    }

    @DexIgnore
    public final String a(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
        } catch (ClassNotFoundException e) {
            e().t().a("Could not find SystemProperties class", e);
            return str2;
        } catch (NoSuchMethodException e2) {
            e().t().a("Could not find SystemProperties.get() method", e2);
            return str2;
        } catch (IllegalAccessException e3) {
            e().t().a("Could not access SystemProperties.get()", e3);
            return str2;
        } catch (InvocationTargetException e4) {
            e().t().a("SystemProperties.get() threw an exception", e4);
            return str2;
        }
    }
}
