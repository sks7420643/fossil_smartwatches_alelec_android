package com.fossil;

import android.util.SparseArray;
import java.util.EnumMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy1 {
    @DexIgnore
    public static SparseArray<dt1> a; // = new SparseArray<>();
    @DexIgnore
    public static EnumMap<dt1, Integer> b;

    /*
    static {
        EnumMap<dt1, Integer> enumMap = new EnumMap<>(dt1.class);
        b = enumMap;
        enumMap.put(dt1.DEFAULT, (Integer) 0);
        b.put(dt1.VERY_LOW, (Integer) 1);
        b.put(dt1.HIGHEST, (Integer) 2);
        for (dt1 dt1 : b.keySet()) {
            a.append(b.get(dt1).intValue(), dt1);
        }
    }
    */

    @DexIgnore
    public static dt1 a(int i) {
        dt1 dt1 = a.get(i);
        if (dt1 != null) {
            return dt1;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i);
    }

    @DexIgnore
    public static int a(dt1 dt1) {
        Integer num = b.get(dt1);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + dt1);
    }
}
