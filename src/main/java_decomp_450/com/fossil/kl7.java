package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "kotlinx.coroutines.TimeoutKt", f = "Timeout.kt", l = {54}, m = "withTimeoutOrNull")
    public static final class a extends rb7 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public a(fb7 fb7) {
            super(fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return kl7.a(0, null, this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object a(long r7, com.fossil.kd7<? super com.fossil.yi7, ? super com.fossil.fb7<? super T>, ? extends java.lang.Object> r9, com.fossil.fb7<? super T> r10) {
        /*
            boolean r0 = r10 instanceof com.fossil.kl7.a
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.kl7$a r0 = (com.fossil.kl7.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.kl7$a r0 = new com.fossil.kl7$a
            r0.<init>(r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r7 = r0.L$1
            com.fossil.se7 r7 = (com.fossil.se7) r7
            java.lang.Object r8 = r0.L$0
            com.fossil.kd7 r8 = (com.fossil.kd7) r8
            long r8 = r0.J$0
            com.fossil.t87.a(r10)     // Catch:{ il7 -> 0x0034 }
            goto L_0x006f
        L_0x0034:
            r8 = move-exception
            goto L_0x0072
        L_0x0036:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003e:
            com.fossil.t87.a(r10)
            r5 = 0
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 > 0) goto L_0x0048
            return r4
        L_0x0048:
            com.fossil.se7 r10 = new com.fossil.se7
            r10.<init>()
            r10.element = r4
            r0.J$0 = r7     // Catch:{ il7 -> 0x0070 }
            r0.L$0 = r9     // Catch:{ il7 -> 0x0070 }
            r0.L$1 = r10     // Catch:{ il7 -> 0x0070 }
            r0.label = r3     // Catch:{ il7 -> 0x0070 }
            com.fossil.jl7 r2 = new com.fossil.jl7     // Catch:{ il7 -> 0x0070 }
            r2.<init>(r7, r0)     // Catch:{ il7 -> 0x0070 }
            r10.element = r2     // Catch:{ il7 -> 0x0070 }
            java.lang.Object r7 = a(r2, r9)     // Catch:{ il7 -> 0x0070 }
            java.lang.Object r8 = com.fossil.nb7.a()     // Catch:{ il7 -> 0x0070 }
            if (r7 != r8) goto L_0x006b
            com.fossil.vb7.c(r0)     // Catch:{ il7 -> 0x0070 }
        L_0x006b:
            if (r7 != r1) goto L_0x006e
            return r1
        L_0x006e:
            r10 = r7
        L_0x006f:
            return r10
        L_0x0070:
            r8 = move-exception
            r7 = r10
        L_0x0072:
            com.fossil.ik7 r9 = r8.coroutine
            T r7 = r7.element
            com.fossil.jl7 r7 = (com.fossil.jl7) r7
            if (r9 != r7) goto L_0x007b
            return r4
        L_0x007b:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kl7.a(long, com.fossil.kd7, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static final <U, T extends U> Object a(jl7<U, ? super T> jl7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) {
        mk7.a(jl7, kj7.a(((jm7) jl7).d.getContext()).a(jl7.e, jl7));
        return um7.b(jl7, jl7, kd7);
    }

    @DexIgnore
    public static final il7 a(long j, ik7 ik7) {
        return new il7("Timed out waiting for " + j + " ms", ik7);
    }
}
