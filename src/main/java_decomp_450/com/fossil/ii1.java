package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii1 implements Parcelable.Creator<gk1> {
    @DexIgnore
    public /* synthetic */ ii1(zd7 zd7) {
    }

    @DexIgnore
    public final gk1 a(byte[] bArr) {
        if (bArr.length < 12) {
            return null;
        }
        boolean z = false;
        cs1 a = cs1.e.a(bArr[0]);
        if (a == null) {
            return null;
        }
        r60 r60 = new r60(bArr[1], bArr[2]);
        if (bArr[3] == ((byte) 1)) {
            z = true;
        }
        return new gk1(a, r60, z, s97.a(bArr, 4, 12));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public gk1 createFromParcel(Parcel parcel) {
        cs1 cs1 = cs1.values()[parcel.readInt()];
        Parcelable readParcelable = parcel.readParcelable(r60.class.getClassLoader());
        if (readParcelable != null) {
            r60 r60 = (r60) readParcelable;
            boolean z = parcel.readByte() != ((byte) 0);
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray == null) {
                createByteArray = new byte[0];
            }
            return new gk1(cs1, r60, z, createByteArray);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public gk1[] newArray(int i) {
        return new gk1[i];
    }
}
