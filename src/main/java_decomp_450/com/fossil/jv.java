package com.fossil;

import com.fossil.mu;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jv implements su {
    @DexIgnore
    public static /* final */ boolean c; // = gv.b;
    @DexIgnore
    public /* final */ iv a;
    @DexIgnore
    public /* final */ kv b;

    @DexIgnore
    @Deprecated
    public jv(pv pvVar) {
        this(pvVar, new kv(4096));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005e, code lost:
        r15 = null;
        r2 = r12;
        r19 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00aa, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b5, code lost:
        r1 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b7, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b8, code lost:
        r19 = r1;
        r15 = null;
        r2 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00be, code lost:
        r19 = r1;
        r15 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c3, code lost:
        r0 = r2.d();
        com.fossil.gv.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r29.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00dc, code lost:
        if (r15 != null) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00de, code lost:
        r1 = new com.fossil.vu(r0, r15, false, android.os.SystemClock.elapsedRealtime() - r9, r19);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ef, code lost:
        if (r0 == 401) goto L_0x012b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f8, code lost:
        if (r0 < 400) goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0104, code lost:
        throw new com.fossil.ou(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0107, code lost:
        if (r0 < 500) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0111, code lost:
        if (r29.shouldRetryServerErrors() != false) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0113, code lost:
        a("server", r29, new com.fossil.dv(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0124, code lost:
        throw new com.fossil.dv(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x012a, code lost:
        throw new com.fossil.dv(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x012b, code lost:
        a("auth", r29, new com.fossil.lu(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0137, code lost:
        a("network", r29, new com.fossil.uu());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0148, code lost:
        throw new com.fossil.wu(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0149, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0164, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r29.getUrl(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0165, code lost:
        a("socket", r29, new com.fossil.ev());
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0149 A[ExcHandler: MalformedURLException (r0v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:77:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000e] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0143 A[SYNTHETIC] */
    @Override // com.fossil.su
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.vu a(com.fossil.yu<?> r29) throws com.fossil.fv {
        /*
            r28 = this;
            r7 = r28
            r8 = r29
            long r9 = android.os.SystemClock.elapsedRealtime()
        L_0x0008:
            java.util.List r1 = java.util.Collections.emptyList()
            r11 = 0
            r2 = 0
            com.fossil.mu$a r0 = r29.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00bd }
            java.util.Map r0 = r7.a(r0)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00bd }
            com.fossil.iv r3 = r7.a     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00bd }
            com.fossil.ov r12 = r3.b(r8, r0)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00bd }
            int r14 = r12.d()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00b7 }
            java.util.List r13 = r12.c()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00b7 }
            r0 = 304(0x130, float:4.26E-43)
            if (r14 != r0) goto L_0x0064
            com.fossil.mu$a r0 = r29.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            if (r0 != 0) goto L_0x0043
            com.fossil.vu r0 = new com.fossil.vu     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            r16 = 304(0x130, float:4.26E-43)
            r17 = 0
            r18 = 1
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            long r19 = r3 - r9
            r15 = r0
            r21 = r13
            r15.<init>(r16, r17, r18, r19, r21)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            return r0
        L_0x0043:
            java.util.List r27 = a(r13, r0)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            com.fossil.vu r1 = new com.fossil.vu     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            r22 = 304(0x130, float:4.26E-43)
            byte[] r0 = r0.a     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            r24 = 1
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            long r25 = r3 - r9
            r21 = r1
            r23 = r0
            r21.<init>(r22, r23, r24, r25, r27)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x005d }
            return r1
        L_0x005d:
            r0 = move-exception
            r15 = r2
            r2 = r12
            r19 = r13
            goto L_0x00c1
        L_0x0064:
            java.io.InputStream r0 = r12.a()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00b4 }
            if (r0 == 0) goto L_0x0073
            int r1 = r12.b()
            byte[] r0 = r7.a(r0, r1)
            goto L_0x0075
        L_0x0073:
            byte[] r0 = new byte[r11]
        L_0x0075:
            r20 = r0
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00ac }
            long r2 = r0 - r9
            r1 = r28
            r4 = r29
            r5 = r20
            r6 = r14
            r1.a(r2, r4, r5, r6)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00ac }
            r0 = 200(0xc8, float:2.8E-43)
            if (r14 < r0) goto L_0x00a3
            r0 = 299(0x12b, float:4.19E-43)
            if (r14 > r0) goto L_0x00a3
            com.fossil.vu r0 = new com.fossil.vu     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00ac }
            r16 = 0
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00ac }
            long r17 = r1 - r9
            r1 = r13
            r13 = r0
            r15 = r20
            r19 = r1
            r13.<init>(r14, r15, r16, r17, r19)     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00aa }
            return r0
        L_0x00a3:
            r1 = r13
            java.io.IOException r0 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00aa }
            r0.<init>()     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00aa }
            throw r0     // Catch:{ SocketTimeoutException -> 0x0165, MalformedURLException -> 0x0149, IOException -> 0x00aa }
        L_0x00aa:
            r0 = move-exception
            goto L_0x00ae
        L_0x00ac:
            r0 = move-exception
            r1 = r13
        L_0x00ae:
            r19 = r1
            r2 = r12
            r15 = r20
            goto L_0x00c1
        L_0x00b4:
            r0 = move-exception
            r1 = r13
            goto L_0x00b8
        L_0x00b7:
            r0 = move-exception
        L_0x00b8:
            r19 = r1
            r15 = r2
            r2 = r12
            goto L_0x00c1
        L_0x00bd:
            r0 = move-exception
            r19 = r1
            r15 = r2
        L_0x00c1:
            if (r2 == 0) goto L_0x0143
            int r0 = r2.d()
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            r1[r11] = r2
            r2 = 1
            java.lang.String r3 = r29.getUrl()
            r1[r2] = r3
            java.lang.String r2 = "Unexpected response code %d for %s"
            com.fossil.gv.c(r2, r1)
            if (r15 == 0) goto L_0x0137
            com.fossil.vu r1 = new com.fossil.vu
            r16 = 0
            long r2 = android.os.SystemClock.elapsedRealtime()
            long r17 = r2 - r9
            r13 = r1
            r14 = r0
            r13.<init>(r14, r15, r16, r17, r19)
            r2 = 401(0x191, float:5.62E-43)
            if (r0 == r2) goto L_0x012b
            r2 = 403(0x193, float:5.65E-43)
            if (r0 != r2) goto L_0x00f6
            goto L_0x012b
        L_0x00f6:
            r2 = 400(0x190, float:5.6E-43)
            if (r0 < r2) goto L_0x0105
            r2 = 499(0x1f3, float:6.99E-43)
            if (r0 <= r2) goto L_0x00ff
            goto L_0x0105
        L_0x00ff:
            com.fossil.ou r0 = new com.fossil.ou
            r0.<init>(r1)
            throw r0
        L_0x0105:
            r2 = 500(0x1f4, float:7.0E-43)
            if (r0 < r2) goto L_0x0125
            r2 = 599(0x257, float:8.4E-43)
            if (r0 > r2) goto L_0x0125
            boolean r0 = r29.shouldRetryServerErrors()
            if (r0 == 0) goto L_0x011f
            com.fossil.dv r0 = new com.fossil.dv
            r0.<init>(r1)
            java.lang.String r1 = "server"
            a(r1, r8, r0)
            goto L_0x0008
        L_0x011f:
            com.fossil.dv r0 = new com.fossil.dv
            r0.<init>(r1)
            throw r0
        L_0x0125:
            com.fossil.dv r0 = new com.fossil.dv
            r0.<init>(r1)
            throw r0
        L_0x012b:
            com.fossil.lu r0 = new com.fossil.lu
            r0.<init>(r1)
            java.lang.String r1 = "auth"
            a(r1, r8, r0)
            goto L_0x0008
        L_0x0137:
            com.fossil.uu r0 = new com.fossil.uu
            r0.<init>()
            java.lang.String r1 = "network"
            a(r1, r8, r0)
            goto L_0x0008
        L_0x0143:
            com.fossil.wu r1 = new com.fossil.wu
            r1.<init>(r0)
            throw r1
        L_0x0149:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Bad URL "
            r2.append(r3)
            java.lang.String r3 = r29.getUrl()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0165:
            com.fossil.ev r0 = new com.fossil.ev
            r0.<init>()
            java.lang.String r1 = "socket"
            a(r1, r8, r0)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jv.a(com.fossil.yu):com.fossil.vu");
    }

    @DexIgnore
    @Deprecated
    public jv(pv pvVar, kv kvVar) {
        this.a = new hv(pvVar);
        this.b = kvVar;
    }

    @DexIgnore
    public jv(iv ivVar) {
        this(ivVar, new kv(4096));
    }

    @DexIgnore
    public jv(iv ivVar, kv kvVar) {
        this.a = ivVar;
        this.b = kvVar;
    }

    @DexIgnore
    public final void a(long j, yu<?> yuVar, byte[] bArr, int i) {
        if (c || j > 3000) {
            Object[] objArr = new Object[5];
            objArr[0] = yuVar;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(i);
            objArr[4] = Integer.valueOf(yuVar.getRetryPolicy().b());
            gv.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    @DexIgnore
    public static void a(String str, yu<?> yuVar, fv fvVar) throws fv {
        cv retryPolicy = yuVar.getRetryPolicy();
        int timeoutMs = yuVar.getTimeoutMs();
        try {
            retryPolicy.a(fvVar);
            yuVar.addMarker(String.format("%s-retry [timeout=%s]", str, Integer.valueOf(timeoutMs)));
        } catch (fv e) {
            yuVar.addMarker(String.format("%s-timeout-giveup [timeout=%s]", str, Integer.valueOf(timeoutMs)));
            throw e;
        }
    }

    @DexIgnore
    public final Map<String, String> a(mu.a aVar) {
        if (aVar == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap();
        String str = aVar.b;
        if (str != null) {
            hashMap.put("If-None-Match", str);
        }
        long j = aVar.d;
        if (j > 0) {
            hashMap.put("If-Modified-Since", nv.a(j));
        }
        return hashMap;
    }

    @DexIgnore
    public final byte[] a(InputStream inputStream, int i) throws IOException, dv {
        vv vvVar = new vv(this.b, i);
        if (inputStream != null) {
            try {
                byte[] a2 = this.b.a(1024);
                while (true) {
                    int read = inputStream.read(a2);
                    if (read == -1) {
                        break;
                    }
                    vvVar.write(a2, 0, read);
                }
                byte[] byteArray = vvVar.toByteArray();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                        gv.d("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.b.a(a2);
                vvVar.close();
                return byteArray;
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused2) {
                        gv.d("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.b.a((byte[]) null);
                vvVar.close();
                throw th;
            }
        } else {
            throw new dv();
        }
    }

    @DexIgnore
    public static List<ru> a(List<ru> list, mu.a aVar) {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        if (!list.isEmpty()) {
            for (ru ruVar : list) {
                treeSet.add(ruVar.a());
            }
        }
        ArrayList arrayList = new ArrayList(list);
        List<ru> list2 = aVar.h;
        if (list2 != null) {
            if (!list2.isEmpty()) {
                for (ru ruVar2 : aVar.h) {
                    if (!treeSet.contains(ruVar2.a())) {
                        arrayList.add(ruVar2);
                    }
                }
            }
        } else if (!aVar.g.isEmpty()) {
            for (Map.Entry<String, String> entry : aVar.g.entrySet()) {
                if (!treeSet.contains(entry.getKey())) {
                    arrayList.add(new ru(entry.getKey(), entry.getValue()));
                }
            }
        }
        return arrayList;
    }
}
