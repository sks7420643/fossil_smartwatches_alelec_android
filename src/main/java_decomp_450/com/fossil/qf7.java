package com.fossil;

import com.fossil.jf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qf7 extends pf7 {
    @DexIgnore
    public static final double a(double d, double d2) {
        return d < d2 ? d2 : d;
    }

    @DexIgnore
    public static final float a(float f, float f2) {
        return f < f2 ? f2 : f;
    }

    @DexIgnore
    public static final int a(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    @DexIgnore
    public static final long a(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    @DexIgnore
    public static final jf7 a(jf7 jf7, int i) {
        ee7.b(jf7, "$this$step");
        pf7.a(i > 0, Integer.valueOf(i));
        jf7.a aVar = jf7.d;
        int first = jf7.getFirst();
        int last = jf7.getLast();
        if (jf7.a() <= 0) {
            i = -i;
        }
        return aVar.a(first, last, i);
    }

    @DexIgnore
    public static final double b(double d, double d2) {
        return d > d2 ? d2 : d;
    }

    @DexIgnore
    public static final int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    @DexIgnore
    public static final long b(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    @DexIgnore
    public static final jf7 c(int i, int i2) {
        return jf7.d.a(i, i2, -1);
    }

    @DexIgnore
    public static final lf7 d(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return lf7.f.a();
        }
        return new lf7(i, i2 - 1);
    }

    @DexIgnore
    public static final of7 a(long j, int i) {
        return new of7(j, ((long) i) - 1);
    }

    @DexIgnore
    public static final int a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
        } else if (i < i2) {
            return i2;
        } else {
            return i > i3 ? i3 : i;
        }
    }

    @DexIgnore
    public static final long a(long j, long j2, long j3) {
        if (j2 > j3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + j3 + " is less than minimum " + j2 + '.');
        } else if (j < j2) {
            return j2;
        } else {
            return j > j3 ? j3 : j;
        }
    }
}
