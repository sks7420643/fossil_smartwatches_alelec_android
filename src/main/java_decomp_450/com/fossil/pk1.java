package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pk1 extends eo0 {
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ qk1 k;

    @DexIgnore
    public pk1(aq0 aq0, qk1 qk1, cx0 cx0) {
        super(aq0, cx0);
        this.k = qk1;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        super.a(s91);
        this.j = false;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean d() {
        return ((eo0) this).d.b == oi0.l && this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public JSONObject a(boolean z) {
        return yz0.a(super.a(z), r51.P0, this.k.a);
    }
}
