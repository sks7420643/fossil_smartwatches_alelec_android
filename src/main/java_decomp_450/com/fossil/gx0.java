package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class gx0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[u70.values().length];
        a = iArr;
        iArr[u70.WEATHER.ordinal()] = 1;
        a[u70.HEART_RATE.ordinal()] = 2;
        a[u70.STEPS.ordinal()] = 3;
        a[u70.DATE.ordinal()] = 4;
        a[u70.CHANCE_OF_RAIN.ordinal()] = 5;
        a[u70.SECOND_TIMEZONE.ordinal()] = 6;
        a[u70.ACTIVE_MINUTES.ordinal()] = 7;
        a[u70.CALORIES.ordinal()] = 8;
        a[u70.BATTERY.ordinal()] = 9;
        a[u70.EMPTY.ordinal()] = 10;
    }
    */
}
