package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok4 implements Factory<qe> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public ok4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static ok4 a(wj4 wj4) {
        return new ok4(wj4);
    }

    @DexIgnore
    public static qe b(wj4 wj4) {
        qe j = wj4.j();
        c87.a(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public qe get() {
        return b(this.a);
    }
}
