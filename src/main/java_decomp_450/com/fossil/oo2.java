package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ ri3 e;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oo2(sn2 sn2, ri3 ri3) {
        super(sn2);
        this.f = sn2;
        this.e = ri3;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        for (int i = 0; i < this.f.e.size(); i++) {
            if (this.e.equals(((Pair) this.f.e.get(i)).first)) {
                Log.w(this.f.a, "OnEventListener already registered.");
                return;
            }
        }
        sn2.c cVar = new sn2.c(this.e);
        this.f.e.add(new Pair(this.e, cVar));
        this.f.h.registerOnMeasurementEventListener(cVar);
    }
}
