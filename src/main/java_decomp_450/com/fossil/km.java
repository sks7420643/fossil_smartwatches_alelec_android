package com.fossil;

import android.os.Build;
import androidx.work.ListenableWorker;
import androidx.work.OverwritingInputMerger;
import com.fossil.sm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km extends sm {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sm.a<a, km> {
        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            super(cls);
            ((sm.a) this).c.d = OverwritingInputMerger.class.getName();
        }

        @DexIgnore
        @Override // com.fossil.sm.a
        public a c() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.sm.a
        public km b() {
            if (!((sm.a) this).a || Build.VERSION.SDK_INT < 23 || !((sm.a) this).c.j.h()) {
                zo zoVar = ((sm.a) this).c;
                if (!zoVar.q || Build.VERSION.SDK_INT < 23 || !zoVar.j.h()) {
                    return new km(this);
                }
                throw new IllegalArgumentException("Cannot run in foreground with an idle mode constraint");
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }
    }

    @DexIgnore
    public km(a aVar) {
        super(((sm.a) aVar).b, ((sm.a) aVar).c, ((sm.a) aVar).d);
    }

    @DexIgnore
    public static km a(Class<? extends ListenableWorker> cls) {
        return (km) new a(cls).a();
    }
}
