package com.fossil;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa6 extends ta6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> f;
    @DexIgnore
    public /* final */ ua6 g;
    @DexIgnore
    public /* final */ GoalTrackingRepository h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ pj4 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1", f = "DashboardGoalTrackingPresenter.kt", l = {55, 62}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wa6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements te5.a {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // com.fossil.te5.a
            public final void a(te5.g gVar) {
                ee7.b(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + gVar);
                if (gVar.a()) {
                    this.a.this$0.k().d();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wa6$b$b")
        /* renamed from: com.fossil.wa6$b$b  reason: collision with other inner class name */
        public static final class C0230b<T> implements zd<qf<GoalTrackingSummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0230b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<GoalTrackingSummary> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
                local.d("DashboardGoalTrackingPresenter", sb.toString());
                if (qfVar != null) {
                    this.a.this$0.k().a(qfVar);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$user$1", f = "DashboardGoalTrackingPresenter.kt", l = {55}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository e = this.this$0.this$0.i;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = e.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(wa6 wa6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wa6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00ae  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0036
                if (r1 == r3) goto L_0x002e
                if (r1 != r2) goto L_0x0026
                java.lang.Object r0 = r8.L$4
                com.fossil.wa6 r0 = (com.fossil.wa6) r0
                java.lang.Object r1 = r8.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r8.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r8.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x0088
            L_0x0026:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x002e:
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x0052
            L_0x0036:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r1 = r8.p$
                com.fossil.wa6 r9 = r8.this$0
                com.fossil.ti7 r9 = r9.b()
                com.fossil.wa6$b$c r4 = new com.fossil.wa6$b$c
                r5 = 0
                r4.<init>(r8, r5)
                r8.L$0 = r1
                r8.label = r3
                java.lang.Object r9 = com.fossil.vh7.a(r9, r4, r8)
                if (r9 != r0) goto L_0x0052
                return r0
            L_0x0052:
                com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
                if (r9 == 0) goto L_0x00b6
                java.lang.String r3 = r9.getCreatedAt()
                java.util.Date r3 = com.fossil.zd5.d(r3)
                com.fossil.wa6 r4 = r8.this$0
                com.portfolio.platform.data.source.GoalTrackingRepository r5 = r4.h
                java.lang.String r6 = "createdDate"
                com.fossil.ee7.a(r3, r6)
                com.fossil.wa6 r6 = r8.this$0
                com.fossil.pj4 r6 = r6.j
                com.fossil.wa6$b$a r7 = new com.fossil.wa6$b$a
                r7.<init>(r8)
                r8.L$0 = r1
                r8.L$1 = r9
                r8.L$2 = r9
                r8.L$3 = r3
                r8.L$4 = r4
                r8.label = r2
                java.lang.Object r9 = r5.getSummariesPaging(r3, r6, r7, r8)
                if (r9 != r0) goto L_0x0087
                return r0
            L_0x0087:
                r0 = r4
            L_0x0088:
                com.portfolio.platform.data.Listing r9 = (com.portfolio.platform.data.Listing) r9
                r0.f = r9
                com.fossil.wa6 r9 = r8.this$0
                com.fossil.ua6 r9 = r9.k()
                com.fossil.wa6 r0 = r8.this$0
                com.portfolio.platform.data.Listing r0 = r0.f
                if (r0 == 0) goto L_0x00b6
                androidx.lifecycle.LiveData r0 = r0.getPagedList()
                if (r0 == 0) goto L_0x00b6
                if (r9 == 0) goto L_0x00ae
                com.fossil.va6 r9 = (com.fossil.va6) r9
                com.fossil.wa6$b$b r1 = new com.fossil.wa6$b$b
                r1.<init>(r8)
                r0.a(r9, r1)
                goto L_0x00b6
            L_0x00ae:
                com.fossil.x87 r9 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment"
                r9.<init>(r0)
                throw r9
            L_0x00b6:
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wa6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public wa6(ua6 ua6, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, pj4 pj4) {
        ee7.b(ua6, "mView");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(pj4, "mAppExecutors");
        this.g = ua6;
        this.h = goalTrackingRepository;
        this.i = userRepository;
        this.j = pj4;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!zd5.w(this.e).booleanValue()) {
            this.e = new Date();
            Listing<GoalTrackingSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.ta6
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.ta6
    public void i() {
        LiveData<qf<GoalTrackingSummary>> pagedList;
        try {
            ua6 ua6 = this.g;
            Listing<GoalTrackingSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (ua6 != null) {
                    pagedList.a((va6) ua6);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ta6
    public void j() {
        vc7<i97> retry;
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final ua6 k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }
}
