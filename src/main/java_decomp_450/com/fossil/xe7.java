package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xe7 {
    @DexIgnore
    public static <T extends Throwable> T a(T t) {
        ee7.a((Throwable) t, xe7.class.getName());
        return t;
    }

    @DexIgnore
    public static List b(Object obj) {
        if (!(obj instanceof ye7)) {
            return e(obj);
        }
        a(obj, "kotlin.collections.MutableList");
        throw null;
    }

    @DexIgnore
    public static Map c(Object obj) {
        if (!(obj instanceof ye7)) {
            return f(obj);
        }
        a(obj, "kotlin.collections.MutableMap");
        throw null;
    }

    @DexIgnore
    public static Iterable d(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static List e(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static Map f(Object obj) {
        try {
            return (Map) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static int g(Object obj) {
        if (obj instanceof be7) {
            return ((be7) obj).getArity();
        }
        if (obj instanceof vc7) {
            return 0;
        }
        if (obj instanceof gd7) {
            return 1;
        }
        if (obj instanceof kd7) {
            return 2;
        }
        if (obj instanceof ld7) {
            return 3;
        }
        if (obj instanceof md7) {
            return 4;
        }
        if (obj instanceof nd7) {
            return 5;
        }
        if (obj instanceof od7) {
            return 6;
        }
        if (obj instanceof pd7) {
            return 7;
        }
        if (obj instanceof qd7) {
            return 8;
        }
        if (obj instanceof rd7) {
            return 9;
        }
        if (obj instanceof wc7) {
            return 10;
        }
        if (obj instanceof xc7) {
            return 11;
        }
        if (obj instanceof yc7) {
            return 12;
        }
        if (obj instanceof zc7) {
            return 13;
        }
        if (obj instanceof ad7) {
            return 14;
        }
        if (obj instanceof bd7) {
            return 15;
        }
        if (obj instanceof cd7) {
            return 16;
        }
        if (obj instanceof dd7) {
            return 17;
        }
        if (obj instanceof ed7) {
            return 18;
        }
        if (obj instanceof fd7) {
            return 19;
        }
        if (obj instanceof hd7) {
            return 20;
        }
        if (obj instanceof id7) {
            return 21;
        }
        return obj instanceof jd7 ? 22 : -1;
    }

    @DexIgnore
    public static void a(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        a(name + " cannot be cast to " + str);
        throw null;
    }

    @DexIgnore
    public static void a(String str) {
        a(new ClassCastException(str));
        throw null;
    }

    @DexIgnore
    public static boolean b(Object obj, int i) {
        return (obj instanceof j87) && g(obj) == i;
    }

    @DexIgnore
    public static ClassCastException a(ClassCastException classCastException) {
        a((Throwable) classCastException);
        throw classCastException;
    }

    @DexIgnore
    public static Iterable a(Object obj) {
        if (!(obj instanceof ye7)) {
            return d(obj);
        }
        a(obj, "kotlin.collections.MutableIterable");
        throw null;
    }

    @DexIgnore
    public static Object a(Object obj, int i) {
        if (obj == null || b(obj, i)) {
            return obj;
        }
        a(obj, "kotlin.jvm.functions.Function" + i);
        throw null;
    }
}
