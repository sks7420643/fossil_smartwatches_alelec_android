package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.im;
import com.fossil.zl;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dn extends rm {
    @DexIgnore
    public static dn j;
    @DexIgnore
    public static dn k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public Context a;
    @DexIgnore
    public zl b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public vp d;
    @DexIgnore
    public List<ym> e;
    @DexIgnore
    public xm f;
    @DexIgnore
    public kp g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public dn(Context context, zl zlVar, vp vpVar) {
        this(context, zlVar, vpVar, context.getResources().getBoolean(nm.workmanager_test_configuration));
    }

    @DexIgnore
    @Override // com.fossil.rm
    public static dn a(Context context) {
        dn j2;
        synchronized (l) {
            j2 = j();
            if (j2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof zl.b) {
                    a(applicationContext, ((zl.b) applicationContext).a());
                    j2 = a(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return j2;
    }

    @DexIgnore
    @Deprecated
    public static dn j() {
        synchronized (l) {
            if (j != null) {
                return j;
            }
            return k;
        }
    }

    @DexIgnore
    public zl b() {
        return this.b;
    }

    @DexIgnore
    public kp c() {
        return this.g;
    }

    @DexIgnore
    public xm d() {
        return this.f;
    }

    @DexIgnore
    public List<ym> e() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase f() {
        return this.c;
    }

    @DexIgnore
    public vp g() {
        return this.d;
    }

    @DexIgnore
    public void h() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void i() {
        if (Build.VERSION.SDK_INT >= 23) {
            on.a(a());
        }
        f().f().e();
        zm.a(b(), f(), e());
    }

    @DexIgnore
    public void b(String str) {
        this.d.a(new np(this, str, true));
    }

    @DexIgnore
    public void c(String str) {
        this.d.a(new np(this, str, false));
    }

    @DexIgnore
    public dn(Context context, zl zlVar, vp vpVar, boolean z) {
        this(context, zlVar, vpVar, WorkDatabase.a(context.getApplicationContext(), vpVar.b(), z));
    }

    @DexIgnore
    public dn(Context context, zl zlVar, vp vpVar, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        im.a(new im.a(zlVar.g()));
        List<ym> a2 = a(applicationContext, zlVar, vpVar);
        a(context, zlVar, vpVar, workDatabase, a2, new xm(context, zlVar, vpVar, workDatabase, a2));
    }

    @DexIgnore
    @Override // com.fossil.rm
    public static void a(Context context, zl zlVar) {
        synchronized (l) {
            if (j != null) {
                if (k != null) {
                    throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
                }
            }
            if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new dn(applicationContext, zlVar, new wp(zlVar.i()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    public Context a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.rm
    public lm a(List<? extends sm> list) {
        if (!list.isEmpty()) {
            return new an(this, list).a();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }

    @DexIgnore
    @Override // com.fossil.rm
    public lm a(String str, dm dmVar, List<km> list) {
        return new an(this, str, dmVar, list).a();
    }

    @DexIgnore
    public lm a(UUID uuid) {
        gp a2 = gp.a(uuid, this);
        this.d.a(a2);
        return a2.a();
    }

    @DexIgnore
    public void a(String str) {
        a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public void a(String str, WorkerParameters.a aVar) {
        this.d.a(new mp(this, str, aVar));
    }

    @DexIgnore
    public void a(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                pendingResult.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public final void a(Context context, zl zlVar, vp vpVar, WorkDatabase workDatabase, List<ym> list, xm xmVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = zlVar;
        this.d = vpVar;
        this.c = workDatabase;
        this.e = list;
        this.f = xmVar;
        this.g = new kp(workDatabase);
        this.h = false;
        if (Build.VERSION.SDK_INT < 24 || !applicationContext.isDeviceProtectedStorage()) {
            this.d.a(new ForceStopRunnable(applicationContext, this));
            return;
        }
        throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
    }

    @DexIgnore
    public List<ym> a(Context context, zl zlVar, vp vpVar) {
        return Arrays.asList(zm.a(context, this), new gn(context, zlVar, vpVar, this));
    }
}
