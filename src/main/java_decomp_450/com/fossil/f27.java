package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f27 extends h27 {
    @DexIgnore
    public f27(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(((h27) this).a).edit();
            edit.putString(j27.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
            edit.commit();
        }
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.h27
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from sharedPreferences");
            string = PreferenceManager.getDefaultSharedPreferences(((h27) this).a).getString(j27.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), null);
        }
        return string;
    }
}
