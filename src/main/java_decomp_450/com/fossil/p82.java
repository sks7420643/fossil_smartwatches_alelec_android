package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.p62;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p82 implements ServiceConnection {
    @DexIgnore
    public /* final */ Map<ServiceConnection, ServiceConnection> a; // = new HashMap();
    @DexIgnore
    public int b; // = 2;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public IBinder d;
    @DexIgnore
    public /* final */ p62.a e;
    @DexIgnore
    public ComponentName f;
    @DexIgnore
    public /* final */ /* synthetic */ q82 g;

    @DexIgnore
    public p82(q82 q82, p62.a aVar) {
        this.g = q82;
        this.e = aVar;
    }

    @DexIgnore
    public final void a(String str) {
        this.b = 3;
        boolean a2 = this.g.g.a(this.g.e, str, this.e.a(this.g.e), this, this.e.c());
        this.c = a2;
        if (a2) {
            this.g.f.sendMessageDelayed(this.g.f.obtainMessage(1, this.e), this.g.i);
            return;
        }
        this.b = 2;
        try {
            this.g.g.a(this.g.e, this);
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final void b(String str) {
        this.g.f.removeMessages(1, this.e);
        this.g.g.a(this.g.e, this);
        this.c = false;
        this.b = 2;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }

    @DexIgnore
    public final boolean e() {
        return this.a.isEmpty();
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.d) {
            this.g.f.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceConnected(componentName, iBinder);
            }
            this.b = 1;
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.d) {
            this.g.f.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceDisconnected(componentName);
            }
            this.b = 2;
        }
    }

    @DexIgnore
    public final ComponentName b() {
        return this.f;
    }

    @DexIgnore
    public final void a(ServiceConnection serviceConnection, ServiceConnection serviceConnection2, String str) {
        e92 unused = this.g.g;
        Context unused2 = this.g.e;
        this.e.a(this.g.e);
        this.a.put(serviceConnection, serviceConnection2);
    }

    @DexIgnore
    public final void a(ServiceConnection serviceConnection, String str) {
        e92 unused = this.g.g;
        Context unused2 = this.g.e;
        this.a.remove(serviceConnection);
    }

    @DexIgnore
    public final boolean a(ServiceConnection serviceConnection) {
        return this.a.containsKey(serviceConnection);
    }

    @DexIgnore
    public final IBinder a() {
        return this.d;
    }
}
