package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w {
    @DexIgnore
    public CopyOnWriteArrayList<v> mCancellables; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public boolean mEnabled;

    @DexIgnore
    public w(boolean z) {
        this.mEnabled = z;
    }

    @DexIgnore
    public void addCancellable(v vVar) {
        this.mCancellables.add(vVar);
    }

    @DexIgnore
    public abstract void handleOnBackPressed();

    @DexIgnore
    public final boolean isEnabled() {
        return this.mEnabled;
    }

    @DexIgnore
    public final void remove() {
        Iterator<v> it = this.mCancellables.iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    @DexIgnore
    public void removeCancellable(v vVar) {
        this.mCancellables.remove(vVar);
    }

    @DexIgnore
    public final void setEnabled(boolean z) {
        this.mEnabled = z;
    }
}
