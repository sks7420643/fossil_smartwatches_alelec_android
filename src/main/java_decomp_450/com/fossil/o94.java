package com.fossil;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class o94 implements Callable {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public o94(Context context, Intent intent) {
        this.a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Integer.valueOf(eb4.b().c(this.a, this.b));
    }
}
