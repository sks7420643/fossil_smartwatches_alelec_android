package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q62 {
    @DexIgnore
    public q62(String str, String str2) {
        a72.a((Object) str, (Object) "log tag cannot be null");
        a72.a(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        if (str2 == null || str2.length() <= 0) {
        }
    }

    @DexIgnore
    public q62(String str) {
        this(str, null);
    }
}
