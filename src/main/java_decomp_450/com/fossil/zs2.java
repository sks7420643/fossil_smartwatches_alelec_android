package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs2<K, V> extends xs2<K, V> {
    @DexIgnore
    public final at2<K, V> a() {
        Set<Map.Entry<K, Collection<V>>> entrySet = ((xs2) this).a.entrySet();
        if (entrySet.isEmpty()) {
            return ns2.zza;
        }
        vs2 vs2 = new vs2(entrySet.size());
        int i = 0;
        for (Map.Entry<K, Collection<V>> entry : entrySet) {
            K key = entry.getKey();
            ws2 zza = ws2.zza(entry.getValue());
            if (!zza.isEmpty()) {
                int i2 = (vs2.b + 1) << 1;
                Object[] objArr = vs2.a;
                if (i2 > objArr.length) {
                    int length = objArr.length;
                    if (i2 >= 0) {
                        int i3 = length + (length >> 1) + 1;
                        if (i3 < i2) {
                            i3 = Integer.highestOneBit(i2 - 1) << 1;
                        }
                        if (i3 < 0) {
                            i3 = Integer.MAX_VALUE;
                        }
                        vs2.a = Arrays.copyOf(objArr, i3);
                    } else {
                        throw new AssertionError("cannot store more than MAX_VALUE elements");
                    }
                }
                as2.a(key, zza);
                Object[] objArr2 = vs2.a;
                int i4 = vs2.b;
                objArr2[i4 * 2] = key;
                objArr2[(i4 * 2) + 1] = zza;
                vs2.b = i4 + 1;
                i += zza.size();
            }
        }
        return new at2<>(lt2.zza(vs2.b, vs2.a), i, null);
    }
}
