package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ sf0 c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hb0 createFromParcel(Parcel parcel) {
            return new hb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hb0[] newArray(int i) {
            return new hb0[i];
        }
    }

    @DexIgnore
    public hb0(byte b, sf0 sf0, byte b2) {
        super(cb0.BATTERY, b);
        this.c = sf0;
        this.d = b2;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.l1, yz0.a(this.c)), r51.a5, Byte.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(hb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            hb0 hb0 = (hb0) obj;
            return this.c == hb0.c && this.d == hb0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.BatteryNotification");
    }

    @DexIgnore
    public final byte getBatteryPercentage() {
        return this.d;
    }

    @DexIgnore
    public final sf0 getBatteryState() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return Byte.valueOf(this.d).hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ hb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        sf0 a2 = sf0.c.a(parcel.readByte());
        this.c = a2 == null ? sf0.LOW : a2;
        this.d = parcel.readByte();
    }
}
