package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.be5;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja6 extends go5 implements ia6 {
    @DexIgnore
    public qw6<wx4> f;
    @DexIgnore
    public ha6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ ja6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ja6 ja6) {
            this.a = ja6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.c
        public void a(Calendar calendar) {
            ee7.b(calendar, "calendar");
            ha6 a2 = this.a.g;
            if (a2 != null) {
                Date time = calendar.getTime();
                ee7.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ ja6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(ja6 ja6) {
            this.a = ja6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(int i, Calendar calendar) {
            ee7.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                CaloriesDetailActivity.a aVar = CaloriesDetailActivity.A;
                Date time = calendar.getTime();
                ee7.a((Object) time, "it.time");
                ee7.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "CaloriesOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        wx4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        qw6<wx4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            be5.a aVar = be5.o;
            ha6 ha6 = this.g;
            if (aVar.a(ha6 != null ? ha6.h() : null)) {
                recyclerViewCalendar.b("dianaActiveCaloriesTab");
            } else {
                recyclerViewCalendar.b("hybridActiveCaloriesTab");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wx4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onCreateView");
        wx4 wx4 = (wx4) qb.a(layoutInflater, 2131558509, viewGroup, false, a1());
        RecyclerViewCalendar recyclerViewCalendar = wx4.q;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        wx4.q.setOnCalendarMonthChanged(new b(this));
        wx4.q.setOnCalendarItemClickListener(new c(this));
        this.f = new qw6<>(this, wx4);
        f1();
        qw6<wx4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onResume");
        f1();
        ha6 ha6 = this.g;
        if (ha6 != null) {
            ha6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onStop");
        ha6 ha6 = this.g;
        if (ha6 != null) {
            ha6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.ia6
    public void a(TreeMap<Long, Float> treeMap) {
        wx4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ee7.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        qw6<wx4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    @Override // com.fossil.ia6
    public void a(Date date, Date date2) {
        wx4 a2;
        ee7.b(date, "selectDate");
        ee7.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        qw6<wx4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            ee7.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            ee7.a((Object) instance2, "startCalendar");
            instance2.setTime(zd5.q(date2));
            ee7.a((Object) instance3, "endCalendar");
            instance3.setTime(zd5.l(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(ha6 ha6) {
        ee7.b(ha6, "presenter");
        this.g = ha6;
    }
}
