package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cd1 implements Parcelable.Creator<we1> {
    @DexIgnore
    public /* synthetic */ cd1(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public we1 createFromParcel(Parcel parcel) {
        return new we1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public we1[] newArray(int i) {
        return new we1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public we1 m8createFromParcel(Parcel parcel) {
        return new we1(parcel, null);
    }
}
