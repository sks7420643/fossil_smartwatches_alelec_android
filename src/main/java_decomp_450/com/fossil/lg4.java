package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg4 {
    @DexIgnore
    public static /* final */ String[] a; // = new String[0];

    @DexIgnore
    public static int a(int i) {
        return i >>> 3;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    @DexIgnore
    public static final int a(gg4 gg4, int i) throws IOException {
        int a2 = gg4.a();
        gg4.f(i);
        int i2 = 1;
        while (gg4.j() == i) {
            gg4.f(i);
            i2++;
        }
        gg4.e(a2);
        return i2;
    }

    @DexIgnore
    public static int b(int i) {
        return i & 7;
    }

    @DexIgnore
    public static boolean b(gg4 gg4, int i) throws IOException {
        return gg4.f(i);
    }
}
