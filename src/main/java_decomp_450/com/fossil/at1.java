package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class at1<T> extends ct1<T> {
    @DexIgnore
    public /* final */ Integer a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ dt1 c;

    @DexIgnore
    public at1(Integer num, T t, dt1 dt1) {
        this.a = num;
        if (t != null) {
            this.b = t;
            if (dt1 != null) {
                this.c = dt1;
                return;
            }
            throw new NullPointerException("Null priority");
        }
        throw new NullPointerException("Null payload");
    }

    @DexIgnore
    @Override // com.fossil.ct1
    public Integer a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ct1
    public T b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ct1
    public dt1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ct1)) {
            return false;
        }
        ct1 ct1 = (ct1) obj;
        Integer num = this.a;
        if (num != null ? num.equals(ct1.a()) : ct1.a() == null) {
            if (!this.b.equals(ct1.b()) || !this.c.equals(ct1.c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Event{code=" + this.a + ", payload=" + ((Object) this.b) + ", priority=" + this.c + "}";
    }
}
