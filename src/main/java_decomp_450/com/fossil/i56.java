package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i56 implements Factory<a66> {
    @DexIgnore
    public static a66 a(h56 h56) {
        a66 a = h56.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
