package com.fossil;

import android.view.View;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hk {
    @DexIgnore
    public /* final */ Map<String, Object> a; // = new HashMap();
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ ArrayList<Transition> c; // = new ArrayList<>();

    @DexIgnore
    @Deprecated
    public hk() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof hk)) {
            return false;
        }
        hk hkVar = (hk) obj;
        return this.b == hkVar.b && this.a.equals(hkVar.a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + "\n") + "    values:";
        for (String str2 : this.a.keySet()) {
            str = str + "    " + str2 + ": " + this.a.get(str2) + "\n";
        }
        return str;
    }

    @DexIgnore
    public hk(View view) {
        this.b = view;
    }
}
