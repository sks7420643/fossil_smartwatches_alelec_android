package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k24 extends z14 {
    @DexIgnore
    public static /* final */ ob4<Set<Object>> e; // = j24.a();
    @DexIgnore
    public /* final */ Map<c24<?>, r24<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, r24<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, r24<Set<?>>> c; // = new HashMap();
    @DexIgnore
    public /* final */ q24 d;

    @DexIgnore
    public k24(Executor executor, Iterable<g24> iterable, c24<?>... c24Arr) {
        this.d = new q24(executor);
        ArrayList<c24<?>> arrayList = new ArrayList();
        arrayList.add(c24.a(this.d, q24.class, i94.class, h94.class));
        for (g24 g24 : iterable) {
            arrayList.addAll(g24.getComponents());
        }
        for (c24<?> c24 : c24Arr) {
            if (c24 != null) {
                arrayList.add(c24);
            }
        }
        l24.a(arrayList);
        for (c24<?> c242 : arrayList) {
            this.a.put(c242, new r24<>(h24.a(this, c242)));
        }
        a();
        b();
    }

    @DexIgnore
    public final void b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<c24<?>, r24<?>> entry : this.a.entrySet()) {
            c24<?> key = entry.getKey();
            if (!key.g()) {
                r24<?> value = entry.getValue();
                for (Class<? super Object> cls : key.c()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(value);
                }
            }
        }
        for (Map.Entry entry2 : hashMap.entrySet()) {
            this.c.put((Class) entry2.getKey(), new r24<>(i24.a((Set) entry2.getValue())));
        }
    }

    @DexIgnore
    public final void c() {
        for (c24<?> c24 : this.a.keySet()) {
            Iterator<m24> it = c24.a().iterator();
            while (true) {
                if (it.hasNext()) {
                    m24 next = it.next();
                    if (next.c() && !this.b.containsKey(next.a())) {
                        throw new s24(String.format("Unsatisfied dependency for component %s: %s", c24, next.a()));
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a() {
        for (Map.Entry<c24<?>, r24<?>> entry : this.a.entrySet()) {
            c24<?> key = entry.getKey();
            if (key.g()) {
                r24<?> value = entry.getValue();
                for (Class<? super Object> cls : key.c()) {
                    this.b.put(cls, value);
                }
            }
        }
        c();
    }

    @DexIgnore
    public static /* synthetic */ Set a(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((r24) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    @Override // com.fossil.d24
    public <T> ob4<T> a(Class<T> cls) {
        t24.a(cls, "Null interface requested.");
        return this.b.get(cls);
    }

    @DexIgnore
    public void a(boolean z) {
        for (Map.Entry<c24<?>, r24<?>> entry : this.a.entrySet()) {
            c24<?> key = entry.getKey();
            r24<?> value = entry.getValue();
            if (key.e() || (key.f() && z)) {
                value.get();
            }
        }
        this.d.a();
    }

    @DexIgnore
    @Override // com.fossil.d24
    public <T> ob4<Set<T>> b(Class<T> cls) {
        r24<Set<?>> r24 = this.c.get(cls);
        return r24 != null ? r24 : (ob4<Set<T>>) e;
    }
}
