package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j73 extends wm2 implements i73 {
    @DexIgnore
    public j73() {
        super("com.google.android.gms.maps.internal.IOnMapClickListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onMapClick((LatLng) xm2.a(parcel, LatLng.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
