package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class vi3 implements Runnable {
    @DexIgnore
    public /* final */ ti3 a;

    @DexIgnore
    public vi3(ti3 ti3) {
        this.a = ti3;
    }

    @DexIgnore
    public final void run() {
        ti3 ti3 = this.a;
        ti3.g();
        if (ti3.k().x.a()) {
            ti3.e().A().a("Deferred Deep Link already retrieved. Not fetching again.");
            return;
        }
        long a2 = ti3.k().y.a();
        ti3.k().y.a(1 + a2);
        if (a2 >= 5) {
            ti3.e().w().a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
            ti3.k().x.a(true);
            return;
        }
        ((ii3) ti3).a.m();
    }
}
