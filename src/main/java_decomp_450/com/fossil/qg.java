package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qg extends RecyclerView.l implements RecyclerView.n {
    @DexIgnore
    public g A;
    @DexIgnore
    public /* final */ RecyclerView.p B; // = new b();
    @DexIgnore
    public Rect C;
    @DexIgnore
    public long D;
    @DexIgnore
    public /* final */ List<View> a; // = new ArrayList();
    @DexIgnore
    public /* final */ float[] b; // = new float[2];
    @DexIgnore
    public RecyclerView.ViewHolder c; // = null;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public f m;
    @DexIgnore
    public int n; // = 0;
    @DexIgnore
    public int o;
    @DexIgnore
    public List<h> p; // = new ArrayList();
    @DexIgnore
    public int q;
    @DexIgnore
    public RecyclerView r;
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public VelocityTracker t;
    @DexIgnore
    public List<RecyclerView.ViewHolder> u;
    @DexIgnore
    public List<Integer> v;
    @DexIgnore
    public RecyclerView.h w; // = null;
    @DexIgnore
    public View x; // = null;
    @DexIgnore
    public int y; // = -1;
    @DexIgnore
    public k9 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            qg qgVar = qg.this;
            if (qgVar.c != null && qgVar.f()) {
                qg qgVar2 = qg.this;
                RecyclerView.ViewHolder viewHolder = qgVar2.c;
                if (viewHolder != null) {
                    qgVar2.b(viewHolder);
                }
                qg qgVar3 = qg.this;
                qgVar3.r.removeCallbacks(qgVar3.s);
                da.a(qg.this.r, this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends h {
        @DexIgnore
        public /* final */ /* synthetic */ int n;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder o;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4, int i3, RecyclerView.ViewHolder viewHolder2) {
            super(viewHolder, i, i2, f, f2, f3, f4);
            this.n = i3;
            this.o = viewHolder2;
        }

        @DexIgnore
        @Override // com.fossil.qg.h
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (!((h) this).k) {
                if (this.n <= 0) {
                    qg qgVar = qg.this;
                    qgVar.m.a(qgVar.r, this.o);
                } else {
                    qg.this.a.add(this.o.itemView);
                    ((h) this).h = true;
                    int i = this.n;
                    if (i > 0) {
                        qg.this.a(this, i);
                    }
                }
                qg qgVar2 = qg.this;
                View view = qgVar2.x;
                View view2 = this.o.itemView;
                if (view == view2) {
                    qgVar2.c(view2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ h a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public d(h hVar, int i) {
            this.a = hVar;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            RecyclerView recyclerView = qg.this.r;
            if (recyclerView != null && recyclerView.isAttachedToWindow()) {
                h hVar = this.a;
                if (!hVar.k && hVar.e.getAdapterPosition() != -1) {
                    RecyclerView.j itemAnimator = qg.this.r.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.isRunning(null)) && !qg.this.c()) {
                        qg.this.m.b(this.a.e, this.b);
                    } else {
                        qg.this.r.post(this);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements RecyclerView.h {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.h
        public int a(int i, int i2) {
            qg qgVar = qg.this;
            View view = qgVar.x;
            if (view == null) {
                return i2;
            }
            int i3 = qgVar.y;
            if (i3 == -1) {
                i3 = qgVar.r.indexOfChild(view);
                qg.this.y = i3;
            }
            if (i2 == i - 1) {
                return i3;
            }
            return i2 < i3 ? i2 : i2 + 1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public boolean a; // = true;

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void a() {
            this.a = false;
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        @DexIgnore
        public void onLongPress(MotionEvent motionEvent) {
            View b2;
            RecyclerView.ViewHolder childViewHolder;
            int i;
            if (this.a && (b2 = qg.this.b(motionEvent)) != null && (childViewHolder = qg.this.r.getChildViewHolder(b2)) != null) {
                qg qgVar = qg.this;
                if (qgVar.m.d(qgVar.r, childViewHolder) && motionEvent.getPointerId(0) == (i = qg.this.l)) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    float x = motionEvent.getX(findPointerIndex);
                    float y = motionEvent.getY(findPointerIndex);
                    qg qgVar2 = qg.this;
                    qgVar2.d = x;
                    qgVar2.e = y;
                    qgVar2.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    qgVar2.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    if (qgVar2.m.c()) {
                        qg.this.c(childViewHolder, 2);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ float a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ RecyclerView.ViewHolder e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ ValueAnimator g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public boolean l; // = false;
        @DexIgnore
        public float m;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ValueAnimator.AnimatorUpdateListener {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                h.this.a(valueAnimator.getAnimatedFraction());
            }
        }

        @DexIgnore
        public h(RecyclerView.ViewHolder viewHolder, int i2, int i3, float f2, float f3, float f4, float f5) {
            this.f = i3;
            this.e = viewHolder;
            this.a = f2;
            this.b = f3;
            this.c = f4;
            this.d = f5;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
            this.g = ofFloat;
            ofFloat.addUpdateListener(new a());
            this.g.setTarget(viewHolder.itemView);
            this.g.addListener(this);
            a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public void a(long j2) {
            this.g.setDuration(j2);
        }

        @DexIgnore
        public void b() {
            this.e.setIsRecyclable(false);
            this.g.start();
        }

        @DexIgnore
        public void c() {
            float f2 = this.a;
            float f3 = this.c;
            if (f2 == f3) {
                this.i = this.e.itemView.getTranslationX();
            } else {
                this.i = f2 + (this.m * (f3 - f2));
            }
            float f4 = this.b;
            float f5 = this.d;
            if (f4 == f5) {
                this.j = this.e.itemView.getTranslationY();
            } else {
                this.j = f4 + (this.m * (f5 - f4));
            }
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            a(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.l) {
                this.e.setIsRecyclable(true);
            }
            this.l = true;
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }

        @DexIgnore
        public void a() {
            this.g.cancel();
        }

        @DexIgnore
        public void a(float f2) {
            this.m = f2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i extends f {
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public i(int i, int i2) {
            this.d = i2;
            this.e = i;
        }

        @DexIgnore
        @Override // com.fossil.qg.f
        public int c(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return f.d(e(recyclerView, viewHolder), f(recyclerView, viewHolder));
        }

        @DexIgnore
        public int e(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.e;
        }

        @DexIgnore
        public int f(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.d;
        }
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        void a(View view, View view2, int i, int i2);
    }

    @DexIgnore
    public qg(f fVar) {
        this.m = fVar;
    }

    @DexIgnore
    public static boolean a(View view, float f2, float f3, float f4, float f5) {
        return f2 >= f4 && f2 <= f4 + ((float) view.getWidth()) && f3 >= f5 && f3 <= f5 + ((float) view.getHeight());
    }

    @DexIgnore
    public final void b() {
        this.r.removeItemDecoration(this);
        this.r.removeOnItemTouchListener(this.B);
        this.r.removeOnChildAttachStateChangeListener(this);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            this.m.a(this.r, this.p.get(0).e);
        }
        this.p.clear();
        this.x = null;
        this.y = -1;
        e();
        i();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void b(View view) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0137  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(androidx.recyclerview.widget.RecyclerView.ViewHolder r24, int r25) {
        /*
            r23 = this;
            r11 = r23
            r12 = r24
            r13 = r25
            androidx.recyclerview.widget.RecyclerView$ViewHolder r0 = r11.c
            if (r12 != r0) goto L_0x000f
            int r0 = r11.n
            if (r13 != r0) goto L_0x000f
            return
        L_0x000f:
            r0 = -9223372036854775808
            r11.D = r0
            int r4 = r11.n
            r14 = 1
            r11.a(r12, r14)
            r11.n = r13
            r15 = 2
            if (r13 != r15) goto L_0x0030
            if (r12 == 0) goto L_0x0028
            android.view.View r0 = r12.itemView
            r11.x = r0
            r23.a()
            goto L_0x0030
        L_0x0028:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Must pass a ViewHolder when dragging"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            int r0 = r13 * 8
            r10 = 8
            int r0 = r0 + r10
            int r0 = r14 << r0
            int r16 = r0 + -1
            androidx.recyclerview.widget.RecyclerView$ViewHolder r9 = r11.c
            r8 = 0
            if (r9 == 0) goto L_0x00ee
            android.view.View r0 = r9.itemView
            android.view.ViewParent r0 = r0.getParent()
            if (r0 == 0) goto L_0x00da
            if (r4 != r15) goto L_0x004a
            r7 = 0
            goto L_0x004f
        L_0x004a:
            int r0 = r11.c(r9)
            r7 = r0
        L_0x004f:
            r23.e()
            r0 = 4
            r1 = 0
            if (r7 == r14) goto L_0x007b
            if (r7 == r15) goto L_0x007b
            if (r7 == r0) goto L_0x0067
            if (r7 == r10) goto L_0x0067
            r2 = 16
            if (r7 == r2) goto L_0x0067
            r2 = 32
            if (r7 == r2) goto L_0x0067
            r17 = 0
            goto L_0x0078
        L_0x0067:
            float r2 = r11.h
            float r2 = java.lang.Math.signum(r2)
            androidx.recyclerview.widget.RecyclerView r3 = r11.r
            int r3 = r3.getWidth()
            float r3 = (float) r3
            float r2 = r2 * r3
            r17 = r2
        L_0x0078:
            r18 = 0
            goto L_0x008e
        L_0x007b:
            float r2 = r11.i
            float r2 = java.lang.Math.signum(r2)
            androidx.recyclerview.widget.RecyclerView r3 = r11.r
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r2 = r2 * r3
            r18 = r2
            r17 = 0
        L_0x008e:
            if (r4 != r15) goto L_0x0093
            r6 = 8
            goto L_0x0098
        L_0x0093:
            if (r7 <= 0) goto L_0x0097
            r6 = 2
            goto L_0x0098
        L_0x0097:
            r6 = 4
        L_0x0098:
            float[] r0 = r11.b
            r11.a(r0)
            float[] r0 = r11.b
            r19 = r0[r8]
            r20 = r0[r14]
            com.fossil.qg$c r5 = new com.fossil.qg$c
            r0 = r5
            r1 = r23
            r2 = r9
            r3 = r6
            r14 = r5
            r5 = r19
            r15 = r6
            r6 = r20
            r21 = r7
            r7 = r17
            r8 = r18
            r22 = r9
            r9 = r21
            r21 = 8
            r10 = r22
            r0.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            com.fossil.qg$f r0 = r11.m
            androidx.recyclerview.widget.RecyclerView r1 = r11.r
            float r2 = r17 - r19
            float r3 = r18 - r20
            long r0 = r0.a(r1, r15, r2, r3)
            r14.a(r0)
            java.util.List<com.fossil.qg$h> r0 = r11.p
            r0.add(r14)
            r14.b()
            r8 = 1
            goto L_0x00ea
        L_0x00da:
            r0 = r9
            r21 = 8
            android.view.View r1 = r0.itemView
            r11.c(r1)
            com.fossil.qg$f r1 = r11.m
            androidx.recyclerview.widget.RecyclerView r2 = r11.r
            r1.a(r2, r0)
            r8 = 0
        L_0x00ea:
            r0 = 0
            r11.c = r0
            goto L_0x00f1
        L_0x00ee:
            r21 = 8
            r8 = 0
        L_0x00f1:
            if (r12 == 0) goto L_0x0122
            com.fossil.qg$f r0 = r11.m
            androidx.recyclerview.widget.RecyclerView r1 = r11.r
            int r0 = r0.b(r1, r12)
            r0 = r0 & r16
            int r1 = r11.n
            int r1 = r1 * 8
            int r0 = r0 >> r1
            r11.o = r0
            android.view.View r0 = r12.itemView
            int r0 = r0.getLeft()
            float r0 = (float) r0
            r11.j = r0
            android.view.View r0 = r12.itemView
            int r0 = r0.getTop()
            float r0 = (float) r0
            r11.k = r0
            r11.c = r12
            r0 = 2
            if (r13 != r0) goto L_0x0122
            android.view.View r0 = r12.itemView
            r1 = 0
            r0.performHapticFeedback(r1)
            goto L_0x0123
        L_0x0122:
            r1 = 0
        L_0x0123:
            androidx.recyclerview.widget.RecyclerView r0 = r11.r
            android.view.ViewParent r0 = r0.getParent()
            if (r0 == 0) goto L_0x0135
            androidx.recyclerview.widget.RecyclerView$ViewHolder r2 = r11.c
            if (r2 == 0) goto L_0x0131
            r14 = 1
            goto L_0x0132
        L_0x0131:
            r14 = 0
        L_0x0132:
            r0.requestDisallowInterceptTouchEvent(r14)
        L_0x0135:
            if (r8 != 0) goto L_0x0140
            androidx.recyclerview.widget.RecyclerView r0 = r11.r
            androidx.recyclerview.widget.RecyclerView$m r0 = r0.getLayoutManager()
            r0.A()
        L_0x0140:
            com.fossil.qg$f r0 = r11.m
            androidx.recyclerview.widget.RecyclerView$ViewHolder r1 = r11.c
            int r2 = r11.n
            r0.a(r1, r2)
            androidx.recyclerview.widget.RecyclerView r0 = r11.r
            r0.invalidate()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qg.c(androidx.recyclerview.widget.RecyclerView$ViewHolder, int):void");
    }

    @DexIgnore
    public void d() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        this.t = VelocityTracker.obtain();
    }

    @DexIgnore
    public final void e() {
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.t = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c5, code lost:
        if (r1 > 0) goto L_0x00c9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0104 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0110  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean f() {
        /*
            r16 = this;
            r0 = r16
            androidx.recyclerview.widget.RecyclerView$ViewHolder r1 = r0.c
            r2 = 0
            r3 = -9223372036854775808
            if (r1 != 0) goto L_0x000c
            r0.D = r3
            return r2
        L_0x000c:
            long r5 = java.lang.System.currentTimeMillis()
            long r7 = r0.D
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x0019
            r7 = 0
            goto L_0x001b
        L_0x0019:
            long r7 = r5 - r7
        L_0x001b:
            androidx.recyclerview.widget.RecyclerView r1 = r0.r
            androidx.recyclerview.widget.RecyclerView$m r1 = r1.getLayoutManager()
            android.graphics.Rect r9 = r0.C
            if (r9 != 0) goto L_0x002c
            android.graphics.Rect r9 = new android.graphics.Rect
            r9.<init>()
            r0.C = r9
        L_0x002c:
            androidx.recyclerview.widget.RecyclerView$ViewHolder r9 = r0.c
            android.view.View r9 = r9.itemView
            android.graphics.Rect r10 = r0.C
            r1.a(r9, r10)
            boolean r9 = r1.a()
            r10 = 0
            if (r9 == 0) goto L_0x007f
            float r9 = r0.j
            float r11 = r0.h
            float r9 = r9 + r11
            int r9 = (int) r9
            android.graphics.Rect r11 = r0.C
            int r11 = r11.left
            int r11 = r9 - r11
            androidx.recyclerview.widget.RecyclerView r12 = r0.r
            int r12 = r12.getPaddingLeft()
            int r11 = r11 - r12
            float r12 = r0.h
            int r12 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))
            if (r12 >= 0) goto L_0x0059
            if (r11 >= 0) goto L_0x0059
            r12 = r11
            goto L_0x0080
        L_0x0059:
            float r11 = r0.h
            int r11 = (r11 > r10 ? 1 : (r11 == r10 ? 0 : -1))
            if (r11 <= 0) goto L_0x007f
            androidx.recyclerview.widget.RecyclerView$ViewHolder r11 = r0.c
            android.view.View r11 = r11.itemView
            int r11 = r11.getWidth()
            int r9 = r9 + r11
            android.graphics.Rect r11 = r0.C
            int r11 = r11.right
            int r9 = r9 + r11
            androidx.recyclerview.widget.RecyclerView r11 = r0.r
            int r11 = r11.getWidth()
            androidx.recyclerview.widget.RecyclerView r12 = r0.r
            int r12 = r12.getPaddingRight()
            int r11 = r11 - r12
            int r9 = r9 - r11
            if (r9 <= 0) goto L_0x007f
            r12 = r9
            goto L_0x0080
        L_0x007f:
            r12 = 0
        L_0x0080:
            boolean r1 = r1.b()
            if (r1 == 0) goto L_0x00c8
            float r1 = r0.k
            float r9 = r0.i
            float r1 = r1 + r9
            int r1 = (int) r1
            android.graphics.Rect r9 = r0.C
            int r9 = r9.top
            int r9 = r1 - r9
            androidx.recyclerview.widget.RecyclerView r11 = r0.r
            int r11 = r11.getPaddingTop()
            int r9 = r9 - r11
            float r11 = r0.i
            int r11 = (r11 > r10 ? 1 : (r11 == r10 ? 0 : -1))
            if (r11 >= 0) goto L_0x00a3
            if (r9 >= 0) goto L_0x00a3
            r1 = r9
            goto L_0x00c9
        L_0x00a3:
            float r9 = r0.i
            int r9 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r9 <= 0) goto L_0x00c8
            androidx.recyclerview.widget.RecyclerView$ViewHolder r9 = r0.c
            android.view.View r9 = r9.itemView
            int r9 = r9.getHeight()
            int r1 = r1 + r9
            android.graphics.Rect r9 = r0.C
            int r9 = r9.bottom
            int r1 = r1 + r9
            androidx.recyclerview.widget.RecyclerView r9 = r0.r
            int r9 = r9.getHeight()
            androidx.recyclerview.widget.RecyclerView r10 = r0.r
            int r10 = r10.getPaddingBottom()
            int r9 = r9 - r10
            int r1 = r1 - r9
            if (r1 <= 0) goto L_0x00c8
            goto L_0x00c9
        L_0x00c8:
            r1 = 0
        L_0x00c9:
            if (r12 == 0) goto L_0x00e2
            com.fossil.qg$f r9 = r0.m
            androidx.recyclerview.widget.RecyclerView r10 = r0.r
            androidx.recyclerview.widget.RecyclerView$ViewHolder r11 = r0.c
            android.view.View r11 = r11.itemView
            int r11 = r11.getWidth()
            androidx.recyclerview.widget.RecyclerView r13 = r0.r
            int r13 = r13.getWidth()
            r14 = r7
            int r12 = r9.a(r10, r11, r12, r13, r14)
        L_0x00e2:
            r14 = r12
            if (r1 == 0) goto L_0x0101
            com.fossil.qg$f r9 = r0.m
            androidx.recyclerview.widget.RecyclerView r10 = r0.r
            androidx.recyclerview.widget.RecyclerView$ViewHolder r11 = r0.c
            android.view.View r11 = r11.itemView
            int r11 = r11.getHeight()
            androidx.recyclerview.widget.RecyclerView r12 = r0.r
            int r13 = r12.getHeight()
            r12 = r1
            r1 = r14
            r14 = r7
            int r7 = r9.a(r10, r11, r12, r13, r14)
            r12 = r1
            r1 = r7
            goto L_0x0102
        L_0x0101:
            r12 = r14
        L_0x0102:
            if (r12 != 0) goto L_0x010a
            if (r1 == 0) goto L_0x0107
            goto L_0x010a
        L_0x0107:
            r0.D = r3
            return r2
        L_0x010a:
            long r7 = r0.D
            int r2 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r2 != 0) goto L_0x0112
            r0.D = r5
        L_0x0112:
            androidx.recyclerview.widget.RecyclerView r2 = r0.r
            r2.scrollBy(r12, r1)
            r1 = 1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qg.f():boolean");
    }

    @DexIgnore
    public final void g() {
        this.q = ViewConfiguration.get(this.r.getContext()).getScaledTouchSlop();
        this.r.addItemDecoration(this);
        this.r.addOnItemTouchListener(this.B);
        this.r.addOnChildAttachStateChangeListener(this);
        h();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        rect.setEmpty();
    }

    @DexIgnore
    public final void h() {
        this.A = new g();
        this.z = new k9(this.r.getContext(), this.A);
    }

    @DexIgnore
    public final void i() {
        g gVar = this.A;
        if (gVar != null) {
            gVar.a();
            this.A = null;
        }
        if (this.z != null) {
            this.z = null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        this.y = -1;
        if (this.c != null) {
            a(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f2 = fArr[1];
            f3 = f4;
        } else {
            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.m.a(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        float f2;
        float f3;
        if (this.c != null) {
            a(this.b);
            float[] fArr = this.b;
            float f4 = fArr[0];
            f2 = fArr[1];
            f3 = f4;
        } else {
            f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.m.b(canvas, recyclerView, this.c, this.p, this.n, f3, f2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {
        @DexIgnore
        public static /* final */ Interpolator b; // = new a();
        @DexIgnore
        public static /* final */ Interpolator c; // = new b();
        @DexIgnore
        public int a; // = -1;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class b implements Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        }

        @DexIgnore
        public static int b(int i, int i2) {
            int i3;
            int i4 = i & 789516;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 << 2;
            } else {
                int i6 = i4 << 1;
                i5 |= -789517 & i6;
                i3 = (i6 & 789516) << 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public static int c(int i, int i2) {
            return i2 << (i * 8);
        }

        @DexIgnore
        public static int d(int i, int i2) {
            int c2 = c(0, i2 | i);
            return c(2, i) | c(1, i2) | c2;
        }

        @DexIgnore
        public float a(float f) {
            return f;
        }

        @DexIgnore
        public float a(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public int a() {
            return 0;
        }

        @DexIgnore
        public int a(int i, int i2) {
            int i3;
            int i4 = i & 3158064;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 >> 2;
            } else {
                int i6 = i4 >> 1;
                i5 |= -3158065 & i6;
                i3 = (i6 & 3158064) >> 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        public RecyclerView.ViewHolder a(RecyclerView.ViewHolder viewHolder, List<RecyclerView.ViewHolder> list, int i, int i2) {
            int bottom;
            int abs;
            int top;
            int abs2;
            int left;
            int abs3;
            int right;
            int abs4;
            int width = i + viewHolder.itemView.getWidth();
            int height = i2 + viewHolder.itemView.getHeight();
            int left2 = i - viewHolder.itemView.getLeft();
            int top2 = i2 - viewHolder.itemView.getTop();
            int size = list.size();
            RecyclerView.ViewHolder viewHolder2 = null;
            int i3 = -1;
            for (int i4 = 0; i4 < size; i4++) {
                RecyclerView.ViewHolder viewHolder3 = list.get(i4);
                if (left2 > 0 && (right = viewHolder3.itemView.getRight() - width) < 0 && viewHolder3.itemView.getRight() > viewHolder.itemView.getRight() && (abs4 = Math.abs(right)) > i3) {
                    viewHolder2 = viewHolder3;
                    i3 = abs4;
                }
                if (left2 < 0 && (left = viewHolder3.itemView.getLeft() - i) > 0 && viewHolder3.itemView.getLeft() < viewHolder.itemView.getLeft() && (abs3 = Math.abs(left)) > i3) {
                    viewHolder2 = viewHolder3;
                    i3 = abs3;
                }
                if (top2 < 0 && (top = viewHolder3.itemView.getTop() - i2) > 0 && viewHolder3.itemView.getTop() < viewHolder.itemView.getTop() && (abs2 = Math.abs(top)) > i3) {
                    viewHolder2 = viewHolder3;
                    i3 = abs2;
                }
                if (top2 > 0 && (bottom = viewHolder3.itemView.getBottom() - height) < 0 && viewHolder3.itemView.getBottom() > viewHolder.itemView.getBottom() && (abs = Math.abs(bottom)) > i3) {
                    viewHolder2 = viewHolder3;
                    i3 = abs;
                }
            }
            return viewHolder2;
        }

        @DexIgnore
        public boolean a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return true;
        }

        @DexIgnore
        public float b(float f) {
            return f;
        }

        @DexIgnore
        public float b(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        public final int b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return a(c(recyclerView, viewHolder), da.p(recyclerView));
        }

        @DexIgnore
        public abstract void b(RecyclerView.ViewHolder viewHolder, int i);

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public abstract boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2);

        @DexIgnore
        public abstract int c(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        public boolean c() {
            return true;
        }

        @DexIgnore
        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            int size = list.size();
            boolean z = false;
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                int save = canvas.save();
                b(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                b(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            for (int i3 = size - 1; i3 >= 0; i3--) {
                h hVar2 = list.get(i3);
                if (hVar2.l && !hVar2.h) {
                    list.remove(i3);
                } else if (!hVar2.l) {
                    z = true;
                }
            }
            if (z) {
                recyclerView.invalidate();
            }
        }

        @DexIgnore
        public boolean d(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return (b(recyclerView, viewHolder) & 16711680) != 0;
        }

        @DexIgnore
        public void b(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            sg.a.a(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public void a(RecyclerView.ViewHolder viewHolder, int i) {
            if (viewHolder != null) {
                sg.a.b(viewHolder.itemView);
            }
        }

        @DexIgnore
        public final int a(RecyclerView recyclerView) {
            if (this.a == -1) {
                this.a = recyclerView.getResources().getDimensionPixelSize(dg.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.a;
        }

        @DexIgnore
        public void a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int i, RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4) {
            RecyclerView.m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof j) {
                ((j) layoutManager).a(viewHolder.itemView, viewHolder2.itemView, i3, i4);
                return;
            }
            if (layoutManager.a()) {
                if (layoutManager.f(viewHolder2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.i(viewHolder2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
            if (layoutManager.b()) {
                if (layoutManager.j(viewHolder2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.scrollToPosition(i2);
                }
                if (layoutManager.e(viewHolder2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.scrollToPosition(i2);
                }
            }
        }

        @DexIgnore
        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<h> list, int i, float f, float f2) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                h hVar = list.get(i2);
                hVar.c();
                int save = canvas.save();
                a(canvas, recyclerView, hVar.e, hVar.i, hVar.j, hVar.f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                a(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        @DexIgnore
        public void a(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            sg.a.a(viewHolder.itemView);
        }

        @DexIgnore
        public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            sg.a.b(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        public long a(RecyclerView recyclerView, int i, float f, float f2) {
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i == 8 ? 200 : 250;
            }
            if (i == 8) {
                return itemAnimator.getMoveDuration();
            }
            return itemAnimator.getRemoveDuration();
        }

        @DexIgnore
        public int a(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f = 1.0f;
            int signum = (int) (((float) (((int) Math.signum((float) i2)) * a(recyclerView))) * c.getInterpolation(Math.min(1.0f, (((float) Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (((float) signum) * b.getInterpolation(f));
            if (interpolation == 0) {
                return i2 > 0 ? 1 : -1;
            }
            return interpolation;
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.r;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                b();
            }
            this.r = recyclerView;
            if (recyclerView != null) {
                Resources resources = recyclerView.getResources();
                this.f = resources.getDimension(dg.item_touch_helper_swipe_escape_velocity);
                this.g = resources.getDimension(dg.item_touch_helper_swipe_escape_max_velocity);
                g();
            }
        }
    }

    @DexIgnore
    public final void a(float[] fArr) {
        if ((this.o & 12) != 0) {
            fArr[0] = (this.j + this.h) - ((float) this.c.itemView.getLeft());
        } else {
            fArr[0] = this.c.itemView.getTranslationX();
        }
        if ((this.o & 3) != 0) {
            fArr[1] = (this.k + this.i) - ((float) this.c.itemView.getTop());
        } else {
            fArr[1] = this.c.itemView.getTranslationY();
        }
    }

    @DexIgnore
    public void b(RecyclerView.ViewHolder viewHolder) {
        if (!this.r.isLayoutRequested() && this.n == 2) {
            float a2 = this.m.a(viewHolder);
            int i2 = (int) (this.j + this.h);
            int i3 = (int) (this.k + this.i);
            if (((float) Math.abs(i3 - viewHolder.itemView.getTop())) >= ((float) viewHolder.itemView.getHeight()) * a2 || ((float) Math.abs(i2 - viewHolder.itemView.getLeft())) >= ((float) viewHolder.itemView.getWidth()) * a2) {
                List<RecyclerView.ViewHolder> a3 = a(viewHolder);
                if (a3.size() != 0) {
                    RecyclerView.ViewHolder a4 = this.m.a(viewHolder, a3, i2, i3);
                    if (a4 == null) {
                        this.u.clear();
                        this.v.clear();
                        return;
                    }
                    int adapterPosition = a4.getAdapterPosition();
                    int adapterPosition2 = viewHolder.getAdapterPosition();
                    if (this.m.b(this.r, viewHolder, a4)) {
                        this.m.a(this.r, viewHolder, adapterPosition2, a4, adapterPosition, i2, i3);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(h hVar, int i2) {
        this.r.post(new d(hVar, i2));
    }

    @DexIgnore
    public final List<RecyclerView.ViewHolder> a(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        List<RecyclerView.ViewHolder> list = this.u;
        if (list == null) {
            this.u = new ArrayList();
            this.v = new ArrayList();
        } else {
            list.clear();
            this.v.clear();
        }
        int a2 = this.m.a();
        int round = Math.round(this.j + this.h) - a2;
        int round2 = Math.round(this.k + this.i) - a2;
        int i2 = a2 * 2;
        int width = viewHolder2.itemView.getWidth() + round + i2;
        int height = viewHolder2.itemView.getHeight() + round2 + i2;
        int i3 = (round + width) / 2;
        int i4 = (round2 + height) / 2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int e2 = layoutManager.e();
        int i5 = 0;
        while (i5 < e2) {
            View d2 = layoutManager.d(i5);
            if (d2 != viewHolder2.itemView && d2.getBottom() >= round2 && d2.getTop() <= height && d2.getRight() >= round && d2.getLeft() <= width) {
                RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(d2);
                if (this.m.a(this.r, this.c, childViewHolder)) {
                    int abs = Math.abs(i3 - ((d2.getLeft() + d2.getRight()) / 2));
                    int abs2 = Math.abs(i4 - ((d2.getTop() + d2.getBottom()) / 2));
                    int i6 = (abs * abs) + (abs2 * abs2);
                    int size = this.u.size();
                    int i7 = 0;
                    int i8 = 0;
                    while (i7 < size && i6 > this.v.get(i7).intValue()) {
                        i8++;
                        i7++;
                    }
                    this.u.add(i8, childViewHolder);
                    this.v.add(i8, Integer.valueOf(i6));
                }
            }
            i5++;
            viewHolder2 = viewHolder;
        }
        return this.u;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements RecyclerView.p {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            qg.this.z.a(motionEvent);
            VelocityTracker velocityTracker = qg.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (qg.this.l != -1) {
                int actionMasked = motionEvent.getActionMasked();
                int findPointerIndex = motionEvent.findPointerIndex(qg.this.l);
                if (findPointerIndex >= 0) {
                    qg.this.a(actionMasked, motionEvent, findPointerIndex);
                }
                qg qgVar = qg.this;
                RecyclerView.ViewHolder viewHolder = qgVar.c;
                if (viewHolder != null) {
                    int i = 0;
                    if (actionMasked != 1) {
                        if (actionMasked != 2) {
                            if (actionMasked == 3) {
                                VelocityTracker velocityTracker2 = qgVar.t;
                                if (velocityTracker2 != null) {
                                    velocityTracker2.clear();
                                }
                            } else if (actionMasked == 6) {
                                int actionIndex = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex) == qg.this.l) {
                                    if (actionIndex == 0) {
                                        i = 1;
                                    }
                                    qg.this.l = motionEvent.getPointerId(i);
                                    qg qgVar2 = qg.this;
                                    qgVar2.a(motionEvent, qgVar2.o, actionIndex);
                                    return;
                                }
                                return;
                            } else {
                                return;
                            }
                        } else if (findPointerIndex >= 0) {
                            qgVar.a(motionEvent, qgVar.o, findPointerIndex);
                            qg.this.b(viewHolder);
                            qg qgVar3 = qg.this;
                            qgVar3.r.removeCallbacks(qgVar3.s);
                            qg.this.s.run();
                            qg.this.r.invalidate();
                            return;
                        } else {
                            return;
                        }
                    }
                    qg.this.c(null, 0);
                    qg.this.l = -1;
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            int findPointerIndex;
            h a2;
            qg.this.z.a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                qg.this.l = motionEvent.getPointerId(0);
                qg.this.d = motionEvent.getX();
                qg.this.e = motionEvent.getY();
                qg.this.d();
                qg qgVar = qg.this;
                if (qgVar.c == null && (a2 = qgVar.a(motionEvent)) != null) {
                    qg qgVar2 = qg.this;
                    qgVar2.d -= a2.i;
                    qgVar2.e -= a2.j;
                    qgVar2.a(a2.e, true);
                    if (qg.this.a.remove(a2.e.itemView)) {
                        qg qgVar3 = qg.this;
                        qgVar3.m.a(qgVar3.r, a2.e);
                    }
                    qg.this.c(a2.e, a2.f);
                    qg qgVar4 = qg.this;
                    qgVar4.a(motionEvent, qgVar4.o, 0);
                }
            } else if (actionMasked == 3 || actionMasked == 1) {
                qg qgVar5 = qg.this;
                qgVar5.l = -1;
                qgVar5.c(null, 0);
            } else {
                int i = qg.this.l;
                if (i != -1 && (findPointerIndex = motionEvent.findPointerIndex(i)) >= 0) {
                    qg.this.a(actionMasked, motionEvent, findPointerIndex);
                }
            }
            VelocityTracker velocityTracker = qg.this.t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (qg.this.c != null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.p
        public void a(boolean z) {
            if (z) {
                qg.this.c(null, 0);
            }
        }
    }

    @DexIgnore
    public View b(MotionEvent motionEvent) {
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        RecyclerView.ViewHolder viewHolder = this.c;
        if (viewHolder != null) {
            View view = viewHolder.itemView;
            if (a(view, x2, y2, this.j + this.h, this.k + this.i)) {
                return view;
            }
        }
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            View view2 = hVar.e.itemView;
            if (a(view2, x2, y2, hVar.i, hVar.j)) {
                return view2;
            }
        }
        return this.r.findChildViewUnder(x2, y2);
    }

    @DexIgnore
    public boolean c() {
        int size = this.p.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!this.p.get(i2).l) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int b(RecyclerView.ViewHolder viewHolder, int i2) {
        if ((i2 & 3) == 0) {
            return 0;
        }
        int i3 = 2;
        int i4 = this.i > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 2 : 1;
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null && this.l > -1) {
            velocityTracker.computeCurrentVelocity(1000, this.m.b(this.g));
            float xVelocity = this.t.getXVelocity(this.l);
            float yVelocity = this.t.getYVelocity(this.l);
            if (yVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = 1;
            }
            float abs = Math.abs(yVelocity);
            if ((i3 & i2) != 0 && i3 == i4 && abs >= this.m.a(this.f) && abs > Math.abs(xVelocity)) {
                return i3;
            }
        }
        float height = ((float) this.r.getHeight()) * this.m.b(viewHolder);
        if ((i2 & i4) == 0 || Math.abs(this.i) <= height) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public final RecyclerView.ViewHolder c(MotionEvent motionEvent) {
        View b2;
        RecyclerView.m layoutManager = this.r.getLayoutManager();
        int i2 = this.l;
        if (i2 == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i2);
        float abs = Math.abs(motionEvent.getX(findPointerIndex) - this.d);
        float abs2 = Math.abs(motionEvent.getY(findPointerIndex) - this.e);
        int i3 = this.q;
        if (abs < ((float) i3) && abs2 < ((float) i3)) {
            return null;
        }
        if (abs > abs2 && layoutManager.a()) {
            return null;
        }
        if ((abs2 <= abs || !layoutManager.b()) && (b2 = b(motionEvent)) != null) {
            return this.r.getChildViewHolder(b2);
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void a(View view) {
        c(view);
        RecyclerView.ViewHolder childViewHolder = this.r.getChildViewHolder(view);
        if (childViewHolder != null) {
            RecyclerView.ViewHolder viewHolder = this.c;
            if (viewHolder == null || childViewHolder != viewHolder) {
                a(childViewHolder, false);
                if (this.a.remove(childViewHolder.itemView)) {
                    this.m.a(this.r, childViewHolder);
                    return;
                }
                return;
            }
            c(null, 0);
        }
    }

    @DexIgnore
    public final int c(RecyclerView.ViewHolder viewHolder) {
        if (this.n == 2) {
            return 0;
        }
        int c2 = this.m.c(this.r, viewHolder);
        int a2 = (this.m.a(c2, da.p(this.r)) & 65280) >> 8;
        if (a2 == 0) {
            return 0;
        }
        int i2 = (c2 & 65280) >> 8;
        if (Math.abs(this.h) > Math.abs(this.i)) {
            int a3 = a(viewHolder, a2);
            if (a3 > 0) {
                return (i2 & a3) == 0 ? f.b(a3, da.p(this.r)) : a3;
            }
            int b2 = b(viewHolder, a2);
            if (b2 > 0) {
                return b2;
            }
        } else {
            int b3 = b(viewHolder, a2);
            if (b3 > 0) {
                return b3;
            }
            int a4 = a(viewHolder, a2);
            if (a4 > 0) {
                return (i2 & a4) == 0 ? f.b(a4, da.p(this.r)) : a4;
            }
        }
        return 0;
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder, boolean z2) {
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e == viewHolder) {
                hVar.k |= z2;
                if (!hVar.l) {
                    hVar.a();
                }
                this.p.remove(size);
                return;
            }
        }
    }

    @DexIgnore
    public void a(int i2, MotionEvent motionEvent, int i3) {
        RecyclerView.ViewHolder c2;
        int b2;
        if (this.c == null && i2 == 2 && this.n != 2 && this.m.b() && this.r.getScrollState() != 1 && (c2 = c(motionEvent)) != null && (b2 = (this.m.b(this.r, c2) & 65280) >> 8) != 0) {
            float x2 = motionEvent.getX(i3);
            float y2 = motionEvent.getY(i3);
            float f2 = x2 - this.d;
            float f3 = y2 - this.e;
            float abs = Math.abs(f2);
            float abs2 = Math.abs(f3);
            int i4 = this.q;
            if (abs >= ((float) i4) || abs2 >= ((float) i4)) {
                if (abs > abs2) {
                    if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 4) == 0) {
                        return;
                    }
                    if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 8) == 0) {
                        return;
                    }
                } else if (f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 1) == 0) {
                    return;
                } else {
                    if (f3 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b2 & 2) == 0) {
                        return;
                    }
                }
                this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.l = motionEvent.getPointerId(0);
                c(c2, 1);
            }
        }
    }

    @DexIgnore
    public void c(View view) {
        if (view == this.x) {
            this.x = null;
            if (this.w != null) {
                this.r.setChildDrawingOrderCallback(null);
            }
        }
    }

    @DexIgnore
    public h a(MotionEvent motionEvent) {
        if (this.p.isEmpty()) {
            return null;
        }
        View b2 = b(motionEvent);
        for (int size = this.p.size() - 1; size >= 0; size--) {
            h hVar = this.p.get(size);
            if (hVar.e.itemView == b2) {
                return hVar;
            }
        }
        return null;
    }

    @DexIgnore
    public void a(MotionEvent motionEvent, int i2, int i3) {
        float x2 = motionEvent.getX(i3);
        float y2 = motionEvent.getY(i3);
        float f2 = x2 - this.d;
        this.h = f2;
        this.i = y2 - this.e;
        if ((i2 & 4) == 0) {
            this.h = Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2);
        }
        if ((i2 & 8) == 0) {
            this.h = Math.min((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.h);
        }
        if ((i2 & 1) == 0) {
            this.i = Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
        if ((i2 & 2) == 0) {
            this.i = Math.min((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.i);
        }
    }

    @DexIgnore
    public final int a(RecyclerView.ViewHolder viewHolder, int i2) {
        if ((i2 & 12) == 0) {
            return 0;
        }
        int i3 = 8;
        int i4 = this.h > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 8 : 4;
        VelocityTracker velocityTracker = this.t;
        if (velocityTracker != null && this.l > -1) {
            velocityTracker.computeCurrentVelocity(1000, this.m.b(this.g));
            float xVelocity = this.t.getXVelocity(this.l);
            float yVelocity = this.t.getYVelocity(this.l);
            if (xVelocity <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = 4;
            }
            float abs = Math.abs(xVelocity);
            if ((i3 & i2) != 0 && i4 == i3 && abs >= this.m.a(this.f) && abs > Math.abs(yVelocity)) {
                return i3;
            }
        }
        float width = ((float) this.r.getWidth()) * this.m.b(viewHolder);
        if ((i2 & i4) == 0 || Math.abs(this.h) <= width) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public final void a() {
        if (Build.VERSION.SDK_INT < 21) {
            if (this.w == null) {
                this.w = new e();
            }
            this.r.setChildDrawingOrderCallback(this.w);
        }
    }
}
