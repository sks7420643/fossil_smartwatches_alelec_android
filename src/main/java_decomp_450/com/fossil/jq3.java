package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq3 extends b62 implements wp3 {
    @DexIgnore
    public jq3(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    @DexIgnore
    public final String a() {
        return c("asset_key");
    }

    @DexIgnore
    @Override // com.fossil.wp3
    public final String getId() {
        return c("asset_id");
    }
}
