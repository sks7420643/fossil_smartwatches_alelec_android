package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class rj extends ViewGroup implements oj {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Matrix e;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            View view;
            da.K(rj.this);
            rj rjVar = rj.this;
            ViewGroup viewGroup = rjVar.a;
            if (viewGroup == null || (view = rjVar.b) == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            da.K(rj.this.a);
            rj rjVar2 = rj.this;
            rjVar2.a = null;
            rjVar2.b = null;
            return true;
        }
    }

    @DexIgnore
    public rj(View view) {
        super(view.getContext());
        this.c = view;
        setWillNotDraw(false);
        setLayerType(2, null);
    }

    @DexIgnore
    public static void b(View view, ViewGroup viewGroup, Matrix matrix) {
        ViewGroup viewGroup2 = (ViewGroup) view.getParent();
        matrix.reset();
        sk.b(viewGroup2, matrix);
        matrix.preTranslate((float) (-viewGroup2.getScrollX()), (float) (-viewGroup2.getScrollY()));
        sk.c(viewGroup, matrix);
    }

    @DexIgnore
    public void a(Matrix matrix) {
        this.e = matrix;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(this.c, this);
        this.c.getViewTreeObserver().addOnPreDrawListener(this.f);
        sk.a(this.c, 4);
        if (this.c.getParent() != null) {
            ((View) this.c.getParent()).invalidate();
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.c.getViewTreeObserver().removeOnPreDrawListener(this.f);
        sk.a(this.c, 0);
        a(this.c, (rj) null);
        if (this.c.getParent() != null) {
            ((View) this.c.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        kj.a(canvas, true);
        canvas.setMatrix(this.e);
        sk.a(this.c, 0);
        this.c.invalidate();
        sk.a(this.c, 4);
        drawChild(canvas, this.c, getDrawingTime());
        kj.a(canvas, false);
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.fossil.oj
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (a(this.c) == this) {
            sk.a(this.c, i == 0 ? 4 : 0);
        }
    }

    @DexIgnore
    @Override // com.fossil.oj
    public void a(ViewGroup viewGroup, View view) {
        this.a = viewGroup;
        this.b = view;
    }

    @DexIgnore
    public static void a(View view, View view2) {
        sk.a(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }

    @DexIgnore
    public static void b(View view) {
        rj a2 = a(view);
        if (a2 != null) {
            int i = a2.d - 1;
            a2.d = i;
            if (i <= 0) {
                ((pj) a2.getParent()).removeView(a2);
            }
        }
    }

    @DexIgnore
    public static rj a(View view) {
        return (rj) view.getTag(yj.ghost_view);
    }

    @DexIgnore
    public static void a(View view, rj rjVar) {
        view.setTag(yj.ghost_view, rjVar);
    }

    @DexIgnore
    public static rj a(View view, ViewGroup viewGroup, Matrix matrix) {
        pj pjVar;
        if (view.getParent() instanceof ViewGroup) {
            pj a2 = pj.a(viewGroup);
            rj a3 = a(view);
            int i = 0;
            if (!(a3 == null || (pjVar = (pj) a3.getParent()) == a2)) {
                i = a3.d;
                pjVar.removeView(a3);
                a3 = null;
            }
            if (a3 == null) {
                if (matrix == null) {
                    matrix = new Matrix();
                    b(view, viewGroup, matrix);
                }
                a3 = new rj(view);
                a3.a(matrix);
                if (a2 == null) {
                    a2 = new pj(viewGroup);
                } else {
                    a2.a();
                }
                a((View) viewGroup, (View) a2);
                a((View) viewGroup, (View) a3);
                a2.a(a3);
                a3.d = i;
            } else if (matrix != null) {
                a3.a(matrix);
            }
            a3.d++;
            return a3;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }
}
