package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s80 extends n80 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c(null);
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ b f;
    @DexIgnore
    public /* final */ b g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ b i;
    @DexIgnore
    public /* final */ b j;
    @DexIgnore
    public /* final */ b k;
    @DexIgnore
    public /* final */ short l;
    @DexIgnore
    public /* final */ short m;

    @DexIgnore
    public enum a {
        DAILY((byte) 0),
        EXTENDED((byte) 1),
        TIME_ONLY((byte) 2),
        CUSTOM((byte) 3);
        
        @DexIgnore
        public static /* final */ C0164a c; // = new C0164a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s80$a$a")
        /* renamed from: com.fossil.s80$a$a  reason: collision with other inner class name */
        public static final class C0164a {
            @DexIgnore
            public /* synthetic */ C0164a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore
    public enum b {
        OFF((byte) 0),
        ON((byte) 1);
        
        @DexIgnore
        public static /* final */ a c; // = new a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public b(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<s80> {
        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
        }

        @DexIgnore
        public final s80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 14) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new s80(a.c.a(order.get(0)), b.c.a(order.get(1)), b.c.a(order.get(2)), b.c.a(order.get(3)), b.c.a(order.get(4)), b.c.a(order.get(5)), b.c.a(order.get(6)), b.c.a(order.get(7)), b.c.a(order.get(8)), b.c.a(order.get(9)), order.getShort(10), order.getShort(12));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 14"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public s80 createFromParcel(Parcel parcel) {
            return new s80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public s80[] newArray(int i) {
            return new s80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public s80 m58createFromParcel(Parcel parcel) {
            return new s80(parcel, null);
        }
    }

    @DexIgnore
    public s80(a aVar, b bVar, b bVar2, b bVar3, b bVar4, b bVar5, b bVar6, b bVar7, b bVar8, b bVar9, short s, short s2) {
        super(o80.HELLAS_BATTERY);
        this.b = aVar;
        this.c = bVar;
        this.d = bVar2;
        this.e = bVar3;
        this.f = bVar4;
        this.g = bVar5;
        this.h = bVar6;
        this.i = bVar7;
        this.j = bVar8;
        this.k = bVar9;
        this.l = s;
        this.m = s2;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(14).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).put(this.c.a()).put(this.d.a()).put(this.e.a()).put(this.f.a()).put(this.g.a()).put(this.h.a()).put(this.i.a()).put(this.j.a()).put(this.k.a()).putShort(this.l).putShort(this.m).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(s80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            s80 s80 = (s80) obj;
            return this.b == s80.b && this.c == s80.c && this.d == s80.d && this.e == s80.e && this.f == s80.f && this.g == s80.g && this.h == s80.h && this.i == s80.i && this.j == s80.j && this.k == s80.k && this.l == s80.l && this.m == s80.m;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HellasBatteryConfig");
    }

    @DexIgnore
    public final b getAlwaysOnScreenState() {
        return this.d;
    }

    @DexIgnore
    public final a getBatteryMode() {
        return this.b;
    }

    @DexIgnore
    public final short getBleFromMinuteOffset() {
        return this.l;
    }

    @DexIgnore
    public final short getBleToMinuteOffset() {
        return this.m;
    }

    @DexIgnore
    public final b getGoogleDetectState() {
        return this.k;
    }

    @DexIgnore
    public final b getLocationState() {
        return this.g;
    }

    @DexIgnore
    public final b getNfcState() {
        return this.c;
    }

    @DexIgnore
    public final b getSpeakerState() {
        return this.i;
    }

    @DexIgnore
    public final b getTiltToWakeState() {
        return this.f;
    }

    @DexIgnore
    public final b getTouchToWakeState() {
        return this.e;
    }

    @DexIgnore
    public final b getVibrationState() {
        return this.h;
    }

    @DexIgnore
    public final b getWifiState() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = Short.valueOf(this.l).hashCode();
        return Short.valueOf(this.m).hashCode() + ((hashCode10 + ((hashCode9 + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.b.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.h.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.i.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.j.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.k.a());
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.l));
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.m));
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.l5, yz0.a(this.b));
            yz0.a(jSONObject, r51.m5, yz0.a(this.c));
            yz0.a(jSONObject, r51.n5, yz0.a(this.d));
            yz0.a(jSONObject, r51.o5, yz0.a(this.e));
            yz0.a(jSONObject, r51.p5, yz0.a(this.f));
            yz0.a(jSONObject, r51.q5, yz0.a(this.g));
            yz0.a(jSONObject, r51.r5, yz0.a(this.h));
            yz0.a(jSONObject, r51.s5, yz0.a(this.i));
            yz0.a(jSONObject, r51.t5, yz0.a(this.j));
            yz0.a(jSONObject, r51.u5, yz0.a(this.k));
            yz0.a(jSONObject, r51.v5, Short.valueOf(this.l));
            yz0.a(jSONObject, r51.w5, Short.valueOf(this.m));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ s80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = a.c.a(parcel.readByte());
        this.c = b.c.a(parcel.readByte());
        this.d = b.c.a(parcel.readByte());
        this.e = b.c.a(parcel.readByte());
        this.f = b.c.a(parcel.readByte());
        this.g = b.c.a(parcel.readByte());
        this.h = b.c.a(parcel.readByte());
        this.i = b.c.a(parcel.readByte());
        this.j = b.c.a(parcel.readByte());
        this.k = b.c.a(parcel.readByte());
        this.l = (short) parcel.readInt();
        this.m = (short) parcel.readInt();
    }
}
