package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt6 implements MembersInjector<et6> {
    @DexIgnore
    public static void a(et6 et6, un5 un5) {
        et6.e = un5;
    }

    @DexIgnore
    public static void b(et6 et6, ch5 ch5) {
        et6.x = ch5;
    }

    @DexIgnore
    public static void a(et6 et6, lh5 lh5) {
        et6.f = lh5;
    }

    @DexIgnore
    public static void a(et6 et6, vn5 vn5) {
        et6.g = vn5;
    }

    @DexIgnore
    public static void a(et6 et6, yn5 yn5) {
        et6.h = yn5;
    }

    @DexIgnore
    public static void a(et6 et6, xn5 xn5) {
        et6.i = xn5;
    }

    @DexIgnore
    public static void a(et6 et6, wn5 wn5) {
        et6.j = wn5;
    }

    @DexIgnore
    public static void a(et6 et6, UserRepository userRepository) {
        et6.k = userRepository;
    }

    @DexIgnore
    public static void a(et6 et6, DeviceRepository deviceRepository) {
        et6.l = deviceRepository;
    }

    @DexIgnore
    public static void a(et6 et6, rl4 rl4) {
        et6.m = rl4;
    }

    @DexIgnore
    public static void a(et6 et6, kn5 kn5) {
        et6.n = kn5;
    }

    @DexIgnore
    public static void a(et6 et6, ln5 ln5) {
        et6.o = ln5;
    }

    @DexIgnore
    public static void a(et6 et6, in5 in5) {
        et6.p = in5;
    }

    @DexIgnore
    public static void a(et6 et6, jn5 jn5) {
        et6.q = jn5;
    }

    @DexIgnore
    public static void a(et6 et6, bn5 bn5) {
        et6.r = bn5;
    }

    @DexIgnore
    public static void a(et6 et6, an5 an5) {
        et6.s = an5;
    }

    @DexIgnore
    public static void a(et6 et6, AlarmsRepository alarmsRepository) {
        et6.t = alarmsRepository;
    }

    @DexIgnore
    public static void a(et6 et6, yl5 yl5) {
        et6.u = yl5;
    }

    @DexIgnore
    public static void a(et6 et6, ad5 ad5) {
        et6.v = ad5;
    }

    @DexIgnore
    public static void a(et6 et6, rn5 rn5) {
        et6.w = rn5;
    }

    @DexIgnore
    public static void a(et6 et6, xv6 xv6) {
        et6.y = xv6;
    }

    @DexIgnore
    public static void a(et6 et6, yv6 yv6) {
        et6.z = yv6;
    }

    @DexIgnore
    public static void a(et6 et6, qd5 qd5) {
        et6.A = qd5;
    }

    @DexIgnore
    public static void a(et6 et6, mn5 mn5) {
        et6.B = mn5;
    }

    @DexIgnore
    public static void a(et6 et6, SummariesRepository summariesRepository) {
        et6.C = summariesRepository;
    }

    @DexIgnore
    public static void a(et6 et6, SleepSummariesRepository sleepSummariesRepository) {
        et6.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(et6 et6, GoalTrackingRepository goalTrackingRepository) {
        et6.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(et6 et6, ym5 ym5) {
        et6.F = ym5;
    }

    @DexIgnore
    public static void a(et6 et6, zm5 zm5) {
        et6.G = zm5;
    }

    @DexIgnore
    public static void a(et6 et6, jw6 jw6) {
        et6.H = jw6;
    }

    @DexIgnore
    public static void a(et6 et6, fw6 fw6) {
        et6.I = fw6;
    }

    @DexIgnore
    public static void a(et6 et6, WatchLocalizationRepository watchLocalizationRepository) {
        et6.J = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(et6 et6, ch5 ch5) {
        et6.K = ch5;
    }

    @DexIgnore
    public static void a(et6 et6, to4 to4) {
        et6.L = to4;
    }

    @DexIgnore
    public static void a(et6 et6, lm4 lm4) {
        et6.M = lm4;
    }

    @DexIgnore
    public static void a(et6 et6, WorkoutSettingRepository workoutSettingRepository) {
        et6.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void a(et6 et6) {
        et6.H();
    }
}
