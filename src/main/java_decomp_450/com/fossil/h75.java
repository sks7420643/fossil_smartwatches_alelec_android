package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h75 extends g75 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362360, 1);
        A.put(2131362000, 2);
        A.put(2131362511, 3);
        A.put(2131363061, 4);
        A.put(2131362468, 5);
        A.put(2131362708, 6);
        A.put(2131362256, 7);
    }
    */

    @DexIgnore
    public h75(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public h75(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleCheckBox) objArr[2], (FlexibleButton) objArr[7], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (ImageView) objArr[6], (ShimmerFrameLayout) objArr[4]);
        this.y = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.x = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
