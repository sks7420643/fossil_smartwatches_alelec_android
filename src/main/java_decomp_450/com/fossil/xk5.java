package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk5 implements Factory<wk5> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<LocationSource> b;

    @DexIgnore
    public xk5(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static xk5 a(Provider<PortfolioApp> provider, Provider<LocationSource> provider2) {
        return new xk5(provider, provider2);
    }

    @DexIgnore
    public static wk5 a(PortfolioApp portfolioApp, LocationSource locationSource) {
        return new wk5(portfolioApp, locationSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public wk5 get() {
        return a(this.a.get(), this.b.get());
    }
}
