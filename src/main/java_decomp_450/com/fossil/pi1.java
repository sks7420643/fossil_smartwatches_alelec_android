package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pi1 extends xp0 {
    @DexIgnore
    public static /* final */ sg1 CREATOR; // = new sg1(null);
    @DexIgnore
    public /* final */ rg0 d;

    @DexIgnore
    public pi1(byte b, rg0 rg0) {
        super(ru0.MICRO_APP_EVENT, b, false, 4);
        this.d = rg0;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        return yz0.a(super.a(), r51.t3, this.d.a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(pi1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            pi1 pi1 = (pi1) obj;
            return ((xp0) this).a == ((xp0) pi1).a && ((xp0) this).b == ((xp0) pi1).b && !(ee7.a(this.d, pi1.d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        return this.d.hashCode() + (((((xp0) this).a.hashCode() * 31) + ((xp0) this).b) * 31);
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public /* synthetic */ pi1(Parcel parcel, zd7 zd7) {
        super(parcel);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
            this.d = new rg0(createByteArray);
            return;
        }
        ee7.a();
        throw null;
    }
}
