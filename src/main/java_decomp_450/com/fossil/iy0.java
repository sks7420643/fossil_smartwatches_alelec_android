package com.fossil;

import android.bluetooth.BluetoothProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy0 implements BluetoothProfile.ServiceListener {
    @DexIgnore
    public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
        t11 t11 = t11.a;
        if (i == 4) {
            zz0 zz0 = zz0.d;
            zz0.b = bluetoothProfile;
        }
    }

    @DexIgnore
    public void onServiceDisconnected(int i) {
        t11 t11 = t11.a;
        if (i == 4) {
            zz0 zz0 = zz0.d;
            zz0.b = null;
        }
    }
}
