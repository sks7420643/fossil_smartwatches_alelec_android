package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs4 implements Factory<us4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;

    @DexIgnore
    public vs4(Provider<ro4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static vs4 a(Provider<ro4> provider) {
        return new vs4(provider);
    }

    @DexIgnore
    public static us4 a(ro4 ro4) {
        return new us4(ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public us4 get() {
        return a(this.a.get());
    }
}
