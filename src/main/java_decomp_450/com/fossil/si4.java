package com.fossil;

import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class si4 {
    @DexIgnore
    public /* final */ ti4[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public si4(int i, int i2) {
        ti4[] ti4Arr = new ti4[i];
        this.a = ti4Arr;
        int length = ti4Arr.length;
        for (int i3 = 0; i3 < length; i3++) {
            this.a[i3] = new ti4(((i2 + 4) * 17) + 1);
        }
        this.d = i2 * 17;
        this.c = i;
        this.b = -1;
    }

    @DexIgnore
    public ti4 a() {
        return this.a[this.b];
    }

    @DexIgnore
    public void b() {
        this.b++;
    }

    @DexIgnore
    public byte[][] a(int i, int i2) {
        int[] iArr = new int[2];
        iArr[1] = this.d * i;
        iArr[0] = this.c * i2;
        byte[][] bArr = (byte[][]) Array.newInstance(byte.class, iArr);
        int i3 = this.c * i2;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - i4) - 1] = this.a[i4 / i2].a(i);
        }
        return bArr;
    }
}
