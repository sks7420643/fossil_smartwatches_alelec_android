package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class na2 extends t82 {
    @DexIgnore
    public int a;

    @DexIgnore
    public na2(byte[] bArr) {
        a72.a(bArr.length == 25);
        this.a = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] zza(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public abstract byte[] E();

    @DexIgnore
    public boolean equals(Object obj) {
        ab2 zzb;
        if (obj != null && (obj instanceof u82)) {
            try {
                u82 u82 = (u82) obj;
                if (u82.zzc() == hashCode() && (zzb = u82.zzb()) != null) {
                    return Arrays.equals(E(), (byte[]) cb2.g(zzb));
                }
                return false;
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.u82
    public final ab2 zzb() {
        return cb2.a(E());
    }

    @DexIgnore
    @Override // com.fossil.u82
    public final int zzc() {
        return hashCode();
    }
}
