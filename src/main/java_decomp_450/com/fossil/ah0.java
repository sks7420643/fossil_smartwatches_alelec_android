package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.types.BooleanCharType;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah0 extends bh0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ah0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ah0 createFromParcel(Parcel parcel) {
            return new ah0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ah0[] newArray(int i) {
            return new ah0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public ah0 m1createFromParcel(Parcel parcel) {
            return new ah0(parcel, null);
        }
    }

    @DexIgnore
    public /* synthetic */ ah0(Parcel parcel, zd7 zd7) {
        super(ch0.values()[parcel.readInt()]);
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            this.b = createStringArray;
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("commuteApp._.config.destinations", yz0.a(this.b));
        ee7.a((Object) put, "JSONObject()\n           \u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.bh0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.bh0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(ah0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((ah0) obj).b);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public final String[] getDestinations() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.bh0
    public int hashCode() {
        return (super.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.bh0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeStringArray(this.b);
        }
    }

    @DexIgnore
    public ah0(String[] strArr) throws IllegalArgumentException {
        super(ch0.COMMUTE);
        this.b = strArr;
        if (strArr.length > 10) {
            StringBuilder b2 = yh0.b("Destinations(");
            b2.append(this.b);
            b2.append(") must be less than or equal to ");
            b2.append(BooleanCharType.DEFAULT_TRUE_FALSE_FORMAT);
            throw new IllegalArgumentException(b2.toString());
        }
    }
}
