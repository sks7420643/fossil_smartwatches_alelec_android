package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Size;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.fx7;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.j256.ormlite.field.FieldType;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx7 implements fx7 {
    @DexIgnore
    public static /* final */ yw7 b; // = new yw7();
    @DexIgnore
    public static xw7 c; // = new xw7();
    @DexIgnore
    public static /* final */ String[] d; // = {"bucket_id", "bucket_display_name"};
    @DexIgnore
    public static /* final */ cx7 e; // = new cx7();

    @DexIgnore
    @Override // com.fossil.fx7
    public List<String> a(Context context, List<String> list) {
        ee7.b(context, "context");
        ee7.b(list, "ids");
        return fx7.b.a(this, context, list);
    }

    @DexIgnore
    public long b(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getLong");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.c(this, cursor, str);
    }

    @DexIgnore
    public String c(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getString");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.d(this, cursor, str);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void k() {
        b.a();
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public boolean a(Context context, String str) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        return fx7.b.a(this, context, str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00cf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d0, code lost:
        com.fossil.hc7.a(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d4, code lost:
        throw r0;
     */
    @DexIgnore
    @Override // com.fossil.fx7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.zw7 b(android.content.Context r19, java.lang.String r20) {
        /*
            r18 = this;
            r0 = r20
            java.lang.String r1 = "context"
            r2 = r19
            com.fossil.ee7.b(r2, r1)
            java.lang.String r1 = "id"
            com.fossil.ee7.b(r0, r1)
            com.fossil.yw7 r1 = com.fossil.cx7.b
            com.fossil.zw7 r1 = r1.a(r0)
            if (r1 == 0) goto L_0x0017
            return r1
        L_0x0017:
            com.fossil.fx7$a r1 = com.fossil.fx7.a
            java.lang.String[] r1 = r1.b()
            com.fossil.fx7$a r3 = com.fossil.fx7.a
            java.lang.String[] r3 = r3.d()
            java.lang.Object[] r1 = com.fossil.s97.b(r1, r3)
            com.fossil.fx7$a r3 = com.fossil.fx7.a
            java.lang.String[] r3 = r3.c()
            java.lang.Object[] r1 = com.fossil.s97.b(r1, r3)
            java.util.List r1 = com.fossil.t97.d(r1)
            r3 = 0
            java.lang.String[] r4 = new java.lang.String[r3]
            java.lang.Object[] r1 = r1.toArray(r4)
            if (r1 == 0) goto L_0x00d6
            r6 = r1
            java.lang.String[] r6 = (java.lang.String[]) r6
            r1 = 1
            java.lang.String[] r8 = new java.lang.String[r1]
            r8[r3] = r0
            android.content.ContentResolver r4 = r19.getContentResolver()
            android.net.Uri r5 = r18.a()
            r9 = 0
            java.lang.String r7 = "_id = ?"
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9)
            r0 = 0
            if (r2 == 0) goto L_0x00d5
            boolean r3 = r2.moveToNext()     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x00c5
            com.fossil.cx7 r3 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "_id"
            java.lang.String r6 = r3.c(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r3 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "_data"
            java.lang.String r7 = r3.c(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r3 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "datetaken"
            long r10 = r3.b(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r3 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "media_type"
            int r3 = r3.a(r2, r4)     // Catch:{ all -> 0x00cc }
            if (r3 != r1) goto L_0x0084
            r4 = 0
        L_0x0082:
            r8 = r4
            goto L_0x008d
        L_0x0084:
            com.fossil.cx7 r1 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "duration"
            long r4 = r1.b(r2, r4)     // Catch:{ all -> 0x00cc }
            goto L_0x0082
        L_0x008d:
            com.fossil.cx7 r1 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "width"
            int r12 = r1.a(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r1 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "height"
            int r13 = r1.a(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r1 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "_display_name"
            java.lang.String r15 = r1.c(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r1 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            java.lang.String r4 = "date_modified"
            long r16 = r1.b(r2, r4)     // Catch:{ all -> 0x00cc }
            com.fossil.zw7 r1 = new com.fossil.zw7     // Catch:{ all -> 0x00cc }
            com.fossil.cx7 r4 = com.fossil.cx7.e     // Catch:{ all -> 0x00cc }
            int r14 = r4.a(r3)     // Catch:{ all -> 0x00cc }
            r5 = r1
            r5.<init>(r6, r7, r8, r10, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x00cc }
            com.fossil.yw7 r3 = com.fossil.cx7.b     // Catch:{ all -> 0x00cc }
            r3.a(r1)     // Catch:{ all -> 0x00cc }
            r2.close()     // Catch:{ all -> 0x00cc }
            com.fossil.hc7.a(r2, r0)
            return r1
        L_0x00c5:
            r2.close()
            com.fossil.hc7.a(r2, r0)
            return r0
        L_0x00cc:
            r0 = move-exception
            r1 = r0
            throw r1     // Catch:{ all -> 0x00cf }
        L_0x00cf:
            r0 = move-exception
            r3 = r0
            com.fossil.hc7.a(r2, r1)
            throw r3
        L_0x00d5:
            return r0
        L_0x00d6:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx7.b(android.content.Context, java.lang.String):com.fossil.zw7");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public ub c(Context context, String str) {
        Uri uri;
        ee7.b(context, "context");
        ee7.b(str, "id");
        try {
            zw7 b2 = b(context, str);
            if (b2 != null) {
                if (b2.j() == 1) {
                    uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, b2.e());
                } else {
                    uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, b2.e());
                }
                Uri requireOriginal = MediaStore.setRequireOriginal(uri);
                ee7.a((Object) requireOriginal, "MediaStore.setRequireOriginal(uri)");
                InputStream openInputStream = context.getContentResolver().openInputStream(requireOriginal);
                if (openInputStream != null) {
                    ee7.a((Object) openInputStream, "context.contentResolver.\u2026iginalUri) ?: return null");
                    return new ub(openInputStream);
                }
            }
        } catch (Exception unused) {
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public Uri a() {
        return fx7.b.a(this);
    }

    @DexIgnore
    public String a(int i, ax7 ax7, ArrayList<String> arrayList) {
        ee7.b(ax7, "filterOptions");
        ee7.b(arrayList, "args");
        return fx7.b.a(this, i, ax7, arrayList);
    }

    @DexIgnore
    public int a(Cursor cursor, String str) {
        ee7.b(cursor, "$this$getInt");
        ee7.b(str, DatabaseFieldConfigLoader.FIELD_NAME_COLUMN_NAME);
        return fx7.b.b(this, cursor, str);
    }

    @DexIgnore
    public int a(int i) {
        return fx7.b.a(this, i);
    }

    @DexIgnore
    public String a(Integer num) {
        return fx7.b.a(this, num);
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public List<bx7> a(Context context, int i, long j, ax7 ax7) {
        ee7.b(context, "context");
        ee7.b(ax7, "option");
        ArrayList arrayList = new ArrayList();
        ArrayList<String> arrayList2 = new ArrayList<>();
        String a = a(i, ax7, arrayList2);
        arrayList2.add(String.valueOf(j));
        String str = "bucket_id IS NOT NULL " + a + ' ' + "AND date_added <= ?" + ' ' + a(Integer.valueOf(i));
        ContentResolver contentResolver = context.getContentResolver();
        Uri a2 = a();
        String[] strArr = d;
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(a2, strArr, str, (String[]) array, null);
            if (query != null) {
                ee7.a((Object) query, "context.contentResolver.\u2026           ?: return list");
                HashMap hashMap = new HashMap();
                HashMap hashMap2 = new HashMap();
                while (query.moveToNext()) {
                    String string = query.getString(0);
                    if (hashMap.containsKey(string)) {
                        ee7.a((Object) string, "galleryId");
                        Object obj = hashMap2.get(string);
                        if (obj != null) {
                            hashMap2.put(string, Integer.valueOf(((Number) obj).intValue() + 1));
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        String string2 = query.getString(1);
                        ee7.a((Object) string, "galleryId");
                        ee7.a((Object) string2, "galleryName");
                        hashMap.put(string, string2);
                        hashMap2.put(string, 1);
                    }
                }
                for (Map.Entry entry : hashMap.entrySet()) {
                    String str2 = (String) entry.getKey();
                    String str3 = (String) entry.getValue();
                    Object obj2 = hashMap2.get(str2);
                    if (obj2 != null) {
                        ee7.a(obj2, "countMap[id]!!");
                        arrayList.add(new bx7(str2, str3, ((Number) obj2).intValue(), i, false));
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                query.close();
            }
            return arrayList;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7, yw7 yw7) {
        String str2;
        long j2;
        ee7.b(context, "context");
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        yw7 yw72 = yw7 != null ? yw7 : b;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri a = a();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String a2 = a(i3, ax7, arrayList2);
        String a3 = a(Integer.valueOf(i3));
        arrayList2.add(String.valueOf(j));
        Object[] array = t97.d(s97.b(s97.b(fx7.a.b(), fx7.a.c()), fx7.a.d())).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + a2 + ' ' + "AND date_added <= ?" + ' ' + a3;
            } else {
                str2 = "bucket_id = ? " + a2 + ' ' + "AND date_added <= ?" + ' ' + a3;
            }
            String str3 = "datetaken DESC LIMIT " + i2 + " OFFSET " + (i2 * i);
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(a, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return w97.a();
                }
                ee7.a((Object) query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String c2 = c(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String c3 = c(query, "_data");
                    long b2 = b(query, "datetaken");
                    int a4 = a(query, MessengerShareContentUtility.MEDIA_TYPE);
                    if (i3 == 1) {
                        j2 = 0;
                    } else {
                        j2 = b(query, "duration");
                    }
                    zw7 zw7 = new zw7(c2, c3, j2, b2, a(query, "width"), a(query, "height"), a(a4), c(query, "_display_name"), b(query, "date_modified"));
                    arrayList.add(zw7);
                    yw72.a(zw7);
                }
                query.close();
                return arrayList;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public List<zw7> a(Context context, String str, int i, int i2, int i3, long j, ax7 ax7) {
        String str2;
        long j2;
        ee7.b(context, "context");
        ee7.b(str, "gId");
        ee7.b(ax7, "option");
        yw7 yw7 = b;
        boolean z = str.length() == 0;
        ArrayList arrayList = new ArrayList();
        Uri a = a();
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (!z) {
            arrayList2.add(str);
        }
        String a2 = a(i3, ax7, arrayList2);
        String a3 = a(Integer.valueOf(i3));
        arrayList2.add(String.valueOf(j));
        Object[] array = t97.d(s97.b(s97.b(fx7.a.b(), fx7.a.c()), fx7.a.d())).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            if (z) {
                str2 = "bucket_id IS NOT NULL " + a2 + ' ' + "AND date_added <= ?" + ' ' + a3;
            } else {
                str2 = "bucket_id = ? " + a2 + ' ' + "AND date_added <= ?" + ' ' + a3;
            }
            String str3 = "datetaken DESC LIMIT " + (i2 - i) + " OFFSET " + i;
            ContentResolver contentResolver = context.getContentResolver();
            Object[] array2 = arrayList2.toArray(new String[0]);
            if (array2 != null) {
                Cursor query = contentResolver.query(a, strArr, str2, (String[]) array2, str3);
                if (query == null) {
                    return w97.a();
                }
                ee7.a((Object) query, "context.contentResolver.\u2026    ?: return emptyList()");
                while (query.moveToNext()) {
                    String c2 = c(query, FieldType.FOREIGN_ID_FIELD_SUFFIX);
                    String c3 = c(query, "_data");
                    long b2 = b(query, "datetaken");
                    int a4 = a(query, MessengerShareContentUtility.MEDIA_TYPE);
                    if (i3 == 1) {
                        j2 = 0;
                    } else {
                        j2 = b(query, "duration");
                    }
                    zw7 zw7 = new zw7(c2, c3, j2, b2, a(query, "width"), a(query, "height"), a(a4), c(query, "_display_name"), b(query, "date_modified"));
                    arrayList.add(zw7);
                    yw7.a(zw7);
                }
                query.close();
                return arrayList;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    @SuppressLint({"Recycle"})
    public bx7 a(Context context, String str, int i, long j, ax7 ax7) {
        ee7.b(context, "context");
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        Uri a = a();
        String[] a2 = fx7.a.a();
        String str2 = "";
        boolean a3 = ee7.a((Object) str, (Object) str2);
        ArrayList<String> arrayList = new ArrayList<>();
        String a4 = a(i, ax7, arrayList);
        arrayList.add(String.valueOf(j));
        if (!a3) {
            arrayList.add(str);
            str2 = "AND bucket_id = ?";
        }
        String str3 = "bucket_id IS NOT NULL " + a4 + ' ' + "AND date_added <= ?" + ' ' + str2 + ' ' + a((Integer) null);
        ContentResolver contentResolver = context.getContentResolver();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            Cursor query = contentResolver.query(a, a2, str3, (String[]) array, null);
            if (query != null) {
                ee7.a((Object) query, "context.contentResolver.\u2026           ?: return null");
                if (query.moveToNext()) {
                    String string = query.getString(1);
                    ee7.a((Object) string, "cursor.getString(1)");
                    return new bx7(str, string, query.getCount(), i, a3);
                }
                query.close();
            }
            return null;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public String a(Context context, String str, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        zw7 b2 = b(context, str);
        if (b2 != null) {
            return c.a(context, str, b2.b(), b2.j(), z).getPath();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public Bitmap a(Context context, String str, int i, int i2, Integer num) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        if (num == null) {
            return null;
        }
        return context.getContentResolver().loadThumbnail(a(this, str, num.intValue(), false, 4, null), new Size(i, i2), null);
    }

    @DexIgnore
    public final Uri a(zw7 zw7, boolean z) {
        return a(zw7.e(), zw7.j(), z);
    }

    @DexIgnore
    public static /* synthetic */ Uri a(cx7 cx7, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        return cx7.a(str, i, z);
    }

    @DexIgnore
    public final Uri a(String str, int i, boolean z) {
        Uri uri;
        if (i == 1) {
            uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, str);
        } else {
            uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, str);
        }
        if (z) {
            uri = MediaStore.setRequireOriginal(uri);
        }
        ee7.a((Object) uri, "uri");
        return uri;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0073, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0074, code lost:
        com.fossil.hc7.a(r5, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0077, code lost:
        throw r7;
     */
    @DexIgnore
    @Override // com.fossil.fx7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a(android.content.Context r5, com.fossil.zw7 r6, boolean r7) {
        /*
            r4 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = "asset"
            com.fossil.ee7.b(r6, r0)
            com.fossil.xw7 r0 = com.fossil.cx7.c
            java.lang.String r1 = r6.e()
            java.lang.String r2 = r6.b()
            r3 = 1
            java.io.File r0 = r0.a(r5, r1, r2, r3)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x003c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "the origin bytes come from "
            r5.append(r6)
            java.lang.String r6 = r0.getAbsolutePath()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.fossil.mx7.a(r5)
            byte[] r5 = com.fossil.pc7.a(r0)
            return r5
        L_0x003c:
            android.net.Uri r7 = r4.a(r6, r7)
            android.content.ContentResolver r5 = r5.getContentResolver()
            java.io.InputStream r5 = r5.openInputStream(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "the cache file no exists, will read from MediaStore: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r7 = r0.toString()
            com.fossil.mx7.a(r7)
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream
            r7.<init>()
            if (r5 == 0) goto L_0x0078
            r0 = 0
            byte[] r1 = com.fossil.gc7.a(r5)     // Catch:{ all -> 0x0071 }
            r7.write(r1)     // Catch:{ all -> 0x0071 }
            com.fossil.i97 r1 = com.fossil.i97.a     // Catch:{ all -> 0x0071 }
            com.fossil.hc7.a(r5, r0)
            goto L_0x0078
        L_0x0071:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x0073 }
        L_0x0073:
            r7 = move-exception
            com.fossil.hc7.a(r5, r6)
            throw r7
        L_0x0078:
            byte[] r5 = r7.toByteArray()
            boolean r7 = com.fossil.mx7.a
            java.lang.String r0 = "byteArray"
            if (r7 == 0) goto L_0x00a6
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r1 = "The asset "
            r7.append(r1)
            java.lang.String r6 = r6.e()
            r7.append(r6)
            java.lang.String r6 = " origin byte length : "
            r7.append(r6)
            com.fossil.ee7.a(r5, r0)
            int r6 = r5.length
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            com.fossil.mx7.a(r6)
        L_0x00a6:
            com.fossil.ee7.a(r5, r0)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx7.a(android.content.Context, com.fossil.zw7, boolean):byte[]");
    }

    @DexIgnore
    @Override // com.fossil.fx7
    public void a(Context context, zw7 zw7, byte[] bArr) {
        ee7.b(context, "context");
        ee7.b(zw7, "asset");
        ee7.b(bArr, "byteArray");
        c.a(context, zw7, bArr, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0076, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0077, code lost:
        com.fossil.hc7.a(r1, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007a, code lost:
        throw r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007c, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007d, code lost:
        com.fossil.hc7.a(r0, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0080, code lost:
        throw r10;
     */
    @DexIgnore
    @Override // com.fossil.fx7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.zw7 a(android.content.Context r9, byte[] r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r9, r0)
            java.lang.String r0 = "image"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = "title"
            com.fossil.ee7.b(r11, r0)
            java.lang.String r1 = "desc"
            com.fossil.ee7.b(r12, r1)
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream
            r1.<init>(r10)
            android.content.ContentResolver r10 = r9.getContentResolver()
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, float:1.401E-42)
            long r4 = (long) r4
            long r2 = r2 / r4
            java.lang.String r4 = java.net.URLConnection.guessContentTypeFromStream(r1)
            android.net.Uri r5 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "_display_name"
            r6.put(r7, r11)
            java.lang.String r7 = "mime_type"
            r6.put(r7, r4)
            r6.put(r0, r11)
            java.lang.String r11 = "description"
            r6.put(r11, r12)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_added"
            r6.put(r12, r11)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_modified"
            r6.put(r12, r11)
            android.net.Uri r11 = r10.insert(r5, r6)
            r12 = 0
            if (r11 == 0) goto L_0x0091
            java.lang.String r0 = "cr.insert(uri, values) ?: return null"
            com.fossil.ee7.a(r11, r0)
            java.io.OutputStream r0 = r10.openOutputStream(r11)
            if (r0 == 0) goto L_0x0081
            r2 = 0
            r3 = 2
            com.fossil.gc7.a(r1, r0, r2, r3, r12)     // Catch:{ all -> 0x0074 }
            com.fossil.hc7.a(r1, r12)     // Catch:{ all -> 0x0072 }
            com.fossil.hc7.a(r0, r12)
            goto L_0x0081
        L_0x0072:
            r9 = move-exception
            goto L_0x007b
        L_0x0074:
            r9 = move-exception
            throw r9     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r10 = move-exception
            com.fossil.hc7.a(r1, r9)
            throw r10
        L_0x007b:
            throw r9     // Catch:{ all -> 0x007c }
        L_0x007c:
            r10 = move-exception
            com.fossil.hc7.a(r0, r9)
            throw r10
        L_0x0081:
            long r0 = android.content.ContentUris.parseId(r11)
            r10.notifyChange(r11, r12)
            java.lang.String r10 = java.lang.String.valueOf(r0)
            com.fossil.zw7 r9 = r8.b(r9, r10)
            return r9
        L_0x0091:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx7.a(android.content.Context, byte[], java.lang.String, java.lang.String):com.fossil.zw7");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0071, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0072, code lost:
        com.fossil.hc7.a(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0075, code lost:
        throw r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0077, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0078, code lost:
        com.fossil.hc7.a(r0, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007b, code lost:
        throw r10;
     */
    @DexIgnore
    @Override // com.fossil.fx7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.zw7 a(android.content.Context r9, java.io.InputStream r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r9, r0)
            java.lang.String r0 = "inputStream"
            com.fossil.ee7.b(r10, r0)
            java.lang.String r0 = "title"
            com.fossil.ee7.b(r11, r0)
            java.lang.String r1 = "desc"
            com.fossil.ee7.b(r12, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, float:1.401E-42)
            long r4 = (long) r4
            long r2 = r2 / r4
            java.lang.String r4 = java.net.URLConnection.guessContentTypeFromStream(r10)
            android.net.Uri r5 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "_display_name"
            r6.put(r7, r11)
            java.lang.String r7 = "mime_type"
            r6.put(r7, r4)
            r6.put(r0, r11)
            java.lang.String r11 = "description"
            r6.put(r11, r12)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_added"
            r6.put(r12, r11)
            java.lang.Long r11 = java.lang.Long.valueOf(r2)
            java.lang.String r12 = "date_modified"
            r6.put(r12, r11)
            android.net.Uri r11 = r1.insert(r5, r6)
            r12 = 0
            if (r11 == 0) goto L_0x008c
            java.lang.String r0 = "cr.insert(uri, values) ?: return null"
            com.fossil.ee7.a(r11, r0)
            java.io.OutputStream r0 = r1.openOutputStream(r11)
            if (r0 == 0) goto L_0x007c
            r2 = 0
            r3 = 2
            com.fossil.gc7.a(r10, r0, r2, r3, r12)     // Catch:{ all -> 0x006f }
            com.fossil.hc7.a(r10, r12)     // Catch:{ all -> 0x006d }
            com.fossil.hc7.a(r0, r12)
            goto L_0x007c
        L_0x006d:
            r9 = move-exception
            goto L_0x0076
        L_0x006f:
            r9 = move-exception
            throw r9     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r11 = move-exception
            com.fossil.hc7.a(r10, r9)
            throw r11
        L_0x0076:
            throw r9     // Catch:{ all -> 0x0077 }
        L_0x0077:
            r10 = move-exception
            com.fossil.hc7.a(r0, r9)
            throw r10
        L_0x007c:
            long r2 = android.content.ContentUris.parseId(r11)
            r1.notifyChange(r11, r12)
            java.lang.String r10 = java.lang.String.valueOf(r2)
            com.fossil.zw7 r9 = r8.b(r9, r10)
            return r9
        L_0x008c:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx7.a(android.content.Context, java.io.InputStream, java.lang.String, java.lang.String):com.fossil.zw7");
    }
}
