package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wb2 extends IInterface {
    @DexIgnore
    int A() throws RemoteException;

    @DexIgnore
    int a(ab2 ab2, String str, boolean z) throws RemoteException;

    @DexIgnore
    ab2 a(ab2 ab2, String str, int i) throws RemoteException;

    @DexIgnore
    int b(ab2 ab2, String str, boolean z) throws RemoteException;

    @DexIgnore
    ab2 b(ab2 ab2, String str, int i) throws RemoteException;
}
