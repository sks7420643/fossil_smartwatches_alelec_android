package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm4 extends RecyclerView.g<b> {
    @DexIgnore
    public ArrayList<Style> a; // = new ArrayList<>();
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public /* final */ ArrayList<Theme> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void F(String str);

        @DexIgnore
        void j(String str);

        @DexIgnore
        void u(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ AppCompatCheckBox a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ bm4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.u(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
                bm4 bm4 = this.a.d;
                bm4.b = ((Theme) bm4.c.get(this.a.getAdapterPosition())).getId();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bm4$b$b")
        /* renamed from: com.fossil.bm4$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0016b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public View$OnClickListenerC0016b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.j(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.F(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bm4 bm4, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = bm4;
            this.a = (AppCompatCheckBox) view.findViewById(2131361990);
            this.b = (ImageView) view.findViewById(2131362665);
            this.c = (ImageView) view.findViewById(2131362660);
            this.a.setOnClickListener(new a(this));
            this.b.setOnClickListener(new View$OnClickListenerC0016b(this));
            this.c.setOnClickListener(new c(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Theme theme) {
            ee7.b(theme, "theme");
            if (ee7.a((Object) theme.getType(), (Object) "fixed")) {
                ImageView imageView = this.b;
                ee7.a((Object) imageView, "ivEdit");
                imageView.setVisibility(4);
                ImageView imageView2 = this.c;
                ee7.a((Object) imageView2, "ivDelete");
                imageView2.setVisibility(4);
            }
            AppCompatCheckBox appCompatCheckBox = this.a;
            ee7.a((Object) appCompatCheckBox, "cbCurrentTheme");
            appCompatCheckBox.setChecked(ee7.a((Object) this.d.b, (Object) theme.getId()));
            AppCompatCheckBox appCompatCheckBox2 = this.a;
            ee7.a((Object) appCompatCheckBox2, "cbCurrentTheme");
            appCompatCheckBox2.setText(theme.getName());
            String a2 = eh5.l.a().a("primaryText", this.d.a);
            String a3 = eh5.l.a().a("nonBrandSurface", this.d.a);
            Typeface b2 = eh5.l.a().b("headline2", this.d.a);
            if (a2 != null) {
                this.a.setTextColor(Color.parseColor(a2));
                ua.a(this.a, new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(a2), Color.parseColor(a3), Color.parseColor(a3)}));
                this.c.setColorFilter(Color.parseColor(a2));
                this.b.setColorFilter(Color.parseColor(a2));
            }
            if (b2 != null) {
                AppCompatCheckBox appCompatCheckBox3 = this.a;
                ee7.a((Object) appCompatCheckBox3, "cbCurrentTheme");
                appCompatCheckBox3.setTypeface(b2);
            }
        }
    }

    @DexIgnore
    public bm4(ArrayList<Theme> arrayList, a aVar) {
        ee7.b(arrayList, "mData");
        this.c = arrayList;
        this.d = aVar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558715, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026tem_theme, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void a(ArrayList<Theme> arrayList) {
        ee7.b(arrayList, "ids");
        this.c.clear();
        this.c.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(List<Style> list, String str) {
        ee7.b(list, "allStyles");
        ee7.b(str, "selectedThemeId");
        this.a.clear();
        this.a.addAll(list);
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Theme theme = this.c.get(i);
            ee7.a((Object) theme, "mData[position]");
            bVar.a(theme);
        }
    }
}
