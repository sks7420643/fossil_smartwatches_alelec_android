package com.fossil;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;

public class lc {
    public final fc a;
    public final Fragment b;
    public int c = -1;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|(3:5|6|8)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /*
        static {
            /*
                androidx.lifecycle.Lifecycle$State[] r0 = androidx.lifecycle.Lifecycle.State.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.lc.a.a = r0
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.RESUMED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.lc.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.STARTED     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.lc.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                androidx.lifecycle.Lifecycle$State r1 = androidx.lifecycle.Lifecycle.State.CREATED     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.lc.a.<clinit>():void");
        }
        */
    }

    public lc(fc fcVar, Fragment fragment) {
        this.a = fcVar;
        this.b = fragment;
    }

    public void a(int i) {
        this.c = i;
    }

    public int b() {
        int i = this.c;
        Fragment fragment = this.b;
        if (fragment.mFromLayout) {
            if (fragment.mInLayout) {
                i = Math.max(i, 1);
            } else if (i < 2) {
                i = Math.min(i, fragment.mState);
            } else {
                i = Math.min(i, 1);
            }
        }
        if (!this.b.mAdded) {
            i = Math.min(i, 1);
        }
        Fragment fragment2 = this.b;
        if (fragment2.mRemoving) {
            if (fragment2.isInBackStack()) {
                i = Math.min(i, 1);
            } else {
                i = Math.min(i, -1);
            }
        }
        Fragment fragment3 = this.b;
        if (fragment3.mDeferStart && fragment3.mState < 3) {
            i = Math.min(i, 2);
        }
        int i2 = a.a[this.b.mMaxState.ordinal()];
        if (i2 == 1) {
            return i;
        }
        if (i2 == 2) {
            return Math.min(i, 3);
        }
        if (i2 != 3) {
            return Math.min(i, -1);
        }
        return Math.min(i, 1);
    }

    public void c() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        if (!fragment.mIsCreated) {
            this.a.c(fragment, fragment.mSavedFragmentState, false);
            Fragment fragment2 = this.b;
            fragment2.performCreate(fragment2.mSavedFragmentState);
            fc fcVar = this.a;
            Fragment fragment3 = this.b;
            fcVar.b(fragment3, fragment3.mSavedFragmentState, false);
            return;
        }
        fragment.restoreChildFragmentState(fragment.mSavedFragmentState);
        this.b.mState = 1;
    }

    public void d() {
        Fragment fragment = this.b;
        if (fragment.mFromLayout && fragment.mInLayout && !fragment.mPerformedCreateView) {
            if (FragmentManager.d(3)) {
                Log.d("FragmentManager", "moveto CREATE_VIEW: " + this.b);
            }
            Fragment fragment2 = this.b;
            fragment2.performCreateView(fragment2.performGetLayoutInflater(fragment2.mSavedFragmentState), null, this.b.mSavedFragmentState);
            View view = this.b.mView;
            if (view != null) {
                view.setSaveFromParentEnabled(false);
                Fragment fragment3 = this.b;
                fragment3.mView.setTag(wb.fragment_container_view_tag, fragment3);
                Fragment fragment4 = this.b;
                if (fragment4.mHidden) {
                    fragment4.mView.setVisibility(8);
                }
                Fragment fragment5 = this.b;
                fragment5.onViewCreated(fragment5.mView, fragment5.mSavedFragmentState);
                fc fcVar = this.a;
                Fragment fragment6 = this.b;
                fcVar.a(fragment6, fragment6.mView, fragment6.mSavedFragmentState, false);
            }
        }
    }

    public Fragment e() {
        return this.b;
    }

    public void f() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom RESUMED: " + this.b);
        }
        this.b.performPause();
        this.a.c(this.b, false);
    }

    public void g() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto RESTORE_VIEW_STATE: " + this.b);
        }
        Fragment fragment = this.b;
        if (fragment.mView != null) {
            fragment.restoreViewState(fragment.mSavedFragmentState);
        }
        this.b.mSavedFragmentState = null;
    }

    public void h() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto RESUMED: " + this.b);
        }
        this.b.performResume();
        this.a.d(this.b, false);
        Fragment fragment = this.b;
        fragment.mSavedFragmentState = null;
        fragment.mSavedViewState = null;
    }

    public final Bundle i() {
        Bundle bundle = new Bundle();
        this.b.performSaveInstanceState(bundle);
        this.a.d(this.b, bundle, false);
        if (bundle.isEmpty()) {
            bundle = null;
        }
        if (this.b.mView != null) {
            l();
        }
        if (this.b.mSavedViewState != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", this.b.mSavedViewState);
        }
        if (!this.b.mUserVisibleHint) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", this.b.mUserVisibleHint);
        }
        return bundle;
    }

    public Fragment.SavedState j() {
        Bundle i;
        if (this.b.mState <= -1 || (i = i()) == null) {
            return null;
        }
        return new Fragment.SavedState(i);
    }

    public kc k() {
        kc kcVar = new kc(this.b);
        if (this.b.mState <= -1 || kcVar.r != null) {
            kcVar.r = this.b.mSavedFragmentState;
        } else {
            Bundle i = i();
            kcVar.r = i;
            if (this.b.mTargetWho != null) {
                if (i == null) {
                    kcVar.r = new Bundle();
                }
                kcVar.r.putString("android:target_state", this.b.mTargetWho);
                int i2 = this.b.mTargetRequestCode;
                if (i2 != 0) {
                    kcVar.r.putInt("android:target_req_state", i2);
                }
            }
        }
        return kcVar;
    }

    public void l() {
        if (this.b.mView != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.b.mView.saveHierarchyState(sparseArray);
            if (sparseArray.size() > 0) {
                this.b.mSavedViewState = sparseArray;
            }
        }
    }

    public void m() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto STARTED: " + this.b);
        }
        this.b.performStart();
        this.a.e(this.b, false);
    }

    public void n() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom STARTED: " + this.b);
        }
        this.b.performStop();
        this.a.f(this.b, false);
    }

    public void a(ClassLoader classLoader) {
        Bundle bundle = this.b.mSavedFragmentState;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
            Fragment fragment = this.b;
            fragment.mSavedViewState = fragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
            Fragment fragment2 = this.b;
            fragment2.mTargetWho = fragment2.mSavedFragmentState.getString("android:target_state");
            Fragment fragment3 = this.b;
            if (fragment3.mTargetWho != null) {
                fragment3.mTargetRequestCode = fragment3.mSavedFragmentState.getInt("android:target_req_state", 0);
            }
            Fragment fragment4 = this.b;
            Boolean bool = fragment4.mSavedUserVisibleHint;
            if (bool != null) {
                fragment4.mUserVisibleHint = bool.booleanValue();
                this.b.mSavedUserVisibleHint = null;
            } else {
                fragment4.mUserVisibleHint = fragment4.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
            }
            Fragment fragment5 = this.b;
            if (!fragment5.mUserVisibleHint) {
                fragment5.mDeferStart = true;
            }
        }
    }

    public lc(fc fcVar, ClassLoader classLoader, dc dcVar, kc kcVar) {
        this.a = fcVar;
        this.b = dcVar.instantiate(classLoader, kcVar.a);
        Bundle bundle = kcVar.j;
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
        }
        this.b.setArguments(kcVar.j);
        Fragment fragment = this.b;
        fragment.mWho = kcVar.b;
        fragment.mFromLayout = kcVar.c;
        fragment.mRestored = true;
        fragment.mFragmentId = kcVar.d;
        fragment.mContainerId = kcVar.e;
        fragment.mTag = kcVar.f;
        fragment.mRetainInstance = kcVar.g;
        fragment.mRemoving = kcVar.h;
        fragment.mDetached = kcVar.i;
        fragment.mHidden = kcVar.p;
        fragment.mMaxState = Lifecycle.State.values()[kcVar.q];
        Bundle bundle2 = kcVar.r;
        if (bundle2 != null) {
            this.b.mSavedFragmentState = bundle2;
        } else {
            this.b.mSavedFragmentState = new Bundle();
        }
        if (FragmentManager.d(2)) {
            Log.v("FragmentManager", "Instantiated fragment " + this.b);
        }
    }

    public void a(FragmentHostCallback<?> fragmentHostCallback, FragmentManager fragmentManager, Fragment fragment) {
        Fragment fragment2 = this.b;
        fragment2.mHost = fragmentHostCallback;
        fragment2.mParentFragment = fragment;
        fragment2.mFragmentManager = fragmentManager;
        this.a.b(fragment2, fragmentHostCallback.c(), false);
        this.b.performAttach();
        Fragment fragment3 = this.b;
        Fragment fragment4 = fragment3.mParentFragment;
        if (fragment4 == null) {
            fragmentHostCallback.a(fragment3);
        } else {
            fragment4.onAttachFragment(fragment3);
        }
        this.a.a(this.b, fragmentHostCallback.c(), false);
    }

    public void a(cc ccVar) {
        String str;
        if (!this.b.mFromLayout) {
            if (FragmentManager.d(3)) {
                Log.d("FragmentManager", "moveto CREATE_VIEW: " + this.b);
            }
            ViewGroup viewGroup = null;
            Fragment fragment = this.b;
            ViewGroup viewGroup2 = fragment.mContainer;
            if (viewGroup2 != null) {
                viewGroup = viewGroup2;
            } else {
                int i = fragment.mContainerId;
                if (i != 0) {
                    if (i != -1) {
                        viewGroup = (ViewGroup) ccVar.a(i);
                        if (viewGroup == null) {
                            Fragment fragment2 = this.b;
                            if (!fragment2.mRestored) {
                                try {
                                    str = fragment2.getResources().getResourceName(this.b.mContainerId);
                                } catch (Resources.NotFoundException unused) {
                                    str = "unknown";
                                }
                                throw new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(this.b.mContainerId) + " (" + str + ") for fragment " + this.b);
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("Cannot create fragment " + this.b + " for a container view with no id");
                    }
                }
            }
            Fragment fragment3 = this.b;
            fragment3.mContainer = viewGroup;
            fragment3.performCreateView(fragment3.performGetLayoutInflater(fragment3.mSavedFragmentState), viewGroup, this.b.mSavedFragmentState);
            View view = this.b.mView;
            if (view != null) {
                boolean z = false;
                view.setSaveFromParentEnabled(false);
                Fragment fragment4 = this.b;
                fragment4.mView.setTag(wb.fragment_container_view_tag, fragment4);
                if (viewGroup != null) {
                    viewGroup.addView(this.b.mView);
                }
                Fragment fragment5 = this.b;
                if (fragment5.mHidden) {
                    fragment5.mView.setVisibility(8);
                }
                da.L(this.b.mView);
                Fragment fragment6 = this.b;
                fragment6.onViewCreated(fragment6.mView, fragment6.mSavedFragmentState);
                fc fcVar = this.a;
                Fragment fragment7 = this.b;
                fcVar.a(fragment7, fragment7.mView, fragment7.mSavedFragmentState, false);
                Fragment fragment8 = this.b;
                if (fragment8.mView.getVisibility() == 0 && this.b.mContainer != null) {
                    z = true;
                }
                fragment8.mIsNewlyAdded = z;
            }
        }
    }

    public lc(fc fcVar, Fragment fragment, kc kcVar) {
        this.a = fcVar;
        this.b = fragment;
        fragment.mSavedViewState = null;
        fragment.mBackStackNesting = 0;
        fragment.mInLayout = false;
        fragment.mAdded = false;
        Fragment fragment2 = fragment.mTarget;
        fragment.mTargetWho = fragment2 != null ? fragment2.mWho : null;
        Fragment fragment3 = this.b;
        fragment3.mTarget = null;
        Bundle bundle = kcVar.r;
        if (bundle != null) {
            fragment3.mSavedFragmentState = bundle;
        } else {
            fragment3.mSavedFragmentState = new Bundle();
        }
    }

    public void a() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "moveto ACTIVITY_CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        fragment.performActivityCreated(fragment.mSavedFragmentState);
        fc fcVar = this.a;
        Fragment fragment2 = this.b;
        fcVar.a(fragment2, fragment2.mSavedFragmentState, false);
    }

    public void a(FragmentHostCallback<?> fragmentHostCallback, ic icVar) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom CREATED: " + this.b);
        }
        Fragment fragment = this.b;
        boolean z = true;
        boolean z2 = fragment.mRemoving && !fragment.isInBackStack();
        if (z2 || icVar.f(this.b)) {
            if (fragmentHostCallback instanceof ke) {
                z = icVar.b();
            } else if (fragmentHostCallback.c() instanceof Activity) {
                z = true ^ ((Activity) fragmentHostCallback.c()).isChangingConfigurations();
            }
            if (z2 || z) {
                icVar.b(this.b);
            }
            this.b.performDestroy();
            this.a.a(this.b, false);
            return;
        }
        this.b.mState = 0;
    }

    public void a(ic icVar) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "movefrom ATTACHED: " + this.b);
        }
        this.b.performDetach();
        boolean z = false;
        this.a.b(this.b, false);
        Fragment fragment = this.b;
        fragment.mState = -1;
        fragment.mHost = null;
        fragment.mParentFragment = null;
        fragment.mFragmentManager = null;
        if (fragment.mRemoving && !fragment.isInBackStack()) {
            z = true;
        }
        if (z || icVar.f(this.b)) {
            if (FragmentManager.d(3)) {
                Log.d("FragmentManager", "initState called for fragment: " + this.b);
            }
            this.b.initState();
        }
    }
}
