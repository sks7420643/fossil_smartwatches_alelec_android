package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ mh1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ia1(mh1 mh1) {
        super(1);
        this.a = mh1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        mh1 mh1 = this.a;
        h41 h41 = ((z81) v81).L;
        mh1.C = h41;
        if (h41 == null) {
            mh1.a(eu0.a(((zk0) mh1).v, null, is0.FLOW_BROKEN, null, 5));
        } else if (e61.a.a(h41, mh1.E)) {
            mh1 mh12 = this.a;
            mh12.a(eu0.a(((zk0) mh12).v, null, is0.SUCCESS, null, 5));
        } else {
            mh1 mh13 = this.a;
            zk0.a(mh13, new xh1(mh13.E, ((zk0) mh13).w), new wd1(mh13), new qf1(mh13), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        }
        return i97.a;
    }
}
