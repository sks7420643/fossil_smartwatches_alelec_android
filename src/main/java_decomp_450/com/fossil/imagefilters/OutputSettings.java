package com.fossil.imagefilters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OutputSettings {
    @DexIgnore
    public static /* final */ OutputSettings BACKGROUND; // = new OutputSettings(240, 240, Format.RAW, false, false);
    @DexIgnore
    public static /* final */ OutputSettings ICON; // = new OutputSettings(24, 24, Format.RLE, false, false);
    @DexIgnore
    public /* final */ boolean mEqualizeHist;
    @DexIgnore
    public /* final */ Format mFormat;
    @DexIgnore
    public /* final */ int mHeight;
    @DexIgnore
    public /* final */ boolean mInvert;
    @DexIgnore
    public /* final */ int mWidth;

    @DexIgnore
    public OutputSettings(int i, int i2, Format format, boolean z, boolean z2) {
        this.mWidth = i;
        this.mHeight = i2;
        this.mFormat = format;
        this.mInvert = z;
        this.mEqualizeHist = z2;
    }

    @DexIgnore
    public boolean getEqualizeHist() {
        return this.mEqualizeHist;
    }

    @DexIgnore
    public Format getFormat() {
        return this.mFormat;
    }

    @DexIgnore
    public int getHeight() {
        return this.mHeight;
    }

    @DexIgnore
    public boolean getInvert() {
        return this.mInvert;
    }

    @DexIgnore
    public int getWidth() {
        return this.mWidth;
    }

    @DexIgnore
    public String toString() {
        return "OutputSettings{mWidth=" + this.mWidth + ",mHeight=" + this.mHeight + ",mFormat=" + this.mFormat + ",mInvert=" + this.mInvert + ",mEqualizeHist=" + this.mEqualizeHist + "}";
    }
}
