package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ sk5 b; // = new sk5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<WrapperTapEventSummary> b;
        @DexIgnore
        public /* final */ List<ActivitySample> c;
        @DexIgnore
        public /* final */ List<GFitSample> d;
        @DexIgnore
        public /* final */ List<ActivitySummary> e;
        @DexIgnore
        public /* final */ List<MFSleepSession> f;

        @DexIgnore
        public a(long j, List<WrapperTapEventSummary> list, List<ActivitySample> list2, List<GFitSample> list3, List<ActivitySummary> list4, List<MFSleepSession> list5) {
            ee7.b(list, "tapEventSummaryList");
            ee7.b(list2, "sampleRawList");
            ee7.b(list3, "gFitSampleList");
            ee7.b(list4, "summaryList");
            ee7.b(list5, "sleepSessionList");
            this.a = j;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
        }

        @DexIgnore
        public final List<GFitSample> a() {
            return this.d;
        }

        @DexIgnore
        public final long b() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> c() {
            return this.c;
        }

        @DexIgnore
        public final List<MFSleepSession> d() {
            return this.f;
        }

        @DexIgnore
        public final List<ActivitySummary> e() {
            return this.e;
        }

        @DexIgnore
        public final List<WrapperTapEventSummary> f() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {308}, m = "processGoalTrackingData")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sk5 sk5, fb7 fb7) {
            super(fb7);
            this.this$0 = sk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing", f = "HybridSyncDataProcessing.kt", l = {122, 133, 148, 156, 166, 176, 183}, m = "saveSyncResult")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$18;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sk5 sk5, fb7 fb7) {
            super(fb7);
            this.this$0 = sk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, null, null, null, null, null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2", f = "HybridSyncDataProcessing.kt", l = {137}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1", f = "HybridSyncDataProcessing.kt", l = {138, 139}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ik7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitSample;
            @DexIgnore
            public /* final */ /* synthetic */ List $listSleepSession;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, List list2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$listGFitSample = list;
                this.$listSleepSession = list2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$listGFitSample, this.$listSleepSession, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    ThirdPartyRepository thirdPartyRepository = this.this$0.$thirdPartyRepository;
                    List<GFitSample> list = this.$listGFitSample;
                    List<GFitHeartRate> a2 = w97.a();
                    List<GFitWorkoutSession> a3 = w97.a();
                    List<MFSleepSession> list2 = this.$listSleepSession;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (thirdPartyRepository.saveData(list, null, a2, a3, list2, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ThirdPartyRepository thirdPartyRepository2 = this.this$0.$thirdPartyRepository;
                this.L$0 = yi7;
                this.label = 2;
                obj = ThirdPartyRepository.uploadData$default(thirdPartyRepository2, null, this, 1, null);
                return obj == a ? a : obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a aVar, ThirdPartyRepository thirdPartyRepository, fb7 fb7) {
            super(2, fb7);
            this.$finalResult = aVar;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.$finalResult, this.$thirdPartyRepository, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ik7> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                List<GFitSample> a3 = this.$finalResult.a();
                List<MFSleepSession> d = this.$finalResult.d();
                ti7 b = qj7.b();
                a aVar = new a(this, a3, d, null);
                this.L$0 = yi7;
                this.L$1 = a3;
                this.L$2 = d;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                List list = (List) this.L$2;
                List list2 = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3", f = "HybridSyncDataProcessing.kt", l = {192}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ se7 $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1", f = "HybridSyncDataProcessing.kt", l = {193, 194, 196, 197, 198}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00c3 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0126 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x0135 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r19) {
                /*
                    r18 = this;
                    r8 = r18
                    java.lang.Object r9 = com.fossil.nb7.a()
                    int r0 = r8.label
                    r10 = 5
                    r11 = 4
                    r12 = 3
                    r13 = 2
                    r1 = 1
                    java.lang.String r14 = "endDate.toDate()"
                    java.lang.String r15 = "startDate.toDate()"
                    if (r0 == 0) goto L_0x0054
                    if (r0 == r1) goto L_0x004c
                    if (r0 == r13) goto L_0x0042
                    if (r0 == r12) goto L_0x0039
                    if (r0 == r11) goto L_0x0030
                    if (r0 != r10) goto L_0x0028
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r19)
                    r0 = r19
                    goto L_0x0136
                L_0x0028:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0030:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r19)
                    goto L_0x0127
                L_0x0039:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r19)
                    goto L_0x00fc
                L_0x0042:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r19)
                L_0x0049:
                    r13 = r0
                    goto L_0x00c4
                L_0x004c:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r19)
                    goto L_0x0099
                L_0x0054:
                    com.fossil.t87.a(r19)
                    com.fossil.yi7 r7 = r8.p$
                    com.fossil.sk5$e r0 = r8.this$0
                    com.portfolio.platform.data.source.ActivitiesRepository r2 = r0.$activityRepository
                    com.fossil.se7 r0 = r0.$startDate
                    T r0 = r0.element
                    org.joda.time.DateTime r0 = (org.joda.time.DateTime) r0
                    java.util.Date r3 = r0.toDate()
                    com.fossil.ee7.a(r3, r15)
                    com.fossil.sk5$e r0 = r8.this$0
                    com.fossil.se7 r0 = r0.$endDate
                    T r0 = r0.element
                    org.joda.time.DateTime r0 = (org.joda.time.DateTime) r0
                    java.util.Date r4 = r0.toDate()
                    com.fossil.ee7.a(r4, r14)
                    r5 = 0
                    r6 = 0
                    r16 = 12
                    r17 = 0
                    r8.L$0 = r7
                    r8.label = r1
                    r0 = r2
                    r1 = r3
                    r2 = r4
                    r3 = r5
                    r4 = r6
                    r5 = r18
                    r6 = r16
                    r16 = r7
                    r7 = r17
                    java.lang.Object r0 = com.portfolio.platform.data.source.ActivitiesRepository.fetchActivitySamples$default(r0, r1, r2, r3, r4, r5, r6, r7)
                    if (r0 != r9) goto L_0x0097
                    return r9
                L_0x0097:
                    r0 = r16
                L_0x0099:
                    com.fossil.sk5$e r1 = r8.this$0
                    com.portfolio.platform.data.source.SummariesRepository r2 = r1.$summaryRepository
                    com.fossil.se7 r1 = r1.$startDate
                    T r1 = r1.element
                    org.joda.time.DateTime r1 = (org.joda.time.DateTime) r1
                    java.util.Date r1 = r1.toDate()
                    com.fossil.ee7.a(r1, r15)
                    com.fossil.sk5$e r3 = r8.this$0
                    com.fossil.se7 r3 = r3.$endDate
                    T r3 = r3.element
                    org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                    java.util.Date r3 = r3.toDate()
                    com.fossil.ee7.a(r3, r14)
                    r8.L$0 = r0
                    r8.label = r13
                    java.lang.Object r1 = r2.loadSummaries(r1, r3, r8)
                    if (r1 != r9) goto L_0x0049
                    return r9
                L_0x00c4:
                    com.fossil.sk5$e r0 = r8.this$0
                    com.portfolio.platform.data.source.SleepSessionsRepository r1 = r0.$sleepSessionsRepository
                    com.fossil.se7 r0 = r0.$startDate
                    T r0 = r0.element
                    org.joda.time.DateTime r0 = (org.joda.time.DateTime) r0
                    java.util.Date r2 = r0.toDate()
                    com.fossil.ee7.a(r2, r15)
                    com.fossil.sk5$e r0 = r8.this$0
                    com.fossil.se7 r0 = r0.$endDate
                    T r0 = r0.element
                    org.joda.time.DateTime r0 = (org.joda.time.DateTime) r0
                    java.util.Date r3 = r0.toDate()
                    com.fossil.ee7.a(r3, r14)
                    r4 = 0
                    r5 = 0
                    r6 = 12
                    r7 = 0
                    r8.L$0 = r13
                    r8.label = r12
                    r0 = r1
                    r1 = r2
                    r2 = r3
                    r3 = r4
                    r4 = r5
                    r5 = r18
                    java.lang.Object r0 = com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r0, r1, r2, r3, r4, r5, r6, r7)
                    if (r0 != r9) goto L_0x00fb
                    return r9
                L_0x00fb:
                    r0 = r13
                L_0x00fc:
                    com.fossil.sk5$e r1 = r8.this$0
                    com.portfolio.platform.data.source.SleepSummariesRepository r2 = r1.$sleepSummariesRepository
                    com.fossil.se7 r1 = r1.$startDate
                    T r1 = r1.element
                    org.joda.time.DateTime r1 = (org.joda.time.DateTime) r1
                    java.util.Date r1 = r1.toDate()
                    com.fossil.ee7.a(r1, r15)
                    com.fossil.sk5$e r3 = r8.this$0
                    com.fossil.se7 r3 = r3.$endDate
                    T r3 = r3.element
                    org.joda.time.DateTime r3 = (org.joda.time.DateTime) r3
                    java.util.Date r3 = r3.toDate()
                    com.fossil.ee7.a(r3, r14)
                    r8.L$0 = r0
                    r8.label = r11
                    java.lang.Object r1 = r2.fetchSleepSummaries(r1, r3, r8)
                    if (r1 != r9) goto L_0x0127
                    return r9
                L_0x0127:
                    com.fossil.sk5$e r1 = r8.this$0
                    com.portfolio.platform.data.source.SummariesRepository r1 = r1.$summaryRepository
                    r8.L$0 = r0
                    r8.label = r10
                    java.lang.Object r0 = r1.fetchActivityStatistic(r8)
                    if (r0 != r9) goto L_0x0136
                    return r9
                L_0x0136:
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.sk5.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(List list, se7 se7, se7 se72, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, fb7 fb7) {
            super(2, fb7);
            this.$fitnessDataList = list;
            this.$startDate = se7;
            this.$endDate = se72;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.$fitnessDataList, this.$startDate, this.$endDate, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < this.$startDate.element.getMillis()) {
                        this.$startDate.element = (T) fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > this.$endDate.element.getMillis()) {
                        this.$endDate.element = (T) fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        String simpleName = sk5.class.getSimpleName();
        ee7.a((Object) simpleName, "HybridSyncDataProcessing::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        ee7.b(str, "serial");
        ee7.b(list, "syncData");
        ee7.b(mFUser, "user");
        ee7.b(userProfile, "userProfile");
        ee7.b(portfolioApp, "portfolioApp");
        FLogger.INSTANCE.getLocal().d(a, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            portfolioApp.a(CommunicateMode.SYNC, str, "Calculating sleep and activity...");
            v87<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = tx6.a(list, str, mFUser.getUserId(), j * 1000);
            List<ActivitySample> first = a2.getFirst();
            List<ActivitySummary> second = a2.getSecond();
            List<GFitSample> third = a2.getThird();
            List<MFSleepSession> a3 = tx6.a(list, str);
            List<WrapperTapEventSummary> b2 = tx6.b(list);
            int size = a3.size();
            double d2 = 0.0d;
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            for (T t : first) {
                d4 += t.getCalories();
                d5 += t.getDistance();
                d2 += t.getSteps();
                t.getActiveTime();
                Boolean w = zd5.w(t.getDate());
                ee7.a((Object) w, "DateHelper.isToday(it.date)");
                if (w.booleanValue()) {
                    d3 += t.getSteps();
                    t.getActiveTime();
                }
            }
            we7 we7 = we7.a;
            String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(new Object[]{String.valueOf(size)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            a(str, format);
            we7 we72 = we7.a;
            String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s. Calories=%s. DistanceWrapper=%s. Taps=%s", Arrays.copyOf(new Object[]{Double.valueOf(d2), Double.valueOf(d3), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d5), c(b2)}, 7));
            ee7.a((Object) format2, "java.lang.String.format(format, *args)");
            a(str, format2);
            FLogger.INSTANCE.getLocal().d(a, "Release=" + he5.a());
            if (!he5.a()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a;
                StringBuilder sb = new StringBuilder();
                sb.append("onSyncCompleted - Sleep session details: ");
                sb.append(a3.isEmpty() ? b(a3) : ", no sleep data");
                local.d(str2, sb.toString());
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Minute data details: " + a(first));
                FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Tap event details: " + c(b2));
            }
            return new a(j2, b2, first, third, second, a3);
        }
        a(str, "Sync data is empty");
        return aVar;
    }

    @DexIgnore
    public final String b(List<MFSleepSession> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (MFSleepSession mFSleepSession : list) {
            sb.append(mFSleepSession.toString());
            sb.append("\n");
            sb.append("State: ");
            String sleepStates = mFSleepSession.getSleepStates();
            int length = sleepStates.length();
            for (int i = 0; i < length; i++) {
                sb.append(String.valueOf(sleepStates.charAt(i)));
                sb.append(" -- ");
            }
            sb.append("\n");
        }
        sb.append("\n");
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final String c(List<? extends WrapperTapEventSummary> list) {
        if (list == null || list.isEmpty()) {
            return "null\n";
        }
        StringBuilder sb = new StringBuilder();
        for (WrapperTapEventSummary wrapperTapEventSummary : list) {
            sb.append("[startTime:");
            sb.append(wrapperTapEventSummary.startTime);
            sb.append(", timezoneOffsetInSecond=");
            sb.append(wrapperTapEventSummary.timezoneOffsetInSecond);
            sb.append(", goalTrackingIds=");
            sb.append(wrapperTapEventSummary.goalId);
            sb.append("]");
            sb.append("\n");
        }
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x04b1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x04b2  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x056a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x056b  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x05e2 A[Catch:{ Exception -> 0x0659 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x0640  */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x06c1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x06c2  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x06db  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x077a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x01b4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x026a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x02e9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0445 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0446  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.sk5.a r24, java.lang.String r25, com.portfolio.platform.data.source.SleepSessionsRepository r26, com.portfolio.platform.data.source.SummariesRepository r27, com.portfolio.platform.data.source.SleepSummariesRepository r28, com.portfolio.platform.data.source.FitnessDataRepository r29, com.portfolio.platform.data.source.ActivitiesRepository r30, com.portfolio.platform.data.source.HybridPresetRepository r31, com.portfolio.platform.data.source.GoalTrackingRepository r32, com.portfolio.platform.data.source.ThirdPartyRepository r33, com.portfolio.platform.data.source.UserRepository r34, com.portfolio.platform.PortfolioApp r35, com.fossil.ge5 r36, com.fossil.fb7<? super com.fossil.i97> r37) {
        /*
            r23 = this;
            r1 = r23
            r2 = r25
            r3 = r26
            r0 = r37
            boolean r4 = r0 instanceof com.fossil.sk5.c
            if (r4 == 0) goto L_0x001b
            r4 = r0
            com.fossil.sk5$c r4 = (com.fossil.sk5.c) r4
            int r5 = r4.label
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = r5 & r6
            if (r7 == 0) goto L_0x001b
            int r5 = r5 - r6
            r4.label = r5
            goto L_0x0020
        L_0x001b:
            com.fossil.sk5$c r4 = new com.fossil.sk5$c
            r4.<init>(r1, r0)
        L_0x0020:
            java.lang.Object r0 = r4.result
            java.lang.Object r5 = com.fossil.nb7.a()
            int r6 = r4.label
            switch(r6) {
                case 0: goto L_0x02e9;
                case 1: goto L_0x026a;
                case 2: goto L_0x0214;
                case 3: goto L_0x01b4;
                case 4: goto L_0x0146;
                case 5: goto L_0x00e0;
                case 6: goto L_0x0084;
                case 7: goto L_0x0033;
                default: goto L_0x002b;
            }
        L_0x002b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0033:
            java.lang.Object r2 = r4.L$18
            com.fossil.se7 r2 = (com.fossil.se7) r2
            java.lang.Object r2 = r4.L$17
            com.fossil.se7 r2 = (com.fossil.se7) r2
            java.lang.Object r2 = r4.L$16
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r4.L$15
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = r4.L$13
            com.fossil.ge5 r2 = (com.fossil.ge5) r2
            java.lang.Object r2 = r4.L$12
            com.portfolio.platform.PortfolioApp r2 = (com.portfolio.platform.PortfolioApp) r2
            java.lang.Object r3 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r3 = (com.portfolio.platform.data.source.UserRepository) r3
            java.lang.Object r3 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r3 = (com.portfolio.platform.data.source.ThirdPartyRepository) r3
            java.lang.Object r3 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r3 = (com.portfolio.platform.data.source.GoalTrackingRepository) r3
            java.lang.Object r3 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r3 = (com.portfolio.platform.data.source.HybridPresetRepository) r3
            java.lang.Object r3 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r3 = (com.portfolio.platform.data.source.ActivitiesRepository) r3
            java.lang.Object r3 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r3 = (com.portfolio.platform.data.source.FitnessDataRepository) r3
            java.lang.Object r3 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r3 = (com.portfolio.platform.data.source.SleepSummariesRepository) r3
            java.lang.Object r3 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r3 = (com.portfolio.platform.data.source.SummariesRepository) r3
            java.lang.Object r3 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r3 = (com.portfolio.platform.data.source.SleepSessionsRepository) r3
            java.lang.Object r3 = r4.L$2
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r5 = r4.L$1
            com.fossil.sk5$a r5 = (com.fossil.sk5.a) r5
            java.lang.Object r4 = r4.L$0
            com.fossil.sk5 r4 = (com.fossil.sk5) r4
            com.fossil.t87.a(r0)
            goto L_0x0778
        L_0x0084:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r8 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r8 = (com.portfolio.platform.data.source.UserRepository) r8
            java.lang.Object r9 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r9 = (com.portfolio.platform.data.source.ThirdPartyRepository) r9
            java.lang.Object r10 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r10 = (com.portfolio.platform.data.source.GoalTrackingRepository) r10
            java.lang.Object r11 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r11 = (com.portfolio.platform.data.source.HybridPresetRepository) r11
            java.lang.Object r12 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r12 = (com.portfolio.platform.data.source.ActivitiesRepository) r12
            java.lang.Object r13 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r13 = (com.portfolio.platform.data.source.FitnessDataRepository) r13
            java.lang.Object r14 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r14 = (com.portfolio.platform.data.source.SleepSummariesRepository) r14
            java.lang.Object r15 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r15 = (com.portfolio.platform.data.source.SummariesRepository) r15
            java.lang.Object r7 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r7 = (com.portfolio.platform.data.source.SleepSessionsRepository) r7
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)
            r1 = r5
            r33 = r14
            r34 = r15
            r5 = r26
            r14 = r12
            r15 = r13
            r12 = r10
            r13 = r11
            r10 = r8
            r11 = r9
            r8 = r3
            r9 = r7
            r7 = r25
            r3 = r2
            r2 = r6
            r6 = r24
            goto L_0x06d3
        L_0x00e0:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r7 = (com.portfolio.platform.data.source.UserRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r9 = (com.portfolio.platform.data.source.GoalTrackingRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r10 = (com.portfolio.platform.data.source.HybridPresetRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x0135 }
            r17 = r24
            r1 = r5
            r16 = r15
            r5 = r25
            r15 = r14
            r14 = r12
            r12 = r10
            r10 = r8
            r8 = r3
            r3 = r26
            goto L_0x062f
        L_0x0135:
            r0 = move-exception
            r17 = r24
            r24 = r26
            r1 = r5
            r16 = r15
            r5 = r25
            r15 = r14
            r14 = r12
            r12 = r10
            r10 = r8
            r8 = r3
            goto L_0x0669
        L_0x0146:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r7 = (com.portfolio.platform.data.source.UserRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r9 = (com.portfolio.platform.data.source.GoalTrackingRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r10 = (com.portfolio.platform.data.source.HybridPresetRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x019f }
            r17 = r24
            r1 = r26
            r16 = r15
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r3
            r3 = r2
            r2 = r5
            r5 = r25
            goto L_0x0572
        L_0x019f:
            r0 = move-exception
            r17 = r24
            r24 = r26
            r16 = r15
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r3
            r3 = r2
            r2 = r5
            r5 = r25
            goto L_0x05a5
        L_0x01b4:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r7 = (com.portfolio.platform.data.source.UserRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r9 = (com.portfolio.platform.data.source.GoalTrackingRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r10 = (com.portfolio.platform.data.source.HybridPresetRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x0206 }
            r17 = r24
            r1 = r5
            r16 = r15
            r5 = r25
            r15 = r7
            r7 = r3
            r3 = r26
            goto L_0x04bd
        L_0x0206:
            r0 = move-exception
            r17 = r24
            r24 = r26
            r1 = r5
            r16 = r15
            r5 = r25
            r15 = r7
            r7 = r3
            goto L_0x04f8
        L_0x0214:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r7 = (com.portfolio.platform.data.source.UserRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r9 = (com.portfolio.platform.data.source.GoalTrackingRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r10 = (com.portfolio.platform.data.source.HybridPresetRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)
            r17 = r24
            r16 = r5
            r1 = r14
            r5 = r25
            r14 = r8
            r8 = r6
            r6 = r15
            r15 = r7
            r7 = r3
            r3 = r2
            r2 = r26
            goto L_0x045d
        L_0x026a:
            java.lang.Object r2 = r4.L$14
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r4.L$13
            com.fossil.ge5 r3 = (com.fossil.ge5) r3
            java.lang.Object r6 = r4.L$12
            com.portfolio.platform.PortfolioApp r6 = (com.portfolio.platform.PortfolioApp) r6
            java.lang.Object r7 = r4.L$11
            com.portfolio.platform.data.source.UserRepository r7 = (com.portfolio.platform.data.source.UserRepository) r7
            java.lang.Object r8 = r4.L$10
            com.portfolio.platform.data.source.ThirdPartyRepository r8 = (com.portfolio.platform.data.source.ThirdPartyRepository) r8
            java.lang.Object r9 = r4.L$9
            com.portfolio.platform.data.source.GoalTrackingRepository r9 = (com.portfolio.platform.data.source.GoalTrackingRepository) r9
            java.lang.Object r10 = r4.L$8
            com.portfolio.platform.data.source.HybridPresetRepository r10 = (com.portfolio.platform.data.source.HybridPresetRepository) r10
            java.lang.Object r11 = r4.L$7
            com.portfolio.platform.data.source.ActivitiesRepository r11 = (com.portfolio.platform.data.source.ActivitiesRepository) r11
            java.lang.Object r12 = r4.L$6
            com.portfolio.platform.data.source.FitnessDataRepository r12 = (com.portfolio.platform.data.source.FitnessDataRepository) r12
            java.lang.Object r13 = r4.L$5
            com.portfolio.platform.data.source.SleepSummariesRepository r13 = (com.portfolio.platform.data.source.SleepSummariesRepository) r13
            java.lang.Object r14 = r4.L$4
            com.portfolio.platform.data.source.SummariesRepository r14 = (com.portfolio.platform.data.source.SummariesRepository) r14
            java.lang.Object r15 = r4.L$3
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = (com.portfolio.platform.data.source.SleepSessionsRepository) r15
            r24 = r2
            java.lang.Object r2 = r4.L$2
            java.lang.String r2 = (java.lang.String) r2
            r25 = r2
            java.lang.Object r2 = r4.L$1
            com.fossil.sk5$a r2 = (com.fossil.sk5.a) r2
            r26 = r2
            java.lang.Object r2 = r4.L$0
            com.fossil.sk5 r2 = (com.fossil.sk5) r2
            com.fossil.t87.a(r0)     // Catch:{ Exception -> 0x02ca }
            r16 = r24
            r1 = r2
            r0 = r3
            r3 = r15
            r2 = r25
            r15 = r7
            r7 = r26
            r20 = r14
            r14 = r8
            r8 = r20
            r21 = r13
            r13 = r9
            r9 = r21
            r22 = r12
            r12 = r10
            r10 = r22
            goto L_0x03ef
        L_0x02ca:
            r0 = move-exception
            r16 = r24
            r17 = r0
            r1 = r2
            r24 = r3
            r3 = r15
            r2 = r25
            r15 = r7
            r7 = r26
            r20 = r14
            r14 = r8
            r8 = r20
            r21 = r13
            r13 = r9
            r9 = r21
            r22 = r12
            r12 = r10
            r10 = r22
            goto L_0x03d1
        L_0x02e9:
            com.fossil.t87.a(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = "Save sync result - size of sleepSessions="
            r0.append(r6)
            java.util.List r6 = r24.d()
            int r6 = r6.size()
            r0.append(r6)
            java.lang.String r6 = ", size of sampleRaws="
            r0.append(r6)
            java.util.List r6 = r24.c()
            int r6 = r6.size()
            r0.append(r6)
            java.lang.String r6 = ", realTimeSteps="
            r0.append(r6)
            long r6 = r24.b()
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r7 = com.fossil.sk5.a
            r0.i(r7, r6)
            r1.a(r2, r6)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.c()
            boolean r0 = com.fossil.mh7.a(r0)
            if (r0 != 0) goto L_0x0791
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r0 = r0.c()
            r7 = 1
            boolean r0 = com.fossil.mh7.b(r0, r2, r7)
            if (r0 != 0) goto L_0x0352
            goto L_0x0791
        L_0x0352:
            java.lang.String r0 = "Saving sleep data"
            r1.a(r2, r0)
            java.util.List r0 = r24.d()     // Catch:{ Exception -> 0x03b4 }
            r4.L$0 = r1     // Catch:{ Exception -> 0x03b4 }
            r7 = r24
            r4.L$1 = r7     // Catch:{ Exception -> 0x03b2 }
            r4.L$2 = r2     // Catch:{ Exception -> 0x03b2 }
            r4.L$3 = r3     // Catch:{ Exception -> 0x03b2 }
            r8 = r27
            r4.L$4 = r8     // Catch:{ Exception -> 0x03b0 }
            r9 = r28
            r4.L$5 = r9     // Catch:{ Exception -> 0x03ae }
            r10 = r29
            r4.L$6 = r10     // Catch:{ Exception -> 0x03ac }
            r11 = r30
            r4.L$7 = r11     // Catch:{ Exception -> 0x03aa }
            r12 = r31
            r4.L$8 = r12     // Catch:{ Exception -> 0x03a8 }
            r13 = r32
            r4.L$9 = r13     // Catch:{ Exception -> 0x03a6 }
            r14 = r33
            r4.L$10 = r14     // Catch:{ Exception -> 0x03a4 }
            r15 = r34
            r4.L$11 = r15     // Catch:{ Exception -> 0x03a2 }
            r1 = r35
            r4.L$12 = r1     // Catch:{ Exception -> 0x03a2 }
            r1 = r36
            r4.L$13 = r1     // Catch:{ Exception -> 0x03a2 }
            r4.L$14 = r6     // Catch:{ Exception -> 0x03a2 }
            r1 = 1
            r4.label = r1     // Catch:{ Exception -> 0x03a2 }
            java.lang.Object r0 = r3.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x03a2 }
            if (r0 != r5) goto L_0x0399
            return r5
        L_0x0399:
            r1 = r23
            r0 = r36
            r16 = r6
            r6 = r35
            goto L_0x03ef
        L_0x03a2:
            r0 = move-exception
            goto L_0x03c7
        L_0x03a4:
            r0 = move-exception
            goto L_0x03c5
        L_0x03a6:
            r0 = move-exception
            goto L_0x03c3
        L_0x03a8:
            r0 = move-exception
            goto L_0x03c1
        L_0x03aa:
            r0 = move-exception
            goto L_0x03bf
        L_0x03ac:
            r0 = move-exception
            goto L_0x03bd
        L_0x03ae:
            r0 = move-exception
            goto L_0x03bb
        L_0x03b0:
            r0 = move-exception
            goto L_0x03b9
        L_0x03b2:
            r0 = move-exception
            goto L_0x03b7
        L_0x03b4:
            r0 = move-exception
            r7 = r24
        L_0x03b7:
            r8 = r27
        L_0x03b9:
            r9 = r28
        L_0x03bb:
            r10 = r29
        L_0x03bd:
            r11 = r30
        L_0x03bf:
            r12 = r31
        L_0x03c1:
            r13 = r32
        L_0x03c3:
            r14 = r33
        L_0x03c5:
            r15 = r34
        L_0x03c7:
            r1 = r23
            r24 = r36
            r17 = r0
            r16 = r6
            r6 = r35
        L_0x03d1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r25 = r3
            java.lang.String r3 = "Saving sleep data. error="
            r0.append(r3)
            java.lang.String r3 = r17.getMessage()
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r1.a(r2, r0)
            r0 = r24
            r3 = r25
        L_0x03ef:
            r17 = r16
            r16 = r5
            java.lang.String r5 = "Saving sleep data. OK"
            r1.a(r2, r5)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            r24 = r0
            java.lang.String r0 = com.fossil.sk5.a
            r25 = r6
            java.lang.String r6 = ".saveSyncResult - Upload sleep sessions to server"
            r5.d(r0, r6)
            java.lang.String r0 = "Saving activity data"
            r1.a(r2, r0)
            com.fossil.sk5$d r0 = new com.fossil.sk5$d
            r5 = 0
            r0.<init>(r7, r14, r5)
            r4.L$0 = r1
            r4.L$1 = r7
            r4.L$2 = r2
            r4.L$3 = r3
            r4.L$4 = r8
            r4.L$5 = r9
            r4.L$6 = r10
            r4.L$7 = r11
            r4.L$8 = r12
            r4.L$9 = r13
            r4.L$10 = r14
            r4.L$11 = r15
            r6 = r25
            r4.L$12 = r6
            r5 = r24
            r4.L$13 = r5
            r24 = r1
            r1 = r17
            r4.L$14 = r1
            r1 = 2
            r4.label = r1
            java.lang.Object r0 = com.fossil.zi7.a(r0, r4)
            r1 = r16
            if (r0 != r1) goto L_0x0446
            return r1
        L_0x0446:
            r16 = r1
            r1 = r8
            r8 = r6
            r6 = r3
            r3 = r24
            r20 = r5
            r5 = r2
            r2 = r7
            r7 = r20
            r21 = r13
            r13 = r9
            r9 = r21
            r22 = r12
            r12 = r10
            r10 = r22
        L_0x045d:
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            r25 = r14
            r24 = r15
            long r14 = r2.b()
            r7.a(r0, r14)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r14 = com.fossil.sk5.a
            java.lang.String r15 = ".saveSyncResult - Update heartbeat step by syncing"
            r0.d(r14, r15)
            java.util.List r0 = r2.c()     // Catch:{ Exception -> 0x04e6 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x04e6 }
            r4.L$1 = r2     // Catch:{ Exception -> 0x04e6 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x04e6 }
            r4.L$3 = r6     // Catch:{ Exception -> 0x04e6 }
            r4.L$4 = r1     // Catch:{ Exception -> 0x04e6 }
            r4.L$5 = r13     // Catch:{ Exception -> 0x04e6 }
            r4.L$6 = r12     // Catch:{ Exception -> 0x04e6 }
            r4.L$7 = r11     // Catch:{ Exception -> 0x04e6 }
            r4.L$8 = r10     // Catch:{ Exception -> 0x04e6 }
            r4.L$9 = r9     // Catch:{ Exception -> 0x04e6 }
            r14 = r25
            r4.L$10 = r14     // Catch:{ Exception -> 0x04e2 }
            r15 = r24
            r4.L$11 = r15     // Catch:{ Exception -> 0x04e0 }
            r4.L$12 = r8     // Catch:{ Exception -> 0x04e0 }
            r4.L$13 = r7     // Catch:{ Exception -> 0x04e0 }
            r24 = r1
            r1 = r17
            r4.L$14 = r1     // Catch:{ Exception -> 0x04dc }
            r17 = r1
            r1 = 3
            r4.label = r1     // Catch:{ Exception -> 0x04da }
            java.lang.Object r0 = r11.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x04da }
            r1 = r16
            if (r0 != r1) goto L_0x04b2
            return r1
        L_0x04b2:
            r16 = r6
            r6 = r8
            r8 = r14
            r14 = r24
            r20 = r3
            r3 = r2
            r2 = r20
        L_0x04bd:
            java.lang.String r0 = "Saving activity data. OK"
            r2.a(r5, r0)     // Catch:{ Exception -> 0x04d6 }
            r20 = r16
            r16 = r1
            r1 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r6
            r6 = r20
            r21 = r3
            r3 = r2
            r2 = r21
            goto L_0x0522
        L_0x04d6:
            r0 = move-exception
            r24 = r3
            goto L_0x04f8
        L_0x04da:
            r0 = move-exception
            goto L_0x04ed
        L_0x04dc:
            r0 = move-exception
            r17 = r1
            goto L_0x04ed
        L_0x04e0:
            r0 = move-exception
            goto L_0x04eb
        L_0x04e2:
            r0 = move-exception
            r15 = r24
            goto L_0x04eb
        L_0x04e6:
            r0 = move-exception
            r15 = r24
            r14 = r25
        L_0x04eb:
            r24 = r1
        L_0x04ed:
            r1 = r16
            r16 = r6
            r6 = r8
            r8 = r14
            r14 = r24
            r24 = r2
            r2 = r3
        L_0x04f8:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r25 = r6
            java.lang.String r6 = "Saving activity data. error="
            r3.append(r6)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.a(r5, r0)
            r3 = r2
            r6 = r16
            r2 = r24
            r16 = r1
            r1 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r8
            r8 = r25
        L_0x0522:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            r24 = r7
            java.lang.String r7 = com.fossil.sk5.a
            r25 = r8
            java.lang.String r8 = ".saveSyncResult - Update activities summary by syncing"
            r0.d(r7, r8)
            java.util.List r0 = r2.e()     // Catch:{ Exception -> 0x0597 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x0597 }
            r4.L$1 = r2     // Catch:{ Exception -> 0x0597 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x0597 }
            r4.L$3 = r6     // Catch:{ Exception -> 0x0597 }
            r4.L$4 = r1     // Catch:{ Exception -> 0x0597 }
            r4.L$5 = r14     // Catch:{ Exception -> 0x0597 }
            r4.L$6 = r13     // Catch:{ Exception -> 0x0597 }
            r4.L$7 = r12     // Catch:{ Exception -> 0x0597 }
            r4.L$8 = r11     // Catch:{ Exception -> 0x0597 }
            r4.L$9 = r10     // Catch:{ Exception -> 0x0597 }
            r4.L$10 = r9     // Catch:{ Exception -> 0x0597 }
            r4.L$11 = r15     // Catch:{ Exception -> 0x0597 }
            r7 = r25
            r4.L$12 = r7     // Catch:{ Exception -> 0x0593 }
            r8 = r24
            r4.L$13 = r8     // Catch:{ Exception -> 0x0591 }
            r24 = r2
            r2 = r17
            r4.L$14 = r2     // Catch:{ Exception -> 0x058d }
            r17 = r2
            r2 = 4
            r4.label = r2     // Catch:{ Exception -> 0x058b }
            java.lang.Object r0 = r1.insertFromDevice(r0, r4)     // Catch:{ Exception -> 0x058b }
            r2 = r16
            if (r0 != r2) goto L_0x056b
            return r2
        L_0x056b:
            r16 = r6
            r6 = r7
            r7 = r15
            r15 = r1
            r1 = r24
        L_0x0572:
            java.lang.String r0 = "Saving activity summaries data. OK"
            r3.a(r5, r0)     // Catch:{ Exception -> 0x0587 }
            r20 = r16
            r16 = r2
            r2 = r15
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r7
            r7 = r6
            r6 = r20
            goto L_0x05cf
        L_0x0587:
            r0 = move-exception
            r24 = r1
            goto L_0x05a5
        L_0x058b:
            r0 = move-exception
            goto L_0x059e
        L_0x058d:
            r0 = move-exception
            r17 = r2
            goto L_0x059e
        L_0x0591:
            r0 = move-exception
            goto L_0x059c
        L_0x0593:
            r0 = move-exception
            r8 = r24
            goto L_0x059c
        L_0x0597:
            r0 = move-exception
            r8 = r24
            r7 = r25
        L_0x059c:
            r24 = r2
        L_0x059e:
            r2 = r16
            r16 = r6
            r6 = r7
            r7 = r15
            r15 = r1
        L_0x05a5:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r25 = r6
            java.lang.String r6 = "Saving activity summaries data. error="
            r1.append(r6)
            java.lang.String r0 = r0.getMessage()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r3.a(r5, r0)
            r1 = r24
            r6 = r16
            r16 = r2
            r2 = r15
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r7
            r7 = r25
        L_0x05cf:
            java.lang.String r0 = "Saving goal tracking data"
            r3.a(r5, r0)
            java.util.List r0 = r1.f()     // Catch:{ Exception -> 0x0659 }
            boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x0659 }
            r18 = 1
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0640
            java.util.List r0 = r1.f()     // Catch:{ Exception -> 0x0659 }
            r4.L$0 = r3     // Catch:{ Exception -> 0x0659 }
            r4.L$1 = r1     // Catch:{ Exception -> 0x0659 }
            r4.L$2 = r5     // Catch:{ Exception -> 0x0659 }
            r4.L$3 = r6     // Catch:{ Exception -> 0x0659 }
            r4.L$4 = r2     // Catch:{ Exception -> 0x0659 }
            r4.L$5 = r15     // Catch:{ Exception -> 0x0659 }
            r4.L$6 = r14     // Catch:{ Exception -> 0x0659 }
            r4.L$7 = r13     // Catch:{ Exception -> 0x0659 }
            r4.L$8 = r12     // Catch:{ Exception -> 0x0659 }
            r4.L$9 = r11     // Catch:{ Exception -> 0x0659 }
            r4.L$10 = r10     // Catch:{ Exception -> 0x0659 }
            r4.L$11 = r9     // Catch:{ Exception -> 0x0659 }
            r4.L$12 = r7     // Catch:{ Exception -> 0x0659 }
            r4.L$13 = r8     // Catch:{ Exception -> 0x0659 }
            r31 = r1
            r1 = r17
            r4.L$14 = r1     // Catch:{ Exception -> 0x063c }
            r17 = r1
            r1 = 5
            r4.label = r1     // Catch:{ Exception -> 0x063a }
            r24 = r3
            r25 = r0
            r26 = r5
            r27 = r12
            r28 = r11
            r29 = r9
            r30 = r4
            java.lang.Object r0 = r24.a(r25, r26, r27, r28, r29, r30)     // Catch:{ Exception -> 0x063a }
            r1 = r16
            if (r0 != r1) goto L_0x0624
            return r1
        L_0x0624:
            r16 = r6
            r6 = r7
            r7 = r9
            r9 = r11
            r11 = r13
            r13 = r15
            r15 = r2
            r2 = r3
            r3 = r31
        L_0x062f:
            java.lang.String r0 = "Saving goal tracking data. OK"
            r2.a(r5, r0)     // Catch:{ Exception -> 0x0636 }
            goto L_0x0687
        L_0x0636:
            r0 = move-exception
            r24 = r3
            goto L_0x0669
        L_0x063a:
            r0 = move-exception
            goto L_0x065c
        L_0x063c:
            r0 = move-exception
            r17 = r1
            goto L_0x065c
        L_0x0640:
            r31 = r1
            r1 = r16
            java.lang.String r0 = "No goal tracking data"
            r3.a(r5, r0)     // Catch:{ Exception -> 0x0657 }
            r16 = r1
            r1 = r2
            r2 = r3
            r0 = r17
            r3 = r31
            r20 = r7
            r7 = r6
            r6 = r20
            goto L_0x0694
        L_0x0657:
            r0 = move-exception
            goto L_0x065e
        L_0x0659:
            r0 = move-exception
            r31 = r1
        L_0x065c:
            r1 = r16
        L_0x065e:
            r24 = r31
            r16 = r6
            r6 = r7
            r7 = r9
            r9 = r11
            r11 = r13
            r13 = r15
            r15 = r2
            r2 = r3
        L_0x0669:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r25 = r6
            java.lang.String r6 = "Saving goal tracking data. error="
            r3.append(r6)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.a(r5, r0)
            r3 = r24
            r6 = r25
        L_0x0687:
            r0 = r17
            r20 = r16
            r16 = r1
            r1 = r15
            r15 = r13
            r13 = r11
            r11 = r9
            r9 = r7
            r7 = r20
        L_0x0694:
            r4.L$0 = r2
            r4.L$1 = r3
            r4.L$2 = r5
            r4.L$3 = r7
            r4.L$4 = r1
            r4.L$5 = r15
            r4.L$6 = r14
            r4.L$7 = r13
            r4.L$8 = r12
            r4.L$9 = r11
            r4.L$10 = r10
            r4.L$11 = r9
            r4.L$12 = r6
            r4.L$13 = r8
            r4.L$14 = r0
            r17 = r0
            r0 = 6
            r4.label = r0
            java.lang.Object r0 = r14.pushPendingFitnessData(r4)
            r18 = r1
            r1 = r16
            if (r0 != r1) goto L_0x06c2
            return r1
        L_0x06c2:
            r33 = r15
            r34 = r18
            r15 = r14
            r14 = r13
            r13 = r12
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r7
            r7 = r5
            r5 = r3
            r3 = r2
            r2 = r6
            r6 = r17
        L_0x06d3:
            com.fossil.zi5 r0 = (com.fossil.zi5) r0
            r16 = r1
            boolean r1 = r0 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x077a
            r1 = r0
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x077e
            boolean r17 = r1.isEmpty()
            r18 = 1
            r17 = r17 ^ 1
            if (r17 == 0) goto L_0x077e
            r35 = r0
            com.fossil.se7 r0 = new com.fossil.se7
            r0.<init>()
            r17 = r6
            r6 = 0
            java.lang.Object r18 = r1.get(r6)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r18 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r18
            org.joda.time.DateTime r6 = r18.getStartTimeTZ()
            r0.element = r6
            com.fossil.se7 r6 = new com.fossil.se7
            r6.<init>()
            r18 = r8
            r8 = 0
            java.lang.Object r8 = r1.get(r8)
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r8 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r8
            org.joda.time.DateTime r8 = r8.getEndTimeTZ()
            r6.element = r8
            com.fossil.ti7 r8 = com.fossil.qj7.a()
            r36 = r8
            com.fossil.sk5$e r8 = new com.fossil.sk5$e
            r19 = 0
            r24 = r8
            r25 = r1
            r26 = r0
            r27 = r6
            r28 = r14
            r29 = r34
            r30 = r9
            r31 = r33
            r32 = r19
            r24.<init>(r25, r26, r27, r28, r29, r30, r31, r32)
            r4.L$0 = r3
            r4.L$1 = r5
            r4.L$2 = r7
            r4.L$3 = r9
            r3 = r34
            r4.L$4 = r3
            r3 = r33
            r4.L$5 = r3
            r4.L$6 = r15
            r4.L$7 = r14
            r4.L$8 = r13
            r4.L$9 = r12
            r4.L$10 = r11
            r4.L$11 = r10
            r4.L$12 = r2
            r3 = r18
            r4.L$13 = r3
            r3 = r17
            r4.L$14 = r3
            r3 = r35
            r4.L$15 = r3
            r4.L$16 = r1
            r4.L$17 = r0
            r4.L$18 = r6
            r0 = 7
            r4.label = r0
            r0 = r36
            java.lang.Object r0 = com.fossil.vh7.a(r0, r8, r4)
            r1 = r16
            if (r0 != r1) goto L_0x0777
            return r1
        L_0x0777:
            r3 = r7
        L_0x0778:
            r7 = r3
            goto L_0x077e
        L_0x077a:
            r3 = r0
            boolean r0 = r3 instanceof com.fossil.yi5
        L_0x077e:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sk5.a
            java.lang.String r3 = "DONE save to database, delete cache data file in button service"
            r0.i(r1, r3)
            r2.b(r7)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x0791:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.sk5.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error inside "
            r2.append(r3)
            java.lang.String r3 = com.fossil.sk5.a
            r2.append(r3)
            java.lang.String r3 = ".saveSyncResult - Sync data does not match any user's device"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
            switch-data {0->0x02e9, 1->0x026a, 2->0x0214, 3->0x01b4, 4->0x0146, 5->0x00e0, 6->0x0084, 7->0x0033, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sk5.a(com.fossil.sk5$a, java.lang.String, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.PortfolioApp, com.fossil.ge5, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final String a(List<ActivitySample> list) {
        if (list.isEmpty()) {
            return "empty\n";
        }
        StringBuilder sb = new StringBuilder();
        for (ActivitySample activitySample : list) {
            sb.append("[");
            sb.append("startTime:");
            sb.append(activitySample.getStartTime());
            sb.append(", step:");
            sb.append(activitySample.getSteps());
            sb.append("]");
        }
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.util.List<? extends com.portfolio.platform.service.syncmodel.WrapperTapEventSummary> r24, java.lang.String r25, com.portfolio.platform.data.source.HybridPresetRepository r26, com.portfolio.platform.data.source.GoalTrackingRepository r27, com.portfolio.platform.data.source.UserRepository r28, com.fossil.fb7<? super com.fossil.i97> r29) {
        /*
            r23 = this;
            r1 = r23
            r2 = r25
            r0 = r26
            r3 = r27
            r4 = r29
            boolean r5 = r4 instanceof com.fossil.sk5.b
            if (r5 == 0) goto L_0x001d
            r5 = r4
            com.fossil.sk5$b r5 = (com.fossil.sk5.b) r5
            int r6 = r5.label
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = r6 & r7
            if (r8 == 0) goto L_0x001d
            int r6 = r6 - r7
            r5.label = r6
            goto L_0x0022
        L_0x001d:
            com.fossil.sk5$b r5 = new com.fossil.sk5$b
            r5.<init>(r1, r4)
        L_0x0022:
            java.lang.Object r4 = r5.result
            java.lang.Object r6 = com.fossil.nb7.a()
            int r7 = r5.label
            r8 = 1
            if (r7 == 0) goto L_0x0065
            if (r7 != r8) goto L_0x005d
            java.lang.Object r0 = r5.L$8
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.lang.Object r0 = r5.L$7
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            java.lang.Object r0 = r5.L$6
            com.portfolio.platform.PortfolioApp r0 = (com.portfolio.platform.PortfolioApp) r0
            java.lang.Object r0 = r5.L$5
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            java.lang.Object r0 = r5.L$4
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            java.lang.Object r0 = r5.L$3
            com.portfolio.platform.data.source.HybridPresetRepository r0 = (com.portfolio.platform.data.source.HybridPresetRepository) r0
            java.lang.Object r0 = r5.L$2
            r2 = r0
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r0 = r5.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r5.L$0
            r3 = r0
            com.fossil.sk5 r3 = (com.fossil.sk5) r3
            com.fossil.t87.a(r4)     // Catch:{ Exception -> 0x005a }
            goto L_0x0122
        L_0x005a:
            r0 = move-exception
            goto L_0x012a
        L_0x005d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0065:
            com.fossil.t87.a(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r7 = com.fossil.sk5.a
            java.lang.String r9 = "start process goal data, get active preset to check goal tracking"
            r4.d(r7, r9)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r7 = r0.getActivePresetBySerial(r2)
            if (r7 == 0) goto L_0x0143
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.sk5.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "processGoalTrackingData getHybridActivePreset success activePreset="
            r11.append(r12)
            r11.append(r7)
            java.lang.String r11 = r11.toString()
            r9.d(r10, r11)
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.Iterator r10 = r24.iterator()
        L_0x00a6:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x00fd
            java.lang.Object r11 = r10.next()
            com.portfolio.platform.service.syncmodel.WrapperTapEventSummary r11 = (com.portfolio.platform.service.syncmodel.WrapperTapEventSummary) r11
            int r12 = r11.startTime
            long r12 = (long) r12
            r14 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 * r14
            java.util.Date r15 = new java.util.Date
            r15.<init>(r12)
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r12 = new com.portfolio.platform.data.model.goaltracking.GoalTrackingData
            java.util.UUID r13 = java.util.UUID.randomUUID()
            java.lang.String r13 = r13.toString()
            java.lang.String r14 = "UUID.randomUUID().toString()"
            com.fossil.ee7.a(r13, r14)
            int r14 = r11.timezoneOffsetInSecond
            org.joda.time.DateTime r14 = com.fossil.zd5.a(r15, r14)
            java.lang.String r8 = "DateHelper.createDateTim\u2026y.timezoneOffsetInSecond)"
            com.fossil.ee7.a(r14, r8)
            int r8 = r11.timezoneOffsetInSecond
            java.util.Date r11 = new java.util.Date
            r11.<init>()
            long r19 = r11.getTime()
            java.util.Date r11 = new java.util.Date
            r11.<init>()
            long r21 = r11.getTime()
            r11 = r14
            r14 = r12
            r18 = r15
            r15 = r13
            r16 = r11
            r17 = r8
            r14.<init>(r15, r16, r17, r18, r19, r21)
            r9.add(r12)
            r8 = 1
            goto L_0x00a6
        L_0x00fd:
            java.util.List r8 = com.fossil.ea7.d(r9)     // Catch:{ Exception -> 0x0128 }
            r5.L$0 = r1     // Catch:{ Exception -> 0x0128 }
            r10 = r24
            r5.L$1 = r10     // Catch:{ Exception -> 0x0128 }
            r5.L$2 = r2     // Catch:{ Exception -> 0x0128 }
            r5.L$3 = r0     // Catch:{ Exception -> 0x0128 }
            r5.L$4 = r3     // Catch:{ Exception -> 0x0128 }
            r0 = r28
            r5.L$5 = r0     // Catch:{ Exception -> 0x0128 }
            r5.L$6 = r4     // Catch:{ Exception -> 0x0128 }
            r5.L$7 = r7     // Catch:{ Exception -> 0x0128 }
            r5.L$8 = r9     // Catch:{ Exception -> 0x0128 }
            r0 = 1
            r5.label = r0     // Catch:{ Exception -> 0x0128 }
            java.lang.Object r0 = r3.insertFromDevice(r8, r5)     // Catch:{ Exception -> 0x0128 }
            if (r0 != r6) goto L_0x0121
            return r6
        L_0x0121:
            r3 = r1
        L_0x0122:
            java.lang.String r0 = "Saving goal tracking data. OK"
            r3.a(r2, r0)
            goto L_0x0157
        L_0x0128:
            r0 = move-exception
            r3 = r1
        L_0x012a:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Saving goal tracking data. error="
            r4.append(r5)
            java.lang.String r0 = r0.getMessage()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r3.a(r2, r0)
            goto L_0x0157
        L_0x0143:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = com.fossil.sk5.a
            java.lang.String r5 = "processGoalTrackingData getHybridActivePreset onFail"
            r0.d(r3, r5)
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC
            java.lang.String r3 = "Goal tracking is not set as active app since user don't have active device."
            r4.a(r0, r2, r3)
        L_0x0157:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sk5.a(java.util.List, java.lang.String, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.UserRepository, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.g0.c().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }
}
