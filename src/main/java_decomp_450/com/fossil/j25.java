package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j25 extends i25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i V;
    @DexIgnore
    public static /* final */ SparseIntArray W;
    @DexIgnore
    public long U;

    /*
    static {
        ViewDataBinding.i iVar = new ViewDataBinding.i(30);
        V = iVar;
        iVar.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{2131558835});
        SparseIntArray sparseIntArray = new SparseIntArray();
        W = sparseIntArray;
        sparseIntArray.put(2131362335, 3);
        W.put(2131362334, 4);
        W.put(2131362747, 5);
        W.put(2131361960, 6);
        W.put(2131362977, 7);
        W.put(2131362470, 8);
        W.put(2131362771, 9);
        W.put(2131363227, 10);
        W.put(2131362736, 11);
        W.put(2131363390, 12);
        W.put(2131362767, 13);
        W.put(2131362342, 14);
        W.put(2131363391, 15);
        W.put(2131362797, 16);
        W.put(2131362526, 17);
        W.put(2131363392, 18);
        W.put(2131362388, 19);
        W.put(2131362490, 20);
        W.put(2131363110, 21);
        W.put(2131362097, 22);
        W.put(2131362493, 23);
        W.put(2131362788, 24);
        W.put(2131362494, 25);
        W.put(2131362491, 26);
        W.put(2131362787, 27);
        W.put(2131362492, 28);
        W.put(2131362461, 29);
    }
    */

    @DexIgnore
    public j25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 30, V, W));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.U = 0;
        }
        ViewDataBinding.d(((i25) this).F);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (((com.fossil.i25) r6).F.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            monitor-enter(r6)
            long r0 = r6.U     // Catch:{ all -> 0x0018 }
            r2 = 0
            r4 = 1
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            return r4
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0018 }
            com.fossil.ua5 r0 = r6.F
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0016
            return r4
        L_0x0016:
            r0 = 0
            return r0
        L_0x0018:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j25.e():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.U = 2;
        }
        ((i25) this).F.f();
        g();
    }

    @DexIgnore
    public j25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 1, (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[22], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[29], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[17], (ua5) objArr[2], (RTLImageView) objArr[11], (ConstraintLayout) objArr[5], (LinearLayout) objArr[13], (ConstraintLayout) objArr[9], (LinearLayout) objArr[27], (LinearLayout) objArr[24], (LinearLayout) objArr[16], (ConstraintLayout) objArr[0], (RecyclerView) objArr[7], (SwitchCompat) objArr[21], (FlexibleTextView) objArr[10], (View) objArr[12], (View) objArr[15], (View) objArr[18]);
        this.U = -1;
        ((i25) this).r.setTag(null);
        ((i25) this).N.setTag(null);
        a(view);
        f();
    }
}
