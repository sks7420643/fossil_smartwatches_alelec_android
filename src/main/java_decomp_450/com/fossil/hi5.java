package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hi5 extends BaseDbProvider implements gi5 {
    @DexIgnore
    public hi5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.gi5
    public void a(PinObject pinObject) {
        if (pinObject != null) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = ((BaseDbProvider) this).TAG;
                local.d(str, "Unpin new object - uuid=" + pinObject.getUuid() + ", className=" + pinObject.getClassName());
                e().delete(pinObject);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ((BaseDbProvider) this).TAG;
                local2.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".unpin - e=" + e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gi5
    public List<PinObject> b(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<PinObject, Integer> queryBuilder = e().queryBuilder();
            Where<PinObject, Integer> where = queryBuilder.where();
            where.eq(PinObject.COLUMN_CLASS_NAME, str);
            queryBuilder.setWhere(where);
            List<PinObject> query = e().query(queryBuilder.prepare());
            return (query == null || query.isEmpty()) ? arrayList : query;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            local.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".find - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public final Dao<PinObject, Integer> e() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(PinObject.class);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{PinObject.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }
}
