package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uc2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uc2> CREATOR; // = new tc2();
    @DexIgnore
    public static /* final */ uc2 b; // = new uc2("com.google.android.gms");
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public uc2(String str) {
        a72.a((Object) str);
        this.a = str;
    }

    @DexIgnore
    public static uc2 b(String str) {
        if ("com.google.android.gms".equals(str)) {
            return b;
        }
        return new uc2(str);
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof uc2)) {
            return false;
        }
        return this.a.equals(((uc2) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s}", this.a);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a, false);
        k72.a(parcel, a2);
    }
}
