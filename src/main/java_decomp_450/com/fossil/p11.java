package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p11 extends ey0 {
    @DexIgnore
    public static /* final */ vz0 N; // = new vz0(null);
    @DexIgnore
    public /* final */ m70 L;
    @DexIgnore
    public /* final */ j70[] M;

    @DexIgnore
    public p11(ri1 ri1, m70 m70, j70[] j70Arr) {
        super(uh0.m, qa1.z, ri1, 0, 8);
        this.L = m70;
        this.M = j70Arr;
        ((uh1) this).E = true;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.U0, yz0.a(this.L)), r51.Z0, yz0.a(this.M));
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.ey0
    public byte[] m() {
        byte[] array = ByteBuffer.allocate((this.M.length * 5) + 2).order(ByteOrder.LITTLE_ENDIAN).put(this.L.a()).put((byte) this.M.length).put(N.a(this.M)).array();
        ee7.a((Object) array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }
}
