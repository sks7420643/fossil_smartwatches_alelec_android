package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd2 implements Parcelable.Creator<pd2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pd2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        ArrayList arrayList = null;
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                arrayList = j72.c(parcel, a, gc2.CREATOR);
            } else if (a2 != 2) {
                j72.v(parcel, a);
            } else {
                status = (Status) j72.a(parcel, a, Status.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new pd2(arrayList, status);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ pd2[] newArray(int i) {
        return new pd2[i];
    }
}
