package com.fossil;

import java.util.concurrent.Executor;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qv1 implements sv1 {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(uu1.class.getName());
    @DexIgnore
    public /* final */ pw1 a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ bv1 c;
    @DexIgnore
    public /* final */ sw1 d;
    @DexIgnore
    public /* final */ ay1 e;

    @DexIgnore
    public qv1(Executor executor, bv1 bv1, pw1 pw1, sw1 sw1, ay1 ay1) {
        this.b = executor;
        this.c = bv1;
        this.a = pw1;
        this.d = sw1;
        this.e = ay1;
    }

    @DexIgnore
    @Override // com.fossil.sv1
    public void a(pu1 pu1, ku1 ku1, ht1 ht1) {
        this.b.execute(ov1.a(this, pu1, ht1, ku1));
    }

    @DexIgnore
    public static /* synthetic */ void a(qv1 qv1, pu1 pu1, ht1 ht1, ku1 ku1) {
        try {
            jv1 a2 = qv1.c.a(pu1.a());
            if (a2 == null) {
                String format = String.format("Transport backend '%s' is not registered", pu1.a());
                f.warning(format);
                ht1.a(new IllegalArgumentException(format));
                return;
            }
            qv1.e.a(pv1.a(qv1, pu1, a2.a(ku1)));
            ht1.a(null);
        } catch (Exception e2) {
            Logger logger = f;
            logger.warning("Error scheduling event " + e2.getMessage());
            ht1.a(e2);
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(qv1 qv1, pu1 pu1, ku1 ku1) {
        qv1.d.a(pu1, ku1);
        qv1.a.a(pu1, 1);
        return null;
    }
}
