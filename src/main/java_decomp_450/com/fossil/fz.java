package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz implements zy<byte[]> {
    @DexIgnore
    @Override // com.fossil.zy
    public int a() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.zy
    public String getTag() {
        return "ByteArrayPool";
    }

    @DexIgnore
    public int a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    @Override // com.fossil.zy
    public byte[] newArray(int i) {
        return new byte[i];
    }
}
