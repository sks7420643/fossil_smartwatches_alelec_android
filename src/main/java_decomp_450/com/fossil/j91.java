package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class j91 extends Enum<j91> {
    @DexIgnore
    public static /* final */ j91 a;
    @DexIgnore
    public static /* final */ j91 b;
    @DexIgnore
    public static /* final */ j91 c;
    @DexIgnore
    public static /* final */ j91 d;
    @DexIgnore
    public static /* final */ j91 e;
    @DexIgnore
    public static /* final */ j91 f;
    @DexIgnore
    public static /* final */ /* synthetic */ j91[] g;

    /*
    static {
        j91 j91 = new j91("MAC_ADDRESS_SERIAL_NUMBER_MAP_PREFERENCE", 0);
        a = j91;
        j91 j912 = new j91("SDK_LOG_PREFERENCE", 2);
        b = j912;
        j91 j913 = new j91("MINUTE_DATA_REFERENCE", 3);
        c = j913;
        j91 j914 = new j91("HARDWARE_LOG_REFERENCE", 4);
        d = j914;
        j91 j915 = new j91("GPS_REFERENCE", 5);
        e = j915;
        j91 j916 = new j91("TEXT_ENCRYPTION_PREFERENCE", 6);
        f = j916;
        g = new j91[]{j91, new j91("DEBUG_LOG_PREFERENCE", 1), j912, j913, j914, j915, j916};
    }
    */

    @DexIgnore
    public j91(String str, int i) {
    }

    @DexIgnore
    public static j91 valueOf(String str) {
        return (j91) Enum.valueOf(j91.class, str);
    }

    @DexIgnore
    public static j91[] values() {
        return (j91[]) g.clone();
    }
}
