package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rb7 extends ob7 {
    @DexIgnore
    public /* final */ ib7 _context;
    @DexIgnore
    public transient fb7<Object> intercepted;

    @DexIgnore
    public rb7(fb7<Object> fb7, ib7 ib7) {
        super(fb7);
        this._context = ib7;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        ib7 ib7 = this._context;
        if (ib7 != null) {
            return ib7;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final fb7<Object> intercepted() {
        fb7<Object> fb7 = this.intercepted;
        if (fb7 == null) {
            gb7 gb7 = (gb7) getContext().get(gb7.m);
            if (gb7 == null || (fb7 = gb7.b(this)) == null) {
                fb7 = this;
            }
            this.intercepted = fb7;
        }
        return fb7;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public void releaseIntercepted() {
        fb7<?> fb7 = this.intercepted;
        if (!(fb7 == null || fb7 == this)) {
            ib7.b bVar = getContext().get(gb7.m);
            if (bVar != null) {
                ((gb7) bVar).a(fb7);
            } else {
                ee7.a();
                throw null;
            }
        }
        this.intercepted = qb7.a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public rb7(fb7<Object> fb7) {
        this(fb7, fb7 != null ? fb7.getContext() : null);
    }
}
