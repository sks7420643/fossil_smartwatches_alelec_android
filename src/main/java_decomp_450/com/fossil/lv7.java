package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv7<T> implements tu7<mo7, T> {
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public lv7(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    public T a(mo7 mo7) throws IOException {
        JsonReader a2 = this.a.a(mo7.charStream());
        try {
            T read = this.b.read(a2);
            if (a2.peek() == pf4.END_DOCUMENT) {
                return read;
            }
            throw new ge4("JSON document was not fully consumed.");
        } finally {
            mo7.close();
        }
    }
}
