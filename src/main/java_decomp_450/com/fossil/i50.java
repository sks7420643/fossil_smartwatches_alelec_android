package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i50 {
    @DexIgnore
    public static /* final */ ConcurrentMap<String, yw> a; // = new ConcurrentHashMap();

    @DexIgnore
    public static String a(PackageInfo packageInfo) {
        if (packageInfo != null) {
            return String.valueOf(packageInfo.versionCode);
        }
        return UUID.randomUUID().toString();
    }

    @DexIgnore
    public static yw b(Context context) {
        String packageName = context.getPackageName();
        yw ywVar = a.get(packageName);
        if (ywVar != null) {
            return ywVar;
        }
        yw c = c(context);
        yw putIfAbsent = a.putIfAbsent(packageName, c);
        return putIfAbsent == null ? c : putIfAbsent;
    }

    @DexIgnore
    public static yw c(Context context) {
        return new k50(a(a(context)));
    }

    @DexIgnore
    public static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("AppVersionSignature", "Cannot resolve info for" + context.getPackageName(), e);
            return null;
        }
    }
}
