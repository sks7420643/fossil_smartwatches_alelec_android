package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h13 implements tr2<g13> {
    @DexIgnore
    public static h13 b; // = new h13();
    @DexIgnore
    public /* final */ tr2<g13> a;

    @DexIgnore
    public h13(tr2<g13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((g13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((g13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ g13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public h13() {
        this(sr2.a(new j13()));
    }
}
