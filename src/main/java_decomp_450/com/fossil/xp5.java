package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp5 extends RecyclerView.g<c> {
    @DexIgnore
    public List<ContactGroup> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ r40 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ContactGroup contactGroup);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ c95 a;
        @DexIgnore
        public /* final */ /* synthetic */ xp5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b = this.a.b.b;
                if (b != null) {
                    b.a();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1 && (b = this.a.b.b) != null) {
                    b.a((ContactGroup) this.a.b.a.get(adapterPosition));
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xp5$c$c")
        /* renamed from: com.fossil.xp5$c$c  reason: collision with other inner class name */
        public static final class C0245c implements q40<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ ContactGroup b;

            @DexIgnore
            public C0245c(c cVar, ContactGroup contactGroup) {
                this.a = cVar;
                this.b = contactGroup;
            }

            @DexIgnore
            @Override // com.fossil.q40
            public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onLoadFailed");
                Contact contact = this.b.getContacts().get(0);
                ee7.a((Object) contact, "contactGroup.contacts[0]");
                contact.setPhotoThumbUri(null);
                return false;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onResourceReady");
                this.a.a.t.setImageDrawable(drawable);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xp5 xp5, c95 c95) {
            super(c95.d());
            ee7.b(c95, "binding");
            this.b = xp5;
            this.a = c95;
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.a.u.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.q.setOnClickListener(new a(this));
            this.a.s.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(ContactGroup contactGroup) {
            Uri uri;
            ee7.b(contactGroup, "contactGroup");
            FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "set favorite contact data");
            if (contactGroup.getContacts().size() > 0) {
                Contact contact = contactGroup.getContacts().get(0);
                ee7.a((Object) contact, "contactGroup.contacts[0]");
                if (!TextUtils.isEmpty(contact.getPhotoThumbUri())) {
                    Contact contact2 = contactGroup.getContacts().get(0);
                    ee7.a((Object) contact2, "contactGroup.contacts[0]");
                    uri = Uri.parse(contact2.getPhotoThumbUri());
                } else {
                    uri = null;
                }
                FossilNotificationImageView fossilNotificationImageView = this.a.t;
                ee7.a((Object) fossilNotificationImageView, "binding.ivFavoriteContactIcon");
                kd5 a2 = hd5.a(fossilNotificationImageView.getContext());
                Contact contact3 = contactGroup.getContacts().get(0);
                ee7.a((Object) contact3, "contactGroup.contacts[0]");
                jd5<Drawable> a3 = a2.a((Object) new gd5(uri, contact3.getDisplayName())).a(true).a(iy.a).a((k40<?>) this.b.c);
                FossilNotificationImageView fossilNotificationImageView2 = this.a.t;
                ee7.a((Object) fossilNotificationImageView2, "binding.ivFavoriteContactIcon");
                kd5 a4 = hd5.a(fossilNotificationImageView2.getContext());
                Contact contact4 = contactGroup.getContacts().get(0);
                ee7.a((Object) contact4, "contactGroup.contacts[0]");
                jd5<Drawable> b2 = a3.a((hw<Drawable>) a4.a((Object) new gd5((Uri) null, contact4.getDisplayName())).a((k40<?>) this.b.c)).b((q40<Drawable>) new C0245c(this, contactGroup));
                FossilNotificationImageView fossilNotificationImageView3 = this.a.t;
                ee7.a((Object) fossilNotificationImageView3, "binding.ivFavoriteContactIcon");
                b2.a((ImageView) fossilNotificationImageView3.getFossilCircleImageView());
                Contact contact5 = contactGroup.getContacts().get(0);
                ee7.a((Object) contact5, "contactGroup.contacts[0]");
                if (TextUtils.isEmpty(contact5.getPhotoThumbUri())) {
                    this.a.t.a();
                    FossilNotificationImageView fossilNotificationImageView4 = this.a.t;
                    ee7.a((Object) fossilNotificationImageView4, "binding.ivFavoriteContactIcon");
                    fossilNotificationImageView4.setBackground(v6.c(PortfolioApp.g0.c(), 2131231270));
                } else {
                    this.a.t.b();
                    this.a.t.setBackgroundResource(R.color.transparent);
                }
                FlexibleTextView flexibleTextView = this.a.r;
                ee7.a((Object) flexibleTextView, "binding.ftvFavoriteContactName");
                Contact contact6 = contactGroup.getContacts().get(0);
                ee7.a((Object) contact6, "contactGroup.contacts[0]");
                flexibleTextView.setText(contact6.getDisplayName());
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public xp5() {
        k40 a2 = new r40().a((ex<Bitmap>) new vd5());
        ee7.a((Object) a2, "RequestOptions().transform(CircleTransform())");
        this.c = (r40) a2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        cVar.a(this.a.get(i));
    }

    @DexIgnore
    public final List<ContactGroup> c() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        c95 a2 = c95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemFavoriteContactNotif\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public final void a(List<ContactGroup> list) {
        ee7.b(list, "listContactGroup");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0061, code lost:
        if (r3 == r5.getContactId()) goto L_0x0065;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.wearables.fsl.contact.ContactGroup r9) {
        /*
            r8 = this;
            java.lang.String r0 = "contactGroup"
            com.fossil.ee7.b(r9, r0)
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r0 = r8.a
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
            r2 = 0
        L_0x000d:
            boolean r3 = r0.hasNext()
            r4 = -1
            if (r3 == 0) goto L_0x006b
            java.lang.Object r3 = r0.next()
            com.fossil.wearables.fsl.contact.ContactGroup r3 = (com.fossil.wearables.fsl.contact.ContactGroup) r3
            java.util.List r5 = r3.getContacts()
            java.lang.String r6 = "it.contacts"
            com.fossil.ee7.a(r5, r6)
            boolean r5 = r5.isEmpty()
            r6 = 1
            r5 = r5 ^ r6
            if (r5 == 0) goto L_0x0064
            java.util.List r5 = r9.getContacts()
            java.lang.String r7 = "contactGroup.contacts"
            com.fossil.ee7.a(r5, r7)
            boolean r5 = r5.isEmpty()
            r5 = r5 ^ r6
            if (r5 == 0) goto L_0x0064
            java.util.List r3 = r3.getContacts()
            java.lang.Object r3 = r3.get(r1)
            java.lang.String r5 = "it.contacts[0]"
            com.fossil.ee7.a(r3, r5)
            com.fossil.wearables.fsl.contact.Contact r3 = (com.fossil.wearables.fsl.contact.Contact) r3
            int r3 = r3.getContactId()
            java.util.List r5 = r9.getContacts()
            java.lang.Object r5 = r5.get(r1)
            java.lang.String r7 = "contactGroup.contacts[0]"
            com.fossil.ee7.a(r5, r7)
            com.fossil.wearables.fsl.contact.Contact r5 = (com.fossil.wearables.fsl.contact.Contact) r5
            int r5 = r5.getContactId()
            if (r3 != r5) goto L_0x0064
            goto L_0x0065
        L_0x0064:
            r6 = 0
        L_0x0065:
            if (r6 == 0) goto L_0x0068
            goto L_0x006c
        L_0x0068:
            int r2 = r2 + 1
            goto L_0x000d
        L_0x006b:
            r2 = -1
        L_0x006c:
            if (r2 == r4) goto L_0x0076
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r9 = r8.a
            r9.remove(r2)
            r8.notifyItemRemoved(r2)
        L_0x0076:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xp5.a(com.fossil.wearables.fsl.contact.ContactGroup):void");
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
