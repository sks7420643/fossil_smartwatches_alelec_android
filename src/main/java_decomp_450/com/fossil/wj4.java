package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import com.fossil.lo7;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wj4 {
    @DexIgnore
    public /* final */ PortfolioApp a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ wj4 a;

        @DexIgnore
        public a(wj4 wj4) {
            this.a = wj4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            lo7.a f = chain.c().f();
            f.a("Content-Type", Constants.CONTENT_TYPE);
            f.a("User-Agent", ab5.b.a());
            f.a("locale", this.a.a.l());
            return chain.a(f.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ wj4 a;

        @DexIgnore
        public b(wj4 wj4) {
            this.a = wj4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            lo7.a f = chain.c().f();
            f.a("Content-Type", Constants.CONTENT_TYPE);
            f.a("User-Agent", ab5.b.a());
            f.a("locale", this.a.a.l());
            return chain.a(f.a());
        }
    }

    @DexIgnore
    public wj4(PortfolioApp portfolioApp) {
        ee7.b(portfolioApp, "mApplication");
        this.a = portfolioApp;
    }

    @DexIgnore
    public final PortfolioApp b() {
        return this.a;
    }

    @DexIgnore
    public final Context c() {
        Context applicationContext = this.a.getApplicationContext();
        ee7.a((Object) applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ShortcutApiService d(hj5 hj5, lj5 lj5) {
        ee7.b(hj5, "interceptor");
        ee7.b(lj5, "authenticator");
        return (ShortcutApiService) a(hj5, lj5, ShortcutApiService.class);
    }

    @DexIgnore
    public final ContentResolver e() {
        ContentResolver contentResolver = this.a.getContentResolver();
        ee7.a((Object) contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final ih5 f() {
        return new ih5();
    }

    @DexIgnore
    public final qg5 g() {
        return new qg5();
    }

    @DexIgnore
    public final FirmwareFileRepository h() {
        Context applicationContext = this.a.getApplicationContext();
        ee7.a((Object) applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    public final GuestApiService i() {
        b bVar = new b(this);
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 0, "2") + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(bVar);
        return (GuestApiService) kj5.g.a(GuestApiService.class);
    }

    @DexIgnore
    public final qe j() {
        qe a2 = qe.a(this.a);
        ee7.a((Object) a2, "LocalBroadcastManager.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final jh5 k() {
        return new jh5();
    }

    @DexIgnore
    public final nj4 l() {
        nj4 a2 = nj4.a(this.a);
        ee7.a((Object) a2, "MFLocationService.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final rl4 m() {
        return new rl4(new tl4());
    }

    @DexIgnore
    public final wx6 n() {
        return wx6.d.a();
    }

    @DexIgnore
    public final df5 o() {
        df5 b2 = df5.b();
        ee7.a((Object) b2, "WatchHelper.getInstance()");
        return b2;
    }

    @DexIgnore
    public final kh5 p() {
        return new kh5();
    }

    @DexIgnore
    public final lh5 q() {
        return new lh5();
    }

    @DexIgnore
    public final ch5 a(Context context) {
        ee7.b(context, "context");
        return new ch5(context);
    }

    @DexIgnore
    public final AuthApiUserService b(hj5 hj5, lj5 lj5) {
        ee7.b(hj5, "interceptor");
        ee7.b(lj5, "authenticator");
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 0, "2.1") + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(lj5);
        kj5.g.a(hj5);
        return (AuthApiUserService) kj5.g.a(AuthApiUserService.class);
    }

    @DexIgnore
    public final GoogleApiService c(hj5 hj5, lj5 lj5) {
        ee7.b(hj5, "interceptor");
        ee7.b(lj5, "authenticator");
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 4) + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(lj5);
        kj5.g.a(hj5);
        return (GoogleApiService) kj5.g.a(GoogleApiService.class);
    }

    @DexIgnore
    public final AuthApiGuestService d() {
        a aVar = new a(this);
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 0, "2.1") + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(aVar);
        return (AuthApiGuestService) kj5.g.a(AuthApiGuestService.class);
    }

    @DexIgnore
    public final ApplicationEventListener a(PortfolioApp portfolioApp, ch5 ch5, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, ad5 ad5, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, fp4 fp4, xo4 xo4, ro4 ro4, FileRepository fileRepository, to4 to4, WorkoutSettingRepository workoutSettingRepository, lm4 lm4, uj5 uj5, ThemeRepository themeRepository) {
        ee7.b(portfolioApp, "app");
        ee7.b(ch5, "sharedPreferencesManager");
        ee7.b(hybridPresetRepository, "hybridPresetRepository");
        ee7.b(categoryRepository, "categoryRepository");
        ee7.b(watchAppRepository, "watchAppRepository");
        ee7.b(complicationRepository, "complicationRepository");
        ee7.b(microAppRepository, "microAppRepository");
        ee7.b(dianaPresetRepository, "dianaPresetRepository");
        ee7.b(deviceRepository, "deviceRepository");
        ee7.b(userRepository, "userRepository");
        ee7.b(alarmsRepository, "alarmsRepository");
        ee7.b(ad5, "deviceSettingFactory");
        ee7.b(watchFaceRepository, "watchFaceRepository");
        ee7.b(watchLocalizationRepository, "watchLocalizationRepository");
        ee7.b(ringStyleRepository, "ringStyleRepository");
        ee7.b(fp4, "socialProfileRepository");
        ee7.b(xo4, "socialFriendRepository");
        ee7.b(ro4, "challengeRepository");
        ee7.b(fileRepository, "fileRepository");
        ee7.b(to4, "fcmRepository");
        ee7.b(workoutSettingRepository, "workoutSettingRepository");
        ee7.b(lm4, "flagRepository");
        ee7.b(uj5, "buddyChallengeManager");
        ee7.b(themeRepository, "themeRepository");
        return new ApplicationEventListener(portfolioApp, ch5, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, ringStyleRepository, deviceRepository, userRepository, ad5, alarmsRepository, watchFaceRepository, watchLocalizationRepository, fp4, xo4, ro4, fileRepository, to4, workoutSettingRepository, lm4, uj5, themeRepository);
    }

    @DexIgnore
    public final ff5 a(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        ee7.b(deviceRepository, "deviceRepository");
        ee7.b(portfolioApp, "app");
        return new ff5(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final qd5 a() {
        return qd5.f.c();
    }

    @DexIgnore
    public final pd5 a(ch5 ch5, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        ee7.b(ch5, "sharedPreferencesManager");
        ee7.b(userRepository, "userRepository");
        ee7.b(alarmsRepository, "alarmsRepository");
        return new pd5(ch5, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final ie5 a(Context context, pj4 pj4, ch5 ch5) {
        ee7.b(context, "context");
        ee7.b(pj4, "appExecutors");
        ee7.b(ch5, "sharedPreferencesManager");
        return new ie5(context, pj4.b(), ch5);
    }

    @DexIgnore
    public final <S> S a(hj5 hj5, lj5 lj5, Class<S> cls) {
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 0) + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(lj5);
        kj5.g.a(hj5);
        return (S) kj5.g.a(cls);
    }

    @DexIgnore
    public final ApiServiceV2 a(hj5 hj5, lj5 lj5) {
        ee7.b(hj5, "interceptor");
        ee7.b(lj5, "authenticator");
        kj5 kj5 = kj5.g;
        kj5.a(rw6.b.a(this.a, 0, "2") + "/");
        kj5 kj52 = kj5.g;
        File cacheDir = this.a.getCacheDir();
        ee7.a((Object) cacheDir, "mApplication.cacheDir");
        kj52.a(cacheDir);
        kj5.g.a(lj5);
        kj5.g.a(hj5);
        return (ApiServiceV2) kj5.g.a(ApiServiceV2.class);
    }

    @DexIgnore
    public final ll4 a(ch5 ch5, UserRepository userRepository, is6 is6, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, ad5 ad5, lm4 lm4) {
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(is6, "getHybridUseCase");
        ee7.b(notificationsRepository, "mNotificationRepository");
        ee7.b(portfolioApp, "mApp");
        ee7.b(goalTrackingRepository, "goalTrackingRepo");
        ee7.b(deviceDao, "deviceDao");
        ee7.b(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        ee7.b(microAppLastSettingRepository, "microAppLastSettingRepository");
        ee7.b(ad5, "deviceSettingFactory");
        ee7.b(lm4, "flagRepository");
        return new ll4(ch5, userRepository, notificationsRepository, is6, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, ad5, lm4);
    }

    @DexIgnore
    public final ph5 a(ch5 ch5, ll4 ll4) {
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(ll4, "migrationManager");
        return new ph5(ch5, ll4);
    }

    @DexIgnore
    public final vg5 a(HybridPresetRepository hybridPresetRepository, uj5 uj5, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, ch5 ch5, kw6 kw6, ek5 ek5, wk5 wk5) {
        ee7.b(hybridPresetRepository, "hybridPresetRepository");
        ee7.b(uj5, "buddyChallengeManager");
        ee7.b(quickResponseRepository, "quickResponseRepository");
        ee7.b(alarmsRepository, "alarmsRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        ee7.b(kw6, "setNotificationUseCase");
        ee7.b(ek5, "musicControlComponent");
        ee7.b(wk5, "mWorkoutTetherGpsManager");
        return new vg5(hybridPresetRepository, alarmsRepository, uj5, wk5, quickResponseRepository, ek5, kw6, ch5);
    }

    @DexIgnore
    public final co6 a(InAppNotificationRepository inAppNotificationRepository) {
        ee7.b(inAppNotificationRepository, "repository");
        return new co6(inAppNotificationRepository);
    }
}
