package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ys1 a;
    @DexIgnore
    public /* final */ /* synthetic */ v81 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bp1(ys1 ys1, v81 v81) {
        super(1);
        this.a = ys1;
        this.b = v81;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        v81 v812 = this.b;
        if ((v812 instanceof nw0) || v812.v.c == ay0.s) {
            this.a.a.a(this.b.v);
        } else {
            this.a.b.invoke();
        }
        return i97.a;
    }
}
