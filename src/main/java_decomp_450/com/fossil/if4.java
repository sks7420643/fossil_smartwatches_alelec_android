package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if4 extends JsonReader {
    @DexIgnore
    public static /* final */ Object A; // = new Object();
    @DexIgnore
    public static /* final */ Reader z; // = new a();
    @DexIgnore
    public Object[] v; // = new Object[32];
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public String[] x; // = new String[32];
    @DexIgnore
    public int[] y; // = new int[32];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Reader {
        @DexIgnore
        @Override // java.io.Closeable, java.io.Reader, java.lang.AutoCloseable
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public if4(JsonElement jsonElement) {
        super(z);
        a(jsonElement);
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    private String q() {
        return " at path " + getPath();
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String A() throws IOException {
        pf4 peek = peek();
        if (peek == pf4.STRING || peek == pf4.NUMBER) {
            String f = ((le4) J()).f();
            int i = this.w;
            if (i > 0) {
                int[] iArr = this.y;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return f;
        }
        throw new IllegalStateException("Expected " + pf4.STRING + " but was " + peek + q());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void H() throws IOException {
        if (peek() == pf4.NAME) {
            y();
            this.x[this.w - 2] = "null";
        } else {
            J();
            int i = this.w;
            if (i > 0) {
                this.x[i - 1] = "null";
            }
        }
        int i2 = this.w;
        if (i2 > 0) {
            int[] iArr = this.y;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    @DexIgnore
    public final Object I() {
        return this.v[this.w - 1];
    }

    @DexIgnore
    public final Object J() {
        Object[] objArr = this.v;
        int i = this.w - 1;
        this.w = i;
        Object obj = objArr[i];
        objArr[i] = null;
        return obj;
    }

    @DexIgnore
    public void K() throws IOException {
        a(pf4.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) I()).next();
        a(entry.getValue());
        a(new le4((String) entry.getKey()));
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void a() throws IOException {
        a(pf4.BEGIN_ARRAY);
        a(((de4) I()).iterator());
        this.y[this.w - 1] = 0;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void b() throws IOException {
        a(pf4.BEGIN_OBJECT);
        a(((ie4) I()).entrySet().iterator());
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable, com.google.gson.stream.JsonReader
    public void close() throws IOException {
        this.v = new Object[]{A};
        this.w = 1;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String getPath() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.w) {
            Object[] objArr = this.v;
            if (objArr[i] instanceof de4) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.y[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof ie4) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.x;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void k() throws IOException {
        a(pf4.END_ARRAY);
        J();
        J();
        int i = this.w;
        if (i > 0) {
            int[] iArr = this.y;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void l() throws IOException {
        a(pf4.END_OBJECT);
        J();
        J();
        int i = this.w;
        if (i > 0) {
            int[] iArr = this.y;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public boolean n() throws IOException {
        pf4 peek = peek();
        return (peek == pf4.END_OBJECT || peek == pf4.END_ARRAY) ? false : true;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public pf4 peek() throws IOException {
        if (this.w == 0) {
            return pf4.END_DOCUMENT;
        }
        Object I = I();
        if (I instanceof Iterator) {
            boolean z2 = this.v[this.w - 2] instanceof ie4;
            Iterator it = (Iterator) I;
            if (!it.hasNext()) {
                return z2 ? pf4.END_OBJECT : pf4.END_ARRAY;
            }
            if (z2) {
                return pf4.NAME;
            }
            a(it.next());
            return peek();
        } else if (I instanceof ie4) {
            return pf4.BEGIN_OBJECT;
        } else {
            if (I instanceof de4) {
                return pf4.BEGIN_ARRAY;
            }
            if (I instanceof le4) {
                le4 le4 = (le4) I;
                if (le4.q()) {
                    return pf4.STRING;
                }
                if (le4.o()) {
                    return pf4.BOOLEAN;
                }
                if (le4.p()) {
                    return pf4.NUMBER;
                }
                throw new AssertionError();
            } else if (I instanceof he4) {
                return pf4.NULL;
            } else {
                if (I == A) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public boolean r() throws IOException {
        a(pf4.BOOLEAN);
        boolean a2 = ((le4) J()).a();
        int i = this.w;
        if (i > 0) {
            int[] iArr = this.y;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return a2;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String toString() {
        return if4.class.getSimpleName();
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public double v() throws IOException {
        pf4 peek = peek();
        if (peek == pf4.NUMBER || peek == pf4.STRING) {
            double l = ((le4) I()).l();
            if (o() || (!Double.isNaN(l) && !Double.isInfinite(l))) {
                J();
                int i = this.w;
                if (i > 0) {
                    int[] iArr = this.y;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return l;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + l);
        }
        throw new IllegalStateException("Expected " + pf4.NUMBER + " but was " + peek + q());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public int w() throws IOException {
        pf4 peek = peek();
        if (peek == pf4.NUMBER || peek == pf4.STRING) {
            int b = ((le4) I()).b();
            J();
            int i = this.w;
            if (i > 0) {
                int[] iArr = this.y;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return b;
        }
        throw new IllegalStateException("Expected " + pf4.NUMBER + " but was " + peek + q());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public long x() throws IOException {
        pf4 peek = peek();
        if (peek == pf4.NUMBER || peek == pf4.STRING) {
            long m = ((le4) I()).m();
            J();
            int i = this.w;
            if (i > 0) {
                int[] iArr = this.y;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return m;
        }
        throw new IllegalStateException("Expected " + pf4.NUMBER + " but was " + peek + q());
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public String y() throws IOException {
        a(pf4.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) I()).next();
        String str = (String) entry.getKey();
        this.x[this.w - 1] = str;
        a(entry.getValue());
        return str;
    }

    @DexIgnore
    @Override // com.google.gson.stream.JsonReader
    public void z() throws IOException {
        a(pf4.NULL);
        J();
        int i = this.w;
        if (i > 0) {
            int[] iArr = this.y;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public final void a(pf4 pf4) throws IOException {
        if (peek() != pf4) {
            throw new IllegalStateException("Expected " + pf4 + " but was " + peek() + q());
        }
    }

    @DexIgnore
    public final void a(Object obj) {
        int i = this.w;
        Object[] objArr = this.v;
        if (i == objArr.length) {
            Object[] objArr2 = new Object[(i * 2)];
            int[] iArr = new int[(i * 2)];
            String[] strArr = new String[(i * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.y, 0, iArr, 0, this.w);
            System.arraycopy(this.x, 0, strArr, 0, this.w);
            this.v = objArr2;
            this.y = iArr;
            this.x = strArr;
        }
        Object[] objArr3 = this.v;
        int i2 = this.w;
        this.w = i2 + 1;
        objArr3[i2] = obj;
    }
}
