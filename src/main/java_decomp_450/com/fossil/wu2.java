package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wu2 extends ev2 {
    @DexIgnore
    public /* final */ int zzc;
    @DexIgnore
    public /* final */ int zzd;

    @DexIgnore
    public wu2(byte[] bArr, int i, int i2) {
        super(bArr);
        tu2.zzb(i, i + i2, bArr.length);
        this.zzc = i;
        this.zzd = i2;
    }

    @DexIgnore
    @Override // com.fossil.ev2, com.fossil.tu2
    public final byte zza(int i) {
        int zza = zza();
        if (((zza - (i + 1)) | i) >= 0) {
            return ((ev2) this).zzb[this.zzc + i];
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(zza);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    @DexIgnore
    @Override // com.fossil.ev2, com.fossil.tu2
    public final byte zzb(int i) {
        return ((ev2) this).zzb[this.zzc + i];
    }

    @DexIgnore
    @Override // com.fossil.ev2
    public final int zze() {
        return this.zzc;
    }

    @DexIgnore
    @Override // com.fossil.ev2, com.fossil.tu2
    public final int zza() {
        return this.zzd;
    }
}
