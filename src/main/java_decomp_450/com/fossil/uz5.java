package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz5 {
    @DexIgnore
    public /* final */ rz5 a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public uz5(rz5 rz5, String str, int i, String str2, String str3) {
        ee7.b(rz5, "mView");
        ee7.b(str, "mPresetId");
        this.a = rz5;
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final rz5 b() {
        return this.a;
    }
}
