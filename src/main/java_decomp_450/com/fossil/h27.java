package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h27 {
    @DexIgnore
    public Context a; // = null;

    @DexIgnore
    public h27(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void a(e27 e27) {
        if (e27 != null) {
            String e272 = e27.toString();
            if (a()) {
                a(j27.d(e272));
            }
        }
    }

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public final e27 c() {
        String c = a() ? j27.c(b()) : null;
        if (c != null) {
            return e27.a(c);
        }
        return null;
    }
}
