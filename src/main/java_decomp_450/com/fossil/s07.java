package com.fossil;

import android.content.Context;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s07 extends i17 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public s07(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean a(g17 g17) {
        return "content".equals(g17.d.getScheme());
    }

    @DexIgnore
    public InputStream c(g17 g17) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(g17.d);
    }

    @DexIgnore
    @Override // com.fossil.i17
    public i17.a a(g17 g17, int i) throws IOException {
        return new i17.a(c(g17), Picasso.LoadedFrom.DISK);
    }
}
