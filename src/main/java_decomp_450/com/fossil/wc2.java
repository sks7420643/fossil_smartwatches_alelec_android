package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc2 implements Parcelable.Creator<DataSet> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        gc2 gc2 = null;
        ArrayList arrayList2 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                gc2 = (gc2) j72.a(parcel, a, gc2.CREATOR);
            } else if (a2 == 1000) {
                i = j72.q(parcel, a);
            } else if (a2 == 3) {
                j72.a(parcel, a, arrayList, wc2.class.getClassLoader());
            } else if (a2 == 4) {
                arrayList2 = j72.c(parcel, a, gc2.CREATOR);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                z = j72.i(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new DataSet(i, gc2, arrayList, arrayList2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataSet[] newArray(int i) {
        return new DataSet[i];
    }
}
