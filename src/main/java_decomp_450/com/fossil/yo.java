package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo implements xo {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ ji b;
    @DexIgnore
    public /* final */ ji c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<wo> {
        @DexIgnore
        public a(yo yoVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, wo woVar) {
            String str = woVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            byte[] a = cm.a(woVar.b);
            if (a == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindBlob(2, a);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(yo yoVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE from WorkProgress where work_spec_id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(yo yoVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM WorkProgress";
        }
    }

    @DexIgnore
    public yo(ci ciVar) {
        this.a = ciVar;
        new a(this, ciVar);
        this.b = new b(this, ciVar);
        this.c = new c(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.xo
    public void a(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.b.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.b.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.xo
    public void deleteAll() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.c.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }
}
