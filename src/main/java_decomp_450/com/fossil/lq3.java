package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lq3 extends IInterface {
    @DexIgnore
    void a(aq3 aq3) throws RemoteException;

    @DexIgnore
    void a(eq3 eq3) throws RemoteException;

    @DexIgnore
    void a(nq3 nq3) throws RemoteException;

    @DexIgnore
    void a(pq3 pq3) throws RemoteException;

    @DexIgnore
    void a(sq3 sq3) throws RemoteException;

    @DexIgnore
    void a(uq3 uq3) throws RemoteException;

    @DexIgnore
    void a(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void b(pq3 pq3) throws RemoteException;

    @DexIgnore
    void b(List<pq3> list) throws RemoteException;
}
