package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.wh;
import com.fossil.xh;
import com.fossil.zh;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ai {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ zh d;
    @DexIgnore
    public /* final */ zh.c e;
    @DexIgnore
    public xh f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ wh h; // = new a();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new b();
    @DexIgnore
    public /* final */ Runnable k; // = new c();
    @DexIgnore
    public /* final */ Runnable l; // = new d();
    @DexIgnore
    public /* final */ Runnable m; // = new e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends wh.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ai$a$a")
        /* renamed from: com.fossil.ai$a$a  reason: collision with other inner class name */
        public class RunnableC0011a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] a;

            @DexIgnore
            public RunnableC0011a(String[] strArr) {
                this.a = strArr;
            }

            @DexIgnore
            public void run() {
                ai.this.d.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.wh
        public void a(String[] strArr) {
            ai.this.g.execute(new RunnableC0011a(strArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ai.this.f = xh.a.a(iBinder);
            ai aiVar = ai.this;
            aiVar.g.execute(aiVar.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            ai aiVar = ai.this;
            aiVar.g.execute(aiVar.l);
            ai.this.f = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                xh xhVar = ai.this.f;
                if (xhVar != null) {
                    ai.this.c = xhVar.a(ai.this.h, ai.this.b);
                    ai.this.d.a(ai.this.e);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            ai aiVar = ai.this;
            aiVar.d.c(aiVar.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            ai aiVar = ai.this;
            aiVar.d.c(aiVar.e);
            try {
                xh xhVar = ai.this.f;
                if (xhVar != null) {
                    xhVar.a(ai.this.h, ai.this.c);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
            }
            ai aiVar2 = ai.this;
            aiVar2.a.unbindService(aiVar2.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends zh.c {
        @DexIgnore
        public f(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            if (!ai.this.i.get()) {
                try {
                    xh xhVar = ai.this.f;
                    if (xhVar != null) {
                        xhVar.a(ai.this.c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public ai(Context context, String str, zh zhVar, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = zhVar;
        this.g = executor;
        this.e = new f((String[]) zhVar.a.keySet().toArray(new String[0]));
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
