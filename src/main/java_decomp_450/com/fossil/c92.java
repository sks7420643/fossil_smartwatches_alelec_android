package com.fossil;

import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c92 {
    @DexIgnore
    public static a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        ScheduledExecutorService a();
    }

    @DexIgnore
    public static synchronized a a() {
        a aVar;
        synchronized (c92.class) {
            if (a == null) {
                a = new d92();
            }
            aVar = a;
        }
        return aVar;
    }
}
