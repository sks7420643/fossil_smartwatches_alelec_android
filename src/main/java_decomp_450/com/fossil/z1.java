package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.v1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z1 extends t1 implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, v1, View.OnKeyListener {
    @DexIgnore
    public static /* final */ int A; // = e0.abc_popup_menu_item_layout;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ p1 c;
    @DexIgnore
    public /* final */ o1 d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ v2 i;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener j; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener p; // = new b();
    @DexIgnore
    public PopupWindow.OnDismissListener q;
    @DexIgnore
    public View r;
    @DexIgnore
    public View s;
    @DexIgnore
    public v1.a t;
    @DexIgnore
    public ViewTreeObserver u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y; // = 0;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (z1.this.a() && !z1.this.i.l()) {
                View view = z1.this.s;
                if (view == null || !view.isShown()) {
                    z1.this.dismiss();
                } else {
                    z1.this.i.show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = z1.this.u;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    z1.this.u = view.getViewTreeObserver();
                }
                z1 z1Var = z1.this;
                z1Var.u.removeGlobalOnLayoutListener(z1Var.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore
    public z1(Context context, p1 p1Var, View view, int i2, int i3, boolean z2) {
        this.b = context;
        this.c = p1Var;
        this.e = z2;
        this.d = new o1(p1Var, LayoutInflater.from(context), this.e, A);
        this.g = i2;
        this.h = i3;
        Resources resources = context.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(b0.abc_config_prefDialogWidth));
        this.r = view;
        this.i = new v2(this.b, null, this.g, this.h);
        p1Var.a(this, context);
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(int i2) {
        this.y = i2;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(p1 p1Var) {
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void b(boolean z2) {
        this.d.a(z2);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void c(int i2) {
        this.i.b(i2);
    }

    @DexIgnore
    @Override // com.fossil.y1
    public void dismiss() {
        if (a()) {
            this.i.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.y1
    public ListView e() {
        return this.i.e();
    }

    @DexIgnore
    public final boolean g() {
        View view;
        if (a()) {
            return true;
        }
        if (this.v || (view = this.r) == null) {
            return false;
        }
        this.s = view;
        this.i.a((PopupWindow.OnDismissListener) this);
        this.i.a((AdapterView.OnItemClickListener) this);
        this.i.a(true);
        View view2 = this.s;
        boolean z2 = this.u == null;
        ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
        this.u = viewTreeObserver;
        if (z2) {
            viewTreeObserver.addOnGlobalLayoutListener(this.j);
        }
        view2.addOnAttachStateChangeListener(this.p);
        this.i.a(view2);
        this.i.f(this.y);
        if (!this.w) {
            this.x = t1.a(this.d, null, this.b, this.f);
            this.w = true;
        }
        this.i.e(this.x);
        this.i.g(2);
        this.i.a(f());
        this.i.show();
        ListView e2 = this.i.e();
        e2.setOnKeyListener(this);
        if (this.z && this.c.h() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.b).inflate(e0.abc_popup_menu_header_item_layout, (ViewGroup) e2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.c.h());
            }
            frameLayout.setEnabled(false);
            e2.addHeaderView(frameLayout, null, false);
        }
        this.i.a((ListAdapter) this.d);
        this.i.show();
        return true;
    }

    @DexIgnore
    public void onDismiss() {
        this.v = true;
        this.c.close();
        ViewTreeObserver viewTreeObserver = this.u;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.u = this.s.getViewTreeObserver();
            }
            this.u.removeGlobalOnLayoutListener(this.j);
            this.u = null;
        }
        this.s.removeOnAttachStateChangeListener(this.p);
        PopupWindow.OnDismissListener onDismissListener = this.q;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.y1
    public void show() {
        if (!g()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    @DexIgnore
    @Override // com.fossil.y1
    public boolean a() {
        return !this.v && this.i.a();
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void b(int i2) {
        this.i.a(i2);
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void c(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z2) {
        this.w = false;
        o1 o1Var = this.d;
        if (o1Var != null) {
            o1Var.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
        this.t = aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(a2 a2Var) {
        if (a2Var.hasVisibleItems()) {
            u1 u1Var = new u1(this.b, a2Var, this.s, this.e, this.g, this.h);
            u1Var.a(this.t);
            u1Var.a(t1.b(a2Var));
            u1Var.a(this.q);
            this.q = null;
            this.c.a(false);
            int b2 = this.i.b();
            int f2 = this.i.f();
            if ((Gravity.getAbsoluteGravity(this.y, da.p(this.r)) & 7) == 5) {
                b2 += this.r.getWidth();
            }
            if (u1Var.a(b2, f2)) {
                v1.a aVar = this.t;
                if (aVar == null) {
                    return true;
                }
                aVar.a(a2Var);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z2) {
        if (p1Var == this.c) {
            dismiss();
            v1.a aVar = this.t;
            if (aVar != null) {
                aVar.a(p1Var, z2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(View view) {
        this.r = view;
    }

    @DexIgnore
    @Override // com.fossil.t1
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.q = onDismissListener;
    }
}
