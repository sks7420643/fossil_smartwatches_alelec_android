package com.fossil;

import java.util.concurrent.ScheduledFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class lb4 implements ho3 {
    @DexIgnore
    public /* final */ ScheduledFuture a;

    @DexIgnore
    public lb4(ScheduledFuture scheduledFuture) {
        this.a = scheduledFuture;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3 no3) {
        this.a.cancel(false);
    }
}
