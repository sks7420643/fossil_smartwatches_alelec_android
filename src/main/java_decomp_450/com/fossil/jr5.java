package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.util.Log;
import android.util.Pair;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr5 {
    @DexIgnore
    public static /* final */ Rect a; // = new Rect();
    @DexIgnore
    public static /* final */ RectF b; // = new RectF();
    @DexIgnore
    public static /* final */ RectF c; // = new RectF();
    @DexIgnore
    public static /* final */ float[] d; // = new float[6];
    @DexIgnore
    public static /* final */ float[] e; // = new float[6];
    @DexIgnore
    public static int f;
    @DexIgnore
    public static Pair<String, WeakReference<Bitmap>> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore
    public static b a(Bitmap bitmap, Context context, Uri uri) {
        int i = 0;
        try {
            if (!(uri.getPath() == null || uri.getScheme() == null)) {
                if (uri.getScheme().equals("file")) {
                    i = new ub(uri.getPath()).c();
                } else {
                    Cursor query = context.getContentResolver().query(uri, new String[]{"orientation"}, null, null, null);
                    if (query != null) {
                        if (query.moveToFirst()) {
                            i = query.getInt(0);
                        }
                        query.close();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return new b(bitmap, i);
    }

    @DexIgnore
    public static float b(float[] fArr) {
        return (f(fArr) + e(fArr)) / 2.0f;
    }

    @DexIgnore
    public static float c(float[] fArr) {
        return (a(fArr) + g(fArr)) / 2.0f;
    }

    @DexIgnore
    public static float d(float[] fArr) {
        return a(fArr) - g(fArr);
    }

    @DexIgnore
    public static float e(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }

    @DexIgnore
    public static float f(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }

    @DexIgnore
    public static float g(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static float h(float[] fArr) {
        return f(fArr) - e(fArr);
    }

    @DexIgnore
    public static b a(Bitmap bitmap, ub ubVar) {
        int a2 = ubVar.a("Orientation", 1);
        return new b(bitmap, a2 != 3 ? a2 != 6 ? a2 != 8 ? 0 : 270 : 90 : 180);
    }

    @DexIgnore
    public static a a(Context context, Uri uri, int i, int i2) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            BitmapFactory.Options a2 = a(contentResolver, uri);
            if (a2.outWidth == -1) {
                if (a2.outHeight == -1) {
                    throw new RuntimeException("File is not a picture");
                }
            }
            a2.inSampleSize = Math.max(a(a2.outWidth, a2.outHeight, i, i2), a(a2.outWidth, a2.outHeight));
            return new a(a(contentResolver, uri, a2), a2.inSampleSize);
        } catch (Exception e2) {
            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e2.getMessage(), e2);
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        Bitmap bitmap2;
        try {
            bitmap2 = Bitmap.createScaledBitmap(bitmap, i, i2, false);
            try {
                return a(bitmap);
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            bitmap2 = null;
            e.printStackTrace();
            return bitmap2;
        }
    }

    @DexIgnore
    public static a a(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
        int i4 = 1;
        do {
            try {
                return new a(a(bitmap, fArr, i, z, i2, i3, 1.0f / ((float) i4), z2, z3), i4);
            } catch (OutOfMemoryError e2) {
                i4 *= 2;
                if (i4 > 8) {
                    throw e2;
                }
            }
        } while (i4 > 8);
        throw e2;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, float f2, boolean z2, boolean z3) {
        float f3 = f2;
        Rect a2 = a(fArr, bitmap.getWidth(), bitmap.getHeight(), z, i2, i3);
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i, (float) (bitmap.getWidth() / 2), (float) (bitmap.getHeight() / 2));
        float f4 = z2 ? -f3 : f3;
        if (z3) {
            f3 = -f3;
        }
        matrix.postScale(f4, f3);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, a2.left, a2.top, a2.width(), a2.height(), matrix, true);
        if (createBitmap == bitmap) {
            createBitmap = bitmap.copy(bitmap.getConfig(), false);
        }
        return i % 90 != 0 ? a(createBitmap, fArr, a2, i, z, i2, i3) : createBitmap;
    }

    @DexIgnore
    public static a a(Context context, Uri uri, float[] fArr, int i, int i2, int i3, boolean z, int i4, int i5, int i6, int i7, boolean z2, boolean z3) {
        int i8 = 1;
        do {
            try {
                return a(context, uri, fArr, i, i2, i3, z, i4, i5, i6, i7, z2, z3, i8);
            } catch (OutOfMemoryError e2) {
                i8 *= 2;
                if (i8 > 16) {
                    throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + e2.getMessage(), e2);
                }
            }
        } while (i8 > 16);
        throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + e2.getMessage(), e2);
    }

    @DexIgnore
    public static float a(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static Rect a(float[] fArr, int i, int i2, boolean z, int i3, int i4) {
        Rect rect = new Rect(Math.round(Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, e(fArr))), Math.round(Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, g(fArr))), Math.round(Math.min((float) i, f(fArr))), Math.round(Math.min((float) i2, a(fArr))));
        if (z) {
            a(rect, i3, i4);
        }
        return rect;
    }

    @DexIgnore
    public static void a(Rect rect, int i, int i2) {
        if (i == i2 && rect.width() != rect.height()) {
            if (rect.height() > rect.width()) {
                rect.bottom -= rect.height() - rect.width();
            } else {
                rect.right -= rect.width() - rect.height();
            }
        }
    }

    @DexIgnore
    public static Uri a(Context context, Bitmap bitmap, Uri uri) {
        boolean z = true;
        if (uri == null) {
            try {
                uri = Uri.fromFile(File.createTempFile("aic_state_store_temp", ".jpg", context.getCacheDir()));
            } catch (Exception e2) {
                Log.w("AIC", "Failed to write bitmap to temp file for image-cropper save instance state", e2);
                return null;
            }
        } else if (new File(uri.getPath()).exists()) {
            z = false;
        }
        if (z) {
            a(context, bitmap, uri, Bitmap.CompressFormat.JPEG, 95);
        }
        return uri;
    }

    @DexIgnore
    public static void a(Context context, Bitmap bitmap, Uri uri, Bitmap.CompressFormat compressFormat, int i) throws FileNotFoundException {
        OutputStream outputStream = null;
        try {
            outputStream = context.getContentResolver().openOutputStream(uri);
            bitmap.compress(compressFormat, i, outputStream);
        } finally {
            a(outputStream);
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, int i2, CropImageView.j jVar) {
        if (i > 0 && i2 > 0) {
            try {
                if (jVar == CropImageView.j.RESIZE_FIT || jVar == CropImageView.j.RESIZE_INSIDE || jVar == CropImageView.j.RESIZE_EXACT) {
                    Bitmap bitmap2 = null;
                    if (jVar == CropImageView.j.RESIZE_EXACT) {
                        bitmap2 = Bitmap.createScaledBitmap(bitmap, i, i2, false);
                    } else {
                        float width = (float) bitmap.getWidth();
                        float height = (float) bitmap.getHeight();
                        float max = Math.max(width / ((float) i), height / ((float) i2));
                        if (max > 1.0f || jVar == CropImageView.j.RESIZE_FIT) {
                            bitmap2 = Bitmap.createScaledBitmap(bitmap, (int) (width / max), (int) (height / max), false);
                        }
                    }
                    if (bitmap2 != null) {
                        if (bitmap2 != bitmap) {
                            bitmap.recycle();
                        }
                        return bitmap2;
                    }
                }
            } catch (Exception e2) {
                Log.w("AIC", "Failed to resize cropped image, return bitmap before resize", e2);
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.jr5.a a(android.content.Context r14, android.net.Uri r15, float[] r16, int r17, int r18, int r19, boolean r20, int r21, int r22, int r23, int r24, boolean r25, boolean r26, int r27) {
        /*
            r0 = r17
            r1 = r16
            r2 = r18
            r3 = r19
            r4 = r20
            r5 = r21
            r6 = r22
            android.graphics.Rect r9 = a(r1, r2, r3, r4, r5, r6)
            if (r23 <= 0) goto L_0x0017
            r10 = r23
            goto L_0x001c
        L_0x0017:
            int r1 = r9.width()
            r10 = r1
        L_0x001c:
            if (r24 <= 0) goto L_0x0021
            r11 = r24
            goto L_0x0026
        L_0x0021:
            int r1 = r9.height()
            r11 = r1
        L_0x0026:
            r1 = 0
            r8 = 1
            r2 = r14
            r3 = r15
            r4 = r9
            r5 = r10
            r6 = r11
            r7 = r27
            com.fossil.jr5$a r2 = a(r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0038 }
            android.graphics.Bitmap r1 = r2.a     // Catch:{ Exception -> 0x0038 }
            int r8 = r2.b     // Catch:{ Exception -> 0x0038 }
            goto L_0x0039
        L_0x0038:
        L_0x0039:
            if (r1 == 0) goto L_0x0067
            r12 = r25
            r13 = r26
            android.graphics.Bitmap r10 = a(r1, r0, r12, r13)     // Catch:{ OutOfMemoryError -> 0x0060 }
            int r1 = r0 % 90
            if (r1 == 0) goto L_0x0057
            r1 = r10
            r2 = r16
            r3 = r9
            r4 = r17
            r5 = r20
            r6 = r21
            r7 = r22
            android.graphics.Bitmap r10 = a(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ OutOfMemoryError -> 0x005d }
        L_0x0057:
            com.fossil.jr5$a r0 = new com.fossil.jr5$a
            r0.<init>(r10, r8)
            return r0
        L_0x005d:
            r0 = move-exception
            r1 = r10
            goto L_0x0061
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.recycle()
        L_0x0066:
            throw r0
        L_0x0067:
            r12 = r25
            r13 = r26
            r1 = r14
            r2 = r15
            r3 = r16
            r4 = r17
            r5 = r20
            r6 = r21
            r7 = r22
            r8 = r27
            com.fossil.jr5$a r0 = a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jr5.a(android.content.Context, android.net.Uri, float[], int, int, int, boolean, int, int, int, int, boolean, boolean, int):com.fossil.jr5$a");
    }

    @DexIgnore
    public static a a(Context context, Uri uri, float[] fArr, int i, boolean z, int i2, int i3, int i4, Rect rect, int i5, int i6, boolean z2, boolean z3) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            int a2 = a(rect.width(), rect.height(), i5, i6) * i4;
            options.inSampleSize = a2;
            Bitmap a3 = a(context.getContentResolver(), uri, options);
            if (a3 != null) {
                try {
                    int length = fArr.length;
                    float[] fArr2 = new float[length];
                    System.arraycopy(fArr, 0, fArr2, 0, fArr.length);
                    for (int i7 = 0; i7 < length; i7++) {
                        fArr2[i7] = fArr2[i7] / ((float) options.inSampleSize);
                    }
                    bitmap = a(a3, fArr2, i, z, i2, i3, 1.0f, z2, z3);
                    if (bitmap != a3) {
                        a3.recycle();
                    }
                } catch (Throwable th) {
                    if (a3 != null) {
                        a3.recycle();
                    }
                    throw th;
                }
            }
            return new a(bitmap, a2);
        } catch (OutOfMemoryError e2) {
            if (0 != 0) {
                bitmap.recycle();
            }
            throw e2;
        } catch (Exception e3) {
            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e3.getMessage(), e3);
        }
    }

    @DexIgnore
    public static BitmapFactory.Options a(ContentResolver contentResolver, Uri uri) throws FileNotFoundException {
        InputStream inputStream;
        try {
            inputStream = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, a, options);
                options.inJustDecodeBounds = false;
                a(inputStream);
                return options;
            } catch (Throwable th) {
                th = th;
                a(inputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            a(inputStream);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:? */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:1:0x0001 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: android.content.ContentResolver */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [android.content.ContentResolver] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.graphics.Bitmap] */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000e, code lost:
        return r2;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0011 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(android.content.ContentResolver r2, android.net.Uri r3, android.graphics.BitmapFactory.Options r4) throws java.io.FileNotFoundException {
        /*
        L_0x0000:
            r0 = 0
            java.io.InputStream r0 = r2.openInputStream(r3)     // Catch:{ OutOfMemoryError -> 0x0011 }
            android.graphics.Rect r1 = com.fossil.jr5.a     // Catch:{ OutOfMemoryError -> 0x0011 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r0, r1, r4)     // Catch:{ OutOfMemoryError -> 0x0011 }
            a(r0)
            return r2
        L_0x000f:
            r2 = move-exception
            goto L_0x0038
        L_0x0011:
            int r1 = r4.inSampleSize     // Catch:{ all -> 0x000f }
            int r1 = r1 * 2
            r4.inSampleSize = r1     // Catch:{ all -> 0x000f }
            a(r0)
            int r0 = r4.inSampleSize
            r1 = 512(0x200, float:7.175E-43)
            if (r0 > r1) goto L_0x0021
            goto L_0x0000
        L_0x0021:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "Failed to decode image: "
            r4.append(r0)
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r2.<init>(r3)
            throw r2
        L_0x0038:
            a(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jr5.a(android.content.ContentResolver, android.net.Uri, android.graphics.BitmapFactory$Options):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0033, code lost:
        r7.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        return r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002e, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        if (r7 == null) goto L_0x0036;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x003b */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.jr5.a a(android.content.Context r4, android.net.Uri r5, android.graphics.Rect r6, int r7, int r8, int r9) {
        /*
            r0 = 0
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            r1.<init>()     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            int r2 = r6.width()     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            int r3 = r6.height()     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            int r7 = a(r2, r3, r7, r8)     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            int r9 = r9 * r7
            r1.inSampleSize = r9     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            java.io.InputStream r4 = r4.openInputStream(r5)     // Catch:{ Exception -> 0x005f, all -> 0x005c }
            r7 = 0
            android.graphics.BitmapRegionDecoder r7 = android.graphics.BitmapRegionDecoder.newInstance(r4, r7)     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
        L_0x0023:
            com.fossil.jr5$a r8 = new com.fossil.jr5$a     // Catch:{ OutOfMemoryError -> 0x003b }
            android.graphics.Bitmap r9 = r7.decodeRegion(r6, r1)     // Catch:{ OutOfMemoryError -> 0x003b }
            int r2 = r1.inSampleSize     // Catch:{ OutOfMemoryError -> 0x003b }
            r8.<init>(r9, r2)     // Catch:{ OutOfMemoryError -> 0x003b }
            a(r4)
            if (r7 == 0) goto L_0x0036
            r7.recycle()
        L_0x0036:
            return r8
        L_0x0037:
            r5 = move-exception
            goto L_0x0056
        L_0x0039:
            r6 = move-exception
            goto L_0x005a
        L_0x003b:
            int r8 = r1.inSampleSize     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            int r8 = r8 * 2
            r1.inSampleSize = r8     // Catch:{ Exception -> 0x0039, all -> 0x0037 }
            r9 = 512(0x200, float:7.175E-43)
            if (r8 <= r9) goto L_0x0023
            a(r4)
            if (r7 == 0) goto L_0x004d
            r7.recycle()
        L_0x004d:
            com.fossil.jr5$a r4 = new com.fossil.jr5$a
            r5 = 1
            r4.<init>(r0, r5)
            return r4
        L_0x0054:
            r5 = move-exception
            r7 = r0
        L_0x0056:
            r0 = r4
            goto L_0x0085
        L_0x0058:
            r6 = move-exception
            r7 = r0
        L_0x005a:
            r0 = r4
            goto L_0x0061
        L_0x005c:
            r5 = move-exception
            r7 = r0
            goto L_0x0085
        L_0x005f:
            r6 = move-exception
            r7 = r0
        L_0x0061:
            java.lang.RuntimeException r4 = new java.lang.RuntimeException     // Catch:{ all -> 0x0084 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0084 }
            r8.<init>()     // Catch:{ all -> 0x0084 }
            java.lang.String r9 = "Failed to load sampled bitmap: "
            r8.append(r9)     // Catch:{ all -> 0x0084 }
            r8.append(r5)     // Catch:{ all -> 0x0084 }
            java.lang.String r5 = "\r\n"
            r8.append(r5)     // Catch:{ all -> 0x0084 }
            java.lang.String r5 = r6.getMessage()     // Catch:{ all -> 0x0084 }
            r8.append(r5)     // Catch:{ all -> 0x0084 }
            java.lang.String r5 = r8.toString()     // Catch:{ all -> 0x0084 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x0084 }
            throw r4     // Catch:{ all -> 0x0084 }
        L_0x0084:
            r5 = move-exception
        L_0x0085:
            a(r0)
            if (r7 == 0) goto L_0x008d
            r7.recycle()
        L_0x008d:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jr5.a(android.content.Context, android.net.Uri, android.graphics.Rect, int, int, int):com.fossil.jr5$a");
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, float[] fArr, Rect rect, int i, boolean z, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        if (i % 90 == 0) {
            return bitmap;
        }
        double radians = Math.toRadians((double) i);
        int i7 = (i < 90 || (i > 180 && i < 270)) ? rect.left : rect.right;
        int i8 = 0;
        int i9 = 0;
        while (true) {
            if (i9 < fArr.length) {
                if (fArr[i9] >= ((float) (i7 - 1)) && fArr[i9] <= ((float) (i7 + 1))) {
                    int i10 = i9 + 1;
                    i8 = (int) Math.abs(Math.sin(radians) * ((double) (((float) rect.bottom) - fArr[i10])));
                    i5 = (int) Math.abs(Math.cos(radians) * ((double) (fArr[i10] - ((float) rect.top))));
                    i6 = (int) Math.abs(((double) (fArr[i10] - ((float) rect.top))) / Math.sin(radians));
                    i4 = (int) Math.abs(((double) (((float) rect.bottom) - fArr[i10])) / Math.cos(radians));
                    break;
                }
                i9 += 2;
            } else {
                i4 = 0;
                i5 = 0;
                i6 = 0;
                break;
            }
        }
        rect.set(i8, i5, i6 + i8, i4 + i5);
        if (z) {
            a(rect, i2, i3);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top, rect.width(), rect.height());
        if (bitmap != createBitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static int a(int i, int i2, int i3, int i4) {
        int i5 = 1;
        if (i2 > i4 || i > i3) {
            while ((i2 / 2) / i5 > i4 && (i / 2) / i5 > i3) {
                i5 *= 2;
            }
        }
        return i5;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        if (f == 0) {
            f = a();
        }
        int i3 = 1;
        if (f > 0) {
            while (true) {
                int i4 = i2 / i3;
                int i5 = f;
                if (i4 <= i5 && i / i3 <= i5) {
                    break;
                }
                i3 *= 2;
            }
        }
        return i3;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, boolean z, boolean z2) {
        if (i <= 0 && !z && !z2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i);
        float f2 = -1.0f;
        float f3 = z ? -1.0f : 1.0f;
        if (!z2) {
            f2 = 1.0f;
        }
        matrix.postScale(f3, f2);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        if (createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static int a() {
        try {
            EGL10 egl10 = (EGL10) EGLContext.getEGL();
            EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            egl10.eglInitialize(eglGetDisplay, new int[2]);
            int[] iArr = new int[1];
            egl10.eglGetConfigs(eglGetDisplay, null, 0, iArr);
            EGLConfig[] eGLConfigArr = new EGLConfig[iArr[0]];
            egl10.eglGetConfigs(eglGetDisplay, eGLConfigArr, iArr[0], iArr);
            int[] iArr2 = new int[1];
            int i = 0;
            for (int i2 = 0; i2 < iArr[0]; i2++) {
                egl10.eglGetConfigAttrib(eglGetDisplay, eGLConfigArr[i2], 12332, iArr2);
                if (i < iArr2[0]) {
                    i = iArr2[0];
                }
            }
            egl10.eglTerminate(eglGetDisplay);
            return Math.max(i, 2048);
        } catch (Exception unused) {
            return 2048;
        }
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        Canvas canvas = new Canvas(createBitmap);
        int width = bitmap.getWidth() / 2;
        int height = bitmap.getHeight() / 2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setARGB(255, 0, 0, 0);
        canvas.drawCircle((float) width, (float) height, (float) Math.min(width, height), paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    @DexIgnore
    public static void a(Context context, String str, String str2, Bitmap bitmap) {
        try {
            a(context, bitmap, Uri.fromFile(new File(str.concat(str2))), Bitmap.CompressFormat.PNG, 100);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
