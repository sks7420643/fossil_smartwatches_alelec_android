package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.dm5;
import com.fossil.fl4;
import com.fossil.ju5;
import com.fossil.nj5;
import com.fossil.nw5;
import com.fossil.ql4;
import com.fossil.vu5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.xg5;
import com.fossil.xu5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt5 extends pt5 {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ a E; // = new a(null);
    @DexIgnore
    public /* final */ dm5 A;
    @DexIgnore
    public /* final */ QuickResponseRepository B;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase C;
    @DexIgnore
    public List<AppNotificationFilter> e; // = new ArrayList();
    @DexIgnore
    public List<at5> f; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> g; // = new ArrayList();
    @DexIgnore
    public /* final */ b h; // = new b();
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ List<bt5> o; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> p; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<List<NotificationSettingsModel>> q; // = this.z.getListNotificationSettings();
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public ju5 s;
    @DexIgnore
    public /* final */ qt5 t;
    @DexIgnore
    public /* final */ rl4 u;
    @DexIgnore
    public /* final */ vu5 v;
    @DexIgnore
    public /* final */ xu5 w;
    @DexIgnore
    public /* final */ nw5 x;
    @DexIgnore
    public /* final */ ch5 y;
    @DexIgnore
    public /* final */ NotificationSettingsDao z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vt5.D;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(vt5.E.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vt5.E.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode + " isSettingRules " + vt5.this.i);
            if (communicateMode == CommunicateMode.SET_NOTIFICATION_FILTERS && vt5.this.i) {
                vt5.this.i = false;
                vt5.this.t.a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(vt5.E.a(), "onReceive - success");
                    vt5.this.t.close();
                    return;
                }
                FLogger.INSTANCE.getLocal().d(vt5.E.a(), "onReceive - failed");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = vt5.E.a();
                local2.d(a3, "mAllowMessagesFromFirsLoad=" + vt5.this.l() + " mAlowCallsFromFirstLoad=" + vt5.this.m() + " mListFavoriteContactFirstLoad=" + vt5.this.p() + " size=" + vt5.this.p().size());
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", vt5.this.m(), true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", vt5.this.l(), false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                vt5.this.a(arrayList);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = vt5.E.a();
                local3.d(a4, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
                int size = integerArrayListExtra.size();
                for (int i = 0; i < size; i++) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a5 = vt5.E.a();
                    local4.d(a5, "error code " + i + " =" + integerArrayListExtra.get(i));
                }
                if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                    List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(integerArrayListExtra);
                    ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                    qt5 o = vt5.this.t;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                    if (array != null) {
                        ib5[] ib5Arr = (ib5[]) array;
                        o.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                }
                vt5.this.t.c();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.d<xu5.c, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ vt5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ContactGroup b;

        @DexIgnore
        public c(vt5 vt5, ContactGroup contactGroup) {
            this.a = vt5;
            this.b = contactGroup;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(xu5.c cVar) {
            ee7.b(cVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(vt5.E.a(), ".Inside mRemoveContactGroup onSuccess");
            this.a.t.a(this.b);
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            ee7.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(vt5.E.a(), ".Inside mRemoveContactGroup onError");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {499}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $settings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vt5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vt5$d$a$a")
            /* renamed from: com.fossil.vt5$d$a$a  reason: collision with other inner class name */
            public static final class RunnableC0222a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                public RunnableC0222a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.a.this$0.this$0.C.getNotificationSettingsDao().insertListNotificationSettings(this.a.this$0.$settings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.C.runInTransaction(new RunnableC0222a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vt5 vt5, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vt5;
            this.$settings = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$settings, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {428}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vt5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements fl4.e<dm5.d, dm5.c> {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(dm5.d dVar) {
                ee7.b(dVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(vt5.E.a(), "Set reply message to device success");
                this.a.this$0.t.a();
                this.a.this$0.t.u(true);
            }

            @DexIgnore
            public void a(dm5.c cVar) {
                ee7.b(cVar, "errorValue");
                FLogger.INSTANCE.getLocal().e(vt5.E.a(), "Set reply message to device fail");
                this.a.this$0.t.a();
                this.a.this$0.t.u(false);
                if (!xg5.b.a(PortfolioApp.g0.c().getApplicationContext(), xg5.c.BLUETOOTH_CONNECTION)) {
                    xg5 xg5 = xg5.b;
                    qt5 o = this.a.this$0.t;
                    if (o != null) {
                        xg5.a(xg5, ((rt5) o).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null);
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
                }
                this.a.this$0.t.e0();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1$messages$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends QuickResponseMessage>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends QuickResponseMessage>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.B.getAllQuickResponse();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(vt5 vt5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vt5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.t.b();
                ti7 b2 = this.this$0.c();
                b bVar = new b(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b2, bVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.A.a(new dm5.b((List) obj), new a(this));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {393, 395}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vt5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {333}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends AppNotificationFilter>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                List list = null;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    vu5 e = this.this$0.this$0.v;
                    this.L$0 = yi7;
                    this.L$1 = null;
                    this.label = 1;
                    obj = gl4.a(e, null, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    list = (List) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                fl4.c cVar = (fl4.c) obj;
                if (cVar instanceof vu5.d) {
                    list = new ArrayList();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = vt5.E.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("GetAllContactGroup onSuccess, size = ");
                    vu5.d dVar = (vu5.d) cVar;
                    sb.append(dVar.a().size());
                    local.d(a2, sb.toString());
                    this.this$0.this$0.g = ea7.d((Collection) dVar.a());
                    if (this.this$0.this$0.t.T() == 0) {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "call everyone");
                        DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                        list.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                    } else if (this.this$0.this$0.t.T() == 1) {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "call favorite");
                        int size = this.this$0.this$0.g.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.g.get(i2);
                            DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                            List<Contact> contacts = contactGroup.getContacts();
                            ee7.a((Object) contacts, "item.contacts");
                            if (!contacts.isEmpty()) {
                                Contact contact = contactGroup.getContacts().get(0);
                                ee7.a((Object) contact, "item.contacts[0]");
                                appNotificationFilter.setSender(contact.getDisplayName());
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String a3 = vt5.E.a();
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("getTypeAllowFromCall - item ");
                                sb2.append(i2);
                                sb2.append(" name = ");
                                Contact contact2 = contactGroup.getContacts().get(0);
                                ee7.a((Object) contact2, "item.contacts[0]");
                                sb2.append(contact2.getDisplayName());
                                local2.d(a3, sb2.toString());
                                list.add(appNotificationFilter);
                            }
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "call no one");
                    }
                    if (this.this$0.this$0.t.h0() == 0) {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "message everyone");
                        DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                        list.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                    } else if (this.this$0.this$0.t.h0() == 1) {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "message favorite");
                        int size2 = this.this$0.this$0.g.size();
                        for (int i3 = 0; i3 < size2; i3++) {
                            ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.g.get(i3);
                            DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                            List<Contact> contacts2 = contactGroup2.getContacts();
                            ee7.a((Object) contacts2, "item.contacts");
                            if (!contacts2.isEmpty()) {
                                Contact contact3 = contactGroup2.getContacts().get(0);
                                ee7.a((Object) contact3, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact3.getDisplayName());
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = vt5.E.a();
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("getTypeAllowFromMessage - item ");
                                sb3.append(i3);
                                sb3.append(" name = ");
                                Contact contact4 = contactGroup2.getContacts().get(0);
                                ee7.a((Object) contact4, "item.contacts[0]");
                                sb3.append(contact4.getDisplayName());
                                local3.d(a4, sb3.toString());
                                list.add(appNotificationFilter2);
                            }
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().d(vt5.E.a(), "message no one");
                    }
                } else if (cVar instanceof vu5.b) {
                    FLogger.INSTANCE.getLocal().d(vt5.E.a(), "GetAllContactGroup onError");
                }
                return list;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$otherAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends AppNotificationFilter>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return mx6.a(this.this$0.this$0.o(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(vt5 vt5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vt5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0182  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x01fa  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r20) {
            /*
                r19 = this;
                r0 = r19
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 2
                r4 = 1
                if (r2 == 0) goto L_0x0055
                if (r2 == r4) goto L_0x0032
                if (r2 != r3) goto L_0x002a
                java.lang.Object r1 = r0.L$3
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                java.lang.Object r1 = r0.L$2
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                long r1 = r0.J$0
                java.lang.Object r3 = r0.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r3 = r0.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r20)
                r5 = r1
                r2 = r20
                goto L_0x017e
            L_0x002a:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0032:
                java.lang.Object r2 = r0.L$4
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r5 = r0.L$3
                com.fossil.hj7 r5 = (com.fossil.hj7) r5
                java.lang.Object r6 = r0.L$2
                com.fossil.hj7 r6 = (com.fossil.hj7) r6
                long r7 = r0.J$0
                java.lang.Object r9 = r0.L$1
                java.util.List r9 = (java.util.List) r9
                java.lang.Object r10 = r0.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r20)
                r11 = r20
                r17 = r7
                r7 = r5
                r8 = r6
                r5 = r17
                goto L_0x0166
            L_0x0055:
                com.fossil.t87.a(r20)
                com.fossil.yi7 r10 = r0.p$
                com.fossil.vt5 r2 = r0.this$0
                com.fossil.qt5 r2 = r2.t
                r2.b()
                com.fossil.vt5 r2 = r0.this$0
                com.fossil.qt5 r2 = r2.t
                r2.U0()
                java.util.ArrayList r9 = new java.util.ArrayList
                r9.<init>()
                com.fossil.vt5 r2 = r0.this$0
                com.fossil.qt5 r2 = r2.t
                int r2 = r2.T()
                com.fossil.vt5 r5 = r0.this$0
                int r5 = r5.m()
                if (r2 == r5) goto L_0x0097
                com.portfolio.platform.data.model.NotificationSettingsModel r2 = new com.portfolio.platform.data.model.NotificationSettingsModel
                com.fossil.vt5 r5 = r0.this$0
                com.fossil.qt5 r5 = r5.t
                int r5 = r5.T()
                java.lang.String r6 = "AllowCallsFrom"
                r2.<init>(r6, r5, r4)
                r9.add(r2)
            L_0x0097:
                com.fossil.vt5 r2 = r0.this$0
                com.fossil.qt5 r2 = r2.t
                int r2 = r2.h0()
                com.fossil.vt5 r5 = r0.this$0
                int r5 = r5.l()
                if (r2 == r5) goto L_0x00be
                com.portfolio.platform.data.model.NotificationSettingsModel r2 = new com.portfolio.platform.data.model.NotificationSettingsModel
                com.fossil.vt5 r5 = r0.this$0
                com.fossil.qt5 r5 = r5.t
                int r5 = r5.h0()
                r6 = 0
                java.lang.String r7 = "AllowMessagesFrom"
                r2.<init>(r7, r5, r6)
                r9.add(r2)
            L_0x00be:
                boolean r2 = r9.isEmpty()
                r2 = r2 ^ r4
                if (r2 == 0) goto L_0x00ca
                com.fossil.vt5 r2 = r0.this$0
                r2.a(r9)
            L_0x00ca:
                long r5 = java.lang.System.currentTimeMillis()
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vt5$a r7 = com.fossil.vt5.E
                java.lang.String r7 = r7.a()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r11 = "filter notification, time start="
                r8.append(r11)
                r8.append(r5)
                java.lang.String r8 = r8.toString()
                r2.d(r7, r8)
                com.fossil.vt5 r2 = r0.this$0
                java.util.List r2 = r2.e
                r2.clear()
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vt5$a r7 = com.fossil.vt5.E
                java.lang.String r7 = r7.a()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r11 = "mListAppWrapper.size = "
                r8.append(r11)
                com.fossil.vt5 r11 = r0.this$0
                java.util.List r11 = r11.o()
                int r11 = r11.size()
                r8.append(r11)
                java.lang.String r8 = r8.toString()
                r2.d(r7, r8)
                com.fossil.vt5 r2 = r0.this$0
                com.fossil.ti7 r12 = r2.b()
                r13 = 0
                com.fossil.vt5$f$b r14 = new com.fossil.vt5$f$b
                r2 = 0
                r14.<init>(r0, r2)
                r15 = 2
                r16 = 0
                r11 = r10
                com.fossil.hj7 r7 = com.fossil.xh7.a(r11, r12, r13, r14, r15, r16)
                com.fossil.vt5 r8 = r0.this$0
                com.fossil.ti7 r12 = r8.b()
                com.fossil.vt5$f$a r14 = new com.fossil.vt5$f$a
                r14.<init>(r0, r2)
                com.fossil.hj7 r2 = com.fossil.xh7.a(r11, r12, r13, r14, r15, r16)
                com.fossil.vt5 r8 = r0.this$0
                java.util.List r8 = r8.e
                r0.L$0 = r10
                r0.L$1 = r9
                r0.J$0 = r5
                r0.L$2 = r7
                r0.L$3 = r2
                r0.L$4 = r8
                r0.label = r4
                java.lang.Object r11 = r7.c(r0)
                if (r11 != r1) goto L_0x0160
                return r1
            L_0x0160:
                r17 = r7
                r7 = r2
                r2 = r8
                r8 = r17
            L_0x0166:
                java.util.Collection r11 = (java.util.Collection) r11
                r2.addAll(r11)
                r0.L$0 = r10
                r0.L$1 = r9
                r0.J$0 = r5
                r0.L$2 = r8
                r0.L$3 = r7
                r0.label = r3
                java.lang.Object r2 = r7.c(r0)
                if (r2 != r1) goto L_0x017e
                return r1
            L_0x017e:
                java.util.List r2 = (java.util.List) r2
                if (r2 == 0) goto L_0x01fa
                com.fossil.vt5 r1 = r0.this$0
                java.util.List r1 = r1.e
                r1.addAll(r2)
                com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
                com.fossil.vt5 r2 = r0.this$0
                java.util.List r2 = r2.e
                long r7 = java.lang.System.currentTimeMillis()
                r1.<init>(r2, r7)
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                java.lang.String r3 = r3.c()
                long r1 = r2.b(r1, r3)
                com.fossil.vt5 r3 = r0.this$0
                r3.i = r4
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.vt5$a r4 = com.fossil.vt5.E
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "filter notification, time end= "
                r7.append(r8)
                r7.append(r1)
                java.lang.String r7 = r7.toString()
                r3.d(r4, r7)
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.vt5$a r4 = com.fossil.vt5.E
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "delayTime ="
                r7.append(r8)
                long r1 = r1 - r5
                r7.append(r1)
                java.lang.String r1 = " mili seconds"
                r7.append(r1)
                java.lang.String r1 = r7.toString()
                r3.d(r4, r1)
                goto L_0x0215
            L_0x01fa:
                com.fossil.vt5 r1 = r0.this$0
                com.fossil.qt5 r1 = r1.t
                r1.a()
                com.fossil.vt5 r1 = r0.this$0
                com.fossil.qt5 r1 = r1.t
                r1.w0()
                com.fossil.vt5 r1 = r0.this$0
                com.fossil.qt5 r1 = r1.t
                r1.close()
            L_0x0215:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt5.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<ju5.a> {
        @DexIgnore
        public /* final */ /* synthetic */ vt5 a;

        @DexIgnore
        public g(vt5 vt5) {
            this.a = vt5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ju5.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vt5.E.a();
            local.d(a2, "NotificationSettingChanged value = " + aVar);
            String a3 = this.a.a(aVar.a());
            if (aVar.b()) {
                this.a.t.B(a3);
            } else {
                this.a.t.m(a3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2", f = "NotificationCallsAndMessagesPresenter.kt", l = {158, 165}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vt5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {159}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements fl4.e<vu5.d, vu5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(vu5.d dVar) {
                ee7.b(dVar, "responseValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = vt5.E.a();
                local.d(a2, "inside start, GetAllContactGroup onSuccess, size = " + dVar.a().size());
                this.a.this$0.t.r(ea7.d((Collection) dVar.a()));
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = vt5.E.a();
                local2.d(a3, "mFlagGetListFavoriteFirstLoad=" + this.a.this$0.n());
                if (this.a.this$0.n()) {
                    this.a.this$0.b(ea7.d((Collection) dVar.a()));
                    for (T t : dVar.a()) {
                        List<Contact> contacts = t.getContacts();
                        ee7.a((Object) contacts, "contactGroup.contacts");
                        if (!contacts.isEmpty()) {
                            Contact contact = t.getContacts().get(0);
                            bt5 bt5 = new bt5(contact, null, 2, null);
                            bt5.setAdded(true);
                            Contact contact2 = bt5.getContact();
                            if (contact2 != null) {
                                ee7.a((Object) contact, "contact");
                                contact2.setDbRowId(contact.getDbRowId());
                            }
                            Contact contact3 = bt5.getContact();
                            if (contact3 != null) {
                                ee7.a((Object) contact, "contact");
                                contact3.setUseSms(contact.isUseSms());
                            }
                            Contact contact4 = bt5.getContact();
                            if (contact4 != null) {
                                ee7.a((Object) contact, "contact");
                                contact4.setUseCall(contact.isUseCall());
                            }
                            ee7.a((Object) contact, "contact");
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            ee7.a((Object) phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                ee7.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                                if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                    bt5.setHasPhoneNumber(true);
                                    PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                    ee7.a((Object) phoneNumber2, "contact.phoneNumbers[0]");
                                    bt5.setPhoneNumber(phoneNumber2.getNumber());
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String a4 = vt5.E.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                    PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                    ee7.a((Object) phoneNumber3, "contact.phoneNumbers[0]");
                                    sb.append(phoneNumber3.getNumber());
                                    local3.d(a4, sb.toString());
                                }
                            }
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String a5 = vt5.E.a();
                            local4.d(a5, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.a.this$0.p().add(bt5);
                        }
                    }
                    this.a.this$0.e(false);
                }
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String a6 = vt5.E.a();
                local5.d(a6, "start, mListFavoriteContactWrapperFirstLoad=" + this.a.this$0.p() + " size=" + this.a.this$0.p().size());
            }

            @DexIgnore
            public void a(vu5.b bVar) {
                ee7.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(vt5.E.a(), "GetAllContactGroup onError");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements fl4.e<nw5.a, fl4.a> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(nw5.a aVar) {
                ee7.b(aVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(vt5.E.a(), "GetApps onSuccess");
                this.a.this$0.o().clear();
                this.a.this$0.o().addAll(aVar.a());
            }

            @DexIgnore
            public void a(fl4.a aVar) {
                ee7.b(aVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(vt5.E.a(), "GetApps onError");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2$settings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends NotificationSettingsModel>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.z.getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(vt5 vt5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vt5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00b6  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 2
                r3 = 1
                r4 = 0
                r5 = 0
                if (r1 == 0) goto L_0x0028
                if (r1 == r3) goto L_0x0020
                if (r1 != r2) goto L_0x0018
                java.lang.Object r0 = r7.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r8)
                goto L_0x0076
            L_0x0018:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x0020:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x0053
            L_0x0028:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r1 = r7.p$
                com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r8 = r8.c()
                com.fossil.ch5 r8 = r8.v()
                boolean r8 = r8.U()
                if (r8 != 0) goto L_0x0053
                com.fossil.vt5 r8 = r7.this$0
                com.fossil.ti7 r8 = r8.b()
                com.fossil.vt5$h$a r6 = new com.fossil.vt5$h$a
                r6.<init>(r4)
                r7.L$0 = r1
                r7.label = r3
                java.lang.Object r8 = com.fossil.vh7.a(r8, r6, r7)
                if (r8 != r0) goto L_0x0053
                return r0
            L_0x0053:
                com.fossil.vt5 r8 = r7.this$0
                boolean r8 = r8.r
                if (r8 == 0) goto L_0x0161
                com.fossil.vt5 r8 = r7.this$0
                r8.r = r5
                com.fossil.vt5 r8 = r7.this$0
                com.fossil.ti7 r8 = r8.c()
                com.fossil.vt5$h$d r6 = new com.fossil.vt5$h$d
                r6.<init>(r7, r4)
                r7.L$0 = r1
                r7.label = r2
                java.lang.Object r8 = com.fossil.vh7.a(r8, r6, r7)
                if (r8 != r0) goto L_0x0076
                return r0
            L_0x0076:
                java.util.List r8 = (java.util.List) r8
                boolean r0 = r8.isEmpty()
                if (r0 == 0) goto L_0x00b6
                java.util.ArrayList r8 = new java.util.ArrayList
                r8.<init>()
                com.portfolio.platform.data.model.NotificationSettingsModel r0 = new com.portfolio.platform.data.model.NotificationSettingsModel
                java.lang.String r1 = "AllowCallsFrom"
                r0.<init>(r1, r5, r3)
                com.portfolio.platform.data.model.NotificationSettingsModel r1 = new com.portfolio.platform.data.model.NotificationSettingsModel
                java.lang.String r2 = "AllowMessagesFrom"
                r1.<init>(r2, r5, r5)
                r8.add(r0)
                r8.add(r1)
                com.fossil.vt5 r0 = r7.this$0
                r0.a(r8)
                com.fossil.vt5 r8 = r7.this$0
                java.lang.String r8 = r8.a(r5)
                com.fossil.vt5 r0 = r7.this$0
                com.fossil.qt5 r0 = r0.t
                r0.B(r8)
                com.fossil.vt5 r0 = r7.this$0
                com.fossil.qt5 r0 = r0.t
                r0.m(r8)
                goto L_0x0161
            L_0x00b6:
                java.util.Iterator r8 = r8.iterator()
            L_0x00ba:
                boolean r0 = r8.hasNext()
                if (r0 == 0) goto L_0x0161
                java.lang.Object r0 = r8.next()
                com.portfolio.platform.data.model.NotificationSettingsModel r0 = (com.portfolio.platform.data.model.NotificationSettingsModel) r0
                int r1 = r0.component2()
                boolean r0 = r0.component3()
                if (r0 == 0) goto L_0x0118
                com.fossil.vt5 r0 = r7.this$0
                java.lang.String r0 = r0.a(r1)
                com.fossil.vt5 r2 = r7.this$0
                boolean r2 = r2.k
                if (r2 == 0) goto L_0x00e8
                com.fossil.vt5 r2 = r7.this$0
                r2.c(r1)
                com.fossil.vt5 r1 = r7.this$0
                r1.k = r5
            L_0x00e8:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vt5$a r2 = com.fossil.vt5.E
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r6 = "start, mAlowCallsFromFirstLoad="
                r3.append(r6)
                com.fossil.vt5 r6 = r7.this$0
                int r6 = r6.m()
                r3.append(r6)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                com.fossil.vt5 r1 = r7.this$0
                com.fossil.qt5 r1 = r1.t
                r1.B(r0)
                goto L_0x00ba
            L_0x0118:
                com.fossil.vt5 r0 = r7.this$0
                java.lang.String r0 = r0.a(r1)
                com.fossil.vt5 r2 = r7.this$0
                boolean r2 = r2.m
                if (r2 == 0) goto L_0x0130
                com.fossil.vt5 r2 = r7.this$0
                r2.b(r1)
                com.fossil.vt5 r1 = r7.this$0
                r1.m = r5
            L_0x0130:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vt5$a r2 = com.fossil.vt5.E
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r6 = "start, mAllowMessagesFromFirsLoad="
                r3.append(r6)
                com.fossil.vt5 r6 = r7.this$0
                int r6 = r6.l()
                r3.append(r6)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                com.fossil.vt5 r1 = r7.this$0
                com.fossil.qt5 r1 = r1.t
                r1.m(r0)
                goto L_0x00ba
            L_0x0161:
                com.fossil.vt5 r8 = r7.this$0
                com.fossil.vu5 r8 = r8.v
                com.fossil.vt5$h$b r0 = new com.fossil.vt5$h$b
                r0.<init>(r7)
                r8.a(r4, r0)
                com.fossil.vt5 r8 = r7.this$0
                com.fossil.nw5 r8 = r8.x
                com.fossil.vt5$h$c r0 = new com.fossil.vt5$h$c
                r0.<init>(r7)
                r8.a(r4, r0)
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = vt5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationCallsAndMess\u2026er::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public vt5(qt5 qt5, rl4 rl4, vu5 vu5, xu5 xu5, yu5 yu5, nw5 nw5, ch5 ch5, NotificationSettingsDao notificationSettingsDao, dm5 dm5, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        ee7.b(qt5, "mView");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(vu5, "mGetAllContactGroup");
        ee7.b(xu5, "mRemoveContactGroup");
        ee7.b(yu5, "mSaveContactGroupsNotification");
        ee7.b(nw5, "mGetApps");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(notificationSettingsDao, "mNotificationSettingDao");
        ee7.b(dm5, "mReplyMessageUseCase");
        ee7.b(quickResponseRepository, "mQRRepository");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.t = qt5;
        this.u = rl4;
        this.v = vu5;
        this.w = xu5;
        this.x = nw5;
        this.y = ch5;
        this.z = notificationSettingsDao;
        this.A = dm5;
        this.B = quickResponseRepository;
        this.C = notificationSettingsDatabase;
    }

    @DexIgnore
    public final List<bt5> p() {
        return this.o;
    }

    @DexIgnore
    public final boolean q() {
        return (this.t.T() == this.j && this.t.h0() == this.l && !(ee7.a(this.p, this.t.H0()) ^ true)) ? false : true;
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d(D, "registerBroadcastReceiver");
        nj5.d.a(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void s() {
        this.t.a(this);
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d(D, "unregisterBroadcastReceiver");
        nj5.d.b(this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void e(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(D, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        r();
        nj5.d.a(CommunicateMode.SET_NOTIFICATION_FILTERS);
        qt5 qt5 = this.t;
        if (qt5 != null) {
            FragmentActivity activity = ((rt5) qt5).getActivity();
            ju5 ju5 = this.s;
            if (ju5 != null) {
                ju5.a().a((LifecycleOwner) this.t, new g(this));
                if (!this.t.P0()) {
                    xg5 xg5 = xg5.b;
                    if (activity == null) {
                        ee7.a();
                        throw null;
                    } else if (xg5.a(xg5, (Context) activity, xg5.a.NOTIFICATION_GET_CONTACTS, false, true, false, (Integer) null, 52, (Object) null)) {
                        ik7 unused = xh7.b(e(), null, null, new h(this, null), 3, null);
                    }
                }
                qt5 qt52 = this.t;
                qt52.d(true ^ xg5.a(xg5.b, ((rt5) qt52).getContext(), xg5.a.QUICK_RESPONSE, false, false, false, (Integer) null, 56, (Object) null));
                return;
            }
            ee7.d("mNotificationSettingViewModel");
            throw null;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        t();
        LiveData<List<NotificationSettingsModel>> liveData = this.q;
        qt5 qt5 = this.t;
        if (qt5 != null) {
            liveData.a((rt5) qt5);
            ju5 ju5 = this.s;
            if (ju5 != null) {
                ju5.a().a((LifecycleOwner) this.t);
                FLogger.INSTANCE.getLocal().d(D, "stop");
                return;
            }
            ee7.d("mNotificationSettingViewModel");
            throw null;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void h() {
        xg5 xg5 = xg5.b;
        qt5 qt5 = this.t;
        if (qt5 != null) {
            xg5.a(xg5, ((rt5) qt5).getContext(), xg5.a.QUICK_RESPONSE, false, false, false, (Integer) null, 60, (Object) null);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public boolean i() {
        Boolean T = this.y.T();
        ee7.a((Object) T, "mSharedPreferencesManager.isQuickResponseEnabled");
        return T.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void j() {
        this.t.a();
        this.t.close();
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void k() {
        this.y.a(PortfolioApp.g0.c().c(), 0L, false);
    }

    @DexIgnore
    public final int l() {
        return this.l;
    }

    @DexIgnore
    public final int m() {
        return this.j;
    }

    @DexIgnore
    public final boolean n() {
        return this.n;
    }

    @DexIgnore
    public final List<at5> o() {
        return this.f;
    }

    @DexIgnore
    public final void c(int i2) {
        this.j = i2;
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void d(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "updateNotificationSetting isCall=" + z2);
        qt5 qt5 = this.t;
        ju5.a aVar = new ju5.a(z2 ? qt5.T() : qt5.h0(), z2);
        ju5 ju5 = this.s;
        if (ju5 != null) {
            ju5.a().a(aVar);
            this.t.F0();
            return;
        }
        ee7.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(int i2) {
        this.l = i2;
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void c(boolean z2) {
        if (!q()) {
            FLogger.INSTANCE.getLocal().d(D, "setRuleToDevice, nothing changed");
            this.t.close();
            return;
        }
        xg5 xg5 = xg5.b;
        qt5 qt5 = this.t;
        if (qt5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        } else if (!xg5.a(xg5, ((rt5) qt5).getContext(), xg5.a.SET_BLE_COMMAND, false, true, false, (Integer) null, 52, (Object) null)) {
        } else {
            if (!z2 || xg5.a(xg5.b, ((rt5) this.t).getContext(), xg5.a.NOTIFICATION_DIANA, false, false, true, (Integer) null, 44, (Object) null)) {
                ik7 unused = xh7.b(e(), null, null, new f(this, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886089);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886091);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886090);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    public final void b(List<ContactGroup> list) {
        ee7.b(list, "<set-?>");
        this.p = list;
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void b(boolean z2) {
        if (z2) {
            xg5 xg5 = xg5.b;
            qt5 qt5 = this.t;
            if (qt5 == null) {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
            } else if (!xg5.a(xg5, ((rt5) qt5).getContext(), xg5.a.QUICK_RESPONSE, false, false, true, (Integer) 112, 12, (Object) null)) {
                return;
            }
        }
        ik7 unused = xh7.b(e(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void a(ContactGroup contactGroup) {
        ee7.b(contactGroup, "contactGroup");
        this.u.a(this.w, new xu5.b(contactGroup), new c(this, contactGroup));
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void a(boolean z2) {
        this.y.d(Boolean.valueOf(z2));
    }

    @DexIgnore
    @Override // com.fossil.pt5
    public void a(ju5 ju5) {
        ee7.b(ju5, "viewModel");
        this.s = ju5;
    }

    @DexIgnore
    public final void a(List<NotificationSettingsModel> list) {
        FLogger.INSTANCE.getLocal().d(D, "saveNotificationSettings");
        ik7 unused = xh7.b(e(), null, null, new d(this, list, null), 3, null);
    }
}
