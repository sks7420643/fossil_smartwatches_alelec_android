package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r24<T> implements ob4<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Object a; // = c;
    @DexIgnore
    public volatile ob4<T> b;

    @DexIgnore
    public r24(ob4<T> ob4) {
        this.b = ob4;
    }

    @DexIgnore
    @Override // com.fossil.ob4
    public T get() {
        T t;
        T t2 = (T) this.a;
        if (t2 == c) {
            synchronized (this) {
                T t3 = this.a;
                if (t3 == c) {
                    t3 = this.b.get();
                    this.a = t3;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
