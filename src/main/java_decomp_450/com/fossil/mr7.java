package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr7 implements ar7 {
    @DexIgnore
    public /* final */ yq7 a; // = new yq7();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ sr7 c;

    @DexIgnore
    public mr7(sr7 sr7) {
        ee7.b(sr7, "source");
        this.c = sr7;
    }

    @DexIgnore
    public long a(byte b2) {
        return a(b2, 0, Long.MAX_VALUE);
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) {
        ee7.b(yq7, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(true ^ this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (this.a.x() == 0 && this.c.b(this.a, (long) 8192) == -1) {
            return -1;
        } else {
            return this.a.b(yq7, Math.min(j, this.a.x()));
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public yq7 buffer() {
        return this.a;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (!this.b) {
            this.b = true;
            this.c.close();
            this.a.k();
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean d(long j) {
        if (!(j >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!this.b) {
            while (this.a.x() < j) {
                if (this.c.b(this.a, (long) 8192) == -1) {
                    return false;
                }
            }
            return true;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte[] f() {
        this.a.a(this.c);
        return this.a.f();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public yq7 getBuffer() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean h() {
        if (!(!this.b)) {
            throw new IllegalStateException("closed".toString());
        } else if (!this.a.h() || this.c.b(this.a, (long) 8192) != -1) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.b;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public long m() {
        int i;
        h(1);
        long j = 0;
        while (true) {
            long j2 = j + 1;
            if (!d(j2)) {
                break;
            }
            byte e = this.a.e(j);
            if ((e >= ((byte) 48) && e <= ((byte) 57)) || (j == 0 && e == ((byte) 45))) {
                j = j2;
            } else if (i == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Expected leading [0-9] or '-' character but was 0x");
                qg7.a(16);
                qg7.a(16);
                String num = Integer.toString(e, 16);
                ee7.a((Object) num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
                sb.append(num);
                throw new NumberFormatException(sb.toString());
            }
        }
        return this.a.m();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String p() {
        return b(Long.MAX_VALUE);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public ar7 peek() {
        return ir7.a(new kr7(this));
    }

    @DexIgnore
    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        ee7.b(byteBuffer, "sink");
        if (this.a.x() == 0 && this.c.b(this.a, (long) 8192) == -1) {
            return -1;
        }
        return this.a.read(byteBuffer);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte readByte() {
        h(1);
        return this.a.readByte();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void readFully(byte[] bArr) {
        ee7.b(bArr, "sink");
        try {
            h((long) bArr.length);
            this.a.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.a.x() > 0) {
                yq7 yq7 = this.a;
                int read = yq7.read(bArr, i, (int) yq7.x());
                if (read != -1) {
                    i += read;
                } else {
                    throw new AssertionError();
                }
            }
            throw e;
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public int readInt() {
        h(4);
        return this.a.readInt();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public short readShort() {
        h(2);
        return this.a.readShort();
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void skip(long j) {
        if (!this.b) {
            while (j > 0) {
                if (this.a.x() == 0 && this.c.b(this.a, (long) 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.a.x());
                this.a.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    @Override // com.fossil.ar7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long t() {
        /*
            r5 = this;
            r0 = 1
            r5.h(r0)
            r0 = 0
        L_0x0006:
            int r1 = r0 + 1
            long r2 = (long) r1
            boolean r2 = r5.d(r2)
            if (r2 == 0) goto L_0x0062
            com.fossil.yq7 r2 = r5.a
            long r3 = (long) r0
            byte r2 = r2.e(r3)
            r3 = 48
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0020
            r3 = 57
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
        L_0x0020:
            r3 = 97
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x002a
            r3 = 102(0x66, float:1.43E-43)
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
        L_0x002a:
            r3 = 65
            byte r3 = (byte) r3
            if (r2 < r3) goto L_0x0037
            r3 = 70
            byte r3 = (byte) r3
            if (r2 <= r3) goto L_0x0035
            goto L_0x0037
        L_0x0035:
            r0 = r1
            goto L_0x0006
        L_0x0037:
            if (r0 == 0) goto L_0x003a
            goto L_0x0062
        L_0x003a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Expected leading [0-9a-fA-F] character but was 0x"
            r0.append(r1)
            r1 = 16
            com.fossil.qg7.a(r1)
            com.fossil.qg7.a(r1)
            java.lang.String r1 = java.lang.Integer.toString(r2, r1)
            java.lang.String r2 = "java.lang.Integer.toStri\u2026(this, checkRadix(radix))"
            com.fossil.ee7.a(r1, r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            r1.<init>(r0)
            throw r1
        L_0x0062:
            com.fossil.yq7 r0 = r5.a
            long r0 = r0.t()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mr7.t():long");
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.c + ')';
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public InputStream u() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public boolean a(long j, br7 br7) {
        ee7.b(br7, "bytes");
        return a(j, br7, 0, br7.size());
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public br7 a(long j) {
        h(j);
        return this.a.a(j);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public byte[] f(long j) {
        h(j);
        return this.a.f(j);
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public long a(qr7 qr7) {
        ee7.b(qr7, "sink");
        long j = 0;
        while (this.c.b(this.a, (long) 8192) != -1) {
            long l = this.a.l();
            if (l > 0) {
                j += l;
                qr7.a(this.a, l);
            }
        }
        if (this.a.x() <= 0) {
            return j;
        }
        long x = j + this.a.x();
        yq7 yq7 = this.a;
        qr7.a(yq7, yq7.x());
        return x;
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public void h(long j) {
        if (!d(j)) {
            throw new EOFException();
        }
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.c.d();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ /* synthetic */ mr7 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(mr7 mr7) {
            this.a = mr7;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            mr7 mr7 = this.a;
            if (!mr7.b) {
                return (int) Math.min(mr7.a.x(), (long) Integer.MAX_VALUE);
            }
            throw new IOException("closed");
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
            this.a.close();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            mr7 mr7 = this.a;
            if (!mr7.b) {
                if (mr7.a.x() == 0) {
                    mr7 mr72 = this.a;
                    if (mr72.c.b(mr72.a, (long) 8192) == -1) {
                        return -1;
                    }
                }
                return this.a.a.readByte() & 255;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public String toString() {
            return this.a + ".inputStream()";
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "data");
            if (!this.a.b) {
                vq7.a((long) bArr.length, (long) i, (long) i2);
                if (this.a.a.x() == 0) {
                    mr7 mr7 = this.a;
                    if (mr7.c.b(mr7.a, (long) 8192) == -1) {
                        return -1;
                    }
                }
                return this.a.a.read(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String b(long j) {
        if (j >= 0) {
            long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j2);
            if (a2 != -1) {
                return ur7.a(this.a, a2);
            }
            if (j2 < Long.MAX_VALUE && d(j2) && this.a.e(j2 - 1) == ((byte) 13) && d(1 + j2) && this.a.e(j2) == b2) {
                return ur7.a(this.a, j2);
            }
            yq7 yq7 = new yq7();
            yq7 yq72 = this.a;
            yq72.a(yq7, 0, Math.min((long) 32, yq72.x()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.a.x(), j) + " content=" + yq7.o().hex() + "\u2026");
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    @DexIgnore
    @Override // com.fossil.ar7
    public String a(Charset charset) {
        ee7.b(charset, "charset");
        this.a.a(this.c);
        return this.a.a(charset);
    }

    @DexIgnore
    public int a() {
        h(4);
        return this.a.q();
    }

    @DexIgnore
    public long a(byte b2, long j, long j2) {
        boolean z = true;
        if (!this.b) {
            if (0 > j || j2 < j) {
                z = false;
            }
            if (z) {
                while (j < j2) {
                    long a2 = this.a.a(b2, j, j2);
                    if (a2 != -1) {
                        return a2;
                    }
                    long x = this.a.x();
                    if (x >= j2 || this.c.b(this.a, (long) 8192) == -1) {
                        return -1;
                    }
                    j = Math.max(j, x);
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex=" + j + " toIndex=" + j2).toString());
        }
        throw new IllegalStateException("closed".toString());
    }

    @DexIgnore
    public short b() {
        h(2);
        return this.a.r();
    }

    @DexIgnore
    public boolean a(long j, br7 br7, int i, int i2) {
        ee7.b(br7, "bytes");
        if (!this.b) {
            if (j >= 0 && i >= 0 && i2 >= 0 && br7.size() - i >= i2) {
                int i3 = 0;
                while (i3 < i2) {
                    long j2 = ((long) i3) + j;
                    if (d(1 + j2) && this.a.e(j2) == br7.getByte(i + i3)) {
                        i3++;
                    }
                }
                return true;
            }
            return false;
        }
        throw new IllegalStateException("closed".toString());
    }
}
