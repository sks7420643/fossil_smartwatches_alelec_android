package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.fossil.v1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ p1 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public View f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public v1.a i;
    @DexIgnore
    public t1 j;
    @DexIgnore
    public PopupWindow.OnDismissListener k;
    @DexIgnore
    public /* final */ PopupWindow.OnDismissListener l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements PopupWindow.OnDismissListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onDismiss() {
            u1.this.e();
        }
    }

    @DexIgnore
    public u1(Context context, p1 p1Var, View view, boolean z, int i2) {
        this(context, p1Var, view, z, i2, 0);
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    @DexIgnore
    public void b() {
        if (d()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    public t1 c() {
        if (this.j == null) {
            this.j = a();
        }
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        t1 t1Var = this.j;
        return t1Var != null && t1Var.a();
    }

    @DexIgnore
    public void e() {
        this.j = null;
        PopupWindow.OnDismissListener onDismissListener = this.k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public void f() {
        if (!g()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    public boolean g() {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public u1(Context context, p1 p1Var, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new a();
        this.a = context;
        this.b = p1Var;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public void a(View view) {
        this.f = view;
    }

    @DexIgnore
    public void a(boolean z) {
        this.h = z;
        t1 t1Var = this.j;
        if (t1Var != null) {
            t1Var.b(z);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    @DexIgnore
    public final t1 a() {
        t1 t1Var;
        Display defaultDisplay = ((WindowManager) this.a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        if (Math.min(point.x, point.y) >= this.a.getResources().getDimensionPixelSize(b0.abc_cascading_menus_min_smallest_width)) {
            t1Var = new m1(this.a, this.f, this.d, this.e, this.c);
        } else {
            t1Var = new z1(this.a, this.b, this.f, this.d, this.e, this.c);
        }
        t1Var.a(this.b);
        t1Var.a(this.l);
        t1Var.a(this.f);
        t1Var.a(this.i);
        t1Var.b(this.h);
        t1Var.a(this.g);
        return t1Var;
    }

    @DexIgnore
    public final void a(int i2, int i3, boolean z, boolean z2) {
        t1 c2 = c();
        c2.c(z2);
        if (z) {
            if ((l9.a(this.g, da.p(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            c2.b(i2);
            c2.c(i3);
            int i4 = (int) ((this.a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i3 + i4));
        }
        c2.show();
    }

    @DexIgnore
    public void a(v1.a aVar) {
        this.i = aVar;
        t1 t1Var = this.j;
        if (t1Var != null) {
            t1Var.a(aVar);
        }
    }
}
