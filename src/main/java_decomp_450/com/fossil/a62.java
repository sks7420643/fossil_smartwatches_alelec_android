package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a62<T> implements Iterator<T> {
    @DexIgnore
    public /* final */ z52<T> a;
    @DexIgnore
    public int b; // = -1;

    @DexIgnore
    public a62(z52<T> z52) {
        a72.a(z52);
        this.a = z52;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.b < this.a.getCount() - 1;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            z52<T> z52 = this.a;
            int i = this.b + 1;
            this.b = i;
            return z52.get(i);
        }
        int i2 = this.b;
        StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
