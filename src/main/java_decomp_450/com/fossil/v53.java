package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v53 implements Parcelable.Creator<u53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ u53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = -1;
        long j2 = -1;
        int i = 1;
        int i2 = 1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                i2 = j72.q(parcel, a);
            } else if (a2 == 3) {
                j = j72.s(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                j2 = j72.s(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new u53(i, i2, j, j2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ u53[] newArray(int i) {
        return new u53[i];
    }
}
