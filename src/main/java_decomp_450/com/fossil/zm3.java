package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm3 implements Parcelable.Creator<wm3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wm3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        em3 em3 = null;
        String str3 = null;
        ub3 ub3 = null;
        ub3 ub32 = null;
        ub3 ub33 = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    str = j72.e(parcel, a);
                    break;
                case 3:
                    str2 = j72.e(parcel, a);
                    break;
                case 4:
                    em3 = (em3) j72.a(parcel, a, em3.CREATOR);
                    break;
                case 5:
                    j = j72.s(parcel, a);
                    break;
                case 6:
                    z = j72.i(parcel, a);
                    break;
                case 7:
                    str3 = j72.e(parcel, a);
                    break;
                case 8:
                    ub3 = (ub3) j72.a(parcel, a, ub3.CREATOR);
                    break;
                case 9:
                    j2 = j72.s(parcel, a);
                    break;
                case 10:
                    ub32 = (ub3) j72.a(parcel, a, ub3.CREATOR);
                    break;
                case 11:
                    j3 = j72.s(parcel, a);
                    break;
                case 12:
                    ub33 = (ub3) j72.a(parcel, a, ub3.CREATOR);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new wm3(str, str2, em3, j, z, str3, ub3, j2, ub32, j3, ub33);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ wm3[] newArray(int i) {
        return new wm3[i];
    }
}
