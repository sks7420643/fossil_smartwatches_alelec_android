package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s97 extends r97 {
    @DexIgnore
    public static final <R> List<R> a(Object[] objArr, Class<R> cls) {
        ee7.b(objArr, "$this$filterIsInstance");
        ee7.b(cls, "klass");
        ArrayList arrayList = new ArrayList();
        a(objArr, arrayList, cls);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> b(T[] tArr) {
        ee7.b(tArr, "$this$asList");
        List<T> a = u97.a(tArr);
        ee7.a((Object) a, "ArraysUtilJVM.asList(this)");
        return a;
    }

    @DexIgnore
    public static final <T> void c(T[] tArr) {
        ee7.b(tArr, "$this$sort");
        if (tArr.length > 1) {
            Arrays.sort(tArr);
        }
    }

    @DexIgnore
    public static final <C extends Collection<? super R>, R> C a(Object[] objArr, C c, Class<R> cls) {
        ee7.b(objArr, "$this$filterIsInstanceTo");
        ee7.b(c, ShareConstants.DESTINATION);
        ee7.b(cls, "klass");
        for (Object obj : objArr) {
            if (cls.isInstance(obj)) {
                c.add(obj);
            }
        }
        return c;
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, T[] tArr2) {
        ee7.b(tArr, "$this$plus");
        ee7.b(tArr2, MessengerShareContentUtility.ELEMENTS);
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (T[]) Arrays.copyOf(tArr, length + length2);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        ee7.a((Object) tArr3, Constants.RESULT);
        return tArr3;
    }

    @DexIgnore
    public static /* synthetic */ int a(int[] iArr, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = iArr.length;
        }
        return a(iArr, i, i2, i3);
    }

    @DexIgnore
    public static final int a(int[] iArr, int i, int i2, int i3) {
        ee7.b(iArr, "$this$binarySearch");
        return Arrays.binarySearch(iArr, i2, i3, i);
    }

    @DexIgnore
    public static /* synthetic */ Object[] a(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        a(objArr, objArr2, i, i2, i3);
        return objArr2;
    }

    @DexIgnore
    public static final <T> T[] a(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        ee7.b(tArr, "$this$copyInto");
        ee7.b(tArr2, ShareConstants.DESTINATION);
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    @DexIgnore
    public static /* synthetic */ byte[] a(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = bArr.length;
        }
        a(bArr, bArr2, i, i2, i3);
        return bArr2;
    }

    @DexIgnore
    public static final Integer[] b(int[] iArr) {
        ee7.b(iArr, "$this$toTypedArray");
        Integer[] numArr = new Integer[iArr.length];
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            numArr[i] = Integer.valueOf(iArr[i]);
        }
        return numArr;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        ee7.b(bArr, "$this$copyInto");
        ee7.b(bArr2, ShareConstants.DESTINATION);
        System.arraycopy(bArr, i2, bArr2, i, i3 - i2);
        return bArr2;
    }

    @DexIgnore
    public static final <T> T[] a(T[] tArr, int i, int i2) {
        ee7.b(tArr, "$this$copyOfRangeImpl");
        q97.a(i2, tArr.length);
        T[] tArr2 = (T[]) Arrays.copyOfRange(tArr, i, i2);
        ee7.a((Object) tArr2, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return tArr2;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, int i, int i2) {
        ee7.b(bArr, "$this$copyOfRangeImpl");
        q97.a(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        ee7.a((Object) copyOfRange, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return copyOfRange;
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, T t, int i, int i2) {
        ee7.b(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte b) {
        ee7.b(bArr, "$this$plus");
        int length = bArr.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + 1);
        copyOf[length] = b;
        ee7.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        ee7.b(bArr, "$this$plus");
        ee7.b(bArr2, MessengerShareContentUtility.ELEMENTS);
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        ee7.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final void a(int[] iArr) {
        ee7.b(iArr, "$this$sort");
        if (iArr.length > 1) {
            Arrays.sort(iArr);
        }
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, Comparator<? super T> comparator) {
        ee7.b(tArr, "$this$sortWith");
        ee7.b(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }
}
