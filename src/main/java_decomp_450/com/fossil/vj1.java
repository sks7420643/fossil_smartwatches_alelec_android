package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj1 extends ey0 {
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    public vj1(long j, ri1 ri1) {
        super(uh0.q, qa1.E, ri1, 0, 8);
        this.L = j;
        ((uh1) this).E = true;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y1, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.ey0
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        ee7.a((Object) array, "ByteBuffer.allocate(4).o\u2026\n                .array()");
        return array;
    }
}
