package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s70 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ u70 a;
    @DexIgnore
    public /* final */ re0 b;
    @DexIgnore
    public ue0 c;
    @DexIgnore
    public ve0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<s70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public s70 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                u70 valueOf = u70.valueOf(readString);
                parcel.setDataPosition(0);
                switch (gx0.a[valueOf.ordinal()]) {
                    case 1:
                        return a80.CREATOR.createFromParcel(parcel);
                    case 2:
                        return x70.CREATOR.createFromParcel(parcel);
                    case 3:
                        return y70.CREATOR.createFromParcel(parcel);
                    case 4:
                        return v70.CREATOR.createFromParcel(parcel);
                    case 5:
                        return r70.CREATOR.createFromParcel(parcel);
                    case 6:
                        return z70.CREATOR.createFromParcel(parcel);
                    case 7:
                        return o70.CREATOR.createFromParcel(parcel);
                    case 8:
                        return q70.CREATOR.createFromParcel(parcel);
                    case 9:
                        return p70.CREATOR.createFromParcel(parcel);
                    case 10:
                        return w70.CREATOR.createFromParcel(parcel);
                    default:
                        throw new p87();
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public s70[] newArray(int i) {
            return new s70[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ s70(u70 u70, re0 re0, ue0 ue0, ve0 ve0, int i) {
        this(u70, (i & 2) != 0 ? new av0() : re0, (i & 4) != 0 ? new ue0(0, 62) : ue0, (i & 8) != 0 ? new ve0(ve0.CREATOR.a()) : ve0);
    }

    @DexIgnore
    public final void a(ue0 ue0) {
        this.c = ue0;
    }

    @DexIgnore
    public final re0 b() {
        return this.b;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            s70 s70 = (s70) obj;
            return this.a == s70.a && !(ee7.a(this.b, s70.b) ^ true) && !(ee7.a(this.c, s70.c) ^ true) && !(ee7.a(this.d, s70.d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.Complication");
    }

    @DexIgnore
    public final u70 getId() {
        return this.a;
    }

    @DexIgnore
    public final ue0 getPositionConfig() {
        return this.c;
    }

    @DexIgnore
    public final ve0 getThemeConfig() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.a.a()).put("pos", this.c.a()).put("data", this.b.a()).put("theme", this.d.a());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public s70(u70 u70, re0 re0, ue0 ue0, ve0 ve0) {
        this.a = u70;
        this.b = re0;
        this.c = ue0;
        this.d = ve0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public s70(android.os.Parcel r6) {
        /*
            r5 = this;
            java.lang.String r0 = r6.readString()
            r1 = 0
            if (r0 == 0) goto L_0x004a
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.ee7.a(r0, r2)
            com.fossil.u70 r0 = com.fossil.u70.valueOf(r0)
            java.lang.Class<com.fossil.re0> r2 = com.fossil.re0.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            android.os.Parcelable r2 = r6.readParcelable(r2)
            if (r2 == 0) goto L_0x0046
            com.fossil.re0 r2 = (com.fossil.re0) r2
            java.lang.Class<com.fossil.ue0> r3 = com.fossil.ue0.class
            java.lang.ClassLoader r3 = r3.getClassLoader()
            android.os.Parcelable r3 = r6.readParcelable(r3)
            if (r3 == 0) goto L_0x0042
            com.fossil.ue0 r3 = (com.fossil.ue0) r3
            java.lang.Class<com.fossil.ve0> r4 = com.fossil.ve0.class
            java.lang.ClassLoader r4 = r4.getClassLoader()
            android.os.Parcelable r6 = r6.readParcelable(r4)
            if (r6 == 0) goto L_0x003e
            com.fossil.ve0 r6 = (com.fossil.ve0) r6
            r5.<init>(r0, r2, r3, r6)
            return
        L_0x003e:
            com.fossil.ee7.a()
            throw r1
        L_0x0042:
            com.fossil.ee7.a()
            throw r1
        L_0x0046:
            com.fossil.ee7.a()
            throw r1
        L_0x004a:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.s70.<init>(android.os.Parcel):void");
    }
}
