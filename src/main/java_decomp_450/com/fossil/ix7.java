package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix7 extends ContentObserver {
    @DexIgnore
    public MethodChannel a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ix7(Handler handler) {
        super(handler);
        ee7.b(handler, "handler");
    }

    @DexIgnore
    public void onChange(boolean z) {
        super.onChange(z);
        MethodChannel methodChannel = this.a;
        if (methodChannel != null) {
            methodChannel.invokeMethod("change", 1);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ix7(Handler handler, int i, zd7 zd7) {
        this((i & 1) != 0 ? new Handler() : handler);
    }
}
