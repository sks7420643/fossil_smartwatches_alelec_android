package com.fossil;

import com.fossil.jv7;
import com.fossil.qn7;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wu7<ResponseT, ReturnT> extends gv7<ReturnT> {
    @DexIgnore
    public /* final */ ev7 a;
    @DexIgnore
    public /* final */ qn7.a b;
    @DexIgnore
    public /* final */ tu7<mo7, ResponseT> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<ResponseT, ReturnT> extends wu7<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ qu7<ResponseT, ReturnT> d;

        @DexIgnore
        public a(ev7 ev7, qn7.a aVar, tu7<mo7, ResponseT> tu7, qu7<ResponseT, ReturnT> qu7) {
            super(ev7, aVar, tu7);
            this.d = qu7;
        }

        @DexIgnore
        @Override // com.fossil.wu7
        public ReturnT a(Call<ResponseT> call, Object[] objArr) {
            return this.d.a(call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<ResponseT> extends wu7<ResponseT, Object> {
        @DexIgnore
        public /* final */ qu7<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public b(ev7 ev7, qn7.a aVar, tu7<mo7, ResponseT> tu7, qu7<ResponseT, Call<ResponseT>> qu7, boolean z) {
            super(ev7, aVar, tu7);
            this.d = qu7;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.wu7
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> a = this.d.a(call);
            fb7 fb7 = (fb7) objArr[objArr.length - 1];
            try {
                if (this.e) {
                    return yu7.b(a, fb7);
                }
                return yu7.a(a, fb7);
            } catch (Exception e2) {
                return yu7.a(e2, fb7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<ResponseT> extends wu7<ResponseT, Object> {
        @DexIgnore
        public /* final */ qu7<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public c(ev7 ev7, qn7.a aVar, tu7<mo7, ResponseT> tu7, qu7<ResponseT, Call<ResponseT>> qu7) {
            super(ev7, aVar, tu7);
            this.d = qu7;
        }

        @DexIgnore
        @Override // com.fossil.wu7
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> a = this.d.a(call);
            fb7 fb7 = (fb7) objArr[objArr.length - 1];
            try {
                return yu7.c(a, fb7);
            } catch (Exception e) {
                return yu7.a(e, fb7);
            }
        }
    }

    @DexIgnore
    public wu7(ev7 ev7, qn7.a aVar, tu7<mo7, ResponseT> tu7) {
        this.a = ev7;
        this.b = aVar;
        this.c = tu7;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> wu7<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, ev7 ev7) {
        Type type;
        boolean z;
        boolean z2 = ev7.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type a2 = jv7.a(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (jv7.b(a2) != fv7.class || !(a2 instanceof ParameterizedType)) {
                z = false;
            } else {
                a2 = jv7.b(0, (ParameterizedType) a2);
                z = true;
            }
            type = new jv7.b(null, Call.class, a2);
            annotations = iv7.a(annotations);
        } else {
            type = method.getGenericReturnType();
            z = false;
        }
        qu7 a3 = a(retrofit3, method, type, annotations);
        Type a4 = a3.a();
        if (a4 == Response.class) {
            throw jv7.a(method, "'" + jv7.b(a4).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a4 == fv7.class) {
            throw jv7.a(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!ev7.c.equals("HEAD") || Void.class.equals(a4)) {
            tu7 a5 = a(retrofit3, method, a4);
            qn7.a aVar = retrofit3.b;
            if (!z2) {
                return new a(ev7, aVar, a5, a3);
            }
            if (z) {
                return new c(ev7, aVar, a5, a3);
            }
            return new b(ev7, aVar, a5, a3, false);
        } else {
            throw jv7.a(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract ReturnT a(Call<ResponseT> call, Object[] objArr);

    @DexIgnore
    public static <ResponseT, ReturnT> qu7<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return (qu7<ResponseT, ReturnT>) retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw jv7.a(method, e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> tu7<mo7, ResponseT> a(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.b(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw jv7.a(method, e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    @Override // com.fossil.gv7
    public final ReturnT a(Object[] objArr) {
        return a(new zu7(this.a, objArr, this.b, this.c), objArr);
    }
}
