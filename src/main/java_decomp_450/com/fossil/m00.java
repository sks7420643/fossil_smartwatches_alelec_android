package com.fossil;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m00<Model, Data> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> {
        @DexIgnore
        public /* final */ yw a;
        @DexIgnore
        public /* final */ List<yw> b;
        @DexIgnore
        public /* final */ ix<Data> c;

        @DexIgnore
        public a(yw ywVar, ix<Data> ixVar) {
            this(ywVar, Collections.emptyList(), ixVar);
        }

        @DexIgnore
        public a(yw ywVar, List<yw> list, ix<Data> ixVar) {
            u50.a(ywVar);
            this.a = ywVar;
            u50.a((Object) list);
            this.b = list;
            u50.a(ixVar);
            this.c = ixVar;
        }
    }

    @DexIgnore
    a<Data> a(Model model, int i, int i2, ax axVar);

    @DexIgnore
    boolean a(Model model);
}
