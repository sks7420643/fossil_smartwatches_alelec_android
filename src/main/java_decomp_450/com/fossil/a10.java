package com.fossil;

import android.content.Context;
import android.net.Uri;
import com.fossil.m00;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a10 implements m00<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n00<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Uri, InputStream> a(q00 q00) {
            return new a10(this.a);
        }
    }

    @DexIgnore
    public a10(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public m00.a<InputStream> a(Uri uri, int i, int i2, ax axVar) {
        if (!vx.a(i, i2) || !a(axVar)) {
            return null;
        }
        return new m00.a<>(new k50(uri), wx.b(this.a, uri));
    }

    @DexIgnore
    public final boolean a(ax axVar) {
        Long l = (Long) axVar.a(h20.d);
        return l != null && l.longValue() == -1;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return vx.c(uri);
    }
}
