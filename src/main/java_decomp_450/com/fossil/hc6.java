package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hc6 extends ec6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public LiveData<qx6<List<HeartRateSample>>> g;
    @DexIgnore
    public LiveData<qx6<List<WorkoutSession>>> h;
    @DexIgnore
    public /* final */ fc6 i;
    @DexIgnore
    public /* final */ HeartRateSampleRepository j;
    @DexIgnore
    public /* final */ WorkoutSessionRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ hc6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mHeartRateSamples$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {35, 35}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<HeartRateSample>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<HeartRateSample>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    HeartRateSampleRepository c = this.this$0.a.j;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = c.getHeartRateSamples(date, date2, false, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public b(hc6 hc6) {
            this.a = hc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<HeartRateSample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mHeartRateSamples onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ hc6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mWorkoutSessions$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {41, 41}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<WorkoutSession>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<WorkoutSession>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    WorkoutSessionRepository e = this.this$0.a.k;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = e.getWorkoutSessions(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(hc6 hc6) {
            this.a = hc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<WorkoutSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mWorkoutSessions onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ hc6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {63, 71, 78}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hc6$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.hc6$d$a$a  reason: collision with other inner class name */
            public static final class C0076a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hc6$d$a$a$a")
                /* renamed from: com.fossil.hc6$d$a$a$a  reason: collision with other inner class name */
                public static final class C0077a<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return bb7.a(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0076a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0076a aVar = new C0076a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0076a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        if (list.size() > 1) {
                            aa7.a(list, new C0077a());
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $listTimeZoneChange;
                @DexIgnore
                public /* final */ /* synthetic */ List $listTodayHeartRateModel;
                @DexIgnore
                public /* final */ /* synthetic */ int $maxHR;
                @DexIgnore
                public /* final */ /* synthetic */ qe7 $previousTimeZoneOffset;
                @DexIgnore
                public /* final */ /* synthetic */ long $startOfDay;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, long j, qe7 qe7, List list, List list2, int i, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$startOfDay = j;
                    this.$previousTimeZoneOffset = qe7;
                    this.$listTimeZoneChange = list;
                    this.$listTodayHeartRateModel = list2;
                    this.$maxHR = i;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Iterator<T> it;
                    char c;
                    StringBuilder sb;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        for (Iterator<T> it2 = list.iterator(); it2.hasNext(); it2 = it) {
                            T next = it2.next();
                            long millis = (next.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                            long millis2 = (next.getEndTime().getMillis() - this.$startOfDay) / 60000;
                            long j = (millis2 + millis) / ((long) 2);
                            if (next.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                                int hourOfDay = next.getStartTimeId().getHourOfDay();
                                String b = ye5.b(hourOfDay);
                                we7 we7 = we7.a;
                                String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{pb7.a(Math.abs(next.getTimezoneOffsetInSecond() / 3600)), pb7.a(Math.abs((next.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                List list2 = this.$listTimeZoneChange;
                                Integer a = pb7.a((int) j);
                                it = it2;
                                r87 r87 = new r87(pb7.a(hourOfDay), pb7.a(((float) next.getTimezoneOffsetInSecond()) / 3600.0f));
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(b);
                                if (next.getTimezoneOffsetInSecond() >= 0) {
                                    sb = new StringBuilder();
                                    c = '+';
                                } else {
                                    sb = new StringBuilder();
                                    c = '-';
                                }
                                sb.append(c);
                                sb.append(format);
                                sb2.append(sb.toString());
                                list2.add(new v87(a, r87, sb2.toString()));
                                this.$previousTimeZoneOffset.element = next.getTimezoneOffsetInSecond();
                            } else {
                                it = it2;
                            }
                            if (!this.$listTodayHeartRateModel.isEmpty()) {
                                fz6 fz6 = (fz6) ea7.f(this.$listTodayHeartRateModel);
                                if (millis - ((long) fz6.a()) > 1) {
                                    this.$listTodayHeartRateModel.add(new fz6(0, 0, 0, fz6.a(), (int) millis, (int) j));
                                }
                            }
                            int average = (int) next.getAverage();
                            if (next.getMax() == this.$maxHR) {
                                average = next.getMax();
                            }
                            this.$listTodayHeartRateModel.add(new fz6(average, next.getMin(), next.getMax(), (int) millis, (int) millis2, (int) j));
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class c extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Comparable] */
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    int i;
                    T t;
                    Integer a;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        List list = this.this$0.$data;
                        if (list != null) {
                            Iterator<T> it = list.iterator();
                            if (!it.hasNext()) {
                                t = null;
                            } else {
                                T next = it.next();
                                if (!it.hasNext()) {
                                    t = next;
                                } else {
                                    Integer num = pb7.a(next.getMax());
                                    do {
                                        T next2 = it.next();
                                        Integer a2 = pb7.a(next2.getMax());
                                        int compareTo = num.compareTo(a2);
                                        Integer num2 = num;
                                        if (compareTo < 0) {
                                            next = next2;
                                            num2 = a2;
                                        }
                                        num = num2;
                                    } while (it.hasNext());
                                }
                                t = next;
                            }
                            T t2 = t;
                            if (!(t2 == null || (a = pb7.a(t2.getMax())) == null)) {
                                i = a.intValue();
                                return pb7.a(i);
                            }
                        }
                        i = 0;
                        return pb7.a(i);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:26:0x012c A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x012d  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r22) {
                /*
                    r21 = this;
                    r9 = r21
                    java.lang.Object r10 = com.fossil.nb7.a()
                    int r0 = r9.label
                    r1 = 0
                    r11 = 3
                    r2 = 2
                    r12 = 1
                    r13 = 0
                    if (r0 == 0) goto L_0x0052
                    if (r0 == r12) goto L_0x0046
                    if (r0 == r2) goto L_0x0032
                    if (r0 != r11) goto L_0x002a
                    java.lang.Object r0 = r9.L$3
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r1 = r9.L$2
                    com.fossil.qe7 r1 = (com.fossil.qe7) r1
                    java.lang.Object r1 = r9.L$1
                    java.util.List r1 = (java.util.List) r1
                    java.lang.Object r2 = r9.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r22)
                    goto L_0x012e
                L_0x002a:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0032:
                    long r0 = r9.J$0
                    java.lang.Object r2 = r9.L$1
                    java.util.List r2 = (java.util.List) r2
                    java.lang.Object r3 = r9.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r22)
                    r14 = r0
                    r8 = r2
                    r7 = r3
                    r1 = r22
                    goto L_0x00d4
                L_0x0046:
                    java.lang.Object r0 = r9.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r3 = r9.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r22)
                    goto L_0x007b
                L_0x0052:
                    com.fossil.t87.a(r22)
                    com.fossil.yi7 r0 = r9.p$
                    java.util.ArrayList r3 = new java.util.ArrayList
                    r3.<init>()
                    com.fossil.hc6$d r4 = r9.this$0
                    com.fossil.hc6 r4 = r4.a
                    com.fossil.ti7 r4 = r4.b()
                    com.fossil.hc6$d$a$a r5 = new com.fossil.hc6$d$a$a
                    r5.<init>(r9, r1)
                    r9.L$0 = r0
                    r9.L$1 = r3
                    r9.label = r12
                    java.lang.Object r4 = com.fossil.vh7.a(r4, r5, r9)
                    if (r4 != r10) goto L_0x0076
                    return r10
                L_0x0076:
                    r20 = r3
                    r3 = r0
                    r0 = r20
                L_0x007b:
                    java.util.List r4 = r9.$data
                    if (r4 == 0) goto L_0x00a0
                    boolean r4 = r4.isEmpty()
                    r4 = r4 ^ r12
                    if (r4 == 0) goto L_0x00a0
                    java.util.List r4 = r9.$data
                    java.lang.Object r4 = r4.get(r13)
                    com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r4 = (com.portfolio.platform.data.model.diana.heartrate.HeartRateSample) r4
                    org.joda.time.DateTime r4 = r4.getStartTimeId()
                    org.joda.time.DateTime r4 = r4.withTimeAtStartOfDay()
                    java.lang.String r5 = "data[0].getStartTimeId().withTimeAtStartOfDay()"
                    com.fossil.ee7.a(r4, r5)
                    long r4 = r4.getMillis()
                    goto L_0x00b5
                L_0x00a0:
                    com.fossil.hc6$d r4 = r9.this$0
                    com.fossil.hc6 r4 = r4.a
                    java.util.Date r4 = r4.e
                    java.util.Date r4 = com.fossil.zd5.q(r4)
                    java.lang.String r5 = "DateHelper.getStartOfDay(mDate)"
                    com.fossil.ee7.a(r4, r5)
                    long r4 = r4.getTime()
                L_0x00b5:
                    com.fossil.hc6$d r6 = r9.this$0
                    com.fossil.hc6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.b()
                    com.fossil.hc6$d$a$c r7 = new com.fossil.hc6$d$a$c
                    r7.<init>(r9, r1)
                    r9.L$0 = r3
                    r9.L$1 = r0
                    r9.J$0 = r4
                    r9.label = r2
                    java.lang.Object r1 = com.fossil.vh7.a(r6, r7, r9)
                    if (r1 != r10) goto L_0x00d1
                    return r10
                L_0x00d1:
                    r8 = r0
                    r7 = r3
                    r14 = r4
                L_0x00d4:
                    java.lang.Number r1 = (java.lang.Number) r1
                    int r6 = r1.intValue()
                    com.fossil.qe7 r5 = new com.fossil.qe7
                    r5.<init>()
                    r0 = -1
                    r5.element = r0
                    java.util.ArrayList r4 = new java.util.ArrayList
                    r4.<init>()
                    com.fossil.hc6$d r0 = r9.this$0
                    com.fossil.hc6 r0 = r0.a
                    com.fossil.ti7 r2 = r0.b()
                    com.fossil.hc6$d$a$b r3 = new com.fossil.hc6$d$a$b
                    r16 = 0
                    r0 = r3
                    r1 = r21
                    r13 = r2
                    r12 = r3
                    r2 = r14
                    r22 = r4
                    r4 = r5
                    r11 = r5
                    r5 = r22
                    r17 = r6
                    r6 = r8
                    r18 = r10
                    r10 = r7
                    r7 = r17
                    r19 = r12
                    r12 = r8
                    r8 = r16
                    r0.<init>(r1, r2, r4, r5, r6, r7, r8)
                    r9.L$0 = r10
                    r9.L$1 = r12
                    r9.J$0 = r14
                    r0 = r17
                    r9.I$0 = r0
                    r9.L$2 = r11
                    r0 = r22
                    r9.L$3 = r0
                    r1 = 3
                    r9.label = r1
                    r1 = r19
                    java.lang.Object r1 = com.fossil.vh7.a(r13, r1, r9)
                    r2 = r18
                    if (r1 != r2) goto L_0x012d
                    return r2
                L_0x012d:
                    r1 = r12
                L_0x012e:
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r4 = "listTimeZoneChange="
                    r3.append(r4)
                    java.lang.String r4 = com.fossil.wc5.a(r0)
                    r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    java.lang.String r4 = "HeartRateOverviewDayPresenter"
                    r2.d(r4, r3)
                    boolean r2 = r0.isEmpty()
                    r3 = 1
                    r2 = r2 ^ r3
                    if (r2 == 0) goto L_0x0350
                    int r2 = r0.size()
                    if (r2 <= r3) goto L_0x0350
                    r2 = 0
                    java.lang.Object r5 = r0.get(r2)
                    com.fossil.v87 r5 = (com.fossil.v87) r5
                    java.lang.Object r2 = r5.getSecond()
                    com.fossil.r87 r2 = (com.fossil.r87) r2
                    java.lang.Object r2 = r2.getSecond()
                    java.lang.Number r2 = (java.lang.Number) r2
                    float r2 = r2.floatValue()
                    int r5 = com.fossil.w97.a(r0)
                    java.lang.Object r5 = r0.get(r5)
                    com.fossil.v87 r5 = (com.fossil.v87) r5
                    java.lang.Object r5 = r5.getSecond()
                    com.fossil.r87 r5 = (com.fossil.r87) r5
                    java.lang.Object r5 = r5.getSecond()
                    java.lang.Number r5 = (java.lang.Number) r5
                    float r5 = r5.floatValue()
                    r6 = 0
                    float r7 = (float) r6
                    r6 = 60
                    int r8 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
                    if (r8 >= 0) goto L_0x0199
                    int r8 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r8 < 0) goto L_0x01a1
                L_0x0199:
                    int r8 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
                    if (r8 < 0) goto L_0x01a8
                    int r8 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r8 < 0) goto L_0x01a8
                L_0x01a1:
                    float r7 = r2 - r5
                    float r6 = (float) r6
                    float r7 = r7 * r6
                    int r6 = (int) r7
                    goto L_0x01b7
                L_0x01a8:
                    float r8 = r2 - r5
                    float r6 = (float) r6
                    float r8 = r8 * r6
                    int r6 = (int) r8
                    int r6 = java.lang.Math.abs(r6)
                    int r7 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                    if (r7 >= 0) goto L_0x01b7
                    int r6 = -r6
                L_0x01b7:
                    java.util.ArrayList r7 = new java.util.ArrayList
                    r7.<init>()
                    java.util.Iterator r8 = r0.iterator()
                L_0x01c0:
                    boolean r10 = r8.hasNext()
                    if (r10 == 0) goto L_0x01f2
                    java.lang.Object r10 = r8.next()
                    r11 = r10
                    com.fossil.v87 r11 = (com.fossil.v87) r11
                    java.lang.Object r11 = r11.getSecond()
                    com.fossil.r87 r11 = (com.fossil.r87) r11
                    java.lang.Object r11 = r11.getSecond()
                    java.lang.Number r11 = (java.lang.Number) r11
                    float r11 = r11.floatValue()
                    int r11 = (r11 > r2 ? 1 : (r11 == r2 ? 0 : -1))
                    if (r11 != 0) goto L_0x01e3
                    r11 = 1
                    goto L_0x01e4
                L_0x01e3:
                    r11 = 0
                L_0x01e4:
                    java.lang.Boolean r11 = com.fossil.pb7.a(r11)
                    boolean r11 = r11.booleanValue()
                    if (r11 == 0) goto L_0x01c0
                    r7.add(r10)
                    goto L_0x01c0
                L_0x01f2:
                    java.util.ArrayList r8 = new java.util.ArrayList
                    r10 = 10
                    int r11 = com.fossil.x97.a(r7, r10)
                    r8.<init>(r11)
                    java.util.Iterator r7 = r7.iterator()
                L_0x0201:
                    boolean r11 = r7.hasNext()
                    if (r11 == 0) goto L_0x0217
                    java.lang.Object r11 = r7.next()
                    com.fossil.v87 r11 = (com.fossil.v87) r11
                    java.lang.Object r11 = r11.getSecond()
                    com.fossil.r87 r11 = (com.fossil.r87) r11
                    r8.add(r11)
                    goto L_0x0201
                L_0x0217:
                    java.util.ArrayList r7 = new java.util.ArrayList
                    int r11 = com.fossil.x97.a(r8, r10)
                    r7.<init>(r11)
                    java.util.Iterator r8 = r8.iterator()
                L_0x0224:
                    boolean r11 = r8.hasNext()
                    if (r11 == 0) goto L_0x0242
                    java.lang.Object r11 = r8.next()
                    com.fossil.r87 r11 = (com.fossil.r87) r11
                    java.lang.Object r11 = r11.getFirst()
                    java.lang.Number r11 = (java.lang.Number) r11
                    int r11 = r11.intValue()
                    java.lang.Integer r11 = com.fossil.pb7.a(r11)
                    r7.add(r11)
                    goto L_0x0224
                L_0x0242:
                    r11 = 0
                    java.lang.Integer r8 = com.fossil.pb7.a(r11)
                    boolean r7 = r7.contains(r8)
                    java.lang.String r8 = "12a"
                    if (r7 != 0) goto L_0x0283
                    com.fossil.hc6$d r7 = r9.this$0
                    com.fossil.hc6 r7 = r7.a
                    java.lang.Float r12 = com.fossil.pb7.a(r2)
                    java.lang.String r7 = r7.a(r12)
                    com.fossil.v87 r12 = new com.fossil.v87
                    java.lang.Integer r13 = com.fossil.pb7.a(r11)
                    com.fossil.r87 r14 = new com.fossil.r87
                    java.lang.Integer r15 = com.fossil.pb7.a(r11)
                    java.lang.Float r2 = com.fossil.pb7.a(r2)
                    r14.<init>(r15, r2)
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    r2.append(r8)
                    r2.append(r7)
                    java.lang.String r2 = r2.toString()
                    r12.<init>(r13, r14, r2)
                    r0.add(r11, r12)
                L_0x0283:
                    java.util.ArrayList r2 = new java.util.ArrayList
                    r2.<init>()
                    java.util.Iterator r7 = r0.iterator()
                L_0x028c:
                    boolean r12 = r7.hasNext()
                    if (r12 == 0) goto L_0x02be
                    java.lang.Object r12 = r7.next()
                    r13 = r12
                    com.fossil.v87 r13 = (com.fossil.v87) r13
                    java.lang.Object r13 = r13.getSecond()
                    com.fossil.r87 r13 = (com.fossil.r87) r13
                    java.lang.Object r13 = r13.getSecond()
                    java.lang.Number r13 = (java.lang.Number) r13
                    float r13 = r13.floatValue()
                    int r13 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                    if (r13 != 0) goto L_0x02af
                    r13 = 1
                    goto L_0x02b0
                L_0x02af:
                    r13 = 0
                L_0x02b0:
                    java.lang.Boolean r13 = com.fossil.pb7.a(r13)
                    boolean r13 = r13.booleanValue()
                    if (r13 == 0) goto L_0x028c
                    r2.add(r12)
                    goto L_0x028c
                L_0x02be:
                    java.util.ArrayList r3 = new java.util.ArrayList
                    int r7 = com.fossil.x97.a(r2, r10)
                    r3.<init>(r7)
                    java.util.Iterator r2 = r2.iterator()
                L_0x02cb:
                    boolean r7 = r2.hasNext()
                    if (r7 == 0) goto L_0x02e1
                    java.lang.Object r7 = r2.next()
                    com.fossil.v87 r7 = (com.fossil.v87) r7
                    java.lang.Object r7 = r7.getSecond()
                    com.fossil.r87 r7 = (com.fossil.r87) r7
                    r3.add(r7)
                    goto L_0x02cb
                L_0x02e1:
                    java.util.ArrayList r2 = new java.util.ArrayList
                    int r7 = com.fossil.x97.a(r3, r10)
                    r2.<init>(r7)
                    java.util.Iterator r3 = r3.iterator()
                L_0x02ee:
                    boolean r7 = r3.hasNext()
                    if (r7 == 0) goto L_0x030c
                    java.lang.Object r7 = r3.next()
                    com.fossil.r87 r7 = (com.fossil.r87) r7
                    java.lang.Object r7 = r7.getFirst()
                    java.lang.Number r7 = (java.lang.Number) r7
                    int r7 = r7.intValue()
                    java.lang.Integer r7 = com.fossil.pb7.a(r7)
                    r2.add(r7)
                    goto L_0x02ee
                L_0x030c:
                    r3 = 24
                    java.lang.Integer r7 = com.fossil.pb7.a(r3)
                    boolean r2 = r2.contains(r7)
                    if (r2 != 0) goto L_0x034e
                    com.fossil.hc6$d r2 = r9.this$0
                    com.fossil.hc6 r2 = r2.a
                    java.lang.Float r7 = com.fossil.pb7.a(r5)
                    java.lang.String r2 = r2.a(r7)
                    com.fossil.v87 r7 = new com.fossil.v87
                    int r10 = r6 + 1440
                    java.lang.Integer r10 = com.fossil.pb7.a(r10)
                    com.fossil.r87 r11 = new com.fossil.r87
                    java.lang.Integer r3 = com.fossil.pb7.a(r3)
                    java.lang.Float r5 = com.fossil.pb7.a(r5)
                    r11.<init>(r3, r5)
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    r3.append(r8)
                    r3.append(r2)
                    java.lang.String r2 = r3.toString()
                    r7.<init>(r10, r11, r2)
                    r0.add(r7)
                L_0x034e:
                    r13 = r6
                    goto L_0x0355
                L_0x0350:
                    r11 = 0
                    r0.clear()
                    r13 = 0
                L_0x0355:
                    int r13 = r13 + 1440
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r5 = "minutesOfDay="
                    r3.append(r5)
                    r3.append(r13)
                    java.lang.String r3 = r3.toString()
                    r2.d(r4, r3)
                    com.fossil.hc6$d r2 = r9.this$0
                    com.fossil.hc6 r2 = r2.a
                    com.fossil.fc6 r2 = r2.i
                    r2.a(r13, r1, r0)
                    com.fossil.i97 r0 = com.fossil.i97.a
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.hc6.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public d(hc6 hc6) {
            this.a = hc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<HeartRateSample>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSamples -- heartRateSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, list, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<qx6<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ hc6 a;

        @DexIgnore
        public e(hc6 hc6) {
            this.a = hc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<WorkoutSession>> qx6) {
            lb5 a2 = qx6.a();
            List<WorkoutSession> list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == lb5.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.i.a(false, new ArrayList());
            } else {
                this.a.i.a(true, list);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public hc6(fc6 fc6, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        ee7.b(fc6, "mView");
        ee7.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.i = fc6;
        this.j = heartRateSampleRepository;
        this.k = workoutSessionRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<qx6<List<HeartRateSample>>> b2 = ge.b(mutableLiveData, new b(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = b2;
        LiveData<qx6<List<WorkoutSession>>> b3 = ge.b(this.f, new c(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.h = b3;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        try {
            LiveData<qx6<List<HeartRateSample>>> liveData = this.g;
            fc6 fc6 = this.i;
            if (fc6 != null) {
                liveData.a((gc6) fc6);
                this.h.a((LifecycleOwner) this.i);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    public void h() {
        this.i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !zd5.w(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewDayPresenter", "loadData - mDate=" + this.e);
        LiveData<qx6<List<HeartRateSample>>> liveData = this.g;
        fc6 fc6 = this.i;
        if (fc6 != null) {
            liveData.a((gc6) fc6, new d(this));
            this.h.a((LifecycleOwner) this.i, new e(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        we7 we7 = we7.a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }
}
