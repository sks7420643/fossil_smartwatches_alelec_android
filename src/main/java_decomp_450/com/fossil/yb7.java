package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yb7 extends xb7 implements be7<Object> {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public yb7(int i, fb7<Object> fb7) {
        super(fb7);
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.be7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public String toString() {
        if (getCompletion() != null) {
            return super.toString();
        }
        String a = te7.a(this);
        ee7.a((Object) a, "Reflection.renderLambdaToString(this)");
        return a;
    }

    @DexIgnore
    public yb7(int i) {
        this(i, null);
    }
}
