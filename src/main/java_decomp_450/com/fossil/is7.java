package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class is7 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map longOpts; // = new HashMap();
    @DexIgnore
    public Map optionGroups; // = new HashMap();
    @DexIgnore
    public List requiredOpts; // = new ArrayList();
    @DexIgnore
    public Map shortOpts; // = new HashMap();

    @DexIgnore
    public is7 addOption(String str, boolean z, String str2) {
        addOption(str, null, z, str2);
        return this;
    }

    @DexIgnore
    public is7 addOptionGroup(gs7 gs7) {
        if (gs7.isRequired()) {
            this.requiredOpts.add(gs7);
        }
        for (fs7 fs7 : gs7.getOptions()) {
            fs7.setRequired(false);
            addOption(fs7);
            this.optionGroups.put(fs7.getKey(), gs7);
        }
        return this;
    }

    @DexIgnore
    public fs7 getOption(String str) {
        String b = os7.b(str);
        if (this.shortOpts.containsKey(b)) {
            return (fs7) this.shortOpts.get(b);
        }
        return (fs7) this.longOpts.get(b);
    }

    @DexIgnore
    public gs7 getOptionGroup(fs7 fs7) {
        return (gs7) this.optionGroups.get(fs7.getKey());
    }

    @DexIgnore
    public Collection getOptionGroups() {
        return new HashSet(this.optionGroups.values());
    }

    @DexIgnore
    public Collection getOptions() {
        return Collections.unmodifiableCollection(helpOptions());
    }

    @DexIgnore
    public List getRequiredOptions() {
        return this.requiredOpts;
    }

    @DexIgnore
    public boolean hasOption(String str) {
        String b = os7.b(str);
        return this.shortOpts.containsKey(b) || this.longOpts.containsKey(b);
    }

    @DexIgnore
    public List helpOptions() {
        return new ArrayList(this.shortOpts.values());
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[ Options: [ short ");
        stringBuffer.append(this.shortOpts.toString());
        stringBuffer.append(" ] [ long ");
        stringBuffer.append(this.longOpts);
        stringBuffer.append(" ]");
        return stringBuffer.toString();
    }

    @DexIgnore
    public is7 addOption(String str, String str2, boolean z, String str3) {
        addOption(new fs7(str, str2, z, str3));
        return this;
    }

    @DexIgnore
    public is7 addOption(fs7 fs7) {
        String key = fs7.getKey();
        if (fs7.hasLongOpt()) {
            this.longOpts.put(fs7.getLongOpt(), fs7);
        }
        if (fs7.isRequired()) {
            if (this.requiredOpts.contains(key)) {
                List list = this.requiredOpts;
                list.remove(list.indexOf(key));
            }
            this.requiredOpts.add(key);
        }
        this.shortOpts.put(key, fs7);
        return this;
    }
}
