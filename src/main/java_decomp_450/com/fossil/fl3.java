package com.fossil;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.fossil.jl3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl3<T extends Context & jl3> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public fl3(T t) {
        a72.a(t);
        this.a = t;
    }

    @DexIgnore
    public final void a() {
        oh3 a2 = oh3.a(this.a, null, null);
        jg3 e = a2.e();
        a2.b();
        e.B().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final void b() {
        oh3 a2 = oh3.a(this.a, null, null);
        jg3 e = a2.e();
        a2.b();
        e.B().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final void c(Intent intent) {
        if (intent == null) {
            c().t().a("onRebind called with null intent");
            return;
        }
        c().B().a("onRebind called. action", intent.getAction());
    }

    @DexIgnore
    public final jg3 c() {
        return oh3.a(this.a, null, null).e();
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        oh3 a2 = oh3.a(this.a, null, null);
        jg3 e = a2.e();
        if (intent == null) {
            e.w().a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a2.b();
        e.B().a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            a(new el3(this, i2, e, intent));
        }
        return 2;
    }

    @DexIgnore
    public final boolean b(Intent intent) {
        if (intent == null) {
            c().t().a("onUnbind called with null intent");
            return true;
        }
        c().B().a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        xl3 a2 = xl3.a(this.a);
        a2.c().a(new gl3(this, a2, runnable));
    }

    @DexIgnore
    public final IBinder a(Intent intent) {
        if (intent == null) {
            c().t().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new ph3(xl3.a(this.a));
        }
        c().w().a("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean a(JobParameters jobParameters) {
        oh3 a2 = oh3.a(this.a, null, null);
        jg3 e = a2.e();
        String string = jobParameters.getExtras().getString("action");
        a2.b();
        e.B().a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a(new hl3(this, e, jobParameters));
        return true;
    }

    @DexIgnore
    public final /* synthetic */ void a(jg3 jg3, JobParameters jobParameters) {
        jg3.B().a("AppMeasurementJobService processed last upload request.");
        this.a.a(jobParameters, false);
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, jg3 jg3, Intent intent) {
        if (this.a.zza(i)) {
            jg3.B().a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            c().B().a("Completed wakeful intent.");
            this.a.a(intent);
        }
    }
}
