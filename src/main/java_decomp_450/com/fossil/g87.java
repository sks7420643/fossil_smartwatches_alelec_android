package com.fossil;

import io.flutter.util.Predicate;
import io.flutter.view.AccessibilityBridge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class g87 implements Predicate {
    @DexIgnore
    private /* final */ /* synthetic */ AccessibilityBridge.SemanticsNode a;

    @DexIgnore
    public /* synthetic */ g87(AccessibilityBridge.SemanticsNode semanticsNode) {
        this.a = semanticsNode;
    }

    @DexIgnore
    @Override // io.flutter.util.Predicate
    public final boolean test(Object obj) {
        return AccessibilityBridge.a(this.a, (AccessibilityBridge.SemanticsNode) obj);
    }
}
