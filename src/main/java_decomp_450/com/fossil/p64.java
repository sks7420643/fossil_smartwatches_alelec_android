package com.fossil;

import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p64 implements FileFilter {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public p64(String str) {
        this.a = str;
    }

    @DexIgnore
    public static FileFilter a(String str) {
        return new p64(str);
    }

    @DexIgnore
    public boolean accept(File file) {
        return u64.a(this.a, file);
    }
}
