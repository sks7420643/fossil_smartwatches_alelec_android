package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t71 extends xp0 {
    @DexIgnore
    public static /* final */ v51 CREATOR; // = new v51(null);
    @DexIgnore
    public /* final */ wf0 d;
    @DexIgnore
    public /* final */ vf0 e;
    @DexIgnore
    public /* final */ yf0 f;
    @DexIgnore
    public /* final */ byte[] g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore
    public t71(byte b, wf0 wf0, vf0 vf0, yf0 yf0, byte[] bArr, int i2, int i3) {
        super(ru0.ENCRYPTED_DATA, b, true);
        this.d = wf0;
        this.e = vf0;
        this.f = yf0;
        this.g = bArr;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(super.a(), r51.h0, yz0.a(this.d)), r51.j5, yz0.a(this.e)), r51.k5, yz0.a(this.f)), r51.S0, Long.valueOf(ik1.a.a(this.g, ng1.CRC32))), r51.y5, Integer.valueOf(this.h)), r51.z5, Integer.valueOf(this.i));
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.g);
        }
        if (parcel != null) {
            parcel.writeInt(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i);
        }
    }

    @DexIgnore
    public t71(Parcel parcel) {
        super(parcel);
        wf0 a = wf0.c.a(parcel.readByte());
        if (a != null) {
            this.d = a;
            vf0 a2 = vf0.c.a(parcel.readByte());
            if (a2 != null) {
                this.e = a2;
                yf0 a3 = yf0.c.a(parcel.readByte());
                if (a3 != null) {
                    this.f = a3;
                    byte[] createByteArray = parcel.createByteArray();
                    this.g = createByteArray == null ? new byte[0] : createByteArray;
                    this.h = parcel.readInt();
                    this.i = parcel.readInt();
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }
}
