package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum f31 {
    PRE_SHARED_KEY((byte) 0),
    SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY((byte) 1),
    SIXTEEN_BYTES_LSB_ECDH_SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ n11 f; // = new n11(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public f31(byte b) {
        this.a = b;
    }
}
