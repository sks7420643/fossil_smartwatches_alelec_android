package com.fossil;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b30 implements f30<Bitmap, byte[]> {
    @DexIgnore
    public /* final */ Bitmap.CompressFormat a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public b30() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    @Override // com.fossil.f30
    public uy<byte[]> a(uy<Bitmap> uyVar, ax axVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        uyVar.get().compress(this.a, this.b, byteArrayOutputStream);
        uyVar.b();
        return new j20(byteArrayOutputStream.toByteArray());
    }

    @DexIgnore
    public b30(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }
}
