package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bb7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ gd7[] a;

        @DexIgnore
        public a(gd7[] gd7Arr) {
            this.a = gd7Arr;
        }

        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.b(t, t2, this.a);
        }
    }

    @DexIgnore
    public static final <T> int b(T t, T t2, gd7<? super T, ? extends Comparable<?>>[] gd7Arr) {
        for (gd7<? super T, ? extends Comparable<?>> gd7 : gd7Arr) {
            int a2 = a((Comparable) gd7.invoke(t), (Comparable) gd7.invoke(t2));
            if (a2 != 0) {
                return a2;
            }
        }
        return 0;
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> Comparator<T> a(gd7<? super T, ? extends Comparable<?>>... gd7Arr) {
        ee7.b(gd7Arr, "selectors");
        if (gd7Arr.length > 0) {
            return new a(gd7Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
