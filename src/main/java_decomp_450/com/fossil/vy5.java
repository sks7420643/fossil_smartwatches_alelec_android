package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy5 extends go5 implements uy5, cy6.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public ty5 f;
    @DexIgnore
    public qw6<e45> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vy5.i;
        }

        @DexIgnore
        public final vy5 b() {
            return new vy5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vy5 a;

        @DexIgnore
        public b(vy5 vy5) {
            this.a = vy5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vy5.a(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vy5 a;

        @DexIgnore
        public c(vy5 vy5) {
            this.a = vy5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vy5.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vy5 a;

        @DexIgnore
        public d(vy5 vy5) {
            this.a = vy5;
        }

        @DexIgnore
        public final void onClick(View view) {
            vy5.a(this.a).j();
        }
    }

    /*
    static {
        String simpleName = vy5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ty5 a(vy5 vy5) {
        ty5 ty5 = vy5.f;
        if (ty5 != null) {
            return ty5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ku7.a
    public void b(int i2, List<String> list) {
        ee7.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(i, ".Inside onPermissionsGranted");
        ty5 ty5 = this.f;
        if (ty5 != null) {
            ty5.l();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        ty5 ty5 = this.f;
        if (ty5 != null) {
            ty5.k();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        e45 e45 = (e45) qb.a(layoutInflater, 2131558594, viewGroup, false, a1());
        e45.x.setOnClickListener(new b(this));
        e45.q.setOnClickListener(new c(this));
        e45.r.setOnClickListener(new d(this));
        String b2 = eh5.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            e45.y.setBackgroundColor(Color.parseColor(b2));
        }
        this.g = new qw6<>(this, e45);
        ee7.a((Object) e45, "binding");
        return e45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ty5 ty5 = this.f;
        if (ty5 != null) {
            ty5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ty5 ty5 = this.f;
        if (ty5 != null) {
            ty5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ty5 ty5) {
        ee7.b(ty5, "presenter");
        this.f = ty5;
    }

    @DexIgnore
    @Override // com.fossil.uy5
    public void b(List<bt5> list, int i2) {
        FlexibleCheckBox flexibleCheckBox;
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        FlexibleCheckBox flexibleCheckBox2;
        FlexibleTextView flexibleTextView3;
        FlexibleTextView flexibleTextView4;
        FlexibleCheckBox flexibleCheckBox3;
        FlexibleTextView flexibleTextView5;
        FlexibleTextView flexibleTextView6;
        FlexibleCheckBox flexibleCheckBox4;
        FlexibleTextView flexibleTextView7;
        FlexibleTextView flexibleTextView8;
        FlexibleCheckBox flexibleCheckBox5;
        FlexibleTextView flexibleTextView9;
        FlexibleTextView flexibleTextView10;
        FlexibleCheckBox flexibleCheckBox6;
        FlexibleTextView flexibleTextView11;
        FlexibleTextView flexibleTextView12;
        ee7.b(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (T t : list) {
            Contact contact = t.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = t.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (t.getCurrentHandGroup() != i2) {
                        qw6<e45> qw6 = this.g;
                        if (qw6 != null) {
                            e45 a2 = qw6.a();
                            if (!(a2 == null || (flexibleTextView8 = a2.v) == null)) {
                                we7 we7 = we7.a;
                                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886157);
                                ee7.a((Object) a3, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                String format = String.format(a3, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                flexibleTextView8.setText(format);
                            }
                            qw6<e45> qw62 = this.g;
                            if (qw62 != null) {
                                e45 a4 = qw62.a();
                                if (!(a4 == null || (flexibleTextView7 = a4.v) == null)) {
                                    flexibleTextView7.setVisibility(0);
                                }
                                qw6<e45> qw63 = this.g;
                                if (qw63 != null) {
                                    e45 a5 = qw63.a();
                                    if (!(a5 == null || (flexibleCheckBox4 = a5.r) == null)) {
                                        flexibleCheckBox4.setChecked(false);
                                    }
                                } else {
                                    ee7.d("mBinding");
                                    throw null;
                                }
                            } else {
                                ee7.d("mBinding");
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    } else {
                        qw6<e45> qw64 = this.g;
                        if (qw64 != null) {
                            e45 a6 = qw64.a();
                            if (!(a6 == null || (flexibleTextView6 = a6.v) == null)) {
                                flexibleTextView6.setText("");
                            }
                            qw6<e45> qw65 = this.g;
                            if (qw65 != null) {
                                e45 a7 = qw65.a();
                                if (!(a7 == null || (flexibleTextView5 = a7.v) == null)) {
                                    flexibleTextView5.setVisibility(8);
                                }
                                qw6<e45> qw66 = this.g;
                                if (qw66 != null) {
                                    e45 a8 = qw66.a();
                                    if (!(a8 == null || (flexibleCheckBox3 = a8.r) == null)) {
                                        flexibleCheckBox3.setChecked(true);
                                    }
                                } else {
                                    ee7.d("mBinding");
                                    throw null;
                                }
                            } else {
                                ee7.d("mBinding");
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    }
                    z2 = true;
                }
            } else {
                if (t.getCurrentHandGroup() != i2) {
                    qw6<e45> qw67 = this.g;
                    if (qw67 != null) {
                        e45 a9 = qw67.a();
                        if (!(a9 == null || (flexibleTextView12 = a9.u) == null)) {
                            we7 we72 = we7.a;
                            String a10 = ig5.a(PortfolioApp.g0.c(), 2131886157);
                            ee7.a((Object) a10, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                            String format2 = String.format(a10, Arrays.copyOf(new Object[]{Integer.valueOf(t.getCurrentHandGroup())}, 1));
                            ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                            flexibleTextView12.setText(format2);
                        }
                        qw6<e45> qw68 = this.g;
                        if (qw68 != null) {
                            e45 a11 = qw68.a();
                            if (!(a11 == null || (flexibleTextView11 = a11.u) == null)) {
                                flexibleTextView11.setVisibility(0);
                            }
                            qw6<e45> qw69 = this.g;
                            if (qw69 != null) {
                                e45 a12 = qw69.a();
                                if (!(a12 == null || (flexibleCheckBox6 = a12.q) == null)) {
                                    flexibleCheckBox6.setChecked(false);
                                }
                            } else {
                                ee7.d("mBinding");
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    } else {
                        ee7.d("mBinding");
                        throw null;
                    }
                } else {
                    qw6<e45> qw610 = this.g;
                    if (qw610 != null) {
                        e45 a13 = qw610.a();
                        if (!(a13 == null || (flexibleTextView10 = a13.u) == null)) {
                            flexibleTextView10.setText("");
                        }
                        qw6<e45> qw611 = this.g;
                        if (qw611 != null) {
                            e45 a14 = qw611.a();
                            if (!(a14 == null || (flexibleTextView9 = a14.u) == null)) {
                                flexibleTextView9.setVisibility(8);
                            }
                            qw6<e45> qw612 = this.g;
                            if (qw612 != null) {
                                e45 a15 = qw612.a();
                                if (!(a15 == null || (flexibleCheckBox5 = a15.q) == null)) {
                                    flexibleCheckBox5.setChecked(true);
                                }
                            } else {
                                ee7.d("mBinding");
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    } else {
                        ee7.d("mBinding");
                        throw null;
                    }
                }
                z = true;
            }
        }
        if (!z) {
            qw6<e45> qw613 = this.g;
            if (qw613 != null) {
                e45 a16 = qw613.a();
                if (!(a16 == null || (flexibleTextView4 = a16.u) == null)) {
                    flexibleTextView4.setText("");
                }
                qw6<e45> qw614 = this.g;
                if (qw614 != null) {
                    e45 a17 = qw614.a();
                    if (!(a17 == null || (flexibleTextView3 = a17.u) == null)) {
                        flexibleTextView3.setVisibility(8);
                    }
                    qw6<e45> qw615 = this.g;
                    if (qw615 != null) {
                        e45 a18 = qw615.a();
                        if (!(a18 == null || (flexibleCheckBox2 = a18.q) == null)) {
                            flexibleCheckBox2.setChecked(false);
                        }
                    } else {
                        ee7.d("mBinding");
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
        if (!z2) {
            qw6<e45> qw616 = this.g;
            if (qw616 != null) {
                e45 a19 = qw616.a();
                if (!(a19 == null || (flexibleTextView2 = a19.v) == null)) {
                    flexibleTextView2.setText("");
                }
                qw6<e45> qw617 = this.g;
                if (qw617 != null) {
                    e45 a20 = qw617.a();
                    if (!(a20 == null || (flexibleTextView = a20.v) == null)) {
                        flexibleTextView.setVisibility(8);
                    }
                    qw6<e45> qw618 = this.g;
                    if (qw618 != null) {
                        e45 a21 = qw618.a();
                        if (a21 != null && (flexibleCheckBox = a21.r) != null) {
                            flexibleCheckBox.setChecked(false);
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.uy5
    public void a(bt5 bt5) {
        ee7.b(bt5, "contactWrapper");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            int currentHandGroup = bt5.getCurrentHandGroup();
            ty5 ty5 = this.f;
            if (ty5 != null) {
                bx6.a(childFragmentManager, bt5, currentHandGroup, ty5.h());
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.uy5
    public void a(ArrayList<bt5> arrayList) {
        ee7.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        bt5 bt5;
        ee7.b(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i2 == 2131363307) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (bt5 = (bt5) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                ty5 ty5 = this.f;
                if (ty5 != null) {
                    ty5.a(bt5);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ku7.a
    public void a(int i2, List<String> list) {
        ee7.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(i, ".Inside onPermissionsDenied");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Permission Denied : " + it.next());
        }
        if (ku7.a(this, list) && isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.K(childFragmentManager);
        }
    }
}
