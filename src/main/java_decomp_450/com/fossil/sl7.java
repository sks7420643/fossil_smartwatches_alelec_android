package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sl7<T> {
    @DexIgnore
    public Object[] a; // = new Object[16];
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public final void a(T t) {
        Object[] objArr = this.a;
        int i = this.c;
        objArr[i] = t;
        int length = (objArr.length - 1) & (i + 1);
        this.c = length;
        if (length == this.b) {
            a();
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.b == this.c;
    }

    @DexIgnore
    public final T c() {
        int i = this.b;
        if (i == this.c) {
            return null;
        }
        Object[] objArr = this.a;
        T t = (T) objArr[i];
        objArr[i] = null;
        this.b = (i + 1) & (objArr.length - 1);
        if (t != null) {
            return t;
        }
        throw new x87("null cannot be cast to non-null type T");
    }

    @DexIgnore
    public final void a() {
        Object[] objArr = this.a;
        int length = objArr.length;
        Object[] objArr2 = new Object[(length << 1)];
        s97.a(objArr, objArr2, 0, this.b, 0, 10, (Object) null);
        Object[] objArr3 = this.a;
        int length2 = objArr3.length;
        int i = this.b;
        s97.a(objArr3, objArr2, length2 - i, 0, i, 4, (Object) null);
        this.a = objArr2;
        this.b = 0;
        this.c = length;
    }
}
