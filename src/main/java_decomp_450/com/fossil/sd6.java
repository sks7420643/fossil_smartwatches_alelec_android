package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sd6 implements Factory<ae6> {
    @DexIgnore
    public static ae6 a(pd6 pd6) {
        ae6 c = pd6.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
