package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fw4 extends ew4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362517, 1);
        y.put(2131362179, 2);
        y.put(2131362559, 3);
        y.put(2131362809, 4);
        y.put(2131361969, 5);
    }
    */

    @DexIgnore
    public fw4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 6, x, y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public fw4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[5], (ConstraintLayout) objArr[0], (NumberPicker) objArr[2], (FlexibleTextView) objArr[1], (NumberPicker) objArr[3], (NumberPicker) objArr[4]);
        this.w = -1;
        ((ew4) this).r.setTag(null);
        a(view);
        f();
    }
}
