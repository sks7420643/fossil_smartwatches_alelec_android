package com.fossil;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h50 implements yw {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ yw c;

    @DexIgnore
    public h50(int i, yw ywVar) {
        this.b = i;
        this.c = ywVar;
    }

    @DexIgnore
    public static yw a(Context context) {
        return new h50(context.getResources().getConfiguration().uiMode & 48, i50.b(context));
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (!(obj instanceof h50)) {
            return false;
        }
        h50 h50 = (h50) obj;
        if (this.b != h50.b || !this.c.equals(h50.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return v50.a(this.c, this.b);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }
}
