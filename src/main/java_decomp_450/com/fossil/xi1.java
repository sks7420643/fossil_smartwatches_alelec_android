package com.fossil;

import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi1 extends fe7 implements gd7<i97, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;
    @DexIgnore
    public /* final */ /* synthetic */ xp0 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xi1(km1 km1, xp0 xp0) {
        super(1);
        this.a = km1;
        this.b = xp0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(i97 i97) {
        km1 km1 = this.a;
        xp0 xp0 = this.b;
        byte b2 = xp0.b;
        l01 l01 = (l01) xp0;
        hb0 hb0 = new hb0(b2, l01.d, l01.e);
        l60.b A = km1.A();
        if (A != null) {
            A.onEventReceived(km1, hb0);
        }
        return i97.a;
    }
}
