package com.fossil;

import com.fossil.c12;
import com.fossil.z62;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f82 implements c12.a {
    @DexIgnore
    public /* final */ /* synthetic */ c12 a;
    @DexIgnore
    public /* final */ /* synthetic */ oo3 b;
    @DexIgnore
    public /* final */ /* synthetic */ z62.a c;
    @DexIgnore
    public /* final */ /* synthetic */ z62.b d;

    @DexIgnore
    public f82(c12 c12, oo3 oo3, z62.a aVar, z62.b bVar) {
        this.a = c12;
        this.b = oo3;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    @Override // com.fossil.c12.a
    public final void a(Status status) {
        if (status.y()) {
            this.b.a(this.c.a(this.a.a(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.a((Exception) this.d.a(status));
    }
}
