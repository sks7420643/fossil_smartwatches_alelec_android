package com.fossil;

import java.util.Collection;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j60 extends i60 {
    @DexIgnore
    public /* final */ HashMap<String, i60> a; // = new HashMap<>();

    @DexIgnore
    @Override // com.fossil.i60
    public boolean a(km1 km1) {
        for (i60 i60 : this.a.values()) {
            if (!i60.a(km1)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final j60 setDeviceTypes(n60[] n60Arr) {
        HashMap<String, i60> hashMap = this.a;
        String simpleName = xu0.class.getSimpleName();
        ee7.a((Object) simpleName, "DeviceTypesScanFilter::class.java.simpleName");
        hashMap.put(simpleName, new xu0(n60Arr));
        return this;
    }

    @DexIgnore
    public final j60 setSerialNumberPattern(String str) {
        HashMap<String, i60> hashMap = this.a;
        String simpleName = pw0.class.getSimpleName();
        ee7.a((Object) simpleName, "SerialNumberPatternScanF\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new pw0(str));
        return this;
    }

    @DexIgnore
    public final j60 setSerialNumberPrefixes(String[] strArr) {
        HashMap<String, i60> hashMap = this.a;
        String simpleName = hy0.class.getSimpleName();
        ee7.a((Object) simpleName, "SerialNumberPrefixesScan\u2026er::class.java.simpleName");
        hashMap.put(simpleName, new hy0(strArr));
        return this;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        r51 r51 = r51.K1;
        Collection<i60> values = this.a.values();
        ee7.a((Object) values, "deviceFilters.values");
        Object[] array = values.toArray(new i60[0]);
        if (array != null) {
            return yz0.a(jSONObject, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
