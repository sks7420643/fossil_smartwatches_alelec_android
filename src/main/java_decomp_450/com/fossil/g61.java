package com.fossil;

import com.fossil.crypto.EllipticCurveKeyPair;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g61 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.AUTHENTICATION}));
    @DexIgnore
    public byte[] D;
    @DexIgnore
    public /* final */ EllipticCurveKeyPair E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public g61(ri1 ri1, en0 en0, byte[] bArr) {
        super(ri1, en0, wm0.Q, null, false, 24);
        this.F = bArr;
        EllipticCurveKeyPair create = EllipticCurveKeyPair.create();
        ee7.a((Object) create, "EllipticCurveKeyPair.create()");
        this.E = create;
    }

    @DexIgnore
    public static final /* synthetic */ void a(g61 g61) {
        ri1 ri1 = ((zk0) g61).w;
        byte[] publicKey = g61.E.publicKey();
        ee7.a((Object) publicKey, "keyPair.publicKey()");
        zk0.a(g61, new nl0(ri1, publicKey), new ez0(g61), new w01(g61), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        byte[] bArr = this.D;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        byte[] bArr = this.F;
        if (bArr.length != 16) {
            a(is0.INVALID_PARAMETER);
        } else {
            zk0.a(this, new jn0(((zk0) this).w, f31.PRE_SHARED_KEY, bArr), new o21(this), new j41(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.o2, yz0.a(this.F, (String) null, 1));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        byte[] bArr = this.D;
        if (bArr != null) {
            yz0.a(k, r51.r2, Long.valueOf(ik1.a.a(bArr, ng1.CRC32)));
        }
        return k;
    }

    @DexIgnore
    public final void m() {
        byte[] bArr = this.D;
        if (bArr != null) {
            zk0.a(this, new um1(((zk0) this).w, ((zk0) this).x, b21.x.b(), s97.a(bArr, 0, 16), ((zk0) this).z), uv0.a, mx0.a, (kd7) null, new au0(this), (gd7) null, 40, (Object) null);
        } else {
            a(eu0.a(((zk0) this).v, null, is0.FLOW_BROKEN, null, 5));
        }
    }
}
