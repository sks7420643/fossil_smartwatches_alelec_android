package com.fossil;

import android.util.AndroidRuntimeException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc extends AndroidRuntimeException {
    @DexIgnore
    public sc(String str) {
        super(str);
    }
}
