package com.fossil;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ jm4 b;
    @DexIgnore
    public /* final */ km4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$fetchFlags$2", f = "FlagRepository.kt", l = {24}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends nm4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $agent;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $flags;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lm4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lm4$a$a")
        /* renamed from: com.fossil.lm4$a$a  reason: collision with other inner class name */
        public static final class C0112a extends TypeToken<List<? extends nm4>> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lm4 lm4, String str, String str2, String[] strArr, fb7 fb7) {
            super(2, fb7);
            this.this$0 = lm4;
            this.$serialNumber = str;
            this.$agent = str2;
            this.$flags = strArr;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$serialNumber, this.$agent, this.$flags, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends nm4>>> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                km4 b = this.this$0.c;
                String str = this.$serialNumber;
                String str2 = this.$agent;
                String[] strArr = this.$flags;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(str, str2, strArr, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            zi5 zi5 = (zi5) obj;
            String str3 = null;
            if (zi5 instanceof bj5) {
                ie4 ie4 = (ie4) ((bj5) zi5).a();
                if (ie4 == null) {
                    return new ko4(new ServerError(600, "success with empty result"));
                }
                try {
                    de4 b2 = ie4.b("flags");
                    Gson a2 = new be4().a();
                    ee7.a((Object) a2, "GsonBuilder().create()");
                    List<nm4> list = (List) a2.a((JsonElement) b2, new C0112a().getType());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String c = this.this$0.a;
                    local.e(c, "result: " + list);
                    jm4 a3 = this.this$0.b;
                    ee7.a((Object) list, "flagItems");
                    a3.a(list);
                    return new ko4(list, null, 2, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    return new ko4(new ServerError(600, "success with empty result"));
                }
            } else if (zi5 instanceof yi5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String c2 = this.this$0.a;
                StringBuilder sb = new StringBuilder();
                sb.append("fetchFlags - failed - ");
                yi5 yi5 = (yi5) zi5;
                sb.append(yi5.a());
                local2.e(c2, sb.toString());
                int a4 = yi5.a();
                ServerError c3 = yi5.c();
                if (c3 != null) {
                    str3 = c3.getMessage();
                }
                return new ko4(new ServerError(a4, str3));
            } else {
                throw new p87();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$isBcOn$2", f = "FlagRepository.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lm4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(lm4 lm4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = lm4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
            if (com.fossil.nh7.a((java.lang.CharSequence) r5, (java.lang.CharSequence) "on", false, 2, (java.lang.Object) null) != false) goto L_0x002c;
         */
        @DexIgnore
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r5) {
            /*
                r4 = this;
                com.fossil.nb7.a()
                int r0 = r4.label
                if (r0 != 0) goto L_0x0032
                com.fossil.t87.a(r5)
                com.fossil.lm4 r5 = r4.this$0
                com.fossil.gm4 r0 = com.fossil.gm4.BUDDY_CHALLENGE
                java.lang.String r0 = r0.getStrType()
                com.fossil.nm4 r5 = r5.a(r0)
                r0 = 0
                if (r5 == 0) goto L_0x002c
                java.lang.String r5 = r5.b()
                if (r5 == 0) goto L_0x0020
                goto L_0x0022
            L_0x0020:
                java.lang.String r5 = ""
            L_0x0022:
                r1 = 2
                r2 = 0
                java.lang.String r3 = "on"
                boolean r5 = com.fossil.nh7.a(r5, r3, r0, r1, r2)
                if (r5 == 0) goto L_0x002d
            L_0x002c:
                r0 = 1
            L_0x002d:
                java.lang.Boolean r5 = com.fossil.pb7.a(r0)
                return r5
            L_0x0032:
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r5.<init>(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.lm4.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public lm4(jm4 jm4, km4 km4) {
        ee7.b(jm4, "local");
        ee7.b(km4, "remote");
        this.b = jm4;
        this.c = km4;
        String simpleName = lm4.class.getSimpleName();
        ee7.a((Object) simpleName, "FlagRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final Object a(String str, String str2, String[] strArr, fb7<? super ko4<List<nm4>>> fb7) {
        return vh7.a(qj7.b(), new a(this, str, str2, strArr, null), fb7);
    }

    @DexIgnore
    public final Object a(fb7<? super Boolean> fb7) {
        return vh7.a(qj7.b(), new b(this, null), fb7);
    }

    @DexIgnore
    public final nm4 a(String str) {
        return this.b.a(str);
    }
}
