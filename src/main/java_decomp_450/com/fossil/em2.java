package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em2 implements Parcelable.Creator<dm2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dm2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        bm2 bm2 = null;
        IBinder iBinder = null;
        PendingIntent pendingIntent = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    bm2 = (bm2) j72.a(parcel, a, bm2.CREATOR);
                    break;
                case 3:
                    iBinder = j72.p(parcel, a);
                    break;
                case 4:
                    pendingIntent = (PendingIntent) j72.a(parcel, a, PendingIntent.CREATOR);
                    break;
                case 5:
                    iBinder2 = j72.p(parcel, a);
                    break;
                case 6:
                    iBinder3 = j72.p(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new dm2(i, bm2, iBinder, pendingIntent, iBinder2, iBinder3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ dm2[] newArray(int i) {
        return new dm2[i];
    }
}
