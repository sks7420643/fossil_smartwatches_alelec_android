package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj6 implements Factory<uj6> {
    @DexIgnore
    public static uj6 a(wj6 wj6) {
        uj6 a = wj6.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
