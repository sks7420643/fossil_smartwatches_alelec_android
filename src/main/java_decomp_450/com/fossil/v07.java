package com.fossil;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v07 extends s07 {
    @DexIgnore
    public v07(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.i17, com.fossil.s07
    public boolean a(g17 g17) {
        return "file".equals(g17.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.i17, com.fossil.s07
    public i17.a a(g17 g17, int i) throws IOException {
        return new i17.a(null, c(g17), Picasso.LoadedFrom.DISK, a(g17.d));
    }

    @DexIgnore
    public static int a(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return 180;
        }
        if (attributeInt != 6) {
            return attributeInt != 8 ? 0 : 270;
        }
        return 90;
    }
}
