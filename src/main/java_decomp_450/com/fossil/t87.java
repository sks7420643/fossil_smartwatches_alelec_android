package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t87 {
    @DexIgnore
    public static final Object a(Throwable th) {
        ee7.b(th, "exception");
        return new s87.b(th);
    }

    @DexIgnore
    public static final void a(Object obj) {
        if (obj instanceof s87.b) {
            throw ((s87.b) obj).exception;
        }
    }
}
