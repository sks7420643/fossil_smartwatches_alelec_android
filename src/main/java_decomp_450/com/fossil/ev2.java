package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ev2 extends bv2 {
    @DexIgnore
    public /* final */ byte[] zzb;

    @DexIgnore
    public ev2(byte[] bArr) {
        if (bArr != null) {
            this.zzb = bArr;
            return;
        }
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof tu2) || zza() != ((tu2) obj).zza()) {
            return false;
        }
        if (zza() == 0) {
            return true;
        }
        if (!(obj instanceof ev2)) {
            return obj.equals(this);
        }
        ev2 ev2 = (ev2) obj;
        int zzd = zzd();
        int zzd2 = ev2.zzd();
        if (zzd == 0 || zzd2 == 0 || zzd == zzd2) {
            return zza(ev2, 0, zza());
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public byte zza(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public byte zzb(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final boolean zzc() {
        int zze = zze();
        return dz2.a(this.zzb, zze, zza() + zze);
    }

    @DexIgnore
    public int zze() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public int zza() {
        return this.zzb.length;
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final tu2 zza(int i, int i2) {
        int zzb2 = tu2.zzb(0, i2, zza());
        if (zzb2 == 0) {
            return tu2.zza;
        }
        return new wu2(this.zzb, zze(), zzb2);
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final void zza(qu2 qu2) throws IOException {
        qu2.a(this.zzb, zze(), zza());
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final String zza(Charset charset) {
        return new String(this.zzb, zze(), zza(), charset);
    }

    @DexIgnore
    @Override // com.fossil.bv2
    public final boolean zza(tu2 tu2, int i, int i2) {
        if (i2 > tu2.zza()) {
            int zza = zza();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(zza);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > tu2.zza()) {
            int zza2 = tu2.zza();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(zza2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(tu2 instanceof ev2)) {
            return tu2.zza(0, i2).equals(zza(0, i2));
        } else {
            ev2 ev2 = (ev2) tu2;
            byte[] bArr = this.zzb;
            byte[] bArr2 = ev2.zzb;
            int zze = zze() + i2;
            int zze2 = zze();
            int zze3 = ev2.zze();
            while (zze2 < zze) {
                if (bArr[zze2] != bArr2[zze3]) {
                    return false;
                }
                zze2++;
                zze3++;
            }
            return true;
        }
    }

    @DexIgnore
    @Override // com.fossil.tu2
    public final int zza(int i, int i2, int i3) {
        return ew2.a(i, this.zzb, zze(), i3);
    }
}
