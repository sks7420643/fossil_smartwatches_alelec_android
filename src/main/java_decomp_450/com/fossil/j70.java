package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j70 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ i70 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ k70 c;
    @DexIgnore
    public /* final */ l70 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<j70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public j70 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                i70 valueOf = i70.valueOf(readString);
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    k70 valueOf2 = k70.valueOf(readString2);
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        ee7.a((Object) readString3, "parcel.readString()!!");
                        return new j70(valueOf, readInt, valueOf2, l70.valueOf(readString3));
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public j70[] newArray(int i) {
            return new j70[i];
        }
    }

    @DexIgnore
    public j70(i70 i70, int i, k70 k70, l70 l70) {
        this.a = i70;
        this.b = i;
        this.c = k70;
        this.d = l70;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.V0, yz0.a(this.a)), r51.W0, Integer.valueOf(this.b)), r51.X0, yz0.a(this.c)), r51.Y0, yz0.a(this.d));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] array = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a()).putShort((short) this.b).put(this.c.a()).put(this.d.a()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(j70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            j70 j70 = (j70) obj;
            return this.a == j70.a && this.b == j70.b && this.c == j70.c && this.d == j70.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.calibration.HandMovingConfig");
    }

    @DexIgnore
    public final int getDegree() {
        return this.b;
    }

    @DexIgnore
    public final i70 getHandId() {
        return this.a;
    }

    @DexIgnore
    public final k70 getMovingDirection() {
        return this.c;
    }

    @DexIgnore
    public final l70 getMovingSpeed() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
