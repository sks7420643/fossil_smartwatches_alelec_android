package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka1 extends fe7 implements kd7<byte[], qk1, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ec1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ka1(ec1 ec1) {
        super(2);
        this.a = ec1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(byte[] bArr, qk1 qk1) {
        byte[] bArr2 = bArr;
        qk1 qk12 = qk1;
        kd7<? super byte[], ? super qk1, i97> kd7 = this.a.D;
        if (kd7 != null) {
            kd7.invoke(bArr2, qk12);
        }
        return i97.a;
    }
}
