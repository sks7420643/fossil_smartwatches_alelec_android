package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rr5 extends gl5<qr5> {
    @DexIgnore
    void W();

    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    void a(FossilDeviceSerialPatternUtil.DEVICE device, int i, boolean z);

    @DexIgnore
    void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration);

    @DexIgnore
    void c(boolean z);

    @DexIgnore
    void f(int i);

    @DexIgnore
    void onActivityResult(int i, int i2, Intent intent);

    @DexIgnore
    void v0();
}
