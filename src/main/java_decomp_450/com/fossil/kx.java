package com.fossil;

import com.fossil.jx;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kx {
    @DexIgnore
    public static /* final */ jx.a<?> b; // = new a();
    @DexIgnore
    public /* final */ Map<Class<?>, jx.a<?>> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jx.a<Object> {
        @DexIgnore
        @Override // com.fossil.jx.a
        public jx<Object> a(Object obj) {
            return new b(obj);
        }

        @DexIgnore
        @Override // com.fossil.jx.a
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements jx<Object> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        @Override // com.fossil.jx
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.jx
        public Object b() {
            return this.a;
        }
    }

    @DexIgnore
    public synchronized void a(jx.a<?> aVar) {
        this.a.put(aVar.getDataClass(), aVar);
    }

    @DexIgnore
    public synchronized <T> jx<T> a(T t) {
        jx.a<?> aVar;
        u50.a((Object) t);
        aVar = this.a.get(t.getClass());
        if (aVar == null) {
            Iterator<jx.a<?>> it = this.a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                jx.a<?> next = it.next();
                if (next.getDataClass().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = b;
        }
        return (jx<T>) aVar.a(t);
    }
}
