package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la2 implements Parcelable.Creator<k02> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ k02 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str = j72.e(parcel, a);
            } else if (a2 == 2) {
                i = j72.q(parcel, a);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                j = j72.s(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new k02(str, i, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ k02[] newArray(int i) {
        return new k02[i];
    }
}
