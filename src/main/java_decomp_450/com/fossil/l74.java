package com.fossil;

import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l74 extends m34 implements j74 {
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public l74(String str, String str2, m64 m64, String str3) {
        super(str, str2, m64, k64.POST);
        this.f = str3;
    }

    @DexIgnore
    @Override // com.fossil.j74
    public boolean a(e74 e74, boolean z) {
        if (z) {
            l64 a = a();
            a(a, e74.b);
            a(a, e74.a, e74.c);
            z24 a2 = z24.a();
            a2.a("Sending report to: " + b());
            try {
                int b = a.b().b();
                z24 a3 = z24.a();
                a3.a("Result was: " + b);
                return p44.a(b) == 0;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public final l64 a(l64 l64, String str) {
        l64.a("User-Agent", "Crashlytics Android SDK/" + y34.e());
        l64.a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        l64.a("X-CRASHLYTICS-API-CLIENT-VERSION", this.f);
        l64.a("X-CRASHLYTICS-GOOGLE-APP-ID", str);
        return l64;
    }

    @DexIgnore
    public final l64 a(l64 l64, String str, g74 g74) {
        if (str != null) {
            l64.b("org_id", str);
        }
        l64.b("report_id", g74.b());
        File[] d = g74.d();
        for (File file : d) {
            if (file.getName().equals("minidump")) {
                l64.a("minidump_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("metadata")) {
                l64.a("crash_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("binaryImages")) {
                l64.a("binary_images_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(Constants.SESSION)) {
                l64.a("session_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("app")) {
                l64.a("app_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("device")) {
                l64.a("device_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("os")) {
                l64.a("os_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("user")) {
                l64.a("user_meta_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(FileLogWriter.LOG_FOLDER)) {
                l64.a("logs_file", file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("keys")) {
                l64.a("keys_file", file.getName(), "application/octet-stream", file);
            }
        }
        return l64;
    }
}
