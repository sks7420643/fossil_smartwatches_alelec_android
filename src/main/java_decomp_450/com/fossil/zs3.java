package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zs3<S> extends Parcelable {
    @DexIgnore
    View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle, ws3 ws3, kt3<S> kt3);

    @DexIgnore
    String a(Context context);

    @DexIgnore
    int b(Context context);

    @DexIgnore
    void g(long j);

    @DexIgnore
    Collection<a9<Long, Long>> l();

    @DexIgnore
    boolean o();

    @DexIgnore
    Collection<Long> q();

    @DexIgnore
    S r();
}
