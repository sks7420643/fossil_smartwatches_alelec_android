package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ bp3 b;

    @DexIgnore
    public cp3(bp3 bp3, no3 no3) {
        this.b = bp3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onFailure(this.a.a());
            }
        }
    }
}
