package com.fossil;

import com.fossil.vp7;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp7 implements Closeable {
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(wp7.class.getName());
    @DexIgnore
    public /* final */ ar7 a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ vp7.a d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements sr7 {
        @DexIgnore
        public /* final */ ar7 a;
        @DexIgnore
        public int b;
        @DexIgnore
        public byte c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public short f;

        @DexIgnore
        public a(ar7 ar7) {
            this.a = ar7;
        }

        @DexIgnore
        public final void a() throws IOException {
            int i = this.d;
            int a2 = zp7.a(this.a);
            this.e = a2;
            this.b = a2;
            byte readByte = (byte) (this.a.readByte() & 255);
            this.c = (byte) (this.a.readByte() & 255);
            if (zp7.e.isLoggable(Level.FINE)) {
                zp7.e.fine(wp7.a(true, this.d, this.b, readByte, this.c));
            }
            int readInt = this.a.readInt() & Integer.MAX_VALUE;
            this.d = readInt;
            if (readByte != 9) {
                wp7.b("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (readInt != i) {
                wp7.b("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            while (true) {
                int i = this.e;
                if (i == 0) {
                    this.a.skip((long) this.f);
                    this.f = 0;
                    if ((this.c & 4) != 0) {
                        return -1;
                    }
                    a();
                } else {
                    long b2 = this.a.b(yq7, Math.min(j, (long) i));
                    if (b2 == -1) {
                        return -1;
                    }
                    this.e = (int) (((long) this.e) - b2);
                    return b2;
                }
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public tr7 d() {
            return this.a.d();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i, int i2, int i3, boolean z);

        @DexIgnore
        void a(int i, int i2, List<up7> list) throws IOException;

        @DexIgnore
        void a(int i, long j);

        @DexIgnore
        void a(int i, tp7 tp7);

        @DexIgnore
        void a(int i, tp7 tp7, br7 br7);

        @DexIgnore
        void a(boolean z, int i, int i2);

        @DexIgnore
        void a(boolean z, int i, int i2, List<up7> list);

        @DexIgnore
        void a(boolean z, int i, ar7 ar7, int i2) throws IOException;

        @DexIgnore
        void a(boolean z, eq7 eq7);
    }

    @DexIgnore
    public zp7(ar7 ar7, boolean z) {
        this.a = ar7;
        this.c = z;
        a aVar = new a(ar7);
        this.b = aVar;
        this.d = new vp7.a(4096, aVar);
    }

    @DexIgnore
    public void a(b bVar) throws IOException {
        if (!this.c) {
            br7 a2 = this.a.a((long) wp7.a.size());
            if (e.isLoggable(Level.FINE)) {
                e.fine(ro7.a("<< CONNECTION %s", a2.hex()));
            }
            if (!wp7.a.equals(a2)) {
                wp7.b("Expected a connection header but was %s", a2.utf8());
                throw null;
            }
        } else if (!a(true, bVar)) {
            wp7.b("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void b(b bVar, int i, byte b2, int i2) throws IOException {
        if (i < 8) {
            wp7.b("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.a.readInt();
            int readInt2 = this.a.readInt();
            int i3 = i - 8;
            tp7 fromHttp2 = tp7.fromHttp2(readInt2);
            if (fromHttp2 != null) {
                br7 br7 = br7.EMPTY;
                if (i3 > 0) {
                    br7 = this.a.a((long) i3);
                }
                bVar.a(readInt, fromHttp2, br7);
                return;
            }
            wp7.b("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            wp7.b("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void c(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.a.readByte() & 255);
            }
            if ((b2 & 32) != 0) {
                a(bVar, i2);
                i -= 5;
            }
            bVar.a(z, i2, -1, a(a(i, b2, s), s, b2, i2));
            return;
        }
        wp7.b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public final void d(b bVar, int i, byte b2, int i2) throws IOException {
        boolean z = false;
        if (i != 8) {
            wp7.b("TYPE_PING length != 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.a.readInt();
            int readInt2 = this.a.readInt();
            if ((b2 & 1) != 0) {
                z = true;
            }
            bVar.a(z, readInt, readInt2);
        } else {
            wp7.b("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void e(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 5) {
            wp7.b("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            a(bVar, i2);
        } else {
            wp7.b("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void f(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            if ((b2 & 8) != 0) {
                s = (short) (this.a.readByte() & 255);
            }
            bVar.a(i2, this.a.readInt() & Integer.MAX_VALUE, a(a(i - 4, b2, s), s, b2, i2));
            return;
        }
        wp7.b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void g(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 4) {
            wp7.b("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            int readInt = this.a.readInt();
            tp7 fromHttp2 = tp7.fromHttp2(readInt);
            if (fromHttp2 != null) {
                bVar.a(i2, fromHttp2);
                return;
            }
            wp7.b("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            wp7.b("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void h(b bVar, int i, byte b2, int i2) throws IOException {
        if (i2 != 0) {
            wp7.b("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b2 & 1) != 0) {
            if (i == 0) {
                bVar.a();
            } else {
                wp7.b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i % 6 == 0) {
            eq7 eq7 = new eq7();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short readShort = this.a.readShort() & 65535;
                int readInt = this.a.readInt();
                if (readShort != 2) {
                    if (readShort == 3) {
                        readShort = 4;
                    } else if (readShort == 4) {
                        readShort = 7;
                        if (readInt < 0) {
                            wp7.b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    } else if (readShort == 5 && (readInt < 16384 || readInt > 16777215)) {
                        wp7.b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                        throw null;
                    }
                } else if (!(readInt == 0 || readInt == 1)) {
                    wp7.b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                    throw null;
                }
                eq7.a(readShort, readInt);
            }
            bVar.a(false, eq7);
        } else {
            wp7.b("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public final void i(b bVar, int i, byte b2, int i2) throws IOException {
        if (i == 4) {
            long readInt = ((long) this.a.readInt()) & 2147483647L;
            if (readInt != 0) {
                bVar.a(i2, readInt);
                return;
            }
            wp7.b("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        wp7.b("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public boolean a(boolean z, b bVar) throws IOException {
        try {
            this.a.h(9);
            int a2 = a(this.a);
            if (a2 < 0 || a2 > 16384) {
                wp7.b("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                throw null;
            }
            byte readByte = (byte) (this.a.readByte() & 255);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.a.readByte() & 255);
                int readInt = this.a.readInt() & Integer.MAX_VALUE;
                if (e.isLoggable(Level.FINE)) {
                    e.fine(wp7.a(true, readInt, a2, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        a(bVar, a2, readByte2, readInt);
                        break;
                    case 1:
                        c(bVar, a2, readByte2, readInt);
                        break;
                    case 2:
                        e(bVar, a2, readByte2, readInt);
                        break;
                    case 3:
                        g(bVar, a2, readByte2, readInt);
                        break;
                    case 4:
                        h(bVar, a2, readByte2, readInt);
                        break;
                    case 5:
                        f(bVar, a2, readByte2, readInt);
                        break;
                    case 6:
                        d(bVar, a2, readByte2, readInt);
                        break;
                    case 7:
                        b(bVar, a2, readByte2, readInt);
                        break;
                    case 8:
                        i(bVar, a2, readByte2, readInt);
                        break;
                    default:
                        this.a.skip((long) a2);
                        break;
                }
                return true;
            }
            wp7.b("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
            throw null;
        } catch (IOException unused) {
            return false;
        }
    }

    @DexIgnore
    public final List<up7> a(int i, short s, byte b2, int i2) throws IOException {
        a aVar = this.b;
        aVar.e = i;
        aVar.b = i;
        aVar.f = s;
        aVar.c = b2;
        aVar.d = i2;
        this.d.f();
        return this.d.c();
    }

    @DexIgnore
    public final void a(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = true;
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b2 & 8) != 0) {
                    s = (short) (this.a.readByte() & 255);
                }
                bVar.a(z2, i2, this.a, a(i, b2, s));
                this.a.skip((long) s);
                return;
            }
            wp7.b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        wp7.b("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void a(b bVar, int i) throws IOException {
        int readInt = this.a.readInt();
        bVar.a(i, readInt & Integer.MAX_VALUE, (this.a.readByte() & 255) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }

    @DexIgnore
    public static int a(ar7 ar7) throws IOException {
        return (ar7.readByte() & 255) | ((ar7.readByte() & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((ar7.readByte() & 255) << 8);
    }

    @DexIgnore
    public static int a(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        wp7.b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }
}
