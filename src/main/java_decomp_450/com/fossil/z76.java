package com.fossil;

import java.util.Date;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z76 extends dl4<y76> {
    @DexIgnore
    void a(Date date, Date date2);

    @DexIgnore
    void a(TreeMap<Long, Float> treeMap);
}
