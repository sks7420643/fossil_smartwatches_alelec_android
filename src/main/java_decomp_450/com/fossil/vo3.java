package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ to3 b;

    @DexIgnore
    public vo3(to3 to3, no3 no3) {
        this.b = to3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        if (this.a.c()) {
            this.b.c.f();
            return;
        }
        try {
            this.b.c.a(this.b.b.then(this.a));
        } catch (lo3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.c.a((Exception) e.getCause());
            } else {
                this.b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.b.c.a(e2);
        }
    }
}
