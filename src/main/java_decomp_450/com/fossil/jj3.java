package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public jj3(ti3 ti3, boolean z) {
        this.b = ti3;
        this.a = z;
    }

    @DexIgnore
    public final void run() {
        boolean g = ((ii3) this.b).a.g();
        boolean d = ((ii3) this.b).a.d();
        ((ii3) this.b).a.a(this.a);
        if (d == this.a) {
            ((ii3) this.b).a.e().B().a("Default data collection state already set to", Boolean.valueOf(this.a));
        }
        if (((ii3) this.b).a.g() == g || ((ii3) this.b).a.g() != ((ii3) this.b).a.d()) {
            ((ii3) this.b).a.e().y().a("Default data collection is different than actual status", Boolean.valueOf(this.a), Boolean.valueOf(g));
        }
        this.b.L();
    }
}
