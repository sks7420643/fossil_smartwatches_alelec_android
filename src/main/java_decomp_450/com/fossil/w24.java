package com.fossil;

import com.google.firebase.crashlytics.CrashlyticsRegistrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class w24 implements f24 {
    @DexIgnore
    public /* final */ CrashlyticsRegistrar a;

    @DexIgnore
    public w24(CrashlyticsRegistrar crashlyticsRegistrar) {
        this.a = crashlyticsRegistrar;
    }

    @DexIgnore
    public static f24 a(CrashlyticsRegistrar crashlyticsRegistrar) {
        return new w24(crashlyticsRegistrar);
    }

    @DexIgnore
    @Override // com.fossil.f24
    public Object a(d24 d24) {
        return this.a.a(d24);
    }
}
