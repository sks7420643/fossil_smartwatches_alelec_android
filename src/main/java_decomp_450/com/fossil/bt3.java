package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ws3;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bt3 implements ws3.c {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bt3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<bt3> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public bt3 createFromParcel(Parcel parcel) {
            return new bt3(parcel.readLong(), null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public bt3[] newArray(int i) {
            return new bt3[i];
        }
    }

    @DexIgnore
    public /* synthetic */ bt3(long j, a aVar) {
        this(j);
    }

    @DexIgnore
    public static bt3 a(long j) {
        return new bt3(j);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ws3.c
    public boolean e(long j) {
        return j >= this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof bt3) && this.a == ((bt3) obj).a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.a)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.a);
    }

    @DexIgnore
    public bt3(long j) {
        this.a = j;
    }
}
