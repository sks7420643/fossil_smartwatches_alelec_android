package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i51 extends vh<bi1> {
    @DexIgnore
    public i51(rc1 rc1, ci ciVar) {
        super(ciVar);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.aj, java.lang.Object] */
    @Override // com.fossil.vh
    public void bind(aj ajVar, bi1 bi1) {
        bi1 bi12 = bi1;
        ajVar.bindLong(1, (long) bi12.a);
        String str = bi12.b;
        if (str == null) {
            ajVar.bindNull(2);
        } else {
            ajVar.bindString(2, str);
        }
        ajVar.bindLong(3, (long) bi12.c);
        ajVar.bindLong(4, (long) bi12.d);
        byte[] bArr = bi12.e;
        if (bArr == null) {
            ajVar.bindNull(5);
        } else {
            ajVar.bindBlob(5, bArr);
        }
        ajVar.bindLong(6, bi12.f);
        ajVar.bindLong(7, bi12.g);
        ajVar.bindLong(8, bi12.b());
        ajVar.bindLong(9, bi12.d() ? 1 : 0);
    }

    @DexIgnore
    @Override // com.fossil.ji
    public String createQuery() {
        return "INSERT OR REPLACE INTO `DeviceFile` (`id`,`deviceMacAddress`,`fileType`,`fileIndex`,`rawData`,`fileLength`,`fileCrc`,`createdTimeStamp`,`isCompleted`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
    }
}
