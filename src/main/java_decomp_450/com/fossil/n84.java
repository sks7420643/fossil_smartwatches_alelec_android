package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n84 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ n84 d;

    @DexIgnore
    public n84(Throwable th, m84 m84) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = m84.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new n84(cause, m84) : null;
    }
}
