package com.fossil;

import com.fossil.rg6;
import com.fossil.sg6;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kg6 extends dl4<jg6> {
    @DexIgnore
    void E();

    @DexIgnore
    void a(MFSleepDay mFSleepDay);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c(ArrayList<rg6.a> arrayList);

    @DexIgnore
    void j(List<sg6.b> list);
}
