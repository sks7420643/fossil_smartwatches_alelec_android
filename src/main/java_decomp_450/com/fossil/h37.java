package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h37 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public b e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static Bundle a(h37 h37) {
            Bundle bundle = new Bundle();
            bundle.putInt("_wxobject_sdkVer", h37.a);
            bundle.putString("_wxobject_title", h37.b);
            bundle.putString("_wxobject_description", h37.c);
            bundle.putByteArray("_wxobject_thumbdata", h37.d);
            b bVar = h37.e;
            if (bVar != null) {
                bundle.putString("_wxobject_identifier_", a(bVar.getClass().getName()));
                h37.e.b(bundle);
            }
            bundle.putString("_wxobject_mediatagname", h37.f);
            bundle.putString("_wxobject_message_action", h37.g);
            bundle.putString("_wxobject_message_ext", h37.h);
            return bundle;
        }

        @DexIgnore
        public static h37 a(Bundle bundle) {
            h37 h37 = new h37();
            h37.a = bundle.getInt("_wxobject_sdkVer");
            h37.b = bundle.getString("_wxobject_title");
            h37.c = bundle.getString("_wxobject_description");
            h37.d = bundle.getByteArray("_wxobject_thumbdata");
            h37.f = bundle.getString("_wxobject_mediatagname");
            h37.g = bundle.getString("_wxobject_message_action");
            h37.h = bundle.getString("_wxobject_message_ext");
            String b = b(bundle.getString("_wxobject_identifier_"));
            if (b != null && b.length() > 0) {
                try {
                    b bVar = (b) Class.forName(b).newInstance();
                    h37.e = bVar;
                    bVar.a(bundle);
                    return h37;
                } catch (Exception e) {
                    e.printStackTrace();
                    p27.a("MicroMsg.SDK.WXMediaMessage", "get media object from bundle failed: unknown ident " + b + ", ex = " + e.getMessage());
                }
            }
            return h37;
        }

        @DexIgnore
        public static String a(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
            }
            p27.a("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
            return str;
        }

        @DexIgnore
        public static String b(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
            }
            p27.a("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
            return str;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        int a();

        @DexIgnore
        void a(Bundle bundle);

        @DexIgnore
        void b(Bundle bundle);

        @DexIgnore
        boolean b();
    }

    @DexIgnore
    public h37() {
        this(null);
    }

    @DexIgnore
    public h37(b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public final boolean a() {
        String str;
        byte[] bArr;
        if (b() == 8 && ((bArr = this.d) == null || bArr.length == 0)) {
            str = "checkArgs fail, thumbData should not be null when send emoji";
        } else {
            byte[] bArr2 = this.d;
            if (bArr2 == null || bArr2.length <= 32768) {
                String str2 = this.b;
                if (str2 == null || str2.length() <= 512) {
                    String str3 = this.c;
                    if (str3 != null && str3.length() > 1024) {
                        str = "checkArgs fail, description is invalid";
                    } else if (this.e == null) {
                        str = "checkArgs fail, mediaObject is null";
                    } else {
                        String str4 = this.f;
                        if (str4 == null || str4.length() <= 64) {
                            String str5 = this.g;
                            if (str5 == null || str5.length() <= 2048) {
                                String str6 = this.h;
                                if (str6 == null || str6.length() <= 2048) {
                                    return this.e.b();
                                }
                                str = "checkArgs fail, messageExt is too long";
                            } else {
                                str = "checkArgs fail, messageAction is too long";
                            }
                        } else {
                            str = "checkArgs fail, mediaTagName is too long";
                        }
                    }
                } else {
                    str = "checkArgs fail, title is invalid";
                }
            } else {
                str = "checkArgs fail, thumbData is invalid";
            }
        }
        p27.a("MicroMsg.SDK.WXMediaMessage", str);
        return false;
    }

    @DexIgnore
    public final int b() {
        b bVar = this.e;
        if (bVar == null) {
            return 0;
        }
        return bVar.a();
    }
}
