package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo4 implements Factory<xo4> {
    @DexIgnore
    public /* final */ Provider<vo4> a;
    @DexIgnore
    public /* final */ Provider<wo4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public yo4(Provider<vo4> provider, Provider<wo4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static yo4 a(Provider<vo4> provider, Provider<wo4> provider2, Provider<ch5> provider3) {
        return new yo4(provider, provider2, provider3);
    }

    @DexIgnore
    public static xo4 a(vo4 vo4, wo4 wo4, ch5 ch5) {
        return new xo4(vo4, wo4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public xo4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
