package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.tabs.TabLayout;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov3 {
    @DexIgnore
    public /* final */ TabLayout a;
    @DexIgnore
    public /* final */ ViewPager2 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public RecyclerView.g<?> e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public c g;
    @DexIgnore
    public TabLayout.d h;
    @DexIgnore
    public RecyclerView.AdapterDataObserver i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            ov3.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void b(int i, int i2) {
            ov3.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void c(int i, int i2) {
            ov3.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a(int i, int i2) {
            ov3.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a(int i, int i2, Object obj) {
            ov3.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a(int i, int i2, int i3) {
            ov3.this.b();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(TabLayout.g gVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements TabLayout.d {
        @DexIgnore
        public /* final */ ViewPager2 a;

        @DexIgnore
        public d(ViewPager2 viewPager2) {
            this.a = viewPager2;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            this.a.a(gVar.c(), true);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore
    public ov3(TabLayout tabLayout, ViewPager2 viewPager2, b bVar) {
        this(tabLayout, viewPager2, true, bVar);
    }

    @DexIgnore
    public void a() {
        if (!this.f) {
            RecyclerView.g<?> adapter = this.b.getAdapter();
            this.e = adapter;
            if (adapter != null) {
                this.f = true;
                c cVar = new c(this.a);
                this.g = cVar;
                this.b.a(cVar);
                d dVar = new d(this.b);
                this.h = dVar;
                this.a.a((TabLayout.d) dVar);
                if (this.c) {
                    a aVar = new a();
                    this.i = aVar;
                    this.e.registerAdapterDataObserver(aVar);
                }
                b();
                this.a.a(this.b.getCurrentItem(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, true);
                return;
            }
            throw new IllegalStateException("TabLayoutMediator attached before ViewPager2 has an adapter");
        }
        throw new IllegalStateException("TabLayoutMediator is already attached");
    }

    @DexIgnore
    public void b() {
        int min;
        this.a.g();
        RecyclerView.g<?> gVar = this.e;
        if (gVar != null) {
            int itemCount = gVar.getItemCount();
            for (int i2 = 0; i2 < itemCount; i2++) {
                TabLayout.g e2 = this.a.e();
                this.d.a(e2, i2);
                this.a.a(e2, false);
            }
            if (itemCount > 0 && (min = Math.min(this.b.getCurrentItem(), this.a.getTabCount() - 1)) != this.a.getSelectedTabPosition()) {
                TabLayout tabLayout = this.a;
                tabLayout.h(tabLayout.b(min));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ WeakReference<TabLayout> a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public c(TabLayout tabLayout) {
            this.a = new WeakReference<>(tabLayout);
            a();
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i) {
            this.b = this.c;
            this.c = i;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i && i < tabLayout.getTabCount()) {
                int i2 = this.c;
                tabLayout.b(tabLayout.b(i), i2 == 0 || (i2 == 2 && this.b == 0));
            }
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null) {
                boolean z = false;
                boolean z2 = this.c != 2 || this.b == 1;
                if (!(this.c == 2 && this.b == 0)) {
                    z = true;
                }
                tabLayout.a(i, f, z2, z);
            }
        }

        @DexIgnore
        public void a() {
            this.c = 0;
            this.b = 0;
        }
    }

    @DexIgnore
    public ov3(TabLayout tabLayout, ViewPager2 viewPager2, boolean z, b bVar) {
        this.a = tabLayout;
        this.b = viewPager2;
        this.c = z;
        this.d = bVar;
    }
}
