package com.fossil;

import android.view.View;
import io.flutter.embedding.engine.systemchannels.PlatformViewsChannel;
import io.flutter.plugin.platform.PlatformViewsController;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class e87 implements View.OnFocusChangeListener {
    @DexIgnore
    private /* final */ /* synthetic */ PlatformViewsController.Anon1 a;
    @DexIgnore
    private /* final */ /* synthetic */ PlatformViewsChannel.PlatformViewCreationRequest b;

    @DexIgnore
    public /* synthetic */ e87(PlatformViewsController.Anon1 anon1, PlatformViewsChannel.PlatformViewCreationRequest platformViewCreationRequest) {
        this.a = anon1;
        this.b = platformViewCreationRequest;
    }

    @DexIgnore
    public final void onFocusChange(View view, boolean z) {
        this.a.a(this.b, view, z);
    }
}
