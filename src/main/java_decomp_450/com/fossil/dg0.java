package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dg0 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public dg0(String str, byte[] bArr) {
        this.b = bArr;
        this.c = ik1.a.a(bArr, ng1.CRC32);
        this.a = str.length() == 0 ? yz0.a((int) this.c) : str;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.G, this.a);
            yz0.a(jSONObject, r51.H, Integer.valueOf(this.b.length));
            yz0.a(jSONObject, r51.I, Long.valueOf(this.c));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final byte[] b() {
        if (!(!(this.b.length == 0))) {
            return new byte[0];
        }
        String str = this.a;
        Charset defaultCharset = Charset.defaultCharset();
        ee7.a((Object) defaultCharset, "Charset.defaultCharset()");
        if (str != null) {
            byte[] bytes = str.getBytes(defaultCharset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] a2 = s97.a(bytes, (byte) 0);
            int length = this.b.length;
            ByteBuffer allocate = ByteBuffer.allocate(a2.length + 2 + length);
            ee7.a((Object) allocate, "ByteBuffer.allocate(totalLen)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putShort((short) (a2.length + length));
            allocate.put(a2);
            allocate.put(this.b);
            byte[] array = allocate.array();
            ee7.a((Object) array, "result.array()");
            return array;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final long c() {
        return this.c;
    }

    @DexIgnore
    public final Object d() {
        Object obj;
        if (e()) {
            obj = JSONObject.NULL;
        } else {
            obj = this.a;
        }
        ee7.a(obj, "if (isEmptyFile()) {\n   \u2026   fileName\n            }");
        return obj;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final boolean e() {
        return this.b.length == 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            g70 g70 = (g70) obj;
            if (!ee7.a((Object) this.a, (Object) g70.getFileName()) || !Arrays.equals(this.b, g70.getFileData())) {
                return false;
            }
            return true;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImage");
    }

    @DexIgnore
    public final byte[] getFileData() {
        return this.b;
    }

    @DexIgnore
    public final String getFileName() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.b) + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
