package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u87<T> implements n87<T>, Serializable {
    @DexIgnore
    public volatile Object _value;
    @DexIgnore
    public vc7<? extends T> initializer;
    @DexIgnore
    public /* final */ Object lock;

    @DexIgnore
    public u87(vc7<? extends T> vc7, Object obj) {
        ee7.b(vc7, "initializer");
        this.initializer = vc7;
        this._value = e97.a;
        this.lock = obj == null ? this : obj;
    }

    @DexIgnore
    private final Object writeReplace() {
        return new k87(getValue());
    }

    @DexIgnore
    @Override // com.fossil.n87
    public T getValue() {
        T t;
        T t2 = (T) this._value;
        if (t2 != e97.a) {
            return t2;
        }
        synchronized (this.lock) {
            T t3 = (T) this._value;
            if (t3 == e97.a) {
                vc7<? extends T> vc7 = this.initializer;
                if (vc7 != null) {
                    t3 = (T) vc7.invoke();
                    this._value = t3;
                    this.initializer = null;
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public boolean isInitialized() {
        return this._value != e97.a;
    }

    @DexIgnore
    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ u87(vc7 vc7, Object obj, int i, zd7 zd7) {
        this(vc7, (i & 2) != 0 ? null : obj);
    }
}
