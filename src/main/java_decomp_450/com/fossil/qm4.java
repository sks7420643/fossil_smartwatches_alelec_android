package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import com.fossil.sm4;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm4 implements sm4 {
    @DexIgnore
    public /* final */ List<String> a; // = w97.d("Agree term of use", "Agree privacy", "Allow location data processing");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean a() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean a(List<String> list) {
        ee7.b(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean c() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean e() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean f() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean g() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public String h() {
        return sm4.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean i() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean j() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean k() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean l() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public void a(go5 go5) {
        ee7.b(go5, "fragment");
        String a2 = vx6.a(vx6.c.REPAIR_CENTER, null);
        ee7.a((Object) a2, "url");
        a(a2, go5);
    }

    @DexIgnore
    public final void a(String str, go5 go5) {
        try {
            go5.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e("CitizenBrandLogic", "Exception when start url with no browser app");
        }
    }
}
