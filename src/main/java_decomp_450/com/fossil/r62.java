package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r62 {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public m02 b;

    @DexIgnore
    public r62(m02 m02) {
        a72.a(m02);
        this.b = m02;
    }

    @DexIgnore
    public int a(Context context, v02.f fVar) {
        a72.a(context);
        a72.a(fVar);
        int i = 0;
        if (!fVar.j()) {
            return 0;
        }
        int k = fVar.k();
        int i2 = this.a.get(k, -1);
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.a.size()) {
                i = i2;
                break;
            }
            int keyAt = this.a.keyAt(i3);
            if (keyAt > k && this.a.get(keyAt) == 0) {
                break;
            }
            i3++;
        }
        if (i == -1) {
            i = this.b.a(context, k);
        }
        this.a.put(k, i);
        return i;
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }
}
