package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg implements xg {
    @DexIgnore
    public /* final */ RecyclerView.g a;

    @DexIgnore
    public hg(RecyclerView.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    @Override // com.fossil.xg
    public void a(int i, int i2) {
        this.a.notifyItemMoved(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.xg
    public void b(int i, int i2) {
        this.a.notifyItemRangeInserted(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.xg
    public void c(int i, int i2) {
        this.a.notifyItemRangeRemoved(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.xg
    public void a(int i, int i2, Object obj) {
        this.a.notifyItemRangeChanged(i, i2, obj);
    }
}
