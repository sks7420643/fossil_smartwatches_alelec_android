package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ uo3 b;

    @DexIgnore
    public xo3(uo3 uo3, no3 no3) {
        this.b = uo3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        try {
            no3 no3 = (no3) this.b.b.then(this.a);
            if (no3 == null) {
                this.b.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            no3.a(po3.b, (jo3) this.b);
            no3.a(po3.b, (io3) this.b);
            no3.a(po3.b, (go3) this.b);
        } catch (lo3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.c.a((Exception) e.getCause());
            } else {
                this.b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.b.c.a(e2);
        }
    }
}
