package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ Guideline I;
    @DexIgnore
    public /* final */ RTLImageView J;
    @DexIgnore
    public /* final */ LinearLayout K;
    @DexIgnore
    public /* final */ LinearLayout L;
    @DexIgnore
    public /* final */ ConstraintLayout M;
    @DexIgnore
    public /* final */ ScrollView N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ View P;
    @DexIgnore
    public /* final */ View Q;
    @DexIgnore
    public /* final */ View R;
    @DexIgnore
    public /* final */ Guideline S;
    @DexIgnore
    public /* final */ Barrier q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat s;
    @DexIgnore
    public /* final */ CustomEditGoalView t;
    @DexIgnore
    public /* final */ CustomEditGoalView u;
    @DexIgnore
    public /* final */ CustomEditGoalView v;
    @DexIgnore
    public /* final */ CustomEditGoalView w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ FlexibleEditText y;
    @DexIgnore
    public /* final */ FlexibleEditText z;

    @DexIgnore
    public i55(Object obj, View view, int i, Barrier barrier, ConstraintLayout constraintLayout, FlexibleSwitchCompat flexibleSwitchCompat, CustomEditGoalView customEditGoalView, CustomEditGoalView customEditGoalView2, CustomEditGoalView customEditGoalView3, CustomEditGoalView customEditGoalView4, ConstraintLayout constraintLayout2, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleEditText flexibleEditText3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, Guideline guideline, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout3, ScrollView scrollView, FlexibleTextView flexibleTextView8, View view2, View view3, View view4, Guideline guideline2) {
        super(obj, view, i);
        this.q = barrier;
        this.r = constraintLayout;
        this.s = flexibleSwitchCompat;
        this.t = customEditGoalView;
        this.u = customEditGoalView2;
        this.v = customEditGoalView3;
        this.w = customEditGoalView4;
        this.x = constraintLayout2;
        this.y = flexibleEditText;
        this.z = flexibleEditText2;
        this.A = flexibleEditText3;
        this.B = flexibleTextView;
        this.C = flexibleTextView2;
        this.D = flexibleTextView3;
        this.E = flexibleTextView4;
        this.F = flexibleTextView5;
        this.G = flexibleTextView6;
        this.H = flexibleTextView7;
        this.I = guideline;
        this.J = rTLImageView;
        this.K = linearLayout;
        this.L = linearLayout2;
        this.M = constraintLayout3;
        this.N = scrollView;
        this.O = flexibleTextView8;
        this.P = view2;
        this.Q = view3;
        this.R = view4;
        this.S = guideline2;
    }
}
