package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij2 implements cc2 {
    @DexIgnore
    @Override // com.fossil.cc2
    public final c12<Status> a(a12 a12, DataSet dataSet) {
        a72.a(dataSet, "Must set the data set");
        a72.b(!dataSet.g().isEmpty(), "Cannot use an empty data set");
        a72.a(dataSet.v().C(), "Must set the app package name for the data source");
        return a12.a(new hj2(this, a12, dataSet, false));
    }
}
