package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc1 implements Parcelable.Creator<pe1> {
    @DexIgnore
    public /* synthetic */ vc1(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public pe1 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            zp1 valueOf = zp1.valueOf(readString);
            parcel.setDataPosition(0);
            switch (bb1.a[valueOf.ordinal()]) {
                case 1:
                    return lr0.CREATOR.createFromParcel(parcel);
                case 2:
                    return cv0.CREATOR.createFromParcel(parcel);
                case 3:
                    return zl0.CREATOR.createFromParcel(parcel);
                case 4:
                    return cm1.CREATOR.createFromParcel(parcel);
                case 5:
                    return x11.CREATOR.createFromParcel(parcel);
                case 6:
                    return gi1.CREATOR.createFromParcel(parcel);
                case 7:
                    return bq1.CREATOR.createFromParcel(parcel);
                case 8:
                    return n51.CREATOR.createFromParcel(parcel);
                case 9:
                    return my0.CREATOR.createFromParcel(parcel);
                case 10:
                    return rp0.CREATOR.createFromParcel(parcel);
                case 11:
                    return fi0.CREATOR.createFromParcel(parcel);
                case 12:
                    return g91.CREATOR.createFromParcel(parcel);
                default:
                    throw new p87();
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public pe1[] newArray(int i) {
        return new pe1[i];
    }
}
