package com.fossil;

import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gg4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = Integer.MAX_VALUE;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i; // = 64;

    @DexIgnore
    public gg4(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i3 + i2;
        this.e = i2;
    }

    @DexIgnore
    public static gg4 a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void b(int i2) {
        this.g = i2;
        k();
    }

    @DexIgnore
    public boolean c() throws IOException {
        return h() != 0;
    }

    @DexIgnore
    public int d() throws IOException {
        return h();
    }

    @DexIgnore
    public void e(int i2) {
        int i3 = this.e;
        int i4 = this.b;
        if (i2 > i3 - i4) {
            int i5 = this.e - this.b;
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(i5);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.e = i4 + i2;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    @DexIgnore
    public boolean f(int i2) throws IOException {
        int b2 = lg4.b(i2);
        if (b2 == 0) {
            d();
            return true;
        } else if (b2 == 1) {
            g();
            return true;
        } else if (b2 == 2) {
            g(h());
            return true;
        } else if (b2 == 3) {
            l();
            a(lg4.a(lg4.a(i2), 4));
            return true;
        } else if (b2 == 4) {
            return false;
        } else {
            if (b2 == 5) {
                f();
                return true;
            }
            throw ig4.invalidWireType();
        }
    }

    @DexIgnore
    public long g() throws IOException {
        byte e2 = e();
        byte e3 = e();
        return ((((long) e3) & 255) << 8) | (((long) e2) & 255) | ((((long) e()) & 255) << 16) | ((((long) e()) & 255) << 24) | ((((long) e()) & 255) << 32) | ((((long) e()) & 255) << 40) | ((((long) e()) & 255) << 48) | ((((long) e()) & 255) << 56);
    }

    @DexIgnore
    public int h() throws IOException {
        int i2;
        byte e2 = e();
        if (e2 >= 0) {
            return e2;
        }
        byte b2 = e2 & Byte.MAX_VALUE;
        byte e3 = e();
        if (e3 >= 0) {
            i2 = e3 << 7;
        } else {
            b2 |= (e3 & Byte.MAX_VALUE) << 7;
            byte e4 = e();
            if (e4 >= 0) {
                i2 = e4 << DateTimeFieldType.HOUR_OF_HALFDAY;
            } else {
                b2 |= (e4 & Byte.MAX_VALUE) << DateTimeFieldType.HOUR_OF_HALFDAY;
                byte e5 = e();
                if (e5 >= 0) {
                    i2 = e5 << DateTimeFieldType.SECOND_OF_MINUTE;
                } else {
                    byte b3 = b2 | ((e5 & Byte.MAX_VALUE) << DateTimeFieldType.SECOND_OF_MINUTE);
                    byte e6 = e();
                    byte b4 = b3 | (e6 << 28);
                    if (e6 >= 0) {
                        return b4;
                    }
                    for (int i3 = 0; i3 < 5; i3++) {
                        if (e() >= 0) {
                            return b4;
                        }
                    }
                    throw ig4.malformedVarint();
                }
            }
        }
        return b2 | i2;
    }

    @DexIgnore
    public String i() throws IOException {
        int h2 = h();
        if (h2 > this.c - this.e || h2 <= 0) {
            return new String(d(h2), "UTF-8");
        }
        String str = new String(this.a, this.e, h2, "UTF-8");
        this.e += h2;
        return str;
    }

    @DexIgnore
    public int j() throws IOException {
        if (b()) {
            this.f = 0;
            return 0;
        }
        int h2 = h();
        this.f = h2;
        if (h2 != 0) {
            return h2;
        }
        throw ig4.invalidTag();
    }

    @DexIgnore
    public final void k() {
        int i2 = this.c + this.d;
        this.c = i2;
        int i3 = this.g;
        if (i2 > i3) {
            int i4 = i2 - i3;
            this.d = i4;
            this.c = i2 - i4;
            return;
        }
        this.d = 0;
    }

    @DexIgnore
    public void l() throws IOException {
        int j;
        do {
            j = j();
            if (j == 0) {
                return;
            }
        } while (f(j));
    }

    @DexIgnore
    public static gg4 a(byte[] bArr, int i2, int i3) {
        return new gg4(bArr, i2, i3);
    }

    @DexIgnore
    public int c(int i2) throws ig4 {
        if (i2 >= 0) {
            int i3 = i2 + this.e;
            int i4 = this.g;
            if (i3 <= i4) {
                this.g = i3;
                k();
                return i4;
            }
            throw ig4.truncatedMessage();
        }
        throw ig4.negativeSize();
    }

    @DexIgnore
    public byte[] d(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = i3 + i2;
            int i5 = this.g;
            if (i4 > i5) {
                g(i5 - i3);
                throw ig4.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                byte[] bArr = new byte[i2];
                System.arraycopy(this.a, i3, bArr, 0, i2);
                this.e += i2;
                return bArr;
            } else {
                throw ig4.truncatedMessage();
            }
        } else {
            throw ig4.negativeSize();
        }
    }

    @DexIgnore
    public void a(int i2) throws ig4 {
        if (this.f != i2) {
            throw ig4.invalidEndTag();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.e == this.c;
    }

    @DexIgnore
    public void a(jg4 jg4) throws IOException {
        int h2 = h();
        if (this.h < this.i) {
            int c2 = c(h2);
            this.h++;
            jg4.a(this);
            a(0);
            this.h--;
            b(c2);
            return;
        }
        throw ig4.recursionLimitExceeded();
    }

    @DexIgnore
    public byte e() throws IOException {
        int i2 = this.e;
        if (i2 != this.c) {
            byte[] bArr = this.a;
            this.e = i2 + 1;
            return bArr[i2];
        }
        throw ig4.truncatedMessage();
    }

    @DexIgnore
    public void g(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = i3 + i2;
            int i5 = this.g;
            if (i4 > i5) {
                g(i5 - i3);
                throw ig4.truncatedMessage();
            } else if (i2 <= this.c - i3) {
                this.e = i3 + i2;
            } else {
                throw ig4.truncatedMessage();
            }
        } else {
            throw ig4.negativeSize();
        }
    }

    @DexIgnore
    public int f() throws IOException {
        return (e() & 255) | ((e() & 255) << 8) | ((e() & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((e() & 255) << 24);
    }

    @DexIgnore
    public int a() {
        return this.e - this.b;
    }
}
