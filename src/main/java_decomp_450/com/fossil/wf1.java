package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wf1 extends qj1 {
    @DexIgnore
    public dd1 A; // = dd1.DISCONNECTED;

    @DexIgnore
    public wf1(ri1 ri1) {
        super(qa1.c, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        this.A = ((o01) eo0).k;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(new JSONObject(), r51.z0, yz0.a(this.A)), 7));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(pm1 pm1) {
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.q1, yz0.a(((v81) this).y.t)), r51.i0, ((v81) this).y.u);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.z0, yz0.a(this.A));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new o01(((v81) this).y.w);
    }
}
