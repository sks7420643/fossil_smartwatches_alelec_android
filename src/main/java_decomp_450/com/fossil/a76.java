package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a76 implements Factory<cd6> {
    @DexIgnore
    public static cd6 a(u66 u66) {
        cd6 f = u66.f();
        c87.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
