package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.cv1;
import com.fossil.dv1;
import java.util.ArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jw1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ bv1 b;
    @DexIgnore
    public /* final */ sw1 c;
    @DexIgnore
    public /* final */ pw1 d;
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ ay1 f;
    @DexIgnore
    public /* final */ by1 g;

    @DexIgnore
    public jw1(Context context, bv1 bv1, sw1 sw1, pw1 pw1, Executor executor, ay1 ay1, by1 by1) {
        this.a = context;
        this.b = bv1;
        this.c = sw1;
        this.d = pw1;
        this.e = executor;
        this.f = ay1;
        this.g = by1;
    }

    @DexIgnore
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public void a(pu1 pu1, int i, Runnable runnable) {
        this.e.execute(ew1.a(this, pu1, i, runnable));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        r5.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r2.d.a(r3, r4 + 1);
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0024 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void a(com.fossil.jw1 r2, com.fossil.pu1 r3, int r4, java.lang.Runnable r5) {
        /*
            com.fossil.ay1 r0 = r2.f     // Catch:{ zx1 -> 0x0024 }
            com.fossil.sw1 r1 = r2.c     // Catch:{ zx1 -> 0x0024 }
            r1.getClass()     // Catch:{ zx1 -> 0x0024 }
            com.fossil.ay1$a r1 = com.fossil.hw1.a(r1)     // Catch:{ zx1 -> 0x0024 }
            r0.a(r1)     // Catch:{ zx1 -> 0x0024 }
            boolean r0 = r2.a()     // Catch:{ zx1 -> 0x0024 }
            if (r0 != 0) goto L_0x001e
            com.fossil.ay1 r0 = r2.f     // Catch:{ zx1 -> 0x0024 }
            com.fossil.ay1$a r1 = com.fossil.iw1.a(r2, r3, r4)     // Catch:{ zx1 -> 0x0024 }
            r0.a(r1)     // Catch:{ zx1 -> 0x0024 }
            goto L_0x002b
        L_0x001e:
            r2.a(r3, r4)     // Catch:{ zx1 -> 0x0024 }
            goto L_0x002b
        L_0x0022:
            r2 = move-exception
            goto L_0x002f
        L_0x0024:
            com.fossil.pw1 r2 = r2.d     // Catch:{ all -> 0x0022 }
            int r4 = r4 + 1
            r2.a(r3, r4)     // Catch:{ all -> 0x0022 }
        L_0x002b:
            r5.run()
            return
        L_0x002f:
            r5.run()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jw1.a(com.fossil.jw1, com.fossil.pu1, int, java.lang.Runnable):void");
    }

    @DexIgnore
    public void a(pu1 pu1, int i) {
        dv1 dv1;
        jv1 a2 = this.b.a(pu1.a());
        Iterable<yw1> iterable = (Iterable) this.f.a(fw1.a(this, pu1));
        if (iterable.iterator().hasNext()) {
            if (a2 == null) {
                kv1.a("Uploader", "Unknown backend for %s, deleting event batch for it...", pu1);
                dv1 = dv1.c();
            } else {
                ArrayList arrayList = new ArrayList();
                for (yw1 yw1 : iterable) {
                    arrayList.add(yw1.a());
                }
                cv1.a c2 = cv1.c();
                c2.a(arrayList);
                c2.a(pu1.b());
                dv1 = a2.a(c2.a());
            }
            this.f.a(gw1.a(this, dv1, iterable, pu1, i));
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(jw1 jw1, dv1 dv1, Iterable iterable, pu1 pu1, int i) {
        if (dv1.b() == dv1.a.TRANSIENT_ERROR) {
            jw1.c.b(iterable);
            jw1.d.a(pu1, i + 1);
            return null;
        }
        jw1.c.a(iterable);
        if (dv1.b() == dv1.a.OK) {
            jw1.c.a(pu1, jw1.g.a() + dv1.a());
        }
        if (!jw1.c.c(pu1)) {
            return null;
        }
        jw1.d.a(pu1, 1);
        return null;
    }
}
