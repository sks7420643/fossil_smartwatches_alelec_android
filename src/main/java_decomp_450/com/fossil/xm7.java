package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm7 extends ym7 {
    @DexIgnore
    public static /* final */ ti7 g;
    @DexIgnore
    public static /* final */ xm7 h;

    /*
    static {
        xm7 xm7 = new xm7();
        h = xm7;
        g = xm7.a(om7.a("kotlinx.coroutines.io.parallelism", qf7.a(64, mm7.a()), 0, 0, 12, (Object) null));
    }
    */

    @DexIgnore
    public xm7() {
        super(0, 0, null, 7, null);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    public final ti7 k() {
        return g;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        return "DefaultDispatcher";
    }
}
