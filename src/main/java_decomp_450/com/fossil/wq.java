package com.fossil;

import android.graphics.Bitmap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq implements uq {
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ yq<Integer, Bitmap> b; // = new yq<>();
    @DexIgnore
    public /* final */ TreeMap<Integer, Integer> c; // = new TreeMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    @Override // com.fossil.uq
    public void a(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int a2 = iu.a(bitmap);
        this.b.a(Integer.valueOf(a2), bitmap);
        Integer num = this.c.get(Integer.valueOf(a2));
        TreeMap<Integer, Integer> treeMap = this.c;
        Integer valueOf = Integer.valueOf(a2);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        treeMap.put(valueOf, Integer.valueOf(i));
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int a2 = iu.a(bitmap);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(a2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap removeLast() {
        Bitmap a2 = this.b.a();
        if (a2 != null) {
            a(iu.a(a2), a2);
        }
        return a2;
    }

    @DexIgnore
    public String toString() {
        return "SizeStrategy: groupedMap=" + this.b + ", sortedSizes=(" + this.c + ')';
    }

    @DexIgnore
    @Override // com.fossil.uq
    public String b(int i, int i2, Bitmap.Config config) {
        ee7.b(config, "config");
        int a2 = ku.a.a(i, i2, config);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(a2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.uq
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        ee7.b(config, "config");
        int a2 = a(ku.a.a(i, i2, config));
        Bitmap a3 = this.b.a(Integer.valueOf(a2));
        if (a3 != null) {
            a(a2, a3);
            a3.reconfigure(i, i2, config);
        }
        return a3;
    }

    @DexIgnore
    public final int a(int i) {
        Integer ceilingKey = this.c.ceilingKey(Integer.valueOf(i));
        return (ceilingKey == null || ceilingKey.intValue() > i * 8) ? i : ceilingKey.intValue();
    }

    @DexIgnore
    public final void a(int i, Bitmap bitmap) {
        Integer num = this.c.get(Integer.valueOf(i));
        if (num != null) {
            ee7.a((Object) num, "sortedSizes[size] ?: run\u2026, this: $this\")\n        }");
            int intValue = num.intValue();
            if (intValue == 1) {
                this.c.remove(Integer.valueOf(i));
            } else {
                this.c.put(Integer.valueOf(i), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", " + "removed: " + b(bitmap) + ", this: " + this);
        }
    }
}
