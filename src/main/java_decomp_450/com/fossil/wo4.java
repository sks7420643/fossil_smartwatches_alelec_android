package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo4 {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {114}, m = "block")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$block$response$1", f = "FriendRemoteDataSource.kt", l = {114}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.block(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {99}, m = "cancelFriendRequest")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$cancelFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {99}, m = "invokeSuspend")
    public static final class d extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.cancelFriendRequest(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {53}, m = "fetchFriends")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$fetchFriends$response$1", f = "FriendRemoteDataSource.kt", l = {53}, m = "invokeSuspend")
    public static final class f extends zb7 implements gd7<fb7<? super fv7<ApiResponse<un4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(wo4 wo4, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$status = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new f(this.this$0, this.$status, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<un4>>> fb7) {
            return ((f) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$status;
                this.label = 1;
                obj = a2.getFriends(str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {17}, m = Constants.FIND)
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$find$response$1", f = "FriendRemoteDataSource.kt", l = {17}, m = "invokeSuspend")
    public static final class h extends zb7 implements gd7<fb7<? super fv7<ApiResponse<un4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(wo4 wo4, String str, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new h(this.this$0, this.$key, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<un4>>> fb7) {
            return ((h) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$key;
                this.label = 1;
                obj = a2.findFriend(str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {145}, m = "respondFriendRequest")
    public static final class i extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(false, (String) null, (fb7<? super zi5<un4>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$respondFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {145}, m = "invokeSuspend")
    public static final class j extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new j(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((j) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.respondFriendRequest(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {69}, m = "sendRequest")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$sendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {69}, m = "invokeSuspend")
    public static final class l extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new l(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((l) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.sendRequest(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {84}, m = "unFriend")
    public static final class m extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unFriend$response$1", f = "FriendRemoteDataSource.kt", l = {84}, m = "invokeSuspend")
    public static final class n extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new n(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((n) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.unFriend(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {129}, m = "unblock")
    public static final class o extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(wo4 wo4, fb7 fb7) {
            super(fb7);
            this.this$0 = wo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unblock$response$1", f = "FriendRemoteDataSource.kt", l = {129}, m = "invokeSuspend")
    public static final class p extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ wo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(wo4 wo4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = wo4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new p(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((p) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.unblock(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public wo4(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.c
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$c r0 = (com.fossil.wo4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$c r0 = new com.fossil.wo4$c
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "friendId"
            r10.a(r2, r9)
            com.fossil.wo4$d r2 = new com.fossil.wo4$d
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x006d
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x008a
        L_0x006d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x008b
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x008a:
            return r9
        L_0x008b:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.b(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.un4>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$e r0 = (com.fossil.wo4.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$e r0 = new com.fossil.wo4$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0031:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0039:
            com.fossil.t87.a(r10)
            com.fossil.wo4$f r10 = new com.fossil.wo4$f
            r2 = 0
            r10.<init>(r8, r9, r2)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0065
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            boolean r10 = r10.b()
            r9.<init>(r0, r10)
            goto L_0x0082
        L_0x0065:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0083
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0082:
            return r9
        L_0x0083:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.c(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.un4>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.g
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$g r0 = (com.fossil.wo4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$g r0 = new com.fossil.wo4$g
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x004f
        L_0x0031:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0039:
            com.fossil.t87.a(r10)
            com.fossil.wo4$h r10 = new com.fossil.wo4$h
            r2 = 0
            r10.<init>(r8, r9, r2)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004f
            return r1
        L_0x004f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0065
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            boolean r10 = r10.b()
            r9.<init>(r0, r10)
            goto L_0x0082
        L_0x0065:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0083
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0082:
            return r9
        L_0x0083:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.d(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.m
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$m r0 = (com.fossil.wo4.m) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$m r0 = new com.fossil.wo4$m
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "friendId"
            r10.a(r2, r9)
            com.fossil.wo4$n r2 = new com.fossil.wo4$n
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x006d
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x008a
        L_0x006d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x008b
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x008a:
            return r9
        L_0x008b:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.e(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.fossil.ln4>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.o
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$o r0 = (com.fossil.wo4.o) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$o r0 = new com.fossil.wo4$o
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "profileID"
            r10.a(r2, r9)
            com.fossil.wo4$p r2 = new com.fossil.wo4$p
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x008d
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r9 = r10.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0072
            goto L_0x0083
        L_0x0072:
            com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x007f }
            r0.<init>()     // Catch:{ oe4 -> 0x007f }
            java.lang.Class<com.fossil.ln4> r1 = com.fossil.ln4.class
            java.lang.Object r9 = r0.a(r9, r1)     // Catch:{ oe4 -> 0x007f }
            r4 = r9
            goto L_0x0083
        L_0x007f:
            r9 = move-exception
            r9.printStackTrace()
        L_0x0083:
            boolean r9 = r10.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00bd
        L_0x008d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00be
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            com.portfolio.platform.data.model.ServerError r0 = r10.c()
            if (r0 == 0) goto L_0x00a6
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00a6
            int r0 = r0.intValue()
            goto L_0x00aa
        L_0x00a6:
            int r0 = r10.a()
        L_0x00aa:
            r1 = r0
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r10 = r9
        L_0x00bd:
            return r10
        L_0x00be:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.f(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, java.lang.String r10, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.fossil.wo4.k
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.wo4$k r0 = (com.fossil.wo4.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$k r0 = new com.fossil.wo4$k
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r9 = r0.L$3
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r11)
            goto L_0x006a
        L_0x003a:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0042:
            com.fossil.t87.a(r11)
            com.fossil.ie4 r11 = new com.fossil.ie4
            r11.<init>()
            java.lang.String r2 = "friendId"
            r11.a(r2, r9)
            java.lang.String r2 = "socialId"
            r11.a(r2, r10)
            com.fossil.wo4$l r2 = new com.fossil.wo4$l
            r2.<init>(r8, r11, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r11
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r2, r0)
            if (r11 != r1) goto L_0x006a
            return r1
        L_0x006a:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r9 = r11 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0078
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r11 = 2
            r9.<init>(r4, r10, r11, r4)
            goto L_0x00a7
        L_0x0078:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00a8
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r10 = r11.c()
            if (r10 == 0) goto L_0x0091
            java.lang.Integer r10 = r10.getCode()
            if (r10 == 0) goto L_0x0091
            int r10 = r10.intValue()
            goto L_0x0095
        L_0x0091:
            int r10 = r11.a()
        L_0x0095:
            r1 = r10
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00a7:
            return r9
        L_0x00a8:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.a(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<java.lang.Void>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.wo4.a
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.wo4$a r0 = (com.fossil.wo4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$a r0 = new com.fossil.wo4$a
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.wo4 r9 = (com.fossil.wo4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "profileID"
            r10.a(r2, r9)
            com.fossil.wo4$b r2 = new com.fossil.wo4$b
            r2.<init>(r8, r10, r4)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x006d
            com.fossil.bj5 r9 = new com.fossil.bj5
            r10 = 0
            r0 = 2
            r9.<init>(r4, r10, r0, r4)
            goto L_0x009c
        L_0x006d:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x009d
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            com.portfolio.platform.data.model.ServerError r0 = r10.c()
            if (r0 == 0) goto L_0x0086
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x0086
            int r0 = r0.intValue()
            goto L_0x008a
        L_0x0086:
            int r0 = r10.a()
        L_0x008a:
            r1 = r0
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x009c:
            return r9
        L_0x009d:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(boolean r9, java.lang.String r10, com.fossil.fb7<? super com.fossil.zi5<com.fossil.un4>> r11) {
        /*
            r8 = this;
            boolean r0 = r11 instanceof com.fossil.wo4.i
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.wo4$i r0 = (com.fossil.wo4.i) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wo4$i r0 = new com.fossil.wo4$i
            r0.<init>(r8, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0040
            if (r2 != r3) goto L_0x0038
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            boolean r9 = r0.Z$0
            java.lang.Object r10 = r0.L$0
            com.fossil.wo4 r10 = (com.fossil.wo4) r10
            com.fossil.t87.a(r11)
            goto L_0x006c
        L_0x0038:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0040:
            com.fossil.t87.a(r11)
            com.fossil.ie4 r11 = new com.fossil.ie4
            r11.<init>()
            java.lang.String r2 = "friendId"
            r11.a(r2, r10)
            java.lang.Boolean r2 = com.fossil.pb7.a(r9)
            java.lang.String r5 = "confirmation"
            r11.a(r5, r2)
            com.fossil.wo4$j r2 = new com.fossil.wo4$j
            r2.<init>(r8, r11, r4)
            r0.L$0 = r8
            r0.Z$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r3
            java.lang.Object r11 = com.fossil.aj5.a(r2, r0)
            if (r11 != r1) goto L_0x006c
            return r1
        L_0x006c:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r10 = r11 instanceof com.fossil.bj5
            if (r10 == 0) goto L_0x00a4
            if (r9 == 0) goto L_0x009c
            com.fossil.fu4 r9 = com.fossil.fu4.a
            com.fossil.bj5 r11 = (com.fossil.bj5) r11
            java.lang.Object r9 = r11.a()
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            if (r9 != 0) goto L_0x0081
            goto L_0x0092
        L_0x0081:
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x008e }
            r10.<init>()     // Catch:{ oe4 -> 0x008e }
            java.lang.Class<com.fossil.un4> r0 = com.fossil.un4.class
            java.lang.Object r9 = r10.a(r9, r0)     // Catch:{ oe4 -> 0x008e }
            r4 = r9
            goto L_0x0092
        L_0x008e:
            r9 = move-exception
            r9.printStackTrace()
        L_0x0092:
            boolean r9 = r11.b()
            com.fossil.bj5 r10 = new com.fossil.bj5
            r10.<init>(r4, r9)
            goto L_0x00a3
        L_0x009c:
            com.fossil.bj5 r10 = new com.fossil.bj5
            r9 = 0
            r11 = 2
            r10.<init>(r4, r9, r11, r4)
        L_0x00a3:
            return r10
        L_0x00a4:
            boolean r9 = r11 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00d4
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r10 = r11.c()
            if (r10 == 0) goto L_0x00bd
            java.lang.Integer r10 = r10.getCode()
            if (r10 == 0) goto L_0x00bd
            int r10 = r10.intValue()
            goto L_0x00c1
        L_0x00bd:
            int r10 = r11.a()
        L_0x00c1:
            r1 = r10
            com.portfolio.platform.data.model.ServerError r2 = r11.c()
            java.lang.Throwable r3 = r11.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return r9
        L_0x00d4:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wo4.a(boolean, java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
