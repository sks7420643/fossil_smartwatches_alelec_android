package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a9<F, S> {
    @DexIgnore
    public /* final */ F a;
    @DexIgnore
    public /* final */ S b;

    @DexIgnore
    public a9(F f, S s) {
        this.a = f;
        this.b = s;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof a9)) {
            return false;
        }
        a9 a9Var = (a9) obj;
        if (!z8.a(a9Var.a, this.a) || !z8.a(a9Var.b, this.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        F f = this.a;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Pair{" + String.valueOf(this.a) + " " + String.valueOf(this.b) + "}";
    }
}
