package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qc1 extends e71 {
    @DexIgnore
    public long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qc1(short s, ri1 ri1, int i, int i2) {
        super(g51.LEGACY_GET_SIZE_WRITTEN, s, qa1.M, ri1, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            long b = yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            this.L = b;
            yz0.a(jSONObject, r51.p3, Long.valueOf(b));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.p3, Long.valueOf(this.L));
    }
}
