package com.fossil;

import android.os.Process;
import com.fossil.mu;
import com.fossil.yu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nu extends Thread {
    @DexIgnore
    public static /* final */ boolean g; // = gv.b;
    @DexIgnore
    public /* final */ BlockingQueue<yu<?>> a;
    @DexIgnore
    public /* final */ BlockingQueue<yu<?>> b;
    @DexIgnore
    public /* final */ mu c;
    @DexIgnore
    public /* final */ bv d;
    @DexIgnore
    public volatile boolean e; // = false;
    @DexIgnore
    public /* final */ b f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yu a;

        @DexIgnore
        public a(yu yuVar) {
            this.a = yuVar;
        }

        @DexIgnore
        public void run() {
            try {
                nu.this.b.put(this.a);
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements yu.b {
        @DexIgnore
        public /* final */ Map<String, List<yu<?>>> a; // = new HashMap();
        @DexIgnore
        public /* final */ nu b;

        @DexIgnore
        public b(nu nuVar) {
            this.b = nuVar;
        }

        @DexIgnore
        public final synchronized boolean b(yu<?> yuVar) {
            String cacheKey = yuVar.getCacheKey();
            if (this.a.containsKey(cacheKey)) {
                List<yu<?>> list = this.a.get(cacheKey);
                if (list == null) {
                    list = new ArrayList<>();
                }
                yuVar.addMarker("waiting-for-response");
                list.add(yuVar);
                this.a.put(cacheKey, list);
                if (gv.b) {
                    gv.b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                }
                return true;
            }
            this.a.put(cacheKey, null);
            yuVar.setNetworkRequestCompleteListener(this);
            if (gv.b) {
                gv.b("new request, sending to network %s", cacheKey);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.yu.b
        public void a(yu<?> yuVar, av<?> avVar) {
            List<yu<?>> remove;
            mu.a aVar = avVar.b;
            if (aVar == null || aVar.a()) {
                a(yuVar);
                return;
            }
            String cacheKey = yuVar.getCacheKey();
            synchronized (this) {
                remove = this.a.remove(cacheKey);
            }
            if (remove != null) {
                if (gv.b) {
                    gv.d("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                }
                for (yu<?> yuVar2 : remove) {
                    this.b.d.a(yuVar2, avVar);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.yu.b
        public synchronized void a(yu<?> yuVar) {
            String cacheKey = yuVar.getCacheKey();
            List<yu<?>> remove = this.a.remove(cacheKey);
            if (remove != null && !remove.isEmpty()) {
                if (gv.b) {
                    gv.d("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), cacheKey);
                }
                yu<?> remove2 = remove.remove(0);
                this.a.put(cacheKey, remove);
                remove2.setNetworkRequestCompleteListener(this);
                try {
                    this.b.b.put(remove2);
                } catch (InterruptedException e) {
                    gv.c("Couldn't add request to queue. %s", e.toString());
                    Thread.currentThread().interrupt();
                    this.b.b();
                }
            }
        }
    }

    @DexIgnore
    public nu(BlockingQueue<yu<?>> blockingQueue, BlockingQueue<yu<?>> blockingQueue2, mu muVar, bv bvVar) {
        this.a = blockingQueue;
        this.b = blockingQueue2;
        this.c = muVar;
        this.d = bvVar;
        this.f = new b(this);
    }

    @DexIgnore
    public void run() {
        if (g) {
            gv.d("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.c.a();
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.e) {
                    Thread.currentThread().interrupt();
                    return;
                }
                gv.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        a(this.a.take());
    }

    @DexIgnore
    public void b() {
        this.e = true;
        interrupt();
    }

    @DexIgnore
    public void a(yu<?> yuVar) throws InterruptedException {
        yuVar.addMarker("cache-queue-take");
        if (yuVar.isCanceled()) {
            yuVar.finish("cache-discard-canceled");
            return;
        }
        mu.a a2 = this.c.a(yuVar.getCacheKey());
        if (a2 == null) {
            yuVar.addMarker("cache-miss");
            if (!this.f.b(yuVar)) {
                this.b.put(yuVar);
            }
        } else if (a2.a()) {
            yuVar.addMarker("cache-hit-expired");
            yuVar.setCacheEntry(a2);
            if (!this.f.b(yuVar)) {
                this.b.put(yuVar);
            }
        } else {
            yuVar.addMarker("cache-hit");
            av<?> parseNetworkResponse = yuVar.parseNetworkResponse(new vu(a2.a, a2.g));
            yuVar.addMarker("cache-hit-parsed");
            if (!a2.b()) {
                this.d.a(yuVar, parseNetworkResponse);
                return;
            }
            yuVar.addMarker("cache-hit-refresh-needed");
            yuVar.setCacheEntry(a2);
            parseNetworkResponse.d = true;
            if (!this.f.b(yuVar)) {
                this.d.a(yuVar, parseNetworkResponse, new a(yuVar));
            } else {
                this.d.a(yuVar, parseNetworkResponse);
            }
        }
    }
}
