package com.fossil;

import com.fossil.by3;
import com.fossil.h14;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e14 {
    @DexIgnore
    public /* final */ c a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends g14 {
        @DexIgnore
        public /* final */ /* synthetic */ Map b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public a(Map map, Type type) {
            this.b = map;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(TypeVariable<?> typeVariable) {
            this.b.put(new d(typeVariable), this.c);
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(WildcardType wildcardType) {
            Type type = this.c;
            if (type instanceof WildcardType) {
                WildcardType wildcardType2 = (WildcardType) type;
                Type[] upperBounds = wildcardType.getUpperBounds();
                Type[] upperBounds2 = wildcardType2.getUpperBounds();
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] lowerBounds2 = wildcardType2.getLowerBounds();
                jw3.a(upperBounds.length == upperBounds2.length && lowerBounds.length == lowerBounds2.length, "Incompatible type: %s vs. %s", wildcardType, this.c);
                for (int i = 0; i < upperBounds.length; i++) {
                    e14.b(this.b, upperBounds[i], upperBounds2[i]);
                }
                for (int i2 = 0; i2 < lowerBounds.length; i2++) {
                    e14.b(this.b, lowerBounds[i2], lowerBounds2[i2]);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(ParameterizedType parameterizedType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                ParameterizedType parameterizedType2 = (ParameterizedType) e14.b(ParameterizedType.class, (Object) type);
                if (!(parameterizedType.getOwnerType() == null || parameterizedType2.getOwnerType() == null)) {
                    e14.b(this.b, parameterizedType.getOwnerType(), parameterizedType2.getOwnerType());
                }
                jw3.a(parameterizedType.getRawType().equals(parameterizedType2.getRawType()), "Inconsistent raw type: %s vs. %s", parameterizedType, this.c);
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                jw3.a(actualTypeArguments.length == actualTypeArguments2.length, "%s not compatible with %s", parameterizedType, parameterizedType2);
                for (int i = 0; i < actualTypeArguments.length; i++) {
                    e14.b(this.b, actualTypeArguments[i], actualTypeArguments2[i]);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(GenericArrayType genericArrayType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                Type a = h14.a(type);
                jw3.a(a != null, "%s is not an array type.", this.c);
                e14.b(this.b, genericArrayType.getGenericComponentType(), a);
            }
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(Class<?> cls) {
            if (!(this.c instanceof WildcardType)) {
                throw new IllegalArgumentException("No type mapping from " + cls + " to " + this.c);
            }
        }
    }

    @DexIgnore
    public /* synthetic */ e14(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public static e14 b(Type type) {
        return new e14().a(b.a(type));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ by3<d, Type> a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends c {
            @DexIgnore
            public /* final */ /* synthetic */ TypeVariable b;
            @DexIgnore
            public /* final */ /* synthetic */ c c;

            @DexIgnore
            public a(c cVar, TypeVariable typeVariable, c cVar2) {
                this.b = typeVariable;
                this.c = cVar2;
            }

            @DexIgnore
            @Override // com.fossil.e14.c
            public Type a(TypeVariable<?> typeVariable, c cVar) {
                if (typeVariable.getGenericDeclaration().equals(this.b.getGenericDeclaration())) {
                    return typeVariable;
                }
                return this.c.a(typeVariable, cVar);
            }
        }

        @DexIgnore
        public c() {
            this.a = by3.of();
        }

        @DexIgnore
        public final c a(Map<d, ? extends Type> map) {
            by3.b builder = by3.builder();
            builder.a(this.a);
            for (Map.Entry<d, ? extends Type> entry : map.entrySet()) {
                d key = entry.getKey();
                Type type = (Type) entry.getValue();
                jw3.a(!key.a(type), "Type variable %s bound to itself", key);
                builder.a(key, type);
            }
            return new c(builder.a());
        }

        @DexIgnore
        public c(by3<d, Type> by3) {
            this.a = by3;
        }

        @DexIgnore
        public final Type a(TypeVariable<?> typeVariable) {
            return a(typeVariable, new a(this, typeVariable, this));
        }

        @DexIgnore
        public Type a(TypeVariable<?> typeVariable, c cVar) {
            Type type = this.a.get(new d(typeVariable));
            if (type != null) {
                return new e14(cVar, null).a(type);
            }
            Type[] bounds = typeVariable.getBounds();
            if (bounds.length == 0) {
                return typeVariable;
            }
            Type[] a2 = new e14(cVar, null).a(bounds);
            if (!h14.f.a || !Arrays.equals(bounds, a2)) {
                return h14.a(typeVariable.getGenericDeclaration(), typeVariable.getName(), a2);
            }
            return typeVariable;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ TypeVariable<?> a;

        @DexIgnore
        public d(TypeVariable<?> typeVariable) {
            jw3.a(typeVariable);
            this.a = typeVariable;
        }

        @DexIgnore
        public static d b(Type type) {
            if (type instanceof TypeVariable) {
                return new d((TypeVariable) type);
            }
            return null;
        }

        @DexIgnore
        public boolean a(Type type) {
            if (type instanceof TypeVariable) {
                return a((TypeVariable) type);
            }
            return false;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return a(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return gw3.a(this.a.getGenericDeclaration(), this.a.getName());
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public final boolean a(TypeVariable<?> typeVariable) {
            return this.a.getGenericDeclaration().equals(typeVariable.getGenericDeclaration()) && this.a.getName().equals(typeVariable.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ AtomicInteger a;

        @DexIgnore
        public e() {
            this.a = new AtomicInteger();
        }

        @DexIgnore
        public Type a(Type type) {
            jw3.a(type);
            if ((type instanceof Class) || (type instanceof TypeVariable)) {
                return type;
            }
            if (type instanceof GenericArrayType) {
                return h14.b(a(((GenericArrayType) type).getGenericComponentType()));
            }
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                return h14.a(b(parameterizedType.getOwnerType()), (Class) parameterizedType.getRawType(), a(parameterizedType.getActualTypeArguments()));
            } else if (type instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) type;
                if (wildcardType.getLowerBounds().length != 0) {
                    return type;
                }
                Type[] upperBounds = wildcardType.getUpperBounds();
                return h14.a(e.class, "capture#" + this.a.incrementAndGet() + "-of ? extends " + ew3.a('&').a((Object[]) upperBounds), wildcardType.getUpperBounds());
            } else {
                throw new AssertionError("must have been one of the known types");
            }
        }

        @DexIgnore
        public final Type b(Type type) {
            if (type == null) {
                return null;
            }
            return a(type);
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }

        @DexIgnore
        public final Type[] a(Type[] typeArr) {
            Type[] typeArr2 = new Type[typeArr.length];
            for (int i = 0; i < typeArr.length; i++) {
                typeArr2[i] = a(typeArr[i]);
            }
            return typeArr2;
        }
    }

    @DexIgnore
    public e14() {
        this.a = new c();
    }

    @DexIgnore
    public static void b(Map<d, Type> map, Type type, Type type2) {
        if (!type.equals(type2)) {
            new a(map, type2).a(type);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends g14 {
        @DexIgnore
        public static /* final */ e c; // = new e(null);
        @DexIgnore
        public /* final */ Map<d, Type> b; // = yy3.b();

        @DexIgnore
        public static by3<d, Type> a(Type type) {
            b bVar = new b();
            bVar.a(c.a(type));
            return by3.copyOf(bVar.b);
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(Class<?> cls) {
            a(cls.getGenericSuperclass());
            a(cls.getGenericInterfaces());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(ParameterizedType parameterizedType) {
            Class cls = (Class) parameterizedType.getRawType();
            TypeVariable[] typeParameters = cls.getTypeParameters();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            jw3.b(typeParameters.length == actualTypeArguments.length);
            for (int i = 0; i < typeParameters.length; i++) {
                a(new d(typeParameters[i]), actualTypeArguments[i]);
            }
            a(cls);
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.g14
        public void a(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public final void a(d dVar, Type type) {
            if (!this.b.containsKey(dVar)) {
                Type type2 = type;
                while (type2 != null) {
                    if (dVar.a(type2)) {
                        while (type != null) {
                            type = this.b.remove(d.b(type));
                        }
                        return;
                    }
                    type2 = this.b.get(d.b(type2));
                }
                this.b.put(dVar, type);
            }
        }
    }

    @DexIgnore
    public e14(c cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public static <T> T b(Class<T> cls, Object obj) {
        try {
            return cls.cast(obj);
        } catch (ClassCastException unused) {
            throw new IllegalArgumentException(obj + " is not a " + cls.getSimpleName());
        }
    }

    @DexIgnore
    public e14 a(Type type, Type type2) {
        HashMap b2 = yy3.b();
        jw3.a(type);
        jw3.a(type2);
        b(b2, type, type2);
        return a(b2);
    }

    @DexIgnore
    public e14 a(Map<d, ? extends Type> map) {
        return new e14(this.a.a(map));
    }

    @DexIgnore
    public Type a(Type type) {
        jw3.a(type);
        if (type instanceof TypeVariable) {
            return this.a.a((TypeVariable) type);
        }
        if (type instanceof ParameterizedType) {
            return a((ParameterizedType) type);
        }
        if (type instanceof GenericArrayType) {
            return a((GenericArrayType) type);
        }
        return type instanceof WildcardType ? a((WildcardType) type) : type;
    }

    @DexIgnore
    public final Type[] a(Type[] typeArr) {
        Type[] typeArr2 = new Type[typeArr.length];
        for (int i = 0; i < typeArr.length; i++) {
            typeArr2[i] = a(typeArr[i]);
        }
        return typeArr2;
    }

    @DexIgnore
    public final WildcardType a(WildcardType wildcardType) {
        return new h14.j(a(wildcardType.getLowerBounds()), a(wildcardType.getUpperBounds()));
    }

    @DexIgnore
    public final Type a(GenericArrayType genericArrayType) {
        return h14.b(a(genericArrayType.getGenericComponentType()));
    }

    @DexIgnore
    public final ParameterizedType a(ParameterizedType parameterizedType) {
        Type type;
        Type ownerType = parameterizedType.getOwnerType();
        if (ownerType == null) {
            type = null;
        } else {
            type = a(ownerType);
        }
        return h14.a(type, (Class) a(parameterizedType.getRawType()), a(parameterizedType.getActualTypeArguments()));
    }
}
