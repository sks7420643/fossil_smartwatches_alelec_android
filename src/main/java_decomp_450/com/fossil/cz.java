package com.fossil;

import com.fossil.lz;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cz<T extends lz> {
    @DexIgnore
    public /* final */ Queue<T> a; // = v50.a(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }

    @DexIgnore
    public T b() {
        T poll = this.a.poll();
        return poll == null ? a() : poll;
    }
}
