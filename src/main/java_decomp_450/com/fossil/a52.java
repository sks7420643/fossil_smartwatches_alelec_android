package com.fossil;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a52 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    public /* final */ AtomicReference<z42> c;
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ l02 e;

    @DexIgnore
    public a52(x12 x12) {
        this(x12, l02.a());
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null) {
            this.c.set(bundle.getBoolean("resolving_error", false) ? new z42(new i02(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @DexIgnore
    public abstract void a(i02 i02, int i);

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void b(Bundle bundle) {
        super.b(bundle);
        z42 z42 = this.c.get();
        if (z42 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", z42.b());
            bundle.putInt("failed_status", z42.a().e());
            bundle.putParcelable("failed_resolution", z42.a().v());
        }
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void d() {
        super.d();
        this.b = true;
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void e() {
        super.e();
        this.b = false;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public final void g() {
        this.c.set(null);
        f();
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        a(new i02(13, null), a(this.c.get()));
        g();
    }

    @DexIgnore
    public a52(x12 x12, l02 l02) {
        super(x12);
        this.c = new AtomicReference<>(null);
        this.d = new bg2(Looper.getMainLooper());
        this.e = l02;
    }

    @DexIgnore
    public final void b(i02 i02, int i) {
        z42 z42 = new z42(i02, i);
        if (this.c.compareAndSet(null, z42)) {
            this.d.post(new c52(this, z42));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0067  */
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r5, int r6, android.content.Intent r7) {
        /*
            r4 = this;
            java.util.concurrent.atomic.AtomicReference<com.fossil.z42> r0 = r4.c
            java.lang.Object r0 = r0.get()
            com.fossil.z42 r0 = (com.fossil.z42) r0
            r1 = 1
            r2 = 0
            if (r5 == r1) goto L_0x0030
            r6 = 2
            if (r5 == r6) goto L_0x0010
            goto L_0x0060
        L_0x0010:
            com.fossil.l02 r5 = r4.e
            android.app.Activity r6 = r4.a()
            int r5 = r5.c(r6)
            if (r5 != 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r1 = 0
        L_0x001e:
            if (r0 != 0) goto L_0x0021
            return
        L_0x0021:
            com.fossil.i02 r6 = r0.a()
            int r6 = r6.e()
            r7 = 18
            if (r6 != r7) goto L_0x0061
            if (r5 != r7) goto L_0x0061
            return
        L_0x0030:
            r5 = -1
            if (r6 != r5) goto L_0x0034
            goto L_0x0061
        L_0x0034:
            if (r6 != 0) goto L_0x0060
            if (r0 != 0) goto L_0x0039
            return
        L_0x0039:
            r5 = 13
            if (r7 == 0) goto L_0x0043
            java.lang.String r6 = "<<ResolutionFailureErrorDetail>>"
            int r5 = r7.getIntExtra(r6, r5)
        L_0x0043:
            com.fossil.z42 r6 = new com.fossil.z42
            com.fossil.i02 r7 = new com.fossil.i02
            r1 = 0
            com.fossil.i02 r3 = r0.a()
            java.lang.String r3 = r3.toString()
            r7.<init>(r5, r1, r3)
            int r5 = a(r0)
            r6.<init>(r7, r5)
            java.util.concurrent.atomic.AtomicReference<com.fossil.z42> r5 = r4.c
            r5.set(r6)
            r0 = r6
        L_0x0060:
            r1 = 0
        L_0x0061:
            if (r1 == 0) goto L_0x0067
            r4.g()
            return
        L_0x0067:
            if (r0 == 0) goto L_0x0074
            com.fossil.i02 r5 = r0.a()
            int r6 = r0.b()
            r4.a(r5, r6)
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a52.a(int, int, android.content.Intent):void");
    }

    @DexIgnore
    public static int a(z42 z42) {
        if (z42 == null) {
            return -1;
        }
        return z42.b();
    }
}
