package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wi6 implements Factory<ui6> {
    @DexIgnore
    public /* final */ Provider<SummariesRepository> a;
    @DexIgnore
    public /* final */ Provider<SleepSummariesRepository> b;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> c;
    @DexIgnore
    public /* final */ Provider<UserRepository> d;
    @DexIgnore
    public /* final */ Provider<am5> e;

    @DexIgnore
    public wi6(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<am5> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static wi6 a(Provider<SummariesRepository> provider, Provider<SleepSummariesRepository> provider2, Provider<GoalTrackingRepository> provider3, Provider<UserRepository> provider4, Provider<am5> provider5) {
        return new wi6(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static ui6 a(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, am5 am5) {
        return new ui6(summariesRepository, sleepSummariesRepository, goalTrackingRepository, userRepository, am5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ui6 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
