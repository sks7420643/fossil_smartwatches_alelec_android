package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.cy6;
import com.fossil.gx6;
import com.fossil.ov3;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq6 extends go5 implements yp6, cy6.g {
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public xp6 f;
    @DexIgnore
    public qw6<s65> g;
    @DexIgnore
    public cm4 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String p; // = "";
    @DexIgnore
    public /* final */ String q; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String r; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String s; // = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wq6 a(boolean z, String str) {
            ee7.b(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putString("SERIAL", str);
            wq6 wq6 = new wq6();
            wq6.setArguments(bundle);
            return wq6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wq6 a;

        @DexIgnore
        public b(wq6 wq6) {
            this.a = wq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ s65 a;
        @DexIgnore
        public /* final */ /* synthetic */ wq6 b;

        @DexIgnore
        public c(s65 s65, wq6 wq6) {
            this.a = s65;
            this.b = wq6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.r)) {
                int parseColor = Color.parseColor(this.b.r);
                TabLayout.g b4 = this.a.A.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.q) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.q);
                TabLayout.g b5 = this.a.A.b(this.b.j);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wq6 a;

        @DexIgnore
        public d(wq6 wq6) {
            this.a = wq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wq6 a;

        @DexIgnore
        public e(wq6 wq6) {
            this.a = wq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
                ee7.a((Object) activity, "it");
                TroubleshootingActivity.a.a(aVar, activity, this.a.p, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ wq6 a;

        @DexIgnore
        public f(wq6 wq6) {
            this.a = wq6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.q) && !TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.q);
                int parseColor2 = Color.parseColor(this.a.r);
                gVar.b(2131230963);
                if (i == this.a.j) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void F() {
        qw6<s65> qw6 = this.g;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            DashBar dashBar = a2 != null ? a2.C : null;
            if (dashBar != null) {
                ee7.a((Object) dashBar, "mBinding.get()?.progressBar!!");
                dashBar.setVisibility(8);
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void U() {
        if (isAdded()) {
            cy6.f fVar = new cy6.f(2131558481);
            fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
            fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886743));
            fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886746));
            fVar.a(2131363307);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void e() {
        DashBar dashBar;
        qw6<s65> qw6 = this.g;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                qw6<s65> qw62 = this.g;
                if (qw62 != null) {
                    s65 a3 = qw62.a();
                    DashBar dashBar2 = a3 != null ? a3.C : null;
                    if (dashBar2 != null) {
                        ee7.a((Object) dashBar2, "mBinding.get()?.progressBar!!");
                        dashBar2.setVisibility(0);
                        gx6.a aVar = gx6.a;
                        ee7.a((Object) dashBar, "this");
                        aVar.f(dashBar, this.i, 500);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final xp6 f1() {
        xp6 xp6 = this.f;
        if (xp6 != null) {
            return xp6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        qw6<s65> qw6 = this.g;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                qw6<s65> qw62 = this.g;
                if (qw62 != null) {
                    s65 a3 = qw62.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new ov3(tabLayout, viewPager2, new f(this)).a();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void i() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void k0() {
        if (isActive()) {
            qw6<s65> qw6 = this.g;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        s65 s65 = (s65) qb.a(layoutInflater, 2131558631, viewGroup, false, a1());
        this.g = new qw6<>(this, s65);
        ee7.a((Object) s65, "binding");
        return s65.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        xp6 xp6 = this.f;
        if (xp6 != null) {
            xp6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        xp6 xp6 = this.f;
        if (xp6 != null) {
            xp6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.i = arguments.getBoolean("IS_ONBOARDING_FLOW");
            String string = arguments.getString("SERIAL");
            if (string == null) {
                string = "";
            }
            this.p = string;
            xp6 xp6 = this.f;
            if (xp6 != null) {
                xp6.a(this.i, string);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        this.h = new cm4(new ArrayList());
        qw6<s65> qw6 = this.g;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                ee7.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                ee7.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                ee7.a((Object) flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                ee7.a((Object) flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                ee7.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this));
                ViewPager2 viewPager2 = a2.F;
                ee7.a((Object) viewPager2, "binding.rvpTutorial");
                cm4 cm4 = this.h;
                if (cm4 != null) {
                    viewPager2.setAdapter(cm4);
                    if (!TextUtils.isEmpty(this.s)) {
                        TabLayout tabLayout = a2.A;
                        ee7.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                    }
                    g1();
                    a2.F.a(new c(a2, this));
                    a2.t.setOnClickListener(new d(this));
                    a2.w.setOnClickListener(new e(this));
                    return;
                }
                ee7.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void t0() {
        if (isActive()) {
            qw6<s65> qw6 = this.g;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    ee7.a((Object) flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    ee7.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    ee7.a((Object) flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    ee7.a((Object) flexibleTextView2, "it.ftvCountdownTime");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.y;
                    ee7.a((Object) flexibleTextView3, "it.ftvUpdate");
                    flexibleTextView3.setText(ig5.a(PortfolioApp.g0.c(), 2131886732));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.z;
            ee7.a((Object) activity, "it");
            xp6 xp6 = this.f;
            if (xp6 != null) {
                aVar.a(activity, xp6.j());
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void c(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        qw6<s65> qw6 = this.g;
        if (qw6 != null) {
            s65 a2 = qw6.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void d(List<? extends Explore> list) {
        ee7.b(list, "data");
        cm4 cm4 = this.h;
        if (cm4 != null) {
            cm4.a(list);
        } else {
            ee7.d("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    public void a(xp6 xp6) {
        ee7.b(xp6, "presenter");
        this.f = xp6;
    }

    @DexIgnore
    @Override // com.fossil.yp6
    public void c(boolean z) {
        if (isActive()) {
            qw6<s65> qw6 = this.g;
            if (qw6 != null) {
                s65 a2 = qw6.a();
                if (a2 != null) {
                    xp6 xp6 = this.f;
                    if (xp6 == null) {
                        ee7.d("mPresenter");
                        throw null;
                    } else if (xp6.j()) {
                        if (z) {
                            ConstraintLayout constraintLayout = a2.q;
                            ee7.a((Object) constraintLayout, "it.clUpdateFwFail");
                            constraintLayout.setVisibility(8);
                            ConstraintLayout constraintLayout2 = a2.r;
                            ee7.a((Object) constraintLayout2, "it.clUpdatingFw");
                            constraintLayout2.setVisibility(0);
                            FlexibleButton flexibleButton = a2.s;
                            ee7.a((Object) flexibleButton, "it.fbContinue");
                            flexibleButton.setVisibility(0);
                            FlexibleTextView flexibleTextView = a2.z;
                            ee7.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                            flexibleTextView.setVisibility(4);
                            FlexibleProgressBar flexibleProgressBar = a2.D;
                            ee7.a((Object) flexibleProgressBar, "it.progressUpdate");
                            flexibleProgressBar.setProgress(1000);
                            FlexibleTextView flexibleTextView2 = a2.y;
                            ee7.a((Object) flexibleTextView2, "it.ftvUpdate");
                            flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131886991));
                            return;
                        }
                        ConstraintLayout constraintLayout3 = a2.q;
                        ee7.a((Object) constraintLayout3, "it.clUpdateFwFail");
                        constraintLayout3.setVisibility(0);
                        ConstraintLayout constraintLayout4 = a2.r;
                        ee7.a((Object) constraintLayout4, "it.clUpdatingFw");
                        constraintLayout4.setVisibility(8);
                    } else if (getActivity() == null) {
                    } else {
                        if (z) {
                            xp6 xp62 = this.f;
                            if (xp62 != null) {
                                xp62.h();
                                i();
                                return;
                            }
                            ee7.d("mPresenter");
                            throw null;
                        }
                        TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
                        Context requireContext = requireContext();
                        ee7.a((Object) requireContext, "requireContext()");
                        TroubleshootingActivity.a.a(aVar, requireContext, this.p, false, false, 12, null);
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.i);
                if (i2 == 2131362268) {
                    xp6 xp6 = this.f;
                    if (xp6 != null) {
                        xp6.l();
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else if (i2 != 2131362359) {
                    if (i2 == 2131362656) {
                        xp6 xp62 = this.f;
                        if (xp62 != null) {
                            xp62.h();
                            requireActivity().finish();
                            return;
                        }
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.z;
                    FragmentActivity requireActivity = requireActivity();
                    ee7.a((Object) requireActivity, "requireActivity()");
                    aVar.a(requireActivity);
                }
            }
        } else if (str.equals("NETWORK_ERROR") && i2 == 2131362260) {
            xp6 xp63 = this.f;
            if (xp63 != null) {
                xp63.i();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }
}
