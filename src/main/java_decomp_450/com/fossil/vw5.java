package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw5 extends dy6 {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ pb t; // = new pm4(this);
    @DexIgnore
    public qw6<o35> u;
    @DexIgnore
    public b v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vw5.x;
        }

        @DexIgnore
        public final vw5 b() {
            return new vw5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vw5 a;

        @DexIgnore
        public c(vw5 vw5) {
            this.a = vw5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.a.getDialog();
            if (dialog != null) {
                vw5 vw5 = this.a;
                ee7.a((Object) dialog, "it");
                vw5.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vw5 a;
        @DexIgnore
        public /* final */ /* synthetic */ o35 b;

        @DexIgnore
        public d(vw5 vw5, o35 o35) {
            this.a = vw5;
            this.b = o35;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.b.v;
            ee7.a((Object) rTLImageView, "binding.ivCallsMessageCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.b.v;
                ee7.a((Object) rTLImageView2, "binding.ivCallsMessageCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.b.u;
                ee7.a((Object) rTLImageView3, "binding.ivCallsCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.b.w;
                ee7.a((Object) rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.a.q = true;
                this.a.p = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vw5 a;
        @DexIgnore
        public /* final */ /* synthetic */ o35 b;

        @DexIgnore
        public e(vw5 vw5, o35 o35) {
            this.a = vw5;
            this.b = o35;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.b.u;
            ee7.a((Object) rTLImageView, "binding.ivCallsCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.b.u;
                ee7.a((Object) rTLImageView2, "binding.ivCallsCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.b.v;
                ee7.a((Object) rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.b.w;
                ee7.a((Object) rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.a.p = true;
                this.a.q = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vw5 a;
        @DexIgnore
        public /* final */ /* synthetic */ o35 b;

        @DexIgnore
        public f(vw5 vw5, o35 o35) {
            this.a = vw5;
            this.b = o35;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.b.w;
            ee7.a((Object) rTLImageView, "binding.ivMessagesCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.b.w;
                ee7.a((Object) rTLImageView2, "binding.ivMessagesCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.b.v;
                ee7.a((Object) rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.b.u;
                ee7.a((Object) rTLImageView4, "binding.ivCallsCheck");
                rTLImageView4.setVisibility(4);
                this.a.q = true;
                this.a.p = false;
            }
        }
    }

    /*
    static {
        String simpleName = vw5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void b1() {
        if (this.q && this.p) {
            qw6<o35> qw6 = this.u;
            if (qw6 != null) {
                o35 a2 = qw6.a();
                if (a2 != null) {
                    RTLImageView rTLImageView = a2.v;
                    ee7.a((Object) rTLImageView, "it.ivCallsMessageCheck");
                    rTLImageView.setVisibility(0);
                    RTLImageView rTLImageView2 = a2.u;
                    ee7.a((Object) rTLImageView2, "it.ivCallsCheck");
                    rTLImageView2.setVisibility(4);
                    RTLImageView rTLImageView3 = a2.w;
                    ee7.a((Object) rTLImageView3, "it.ivMessagesCheck");
                    rTLImageView3.setVisibility(4);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else if (this.p) {
            qw6<o35> qw62 = this.u;
            if (qw62 != null) {
                o35 a3 = qw62.a();
                if (a3 != null) {
                    RTLImageView rTLImageView4 = a3.v;
                    ee7.a((Object) rTLImageView4, "it.ivCallsMessageCheck");
                    rTLImageView4.setVisibility(4);
                    RTLImageView rTLImageView5 = a3.u;
                    ee7.a((Object) rTLImageView5, "it.ivCallsCheck");
                    rTLImageView5.setVisibility(0);
                    RTLImageView rTLImageView6 = a3.w;
                    ee7.a((Object) rTLImageView6, "it.ivMessagesCheck");
                    rTLImageView6.setVisibility(4);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else {
            qw6<o35> qw63 = this.u;
            if (qw63 != null) {
                o35 a4 = qw63.a();
                if (a4 != null) {
                    RTLImageView rTLImageView7 = a4.v;
                    ee7.a((Object) rTLImageView7, "it.ivCallsMessageCheck");
                    rTLImageView7.setVisibility(4);
                    RTLImageView rTLImageView8 = a4.u;
                    ee7.a((Object) rTLImageView8, "it.ivCallsCheck");
                    rTLImageView8.setVisibility(4);
                    RTLImageView rTLImageView9 = a4.w;
                    ee7.a((Object) rTLImageView9, "it.ivMessagesCheck");
                    rTLImageView9.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        o35 o35 = (o35) qb.a(layoutInflater, 2131558586, viewGroup, false, this.t);
        o35.x.setOnClickListener(new c(this));
        this.u = new qw6<>(this, o35);
        b1();
        o35.r.setOnClickListener(new d(this, o35));
        o35.q.setOnClickListener(new e(this, o35));
        o35.s.setOnClickListener(new f(this, o35));
        ee7.a((Object) o35, "binding");
        return o35.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // com.fossil.dy6, com.fossil.ac
    public void onDismiss(DialogInterface dialogInterface) {
        b bVar;
        ee7.b(dialogInterface, "dialog");
        if (!((this.r == this.p && this.s == this.q) || (bVar = this.v) == null)) {
            bVar.a(this.j, this.p, this.q);
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        b1();
    }

    @DexIgnore
    public final void a(int i, boolean z, boolean z2) {
        this.j = i;
        this.p = z;
        this.q = z2;
        this.r = z;
        this.s = z2;
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "onDismissListener");
        this.v = bVar;
    }
}
