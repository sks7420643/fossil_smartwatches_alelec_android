package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc6 implements Factory<yc6> {
    @DexIgnore
    public static yc6 a(wc6 wc6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new yc6(wc6, userRepository, heartRateSummaryRepository);
    }
}
