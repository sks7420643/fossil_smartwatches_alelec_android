package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku5 implements Factory<ju5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ ku5 a; // = new ku5();
    }

    @DexIgnore
    public static ku5 a() {
        return a.a;
    }

    @DexIgnore
    public static ju5 b() {
        return new ju5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ju5 get() {
        return b();
    }
}
