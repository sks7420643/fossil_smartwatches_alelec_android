package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk5 implements Factory<ek5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ gk5 a; // = new gk5();
    }

    @DexIgnore
    public static gk5 a() {
        return a.a;
    }

    @DexIgnore
    public static ek5 b() {
        return new ek5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ek5 get() {
        return b();
    }
}
