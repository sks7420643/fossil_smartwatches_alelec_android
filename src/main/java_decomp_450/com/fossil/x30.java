package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x30 implements q30 {
    @DexIgnore
    public /* final */ Set<c50<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void a(c50<?> c50) {
        this.a.add(c50);
    }

    @DexIgnore
    public void b(c50<?> c50) {
        this.a.remove(c50);
    }

    @DexIgnore
    public List<c50<?>> c() {
        return v50.a(this.a);
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onDestroy() {
        for (c50 c50 : v50.a(this.a)) {
            c50.onDestroy();
        }
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStart() {
        for (c50 c50 : v50.a(this.a)) {
            c50.onStart();
        }
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStop() {
        for (c50 c50 : v50.a(this.a)) {
            c50.onStop();
        }
    }

    @DexIgnore
    public void b() {
        this.a.clear();
    }
}
