package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import com.fossil.an6;
import com.fossil.fn6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn6 extends go5 implements gy6 {
    @DexIgnore
    public static String j; // = "";
    @DexIgnore
    public static boolean p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public fn6 g;
    @DexIgnore
    public qw6<u65> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return b();
        }

        @DexIgnore
        public final String b() {
            return cn6.j;
        }

        @DexIgnore
        public final cn6 c() {
            return new cn6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<fn6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ cn6 a;

        @DexIgnore
        public b(cn6 cn6) {
            this.a = cn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fn6.b bVar) {
            String a2;
            if (bVar != null && (a2 = bVar.a()) != null) {
                this.a.Y(a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cn6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements an6.b {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // com.fossil.an6.b
            public void a(String str) {
                ee7.b(str, "themeName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UserCustomizeThemeFragment", "showRenamePresetDialog - themeName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    cn6.a(this.a.a).a(str);
                }
            }

            @DexIgnore
            @Override // com.fossil.an6.b
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        public c(cn6 cn6) {
            this.a = cn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            an6.g.a("", new a(this)).show(this.a.getChildFragmentManager(), "RenamePresetDialogFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cn6 a;

        @DexIgnore
        public d(cn6 cn6) {
            this.a = cn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ fn6 a(cn6 cn6) {
        fn6 fn6 = cn6.g;
        if (fn6 != null) {
            return fn6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        ee7.b(str, "name");
        qw6<u65> qw6 = this.h;
        if (qw6 != null) {
            u65 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setText(str);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        ee7.b(layoutInflater, "inflater");
        boolean z = false;
        u65 u65 = (u65) qb.a(LayoutInflater.from(getContext()), 2131558632, null, false, a1());
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("THEME_ID")) == null) {
            str = "";
        }
        j = str;
        Bundle arguments2 = getArguments();
        if (arguments2 != null) {
            z = arguments2.getBoolean("THEME_MODE_EDIT");
        }
        p = z;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UserCustomizeThemeFragment", "themeId=" + j + " isModeEdit=" + p);
        PortfolioApp.g0.c().f().a(new en6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(fn6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026emeViewModel::class.java)");
            fn6 fn6 = (fn6) a2;
            this.g = fn6;
            if (fn6 != null) {
                fn6.b().a(getViewLifecycleOwner(), new b(this));
                fn6 fn62 = this.g;
                if (fn62 != null) {
                    fn62.c();
                    this.h = new qw6<>(this, u65);
                    ee7.a((Object) u65, "binding");
                    return u65.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<u65> qw6 = this.h;
        if (qw6 != null) {
            u65 a2 = qw6.a();
            if (a2 != null) {
                ViewPager viewPager = a2.u;
                ee7.a((Object) viewPager, "it.viewPager");
                a(viewPager);
                a2.r.a(a2.u, false);
                a2.s.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(ViewPager viewPager) {
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        dm4 dm4 = new dm4(childFragmentManager);
        vm6 vm6 = new vm6();
        String string = PortfolioApp.g0.c().getString(2131887541);
        ee7.a((Object) string, "PortfolioApp.instance.getString(R.string.text)");
        dm4.a(vm6, string);
        ql6 ql6 = new ql6();
        String string2 = PortfolioApp.g0.c().getString(2131887277);
        ee7.a((Object) string2, "PortfolioApp.instance.getString(R.string.button)");
        dm4.a(ql6, string2);
        kl6 kl6 = new kl6();
        String string3 = PortfolioApp.g0.c().getString(2131887254);
        ee7.a((Object) string3, "PortfolioApp.instance.ge\u2026ring(R.string.background)");
        dm4.a(kl6, string3);
        jm6 jm6 = new jm6();
        String string4 = PortfolioApp.g0.c().getString(2131887512);
        ee7.a((Object) string4, "PortfolioApp.instance.getString(R.string.ring)");
        dm4.a(jm6, string4);
        el6 el6 = new el6();
        String string5 = PortfolioApp.g0.c().getString(2131887224);
        ee7.a((Object) string5, "PortfolioApp.instance.getString(R.string.activity)");
        dm4.a(el6, string5);
        yk6 yk6 = new yk6();
        String string6 = PortfolioApp.g0.c().getString(2131887223);
        ee7.a((Object) string6, "PortfolioApp.instance.ge\u2026(R.string.active_minutes)");
        dm4.a(yk6, string6);
        sk6 sk6 = new sk6();
        String string7 = PortfolioApp.g0.c().getString(2131887222);
        ee7.a((Object) string7, "PortfolioApp.instance.ge\u2026R.string.active_calories)");
        dm4.a(sk6, string7);
        dm6 dm6 = new dm6();
        String string8 = PortfolioApp.g0.c().getString(2131887381);
        ee7.a((Object) string8, "PortfolioApp.instance.ge\u2026ring(R.string.heart_rate)");
        dm4.a(dm6, string8);
        pm6 pm6 = new pm6();
        String string9 = PortfolioApp.g0.c().getString(2131887531);
        ee7.a((Object) string9, "PortfolioApp.instance.getString(R.string.sleep)");
        dm4.a(pm6, string9);
        xl6 xl6 = new xl6();
        String string10 = PortfolioApp.g0.c().getString(2131887374);
        ee7.a((Object) string10, "PortfolioApp.instance.ge\u2026g(R.string.goal_tracking)");
        dm4.a(xl6, string10);
        vl6 vl6 = new vl6();
        String string11 = PortfolioApp.g0.c().getString(2131887369);
        ee7.a((Object) string11, "PortfolioApp.instance.getString(R.string.font)");
        dm4.a(vl6, string11);
        viewPager.setAdapter(dm4);
    }
}
