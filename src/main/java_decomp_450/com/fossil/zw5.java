package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zw5 extends gl5<yw5> {
    @DexIgnore
    void D(boolean z);

    @DexIgnore
    void a(int i, ArrayList<bt5> arrayList);

    @DexIgnore
    void b(int i, ArrayList<String> arrayList);

    @DexIgnore
    void c(int i, ArrayList<bt5> arrayList);

    @DexIgnore
    void close();

    @DexIgnore
    void g(List<Object> list);

    @DexIgnore
    void h();

    @DexIgnore
    void j();

    @DexIgnore
    void k(int i);

    @DexIgnore
    void l();
}
