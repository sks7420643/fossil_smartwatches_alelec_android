package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.y62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k02 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<k02> CREATOR; // = new la2();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    @Deprecated
    public /* final */ int b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public k02(String str, int i, long j) {
        this.a = str;
        this.b = i;
        this.c = j;
    }

    @DexIgnore
    public String e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof k02) {
            k02 k02 = (k02) obj;
            if (((e() == null || !e().equals(k02.e())) && (e() != null || k02.e() != null)) || g() != k02.g()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public long g() {
        long j = this.c;
        return j == -1 ? (long) this.b : j;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(e(), Long.valueOf(g()));
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a("name", e());
        a2.a("version", Long.valueOf(g()));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, e(), false);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, g());
        k72.a(parcel, a2);
    }
}
