package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface eu extends ComponentCallbacks2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static void a(eu euVar) {
            euVar.onTrimMemory(80);
        }

        @DexIgnore
        public static void a(eu euVar, Configuration configuration) {
        }
    }

    @DexIgnore
    void onTrimMemory(int i);
}
