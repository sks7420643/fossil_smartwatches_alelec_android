package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt1 implements y84 {
    @DexIgnore
    public static /* final */ y84 a; // = new kt1();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements u84<jt1> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            jt1 jt1 = (jt1) obj;
            v84 v842 = v84;
            v842.a("sdkVersion", jt1.h());
            v842.a(DeviceRequestsHelper.DEVICE_INFO_MODEL, jt1.e());
            v842.a("hardware", jt1.c());
            v842.a("device", jt1.a());
            v842.a("product", jt1.g());
            v842.a("osBuild", jt1.f());
            v842.a("manufacturer", jt1.d());
            v842.a("fingerprint", jt1.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements u84<st1> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            v84.a("logRequest", ((st1) obj).a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements u84<tt1> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            tt1 tt1 = (tt1) obj;
            v84 v842 = v84;
            v842.a("clientType", tt1.b());
            v842.a("androidClientInfo", tt1.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements u84<ut1> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            ut1 ut1 = (ut1) obj;
            v84 v842 = v84;
            v842.a("eventTimeMs", ut1.b());
            v842.a("eventCode", ut1.a());
            v842.a("eventUptimeMs", ut1.c());
            v842.a("sourceExtension", ut1.e());
            v842.a("sourceExtensionJsonProto3", ut1.f());
            v842.a("timezoneOffsetSeconds", ut1.g());
            v842.a("networkConnectionInfo", ut1.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements u84<vt1> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            vt1 vt1 = (vt1) obj;
            v84 v842 = v84;
            v842.a("requestTimeMs", vt1.f());
            v842.a("requestUptimeMs", vt1.g());
            v842.a("clientInfo", vt1.a());
            v842.a("logSource", vt1.c());
            v842.a("logSourceName", vt1.d());
            v842.a("logEvent", vt1.b());
            v842.a("qosTier", vt1.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements u84<xt1> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.s84
        public void a(Object obj, v84 v84) throws IOException {
            xt1 xt1 = (xt1) obj;
            v84 v842 = v84;
            v842.a("networkType", xt1.b());
            v842.a("mobileSubtype", xt1.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.y84
    public void a(z84<?> z84) {
        z84.a(st1.class, b.a);
        z84.a(mt1.class, b.a);
        z84.a(vt1.class, e.a);
        z84.a(pt1.class, e.a);
        z84.a(tt1.class, c.a);
        z84.a(nt1.class, c.a);
        z84.a(jt1.class, a.a);
        z84.a(lt1.class, a.a);
        z84.a(ut1.class, d.a);
        z84.a(ot1.class, d.a);
        z84.a(xt1.class, f.a);
        z84.a(rt1.class, f.a);
    }
}
