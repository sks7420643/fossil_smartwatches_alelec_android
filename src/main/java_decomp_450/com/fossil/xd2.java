package com.fossil;

import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xd2 {
    @DexIgnore
    public static Set<Scope> a(Collection<Scope> collection) {
        Scope scope;
        HashSet hashSet = new HashSet(collection.size());
        for (Scope scope2 : collection) {
            if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.activity.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.activity.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.location.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.location.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.body.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.body.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.nutrition.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.blood_pressure.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.blood_pressure.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.blood_glucose.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.blood_glucose.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.oxygen_saturation.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.oxygen_saturation.write");
            } else if (scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.body_temperature.read"))) {
                scope = new Scope("https://www.googleapis.com/auth/fitness.body_temperature.write");
            } else {
                scope = scope2.equals(new Scope("https://www.googleapis.com/auth/fitness.reproductive_health.read")) ? new Scope("https://www.googleapis.com/auth/fitness.reproductive_health.write") : scope2;
            }
            if (scope.equals(scope2) || !collection.contains(scope)) {
                hashSet.add(scope2);
            }
        }
        return hashSet;
    }
}
