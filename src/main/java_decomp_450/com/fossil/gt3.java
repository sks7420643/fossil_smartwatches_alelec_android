package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt3<S> extends lt3<S> {
    @DexIgnore
    public zs3<S> b;
    @DexIgnore
    public ws3 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements kt3<S> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.kt3
        public void a(S s) {
            Iterator<kt3<S>> it = ((lt3) gt3.this).a.iterator();
            while (it.hasNext()) {
                it.next().a(s);
            }
        }
    }

    @DexIgnore
    public static <T> gt3<T> a(zs3<T> zs3, ws3 ws3) {
        gt3<T> gt3 = new gt3<>();
        Bundle bundle = new Bundle();
        bundle.putParcelable("DATE_SELECTOR_KEY", zs3);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", ws3);
        gt3.setArguments(bundle);
        return gt3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.b = (zs3) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.c = (ws3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.b.a(layoutInflater, viewGroup, bundle, this.c, new a());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.b);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.c);
    }
}
