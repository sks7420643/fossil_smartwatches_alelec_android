package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wh4 extends lh4 {
    @DexIgnore
    @Override // com.fossil.lh4
    public int a() {
        return 3;
    }

    @DexIgnore
    @Override // com.fossil.lh4, com.fossil.ph4
    public void a(qh4 qh4) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!qh4.i()) {
                break;
            }
            char c = qh4.c();
            qh4.f++;
            a(c, sb);
            if (sb.length() % 3 == 0) {
                lh4.b(qh4, sb);
                int a = sh4.a(qh4.d(), qh4.f, a());
                if (a != a()) {
                    qh4.b(a);
                    break;
                }
            }
        }
        a(qh4, sb);
    }

    @DexIgnore
    @Override // com.fossil.lh4
    public int a(char c, StringBuilder sb) {
        if (c == '\r') {
            sb.append((char) 0);
        } else if (c == '*') {
            sb.append((char) 1);
        } else if (c == '>') {
            sb.append((char) 2);
        } else if (c == ' ') {
            sb.append((char) 3);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            sh4.a(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.lh4
    public void a(qh4 qh4, StringBuilder sb) {
        qh4.l();
        int a = qh4.g().a() - qh4.a();
        qh4.f -= sb.length();
        if (qh4.f() > 1 || a > 1 || qh4.f() != a) {
            qh4.a('\u00fe');
        }
        if (qh4.e() < 0) {
            qh4.b(0);
        }
    }
}
