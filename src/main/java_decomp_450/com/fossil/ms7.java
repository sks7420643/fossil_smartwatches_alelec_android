package com.fossil;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ms7 {
    @DexIgnore
    public static Object a(String str, Object obj) throws js7 {
        return a(str, (Class) obj);
    }

    @DexIgnore
    public static Date b(String str) throws js7 {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @DexIgnore
    public static File c(String str) throws js7 {
        return new File(str);
    }

    @DexIgnore
    public static File[] d(String str) throws js7 {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @DexIgnore
    public static Number e(String str) throws js7 {
        try {
            if (str.indexOf(46) != -1) {
                return Double.valueOf(str);
            }
            return Long.valueOf(str);
        } catch (NumberFormatException e) {
            throw new js7(e.getMessage());
        }
    }

    @DexIgnore
    public static Object f(String str) throws js7 {
        try {
            try {
                return Class.forName(str).newInstance();
            } catch (Exception e) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(e.getClass().getName());
                stringBuffer.append("; Unable to create an instance of: ");
                stringBuffer.append(str);
                throw new js7(stringBuffer.toString());
            }
        } catch (ClassNotFoundException unused) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Unable to find the class: ");
            stringBuffer2.append(str);
            throw new js7(stringBuffer2.toString());
        }
    }

    @DexIgnore
    public static URL g(String str) throws js7 {
        try {
            return new URL(str);
        } catch (MalformedURLException unused) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unable to parse the URL: ");
            stringBuffer.append(str);
            throw new js7(stringBuffer.toString());
        }
    }

    @DexIgnore
    public static Object a(String str, Class cls) throws js7 {
        if (ls7.a == cls) {
            return str;
        }
        if (ls7.b == cls) {
            return f(str);
        }
        if (ls7.c == cls) {
            return e(str);
        }
        if (ls7.d == cls) {
            b(str);
            throw null;
        } else if (ls7.e == cls) {
            return a(str);
        } else {
            if (ls7.g == cls) {
                return c(str);
            }
            if (ls7.f == cls) {
                return c(str);
            }
            if (ls7.h == cls) {
                d(str);
                throw null;
            } else if (ls7.i == cls) {
                return g(str);
            } else {
                return null;
            }
        }
    }

    @DexIgnore
    public static Class a(String str) throws js7 {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException unused) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unable to find the class: ");
            stringBuffer.append(str);
            throw new js7(stringBuffer.toString());
        }
    }
}
