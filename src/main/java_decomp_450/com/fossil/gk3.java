package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ em3 b;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 c;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 d;

    @DexIgnore
    public gk3(ek3 ek3, boolean z, em3 em3, nm3 nm3) {
        this.d = ek3;
        this.a = z;
        this.b = em3;
        this.c = nm3;
    }

    @DexIgnore
    public final void run() {
        bg3 d2 = this.d.d;
        if (d2 == null) {
            this.d.e().t().a("Discarding data. Failed to set user property");
            return;
        }
        this.d.a(d2, this.a ? null : this.b, this.c);
        this.d.J();
    }
}
