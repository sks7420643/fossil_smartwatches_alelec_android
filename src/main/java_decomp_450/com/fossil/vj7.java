package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vj7 extends ti7 {
    @DexIgnore
    public long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public sl7<oj7<?>> d;

    @DexIgnore
    public static /* synthetic */ void b(vj7 vj7, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            vj7.c(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
    }

    @DexIgnore
    public final void a(oj7<?> oj7) {
        sl7<oj7<?>> sl7 = this.d;
        if (sl7 == null) {
            sl7 = new sl7<>();
            this.d = sl7;
        }
        sl7.a(oj7);
    }

    @DexIgnore
    public final long b(boolean z) {
        return z ? 4294967296L : 1;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.b += b(z);
        if (!z) {
            this.c = true;
        }
    }

    @DexIgnore
    public long g() {
        sl7<oj7<?>> sl7 = this.d;
        if (sl7 == null || sl7.b()) {
            return Long.MAX_VALUE;
        }
        return 0;
    }

    @DexIgnore
    public final boolean k() {
        return this.b >= b(true);
    }

    @DexIgnore
    public final boolean l() {
        sl7<oj7<?>> sl7 = this.d;
        if (sl7 != null) {
            return sl7.b();
        }
        return true;
    }

    @DexIgnore
    public long n() {
        if (!o()) {
            return Long.MAX_VALUE;
        }
        return g();
    }

    @DexIgnore
    public final boolean o() {
        oj7<?> c2;
        sl7<oj7<?>> sl7 = this.d;
        if (sl7 == null || (c2 = sl7.c()) == null) {
            return false;
        }
        c2.run();
        return true;
    }

    @DexIgnore
    public boolean q() {
        return false;
    }

    @DexIgnore
    public void shutdown() {
    }

    @DexIgnore
    public static /* synthetic */ void a(vj7 vj7, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            vj7.a(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
    }

    @DexIgnore
    public final void a(boolean z) {
        long b2 = this.b - b(z);
        this.b = b2;
        if (b2 <= 0) {
            if (dj7.a()) {
                if (!(this.b == 0)) {
                    throw new AssertionError();
                }
            }
            if (this.c) {
                shutdown();
            }
        }
    }
}
