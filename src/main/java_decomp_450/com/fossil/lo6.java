package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo6 implements Factory<cl5> {
    @DexIgnore
    public static cl5 a(ko6 ko6) {
        cl5 a = ko6.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
