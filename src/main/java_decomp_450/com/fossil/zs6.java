package com.fossil;

import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface zs6 extends dl4<ys6> {
    @DexIgnore
    void C0();

    @DexIgnore
    void S(String str);

    @DexIgnore
    void a(boolean z, boolean z2, String str);

    @DexIgnore
    void b(int i, String str);

    @DexIgnore
    void b(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void b(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    void b(boolean z, boolean z2);

    @DexIgnore
    void c0();

    @DexIgnore
    void e();

    @DexIgnore
    void e(int i, String str);

    @DexIgnore
    void f();

    @DexIgnore
    void g();

    @DexIgnore
    void i();

    @DexIgnore
    void q();

    @DexIgnore
    void y(boolean z);
}
