package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up7 {
    @DexIgnore
    public static /* final */ br7 d; // = br7.encodeUtf8(":");
    @DexIgnore
    public static /* final */ br7 e; // = br7.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ br7 f; // = br7.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ br7 g; // = br7.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ br7 h; // = br7.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ br7 i; // = br7.encodeUtf8(":authority");
    @DexIgnore
    public /* final */ br7 a;
    @DexIgnore
    public /* final */ br7 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(fo7 fo7);
    }

    @DexIgnore
    public up7(String str, String str2) {
        this(br7.encodeUtf8(str), br7.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof up7)) {
            return false;
        }
        up7 up7 = (up7) obj;
        if (!this.a.equals(up7.a) || !this.b.equals(up7.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.a.hashCode()) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return ro7.a("%s: %s", this.a.utf8(), this.b.utf8());
    }

    @DexIgnore
    public up7(br7 br7, String str) {
        this(br7, br7.encodeUtf8(str));
    }

    @DexIgnore
    public up7(br7 br7, br7 br72) {
        this.a = br7;
        this.b = br72;
        this.c = br7.size() + 32 + br72.size();
    }
}
