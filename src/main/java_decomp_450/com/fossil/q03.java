package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q03 implements tr2<t03> {
    @DexIgnore
    public static q03 b; // = new q03();
    @DexIgnore
    public /* final */ tr2<t03> a;

    @DexIgnore
    public q03(tr2<t03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((t03) b.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ t03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public q03() {
        this(sr2.a(new s03()));
    }
}
