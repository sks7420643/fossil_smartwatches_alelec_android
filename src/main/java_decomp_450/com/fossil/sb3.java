package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sb3 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> a; // = this.b.a.keySet().iterator();
    @DexIgnore
    public /* final */ /* synthetic */ tb3 b;

    @DexIgnore
    public sb3(tb3 tb3) {
        this.b = tb3;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.a.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
