package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m54 extends v54.d.AbstractC0206d.a.b.AbstractC0208a {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a
        public v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a
        public v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a b(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a
        public v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a a(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a
        public v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a b(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a
        public v54.d.AbstractC0206d.a.b.AbstractC0208a a() {
            String str = "";
            if (this.a == null) {
                str = str + " baseAddress";
            }
            if (this.b == null) {
                str = str + " size";
            }
            if (this.c == null) {
                str = str + " name";
            }
            if (str.isEmpty()) {
                return new m54(this.a.longValue(), this.b.longValue(), this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a
    public long a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a
    public long c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.AbstractC0208a
    public String d() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b.AbstractC0208a)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b.AbstractC0208a aVar = (v54.d.AbstractC0206d.a.b.AbstractC0208a) obj;
        if (this.a == aVar.a() && this.b == aVar.c() && this.c.equals(aVar.b())) {
            String str = this.d;
            if (str == null) {
                if (aVar.d() == null) {
                    return true;
                }
            } else if (str.equals(aVar.d())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        int hashCode = (((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.c.hashCode()) * 1000003;
        String str = this.d;
        return (str == null ? 0 : str.hashCode()) ^ hashCode;
    }

    @DexIgnore
    public String toString() {
        return "BinaryImage{baseAddress=" + this.a + ", size=" + this.b + ", name=" + this.c + ", uuid=" + this.d + "}";
    }

    @DexIgnore
    public m54(long j, long j2, String str, String str2) {
        this.a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
    }
}
