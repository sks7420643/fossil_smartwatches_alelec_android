package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vs3 {
    @DexIgnore
    public static int a(View view, int i) {
        return ou3.a(view, i);
    }

    @DexIgnore
    public static int a(Context context, int i, String str) {
        return ou3.a(context, i, str);
    }

    @DexIgnore
    public static int a(View view, int i, int i2) {
        return a(view.getContext(), i, i2);
    }

    @DexIgnore
    public static int a(Context context, int i, int i2) {
        TypedValue a = ou3.a(context, i);
        return a != null ? a.data : i2;
    }

    @DexIgnore
    public static int a(View view, int i, int i2, float f) {
        return a(a(view, i), a(view, i2), f);
    }

    @DexIgnore
    public static int a(int i, int i2, float f) {
        return a(i, e7.c(i2, Math.round(((float) Color.alpha(i2)) * f)));
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return e7.b(i2, i);
    }
}
