package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t65 extends s65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131362927, 1);
        I.put(2131362114, 2);
        I.put(2131362522, 3);
        I.put(2131362932, 4);
        I.put(2131362365, 5);
        I.put(2131362523, 6);
        I.put(2131363012, 7);
        I.put(2131362602, 8);
        I.put(2131362249, 9);
        I.put(2131362113, 10);
        I.put(2131362519, 11);
        I.put(2131362685, 12);
        I.put(2131362404, 13);
        I.put(2131362423, 14);
        I.put(2131362268, 15);
    }
    */

    @DexIgnore
    public t65(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 16, H, I));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public t65(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[10], (ConstraintLayout) objArr[2], (FlexibleButton) objArr[9], (FlexibleButton) objArr[15], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[6], (TabLayout) objArr[8], (ImageView) objArr[12], (DashBar) objArr[1], (FlexibleProgressBar) objArr[4], (ConstraintLayout) objArr[0], (ViewPager2) objArr[7]);
        this.G = -1;
        ((s65) this).E.setTag(null);
        a(view);
        f();
    }
}
