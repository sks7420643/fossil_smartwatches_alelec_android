package com.fossil;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fk4 implements Factory<ContentResolver> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public fk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static fk4 a(wj4 wj4) {
        return new fk4(wj4);
    }

    @DexIgnore
    public static ContentResolver b(wj4 wj4) {
        ContentResolver e = wj4.e();
        c87.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ContentResolver get() {
        return b(this.a);
    }
}
