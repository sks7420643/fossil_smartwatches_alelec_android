package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo0 implements Parcelable.Creator<xp0> {
    @DexIgnore
    public /* synthetic */ bo0(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public xp0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            ru0 valueOf = ru0.valueOf(readString);
            parcel.setDataPosition(0);
            switch (fm0.a[valueOf.ordinal()]) {
                case 1:
                    return we1.CREATOR.createFromParcel(parcel);
                case 2:
                    return ib1.CREATOR.createFromParcel(parcel);
                case 3:
                    return y31.CREATOR.createFromParcel(parcel);
                case 4:
                    return ik0.CREATOR.createFromParcel(parcel);
                case 5:
                    return lm1.CREATOR.createFromParcel(parcel);
                case 6:
                    return bx0.CREATOR.createFromParcel(parcel);
                case 7:
                    return kq1.CREATOR.createFromParcel(parcel);
                case 8:
                    return pi1.CREATOR.createFromParcel(parcel);
                case 9:
                    return gm0.CREATOR.createFromParcel(parcel);
                case 10:
                    return pt0.CREATOR.createFromParcel(parcel);
                case 11:
                    return new l01(parcel);
                case 12:
                    return new t71(parcel);
                default:
                    throw new p87();
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public xp0[] newArray(int i) {
        return new xp0[i];
    }
}
