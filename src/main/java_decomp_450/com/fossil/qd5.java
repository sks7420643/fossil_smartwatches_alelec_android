package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.zendesk.sdk.network.impl.DeviceInfo;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static FirebaseAnalytics c;
    @DexIgnore
    public static qd5 d;
    @DexIgnore
    public static /* final */ HashMap<String, if5> e; // = new HashMap<>();
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public volatile String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(qd5 qd5) {
            qd5.d = qd5;
        }

        @DexIgnore
        public final if5 b(String str) {
            ee7.b(str, "traceName");
            qd5 c = c();
            if (ee7.a((Object) "view_appearance", (Object) str)) {
                return new jf5(c, str, c.a());
            }
            return new if5(c, str, c.a());
        }

        @DexIgnore
        public final if5 c(String str) {
            ee7.b(str, "tracingKey");
            return d().get(str);
        }

        @DexIgnore
        public final HashMap<String, if5> d() {
            return qd5.e;
        }

        @DexIgnore
        public final qd5 e() {
            return qd5.d;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(String str, if5 if5) {
            ee7.b(str, "tracingKey");
            ee7.b(if5, "trace");
            d().put(str, if5);
        }

        @DexIgnore
        public final synchronized qd5 c() {
            qd5 e;
            if (e() == null) {
                a(new qd5(null));
            }
            e = e();
            if (e == null) {
                ee7.a();
                throw null;
            }
            return e;
        }

        @DexIgnore
        public final String d(String str) {
            ee7.b(str, "input");
            return new ah7("[^a-zA-Z0-9]+").replace(str, LocaleConverter.LOCALE_DELIMITER);
        }

        @DexIgnore
        public final void e(String str) {
            ee7.b(str, "tracingKey");
            d().remove(str);
        }

        @DexIgnore
        public final gf5 a(String str) {
            ee7.b(str, "eventName");
            return new gf5(c(), str);
        }

        @DexIgnore
        public final hf5 a() {
            return new hf5(c());
        }

        @DexIgnore
        public final jf5 b() {
            qd5 c = c();
            return new jf5(c, "view_appearance", c.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.AnalyticsHelper$doLogEvent$1", f = "AnalyticsHelper.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bundle $data;
        @DexIgnore
        public /* final */ /* synthetic */ String $event;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Bundle bundle, fb7 fb7) {
            super(2, fb7);
            this.$event = str;
            this.$data = bundle;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$event, this.$data, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                FirebaseAnalytics c = qd5.c;
                if (c != null) {
                    c.a(this.$event, this.$data);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = qd5.class.getSimpleName();
        ee7.a((Object) simpleName, "AnalyticsHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public qd5() {
        c = FirebaseAnalytics.getInstance(PortfolioApp.g0.c());
    }

    @DexIgnore
    public final void b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "set userId: " + str);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str);
        }
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final void c(String str, String str2, int i) {
        ee7.b(str, "styleNumber");
        ee7.b(str2, "deviceName");
        a("sync_success", a(str, str2, i));
    }

    @DexIgnore
    public /* synthetic */ qd5(zd7 zd7) {
        this();
    }

    @DexIgnore
    public final void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "setAnalyticEnable: " + z);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(z);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        b("Optin_Usage", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void b(String str, String str2, String str3) {
        ee7.b(str, Constants.EVENT);
        ee7.b(str2, "paramName");
        ee7.b(str3, "paramValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = b;
        local.d(str4, "Inside .analyticLogEvent event=" + str + ", name=" + str2 + ", value=" + str3);
        Bundle bundle = new Bundle();
        bundle.putString(str2, str3);
        a(str, bundle);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, Constants.EVENT);
        ee7.b(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "Inside .analyticLogEvent event=" + str + ", value=" + str2);
        Bundle bundle = new Bundle();
        bundle.putString(str, str2);
        a(str, bundle);
    }

    @DexIgnore
    public final void b(String str, String str2) {
        ee7.b(str, "propertyName");
        ee7.b(str2, "value");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = b;
        local.d(str3, "User property " + str + " with value=" + str2);
        FirebaseAnalytics firebaseAnalytics = c;
        if (firebaseAnalytics != null) {
            firebaseAnalytics.a(str, str2);
        }
    }

    @DexIgnore
    public final void a(String str, Map<String, ? extends Object> map) {
        ee7.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str + ", value=" + map);
        if (map != null && (!map.isEmpty())) {
            Bundle bundle = new Bundle();
            for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof Integer) {
                    bundle.putInt(key, ((Number) value).intValue());
                } else if (value instanceof Long) {
                    bundle.putLong(key, ((Number) value).longValue());
                } else if (value instanceof Boolean) {
                    bundle.putBoolean(key, ((Boolean) value).booleanValue());
                } else if (value instanceof Float) {
                    bundle.putFloat(key, ((Number) value).floatValue());
                } else if (value != null) {
                    bundle.putString(key, (String) value);
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.String");
                }
            }
            a(str, bundle);
        }
    }

    @DexIgnore
    public final void b(String str, String str2, int i) {
        ee7.b(str, "styleNumber");
        ee7.b(str2, "deviceName");
        a("sync_error", a(str, str2, i));
    }

    @DexIgnore
    public final void b() {
        b("Auth", "None");
    }

    @DexIgnore
    public final void b(boolean z) {
        b("Optin_Emails", z ? "Yes" : "No");
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "Inside .analyticLogEvent event=" + str);
        a(str, (Bundle) null);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(str, bundle, null), 3, null);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        if (map != null && (!map.isEmpty())) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = b;
                local.d(str, "User property " + key + " with value=" + value);
                FirebaseAnalytics firebaseAnalytics = c;
                if (firebaseAnalytics != null) {
                    firebaseAnalytics.a(key, value);
                }
            }
        }
    }

    @DexIgnore
    public final Map<String, String> a(String str, String str2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Style_Number", str);
        hashMap.put("Device_Name", str2);
        hashMap.put("Type", String.valueOf(i));
        return hashMap;
    }

    @DexIgnore
    public final void a(int i, SKUModel sKUModel) {
        if (sKUModel != null) {
            String sku = sKUModel.getSku();
            if (sku != null) {
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                a("sync_start", a(sku, deviceName, i));
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        ee7.b(str, "serialPrefix");
        ee7.b(str2, "activeDeviceName");
        ee7.b(str3, "firmwareVersion");
        hf5 a2 = f.a();
        a2.a("serial_number_prefix", str);
        a2.a(DeviceInfo.DEVICE_INFO_DEVICE_NAME, str2);
        a2.a(Constants.FIRMWARE_VERSION, str3);
        a2.a();
    }

    @DexIgnore
    public final void a(String str, Activity activity) {
        FirebaseAnalytics firebaseAnalytics;
        if (!(str == null || str.length() == 0) && activity != null && (firebaseAnalytics = c) != null) {
            firebaseAnalytics.setCurrentScreen(activity, str, activity.getClass().getSimpleName() + " - " + System.currentTimeMillis());
        }
    }
}
