package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("socialId")
    public String b;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @te4("isDeleted")
    public Boolean e;
    @DexIgnore
    @te4("ranking")
    public Integer f;
    @DexIgnore
    @te4("status")
    public String g;
    @DexIgnore
    @te4("stepsBase")
    public Integer h;
    @DexIgnore
    @te4("stepsOffset")
    public Integer i;
    @DexIgnore
    @te4("caloriesBase")
    public Integer j;
    @DexIgnore
    @te4("caloriesOffset")
    public Integer p;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String q;
    @DexIgnore
    @te4("lastSync")
    public Date r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jn4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public jn4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new jn4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public jn4[] newArray(int i) {
            return new jn4[i];
        }
    }

    @DexIgnore
    public jn4(String str, String str2, String str3, String str4, Boolean bool, Integer num, String str5, Integer num2, Integer num3, Integer num4, Integer num5, String str6, Date date) {
        ee7.b(str, "id");
        ee7.b(str2, "socialId");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = bool;
        this.f = num;
        this.g = str5;
        this.h = num2;
        this.i = num3;
        this.j = num4;
        this.p = num5;
        this.q = str6;
        this.r = date;
    }

    @DexIgnore
    public final Integer a() {
        return this.j;
    }

    @DexIgnore
    public final Integer b() {
        return this.p;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof jn4)) {
            return false;
        }
        jn4 jn4 = (jn4) obj;
        return ee7.a(this.a, jn4.a) && ee7.a(this.b, jn4.b) && ee7.a(this.c, jn4.c) && ee7.a(this.d, jn4.d) && ee7.a(this.e, jn4.e) && ee7.a(this.f, jn4.f) && ee7.a(this.g, jn4.g) && ee7.a(this.h, jn4.h) && ee7.a(this.i, jn4.i) && ee7.a(this.j, jn4.j) && ee7.a(this.p, jn4.p) && ee7.a(this.q, jn4.q) && ee7.a(this.r, jn4.r);
    }

    @DexIgnore
    public final Date f() {
        return this.r;
    }

    @DexIgnore
    public final String g() {
        return this.q;
    }

    @DexIgnore
    public final Integer h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool = this.e;
        int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Integer num = this.f;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        String str5 = this.g;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        Integer num2 = this.h;
        int hashCode8 = (hashCode7 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.i;
        int hashCode9 = (hashCode8 + (num3 != null ? num3.hashCode() : 0)) * 31;
        Integer num4 = this.j;
        int hashCode10 = (hashCode9 + (num4 != null ? num4.hashCode() : 0)) * 31;
        Integer num5 = this.p;
        int hashCode11 = (hashCode10 + (num5 != null ? num5.hashCode() : 0)) * 31;
        String str6 = this.q;
        int hashCode12 = (hashCode11 + (str6 != null ? str6.hashCode() : 0)) * 31;
        Date date = this.r;
        if (date != null) {
            i2 = date.hashCode();
        }
        return hashCode12 + i2;
    }

    @DexIgnore
    public final String i() {
        return this.b;
    }

    @DexIgnore
    public final String j() {
        return this.g;
    }

    @DexIgnore
    public final Integer m() {
        return this.h;
    }

    @DexIgnore
    public final Integer p() {
        return this.i;
    }

    @DexIgnore
    public final Boolean s() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        return "BCPlayer(id=" + this.a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", isDeleted=" + this.e + ", ranking=" + this.f + ", status=" + this.g + ", stepsBase=" + this.h + ", stepsOffset=" + this.i + ", caloriesBase=" + this.j + ", caloriesOffset=" + this.p + ", profilePicture=" + this.q + ", lastSync=" + this.r + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeValue(this.e);
        parcel.writeValue(this.f);
        parcel.writeValue(this.g);
        parcel.writeValue(this.h);
        parcel.writeValue(this.i);
        parcel.writeValue(this.j);
        parcel.writeValue(this.p);
        parcel.writeString(this.q);
        Date date = this.r;
        if (date != null) {
            parcel.writeLong(date.getTime());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public jn4(android.os.Parcel r17) {
        /*
            r16 = this;
            r0 = r17
            java.lang.String r1 = "parcel"
            com.fossil.ee7.b(r0, r1)
            java.lang.String r3 = r17.readString()
            r1 = 0
            if (r3 == 0) goto L_0x00ac
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.ee7.a(r3, r2)
            java.lang.String r4 = r17.readString()
            if (r4 == 0) goto L_0x00a8
            com.fossil.ee7.a(r4, r2)
            java.lang.String r5 = r17.readString()
            java.lang.String r6 = r17.readString()
            java.lang.Class r2 = java.lang.Boolean.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r7 = r2 instanceof java.lang.Boolean
            if (r7 != 0) goto L_0x0033
            r2 = r1
        L_0x0033:
            r7 = r2
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r8 = r2 instanceof java.lang.Integer
            if (r8 != 0) goto L_0x0045
            r2 = r1
        L_0x0045:
            r8 = r2
            java.lang.Integer r8 = (java.lang.Integer) r8
            java.lang.String r9 = r17.readString()
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r10 = r2 instanceof java.lang.Integer
            if (r10 != 0) goto L_0x005b
            r2 = r1
        L_0x005b:
            r10 = r2
            java.lang.Integer r10 = (java.lang.Integer) r10
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r11 = r2 instanceof java.lang.Integer
            if (r11 != 0) goto L_0x006d
            r2 = r1
        L_0x006d:
            r11 = r2
            java.lang.Integer r11 = (java.lang.Integer) r11
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r12 = r2 instanceof java.lang.Integer
            if (r12 != 0) goto L_0x007f
            r2 = r1
        L_0x007f:
            r12 = r2
            java.lang.Integer r12 = (java.lang.Integer) r12
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r13 = r2 instanceof java.lang.Integer
            if (r13 != 0) goto L_0x0091
            goto L_0x0092
        L_0x0091:
            r1 = r2
        L_0x0092:
            r13 = r1
            java.lang.Integer r13 = (java.lang.Integer) r13
            java.lang.String r14 = r17.readString()
            java.util.Date r15 = new java.util.Date
            long r0 = r17.readLong()
            r15.<init>(r0)
            r2 = r16
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        L_0x00a8:
            com.fossil.ee7.a()
            throw r1
        L_0x00ac:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jn4.<init>(android.os.Parcel):void");
    }
}
