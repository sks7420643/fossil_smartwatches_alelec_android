package com.fossil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k5 {
    @DexIgnore
    public List<i5> a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public /* final */ int[] e; // = {-1, -1};
    @DexIgnore
    public List<i5> f; // = new ArrayList();
    @DexIgnore
    public List<i5> g; // = new ArrayList();
    @DexIgnore
    public HashSet<i5> h; // = new HashSet<>();
    @DexIgnore
    public HashSet<i5> i; // = new HashSet<>();
    @DexIgnore
    public List<i5> j; // = new ArrayList();
    @DexIgnore
    public List<i5> k; // = new ArrayList();

    @DexIgnore
    public k5(List<i5> list) {
        this.a = list;
    }

    @DexIgnore
    public List<i5> a(int i2) {
        if (i2 == 0) {
            return this.f;
        }
        if (i2 == 1) {
            return this.g;
        }
        return null;
    }

    @DexIgnore
    public Set<i5> b(int i2) {
        if (i2 == 0) {
            return this.h;
        }
        if (i2 == 1) {
            return this.i;
        }
        return null;
    }

    @DexIgnore
    public void a(i5 i5Var, int i2) {
        if (i2 == 0) {
            this.h.add(i5Var);
        } else if (i2 == 1) {
            this.i.add(i5Var);
        }
    }

    @DexIgnore
    public void b() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            a(this.k.get(i2));
        }
    }

    @DexIgnore
    public List<i5> a() {
        if (!this.j.isEmpty()) {
            return this.j;
        }
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            i5 i5Var = this.a.get(i2);
            if (!i5Var.b0) {
                a((ArrayList) this.j, i5Var);
            }
        }
        this.k.clear();
        this.k.addAll(this.a);
        this.k.removeAll(this.j);
        return this.j;
    }

    @DexIgnore
    public k5(List<i5> list, boolean z) {
        this.a = list;
        this.d = z;
    }

    @DexIgnore
    public final void a(ArrayList<i5> arrayList, i5 i5Var) {
        if (!i5Var.d0) {
            arrayList.add(i5Var);
            i5Var.d0 = true;
            if (!i5Var.z()) {
                if (i5Var instanceof m5) {
                    m5 m5Var = (m5) i5Var;
                    int i2 = m5Var.l0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        a(arrayList, m5Var.k0[i3]);
                    }
                }
                int length = i5Var.A.length;
                for (int i4 = 0; i4 < length; i4++) {
                    h5 h5Var = i5Var.A[i4].d;
                    if (h5Var != null) {
                        i5 i5Var2 = h5Var.b;
                        if (!(h5Var == null || i5Var2 == i5Var.l())) {
                            a(arrayList, i5Var2);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.i5 r7) {
        /*
            r6 = this;
            boolean r0 = r7.b0
            if (r0 == 0) goto L_0x00de
            boolean r0 = r7.z()
            if (r0 == 0) goto L_0x000b
            return
        L_0x000b:
            com.fossil.h5 r0 = r7.u
            com.fossil.h5 r0 = r0.d
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0015
            r0 = 1
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 == 0) goto L_0x001d
            com.fossil.h5 r3 = r7.u
            com.fossil.h5 r3 = r3.d
            goto L_0x0021
        L_0x001d:
            com.fossil.h5 r3 = r7.s
            com.fossil.h5 r3 = r3.d
        L_0x0021:
            if (r3 == 0) goto L_0x0045
            com.fossil.i5 r4 = r3.b
            boolean r5 = r4.c0
            if (r5 != 0) goto L_0x002c
            r6.a(r4)
        L_0x002c:
            com.fossil.h5$d r4 = r3.c
            com.fossil.h5$d r5 = com.fossil.h5.d.RIGHT
            if (r4 != r5) goto L_0x003c
            com.fossil.i5 r3 = r3.b
            int r4 = r3.I
            int r3 = r3.t()
            int r4 = r4 + r3
            goto L_0x0046
        L_0x003c:
            com.fossil.h5$d r5 = com.fossil.h5.d.LEFT
            if (r4 != r5) goto L_0x0045
            com.fossil.i5 r3 = r3.b
            int r4 = r3.I
            goto L_0x0046
        L_0x0045:
            r4 = 0
        L_0x0046:
            if (r0 == 0) goto L_0x0050
            com.fossil.h5 r0 = r7.u
            int r0 = r0.b()
            int r4 = r4 - r0
            goto L_0x005c
        L_0x0050:
            com.fossil.h5 r0 = r7.s
            int r0 = r0.b()
            int r3 = r7.t()
            int r0 = r0 + r3
            int r4 = r4 + r0
        L_0x005c:
            int r0 = r7.t()
            int r0 = r4 - r0
            r7.a(r0, r4)
            com.fossil.h5 r0 = r7.w
            com.fossil.h5 r0 = r0.d
            if (r0 == 0) goto L_0x0087
            com.fossil.i5 r1 = r0.b
            boolean r3 = r1.c0
            if (r3 != 0) goto L_0x0074
            r6.a(r1)
        L_0x0074:
            com.fossil.i5 r0 = r0.b
            int r1 = r0.J
            int r0 = r0.Q
            int r1 = r1 + r0
            int r0 = r7.Q
            int r1 = r1 - r0
            int r0 = r7.F
            int r0 = r0 + r1
            r7.e(r1, r0)
            r7.c0 = r2
            return
        L_0x0087:
            com.fossil.h5 r0 = r7.v
            com.fossil.h5 r0 = r0.d
            if (r0 == 0) goto L_0x008e
            r1 = 1
        L_0x008e:
            if (r1 == 0) goto L_0x0095
            com.fossil.h5 r0 = r7.v
            com.fossil.h5 r0 = r0.d
            goto L_0x0099
        L_0x0095:
            com.fossil.h5 r0 = r7.t
            com.fossil.h5 r0 = r0.d
        L_0x0099:
            if (r0 == 0) goto L_0x00bd
            com.fossil.i5 r3 = r0.b
            boolean r5 = r3.c0
            if (r5 != 0) goto L_0x00a4
            r6.a(r3)
        L_0x00a4:
            com.fossil.h5$d r3 = r0.c
            com.fossil.h5$d r5 = com.fossil.h5.d.BOTTOM
            if (r3 != r5) goto L_0x00b5
            com.fossil.i5 r0 = r0.b
            int r3 = r0.J
            int r0 = r0.j()
            int r4 = r3 + r0
            goto L_0x00bd
        L_0x00b5:
            com.fossil.h5$d r5 = com.fossil.h5.d.TOP
            if (r3 != r5) goto L_0x00bd
            com.fossil.i5 r0 = r0.b
            int r4 = r0.J
        L_0x00bd:
            if (r1 == 0) goto L_0x00c7
            com.fossil.h5 r0 = r7.v
            int r0 = r0.b()
            int r4 = r4 - r0
            goto L_0x00d3
        L_0x00c7:
            com.fossil.h5 r0 = r7.t
            int r0 = r0.b()
            int r1 = r7.j()
            int r0 = r0 + r1
            int r4 = r4 + r0
        L_0x00d3:
            int r0 = r7.j()
            int r0 = r4 - r0
            r7.e(r0, r4)
            r7.c0 = r2
        L_0x00de:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k5.a(com.fossil.i5):void");
    }
}
