package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.fossil.oe;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ne extends me<Cursor> {
    @DexIgnore
    public /* final */ oe<Cursor>.a a; // = new oe.a();
    @DexIgnore
    public Uri b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String[] e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Cursor g;
    @DexIgnore
    public d8 h;

    @DexIgnore
    public ne(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        super(context);
        this.b = uri;
        this.c = strArr;
        this.d = str;
        this.e = strArr2;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.me
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (this.h != null) {
                this.h.a();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.me, com.fossil.oe
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.b);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.c));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.d);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.e));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.f);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.g);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(((oe) this).mContentChanged);
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onReset() {
        super.onReset();
        onStopLoading();
        Cursor cursor = this.g;
        if (cursor != null && !cursor.isClosed()) {
            this.g.close();
        }
        this.g = null;
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onStartLoading() {
        Cursor cursor = this.g;
        if (cursor != null) {
            deliverResult(cursor);
        }
        if (takeContentChanged() || this.g == null) {
            forceLoad();
        }
    }

    @DexIgnore
    @Override // com.fossil.oe
    public void onStopLoading() {
        cancelLoad();
    }

    @DexIgnore
    public void deliverResult(Cursor cursor) {
        if (!isReset()) {
            Cursor cursor2 = this.g;
            this.g = cursor;
            if (isStarted()) {
                super.deliverResult((Object) cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.me
    public Cursor loadInBackground() {
        synchronized (this) {
            if (!isLoadInBackgroundCanceled()) {
                this.h = new d8();
            } else {
                throw new k8();
            }
        }
        try {
            Cursor a2 = u6.a(getContext().getContentResolver(), this.b, this.c, this.d, this.e, this.f, this.h);
            if (a2 != null) {
                try {
                    a2.getCount();
                    a2.registerContentObserver(this.a);
                } catch (RuntimeException e2) {
                    a2.close();
                    throw e2;
                }
            }
            synchronized (this) {
                this.h = null;
            }
            return a2;
        } catch (Throwable th) {
            synchronized (this) {
                this.h = null;
                throw th;
            }
        }
    }

    @DexIgnore
    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
