package com.fossil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.c36;
import com.fossil.cy6;
import com.fossil.e36;
import com.fossil.h36;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a36 extends ho5 implements e36.d, CropImageView.i, CropImageView.g, FragmentManager.g, cy6.g {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public c36 g;
    @DexIgnore
    public qw6<w05> h;
    @DexIgnore
    public Uri i;
    @DexIgnore
    public e36 j;
    @DexIgnore
    public /* final */ ArrayList<FilterType> p;
    @DexIgnore
    public FilterType q;
    @DexIgnore
    public rj4 r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final a36 a(Uri uri, ArrayList<fz5> arrayList) {
            ee7.b(uri, "uri");
            ee7.b(arrayList, "complications");
            a36 a36 = new a36();
            Bundle bundle = new Bundle();
            bundle.putParcelable("MAGE_URI_ARG", uri);
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", arrayList);
            a36.setArguments(bundle);
            return a36;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<c36.e> {
        @DexIgnore
        public /* final */ /* synthetic */ a36 a;

        @DexIgnore
        public b(a36 a36) {
            this.a = a36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(c36.e eVar) {
            TextView textView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EditPhotoFragment", "Crop image: result = " + eVar);
            this.a.a();
            w05 b = this.a.i1();
            if (!(b == null || (textView = b.w) == null)) {
                textView.setClickable(true);
            }
            if (eVar.a() && this.a.getActivity() != null) {
                if (a36.f(this.a).c() != null) {
                    a36 a36 = this.a;
                    h36.a aVar = h36.j;
                    ArrayList<fz5> c = a36.f(a36).c();
                    if (c != null) {
                        a36.a(aVar.a(c, this.a.q), "PreviewFragment", 2131362149);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "Can not initialize PreviewFragment without complications");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<c36.b> {
        @DexIgnore
        public /* final */ /* synthetic */ a36 a;

        @DexIgnore
        public c(a36 a36) {
            this.a = a36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(c36.b bVar) {
            List<FilterResult> a2 = bVar.a();
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(a2));
            this.a.a();
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (FilterResult filterResult : bVar.a()) {
                    Bitmap preview = filterResult.getPreview();
                    ee7.a((Object) preview, "filter.preview");
                    Bitmap a3 = jr5.a(preview);
                    ee7.a((Object) a3, "circleBitmap");
                    FilterType type = filterResult.getType();
                    ee7.a((Object) type, "filter.type");
                    arrayList.add(new e36.b(a3, type));
                }
                this.a.q = ((e36.b) arrayList.get(0)).b();
                e36 c = this.a.j;
                if (c != null) {
                    c.a(arrayList);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<c36.a> {
        @DexIgnore
        public /* final */ /* synthetic */ a36 a;

        @DexIgnore
        public d(a36 a36) {
            this.a = a36;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(c36.a aVar) {
            CropImageView cropImageView;
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(aVar.a()));
            FilterResult a2 = aVar.a();
            w05 b = this.a.i1();
            if (!(b == null || (cropImageView = b.q) == null)) {
                cropImageView.a(a2.getPreview());
            }
            a36 a36 = this.a;
            FilterType type = a2.getType();
            ee7.a((Object) type, "it.type");
            a36.q = type;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a36 a;

        @DexIgnore
        public e(a36 a36, w05 w05) {
            this.a = a36;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a36 a;

        @DexIgnore
        public f(a36 a36, w05 w05) {
            this.a = a36;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.i != null) {
                this.a.g1();
            } else {
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Can not crop image cause of null imageURI");
            }
        }
    }

    @DexIgnore
    public a36() {
        ArrayList<FilterType> arrayList = new ArrayList<>();
        arrayList.add(FilterType.ATKINSON_DITHERING);
        arrayList.add(FilterType.BURKES_DITHERING);
        arrayList.add(FilterType.DIRECT_MAPPING);
        arrayList.add(FilterType.JAJUNI_DITHERING);
        arrayList.add(FilterType.ORDERED_DITHERING);
        arrayList.add(FilterType.SIERRA_DITHERING);
        this.p = arrayList;
        FilterType filterType = arrayList.get(0);
        ee7.a((Object) filterType, "mFilterList[0]");
        this.q = filterType;
    }

    @DexIgnore
    public static final /* synthetic */ c36 f(a36 a36) {
        c36 c36 = a36.g;
        if (c36 != null) {
            return c36;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        c36 c36 = this.g;
        if (c36 != null) {
            c36.d().a(getViewLifecycleOwner(), new b(this));
            c36 c362 = this.g;
            if (c362 != null) {
                c362.b().a(getViewLifecycleOwner(), new c(this));
                c36 c363 = this.g;
                if (c363 != null) {
                    c363.a().a(getViewLifecycleOwner(), new d(this));
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "cropImage()");
        c36.d h1 = h1();
        if (h1 != null) {
            b();
            c36 c36 = this.g;
            if (c36 != null) {
                c36.a(h1, this.q);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final c36.d h1() {
        CropImageView cropImageView;
        ConstraintLayout constraintLayout;
        w05 i1 = i1();
        if (!(i1 == null || (constraintLayout = i1.r) == null)) {
            constraintLayout.invalidate();
        }
        w05 i12 = i1();
        if (i12 == null || (cropImageView = i12.q) == null) {
            return null;
        }
        ee7.a((Object) cropImageView, "it");
        if (cropImageView.getInitializeBitmap() == null) {
            return null;
        }
        cropImageView.clearAnimation();
        float[] fArr = new float[8];
        float[] cropPoints = cropImageView.getCropPoints();
        ee7.a((Object) cropPoints, "it.cropPoints");
        int length = cropPoints.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            fArr[i3] = cropPoints[i2] / ((float) cropImageView.getLoadedSampleSize());
            i2++;
            i3++;
        }
        Bitmap initializeBitmap = cropImageView.getInitializeBitmap();
        ee7.a((Object) initializeBitmap, "it.initializeBitmap");
        int rotatedDegrees = cropImageView.getRotatedDegrees();
        boolean c2 = cropImageView.c();
        Object obj = cropImageView.getAspectRatio().first;
        ee7.a(obj, "it.aspectRatio.first");
        int intValue = ((Number) obj).intValue();
        Object obj2 = cropImageView.getAspectRatio().second;
        ee7.a(obj2, "it.aspectRatio.second");
        return new c36.d(initializeBitmap, fArr, rotatedDegrees, c2, intValue, ((Number) obj2).intValue(), cropImageView.d(), cropImageView.e());
    }

    @DexIgnore
    public final w05 i1() {
        qw6<w05> qw6 = this.h;
        if (qw6 != null) {
            return qw6.a();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "updateFilterList()");
        c36.d h1 = h1();
        if (h1 != null) {
            c36 c36 = this.g;
            if (c36 != null) {
                c36.a(h1, this.p);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            b(bundle);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.g
    public void onBackStackChanged() {
        CropImageView cropImageView;
        CropImageView cropImageView2;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onBackStackChanged()");
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        if (childFragmentManager.t() == 0) {
            qw6<w05> qw6 = this.h;
            if (qw6 != null) {
                w05 a2 = qw6.a();
                if (a2 != null && (cropImageView2 = a2.q) != null) {
                    cropImageView2.setOnSetImageUriCompleteListener(this);
                    cropImageView2.setOnSetCropOverlayReleasedListener(this);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<w05> qw62 = this.h;
        if (qw62 != null) {
            w05 a3 = qw62.a();
            if (a3 != null && (cropImageView = a3.q) != null) {
                cropImageView.a();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().j().a(this);
        FragmentActivity requireActivity = requireActivity();
        rj4 rj4 = this.r;
        if (rj4 != null) {
            he a2 = je.a(requireActivity, rj4).a(c36.class);
            ee7.a((Object) a2, "ViewModelProviders.of(re\u2026otoViewModel::class.java)");
            this.g = (c36) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                ee7.a((Object) arguments, "it");
                b(arguments);
            }
            getChildFragmentManager().a((FragmentManager.g) this);
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        w05 w05 = (w05) qb.a(LayoutInflater.from(getContext()), 2131558550, null, true, a1());
        this.h = new qw6<>(this, w05);
        this.j = new e36(null, this, 1, null);
        qw6<w05> qw6 = this.h;
        if (qw6 != null) {
            w05 a2 = qw6.a();
            if (a2 != null) {
                TextView textView = a2.v;
                ee7.a((Object) textView, "fragmentBinding.tvCancel");
                bf5.a(textView, new e(this, w05));
                TextView textView2 = a2.w;
                ee7.a((Object) textView2, "fragmentBinding.tvPreview");
                bf5.a(textView2, new f(this, w05));
                if (this.i != null) {
                    a2.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView = a2.q;
                    if (cropImageView != null) {
                        cropImageView.setOnSetCropOverlayReleasedListener(this);
                    }
                    b();
                    a2.q.a(this.i, this.q);
                } else {
                    a();
                }
                RecyclerView recyclerView = w05.u;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.j);
            }
            f1();
            ee7.a((Object) w05, "binding");
            return w05.d();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        getChildFragmentManager().b((FragmentManager.g) this);
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        CropImageView cropImageView;
        qw6<w05> qw6 = this.h;
        if (qw6 != null) {
            w05 a2 = qw6.a();
            if (!(a2 == null || (cropImageView = a2.q) == null)) {
                cropImageView.a();
            }
            e36 e36 = this.j;
            if (e36 != null) {
                e36.a((e36.d) null);
            }
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("MAGE_URI_ARG", this.i);
        c36 c36 = this.g;
        if (c36 != null) {
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", c36.c());
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "initialize()");
        ArrayList<fz5> arrayList = null;
        if (bundle.containsKey("MAGE_URI_ARG")) {
            Bundle arguments = getArguments();
            this.i = arguments != null ? (Uri) arguments.getParcelable("MAGE_URI_ARG") : null;
        }
        if (bundle.containsKey("COMPLICATIONS_ARG")) {
            c36 c36 = this.g;
            if (c36 != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getParcelableArrayList("COMPLICATIONS_ARG");
                }
                c36.a(arrayList);
                return;
            }
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.e36.d
    public void a(e36.b bVar) {
        w05 i1;
        CropImageView cropImageView;
        Bitmap initializeBitmap;
        ee7.b(bVar, "imageFilter");
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(bVar.b()));
        if (this.q != bVar.b() && (i1 = i1()) != null && (cropImageView = i1.q) != null && (initializeBitmap = cropImageView.getInitializeBitmap()) != null) {
            c36 c36 = this.g;
            if (c36 != null) {
                c36.a(initializeBitmap, bVar.b());
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.i
    public void a(CropImageView cropImageView, Uri uri, Exception exc) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onSetImageUriComplete, uri = " + uri + ", error = " + exc);
        if (isActive()) {
            if (exc == null) {
                j1();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("EditPhotoFragment", "error = " + exc);
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.G(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.g
    public void a(Rect rect) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onCropOverlayReleased()");
        j1();
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onDialogFragmentResult tag = " + str);
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() == 407101428 && str.equals("PROCESS_IMAGE_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        super.a(str, i2, intent);
    }
}
