package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os5 extends ac implements View.OnClickListener {
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public b a;
    @DexIgnore
    public String b;
    @DexIgnore
    public FlexibleTextView c;
    @DexIgnore
    public FlexibleEditText d;
    @DexIgnore
    public ImageView e;
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final os5 a(String str, b bVar) {
            ee7.b(bVar, "listener");
            os5 os5 = new os5();
            os5.a = bVar;
            os5.b = str;
            return os5;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void onCancel();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ os5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(os5 os5) {
            this.a = os5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ee7.b(editable, "text");
            os5 os5 = this.a;
            String obj = editable.toString();
            int length = obj.length() - 1;
            int i = 0;
            boolean z = false;
            while (i <= length) {
                boolean z2 = obj.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            os5.b = obj.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.a.b);
            os5.a(this.a).setEnabled(z3);
            if (z3) {
                os5.b(this.a).setVisibility(0);
            } else {
                os5.b(this.a).setVisibility(8);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "text");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "text");
        }
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView a(os5 os5) {
        FlexibleTextView flexibleTextView = os5.c;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        ee7.d("ftvRename");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ImageView b(os5 os5) {
        ImageView imageView = os5.e;
        if (imageView != null) {
            return imageView;
        }
        ee7.d("ivClearName");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        b bVar = this.a;
        if (bVar != null) {
            if (z) {
                bVar.onCancel();
            } else {
                String str = this.b;
                if (str != null) {
                    bVar.a(str);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public void Z0() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362352) {
                T(true);
            } else if (id != 2131362484) {
                if (id == 2131362654) {
                    FlexibleEditText flexibleEditText = this.d;
                    if (flexibleEditText != null) {
                        flexibleEditText.setText("");
                    } else {
                        ee7.d("fetRename");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.b)) {
                T(false);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 2131951890);
    }

    @DexIgnore
    @Override // com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        ee7.a((Object) onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558615, viewGroup);
        ViewGroup viewGroup2 = (ViewGroup) inflate.findViewById(2131362962);
        String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        if (b2 != null) {
            viewGroup2.setBackgroundColor(Color.parseColor(b2));
        }
        View findViewById = inflate.findViewById(2131362279);
        ee7.a((Object) findViewById, "view.findViewById(R.id.fet_rename)");
        this.d = (FlexibleEditText) findViewById;
        View findViewById2 = inflate.findViewById(2131362484);
        ee7.a((Object) findViewById2, "view.findViewById(R.id.ftv_rename)");
        this.c = (FlexibleTextView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362654);
        ee7.a((Object) findViewById3, "view.findViewById(R.id.iv_clear_name)");
        this.e = (ImageView) findViewById3;
        String str = this.b;
        if (str != null) {
            FlexibleEditText flexibleEditText = this.d;
            if (flexibleEditText != null) {
                flexibleEditText.setText(str);
                FlexibleEditText flexibleEditText2 = this.d;
                if (flexibleEditText2 != null) {
                    flexibleEditText2.setSelection(str.length());
                    if (str.length() == 0) {
                        ImageView imageView = this.e;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        } else {
                            ee7.d("ivClearName");
                            throw null;
                        }
                    } else {
                        ImageView imageView2 = this.e;
                        if (imageView2 != null) {
                            imageView2.setVisibility(0);
                        } else {
                            ee7.d("ivClearName");
                            throw null;
                        }
                    }
                } else {
                    ee7.d("fetRename");
                    throw null;
                }
            } else {
                ee7.d("fetRename");
                throw null;
            }
        }
        FlexibleEditText flexibleEditText3 = this.d;
        if (flexibleEditText3 != null) {
            flexibleEditText3.addTextChangedListener(new c(this));
            ImageView imageView3 = this.e;
            if (imageView3 != null) {
                imageView3.setOnClickListener(this);
                FlexibleTextView flexibleTextView = this.c;
                if (flexibleTextView != null) {
                    flexibleTextView.setOnClickListener(this);
                    inflate.findViewById(2131362352).setOnClickListener(this);
                    return inflate;
                }
                ee7.d("ftvRename");
                throw null;
            }
            ee7.d("ivClearName");
            throw null;
        }
        ee7.d("fetRename");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }
}
