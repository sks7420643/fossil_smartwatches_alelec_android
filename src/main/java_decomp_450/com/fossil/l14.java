package com.fossil;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.q12;
import com.fossil.y62;
import com.google.firebase.components.ComponentDiscoveryService;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l14 {
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static /* final */ Executor j; // = new d();
    @DexIgnore
    public static /* final */ Map<String, l14> k; // = new n4();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ n14 c;
    @DexIgnore
    public /* final */ k24 d;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ r24<nc4> g;
    @DexIgnore
    public /* final */ List<b> h; // = new CopyOnWriteArrayList();

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(14)
    public static class c implements q12.a {
        @DexIgnore
        public static AtomicReference<c> a; // = new AtomicReference<>();

        @DexIgnore
        public static void b(Context context) {
            if (v92.a() && (context.getApplicationContext() instanceof Application)) {
                Application application = (Application) context.getApplicationContext();
                if (a.get() == null) {
                    c cVar = new c();
                    if (a.compareAndSet(null, cVar)) {
                        q12.a(application);
                        q12.b().a(cVar);
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.q12.a
        public void a(boolean z) {
            synchronized (l14.i) {
                Iterator it = new ArrayList(l14.k.values()).iterator();
                while (it.hasNext()) {
                    l14 l14 = (l14) it.next();
                    if (l14.e.get()) {
                        l14.a(z);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Executor {
        @DexIgnore
        public static /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            a.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(24)
    public static class e extends BroadcastReceiver {
        @DexIgnore
        public static AtomicReference<e> b; // = new AtomicReference<>();
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public e(Context context) {
            this.a = context;
        }

        @DexIgnore
        public static void b(Context context) {
            if (b.get() == null) {
                e eVar = new e(context);
                if (b.compareAndSet(null, eVar)) {
                    context.registerReceiver(eVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            synchronized (l14.i) {
                for (l14 l14 : l14.k.values()) {
                    l14.f();
                }
            }
            a();
        }

        @DexIgnore
        public void a() {
            this.a.unregisterReceiver(this);
        }
    }

    @DexIgnore
    public l14(Context context, String str, n14 n14) {
        new CopyOnWriteArrayList();
        a72.a(context);
        this.a = context;
        a72.b(str);
        this.b = str;
        a72.a(n14);
        this.c = n14;
        List<g24> a2 = e24.a(context, ComponentDiscoveryService.class).a();
        String a3 = sd4.a();
        Executor executor = j;
        c24[] c24Arr = new c24[8];
        c24Arr[0] = c24.a(context, Context.class, new Class[0]);
        c24Arr[1] = c24.a(this, l14.class, new Class[0]);
        c24Arr[2] = c24.a(n14, n14.class, new Class[0]);
        c24Arr[3] = ud4.a("fire-android", "");
        c24Arr[4] = ud4.a("fire-core", "19.3.0");
        c24Arr[5] = a3 != null ? ud4.a("kotlin", a3) : null;
        c24Arr[6] = qd4.b();
        c24Arr[7] = k94.a();
        this.d = new k24(executor, a2, c24Arr);
        this.g = new r24<>(k14.a(this, context));
    }

    @DexIgnore
    public static l14 j() {
        l14 l14;
        synchronized (i) {
            l14 = k.get("[DEFAULT]");
            if (l14 == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + w92.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return l14;
    }

    @DexIgnore
    public String c() {
        a();
        return this.b;
    }

    @DexIgnore
    public n14 d() {
        a();
        return this.c;
    }

    @DexIgnore
    public String e() {
        return l92.a(c().getBytes(Charset.defaultCharset())) + vt7.ANY_NON_NULL_MARKER + l92.a(d().b().getBytes(Charset.defaultCharset()));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof l14)) {
            return false;
        }
        return this.b.equals(((l14) obj).c());
    }

    @DexIgnore
    public final void f() {
        if (!m8.a(this.a)) {
            e.b(this.a);
        } else {
            this.d.a(h());
        }
    }

    @DexIgnore
    public boolean g() {
        a();
        return this.g.get().a();
    }

    @DexIgnore
    public boolean h() {
        return "[DEFAULT]".equals(c());
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a("name", this.b);
        a2.a("options", this.c);
        return a2.toString();
    }

    @DexIgnore
    public Context b() {
        a();
        return this.a;
    }

    @DexIgnore
    public static l14 a(Context context) {
        synchronized (i) {
            if (k.containsKey("[DEFAULT]")) {
                return j();
            }
            n14 a2 = n14.a(context);
            if (a2 == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            return a(context, a2);
        }
    }

    @DexIgnore
    public static l14 a(Context context, n14 n14) {
        return a(context, n14, "[DEFAULT]");
    }

    @DexIgnore
    public static l14 a(Context context, n14 n14, String str) {
        l14 l14;
        c.b(context);
        String a2 = a(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (i) {
            boolean z = !k.containsKey(a2);
            a72.b(z, "FirebaseApp name " + a2 + " already exists!");
            a72.a(context, "Application context cannot be null.");
            l14 = new l14(context, a2, n14);
            k.put(a2, l14);
        }
        l14.f();
        return l14;
    }

    @DexIgnore
    public <T> T a(Class<T> cls) {
        a();
        return (T) this.d.get(cls);
    }

    @DexIgnore
    public static /* synthetic */ nc4 a(l14 l14, Context context) {
        return new nc4(context, l14.e(), (h94) l14.d.get(h94.class));
    }

    @DexIgnore
    public final void a() {
        a72.b(!this.f.get(), "FirebaseApp was deleted");
    }

    @DexIgnore
    public final void a(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (b bVar : this.h) {
            bVar.a(z);
        }
    }

    @DexIgnore
    public static String a(String str) {
        return str.trim();
    }
}
