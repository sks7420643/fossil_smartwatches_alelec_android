package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g97 implements Collection<f97>, ye7 {
    @DexIgnore
    public /* final */ short[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ya7 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ short[] b;

        @DexIgnore
        public a(short[] sArr) {
            ee7.b(sArr, "array");
            this.b = sArr;
        }

        @DexIgnore
        @Override // com.fossil.ya7
        public short a() {
            int i = this.a;
            short[] sArr = this.b;
            if (i < sArr.length) {
                this.a = i + 1;
                short s = sArr[i];
                f97.c(s);
                return s;
            }
            throw new NoSuchElementException(String.valueOf(this.a));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(short[] sArr, Object obj) {
        return (obj instanceof g97) && ee7.a(sArr, ((g97) obj).b());
    }

    @DexIgnore
    public static int b(short[] sArr) {
        if (sArr != null) {
            return Arrays.hashCode(sArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(short[] sArr) {
        return sArr.length == 0;
    }

    @DexIgnore
    public static ya7 d(short[] sArr) {
        return new a(sArr);
    }

    @DexIgnore
    public static String e(short[] sArr) {
        return "UShortArray(storage=" + Arrays.toString(sArr) + ")";
    }

    @DexIgnore
    public boolean a(short s) {
        return a(this.a, s);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(f97 f97) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends f97> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ short[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof f97) {
            return a(((f97) obj).a());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<f97>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.ya7' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<f97> iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(short[] sArr) {
        return sArr.length;
    }

    @DexIgnore
    public static boolean a(short[] sArr, short s) {
        return t97.a(sArr, s);
    }

    @DexIgnore
    public static boolean a(short[] sArr, Collection<f97> collection) {
        boolean z;
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof f97) || !t97.a(sArr, t.a())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }
}
