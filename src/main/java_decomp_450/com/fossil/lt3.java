package com.fossil;

import androidx.fragment.app.Fragment;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lt3<S> extends Fragment {
    @DexIgnore
    public /* final */ LinkedHashSet<kt3<S>> a; // = new LinkedHashSet<>();

    @DexIgnore
    public void Z0() {
        this.a.clear();
    }

    @DexIgnore
    public boolean a(kt3<S> kt3) {
        return this.a.add(kt3);
    }
}
