package com.fossil;

import com.fossil.du1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ku1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(ju1 ju1);

        @DexIgnore
        public abstract a a(Integer num);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public final a a(String str, String str2) {
            b().put(str, str2);
            return this;
        }

        @DexIgnore
        public abstract a a(Map<String, String> map);

        @DexIgnore
        public abstract ku1 a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract Map<String, String> b();

        @DexIgnore
        public final a a(String str, long j) {
            b().put(str, String.valueOf(j));
            return this;
        }

        @DexIgnore
        public final a a(String str, int i) {
            b().put(str, String.valueOf(i));
            return this;
        }
    }

    @DexIgnore
    public static a i() {
        du1.b bVar = new du1.b();
        bVar.a(new HashMap());
        return bVar;
    }

    @DexIgnore
    public final String a(String str) {
        String str2 = a().get(str);
        return str2 == null ? "" : str2;
    }

    @DexIgnore
    public abstract Map<String, String> a();

    @DexIgnore
    public final int b(String str) {
        String str2 = a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Integer.valueOf(str2).intValue();
    }

    @DexIgnore
    public abstract Integer b();

    @DexIgnore
    public final long c(String str) {
        String str2 = a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Long.valueOf(str2).longValue();
    }

    @DexIgnore
    public abstract ju1 c();

    @DexIgnore
    public abstract long d();

    @DexIgnore
    public final Map<String, String> e() {
        return Collections.unmodifiableMap(a());
    }

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public a h() {
        du1.b bVar = new du1.b();
        bVar.a(f());
        bVar.a(b());
        bVar.a(c());
        bVar.a(d());
        bVar.b(g());
        bVar.a(new HashMap(a()));
        return bVar;
    }
}
