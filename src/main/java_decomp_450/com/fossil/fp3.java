package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp3<TResult, TContinuationResult> implements go3, io3, jo3<TContinuationResult>, hp3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ mo3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ lp3<TContinuationResult> c;

    @DexIgnore
    public fp3(Executor executor, mo3<TResult, TContinuationResult> mo3, lp3<TContinuationResult> lp3) {
        this.a = executor;
        this.b = mo3;
        this.c = lp3;
    }

    @DexIgnore
    @Override // com.fossil.hp3
    public final void a(no3<TResult> no3) {
        this.a.execute(new gp3(this, no3));
    }

    @DexIgnore
    @Override // com.fossil.go3
    public final void onCanceled() {
        this.c.f();
    }

    @DexIgnore
    @Override // com.fossil.io3
    public final void onFailure(Exception exc) {
        this.c.a(exc);
    }

    @DexIgnore
    @Override // com.fossil.jo3
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.a(tcontinuationresult);
    }
}
