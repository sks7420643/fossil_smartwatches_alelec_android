package com.fossil;

import android.util.Base64;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ po4 b;
    @DexIgnore
    public /* final */ qo4 c;
    @DexIgnore
    public /* final */ ch5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.h(), t2.h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {128}, m = "createChallenge")
    public static final class b extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((qn4) null, 0, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {158}, m = "editChallenge")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String) null, (String) null, (Integer) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {73}, m = "fetchChallenge")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {30}, m = "fetchChallengesWithStatus")
    public static final class e extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String[]) null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {238}, m = "fetchFocusedPlayers")
    public static final class f extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((List<String>) null, 0, 0, false, (fb7<? super ko4<List<hn4>>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {474}, m = "fetchHistoryChallenges")
    public static final class g extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(0, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {185}, m = "fetchPlayers")
    public static final class h extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String[]) null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {448}, m = "fetchRecommendedChallenges")
    public static final class i extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {384}, m = "fetchSyncData")
    public static final class j extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {104}, m = "fetchWatchDisplayChallenges")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {346}, m = "joinChallenge")
    public static final class l extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, 0, (String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {268}, m = "leaveChallenge")
    public static final class m extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {414}, m = "pushPendingStepData")
    public static final class n extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {421}, m = "pushStepData")
    public static final class o extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((in4) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {320}, m = "respondInvitation")
    public static final class p extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, false, 0, (String) null, (fb7<? super ko4<mn4>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ChallengeRepository", f = "ChallengeRepository.kt", l = {295}, m = "sendInvitation")
    public static final class q extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ro4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(ro4 ro4, fb7 fb7) {
            super(fb7);
            this.this$0 = ro4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, (String[]) null, this);
        }
    }

    @DexIgnore
    public ro4(po4 po4, qo4 qo4, ch5 ch5) {
        ee7.b(po4, "local");
        ee7.b(qo4, "remote");
        ee7.b(ch5, "shared");
        this.b = po4;
        this.c = qo4;
        this.d = ch5;
        String simpleName = ro4.class.getSimpleName();
        ee7.a((Object) simpleName, "ChallengeRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String[] r7, int r8, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.mn4>>> r9) {
        /*
            r6 = this;
            boolean r0 = r9 instanceof com.fossil.ro4.e
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ro4$e r0 = (com.fossil.ro4.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$e r0 = new com.fossil.ro4$e
            r0.<init>(r6, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003b
            if (r2 != r3) goto L_0x0033
            int r7 = r0.I$0
            java.lang.Object r7 = r0.L$1
            java.lang.String[] r7 = (java.lang.String[]) r7
            java.lang.Object r8 = r0.L$0
            com.fossil.ro4 r8 = (com.fossil.ro4) r8
            com.fossil.t87.a(r9)
            goto L_0x0050
        L_0x0033:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003b:
            com.fossil.t87.a(r9)
            com.fossil.qo4 r9 = r6.c
            r0.L$0 = r6
            r0.L$1 = r7
            r0.I$0 = r8
            r0.label = r3
            java.lang.Object r9 = r9.a(r7, r8, r0)
            if (r9 != r1) goto L_0x004f
            return r1
        L_0x004f:
            r8 = r6
        L_0x0050:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r0 = r9 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00ef
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r2 = r9.a()
            if (r2 == 0) goto L_0x00e8
            java.lang.Object r9 = r9.a()
            com.portfolio.platform.data.source.remote.ApiResponse r9 = (com.portfolio.platform.data.source.remote.ApiResponse) r9
            java.util.List r0 = r9.get_items()
            boolean r9 = r0.isEmpty()
            if (r9 == 0) goto L_0x0087
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r2 = r8.a
            java.lang.String r3 = "fetchChallengesWithStatus - Success with empty response"
            r9.e(r2, r3)
            com.fossil.po4 r8 = r8.b
            r8.a(r7)
            goto L_0x00e8
        L_0x0087:
            com.fossil.po4 r9 = r8.b
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            com.fossil.mn4 r9 = r9.a(r7, r2)
            if (r9 == 0) goto L_0x00c6
            java.util.Iterator r2 = r0.iterator()
        L_0x0098:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00bc
            java.lang.Object r3 = r2.next()
            r4 = r3
            com.fossil.mn4 r4 = (com.fossil.mn4) r4
            java.lang.String r5 = r9.f()
            java.lang.String r4 = r4.f()
            boolean r4 = com.fossil.ee7.a(r5, r4)
            java.lang.Boolean r4 = com.fossil.pb7.a(r4)
            boolean r4 = r4.booleanValue()
            if (r4 == 0) goto L_0x0098
            goto L_0x00bd
        L_0x00bc:
            r3 = r1
        L_0x00bd:
            com.fossil.mn4 r3 = (com.fossil.mn4) r3
            if (r3 != 0) goto L_0x00c6
            com.fossil.po4 r9 = r8.b
            r9.a(r7)
        L_0x00c6:
            com.fossil.po4 r7 = r8.b
            java.lang.Long[] r7 = r7.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r8 = r8.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchChallengesWithStatus - rowIds: "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r7 = r2.toString()
            r9.d(r8, r7)
        L_0x00e8:
            com.fossil.ko4 r7 = new com.fossil.ko4
            r8 = 2
            r7.<init>(r0, r1, r8, r1)
            goto L_0x0141
        L_0x00ef:
            boolean r7 = r9 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x0142
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = r8.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "fetchJoinedChallengesWithStatus - errorCode: "
            r0.append(r2)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r2 = r9.a()
            r0.append(r2)
            java.lang.String r2 = "message: "
            r0.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r9.c()
            if (r2 == 0) goto L_0x011e
            java.lang.String r2 = r2.getMessage()
            goto L_0x011f
        L_0x011e:
            r2 = r1
        L_0x011f:
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r7.e(r8, r0)
            com.fossil.ko4 r7 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r8 = new com.portfolio.platform.data.model.ServerError
            int r0 = r9.a()
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x013b
            java.lang.String r1 = r9.getMessage()
        L_0x013b:
            r8.<init>(r0, r1)
            r7.<init>(r8)
        L_0x0141:
            return r7
        L_0x0142:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String[], int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.ko4<com.fossil.no4>> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.ro4.j
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.ro4$j r0 = (com.fossil.ro4.j) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$j r0 = new com.fossil.ro4$j
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.ro4 r0 = (com.fossil.ro4) r0
            com.fossil.t87.a(r7)
            goto L_0x0053
        L_0x002d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0035:
            com.fossil.t87.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r2 = r6.a
            java.lang.String r4 = "fetchSyncData - start sync data"
            r7.d(r2, r4)
            com.fossil.qo4 r7 = r6.c
            r0.L$0 = r6
            r0.label = r3
            java.lang.Object r7 = r7.b(r0)
            if (r7 != r1) goto L_0x0052
            return r1
        L_0x0052:
            r0 = r6
        L_0x0053:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0097
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            com.fossil.no4 r7 = (com.fossil.no4) r7
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchSyncData - syncData: "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            if (r7 != 0) goto L_0x008f
            com.fossil.ko4 r7 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = "fetchSyncData success but got null response"
            r0.<init>(r1, r2)
            r7.<init>(r0)
            goto L_0x00f7
        L_0x008f:
            com.fossil.ko4 r0 = new com.fossil.ko4
            r1 = 2
            r0.<init>(r7, r2, r1, r2)
        L_0x0095:
            r7 = r0
            goto L_0x00f7
        L_0x0097:
            boolean r1 = r7 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00f8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r0.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchSyncData - errorCode: "
            r4.append(r5)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r5 = r7.a()
            r4.append(r5)
            java.lang.String r5 = " - message: "
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r5 = r7.c()
            if (r5 == 0) goto L_0x00c6
            java.lang.String r5 = r5.getMessage()
            goto L_0x00c7
        L_0x00c6:
            r5 = r2
        L_0x00c7:
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            int r1 = r7.a()
            r3 = 400(0x190, float:5.6E-43)
            if (r1 != r3) goto L_0x00de
            com.fossil.po4 r0 = r0.b
            r0.b()
        L_0x00de:
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            int r3 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00f0
            java.lang.String r2 = r7.getMessage()
        L_0x00f0:
            r1.<init>(r3, r2)
            r0.<init>(r1)
            goto L_0x0095
        L_0x00f7:
            return r7
        L_0x00f8:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.fb7<? super java.util.List<com.fossil.mn4>> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.ro4.k
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.ro4$k r0 = (com.fossil.ro4.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$k r0 = new com.fossil.ro4$k
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.ro4 r0 = (com.fossil.ro4) r0
            com.fossil.t87.a(r6)
            goto L_0x004f
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.fossil.qo4 r6 = r5.c
            java.lang.String r2 = "running"
            java.lang.String r4 = "completed"
            java.lang.String[] r2 = new java.lang.String[]{r2, r4}
            r4 = 5
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.a(r2, r4, r0)
            if (r6 != r1) goto L_0x004e
            return r1
        L_0x004e:
            r0 = r5
        L_0x004f:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            boolean r1 = r6 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0074
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r6 = r6.a()
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6
            if (r6 == 0) goto L_0x0064
            java.util.List r2 = r6.get_items()
        L_0x0064:
            if (r2 != 0) goto L_0x00ae
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r0 = r0.a
            java.lang.String r1 = "getWatchDisplayChallenges - Success with null response"
            r6.e(r0, r1)
            goto L_0x00ae
        L_0x0074:
            boolean r1 = r6 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getWatchDisplayChallenges - errorCode: "
            r3.append(r4)
            com.fossil.yi5 r6 = (com.fossil.yi5) r6
            int r4 = r6.a()
            r3.append(r4)
            java.lang.String r4 = "message: "
            r3.append(r4)
            com.portfolio.platform.data.model.ServerError r6 = r6.c()
            if (r6 == 0) goto L_0x00a3
            java.lang.String r6 = r6.getMessage()
            goto L_0x00a4
        L_0x00a3:
            r6 = r2
        L_0x00a4:
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            r1.e(r0, r6)
        L_0x00ae:
            return r2
        L_0x00af:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.c(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.ro4.n
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.ro4$n r0 = (com.fossil.ro4.n) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$n r0 = new com.fossil.ro4$n
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r1 = r0.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.ro4 r0 = (com.fossil.ro4) r0
            com.fossil.t87.a(r6)
            goto L_0x006a
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = r5.a
            java.lang.String r4 = "pushPendingStepData"
            r6.e(r2, r4)
            com.fossil.po4 r6 = r5.b
            java.util.List r6 = r6.d()
            boolean r2 = r6.isEmpty()
            r2 = r2 ^ r3
            if (r2 == 0) goto L_0x006a
            r2 = 0
            java.lang.Object r2 = r6.get(r2)
            com.fossil.in4 r2 = (com.fossil.in4) r2
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r6 = r5.a(r2, r0)
            if (r6 != r1) goto L_0x006a
            return r1
        L_0x006a:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.d(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final hn4 e(String str) {
        ee7.b(str, "id");
        return this.b.e(str);
    }

    @DexIgnore
    public final LiveData<yn4> f(String str) {
        ee7.b(str, "challengeId");
        return this.b.f(str);
    }

    @DexIgnore
    public final yn4 g(String str) {
        ee7.b(str, "id");
        return this.b.g(str);
    }

    @DexIgnore
    public final int d(String str) {
        ee7.b(str, "id");
        return this.b.d(str);
    }

    @DexIgnore
    public final LiveData<List<yn4>> d() {
        return this.b.f();
    }

    @DexIgnore
    public final int c(String str) {
        ee7.b(str, "id");
        return this.b.c(str);
    }

    @DexIgnore
    public final List<jn4> c() {
        return this.b.e();
    }

    @DexIgnore
    public final LiveData<mn4> b(String[] strArr, Date date) {
        ee7.b(strArr, "array");
        ee7.b(date, "date");
        return this.b.b(strArr, date);
    }

    @DexIgnore
    public final LiveData<mn4> b(String str) {
        ee7.b(str, "id");
        return this.b.b(str);
    }

    @DexIgnore
    public final void b() {
        this.b.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r6, java.lang.String r7, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof com.fossil.ro4.d
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.ro4$d r0 = (com.fossil.ro4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$d r0 = new com.fossil.ro4$d
            r0.<init>(r5, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r6 = r0.L$2
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r7 = r0.L$0
            com.fossil.ro4 r7 = (com.fossil.ro4) r7
            com.fossil.t87.a(r8)
            goto L_0x0052
        L_0x0035:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003d:
            com.fossil.t87.a(r8)
            com.fossil.qo4 r8 = r5.c
            r0.L$0 = r5
            r0.L$1 = r6
            r0.L$2 = r7
            r0.label = r3
            java.lang.Object r8 = r8.a(r6, r0)
            if (r8 != r1) goto L_0x0051
            return r1
        L_0x0051:
            r7 = r5
        L_0x0052:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r0 = r8 instanceof com.fossil.bj5
            r1 = 0
            if (r0 == 0) goto L_0x00cc
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r8 = r8.a()
            com.fossil.mn4 r8 = (com.fossil.mn4) r8
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r7.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchChallenge - response: "
            r3.append(r4)
            r3.append(r8)
            java.lang.String r3 = r3.toString()
            r0.e(r2, r3)
            if (r8 != 0) goto L_0x008f
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            r8 = 404(0x194, float:5.66E-43)
            java.lang.String r0 = "success request  with no data"
            r7.<init>(r8, r0)
            r6.<init>(r7)
            goto L_0x011e
        L_0x008f:
            com.fossil.po4 r0 = r7.b
            com.fossil.mn4 r6 = r0.a(r6)
            if (r6 == 0) goto L_0x00c5
            com.fossil.po4 r6 = r7.b
            long r2 = r6.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r7.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = "fetchChallenge - rowId: "
            r0.append(r4)
            r0.append(r2)
            java.lang.String r2 = " - challengeId: "
            r0.append(r2)
            java.lang.String r2 = r8.f()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r6.e(r7, r0)
        L_0x00c5:
            com.fossil.ko4 r6 = new com.fossil.ko4
            r7 = 2
            r6.<init>(r8, r1, r7, r1)
            goto L_0x011e
        L_0x00cc:
            boolean r6 = r8 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x011f
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r7.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "fetchChallenge - errorCode: "
            r0.append(r2)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r2 = r8.a()
            r0.append(r2)
            java.lang.String r2 = "message: "
            r0.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r8.c()
            if (r2 == 0) goto L_0x00fb
            java.lang.String r2 = r2.getMessage()
            goto L_0x00fc
        L_0x00fb:
            r2 = r1
        L_0x00fc:
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r6.e(r7, r0)
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            int r0 = r8.a()
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x0118
            java.lang.String r1 = r8.getMessage()
        L_0x0118:
            r7.<init>(r0, r1)
            r6.<init>(r7)
        L_0x011e:
            return r6
        L_0x011f:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ro4 ro4, String str, String str2, fb7 fb7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str2 = "joined_challenge";
        }
        return ro4.a(str, str2, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.qn4 r6, int r7, java.lang.String r8, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r9) {
        /*
            r5 = this;
            boolean r0 = r9 instanceof com.fossil.ro4.b
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ro4$b r0 = (com.fossil.ro4.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$b r0 = new com.fossil.ro4$b
            r0.<init>(r5, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            java.lang.Object r6 = r0.L$2
            java.lang.String r6 = (java.lang.String) r6
            int r6 = r0.I$0
            java.lang.Object r6 = r0.L$1
            com.fossil.qn4 r6 = (com.fossil.qn4) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.ro4 r6 = (com.fossil.ro4) r6
            com.fossil.t87.a(r9)
            goto L_0x0056
        L_0x0037:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003f:
            com.fossil.t87.a(r9)
            com.fossil.qo4 r9 = r5.c
            r0.L$0 = r5
            r0.L$1 = r6
            r0.I$0 = r7
            r0.L$2 = r8
            r0.label = r3
            java.lang.Object r9 = r9.a(r6, r7, r8, r0)
            if (r9 != r1) goto L_0x0055
            return r1
        L_0x0055:
            r6 = r5
        L_0x0056:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r7 = r9 instanceof com.fossil.bj5
            r8 = 0
            if (r7 == 0) goto L_0x00b5
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r7 = r9.a()
            com.fossil.mn4 r7 = (com.fossil.mn4) r7
            if (r7 != 0) goto L_0x0075
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r6 = r6.a
            java.lang.String r0 = "createChallenge - Success with null response"
            r9.e(r6, r0)
            goto L_0x00ae
        L_0x0075:
            com.fossil.po4 r9 = r6.b
            java.lang.String r0 = "running"
            java.lang.String r1 = "waiting"
            java.lang.String[] r0 = new java.lang.String[]{r0, r1}
            r9.a(r0)
            com.fossil.i97 r9 = com.fossil.i97.a
            com.fossil.po4 r0 = r6.b
            long r0 = r0.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r6 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "createChallenge - deletedRowId: "
            r3.append(r4)
            r3.append(r9)
            java.lang.String r9 = " - insertRowId: "
            r3.append(r9)
            r3.append(r0)
            java.lang.String r9 = r3.toString()
            r2.d(r6, r9)
        L_0x00ae:
            com.fossil.ko4 r6 = new com.fossil.ko4
            r9 = 2
            r6.<init>(r7, r8, r9, r8)
            goto L_0x0118
        L_0x00b5:
            boolean r7 = r9 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x0119
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r6 = r6.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "createChallenge - errorCode: "
            r0.append(r1)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r1 = r9.a()
            r0.append(r1)
            java.lang.String r1 = "message: "
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r9.c()
            if (r1 == 0) goto L_0x00e4
            java.lang.String r1 = r1.getMessage()
            goto L_0x00e5
        L_0x00e4:
            r1 = r8
        L_0x00e5:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r7.e(r6, r0)
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r0 = r9.c()
            if (r0 == 0) goto L_0x0104
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x0104
            int r0 = r0.intValue()
            goto L_0x0108
        L_0x0104:
            int r0 = r9.a()
        L_0x0108:
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x0112
            java.lang.String r8 = r9.getMessage()
        L_0x0112:
            r7.<init>(r0, r8)
            r6.<init>(r7)
        L_0x0118:
            return r6
        L_0x0119:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(com.fossil.qn4, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r8, java.lang.String r9, java.lang.String r10, java.lang.Integer r11, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r12) {
        /*
            r7 = this;
            boolean r0 = r12 instanceof com.fossil.ro4.c
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.ro4$c r0 = (com.fossil.ro4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$c r0 = new com.fossil.ro4$c
            r0.<init>(r7, r12)
        L_0x0018:
            r6 = r0
            java.lang.Object r12 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            r2 = 1
            if (r1 == 0) goto L_0x0046
            if (r1 != r2) goto L_0x003e
            java.lang.Object r8 = r6.L$4
            java.lang.Integer r8 = (java.lang.Integer) r8
            java.lang.Object r8 = r6.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r6.L$2
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r6.L$1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r6.L$0
            com.fossil.ro4 r8 = (com.fossil.ro4) r8
            com.fossil.t87.a(r12)
            goto L_0x0063
        L_0x003e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0046:
            com.fossil.t87.a(r12)
            com.fossil.qo4 r1 = r7.c
            r6.L$0 = r7
            r6.L$1 = r8
            r6.L$2 = r9
            r6.L$3 = r10
            r6.L$4 = r11
            r6.label = r2
            r2 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            java.lang.Object r12 = r1.a(r2, r3, r4, r5, r6)
            if (r12 != r0) goto L_0x0062
            return r0
        L_0x0062:
            r8 = r7
        L_0x0063:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r9 = r12 instanceof com.fossil.bj5
            r10 = 0
            if (r9 == 0) goto L_0x00ba
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r9 = r12.a()
            com.fossil.mn4 r9 = (com.fossil.mn4) r9
            if (r9 != 0) goto L_0x0091
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r8 = r8.a
            java.lang.String r10 = "editChallenge - Success with null response"
            r9.e(r8, r10)
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r9 = new com.portfolio.platform.data.model.ServerError
            r10 = 600(0x258, float:8.41E-43)
            java.lang.String r11 = "request success, none response"
            r9.<init>(r10, r11)
            r8.<init>(r9)
            goto L_0x011d
        L_0x0091:
            com.fossil.po4 r11 = r8.b
            long r11 = r11.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r8 = r8.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "editChallenge - rowId: "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            r0.d(r8, r11)
            com.fossil.ko4 r8 = new com.fossil.ko4
            r11 = 2
            r8.<init>(r9, r10, r11, r10)
            goto L_0x011d
        L_0x00ba:
            boolean r9 = r12 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x011e
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r8 = r8.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r0 = "editChallenge - errorCode: "
            r11.append(r0)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r0 = r12.a()
            r11.append(r0)
            java.lang.String r0 = "message: "
            r11.append(r0)
            com.portfolio.platform.data.model.ServerError r0 = r12.c()
            if (r0 == 0) goto L_0x00e9
            java.lang.String r0 = r0.getMessage()
            goto L_0x00ea
        L_0x00e9:
            r0 = r10
        L_0x00ea:
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            r9.e(r8, r11)
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r9 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r11 = r12.c()
            if (r11 == 0) goto L_0x0109
            java.lang.Integer r11 = r11.getCode()
            if (r11 == 0) goto L_0x0109
            int r11 = r11.intValue()
            goto L_0x010d
        L_0x0109:
            int r11 = r12.a()
        L_0x010d:
            com.portfolio.platform.data.model.ServerError r12 = r12.c()
            if (r12 == 0) goto L_0x0117
            java.lang.String r10 = r12.getMessage()
        L_0x0117:
            r9.<init>(r11, r10)
            r8.<init>(r9)
        L_0x011d:
            return r8
        L_0x011e:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ro4 ro4, String str, String str2, String str3, Integer num, fb7 fb7, int i2, Object obj) {
        return ro4.a(str, (i2 & 2) != 0 ? null : str2, (i2 & 4) != 0 ? null : str3, (i2 & 8) != 0 ? null : num, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r5, java.lang.String[] r6, int r7, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.jn4>>> r8) {
        /*
            r4 = this;
            boolean r0 = r8 instanceof com.fossil.ro4.h
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.ro4$h r0 = (com.fossil.ro4.h) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$h r0 = new com.fossil.ro4$h
            r0.<init>(r4, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            int r5 = r0.I$0
            java.lang.Object r5 = r0.L$2
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.ro4 r5 = (com.fossil.ro4) r5
            com.fossil.t87.a(r8)
            goto L_0x0056
        L_0x0037:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003f:
            com.fossil.t87.a(r8)
            com.fossil.qo4 r8 = r4.c
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.I$0 = r7
            r0.label = r3
            java.lang.Object r8 = r8.a(r5, r6, r7, r0)
            if (r8 != r1) goto L_0x0055
            return r1
        L_0x0055:
            r5 = r4
        L_0x0056:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r6 = r8 instanceof com.fossil.bj5
            r7 = 0
            if (r6 == 0) goto L_0x00a5
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r6 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6
            if (r6 == 0) goto L_0x006c
            java.util.List r6 = r6.get_items()
            goto L_0x006d
        L_0x006c:
            r6 = r7
        L_0x006d:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "fetchPlayers - players: "
            r0.append(r1)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            r8.e(r5, r0)
            if (r6 != 0) goto L_0x009a
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r7 = 404(0x194, float:5.66E-43)
            java.lang.String r8 = "success with no data"
            r6.<init>(r7, r8)
            r5.<init>(r6)
            goto L_0x00f7
        L_0x009a:
            com.fossil.ko4 r5 = new com.fossil.ko4
            java.util.List r6 = com.fossil.ea7.n(r6)
            r8 = 2
            r5.<init>(r6, r7, r8, r7)
            goto L_0x00f7
        L_0x00a5:
            boolean r6 = r8 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x00f8
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "fetchPlayers - errorCode: "
            r0.append(r1)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r1 = r8.a()
            r0.append(r1)
            java.lang.String r1 = "message: "
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r8.c()
            if (r1 == 0) goto L_0x00d4
            java.lang.String r1 = r1.getMessage()
            goto L_0x00d5
        L_0x00d4:
            r1 = r7
        L_0x00d5:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r6.e(r5, r0)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            int r0 = r8.a()
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00f1
            java.lang.String r7 = r8.getMessage()
        L_0x00f1:
            r6.<init>(r0, r7)
            r5.<init>(r6)
        L_0x00f7:
            return r5
        L_0x00f8:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, java.lang.String[], int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ro4 ro4, String str, String[] strArr, int i2, fb7 fb7, int i3, Object obj) {
        if ((i3 & 4) != 0) {
            i2 = 100;
        }
        return ro4.a(str, strArr, i2, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.util.List<java.lang.String> r5, int r6, int r7, boolean r8, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.hn4>>> r9) {
        /*
            r4 = this;
            boolean r0 = r9 instanceof com.fossil.ro4.f
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ro4$f r0 = (com.fossil.ro4.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$f r0 = new com.fossil.ro4$f
            r0.<init>(r4, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            boolean r8 = r0.Z$0
            int r5 = r0.I$1
            int r5 = r0.I$0
            java.lang.Object r5 = r0.L$1
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.ro4 r5 = (com.fossil.ro4) r5
            com.fossil.t87.a(r9)
            goto L_0x0058
        L_0x0037:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003f:
            com.fossil.t87.a(r9)
            com.fossil.qo4 r9 = r4.c
            r0.L$0 = r4
            r0.L$1 = r5
            r0.I$0 = r6
            r0.I$1 = r7
            r0.Z$0 = r8
            r0.label = r3
            java.lang.Object r9 = r9.a(r5, r6, r7, r0)
            if (r9 != r1) goto L_0x0057
            return r1
        L_0x0057:
            r5 = r4
        L_0x0058:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r6 = r9 instanceof com.fossil.bj5
            r7 = 0
            if (r6 == 0) goto L_0x00cb
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r6 = r9.a()
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6
            if (r6 == 0) goto L_0x006e
            java.util.List r6 = r6.get_items()
            goto L_0x006f
        L_0x006e:
            r6 = r7
        L_0x006f:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = r5.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "fetchFocusedPlayers - success - data: "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r1 = r1.toString()
            r9.e(r0, r1)
            if (r6 != 0) goto L_0x009c
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r7 = 600(0x258, float:8.41E-43)
            java.lang.String r8 = "code: 200 - null result"
            r6.<init>(r7, r8)
            r5.<init>(r6)
            goto L_0x0109
        L_0x009c:
            if (r8 == 0) goto L_0x00c0
            com.fossil.po4 r8 = r5.b
            java.lang.Long[] r8 = r8.b(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "fetchFocusedPlayers - rowIds: "
            r0.append(r1)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            r9.e(r5, r8)
        L_0x00c0:
            com.fossil.ko4 r5 = new com.fossil.ko4
            java.util.List r6 = com.fossil.ea7.n(r6)
            r8 = 2
            r5.<init>(r6, r7, r8, r7)
            goto L_0x0109
        L_0x00cb:
            boolean r6 = r9 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x010a
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r0 = "fetchFocusedPlayers - errorCode: "
            r8.append(r0)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            int r0 = r9.a()
            r8.append(r0)
            java.lang.String r8 = r8.toString()
            r6.e(r5, r8)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            int r8 = r9.a()
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x0103
            java.lang.String r7 = r9.getMessage()
        L_0x0103:
            r6.<init>(r8, r7)
            r5.<init>(r6)
        L_0x0109:
            return r5
        L_0x010a:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.util.List, int, int, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ro4 ro4, List list, int i2, int i3, boolean z, fb7 fb7, int i4, Object obj) {
        return ro4.a(list, i2, i3, (i4 & 8) != 0 ? true : z, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r27, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r28) {
        /*
            r26 = this;
            r0 = r26
            r1 = r27
            r2 = r28
            boolean r3 = r2 instanceof com.fossil.ro4.m
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.ro4$m r3 = (com.fossil.ro4.m) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.ro4$m r3 = new com.fossil.ro4$m
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 1
            if (r5 == 0) goto L_0x003f
            if (r5 != r6) goto L_0x0037
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r3 = r3.L$0
            com.fossil.ro4 r3 = (com.fossil.ro4) r3
            com.fossil.t87.a(r2)
            goto L_0x0052
        L_0x0037:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x003f:
            com.fossil.t87.a(r2)
            com.fossil.qo4 r2 = r0.c
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r6
            java.lang.Object r2 = r2.b(r1, r3)
            if (r2 != r4) goto L_0x0051
            return r4
        L_0x0051:
            r3 = r0
        L_0x0052:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r4 = r2 instanceof com.fossil.bj5
            r5 = 0
            if (r4 == 0) goto L_0x00a6
            com.fossil.po4 r4 = r3.b
            r4.d(r1)
            com.fossil.bj5 r2 = (com.fossil.bj5) r2
            java.lang.Object r1 = r2.a()
            com.fossil.mn4 r1 = (com.fossil.mn4) r1
            r2 = 2
            if (r1 != 0) goto L_0x00a0
            com.fossil.mn4 r1 = new com.fossil.mn4
            r6 = r1
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 65536(0x10000, float:9.18355E-41)
            r25 = 0
            java.lang.String r7 = ""
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r3 = r3.a
            java.lang.String r6 = "leaveChallenge - Success with null response"
            r4.e(r3, r6)
            com.fossil.ko4 r3 = new com.fossil.ko4
            r3.<init>(r1, r5, r2, r5)
            goto L_0x00f8
        L_0x00a0:
            com.fossil.ko4 r3 = new com.fossil.ko4
            r3.<init>(r1, r5, r2, r5)
            goto L_0x00f8
        L_0x00a6:
            boolean r1 = r2 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00f9
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r3.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "leaveChallenge - errorCode: "
            r4.append(r6)
            com.fossil.yi5 r2 = (com.fossil.yi5) r2
            int r6 = r2.a()
            r4.append(r6)
            java.lang.String r6 = " - message: "
            r4.append(r6)
            com.portfolio.platform.data.model.ServerError r6 = r2.c()
            if (r6 == 0) goto L_0x00d5
            java.lang.String r6 = r6.getMessage()
            goto L_0x00d6
        L_0x00d5:
            r6 = r5
        L_0x00d6:
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            com.fossil.ko4 r3 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            int r4 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            if (r2 == 0) goto L_0x00f2
            java.lang.String r5 = r2.getMessage()
        L_0x00f2:
            r1.<init>(r4, r5)
            r3.<init>(r1)
        L_0x00f8:
            return r3
        L_0x00f9:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r5, java.lang.String[] r6, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r7) {
        /*
            r4 = this;
            boolean r0 = r7 instanceof com.fossil.ro4.q
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.ro4$q r0 = (com.fossil.ro4.q) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$q r0 = new com.fossil.ro4$q
            r0.<init>(r4, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r5 = r0.L$2
            java.lang.String[] r5 = (java.lang.String[]) r5
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.ro4 r5 = (com.fossil.ro4) r5
            com.fossil.t87.a(r7)
            goto L_0x0052
        L_0x0035:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003d:
            com.fossil.t87.a(r7)
            com.fossil.qo4 r7 = r4.c
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.label = r3
            java.lang.Object r7 = r7.a(r5, r6, r0)
            if (r7 != r1) goto L_0x0051
            return r1
        L_0x0051:
            r5 = r4
        L_0x0052:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r6 = r7 instanceof com.fossil.bj5
            r0 = 0
            if (r6 == 0) goto L_0x0095
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r6 = r7.a()
            com.fossil.mn4 r6 = (com.fossil.mn4) r6
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "sendInvitation - "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r1 = r1.toString()
            r7.d(r5, r1)
            if (r6 != 0) goto L_0x008e
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r7 = 600(0x258, float:8.41E-43)
            java.lang.String r0 = "sendInvitation has been successfully - return null response"
            r6.<init>(r7, r0)
            r5.<init>(r6)
            goto L_0x00f8
        L_0x008e:
            com.fossil.ko4 r5 = new com.fossil.ko4
            r7 = 2
            r5.<init>(r6, r0, r7, r0)
            goto L_0x00f8
        L_0x0095:
            boolean r6 = r7 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x00f9
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "sendInvitation - errorCode: "
            r1.append(r2)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r2 = r7.a()
            r1.append(r2)
            java.lang.String r2 = " - message: "
            r1.append(r2)
            com.portfolio.platform.data.model.ServerError r2 = r7.c()
            if (r2 == 0) goto L_0x00c4
            java.lang.String r2 = r2.getMessage()
            goto L_0x00c5
        L_0x00c4:
            r2 = r0
        L_0x00c5:
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r6.e(r5, r1)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r7.c()
            if (r1 == 0) goto L_0x00e4
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x00e4
            int r1 = r1.intValue()
            goto L_0x00e8
        L_0x00e4:
            int r1 = r7.a()
        L_0x00e8:
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00f2
            java.lang.String r0 = r7.getMessage()
        L_0x00f2:
            r6.<init>(r1, r0)
            r5.<init>(r6)
        L_0x00f8:
            return r5
        L_0x00f9:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, java.lang.String[], com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r8, boolean r9, int r10, java.lang.String r11, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r12) {
        /*
            r7 = this;
            boolean r0 = r12 instanceof com.fossil.ro4.p
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.ro4$p r0 = (com.fossil.ro4.p) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$p r0 = new com.fossil.ro4$p
            r0.<init>(r7, r12)
        L_0x0018:
            r6 = r0
            java.lang.Object r12 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            r2 = 1
            if (r1 == 0) goto L_0x0042
            if (r1 != r2) goto L_0x003a
            java.lang.Object r8 = r6.L$2
            java.lang.String r8 = (java.lang.String) r8
            int r8 = r6.I$0
            boolean r8 = r6.Z$0
            java.lang.Object r8 = r6.L$1
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r6.L$0
            com.fossil.ro4 r8 = (com.fossil.ro4) r8
            com.fossil.t87.a(r12)
            goto L_0x005f
        L_0x003a:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0042:
            com.fossil.t87.a(r12)
            com.fossil.qo4 r1 = r7.c
            r6.L$0 = r7
            r6.L$1 = r8
            r6.Z$0 = r9
            r6.I$0 = r10
            r6.L$2 = r11
            r6.label = r2
            r2 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            java.lang.Object r12 = r1.a(r2, r3, r4, r5, r6)
            if (r12 != r0) goto L_0x005e
            return r0
        L_0x005e:
            r8 = r7
        L_0x005f:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r9 = r12 instanceof com.fossil.bj5
            r10 = 0
            if (r9 == 0) goto L_0x00a8
            com.fossil.bj5 r12 = (com.fossil.bj5) r12
            java.lang.Object r9 = r12.a()
            com.fossil.mn4 r9 = (com.fossil.mn4) r9
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = r8.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "respondInvitation - "
            r0.append(r1)
            r0.append(r9)
            java.lang.String r0 = r0.toString()
            r11.e(r12, r0)
            if (r9 == 0) goto L_0x0099
            com.fossil.po4 r8 = r8.b
            r8.a(r9)
            com.fossil.ko4 r8 = new com.fossil.ko4
            r11 = 2
            r8.<init>(r9, r10, r11, r10)
            goto L_0x010b
        L_0x0099:
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r9 = new com.portfolio.platform.data.model.ServerError
            r10 = 600(0x258, float:8.41E-43)
            java.lang.String r11 = "respondInvitation successfully - challenge null"
            r9.<init>(r10, r11)
            r8.<init>(r9)
            goto L_0x010b
        L_0x00a8:
            boolean r9 = r12 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x010c
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r8 = r8.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r0 = "respondInvitation - errorCode: "
            r11.append(r0)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r0 = r12.a()
            r11.append(r0)
            java.lang.String r0 = " - message: "
            r11.append(r0)
            com.portfolio.platform.data.model.ServerError r0 = r12.c()
            if (r0 == 0) goto L_0x00d7
            java.lang.String r0 = r0.getMessage()
            goto L_0x00d8
        L_0x00d7:
            r0 = r10
        L_0x00d8:
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            r9.e(r8, r11)
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r9 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r11 = r12.c()
            if (r11 == 0) goto L_0x00f7
            java.lang.Integer r11 = r11.getCode()
            if (r11 == 0) goto L_0x00f7
            int r11 = r11.intValue()
            goto L_0x00fb
        L_0x00f7:
            int r11 = r12.a()
        L_0x00fb:
            com.portfolio.platform.data.model.ServerError r12 = r12.c()
            if (r12 == 0) goto L_0x0105
            java.lang.String r10 = r12.getMessage()
        L_0x0105:
            r9.<init>(r11, r10)
            r8.<init>(r9)
        L_0x010b:
            return r8
        L_0x010c:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, boolean, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r5, int r6, java.lang.String r7, com.fossil.fb7<? super com.fossil.ko4<com.fossil.mn4>> r8) {
        /*
            r4 = this;
            boolean r0 = r8 instanceof com.fossil.ro4.l
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.ro4$l r0 = (com.fossil.ro4.l) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$l r0 = new com.fossil.ro4$l
            r0.<init>(r4, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003f
            if (r2 != r3) goto L_0x0037
            java.lang.Object r5 = r0.L$2
            java.lang.String r5 = (java.lang.String) r5
            int r5 = r0.I$0
            java.lang.Object r5 = r0.L$1
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.ro4 r5 = (com.fossil.ro4) r5
            com.fossil.t87.a(r8)
            goto L_0x0056
        L_0x0037:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x003f:
            com.fossil.t87.a(r8)
            com.fossil.qo4 r8 = r4.c
            r0.L$0 = r4
            r0.L$1 = r5
            r0.I$0 = r6
            r0.L$2 = r7
            r0.label = r3
            java.lang.Object r8 = r8.a(r5, r6, r7, r0)
            if (r8 != r1) goto L_0x0055
            return r1
        L_0x0055:
            r5 = r4
        L_0x0056:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r6 = r8 instanceof com.fossil.bj5
            r7 = 0
            if (r6 == 0) goto L_0x009f
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r6 = r8.a()
            com.fossil.mn4 r6 = (com.fossil.mn4) r6
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r0 = r5.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "joinChallenge - "
            r1.append(r2)
            r1.append(r6)
            java.lang.String r1 = r1.toString()
            r8.e(r0, r1)
            if (r6 == 0) goto L_0x0090
            com.fossil.po4 r5 = r5.b
            r5.a(r6)
            com.fossil.ko4 r5 = new com.fossil.ko4
            r8 = 2
            r5.<init>(r6, r7, r8, r7)
            goto L_0x0102
        L_0x0090:
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            r7 = 600(0x258, float:8.41E-43)
            java.lang.String r8 = "joinChallenge successfully - challenge null"
            r6.<init>(r7, r8)
            r5.<init>(r6)
            goto L_0x0102
        L_0x009f:
            boolean r6 = r8 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x0103
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "joinChallenge - errorCode: "
            r0.append(r1)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r1 = r8.a()
            r0.append(r1)
            java.lang.String r1 = " - message: "
            r0.append(r1)
            com.portfolio.platform.data.model.ServerError r1 = r8.c()
            if (r1 == 0) goto L_0x00ce
            java.lang.String r1 = r1.getMessage()
            goto L_0x00cf
        L_0x00ce:
            r1 = r7
        L_0x00cf:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r6.e(r5, r0)
            com.fossil.ko4 r5 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r6 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r0 = r8.c()
            if (r0 == 0) goto L_0x00ee
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00ee
            int r0 = r0.intValue()
            goto L_0x00f2
        L_0x00ee:
            int r0 = r8.a()
        L_0x00f2:
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00fc
            java.lang.String r7 = r8.getMessage()
        L_0x00fc:
            r6.<init>(r0, r7)
            r5.<init>(r6)
        L_0x0102:
            return r5
        L_0x0103:
            com.fossil.p87 r5 = new com.fossil.p87
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(java.lang.String, int, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final Object a(EncryptedData encryptedData, fb7<? super ko4<lo4>> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "processStepData - encryptedData=" + encryptedData);
        int ordinal = encryptedData.getEncryptMethod().ordinal();
        String encodeToString = Base64.encodeToString(encryptedData.getEncryptedData(), 2);
        ee7.a((Object) encodeToString, "Base64.encodeToString(en\u2026ptedData, Base64.NO_WRAP)");
        return a(new in4(0, ordinal, encodeToString, encryptedData.getKeyType().ordinal(), String.valueOf((int) encryptedData.getSequence())), fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.in4 r8, com.fossil.fb7<? super com.fossil.ko4<com.fossil.lo4>> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.fossil.ro4.o
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ro4$o r0 = (com.fossil.ro4.o) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$o r0 = new com.fossil.ro4$o
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r8 = r0.L$1
            com.fossil.in4 r8 = (com.fossil.in4) r8
            java.lang.Object r0 = r0.L$0
            com.fossil.ro4 r0 = (com.fossil.ro4) r0
            com.fossil.t87.a(r9)
            goto L_0x0068
        L_0x0031:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0039:
            com.fossil.t87.a(r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r2 = r7.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "pushStepData - fitnessData: "
            r4.append(r5)
            r4.append(r8)
            java.lang.String r4 = r4.toString()
            r9.d(r2, r4)
            com.fossil.qo4 r9 = r7.c
            r0.L$0 = r7
            r0.L$1 = r8
            r0.label = r3
            java.lang.Object r9 = r9.a(r8, r0)
            if (r9 != r1) goto L_0x0067
            return r1
        L_0x0067:
            r0 = r7
        L_0x0068:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r1 = r9 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00b2
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r1 = r0.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "pushStepData - success - stepResult: "
            r4.append(r5)
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r5 = r9.a()
            com.fossil.lo4 r5 = (com.fossil.lo4) r5
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r8.e(r1, r4)
            java.lang.Object r8 = r9.a()
            com.fossil.lo4 r8 = (com.fossil.lo4) r8
            if (r8 == 0) goto L_0x009c
            goto L_0x00a5
        L_0x009c:
            com.fossil.lo4 r8 = new com.fossil.lo4
            java.lang.Integer r9 = com.fossil.pb7.a(r3)
            r8.<init>(r9)
        L_0x00a5:
            com.fossil.po4 r9 = r0.b
            r9.b()
            com.fossil.ko4 r9 = new com.fossil.ko4
            r0 = 2
            r9.<init>(r8, r2, r0, r2)
            goto L_0x012a
        L_0x00b2:
            boolean r1 = r9 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x012b
            r1 = 404(0x194, float:5.66E-43)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            com.portfolio.platform.data.model.ServerError r3 = r9.c()
            if (r3 == 0) goto L_0x00c5
            java.lang.Integer r3 = r3.getCode()
            goto L_0x00c6
        L_0x00c5:
            r3 = r2
        L_0x00c6:
            if (r3 != 0) goto L_0x00c9
            goto L_0x00cf
        L_0x00c9:
            int r3 = r3.intValue()
            if (r1 == r3) goto L_0x00f1
        L_0x00cf:
            com.fossil.po4 r1 = r0.b
            long r3 = r1.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r1 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "pushStepData - insert - rowId: "
            r5.append(r6)
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r8.e(r1, r3)
        L_0x00f1:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "pushStepData failed, errorCode="
            r1.append(r3)
            int r3 = r9.a()
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r8.d(r0, r1)
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r1 = r9.a()
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x0123
            java.lang.String r2 = r9.getMessage()
        L_0x0123:
            r0.<init>(r1, r2)
            r8.<init>(r0)
            r9 = r8
        L_0x012a:
            return r9
        L_0x012b:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(com.fossil.in4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.jo4>>> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.ro4.i
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.ro4$i r0 = (com.fossil.ro4.i) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$i r0 = new com.fossil.ro4$i
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.ro4 r0 = (com.fossil.ro4) r0
            com.fossil.t87.a(r7)
            goto L_0x0046
        L_0x002d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0035:
            com.fossil.t87.a(r7)
            com.fossil.qo4 r7 = r6.c
            r0.L$0 = r6
            r0.label = r3
            java.lang.Object r7 = r7.a(r0)
            if (r7 != r1) goto L_0x0045
            return r1
        L_0x0045:
            r0 = r6
        L_0x0046:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0097
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            if (r7 == 0) goto L_0x005c
            java.util.List r7 = r7.get_items()
            goto L_0x005d
        L_0x005c:
            r7 = r2
        L_0x005d:
            if (r7 != 0) goto L_0x0065
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            goto L_0x0090
        L_0x0065:
            java.util.Iterator r1 = r7.iterator()
        L_0x0069:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x0090
            java.lang.Object r3 = r1.next()
            com.fossil.jo4 r3 = (com.fossil.jo4) r3
            com.fossil.ch5 r4 = r0.d
            com.fossil.mn4 r5 = r3.b()
            java.lang.String r5 = r5.f()
            java.lang.Boolean r4 = r4.m(r5)
            java.lang.String r5 = "shared.isNewChallenge(it.challenge.id)"
            com.fossil.ee7.a(r4, r5)
            boolean r4 = r4.booleanValue()
            r3.a(r4)
            goto L_0x0069
        L_0x0090:
            com.fossil.ko4 r0 = new com.fossil.ko4
            r1 = 2
            r0.<init>(r7, r2, r1, r2)
            goto L_0x00b5
        L_0x0097:
            boolean r0 = r7 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00b6
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r3 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00af
            java.lang.String r2 = r7.getMessage()
        L_0x00af:
            r1.<init>(r3, r2)
            r0.<init>(r1)
        L_0x00b5:
            return r0
        L_0x00b6:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(int r6, int r7, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.yn4>>> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof com.fossil.ro4.g
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.ro4$g r0 = (com.fossil.ro4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ro4$g r0 = new com.fossil.ro4$g
            r0.<init>(r5, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            int r6 = r0.I$1
            int r6 = r0.I$0
            java.lang.Object r6 = r0.L$0
            com.fossil.ro4 r6 = (com.fossil.ro4) r6
            com.fossil.t87.a(r8)
            goto L_0x004e
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r8)
            com.fossil.qo4 r8 = r5.c
            r0.L$0 = r5
            r0.I$0 = r6
            r0.I$1 = r7
            r0.label = r3
            java.lang.Object r8 = r8.a(r6, r7, r0)
            if (r8 != r1) goto L_0x004d
            return r1
        L_0x004d:
            r6 = r5
        L_0x004e:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r7 = r8 instanceof com.fossil.bj5
            r0 = 0
            if (r7 == 0) goto L_0x00fa
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r7 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            if (r7 == 0) goto L_0x0064
            java.util.List r7 = r7.get_items()
            goto L_0x0065
        L_0x0064:
            r7 = r0
        L_0x0065:
            java.lang.Object r8 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r8 = (com.portfolio.platform.data.source.remote.ApiResponse) r8
            if (r8 == 0) goto L_0x0072
            com.portfolio.platform.data.model.Range r8 = r8.get_range()
            goto L_0x0073
        L_0x0072:
            r8 = r0
        L_0x0073:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchHistoryChallenges - challenges: "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r4 = " - hasNext: "
            r3.append(r4)
            if (r8 == 0) goto L_0x0098
            boolean r4 = r8.isHasNext()
            java.lang.Boolean r4 = com.fossil.pb7.a(r4)
            goto L_0x0099
        L_0x0098:
            r4 = r0
        L_0x0099:
            r3.append(r4)
            java.lang.String r4 = " - "
            r3.append(r4)
            if (r8 == 0) goto L_0x00ab
            int r0 = r8.getOffset()
            java.lang.Integer r0 = com.fossil.pb7.a(r0)
        L_0x00ab:
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            if (r7 != 0) goto L_0x00c6
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            r8 = 600(0x258, float:8.41E-43)
            java.lang.String r0 = "fetchHistoryChallenges is successfully but the result is null"
            r7.<init>(r8, r0)
            r6.<init>(r7)
            goto L_0x0138
        L_0x00c6:
            java.util.Iterator r0 = r7.iterator()
        L_0x00ca:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00eb
            java.lang.Object r1 = r0.next()
            com.fossil.yn4 r1 = (com.fossil.yn4) r1
            java.util.List r2 = r1.k()
            java.util.List r2 = com.fossil.ea7.d(r2)
            com.fossil.ro4$a r3 = new com.fossil.ro4$a
            r3.<init>()
            java.util.List r2 = com.fossil.ea7.a(r2, r3)
            r1.a(r2)
            goto L_0x00ca
        L_0x00eb:
            com.fossil.po4 r6 = r6.b
            r6.c(r7)
            com.fossil.ko4 r6 = new com.fossil.ko4
            java.util.List r7 = com.fossil.ea7.n(r7)
            r6.<init>(r7, r8)
            goto L_0x0138
        L_0x00fa:
            boolean r7 = r8 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x0139
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r6 = r6.a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "fetchHistoryChallenges failed, errorCode="
            r1.append(r2)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r2 = r8.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r7.d(r6, r1)
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            int r1 = r8.a()
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x0132
            java.lang.String r0 = r8.getMessage()
        L_0x0132:
            r7.<init>(r1, r0)
            r6.<init>(r7)
        L_0x0138:
            return r6
        L_0x0139:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ro4.a(int, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ro4 ro4, int i2, int i3, fb7 fb7, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i2 = 5;
        }
        if ((i4 & 2) != 0) {
            i3 = 0;
        }
        return ro4.a(i2, i3, fb7);
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        ee7.b(str, "challengeId");
        this.d.b(str, Boolean.valueOf(z));
    }

    @DexIgnore
    public final mn4 a(String[] strArr, Date date) {
        ee7.b(strArr, "array");
        ee7.b(date, "date");
        return this.b.a(strArr, date);
    }

    @DexIgnore
    public final long a(mn4 mn4) {
        ee7.b(mn4, "challenge");
        return this.b.a(mn4);
    }

    @DexIgnore
    public final mn4 a(String str) {
        ee7.b(str, "id");
        return this.b.a(str);
    }

    @DexIgnore
    public final int a(yn4 yn4) {
        ee7.b(yn4, "historyChallenge");
        return this.b.a(yn4);
    }

    @DexIgnore
    public final void a(List<jn4> list) {
        ee7.b(list, "players");
        this.b.c();
        this.b.d(list);
    }

    @DexIgnore
    public final void a() {
        this.b.a();
    }
}
