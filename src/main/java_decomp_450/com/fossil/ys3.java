package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys3 {
    @DexIgnore
    public /* final */ xs3 a;
    @DexIgnore
    public /* final */ xs3 b;
    @DexIgnore
    public /* final */ xs3 c;
    @DexIgnore
    public /* final */ xs3 d;
    @DexIgnore
    public /* final */ xs3 e;
    @DexIgnore
    public /* final */ xs3 f;
    @DexIgnore
    public /* final */ xs3 g;
    @DexIgnore
    public /* final */ Paint h;

    @DexIgnore
    public ys3(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(ou3.a(context, jr3.materialCalendarStyle, dt3.class.getCanonicalName()), tr3.MaterialCalendar);
        this.a = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_dayStyle, 0));
        this.g = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_dayInvalidStyle, 0));
        this.b = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_daySelectedStyle, 0));
        this.c = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList a2 = pu3.a(context, obtainStyledAttributes, tr3.MaterialCalendar_rangeFillColor);
        this.d = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_yearStyle, 0));
        this.e = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_yearSelectedStyle, 0));
        this.f = xs3.a(context, obtainStyledAttributes.getResourceId(tr3.MaterialCalendar_yearTodayStyle, 0));
        Paint paint = new Paint();
        this.h = paint;
        paint.setColor(a2.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
