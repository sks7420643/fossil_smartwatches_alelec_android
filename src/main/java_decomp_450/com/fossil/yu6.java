package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.be5;
import com.fossil.nj5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu6 extends uu6 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ long u; // = TimeUnit.SECONDS.toMillis(15);
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 3;
    @DexIgnore
    public /* final */ e h; // = new e(this, u, v);
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ Runnable k; // = new f(this);
    @DexIgnore
    public /* final */ g l; // = new g(this);
    @DexIgnore
    public /* final */ d m; // = new d(this);
    @DexIgnore
    public /* final */ c n; // = new c(this);
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ vu6 p;
    @DexIgnore
    public /* final */ UserRepository q;
    @DexIgnore
    public /* final */ qe r;
    @DexIgnore
    public /* final */ df5 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return yu6.t;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter$completeCalibration$1", f = "CalibrationPresenter.kt", l = {383}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yu6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yu6 yu6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = yu6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            df5 df5;
            String str;
            String str2;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.h.cancel();
                this.this$0.p.b();
                df5 = this.this$0.s;
                String c = this.this$0.o.c();
                UserRepository d = this.this$0.q;
                this.L$0 = yi7;
                this.L$1 = df5;
                this.L$2 = c;
                this.label = 1;
                obj = d.getCurrentUser(this);
                if (obj == a) {
                    return a;
                }
                str = c;
            } else if (i == 1) {
                str = (String) this.L$2;
                df5 = (df5) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser == null || (str2 = mFUser.getUserId()) == null) {
                str2 = "";
            }
            df5.a(str, str2);
            this.this$0.f = 4;
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ yu6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(yu6 yu6) {
            this.a = yu6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            if (ee7.a((Object) "android.bluetooth.adapter.action.STATE_CHANGED", (Object) intent.getAction())) {
                int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = yu6.w.a();
                local.d(a2, "mBluetoothStateChangeReceiver - onReceive() - state = " + intExtra);
                if (intExtra == 10) {
                    FLogger.INSTANCE.getLocal().d(yu6.w.a(), "Bluetooth off");
                    this.a.g();
                    this.a.p.a();
                    this.a.p.O();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ yu6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(yu6 yu6) {
            this.a = yu6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = yu6.w.a();
            local.d(a2, "mConnectionStateChangeReceiver onReceive: status = " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                ee7.a((Object) defaultAdapter, "BluetoothAdapter.getDefaultAdapter()");
                if (!defaultAdapter.isEnabled()) {
                    this.a.p.a();
                    this.a.p.O();
                    return;
                }
                this.a.g();
                this.a.d(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ yu6 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(yu6 yu6, long j, long j2) {
            super(j, j2);
            this.a = yu6;
        }

        @DexIgnore
        public void onFinish() {
            FLogger.INSTANCE.getLocal().d(yu6.w.a(), "CountDownTimer onFinish");
            this.a.s.a(true, this.a.o.c(), 0, CalibrationEnums.HandId.HOUR);
            cancel();
            start();
        }

        @DexIgnore
        public void onTick(long j) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yu6 a;

        @DexIgnore
        public f(yu6 yu6) {
            this.a = yu6;
        }

        @DexIgnore
        public final void run() {
            if (this.a.j) {
                this.a.p.a();
                this.a.o();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ yu6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(yu6 yu6) {
            this.a = yu6;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            boolean z = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal();
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = yu6.w.a();
            local.d(a2, "receiver mode=" + communicateMode + ", isSuccess=" + z);
            int i = zu6.a[communicateMode.ordinal()];
            if (i == 1) {
                this.a.f(z);
            } else if (i == 2) {
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                this.a.a(z, integerArrayListExtra, intExtra);
            } else if (i == 3) {
                this.a.a(z, intExtra);
            } else if (i == 4) {
                this.a.e(z);
            } else if (i == 5) {
                this.a.d(z);
            }
        }
    }

    /*
    static {
        String simpleName = yu6.class.getSimpleName();
        ee7.a((Object) simpleName, "CalibrationPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public yu6(PortfolioApp portfolioApp, vu6 vu6, UserRepository userRepository, qe qeVar, df5 df5) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(vu6, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(qeVar, "mLocalBroadcastManager");
        ee7.b(df5, "mWatchHelper");
        this.o = portfolioApp;
        this.p = vu6;
        this.q = userRepository;
        this.r = qeVar;
        this.s = df5;
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void h() {
        FLogger.INSTANCE.getLocal().d(t, "back");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.v();
        } else if (i2 == 1) {
            this.f = 0;
            this.p.o(this.o.c());
        } else if (i2 == 2) {
            this.f = 1;
            this.p.D(this.o.c());
        } else if (i2 == 3) {
            if (be5.o.a(this.o.c()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.n(this.o.c());
            } else {
                this.f = 1;
                this.p.D(this.o.c());
            }
        }
        this.p.d(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public List<be5.b> i() {
        ArrayList arrayList = new ArrayList();
        MFDeviceFamily a2 = be5.o.a(this.o.c());
        if (a2 == MFDeviceFamily.DEVICE_FAMILY_SAM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_MINI) {
            arrayList.add(be5.b.HYBRID_WATCH_HOUR);
            arrayList.add(be5.b.HYBRID_WATCH_MINUTE);
            arrayList.add(be5.b.HYBRID_WATCH_SUBEYE);
        } else {
            arrayList.add(be5.b.DIANA_WATCH_HOUR);
            arrayList.add(be5.b.DIANA_WATCH_MINUTE);
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void j() {
        FLogger.INSTANCE.getLocal().d(t, "next");
        int i2 = this.f;
        if (i2 == 0) {
            a("device_calibrate", "Step", AppFilter.COLUMN_HOUR);
            this.f = 1;
            this.p.D(this.o.c());
        } else if (i2 == 1) {
            a("device_calibrate", "Step", MFSleepGoal.COLUMN_MINUTE);
            if (be5.o.a(this.o.c()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.n(this.o.c());
            } else {
                this.f = 3;
                l();
            }
        } else if (i2 == 2) {
            a("device_calibrate", "Step", "subeye");
            this.f = 3;
            l();
        } else if (i2 == 3) {
            l();
        }
        this.p.d(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void k() {
        FLogger.INSTANCE.getLocal().d(t, "stopSmartMove");
        this.s.a();
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(t, "completeCalibration");
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(t, "registerBroadcastReceiver()");
        nj5.d.a(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
        qe qeVar = this.r;
        d dVar = this.m;
        qeVar.a(dVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        this.o.registerReceiver(this.n, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public final void o() {
        if (!this.e) {
            this.p.y0();
        }
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(t, "startCalibration");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.o(this.o.c());
        } else if (i2 == 1) {
            this.p.D(this.o.c());
        } else if (i2 == 2) {
            this.p.n(this.o.c());
        }
        if (!this.e) {
            this.p.b();
            FLogger.INSTANCE.getLocal().e(t, "mStartCalibration");
            this.s.b(this.o.c());
            a(30);
        }
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d(t, "stopSetConfigTimeOutTimer");
        this.j = false;
        this.i.removeCallbacks(this.k);
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d(t, "unregisterBroadcastReceiver()");
        try {
            nj5.d.b(this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
            this.r.a(this.m);
            this.o.unregisterReceiver(this.n);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "unregisterBroadcastReceiver() - ex = " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startMove: clockwise = " + z);
        int i2 = FossilDeviceSerialPatternUtil.isDianaDevice(this.o.c()) ? 1 : 2;
        int i3 = this.f;
        if (i3 == 0) {
            this.s.a(z, this.o.c(), i2, CalibrationEnums.HandId.HOUR);
        } else if (i3 == 1) {
            this.s.a(z, this.o.c(), i2, CalibrationEnums.HandId.MINUTE);
        } else if (i3 == 2) {
            this.s.a(z, this.o.c(), i2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void c(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSmartMove: clockwise = " + z + ", mCalibrationStep = " + this.f);
        int i2 = this.f;
        if (i2 == 0) {
            this.s.b(z, this.o.c(), 2, CalibrationEnums.HandId.HOUR);
        } else if (i2 == 1) {
            this.s.b(z, this.o.c(), 2, CalibrationEnums.HandId.MINUTE);
        } else if (i2 == 2) {
            this.s.b(z, this.o.c(), 2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onExitCalibrationComplete success=" + z);
        this.p.a();
        if (z) {
            this.p.v();
        } else {
            o();
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onMoveHandComplete isSuccess=" + z);
        if (!z) {
            this.s.a(this.o.c());
            o();
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(t, "start()");
        r();
        m();
        p();
        nj5.d.a(CommunicateMode.ENTER_CALIBRATION);
        int i2 = be5.o.a(this.o.c()) == MFDeviceFamily.DEVICE_FAMILY_SAM ? 3 : 2;
        this.g = i2;
        this.p.d(this.f, i2);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        if (this.f != 3) {
            this.s.a(this.o.c());
        }
        r();
        this.h.cancel();
        q();
    }

    @DexIgnore
    public final void a(boolean z, ArrayList<Integer> arrayList, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onEnterCalibrationComplete isSuccess=" + z + ", lastErrorCode=" + i2);
        if (z) {
            this.s.c(this.o.c());
            return;
        }
        this.p.a();
        if (i2 != 1101 && i2 != 1112 && i2 != 1113) {
            o();
        } else if (arrayList != null) {
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(arrayList);
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026onErrorCode(errorCodes!!)");
            vu6 vu6 = this.p;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                vu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.e(str, "onResetHandComplete isSuccess=" + z);
        this.p.a();
        if (z) {
            q();
            this.h.start();
            return;
        }
        o();
    }

    @DexIgnore
    public final void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "onApplyHandsComplete isSuccess=" + z);
        if (z) {
            a("device_calibrate_result", "errorCode", "N/A");
            return;
        }
        a("device_calibrate_result", "errorCode", String.valueOf(i2));
        this.p.a();
        o();
    }

    @DexIgnore
    public final void a(int i2) {
        q();
        this.j = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "startSetConfigTimeOutTimer:  timeout = " + i2);
        this.i.postDelayed(this.k, ((long) i2) * 1000);
    }

    @DexIgnore
    @Override // com.fossil.uu6
    public void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        qd5.f.c().b(str, str2, str3);
    }
}
