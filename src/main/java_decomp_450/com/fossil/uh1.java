package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uh1 extends yf1 {
    @DexIgnore
    public /* final */ byte[] A; // = new byte[0];
    @DexIgnore
    public byte[] B; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] C; // = new byte[0];
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ boolean F; // = true;

    @DexIgnore
    public uh1(qa1 qa1, ri1 ri1, int i) {
        super(qa1, ri1, i);
    }

    @DexIgnore
    public abstract mw0 a(byte b);

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.E = true;
        return new JSONObject();
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final void a(eo0 eo0) {
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final void b(rk1 rk1) {
        if (d(rk1)) {
            e(rk1);
        } else if (c(rk1)) {
            f(rk1);
        }
        if (this.D && this.E) {
            a(((v81) this).v);
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final void c(eo0 eo0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.D = true;
        wr1 wr1 = ((v81) this).f;
        if (wr1 != null) {
            wr1.i = true;
        }
        wr1 wr12 = ((v81) this).f;
        if (!(wr12 == null || (jSONObject2 = wr12.m) == null)) {
            yz0.a(jSONObject2, r51.j, yz0.a(ay0.a));
        }
        if (((v81) this).v.c == ay0.b) {
            sz0 a = sz0.a(((v81) this).v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
            ((v81) this).v = a;
            ay0 ay0 = a.c;
            ay0 ay02 = ay0.a;
        }
        wr1 wr13 = ((v81) this).f;
        if (wr13 != null) {
            wr13.i = true;
        }
        wr1 wr14 = ((v81) this).f;
        if (!(wr14 == null || (jSONObject = wr14.m) == null)) {
            yz0.a(jSONObject, r51.j, yz0.a(ay0.a));
        }
        j();
        if (this.E) {
            a(((v81) this).v);
        }
    }

    @DexIgnore
    public boolean c(rk1 rk1) {
        return false;
    }

    @DexIgnore
    public final boolean d(rk1 rk1) {
        if (rk1.a == o() && rk1.b.length >= q().length) {
            if (Arrays.equals(q(), s97.a(rk1.b, 0, q().length))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void e(rk1 rk1) {
        byte[] bArr = rk1.b;
        JSONObject jSONObject = new JSONObject();
        if (p()) {
            mw0 a = a(bArr[q().length]);
            sz0 a2 = sz0.a(((v81) this).v, null, null, sz0.f.a(a).c, null, a, 11);
            ((v81) this).v = a2;
            if (a2.c == ay0.a) {
                jSONObject = a(s97.a(bArr, q().length + 1, bArr.length));
            } else {
                this.E = true;
            }
        } else {
            jSONObject = a(s97.a(bArr, q().length, bArr.length));
        }
        ((v81) this).g.add(new zq0(0, rk1.a, bArr, jSONObject, 1));
    }

    @DexIgnore
    public void f(rk1 rk1) {
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public final eo0 k() {
        ByteBuffer put = ByteBuffer.allocate(n().length + m().length).order(ByteOrder.LITTLE_ENDIAN).put(n()).put(m());
        qk1 l = l();
        byte[] array = put.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return new ze1(l, array, ((v81) this).y.w);
    }

    @DexIgnore
    public abstract qk1 l();

    @DexIgnore
    public byte[] m() {
        return this.C;
    }

    @DexIgnore
    public byte[] n() {
        return this.A;
    }

    @DexIgnore
    public abstract qk1 o();

    @DexIgnore
    public boolean p() {
        return this.F;
    }

    @DexIgnore
    public byte[] q() {
        return this.B;
    }
}
