package com.fossil;

import com.fossil.y44;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z44 implements w44 {
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public y44 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements y44.d {
        @DexIgnore
        public /* final */ /* synthetic */ byte[] a;
        @DexIgnore
        public /* final */ /* synthetic */ int[] b;

        @DexIgnore
        public a(z44 z44, byte[] bArr, int[] iArr) {
            this.a = bArr;
            this.b = iArr;
        }

        @DexIgnore
        @Override // com.fossil.y44.d
        public void a(InputStream inputStream, int i) throws IOException {
            try {
                inputStream.read(this.a, this.b[0], i);
                int[] iArr = this.b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public /* final */ byte[] a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(z44 z44, byte[] bArr, int i) {
            this.a = bArr;
            this.b = i;
        }
    }

    @DexIgnore
    public z44(File file, int i) {
        this.a = file;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.w44
    public void a(long j, String str) {
        f();
        b(j, str);
    }

    @DexIgnore
    @Override // com.fossil.w44
    public String b() {
        byte[] c2 = c();
        if (c2 != null) {
            return new String(c2, d);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.w44
    public byte[] c() {
        b e = e();
        if (e == null) {
            return null;
        }
        int i = e.b;
        byte[] bArr = new byte[i];
        System.arraycopy(e.a, 0, bArr, 0, i);
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.w44
    public void d() {
        a();
        this.a.delete();
    }

    @DexIgnore
    public final b e() {
        if (!this.a.exists()) {
            return null;
        }
        f();
        y44 y44 = this.c;
        if (y44 == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[y44.k()];
        try {
            this.c.a(new a(this, bArr, iArr));
        } catch (IOException e) {
            z24.a().b("A problem occurred while reading the Crashlytics log file.", e);
        }
        return new b(this, bArr, iArr[0]);
    }

    @DexIgnore
    public final void f() {
        if (this.c == null) {
            try {
                this.c = new y44(this.a);
            } catch (IOException e) {
                z24 a2 = z24.a();
                a2.b("Could not open log file: " + this.a, e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.w44
    public void a() {
        t34.a(this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    @DexIgnore
    public final void b(long j, String str) {
        if (this.c != null) {
            if (str == null) {
                str = "null";
            }
            try {
                int i = this.b / 4;
                if (str.length() > i) {
                    str = "..." + str.substring(str.length() - i);
                }
                this.c.a(String.format(Locale.US, "%d %s%n", Long.valueOf(j), str.replaceAll("\r", " ").replaceAll("\n", " ")).getBytes(d));
                while (!this.c.b() && this.c.k() > this.b) {
                    this.c.g();
                }
            } catch (IOException e) {
                z24.a().b("There was a problem writing to the Crashlytics log.", e);
            }
        }
    }
}
