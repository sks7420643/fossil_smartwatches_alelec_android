package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.ln;
import com.fossil.rp;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kn implements rn, vm, rp.b {
    @DexIgnore
    public static /* final */ String j; // = im.a("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ ln d;
    @DexIgnore
    public /* final */ sn e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public PowerManager.WakeLock h;
    @DexIgnore
    public boolean i; // = false;

    @DexIgnore
    public kn(Context context, int i2, String str, ln lnVar) {
        this.a = context;
        this.b = i2;
        this.d = lnVar;
        this.c = str;
        this.e = new sn(this.a, lnVar.d(), this);
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        im.a().a(j, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        a();
        if (z) {
            Intent b2 = in.b(this.a, this.c);
            ln lnVar = this.d;
            lnVar.a(new ln.b(lnVar, b2, this.b));
        }
        if (this.i) {
            Intent a2 = in.a(this.a);
            ln lnVar2 = this.d;
            lnVar2.a(new ln.b(lnVar2, a2, this.b));
        }
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void b(List<String> list) {
        if (list.contains(this.c)) {
            synchronized (this.f) {
                if (this.g == 0) {
                    this.g = 1;
                    im.a().a(j, String.format("onAllConstraintsMet for %s", this.c), new Throwable[0]);
                    if (this.d.c().e(this.c)) {
                        this.d.f().a(this.c, 600000, this);
                    } else {
                        a();
                    }
                } else {
                    im.a().a(j, String.format("Already started work for %s", this.c), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        synchronized (this.f) {
            if (this.g < 2) {
                this.g = 2;
                im.a().a(j, String.format("Stopping work for WorkSpec %s", this.c), new Throwable[0]);
                this.d.a(new ln.b(this.d, in.c(this.a, this.c), this.b));
                if (this.d.c().c(this.c)) {
                    im.a().a(j, String.format("WorkSpec %s needs to be rescheduled", this.c), new Throwable[0]);
                    this.d.a(new ln.b(this.d, in.b(this.a, this.c), this.b));
                } else {
                    im.a().a(j, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.c), new Throwable[0]);
                }
            } else {
                im.a().a(j, String.format("Already stopped work for %s", this.c), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.rp.b
    public void a(String str) {
        im.a().a(j, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
        c();
    }

    @DexIgnore
    @Override // com.fossil.rn
    public void a(List<String> list) {
        c();
    }

    @DexIgnore
    public void b() {
        this.h = op.a(this.a, String.format("%s (%s)", this.c, Integer.valueOf(this.b)));
        im.a().a(j, String.format("Acquiring wakelock %s for WorkSpec %s", this.h, this.c), new Throwable[0]);
        this.h.acquire();
        zo e2 = this.d.e().f().f().e(this.c);
        if (e2 == null) {
            c();
            return;
        }
        boolean b2 = e2.b();
        this.i = b2;
        if (!b2) {
            im.a().a(j, String.format("No constraints for %s", this.c), new Throwable[0]);
            b(Collections.singletonList(this.c));
            return;
        }
        this.e.a((Iterable<zo>) Collections.singletonList(e2));
    }

    @DexIgnore
    public final void a() {
        synchronized (this.f) {
            this.e.a();
            this.d.f().a(this.c);
            if (this.h != null && this.h.isHeld()) {
                im.a().a(j, String.format("Releasing wakelock %s for WorkSpec %s", this.h, this.c), new Throwable[0]);
                this.h.release();
            }
        }
    }
}
