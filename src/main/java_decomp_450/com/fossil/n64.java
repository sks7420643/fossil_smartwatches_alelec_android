package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n64 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public fo7 c;

    @DexIgnore
    public n64(int i, String str, fo7 fo7) {
        this.a = i;
        this.b = str;
        this.c = fo7;
    }

    @DexIgnore
    public static n64 a(Response response) throws IOException {
        return new n64(response.e(), response.a() == null ? null : response.a().string(), response.k());
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }
}
