package com.fossil;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.cm5;
import com.fossil.cz5;
import com.fossil.dy5;
import com.fossil.fl4;
import com.fossil.ql4;
import com.fossil.qy5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex5 extends yw5 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public /* final */ List<Object> f; // = new ArrayList();
    @DexIgnore
    public List<Object> g; // = new ArrayList();
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public /* final */ List<bt5> i; // = new ArrayList();
    @DexIgnore
    public /* final */ List<at5> j; // = new ArrayList();
    @DexIgnore
    public /* final */ List<bt5> k; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> m; // = new ArrayList();
    @DexIgnore
    public /* final */ LoaderManager n;
    @DexIgnore
    public /* final */ zw5 o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ rl4 q;
    @DexIgnore
    public /* final */ qy5 r;
    @DexIgnore
    public /* final */ dy5 s;
    @DexIgnore
    public /* final */ cz5 t;
    @DexIgnore
    public /* final */ cm5 u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ex5.v;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.d<dy5.b, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ex5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ex5 ex5) {
            this.a = ex5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dy5.b bVar) {
            ee7.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetApps onSuccess");
            ArrayList arrayList = new ArrayList();
            for (at5 at5 : bVar.a()) {
                InstalledApp installedApp = at5.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    ee7.a();
                    throw null;
                } else if (isSelected.booleanValue() && at5.getCurrentHandGroup() == this.a.p) {
                    arrayList.add(at5);
                }
            }
            this.a.o().addAll(arrayList);
            this.a.n().addAll(arrayList);
            List<Object> p = this.a.p();
            Object[] array = arrayList.toArray(new at5[0]);
            if (array != null) {
                Serializable a2 = ws7.a((Serializable) array);
                ee7.a((Object) a2, "SerializationUtils.clone\u2026sSelected.toTypedArray())");
                ba7.a(p, (Object[]) a2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = ex5.w.a();
                local.d(a3, "mContactAndAppDataFirstLoad.size=" + this.a.p().size());
                this.a.n.a(1, new Bundle(), this.a);
                this.a.o.g(this.a.o());
                this.a.o.h();
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            ee7.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetApps onError");
            this.a.o.h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.d<qy5.d, qy5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ex5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ qy5.d $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ex5$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ex5$c$a$a  reason: collision with other inner class name */
            public static final class C0063a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $contactWrapperList;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0063a(a aVar, List list, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$contactWrapperList = list;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0063a aVar = new C0063a(this.this$0, this.$contactWrapperList, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0063a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Contact contact;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        for (ContactGroup contactGroup : this.this$0.$responseValue.a()) {
                            for (Contact contact2 : contactGroup.getContacts()) {
                                if (contactGroup.getHour() == this.this$0.this$0.a.p) {
                                    bt5 bt5 = new bt5(contact2, "");
                                    bt5.setAdded(true);
                                    ee7.a((Object) contact2, "contact");
                                    ContactGroup contactGroup2 = contact2.getContactGroup();
                                    ee7.a((Object) contactGroup2, "contact.contactGroup");
                                    bt5.setCurrentHandGroup(contactGroup2.getHour());
                                    Contact contact3 = bt5.getContact();
                                    if (contact3 != null) {
                                        contact3.setDbRowId(contact2.getDbRowId());
                                        contact3.setUseSms(contact2.isUseSms());
                                        contact3.setUseCall(contact2.isUseCall());
                                    }
                                    List<PhoneNumber> phoneNumbers = contact2.getPhoneNumbers();
                                    ee7.a((Object) phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact2.getPhoneNumbers().get(0);
                                        ee7.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            bt5.setHasPhoneNumber(true);
                                            bt5.setPhoneNumber(number);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a = ex5.w.a();
                                            local.d(a, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a2 = ex5.w.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                                    ContactGroup contactGroup3 = contact2.getContactGroup();
                                    ee7.a((Object) contactGroup3, "contact.contactGroup");
                                    sb.append(contactGroup3.getHour());
                                    sb.append(" ,rowId=");
                                    sb.append(contact2.getDbRowId());
                                    sb.append(" ,isUseText=");
                                    sb.append(contact2.isUseSms());
                                    sb.append(" ,isUseCall=");
                                    sb.append(contact2.isUseCall());
                                    local2.d(a2, sb.toString());
                                    this.$contactWrapperList.add(bt5);
                                    Contact contact4 = bt5.getContact();
                                    if ((contact4 != null && contact4.getContactId() == -100) || ((contact = bt5.getContact()) != null && contact.getContactId() == -200)) {
                                        this.this$0.this$0.a.r().add(bt5);
                                    }
                                }
                            }
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qy5.d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetAllHybridContactGroups onSuccess");
                    ArrayList arrayList = new ArrayList();
                    hj7 a2 = xh7.a(yi7, this.this$0.a.b(), null, new C0063a(this, arrayList, null), 2, null);
                    this.L$0 = yi7;
                    this.L$1 = arrayList;
                    this.L$2 = a2;
                    this.label = 1;
                    if (a2.c(this) == a) {
                        return a;
                    }
                    list = arrayList;
                } else if (i == 1) {
                    hj7 hj7 = (hj7) this.L$2;
                    list = (List) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.o().addAll(list);
                this.this$0.a.q().addAll(list);
                List<Object> p = this.this$0.a.p();
                Object[] array = list.toArray(new bt5[0]);
                if (array != null) {
                    Serializable a3 = ws7.a((Serializable) array);
                    ee7.a((Object) a3, "SerializationUtils.clone\u2026apperList.toTypedArray())");
                    ba7.a(p, (Object[]) a3);
                    this.this$0.a.t();
                    return i97.a;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(ex5 ex5) {
            this.a = ex5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qy5.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, dVar, null), 3, null);
        }

        @DexIgnore
        public void a(qy5.b bVar) {
            ee7.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetAllHybridContactGroups onError");
            this.a.o.h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ql4.d<dy5.b, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ex5 a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public d(ex5 ex5, ArrayList arrayList, List list) {
            this.a = ex5;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dy5.b bVar) {
            ee7.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetHybridApp onSuccess");
            ArrayList arrayList = new ArrayList();
            for (String str : this.b) {
                Iterator<T> it = bVar.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        T next = it.next();
                        if (ee7.a((Object) String.valueOf(next.getUri()), (Object) str)) {
                            InstalledApp installedApp = next.getInstalledApp();
                            if (installedApp != null) {
                                installedApp.setSelected(true);
                            }
                            next.setCurrentHandGroup(this.a.p);
                            arrayList.add(next);
                            List list = this.c;
                            Uri uri = next.getUri();
                            if (uri != null) {
                                list.add(uri);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ex5.w.a();
            local.d(a2, "queryAppsString: appsSelected=" + arrayList);
            this.a.o().addAll(arrayList);
            ex5 ex5 = this.a;
            ex5.a(ex5.s());
            this.a.o.D(this.a.i());
            int size = this.a.n().size();
            for (int i = 0; i < size; i++) {
                if (!ea7.a((Iterable) this.c, (Object) this.a.n().get(i).getUri())) {
                    at5 at5 = this.a.n().get(i);
                    InstalledApp installedApp2 = at5.getInstalledApp();
                    if (installedApp2 != null) {
                        installedApp2.setSelected(false);
                        this.a.o().add(at5);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            this.a.o.g(this.a.o());
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            ee7.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "mGetApps onError");
            this.a.o.h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<cm5.d, cm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ex5 a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ql4.d<cz5.c, ql4.a> {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(cz5.c cVar) {
                ee7.b(cVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(ex5.w.a(), ".Inside mSaveAllNotification onSuccess");
                this.a.a.o.h();
                e eVar = this.a;
                if (eVar.d) {
                    eVar.a.o.close();
                }
            }

            @DexIgnore
            public void a(ql4.a aVar) {
                ee7.b(aVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(ex5.w.a(), ".Inside mSaveAllNotification onError");
                this.a.a.o.h();
            }
        }

        @DexIgnore
        public e(ex5 ex5, List list, List list2, boolean z) {
            this.a = ex5;
            this.b = list;
            this.c = list2;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(cm5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "saveNotification success!!");
            this.a.q.a(this.a.t, new cz5.b(this.b, this.c), new a(this));
        }

        @DexIgnore
        public void a(cm5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ex5.w.a();
            local.d(a2, "saveNotification fail!! errorValue=" + cVar.a());
            this.a.o.h();
            if (cVar.c() != 1101) {
                this.a.o.l();
                return;
            }
            FLogger.INSTANCE.getLocal().d(ex5.w.a(), "Bluetooth is disabled");
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(cVar.b());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            zw5 f = this.a.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                f.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = ex5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationContactsAndA\u2026er::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public ex5(LoaderManager loaderManager, zw5 zw5, int i2, rl4 rl4, qy5 qy5, dy5 dy5, cz5 cz5, cm5 cm5) {
        ee7.b(loaderManager, "mLoaderManager");
        ee7.b(zw5, "mView");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(qy5, "mGetAllHybridContactGroups");
        ee7.b(dy5, "mGetHybridApp");
        ee7.b(cz5, "mSaveAllHybridNotification");
        ee7.b(cm5, "mSetNotificationFiltersUserCase");
        this.n = loaderManager;
        this.o = zw5;
        this.p = i2;
        this.q = rl4;
        this.r = qy5;
        this.s = dy5;
        this.t = cz5;
        this.u = cm5;
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void a(oe<Cursor> oeVar) {
        ee7.b(oeVar, "loader");
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void j() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if (obj instanceof at5) {
                    at5 at5 = (at5) obj;
                    InstalledApp installedApp = at5.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        ee7.a();
                        throw null;
                    } else if (isSelected.booleanValue()) {
                        Uri uri = at5.getUri();
                        if (uri != null) {
                            arrayList.add(uri.toString());
                        }
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.b(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.zw5 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.yw5
    public void k() {
        bt5 bt5;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof bt5) && ((contact = (bt5 = (bt5) obj).getContact()) == null || contact.getContactId() != -100 || (contact2 = bt5.getContact()) == null || contact2.getContactId() != -200)) {
                    if (bt5.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.c(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.zw5 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.yw5
    public void l() {
        bt5 bt5;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof bt5) && (((contact = (bt5 = (bt5) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = bt5.getContact()) != null && contact2.getContactId() == -200))) {
                    if (bt5.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.a(this.p, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "mContactAndAppData.size=" + this.f.size() + " mContactAndAppDataFirstLoad.size=" + this.g.size());
        if (i()) {
            this.o.j();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (T t2 : this.f) {
                if (t2 instanceof bt5) {
                    arrayList.add(t2);
                } else if (t2 != null) {
                    arrayList2.add(t2);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                }
            }
            a((List<bt5>) arrayList, (List<at5>) arrayList2, true);
        }
    }

    @DexIgnore
    public final List<at5> n() {
        return this.j;
    }

    @DexIgnore
    public final List<Object> o() {
        return this.f;
    }

    @DexIgnore
    public final List<Object> p() {
        return this.g;
    }

    @DexIgnore
    public final List<bt5> q() {
        return this.i;
    }

    @DexIgnore
    public final List<bt5> r() {
        return this.k;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0071 A[EDGE_INSN: B:128:0x0071->B:30:0x0071 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x012e A[EDGE_INSN: B:131:0x012e->B:86:0x012e ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean s() {
        /*
            r11 = this;
            java.util.List<java.lang.Object> r0 = r11.g
            int r0 = r0.size()
            java.util.List<java.lang.Object> r1 = r11.f
            int r1 = r1.size()
            if (r0 < r1) goto L_0x0013
            java.util.List<java.lang.Object> r0 = r11.g
            java.util.List<java.lang.Object> r1 = r11.f
            goto L_0x0017
        L_0x0013:
            java.util.List<java.lang.Object> r0 = r11.f
            java.util.List<java.lang.Object> r1 = r11.g
        L_0x0017:
            java.util.Iterator r0 = r0.iterator()
        L_0x001b:
            boolean r2 = r0.hasNext()
            r3 = 0
            if (r2 == 0) goto L_0x0162
            java.lang.Object r2 = r0.next()
            boolean r4 = r2 instanceof com.fossil.bt5
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x00ea
            r4 = r2
            com.fossil.bt5 r4 = (com.fossil.bt5) r4
            com.fossil.wearables.fsl.contact.Contact r7 = r4.getContact()
            if (r7 == 0) goto L_0x00ea
            com.fossil.wearables.fsl.contact.Contact r2 = r4.getContact()
            if (r2 == 0) goto L_0x00e6
            int r2 = r2.getContactId()
            java.util.Iterator r7 = r1.iterator()
        L_0x0043:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x0070
            java.lang.Object r8 = r7.next()
            boolean r9 = r8 instanceof com.fossil.bt5
            if (r9 == 0) goto L_0x006c
            r9 = r8
            com.fossil.bt5 r9 = (com.fossil.bt5) r9
            com.fossil.wearables.fsl.contact.Contact r10 = r9.getContact()
            if (r10 == 0) goto L_0x006c
            com.fossil.wearables.fsl.contact.Contact r9 = r9.getContact()
            if (r9 == 0) goto L_0x0068
            int r9 = r9.getContactId()
            if (r9 != r2) goto L_0x006c
            r9 = 1
            goto L_0x006d
        L_0x0068:
            com.fossil.ee7.a()
            throw r6
        L_0x006c:
            r9 = 0
        L_0x006d:
            if (r9 == 0) goto L_0x0043
            goto L_0x0071
        L_0x0070:
            r8 = r6
        L_0x0071:
            if (r8 != 0) goto L_0x0074
            return r5
        L_0x0074:
            com.fossil.bt5 r8 = (com.fossil.bt5) r8
            boolean r2 = r4.isAdded()
            boolean r3 = r8.isAdded()
            if (r2 != r3) goto L_0x00e5
            com.fossil.wearables.fsl.contact.Contact r2 = r4.getContact()
            if (r2 == 0) goto L_0x00e1
            boolean r2 = r2.isUseSms()
            com.fossil.wearables.fsl.contact.Contact r3 = r8.getContact()
            if (r3 == 0) goto L_0x00dd
            boolean r3 = r3.isUseSms()
            if (r2 != r3) goto L_0x00e5
            com.fossil.wearables.fsl.contact.Contact r2 = r4.getContact()
            if (r2 == 0) goto L_0x00d9
            boolean r2 = r2.isUseCall()
            com.fossil.wearables.fsl.contact.Contact r3 = r8.getContact()
            if (r3 == 0) goto L_0x00d5
            boolean r3 = r3.isUseCall()
            if (r2 != r3) goto L_0x00e5
            com.fossil.wearables.fsl.contact.Contact r2 = r4.getContact()
            if (r2 == 0) goto L_0x00d1
            boolean r2 = r2.isUseEmail()
            com.fossil.wearables.fsl.contact.Contact r3 = r8.getContact()
            if (r3 == 0) goto L_0x00cd
            boolean r3 = r3.isUseEmail()
            if (r2 != r3) goto L_0x00e5
            int r2 = r4.getCurrentHandGroup()
            int r3 = r8.getCurrentHandGroup()
            if (r2 == r3) goto L_0x001b
            goto L_0x00e5
        L_0x00cd:
            com.fossil.ee7.a()
            throw r6
        L_0x00d1:
            com.fossil.ee7.a()
            throw r6
        L_0x00d5:
            com.fossil.ee7.a()
            throw r6
        L_0x00d9:
            com.fossil.ee7.a()
            throw r6
        L_0x00dd:
            com.fossil.ee7.a()
            throw r6
        L_0x00e1:
            com.fossil.ee7.a()
            throw r6
        L_0x00e5:
            return r5
        L_0x00e6:
            com.fossil.ee7.a()
            throw r6
        L_0x00ea:
            boolean r4 = r2 instanceof com.fossil.at5
            if (r4 == 0) goto L_0x001b
            com.fossil.at5 r2 = (com.fossil.at5) r2
            com.portfolio.platform.data.model.InstalledApp r4 = r2.getInstalledApp()
            if (r4 == 0) goto L_0x001b
            java.util.Iterator r4 = r1.iterator()
        L_0x00fa:
            boolean r7 = r4.hasNext()
            if (r7 == 0) goto L_0x012d
            java.lang.Object r7 = r4.next()
            boolean r8 = r7 instanceof com.fossil.at5
            if (r8 == 0) goto L_0x0129
            r8 = r7
            com.fossil.at5 r8 = (com.fossil.at5) r8
            android.net.Uri r9 = r8.getUri()
            java.lang.String r9 = java.lang.String.valueOf(r9)
            android.net.Uri r10 = r2.getUri()
            java.lang.String r10 = java.lang.String.valueOf(r10)
            boolean r9 = com.fossil.ee7.a(r9, r10)
            if (r9 == 0) goto L_0x0129
            com.portfolio.platform.data.model.InstalledApp r8 = r8.getInstalledApp()
            if (r8 == 0) goto L_0x0129
            r8 = 1
            goto L_0x012a
        L_0x0129:
            r8 = 0
        L_0x012a:
            if (r8 == 0) goto L_0x00fa
            goto L_0x012e
        L_0x012d:
            r7 = r6
        L_0x012e:
            if (r7 != 0) goto L_0x0131
            return r5
        L_0x0131:
            com.fossil.at5 r7 = (com.fossil.at5) r7
            int r3 = r7.getCurrentHandGroup()
            int r4 = r2.getCurrentHandGroup()
            if (r3 != r4) goto L_0x0161
            com.portfolio.platform.data.model.InstalledApp r3 = r7.getInstalledApp()
            if (r3 == 0) goto L_0x015d
            java.lang.Boolean r3 = r3.isSelected()
            com.portfolio.platform.data.model.InstalledApp r2 = r2.getInstalledApp()
            if (r2 == 0) goto L_0x0159
            java.lang.Boolean r2 = r2.isSelected()
            boolean r2 = com.fossil.ee7.a(r3, r2)
            r2 = r2 ^ r5
            if (r2 == 0) goto L_0x001b
            goto L_0x0161
        L_0x0159:
            com.fossil.ee7.a()
            throw r6
        L_0x015d:
            com.fossil.ee7.a()
            throw r6
        L_0x0161:
            return r5
        L_0x0162:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ex5.s():boolean");
    }

    @DexIgnore
    public final void t() {
        this.q.a(this.s, null, new b(this));
    }

    @DexIgnore
    public final void u() {
        this.o.j();
        this.q.a(this.r, null, new c(this));
    }

    @DexIgnore
    public final void v() {
        if (!this.m.isEmpty()) {
            Collection a2 = qt7.a(this.m, this.l);
            ee7.a((Object) a2, "CollectionUtils.subtract\u2026actIds, mPhoneContactIds)");
            List n2 = ea7.n(a2);
            if (!n2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                int size = this.f.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    Object obj = this.f.get(size);
                    if (obj instanceof bt5) {
                        bt5 bt5 = (bt5) obj;
                        Contact contact = bt5.getContact();
                        Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                        int size2 = n2.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                break;
                            } else if (ee7.a((Integer) n2.get(i2), valueOf)) {
                                bt5.setAdded(false);
                                arrayList.add(obj);
                                this.f.remove(size);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.o.j();
                    a((List<bt5>) arrayList, (List<at5>) new ArrayList(), false);
                }
            }
        }
    }

    @DexIgnore
    public void w() {
        this.o.a(this);
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void b(ArrayList<bt5> arrayList) {
        bt5 bt5;
        Contact contact;
        Contact contact2;
        ee7.b(arrayList, "contactWrappersSelected");
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof bt5) && (((contact = (bt5 = (bt5) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = bt5.getContact()) != null && contact2.getContactId() == -200))) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact3 = t2.getContact();
            if (contact3 != null) {
                arrayList2.add(Integer.valueOf(contact3.getContactId()));
            } else {
                ee7.a();
                throw null;
            }
        }
        a(s());
        this.o.D(i());
        int size2 = this.k.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact4 = this.k.get(i2).getContact();
            if (!ea7.a((Iterable) arrayList2, (Object) (contact4 != null ? Integer.valueOf(contact4.getContactId()) : null))) {
                bt5 bt52 = this.k.get(i2);
                bt52.setAdded(false);
                this.f.add(bt52);
            }
        }
        this.o.g(this.f);
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void c(ArrayList<String> arrayList) {
        ee7.b(arrayList, "stringAppsSelected");
        FLogger.INSTANCE.getLocal().d(v, "queryAppsString = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof at5) {
                    this.f.remove(size);
                }
            }
        }
        this.q.a(this.s, null, new d(this, arrayList, new ArrayList()));
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(v, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.u.e();
        nj5.d.a(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
        zw5 zw5 = this.o;
        if (zw5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedFragment");
        } else if (((ax5) zw5).getContext() != null) {
            this.o.k(this.p);
            if (!this.f.isEmpty() || !this.h) {
                ee7.a((Object) this.n.a(1, new Bundle(), this), "mLoaderManager.initLoade\u2026AndAppsAssignedPresenter)");
                return;
            }
            this.h = false;
            u();
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(v, "stop");
        this.u.e();
        this.n.a(1);
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void h() {
        a(s());
        this.o.D(i());
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void a(int i2, boolean z, boolean z2) {
        bt5 bt5;
        Contact contact;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "updateContactWrapper: contactId=" + i2 + ", useCall=" + z + ", useText=" + z2);
        Iterator<Object> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if ((next instanceof bt5) && (contact = (bt5 = (bt5) next).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = bt5.getContact();
                if (contact2 != null) {
                    contact2.setUseCall(z);
                }
                Contact contact3 = bt5.getContact();
                if (contact3 != null) {
                    contact3.setUseSms(z2);
                }
            }
        }
        a(s());
        this.o.D(i());
        this.o.g(this.f);
    }

    @DexIgnore
    @Override // com.fossil.yw5
    public void a(ArrayList<bt5> arrayList) {
        ee7.b(arrayList, "contactWrappersSelected");
        FLogger.INSTANCE.getLocal().d(v, "addContactWrapperList: contactWrappersSelected = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof bt5) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact = t2.getContact();
            if (contact != null) {
                arrayList2.add(Integer.valueOf(contact.getContactId()));
            } else {
                ee7.a();
                throw null;
            }
        }
        a(s());
        this.o.D(i());
        int size2 = this.i.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact2 = this.i.get(i2).getContact();
            if (!ea7.a((Iterable) arrayList2, (Object) (contact2 != null ? Integer.valueOf(contact2.getContactId()) : null))) {
                bt5 bt5 = this.i.get(i2);
                bt5.setAdded(false);
                this.f.add(bt5);
            }
        }
        this.o.g(this.f);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public oe<Cursor> a(int i2, Bundle bundle) {
        boolean z;
        this.m.clear();
        this.l.clear();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("has_phone_number != 0 AND mimetype =? ");
        ArrayList arrayList = new ArrayList();
        arrayList.add("vnd.android.cursor.item/phone_v2");
        if (!this.f.isEmpty()) {
            z = false;
            for (T t2 : this.f) {
                if (t2 instanceof bt5) {
                    T t3 = t2;
                    if (t3.getContact() != null) {
                        Contact contact = t3.getContact();
                        Integer num = null;
                        if (contact == null) {
                            ee7.a();
                            throw null;
                        } else if (contact.getContactId() >= 0) {
                            if (!z) {
                                sb2.append("AND contact_id IN (");
                                z = true;
                            }
                            sb2.append("?, ");
                            Contact contact2 = t3.getContact();
                            if (contact2 != null) {
                                num = Integer.valueOf(contact2.getContactId());
                            }
                            if (num != null) {
                                this.m.add(num);
                            }
                            arrayList.add(String.valueOf(num));
                        }
                    } else {
                        continue;
                    }
                }
            }
        } else {
            z = false;
        }
        if (z) {
            sb.append((CharSequence) new StringBuilder(sb2.substring(0, sb2.length() - 2)));
            sb.append(")");
        } else {
            sb.append((CharSequence) sb2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, ".Inside onCreateLoader, selectionQuery = " + ((Object) sb));
        String[] strArr = {"contact_id", "display_name", "photo_thumb_uri"};
        PortfolioApp c2 = PortfolioApp.g0.c();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String sb3 = sb.toString();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new ne(c2, uri, strArr, sb3, (String[]) array, null);
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(oe<Cursor> oeVar, Cursor cursor) {
        ee7.b(oeVar, "loader");
        if (cursor != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = v;
            local.d(str, "onLoadFinished, loader id = " + oeVar.getId() + "cursor is closed = " + cursor.isClosed());
            if (cursor.isClosed()) {
                this.n.b(oeVar.getId(), new Bundle(), this);
                return;
            }
            if (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex("display_name"));
                    String string2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                    int i2 = cursor.getInt(cursor.getColumnIndex("contact_id"));
                    this.l.add(Integer.valueOf(i2));
                    a(i2, string, string2);
                } while (cursor.moveToNext());
            }
            v();
            this.o.g(this.f);
            cursor.close();
        }
    }

    @DexIgnore
    public final void a(int i2, String str, String str2) {
        bt5 bt5;
        Contact contact;
        for (Object obj : this.f) {
            if ((obj instanceof bt5) && (contact = (bt5 = (bt5) obj).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = bt5.getContact();
                if (contact2 != null) {
                    contact2.setFirstName(str);
                }
                Contact contact3 = bt5.getContact();
                if (contact3 != null) {
                    contact3.setPhotoThumbUri(str2);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    public final void a(List<bt5> list, List<at5> list2, boolean z) {
        this.u.a(new cm5.b(list, list2, this.p), new e(this, list, list2, z));
    }
}
