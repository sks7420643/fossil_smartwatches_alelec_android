package com.fossil;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t6 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public CharSequence c;
    @DexIgnore
    public ArrayList<String> d;
    @DexIgnore
    public ArrayList<String> e;
    @DexIgnore
    public ArrayList<String> f;
    @DexIgnore
    public ArrayList<Uri> g;

    @DexIgnore
    public t6(Context context, ComponentName componentName) {
        e9.a(context);
        this.a = context;
        Intent action = new Intent().setAction("android.intent.action.SEND");
        this.b = action;
        action.putExtra("androidx.core.app.EXTRA_CALLING_PACKAGE", context.getPackageName());
        this.b.putExtra("android.support.v4.app.EXTRA_CALLING_PACKAGE", context.getPackageName());
        this.b.putExtra("androidx.core.app.EXTRA_CALLING_ACTIVITY", componentName);
        this.b.putExtra("android.support.v4.app.EXTRA_CALLING_ACTIVITY", componentName);
        this.b.addFlags(524288);
    }

    @DexIgnore
    public static t6 a(Activity activity) {
        e9.a(activity);
        return a(activity, activity.getComponentName());
    }

    @DexIgnore
    public Intent b() {
        ArrayList<String> arrayList = this.d;
        if (arrayList != null) {
            a("android.intent.extra.EMAIL", arrayList);
            this.d = null;
        }
        ArrayList<String> arrayList2 = this.e;
        if (arrayList2 != null) {
            a("android.intent.extra.CC", arrayList2);
            this.e = null;
        }
        ArrayList<String> arrayList3 = this.f;
        if (arrayList3 != null) {
            a("android.intent.extra.BCC", arrayList3);
            this.f = null;
        }
        ArrayList<Uri> arrayList4 = this.g;
        boolean z = true;
        if (arrayList4 == null || arrayList4.size() <= 1) {
            z = false;
        }
        boolean equals = "android.intent.action.SEND_MULTIPLE".equals(this.b.getAction());
        if (!z && equals) {
            this.b.setAction("android.intent.action.SEND");
            ArrayList<Uri> arrayList5 = this.g;
            if (arrayList5 == null || arrayList5.isEmpty()) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putExtra("android.intent.extra.STREAM", this.g.get(0));
            }
            this.g = null;
        }
        if (z && !equals) {
            this.b.setAction("android.intent.action.SEND_MULTIPLE");
            ArrayList<Uri> arrayList6 = this.g;
            if (arrayList6 == null || arrayList6.isEmpty()) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putParcelableArrayListExtra("android.intent.extra.STREAM", this.g);
            }
        }
        return this.b;
    }

    @DexIgnore
    public void c() {
        this.a.startActivity(a());
    }

    @DexIgnore
    public static t6 a(Context context, ComponentName componentName) {
        return new t6(context, componentName);
    }

    @DexIgnore
    public final void a(String str, ArrayList<String> arrayList) {
        String[] stringArrayExtra = this.b.getStringArrayExtra(str);
        int length = stringArrayExtra != null ? stringArrayExtra.length : 0;
        String[] strArr = new String[(arrayList.size() + length)];
        arrayList.toArray(strArr);
        if (stringArrayExtra != null) {
            System.arraycopy(stringArrayExtra, 0, strArr, arrayList.size(), length);
        }
        this.b.putExtra(str, strArr);
    }

    @DexIgnore
    public Intent a() {
        return Intent.createChooser(b(), this.c);
    }

    @DexIgnore
    public t6 a(String str) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.add(str);
        return this;
    }

    @DexIgnore
    public t6 b(String str) {
        this.b.setType(str);
        return this;
    }
}
