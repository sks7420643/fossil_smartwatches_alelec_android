package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r15 extends q15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131361980, 1);
        z.put(2131362025, 2);
        z.put(2131363195, 3);
        z.put(2131363193, 4);
        z.put(2131363194, 5);
        z.put(2131363187, 6);
    }
    */

    @DexIgnore
    public r15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 7, y, z));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public r15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RecyclerViewCalendar) objArr[1], (ConstraintLayout) objArr[2], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3]);
        this.x = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.w = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
