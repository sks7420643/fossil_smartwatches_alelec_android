package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s72 extends t72<Status> {
    @DexIgnore
    public s72(a12 a12) {
        super(a12);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public /* synthetic */ i12 a(Status status) {
        return status;
    }
}
