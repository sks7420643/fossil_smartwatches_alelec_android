package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv1 implements Factory<pw1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<sw1> b;
    @DexIgnore
    public /* final */ Provider<dw1> c;
    @DexIgnore
    public /* final */ Provider<by1> d;

    @DexIgnore
    public wv1(Provider<Context> provider, Provider<sw1> provider2, Provider<dw1> provider3, Provider<by1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static wv1 a(Provider<Context> provider, Provider<sw1> provider2, Provider<dw1> provider3, Provider<by1> provider4) {
        return new wv1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static pw1 a(Context context, sw1 sw1, dw1 dw1, by1 by1) {
        pw1 a2 = vv1.a(context, sw1, dw1, by1);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pw1 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
