package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm5 implements Factory<um5> {
    @DexIgnore
    public /* final */ Provider<LocationSource> a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;

    @DexIgnore
    public vm5(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static vm5 a(Provider<LocationSource> provider, Provider<PortfolioApp> provider2) {
        return new vm5(provider, provider2);
    }

    @DexIgnore
    public static um5 a(LocationSource locationSource, PortfolioApp portfolioApp) {
        return new um5(locationSource, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public um5 get() {
        return a(this.a.get(), this.b.get());
    }
}
