package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z86 {
    @DexIgnore
    public /* final */ s86 a;
    @DexIgnore
    public /* final */ k96 b;
    @DexIgnore
    public /* final */ e96 c;

    @DexIgnore
    public z86(s86 s86, k96 k96, e96 e96) {
        ee7.b(s86, "mActivityOverviewDayView");
        ee7.b(k96, "mActivityOverviewWeekView");
        ee7.b(e96, "mActivityOverviewMonthView");
        this.a = s86;
        this.b = k96;
        this.c = e96;
    }

    @DexIgnore
    public final s86 a() {
        return this.a;
    }

    @DexIgnore
    public final e96 b() {
        return this.c;
    }

    @DexIgnore
    public final k96 c() {
        return this.b;
    }
}
