package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vu {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ List<ru> d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    @Deprecated
    public vu(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        this(i, bArr, map, a(map), z, j);
    }

    @DexIgnore
    public static Map<String, String> a(List<ru> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (ru ruVar : list) {
            treeMap.put(ruVar.a(), ruVar.b());
        }
        return treeMap;
    }

    @DexIgnore
    public vu(int i, byte[] bArr, boolean z, long j, List<ru> list) {
        this(i, bArr, a(list), list, z, j);
    }

    @DexIgnore
    @Deprecated
    public vu(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false, 0L);
    }

    @DexIgnore
    public vu(int i, byte[] bArr, Map<String, String> map, List<ru> list, boolean z, long j) {
        this.a = i;
        this.b = bArr;
        this.c = map;
        if (list == null) {
            this.d = null;
        } else {
            this.d = Collections.unmodifiableList(list);
        }
        this.e = z;
    }

    @DexIgnore
    public static List<ru> a(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            arrayList.add(new ru(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }
}
