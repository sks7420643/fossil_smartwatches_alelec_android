package com.fossil;

import android.annotation.SuppressLint;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class a3 extends gb implements View.OnClickListener {
    @DexIgnore
    public int A; // = -1;
    @DexIgnore
    public int B; // = -1;
    @DexIgnore
    public int C; // = -1;
    @DexIgnore
    public int D; // = -1;
    @DexIgnore
    public /* final */ SearchView q;
    @DexIgnore
    public /* final */ SearchableInfo r;
    @DexIgnore
    public /* final */ Context s;
    @DexIgnore
    public /* final */ WeakHashMap<String, Drawable.ConstantState> t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public boolean v; // = false;
    @DexIgnore
    public int w; // = 1;
    @DexIgnore
    public ColorStateList x;
    @DexIgnore
    public int y; // = -1;
    @DexIgnore
    public int z; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ ImageView d;
        @DexIgnore
        public /* final */ ImageView e;

        @DexIgnore
        public a(View view) {
            this.a = (TextView) view.findViewById(16908308);
            this.b = (TextView) view.findViewById(16908309);
            this.c = (ImageView) view.findViewById(16908295);
            this.d = (ImageView) view.findViewById(16908296);
            this.e = (ImageView) view.findViewById(d0.edit_query);
        }
    }

    @DexIgnore
    public a3(Context context, SearchView searchView, SearchableInfo searchableInfo, WeakHashMap<String, Drawable.ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), null, true);
        this.q = searchView;
        this.r = searchableInfo;
        this.u = searchView.getSuggestionCommitIconResId();
        this.s = context;
        this.t = weakHashMap;
    }

    @DexIgnore
    public void a(int i) {
        this.w = i;
    }

    @DexIgnore
    @Override // com.fossil.eb, com.fossil.gb
    public View b(Context context, Cursor cursor, ViewGroup viewGroup) {
        View b = super.b(context, cursor, viewGroup);
        b.setTag(new a(b));
        ((ImageView) b.findViewById(d0.edit_query)).setImageResource(this.u);
        return b;
    }

    @DexIgnore
    public final Drawable c() {
        Drawable b = b(this.r.getSearchActivity());
        if (b != null) {
            return b;
        }
        return ((eb) this).d.getPackageManager().getDefaultActivityIcon();
    }

    @DexIgnore
    public final Drawable d(Cursor cursor) {
        int i = this.B;
        if (i == -1) {
            return null;
        }
        Drawable b = b(cursor.getString(i));
        if (b != null) {
            return b;
        }
        return c();
    }

    @DexIgnore
    public final Drawable e(Cursor cursor) {
        int i = this.C;
        if (i == -1) {
            return null;
        }
        return b(cursor.getString(i));
    }

    @DexIgnore
    public final void f(Cursor cursor) {
        Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    @DexIgnore
    @Override // com.fossil.eb
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View a2 = a(((eb) this).d, ((eb) this).c, viewGroup);
            if (a2 != null) {
                ((a) a2.getTag()).a.setText(e.toString());
            }
            return a2;
        }
    }

    @DexIgnore
    @Override // com.fossil.eb
    public View getView(int i, View view, ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (RuntimeException e) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            View b = b(((eb) this).d, ((eb) this).c, viewGroup);
            if (b != null) {
                ((a) b.getTag()).a.setText(e.toString());
            }
            return b;
        }
    }

    @DexIgnore
    public boolean hasStableIds() {
        return false;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        f(a());
    }

    @DexIgnore
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        f(a());
    }

    @DexIgnore
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.q.onQueryRefine((CharSequence) tag);
        }
    }

    @DexIgnore
    @Override // com.fossil.fb.a
    public Cursor a(CharSequence charSequence) {
        String charSequence2 = charSequence == null ? "" : charSequence.toString();
        if (this.q.getVisibility() == 0 && this.q.getWindowVisibility() == 0) {
            try {
                Cursor a2 = a(this.r, charSequence2, 50);
                if (a2 != null) {
                    a2.getCount();
                    return a2;
                }
            } catch (RuntimeException e) {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
            }
        }
        return null;
    }

    @DexIgnore
    public final CharSequence b(CharSequence charSequence) {
        if (this.x == null) {
            TypedValue typedValue = new TypedValue();
            ((eb) this).d.getTheme().resolveAttribute(y.textColorSearchUrl, typedValue, true);
            this.x = ((eb) this).d.getResources().getColorStateList(typedValue.resourceId);
        }
        SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan(new TextAppearanceSpan(null, 0, 0, this.x, null), 0, charSequence.length(), 33);
        return spannableString;
    }

    @DexIgnore
    @Override // com.fossil.fb.a, com.fossil.eb
    public void a(Cursor cursor) {
        if (this.v) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.a(cursor);
            if (cursor != null) {
                this.y = cursor.getColumnIndex("suggest_text_1");
                this.z = cursor.getColumnIndex("suggest_text_2");
                this.A = cursor.getColumnIndex("suggest_text_2_url");
                this.B = cursor.getColumnIndex("suggest_icon_1");
                this.C = cursor.getColumnIndex("suggest_icon_2");
                this.D = cursor.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    @DexIgnore
    @Override // com.fossil.fb.a, com.fossil.eb
    public CharSequence b(Cursor cursor) {
        String a2;
        String a3;
        if (cursor == null) {
            return null;
        }
        String a4 = a(cursor, "suggest_intent_query");
        if (a4 != null) {
            return a4;
        }
        if (this.r.shouldRewriteQueryFromData() && (a3 = a(cursor, "suggest_intent_data")) != null) {
            return a3;
        }
        if (!this.r.shouldRewriteQueryFromText() || (a2 = a(cursor, "suggest_text_1")) == null) {
            return null;
        }
        return a2;
    }

    @DexIgnore
    public final Drawable b(String str) {
        if (str == null || str.isEmpty() || "0".equals(str)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = "android.resource://" + this.s.getPackageName() + "/" + parseInt;
            Drawable a2 = a(str2);
            if (a2 != null) {
                return a2;
            }
            Drawable c = v6.c(this.s, parseInt);
            a(str2, c);
            return c;
        } catch (NumberFormatException unused) {
            Drawable a3 = a(str);
            if (a3 != null) {
                return a3;
            }
            Drawable a4 = a(Uri.parse(str));
            a(str, a4);
            return a4;
        } catch (Resources.NotFoundException unused2) {
            Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.eb
    public void a(View view, Context context, Cursor cursor) {
        CharSequence charSequence;
        a aVar = (a) view.getTag();
        int i = this.D;
        int i2 = i != -1 ? cursor.getInt(i) : 0;
        if (aVar.a != null) {
            a(aVar.a, a(cursor, this.y));
        }
        if (aVar.b != null) {
            String a2 = a(cursor, this.A);
            if (a2 != null) {
                charSequence = b((CharSequence) a2);
            } else {
                charSequence = a(cursor, this.z);
            }
            if (TextUtils.isEmpty(charSequence)) {
                TextView textView = aVar.a;
                if (textView != null) {
                    textView.setSingleLine(false);
                    aVar.a.setMaxLines(2);
                }
            } else {
                TextView textView2 = aVar.a;
                if (textView2 != null) {
                    textView2.setSingleLine(true);
                    aVar.a.setMaxLines(1);
                }
            }
            a(aVar.b, charSequence);
        }
        ImageView imageView = aVar.c;
        if (imageView != null) {
            a(imageView, d(cursor), 4);
        }
        ImageView imageView2 = aVar.d;
        if (imageView2 != null) {
            a(imageView2, e(cursor), 8);
        }
        int i3 = this.w;
        if (i3 == 2 || (i3 == 1 && (i2 & 1) != 0)) {
            aVar.e.setVisibility(0);
            aVar.e.setTag(aVar.a.getText());
            aVar.e.setOnClickListener(this);
            return;
        }
        aVar.e.setVisibility(8);
    }

    @DexIgnore
    public final Drawable b(ComponentName componentName) {
        String flattenToShortString = componentName.flattenToShortString();
        Drawable.ConstantState constantState = null;
        if (this.t.containsKey(flattenToShortString)) {
            Drawable.ConstantState constantState2 = this.t.get(flattenToShortString);
            if (constantState2 == null) {
                return null;
            }
            return constantState2.newDrawable(this.s.getResources());
        }
        Drawable a2 = a(componentName);
        if (a2 != null) {
            constantState = a2.getConstantState();
        }
        this.t.put(flattenToShortString, constantState);
        return a2;
    }

    @DexIgnore
    public Drawable b(Uri uri) throws FileNotFoundException {
        int i;
        String authority = uri.getAuthority();
        if (!TextUtils.isEmpty(authority)) {
            try {
                Resources resourcesForApplication = ((eb) this).d.getPackageManager().getResourcesForApplication(authority);
                List<String> pathSegments = uri.getPathSegments();
                if (pathSegments != null) {
                    int size = pathSegments.size();
                    if (size == 1) {
                        try {
                            i = Integer.parseInt(pathSegments.get(0));
                        } catch (NumberFormatException unused) {
                            throw new FileNotFoundException("Single path segment is not a resource ID: " + uri);
                        }
                    } else if (size == 2) {
                        i = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
                    } else {
                        throw new FileNotFoundException("More than two path segments: " + uri);
                    }
                    if (i != 0) {
                        return resourcesForApplication.getDrawable(i);
                    }
                    throw new FileNotFoundException("No resource found for: " + uri);
                }
                throw new FileNotFoundException("No path: " + uri);
            } catch (PackageManager.NameNotFoundException unused2) {
                throw new FileNotFoundException("No package found for authority: " + uri);
            }
        } else {
            throw new FileNotFoundException("No authority: " + uri);
        }
    }

    @DexIgnore
    public final void a(TextView textView, CharSequence charSequence) {
        textView.setText(charSequence);
        if (TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    @DexIgnore
    public final void a(ImageView imageView, Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    @DexIgnore
    public final Drawable a(Uri uri) {
        try {
            if ("android.resource".equals(uri.getScheme())) {
                try {
                    return b(uri);
                } catch (Resources.NotFoundException unused) {
                    throw new FileNotFoundException("Resource does not exist: " + uri);
                }
            } else {
                InputStream openInputStream = this.s.getContentResolver().openInputStream(uri);
                if (openInputStream != null) {
                    try {
                        return Drawable.createFromStream(openInputStream, null);
                    } finally {
                        try {
                            openInputStream.close();
                        } catch (IOException e) {
                            Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e);
                        }
                    }
                } else {
                    throw new FileNotFoundException("Failed to open " + uri);
                }
            }
        } catch (FileNotFoundException e2) {
            Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e2.getMessage());
            return null;
        }
    }

    @DexIgnore
    public final Drawable a(String str) {
        Drawable.ConstantState constantState = this.t.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    @DexIgnore
    public final void a(String str, Drawable drawable) {
        if (drawable != null) {
            this.t.put(str, drawable.getConstantState());
        }
    }

    @DexIgnore
    public final Drawable a(ComponentName componentName) {
        PackageManager packageManager = ((eb) this).d.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    @DexIgnore
    public static String a(Cursor cursor, String str) {
        return a(cursor, cursor.getColumnIndex(str));
    }

    @DexIgnore
    public static String a(Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (Exception e) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    @DexIgnore
    public Cursor a(SearchableInfo searchableInfo, String str, int i) {
        String suggestAuthority;
        String[] strArr = null;
        if (searchableInfo == null || (suggestAuthority = searchableInfo.getSuggestAuthority()) == null) {
            return null;
        }
        Uri.Builder fragment = new Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo.getSuggestSelection();
        if (suggestSelection != null) {
            strArr = new String[]{str};
        } else {
            fragment.appendPath(str);
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return ((eb) this).d.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
    }
}
