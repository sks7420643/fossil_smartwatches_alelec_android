package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tu3 extends Drawable implements kv3, q7 {
    @DexIgnore
    public b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Drawable.ConstantState {
        @DexIgnore
        public dv3 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(dv3 dv3) {
            this.a = dv3;
            this.b = false;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public tu3 newDrawable() {
            return new tu3(new b(this));
        }

        @DexIgnore
        public b(b bVar) {
            this.a = (dv3) bVar.a.getConstantState().newDrawable();
            this.b = bVar.b;
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        b bVar = this.a;
        if (bVar.b) {
            bVar.a.draw(canvas);
        }
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @DexIgnore
    public int getOpacity() {
        return this.a.a.getOpacity();
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.a.a.setBounds(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        if (this.a.a.setState(iArr)) {
            onStateChange = true;
        }
        boolean a2 = uu3.a(iArr);
        b bVar = this.a;
        if (bVar.b == a2) {
            return onStateChange;
        }
        bVar.b = a2;
        return true;
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.a.a.setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.a.setColorFilter(colorFilter);
    }

    @DexIgnore
    @Override // com.fossil.kv3
    public void setShapeAppearanceModel(hv3 hv3) {
        this.a.a.setShapeAppearanceModel(hv3);
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTint(int i) {
        this.a.a.setTint(i);
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintList(ColorStateList colorStateList) {
        this.a.a.setTintList(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintMode(PorterDuff.Mode mode) {
        this.a.a.setTintMode(mode);
    }

    @DexIgnore
    public tu3(hv3 hv3) {
        this(new b(new dv3(hv3)));
    }

    @DexIgnore
    public tu3 mutate() {
        this.a = new b(this.a);
        return this;
    }

    @DexIgnore
    public tu3(b bVar) {
        this.a = bVar;
    }
}
