package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.io.ByteArrayOutputStream;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx7 {
    @DexIgnore
    public static /* final */ lx7 a; // = new lx7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends jx7 {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ gd7 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, gd7 gd7, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.e = i;
            this.f = gd7;
        }

        @DexIgnore
        @Override // com.fossil.c50
        public /* bridge */ /* synthetic */ void a(Object obj, f50 f50) {
            a((Bitmap) obj, (f50<? super Bitmap>) f50);
        }

        @DexIgnore
        @Override // com.fossil.c50
        public void c(Drawable drawable) {
            this.f.invoke(null);
        }

        @DexIgnore
        @Override // com.fossil.jx7
        public void a(Bitmap bitmap, f50<? super Bitmap> f50) {
            Bitmap.CompressFormat compressFormat;
            ee7.b(bitmap, "resource");
            super.a(bitmap, f50);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (this.e == 1) {
                compressFormat = Bitmap.CompressFormat.PNG;
            } else {
                compressFormat = Bitmap.CompressFormat.JPEG;
            }
            bitmap.compress(compressFormat, 100, byteArrayOutputStream);
            this.f.invoke(byteArrayOutputStream.toByteArray());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends jx7 {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, nx7 nx7, int i2, int i3, int i4, int i5) {
            super(i4, i5);
            this.e = i;
            this.f = nx7;
        }

        @DexIgnore
        @Override // com.fossil.c50
        public /* bridge */ /* synthetic */ void a(Object obj, f50 f50) {
            a((Bitmap) obj, (f50<? super Bitmap>) f50);
        }

        @DexIgnore
        @Override // com.fossil.c50
        public void c(Drawable drawable) {
            this.f.a(null);
        }

        @DexIgnore
        @Override // com.fossil.jx7
        public void a(Bitmap bitmap, f50<? super Bitmap> f50) {
            Bitmap.CompressFormat compressFormat;
            ee7.b(bitmap, "resource");
            super.a(bitmap, f50);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (this.e == 1) {
                compressFormat = Bitmap.CompressFormat.PNG;
            } else {
                compressFormat = Bitmap.CompressFormat.JPEG;
            }
            bitmap.compress(compressFormat, 100, byteArrayOutputStream);
            this.f.a(byteArrayOutputStream.toByteArray());
        }

        @DexIgnore
        @Override // com.fossil.c50, com.fossil.kx7
        public void a(Drawable drawable) {
            this.f.a(null);
        }
    }

    @DexIgnore
    public final void a(Context context, String str, int i, int i2, int i3, MethodChannel.Result result) {
        ee7.b(context, "ctx");
        ee7.b(str, "path");
        aw.d(context).b().a(new File(str)).a((c50) new b(i3, new nx7(result), i, i2, i, i2));
    }

    @DexIgnore
    public final void a(Context context, Bitmap bitmap, int i, int i2, int i3, gd7<? super byte[], i97> gd7) {
        ee7.b(context, "ctx");
        ee7.b(gd7, Constants.CALLBACK);
        aw.d(context).b().a(bitmap).a((c50) new a(i3, gd7, i, i2, i, i2));
    }
}
