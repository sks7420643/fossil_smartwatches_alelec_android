package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bd5 implements Factory<ad5> {
    @DexIgnore
    public /* final */ Provider<gs6> a;
    @DexIgnore
    public /* final */ Provider<is6> b;
    @DexIgnore
    public /* final */ Provider<ul5> c;
    @DexIgnore
    public /* final */ Provider<rl5> d;

    @DexIgnore
    public bd5(Provider<gs6> provider, Provider<is6> provider2, Provider<ul5> provider3, Provider<rl5> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static bd5 a(Provider<gs6> provider, Provider<is6> provider2, Provider<ul5> provider3, Provider<rl5> provider4) {
        return new bd5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static ad5 a(gs6 gs6, is6 is6, ul5 ul5, rl5 rl5) {
        return new ad5(gs6, is6, ul5, rl5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ad5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
