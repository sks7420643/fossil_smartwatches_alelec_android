package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ig4 extends IOException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -1616151763072450476L;

    @DexIgnore
    public ig4(String str) {
        super(str);
    }

    @DexIgnore
    public static ig4 invalidEndTag() {
        return new ig4("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static ig4 invalidTag() {
        return new ig4("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static ig4 invalidWireType() {
        return new ig4("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static ig4 malformedVarint() {
        return new ig4("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static ig4 negativeSize() {
        return new ig4("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static ig4 recursionLimitExceeded() {
        return new ig4("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    @DexIgnore
    public static ig4 sizeLimitExceeded() {
        return new ig4("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }

    @DexIgnore
    public static ig4 truncatedMessage() {
        return new ig4("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }
}
