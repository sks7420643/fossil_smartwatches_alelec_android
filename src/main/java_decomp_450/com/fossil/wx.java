package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.fossil.ix;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wx implements ix<InputStream> {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ yx b;
    @DexIgnore
    public InputStream c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements xx {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.xx
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements xx {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.xx
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore
    public wx(Uri uri, yx yxVar) {
        this.a = uri;
        this.b = yxVar;
    }

    @DexIgnore
    public static wx a(Context context, Uri uri) {
        return a(context, uri, new a(context.getContentResolver()));
    }

    @DexIgnore
    public static wx b(Context context, Uri uri) {
        return a(context, uri, new b(context.getContentResolver()));
    }

    @DexIgnore
    public final InputStream c() throws FileNotFoundException {
        InputStream c2 = this.b.c(this.a);
        int a2 = c2 != null ? this.b.a(this.a) : -1;
        return a2 != -1 ? new lx(c2, a2) : c2;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.ix
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public static wx a(Context context, Uri uri, xx xxVar) {
        return new wx(uri, new yx(aw.a(context).g().a(), xxVar, aw.a(context).b(), context.getContentResolver()));
    }

    @DexIgnore
    @Override // com.fossil.ix
    public sw b() {
        return sw.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a(ew ewVar, ix.a<? super InputStream> aVar) {
        try {
            InputStream c2 = c();
            this.c = c2;
            aVar.a(c2);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a() {
        InputStream inputStream = this.c;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }
}
