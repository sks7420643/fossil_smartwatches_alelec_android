package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.ShareConstants;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn4 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ List<dn4> a;
    @DexIgnore
    public /* final */ View.OnClickListener b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ iw4 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(iw4 iw4, View view, View.OnClickListener onClickListener) {
            super(view);
            ee7.b(iw4, "binding");
            ee7.b(view, "root");
            ee7.b(onClickListener, "listener");
            this.a = iw4;
            view.setTag(this);
            view.setOnClickListener(onClickListener);
        }

        @DexIgnore
        public final void a(dn4 dn4) {
            ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            iw4 iw4 = this.a;
            FlexibleTextView flexibleTextView = iw4.q;
            ee7.a((Object) flexibleTextView, "ftvName");
            flexibleTextView.setText(dn4.b());
            RTLImageView rTLImageView = iw4.r;
            ee7.a((Object) rTLImageView, "ivCheck");
            rTLImageView.setVisibility(dn4.c() ? 0 : 8);
        }
    }

    @DexIgnore
    public cn4(List<dn4> list, View.OnClickListener onClickListener) {
        ee7.b(list, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        ee7.b(onClickListener, "listener");
        this.a = list;
        this.b = onClickListener;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        aVar.a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        iw4 a2 = iw4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "DialogItemPrivacyLayoutB\u2026tInflater, parent, false)");
        View d = a2.d();
        ee7.a((Object) d, "binding.root");
        return new a(a2, d, this.b);
    }
}
