package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class hw<TranscodeType> extends k40<hw<TranscodeType>> implements Cloneable {
    public final Context F;
    public final iw G;
    public final Class<TranscodeType> H;
    public final cw I;
    public jw<?, ? super TranscodeType> J;
    public Object K;
    public List<q40<TranscodeType>> L;
    public hw<TranscodeType> M;
    public hw<TranscodeType> N;
    public Float O;
    public boolean P = true;
    public boolean Q;
    public boolean R;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|9|10|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0044 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x004e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0058 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0083 */
        /*
        static {
            /*
                com.fossil.ew[] r0 = com.fossil.ew.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.hw.a.b = r0
                r1 = 1
                com.fossil.ew r2 = com.fossil.ew.LOW     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.hw.a.b     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.ew r3 = com.fossil.ew.NORMAL     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                r2 = 3
                int[] r3 = com.fossil.hw.a.b     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.ew r4 = com.fossil.ew.HIGH     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                r3 = 4
                int[] r4 = com.fossil.hw.a.b     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.ew r5 = com.fossil.ew.IMMEDIATE     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                android.widget.ImageView$ScaleType[] r4 = android.widget.ImageView.ScaleType.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.fossil.hw.a.a = r4
                android.widget.ImageView$ScaleType r5 = android.widget.ImageView.ScaleType.CENTER_CROP     // Catch:{ NoSuchFieldError -> 0x0044 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0044 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0044 }
            L_0x0044:
                int[] r1 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x004e }
                android.widget.ImageView$ScaleType r4 = android.widget.ImageView.ScaleType.CENTER_INSIDE     // Catch:{ NoSuchFieldError -> 0x004e }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004e }
                r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x004e }
            L_0x004e:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x0058 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x0058 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0058 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0058 }
            L_0x0058:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x0062 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x006d }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x006d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006d }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006d }
            L_0x006d:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x0078 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x0083 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER     // Catch:{ NoSuchFieldError -> 0x0083 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0083 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0083 }
            L_0x0083:
                int[] r0 = com.fossil.hw.a.a     // Catch:{ NoSuchFieldError -> 0x008f }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.MATRIX     // Catch:{ NoSuchFieldError -> 0x008f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x008f }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x008f }
            L_0x008f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hw.a.<clinit>():void");
        }
        */
    }

    /*
    static {
        r40 r40 = (r40) ((r40) ((r40) new r40().a(iy.b)).a(ew.LOW)).a(true);
    }
    */

    @SuppressLint({"CheckResult"})
    public hw(aw awVar, iw iwVar, Class<TranscodeType> cls, Context context) {
        this.G = iwVar;
        this.H = cls;
        this.F = context;
        this.J = iwVar.b(cls);
        this.I = awVar.f();
        a(iwVar.d());
        a((k40<?>) iwVar.e());
    }

    public m40<TranscodeType> O() {
        return c((int) RecyclerView.UNDEFINED_DURATION, (int) RecyclerView.UNDEFINED_DURATION);
    }

    public hw<TranscodeType> b(q40<TranscodeType> q40) {
        this.L = null;
        return a((q40) q40);
    }

    public m40<TranscodeType> c(int i, int i2) {
        p40 p40 = new p40(i, i2);
        a(p40, p40, p50.a());
        return p40;
    }

    @SuppressLint({"CheckResult"})
    public final void a(List<q40<Object>> list) {
        for (q40<Object> q40 : list) {
            a((q40) q40);
        }
    }

    public final hw<TranscodeType> b(Object obj) {
        this.K = obj;
        this.Q = true;
        return this;
    }

    @Override // com.fossil.k40, com.fossil.k40, java.lang.Object
    public hw<TranscodeType> clone() {
        hw<TranscodeType> hwVar = (hw) super.clone();
        hwVar.J = hwVar.J.clone();
        return hwVar;
    }

    @Override // com.fossil.k40
    public hw<TranscodeType> a(k40<?> k40) {
        u50.a(k40);
        return (hw) super.a(k40);
    }

    public final <Y extends c50<TranscodeType>> Y b(Y y, q40<TranscodeType> q40, k40<?> k40, Executor executor) {
        u50.a(y);
        if (this.Q) {
            n40 a2 = a(y, q40, k40, executor);
            n40 a3 = y.a();
            if (!a2.b(a3) || a(k40, a3)) {
                this.G.a((c50<?>) y);
                y.a(a2);
                this.G.a(y, a2);
                return y;
            }
            u50.a(a3);
            if (!a3.isRunning()) {
                a3.c();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    public hw<TranscodeType> a(q40<TranscodeType> q40) {
        if (q40 != null) {
            if (this.L == null) {
                this.L = new ArrayList();
            }
            this.L.add(q40);
        }
        return this;
    }

    public hw<TranscodeType> a(hw<TranscodeType> hwVar) {
        this.N = hwVar;
        return this;
    }

    public hw<TranscodeType> a(Object obj) {
        b(obj);
        return this;
    }

    public hw<TranscodeType> a(Bitmap bitmap) {
        b(bitmap);
        return a((k40<?>) r40.b(iy.a));
    }

    public hw<TranscodeType> a(String str) {
        b(str);
        return this;
    }

    public hw<TranscodeType> a(Uri uri) {
        b(uri);
        return this;
    }

    public hw<TranscodeType> a(File file) {
        b(file);
        return this;
    }

    public hw<TranscodeType> a(Integer num) {
        b(num);
        return a((k40<?>) r40.b(h50.a(this.F)));
    }

    public <Y extends c50<TranscodeType>> Y a(Y y) {
        a(y, (q40) null, p50.b());
        return y;
    }

    public <Y extends c50<TranscodeType>> Y a(Y y, q40<TranscodeType> q40, Executor executor) {
        b(y, q40, this, executor);
        return y;
    }

    public final ew b(ew ewVar) {
        int i = a.b[ewVar.ordinal()];
        if (i == 1) {
            return ew.NORMAL;
        }
        if (i == 2) {
            return ew.HIGH;
        }
        if (i == 3 || i == 4) {
            return ew.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + t());
    }

    public final boolean a(k40<?> k40, n40 n40) {
        return !k40.B() && n40.f();
    }

    public d50<ImageView, TranscodeType> a(ImageView imageView) {
        k40<?> k40;
        v50.b();
        u50.a(imageView);
        if (!G() && E() && imageView.getScaleType() != null) {
            switch (a.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    k40 = clone().J();
                    break;
                case 2:
                    k40 = clone().K();
                    break;
                case 3:
                case 4:
                case 5:
                    k40 = clone().L();
                    break;
                case 6:
                    k40 = clone().K();
                    break;
            }
            d50<ImageView, TranscodeType> a2 = this.I.a(imageView, this.H);
            b(a2, null, k40, p50.b());
            return a2;
        }
        k40 = this;
        d50<ImageView, TranscodeType> a22 = this.I.a(imageView, this.H);
        b(a22, null, k40, p50.b());
        return a22;
    }

    public final n40 b(Object obj, c50<TranscodeType> c50, q40<TranscodeType> q40, o40 o40, jw<?, ? super TranscodeType> jwVar, ew ewVar, int i, int i2, k40<?> k40, Executor executor) {
        ew ewVar2;
        hw<TranscodeType> hwVar = this.M;
        if (hwVar != null) {
            if (!this.R) {
                jw<?, ? super TranscodeType> jwVar2 = hwVar.P ? jwVar : hwVar.J;
                if (this.M.C()) {
                    ewVar2 = this.M.t();
                } else {
                    ewVar2 = b(ewVar);
                }
                int q = this.M.q();
                int p = this.M.p();
                if (v50.b(i, i2) && !this.M.H()) {
                    q = k40.q();
                    p = k40.p();
                }
                u40 u40 = new u40(obj, o40);
                n40 a2 = a(obj, c50, q40, k40, u40, jwVar, ewVar, i, i2, executor);
                this.R = true;
                hw<TranscodeType> hwVar2 = this.M;
                n40 a3 = hwVar2.a(obj, c50, q40, u40, jwVar2, ewVar2, q, p, hwVar2, executor);
                this.R = false;
                u40.a(a2, a3);
                return u40;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.O == null) {
            return a(obj, c50, q40, k40, o40, jwVar, ewVar, i, i2, executor);
        } else {
            u40 u402 = new u40(obj, o40);
            u402.a(a(obj, c50, q40, k40, u402, jwVar, ewVar, i, i2, executor), a(obj, c50, q40, k40.clone().a(this.O.floatValue()), u402, jwVar, b(ewVar), i, i2, executor));
            return u402;
        }
    }

    public final n40 a(c50<TranscodeType> c50, q40<TranscodeType> q40, k40<?> k40, Executor executor) {
        return a(new Object(), c50, q40, (o40) null, this.J, k40.t(), k40.q(), k40.p(), k40, executor);
    }

    public final n40 a(Object obj, c50<TranscodeType> c50, q40<TranscodeType> q40, o40 o40, jw<?, ? super TranscodeType> jwVar, ew ewVar, int i, int i2, k40<?> k40, Executor executor) {
        l40 l40;
        l40 l402;
        if (this.N != null) {
            l402 = new l40(obj, o40);
            l40 = l402;
        } else {
            l40 = null;
            l402 = o40;
        }
        n40 b = b(obj, c50, q40, l402, jwVar, ewVar, i, i2, k40, executor);
        if (l40 == null) {
            return b;
        }
        int q = this.N.q();
        int p = this.N.p();
        if (v50.b(i, i2) && !this.N.H()) {
            q = k40.q();
            p = k40.p();
        }
        hw<TranscodeType> hwVar = this.N;
        l40.a(b, hwVar.a(obj, c50, q40, l40, hwVar.J, hwVar.t(), q, p, this.N, executor));
        return l40;
    }

    public final n40 a(Object obj, c50<TranscodeType> c50, q40<TranscodeType> q40, k40<?> k40, o40 o40, jw<?, ? super TranscodeType> jwVar, ew ewVar, int i, int i2, Executor executor) {
        Context context = this.F;
        cw cwVar = this.I;
        return t40.a(context, cwVar, obj, this.K, this.H, k40, i, i2, ewVar, c50, q40, this.L, o40, cwVar.d(), jwVar.d(), executor);
    }
}
