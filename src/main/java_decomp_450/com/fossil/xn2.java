package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xn2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle g;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xn2(sn2 sn2, String str, String str2, Bundle bundle) {
        super(sn2);
        this.h = sn2;
        this.e = str;
        this.f = str2;
        this.g = bundle;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.h.h.clearConditionalUserProperty(this.e, this.f, this.g);
    }
}
