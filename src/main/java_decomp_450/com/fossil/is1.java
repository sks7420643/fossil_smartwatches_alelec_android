package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class is1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[is0.values().length];
        a = iArr;
        iArr[is0.INTERRUPTED.ordinal()] = 1;
        a[is0.CONNECTION_DROPPED.ordinal()] = 2;
        a[is0.INCOMPATIBLE_FIRMWARE.ordinal()] = 3;
        a[is0.REQUEST_ERROR.ordinal()] = 4;
        a[is0.LACK_OF_SERVICE.ordinal()] = 5;
        a[is0.LACK_OF_CHARACTERISTIC.ordinal()] = 6;
        a[is0.INVALID_SERIAL_NUMBER.ordinal()] = 7;
        a[is0.DATA_TRANSFER_RETRY_REACH_THRESHOLD.ordinal()] = 8;
        a[is0.EXCHANGED_VALUE_NOT_SATISFIED.ordinal()] = 9;
        a[is0.INVALID_FILE_LENGTH.ordinal()] = 10;
        a[is0.MISMATCH_VERSION.ordinal()] = 11;
        a[is0.INVALID_FILE_CRC.ordinal()] = 12;
        a[is0.INVALID_RESPONSE.ordinal()] = 13;
        a[is0.INVALID_DATA_LENGTH.ordinal()] = 14;
        a[is0.NOT_ENOUGH_FILE_TO_PROCESS.ordinal()] = 15;
        a[is0.WAITING_FOR_EXECUTION_TIMEOUT.ordinal()] = 16;
        a[is0.REQUEST_UNSUPPORTED.ordinal()] = 17;
        a[is0.UNSUPPORTED_FILE_HANDLE.ordinal()] = 18;
        a[is0.BLUETOOTH_OFF.ordinal()] = 19;
        a[is0.AUTHENTICATION_FAILED.ordinal()] = 20;
        a[is0.WRONG_RANDOM_NUMBER.ordinal()] = 21;
        a[is0.SECRET_KEY_IS_REQUIRED.ordinal()] = 22;
        a[is0.INVALID_PARAMETER.ordinal()] = 23;
        a[is0.HID_INPUT_DEVICE_DISABLED.ordinal()] = 24;
        a[is0.NOT_ALLOW_TO_START.ordinal()] = 25;
        a[is0.UNSUPPORTED_FORMAT.ordinal()] = 26;
        a[is0.TARGET_FIRMWARE_NOT_MATCHED.ordinal()] = 27;
        a[is0.DATABASE_ERROR.ordinal()] = 28;
        a[is0.FLOW_BROKEN.ordinal()] = 29;
        a[is0.SUCCESS.ordinal()] = 30;
        a[is0.NOT_START.ordinal()] = 31;
    }
    */
}
