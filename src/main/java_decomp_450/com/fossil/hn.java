package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hn {
    @DexIgnore
    public static /* final */ String a; // = im.a("Alarms");

    @DexIgnore
    public static void a(Context context, dn dnVar, String str, long j) {
        WorkDatabase f = dnVar.f();
        ro c = f.c();
        qo a2 = c.a(str);
        if (a2 != null) {
            a(context, str, a2.b);
            a(context, str, a2.b, j);
            return;
        }
        int a3 = new ip(f).a();
        c.a(new qo(str, a3));
        a(context, str, a3, j);
    }

    @DexIgnore
    public static void a(Context context, dn dnVar, String str) {
        ro c = dnVar.f().c();
        qo a2 = c.a(str);
        if (a2 != null) {
            a(context, str, a2.b);
            im.a().a(a, String.format("Removing SystemIdInfo for workSpecId (%s)", str), new Throwable[0]);
            c.b(str);
        }
    }

    @DexIgnore
    public static void a(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, in.a(context, str), 536870912);
        if (service != null && alarmManager != null) {
            im.a().a(a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i)), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    @DexIgnore
    public static void a(Context context, String str, int i, long j) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Alarm.TABLE_NAME);
        PendingIntent service = PendingIntent.getService(context, i, in.a(context, str), 134217728);
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }
}
