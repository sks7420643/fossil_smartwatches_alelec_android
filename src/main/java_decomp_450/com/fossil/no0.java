package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class no0 extends zk1 {
    @DexIgnore
    public no0(ri1 ri1, en0 en0, wm0 wm0, HashMap<xf0, Object> hashMap, String str) {
        super(ri1, en0, wm0, gq0.b.a(ri1.u, pb1.DATA_COLLECTION_FILE), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 32);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public JSONObject k() {
        JSONArray jSONArray = new JSONArray();
        Iterator<bi1> it = ((zk1) this).H.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().a(false));
        }
        JSONObject put = super.k().put(yz0.a(xf0.SKIP_ERASE), ((zk1) this).P);
        ee7.a((Object) put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        return yz0.a(put, r51.N2, jSONArray);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public byte[][] d() {
        ArrayList<bi1> arrayList = ((zk1) this).H;
        ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(((bi1) it.next()).e);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
