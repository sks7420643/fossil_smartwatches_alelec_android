package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p9 {
    @DexIgnore
    public static MenuItem a(MenuItem menuItem, h9 h9Var) {
        if (menuItem instanceof w7) {
            return ((w7) menuItem).a(h9Var);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    @DexIgnore
    public static void b(MenuItem menuItem, CharSequence charSequence) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setTooltipText(charSequence);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setTooltipText(charSequence);
        }
    }

    @DexIgnore
    public static void a(MenuItem menuItem, CharSequence charSequence) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setContentDescription(charSequence);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setContentDescription(charSequence);
        }
    }

    @DexIgnore
    public static void b(MenuItem menuItem, char c, int i) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setNumericShortcut(c, i);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setNumericShortcut(c, i);
        }
    }

    @DexIgnore
    public static void a(MenuItem menuItem, char c, int i) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setAlphabeticShortcut(c, i);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setAlphabeticShortcut(c, i);
        }
    }

    @DexIgnore
    public static void a(MenuItem menuItem, ColorStateList colorStateList) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setIconTintList(colorStateList);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintList(colorStateList);
        }
    }

    @DexIgnore
    public static void a(MenuItem menuItem, PorterDuff.Mode mode) {
        if (menuItem instanceof w7) {
            ((w7) menuItem).setIconTintMode(mode);
        } else if (Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintMode(mode);
        }
    }
}
