package com.fossil;

import com.fossil.fl4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds6 implements fl4.a {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public ds6(int i, String str) {
        ee7.b(str, "errorMessage");
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }
}
