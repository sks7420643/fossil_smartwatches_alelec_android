package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import com.facebook.appevents.internal.InAppPurchaseEventManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static zg5 b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(zg5 zg5) {
            zg5.b = zg5;
        }

        @DexIgnore
        public final zg5 b() {
            return zg5.b;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final synchronized zg5 a() {
            zg5 b;
            if (zg5.c.b() == null) {
                zg5.c.a(new zg5(null));
            }
            b = zg5.c.b();
            if (b == null) {
                ee7.a();
                throw null;
            }
            return b;
        }
    }

    /*
    static {
        String simpleName = zg5.class.getSimpleName();
        ee7.a((Object) simpleName, "PhoneCallManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public zg5() {
    }

    @DexIgnore
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void b() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.g0.c().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            ee7.a((Object) cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class<?> cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            ee7.a((Object) cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            ee7.a((Object) cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", String.class);
            ee7.a((Object) method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
            ee7.a((Object) method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface(null, "fake");
            Object invoke = method.invoke(method2.invoke(null, binder), PlaceFields.PHONE);
            if (invoke != null) {
                Method method3 = cls2.getMethod(InAppPurchaseEventManager.AS_INTERFACE, IBinder.class);
                ee7.a((Object) method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke(null, (IBinder) invoke);
                Method method4 = cls.getMethod("endCall", new Class[0]);
                ee7.a((Object) method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new x87("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when decline call " + e2);
        }
    }

    @DexIgnore
    public /* synthetic */ zg5(zd7 zd7) {
        this();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        try {
            Object systemService = PortfolioApp.g0.c().getSystemService("telecom");
            if (systemService != null) {
                ((TelecomManager) systemService).acceptRingingCall();
                return;
            }
            throw new x87("null cannot be cast to non-null type android.telecom.TelecomManager");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "exception when accept call call " + e);
        }
    }

    @DexIgnore
    public final void a(String str, String str2, BroadcastReceiver broadcastReceiver) {
        ee7.b(str, "number");
        ee7.b(str2, "message");
        ee7.b(broadcastReceiver, "receiver");
        we7 we7 = we7.a;
        String format = String.format("DELIVERY_%s", Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h()}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a;
        local.d(str3, "sendSMSTo with " + format);
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.g0.c(), 0, new Intent(format), 0);
        PortfolioApp.g0.c().registerReceiver(broadcastReceiver, new IntentFilter(format));
        if (xg5.b.a(PortfolioApp.g0.c().getBaseContext(), xg5.c.SEND_SMS)) {
            SmsManager smsManager = SmsManager.getDefault();
            if (smsManager != null) {
                smsManager.sendTextMessage(str, null, str2, broadcast, null);
                return;
            }
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("SEND_SMS_PERMISSION_REQUIRED", true);
        broadcastReceiver.onReceive(PortfolioApp.g0.c().getBaseContext(), intent);
    }
}
