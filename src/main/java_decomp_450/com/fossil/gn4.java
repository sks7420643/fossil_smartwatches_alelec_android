package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn4 {
    @DexIgnore
    @te4("challengeId")
    public String a;
    @DexIgnore
    @te4("challengeName")
    public String b;
    @DexIgnore
    @te4("players")
    public List<jn4> c;

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final List<jn4> c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof gn4)) {
            return false;
        }
        gn4 gn4 = (gn4) obj;
        return ee7.a(this.a, gn4.a) && ee7.a(this.b, gn4.b) && ee7.a(this.c, gn4.c);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<jn4> list = this.c;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "BCChallengePlayer(challengeId=" + this.a + ", challengeName=" + this.b + ", players=" + this.c + ")";
    }
}
