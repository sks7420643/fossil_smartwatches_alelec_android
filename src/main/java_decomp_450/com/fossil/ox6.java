package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ox6 {
    @DexIgnore
    public static ox6 a;

    @DexIgnore
    public static ox6 a() {
        if (a == null) {
            a = new ox6();
        }
        return a;
    }

    @DexIgnore
    public List<ContactGroup> b(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingSms = ah5.p.a().b().getContactGroupsMatchingSms(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingSms == null ? new ArrayList() : contactGroupsMatchingSms;
    }

    @DexIgnore
    public List<ContactGroup> a(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingIncomingCall = ah5.p.a().b().getContactGroupsMatchingIncomingCall(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingIncomingCall == null ? new ArrayList() : contactGroupsMatchingIncomingCall;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0014 A[LOOP:0: B:3:0x0014->B:16:0x0014, LOOP_END, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.wearables.fsl.contact.Contact a(java.lang.String r4, int r5) {
        /*
            r3 = this;
            com.fossil.ah5$a r0 = com.fossil.ah5.p
            com.fossil.ah5 r0 = r0.a()
            com.fossil.wearables.fsl.contact.ContactProvider r0 = r0.b()
            java.util.List r5 = r0.getAllContactGroups(r5)
            if (r5 == 0) goto L_0x0045
            java.util.Iterator r5 = r5.iterator()
        L_0x0014:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0045
            java.lang.Object r0 = r5.next()
            com.fossil.wearables.fsl.contact.ContactGroup r0 = (com.fossil.wearables.fsl.contact.ContactGroup) r0
            java.util.List r0 = r0.getContacts()
            java.util.Iterator r0 = r0.iterator()
        L_0x0028:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0014
            java.lang.Object r1 = r0.next()
            com.fossil.wearables.fsl.contact.Contact r1 = (com.fossil.wearables.fsl.contact.Contact) r1
            java.lang.String r2 = r1.getFirstName()
            boolean r2 = android.text.TextUtils.equals(r4, r2)
            if (r2 != 0) goto L_0x0044
            boolean r2 = r1.containsPhoneNumber(r4)
            if (r2 == 0) goto L_0x0028
        L_0x0044:
            return r1
        L_0x0045:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ox6.a(java.lang.String, int):com.fossil.wearables.fsl.contact.Contact");
    }
}
