package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public po2(sn2 sn2, String str, String str2, Object obj, boolean z) {
        super(sn2);
        this.i = sn2;
        this.e = str;
        this.f = str2;
        this.g = obj;
        this.h = z;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.i.h.setUserProperty(this.e, this.f, cb2.a(this.g), this.h, ((sn2.a) this).a);
    }
}
