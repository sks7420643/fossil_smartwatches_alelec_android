package com.fossil;

import androidx.lifecycle.ViewModelProvider;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rj4 implements ViewModelProvider.Factory {
    @DexIgnore
    public /* final */ Map<Class<? extends he>, Provider<he>> a;

    @DexIgnore
    public rj4(Map<Class<? extends he>, Provider<he>> map) {
        ee7.b(map, "creators");
        this.a = map;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.Factory
    public <T extends he> T create(Class<T> cls) {
        T t;
        ee7.b(cls, "modelClass");
        Provider<he> provider = this.a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (cls.isAssignableFrom((Class) t.getKey())) {
                    break;
                }
            }
            T t2 = t;
            provider = t2 != null ? (Provider) t2.getValue() : null;
        }
        if (provider != null) {
            try {
                he heVar = provider.get();
                if (heVar != null) {
                    return (T) heVar;
                }
                throw new x87("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
