package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ xg0[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public wg0 createFromParcel(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(xg0.class.getClassLoader());
            if (readParcelableArray != null) {
                return new wg0((xg0[]) readParcelableArray);
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.preset.DevicePresetItem>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public wg0[] newArray(int i) {
            return new wg0[i];
        }
    }

    @DexIgnore
    public wg0(xg0[] xg0Arr) {
        this.a = xg0Arr;
        b();
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (xg0 xg0 : this.a) {
            jSONArray.put(xg0.a());
        }
        return yz0.a(new JSONObject(), r51.e3, jSONArray);
    }

    @DexIgnore
    public final void b() {
        ArrayList arrayList = new ArrayList();
        for (t70 t70 : s97.a(this.a, t70.class)) {
            arrayList.addAll(t70.d());
        }
        xg0[] xg0Arr = this.a;
        for (xg0 xg0 : xg0Arr) {
            if (xg0 instanceof h70) {
                ba7.a((List) ((h70) xg0).d(), (gd7) new m71(arrayList));
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final xg0[] getPresetItems() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelableArray(this.a, i);
        }
    }
}
