package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ cw6 d;
    @DexIgnore
    public /* final */ DeviceRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            ee7.b(str, "deviceId");
            ee7.b(str2, ButtonService.USER_ID);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMesagge");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.GetSecretKeyUseCase", f = "GetSecretKeyUseCase.kt", l = {23, 29}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ fw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(fw6 fw6, fb7 fb7) {
            super(fb7);
            this.this$0 = fw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public fw6(cw6 cw6, DeviceRepository deviceRepository) {
        ee7.b(cw6, "mEncryptValueKeyStoreUseCase");
        ee7.b(deviceRepository, "mDeviceRepository");
        this.d = cw6;
        this.e = deviceRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetSecretKeyUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.fw6.b r11, com.fossil.fb7<java.lang.Object> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.fw6.e
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.fw6$e r0 = (com.fossil.fw6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.fw6$e r0 = new com.fossil.fw6$e
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x0056
            if (r2 == r5) goto L_0x004a
            if (r2 != r4) goto L_0x0042
            java.lang.Object r11 = r0.L$4
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$3
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r1 = r0.L$2
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.fw6$b r1 = (com.fossil.fw6.b) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.fw6 r0 = (com.fossil.fw6) r0
            com.fossil.t87.a(r12)
            goto L_0x00ec
        L_0x0042:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x004a:
            java.lang.Object r11 = r0.L$1
            com.fossil.fw6$b r11 = (com.fossil.fw6.b) r11
            java.lang.Object r2 = r0.L$0
            com.fossil.fw6 r2 = (com.fossil.fw6) r2
            com.fossil.t87.a(r12)
            goto L_0x006f
        L_0x0056:
            com.fossil.t87.a(r12)
            com.portfolio.platform.data.source.DeviceRepository r12 = r10.e
            if (r11 == 0) goto L_0x0146
            java.lang.String r2 = r11.a()
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r5
            java.lang.Object r12 = r12.getDeviceSecretKey(r2, r0)
            if (r12 != r1) goto L_0x006e
            return r1
        L_0x006e:
            r2 = r10
        L_0x006f:
            com.fossil.zi5 r12 = (com.fossil.zi5) r12
            boolean r5 = r12 instanceof com.fossil.bj5
            java.lang.String r6 = "GetSecretKeyUseCase"
            if (r5 == 0) goto L_0x010e
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "success to get server secret key "
            r7.append(r8)
            r8 = r12
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r9 = r8.a()
            java.lang.String r9 = (java.lang.String) r9
            r7.append(r9)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            java.lang.Object r5 = r8.a()
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 != 0) goto L_0x0105
            java.lang.Object r5 = r8.a()
            if (r5 == 0) goto L_0x0101
            r3 = r5
            java.lang.String r3 = (java.lang.String) r3
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r11.b()
            r5.append(r6)
            r6 = 58
            r5.append(r6)
            java.lang.String r6 = r11.a()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.fossil.cw6 r6 = r2.d
            com.fossil.cw6$b r7 = new com.fossil.cw6$b
            com.fossil.pb5 r8 = new com.fossil.pb5
            r8.<init>()
            r7.<init>(r5, r8, r3)
            r0.L$0 = r2
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r3
            r0.L$4 = r5
            r0.label = r4
            java.lang.Object r11 = com.fossil.gl4.a(r6, r7, r0)
            if (r11 != r1) goto L_0x00ea
            return r1
        L_0x00ea:
            r0 = r2
            r11 = r3
        L_0x00ec:
            com.portfolio.platform.PortfolioApp$a r12 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r12 = r12.c()
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r1 = r1.c()
            r12.e(r11, r1)
            r2 = r0
            goto L_0x0105
        L_0x0101:
            com.fossil.ee7.a()
            throw r3
        L_0x0105:
            com.fossil.fw6$d r11 = new com.fossil.fw6$d
            r11.<init>()
            r2.a(r11)
            goto L_0x0140
        L_0x010e:
            boolean r11 = r12 instanceof com.fossil.yi5
            if (r11 == 0) goto L_0x0140
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "fail to get server secret key "
            r0.append(r1)
            com.fossil.yi5 r12 = (com.fossil.yi5) r12
            int r1 = r12.a()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r11.d(r6, r0)
            com.fossil.fw6$c r11 = new com.fossil.fw6$c
            int r12 = r12.a()
            java.lang.String r0 = ""
            r11.<init>(r12, r0)
            r2.a(r11)
        L_0x0140:
            java.lang.Object r11 = new java.lang.Object
            r11.<init>()
            return r11
        L_0x0146:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fw6.a(com.fossil.fw6$b, com.fossil.fb7):java.lang.Object");
    }
}
