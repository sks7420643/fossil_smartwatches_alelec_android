package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.ws3;
import com.google.android.material.internal.CheckableImageButton;
import java.util.Iterator;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et3<S> extends ac {
    @DexIgnore
    public static /* final */ Object v; // = "CONFIRM_BUTTON_TAG";
    @DexIgnore
    public static /* final */ Object w; // = "CANCEL_BUTTON_TAG";
    @DexIgnore
    public static /* final */ Object x; // = "TOGGLE_BUTTON_TAG";
    @DexIgnore
    public /* final */ LinkedHashSet<ft3<? super S>> a; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<View.OnClickListener> b; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<DialogInterface.OnCancelListener> c; // = new LinkedHashSet<>();
    @DexIgnore
    public /* final */ LinkedHashSet<DialogInterface.OnDismissListener> d; // = new LinkedHashSet<>();
    @DexIgnore
    public int e;
    @DexIgnore
    public zs3<S> f;
    @DexIgnore
    public lt3<S> g;
    @DexIgnore
    public ws3 h;
    @DexIgnore
    public dt3<S> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public CharSequence p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public TextView r;
    @DexIgnore
    public CheckableImageButton s;
    @DexIgnore
    public dv3 t;
    @DexIgnore
    public Button u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.ft3 */
        /* JADX WARN: Multi-variable type inference failed */
        public void onClick(View view) {
            Iterator it = et3.this.a.iterator();
            while (it.hasNext()) {
                ((ft3) it.next()).a(et3.this.a1());
            }
            et3.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onClick(View view) {
            Iterator it = et3.this.b.iterator();
            while (it.hasNext()) {
                ((View.OnClickListener) it.next()).onClick(view);
            }
            et3.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements kt3<S> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.kt3
        public void a(S s) {
            et3.this.c1();
            if (et3.this.f.o()) {
                et3.this.u.setEnabled(true);
            } else {
                et3.this.u.setEnabled(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            et3.this.s.toggle();
            et3 et3 = et3.this;
            et3.a(et3.s);
            et3.this.b1();
        }
    }

    @DexIgnore
    public static long d1() {
        return ht3.d().g;
    }

    @DexIgnore
    public String Z0() {
        return this.f.a(getContext());
    }

    @DexIgnore
    public final S a1() {
        return this.f.r();
    }

    @DexIgnore
    public final void b1() {
        this.i = dt3.a(this.f, a(requireContext()), this.h);
        this.g = this.s.isChecked() ? gt3.a(this.f, this.h) : this.i;
        c1();
        nc b2 = getChildFragmentManager().b();
        b2.a(nr3.mtrl_calendar_frame, this.g);
        b2.c();
        this.g.a(new c());
    }

    @DexIgnore
    public final void c1() {
        String Z0 = Z0();
        this.r.setContentDescription(String.format(getString(rr3.mtrl_picker_announce_current_selection), Z0));
        this.r.setText(Z0);
    }

    @DexIgnore
    @Override // com.fossil.ac
    public final void onCancel(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnCancelListener> it = this.c.iterator();
        while (it.hasNext()) {
            it.next().onCancel(dialogInterface);
        }
        super.onCancel(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.e = bundle.getInt("OVERRIDE_THEME_RES_ID");
        this.f = (zs3) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.h = (ws3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.j = bundle.getInt("TITLE_TEXT_RES_ID_KEY");
        this.p = bundle.getCharSequence("TITLE_TEXT_KEY");
    }

    @DexIgnore
    @Override // com.fossil.ac
    public final Dialog onCreateDialog(Bundle bundle) {
        Dialog dialog = new Dialog(requireContext(), a(requireContext()));
        Context context = dialog.getContext();
        this.q = f(context);
        int a2 = ou3.a(context, jr3.colorSurface, et3.class.getCanonicalName());
        dv3 dv3 = new dv3(context, null, jr3.materialCalendarStyle, sr3.Widget_MaterialComponents_MaterialCalendar);
        this.t = dv3;
        dv3.a(context);
        this.t.a(ColorStateList.valueOf(a2));
        this.t.b(da.l(dialog.getWindow().getDecorView()));
        return dialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(this.q ? pr3.mtrl_picker_fullscreen : pr3.mtrl_picker_dialog, viewGroup);
        Context context = inflate.getContext();
        if (this.q) {
            inflate.findViewById(nr3.mtrl_calendar_frame).setLayoutParams(new LinearLayout.LayoutParams(e(context), -2));
        } else {
            View findViewById = inflate.findViewById(nr3.mtrl_calendar_main_pane);
            View findViewById2 = inflate.findViewById(nr3.mtrl_calendar_frame);
            findViewById.setLayoutParams(new LinearLayout.LayoutParams(e(context), -1));
            findViewById2.setMinimumHeight(d(requireContext()));
        }
        TextView textView = (TextView) inflate.findViewById(nr3.mtrl_picker_header_selection_text);
        this.r = textView;
        da.g(textView, 1);
        this.s = (CheckableImageButton) inflate.findViewById(nr3.mtrl_picker_header_toggle);
        TextView textView2 = (TextView) inflate.findViewById(nr3.mtrl_picker_title_text);
        CharSequence charSequence = this.p;
        if (charSequence != null) {
            textView2.setText(charSequence);
        } else {
            textView2.setText(this.j);
        }
        b(context);
        this.u = (Button) inflate.findViewById(nr3.confirm_button);
        if (this.f.o()) {
            this.u.setEnabled(true);
        } else {
            this.u.setEnabled(false);
        }
        this.u.setTag(v);
        this.u.setOnClickListener(new a());
        Button button = (Button) inflate.findViewById(nr3.cancel_button);
        button.setTag(w);
        button.setOnClickListener(new b());
        return inflate;
    }

    @DexIgnore
    @Override // com.fossil.ac
    public final void onDismiss(DialogInterface dialogInterface) {
        Iterator<DialogInterface.OnDismissListener> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().onDismiss(dialogInterface);
        }
        ViewGroup viewGroup = (ViewGroup) getView();
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("OVERRIDE_THEME_RES_ID", this.e);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.f);
        ws3.b bVar = new ws3.b(this.h);
        if (this.i.d1() != null) {
            bVar.a(this.i.d1().g);
        }
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", bVar.a());
        bundle.putInt("TITLE_TEXT_RES_ID_KEY", this.j);
        bundle.putCharSequence("TITLE_TEXT_KEY", this.p);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onStart() {
        super.onStart();
        Window window = requireDialog().getWindow();
        if (this.q) {
            window.setLayout(-1, -1);
            window.setBackgroundDrawable(this.t);
        } else {
            window.setLayout(-2, -2);
            int dimensionPixelOffset = getResources().getDimensionPixelOffset(lr3.mtrl_calendar_dialog_background_inset);
            Rect rect = new Rect(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset);
            window.setBackgroundDrawable(new InsetDrawable((Drawable) this.t, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset));
            window.getDecorView().setOnTouchListener(new pt3(requireDialog(), rect));
        }
        b1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onStop() {
        this.g.Z0();
        super.onStop();
    }

    @DexIgnore
    public static Drawable c(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842912}, t0.c(context, mr3.ic_calendar_black_24dp));
        stateListDrawable.addState(new int[0], t0.c(context, mr3.ic_edit_black_24dp));
        return stateListDrawable;
    }

    @DexIgnore
    public static int d(Context context) {
        Resources resources = context.getResources();
        int dimensionPixelSize = resources.getDimensionPixelSize(lr3.mtrl_calendar_navigation_height) + resources.getDimensionPixelOffset(lr3.mtrl_calendar_navigation_top_padding) + resources.getDimensionPixelOffset(lr3.mtrl_calendar_navigation_bottom_padding);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(lr3.mtrl_calendar_days_of_week_height);
        return dimensionPixelSize + dimensionPixelSize2 + (it3.e * resources.getDimensionPixelSize(lr3.mtrl_calendar_day_height)) + ((it3.e - 1) * resources.getDimensionPixelOffset(lr3.mtrl_calendar_month_vertical_padding)) + resources.getDimensionPixelOffset(lr3.mtrl_calendar_bottom_padding);
    }

    @DexIgnore
    public static int e(Context context) {
        Resources resources = context.getResources();
        int dimensionPixelOffset = resources.getDimensionPixelOffset(lr3.mtrl_calendar_content_padding);
        int i2 = ht3.d().e;
        return (dimensionPixelOffset * 2) + (resources.getDimensionPixelSize(lr3.mtrl_calendar_day_width) * i2) + ((i2 - 1) * resources.getDimensionPixelOffset(lr3.mtrl_calendar_month_horizontal_padding));
    }

    @DexIgnore
    public static boolean f(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(ou3.a(context, jr3.materialCalendarStyle, dt3.class.getCanonicalName()), new int[]{16843277});
        boolean z = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        return z;
    }

    @DexIgnore
    public final void b(Context context) {
        this.s.setTag(x);
        this.s.setImageDrawable(c(context));
        da.a(this.s, (g9) null);
        a(this.s);
        this.s.setOnClickListener(new d());
    }

    @DexIgnore
    public final int a(Context context) {
        int i2 = this.e;
        if (i2 != 0) {
            return i2;
        }
        return this.f.b(context);
    }

    @DexIgnore
    public final void a(CheckableImageButton checkableImageButton) {
        String str;
        if (this.s.isChecked()) {
            str = checkableImageButton.getContext().getString(rr3.mtrl_picker_toggle_to_calendar_input_mode);
        } else {
            str = checkableImageButton.getContext().getString(rr3.mtrl_picker_toggle_to_text_input_mode);
        }
        this.s.setContentDescription(str);
    }
}
