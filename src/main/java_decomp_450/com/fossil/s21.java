package com.fossil;

import com.fossil.l60;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s21 extends zk0 {
    @DexIgnore
    public static /* final */ ArrayList<qk1> Q; // = w97.a((Object[]) new qk1[]{qk1.DC, qk1.FTC, qk1.FTD, qk1.AUTHENTICATION, qk1.ASYNC, qk1.FTD_1});
    @DexIgnore
    public m60 C;
    @DexIgnore
    public /* final */ ArrayList<ul0> D; // = new ArrayList<>();
    @DexIgnore
    public long E; // = System.currentTimeMillis();
    @DexIgnore
    public long F;
    @DexIgnore
    public int G;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<qk1> H;
    @DexIgnore
    public int I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ HashMap<yo1, Object> P;

    @DexIgnore
    public s21(ri1 ri1, en0 en0, HashMap<yo1, Object> hashMap, String str) {
        super(ri1, en0, wm0.b, str, false, 16);
        this.P = hashMap;
        this.C = new m60(ri1.d(), ri1.u, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        v97.a(b21.x.t());
        w97.c(qk1.MODEL_NUMBER, qk1.SERIAL_NUMBER, qk1.FIRMWARE_VERSION, qk1.DC, qk1.FTC, qk1.FTD, qk1.AUTHENTICATION, qk1.ASYNC);
        this.H = new CopyOnWriteArrayList<>();
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean a(v81 v81) {
        qa1 qa1 = v81 != null ? v81.x : null;
        if (qa1 != null && xq1.c[qa1.ordinal()] == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        en0 en0 = ((zk0) this).x;
        if (en0.a.u == l60.c.CONNECTED) {
            this.C = en0.a();
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
            return;
        }
        this.E = System.currentTimeMillis();
        this.M = true;
        q();
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(yz0.a(super.i(), r51.m1, yz0.a(((zk0) this).w.getBondState())), r51.e5, Integer.valueOf(((zk0) this).w.x.getType()));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(yz0.a(yz0.a(super.k(), r51.i2, Long.valueOf(this.F)), r51.j2, Integer.valueOf(this.N)), r51.k2, Integer.valueOf(this.O));
    }

    @DexIgnore
    public final void m() {
        zk0.a(this, new sn1(pb1.ALL_FILE.a, ((zk0) this).w, 0, 4), vs1.a, yi0.a, (kd7) null, new wk0(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    public final void n() {
        zk0.a(this, new oc1(((zk0) this).w), new da1(this), xb1.a, (kd7) null, new rd1(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    public final void o() {
        zk0.a(this, new vj1(zs1.b.b(), ((zk0) this).w), an1.a, zo1.a, (kd7) null, new yq1(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    public final void p() {
        int i;
        le0 le0 = le0.DEBUG;
        if (this.I >= this.H.size()) {
            Hashtable hashtable = new Hashtable();
            hashtable.put(ul0.DEVICE_INFORMATION, Integer.MAX_VALUE);
            for (T t : this.H) {
                ul0 a = t.a();
                switch (si1.b[t.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        i = Integer.MAX_VALUE;
                        break;
                    case 8:
                    case 9:
                    case 10:
                        i = 1;
                        break;
                    default:
                        i = 0;
                        break;
                }
                Integer num = (Integer) hashtable.get(a);
                if (num == null) {
                    num = 0;
                }
                ee7.a((Object) num, "resourceQuotas[resourceType] ?: 0");
                hashtable.put(a, Integer.valueOf(num.intValue() + i));
            }
            ((zk0) this).x.b().a(new wj0(hashtable, null));
            zk0.a(this, new aj1(((zk0) this).w, ((zk0) this).x, ((zk0) this).z), new lf1(this), new hh1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
            return;
        }
        qk1 qk1 = this.H.get(this.I);
        ee7.a((Object) qk1, "characteristicsToSubscri\u2026ibingCharacteristicIndex]");
        zk0.a(this, new on1(qk1, true, ((zk0) this).w), new ws1(this), new zi0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void q() {
        /*
            r13 = this;
            boolean r0 = r13.L
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x001d
            java.util.HashMap<com.fossil.yo1, java.lang.Object> r0 = r13.P
            com.fossil.yo1 r3 = com.fossil.yo1.AUTO_CONNECT
            java.lang.Object r0 = r0.get(r3)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0017
            boolean r0 = r0.booleanValue()
            goto L_0x0018
        L_0x0017:
            r0 = 0
        L_0x0018:
            if (r0 == 0) goto L_0x001b
            goto L_0x001d
        L_0x001b:
            r0 = 0
            goto L_0x001e
        L_0x001d:
            r0 = 1
        L_0x001e:
            r13.L = r2
            java.util.HashMap<com.fossil.yo1, java.lang.Object> r3 = r13.P
            com.fossil.yo1 r4 = com.fossil.yo1.CONNECTION_TIME_OUT
            java.lang.Object r3 = r3.get(r4)
            java.lang.Long r3 = (java.lang.Long) r3
            if (r3 == 0) goto L_0x0031
            long r3 = r3.longValue()
            goto L_0x0033
        L_0x0031:
            r3 = 30000(0x7530, double:1.4822E-319)
        L_0x0033:
            com.fossil.yp0 r5 = com.fossil.yp0.f
            long r5 = r5.a(r0)
            r7 = 0
            int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r9 != 0) goto L_0x0040
            goto L_0x004c
        L_0x0040:
            long r9 = java.lang.System.currentTimeMillis()
            long r11 = r13.E
            long r9 = r9 - r11
            long r3 = r3 - r9
            long r5 = java.lang.Math.min(r3, r5)
        L_0x004c:
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 >= 0) goto L_0x0056
            com.fossil.eu0 r0 = r13.v
            r13.a(r0)
            goto L_0x008c
        L_0x0056:
            long r3 = java.lang.System.currentTimeMillis()
            r13.J = r3
            com.fossil.d31 r3 = new com.fossil.d31
            com.fossil.ri1 r4 = r13.w
            r3.<init>(r4, r0, r5)
            boolean r0 = r13.M
            r3.w = r0
            r13.M = r2
            int r0 = r13.N
            int r0 = r0 + r1
            r13.N = r0
            com.fossil.xk0 r2 = new com.fossil.xk0
            r2.<init>(r13)
            com.fossil.hs0 r4 = new com.fossil.hs0
            r4.<init>(r13)
            com.fossil.du0 r5 = new com.fossil.du0
            r5.<init>(r13)
            com.fossil.xv0 r6 = com.fossil.xv0.a
            r7 = 0
            r8 = 8
            r9 = 0
            r0 = r13
            r1 = r3
            r3 = r4
            r4 = r7
            r7 = r8
            r8 = r9
            com.fossil.zk0.a(r0, r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x008c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.s21.q():void");
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean a(zk0 zk0) {
        return b(zk0) || (zk0.f().isEmpty() && ((zk0) this).y != zk0.y);
    }

    @DexIgnore
    public final void b(eu0 eu0) {
        if (eu0.b == is0.SUCCESS) {
            a(eu0);
            return;
        }
        ((zk0) this).v = eu0;
        this.O++;
        zk0.a(this, new wf1(((zk0) this).w), lq0.a, gs0.a, (kd7) null, new cu0(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void a(dd1 dd1) {
        if (xq1.a[dd1.ordinal()] == 1) {
            v81 v81 = ((zk0) this).b;
            if (v81 == null || v81.t) {
                zk0 zk0 = ((zk0) this).n;
                if (zk0 == null || zk0.t) {
                    a(is0.CONNECTION_DROPPED);
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ void a(s21 s21, long j) {
        if (s21.G < 2) {
            ((zk0) s21).b = null;
            ((zk0) s21).o.postDelayed(new r21(s21), j);
            return;
        }
        eu0 eu0 = ((zk0) s21).v;
        s21.b(eu0.a(eu0, null, is0.G.a(eu0.c), null, 5));
    }
}
