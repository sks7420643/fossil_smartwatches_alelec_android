package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b27 {
    @DexIgnore
    public static Map<String, Object> a(z17 z17) {
        s17 a = z17.a();
        if (a == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("sql", a.c());
        hashMap.put("arguments", a.b());
        return hashMap;
    }
}
