package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ob5 {
    IMPERIAL("imperial"),
    METRIC("metric");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public ob5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static ob5 fromString(String str) {
        if (TextUtils.isEmpty(str) || !str.equalsIgnoreCase("imperial")) {
            return METRIC;
        }
        return IMPERIAL;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
