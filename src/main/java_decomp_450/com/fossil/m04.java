package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m04 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends n04 {
        @DexIgnore
        public /* final */ Charset a;

        @DexIgnore
        public a(Charset charset) {
            jw3.a(charset);
            this.a = charset;
        }

        @DexIgnore
        @Override // com.fossil.n04
        public Reader a() throws IOException {
            return new InputStreamReader(m04.this.a(), this.a);
        }

        @DexIgnore
        public String toString() {
            return m04.this.toString() + ".asCharSource(" + this.a + ")";
        }
    }

    @DexIgnore
    public n04 a(Charset charset) {
        return new a(charset);
    }

    @DexIgnore
    public abstract InputStream a() throws IOException;
}
