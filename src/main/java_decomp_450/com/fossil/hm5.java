package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hm5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);
    @DexIgnore
    public b d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public c f;
    @DexIgnore
    public /* final */ nj5.b g; // = new i(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ ad5 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ ch5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hm5.m;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            ee7.b(str, "newActiveSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(int i, ArrayList<Integer> arrayList, String str) {
            this.a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public d(Device device) {
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", f = "SwitchActiveDeviceUseCase.kt", l = {154}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(hm5 hm5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = hm5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = hm5.n.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doSwitchDevice serial ");
                b f = this.this$0.f();
                if (f != null) {
                    sb.append(f.b());
                    local.d(a2, sb.toString());
                    PortfolioApp c = PortfolioApp.g0.c();
                    b f2 = this.this$0.f();
                    if (f2 != null) {
                        String b = f2.b();
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = c.d(b, this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!((Boolean) obj).booleanValue()) {
                this.this$0.i();
                this.this$0.a((fl4.a) new c(116, null, ""));
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", f = "SwitchActiveDeviceUseCase.kt", l = {180, 183}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $newActiveDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(hm5 hm5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = hm5;
            this.$newActiveDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$newActiveDeviceSerial, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            String str;
            boolean z;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = hm5.n.a();
                local.d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
                String c = this.this$0.k.c();
                hm5 hm5 = this.this$0;
                hm5.a(hm5.i.getDeviceBySerial(this.$newActiveDeviceSerial));
                z = PortfolioApp.g0.c().c(this.$newActiveDeviceSerial);
                if (z) {
                    hm5 hm52 = this.this$0;
                    String str2 = this.$newActiveDeviceSerial;
                    this.L$0 = yi7;
                    this.L$1 = c;
                    this.Z$0 = z;
                    this.label = 1;
                    Object a3 = hm52.a(str2, null, this);
                    if (a3 == a) {
                        return a;
                    }
                    str = c;
                    obj = a3;
                } else {
                    this.this$0.a((fl4.a) new c(116, null, ""));
                    return i97.a;
                }
            } else if (i == 1) {
                z = this.Z$0;
                str = (String) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                String str3 = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                hm5 hm53 = this.this$0;
                hm53.a(new d(hm53.e()));
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean booleanValue = ((Boolean) ((r87) obj).component1()).booleanValue();
            if (booleanValue) {
                this.this$0.b(this.$newActiveDeviceSerial);
                yw6 a4 = yw6.h.a();
                this.L$0 = yi7;
                this.L$1 = str;
                this.Z$0 = z;
                this.Z$1 = booleanValue;
                this.label = 2;
                if (a4.a(this) == a) {
                    return a;
                }
                hm5 hm532 = this.this$0;
                hm532.a(new d(hm532.e()));
                return i97.a;
            }
            PortfolioApp.g0.c().c(str);
            c g = this.this$0.g();
            if (g == null || this.this$0.a((fl4.a) g) == null) {
                this.this$0.a((fl4.a) new c(116, null, ""));
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<fs6, ds6> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fs6 fs6) {
            ee7.b(fs6, "responseValue");
            FLogger.INSTANCE.getLocal().d(hm5.n.a(), "getDeviceSetting success");
        }

        @DexIgnore
        public void a(ds6 ds6) {
            ee7.b(ds6, "errorValue");
            FLogger.INSTANCE.getLocal().d(hm5.n.a(), "getDeviceSetting fail");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2", f = "SwitchActiveDeviceUseCase.kt", l = {233, 236}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super r87<? extends Boolean, ? extends Integer>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ hm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(hm5 hm5, MisfitDeviceProfile misfitDeviceProfile, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = hm5;
            this.$currentDeviceProfile = misfitDeviceProfile;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$currentDeviceProfile, this.$serial, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super r87<? extends Boolean, ? extends Integer>> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x008b, code lost:
            if (r12 != null) goto L_0x00b3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b3, code lost:
            if (r12 != null) goto L_0x00c2;
         */
        @DexIgnore
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r24) {
            /*
                r23 = this;
                r0 = r23
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 0
                r4 = 2
                r5 = -1
                r6 = 1
                r7 = 0
                if (r2 == 0) goto L_0x0035
                if (r2 == r6) goto L_0x002a
                if (r2 != r4) goto L_0x0022
                java.lang.Object r1 = r0.L$1
                com.fossil.zi5 r1 = (com.fossil.zi5) r1
                java.lang.Object r1 = r0.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r24)
                r2 = r24
                goto L_0x015e
            L_0x0022:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x002a:
                java.lang.Object r2 = r0.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r24)
                r8 = r24
                goto L_0x0134
            L_0x0035:
                com.fossil.t87.a(r24)
                com.fossil.yi7 r2 = r0.p$
                com.fossil.hm5 r8 = r0.this$0
                com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r0.$currentDeviceProfile
                if (r9 == 0) goto L_0x00b6
                int r10 = r9.getBatteryLevel()
                r11 = 100
                if (r10 >= 0) goto L_0x0049
                goto L_0x004c
            L_0x0049:
                if (r11 < r10) goto L_0x004c
                goto L_0x0052
            L_0x004c:
                if (r10 >= 0) goto L_0x0050
                r10 = 0
                goto L_0x0052
            L_0x0050:
                r10 = 100
            L_0x0052:
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r11 = r9.getVibrationStrength()
                int r11 = r11.getVibrationStrengthLevel()
                int r11 = com.fossil.he5.b(r11)
                com.fossil.hm5 r12 = r0.this$0
                com.portfolio.platform.data.source.DeviceRepository r12 = r12.i
                java.lang.String r13 = r0.$serial
                com.portfolio.platform.data.model.Device r12 = r12.getDeviceBySerial(r13)
                if (r12 == 0) goto L_0x008e
                r12.setBatteryLevel(r10)
                java.lang.String r13 = r9.getFirmwareVersion()
                r12.setFirmwareRevision(r13)
                java.lang.String r13 = r9.getAddress()
                r12.setMacAddress(r13)
                short r13 = r9.getMicroAppMajorVersion()
                r12.setMajor(r13)
                short r13 = r9.getMicroAppMinorVersion()
                r12.setMinor(r13)
                if (r12 == 0) goto L_0x008e
                goto L_0x00b3
            L_0x008e:
                com.portfolio.platform.data.model.Device r22 = new com.portfolio.platform.data.model.Device
                java.lang.String r13 = r9.getDeviceSerial()
                java.lang.String r14 = r9.getAddress()
                java.lang.String r15 = r9.getDeviceModel()
                java.lang.String r16 = r9.getFirmwareVersion()
                java.lang.Integer r18 = com.fossil.pb7.a(r11)
                r19 = 0
                r20 = 64
                r21 = 0
                r12 = r22
                r17 = r10
                r12.<init>(r13, r14, r15, r16, r17, r18, r19, r20, r21)
                com.fossil.i97 r9 = com.fossil.i97.a
            L_0x00b3:
                if (r12 == 0) goto L_0x00b6
                goto L_0x00c2
            L_0x00b6:
                com.fossil.hm5 r9 = r0.this$0
                com.portfolio.platform.data.source.DeviceRepository r9 = r9.i
                java.lang.String r10 = r0.$serial
                com.portfolio.platform.data.model.Device r12 = r9.getDeviceBySerial(r10)
            L_0x00c2:
                r8.a(r12)
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.fossil.hm5$a r9 = com.fossil.hm5.n
                java.lang.String r9 = r9.a()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "doSwitchDevice device "
                r10.append(r11)
                com.fossil.hm5 r11 = r0.this$0
                com.portfolio.platform.data.model.Device r11 = r11.e()
                r10.append(r11)
                java.lang.String r10 = r10.toString()
                r8.d(r9, r10)
                com.fossil.hm5 r8 = r0.this$0
                com.portfolio.platform.data.model.Device r8 = r8.e()
                if (r8 != 0) goto L_0x011b
                com.fossil.hm5 r1 = r0.this$0
                com.fossil.hm5$c r2 = new com.fossil.hm5$c
                r3 = 116(0x74, float:1.63E-43)
                java.lang.Integer[] r4 = new java.lang.Integer[r6]
                java.lang.Integer r6 = com.fossil.pb7.a(r5)
                r4[r7] = r6
                java.util.ArrayList r4 = com.fossil.w97.a(r4)
                java.lang.String r6 = "No device data"
                r2.<init>(r3, r4, r6)
                r1.a(r2)
                com.fossil.r87 r1 = new com.fossil.r87
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                java.lang.Integer r3 = com.fossil.pb7.a(r5)
                r1.<init>(r2, r3)
                return r1
            L_0x011b:
                com.fossil.hm5 r8 = r0.this$0
                com.portfolio.platform.data.source.DeviceRepository r8 = r8.i
                com.fossil.hm5 r9 = r0.this$0
                com.portfolio.platform.data.model.Device r9 = r9.e()
                if (r9 == 0) goto L_0x01ed
                r0.L$0 = r2
                r0.label = r6
                java.lang.Object r8 = r8.forceLinkDevice(r9, r0)
                if (r8 != r1) goto L_0x0134
                return r1
            L_0x0134:
                com.fossil.zi5 r8 = (com.fossil.zi5) r8
                boolean r9 = r8 instanceof com.fossil.bj5
                if (r9 == 0) goto L_0x016e
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.hm5$a r7 = com.fossil.hm5.n
                java.lang.String r7 = r7.a()
                java.lang.String r9 = "doSwitchDevice success"
                r3.d(r7, r9)
                com.fossil.hm5 r3 = r0.this$0
                com.portfolio.platform.data.source.UserRepository r3 = r3.h
                r0.L$0 = r2
                r0.L$1 = r8
                r0.label = r4
                java.lang.Object r2 = r3.getCurrentUser(r0)
                if (r2 != r1) goto L_0x015e
                return r1
            L_0x015e:
                com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                com.fossil.r87 r1 = new com.fossil.r87
                java.lang.Boolean r2 = com.fossil.pb7.a(r6)
                java.lang.Integer r3 = com.fossil.pb7.a(r5)
                r1.<init>(r2, r3)
                return r1
            L_0x016e:
                boolean r1 = r8 instanceof com.fossil.yi5
                if (r1 == 0) goto L_0x01df
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.hm5$a r2 = com.fossil.hm5.n
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "doSwitchDevice fail "
                r4.append(r5)
                com.fossil.yi5 r8 = (com.fossil.yi5) r8
                int r5 = r8.a()
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r1.d(r2, r4)
                com.fossil.hm5 r1 = r0.this$0
                com.fossil.hm5$c r2 = new com.fossil.hm5$c
                r4 = 114(0x72, float:1.6E-43)
                java.lang.Integer[] r5 = new java.lang.Integer[r6]
                int r6 = r8.a()
                java.lang.Integer r6 = com.fossil.pb7.a(r6)
                r5[r7] = r6
                java.util.ArrayList r5 = com.fossil.w97.a(r5)
                com.portfolio.platform.data.model.ServerError r6 = r8.c()
                if (r6 == 0) goto L_0x01b8
                java.lang.String r3 = r6.getMessage()
            L_0x01b8:
                r2.<init>(r4, r5, r3)
                r1.a(r2)
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                java.lang.String r2 = r0.$serial
                int r3 = r8.a()
                r1.a(r2, r7, r3)
                com.fossil.r87 r1 = new com.fossil.r87
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                int r3 = r8.a()
                java.lang.Integer r3 = com.fossil.pb7.a(r3)
                r1.<init>(r2, r3)
                return r1
            L_0x01df:
                com.fossil.r87 r1 = new com.fossil.r87
                java.lang.Boolean r2 = com.fossil.pb7.a(r7)
                java.lang.Integer r3 = com.fossil.pb7.a(r5)
                r1.<init>(r2, r3)
                return r1
            L_0x01ed:
                com.fossil.ee7.a()
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hm5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ hm5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1", f = "SwitchActiveDeviceUseCase.kt", l = {74}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, String str, MisfitDeviceProfile misfitDeviceProfile, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
                this.$serial = str;
                this.$currentDeviceProfile = misfitDeviceProfile;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$serial, this.$currentDeviceProfile, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    hm5 hm5 = this.this$0.a;
                    String str = this.$serial;
                    ee7.a((Object) str, "serial");
                    MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
                    if (misfitDeviceProfile != null) {
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = hm5.a(str, misfitDeviceProfile, this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                r87 r87 = (r87) obj;
                boolean booleanValue = ((Boolean) r87.component1()).booleanValue();
                int intValue = ((Number) r87.component2()).intValue();
                PortfolioApp c = PortfolioApp.g0.c();
                String str2 = this.$serial;
                ee7.a((Object) str2, "serial");
                c.a(str2, booleanValue, intValue);
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2", f = "SwitchActiveDeviceUseCase.kt", l = {94, 98}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, MisfitDeviceProfile misfitDeviceProfile, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
                this.$currentDeviceProfile = misfitDeviceProfile;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$currentDeviceProfile, this.$serial, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00a9 A[RETURN] */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r8.label
                    java.lang.String r2 = "serial"
                    r3 = 2
                    r4 = 1
                    if (r1 == 0) goto L_0x0033
                    if (r1 == r4) goto L_0x0025
                    if (r1 != r3) goto L_0x001d
                    java.lang.Object r0 = r8.L$1
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.yi7 r0 = (com.fossil.yi7) r0
                    com.fossil.t87.a(r9)
                    goto L_0x00aa
                L_0x001d:
                    java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r9.<init>(r0)
                    throw r9
                L_0x0025:
                    java.lang.Object r1 = r8.L$1
                    com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
                    int r4 = r8.I$0
                    java.lang.Object r5 = r8.L$0
                    com.fossil.yi7 r5 = (com.fossil.yi7) r5
                    com.fossil.t87.a(r9)
                    goto L_0x0086
                L_0x0033:
                    com.fossil.t87.a(r9)
                    com.fossil.yi7 r5 = r8.p$
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r9 = r8.$currentDeviceProfile
                    com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r9 = r9.getVibrationStrength()
                    int r9 = r9.getVibrationStrengthLevel()
                    int r9 = com.fossil.he5.b(r9)
                    com.fossil.hm5$i r1 = r8.this$0
                    com.fossil.hm5 r1 = r1.a
                    com.portfolio.platform.data.source.DeviceRepository r1 = r1.i
                    java.lang.String r6 = r8.$serial
                    com.fossil.ee7.a(r6, r2)
                    com.portfolio.platform.data.model.Device r1 = r1.getDeviceBySerial(r6)
                    if (r1 == 0) goto L_0x0087
                    java.lang.Integer r6 = r1.getVibrationStrength()
                    if (r6 != 0) goto L_0x0060
                    goto L_0x0066
                L_0x0060:
                    int r6 = r6.intValue()
                    if (r6 == r9) goto L_0x0087
                L_0x0066:
                    java.lang.Integer r6 = com.fossil.pb7.a(r9)
                    r1.setVibrationStrength(r6)
                    com.fossil.hm5$i r6 = r8.this$0
                    com.fossil.hm5 r6 = r6.a
                    com.portfolio.platform.data.source.DeviceRepository r6 = r6.i
                    r7 = 0
                    r8.L$0 = r5
                    r8.I$0 = r9
                    r8.L$1 = r1
                    r8.label = r4
                    java.lang.Object r4 = r6.updateDevice(r1, r7, r8)
                    if (r4 != r0) goto L_0x0085
                    return r0
                L_0x0085:
                    r4 = r9
                L_0x0086:
                    r9 = r4
                L_0x0087:
                    com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                    com.portfolio.platform.PortfolioApp r4 = r4.c()
                    java.lang.String r6 = r8.$serial
                    com.fossil.ee7.a(r6, r2)
                    r4.i(r6)
                    com.fossil.yw6$a r2 = com.fossil.yw6.h
                    com.fossil.yw6 r2 = r2.a()
                    r8.L$0 = r5
                    r8.I$0 = r9
                    r8.L$1 = r1
                    r8.label = r3
                    java.lang.Object r9 = r2.a(r8)
                    if (r9 != r0) goto L_0x00aa
                    return r0
                L_0x00aa:
                    com.fossil.hm5$i r9 = r8.this$0
                    com.fossil.hm5 r9 = r9.a
                    com.fossil.hm5$d r0 = new com.fossil.hm5$d
                    com.portfolio.platform.data.model.Device r1 = r9.e()
                    r0.<init>(r1)
                    r9.a(r0)
                    com.fossil.i97 r9 = com.fossil.i97.a
                    return r9
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.hm5.i.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(hm5 hm5) {
            this.a = hm5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (communicateMode == CommunicateMode.SWITCH_DEVICE) {
                b f = this.a.f();
                if (!ee7.a((Object) stringExtra, (Object) (f != null ? f.b() : null))) {
                    return;
                }
                if (intExtra == ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, stringExtra, (MisfitDeviceProfile) extras.getParcelable("device"), null), 3, null);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.i();
                    FLogger.INSTANCE.getLocal().d(hm5.n.a(), "Switch device  success");
                    Bundle extras2 = intent.getExtras();
                    if (extras2 != null) {
                        MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                        if (misfitDeviceProfile != null) {
                            if (misfitDeviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                                this.a.l.a(misfitDeviceProfile.getHeartRateMode());
                            }
                            hm5 hm5 = this.a;
                            ee7.a((Object) stringExtra, "serial");
                            hm5.b(stringExtra);
                            ik7 unused2 = xh7.b(zi7.a(qj7.b()), null, null, new b(this, misfitDeviceProfile, stringExtra, null), 3, null);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    this.a.i();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = hm5.n.a();
                    local.d(a2, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            c g = this.a.g();
                            if (g == null || this.a.a((fl4.a) g) == null) {
                                this.a.a((fl4.a) new c(116, null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.a.a((fl4.a) new c(117, null, ""));
                            return;
                        }
                    }
                    this.a.a((fl4.a) new c(113, integerArrayListExtra, ""));
                }
            }
        }
    }

    /*
    static {
        String simpleName = hm5.class.getSimpleName();
        ee7.a((Object) simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public hm5(UserRepository userRepository, DeviceRepository deviceRepository, ad5 ad5, PortfolioApp portfolioApp, ch5 ch5) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(portfolioApp, "mApp");
        ee7.b(ch5, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = ad5;
        this.k = portfolioApp;
        this.l = ch5;
    }

    @DexIgnore
    public final Device e() {
        return this.e;
    }

    @DexIgnore
    public final b f() {
        return this.d;
    }

    @DexIgnore
    public final c g() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        nj5.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
        nj5.d.a(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        nj5.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).a(new es6(str), new g());
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return m;
    }

    @DexIgnore
    public final ik7 d() {
        return xh7.b(b(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void a(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void a(c cVar) {
        this.f = cVar;
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        if (bVar == null) {
            return new c(600, null, "");
        }
        this.d = bVar;
        String c2 = this.k.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bVar.a() + " currentActive " + c2);
        if (bVar.a() != 4) {
            d();
        } else {
            a(bVar.b());
        }
        return new Object();
    }

    @DexIgnore
    public final ik7 a(String str) {
        return xh7.b(b(), null, null, new f(this, str, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, MisfitDeviceProfile misfitDeviceProfile, fb7<? super r87<Boolean, Integer>> fb7) {
        return vh7.a(qj7.b(), new h(this, misfitDeviceProfile, str, null), fb7);
    }
}
