package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gz1 extends zy1 {
    @DexIgnore
    public /* final */ /* synthetic */ fz1 a;

    @DexIgnore
    public gz1(fz1 fz1) {
        this.a = fz1;
    }

    @DexIgnore
    @Override // com.fossil.zy1, com.fossil.nz1
    public final void a(Status status) throws RemoteException {
        this.a.a((i12) status);
    }
}
