package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj4 {
    @DexIgnore
    public static int a(ej4 ej4) {
        return a(ej4, true) + a(ej4, false);
    }

    @DexIgnore
    public static int b(ej4 ej4) {
        byte[][] a = ej4.a();
        int c = ej4.c();
        int b = ej4.b();
        int i = 0;
        for (int i2 = 0; i2 < b - 1; i2++) {
            int i3 = 0;
            while (i3 < c - 1) {
                byte b2 = a[i2][i3];
                int i4 = i3 + 1;
                if (b2 == a[i2][i4]) {
                    int i5 = i2 + 1;
                    if (b2 == a[i5][i3] && b2 == a[i5][i4]) {
                        i++;
                    }
                }
                i3 = i4;
            }
        }
        return i * 3;
    }

    @DexIgnore
    public static int c(ej4 ej4) {
        byte[][] a = ej4.a();
        int c = ej4.c();
        int b = ej4.b();
        int i = 0;
        for (int i2 = 0; i2 < b; i2++) {
            for (int i3 = 0; i3 < c; i3++) {
                byte[] bArr = a[i2];
                int i4 = i3 + 6;
                if (i4 < c && bArr[i3] == 1 && bArr[i3 + 1] == 0 && bArr[i3 + 2] == 1 && bArr[i3 + 3] == 1 && bArr[i3 + 4] == 1 && bArr[i3 + 5] == 0 && bArr[i4] == 1 && (a(bArr, i3 - 4, i3) || a(bArr, i3 + 7, i3 + 11))) {
                    i++;
                }
                int i5 = i2 + 6;
                if (i5 < b && a[i2][i3] == 1 && a[i2 + 1][i3] == 0 && a[i2 + 2][i3] == 1 && a[i2 + 3][i3] == 1 && a[i2 + 4][i3] == 1 && a[i2 + 5][i3] == 0 && a[i5][i3] == 1 && (a(a, i3, i2 - 4, i2) || a(a, i3, i2 + 7, i2 + 11))) {
                    i++;
                }
            }
        }
        return i * 40;
    }

    @DexIgnore
    public static int d(ej4 ej4) {
        byte[][] a = ej4.a();
        int c = ej4.c();
        int b = ej4.b();
        int i = 0;
        for (int i2 = 0; i2 < b; i2++) {
            byte[] bArr = a[i2];
            for (int i3 = 0; i3 < c; i3++) {
                if (bArr[i3] == 1) {
                    i++;
                }
            }
        }
        int b2 = ej4.b() * ej4.c();
        return ((Math.abs((i << 1) - b2) * 10) / b2) * 10;
    }

    @DexIgnore
    public static boolean a(byte[] bArr, int i, int i2) {
        int min = Math.min(i2, bArr.length);
        for (int max = Math.max(i, 0); max < min; max++) {
            if (bArr[max] == 1) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean a(byte[][] bArr, int i, int i2, int i3) {
        int min = Math.min(i3, bArr.length);
        for (int max = Math.max(i2, 0); max < min; max++) {
            if (bArr[max][i] == 1) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static boolean a(int i, int i2, int i3) {
        int i4;
        int i5;
        switch (i) {
            case 0:
                i3 += i2;
                i4 = i3 & 1;
                break;
            case 1:
                i4 = i3 & 1;
                break;
            case 2:
                i4 = i2 % 3;
                break;
            case 3:
                i4 = (i3 + i2) % 3;
                break;
            case 4:
                i3 /= 2;
                i2 /= 3;
                i3 += i2;
                i4 = i3 & 1;
                break;
            case 5:
                int i6 = i3 * i2;
                i4 = (i6 & 1) + (i6 % 3);
                break;
            case 6:
                int i7 = i3 * i2;
                i5 = (i7 & 1) + (i7 % 3);
                i4 = i5 & 1;
                break;
            case 7:
                i5 = ((i3 * i2) % 3) + ((i3 + i2) & 1);
                i4 = i5 & 1;
                break;
            default:
                throw new IllegalArgumentException("Invalid mask pattern: " + i);
        }
        if (i4 == 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static int a(ej4 ej4, boolean z) {
        int b = z ? ej4.b() : ej4.c();
        int c = z ? ej4.c() : ej4.b();
        byte[][] a = ej4.a();
        int i = 0;
        for (int i2 = 0; i2 < b; i2++) {
            byte b2 = -1;
            int i3 = 0;
            for (int i4 = 0; i4 < c; i4++) {
                byte b3 = z ? a[i2][i4] : a[i4][i2];
                if (b3 == b2) {
                    i3++;
                } else {
                    if (i3 >= 5) {
                        i += (i3 - 5) + 3;
                    }
                    b2 = b3;
                    i3 = 1;
                }
            }
            if (i3 >= 5) {
                i += (i3 - 5) + 3;
            }
        }
        return i;
    }
}
