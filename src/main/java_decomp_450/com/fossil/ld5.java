package com.fossil;

import com.fossil.m00;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ld5 implements m00<md5, InputStream> {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ld5.a;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n00<md5, InputStream> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.ld5' to match base method */
        @Override // com.fossil.n00
        public m00<md5, InputStream> a(q00 q00) {
            ee7.b(q00, "multiFactory");
            return new ld5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements ix<InputStream> {
        @DexIgnore
        public volatile boolean a;
        @DexIgnore
        public /* final */ md5 b;

        @DexIgnore
        public c(ld5 ld5, md5 md5) {
            this.b = md5;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:83:0x023f  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x024b  */
        @Override // com.fossil.ix
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.fossil.ew r11, com.fossil.ix.a<? super java.io.InputStream> r12) {
            /*
                r10 = this;
                java.lang.String r0 = "priority"
                com.fossil.ee7.b(r11, r0)
                java.lang.String r11 = "callback"
                com.fossil.ee7.b(r12, r11)
                java.util.ArrayList r11 = new java.util.ArrayList
                r0 = 5
                r11.<init>(r0)
                com.fossil.md5 r1 = r10.b
                if (r1 == 0) goto L_0x0257
                java.util.List r1 = r1.a()
                if (r1 == 0) goto L_0x0257
                java.util.Iterator r1 = r1.iterator()
                r2 = 0
                r3 = 0
            L_0x0020:
                boolean r4 = r1.hasNext()
                r5 = 0
                if (r4 == 0) goto L_0x0114
                java.lang.Object r4 = r1.next()
                com.fossil.wearables.fsl.shared.BaseFeatureModel r4 = (com.fossil.wearables.fsl.shared.BaseFeatureModel) r4
                boolean r6 = r4 instanceof com.fossil.wearables.fsl.contact.ContactGroup
                if (r6 == 0) goto L_0x00ab
                r6 = r4
                com.fossil.wearables.fsl.contact.ContactGroup r6 = (com.fossil.wearables.fsl.contact.ContactGroup) r6
                java.util.List r6 = r6.getContacts()
                java.util.Iterator r6 = r6.iterator()
            L_0x003c:
                boolean r7 = r6.hasNext()
                if (r7 == 0) goto L_0x00ab
                java.lang.Object r7 = r6.next()
                com.fossil.wearables.fsl.contact.Contact r7 = (com.fossil.wearables.fsl.contact.Contact) r7
                int r8 = r11.size()
                if (r8 >= r0) goto L_0x00a8
                java.lang.String r8 = "contactItem"
                com.fossil.ee7.a(r7, r8)
                java.lang.String r8 = r7.getPhotoThumbUri()
                boolean r8 = android.text.TextUtils.isEmpty(r8)
                if (r8 != 0) goto L_0x006b
                int r7 = r7.getContactId()
                long r7 = (long) r7
                java.lang.Long r7 = java.lang.Long.valueOf(r7)
                android.graphics.Bitmap r7 = com.fossil.sw6.a(r7)
                goto L_0x00a3
            L_0x006b:
                int r8 = r7.getContactId()
                r9 = -100
                if (r8 != r9) goto L_0x0085
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                r8 = 2131231108(0x7f080184, float:1.8078288E38)
                android.graphics.drawable.Drawable r7 = com.fossil.v6.c(r7, r8)
                android.graphics.Bitmap r7 = com.fossil.sw6.a(r7)
                goto L_0x00a3
            L_0x0085:
                int r8 = r7.getContactId()
                r9 = -200(0xffffffffffffff38, float:NaN)
                if (r8 != r9) goto L_0x009f
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                r8 = 2131231109(0x7f080185, float:1.807829E38)
                android.graphics.drawable.Drawable r7 = com.fossil.v6.c(r7, r8)
                android.graphics.Bitmap r7 = com.fossil.sw6.a(r7)
                goto L_0x00a3
            L_0x009f:
                android.graphics.Bitmap r7 = com.fossil.sw6.a(r7)
            L_0x00a3:
                if (r7 == 0) goto L_0x00a8
                r11.add(r7)
            L_0x00a8:
                int r3 = r3 + 1
                goto L_0x003c
            L_0x00ab:
                boolean r6 = r4 instanceof com.fossil.wearables.fsl.appfilter.AppFilter
                if (r6 == 0) goto L_0x0020
                int r6 = r11.size()
                if (r6 >= r0) goto L_0x0110
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                android.content.pm.PackageManager r6 = r6.getPackageManager()
                com.fossil.wearables.fsl.appfilter.AppFilter r4 = (com.fossil.wearables.fsl.appfilter.AppFilter) r4     // Catch:{ Exception -> 0x010c }
                java.lang.String r4 = r4.getType()     // Catch:{ Exception -> 0x010c }
                r7 = 128(0x80, float:1.794E-43)
                android.content.pm.ApplicationInfo r4 = r6.getApplicationInfo(r4, r7)     // Catch:{ Exception -> 0x010c }
                java.lang.String r7 = "packageManager.getApplic\u2026ageManager.GET_META_DATA)"
                com.fossil.ee7.a(r4, r7)     // Catch:{ Exception -> 0x010c }
                int r7 = r4.icon     // Catch:{ Exception -> 0x010c }
                if (r7 == 0) goto L_0x00f2
                android.content.res.Resources r6 = r6.getResourcesForApplication(r4)     // Catch:{ Exception -> 0x010c }
                int r7 = r4.icon     // Catch:{ Exception -> 0x00e1 }
                r8 = 480(0x1e0, float:6.73E-43)
                android.graphics.drawable.Drawable r4 = r6.getDrawableForDensity(r7, r8, r5)     // Catch:{ Exception -> 0x00e1 }
                goto L_0x0102
            L_0x00e1:
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                android.content.pm.PackageManager r5 = r5.getPackageManager()
                java.lang.String r4 = r4.packageName
                android.graphics.drawable.Drawable r4 = r5.getApplicationIcon(r4)
                goto L_0x0102
            L_0x00f2:
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                android.content.pm.PackageManager r5 = r5.getPackageManager()
                java.lang.String r4 = r4.packageName
                android.graphics.drawable.Drawable r4 = r5.getApplicationIcon(r4)
            L_0x0102:
                android.graphics.Bitmap r4 = com.fossil.sw6.a(r4)
                if (r4 == 0) goto L_0x0110
                r11.add(r4)
                goto L_0x0110
            L_0x010c:
                r4 = move-exception
                r4.printStackTrace()
            L_0x0110:
                int r3 = r3 + 1
                goto L_0x0020
            L_0x0114:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.ld5$a r1 = com.fossil.ld5.b
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r6 = "loadData: itemCounter = "
                r4.append(r6)
                r4.append(r3)
                java.lang.String r4 = r4.toString()
                r0.d(r1, r4)
                java.util.ArrayList r0 = new java.util.ArrayList
                r1 = 10
                int r1 = com.fossil.x97.a(r11, r1)
                r0.<init>(r1)
                java.util.Iterator r1 = r11.iterator()
            L_0x0143:
                boolean r4 = r1.hasNext()
                if (r4 == 0) goto L_0x0163
                java.lang.Object r4 = r1.next()
                android.graphics.Bitmap r4 = (android.graphics.Bitmap) r4
                int r6 = r4.getWidth()
                int r4 = r4.getHeight()
                int r4 = java.lang.Math.max(r6, r4)
                java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
                r0.add(r4)
                goto L_0x0143
            L_0x0163:
                java.lang.Comparable r0 = com.fossil.ea7.h(r0)
                java.lang.Integer r0 = (java.lang.Integer) r0
                if (r0 == 0) goto L_0x0170
                int r0 = r0.intValue()
                goto L_0x0171
            L_0x0170:
                r0 = 0
            L_0x0171:
                if (r3 == 0) goto L_0x0217
                r1 = 1
                if (r3 == r1) goto L_0x020a
                r4 = 2
                if (r3 == r4) goto L_0x01f3
                r6 = 3
                if (r3 == r6) goto L_0x01d6
                r7 = 4
                if (r3 == r7) goto L_0x01b2
                int r3 = r3 - r7
                int r3 = r3 + r1
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                r7 = 43
                r6.append(r7)
                r6.append(r3)
                java.lang.String r3 = r6.toString()
                android.graphics.Bitmap r3 = com.fossil.sw6.b(r3)
                java.lang.Object r2 = r11.get(r2)
                android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
                java.lang.Object r1 = r11.get(r1)
                android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1
                java.lang.Object r4 = r11.get(r4)
                android.graphics.Bitmap r4 = (android.graphics.Bitmap) r4
                android.graphics.Bitmap r0 = com.fossil.sw6.a(r2, r1, r4, r3, r0)
                if (r3 == 0) goto L_0x0218
                r11.add(r3)
                goto L_0x0218
            L_0x01b2:
                int r3 = r11.size()
                if (r3 <= r6) goto L_0x0217
                java.lang.Object r2 = r11.get(r2)
                android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
                java.lang.Object r1 = r11.get(r1)
                android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1
                java.lang.Object r3 = r11.get(r4)
                android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3
                java.lang.Object r11 = r11.get(r6)
                android.graphics.Bitmap r11 = (android.graphics.Bitmap) r11
                android.graphics.Bitmap r11 = com.fossil.sw6.a(r2, r1, r3, r11, r0)
            L_0x01d4:
                r0 = r11
                goto L_0x0218
            L_0x01d6:
                int r3 = r11.size()
                if (r3 <= r4) goto L_0x0217
                java.lang.Object r2 = r11.get(r2)
                android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
                java.lang.Object r1 = r11.get(r1)
                android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1
                java.lang.Object r11 = r11.get(r4)
                android.graphics.Bitmap r11 = (android.graphics.Bitmap) r11
                android.graphics.Bitmap r11 = com.fossil.sw6.a(r2, r1, r11, r0)
                goto L_0x01d4
            L_0x01f3:
                int r3 = r11.size()
                if (r3 <= r1) goto L_0x0217
                java.lang.Object r2 = r11.get(r2)
                android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
                java.lang.Object r11 = r11.get(r1)
                android.graphics.Bitmap r11 = (android.graphics.Bitmap) r11
                android.graphics.Bitmap r11 = com.fossil.sw6.a(r2, r11, r0)
                goto L_0x01d4
            L_0x020a:
                int r0 = r11.size()
                if (r0 <= 0) goto L_0x0217
                java.lang.Object r11 = r11.get(r2)
                android.graphics.Bitmap r11 = (android.graphics.Bitmap) r11
                goto L_0x01d4
            L_0x0217:
                r0 = r5
            L_0x0218:
                com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
                com.fossil.ld5$a r1 = com.fossil.ld5.b
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "loadData: result = "
                r2.append(r3)
                r2.append(r0)
                java.lang.String r2 = r2.toString()
                r11.d(r1, r2)
                java.io.ByteArrayOutputStream r11 = new java.io.ByteArrayOutputStream
                r11.<init>()
                if (r0 == 0) goto L_0x0246
                android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.PNG
                r2 = 100
                r0.compress(r1, r2, r11)
            L_0x0246:
                boolean r0 = r10.a
                if (r0 == 0) goto L_0x024b
                goto L_0x0254
            L_0x024b:
                java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream
                byte[] r11 = r11.toByteArray()
                r5.<init>(r11)
            L_0x0254:
                r12.a(r5)
            L_0x0257:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ld5.c.a(com.fossil.ew, com.fossil.ix$a):void");
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
            this.a = true;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    /*
    static {
        String simpleName = ld5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationLoader::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public boolean a(md5 md5) {
        ee7.b(md5, "notificationModel");
        return true;
    }

    @DexIgnore
    public m00.a<InputStream> a(md5 md5, int i, int i2, ax axVar) {
        ee7.b(md5, "notificationModel");
        ee7.b(axVar, "options");
        return new m00.a<>(md5, new c(this, md5));
    }
}
