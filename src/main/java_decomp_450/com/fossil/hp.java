package com.fossil;

import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import com.fossil.cm;
import com.fossil.lm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hp implements Runnable {
    @DexIgnore
    public static /* final */ String c; // = im.a("EnqueueRunnable");
    @DexIgnore
    public /* final */ an a;
    @DexIgnore
    public /* final */ wm b; // = new wm();

    @DexIgnore
    public hp(an anVar) {
        this.a = anVar;
    }

    @DexIgnore
    public boolean a() {
        WorkDatabase f = this.a.g().f();
        f.beginTransaction();
        try {
            boolean b2 = b(this.a);
            f.setTransactionSuccessful();
            return b2;
        } finally {
            f.endTransaction();
        }
    }

    @DexIgnore
    public lm b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        dn g = this.a.g();
        zm.a(g.b(), g.f(), g.e());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.a.h()) {
                if (a()) {
                    jp.a(this.a.g().a(), RescheduleReceiver.class, true);
                    c();
                }
                this.b.a(lm.a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", this.a));
        } catch (Throwable th) {
            this.b.a(new lm.b.a(th));
        }
    }

    @DexIgnore
    public static boolean b(an anVar) {
        List<an> e = anVar.e();
        boolean z = false;
        if (e != null) {
            boolean z2 = false;
            for (an anVar2 : e) {
                if (!anVar2.i()) {
                    z2 |= b(anVar2);
                } else {
                    im.a().e(c, String.format("Already enqueued work ids (%s).", TextUtils.join(", ", anVar2.c())), new Throwable[0]);
                }
            }
            z = z2;
        }
        return a(anVar) | z;
    }

    @DexIgnore
    public static boolean a(an anVar) {
        boolean a2 = a(anVar.g(), anVar.f(), (String[]) an.a(anVar).toArray(new String[0]), anVar.d(), anVar.b());
        anVar.j();
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01e5 A[LOOP:7: B:120:0x01df->B:122:0x01e5, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x020e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x015b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(com.fossil.dn r19, java.util.List<? extends com.fossil.sm> r20, java.lang.String[] r21, java.lang.String r22, com.fossil.dm r23) {
        /*
            r0 = r19
            r1 = r21
            r2 = r22
            r3 = r23
            long r4 = java.lang.System.currentTimeMillis()
            androidx.work.impl.WorkDatabase r6 = r19.f()
            r7 = 1
            if (r1 == 0) goto L_0x0018
            int r9 = r1.length
            if (r9 <= 0) goto L_0x0018
            r9 = 1
            goto L_0x0019
        L_0x0018:
            r9 = 0
        L_0x0019:
            if (r9 == 0) goto L_0x005d
            int r10 = r1.length
            r11 = 0
            r12 = 1
            r13 = 0
            r14 = 0
        L_0x0020:
            if (r11 >= r10) goto L_0x0060
            r15 = r1[r11]
            com.fossil.ap r8 = r6.f()
            com.fossil.zo r8 = r8.e(r15)
            if (r8 != 0) goto L_0x0045
            com.fossil.im r0 = com.fossil.im.a()
            java.lang.String r1 = com.fossil.hp.c
            java.lang.Object[] r2 = new java.lang.Object[r7]
            r3 = 0
            r2[r3] = r15
            java.lang.String r4 = "Prerequisite %s doesn't exist; not enqueuing"
            java.lang.String r2 = java.lang.String.format(r4, r2)
            java.lang.Throwable[] r4 = new java.lang.Throwable[r3]
            r0.b(r1, r2, r4)
            return r3
        L_0x0045:
            com.fossil.qm$a r8 = r8.b
            com.fossil.qm$a r15 = com.fossil.qm.a.SUCCEEDED
            if (r8 != r15) goto L_0x004d
            r15 = 1
            goto L_0x004e
        L_0x004d:
            r15 = 0
        L_0x004e:
            r12 = r12 & r15
            com.fossil.qm$a r15 = com.fossil.qm.a.FAILED
            if (r8 != r15) goto L_0x0055
            r14 = 1
            goto L_0x005a
        L_0x0055:
            com.fossil.qm$a r15 = com.fossil.qm.a.CANCELLED
            if (r8 != r15) goto L_0x005a
            r13 = 1
        L_0x005a:
            int r11 = r11 + 1
            goto L_0x0020
        L_0x005d:
            r12 = 1
            r13 = 0
            r14 = 0
        L_0x0060:
            boolean r8 = android.text.TextUtils.isEmpty(r22)
            r8 = r8 ^ r7
            if (r8 == 0) goto L_0x006b
            if (r9 != 0) goto L_0x006b
            r10 = 1
            goto L_0x006c
        L_0x006b:
            r10 = 0
        L_0x006c:
            if (r10 == 0) goto L_0x0150
            com.fossil.ap r10 = r6.f()
            java.util.List r10 = r10.b(r2)
            boolean r11 = r10.isEmpty()
            if (r11 != 0) goto L_0x0150
            com.fossil.dm r11 = com.fossil.dm.APPEND
            if (r3 == r11) goto L_0x00ca
            com.fossil.dm r11 = com.fossil.dm.APPEND_OR_REPLACE
            if (r3 != r11) goto L_0x0085
            goto L_0x00ca
        L_0x0085:
            com.fossil.dm r11 = com.fossil.dm.KEEP
            if (r3 != r11) goto L_0x00a5
            java.util.Iterator r3 = r10.iterator()
        L_0x008d:
            boolean r11 = r3.hasNext()
            if (r11 == 0) goto L_0x00a5
            java.lang.Object r11 = r3.next()
            com.fossil.zo$b r11 = (com.fossil.zo.b) r11
            com.fossil.qm$a r11 = r11.b
            com.fossil.qm$a r15 = com.fossil.qm.a.ENQUEUED
            if (r11 == r15) goto L_0x00a3
            com.fossil.qm$a r15 = com.fossil.qm.a.RUNNING
            if (r11 != r15) goto L_0x008d
        L_0x00a3:
            r11 = 0
            return r11
        L_0x00a5:
            r11 = 0
            com.fossil.gp r3 = com.fossil.gp.a(r2, r0, r11)
            r3.run()
            com.fossil.ap r3 = r6.f()
            java.util.Iterator r10 = r10.iterator()
        L_0x00b5:
            boolean r15 = r10.hasNext()
            if (r15 == 0) goto L_0x00c7
            java.lang.Object r15 = r10.next()
            com.fossil.zo$b r15 = (com.fossil.zo.b) r15
            java.lang.String r15 = r15.a
            r3.a(r15)
            goto L_0x00b5
        L_0x00c7:
            r3 = 1
            goto L_0x0151
        L_0x00ca:
            r11 = 0
            com.fossil.lo r9 = r6.a()
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            java.util.Iterator r10 = r10.iterator()
        L_0x00d8:
            boolean r16 = r10.hasNext()
            if (r16 == 0) goto L_0x0115
            java.lang.Object r16 = r10.next()
            r7 = r16
            com.fossil.zo$b r7 = (com.fossil.zo.b) r7
            java.lang.String r11 = r7.a
            boolean r11 = r9.c(r11)
            if (r11 != 0) goto L_0x010e
            com.fossil.qm$a r11 = r7.b
            r17 = r9
            com.fossil.qm$a r9 = com.fossil.qm.a.SUCCEEDED
            if (r11 != r9) goto L_0x00f8
            r9 = 1
            goto L_0x00f9
        L_0x00f8:
            r9 = 0
        L_0x00f9:
            r9 = r9 & r12
            com.fossil.qm$a r11 = r7.b
            com.fossil.qm$a r12 = com.fossil.qm.a.FAILED
            if (r11 != r12) goto L_0x0102
            r14 = 1
            goto L_0x0107
        L_0x0102:
            com.fossil.qm$a r12 = com.fossil.qm.a.CANCELLED
            if (r11 != r12) goto L_0x0107
            r13 = 1
        L_0x0107:
            java.lang.String r7 = r7.a
            r15.add(r7)
            r12 = r9
            goto L_0x0110
        L_0x010e:
            r17 = r9
        L_0x0110:
            r9 = r17
            r7 = 1
            r11 = 0
            goto L_0x00d8
        L_0x0115:
            com.fossil.dm r7 = com.fossil.dm.APPEND_OR_REPLACE
            if (r3 != r7) goto L_0x0142
            if (r13 != 0) goto L_0x011d
            if (r14 == 0) goto L_0x0142
        L_0x011d:
            com.fossil.ap r3 = r6.f()
            java.util.List r7 = r3.b(r2)
            java.util.Iterator r7 = r7.iterator()
        L_0x0129:
            boolean r9 = r7.hasNext()
            if (r9 == 0) goto L_0x013b
            java.lang.Object r9 = r7.next()
            com.fossil.zo$b r9 = (com.fossil.zo.b) r9
            java.lang.String r9 = r9.a
            r3.a(r9)
            goto L_0x0129
        L_0x013b:
            java.util.List r15 = java.util.Collections.emptyList()
            r3 = 0
            r13 = 0
            goto L_0x0143
        L_0x0142:
            r3 = r14
        L_0x0143:
            java.lang.Object[] r1 = r15.toArray(r1)
            java.lang.String[] r1 = (java.lang.String[]) r1
            int r7 = r1.length
            if (r7 <= 0) goto L_0x014e
            r9 = 1
            goto L_0x014f
        L_0x014e:
            r9 = 0
        L_0x014f:
            r14 = r3
        L_0x0150:
            r3 = 0
        L_0x0151:
            java.util.Iterator r7 = r20.iterator()
        L_0x0155:
            boolean r10 = r7.hasNext()
            if (r10 == 0) goto L_0x0214
            java.lang.Object r10 = r7.next()
            com.fossil.sm r10 = (com.fossil.sm) r10
            com.fossil.zo r11 = r10.d()
            if (r9 == 0) goto L_0x017c
            if (r12 != 0) goto L_0x017c
            if (r14 == 0) goto L_0x0170
            com.fossil.qm$a r15 = com.fossil.qm.a.FAILED
            r11.b = r15
            goto L_0x0184
        L_0x0170:
            if (r13 == 0) goto L_0x0177
            com.fossil.qm$a r15 = com.fossil.qm.a.CANCELLED
            r11.b = r15
            goto L_0x0184
        L_0x0177:
            com.fossil.qm$a r15 = com.fossil.qm.a.BLOCKED
            r11.b = r15
            goto L_0x0184
        L_0x017c:
            boolean r15 = r11.d()
            if (r15 != 0) goto L_0x0187
            r11.n = r4
        L_0x0184:
            r17 = r4
            goto L_0x018d
        L_0x0187:
            r17 = r4
            r4 = 0
            r11.n = r4
        L_0x018d:
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 23
            if (r4 < r5) goto L_0x019b
            r5 = 25
            if (r4 > r5) goto L_0x019b
            a(r11)
            goto L_0x01ac
        L_0x019b:
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 22
            if (r4 > r5) goto L_0x01ac
            java.lang.String r4 = "androidx.work.impl.background.gcm.GcmScheduler"
            boolean r4 = a(r0, r4)
            if (r4 == 0) goto L_0x01ac
            a(r11)
        L_0x01ac:
            com.fossil.qm$a r4 = r11.b
            com.fossil.qm$a r5 = com.fossil.qm.a.ENQUEUED
            if (r4 != r5) goto L_0x01b3
            r3 = 1
        L_0x01b3:
            com.fossil.ap r4 = r6.f()
            r4.a(r11)
            if (r9 == 0) goto L_0x01d7
            int r4 = r1.length
            r5 = 0
        L_0x01be:
            if (r5 >= r4) goto L_0x01d7
            r11 = r1[r5]
            com.fossil.ko r15 = new com.fossil.ko
            java.lang.String r0 = r10.b()
            r15.<init>(r0, r11)
            com.fossil.lo r0 = r6.a()
            r0.a(r15)
            int r5 = r5 + 1
            r0 = r19
            goto L_0x01be
        L_0x01d7:
            java.util.Set r0 = r10.c()
            java.util.Iterator r0 = r0.iterator()
        L_0x01df:
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x01fc
            java.lang.Object r4 = r0.next()
            java.lang.String r4 = (java.lang.String) r4
            com.fossil.dp r5 = r6.g()
            com.fossil.cp r11 = new com.fossil.cp
            java.lang.String r15 = r10.b()
            r11.<init>(r4, r15)
            r5.a(r11)
            goto L_0x01df
        L_0x01fc:
            if (r8 == 0) goto L_0x020e
            com.fossil.uo r0 = r6.d()
            com.fossil.to r4 = new com.fossil.to
            java.lang.String r5 = r10.b()
            r4.<init>(r2, r5)
            r0.a(r4)
        L_0x020e:
            r0 = r19
            r4 = r17
            goto L_0x0155
        L_0x0214:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hp.a(com.fossil.dn, java.util.List, java.lang.String[], java.lang.String, com.fossil.dm):boolean");
    }

    @DexIgnore
    public static void a(zo zoVar) {
        am amVar = zoVar.j;
        if (amVar.f() || amVar.i()) {
            String str = zoVar.c;
            cm.a aVar = new cm.a();
            aVar.a(zoVar.e);
            aVar.a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            zoVar.c = ConstraintTrackingWorker.class.getName();
            zoVar.e = aVar.a();
        }
    }

    @DexIgnore
    public static boolean a(dn dnVar, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (ym ymVar : dnVar.e()) {
                if (cls.isAssignableFrom(ymVar.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }
}
