package com.fossil;

import com.fossil.gc4;
import com.fossil.hc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ec4 extends hc4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ gc4.a b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends hc4.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public gc4.a b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;
        @DexIgnore
        public String g;

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a a(gc4.a aVar) {
            if (aVar != null) {
                this.b = aVar;
                return this;
            }
            throw new NullPointerException("Null registrationStatus");
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a b(String str) {
            this.a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a c(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a d(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a b(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public b(hc4 hc4) {
            this.a = hc4.c();
            this.b = hc4.f();
            this.c = hc4.a();
            this.d = hc4.e();
            this.e = Long.valueOf(hc4.b());
            this.f = Long.valueOf(hc4.g());
            this.g = hc4.d();
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a a(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4.a a(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.hc4.a
        public hc4 a() {
            String str = "";
            if (this.b == null) {
                str = str + " registrationStatus";
            }
            if (this.e == null) {
                str = str + " expiresInSecs";
            }
            if (this.f == null) {
                str = str + " tokenCreationEpochInSecs";
            }
            if (str.isEmpty()) {
                return new ec4(this.a, this.b, this.c, this.d, this.e.longValue(), this.f.longValue(), this.g);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public long b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public String c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public String d() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof hc4)) {
            return false;
        }
        hc4 hc4 = (hc4) obj;
        String str3 = this.a;
        if (str3 != null ? str3.equals(hc4.c()) : hc4.c() == null) {
            if (this.b.equals(hc4.f()) && ((str = this.c) != null ? str.equals(hc4.a()) : hc4.a() == null) && ((str2 = this.d) != null ? str2.equals(hc4.e()) : hc4.e() == null) && this.e == hc4.b() && this.f == hc4.g()) {
                String str4 = this.g;
                if (str4 == null) {
                    if (hc4.d() == null) {
                        return true;
                    }
                } else if (str4.equals(hc4.d())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public gc4.a f() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public long g() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = ((((str == null ? 0 : str.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003;
        String str2 = this.c;
        int hashCode2 = (hashCode ^ (str2 == null ? 0 : str2.hashCode())) * 1000003;
        String str3 = this.d;
        int hashCode3 = str3 == null ? 0 : str3.hashCode();
        long j = this.e;
        long j2 = this.f;
        int i2 = (((((hashCode2 ^ hashCode3) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003;
        String str4 = this.g;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return i2 ^ i;
    }

    @DexIgnore
    @Override // com.fossil.hc4
    public hc4.a m() {
        return new b(this);
    }

    @DexIgnore
    public String toString() {
        return "PersistedInstallationEntry{firebaseInstallationId=" + this.a + ", registrationStatus=" + this.b + ", authToken=" + this.c + ", refreshToken=" + this.d + ", expiresInSecs=" + this.e + ", tokenCreationEpochInSecs=" + this.f + ", fisError=" + this.g + "}";
    }

    @DexIgnore
    public ec4(String str, gc4.a aVar, String str2, String str3, long j, long j2, String str4) {
        this.a = str;
        this.b = aVar;
        this.c = str2;
        this.d = str3;
        this.e = j;
        this.f = j2;
        this.g = str4;
    }
}
