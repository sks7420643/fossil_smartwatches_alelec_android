package com.fossil;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import com.fossil.p1;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e1 extends ActionMode implements p1.a {
    @DexIgnore
    public Context c;
    @DexIgnore
    public ActionBarContextView d;
    @DexIgnore
    public ActionMode.Callback e;
    @DexIgnore
    public WeakReference<View> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public p1 h;

    @DexIgnore
    public e1(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.c = context;
        this.d = actionBarContextView;
        this.e = callback;
        p1 p1Var = new p1(actionBarContextView.getContext());
        p1Var.c(1);
        this.h = p1Var;
        p1Var.a(this);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void a(CharSequence charSequence) {
        this.d.setSubtitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void b(CharSequence charSequence) {
        this.d.setTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public Menu c() {
        return this.h;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public MenuInflater d() {
        return new g1(this.d.getContext());
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public CharSequence e() {
        return this.d.getSubtitle();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public CharSequence g() {
        return this.d.getTitle();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void i() {
        this.e.b(this, this.h);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public boolean j() {
        return this.d.c();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void a(int i) {
        a((CharSequence) this.c.getString(i));
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void b(int i) {
        b(this.c.getString(i));
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void a(boolean z) {
        super.a(z);
        this.d.setTitleOptional(z);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public View b() {
        WeakReference<View> weakReference = this.f;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void a(View view) {
        this.d.setCustomView(view);
        this.f = view != null ? new WeakReference<>(view) : null;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void a() {
        if (!this.g) {
            this.g = true;
            this.d.sendAccessibilityEvent(32);
            this.e.a(this);
        }
    }

    @DexIgnore
    @Override // com.fossil.p1.a
    public boolean a(p1 p1Var, MenuItem menuItem) {
        return this.e.a(this, menuItem);
    }

    @DexIgnore
    @Override // com.fossil.p1.a
    public void a(p1 p1Var) {
        i();
        this.d.e();
    }
}
