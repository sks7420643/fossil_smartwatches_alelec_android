package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jo6 extends go5 implements io6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public ho6 f;
    @DexIgnore
    public qw6<w55> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jo6.i;
        }

        @DexIgnore
        public final jo6 b() {
            return new jo6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ jo6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ w55 a;

            @DexIgnore
            public a(w55 w55) {
                this.a = w55;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.a.K;
                ee7.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.a.L;
                ee7.a((Object) flexibleButton, "it.tvSignup");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, jo6 jo6) {
            this.a = view;
            this.b = jo6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                w55 w55 = (w55) jo6.a(this.b).a();
                if (w55 != null) {
                    try {
                        FlexibleTextView flexibleTextView = w55.K;
                        ee7.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = w55.L;
                        ee7.a((Object) flexibleButton, "it.tvSignup");
                        flexibleButton.setVisibility(8);
                        i97 i97 = i97.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = jo6.j.a();
                        local.d(a2, "onCreateView - e=" + e);
                        i97 i972 = i97.a;
                    }
                }
            } else {
                w55 w552 = (w55) jo6.a(this.b).a();
                if (w552 != null) {
                    try {
                        ee7.a((Object) w552, "it");
                        w552.d().postDelayed(new a(w552), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = jo6.j.a();
                        local2.d(a3, "onCreateView - e=" + e2);
                        i97 i973 = i97.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public c(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public d(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                SignUpActivity.a aVar = SignUpActivity.D;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public e(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public f(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(jo6.j.a(), "Password DONE key, trigger login flow");
            this.a.g1();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ w55 a;
        @DexIgnore
        public /* final */ /* synthetic */ jo6 b;

        @DexIgnore
        public g(w55 w55, jo6 jo6) {
            this.a = w55;
            this.b = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.a.w;
            ee7.a((Object) flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.a.v;
            ee7.a((Object) flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.b.g1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public h(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.a.f1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public i(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            this.a.f1().a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public j(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.a.f1().b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public k(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public l(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public m(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public n(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jo6 a;

        @DexIgnore
        public o(jo6 jo6) {
            this.a = jo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().l();
        }
    }

    /*
    static {
        String simpleName = jo6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "LoginFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(jo6 jo6) {
        qw6<w55> qw6 = jo6.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void G() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        qw6<w55> qw6 = this.g;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(false);
            }
            qw6<w55> qw62 = this.g;
            if (qw62 != null) {
                w55 a3 = qw62.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void L(boolean z) {
        qw6<w55> qw6 = this.g;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ConstraintLayout constraintLayout = a2.D;
                ee7.a((Object) constraintLayout, "it.ivWeibo");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.C;
                ee7.a((Object) constraintLayout2, "it.ivWechat");
                constraintLayout2.setVisibility(0);
                ImageView imageView = a2.B;
                ee7.a((Object) imageView, "it.ivGoogle");
                imageView.setVisibility(8);
                ImageView imageView2 = a2.A;
                ee7.a((Object) imageView2, "it.ivFacebook");
                imageView2.setVisibility(8);
                return;
            }
            ConstraintLayout constraintLayout3 = a2.D;
            ee7.a((Object) constraintLayout3, "it.ivWeibo");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.C;
            ee7.a((Object) constraintLayout4, "it.ivWechat");
            constraintLayout4.setVisibility(8);
            ImageView imageView3 = a2.B;
            ee7.a((Object) imageView3, "it.ivGoogle");
            imageView3.setVisibility(0);
            ImageView imageView4 = a2.A;
            ee7.a((Object) imageView4, "it.ivFacebook");
            imageView4.setVisibility(0);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void N(String str) {
        ee7.b(str, Constants.EMAIL);
        Context context = getContext();
        if (context != null) {
            ForgotPasswordActivity.a aVar = ForgotPasswordActivity.z;
            ee7.a((Object) context, "it");
            aVar.a(context, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void Y() {
        if (isActive()) {
            qw6<w55> qw6 = this.g;
            if (qw6 != null) {
                w55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setError(" ");
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.w;
                    ee7.a((Object) flexibleTextInputLayout2, "it.inputPassword");
                    flexibleTextInputLayout2.setError(ig5.a(PortfolioApp.g0.c(), 2131886858));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void c(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return i;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void f() {
        a();
    }

    @DexIgnore
    public final ho6 f1() {
        ho6 ho6 = this.f;
        if (ho6 != null) {
            return ho6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void g() {
        String string = getString(2131886857);
        ee7.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        W(string);
    }

    @DexIgnore
    public final void g1() {
        qw6<w55> qw6 = this.g;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (a2 != null) {
                ho6 ho6 = this.f;
                if (ho6 != null) {
                    FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                    ee7.a((Object) flexibleTextInputEditText, "etEmail");
                    String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                    FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                    ee7.a((Object) flexibleTextInputEditText2, "etPassword");
                    ho6.a(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                    return;
                }
                ee7.d("mPresenter");
                throw null;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void i() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void o0() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        qw6<w55> qw6 = this.g;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (!(a2 == null || (flexibleButton2 = a2.s) == null)) {
                flexibleButton2.setEnabled(true);
            }
            qw6<w55> qw62 = this.g;
            if (qw62 != null) {
                w55 a3 = qw62.a();
                if (a3 != null && (flexibleButton = a3.s) != null) {
                    flexibleButton.a("flexible_button_primary");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e(i, "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Apple Auth Info = " + signUpSocialAuth);
            ho6 ho6 = this.f;
            if (ho6 != null) {
                ee7.a((Object) signUpSocialAuth, "authCode");
                ho6.a(signUpSocialAuth);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        w55 w55 = (w55) qb.a(layoutInflater, 2131558620, viewGroup, false, a1());
        ee7.a((Object) w55, "binding");
        View d2 = w55.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        qw6<w55> qw6 = new qw6<>(this, w55);
        this.g = qw6;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ho6 ho6 = this.f;
        if (ho6 != null) {
            ho6.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ho6 ho6 = this.f;
        if (ho6 != null) {
            ho6.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<w55> qw6 = this.g;
        if (qw6 != null) {
            w55 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new g(a2, this));
                a2.t.addTextChangedListener(new h(this));
                a2.t.setOnFocusChangeListener(new i(this));
                a2.u.addTextChangedListener(new j(this));
                a2.z.setOnClickListener(new k(this));
                a2.A.setOnClickListener(new l(this));
                a2.B.setOnClickListener(new m(this));
                a2.x.setOnClickListener(new n(this));
                a2.C.setOnClickListener(new o(this));
                a2.D.setOnClickListener(new c(this));
                a2.L.setOnClickListener(new d(this));
                a2.J.setOnClickListener(new e(this));
                a2.u.setOnEditorActionListener(new f(this));
            }
            V("login_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void q() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = rw6.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.C;
            ee7.a((Object) activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void b(int i2, String str) {
        ee7.b(str, "errorMessage");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.io6
    public void a(boolean z, String str) {
        FlexibleTextInputLayout flexibleTextInputLayout;
        ee7.b(str, "errorMessage");
        if (isActive()) {
            qw6<w55> qw6 = this.g;
            if (qw6 != null) {
                w55 a2 = qw6.a();
                if (a2 != null && (flexibleTextInputLayout = a2.v) != null) {
                    ee7.a((Object) flexibleTextInputLayout, "it");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    flexibleTextInputLayout.setError(str);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ho6 ho6) {
        ee7.b(ho6, "presenter");
        this.f = ho6;
    }
}
