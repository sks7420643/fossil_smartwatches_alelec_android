package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rj0 {
    @DexIgnore
    public /* synthetic */ rj0(zd7 zd7) {
    }

    @DexIgnore
    public final pl0 a(byte b) {
        pl0 pl0;
        pl0[] values = pl0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                pl0 = null;
                break;
            }
            pl0 = values[i];
            if (pl0.b == b) {
                break;
            }
            i++;
        }
        return pl0 != null ? pl0 : pl0.UNKNOWN;
    }
}
