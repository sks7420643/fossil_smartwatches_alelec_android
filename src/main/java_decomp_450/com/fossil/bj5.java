package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj5<T> extends zi5<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public bj5(T t, boolean z) {
        super(null);
        this.a = t;
        this.b = z;
    }

    @DexIgnore
    public final T a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bj5)) {
            return false;
        }
        bj5 bj5 = (bj5) obj;
        return ee7.a(this.a, bj5.a) && this.b == bj5.b;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.a;
        int hashCode = (t != null ? t.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + ((Object) this.a) + ", isFromCache=" + this.b + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bj5(Object obj, boolean z, int i, zd7 zd7) {
        this(obj, (i & 2) != 0 ? false : z);
    }
}
