package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.gx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo5 extends go5 implements hq6 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public gq6 f;
    @DexIgnore
    public qw6<k55> g;
    @DexIgnore
    public jo5 h;
    @DexIgnore
    public sq6 i;
    @DexIgnore
    public yz6 j;
    @DexIgnore
    public Integer p;
    @DexIgnore
    public /* final */ String q; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final oo5 a() {
            return new oo5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public b(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            oo5.a(this.a).e(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public c(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            oo5.a(this.a).d(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public d(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            oo5.a(this.a).c(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public e(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            oo5.a(this.a).b(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public f(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            oo5.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public g(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            oo5.a(this.a).a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public h(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            oo5.a(this.a).b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public i(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            oo5.a(this.a).c(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public j(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ k55 a;
        @DexIgnore
        public /* final */ /* synthetic */ oo5 b;

        @DexIgnore
        public k(k55 k55, oo5 oo5) {
            this.a = k55;
            this.b = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.v;
            ee7.a((Object) flexibleTextInputEditText, "binding.etBirthday");
            Editable text = flexibleTextInputEditText.getText();
            if (text == null || text.length() == 0) {
                oo5.a(this.b).k();
            } else {
                oo5.a(this.b).j();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public l(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            oo5.a(this.a).a(fb5.MALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public m(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            oo5.a(this.a).a(fb5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public n(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            oo5.a(this.a).a(fb5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public o(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            oo5.a(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements zd<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ oo5 a;

        @DexIgnore
        public p(oo5 oo5) {
            this.a = oo5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                Calendar i = oo5.a(this.a).i();
                i.setTime(date);
                oo5.a(this.a).a(date, i);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ gq6 a(oo5 oo5) {
        gq6 gq6 = oo5.f;
        if (gq6 != null) {
            return gq6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void A(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null && (rTLImageView = a2.Q) != null) {
                    ee7.a((Object) rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void E(boolean z) {
        RTLImageView rTLImageView;
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null && (rTLImageView = a2.P) != null) {
                    ee7.a((Object) rTLImageView, "it");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void P(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        ee7.b(str, "birthdate");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null && (flexibleTextInputEditText = a2.v) != null) {
                    flexibleTextInputEditText.setText(str);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void T0() {
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    ee7.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.z;
                    ee7.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.z;
                    ee7.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(false);
                    a2.z.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void V() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void b(boolean z, String str) {
        ee7.b(str, "message");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.J;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputBirthday");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.J;
                    ee7.a((Object) flexibleTextInputLayout2, "it.inputBirthday");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.N;
                    ee7.a((Object) rTLImageView, "it.ivCheckedBirthday");
                    rTLImageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void c(Spanned spanned) {
        ee7.b(spanned, "message");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.D;
                    ee7.a((Object) flexibleTextView, "binding.ftvFive");
                    flexibleTextView.setText(spanned);
                    Integer num = this.p;
                    if (num != null) {
                        a2.D.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void d(Spanned spanned) {
        ee7.b(spanned, "message");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.H;
                    ee7.a((Object) flexibleTextView, "binding.ftvThree");
                    flexibleTextView.setText(spanned);
                    Integer num = this.p;
                    if (num != null) {
                        a2.H.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void e() {
        DashBar dashBar;
        qw6<k55> qw6 = this.g;
        if (qw6 != null) {
            k55 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.S) != null) {
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.b(dashBar, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void f() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void g() {
        if (isActive()) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886907);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            W(a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void h(int i2, String str) {
        ee7.b(str, "errorMessage");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<k55> qw6 = new qw6<>(this, (k55) qb.a(layoutInflater, 2131558611, viewGroup, false, a1()));
        this.g = qw6;
        if (qw6 != null) {
            k55 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        gq6 gq6 = this.f;
        if (gq6 != null) {
            gq6.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gq6 gq6 = this.f;
        if (gq6 != null) {
            gq6.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        he a2 = je.a(requireActivity()).a(yz6.class);
        ee7.a((Object) a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.j = (yz6) a2;
        jo5 jo5 = (jo5) getChildFragmentManager().b(jo5.t.a());
        this.h = jo5;
        if (jo5 == null) {
            this.h = jo5.t.b();
        }
        tj4 f2 = PortfolioApp.g0.c().f();
        jo5 jo52 = this.h;
        if (jo52 != null) {
            f2.a(new qq6(jo52)).a(this);
            yz6 yz6 = this.j;
            if (yz6 != null) {
                yz6.a().a(getViewLifecycleOwner(), new p(this));
                qw6<k55> qw6 = this.g;
                if (qw6 != null) {
                    k55 a3 = qw6.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.q)) {
                            Integer valueOf = Integer.valueOf(Color.parseColor(this.q));
                            this.p = valueOf;
                            if (valueOf != null) {
                                int intValue = valueOf.intValue();
                                FlexibleCheckBox flexibleCheckBox = a3.u;
                                ee7.a((Object) flexibleCheckBox, "binding.cbTwo");
                                flexibleCheckBox.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox2 = a3.u;
                                ee7.a((Object) flexibleCheckBox2, "binding.cbTwo");
                                flexibleCheckBox2.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox3 = a3.s;
                                ee7.a((Object) flexibleCheckBox3, "binding.cbOne");
                                flexibleCheckBox3.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox4 = a3.s;
                                ee7.a((Object) flexibleCheckBox4, "binding.cbOne");
                                flexibleCheckBox4.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox5 = a3.t;
                                ee7.a((Object) flexibleCheckBox5, "binding.cbThree");
                                flexibleCheckBox5.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox6 = a3.t;
                                ee7.a((Object) flexibleCheckBox6, "binding.cbThree");
                                flexibleCheckBox6.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox7 = a3.q;
                                ee7.a((Object) flexibleCheckBox7, "binding.cbFive");
                                flexibleCheckBox7.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox8 = a3.q;
                                ee7.a((Object) flexibleCheckBox8, "binding.cbFive");
                                flexibleCheckBox8.setButtonTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox9 = a3.r;
                                ee7.a((Object) flexibleCheckBox9, "binding.cbFour");
                                flexibleCheckBox9.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                FlexibleCheckBox flexibleCheckBox10 = a3.r;
                                ee7.a((Object) flexibleCheckBox10, "binding.cbFour");
                                flexibleCheckBox10.setButtonTintList(ColorStateList.valueOf(intValue));
                            }
                        }
                        a3.w.addTextChangedListener(new g(this));
                        a3.x.addTextChangedListener(new h(this));
                        a3.y.addTextChangedListener(new i(this));
                        a3.R.setOnClickListener(new j(this));
                        a3.v.setOnClickListener(new k(a3, this));
                        a3.B.setOnClickListener(new l(this));
                        a3.A.setOnClickListener(new m(this));
                        a3.C.setOnClickListener(new n(this));
                        a3.u.setOnCheckedChangeListener(new o(this));
                        a3.s.setOnCheckedChangeListener(new b(this));
                        a3.t.setOnCheckedChangeListener(new c(this));
                        a3.r.setOnCheckedChangeListener(new d(this));
                        a3.q.setOnCheckedChangeListener(new e(this));
                        a3.z.setOnClickListener(new f(this));
                        FlexibleTextView flexibleTextView = a3.G;
                        ee7.a((Object) flexibleTextView, "binding.ftvOne");
                        flexibleTextView.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView2 = a3.H;
                        ee7.a((Object) flexibleTextView2, "binding.ftvThree");
                        flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView3 = a3.E;
                        ee7.a((Object) flexibleTextView3, "binding.ftvFour");
                        flexibleTextView3.setMovementMethod(new LinkMovementMethod());
                        FlexibleTextView flexibleTextView4 = a3.D;
                        ee7.a((Object) flexibleTextView4, "binding.ftvFive");
                        flexibleTextView4.setMovementMethod(new LinkMovementMethod());
                        if (tm4.a.a().i()) {
                            FlexibleCheckBox flexibleCheckBox11 = a3.s;
                            ee7.a((Object) flexibleCheckBox11, "binding.cbOne");
                            flexibleCheckBox11.setVisibility(8);
                            FlexibleTextView flexibleTextView5 = a3.G;
                            ee7.a((Object) flexibleTextView5, "binding.ftvOne");
                            flexibleTextView5.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox12 = a3.u;
                            ee7.a((Object) flexibleCheckBox12, "binding.cbTwo");
                            flexibleCheckBox12.setVisibility(8);
                            FlexibleTextView flexibleTextView6 = a3.I;
                            ee7.a((Object) flexibleTextView6, "binding.ftvTwo");
                            flexibleTextView6.setVisibility(8);
                            FlexibleCheckBox flexibleCheckBox13 = a3.t;
                            ee7.a((Object) flexibleCheckBox13, "binding.cbThree");
                            flexibleCheckBox13.setVisibility(0);
                            FlexibleTextView flexibleTextView7 = a3.H;
                            ee7.a((Object) flexibleTextView7, "binding.ftvThree");
                            flexibleTextView7.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox14 = a3.r;
                            ee7.a((Object) flexibleCheckBox14, "binding.cbFour");
                            flexibleCheckBox14.setVisibility(0);
                            FlexibleTextView flexibleTextView8 = a3.E;
                            ee7.a((Object) flexibleTextView8, "binding.ftvFour");
                            flexibleTextView8.setVisibility(0);
                            FlexibleCheckBox flexibleCheckBox15 = a3.q;
                            ee7.a((Object) flexibleCheckBox15, "binding.cbFive");
                            flexibleCheckBox15.setVisibility(0);
                            FlexibleTextView flexibleTextView9 = a3.D;
                            ee7.a((Object) flexibleTextView9, "binding.ftvFive");
                            flexibleTextView9.setVisibility(0);
                        }
                    }
                    V("profile_setup_view");
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mUserBirthDayViewModel");
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void r0() {
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    ee7.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.z;
                    ee7.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.z;
                    ee7.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(true);
                    a2.z.a("flexible_button_primary");
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(gq6 gq6) {
        ee7.b(gq6, "presenter");
        this.f = gq6;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(boolean z, boolean z2, String str) {
        int i2;
        ee7.b(str, "errorMessage");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                    ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                    RTLImageView rTLImageView = a2.O;
                    ee7.a((Object) rTLImageView, "it.ivCheckedEmail");
                    if (z) {
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        ee7.a((Object) flexibleTextInputLayout3, "it.inputEmail");
                        if (flexibleTextInputLayout3.getVisibility() == 0) {
                            i2 = 0;
                            rTLImageView.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    rTLImageView.setVisibility(i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void b(Spanned spanned) {
        ee7.b(spanned, "message");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.E;
                    ee7.a((Object) flexibleTextView, "binding.ftvFour");
                    flexibleTextView.setText(spanned);
                    Integer num = this.p;
                    if (num != null) {
                        a2.E.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void c(SignUpEmailAuth signUpEmailAuth) {
        ee7.b(signUpEmailAuth, "auth");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    ee7.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(ig5.a(PortfolioApp.g0.c(), 2131886914));
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(8);
                    RTLImageView rTLImageView = a2.O;
                    ee7.a((Object) rTLImageView, "it.ivCheckedEmail");
                    rTLImageView.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.w.setText(signUpEmailAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        ee7.a((Object) flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        ee7.a((Object) flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(Bundle bundle) {
        ee7.b(bundle, "data");
        jo5 jo5 = this.h;
        if (jo5 != null) {
            jo5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            jo5.show(childFragmentManager, jo5.t.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(Spanned spanned) {
        ee7.b(spanned, "message");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.G;
                    ee7.a((Object) flexibleTextView, "binding.ftvOne");
                    flexibleTextView.setText(spanned);
                    Integer num = this.p;
                    if (num != null) {
                        a2.G.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(SignUpSocialAuth signUpSocialAuth) {
        ee7.b(signUpSocialAuth, "auth");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    ee7.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(ig5.a(PortfolioApp.g0.c(), 2131886914));
                    a2.x.setText(signUpSocialAuth.getFirstName());
                    a2.y.setText(signUpSocialAuth.getLastName());
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.K;
                    ee7.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.w.setText(signUpSocialAuth.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        ee7.a((Object) flexibleTextInputEditText, "it.etEmail");
                        flexibleTextInputEditText.setKeyListener(null);
                        FlexibleTextInputLayout flexibleTextInputLayout2 = a2.K;
                        ee7.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                        flexibleTextInputLayout2.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout3 = a2.K;
                        ee7.a((Object) flexibleTextInputLayout3, "it.inputEmail");
                        flexibleTextInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(MFUser mFUser) {
        Date date;
        ee7.b(mFUser, "user");
        if (isActive()) {
            T0();
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.z;
                    ee7.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(ig5.a(PortfolioApp.g0.c(), 2131886897));
                    a(fb5.Companion.a(mFUser.getGender()));
                    if (!mh7.a((CharSequence) mFUser.getEmail())) {
                        a2.w.setText(mFUser.getEmail());
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.w;
                        ee7.a((Object) flexibleTextInputEditText, "it.etEmail");
                        a((TextView) flexibleTextInputEditText);
                    }
                    if (!mh7.a((CharSequence) mFUser.getFirstName())) {
                        a2.x.setText(mFUser.getFirstName());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.x;
                        ee7.a((Object) flexibleTextInputEditText2, "it.etFirstName");
                        a((TextView) flexibleTextInputEditText2);
                    }
                    if (!mh7.a((CharSequence) mFUser.getLastName())) {
                        a2.y.setText(mFUser.getLastName());
                        FlexibleTextInputEditText flexibleTextInputEditText3 = a2.y;
                        ee7.a((Object) flexibleTextInputEditText3, "it.etLastName");
                        a((TextView) flexibleTextInputEditText3);
                    }
                    if (!mh7.a((CharSequence) mFUser.getBirthday())) {
                        FlexibleTextInputEditText flexibleTextInputEditText4 = a2.v;
                        ee7.a((Object) flexibleTextInputEditText4, "it.etBirthday");
                        a((TextView) flexibleTextInputEditText4);
                        try {
                            SimpleDateFormat simpleDateFormat = zd5.a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                gq6 gq6 = this.f;
                                if (gq6 != null) {
                                    Calendar i2 = gq6.i();
                                    i2.setTime(date);
                                    gq6 gq62 = this.f;
                                    if (gq62 == null) {
                                        ee7.d("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        gq62.a(date, i2);
                                    } else {
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    ee7.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e2);
                            date = new Date();
                        }
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(TextView textView) {
        Drawable c2 = v6.c(PortfolioApp.g0.c(), 2131230835);
        textView.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099707));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(c2);
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void a(fb5 fb5) {
        ee7.b(fb5, "gender");
        if (isActive()) {
            qw6<k55> qw6 = this.g;
            if (qw6 != null) {
                k55 a2 = qw6.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.g0.c();
                    int a3 = v6.a(c2, 2131099972);
                    int a4 = v6.a(c2, 2131100361);
                    Drawable c3 = v6.c(c2, 2131230835);
                    Drawable c4 = v6.c(c2, 2131230829);
                    a2.C.setTextColor(a3);
                    a2.A.setTextColor(a3);
                    a2.B.setTextColor(a3);
                    FlexibleButton flexibleButton = a2.C;
                    ee7.a((Object) flexibleButton, "it.fbOther");
                    flexibleButton.setBackground(c3);
                    FlexibleButton flexibleButton2 = a2.A;
                    ee7.a((Object) flexibleButton2, "it.fbFemale");
                    flexibleButton2.setBackground(c3);
                    FlexibleButton flexibleButton3 = a2.B;
                    ee7.a((Object) flexibleButton3, "it.fbMale");
                    flexibleButton3.setBackground(c3);
                    a2.B.a("flexible_button_secondary");
                    a2.A.a("flexible_button_secondary");
                    a2.C.a("flexible_button_secondary");
                    int i2 = po5.a[fb5.ordinal()];
                    if (i2 == 1) {
                        a2.B.setTextColor(a4);
                        FlexibleButton flexibleButton4 = a2.B;
                        ee7.a((Object) flexibleButton4, "it.fbMale");
                        flexibleButton4.setBackground(c4);
                        a2.B.a("flexible_button_primary");
                    } else if (i2 == 2) {
                        a2.A.setTextColor(a4);
                        FlexibleButton flexibleButton5 = a2.A;
                        ee7.a((Object) flexibleButton5, "it.fbFemale");
                        flexibleButton5.setBackground(c4);
                        a2.A.a("flexible_button_primary");
                    } else if (i2 == 3) {
                        a2.C.setTextColor(a4);
                        FlexibleButton flexibleButton6 = a2.C;
                        ee7.a((Object) flexibleButton6, "it.fbOther");
                        flexibleButton6.setBackground(c4);
                        a2.C.a("flexible_button_primary");
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
    }
}
