package com.fossil;

import android.content.Context;
import android.view.View;
import java.io.File;
import okhttp3.Cache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du {
    /*
    static {
        new du();
    }
    */

    @DexIgnore
    public static final Cache a(Context context) {
        ee7.b(context, "context");
        File b = ku.a.b(context);
        return new Cache(b, ku.a.a(b));
    }

    @DexIgnore
    public static final void a(View view) {
        ee7.b(view, "view");
        iu.a(view).a();
    }
}
