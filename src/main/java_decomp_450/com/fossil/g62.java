package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g62 {
    @DexIgnore
    public static w02 a(Status status) {
        if (status.w()) {
            return new f12(status);
        }
        return new w02(status);
    }
}
