package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TabLayout A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ DashBar C;
    @DexIgnore
    public /* final */ FlexibleProgressBar D;
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public /* final */ ViewPager2 F;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public s65(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, TabLayout tabLayout, ImageView imageView, DashBar dashBar, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout3, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleButton;
        this.t = flexibleButton2;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = flexibleTextView4;
        this.y = flexibleTextView5;
        this.z = flexibleTextView6;
        this.A = tabLayout;
        this.B = imageView;
        this.C = dashBar;
        this.D = flexibleProgressBar;
        this.E = constraintLayout3;
        this.F = viewPager2;
    }
}
