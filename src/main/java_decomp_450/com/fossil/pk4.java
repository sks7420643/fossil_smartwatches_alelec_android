package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk4 implements Factory<jh5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public pk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static pk4 a(wj4 wj4) {
        return new pk4(wj4);
    }

    @DexIgnore
    public static jh5 b(wj4 wj4) {
        jh5 k = wj4.k();
        c87.a(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public jh5 get() {
        return b(this.a);
    }
}
