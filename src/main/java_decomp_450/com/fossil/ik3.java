package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nm3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 b;

    @DexIgnore
    public ik3(ek3 ek3, nm3 nm3) {
        this.b = ek3;
        this.a = nm3;
    }

    @DexIgnore
    public final void run() {
        bg3 d = this.b.d;
        if (d == null) {
            this.b.e().t().a("Failed to reset data on the service: not connected to service");
            return;
        }
        try {
            d.b(this.a);
        } catch (RemoteException e) {
            this.b.e().t().a("Failed to reset data on the service: remote exception", e);
        }
        this.b.J();
    }
}
