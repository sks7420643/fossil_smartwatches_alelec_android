package com.fossil;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kr5 extends Animation implements Animation.AnimationListener {
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public /* final */ CropOverlayView b;
    @DexIgnore
    public /* final */ float[] c; // = new float[8];
    @DexIgnore
    public /* final */ float[] d; // = new float[8];
    @DexIgnore
    public /* final */ RectF e; // = new RectF();
    @DexIgnore
    public /* final */ RectF f; // = new RectF();
    @DexIgnore
    public /* final */ float[] g; // = new float[9];
    @DexIgnore
    public /* final */ float[] h; // = new float[9];
    @DexIgnore
    public /* final */ RectF i; // = new RectF();
    @DexIgnore
    public /* final */ float[] j; // = new float[8];
    @DexIgnore
    public /* final */ float[] p; // = new float[9];

    @DexIgnore
    public kr5(ImageView imageView, CropOverlayView cropOverlayView) {
        this.a = imageView;
        this.b = cropOverlayView;
        setDuration(300);
        setFillAfter(true);
        setInterpolator(new AccelerateDecelerateInterpolator());
        setAnimationListener(this);
    }

    @DexIgnore
    public void a(float[] fArr, Matrix matrix) {
        System.arraycopy(fArr, 0, this.d, 0, 8);
        this.f.set(this.b.getCropWindowRect());
        matrix.getValues(this.h);
    }

    @DexIgnore
    public void applyTransformation(float f2, Transformation transformation) {
        float[] fArr;
        RectF rectF = this.i;
        RectF rectF2 = this.e;
        float f3 = rectF2.left;
        RectF rectF3 = this.f;
        rectF.left = f3 + ((rectF3.left - f3) * f2);
        float f4 = rectF2.top;
        rectF.top = f4 + ((rectF3.top - f4) * f2);
        float f5 = rectF2.right;
        rectF.right = f5 + ((rectF3.right - f5) * f2);
        float f6 = rectF2.bottom;
        rectF.bottom = f6 + ((rectF3.bottom - f6) * f2);
        this.b.setCropWindowRect(rectF);
        int i2 = 0;
        int i3 = 0;
        while (true) {
            fArr = this.j;
            if (i3 >= fArr.length) {
                break;
            }
            float[] fArr2 = this.c;
            fArr[i3] = fArr2[i3] + ((this.d[i3] - fArr2[i3]) * f2);
            i3++;
        }
        this.b.a(fArr, this.a.getWidth(), this.a.getHeight());
        while (true) {
            float[] fArr3 = this.p;
            if (i2 < fArr3.length) {
                float[] fArr4 = this.g;
                fArr3[i2] = fArr4[i2] + ((this.h[i2] - fArr4[i2]) * f2);
                i2++;
            } else {
                Matrix imageMatrix = this.a.getImageMatrix();
                imageMatrix.setValues(this.p);
                this.a.setImageMatrix(imageMatrix);
                this.a.invalidate();
                this.b.invalidate();
                return;
            }
        }
    }

    @DexIgnore
    public void b(float[] fArr, Matrix matrix) {
        reset();
        System.arraycopy(fArr, 0, this.c, 0, 8);
        this.e.set(this.b.getCropWindowRect());
        matrix.getValues(this.g);
    }

    @DexIgnore
    public void onAnimationEnd(Animation animation) {
        this.a.clearAnimation();
    }

    @DexIgnore
    public void onAnimationRepeat(Animation animation) {
    }

    @DexIgnore
    public void onAnimationStart(Animation animation) {
    }
}
