package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.fp5;
import com.fossil.gx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yq6 extends mr6 implements qr6, fp5.b, cy6.g {
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<s45> g;
    @DexIgnore
    public rr6 h;
    @DexIgnore
    public fp5 i;
    @DexIgnore
    public vo5 j;
    @DexIgnore
    public List<r87<ShineDevice, String>> p; // = new ArrayList();
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = yq6.class.getSimpleName();
            ee7.a((Object) simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final yq6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            yq6 yq6 = new yq6();
            yq6.setArguments(bundle);
            return yq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ s45 a;
        @DexIgnore
        public /* final */ /* synthetic */ yq6 b;

        @DexIgnore
        public b(s45 s45, yq6 yq6, se7 se7) {
            this.a = s45;
            this.b = yq6;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b.getActivity(), 2130772008);
            this.a.s.startAnimation(loadAnimation);
            this.a.w.startAnimation(loadAnimation);
            this.a.q.startAnimation(loadAnimation);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yq6 a;

        @DexIgnore
        public c(yq6 yq6) {
            this.a = yq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            yq6.b(this.a).a(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yq6 a;

        @DexIgnore
        public d(yq6 yq6) {
            this.a = yq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            yq6.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ yq6 a;

        @DexIgnore
        public e(yq6 yq6) {
            this.a = yq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (yq6.c(this.a).d() < this.a.p.size()) {
                yq6 yq6 = this.a;
                yq6.a((ShineDevice) ((r87) yq6.p.get(yq6.c(this.a).d())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ rr6 b(yq6 yq6) {
        rr6 rr6 = yq6.h;
        if (rr6 != null) {
            return rr6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ vo5 c(yq6 yq6) {
        vo5 vo5 = yq6.j;
        if (vo5 != null) {
            return vo5;
        }
        ee7.d("mSnapHelper");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void l(List<r87<ShineDevice, String>> list) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        ee7.b(list, "shineDeviceList");
        this.p = list;
        fp5 fp5 = this.i;
        if (fp5 != null) {
            fp5.a(list);
            int size = this.p.size();
            if (size <= 1) {
                qw6<s45> qw6 = this.g;
                if (qw6 != null) {
                    s45 a2 = qw6.a();
                    if (a2 != null && (flexibleTextView2 = a2.s) != null) {
                        we7 we7 = we7.a;
                        Locale locale = Locale.US;
                        ee7.a((Object) locale, "Locale.US");
                        String a3 = ig5.a(getActivity(), 2131886889);
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        String format = String.format(locale, a3, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView2.setText(format);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
            qw6<s45> qw62 = this.g;
            if (qw62 != null) {
                s45 a4 = qw62.a();
                if (a4 != null && (flexibleTextView = a4.s) != null) {
                    we7 we72 = we7.a;
                    Locale locale2 = Locale.US;
                    ee7.a((Object) locale2, "Locale.US");
                    String a5 = ig5.a(getActivity(), 2131886889);
                    ee7.a((Object) a5, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format2 = String.format(locale2, a5, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                    ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView.setText(format2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        se7 se7 = new se7();
        se7.element = null;
        if (z) {
            qw6<s45> qw6 = this.g;
            if (qw6 != null) {
                s45 a2 = qw6.a();
                if (!(a2 == null || getActivity() == null)) {
                    T t = (T) AnimationUtils.loadAnimation(getActivity(), 2130771999);
                    se7.element = t;
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAnimationListener(new b(a2, this, se7));
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
        return se7.element;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        s45 s45 = (s45) qb.a(layoutInflater, 2131558602, viewGroup, false, a1());
        this.g = new qw6<>(this, s45);
        ee7.a((Object) s45, "binding");
        return s45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.mr6, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        FlexibleButton flexibleButton;
        FlexibleTextView flexibleTextView;
        RecyclerView recyclerView;
        FlexibleTextView flexibleTextView2;
        RTLImageView rTLImageView;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<s45> qw6 = this.g;
        if (qw6 != null) {
            s45 a2 = qw6.a();
            if (!(a2 == null || (rTLImageView = a2.t) == null)) {
                rTLImageView.setOnClickListener(new c(this));
            }
            qw6<s45> qw62 = this.g;
            if (qw62 != null) {
                s45 a3 = qw62.a();
                if (!(a3 == null || (flexibleTextView2 = a3.s) == null)) {
                    we7 we7 = we7.a;
                    Locale locale = Locale.US;
                    ee7.a((Object) locale, "Locale.US");
                    String a4 = ig5.a(getContext(), 2131886889);
                    ee7.a((Object) a4, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format = String.format(locale, a4, Arrays.copyOf(new Object[]{Integer.valueOf(this.p.size())}, 1));
                    ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView2.setText(format);
                }
                iw a5 = aw.a(this);
                ee7.a((Object) a5, "Glide.with(this)");
                fp5 fp5 = new fp5(a5, this);
                this.i = fp5;
                fp5.a(this.p);
                qw6<s45> qw63 = this.g;
                if (qw63 != null) {
                    s45 a6 = qw63.a();
                    if (!(a6 == null || (recyclerView = a6.w) == null)) {
                        ee7.a((Object) recyclerView, "it");
                        recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.g0.c().getApplicationContext(), 0, false));
                        recyclerView.addItemDecoration(new rz6());
                        recyclerView.setAdapter(this.i);
                        vo5 vo5 = new vo5();
                        this.j = vo5;
                        vo5.a(recyclerView);
                    }
                    qw6<s45> qw64 = this.g;
                    if (qw64 != null) {
                        s45 a7 = qw64.a();
                        if (!(a7 == null || (flexibleTextView = a7.r) == null)) {
                            flexibleTextView.setOnClickListener(new d(this));
                        }
                        qw6<s45> qw65 = this.g;
                        if (qw65 != null) {
                            s45 a8 = qw65.a();
                            if (!(a8 == null || (flexibleButton = a8.q) == null)) {
                                flexibleButton.setOnClickListener(new e(this));
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                qw6<s45> qw66 = this.g;
                                if (qw66 != null) {
                                    s45 a9 = qw66.a();
                                    if (a9 != null && (dashBar = a9.u) != null) {
                                        gx6.a aVar = gx6.a;
                                        ee7.a((Object) dashBar, "this");
                                        aVar.b(dashBar, z, 500);
                                        return;
                                    }
                                    return;
                                }
                                ee7.d("mBinding");
                                throw null;
                            }
                            return;
                        }
                        ee7.d("mBinding");
                        throw null;
                    }
                    ee7.d("mBinding");
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rr6 rr6) {
        ee7.b(rr6, "presenter");
        this.h = rr6;
    }

    @DexIgnore
    public final void a(ShineDevice shineDevice) {
        String serial = shineDevice.getSerial();
        ee7.a((Object) serial, "device.serial");
        if (serial.length() > 0) {
            rr6 rr6 = this.h;
            if (rr6 != null) {
                rr6.c(shineDevice);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.l(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.fp5.b
    public void a(View view, fp5.c cVar, int i2) {
        RecyclerView recyclerView;
        ee7.b(view, "view");
        ee7.b(cVar, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = r.a();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i2);
        sb.append(", mSnappedPos=");
        vo5 vo5 = this.j;
        if (vo5 != null) {
            sb.append(vo5.d());
            local.d(a2, sb.toString());
            vo5 vo52 = this.j;
            if (vo52 == null) {
                ee7.d("mSnapHelper");
                throw null;
            } else if (vo52.d() != i2) {
                qw6<s45> qw6 = this.g;
                if (qw6 != null) {
                    s45 a3 = qw6.a();
                    if (a3 != null && (recyclerView = a3.w) != null) {
                        vo5 vo53 = this.j;
                        if (vo53 != null) {
                            recyclerView.smoothScrollBy((i2 - vo53.d()) * view.getWidth(), view.getHeight());
                        } else {
                            ee7.d("mSnapHelper");
                            throw null;
                        }
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                List<r87<ShineDevice, String>> list = this.p;
                vo5 vo54 = this.j;
                if (vo54 != null) {
                    a(list.get(vo54.d()).getFirst());
                } else {
                    ee7.d("mSnapHelper");
                    throw null;
                }
            }
        } else {
            ee7.d("mSnapHelper");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() == -2084521848 && str.equals("DOWNLOAD") && i2 == 2131363307) {
            rr6 rr6 = this.h;
            if (rr6 != null) {
                List<r87<ShineDevice, String>> list = this.p;
                vo5 vo5 = this.j;
                if (vo5 != null) {
                    rr6.a(list.get(vo5.d()).getFirst());
                } else {
                    ee7.d("mSnapHelper");
                    throw null;
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }
}
