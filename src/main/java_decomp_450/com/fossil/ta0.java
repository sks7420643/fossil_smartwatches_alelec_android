package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ta0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ta0 createFromParcel(Parcel parcel) {
            return new ta0(parcel.readLong(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ta0[] newArray(int i) {
            return new ta0[i];
        }
    }

    @DexIgnore
    public ta0(long j, int i) {
        this.a = j;
        this.b = i;
        if (!(i >= 0 && 100 >= i)) {
            throw new IllegalArgumentException(yh0.a(yh0.b("chanceOfRain("), this.b, ") is out of range ", "[0, 100]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.K2, Long.valueOf(this.a)), r51.q, Integer.valueOf(this.b));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.a);
            jSONObject.put("rain", this.b);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ta0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ta0 ta0 = (ta0) obj;
            return this.a == ta0.a && this.b == ta0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.ChanceOfRainInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.b;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return (((int) this.a) * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
