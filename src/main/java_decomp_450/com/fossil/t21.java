package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t21 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk0 a;
    @DexIgnore
    public /* final */ /* synthetic */ gd7 b;
    @DexIgnore
    public /* final */ /* synthetic */ gd7 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t21(zk0 zk0, gd7 gd7, gd7 gd72) {
        super(1);
        this.a = zk0;
        this.b = gd7;
        this.c = gd72;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        v81 v812 = v81;
        if (((Boolean) this.b.invoke(v812.v)).booleanValue()) {
            this.a.a(v812.v);
        } else {
            this.c.invoke(v812);
        }
        return i97.a;
    }
}
