package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j20 implements uy<byte[]> {
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public j20(byte[] bArr) {
        u50.a(bArr);
        this.a = bArr;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return this.a.length;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<byte[]> d() {
        return byte[].class;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public byte[] get() {
        return this.a;
    }
}
