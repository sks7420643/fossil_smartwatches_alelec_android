package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c43 implements tr2<a43> {
    @DexIgnore
    public static c43 b; // = new c43();
    @DexIgnore
    public /* final */ tr2<a43> a;

    @DexIgnore
    public c43(tr2<a43> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((a43) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((a43) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ a43 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public c43() {
        this(sr2.a(new e43()));
    }
}
