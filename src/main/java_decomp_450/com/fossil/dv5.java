package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.el4;
import com.fossil.fv5;
import com.fossil.xg5;
import com.fossil.zu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv5 extends ho5 implements zu5.b {
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public rj4 g;
    @DexIgnore
    public qw6<m55> h;
    @DexIgnore
    public fv5 i;
    @DexIgnore
    public zu5 j;
    @DexIgnore
    public ArrayList<QuickResponseMessage> p; // = new ArrayList<>();
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final dv5 a() {
            return new dv5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<fv5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ dv5 a;

        @DexIgnore
        public b(dv5 dv5) {
            this.a = dv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(fv5.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("QuickResponseFragment", "data changed " + bVar);
                this.a.a(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<el4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ dv5 a;

        @DexIgnore
        public c(dv5 dv5) {
            this.a = dv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.a aVar) {
            if (aVar != null) {
                if (aVar.a()) {
                    this.a.b();
                }
                if (aVar.b()) {
                    this.a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<el4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ dv5 a;

        @DexIgnore
        public d(dv5 dv5) {
            this.a = dv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.b bVar) {
            if (bVar != null && (!bVar.a().isEmpty())) {
                xg5.a(xg5.b, this.a.getContext(), xg5.a.SET_BLE_COMMAND, false, false, true, (Integer) 113, 12, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ m55 a;
        @DexIgnore
        public /* final */ /* synthetic */ dv5 b;

        @DexIgnore
        public e(m55 m55, dv5 dv5) {
            this.a = m55;
            this.b = dv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleEditText flexibleEditText = this.a.s;
            ee7.a((Object) flexibleEditText, "etMessage");
            this.b.g1().a(String.valueOf(flexibleEditText.getText()), 50);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dv5 a;

        @DexIgnore
        public f(dv5 dv5) {
            this.a = dv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.g1().a(this.a.p);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m55 a;
        @DexIgnore
        public /* final */ /* synthetic */ dv5 b;

        @DexIgnore
        public g(m55 m55, dv5 dv5) {
            this.a = m55;
            this.b = dv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            fv5 g1 = this.b.g1();
            FlexibleEditText flexibleEditText = this.a.s;
            ee7.a((Object) flexibleEditText, "etMessage");
            g1.a(String.valueOf(flexibleEditText.getText()), this.b.p.size(), 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<List<? extends QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ dv5 a;

        @DexIgnore
        public h(dv5 dv5) {
            this.a = dv5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<QuickResponseMessage> list) {
            m55 a2;
            FlexibleEditText flexibleEditText;
            if (list.isEmpty()) {
                this.a.g1().h();
                return;
            }
            if (!(list.size() <= this.a.p.size() || (a2 = this.a.f1().a()) == null || (flexibleEditText = a2.s) == null)) {
                flexibleEditText.setText("");
            }
            this.a.p.clear();
            this.a.p.addAll(list);
            zu5 a3 = dv5.a(this.a);
            ee7.a((Object) list, "qrs");
            a3.a(list);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        fv5 fv5 = this.i;
        if (fv5 != null) {
            fv5.a(this.p);
            return false;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final qw6<m55> f1() {
        qw6<m55> qw6 = this.h;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zu5.b
    public void g(int i2, String str) {
        T t2;
        boolean z;
        ee7.b(str, "message");
        Iterator<T> it = this.p.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (t2.getId() == i2) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        T t3 = t2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "changed message " + ((Object) t3) + ", new message " + str);
        if (t3 != null) {
            t3.setResponse(str);
        }
    }

    @DexIgnore
    public final fv5 g1() {
        fv5 fv5 = this.i;
        if (fv5 != null) {
            return fv5;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zu5.b
    public void j0() {
        View d2;
        qw6<m55> qw6 = this.h;
        if (qw6 != null) {
            m55 a2 = qw6.a();
            if (a2 != null && (d2 = a2.d()) != null) {
                d2.requestFocus();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "onActivityResult result code is " + i3);
        if (i3 != 1) {
            return;
        }
        if (i2 == 112) {
            fv5 fv5 = this.i;
            if (fv5 != null) {
                Context requireContext = requireContext();
                ee7.a((Object) requireContext, "requireContext()");
                fv5.a(requireContext, this.p, false);
                return;
            }
            ee7.d("mViewModel");
            throw null;
        } else if (i2 == 113 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        m55 m55 = (m55) qb.a(LayoutInflater.from(getContext()), 2131558612, null, false, a1());
        PortfolioApp.g0.c().f().u().a(this);
        rj4 rj4 = this.g;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(fv5.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026nseViewModel::class.java)");
            fv5 fv5 = (fv5) a2;
            this.i = fv5;
            if (fv5 != null) {
                fv5.g().a(getViewLifecycleOwner(), new b(this));
                fv5 fv52 = this.i;
                if (fv52 != null) {
                    fv52.b().a(getViewLifecycleOwner(), new c(this));
                    fv5 fv53 = this.i;
                    if (fv53 != null) {
                        fv53.d().a(getViewLifecycleOwner(), new d(this));
                        this.q = eh5.l.a().b("error");
                        this.r = eh5.l.a().b("nonBrandSurface");
                        this.h = new qw6<>(this, m55);
                        ee7.a((Object) m55, "binding");
                        return m55.d();
                    }
                    ee7.d("mViewModel");
                    throw null;
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<m55> qw6 = this.h;
        if (qw6 != null) {
            m55 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "ftvLimitValue");
                flexibleTextView.setText("0");
                FlexibleEditText flexibleEditText = a2.s;
                ee7.a((Object) flexibleEditText, "etMessage");
                flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(50)});
                FlexibleEditText flexibleEditText2 = a2.s;
                ee7.a((Object) flexibleEditText2, "etMessage");
                flexibleEditText2.addTextChangedListener(new e(a2, this));
                this.j = new zu5(new ArrayList(), this, 50);
                RecyclerView recyclerView = a2.x;
                ee7.a((Object) recyclerView, "rvMessage");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.x;
                ee7.a((Object) recyclerView2, "rvMessage");
                zu5 zu5 = this.j;
                if (zu5 != null) {
                    recyclerView2.setAdapter(zu5);
                    a2.r.setOnClickListener(new f(this));
                    a2.q.setOnClickListener(new g(a2, this));
                } else {
                    ee7.d("mResponseAdapter");
                    throw null;
                }
            }
            fv5 fv5 = this.i;
            if (fv5 != null) {
                LiveData<List<QuickResponseMessage>> f2 = fv5.f();
                if (f2 != null) {
                    f2.a(getViewLifecycleOwner(), new h(this));
                    return;
                }
                return;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zu5 a(dv5 dv5) {
        zu5 zu5 = dv5.j;
        if (zu5 != null) {
            return zu5;
        }
        ee7.d("mResponseAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != -4398250 || !str.equals("SET TO WATCH FAIL")) {
                return;
            }
            if (i2 == 2131363229) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363307) {
                fv5 fv5 = this.i;
                if (fv5 != null) {
                    Context requireContext = requireContext();
                    ee7.a((Object) requireContext, "requireContext()");
                    fv5.a(requireContext, this.p, true);
                    return;
                }
                ee7.d("mViewModel");
                throw null;
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363307) {
                fv5 fv52 = this.i;
                if (fv52 != null) {
                    Context requireContext2 = requireContext();
                    ee7.a((Object) requireContext2, "requireContext()");
                    fv52.a(requireContext2, this.p, true);
                    return;
                }
                ee7.d("mViewModel");
                throw null;
            } else if (i2 == 2131363229 && (activity = getActivity()) != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public final void a(fv5.b bVar) {
        FragmentActivity activity;
        Boolean a2 = bVar.a();
        if (a2 != null && a2.booleanValue()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.V(childFragmentManager);
        }
        Boolean f2 = bVar.f();
        if (f2 != null && f2.booleanValue()) {
            bx6 bx62 = bx6.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager2, "childFragmentManager");
            bx62.T(childFragmentManager2);
        }
        Boolean c2 = bVar.c();
        if (!(c2 == null || !c2.booleanValue() || (activity = getActivity()) == null)) {
            activity.finish();
        }
        Boolean b2 = bVar.b();
        if (b2 != null && b2.booleanValue()) {
            bx6 bx63 = bx6.c;
            FragmentManager childFragmentManager3 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager3, "childFragmentManager");
            bx63.H(childFragmentManager3);
        }
        Boolean d2 = bVar.d();
        if (d2 != null) {
            if (d2.booleanValue()) {
                qw6<m55> qw6 = this.h;
                if (qw6 != null) {
                    m55 a3 = qw6.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.q)) {
                            FlexibleEditText flexibleEditText = a3.s;
                            ee7.a((Object) flexibleEditText, "etMessage");
                            flexibleEditText.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.q)));
                        } else {
                            FlexibleEditText flexibleEditText2 = a3.s;
                            ee7.a((Object) flexibleEditText2, "etMessage");
                            flexibleEditText2.setBackgroundTintList(ColorStateList.valueOf(-65536));
                        }
                        FlexibleTextView flexibleTextView = a3.v;
                        ee7.a((Object) flexibleTextView, "ftvLimitedWarning");
                        flexibleTextView.setVisibility(0);
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                qw6<m55> qw62 = this.h;
                if (qw62 != null) {
                    m55 a4 = qw62.a();
                    if (a4 != null) {
                        FlexibleTextView flexibleTextView2 = a4.v;
                        ee7.a((Object) flexibleTextView2, "ftvLimitedWarning");
                        if (flexibleTextView2.getVisibility() == 0) {
                            FlexibleTextView flexibleTextView3 = a4.v;
                            ee7.a((Object) flexibleTextView3, "ftvLimitedWarning");
                            flexibleTextView3.setVisibility(8);
                            if (!TextUtils.isEmpty(this.r)) {
                                FlexibleEditText flexibleEditText3 = a4.s;
                                ee7.a((Object) flexibleEditText3, "etMessage");
                                flexibleEditText3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.r)));
                            } else {
                                FlexibleEditText flexibleEditText4 = a4.s;
                                ee7.a((Object) flexibleEditText4, "etMessage");
                                flexibleEditText4.setBackgroundTintList(ColorStateList.valueOf(-1));
                            }
                        }
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            }
        }
        Boolean e2 = bVar.e();
        if (e2 != null && e2.booleanValue()) {
            we7 we7 = we7.a;
            String a5 = ig5.a(getContext(), 2131887510);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026response_limited_warning)");
            String format = String.format(a5, Arrays.copyOf(new Object[]{String.valueOf(10)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            Toast.makeText(getContext(), format, 1).show();
        }
        Integer g2 = bVar.g();
        if (g2 != null) {
            int intValue = g2.intValue();
            qw6<m55> qw63 = this.h;
            if (qw63 != null) {
                m55 a6 = qw63.a();
                if (a6 != null) {
                    FlexibleTextView flexibleTextView4 = a6.u;
                    ee7.a((Object) flexibleTextView4, "ftvLimitValue");
                    flexibleTextView4.setText(String.valueOf(intValue));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zu5.b
    public void a(QuickResponseMessage quickResponseMessage) {
        ee7.b(quickResponseMessage, "qr");
        fv5 fv5 = this.i;
        if (fv5 != null) {
            fv5.a(quickResponseMessage);
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }
}
