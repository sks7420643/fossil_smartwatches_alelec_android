package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.ql4;
import com.fossil.rd5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy5 extends ql4<ql4.b, b, ql4.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ Context d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.c {
        @DexIgnore
        public /* final */ List<at5> a;

        @DexIgnore
        public b(List<at5> list) {
            ee7.b(list, "apps");
            this.a = list;
        }

        @DexIgnore
        public final List<at5> a() {
            return this.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = nw5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetApps::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public dy5(Context context) {
        ee7.b(context, "mContext");
        this.d = context;
    }

    @DexIgnore
    @Override // com.fossil.ql4
    public void a(ql4.b bVar) {
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase GetHybridApps");
        List<AppFilter> allAppFilters = ah5.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<rd5.b> it = rd5.g.f().iterator();
        while (it.hasNext()) {
            rd5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !mh7.b(next.b(), this.d.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    ee7.a((Object) next2, "appFilter");
                    if (ee7.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                at5 at5 = new at5();
                at5.setInstalledApp(installedApp);
                at5.setUri(next.c());
                at5.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(at5);
            }
        }
        aa7.c(linkedList);
        a().onSuccess(new b(linkedList));
    }
}
