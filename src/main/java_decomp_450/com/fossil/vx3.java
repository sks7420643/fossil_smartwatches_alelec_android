package com.fossil;

import com.fossil.zx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vx3<E> extends AbstractCollection<E> implements Serializable {
    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public zx3<E> asList() {
        int size = size();
        if (size == 0) {
            return zx3.of();
        }
        if (size != 1) {
            return new nz3(this, toArray());
        }
        return zx3.of(iterator().next());
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean contains(Object obj);

    @DexIgnore
    @CanIgnoreReturnValue
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = iterator();
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return i;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract j04<E> iterator();

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object[] toArray() {
        int size = size();
        if (size == 0) {
            return iz3.a;
        }
        Object[] objArr = new Object[size];
        copyIntoArray(objArr, 0);
        return objArr;
    }

    @DexIgnore
    public Object writeReplace() {
        return new zx3.d(toArray());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<E> extends b<E> {
        @DexIgnore
        public Object[] a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a(int i) {
            bx3.a(i, "initialCapacity");
            this.a = new Object[i];
        }

        @DexIgnore
        public final void a(int i) {
            Object[] objArr = this.a;
            if (objArr.length < i) {
                this.a = iz3.a(objArr, b.a(objArr.length, i));
            }
        }

        @DexIgnore
        @Override // com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(E e) {
            jw3.a(e);
            a(this.b + 1);
            Object[] objArr = this.a;
            int i = this.b;
            this.b = i + 1;
            objArr[i] = e;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vx3.b
        @CanIgnoreReturnValue
        public b<E> a(E... eArr) {
            iz3.a(eArr);
            a(this.b + eArr.length);
            System.arraycopy(eArr, 0, this.a, this.b, eArr.length);
            this.b += eArr.length;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> {
        @DexIgnore
        public static int a(int i, int i2) {
            if (i2 >= 0) {
                int i3 = i + (i >> 1) + 1;
                if (i3 < i2) {
                    i3 = Integer.highestOneBit(i2 - 1) << 1;
                }
                if (i3 < 0) {
                    return Integer.MAX_VALUE;
                }
                return i3;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public abstract b<E> a(E e);

        @DexIgnore
        @CanIgnoreReturnValue
        public b<E> a(E... eArr) {
            for (E e : eArr) {
                a((Object) e);
            }
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.vx3$b<E> */
        /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public b<E> a(Iterator<? extends E> it) {
            while (it.hasNext()) {
                a(it.next());
            }
            return this;
        }
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public final <T> T[] toArray(T[] tArr) {
        T[] tArr2;
        jw3.a(tArr);
        int size = size();
        if (tArr.length < size) {
            tArr = (T[]) iz3.c(tArr, size);
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        copyIntoArray(tArr2, 0);
        return tArr2;
    }
}
