package com.fossil;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.pc4;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed4 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ed4> CREATOR; // = new fd4();
    @DexIgnore
    public Bundle a;
    @DexIgnore
    public Map<String, String> b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Uri a;

        @DexIgnore
        public b(dd4 dd4) {
            dd4.g("gcm.n.title");
            dd4.e("gcm.n.title");
            a(dd4, "gcm.n.title");
            dd4.g("gcm.n.body");
            dd4.e("gcm.n.body");
            a(dd4, "gcm.n.body");
            dd4.g("gcm.n.icon");
            dd4.f();
            dd4.g("gcm.n.tag");
            dd4.g("gcm.n.color");
            dd4.g("gcm.n.click_action");
            dd4.g("gcm.n.android_channel_id");
            this.a = dd4.b();
            dd4.g("gcm.n.image");
            dd4.g("gcm.n.ticker");
            dd4.b("gcm.n.notification_priority");
            dd4.b("gcm.n.visibility");
            dd4.b("gcm.n.notification_count");
            dd4.a("gcm.n.sticky");
            dd4.a("gcm.n.local_only");
            dd4.a("gcm.n.default_sound");
            dd4.a("gcm.n.default_vibrate_timings");
            dd4.a("gcm.n.default_light_settings");
            dd4.f("gcm.n.event_time");
            dd4.a();
            dd4.g();
        }

        @DexIgnore
        public static String[] a(dd4 dd4, String str) {
            Object[] d = dd4.d(str);
            if (d == null) {
                return null;
            }
            String[] strArr = new String[d.length];
            for (int i = 0; i < d.length; i++) {
                strArr[i] = String.valueOf(d[i]);
            }
            return strArr;
        }
    }

    @DexIgnore
    public ed4(Bundle bundle) {
        this.a = bundle;
    }

    @DexIgnore
    public final int b(String str) {
        if ("high".equals(str)) {
            return 1;
        }
        return "normal".equals(str) ? 2 : 0;
    }

    @DexIgnore
    public final Map<String, String> e() {
        if (this.b == null) {
            this.b = pc4.a.a(this.a);
        }
        return this.b;
    }

    @DexIgnore
    public final String g() {
        return this.a.getString("from");
    }

    @DexIgnore
    public final b v() {
        if (this.c == null && dd4.a(this.a)) {
            this.c = new b(new dd4(this.a));
        }
        return this.c;
    }

    @DexIgnore
    public final int w() {
        String string = this.a.getString("google.delivered_priority");
        if (string == null) {
            if ("1".equals(this.a.getString("google.priority_reduced"))) {
                return 2;
            }
            string = this.a.getString("google.priority");
        }
        return b(string);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        fd4.a(this, parcel, i);
    }
}
