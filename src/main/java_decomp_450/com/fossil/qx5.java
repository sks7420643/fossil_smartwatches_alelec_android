package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx5 implements Factory<px5> {
    @DexIgnore
    public static px5 a(kx5 kx5, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        return new px5(kx5, notificationsLoader, loaderManager);
    }
}
