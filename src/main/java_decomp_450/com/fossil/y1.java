package com.fossil;

import android.widget.ListView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y1 {
    @DexIgnore
    boolean a();

    @DexIgnore
    void dismiss();

    @DexIgnore
    ListView e();

    @DexIgnore
    void show();
}
