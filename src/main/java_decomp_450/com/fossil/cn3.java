package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn3 extends v43 implements bn3 {
    @DexIgnore
    public cn3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.bn3
    public final ab2 a(ab2 ab2, int i, int i2, String str, int i3) throws RemoteException {
        Parcel E = E();
        w43.a(E, ab2);
        E.writeInt(i);
        E.writeInt(i2);
        E.writeString(str);
        E.writeInt(i3);
        Parcel a = a(1, E);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
