package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d35 extends c35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i M; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray N;
    @DexIgnore
    public long L;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        N = sparseIntArray;
        sparseIntArray.put(2131363310, 1);
        N.put(2131363229, 2);
        N.put(2131362499, 3);
        N.put(2131362047, 4);
        N.put(2131362758, 5);
        N.put(2131362759, 6);
        N.put(2131362757, 7);
        N.put(2131362738, 8);
        N.put(2131362021, 9);
        N.put(2131362543, 10);
        N.put(2131362078, 11);
        N.put(2131363454, 12);
        N.put(2131363355, 13);
        N.put(2131363453, 14);
        N.put(2131363354, 15);
        N.put(2131363452, 16);
        N.put(2131363353, 17);
        N.put(2131363379, 18);
        N.put(2131362167, 19);
        N.put(2131362998, 20);
    }
    */

    @DexIgnore
    public d35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 21, M, N));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.L = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.L != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.L = 1;
        }
        g();
    }

    @DexIgnore
    public d35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (View) objArr[9], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[11], (CardView) objArr[19], (ImageButton) objArr[3], (View) objArr[10], (ImageView) objArr[8], (View) objArr[7], (View) objArr[5], (View) objArr[6], (ConstraintLayout) objArr[0], (ViewPager2) objArr[20], (RTLImageView) objArr[2], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[13], (View) objArr[18], (CustomizeWidget) objArr[16], (CustomizeWidget) objArr[14], (CustomizeWidget) objArr[12]);
        this.L = -1;
        ((c35) this).A.setTag(null);
        a(view);
        f();
    }
}
