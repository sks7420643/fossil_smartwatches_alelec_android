package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x93 implements Parcelable.Creator<b93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        IBinder iBinder = null;
        Float f = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                i = j72.q(parcel, a);
            } else if (a2 == 3) {
                iBinder = j72.p(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                f = j72.o(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new b93(i, iBinder, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ b93[] newArray(int i) {
        return new b93[i];
    }
}
