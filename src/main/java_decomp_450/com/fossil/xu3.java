package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xu3 implements zu3 {
    @DexIgnore
    public /* final */ float a;

    @DexIgnore
    public xu3(float f) {
        this.a = f;
    }

    @DexIgnore
    @Override // com.fossil.zu3
    public float a(RectF rectF) {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof xu3) && this.a == ((xu3) obj).a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
