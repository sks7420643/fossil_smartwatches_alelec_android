package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo5 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ Category a;
    @DexIgnore
    public int b;
    @DexIgnore
    public ArrayList<Category> c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleButton a;
        @DexIgnore
        public /* final */ /* synthetic */ wo5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wo5$a$a")
        /* renamed from: com.fossil.wo5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0234a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0234a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.b.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CategoriesAdapter", "onItemClick pos=" + this.a.getAdapterPosition() + " currentPos=" + this.a.b.b);
                    a aVar = this.a;
                    aVar.b.b = aVar.getAdapterPosition();
                    if (this.a.b.b != 0) {
                        c c = this.a.b.c();
                        if (c != null) {
                            Object obj = this.a.b.c.get(this.a.b.b);
                            ee7.a(obj, "mData[mSelectedIndex]");
                            c.a((Category) obj);
                            return;
                        }
                        return;
                    }
                    c c2 = this.a.b.c();
                    if (c2 != null) {
                        c2.a();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(wo5 wo5, View view) {
            super(view);
            ee7.b(view, "view");
            this.b = wo5;
            View findViewById = view.findViewById(2131362353);
            ee7.a((Object) findViewById, "view.findViewById(R.id.ftv_category)");
            FlexibleButton flexibleButton = (FlexibleButton) findViewById;
            this.a = flexibleButton;
            flexibleButton.setOnClickListener(new View$OnClickListenerC0234a(this));
        }

        @DexIgnore
        public final void a(Category category, int i) {
            ee7.b(category, "category");
            Context context = this.a.getContext();
            FlexibleButton flexibleButton = this.a;
            flexibleButton.setCompoundDrawablePadding((int) yx6.a(6, flexibleButton.getContext()));
            if (TextUtils.isEmpty(category.getId())) {
                FlexibleButton flexibleButton2 = this.a;
                ee7.a((Object) context, "context");
                flexibleButton2.setText(context.getResources().getString(2131886942));
            } else {
                String a2 = ig5.a(PortfolioApp.g0.c(), category.getName(), category.getEnglishName());
                if (i == 0) {
                    this.a.setText(a2);
                } else {
                    ee7.a((Object) a2, "name");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        this.a.setText(upperCase);
                    } else {
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            if (i == 0) {
                this.a.a("flexible_button_search");
                Drawable c = v6.c(context, 2131231046);
                if (c != null) {
                    c.setTintList(ColorStateList.valueOf(v6.a(context, 2131099971)));
                }
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds(c, (Drawable) null, (Drawable) null, (Drawable) null);
            } else if (i == this.b.b) {
                this.a.setSelected(true);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.a("flexible_button_primary");
            } else {
                this.a.setSelected(false);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.a("flexible_button_secondary");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(Category category);
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wo5(ArrayList arrayList, c cVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final c c() {
        return this.d;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public wo5(ArrayList<Category> arrayList, c cVar) {
        ee7.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.a = new Category("Search", "Search", "Customization_Complications_Elements_Input__Search", "", "", -1);
        this.b = -1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558665, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026_category, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<Category> list) {
        ee7.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        this.c.add(0, this.a);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        ee7.b(str, "category");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a((Object) t.getId(), (Object) str)) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            Category category = this.c.get(i);
            ee7.a((Object) category, "mData[position]");
            aVar.a(category, i);
        }
    }

    @DexIgnore
    public final void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CategoriesAdapter", "setSelectedCategory pos=" + i);
        if (i < getItemCount()) {
            this.b = i;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        ee7.b(cVar, "listener");
        this.d = cVar;
    }
}
