package com.fossil;

import com.facebook.internal.Utility;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp7 {
    @DexIgnore
    public static /* final */ up7[] a; // = {new up7(up7.i, ""), new up7(up7.f, "GET"), new up7(up7.f, "POST"), new up7(up7.g, "/"), new up7(up7.g, "/index.html"), new up7(up7.h, "http"), new up7(up7.h, Utility.URL_SCHEME), new up7(up7.e, "200"), new up7(up7.e, "204"), new up7(up7.e, "206"), new up7(up7.e, "304"), new up7(up7.e, "400"), new up7(up7.e, "404"), new up7(up7.e, "500"), new up7("accept-charset", ""), new up7("accept-encoding", "gzip, deflate"), new up7("accept-language", ""), new up7("accept-ranges", ""), new up7("accept", ""), new up7("access-control-allow-origin", ""), new up7("age", ""), new up7("allow", ""), new up7(Constants.IF_AUTHORIZATION, ""), new up7("cache-control", ""), new up7("content-disposition", ""), new up7("content-encoding", ""), new up7("content-language", ""), new up7("content-length", ""), new up7("content-location", ""), new up7("content-range", ""), new up7("content-type", ""), new up7("cookie", ""), new up7("date", ""), new up7(Constants.JSON_KEY_ETAG, ""), new up7("expect", ""), new up7("expires", ""), new up7("from", ""), new up7("host", ""), new up7("if-match", ""), new up7("if-modified-since", ""), new up7("if-none-match", ""), new up7("if-range", ""), new up7("if-unmodified-since", ""), new up7("last-modified", ""), new up7("link", ""), new up7(PlaceFields.LOCATION, ""), new up7("max-forwards", ""), new up7("proxy-authenticate", ""), new up7("proxy-authorization", ""), new up7("range", ""), new up7("referer", ""), new up7("refresh", ""), new up7("retry-after", ""), new up7("server", ""), new up7("set-cookie", ""), new up7("strict-transport-security", ""), new up7("transfer-encoding", ""), new up7("user-agent", ""), new up7("vary", ""), new up7("via", ""), new up7("www-authenticate", "")};
    @DexIgnore
    public static /* final */ Map<br7, Integer> b; // = a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<up7> a;
        @DexIgnore
        public /* final */ ar7 b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public up7[] e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;

        @DexIgnore
        public a(int i, sr7 sr7) {
            this(i, i, sr7);
        }

        @DexIgnore
        public final void a() {
            int i = this.d;
            int i2 = this.h;
            if (i >= i2) {
                return;
            }
            if (i == 0) {
                b();
            } else {
                b(i2 - i);
            }
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.e, (Object) null);
            this.f = this.e.length - 1;
            this.g = 0;
            this.h = 0;
        }

        @DexIgnore
        public List<up7> c() {
            ArrayList arrayList = new ArrayList(this.a);
            this.a.clear();
            return arrayList;
        }

        @DexIgnore
        public final boolean d(int i) {
            return i >= 0 && i <= vp7.a.length - 1;
        }

        @DexIgnore
        public final void e(int i) throws IOException {
            if (d(i)) {
                this.a.add(vp7.a[i]);
                return;
            }
            int a2 = a(i - vp7.a.length);
            if (a2 >= 0) {
                up7[] up7Arr = this.e;
                if (a2 < up7Arr.length) {
                    this.a.add(up7Arr[a2]);
                    return;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public void f() throws IOException {
            while (!this.b.h()) {
                byte readByte = this.b.readByte() & 255;
                if (readByte == 128) {
                    throw new IOException("index == 0");
                } else if ((readByte & 128) == 128) {
                    e(a(readByte, 127) - 1);
                } else if (readByte == 64) {
                    g();
                } else if ((readByte & 64) == 64) {
                    f(a(readByte, 63) - 1);
                } else if ((readByte & 32) == 32) {
                    int a2 = a(readByte, 31);
                    this.d = a2;
                    if (a2 < 0 || a2 > this.c) {
                        throw new IOException("Invalid dynamic table size update " + this.d);
                    }
                    a();
                } else if (readByte == 16 || readByte == 0) {
                    h();
                } else {
                    g(a(readByte, 15) - 1);
                }
            }
        }

        @DexIgnore
        public final void g(int i) throws IOException {
            this.a.add(new up7(c(i), e()));
        }

        @DexIgnore
        public final void h() throws IOException {
            br7 e2 = e();
            vp7.a(e2);
            this.a.add(new up7(e2, e()));
        }

        @DexIgnore
        public a(int i, int i2, sr7 sr7) {
            this.a = new ArrayList();
            up7[] up7Arr = new up7[8];
            this.e = up7Arr;
            this.f = up7Arr.length - 1;
            this.g = 0;
            this.h = 0;
            this.c = i;
            this.d = i2;
            this.b = ir7.a(sr7);
        }

        @DexIgnore
        public final int d() throws IOException {
            return this.b.readByte() & 255;
        }

        @DexIgnore
        public final br7 c(int i) throws IOException {
            if (d(i)) {
                return vp7.a[i].a;
            }
            int a2 = a(i - vp7.a.length);
            if (a2 >= 0) {
                up7[] up7Arr = this.e;
                if (a2 < up7Arr.length) {
                    return up7Arr[a2].a;
                }
            }
            throw new IOException("Header index too large " + (i + 1));
        }

        @DexIgnore
        public final int a(int i) {
            return this.f + 1 + i;
        }

        @DexIgnore
        public final void g() throws IOException {
            br7 e2 = e();
            vp7.a(e2);
            a(-1, new up7(e2, e()));
        }

        @DexIgnore
        public final void a(int i, up7 up7) {
            this.a.add(up7);
            int i2 = up7.c;
            if (i != -1) {
                i2 -= this.e[a(i)].c;
            }
            int i3 = this.d;
            if (i2 > i3) {
                b();
                return;
            }
            int b2 = b((this.h + i2) - i3);
            if (i == -1) {
                int i4 = this.g + 1;
                up7[] up7Arr = this.e;
                if (i4 > up7Arr.length) {
                    up7[] up7Arr2 = new up7[(up7Arr.length * 2)];
                    System.arraycopy(up7Arr, 0, up7Arr2, up7Arr.length, up7Arr.length);
                    this.f = this.e.length - 1;
                    this.e = up7Arr2;
                }
                int i5 = this.f;
                this.f = i5 - 1;
                this.e[i5] = up7;
                this.g++;
            } else {
                this.e[i + a(i) + b2] = up7;
            }
            this.h += i2;
        }

        @DexIgnore
        public final int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.e.length;
                while (true) {
                    length--;
                    if (length < this.f || i <= 0) {
                        up7[] up7Arr = this.e;
                        int i3 = this.f;
                        System.arraycopy(up7Arr, i3 + 1, up7Arr, i3 + 1 + i2, this.g);
                        this.f += i2;
                    } else {
                        up7[] up7Arr2 = this.e;
                        i -= up7Arr2[length].c;
                        this.h -= up7Arr2[length].c;
                        this.g--;
                        i2++;
                    }
                }
                up7[] up7Arr3 = this.e;
                int i32 = this.f;
                System.arraycopy(up7Arr3, i32 + 1, up7Arr3, i32 + 1 + i2, this.g);
                this.f += i2;
            }
            return i2;
        }

        @DexIgnore
        public br7 e() throws IOException {
            int d2 = d();
            boolean z = (d2 & 128) == 128;
            int a2 = a(d2, 127);
            if (z) {
                return br7.of(cq7.b().a(this.b.f((long) a2)));
            }
            return this.b.a((long) a2);
        }

        @DexIgnore
        public final void f(int i) throws IOException {
            a(-1, new up7(c(i), e()));
        }

        @DexIgnore
        public int a(int i, int i2) throws IOException {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int d2 = d();
                if ((d2 & 128) == 0) {
                    return i2 + (d2 << i4);
                }
                i2 += (d2 & 127) << i4;
                i4 += 7;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ yq7 a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;
        @DexIgnore
        public up7[] f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;

        @DexIgnore
        public b(yq7 yq7) {
            this(4096, true, yq7);
        }

        @DexIgnore
        public final int a(int i2) {
            int i3 = 0;
            if (i2 > 0) {
                int length = this.f.length;
                while (true) {
                    length--;
                    if (length < this.g || i2 <= 0) {
                        up7[] up7Arr = this.f;
                        int i4 = this.g;
                        System.arraycopy(up7Arr, i4 + 1, up7Arr, i4 + 1 + i3, this.h);
                        up7[] up7Arr2 = this.f;
                        int i5 = this.g;
                        Arrays.fill(up7Arr2, i5 + 1, i5 + 1 + i3, (Object) null);
                        this.g += i3;
                    } else {
                        up7[] up7Arr3 = this.f;
                        i2 -= up7Arr3[length].c;
                        this.i -= up7Arr3[length].c;
                        this.h--;
                        i3++;
                    }
                }
                up7[] up7Arr4 = this.f;
                int i42 = this.g;
                System.arraycopy(up7Arr4, i42 + 1, up7Arr4, i42 + 1 + i3, this.h);
                up7[] up7Arr22 = this.f;
                int i52 = this.g;
                Arrays.fill(up7Arr22, i52 + 1, i52 + 1 + i3, (Object) null);
                this.g += i3;
            }
            return i3;
        }

        @DexIgnore
        public final void b() {
            Arrays.fill(this.f, (Object) null);
            this.g = this.f.length - 1;
            this.h = 0;
            this.i = 0;
        }

        @DexIgnore
        public b(int i2, boolean z, yq7 yq7) {
            this.c = Integer.MAX_VALUE;
            up7[] up7Arr = new up7[8];
            this.f = up7Arr;
            this.g = up7Arr.length - 1;
            this.h = 0;
            this.i = 0;
            this.e = i2;
            this.b = z;
            this.a = yq7;
        }

        @DexIgnore
        public void b(int i2) {
            int min = Math.min(i2, 16384);
            int i3 = this.e;
            if (i3 != min) {
                if (min < i3) {
                    this.c = Math.min(this.c, min);
                }
                this.d = true;
                this.e = min;
                a();
            }
        }

        @DexIgnore
        public final void a(up7 up7) {
            int i2 = up7.c;
            int i3 = this.e;
            if (i2 > i3) {
                b();
                return;
            }
            a((this.i + i2) - i3);
            int i4 = this.h + 1;
            up7[] up7Arr = this.f;
            if (i4 > up7Arr.length) {
                up7[] up7Arr2 = new up7[(up7Arr.length * 2)];
                System.arraycopy(up7Arr, 0, up7Arr2, up7Arr.length, up7Arr.length);
                this.g = this.f.length - 1;
                this.f = up7Arr2;
            }
            int i5 = this.g;
            this.g = i5 - 1;
            this.f[i5] = up7;
            this.h++;
            this.i += i2;
        }

        @DexIgnore
        public void a(List<up7> list) throws IOException {
            int i2;
            int i3;
            if (this.d) {
                int i4 = this.c;
                if (i4 < this.e) {
                    a(i4, 31, 32);
                }
                this.d = false;
                this.c = Integer.MAX_VALUE;
                a(this.e, 31, 32);
            }
            int size = list.size();
            for (int i5 = 0; i5 < size; i5++) {
                up7 up7 = list.get(i5);
                br7 asciiLowercase = up7.a.toAsciiLowercase();
                br7 br7 = up7.b;
                Integer num = vp7.b.get(asciiLowercase);
                if (num != null) {
                    i3 = num.intValue() + 1;
                    if (i3 > 1 && i3 < 8) {
                        if (ro7.a(vp7.a[i3 - 1].b, br7)) {
                            i2 = i3;
                        } else if (ro7.a(vp7.a[i3].b, br7)) {
                            i2 = i3;
                            i3++;
                        }
                    }
                    i2 = i3;
                    i3 = -1;
                } else {
                    i3 = -1;
                    i2 = -1;
                }
                if (i3 == -1) {
                    int i6 = this.g + 1;
                    int length = this.f.length;
                    while (true) {
                        if (i6 >= length) {
                            break;
                        }
                        if (ro7.a(this.f[i6].a, asciiLowercase)) {
                            if (ro7.a(this.f[i6].b, br7)) {
                                i3 = vp7.a.length + (i6 - this.g);
                                break;
                            } else if (i2 == -1) {
                                i2 = (i6 - this.g) + vp7.a.length;
                            }
                        }
                        i6++;
                    }
                }
                if (i3 != -1) {
                    a(i3, 127, 128);
                } else if (i2 == -1) {
                    this.a.writeByte(64);
                    a(asciiLowercase);
                    a(br7);
                    a(up7);
                } else if (!asciiLowercase.startsWith(up7.d) || up7.i.equals(asciiLowercase)) {
                    a(i2, 63, 64);
                    a(br7);
                    a(up7);
                } else {
                    a(i2, 15, 0);
                    a(br7);
                }
            }
        }

        @DexIgnore
        public void a(int i2, int i3, int i4) {
            if (i2 < i3) {
                this.a.writeByte(i2 | i4);
                return;
            }
            this.a.writeByte(i4 | i3);
            int i5 = i2 - i3;
            while (i5 >= 128) {
                this.a.writeByte(128 | (i5 & 127));
                i5 >>>= 7;
            }
            this.a.writeByte(i5);
        }

        @DexIgnore
        public void a(br7 br7) throws IOException {
            if (!this.b || cq7.b().a(br7) >= br7.size()) {
                a(br7.size(), 127, 0);
                this.a.a(br7);
                return;
            }
            yq7 yq7 = new yq7();
            cq7.b().a(br7, yq7);
            br7 o = yq7.o();
            a(o.size(), 127, 128);
            this.a.a(o);
        }

        @DexIgnore
        public final void a() {
            int i2 = this.e;
            int i3 = this.i;
            if (i2 >= i3) {
                return;
            }
            if (i2 == 0) {
                b();
            } else {
                a(i3 - i2);
            }
        }
    }

    @DexIgnore
    public static Map<br7, Integer> a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(a.length);
        int i = 0;
        while (true) {
            up7[] up7Arr = a;
            if (i >= up7Arr.length) {
                return Collections.unmodifiableMap(linkedHashMap);
            }
            if (!linkedHashMap.containsKey(up7Arr[i].a)) {
                linkedHashMap.put(a[i].a, Integer.valueOf(i));
            }
            i++;
        }
    }

    @DexIgnore
    public static br7 a(br7 br7) throws IOException {
        int size = br7.size();
        int i = 0;
        while (i < size) {
            byte b2 = br7.getByte(i);
            if (b2 < 65 || b2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + br7.utf8());
            }
        }
        return br7;
    }
}
