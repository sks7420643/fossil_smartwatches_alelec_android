package com.fossil;

import com.fossil.s87;
import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu implements rn7, gd7<Throwable, i97> {
    @DexIgnore
    public /* final */ qn7 a;
    @DexIgnore
    public /* final */ ai7<Response> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ai7<? super okhttp3.Response> */
    /* JADX WARN: Multi-variable type inference failed */
    public gu(qn7 qn7, ai7<? super Response> ai7) {
        ee7.b(qn7, "call");
        ee7.b(ai7, "continuation");
        this.a = qn7;
        this.b = ai7;
    }

    @DexIgnore
    public void a(Throwable th) {
        try {
            this.a.cancel();
        } catch (Throwable unused) {
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        a(th);
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.rn7
    public void onFailure(qn7 qn7, IOException iOException) {
        ee7.b(qn7, "call");
        ee7.b(iOException, "e");
        if (!qn7.e()) {
            ai7<Response> ai7 = this.b;
            s87.a aVar = s87.Companion;
            ai7.resumeWith(s87.m60constructorimpl(t87.a((Throwable) iOException)));
        }
    }

    @DexIgnore
    @Override // com.fossil.rn7
    public void onResponse(qn7 qn7, Response response) {
        ee7.b(qn7, "call");
        ee7.b(response, "response");
        ai7<Response> ai7 = this.b;
        s87.a aVar = s87.Companion;
        ai7.resumeWith(s87.m60constructorimpl(response));
    }
}
