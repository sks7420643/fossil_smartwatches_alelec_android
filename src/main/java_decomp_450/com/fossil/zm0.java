package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm0 extends lj0 {
    @DexIgnore
    public /* final */ boolean U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ zm0(com.fossil.ri1 r10, com.fossil.en0 r11, boolean r12, java.lang.String r13, int r14) {
        /*
            r9 = this;
            r14 = r14 & 8
            if (r14 == 0) goto L_0x000a
            java.lang.String r13 = "UUID.randomUUID().toString()"
            java.lang.String r13 = com.fossil.yh0.a(r13)
        L_0x000a:
            r7 = r13
            com.fossil.wm0 r3 = com.fossil.wm0.B
            com.fossil.rv0 r13 = new com.fossil.rv0
            r13.<init>(r12)
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            org.json.JSONObject r14 = new org.json.JSONObject     // Catch:{ JSONException -> 0x002b }
            r14.<init>()     // Catch:{ JSONException -> 0x002b }
            java.lang.String r0 = "set"
            org.json.JSONObject r13 = r13.b()
            r14.put(r0, r13)
            java.lang.String r13 = "push"
            r4.put(r13, r14)
            goto L_0x0031
        L_0x002b:
            r13 = move-exception
            com.fossil.wl0 r14 = com.fossil.wl0.h
            r14.a(r13)
        L_0x0031:
            r5 = 0
            r6 = 0
            r8 = 48
            r0 = r9
            r1 = r10
            r2 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r9.U = r12
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm0.<init>(com.fossil.ri1, com.fossil.en0, boolean, java.lang.String, int):void");
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.E, Boolean.valueOf(this.U));
    }
}
