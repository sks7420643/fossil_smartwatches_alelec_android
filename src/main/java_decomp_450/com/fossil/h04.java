package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h04<F, T> implements Iterator<T> {
    @DexIgnore
    public /* final */ Iterator<? extends F> a;

    @DexIgnore
    public h04(Iterator<? extends F> it) {
        jw3.a(it);
        this.a = it;
    }

    @DexIgnore
    public abstract T a(F f);

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.h04<F, T> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Iterator
    public final T next() {
        return (T) a(this.a.next());
    }

    @DexIgnore
    public final void remove() {
        this.a.remove();
    }
}
