package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jk4 implements Factory<GoogleApiService> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<hj5> b;
    @DexIgnore
    public /* final */ Provider<lj5> c;

    @DexIgnore
    public jk4(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static jk4 a(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        return new jk4(wj4, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService a(wj4 wj4, hj5 hj5, lj5 lj5) {
        GoogleApiService c2 = wj4.c(hj5, lj5);
        c87.a(c2, "Cannot return null from a non-@Nullable @Provides method");
        return c2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public GoogleApiService get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
