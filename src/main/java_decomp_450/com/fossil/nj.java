package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.Transition;
import androidx.transition.TransitionSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedApi"})
public class nj extends qc {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Transition.e {
        @DexIgnore
        public /* final */ /* synthetic */ Rect a;

        @DexIgnore
        public a(nj njVar, Rect rect) {
            this.a = rect;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Transition.f {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public b(nj njVar, View view, ArrayList arrayList) {
            this.a = view;
            this.b = arrayList;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void a(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void b(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void c(Transition transition) {
            transition.b(this);
            this.a.setVisibility(8);
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                ((View) this.b.get(i)).setVisibility(0);
            }
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void d(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void e(Transition transition) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends dk {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;

        @DexIgnore
        public c(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.a = obj;
            this.b = arrayList;
            this.c = obj2;
            this.d = arrayList2;
            this.e = obj3;
            this.f = arrayList3;
        }

        @DexIgnore
        @Override // com.fossil.dk, androidx.transition.Transition.f
        public void a(Transition transition) {
            Object obj = this.a;
            if (obj != null) {
                nj.this.a(obj, this.b, (ArrayList<View>) null);
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                nj.this.a(obj2, this.d, (ArrayList<View>) null);
            }
            Object obj3 = this.e;
            if (obj3 != null) {
                nj.this.a(obj3, this.f, (ArrayList<View>) null);
            }
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void c(Transition transition) {
            transition.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends Transition.e {
        @DexIgnore
        public /* final */ /* synthetic */ Rect a;

        @DexIgnore
        public d(nj njVar, Rect rect) {
            this.a = rect;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            Rect rect = this.a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public boolean a(Object obj) {
        return obj instanceof Transition;
    }

    @DexIgnore
    @Override // com.fossil.qc
    public Object b(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.qc
    public Object c(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.a((Transition) obj);
        return transitionSet;
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            int i = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int w = transitionSet.w();
                while (i < w) {
                    a(transitionSet.b(i), arrayList);
                    i++;
                }
            } else if (!a(transition) && qc.a((List) transition.r())) {
                int size = arrayList.size();
                while (i < size) {
                    transition.a(arrayList.get(i));
                    i++;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void b(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> r = transitionSet.r();
        r.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            qc.a(r, arrayList.get(i));
        }
        r.add(view);
        arrayList.add(view);
        a(transitionSet, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void c(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            a(view, rect);
            ((Transition) obj).a(new a(this, rect));
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public Object b(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.a((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.a((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.a((Transition) obj3);
        }
        return transitionSet;
    }

    @DexIgnore
    public static boolean a(Transition transition) {
        return !qc.a(transition.o()) || !qc.a(transition.p()) || !qc.a(transition.q());
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.r().clear();
            transitionSet.r().addAll(arrayList2);
            a((Object) transitionSet, arrayList, arrayList2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).a(new b(this, view, arrayList));
    }

    @DexIgnore
    @Override // com.fossil.qc
    public Object a(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition != null && transition2 != null) {
            TransitionSet transitionSet = new TransitionSet();
            transitionSet.a(transition);
            transitionSet.a(transition2);
            transitionSet.c(1);
            transition = transitionSet;
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        TransitionSet transitionSet2 = new TransitionSet();
        if (transition != null) {
            transitionSet2.a(transition);
        }
        transitionSet2.a(transition3);
        return transitionSet2;
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void b(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).d(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(ViewGroup viewGroup, Object obj) {
        ek.a(viewGroup, (Transition) obj);
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).a(new c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        int i;
        Transition transition = (Transition) obj;
        int i2 = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int w = transitionSet.w();
            while (i2 < w) {
                a((Object) transitionSet.b(i2), arrayList, arrayList2);
                i2++;
            }
        } else if (!a(transition)) {
            List<View> r = transition.r();
            if (r.size() == arrayList.size() && r.containsAll(arrayList)) {
                if (arrayList2 == null) {
                    i = 0;
                } else {
                    i = arrayList2.size();
                }
                while (i2 < i) {
                    transition.a(arrayList2.get(i2));
                    i2++;
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    transition.d(arrayList.get(size));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).a(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.qc
    public void a(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).a(new d(this, rect));
        }
    }
}
