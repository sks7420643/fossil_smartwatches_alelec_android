package com.fossil;

import android.os.Bundle;
import com.fossil.o62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f32 implements o62.a {
    @DexIgnore
    public /* final */ /* synthetic */ c32 a;

    @DexIgnore
    public f32(c32 c32) {
        this.a = c32;
    }

    @DexIgnore
    @Override // com.fossil.o62.a
    public final boolean c() {
        return this.a.g();
    }

    @DexIgnore
    @Override // com.fossil.o62.a
    public final Bundle q() {
        return null;
    }
}
