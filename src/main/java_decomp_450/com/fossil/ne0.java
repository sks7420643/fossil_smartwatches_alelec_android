package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne0 extends k60 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ qe0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ne0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ne0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    Object[] createTypedArray = parcel.createTypedArray(qe0.CREATOR);
                    if (createTypedArray != null) {
                        ee7.a((Object) createTypedArray, "parcel.createTypedArray(Player.CREATOR)!!");
                        return new ne0(readString, readString2, (qe0[]) createTypedArray);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ne0[] newArray(int i) {
            return new ne0[i];
        }
    }

    @DexIgnore
    public ne0(String str, String str2, qe0[] qe0Arr) {
        this.a = str;
        this.b = str2;
        this.c = qe0Arr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D4, this.a), r51.G, this.b), r51.H4, Integer.valueOf(this.c.length));
        yz0.a(a2, r51.F4, yz0.a(this.c));
        return a2;
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject a2 = yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D4, this.a), r51.G, this.b), r51.H4, Integer.valueOf(this.c.length));
        yz0.a(a2, r51.F4, yz0.a(this.c));
        return a2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.a;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final qe0[] getPlayers() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeTypedArray(this.c, i);
    }
}
