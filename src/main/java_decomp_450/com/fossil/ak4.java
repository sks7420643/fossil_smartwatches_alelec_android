package com.fossil;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ak4 implements Factory<Context> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public ak4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static ak4 a(wj4 wj4) {
        return new ak4(wj4);
    }

    @DexIgnore
    public static Context b(wj4 wj4) {
        Context c = wj4.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Context get() {
        return b(this.a);
    }
}
