package com.fossil;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e; // = AtomicReferenceFieldUpdater.newUpdater(dm7.class, Object.class, "_next");
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater f; // = AtomicLongFieldUpdater.newUpdater(dm7.class, "_state");
    @DexIgnore
    public static /* final */ lm7 g; // = new lm7("REMOVE_FROZEN");
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public volatile Object _next; // = null;
    @DexIgnore
    public volatile long _state; // = 0;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public AtomicReferenceArray b; // = new AtomicReferenceArray(this.c);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(long j) {
            return (j & 2305843009213693952L) != 0 ? 2 : 1;
        }

        @DexIgnore
        public final long a(long j, int i) {
            return a(j, 1073741823L) | (((long) i) << 0);
        }

        @DexIgnore
        public final long a(long j, long j2) {
            return j & (~j2);
        }

        @DexIgnore
        public final long b(long j, int i) {
            return a(j, 1152921503533105152L) | (((long) i) << 30);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public b(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public dm7(int i, boolean z) {
        this.c = i;
        this.d = z;
        boolean z2 = true;
        this.a = i - 1;
        if (this.a <= 1073741823) {
            if (!((this.c & this.a) != 0 ? false : z2)) {
                throw new IllegalStateException("Check failed.".toString());
            }
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final dm7<E> a(int i, E e2) {
        Object obj = this.b.get(this.a & i);
        if (!(obj instanceof b) || ((b) obj).a != i) {
            return null;
        }
        this.b.set(i & this.a, e2);
        return this;
    }

    @DexIgnore
    public final int b() {
        long j = this._state;
        return 1073741823 & (((int) ((j & 1152921503533105152L) >> 30)) - ((int) ((1073741823 & j) >> 0)));
    }

    @DexIgnore
    public final boolean c() {
        long j = this._state;
        return ((int) ((1073741823 & j) >> 0)) == ((int) ((j & 1152921503533105152L) >> 30));
    }

    @DexIgnore
    public final long d() {
        long j;
        long j2;
        do {
            j = this._state;
            if ((j & 1152921504606846976L) != 0) {
                return j;
            }
            j2 = j | 1152921504606846976L;
        } while (!f.compareAndSet(this, j, j2));
        return j2;
    }

    @DexIgnore
    public final dm7<E> e() {
        return b(d());
    }

    @DexIgnore
    public final Object f() {
        while (true) {
            long j = this._state;
            if ((1152921504606846976L & j) != 0) {
                return g;
            }
            int i = (int) ((1073741823 & j) >> 0);
            int i2 = (int) ((1152921503533105152L & j) >> 30);
            int i3 = this.a;
            if ((i2 & i3) == (i & i3)) {
                return null;
            }
            Object obj = this.b.get(i3 & i);
            if (obj == null) {
                if (this.d) {
                    return null;
                }
            } else if (obj instanceof b) {
                return null;
            } else {
                int i4 = (i + 1) & 1073741823;
                if (f.compareAndSet(this, j, h.a(j, i4))) {
                    this.b.set(this.a & i, null);
                    return obj;
                } else if (this.d) {
                    dm7<E> dm7 = this;
                    do {
                        dm7 = dm7.a(i, i4);
                    } while (dm7 != null);
                    return obj;
                }
            }
        }
    }

    @DexIgnore
    public final dm7<E> b(long j) {
        while (true) {
            dm7<E> dm7 = (dm7) this._next;
            if (dm7 != null) {
                return dm7;
            }
            e.compareAndSet(this, null, a(j));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v2, resolved type: java.util.concurrent.atomic.AtomicReferenceArray */
    /* JADX WARN: Multi-variable type inference failed */
    public final dm7<E> a(long j) {
        dm7<E> dm7 = new dm7<>(this.c * 2, this.d);
        int i = (int) ((1073741823 & j) >> 0);
        int i2 = (int) ((1152921503533105152L & j) >> 30);
        while (true) {
            int i3 = this.a;
            if ((i & i3) != (i2 & i3)) {
                Object obj = this.b.get(i3 & i);
                if (obj == null) {
                    obj = new b(i);
                }
                dm7.b.set(dm7.a & i, obj);
                i++;
            } else {
                dm7._state = h.a(j, 1152921504606846976L);
                return dm7;
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        long j;
        do {
            j = this._state;
            if ((j & 2305843009213693952L) != 0) {
                return true;
            }
            if ((1152921504606846976L & j) != 0) {
                return false;
            }
        } while (!f.compareAndSet(this, j, j | 2305843009213693952L));
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(E r13) {
        /*
            r12 = this;
        L_0x0000:
            long r2 = r12._state
            r0 = 3458764513820540928(0x3000000000000000, double:1.727233711018889E-77)
            long r0 = r0 & r2
            r6 = 0
            int r4 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x0012
            com.fossil.dm7$a r13 = com.fossil.dm7.h
            int r13 = r13.a(r2)
            return r13
        L_0x0012:
            r0 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r0 = r0 & r2
            r8 = 0
            long r0 = r0 >> r8
            int r1 = (int) r0
            r4 = 1152921503533105152(0xfffffffc0000000, double:1.2882296003504729E-231)
            long r4 = r4 & r2
            r0 = 30
            long r4 = r4 >> r0
            int r9 = (int) r4
            int r10 = r12.a
            int r0 = r9 + 2
            r0 = r0 & r10
            r4 = r1 & r10
            r5 = 1
            if (r0 != r4) goto L_0x002e
            return r5
        L_0x002e:
            boolean r0 = r12.d
            r4 = 1073741823(0x3fffffff, float:1.9999999)
            if (r0 != 0) goto L_0x004d
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r12.b
            r11 = r9 & r10
            java.lang.Object r0 = r0.get(r11)
            if (r0 == 0) goto L_0x004d
            int r0 = r12.c
            r2 = 1024(0x400, float:1.435E-42)
            if (r0 < r2) goto L_0x004c
            int r9 = r9 - r1
            r1 = r9 & r4
            int r0 = r0 >> 1
            if (r1 <= r0) goto L_0x0000
        L_0x004c:
            return r5
        L_0x004d:
            int r0 = r9 + 1
            r0 = r0 & r4
            java.util.concurrent.atomic.AtomicLongFieldUpdater r1 = com.fossil.dm7.f
            com.fossil.dm7$a r4 = com.fossil.dm7.h
            long r4 = r4.b(r2, r0)
            r0 = r1
            r1 = r12
            boolean r0 = r0.compareAndSet(r1, r2, r4)
            if (r0 == 0) goto L_0x0000
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r12.b
            r1 = r9 & r10
            r0.set(r1, r13)
            r0 = r12
        L_0x0068:
            long r1 = r0._state
            r3 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r1 = r1 & r3
            int r3 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r3 != 0) goto L_0x0072
            goto L_0x007d
        L_0x0072:
            com.fossil.dm7 r0 = r0.e()
            com.fossil.dm7 r0 = r0.a(r9, r13)
            if (r0 == 0) goto L_0x007d
            goto L_0x0068
        L_0x007d:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dm7.a(java.lang.Object):int");
    }

    @DexIgnore
    public final dm7<E> a(int i, int i2) {
        long j;
        int i3;
        do {
            j = this._state;
            boolean z = false;
            i3 = (int) ((1073741823 & j) >> 0);
            if (dj7.a()) {
                if (i3 == i) {
                    z = true;
                }
                if (!z) {
                    throw new AssertionError();
                }
            }
            if ((1152921504606846976L & j) != 0) {
                return e();
            }
        } while (!f.compareAndSet(this, j, h.a(j, i2)));
        this.b.set(this.a & i3, null);
        return null;
    }
}
