package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y50 {
    @DexIgnore
    public static y50 b() {
        return new b();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends y50 {
        @DexIgnore
        public volatile boolean a;

        @DexIgnore
        public b() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.y50
        public void a() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }

        @DexIgnore
        @Override // com.fossil.y50
        public void a(boolean z) {
            this.a = z;
        }
    }

    @DexIgnore
    public y50() {
    }
}
