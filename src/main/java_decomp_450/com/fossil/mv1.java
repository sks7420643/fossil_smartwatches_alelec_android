package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv1 {
    @DexIgnore
    public static <TInput, TResult, TException extends Throwable> TResult a(int i, TInput tinput, lv1<TInput, TResult, TException> lv1, nv1<TInput, TResult> nv1) throws Throwable {
        TResult apply;
        if (i < 1) {
            return lv1.apply(tinput);
        }
        do {
            apply = lv1.apply(tinput);
            tinput = nv1.a(tinput, apply);
            if (tinput == null) {
                break;
            }
            i--;
        } while (i >= 1);
        return apply;
    }
}
