package com.fossil;

import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx6 {
    @DexIgnore
    public static /* synthetic */ List a(List list, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(list, z);
    }

    @DexIgnore
    public static final AppNotificationFilter a() {
        AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getAppName(), DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getPackageName(), "", NotificationBaseObj.ANotificationType.NOTIFICATION));
        appNotificationFilter.setVibePattern(ca0.DEFAULT_OTHER_APPS);
        return appNotificationFilter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter> a(java.util.List<com.fossil.at5> r10, boolean r11) {
        /*
            java.lang.String r0 = "$this$toAppNotificationFilter"
            com.fossil.ee7.b(r10, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r1 = r10.iterator()
        L_0x000e:
            boolean r2 = r1.hasNext()
            r3 = 0
            if (r2 == 0) goto L_0x003e
            java.lang.Object r2 = r1.next()
            com.fossil.at5 r2 = (com.fossil.at5) r2
            com.portfolio.platform.data.model.InstalledApp r2 = r2.getInstalledApp()
            if (r2 == 0) goto L_0x0038
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r2.getIdentifier()
            r3.append(r4)
            java.lang.Boolean r2 = r2.isSelected()
            r3.append(r2)
            java.lang.String r3 = r3.toString()
        L_0x0038:
            if (r3 == 0) goto L_0x000e
            r0.add(r3)
            goto L_0x000e
        L_0x003e:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "toAppNotificationFilter(), inputAppWrapper = "
            r2.append(r4)
            r2.append(r0)
            java.lang.String r0 = ", isAllAppEnabled="
            r2.append(r0)
            r2.append(r11)
            java.lang.String r0 = r2.toString()
            java.lang.String r2 = "NotificationExtension"
            r1.d(r2, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r10 = r10.iterator()
        L_0x006b:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x012d
            java.lang.Object r1 = r10.next()
            com.fossil.at5 r1 = (com.fossil.at5) r1
            com.portfolio.platform.data.model.InstalledApp r1 = r1.getInstalledApp()
            if (r1 == 0) goto L_0x0125
            java.lang.String r4 = r1.getIdentifier()
            java.lang.Boolean r5 = r1.isSelected()
            java.lang.String r6 = "installedApp.isSelected"
            com.fossil.ee7.a(r5, r6)
            boolean r5 = r5.booleanValue()
            if (r5 != 0) goto L_0x0092
            if (r11 == 0) goto L_0x0125
        L_0x0092:
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r5 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            java.lang.String r6 = "packageNameApp"
            com.fossil.ee7.a(r4, r6)
            boolean r5 = r5.isPackageNameSupportedBySDK(r4)
            if (r5 == 0) goto L_0x00c0
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r1 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r1 = r1.getSupportedBySDKApp(r4)
            com.misfit.frameworks.buttonservice.model.notification.FNotification r4 = new com.misfit.frameworks.buttonservice.model.notification.FNotification
            java.lang.String r5 = r1.getAppName()
            java.lang.String r6 = r1.getPackageName()
            java.lang.String r7 = r1.getIconFwPath()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r1 = r1.getNotificationType()
            r4.<init>(r5, r6, r7, r1)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter
            r1.<init>(r4)
            goto L_0x0126
        L_0x00c0:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "app name "
            r5.append(r6)
            java.lang.String r6 = r1.getTitle()
            r5.append(r6)
            java.lang.String r6 = " identifier "
            r5.append(r6)
            java.lang.String r6 = r1.getIdentifier()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "toAppNotificationFilter"
            r4.d(r6, r5)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r4 = new com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName
            java.lang.String r5 = r1.getTitle()
            java.lang.String r6 = "installedApp.title"
            com.fossil.ee7.a(r5, r6)
            java.lang.String r6 = r1.getIdentifier()
            java.lang.String r7 = "installedApp.identifier"
            com.fossil.ee7.a(r6, r7)
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r8 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.NOTIFICATION
            java.lang.String r9 = "general_white.bin"
            r4.<init>(r5, r6, r9, r8)
            com.misfit.frameworks.buttonservice.model.notification.FNotification r5 = new com.misfit.frameworks.buttonservice.model.notification.FNotification
            java.lang.String r6 = r4.getAppName()
            java.lang.String r1 = r1.getIdentifier()
            com.fossil.ee7.a(r1, r7)
            java.lang.String r7 = r4.getIconFwPath()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r4 = r4.getNotificationType()
            r5.<init>(r6, r1, r7, r4)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter
            r1.<init>(r5)
            goto L_0x0126
        L_0x0125:
            r1 = r3
        L_0x0126:
            if (r1 == 0) goto L_0x006b
            r0.add(r1)
            goto L_0x006b
        L_0x012d:
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>(r0)
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter r11 = a()
            r10.add(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "toAppNotificationFilter(), output = "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11.d(r2, r0)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mx6.a(java.util.List, boolean):java.util.List");
    }
}
