package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ah3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ wg3 e;

    @DexIgnore
    public ah3(wg3 wg3, String str, long j) {
        this.e = wg3;
        a72.b(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.s().getLong(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(long j) {
        SharedPreferences.Editor edit = this.e.s().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
