package com.fossil;

import java.io.Closeable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sr7 extends Closeable {
    @DexIgnore
    long b(yq7 yq7, long j) throws IOException;

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close() throws IOException;

    @DexIgnore
    tr7 d();
}
