package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hv3;
import com.fossil.iv3;
import com.fossil.jv3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dv3 extends Drawable implements q7, kv3 {
    @DexIgnore
    public static /* final */ Paint A; // = new Paint(1);
    @DexIgnore
    public c a;
    @DexIgnore
    public /* final */ jv3.g[] b;
    @DexIgnore
    public /* final */ jv3.g[] c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Matrix e;
    @DexIgnore
    public /* final */ Path f;
    @DexIgnore
    public /* final */ Path g;
    @DexIgnore
    public /* final */ RectF h;
    @DexIgnore
    public /* final */ RectF i;
    @DexIgnore
    public /* final */ Region j;
    @DexIgnore
    public /* final */ Region p;
    @DexIgnore
    public hv3 q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ vu3 t;
    @DexIgnore
    public /* final */ iv3.a u;
    @DexIgnore
    public /* final */ iv3 v;
    @DexIgnore
    public PorterDuffColorFilter w;
    @DexIgnore
    public PorterDuffColorFilter x;
    @DexIgnore
    public Rect y;
    @DexIgnore
    public /* final */ RectF z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements iv3.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.iv3.a
        public void a(jv3 jv3, Matrix matrix, int i) {
            dv3.this.b[i] = jv3.a(matrix);
        }

        @DexIgnore
        @Override // com.fossil.iv3.a
        public void b(jv3 jv3, Matrix matrix, int i) {
            dv3.this.c[i] = jv3.a(matrix);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements hv3.c {
        @DexIgnore
        public /* final */ /* synthetic */ float a;

        @DexIgnore
        public b(dv3 dv3, float f) {
            this.a = f;
        }

        @DexIgnore
        @Override // com.fossil.hv3.c
        public zu3 a(zu3 zu3) {
            return zu3 instanceof fv3 ? zu3 : new yu3(this.a, zu3);
        }
    }

    @DexIgnore
    public /* synthetic */ dv3(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public static int a(int i2, int i3) {
        return (i2 * (i3 + (i3 >>> 7))) >>> 8;
    }

    @DexIgnore
    public final boolean A() {
        return Build.VERSION.SDK_INT < 21 || (!z() && !this.f.isConvex());
    }

    @DexIgnore
    public final boolean B() {
        PorterDuffColorFilter porterDuffColorFilter = this.w;
        PorterDuffColorFilter porterDuffColorFilter2 = this.x;
        c cVar = this.a;
        this.w = a(cVar.g, cVar.h, this.r, true);
        c cVar2 = this.a;
        this.x = a(cVar2.f, cVar2.h, this.s, false);
        c cVar3 = this.a;
        if (cVar3.u) {
            this.t.a(cVar3.g.getColorForState(getState(), 0));
        }
        if (!z8.a(porterDuffColorFilter, this.w) || !z8.a(porterDuffColorFilter2, this.x)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void C() {
        float t2 = t();
        this.a.r = (int) Math.ceil((double) (0.75f * t2));
        this.a.s = (int) Math.ceil((double) (t2 * 0.25f));
        B();
        x();
    }

    @DexIgnore
    public void c(float f2) {
        c cVar = this.a;
        if (cVar.k != f2) {
            cVar.k = f2;
            this.d = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void d(int i2) {
        c cVar = this.a;
        if (cVar.q != i2) {
            cVar.q = i2;
            x();
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.r.setColorFilter(this.w);
        int alpha = this.r.getAlpha();
        this.r.setAlpha(a(alpha, this.a.m));
        this.s.setColorFilter(this.x);
        this.s.setStrokeWidth(this.a.l);
        int alpha2 = this.s.getAlpha();
        this.s.setAlpha(a(alpha2, this.a.m));
        if (this.d) {
            b();
            a(e(), this.f);
            this.d = false;
        }
        if (u()) {
            canvas.save();
            d(canvas);
            int width = (int) (this.z.width() - ((float) getBounds().width()));
            int height = (int) (this.z.height() - ((float) getBounds().height()));
            Bitmap createBitmap = Bitmap.createBitmap(((int) this.z.width()) + (this.a.r * 2) + width, ((int) this.z.height()) + (this.a.r * 2) + height, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap);
            float f2 = (float) ((getBounds().left - this.a.r) - width);
            float f3 = (float) ((getBounds().top - this.a.r) - height);
            canvas2.translate(-f2, -f3);
            a(canvas2);
            canvas.drawBitmap(createBitmap, f2, f3, (Paint) null);
            createBitmap.recycle();
            canvas.restore();
        }
        if (v()) {
            b(canvas);
        }
        if (w()) {
            c(canvas);
        }
        this.r.setAlpha(alpha);
        this.s.setAlpha(alpha2);
    }

    @DexIgnore
    public void e(float f2) {
        this.a.l = f2;
        invalidateSelf();
    }

    @DexIgnore
    public final RectF f() {
        RectF e2 = e();
        float o = o();
        this.i.set(e2.left + o, e2.top + o, e2.right - o, e2.bottom - o);
        return this.i;
    }

    @DexIgnore
    public float g() {
        return this.a.o;
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.a.q != 2) {
            if (z()) {
                outline.setRoundRect(getBounds(), q());
                return;
            }
            a(e(), this.f);
            if (this.f.isConvex()) {
                outline.setConvexPath(this.f);
            }
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        Rect rect2 = this.y;
        if (rect2 == null) {
            return super.getPadding(rect);
        }
        rect.set(rect2);
        return true;
    }

    @DexIgnore
    public Region getTransparentRegion() {
        this.j.set(getBounds());
        a(e(), this.f);
        this.p.setPath(this.f, this.j);
        this.j.op(this.p, Region.Op.DIFFERENCE);
        return this.j;
    }

    @DexIgnore
    public ColorStateList h() {
        return this.a.d;
    }

    @DexIgnore
    public float i() {
        return this.a.k;
    }

    @DexIgnore
    public void invalidateSelf() {
        this.d = true;
        super.invalidateSelf();
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        return super.isStateful() || ((colorStateList = this.a.g) != null && colorStateList.isStateful()) || (((colorStateList2 = this.a.f) != null && colorStateList2.isStateful()) || (((colorStateList3 = this.a.e) != null && colorStateList3.isStateful()) || ((colorStateList4 = this.a.d) != null && colorStateList4.isStateful())));
    }

    @DexIgnore
    public float j() {
        return this.a.n;
    }

    @DexIgnore
    public int k() {
        c cVar = this.a;
        return (int) (((double) cVar.s) * Math.sin(Math.toRadians((double) cVar.t)));
    }

    @DexIgnore
    public int l() {
        c cVar = this.a;
        return (int) (((double) cVar.s) * Math.cos(Math.toRadians((double) cVar.t)));
    }

    @DexIgnore
    public int m() {
        return this.a.r;
    }

    @DexIgnore
    public Drawable mutate() {
        this.a = new c(this.a);
        return this;
    }

    @DexIgnore
    public hv3 n() {
        return this.a.a;
    }

    @DexIgnore
    public final float o() {
        return w() ? this.s.getStrokeWidth() / 2.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.d = true;
        super.onBoundsChange(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean z2 = a(iArr) || B();
        if (z2) {
            invalidateSelf();
        }
        return z2;
    }

    @DexIgnore
    public ColorStateList p() {
        return this.a.g;
    }

    @DexIgnore
    public float q() {
        return this.a.a.j().a(e());
    }

    @DexIgnore
    public float r() {
        return this.a.a.l().a(e());
    }

    @DexIgnore
    public float s() {
        return this.a.p;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        c cVar = this.a;
        if (cVar.m != i2) {
            cVar.m = i2;
            x();
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.c = colorFilter;
        x();
    }

    @DexIgnore
    @Override // com.fossil.kv3
    public void setShapeAppearanceModel(hv3 hv3) {
        this.a.a = hv3;
        invalidateSelf();
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintList(ColorStateList colorStateList) {
        this.a.g = colorStateList;
        B();
        x();
    }

    @DexIgnore
    @Override // com.fossil.q7
    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.a;
        if (cVar.h != mode) {
            cVar.h = mode;
            B();
            x();
        }
    }

    @DexIgnore
    public float t() {
        return g() + s();
    }

    @DexIgnore
    public final boolean u() {
        c cVar = this.a;
        int i2 = cVar.q;
        if (i2 == 1 || cVar.r <= 0 || (i2 != 2 && !A())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean v() {
        Paint.Style style = this.a.v;
        return style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.FILL;
    }

    @DexIgnore
    public final boolean w() {
        Paint.Style style = this.a.v;
        return (style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.STROKE) && this.s.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void x() {
        super.invalidateSelf();
    }

    @DexIgnore
    public boolean y() {
        rt3 rt3 = this.a.b;
        return rt3 != null && rt3.a();
    }

    @DexIgnore
    public boolean z() {
        return this.a.a.a(e());
    }

    @DexIgnore
    public dv3() {
        this(new hv3());
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.e != colorStateList) {
            cVar.e = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public dv3(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(hv3.a(context, attributeSet, i2, i3).a());
    }

    @DexIgnore
    public static dv3 a(Context context, float f2) {
        int a2 = vs3.a(context, jr3.colorSurface, dv3.class.getSimpleName());
        dv3 dv3 = new dv3();
        dv3.a(context);
        dv3.a(ColorStateList.valueOf(a2));
        dv3.b(f2);
        return dv3;
    }

    @DexIgnore
    public RectF e() {
        Rect bounds = getBounds();
        this.h.set((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        return this.h;
    }

    @DexIgnore
    public dv3(hv3 hv3) {
        this(new c(hv3, null));
    }

    @DexIgnore
    public void d(float f2) {
        c cVar = this.a;
        if (cVar.n != f2) {
            cVar.n = f2;
            C();
        }
    }

    @DexIgnore
    public dv3(c cVar) {
        this.b = new jv3.g[4];
        this.c = new jv3.g[4];
        this.e = new Matrix();
        this.f = new Path();
        this.g = new Path();
        this.h = new RectF();
        this.i = new RectF();
        this.j = new Region();
        this.p = new Region();
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new vu3();
        this.v = new iv3();
        this.z = new RectF();
        this.a = cVar;
        this.s.setStyle(Paint.Style.STROKE);
        this.r.setStyle(Paint.Style.FILL);
        A.setColor(-1);
        A.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        B();
        a(getState());
        this.u = new a();
    }

    @DexIgnore
    public void b(float f2) {
        c cVar = this.a;
        if (cVar.o != f2) {
            cVar.o = f2;
            C();
        }
    }

    @DexIgnore
    public void c(int i2) {
        c cVar = this.a;
        if (cVar.t != i2) {
            cVar.t = i2;
            x();
        }
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        int k = k();
        int l = l();
        if (Build.VERSION.SDK_INT < 21) {
            Rect clipBounds = canvas.getClipBounds();
            int i2 = this.a.r;
            clipBounds.inset(-i2, -i2);
            clipBounds.offset(k, l);
            canvas.clipRect(clipBounds, Region.Op.REPLACE);
        }
        canvas.translate((float) k, (float) l);
    }

    @DexIgnore
    public void b(int i2) {
        this.t.a(i2);
        this.a.u = false;
        x();
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        a(canvas, this.s, this.g, this.q, f());
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.d != colorStateList) {
            cVar.d = colorStateList;
            onStateChange(getState());
        }
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        a(canvas, this.r, this.f, this.a.a, e());
    }

    @DexIgnore
    public float c() {
        return this.a.a.c().a(e());
    }

    @DexIgnore
    public final void b(RectF rectF, Path path) {
        iv3 iv3 = this.v;
        c cVar = this.a;
        iv3.a(cVar.a, cVar.k, rectF, this.u, path);
    }

    @DexIgnore
    public void a(float f2, int i2) {
        e(f2);
        b(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public final void b() {
        hv3 a2 = n().a(new b(this, -o()));
        this.q = a2;
        this.v.a(a2, this.a.k, f(), this.g);
    }

    @DexIgnore
    public void a(float f2, ColorStateList colorStateList) {
        e(f2);
        b(colorStateList);
    }

    @DexIgnore
    public float d() {
        return this.a.a.e().a(e());
    }

    @DexIgnore
    public void a(float f2) {
        setShapeAppearanceModel(this.a.a.a(f2));
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        c cVar = this.a;
        if (cVar.i == null) {
            cVar.i = new Rect();
        }
        this.a.i.set(i2, i3, i4, i5);
        this.y = this.a.i;
        invalidateSelf();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Drawable.ConstantState {
        @DexIgnore
        public hv3 a;
        @DexIgnore
        public rt3 b;
        @DexIgnore
        public ColorFilter c;
        @DexIgnore
        public ColorStateList d; // = null;
        @DexIgnore
        public ColorStateList e; // = null;
        @DexIgnore
        public ColorStateList f; // = null;
        @DexIgnore
        public ColorStateList g; // = null;
        @DexIgnore
        public PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
        @DexIgnore
        public Rect i; // = null;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = 1.0f;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m; // = 255;
        @DexIgnore
        public float n; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float o; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float p; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public int q; // = 0;
        @DexIgnore
        public int r; // = 0;
        @DexIgnore
        public int s; // = 0;
        @DexIgnore
        public int t; // = 0;
        @DexIgnore
        public boolean u; // = false;
        @DexIgnore
        public Paint.Style v; // = Paint.Style.FILL_AND_STROKE;

        @DexIgnore
        public c(hv3 hv3, rt3 rt3) {
            this.a = hv3;
            this.b = rt3;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            dv3 dv3 = new dv3(this, null);
            boolean unused = dv3.d = true;
            return dv3;
        }

        @DexIgnore
        public c(c cVar) {
            this.a = cVar.a;
            this.b = cVar.b;
            this.l = cVar.l;
            this.c = cVar.c;
            this.d = cVar.d;
            this.e = cVar.e;
            this.h = cVar.h;
            this.g = cVar.g;
            this.m = cVar.m;
            this.j = cVar.j;
            this.s = cVar.s;
            this.q = cVar.q;
            this.u = cVar.u;
            this.k = cVar.k;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
            this.r = cVar.r;
            this.t = cVar.t;
            this.f = cVar.f;
            this.v = cVar.v;
            if (cVar.i != null) {
                this.i = new Rect(cVar.i);
            }
        }
    }

    @DexIgnore
    public void a(Context context) {
        this.a.b = new rt3(context);
        C();
    }

    @DexIgnore
    public final int a(int i2) {
        float t2 = t() + j();
        rt3 rt3 = this.a.b;
        return rt3 != null ? rt3.b(i2, t2) : i2;
    }

    @DexIgnore
    public void a(Paint.Style style) {
        this.a.v = style;
        x();
    }

    @DexIgnore
    public void a(Canvas canvas, Paint paint, Path path, RectF rectF) {
        a(canvas, paint, path, this.a.a, rectF);
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, Path path, hv3 hv3, RectF rectF) {
        if (hv3.a(rectF)) {
            float a2 = hv3.l().a(rectF);
            canvas.drawRoundRect(rectF, a2, a2, paint);
            return;
        }
        canvas.drawPath(path, paint);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        if (this.a.s != 0) {
            canvas.drawPath(this.f, this.t.a());
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.b[i2].a(this.t, this.a.r, canvas);
            this.c[i2].a(this.t, this.a.r, canvas);
        }
        int k = k();
        int l = l();
        canvas.translate((float) (-k), (float) (-l));
        canvas.drawPath(this.f, A);
        canvas.translate((float) k, (float) l);
    }

    @DexIgnore
    public final void a(RectF rectF, Path path) {
        b(rectF, path);
        if (this.a.j != 1.0f) {
            this.e.reset();
            Matrix matrix = this.e;
            float f2 = this.a.j;
            matrix.setScale(f2, f2, rectF.width() / 2.0f, rectF.height() / 2.0f);
            path.transform(this.e);
        }
        path.computeBounds(this.z, true);
    }

    @DexIgnore
    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, Paint paint, boolean z2) {
        if (colorStateList == null || mode == null) {
            return a(paint, z2);
        }
        return a(colorStateList, mode, z2);
    }

    @DexIgnore
    public final PorterDuffColorFilter a(Paint paint, boolean z2) {
        int color;
        int a2;
        if (!z2 || (a2 = a((color = paint.getColor()))) == color) {
            return null;
        }
        return new PorterDuffColorFilter(a2, PorterDuff.Mode.SRC_IN);
    }

    @DexIgnore
    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, boolean z2) {
        int colorForState = colorStateList.getColorForState(getState(), 0);
        if (z2) {
            colorForState = a(colorForState);
        }
        return new PorterDuffColorFilter(colorForState, mode);
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        boolean z2;
        int color;
        int colorForState;
        int color2;
        int colorForState2;
        if (this.a.d == null || color2 == (colorForState2 = this.a.d.getColorForState(iArr, (color2 = this.r.getColor())))) {
            z2 = false;
        } else {
            this.r.setColor(colorForState2);
            z2 = true;
        }
        if (this.a.e == null || color == (colorForState = this.a.e.getColorForState(iArr, (color = this.s.getColor())))) {
            return z2;
        }
        this.s.setColor(colorForState);
        return true;
    }
}
