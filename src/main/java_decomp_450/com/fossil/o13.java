package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o13 implements tr2<r13> {
    @DexIgnore
    public static o13 b; // = new o13();
    @DexIgnore
    public /* final */ tr2<r13> a;

    @DexIgnore
    public o13(tr2<r13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((r13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((r13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ r13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public o13() {
        this(sr2.a(new q13()));
    }
}
