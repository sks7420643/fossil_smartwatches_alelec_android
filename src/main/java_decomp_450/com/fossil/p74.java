package com.fossil;

import android.content.Context;
import com.facebook.AccessToken;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p74 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public p74(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final File a() {
        return new File(new w64(this.a).b(), "com.crashlytics.settings.json");
    }

    @DexIgnore
    public JSONObject b() {
        FileInputStream fileInputStream;
        JSONObject jSONObject;
        z24.a().a("Reading cached settings...");
        FileInputStream fileInputStream2 = null;
        try {
            File a2 = a();
            if (a2.exists()) {
                fileInputStream = new FileInputStream(a2);
                try {
                    jSONObject = new JSONObject(t34.b(fileInputStream));
                    fileInputStream2 = fileInputStream;
                } catch (Exception e) {
                    e = e;
                    try {
                        z24.a().b("Failed to fetch cached settings", e);
                        t34.a((Closeable) fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        t34.a((Closeable) fileInputStream, "Error while closing settings cache file.");
                        throw th;
                    }
                }
            } else {
                z24.a().a("No cached settings found.");
                jSONObject = null;
            }
            t34.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e2) {
            e = e2;
            fileInputStream = null;
            z24.a().b("Failed to fetch cached settings", e);
            t34.a((Closeable) fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            fileInputStream = null;
            th = th2;
            t34.a((Closeable) fileInputStream, "Error while closing settings cache file.");
            throw th;
        }
    }

    @DexIgnore
    public void a(long j, JSONObject jSONObject) {
        z24.a().a("Writing settings to cache file...");
        if (jSONObject != null) {
            FileWriter fileWriter = null;
            try {
                jSONObject.put(AccessToken.EXPIRES_AT_KEY, j);
                FileWriter fileWriter2 = new FileWriter(a());
                try {
                    fileWriter2.write(jSONObject.toString());
                    fileWriter2.flush();
                    t34.a((Closeable) fileWriter2, "Failed to close settings writer.");
                } catch (Exception e) {
                    e = e;
                    fileWriter = fileWriter2;
                    try {
                        z24.a().b("Failed to cache settings", e);
                        t34.a((Closeable) fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        th = th;
                        t34.a((Closeable) fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileWriter = fileWriter2;
                    t34.a((Closeable) fileWriter, "Failed to close settings writer.");
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                z24.a().b("Failed to cache settings", e);
                t34.a((Closeable) fileWriter, "Failed to close settings writer.");
            }
        }
    }
}
