package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gf7 {
    @DexIgnore
    public static /* final */ gf7 a; // = bc7.a.a();
    @DexIgnore
    public static /* final */ b b; // = new b(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends gf7 {
        @DexIgnore
        public static /* final */ a c; // = new a();

        @DexIgnore
        @Override // com.fossil.gf7
        public int a(int i) {
            return gf7.b.a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends gf7 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.gf7
        public int a(int i) {
            return gf7.a.a(i);
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.gf7
        public int a() {
            return gf7.a.a();
        }
    }

    /*
    static {
        a aVar = a.c;
    }
    */

    @DexIgnore
    public int a() {
        return a(32);
    }

    @DexIgnore
    public abstract int a(int i);
}
