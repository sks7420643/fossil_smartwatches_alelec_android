package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q53 implements Parcelable.Creator<p53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ p53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = "";
        String str2 = str;
        String str3 = str2;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str2 = j72.e(parcel, a);
            } else if (a2 == 2) {
                str3 = j72.e(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                str = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new p53(str, str2, str3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ p53[] newArray(int i) {
        return new p53[i];
    }
}
