package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b23 implements y13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.collection.efficient_engagement_reporting_enabled_2", true);
        b = dr2.a("measurement.collection.redundant_engagement_removal_enabled", false);
        dr2.a("measurement.id.collection.redundant_engagement_removal_enabled", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.y13
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.y13
    public final boolean zzb() {
        return b.b().booleanValue();
    }
}
