package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.fl4;
import com.fossil.kw6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vg5 implements dk5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<bk5> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ yi7 b; // = zi7.a(dl7.a(null, 1, null).plus(qj7.b()));
    @DexIgnore
    public /* final */ HybridPresetRepository c;
    @DexIgnore
    public /* final */ AlarmsRepository d;
    @DexIgnore
    public /* final */ uj5 e;
    @DexIgnore
    public /* final */ wk5 f;
    @DexIgnore
    public /* final */ QuickResponseRepository g;
    @DexIgnore
    public /* final */ ek5 h;
    @DexIgnore
    public /* final */ kw6 i;
    @DexIgnore
    public /* final */ ch5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vg5.k;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ vg5 c;

        @DexIgnore
        public b(vg5 vg5, String str, int i) {
            ee7.b(str, "serial");
            this.c = vg5;
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = vg5.l.a();
            local.d(a2, "Inside SteamingAction .run - eventId=" + this.b + ", serial=" + this.a);
            if (TextUtils.isEmpty(this.a)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = vg5.l.a();
                local2.e(a3, "Inside .onStreamingEvent of " + this.a + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.c.c.getActivePresetBySerial(this.a);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (ee7.a((Object) next.getAppId(), (Object) MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.b).getValue())) {
                        int i = this.b;
                        if (i == cb0.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = vg5.l.a();
                            local3.d(a4, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            vg5 vg5 = this.c;
                            String str = this.a;
                            String settings = next.getSettings();
                            if (settings != null) {
                                vg5.a(str, settings);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else if (i == cb0.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == cb0.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.B.a(PortfolioApp.g0.c(), bundle, this.c);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LinkStreamingManager$onDeviceAppEvent$1", f = "LinkStreamingManager.kt", l = {109, 116, 123, 219}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jc5 $event;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vg5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ EnumMap $map$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, EnumMap enumMap, c cVar) {
                super(2, fb7);
                this.$map$inlined = enumMap;
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.$map$inlined, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ek5 d = this.this$0.this$0.h;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (d.b(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vg5 vg5, jc5 jc5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vg5;
            this.$event = jc5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$event, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:102:0x03c0  */
        /* JADX WARNING: Removed duplicated region for block: B:103:0x03c2  */
        /* JADX WARNING: Removed duplicated region for block: B:105:0x03c5  */
        /* JADX WARNING: Removed duplicated region for block: B:142:0x0337 A[SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
                r17 = this;
                r0 = r17
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r2 == 0) goto L_0x0038
                if (r2 == r6) goto L_0x0027
                if (r2 == r5) goto L_0x0027
                if (r2 == r4) goto L_0x002f
                if (r2 != r3) goto L_0x001f
                java.lang.Object r1 = r0.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r1 = r0.L$1
                java.lang.Boolean r1 = (java.lang.Boolean) r1
                goto L_0x002f
            L_0x001f:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0027:
                java.lang.Object r1 = r0.L$2
                com.fossil.if5 r1 = (com.fossil.if5) r1
                java.lang.Object r1 = r0.L$1
                java.lang.String r1 = (java.lang.String) r1
            L_0x002f:
                java.lang.Object r1 = r0.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r18)
                goto L_0x0580
            L_0x0038:
                com.fossil.t87.a(r18)
                com.fossil.yi7 r2 = r0.p$
                com.fossil.jc5 r7 = r0.$event
                int r7 = r7.a()
                com.fossil.cb0 r8 = com.fossil.cb0.WEATHER_COMPLICATION
                int r8 = r8.ordinal()
                java.lang.String r9 = "event.serial"
                if (r7 != r8) goto L_0x0087
                com.fossil.qd5$a r3 = com.fossil.qd5.f
                java.lang.String r4 = "weather"
                java.lang.String r3 = r3.d(r4)
                com.fossil.qd5$a r5 = com.fossil.qd5.f
                com.fossil.vg5 r7 = r0.this$0
                java.lang.String r7 = r7.a(r3)
                com.fossil.if5 r5 = r5.b(r7)
                r5.d()
                com.fossil.qd5$a r7 = com.fossil.qd5.f
                r7.a(r4, r5)
                com.fossil.gh5$a r4 = com.fossil.gh5.m
                com.fossil.gh5 r4 = r4.a()
                com.fossil.jc5 r7 = r0.$event
                java.lang.String r7 = r7.c()
                com.fossil.ee7.a(r7, r9)
                r0.L$0 = r2
                r0.L$1 = r3
                r0.L$2 = r5
                r0.label = r6
                java.lang.Object r2 = r4.b(r7, r0)
                if (r2 != r1) goto L_0x0580
                return r1
            L_0x0087:
                com.fossil.cb0 r8 = com.fossil.cb0.CHANCE_OF_RAIN_COMPLICATION
                int r8 = r8.ordinal()
                if (r7 != r8) goto L_0x00c9
                com.fossil.qd5$a r3 = com.fossil.qd5.f
                java.lang.String r4 = "chance-of-rain"
                java.lang.String r3 = r3.d(r4)
                com.fossil.qd5$a r6 = com.fossil.qd5.f
                com.fossil.vg5 r7 = r0.this$0
                java.lang.String r7 = r7.a(r3)
                com.fossil.if5 r6 = r6.b(r7)
                r6.d()
                com.fossil.qd5$a r7 = com.fossil.qd5.f
                r7.a(r4, r6)
                com.fossil.gh5$a r4 = com.fossil.gh5.m
                com.fossil.gh5 r4 = r4.a()
                com.fossil.jc5 r7 = r0.$event
                java.lang.String r7 = r7.c()
                com.fossil.ee7.a(r7, r9)
                r0.L$0 = r2
                r0.L$1 = r3
                r0.L$2 = r6
                r0.label = r5
                java.lang.Object r2 = r4.a(r7, r0)
                if (r2 != r1) goto L_0x0580
                return r1
            L_0x00c9:
                com.fossil.cb0 r8 = com.fossil.cb0.RING_PHONE
                int r8 = r8.ordinal()
                r10 = -1
                java.lang.String r11 = "DEVICE_REQUEST_ACTION"
                if (r7 != r8) goto L_0x00ee
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                int r1 = r1.getInt(r11, r10)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                r2.d(r3, r1)
                goto L_0x0580
            L_0x00ee:
                com.fossil.cb0 r8 = com.fossil.cb0.WEATHER_WATCH_APP
                int r8 = r8.ordinal()
                if (r7 != r8) goto L_0x0110
                com.fossil.gh5$a r3 = com.fossil.gh5.m
                com.fossil.gh5 r3 = r3.a()
                com.fossil.jc5 r5 = r0.$event
                java.lang.String r5 = r5.c()
                com.fossil.ee7.a(r5, r9)
                r0.L$0 = r2
                r0.label = r4
                java.lang.Object r2 = r3.c(r5, r0)
                if (r2 != r1) goto L_0x0580
                return r1
            L_0x0110:
                com.fossil.cb0 r4 = com.fossil.cb0.NOTIFICATION_FILTER_SYNC
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x0132
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                int r1 = r1.getInt(r11, r10)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                com.fossil.ik7 unused = r2.c(r3, r1)
                goto L_0x0580
            L_0x0132:
                com.fossil.cb0 r4 = com.fossil.cb0.ALARM_SYNC
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x0154
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                int r1 = r1.getInt(r11, r10)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                com.fossil.ik7 unused = r2.a(r3, r1)
                goto L_0x0580
            L_0x0154:
                com.fossil.cb0 r4 = com.fossil.cb0.DEVICE_CONFIG_SYNC
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x0176
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                int r1 = r1.getInt(r11, r10)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                com.fossil.ik7 unused = r2.b(r3, r1)
                goto L_0x0580
            L_0x0176:
                com.fossil.cb0 r4 = com.fossil.cb0.MUSIC_CONTROL
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x01e1
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                com.misfit.frameworks.buttonservice.ButtonService$Companion r2 = com.misfit.frameworks.buttonservice.ButtonService.Companion
                java.lang.String r2 = r2.getMUSIC_ACTION_EVENT()
                com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse$MusicMediaAction r3 = com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse.MusicMediaAction.NONE
                int r3 = r3.ordinal()
                int r1 = r1.getInt(r2, r3)
                com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse$MusicMediaAction[] r2 = com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse.MusicMediaAction.values()
                r1 = r2[r1]
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vg5$a r3 = com.fossil.vg5.l
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "Inside "
                r4.append(r5)
                com.fossil.vg5$a r5 = com.fossil.vg5.l
                java.lang.String r5 = r5.a()
                r4.append(r5)
                java.lang.String r5 = ".onMusicActionEvent - musicActionEvent="
                r4.append(r5)
                r4.append(r1)
                java.lang.String r5 = ", serial="
                r4.append(r5)
                com.fossil.jc5 r5 = r0.$event
                java.lang.String r5 = r5.c()
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r3, r4)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.ek5 r2 = r2.h
                r2.a(r1)
                goto L_0x0580
            L_0x01e1:
                com.fossil.cb0 r4 = com.fossil.cb0.APP_NOTIFICATION_CONTROL
                int r4 = r4.ordinal()
                java.lang.String r8 = "DEVICE_REQUEST_EXTRA"
                if (r7 != r4) goto L_0x020f
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vg5$a r2 = com.fossil.vg5.l
                java.lang.String r2 = r2.a()
                java.lang.String r3 = "on app notification control received"
                r1.d(r2, r3)
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                android.os.Parcelable r1 = r1.getParcelable(r8)
                com.fossil.kb0 r1 = (com.fossil.kb0) r1
                com.fossil.vg5 r2 = r0.this$0
                r2.a(r1)
                goto L_0x0580
            L_0x020f:
                com.fossil.cb0 r4 = com.fossil.cb0.COMMUTE_TIME_WATCH_APP
                int r4 = r4.ordinal()
                java.lang.String r12 = ""
                if (r7 != r4) goto L_0x0262
                com.fossil.qd5$a r1 = com.fossil.qd5.f
                java.lang.String r2 = "commute-time"
                java.lang.String r1 = r1.d(r2)
                com.fossil.qd5$a r3 = com.fossil.qd5.f
                com.fossil.vg5 r4 = r0.this$0
                java.lang.String r1 = r4.a(r1)
                com.fossil.if5 r1 = r3.b(r1)
                r1.d()
                com.fossil.qd5$a r3 = com.fossil.qd5.f
                r3.a(r2, r1)
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                int r1 = r1.getInt(r11, r10)
                com.fossil.jc5 r2 = r0.$event
                android.os.Bundle r2 = r2.b()
                java.lang.String r2 = r2.getString(r8, r12)
                com.fossil.tk5$a r3 = com.fossil.tk5.r
                com.fossil.tk5 r3 = r3.a()
                com.fossil.jc5 r4 = r0.$event
                java.lang.String r4 = r4.c()
                com.fossil.ee7.a(r4, r9)
                java.lang.String r5 = "destination"
                com.fossil.ee7.a(r2, r5)
                r3.a(r4, r2, r1)
                goto L_0x0580
            L_0x0262:
                com.fossil.cb0 r4 = com.fossil.cb0.WORKOUT_START
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x029b
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                android.os.Parcelable r1 = r1.getParcelable(r8)
                com.fossil.ec0 r1 = (com.fossil.ec0) r1
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vg5$a r3 = com.fossil.vg5.l
                java.lang.String r3 = r3.a()
                java.lang.String r4 = "startWorkout data workoutRequestData"
                r2.d(r3, r4)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.wk5 r2 = r2.f
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                r2.a(r3, r1)
                goto L_0x0580
            L_0x029b:
                com.fossil.cb0 r4 = com.fossil.cb0.WORKOUT_STOP
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x02a4
                goto L_0x02ac
            L_0x02a4:
                com.fossil.cb0 r4 = com.fossil.cb0.WORKOUT_PAUSE
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x02dd
            L_0x02ac:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vg5$a r2 = com.fossil.vg5.l
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "stopWorkout or pause workout "
                r3.append(r4)
                com.fossil.jc5 r4 = r0.$event
                int r4 = r4.a()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                com.fossil.vg5 r1 = r0.this$0
                com.fossil.wk5 r1 = r1.f
                r1.i()
                goto L_0x0580
            L_0x02dd:
                com.fossil.cb0 r4 = com.fossil.cb0.WORKOUT_RESUME
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x030d
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vg5$a r2 = com.fossil.vg5.l
                java.lang.String r2 = r2.a()
                java.lang.String r3 = "resumeWorkout"
                r1.d(r2, r3)
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                android.os.Parcelable r1 = r1.getParcelable(r8)
                com.fossil.bc0 r1 = (com.fossil.bc0) r1
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.wk5 r2 = r2.f
                r2.a(r1)
                goto L_0x0580
            L_0x030d:
                com.fossil.cb0 r4 = com.fossil.cb0.WATCH_APP_LIFE_CYCLE
                int r4 = r4.ordinal()
                r10 = 0
                if (r7 != r4) goto L_0x03ef
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                android.os.Parcelable r1 = r1.getParcelable(r8)
                com.fossil.qb0 r1 = (com.fossil.qb0) r1
                if (r1 == 0) goto L_0x0580
                java.util.EnumMap r1 = r1.getWatchAppStatus()
                if (r1 == 0) goto L_0x0580
                java.util.Set r2 = r1.keySet()
                java.lang.String r3 = "map.keys"
                com.fossil.ee7.a(r2, r3)
                java.util.Iterator r2 = r2.iterator()
            L_0x0337:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x03eb
                java.lang.Object r3 = r2.next()
                com.fossil.cg0 r3 = (com.fossil.cg0) r3
                if (r3 != 0) goto L_0x0346
                goto L_0x0337
            L_0x0346:
                int[] r4 = com.fossil.wg5.a
                int r7 = r3.ordinal()
                r4 = r4[r7]
                r7 = 0
                if (r4 == r6) goto L_0x039a
                if (r4 == r5) goto L_0x0354
                goto L_0x0337
            L_0x0354:
                java.lang.Object r3 = r1.get(r3)
                com.fossil.pa0[] r3 = (com.fossil.pa0[]) r3
                if (r3 == 0) goto L_0x0378
                int r4 = r3.length
                r8 = 0
            L_0x035e:
                if (r8 >= r4) goto L_0x0378
                r9 = r3[r8]
                com.fossil.pa0 r11 = com.fossil.pa0.MUSIC
                if (r9 != r11) goto L_0x0368
                r11 = 1
                goto L_0x0369
            L_0x0368:
                r11 = 0
            L_0x0369:
                java.lang.Boolean r11 = com.fossil.pb7.a(r11)
                boolean r11 = r11.booleanValue()
                if (r11 == 0) goto L_0x0375
                r7 = r9
                goto L_0x0378
            L_0x0375:
                int r8 = r8 + 1
                goto L_0x035e
            L_0x0378:
                if (r7 == 0) goto L_0x037c
                r3 = 1
                goto L_0x037d
            L_0x037c:
                r3 = 0
            L_0x037d:
                if (r3 == 0) goto L_0x0337
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.vg5$a r4 = com.fossil.vg5.l
                java.lang.String r4 = r4.a()
                java.lang.String r7 = "User close watch app music"
                r3.d(r4, r7)
                com.fossil.vg5 r3 = r0.this$0
                com.fossil.ek5 r3 = r3.h
                r3.a(r10)
                goto L_0x0337
            L_0x039a:
                java.lang.Object r3 = r1.get(r3)
                com.fossil.pa0[] r3 = (com.fossil.pa0[]) r3
                if (r3 == 0) goto L_0x03bd
                int r4 = r3.length
                r8 = 0
            L_0x03a4:
                if (r8 >= r4) goto L_0x03bd
                r9 = r3[r8]
                com.fossil.pa0 r11 = com.fossil.pa0.MUSIC
                if (r9 != r11) goto L_0x03ae
                r11 = 1
                goto L_0x03af
            L_0x03ae:
                r11 = 0
            L_0x03af:
                java.lang.Boolean r11 = com.fossil.pb7.a(r11)
                boolean r11 = r11.booleanValue()
                if (r11 == 0) goto L_0x03ba
                goto L_0x03be
            L_0x03ba:
                int r8 = r8 + 1
                goto L_0x03a4
            L_0x03bd:
                r9 = r7
            L_0x03be:
                if (r9 == 0) goto L_0x03c2
                r3 = 1
                goto L_0x03c3
            L_0x03c2:
                r3 = 0
            L_0x03c3:
                if (r3 == 0) goto L_0x0337
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.vg5$a r4 = com.fossil.vg5.l
                java.lang.String r4 = r4.a()
                java.lang.String r8 = "User open watch app music"
                r3.d(r4, r8)
                com.fossil.vg5 r3 = r0.this$0
                com.fossil.yi7 r11 = r3.b
                r12 = 0
                r13 = 0
                com.fossil.vg5$c$a r14 = new com.fossil.vg5$c$a
                r14.<init>(r7, r1, r0)
                r15 = 3
                r16 = 0
                com.fossil.ik7 unused = com.fossil.xh7.b(r11, r12, r13, r14, r15, r16)
                goto L_0x0337
            L_0x03eb:
                com.fossil.i97 r1 = com.fossil.i97.a
                goto L_0x0580
            L_0x03ef:
                com.fossil.cb0 r4 = com.fossil.cb0.AUTHENTICATION_REQUEST
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x0431
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vg5$a r2 = com.fossil.vg5.l
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Receive AUTHENTICATION_REQUEST serial= "
                r3.append(r4)
                com.fossil.jc5 r4 = r0.$event
                java.lang.String r4 = r4.c()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                com.fossil.jc5 r2 = r0.$event
                java.lang.String r2 = r2.c()
                com.fossil.ee7.a(r2, r9)
                r1.s(r2)
                goto L_0x0580
            L_0x0431:
                com.fossil.cb0 r4 = com.fossil.cb0.ENCRYPTED_DATA
                int r4 = r4.ordinal()
                java.lang.String r5 = "isBcOn"
                if (r7 != r4) goto L_0x04ae
                com.fossil.vg5 r1 = r0.this$0
                com.fossil.ch5 r1 = r1.j
                java.lang.Boolean r1 = r1.a()
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vg5$a r3 = com.fossil.vg5.l
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r6 = "ENCRYPTED_DATA - isBCOn ="
                r4.append(r6)
                r4.append(r1)
                java.lang.String r4 = r4.toString()
                r2.e(r3, r4)
                com.fossil.ee7.a(r1, r5)
                boolean r1 = r1.booleanValue()
                if (r1 == 0) goto L_0x0580
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                android.os.Parcelable r1 = r1.getParcelable(r8)
                com.misfit.frameworks.buttonservice.model.notification.EncryptedData r1 = (com.misfit.frameworks.buttonservice.model.notification.EncryptedData) r1
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.vg5$a r3 = com.fossil.vg5.l
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "ENCRYPTED_DATA - "
                r4.append(r5)
                r4.append(r1)
                java.lang.String r4 = r4.toString()
                r2.e(r3, r4)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.uj5 r2 = r2.e
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                r2.a(r3, r1)
                goto L_0x0580
            L_0x04ae:
                com.fossil.cb0 r4 = com.fossil.cb0.BUDDY_CHALLENGE_LIST_CHALLENGES
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x04ca
                com.fossil.vg5 r1 = r0.this$0
                com.fossil.uj5 r1 = r1.e
                com.fossil.jc5 r2 = r0.$event
                java.lang.String r2 = r2.c()
                com.fossil.ee7.a(r2, r9)
                r1.a(r2)
                goto L_0x0580
            L_0x04ca:
                com.fossil.cb0 r4 = com.fossil.cb0.BUDDY_CHALLENGE_SYNC_DATA
                int r4 = r4.ordinal()
                if (r7 != r4) goto L_0x0538
                com.fossil.vg5 r4 = r0.this$0
                com.fossil.ch5 r4 = r4.j
                java.lang.Boolean r4 = r4.a()
                com.fossil.vg5 r7 = r0.this$0
                com.fossil.ch5 r7 = r7.j
                java.lang.String r7 = r7.A()
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.fossil.vg5$a r11 = com.fossil.vg5.l
                java.lang.String r11 = r11.a()
                java.lang.StringBuilder r12 = new java.lang.StringBuilder
                r12.<init>()
                java.lang.String r13 = "BUDDY_CHALLENGE_SYNC_DATA - isBCOn ="
                r12.append(r13)
                r12.append(r4)
                java.lang.String r12 = r12.toString()
                r8.e(r11, r12)
                com.fossil.ee7.a(r4, r5)
                boolean r5 = r4.booleanValue()
                if (r5 == 0) goto L_0x0580
                if (r7 == 0) goto L_0x0517
                int r5 = r7.length()
                if (r5 != 0) goto L_0x0518
            L_0x0517:
                r10 = 1
            L_0x0518:
                if (r10 != 0) goto L_0x0580
                com.fossil.vg5 r5 = r0.this$0
                com.fossil.uj5 r5 = r5.e
                com.fossil.jc5 r8 = r0.$event
                java.lang.String r8 = r8.c()
                com.fossil.ee7.a(r8, r9)
                r0.L$0 = r2
                r0.L$1 = r4
                r0.L$2 = r7
                r0.label = r3
                java.lang.Object r2 = r5.a(r8, r6, r0)
                if (r2 != r1) goto L_0x0580
                return r1
            L_0x0538:
                com.fossil.cb0 r1 = com.fossil.cb0.BUDDY_CHALLENGE_GET_INFO
                int r1 = r1.ordinal()
                if (r7 != r1) goto L_0x0562
                com.fossil.jc5 r1 = r0.$event
                android.os.Bundle r1 = r1.b()
                java.lang.String r1 = r1.getString(r8, r12)
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.uj5 r2 = r2.e
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                java.lang.String r4 = "challengeId"
                com.fossil.ee7.a(r1, r4)
                r2.a(r3, r1)
                goto L_0x0580
            L_0x0562:
                com.fossil.vg5$b r1 = new com.fossil.vg5$b
                com.fossil.vg5 r2 = r0.this$0
                com.fossil.jc5 r3 = r0.$event
                java.lang.String r3 = r3.c()
                com.fossil.ee7.a(r3, r9)
                com.fossil.jc5 r4 = r0.$event
                int r4 = r4.a()
                r1.<init>(r2, r3, r4)
                java.lang.Thread r2 = new java.lang.Thread
                r2.<init>(r1)
                r2.start()
            L_0x0580:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vg5.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {419}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vg5 vg5, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vg5;
            this.$action = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$action, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.$action == db0.SET.ordinal()) {
                    AlarmsRepository a2 = this.this$0.d;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = a2.getActiveAlarms(this);
                    if (obj == a) {
                        return a;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) obj;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.g0.c().a(rc5.a(list));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ kb0 $notificationEvent$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ q90 $subEvent;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vg5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BroadcastReceiver {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar, byte b, QuickResponseMessage quickResponseMessage) {
                this.a = eVar;
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus;
                boolean z = false;
                if (intent != null) {
                    z = intent.getBooleanExtra("SEND_SMS_PERMISSION_REQUIRED", false);
                }
                if (getResultCode() == -1) {
                    FLogger.INSTANCE.getLocal().d(vg5.l.a(), "SMS delivered");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.SUCCESS;
                } else if (z) {
                    FLogger.INSTANCE.getLocal().d(vg5.l.a(), "SMS permission required");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.NOT_FOUND;
                } else {
                    FLogger.INSTANCE.getLocal().d(vg5.l.a(), "SMS failed");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.FAILED;
                }
                PortfolioApp.g0.c().a(PortfolioApp.g0.c().c(), new DianaNotificationObj(((fb0) this.a.$notificationEvent$inlined).getNotificationUid(), NotificationBaseObj.ANotificationType.TEXT, DianaNotificationObj.AApplicationName.Companion.getMESSAGES(), "", "", 1, "", new ArrayList(), Long.valueOf(System.currentTimeMillis()), notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType.REPLY_MESSAGE));
                PortfolioApp.g0.c().unregisterReceiver(this);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(q90 q90, fb7 fb7, vg5 vg5, kb0 kb0) {
            super(2, fb7);
            this.$subEvent = q90;
            this.this$0 = vg5;
            this.$notificationEvent$inlined = kb0;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.$subEvent, fb7, this.this$0, this.$notificationEvent$inlined);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            T t;
            QuickResponseSender quickResponseSender;
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                byte messageId = ((v90) this.$subEvent).getMessageId();
                Iterator<T> it = this.this$0.g.getAllQuickResponse().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (pb7.a(t.getId() == messageId).booleanValue()) {
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (quickResponseSender = this.this$0.g.getQuickResponseSender(((v90) this.$subEvent).getSenderId())) == null)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = vg5.l.a();
                    local.d(a2, "process to send sms to " + quickResponseSender.getContent() + " with id " + ((int) messageId) + " senderId " + ((v90) this.$subEvent).getSenderId());
                    zg5.c.a().a(quickResponseSender.getContent(), t2.getResponse(), new a(this, messageId, t2));
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {431}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(int i, String str, fb7 fb7) {
            super(2, fb7);
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.$action, this.$deviceId, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.$action == db0.SET.ordinal()) {
                    PortfolioApp c = PortfolioApp.g0.c();
                    String str = this.$deviceId;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.c(str, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(vg5 vg5, int i, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vg5;
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$action, this.$deviceId, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                if (this.$action == db0.SET.ordinal()) {
                    this.this$0.i.a(new kw6.b(this.$deviceId), (fl4.e) null);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $ringtoneName;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str, fb7 fb7) {
            super(2, fb7);
            this.$ringtoneName = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.$ringtoneName, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                List<Ringtone> d = rd5.g.d();
                if (!d.isEmpty()) {
                    for (Ringtone ringtone : d) {
                        if (ee7.a((Object) ringtone.getRingtoneName(), (Object) this.$ringtoneName)) {
                            dh5.o.a().b(ringtone);
                            return i97.a;
                        }
                    }
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String canonicalName = vg5.class.getCanonicalName();
        if (canonicalName != null) {
            ee7.a((Object) canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            k = canonicalName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public vg5(HybridPresetRepository hybridPresetRepository, AlarmsRepository alarmsRepository, uj5 uj5, wk5 wk5, QuickResponseRepository quickResponseRepository, ek5 ek5, kw6 kw6, ch5 ch5) {
        ee7.b(hybridPresetRepository, "mPresetRepository");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        ee7.b(uj5, "mBuddyChallengeManager");
        ee7.b(wk5, "mWorkoutTetherGpsManager");
        ee7.b(quickResponseRepository, "mQuickResponseRepository");
        ee7.b(ek5, "mMusicControlComponent");
        ee7.b(kw6, "mSetNotificationUseCase");
        ee7.b(ch5, "sharedPreferencesManager");
        this.c = hybridPresetRepository;
        this.d = alarmsRepository;
        this.e = uj5;
        this.f = wk5;
        this.g = quickResponseRepository;
        this.h = ek5;
        this.i = kw6;
        this.j = ch5;
        PortfolioApp.g0.b(this);
    }

    @DexIgnore
    @k07
    public final void onDeviceAppEvent(jc5 jc5) {
        ee7.b(jc5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onDeviceAppEvent - appId=" + jc5.a() + ", serial=" + jc5.c());
        ik7 unused = xh7.b(this.b, null, null, new c(this, jc5, null), 3, null);
    }

    @DexIgnore
    @k07
    public final void onMicroAppCancelEvent(kc5 kc5) {
        ee7.b(kc5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onMicroAppCancelEvent - event=" + kc5.a() + ", serial=" + kc5.b());
        a(this.a);
    }

    @DexIgnore
    @k07
    public final void onMusicActionEvent(nc5 nc5) {
        ee7.b(nc5, Constants.EVENT);
        FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Inside ");
        sb.append(k);
        sb.append(".onMusicActionEvent - musicActionEvent=");
        nc5.a();
        throw null;
    }

    @DexIgnore
    @k07
    public final void onStreamingEvent(pc5 pc5) {
        ee7.b(pc5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onStreamingEvent - event=" + pc5.a() + ", serial=" + pc5.b());
        String b2 = pc5.b();
        ee7.a((Object) b2, "event.serial");
        new Thread(new b(this, b2, pc5.a())).start();
    }

    @DexIgnore
    @Override // com.fossil.dk5
    public void b(bk5 bk5) {
        ee7.b(bk5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "removeObject");
        if (!this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "removeObject mServiceListSize=" + this.a.size());
            Iterator<bk5> it = this.a.iterator();
            while (it.hasNext()) {
                bk5 next = it.next();
                ee7.a((Object) next, "autoStopBaseService");
                if (next.c() == bk5.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = k;
                    local2.d(str2, "removeObject actionId " + bk5.c());
                    this.a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    public final ik7 c(String str, int i2) {
        return xh7.b(zi7.a(qj7.a()), null, null, new g(this, i2, str, null), 3, null);
    }

    @DexIgnore
    public final void d(String str, int i2) {
        ag0 ag0;
        FLogger.INSTANCE.getLocal().d(k, "startRingMyPhone");
        if (i2 == ag0.ON.ordinal()) {
            dh5.o.a().a(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000L);
            ag0 = ag0.ON;
        } else {
            dh5.o.a().b();
            ag0 = ag0.OFF;
        }
        PortfolioApp.g0.c().a(new RingPhoneComplicationAppInfo(ag0), str);
    }

    @DexIgnore
    public final void a(kb0 kb0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("processCallMessageNotificationEvent event ");
        sb.append(kb0);
        sb.append(" subEvent ");
        fb0 fb0 = (fb0) kb0;
        sb.append(fb0 != null ? fb0.getAction() : null);
        local.d(str, sb.toString());
        if (kb0 != null) {
            q90 action = fb0.getAction();
            if (action instanceof u90) {
                if (be5.o.m()) {
                    zg5.c.a().b();
                }
            } else if (action instanceof p90) {
                if (be5.o.m()) {
                    zg5.c.a().a();
                }
            } else if (action instanceof v90) {
                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new e(action, null, this, kb0), 3, null);
            } else {
                if (action instanceof t90) {
                }
            }
        }
    }

    @DexIgnore
    public final ik7 b(String str, int i2) {
        return xh7.b(zi7.a(qj7.a()), null, null, new f(i2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.dk5
    public void a(int i2, jb5 jb5) {
        ee7.b(jb5, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onStatusChanged action " + i2);
    }

    @DexIgnore
    @Override // com.fossil.dk5
    public void a(bk5 bk5) {
        ee7.b(bk5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "addObject");
        if (!this.a.contains(bk5)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "addObject service actionId " + bk5.c());
            this.a.add(bk5);
        }
    }

    @DexIgnore
    public final void a(List<? extends bk5> list) {
        if (list != null && !this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "stopServices serviceListSize=" + this.a.size());
            for (bk5 bk5 : list) {
                bk5.b();
            }
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, "deviceId");
        ee7.b(str2, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = k;
        local.d(str3, "startRingMyPhone, setting=" + str2);
        try {
            if (TextUtils.isEmpty(str2)) {
                List<Ringtone> d2 = rd5.g.d();
                Ringtone ringtone = d2.get(0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = k;
                local2.d(str4, "ringtones=" + d2 + ", defaultRingtone=" + ringtone);
                dh5.o.a().b(ringtone);
                return;
            }
            String component1 = ((Ringtone) new Gson().a(str2, Ringtone.class)).component1();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = k;
            local3.d(str5, "MicroAppAction.run - ringtone " + str2);
            PortfolioApp.g0.c().a(new RingMyPhoneMicroAppResponse(), str);
            ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new h(component1, null), 3, null);
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AppUtil", "Error when read asset file - ex=" + e2);
        }
    }

    @DexIgnore
    public final ik7 a(String str, int i2) {
        return xh7.b(zi7.a(qj7.b()), null, null, new d(this, i2, null), 3, null);
    }

    @DexIgnore
    public final String a(String str) {
        we7 we7 = we7.a;
        String format = String.format("update_%s", Arrays.copyOf(new Object[]{str}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
