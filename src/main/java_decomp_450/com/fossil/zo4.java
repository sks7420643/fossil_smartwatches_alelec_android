package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo4 {
    @DexIgnore
    public /* final */ co4 a;

    @DexIgnore
    public zo4(co4 co4) {
        ee7.b(co4, "dao");
        this.a = co4;
    }

    @DexIgnore
    public final Long[] a(List<ao4> list) {
        ee7.b(list, "notifications");
        return this.a.a(list);
    }

    @DexIgnore
    public final LiveData<List<ao4>> b() {
        return this.a.b();
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }
}
