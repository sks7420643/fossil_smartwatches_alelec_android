package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc6 extends pc6 {
    @DexIgnore
    public /* final */ MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public List<DailyHeartRateSummary> h; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<qx6<List<DailyHeartRateSummary>>> i;
    @DexIgnore
    public /* final */ qc6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sc6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1$currentUser$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.k;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sc6 sc6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sc6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.g = zd5.d(mFUser.getCreatedAt());
                qc6 i2 = this.this$0.j;
                Date d = this.this$0.f;
                if (d != null) {
                    Date c = this.this$0.g;
                    if (c == null) {
                        c = new Date();
                    }
                    i2.a(d, c);
                    this.this$0.e.a(this.this$0.f);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ sc6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$mHeartRateSummaries$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {44, 44}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<DailyHeartRateSummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<DailyHeartRateSummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                Date date;
                Date date2;
                vd vdVar2;
                Date date3;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    Date c = this.this$0.a.g;
                    if (c == null) {
                        c = new Date();
                    }
                    long time = c.getTime();
                    Date date4 = this.$it;
                    ee7.a((Object) date4, "it");
                    if (!zd5.a(time, date4.getTime())) {
                        Calendar r = zd5.r(this.$it);
                        ee7.a((Object) r, "DateHelper.getStartOfMonth(it)");
                        c = r.getTime();
                        ee7.a((Object) c, "DateHelper.getStartOfMonth(it).time");
                    }
                    date = c;
                    Boolean v = zd5.v(this.$it);
                    ee7.a((Object) v, "DateHelper.isThisMonth(it)");
                    if (v.booleanValue()) {
                        date3 = new Date();
                    } else {
                        Calendar m = zd5.m(this.$it);
                        ee7.a((Object) m, "DateHelper.getEndOfMonth(it)");
                        date3 = m.getTime();
                    }
                    ee7.a((Object) date3, GoalPhase.COLUMN_END_DATE);
                    boolean z = date3.getTime() >= date.getTime();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("HeartRateOverviewMonthPresenter", "mActivitySummaries onDateChanged " + this.$it + " startDate " + date + " endDate " + date3 + " isValid " + z);
                    if (z) {
                        HeartRateSummaryRepository g = this.this$0.a.l;
                        this.L$0 = vdVar2;
                        this.L$1 = date;
                        this.L$2 = date3;
                        this.L$3 = vdVar2;
                        this.label = 1;
                        Object heartRateSummaries = g.getHeartRateSummaries(date, date3, true, this);
                        if (heartRateSummaries == a) {
                            return a;
                        }
                        vdVar = vdVar2;
                        date2 = date3;
                        obj = heartRateSummaries;
                    }
                    return i97.a;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$3;
                    date2 = (Date) this.L$2;
                    date = (Date) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    Date date5 = (Date) this.L$2;
                    Date date6 = (Date) this.L$1;
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.L$1 = date;
                this.L$2 = date2;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(sc6 sc6) {
            this.a = sc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<DailyHeartRateSummary>>> apply(Date date) {
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<qx6<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ sc6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {69}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public /* final */ /* synthetic */ qx6 $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sc6$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.sc6$d$a$a  reason: collision with other inner class name */
            public static final class C0166a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $calendar;
                @DexIgnore
                public /* final */ /* synthetic */ TreeMap $map;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0166a(a aVar, Calendar calendar, TreeMap treeMap, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$calendar = calendar;
                    this.$map = treeMap;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0166a aVar = new C0166a(this.this$0, this.$calendar, this.$map, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0166a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Integer a;
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        for (DailyHeartRateSummary dailyHeartRateSummary : (List) this.this$0.$it.c()) {
                            Calendar calendar = this.$calendar;
                            ee7.a((Object) calendar, "calendar");
                            calendar.setTime(dailyHeartRateSummary.getDate());
                            int i = 0;
                            this.$calendar.set(14, 0);
                            TreeMap treeMap = this.$map;
                            Calendar calendar2 = this.$calendar;
                            ee7.a((Object) calendar2, "calendar");
                            Long a2 = pb7.a(calendar2.getTimeInMillis());
                            Resting resting = dailyHeartRateSummary.getResting();
                            if (!(resting == null || (a = pb7.a(resting.getValue())) == null)) {
                                i = a.intValue();
                            }
                            treeMap.put(a2, pb7.a(i));
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, List list, qx6 qx6, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$data = list;
                this.$it = qx6;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$data, this.$it, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                TreeMap<Long, Integer> treeMap;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    this.this$0.a.h = this.$data;
                    TreeMap<Long, Integer> treeMap2 = new TreeMap<>();
                    Calendar instance = Calendar.getInstance();
                    ti7 a2 = this.this$0.a.b();
                    C0166a aVar = new C0166a(this, instance, treeMap2, null);
                    this.L$0 = yi7;
                    this.L$1 = treeMap2;
                    this.L$2 = instance;
                    this.label = 1;
                    if (vh7.a(a2, aVar, this) == a) {
                        return a;
                    }
                    treeMap = treeMap2;
                } else if (i == 1) {
                    Calendar calendar = (Calendar) this.L$2;
                    treeMap = (TreeMap) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.j.a(treeMap);
                return i97.a;
            }
        }

        @DexIgnore
        public d(sc6 sc6) {
            this.a = sc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<DailyHeartRateSummary>> qx6) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(qx6 != null ? qx6.d() : null);
            sb.append(" -- data.size=");
            sb.append((qx6 == null || (list = (List) qx6.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("HeartRateOverviewMonthPresenter", sb.toString());
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                List list2 = qx6 != null ? (List) qx6.c() : null;
                if (list2 != null && (!ee7.a(this.a.h, list2))) {
                    ik7 unused = xh7.b(this.a.e(), null, null, new a(this, list2, qx6, null), 3, null);
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public sc6(qc6 qc6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        ee7.b(qc6, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.j = qc6;
        this.k = userRepository;
        this.l = heartRateSummaryRepository;
        LiveData<qx6<List<DailyHeartRateSummary>>> b2 = ge.b(this.e, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.i = b2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        h();
        LiveData<qx6<List<DailyHeartRateSummary>>> liveData = this.i;
        qc6 qc6 = this.j;
        if (qc6 != null) {
            liveData.a((rc6) qc6, new d(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        MFLogger.d("HeartRateOverviewMonthPresenter", "stop");
        try {
            LiveData<qx6<List<DailyHeartRateSummary>>> liveData = this.i;
            qc6 qc6 = this.j;
            if (qc6 != null) {
                liveData.a((rc6) qc6);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
        } catch (Exception e2) {
            MFLogger.d("HeartRateOverviewMonthPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.f = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
            ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
    }

    @DexIgnore
    public void i() {
        this.j.a(this);
    }

    @DexIgnore
    @Override // com.fossil.pc6
    public void a(Date date) {
        ee7.b(date, "date");
        if (this.e.a() == null || !zd5.d(this.e.a(), date)) {
            this.e.a(date);
        }
    }
}
