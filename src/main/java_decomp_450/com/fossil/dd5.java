package com.fossil;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.fossil.ix;
import com.fossil.m00;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dd5 implements m00<ed5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements ix<InputStream> {
        @DexIgnore
        public volatile boolean a;
        @DexIgnore
        public /* final */ ed5 b;

        @DexIgnore
        public a(dd5 dd5, ed5 ed5) {
            ee7.b(ed5, "mAppIconModel");
            this.b = ed5;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super InputStream> aVar) {
            ee7.b(ewVar, "priority");
            ee7.b(aVar, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.g0.c().getPackageManager().getApplicationIcon(this.b.a().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap a2 = sw6.a(applicationIcon);
                if (a2 != null) {
                    a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                aVar.a(this.a ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e) {
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
            this.a = true;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n00<ed5, InputStream> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.dd5' to match base method */
        @Override // com.fossil.n00
        public m00<ed5, InputStream> a(q00 q00) {
            ee7.b(q00, "multiFactory");
            return new dd5();
        }
    }

    @DexIgnore
    public boolean a(ed5 ed5) {
        ee7.b(ed5, "appIconModel");
        return true;
    }

    @DexIgnore
    public m00.a<InputStream> a(ed5 ed5, int i, int i2, ax axVar) {
        ee7.b(ed5, "appIconModel");
        ee7.b(axVar, "options");
        return new m00.a<>(ed5, new a(this, ed5));
    }
}
