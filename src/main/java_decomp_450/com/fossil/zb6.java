package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zb6 extends go5 implements yb6, vp5, ro5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<m05> f;
    @DexIgnore
    public xb6 g;
    @DexIgnore
    public up5 h;
    @DexIgnore
    public HeartRateOverviewFragment i;
    @DexIgnore
    public pz6 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return zb6.q;
        }

        @DexIgnore
        public final zb6 b() {
            return new zb6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends pz6 {
        @DexIgnore
        public /* final */ /* synthetic */ zb6 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zb6 zb6, LinearLayoutManager linearLayoutManager, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager2);
            this.e = zb6;
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i) {
            zb6.a(this.e).j();
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = zb6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "DashboardHeartRateFragme\u2026::class.java.simpleName!!");
            q = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ xb6 a(zb6 zb6) {
        xb6 xb6 = zb6.g;
        if (xb6 != null) {
            return xb6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.vp5
    public void b(Date date, Date date2) {
        ee7.b(date, "startWeekDate");
        ee7.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void d() {
        pz6 pz6 = this.j;
        if (pz6 != null) {
            pz6.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return q;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        m05 m05 = (m05) qb.a(layoutInflater, 2131558545, viewGroup, false, a1());
        HeartRateOverviewFragment heartRateOverviewFragment = (HeartRateOverviewFragment) getChildFragmentManager().b("HeartRateOverviewFragment");
        this.i = heartRateOverviewFragment;
        if (heartRateOverviewFragment == null) {
            this.i = new HeartRateOverviewFragment();
        }
        tp5 tp5 = new tp5();
        PortfolioApp c = PortfolioApp.g0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        HeartRateOverviewFragment heartRateOverviewFragment2 = this.i;
        if (heartRateOverviewFragment2 != null) {
            this.h = new up5(tp5, c, this, childFragmentManager, heartRateOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            this.j = new b(this, linearLayoutManager, linearLayoutManager);
            s66 s66 = new s66(linearLayoutManager.Q());
            Drawable c2 = v6.c(requireContext(), 2131230855);
            if (c2 != null) {
                ee7.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                s66.a(c2);
                RecyclerView recyclerView = m05.q;
                ee7.a((Object) recyclerView, "this");
                recyclerView.setLayoutManager(linearLayoutManager);
                up5 up5 = this.h;
                if (up5 != null) {
                    recyclerView.setAdapter(up5);
                    pz6 pz6 = this.j;
                    if (pz6 != null) {
                        recyclerView.addOnScrollListener(pz6);
                        recyclerView.setItemViewCacheSize(0);
                        recyclerView.addItemDecoration(s66);
                        if (recyclerView.getItemAnimator() instanceof dh) {
                            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                            if (itemAnimator != null) {
                                ((dh) itemAnimator).setSupportsChangeAnimations(false);
                            } else {
                                throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator");
                            }
                        }
                        xb6 xb6 = this.g;
                        if (xb6 != null) {
                            xb6.h();
                            qw6<m05> qw6 = new qw6<>(this, m05);
                            this.f = qw6;
                            if (qw6 != null) {
                                m05 a2 = qw6.a();
                                if (a2 != null) {
                                    ee7.a((Object) a2, "mBinding.get()!!");
                                    return a2.d();
                                }
                                ee7.a();
                                throw null;
                            }
                            ee7.d("mBinding");
                            throw null;
                        }
                        ee7.d("mPresenter");
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mDashboardHeartRatesAdapter");
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(q, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        xb6 xb6 = this.g;
        if (xb6 != null) {
            xb6.i();
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        xb6 xb6 = this.g;
        if (xb6 != null) {
            xb6.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
            }
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        xb6 xb6 = this.g;
        if (xb6 != null) {
            xb6.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        V("heart_rate_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            he a2 = je.a(activity).a(xz6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            xz6 xz6 = (xz6) a2;
        }
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        qw6<m05> qw6;
        RecyclerView.ViewHolder findViewHolderForAdapterPosition;
        View view;
        if (isVisible() && (qw6 = this.f) != null) {
            RecyclerView recyclerView = null;
            if (qw6 != null) {
                m05 a2 = qw6.a();
                if (a2 != null) {
                    recyclerView = a2.q;
                }
                if (recyclerView == null || (findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0)) == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    if (recyclerView != null) {
                        recyclerView.smoothScrollToPosition(0);
                    }
                    pz6 pz6 = this.j;
                    if (pz6 != null) {
                        pz6.a();
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yb6
    public void b(qf<DailyHeartRateSummary> qfVar) {
        up5 up5 = this.h;
        if (up5 != null) {
            up5.c(qfVar);
        } else {
            ee7.d("mDashboardHeartRatesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(xb6 xb6) {
        ee7.b(xb6, "presenter");
        this.g = xb6;
    }

    @DexIgnore
    @Override // com.fossil.vp5
    public void a(Date date) {
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.A;
            ee7.a((Object) context, "it");
            aVar.a(date, context);
        }
    }
}
