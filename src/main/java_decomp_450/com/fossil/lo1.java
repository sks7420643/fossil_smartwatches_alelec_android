package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public lo1(ri1 ri1) {
        this.a = ri1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == 2084501365 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED")) {
            m01 a2 = m01.e.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", 10));
            m01 a3 = m01.e.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", 10));
            if (ee7.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE"), this.a.x)) {
                ri1 ri1 = this.a;
                ri1.a.post(new dx0(ri1, new x71(z51.SUCCESS, 0, 2), a2, a3));
                ri1 ri12 = this.a;
                ri12.a.post(new kb1(ri12, new x71(z51.SUCCESS, 0, 2), a2, a3));
            }
        }
    }
}
