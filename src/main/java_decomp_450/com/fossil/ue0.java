package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ue0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ue0 createFromParcel(Parcel parcel) {
            return new ue0(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ue0[] newArray(int i) {
            return new ue0[i];
        }
    }

    @DexIgnore
    public ue0(int i, int i2) throws IllegalArgumentException {
        this.a = i;
        this.b = i2;
        boolean z = true;
        if (i >= 0 && 359 >= i) {
            int i3 = this.b;
            if (!((i3 < 0 || 120 < i3) ? false : z)) {
                throw new IllegalArgumentException(yh0.a(yh0.b("distanceFromCenter("), this.b, ") is out of ", "range [0, 120]."));
            }
            return;
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("angle("), this.a, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("angle", this.a).put("distance", this.b);
        ee7.a((Object) put, "JSONObject()\n           \u2026ANCE, distanceFromCenter)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ue0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ue0 ue0 = (ue0) obj;
            return this.a == ue0.a && this.b == ue0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.a;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
