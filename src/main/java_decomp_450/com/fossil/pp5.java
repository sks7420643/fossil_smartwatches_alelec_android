package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp5 extends rf<SleepSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ rp5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ go5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public int f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4) {
            ee7.b(str, "mDayOfWeek");
            ee7.b(str2, "mDayOfMonth");
            ee7.b(str3, "mDailyUnit");
            ee7.b(str4, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = i;
            this.g = str3;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void c(String str) {
            ee7.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ b(java.util.Date r10, boolean r11, boolean r12, java.lang.String r13, java.lang.String r14, int r15, java.lang.String r16, java.lang.String r17, int r18, com.fossil.zd7 r19) {
            /*
                r9 = this;
                r0 = r18
                r1 = r0 & 1
                if (r1 == 0) goto L_0x0008
                r1 = 0
                goto L_0x0009
            L_0x0008:
                r1 = r10
            L_0x0009:
                r2 = r0 & 2
                r3 = 0
                if (r2 == 0) goto L_0x0010
                r2 = 0
                goto L_0x0011
            L_0x0010:
                r2 = r11
            L_0x0011:
                r4 = r0 & 4
                if (r4 == 0) goto L_0x0017
                r4 = 0
                goto L_0x0018
            L_0x0017:
                r4 = r12
            L_0x0018:
                r5 = r0 & 8
                java.lang.String r6 = ""
                if (r5 == 0) goto L_0x0020
                r5 = r6
                goto L_0x0021
            L_0x0020:
                r5 = r13
            L_0x0021:
                r7 = r0 & 16
                if (r7 == 0) goto L_0x0027
                r7 = r6
                goto L_0x0028
            L_0x0027:
                r7 = r14
            L_0x0028:
                r8 = r0 & 32
                if (r8 == 0) goto L_0x002d
                goto L_0x002e
            L_0x002d:
                r3 = r15
            L_0x002e:
                r8 = r0 & 64
                if (r8 == 0) goto L_0x0034
                r8 = r6
                goto L_0x0036
            L_0x0034:
                r8 = r16
            L_0x0036:
                r0 = r0 & 128(0x80, float:1.794E-43)
                if (r0 == 0) goto L_0x003b
                goto L_0x003d
            L_0x003b:
                r6 = r17
            L_0x003d:
                r10 = r9
                r11 = r1
                r12 = r2
                r13 = r4
                r14 = r5
                r15 = r7
                r16 = r3
                r17 = r8
                r18 = r6
                r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pp5.b.<init>(java.util.Date, boolean, boolean, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, int, com.fossil.zd7):void");
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final int c() {
            return this.f;
        }

        @DexIgnore
        public final void d(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.f = i;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ ka5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ pp5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.d.l.a(a2);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pp5 pp5, ka5 ka5, View view) {
            super(view);
            ee7.b(ka5, "binding");
            ee7.b(view, "root");
            this.d = pp5;
            this.b = ka5;
            this.c = view;
            ka5.d().setOnClickListener(new a(this));
            this.b.s.setTextColor(pp5.f);
            this.b.v.setTextColor(pp5.f);
        }

        @DexIgnore
        public void a(SleepSummary sleepSummary) {
            b a2 = this.d.a(sleepSummary);
            this.a = a2.d();
            FlexibleTextView flexibleTextView = this.b.u;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.e());
            if (a2.c() <= 0) {
                ConstraintLayout constraintLayout = this.b.q;
                ee7.a((Object) constraintLayout, "binding.clDailyValue");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.s;
                ee7.a((Object) flexibleTextView3, "binding.ftvDailyUnit");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                ee7.a((Object) constraintLayout2, "binding.clDailyValue");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.A;
                ee7.a((Object) flexibleTextView4, "binding.tvMin");
                flexibleTextView4.setText(String.valueOf(ze5.b(a2.c())));
                FlexibleTextView flexibleTextView5 = this.b.y;
                ee7.a((Object) flexibleTextView5, "binding.tvHour");
                flexibleTextView5.setText(String.valueOf(ze5.a(a2.c())));
                FlexibleTextView flexibleTextView6 = this.b.B;
                ee7.a((Object) flexibleTextView6, "binding.tvMinUnit");
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886836);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView6.setText(lowerCase);
                    FlexibleTextView flexibleTextView7 = this.b.s;
                    ee7.a((Object) flexibleTextView7, "binding.ftvDailyUnit");
                    flexibleTextView7.setVisibility(8);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            FlexibleTextView flexibleTextView8 = this.b.s;
            ee7.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
            flexibleTextView8.setText(a2.b());
            FlexibleTextView flexibleTextView9 = this.b.v;
            ee7.a((Object) flexibleTextView9, "binding.ftvEst");
            flexibleTextView9.setText(a2.a());
            if (a2.g()) {
                this.b.s.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099942));
                FlexibleTextView flexibleTextView10 = this.b.s;
                ee7.a((Object) flexibleTextView10, "binding.ftvDailyUnit");
                flexibleTextView10.setAllCaps(true);
            } else {
                this.b.s.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099944));
                FlexibleTextView flexibleTextView11 = this.b.s;
                ee7.a((Object) flexibleTextView11, "binding.ftvDailyUnit");
                flexibleTextView11.setAllCaps(false);
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            ee7.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(true ^ a2.g());
            FlexibleTextView flexibleTextView12 = this.b.u;
            ee7.a((Object) flexibleTextView12, "binding.ftvDayOfWeek");
            flexibleTextView12.setSelected(a2.h());
            FlexibleTextView flexibleTextView13 = this.b.t;
            ee7.a((Object) flexibleTextView13, "binding.ftvDayOfMonth");
            flexibleTextView13.setSelected(a2.h());
            if (a2.h()) {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (a2.g()) {
                this.b.r.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            ee7.b(str, "mWeekly");
            ee7.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ ma5 g;
        @DexIgnore
        public /* final */ /* synthetic */ pp5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.e != null && this.a.f != null) {
                    rp5 e = this.a.h.l;
                    Date b = this.a.e;
                    if (b != null) {
                        Date a2 = this.a.f;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public e(com.fossil.pp5 r4, com.fossil.ma5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.ee7.b(r5, r0)
                r3.h = r4
                com.fossil.ka5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.ee7.a(r0, r1)
                android.view.View r1 = r5.d()
                java.lang.String r2 = "binding.root"
                com.fossil.ee7.a(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r4 = r5.q
                com.fossil.pp5$e$a r5 = new com.fossil.pp5$e$a
                r5.<init>(r3)
                r4.setOnClickListener(r5)
                return
            L_0x0029:
                com.fossil.ee7.a()
                r4 = 0
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pp5.e.<init>(com.fossil.pp5, com.fossil.ma5):void");
        }

        @DexIgnore
        @Override // com.fossil.pp5.c
        public void a(SleepSummary sleepSummary) {
            d b = this.h.b(sleepSummary);
            this.f = b.a();
            this.e = b.b();
            FlexibleTextView flexibleTextView = this.g.s;
            ee7.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(sleepSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ pp5 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(pp5 pp5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = pp5;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            ee7.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.n.getId() + ", isAdded=" + this.a.n.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.m.b(this.a.n.d1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                nc b3 = this.a.m.b();
                b3.a(view.getId(), this.a.n, this.a.n.d1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                nc b4 = this.a.m.b();
                b4.d(b2);
                b4.d();
                nc b5 = this.a.m.b();
                b5.a(view.getId(), this.a.n, this.a.n.d1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.n.getId() + ", isAdded2=" + this.a.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ee7.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pp5(sp5 sp5, PortfolioApp portfolioApp, rp5 rp5, FragmentManager fragmentManager, go5 go5) {
        super(sp5);
        ee7.b(sp5, "sleepDifference");
        ee7.b(portfolioApp, "mApp");
        ee7.b(rp5, "mOnItemClick");
        ee7.b(fragmentManager, "mFragmentManager");
        ee7.b(go5, "mFragment");
        this.k = portfolioApp;
        this.l = rp5;
        this.m = fragmentManager;
        this.n = go5;
        String b2 = eh5.l.a().b("primaryText");
        String str = "#FFFFFF";
        this.d = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("nonBrandSurface");
        this.e = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("nonBrandNonReachGoal");
        this.f = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("dianaSleepTab");
        this.g = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("secondaryText");
        this.h = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(b7 == null ? str : b7);
        String b8 = eh5.l.a().b("onDianaSleepTab");
        this.j = Color.parseColor(b8 != null ? b8 : str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        SleepSummary sleepSummary = (SleepSummary) getItem(i2);
        if (sleepSummary == null) {
            return 1;
        }
        Calendar calendar = this.c;
        ee7.a((Object) calendar, "mCalendar");
        calendar.setTime(sleepSummary.getDate());
        Calendar calendar2 = this.c;
        ee7.a((Object) calendar2, "mCalendar");
        Boolean w = zd5.w(calendar2.getTime());
        ee7.a((Object) w, "DateHelper.isToday(mCalendar.time)");
        if (w.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        ee7.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepsAdapter", "onBindViewHolder - position=" + i2 + ", viewType=" + getItemViewType(i2));
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            ee7.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            ee7.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardSleepsAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            ee7.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((SleepSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((SleepSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((SleepSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        ee7.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            ka5 a2 = ka5.a(from, viewGroup, false);
            ee7.a((Object) a2, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d2 = a2.d();
            ee7.a((Object) d2, "itemSleepDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            ka5 a3 = ka5.a(from, viewGroup, false);
            ee7.a((Object) a3, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d3 = a3.d();
            ee7.a((Object) d3, "itemSleepDayBinding.root");
            return new c(this, a3, d3);
        } else {
            ma5 a4 = ma5.a(from, viewGroup, false);
            ee7.a((Object) a4, "ItemSleepWeekBinding.inf\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(qf<SleepSummary> qfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList pagedList=");
        sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
        local.d("DashboardSleepsAdapter", sb.toString());
        super.b(qfVar);
    }

    @DexIgnore
    public final b a(SleepSummary sleepSummary) {
        MFSleepDay sleepDay;
        b bVar = new b(null, false, false, null, null, 0, null, null, 255, null);
        if (!(sleepSummary == null || (sleepDay = sleepSummary.getSleepDay()) == null)) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(sleepDay.getDate());
            int i2 = instance.get(7);
            Boolean w = zd5.w(instance.getTime());
            ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
            if (w.booleanValue()) {
                String a2 = ig5.a(this.k, 2131886613);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026n_SleepToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(xe5.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            int sleepMinutes = sleepDay.getSleepMinutes();
            boolean z = true;
            if (sleepMinutes > 0) {
                bVar.a(sleepMinutes);
                bVar.b("");
                List<MFSleepSession> sleepSessions = sleepSummary.getSleepSessions();
                if (sleepSessions != null && (!sleepSessions.isEmpty())) {
                    MFSleepSession mFSleepSession = sleepSessions.get(0);
                    int startTime = mFSleepSession.getStartTime();
                    int endTime = mFSleepSession.getEndTime();
                    xe5 xe5 = xe5.b;
                    String a3 = zd5.a(((long) startTime) * 1000, mFSleepSession.getTimezoneOffset());
                    ee7.a((Object) a3, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String b2 = xe5.b(a3);
                    xe5 xe52 = xe5.b;
                    String a4 = zd5.a(((long) endTime) * 1000, mFSleepSession.getTimezoneOffset());
                    ee7.a((Object) a4, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String b3 = xe52.b(a4);
                    we7 we7 = we7.a;
                    String a5 = ig5.a(this.k, 2131887532);
                    ee7.a((Object) a5, "LanguageHelper.getString\u2026ing.sleep_start_end_time)");
                    String format = String.format(a5, Arrays.copyOf(new Object[]{b2, b3}, 2));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    int size = sleepSessions.size();
                    if (size > 1) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(format);
                        sb.append("\n");
                        we7 we72 = we7.a;
                        String a6 = ig5.a(this.k, 2131886603);
                        ee7.a((Object) a6, "LanguageHelper.getString\u2026ltiple_Label__NumberMore)");
                        String format2 = String.format(a6, Arrays.copyOf(new Object[]{Integer.valueOf(size - 1)}, 1));
                        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                        sb.append(format2);
                        bVar.a(sb.toString());
                    } else {
                        bVar.a(format);
                    }
                }
                if (sleepDay.getGoalMinutes() > 0) {
                    if (sleepMinutes < sleepDay.getGoalMinutes()) {
                        z = false;
                    }
                    bVar.b(z);
                } else {
                    bVar.b(false);
                }
            } else {
                String a7 = ig5.a(this.k, 2131886611);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026leepToday_Text__NoRecord)");
                bVar.b(a7);
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(SleepSummary sleepSummary) {
        String str;
        Double averageSleepOfWeek;
        d dVar = new d(null, null, null, null, 15, null);
        if (sleepSummary != null) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(sleepSummary.getDate());
            Boolean w = zd5.w(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String c2 = zd5.c(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String c3 = zd5.c(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            ee7.a((Object) w, "isToday");
            if (w.booleanValue()) {
                str = ig5.a(this.k, 2131886615);
                ee7.a((Object) str, "LanguageHelper.getString\u2026eepToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = c3 + ' ' + i5 + " - " + c3 + ' ' + i2;
            } else if (i7 == i4) {
                str = c3 + ' ' + i5 + " - " + c2 + ' ' + i2;
            } else {
                str = c3 + ' ' + i5 + ", " + i7 + " - " + c2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            double doubleValue = (sleepDay == null || (averageSleepOfWeek = sleepDay.getAverageSleepOfWeek()) == null) ? 0.0d : averageSleepOfWeek.doubleValue();
            if (doubleValue <= 0.0d) {
                dVar.b("");
            } else {
                double d2 = (double) 60;
                we7 we7 = we7.a;
                String a2 = ig5.a(this.k, 2131886612);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026xt__NumberHrNumberMinAvg)");
                String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf((int) (doubleValue / d2)), Integer.valueOf((int) (doubleValue % d2))}, 2));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                dVar.b(format);
            }
        }
        return dVar;
    }
}
