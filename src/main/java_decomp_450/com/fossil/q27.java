package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.p27;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q27 implements p27.a {
    @DexIgnore
    public q27() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    @Override // com.fossil.p27.a
    public final int a() {
        return p27.a;
    }

    @DexIgnore
    @Override // com.fossil.p27.a
    public final void a(String str, String str2) {
        if (p27.a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.p27.a
    public final void b(String str, String str2) {
        if (p27.a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.p27.a
    public final void c(String str, String str2) {
        if (p27.a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.p27.a
    public final void i(String str, String str2) {
        if (p27.a <= 4) {
            Log.e(str, str2);
        }
    }
}
