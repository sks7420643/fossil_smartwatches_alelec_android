package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p61 extends lj0 {
    @DexIgnore
    public static /* final */ s41 V; // = new s41(null);
    @DexIgnore
    public /* final */ long U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ p61(ri1 ri1, en0 en0, long j, String str, int i) {
        super(ri1, en0, wm0.E0, V.a(j), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, 48);
        this.U = j;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.x5, Long.valueOf(this.U));
    }
}
