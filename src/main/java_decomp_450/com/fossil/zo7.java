package com.fossil;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo7 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;

    @DexIgnore
    public zo7(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        mp7 mp7 = (mp7) chain;
        lo7 c = mp7.c();
        fp7 i = mp7.i();
        return mp7.a(c, i, i.a(this.a, chain, !c.e().equals("GET")), i.c());
    }
}
