package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f81 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk1 a;
    @DexIgnore
    public /* final */ /* synthetic */ bi1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f81(zk1 zk1, bi1 bi1) {
        super(1);
        this.a = zk1;
        this.b = bi1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        byte[] bArr;
        v81 v812 = v81;
        rl0 rl0 = (rl0) v812;
        a81 a81 = new a81(this.b.c());
        bi1 a2 = this.a.a(a81.a, a81.b);
        zk1 zk1 = this.a;
        ri1 ri1 = ((zk0) zk1).w;
        le0 le0 = le0.DEBUG;
        String str = ((zk0) zk1).a;
        String str2 = ri1.u;
        if (a2 != null) {
            a2.toString();
        }
        if (a2 == null || (bArr = a2.e) == null) {
            bArr = new byte[0];
        }
        bi1 bi1 = new bi1(((zk0) this.a).w.u, a81.a, a81.b, yz0.a(bArr, rl0.X), rl0.s(), rl0.r(), this.a.g(), false);
        if (v812.v.c == ay0.a) {
            this.a.d(bi1);
        } else {
            this.a.a(bi1);
        }
        return i97.a;
    }
}
