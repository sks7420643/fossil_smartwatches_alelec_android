package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.vx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi6 extends go5 implements lj6, View.OnClickListener, cy6.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<q05> f;
    @DexIgnore
    public kj6 g;
    @DexIgnore
    public Integer h;
    @DexIgnore
    public /* final */ String i; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final xi6 a() {
            return new xi6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xi6 a;

        @DexIgnore
        public b(xi6 xi6) {
            this.a = xi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xi6 a;

        @DexIgnore
        public c(xi6 xi6) {
            this.a = xi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.j(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xi6 a;

        @DexIgnore
        public d(xi6 xi6) {
            this.a = xi6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (tm4.a.a().l()) {
                String a2 = vx6.a(vx6.c.REPAIR_CENTER, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String g1 = xi6.p;
                local.d(g1, "Support mail = " + a2);
                xi6 xi6 = this.a;
                ee7.a((Object) a2, "url");
                xi6.Y(a2);
                return;
            }
            this.a.f1().j();
            this.a.f1().a("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = xi6.class.getSimpleName();
        ee7.a((Object) simpleName, "DeleteAccountFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.lj6
    public void S0() {
        b1().b();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(p, "deleteUser - successfully");
                WelcomeActivity.z.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public final void Y(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), p);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final kj6 f1() {
        kj6 kj6 = this.g;
        if (kj6 != null) {
            return kj6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.lj6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.lj6
    public void j() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            kj6 kj6 = this.g;
            if (kj6 != null) {
                kj6.i();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ee7.b(view, "v");
        if (view.getId() == 2131361851) {
            v();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        q05 q05 = (q05) qb.a(LayoutInflater.from(getContext()), 2131558547, null, false, a1());
        this.f = new qw6<>(this, q05);
        ee7.a((Object) q05, "binding");
        return q05.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        kj6 kj6 = this.g;
        if (kj6 != null) {
            kj6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        kj6 kj6 = this.g;
        if (kj6 != null) {
            kj6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        if (!TextUtils.isEmpty(this.i)) {
            this.h = Integer.valueOf(Color.parseColor(this.i));
        }
        qw6<q05> qw6 = this.f;
        if (qw6 != null) {
            q05 a2 = qw6.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                String a3 = mh7.a(ig5.a(PortfolioApp.g0.c(), 2131887040).toString(), "contact_our_support_team", "", false, 4, (Object) null);
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "binding.tvDescription");
                flexibleTextView.setText(Html.fromHtml(a3));
                Integer num = this.h;
                if (num != null) {
                    a2.u.setLinkTextColor(num.intValue());
                }
                a2.u.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.lj6
    public void a(int i2, String str) {
        ee7.b(str, "message");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(kj6 kj6) {
        ee7.b(kj6, "presenter");
        jw3.a(kj6);
        ee7.a((Object) kj6, "Preconditions.checkNotNull(presenter)");
        this.g = kj6;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((cl5) activity).a(str, i2, intent);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i2 == 2131363307) {
                kj6 kj6 = this.g;
                if (kj6 != null) {
                    kj6.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.lj6
    public void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        ee7.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }
}
