package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx extends FilterInputStream {
    @DexIgnore
    public static /* final */ byte[] c;
    @DexIgnore
    public static /* final */ int d;
    @DexIgnore
    public static /* final */ int e;
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public int b;

    /*
    static {
        byte[] bArr = {-1, -31, 0, 28, 69, Framer.EXIT_FRAME_PREFIX, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, DateTimeFieldType.MINUTE_OF_DAY, 0, 2, 0, 0, 0, 1, 0};
        c = bArr;
        int length = bArr.length;
        d = length;
        e = length + 2;
    }
    */

    @DexIgnore
    public lx(InputStream inputStream, int i) {
        super(inputStream);
        if (i < -1 || i > 8) {
            throw new IllegalArgumentException("Cannot add invalid orientation: " + i);
        }
        this.a = (byte) i;
    }

    @DexIgnore
    public void mark(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean markSupported() {
        return false;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        int i;
        int i2;
        int i3 = this.b;
        if (i3 < 2 || i3 > (i2 = e)) {
            i = super.read();
        } else if (i3 == i2) {
            i = this.a;
        } else {
            i = c[i3 - 2] & 255;
        }
        if (i != -1) {
            this.b++;
        }
        return i;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        long skip = super.skip(j);
        if (skip > 0) {
            this.b = (int) (((long) this.b) + skip);
        }
        return skip;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4 = this.b;
        int i5 = e;
        if (i4 > i5) {
            i3 = super.read(bArr, i, i2);
        } else if (i4 == i5) {
            bArr[i] = this.a;
            i3 = 1;
        } else if (i4 < 2) {
            i3 = super.read(bArr, i, 2 - i4);
        } else {
            int min = Math.min(i5 - i4, i2);
            System.arraycopy(c, this.b - 2, bArr, i, min);
            i3 = min;
        }
        if (i3 > 0) {
            this.b += i3;
        }
        return i3;
    }
}
