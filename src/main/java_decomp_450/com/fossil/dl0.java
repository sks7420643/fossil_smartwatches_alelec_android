package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.LinkedHashSet;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dl0 extends zk0 {
    @DexIgnore
    public static /* final */ gl1 D; // = new gl1(null);
    @DexIgnore
    public /* final */ wg0 C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ dl0(ri1 ri1, en0 en0, wg0 wg0, String str, int i) {
        super(ri1, en0, wm0.Z, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = wg0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        boolean z = false;
        if (!(this.C.getPresetItems().length == 0)) {
            g70[] m = m();
            if (m.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    gl1 gl1 = D;
                    r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.ASSET.a));
                    if (r60 == null) {
                        r60 = b21.x.d();
                    }
                    zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.E, true, 1792, gl1.a(m, 1792, r60), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new fn1(this), new ep1(this), new dr1(this), (gd7) null, (gd7) null, 48, (Object) null);
                } catch (f41 e) {
                    wl0.h.a(e);
                    a(eu0.a(((zk0) this).v, null, is0.UNSUPPORTED_FORMAT, null, 5));
                }
            } else {
                n();
            }
        } else {
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.e3, this.C.a());
    }

    @DexIgnore
    public final g70[] m() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        xg0[] presetItems = this.C.getPresetItems();
        for (xg0 xg0 : presetItems) {
            if (xg0 instanceof h70) {
                linkedHashSet.addAll(((h70) xg0).d());
            }
        }
        Object[] array = ea7.c((Iterable) linkedHashSet).toArray(new g70[0]);
        if (array != null) {
            return (g70[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void n() {
        JSONObject jSONObject;
        ri1 ri1 = ((zk0) this).w;
        en0 en0 = ((zk0) this).x;
        wm0 wm0 = wm0.a0;
        try {
            r60 r60 = en0.a().i().get(Short.valueOf(pb1.ASSET.a));
            if (r60 == null) {
                r60 = b21.x.d();
            }
            ee7.a((Object) r60, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
            JSONObject jSONObject2 = new JSONObject();
            xg0[] presetItems = this.C.getPresetItems();
            for (xg0 xg0 : presetItems) {
                xg0.a(r60);
                jSONObject2 = yz0.a(jSONObject2, xg0.b());
            }
            jSONObject = new JSONObject().put("push", new JSONObject().put("set", jSONObject2));
            ee7.a((Object) jSONObject, "JSONObject().put(UIScrip\u2026T, assignmentJsonObject))");
        } catch (JSONException e) {
            wl0.h.a(e);
            jSONObject = new JSONObject();
        }
        zk0.a(this, new lj0(ri1, en0, wm0, jSONObject, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 48), new ih0(this), new fj0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }
}
