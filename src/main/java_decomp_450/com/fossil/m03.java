package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m03 implements n03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.sdk.dynamite.allow_remote_dynamite2", false);
        dr2.a("measurement.collection.init_params_control_enabled", true);
        dr2.a("measurement.sdk.dynamite.use_dynamite3", true);
        dr2.a("measurement.id.sdk.dynamite.use_dynamite", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.n03
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
