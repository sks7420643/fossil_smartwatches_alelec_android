package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface j06 extends dl4<i06> {
    @DexIgnore
    void a(String str, String str2, String str3, String str4);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, Parcelable parcelable);

    @DexIgnore
    void b(Complication complication);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void d(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void h(List<Complication> list);

    @DexIgnore
    void z(String str);
}
