package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ep3 b;

    @DexIgnore
    public dp3(ep3 ep3, no3 no3) {
        this.b = ep3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onSuccess(this.a.b());
            }
        }
    }
}
