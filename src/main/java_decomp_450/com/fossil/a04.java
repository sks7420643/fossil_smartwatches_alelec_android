package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a04<E> extends zx3<E> {
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public a04(E e) {
        jw3.a(e);
        this.element = e;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        jw3.a(i, 1);
        return this.element;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, com.fossil.vx3, com.fossil.vx3, java.util.Collection, com.fossil.zx3, com.fossil.zx3, java.lang.Iterable
    public j04<E> iterator() {
        return qy3.a((Object) this.element);
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.zx3, com.fossil.zx3
    public zx3<E> subList(int i, int i2) {
        jw3.b(i, i2, 1);
        return i == i2 ? zx3.of() : this;
    }
}
