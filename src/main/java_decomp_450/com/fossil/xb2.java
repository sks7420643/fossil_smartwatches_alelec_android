package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb2 extends eg2 implements yb2 {
    @DexIgnore
    public xb2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final ab2 a(ab2 ab2, String str, int i, ab2 ab22) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        zza.writeInt(i);
        fg2.a(zza, ab22);
        Parcel a = a(2, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.yb2
    public final ab2 b(ab2 ab2, String str, int i, ab2 ab22) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        zza.writeInt(i);
        fg2.a(zza, ab22);
        Parcel a = a(3, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
