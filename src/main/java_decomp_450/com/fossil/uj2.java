package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uj2 extends kh2 implements sj2 {
    @DexIgnore
    public uj2() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    @Override // com.fossil.kh2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((pj2) ej2.a(parcel, pj2.CREATOR), ki2.a(parcel.readStrongBinder()));
        } else if (i == 2) {
            a((sd2) ej2.a(parcel, sd2.CREATOR), bj2.a(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            a((qj2) ej2.a(parcel, qj2.CREATOR), bj2.a(parcel.readStrongBinder()));
        }
        return true;
    }
}
