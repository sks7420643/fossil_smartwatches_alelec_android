package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc7 extends ByteArrayOutputStream {
    @DexIgnore
    public jc7(int i) {
        super(i);
    }

    @DexIgnore
    public final byte[] getBuffer() {
        byte[] bArr = ((ByteArrayOutputStream) this).buf;
        ee7.a((Object) bArr, "buf");
        return bArr;
    }
}
