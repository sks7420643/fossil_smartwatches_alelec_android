package com.fossil;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c83 extends IInterface {
    @DexIgnore
    void c(ab2 ab2) throws RemoteException;

    @DexIgnore
    void onSnapshotReady(Bitmap bitmap) throws RemoteException;
}
