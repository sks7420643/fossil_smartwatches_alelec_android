package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n40 {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean b(n40 n40);

    @DexIgnore
    void c();

    @DexIgnore
    void clear();

    @DexIgnore
    void d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean isRunning();
}
