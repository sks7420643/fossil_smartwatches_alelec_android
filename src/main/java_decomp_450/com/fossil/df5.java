package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class df5 {
    @DexIgnore
    public static /* final */ String g; // = "df5";
    @DexIgnore
    public static df5 h;
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public CalibrationEnums.HandId e;
    @DexIgnore
    public ch5 f;

    @DexIgnore
    public df5() {
        PortfolioApp.c0.f().a(this);
    }

    @DexIgnore
    public static synchronized df5 b() {
        df5 df5;
        synchronized (df5.class) {
            if (h == null) {
                h = new df5();
            }
            df5 = h;
        }
        return df5;
    }

    @DexIgnore
    public boolean a(String str) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Cancel Calibration - serial=" + str);
            this.c.set(false);
            PortfolioApp.Y().deviceCancelCalibration(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public void c(String str) {
        try {
            PortfolioApp.Y().resetHandsToZeroDegree(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Error reset hands second timezone of serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    @k07
    public void onMovingHandCompleted(ic5 ic5) {
        synchronized (this.c) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.e(str, "onMovingHandCompleted isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            if (this.c.get()) {
                if (ic5.b()) {
                    int currentTimeMillis = ((int) (((System.currentTimeMillis() - this.b) / 10) * 2)) % 360;
                    int i = currentTimeMillis >= this.a ? currentTimeMillis - this.a : (currentTimeMillis + 360) - this.a;
                    this.a = currentTimeMillis;
                    a(this.d.get(), ic5.a(), i, this.e);
                }
            }
        }
    }

    @DexIgnore
    public boolean a(String str, String str2) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.d(str3, "Complete Calibration - serial=" + str);
            PortfolioApp.Y().deviceCompleteCalibration(str);
            this.c.set(false);
            this.f.a(str, str2, true);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        try {
            this.a = 0;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Enter Calibration - serial=" + str);
            PortfolioApp.Y().deviceStartCalibration(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public void b(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        synchronized (this.c) {
            if (!this.c.get()) {
                FLogger.INSTANCE.getLocal().d(g, "Start smart movement");
                try {
                    PortfolioApp.g0.b(b());
                } catch (Exception unused) {
                    FLogger.INSTANCE.getLocal().e(g, "Exception when register bus events");
                }
                this.c.set(true);
                this.b = System.currentTimeMillis();
                this.a = 0;
                this.d.set(z);
                this.e = handId;
                a(z, str, i, handId);
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, int i, CalibrationEnums.HandId handId, CalibrationEnums.MovingType movingType, CalibrationEnums.Direction direction, CalibrationEnums.Speed speed) {
        try {
            PortfolioApp.Y().deviceMovingHand(str, new HandCalibrationObj(handId, movingType, direction, speed, i));
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        return a(str, i, handId, CalibrationEnums.MovingType.DISTANCE, z ? CalibrationEnums.Direction.CLOCKWISE : CalibrationEnums.Direction.COUNTER_CLOCKWISE, CalibrationEnums.Speed.FULL);
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            this.c.set(false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "Stop smart movement isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            try {
                PortfolioApp.g0.c(b());
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(g, "Exception when unregister receiver");
                e2.printStackTrace();
            }
        }
    }
}
