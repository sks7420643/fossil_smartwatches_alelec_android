package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep implements dp {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<cp> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<cp> {
        @DexIgnore
        public a(ep epVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, cp cpVar) {
            String str = cpVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            String str2 = cpVar.b;
            if (str2 == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, str2);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public ep(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.dp
    public void a(cp cpVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(cpVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.dp
    public List<String> a(String str) {
        fi b2 = fi.b("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
