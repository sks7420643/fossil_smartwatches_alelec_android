package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.be5;
import com.fossil.cy6;
import com.fossil.el4;
import com.fossil.pu6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu6 extends ho5 implements cy6.g {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public qw6<c75> g;
    @DexIgnore
    public pu6 h;
    @DexIgnore
    public String i; // = eh5.l.a().b("primaryText");
    @DexIgnore
    public iw j;
    @DexIgnore
    public rj4 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return nu6.r;
        }

        @DexIgnore
        public final nu6 b() {
            return new nu6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<el4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public b(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = nu6.s.a();
                local.d(a2, "loadingState start " + aVar.a() + " stop " + aVar.b());
                if (aVar.a()) {
                    this.a.g();
                }
                if (aVar.b()) {
                    this.a.f();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<el4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public c(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.b bVar) {
            if (!bVar.a().isEmpty()) {
                nu6 nu6 = this.a;
                Object[] array = bVar.a().toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    nu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<pu6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public d(nu6 nu6, String str) {
            this.a = nu6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pu6.c cVar) {
            if (cVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = nu6.s.a();
                local.d(a2, "onUIState changed, modelWrapper=" + cVar);
                if (cVar.k() != null) {
                    nu6 nu6 = this.a;
                    pu6.d k = cVar.k();
                    if (k != null) {
                        nu6.a(k);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.a()) {
                    this.a.v();
                }
                if (cVar.l() != null) {
                    nu6 nu62 = this.a;
                    String l = cVar.l();
                    if (l != null) {
                        nu62.e0(l);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                Integer m = cVar.m();
                if (m != null) {
                    m.intValue();
                    nu6 nu63 = this.a;
                    Integer m2 = cVar.m();
                    if (m2 != null) {
                        nu63.n(m2.intValue());
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.f() != null) {
                    nu6 nu64 = this.a;
                    r87<Integer, String> f = cVar.f();
                    if (f != null) {
                        int intValue = f.getFirst().intValue();
                        r87<Integer, String> f2 = cVar.f();
                        if (f2 != null) {
                            nu64.a(intValue, f2.getSecond());
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.d()) {
                    this.a.a0(this.b);
                }
                if (cVar.c() != null) {
                    nu6 nu65 = this.a;
                    String c = cVar.c();
                    if (c != null) {
                        nu65.Z(c);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.i() != null) {
                    nu6 nu66 = this.a;
                    String i = cVar.i();
                    if (i != null) {
                        nu66.d0(i);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.e() != null) {
                    nu6 nu67 = this.a;
                    String e = cVar.e();
                    if (e != null) {
                        nu67.b0(e);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.b() != null) {
                    nu6 nu68 = this.a;
                    String b2 = cVar.b();
                    if (b2 != null) {
                        nu68.Y(b2);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.h() != null) {
                    nu6 nu69 = this.a;
                    String h = cVar.h();
                    if (h != null) {
                        nu69.c0(h);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                if (cVar.g()) {
                    this.a.c();
                }
                if (cVar.j()) {
                    this.a.g1();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<pu6.a> {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public e(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pu6.a aVar) {
            String str;
            boolean b = aVar.b();
            boolean c = aVar.c();
            if (b) {
                if (c) {
                    str = ig5.a(PortfolioApp.g0.c(), 2131886253);
                } else {
                    str = ig5.a(PortfolioApp.g0.c(), 2131886252);
                }
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                ee7.a((Object) str, "title");
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886238);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
                bx6.a(childFragmentManager, str, a2, aVar.a());
            } else if (!c) {
                bx6 bx62 = bx6.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager2, "childFragmentManager");
                bx62.b(childFragmentManager2, nu6.a(this.a).h());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public f(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nu6.a(this.a).a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public g(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = nu6.s.a();
            local.d(a2, "getWatchSerial=" + nu6.a(this.a).l());
            if (this.a.isActive() && !TextUtils.isEmpty(nu6.a(this.a).l())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.z;
                FragmentActivity requireActivity = this.a.requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                String l = nu6.a(this.a).l();
                if (l != null) {
                    aVar.a(requireActivity, l);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public h(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.isActive() && !TextUtils.isEmpty(nu6.a(this.a).l())) {
                CalibrationActivity.a aVar = CalibrationActivity.z;
                FragmentActivity requireActivity = this.a.requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public i(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nu6.a(this.a).a(100);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public j(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nu6.a(this.a).a(50);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public k(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nu6.a(this.a).a(25);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public l(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nu6 a;

        @DexIgnore
        public m(nu6 nu6) {
            this.a = nu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nu6.a(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ c75 a;
        @DexIgnore
        public /* final */ /* synthetic */ nu6 b;

        @DexIgnore
        public n(c75 c75, nu6 nu6, pu6.d dVar) {
            this.a = c75;
            this.b = nu6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            this.b.f1().a(str2).a(this.a.z);
        }
    }

    /*
    static {
        String simpleName = nu6.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchSettingFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pu6 a(nu6 nu6) {
        pu6 pu6 = nu6.h;
        if (pu6 != null) {
            return pu6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.b(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Z(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.e(str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a0(String str) {
        ee7.b(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            TroubleshootingActivity.a.a(aVar, requireContext, str, false, false, 12, null);
        }
    }

    @DexIgnore
    public final void b(mn4 mn4) {
        long b2 = vt4.a.b();
        Date m2 = mn4.m();
        if (b2 > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.y.a(this, mn4, null);
        } else {
            BCWaitingChallengeDetailActivity.z.a(this, mn4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    public final void b0(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.d(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    public final void c0(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.c(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void d0(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.f(str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return r;
    }

    @DexIgnore
    public final void e0(String str) {
        ee7.b(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            qw6<c75> qw6 = this.g;
            if (qw6 != null) {
                c75 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.M;
                    ee7.a((Object) flexibleTextView, "it.tvLastSyncValue");
                    flexibleTextView.setText(str);
                    FlexibleTextView flexibleTextView2 = a2.M;
                    ee7.a((Object) flexibleTextView2, "it.tvLastSyncValue");
                    flexibleTextView2.setSelected(true);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d(r, "stopLoading");
        a();
    }

    @DexIgnore
    public final iw f1() {
        iw iwVar = this.j;
        if (iwVar != null) {
            return iwVar;
        }
        ee7.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(r, "startLoading");
        b();
    }

    @DexIgnore
    public final void g1() {
        FlexibleButton flexibleButton;
        qw6<c75> qw6 = this.g;
        if (qw6 != null) {
            c75 a2 = qw6.a();
            if (a2 != null && (flexibleButton = a2.s) != null) {
                flexibleButton.setText(ig5.a(PortfolioApp.g0.c(), 2131887180));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<c75> qw6 = this.g;
        if (qw6 != null) {
            c75 a2 = qw6.a();
            if (a2 != null) {
                a2.w.a("flexible_button_secondary");
                a2.y.a("flexible_button_secondary");
                a2.x.a("flexible_button_secondary");
                if (i2 == 25) {
                    a2.x.a("flexible_button_primary");
                } else if (i2 == 50) {
                    a2.y.a("flexible_button_primary");
                } else if (i2 == 100) {
                    a2.w.a("flexible_button_primary");
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        c75 c75 = (c75) qb.a(layoutInflater, 2131558637, viewGroup, false, a1());
        PortfolioApp.g0.c().f().n().a(this);
        rj4 rj4 = this.p;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(pu6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            pu6 pu6 = (pu6) a2;
            this.h = pu6;
            if (pu6 != null) {
                pu6.o();
                Bundle arguments = getArguments();
                Object obj = arguments != null ? arguments.get("SERIAL") : null;
                if (obj != null) {
                    String str = (String) obj;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = r;
                    local.d(str2, "serial=" + str);
                    pu6 pu62 = this.h;
                    if (pu62 != null) {
                        pu62.i(str);
                        pu6 pu63 = this.h;
                        if (pu63 != null) {
                            pu63.b().a(getViewLifecycleOwner(), new b(this));
                            pu6 pu64 = this.h;
                            if (pu64 != null) {
                                pu64.d().a(getViewLifecycleOwner(), new c(this));
                                pu6 pu65 = this.h;
                                if (pu65 != null) {
                                    pu65.k().a(getViewLifecycleOwner(), new d(this, str));
                                    pu6 pu66 = this.h;
                                    if (pu66 != null) {
                                        if (pu66.r()) {
                                            FlexibleTextView flexibleTextView = c75.R;
                                            ee7.a((Object) flexibleTextView, "bindingLocal.tvVibration");
                                            flexibleTextView.setVisibility(0);
                                            FlexibleButton flexibleButton = c75.x;
                                            ee7.a((Object) flexibleButton, "bindingLocal.fbVibrationLow");
                                            flexibleButton.setVisibility(0);
                                            FlexibleButton flexibleButton2 = c75.y;
                                            ee7.a((Object) flexibleButton2, "bindingLocal.fbVibrationMedium");
                                            flexibleButton2.setVisibility(0);
                                            FlexibleButton flexibleButton3 = c75.w;
                                            ee7.a((Object) flexibleButton3, "bindingLocal.fbVibrationHigh");
                                            flexibleButton3.setVisibility(0);
                                        } else {
                                            FlexibleTextView flexibleTextView2 = c75.R;
                                            ee7.a((Object) flexibleTextView2, "bindingLocal.tvVibration");
                                            flexibleTextView2.setVisibility(8);
                                            FlexibleButton flexibleButton4 = c75.x;
                                            ee7.a((Object) flexibleButton4, "bindingLocal.fbVibrationLow");
                                            flexibleButton4.setVisibility(8);
                                            FlexibleButton flexibleButton5 = c75.y;
                                            ee7.a((Object) flexibleButton5, "bindingLocal.fbVibrationMedium");
                                            flexibleButton5.setVisibility(8);
                                            FlexibleButton flexibleButton6 = c75.w;
                                            ee7.a((Object) flexibleButton6, "bindingLocal.fbVibrationHigh");
                                            flexibleButton6.setVisibility(8);
                                        }
                                        pu6 pu67 = this.h;
                                        if (pu67 != null) {
                                            pu67.g().a(getViewLifecycleOwner(), new e(this));
                                            qw6<c75> qw6 = new qw6<>(this, c75);
                                            this.g = qw6;
                                            if (qw6 != null) {
                                                c75 a3 = qw6.a();
                                                if (a3 != null) {
                                                    ee7.a((Object) a3, "mBinding.get()!!");
                                                    return a3.d();
                                                }
                                                ee7.a();
                                                throw null;
                                            }
                                            ee7.d("mBinding");
                                            throw null;
                                        }
                                        ee7.d("mViewModel");
                                        throw null;
                                    }
                                    ee7.d("mViewModel");
                                    throw null;
                                }
                                ee7.d("mViewModel");
                                throw null;
                            }
                            ee7.d("mViewModel");
                            throw null;
                        }
                        ee7.d("mViewModel");
                        throw null;
                    }
                    ee7.d("mViewModel");
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type kotlin.String");
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        pu6 pu6 = this.h;
        if (pu6 != null) {
            pu6.p();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pu6 pu6 = this.h;
        if (pu6 != null) {
            pu6.o();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        ConstraintLayout constraintLayout2;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        iw a2 = aw.a(this);
        ee7.a((Object) a2, "Glide.with(this)");
        this.j = a2;
        qw6<c75> qw6 = this.g;
        if (qw6 != null) {
            c75 a3 = qw6.a();
            if (a3 != null) {
                a3.N.setOnClickListener(new f(this));
                a3.I.setOnClickListener(new g(this));
                a3.E.setOnClickListener(new h(this));
                String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b2) && (constraintLayout2 = a3.u) != null) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(b2));
                }
                String b3 = eh5.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b3) && (constraintLayout = a3.t) != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b3));
                }
                a3.w.setOnClickListener(new i(this));
                a3.y.setOnClickListener(new j(this));
                a3.x.setOnClickListener(new k(this));
                a3.r.setOnClickListener(new l(this));
                a3.s.setOnClickListener(new m(this));
            }
            V("watch_setting_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void v() {
        FragmentActivity activity;
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        if (isActive() && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void a(pu6.d dVar) {
        ee7.b(dVar, "watchSetting");
        FLogger.INSTANCE.getLocal().d(r, "updateDeviceInfo");
        if (isActive()) {
            qw6<c75> qw6 = this.g;
            if (qw6 != null) {
                c75 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.P;
                    ee7.a((Object) flexibleTextView, "it.tvSerialValue");
                    flexibleTextView.setText(dVar.a().getDeviceId());
                    FlexibleTextView flexibleTextView2 = a2.K;
                    ee7.a((Object) flexibleTextView2, "it.tvFwVersionValue");
                    flexibleTextView2.setText(dVar.a().getFirmwareRevision());
                    FlexibleTextView flexibleTextView3 = a2.H;
                    ee7.a((Object) flexibleTextView3, "it.tvDeviceName");
                    flexibleTextView3.setText(dVar.b());
                    boolean d2 = dVar.d();
                    if (d2) {
                        Boolean c2 = dVar.c();
                        if (c2 != null && c2.booleanValue()) {
                            Integer vibrationStrength = dVar.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                n(vibrationStrength.intValue());
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        if (dVar.e()) {
                            FlexibleTextView flexibleTextView4 = a2.H;
                            ee7.a((Object) flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(1.0f);
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, v6.c(PortfolioApp.g0.c(), 2131231071), (Drawable) null);
                            FlexibleTextView flexibleTextView5 = a2.G;
                            ee7.a((Object) flexibleTextView5, "it.tvConnectionStatus");
                            flexibleTextView5.setAlpha(1.0f);
                            FlexibleTextView flexibleTextView6 = a2.G;
                            ee7.a((Object) flexibleTextView6, "it.tvConnectionStatus");
                            flexibleTextView6.setText(a(true, dVar.a().getBatteryLevel()));
                            if (dVar.a().getBatteryLevel() >= 0) {
                                int batteryLevel = dVar.a().getBatteryLevel();
                                a2.G.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, v6.c(PortfolioApp.g0.c(), (batteryLevel >= 0 && 25 >= batteryLevel) ? 2131231004 : (25 <= batteryLevel && 50 >= batteryLevel) ? 2131231006 : (50 <= batteryLevel && 75 >= batteryLevel) ? 2131231008 : 2131231002), (Drawable) null);
                            }
                            FlexibleButton flexibleButton = a2.q;
                            ee7.a((Object) flexibleButton, "it.btActive");
                            flexibleButton.setVisibility(0);
                            FlexibleButton flexibleButton2 = a2.s;
                            ee7.a((Object) flexibleButton2, "it.btConnect");
                            flexibleButton2.setVisibility(8);
                            FlexibleTextView flexibleTextView7 = a2.E;
                            ee7.a((Object) flexibleTextView7, "it.tvCalibration");
                            flexibleTextView7.setAlpha(1.0f);
                            FlexibleButton flexibleButton3 = a2.w;
                            ee7.a((Object) flexibleButton3, "it.fbVibrationHigh");
                            flexibleButton3.setEnabled(true);
                            FlexibleButton flexibleButton4 = a2.x;
                            ee7.a((Object) flexibleButton4, "it.fbVibrationLow");
                            flexibleButton4.setEnabled(true);
                            FlexibleButton flexibleButton5 = a2.y;
                            ee7.a((Object) flexibleButton5, "it.fbVibrationMedium");
                            flexibleButton5.setEnabled(true);
                            FlexibleTextView flexibleTextView8 = a2.E;
                            ee7.a((Object) flexibleTextView8, "it.tvCalibration");
                            flexibleTextView8.setEnabled(true);
                        } else {
                            a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            String str = this.i;
                            if (str != null) {
                                a2.H.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView9 = a2.H;
                            ee7.a((Object) flexibleTextView9, "it.tvDeviceName");
                            flexibleTextView9.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView10 = a2.G;
                            ee7.a((Object) flexibleTextView10, "it.tvConnectionStatus");
                            flexibleTextView10.setText(a(false, dVar.a().getBatteryLevel()));
                            FlexibleTextView flexibleTextView11 = a2.G;
                            ee7.a((Object) flexibleTextView11, "it.tvConnectionStatus");
                            flexibleTextView11.setAlpha(0.4f);
                            FlexibleButton flexibleButton6 = a2.q;
                            ee7.a((Object) flexibleButton6, "it.btActive");
                            flexibleButton6.setVisibility(8);
                            FlexibleButton flexibleButton7 = a2.s;
                            ee7.a((Object) flexibleButton7, "it.btConnect");
                            flexibleButton7.setVisibility(0);
                            FlexibleButton flexibleButton8 = a2.s;
                            ee7.a((Object) flexibleButton8, "it.btConnect");
                            flexibleButton8.setText(ig5.a(PortfolioApp.g0.c(), 2131887180));
                            FlexibleTextView flexibleTextView12 = a2.E;
                            ee7.a((Object) flexibleTextView12, "it.tvCalibration");
                            flexibleTextView12.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView13 = a2.E;
                            ee7.a((Object) flexibleTextView13, "it.tvCalibration");
                            flexibleTextView13.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.w.a("flexible_button_secondary");
                        a2.y.a("flexible_button_secondary");
                        a2.x.a("flexible_button_secondary");
                        a2.H.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        String str2 = this.i;
                        if (str2 != null) {
                            a2.H.setTextColor(Color.parseColor(str2));
                        }
                        FlexibleTextView flexibleTextView14 = a2.H;
                        ee7.a((Object) flexibleTextView14, "it.tvDeviceName");
                        flexibleTextView14.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView15 = a2.G;
                        ee7.a((Object) flexibleTextView15, "it.tvConnectionStatus");
                        flexibleTextView15.setText(a(false, dVar.a().getBatteryLevel()));
                        FlexibleTextView flexibleTextView16 = a2.G;
                        ee7.a((Object) flexibleTextView16, "it.tvConnectionStatus");
                        flexibleTextView16.setAlpha(0.4f);
                        FlexibleButton flexibleButton9 = a2.q;
                        ee7.a((Object) flexibleButton9, "it.btActive");
                        flexibleButton9.setVisibility(8);
                        FlexibleButton flexibleButton10 = a2.s;
                        ee7.a((Object) flexibleButton10, "it.btConnect");
                        flexibleButton10.setText(ig5.a(PortfolioApp.g0.c(), 2131887181));
                        FlexibleTextView flexibleTextView17 = a2.E;
                        ee7.a((Object) flexibleTextView17, "it.tvCalibration");
                        flexibleTextView17.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView18 = a2.E;
                        ee7.a((Object) flexibleTextView18, "it.tvCalibration");
                        flexibleTextView18.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(dVar.a().getDeviceId()).setSerialPrefix(be5.o.b(dVar.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.z;
                    ee7.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, be5.o.b(dVar.a().getDeviceId(), be5.b.SMALL)).setImageCallback(new n(a2, this, dVar)).download();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        ee7.b(str, "message");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        mn4 mn4 = null;
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == 2131363229) {
                        pu6 pu6 = this.h;
                        if (pu6 != null) {
                            pu6.g(stringExtra);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363307) {
                        pu6 pu62 = this.h;
                        if (pu62 != null) {
                            pu62.c(stringExtra);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == 2131363307) {
                        pu6 pu63 = this.h;
                        if (pu63 != null) {
                            pu63.e(stringExtra2);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == 2131363307) {
                        pu6 pu64 = this.h;
                        if (pu64 != null) {
                            pu64.n();
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == 2131363307) {
                        pu6 pu65 = this.h;
                        if (pu65 != null) {
                            pu65.b(stringExtra3);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == 2131363307) {
                        pu6 pu66 = this.h;
                        if (pu66 != null) {
                            pu66.f(stringExtra4);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == 2131363229) {
                        pu6 pu67 = this.h;
                        if (pu67 != null) {
                            pu67.h(stringExtra5);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == 2131363307) {
                        pu6 pu68 = this.h;
                        if (pu68 != null) {
                            pu68.d(stringExtra5);
                            return;
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 1970588827:
                if (str.equals("LEAVE_CHALLENGE")) {
                    a();
                    if (i2 == 2131363307) {
                        if (intent != null) {
                            mn4 = (mn4) intent.getParcelableExtra("CHALLENGE");
                        }
                        if (mn4 != null) {
                            b(mn4);
                            return;
                        }
                        return;
                    }
                    return;
                }
                break;
        }
        super.a(str, i2, intent);
    }

    @DexIgnore
    public final String a(boolean z, int i2) {
        String a2 = ig5.a(PortfolioApp.g0.c(), z ? 2131887143 : 2131887120);
        if (!z || i2 <= 0) {
            ee7.a((Object) a2, "connectedString");
            return a2;
        }
        return a2 + ", " + i2 + '%';
    }
}
