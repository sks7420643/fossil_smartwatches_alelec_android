package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt2 extends yt2<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ Object b;

    @DexIgnore
    public gt2(Object obj) {
        this.b = obj;
    }

    @DexIgnore
    public final boolean hasNext() {
        return !this.a;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final T next() {
        if (!this.a) {
            this.a = true;
            return (T) this.b;
        }
        throw new NoSuchElementException();
    }
}
