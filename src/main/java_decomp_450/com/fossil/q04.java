package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q04 implements Closeable {
    @DexIgnore
    public static /* final */ c d; // = (b.b() ? b.a : a.a);
    @DexIgnore
    public /* final */ c a;
    @DexIgnore
    public /* final */ Deque<Closeable> b; // = new ArrayDeque(4);
    @DexIgnore
    public Throwable c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        @Override // com.fossil.q04.c
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            Logger logger = p04.a;
            Level level = Level.WARNING;
            logger.log(level, "Suppressing exception thrown when closing " + closeable, th2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {
        @DexIgnore
        public static /* final */ b a; // = new b();
        @DexIgnore
        public static /* final */ Method b; // = a();

        @DexIgnore
        public static Method a() {
            try {
                return Throwable.class.getMethod("addSuppressed", Throwable.class);
            } catch (Throwable unused) {
                return null;
            }
        }

        @DexIgnore
        public static boolean b() {
            return b != null;
        }

        @DexIgnore
        @Override // com.fossil.q04.c
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            if (th != th2) {
                try {
                    b.invoke(th, th2);
                } catch (Throwable unused) {
                    a.a.a(closeable, th, th2);
                }
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Closeable closeable, Throwable th, Throwable th2);
    }

    @DexIgnore
    public q04(c cVar) {
        jw3.a(cVar);
        this.a = cVar;
    }

    @DexIgnore
    public static q04 a() {
        return new q04(d);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        Throwable th = this.c;
        while (!this.b.isEmpty()) {
            Closeable removeFirst = this.b.removeFirst();
            try {
                removeFirst.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.a.a(removeFirst, th, th2);
                }
            }
        }
        if (this.c == null && th != null) {
            ow3.b(th, IOException.class);
            throw new AssertionError(th);
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <C extends Closeable> C a(C c2) {
        if (c2 != null) {
            this.b.addFirst(c2);
        }
        return c2;
    }

    @DexIgnore
    public RuntimeException a(Throwable th) throws IOException {
        jw3.a(th);
        this.c = th;
        ow3.b(th, IOException.class);
        throw new RuntimeException(th);
    }
}
