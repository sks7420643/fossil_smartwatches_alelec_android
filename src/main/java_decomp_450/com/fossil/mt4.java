package com.fossil;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt4 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ List<oo4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ SparseArray<a> b; // = new SparseArray<>();
    @DexIgnore
    public /* final */ fr5 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ u95 a;
        @DexIgnore
        public /* final */ fr5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mt4$a$a")
        /* renamed from: com.fossil.mt4$a$a  reason: collision with other inner class name */
        public static final class C0130a extends fe7 implements gd7<View, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ mn4 $challenge;
            @DexIgnore
            public /* final */ /* synthetic */ oo4 $history$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0130a(mn4 mn4, a aVar, oo4 oo4) {
                super(1);
                this.$challenge = mn4;
                this.this$0 = aVar;
                this.$history$inlined = oo4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(View view) {
                invoke(view);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                this.this$0.a().a(this.$challenge);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(u95 u95, fr5 fr5) {
            super(u95.d());
            ee7.b(u95, "binding");
            ee7.b(fr5, "listener");
            this.a = u95;
            this.b = fr5;
        }

        @DexIgnore
        public final fr5 a() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0236  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x023b  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0285  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(com.fossil.oo4 r19) {
            /*
                r18 = this;
                r0 = r18
                r1 = r19
                java.lang.String r2 = "history"
                com.fossil.ee7.b(r1, r2)
                com.fossil.u95 r2 = r0.a
                com.fossil.mn4 r3 = r19.a()
                com.fossil.jn4 r4 = r19.c()
                com.fossil.jn4 r5 = r19.b()
                java.lang.Integer r6 = r3.h()
                com.portfolio.platform.view.FlexibleTextView r7 = r2.q
                java.lang.String r8 = "ftvChallengeName"
                com.fossil.ee7.a(r7, r8)
                java.lang.String r8 = r3.g()
                r7.setText(r8)
                com.portfolio.platform.view.FlexibleTextView r7 = r2.s
                java.lang.String r8 = "ftvCountPlayers"
                com.fossil.ee7.a(r7, r8)
                r8 = 0
                if (r6 == 0) goto L_0x003c
                int r9 = r6.intValue()
                java.lang.String r9 = java.lang.String.valueOf(r9)
                goto L_0x003d
            L_0x003c:
                r9 = r8
            L_0x003d:
                r7.setText(r9)
                com.portfolio.platform.uirenew.customview.TimerTextView r7 = r2.r
                java.lang.String r9 = "ftvCountDown"
                com.fossil.ee7.a(r7, r9)
                java.lang.String r10 = "--"
                r7.setText(r10)
                if (r4 == 0) goto L_0x0053
                java.lang.Boolean r7 = r4.s()
                goto L_0x0054
            L_0x0053:
                r7 = r8
            L_0x0054:
                r10 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r10)
                boolean r7 = com.fossil.ee7.a(r7, r11)
                java.lang.String r11 = "ftvPlayerInfo"
                if (r7 == 0) goto L_0x0073
                com.portfolio.platform.view.FlexibleTextView r7 = r2.t
                com.fossil.ee7.a(r7, r11)
                android.content.Context r7 = r7.getContext()
                r8 = 2131887275(0x7f1204ab, float:1.9409152E38)
                java.lang.String r7 = com.fossil.ig5.a(r7, r8)
            L_0x0071:
                r13 = r7
                goto L_0x0093
            L_0x0073:
                com.fossil.fu4 r7 = com.fossil.fu4.a
                if (r4 == 0) goto L_0x007c
                java.lang.String r12 = r4.c()
                goto L_0x007d
            L_0x007c:
                r12 = r8
            L_0x007d:
                if (r4 == 0) goto L_0x0083
                java.lang.String r8 = r4.e()
            L_0x0083:
                if (r4 == 0) goto L_0x008c
                java.lang.String r13 = r4.i()
                if (r13 == 0) goto L_0x008c
                goto L_0x008e
            L_0x008c:
                java.lang.String r13 = "-"
            L_0x008e:
                java.lang.String r7 = r7.a(r12, r8, r13)
                goto L_0x0071
            L_0x0093:
                java.lang.String r7 = r3.u()
                java.lang.String r8 = "imgProgress"
                java.lang.String r15 = "java.lang.String.format(format, *args)"
                if (r7 != 0) goto L_0x009e
                goto L_0x00b0
            L_0x009e:
                int r12 = r7.hashCode()
                r14 = -1348781656(0xffffffffaf9b39a8, float:-2.8235303E-10)
                java.lang.String r10 = "LanguageHelper.getString\u2026ry_Text__NameIsTheWinner)"
                java.lang.String r0 = "namePlayer"
                if (r12 == r14) goto L_0x01a1
                r14 = -637042289(0xffffffffda07818f, float:-9.5353933E15)
                if (r12 == r14) goto L_0x00b4
            L_0x00b0:
                r4 = r15
                r10 = 0
                goto L_0x0217
            L_0x00b4:
                java.lang.String r12 = "activity_reach_goal"
                boolean r7 = r7.equals(r12)
                if (r7 == 0) goto L_0x00b0
                com.portfolio.platform.buddy_challenge.customview.CircleProgressView r7 = r2.v
                com.fossil.ee7.a(r7, r8)
                android.content.Context r12 = r7.getContext()
                r14 = 2131099690(0x7f06002a, float:1.781174E38)
                int r12 = com.fossil.v6.a(r12, r14)
                r7.setProgressColour(r12)
                if (r4 == 0) goto L_0x00dc
                java.lang.Integer r7 = r4.p()
                if (r7 == 0) goto L_0x00dc
                int r7 = r7.intValue()
                goto L_0x00dd
            L_0x00dc:
                r7 = 0
            L_0x00dd:
                java.lang.Integer r12 = r3.t()
                if (r12 == 0) goto L_0x00e8
                int r12 = r12.intValue()
                goto L_0x00e9
            L_0x00e8:
                r12 = 0
            L_0x00e9:
                if (r7 < r12) goto L_0x0146
                boolean r4 = com.fossil.ee7.a(r5, r4)
                if (r4 == 0) goto L_0x0107
                com.portfolio.platform.view.FlexibleTextView r0 = r2.t
                com.fossil.ee7.a(r0, r11)
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r7 = 2131886236(0x7f12009c, float:1.9407045E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r7)
                r0.setText(r4)
                goto L_0x00b0
            L_0x0107:
                com.fossil.we7 r4 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r7 = 2131886234(0x7f12009a, float:1.9407041E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r7)
                com.fossil.ee7.a(r4, r10)
                r7 = 1
                java.lang.Object[] r10 = new java.lang.Object[r7]
                r14 = 0
                r10[r14] = r13
                java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r7)
                java.lang.String r4 = java.lang.String.format(r4, r10)
                com.fossil.ee7.a(r4, r15)
                com.portfolio.platform.view.FlexibleTextView r7 = r2.t
                com.fossil.ee7.a(r7, r11)
                com.fossil.xe5 r12 = com.fossil.xe5.b
                com.fossil.ee7.a(r13, r0)
                r0 = 0
                r16 = 4
                r17 = 0
                r10 = 0
                r14 = r4
                r4 = r15
                r15 = r0
                android.text.Spannable r0 = com.fossil.xe5.a(r12, r13, r14, r15, r16, r17)
                r7.setText(r0)
                goto L_0x0217
            L_0x0146:
                r7 = r15
                r10 = 0
                boolean r4 = com.fossil.ee7.a(r5, r4)
                if (r4 == 0) goto L_0x0164
                com.portfolio.platform.view.FlexibleTextView r0 = r2.t
                com.fossil.ee7.a(r0, r11)
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r11 = 2131887194(0x7f12045a, float:1.9408988E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r11)
                r0.setText(r4)
                goto L_0x019e
            L_0x0164:
                com.fossil.we7 r4 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r12 = 2131887190(0x7f120456, float:1.940898E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r12)
                java.lang.String r12 = "LanguageHelper.getString\u2026ring._____NameWasLeading)"
                com.fossil.ee7.a(r4, r12)
                r12 = 1
                java.lang.Object[] r14 = new java.lang.Object[r12]
                r14[r10] = r13
                java.lang.Object[] r14 = java.util.Arrays.copyOf(r14, r12)
                java.lang.String r14 = java.lang.String.format(r4, r14)
                com.fossil.ee7.a(r14, r7)
                com.portfolio.platform.view.FlexibleTextView r4 = r2.t
                com.fossil.ee7.a(r4, r11)
                com.fossil.xe5 r12 = com.fossil.xe5.b
                com.fossil.ee7.a(r13, r0)
                r15 = 0
                r16 = 4
                r17 = 0
                android.text.Spannable r0 = com.fossil.xe5.a(r12, r13, r14, r15, r16, r17)
                r4.setText(r0)
            L_0x019e:
                r4 = r7
                goto L_0x0217
            L_0x01a1:
                r14 = 0
                java.lang.String r12 = "activity_best_result"
                boolean r7 = r7.equals(r12)
                if (r7 == 0) goto L_0x00b0
                com.portfolio.platform.buddy_challenge.customview.CircleProgressView r7 = r2.v
                com.fossil.ee7.a(r7, r8)
                android.content.Context r12 = r7.getContext()
                r14 = 2131099678(0x7f06001e, float:1.7811716E38)
                int r12 = com.fossil.v6.a(r12, r14)
                r7.setProgressColour(r12)
                boolean r4 = com.fossil.ee7.a(r5, r4)
                if (r4 == 0) goto L_0x01da
                com.portfolio.platform.view.FlexibleTextView r0 = r2.t
                com.fossil.ee7.a(r0, r11)
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r7 = 2131886236(0x7f12009c, float:1.9407045E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r7)
                r0.setText(r4)
                goto L_0x00b0
            L_0x01da:
                com.fossil.we7 r4 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r4 = r4.c()
                r7 = 2131886234(0x7f12009a, float:1.9407041E38)
                java.lang.String r4 = com.fossil.ig5.a(r4, r7)
                com.fossil.ee7.a(r4, r10)
                r7 = 1
                java.lang.Object[] r10 = new java.lang.Object[r7]
                r14 = 0
                r10[r14] = r13
                java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r7)
                java.lang.String r4 = java.lang.String.format(r4, r10)
                com.fossil.ee7.a(r4, r15)
                com.portfolio.platform.view.FlexibleTextView r7 = r2.t
                com.fossil.ee7.a(r7, r11)
                com.fossil.xe5 r12 = com.fossil.xe5.b
                com.fossil.ee7.a(r13, r0)
                r0 = 0
                r16 = 4
                r17 = 0
                r10 = 0
                r14 = r4
                r4 = r15
                r15 = r0
                android.text.Spannable r0 = com.fossil.xe5.a(r12, r13, r14, r15, r16, r17)
                r7.setText(r0)
            L_0x0217:
                com.portfolio.platform.uirenew.customview.TimerTextView r0 = r2.r
                com.fossil.ee7.a(r0, r9)
                if (r5 == 0) goto L_0x022f
                java.lang.Integer r7 = r5.p()
                if (r7 == 0) goto L_0x022f
                int r7 = r7.intValue()
                java.lang.String r7 = java.lang.String.valueOf(r7)
                if (r7 == 0) goto L_0x022f
                goto L_0x0231
            L_0x022f:
                java.lang.String r7 = "0"
            L_0x0231:
                r0.setText(r7)
                if (r6 == 0) goto L_0x023b
                int r14 = r6.intValue()
                goto L_0x023c
            L_0x023b:
                r14 = 0
            L_0x023c:
                com.fossil.we7 r0 = com.fossil.we7.a
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r6 = 2131887272(0x7f1204a8, float:1.9409146E38)
                java.lang.String r0 = com.fossil.ig5.a(r0, r6)
                java.lang.String r6 = "LanguageHelper.getString\u2026.buddy_challenge_ranking)"
                com.fossil.ee7.a(r0, r6)
                r6 = 2
                java.lang.Object[] r7 = new java.lang.Object[r6]
                if (r5 == 0) goto L_0x025c
                java.lang.Integer r5 = r5.h()
                if (r5 == 0) goto L_0x025c
                goto L_0x0260
            L_0x025c:
                java.lang.Integer r5 = java.lang.Integer.valueOf(r14)
            L_0x0260:
                r7[r10] = r5
                java.lang.Integer r5 = java.lang.Integer.valueOf(r14)
                r9 = 1
                r7[r9] = r5
                java.lang.Object[] r5 = java.util.Arrays.copyOf(r7, r6)
                java.lang.String r0 = java.lang.String.format(r0, r5)
                com.fossil.ee7.a(r0, r4)
                com.portfolio.platform.view.FlexibleTextView r4 = r2.u
                java.lang.String r5 = "ftvRanking"
                com.fossil.ee7.a(r4, r5)
                r4.setText(r0)
                java.lang.String r0 = r3.j()
                if (r0 != 0) goto L_0x0285
                goto L_0x02a0
            L_0x0285:
                int r4 = r0.hashCode()
                r5 = -314497661(0xffffffffed412583, float:-3.7359972E27)
                if (r4 == r5) goto L_0x028f
                goto L_0x02a0
            L_0x028f:
                java.lang.String r4 = "private"
                boolean r0 = r0.equals(r4)
                if (r0 == 0) goto L_0x02a0
                com.portfolio.platform.view.FlexibleTextView r0 = r2.s
                r4 = 2131231024(0x7f080130, float:1.8078117E38)
                r0.setCompoundDrawablesWithIntrinsicBounds(r4, r10, r10, r10)
                goto L_0x02a8
            L_0x02a0:
                com.portfolio.platform.view.FlexibleTextView r0 = r2.s
                r4 = 2131231025(0x7f080131, float:1.807812E38)
                r0.setCompoundDrawablesWithIntrinsicBounds(r4, r10, r10, r10)
            L_0x02a8:
                com.portfolio.platform.buddy_challenge.customview.CircleProgressView r0 = r2.v
                com.fossil.ee7.a(r0, r8)
                com.fossil.mt4$a$a r2 = new com.fossil.mt4$a$a
                r4 = r18
                r2.<init>(r3, r4, r1)
                com.fossil.du4.a(r0, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mt4.a.a(com.fossil.oo4):void");
        }
    }

    @DexIgnore
    public mt4(fr5 fr5) {
        ee7.b(fr5, "listener");
        this.c = fr5;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        this.b.put(i, aVar);
        aVar.a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        u95 a2 = u95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemHistoryChallengeLayo\u2026e(inflate, parent, false)");
        return new a(a2, this.c);
    }

    @DexIgnore
    public final void a(List<oo4> list) {
        ee7.b(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }
}
