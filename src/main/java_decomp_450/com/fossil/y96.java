package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.be5;
import com.fossil.cc5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y96 extends go5 implements x96 {
    @DexIgnore
    public qw6<ux4> f;
    @DexIgnore
    public w96 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.x96
    public void b(do5 do5, ArrayList<String> arrayList) {
        ux4 a2;
        OverviewDayChart overviewDayChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayFragment", "showDayDetails - baseModel=" + do5);
        qw6<ux4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayChart.a(do5);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "CaloriesOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        ux4 a2;
        OverviewDayChart overviewDayChart;
        qw6<ux4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            be5.a aVar = be5.o;
            w96 w96 = this.g;
            if (aVar.a(w96 != null ? w96.h() : null)) {
                overviewDayChart.a("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ux4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onCreateView");
        ux4 ux4 = (ux4) qb.a(layoutInflater, 2131558508, viewGroup, false, a1());
        ux4.r.setOnClickListener(b.a);
        this.f = new qw6<>(this, ux4);
        f1();
        qw6<ux4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onResume");
        f1();
        w96 w96 = this.g;
        if (w96 != null) {
            w96.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onStop");
        w96 w96 = this.g;
        if (w96 != null) {
            w96.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.x96
    public void a(boolean z, List<WorkoutSession> list) {
        ux4 a2;
        View d;
        View d2;
        ee7.b(list, "workoutSessions");
        qw6<ux4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    s85 s85 = a2.t;
                    if (s85 != null && (d2 = s85.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                s85 s852 = a2.t;
                if (!(s852 == null || (d = s852.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            ee7.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(w96 w96) {
        ee7.b(w96, "presenter");
        this.g = w96;
    }

    @DexIgnore
    public final void a(s85 s85, WorkoutSession workoutSession) {
        String str;
        AppCompatImageView appCompatImageView;
        if (s85 != null) {
            View d = s85.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            cc5.a aVar = cc5.Companion;
            r87<Integer, Integer> a2 = aVar.a(aVar.a(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String a3 = ig5.a(context, a2.getSecond().intValue());
            s85.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = s85.r;
            ee7.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = s85.s;
            ee7.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            we7 we7 = we7.a;
            String a4 = ig5.a(context, 2131886582);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026esToday_Text__NumberCals)");
            Object[] objArr = new Object[1];
            Float totalCalorie = workoutSession.getTotalCalorie();
            objArr[0] = re5.b(totalCalorie != null ? totalCalorie.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1);
            String format = String.format(a4, Arrays.copyOf(objArr, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = s85.q;
            ee7.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(zd5.a(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            be5.a aVar2 = be5.o;
            w96 w96 = this.g;
            if (aVar2.a(w96 != null ? w96.h() : null)) {
                str = eh5.l.a().b("dianaActiveCaloriesTab");
            } else {
                str = eh5.l.a().b("hybridActiveCaloriesTab");
            }
            if (str != null && (appCompatImageView = s85.t) != null) {
                appCompatImageView.setColorFilter(Color.parseColor(str));
            }
        }
    }
}
