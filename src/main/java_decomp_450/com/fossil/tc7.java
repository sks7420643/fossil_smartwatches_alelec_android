package com.fossil;

import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc7 {
    @DexIgnore
    public static final <T> Class<T> a(tf7<T> tf7) {
        Class<T> cls;
        ee7.b(tf7, "$this$javaObjectType");
        Class<T> cls2 = (Class<T>) ((wd7) tf7).a();
        if (cls2.isPrimitive()) {
            String name = cls2.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals(LegacyTokenHelper.TYPE_DOUBLE)) {
                            cls2 = (Class<T>) Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals(LegacyTokenHelper.TYPE_INTEGER)) {
                            cls2 = (Class<T>) Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals(LegacyTokenHelper.TYPE_BYTE)) {
                            cls2 = (Class<T>) Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals(LegacyTokenHelper.TYPE_CHAR)) {
                            cls2 = (Class<T>) Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals(LegacyTokenHelper.TYPE_LONG)) {
                            cls2 = (Class<T>) Long.class;
                            break;
                        }
                        break;
                    case 3625364:
                        if (name.equals("void")) {
                            cls2 = (Class<T>) Void.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            cls2 = (Class<T>) Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals(LegacyTokenHelper.TYPE_FLOAT)) {
                            cls2 = (Class<T>) Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals(LegacyTokenHelper.TYPE_SHORT)) {
                            cls2 = (Class<T>) Short.class;
                            break;
                        }
                        break;
                }
            }
            if (cls != null) {
                return cls;
            }
            throw new x87("null cannot be cast to non-null type java.lang.Class<T>");
        } else if (cls2 != null) {
            return cls2;
        } else {
            throw new x87("null cannot be cast to non-null type java.lang.Class<T>");
        }
    }

    @DexIgnore
    public static final <T> tf7<T> a(Class<T> cls) {
        ee7.b(cls, "$this$kotlin");
        return te7.a(cls);
    }

    @DexIgnore
    public static final <T> Class<T> a(T t) {
        ee7.b(t, "$this$javaClass");
        Class<T> cls = (Class<T>) t.getClass();
        if (cls != null) {
            return cls;
        }
        throw new x87("null cannot be cast to non-null type java.lang.Class<T>");
    }
}
