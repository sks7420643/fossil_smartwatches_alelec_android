package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij4 {
    @DexIgnore
    public bj4 a;
    @DexIgnore
    public aj4 b;
    @DexIgnore
    public cj4 c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public ej4 e;

    @DexIgnore
    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    @DexIgnore
    public ej4 a() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }

    @DexIgnore
    public void a(bj4 bj4) {
        this.a = bj4;
    }

    @DexIgnore
    public void a(aj4 aj4) {
        this.b = aj4;
    }

    @DexIgnore
    public void a(cj4 cj4) {
        this.c = cj4;
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(ej4 ej4) {
        this.e = ej4;
    }
}
