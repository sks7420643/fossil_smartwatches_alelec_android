package com.fossil;

import android.content.Context;
import android.util.Log;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bz1 extends me<Void> implements c22 {
    @DexIgnore
    public Semaphore a; // = new Semaphore(0);
    @DexIgnore
    public Set<a12> b;

    @DexIgnore
    public bz1(Context context, Set<a12> set) {
        super(context);
        this.b = set;
    }

    @DexIgnore
    /* renamed from: a */
    public final Void loadInBackground() {
        int i = 0;
        for (a12 a12 : this.b) {
            if (a12.a(this)) {
                i++;
            }
        }
        try {
            this.a.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.c22
    public final void onComplete() {
        this.a.release();
    }

    @DexIgnore
    @Override // com.fossil.oe
    public final void onStartLoading() {
        this.a.drainPermits();
        forceLoad();
    }
}
