package com.fossil;

import com.fossil.dz3;
import com.fossil.iy3;
import com.fossil.vx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hy3<E> extends vx3<E> implements dz3<E> {
    @DexIgnore
    @LazyInit
    public transient zx3<E> a;
    @DexIgnore
    @LazyInit
    public transient iy3<dz3.a<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends j04<E> {
        @DexIgnore
        public int a;
        @DexIgnore
        public E b;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator c;

        @DexIgnore
        public a(hy3 hy3, Iterator it) {
            this.c = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a > 0 || this.c.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (this.a <= 0) {
                dz3.a aVar = (dz3.a) this.c.next();
                this.b = (E) aVar.getElement();
                this.a = aVar.getCount();
            }
            this.a--;
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> extends vx3.b<E> {
        @DexIgnore
        public /* final */ dz3<E> a;

        @DexIgnore
        public b() {
            this(sy3.create());
        }

        @DexIgnore
        public b(dz3<E> dz3) {
            this.a = dz3;
        }

        @DexIgnore
        @Override // com.fossil.vx3.b
        @CanIgnoreReturnValue
        public b<E> a(E e) {
            dz3<E> dz3 = this.a;
            jw3.a(e);
            dz3.add(e);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.vx3.b
        @CanIgnoreReturnValue
        public b<E> a(E... eArr) {
            super.a((Object[]) eArr);
            return this;
        }

        @DexIgnore
        public hy3<E> a() {
            return hy3.copyOf(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends iy3.b<dz3.a<E>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            if (!(obj instanceof dz3.a)) {
                return false;
            }
            dz3.a aVar = (dz3.a) obj;
            if (aVar.getCount() > 0 && hy3.this.count(aVar.getElement()) == aVar.getCount()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.iy3
        public int hashCode() {
            return hy3.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return hy3.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return hy3.this.elementSet().size();
        }

        @DexIgnore
        @Override // com.fossil.vx3, com.fossil.iy3
        public Object writeReplace() {
            return new d(hy3.this);
        }

        @DexIgnore
        public /* synthetic */ c(hy3 hy3, a aVar) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.iy3.b
        public dz3.a<E> get(int i) {
            return hy3.this.getEntry(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<E> implements Serializable {
        @DexIgnore
        public /* final */ hy3<E> multiset;

        @DexIgnore
        public d(hy3<E> hy3) {
            this.multiset = hy3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int[] counts;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public e(dz3<?> dz3) {
            int size = dz3.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            int i = 0;
            for (dz3.a<?> aVar : dz3.entrySet()) {
                this.elements[i] = aVar.getElement();
                this.counts[i] = aVar.getCount();
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            sy3 create = sy3.create(this.elements.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.elements;
                if (i >= objArr.length) {
                    return hy3.copyOf(create);
                }
                create.add(objArr[i], this.counts[i]);
                i++;
            }
        }
    }

    @DexIgnore
    public static <E> hy3<E> a(E... eArr) {
        sy3 create = sy3.create();
        Collections.addAll(create, eArr);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> b<E> builder() {
        return new b<>();
    }

    @DexIgnore
    public static <E> hy3<E> copyFromEntries(Collection<? extends dz3.a<? extends E>> collection) {
        if (collection.isEmpty()) {
            return of();
        }
        return new rz3(collection);
    }

    @DexIgnore
    public static <E> hy3<E> copyOf(E[] eArr) {
        return a(eArr);
    }

    @DexIgnore
    public static <E> hy3<E> of() {
        return rz3.EMPTY;
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    @Deprecated
    public final int add(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public zx3<E> asList() {
        zx3<E> zx3 = this.a;
        if (zx3 != null) {
            return zx3;
        }
        zx3<E> createAsList = createAsList();
        this.a = createAsList;
        return createAsList;
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.vx3
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = entrySet().iterator();
        while (it.hasNext()) {
            dz3.a aVar = (dz3.a) it.next();
            Arrays.fill(objArr, i, aVar.getCount() + i, aVar.getElement());
            i += aVar.getCount();
        }
        return i;
    }

    @DexIgnore
    public zx3<E> createAsList() {
        if (isEmpty()) {
            return zx3.of();
        }
        return new nz3(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return ez3.a(this, obj);
    }

    @DexIgnore
    public abstract dz3.a<E> getEntry(int i);

    @DexIgnore
    public int hashCode() {
        return yz3.a(entrySet());
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    @Deprecated
    public final int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    @Deprecated
    public final int setCount(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public Object writeReplace() {
        return new e(this);
    }

    @DexIgnore
    public static <E> hy3<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof hy3) {
            hy3<E> hy3 = (hy3) iterable;
            if (!hy3.isPartialView()) {
                return hy3;
            }
        }
        return copyFromEntries((iterable instanceof dz3 ? ez3.a(iterable) : sy3.create(iterable)).entrySet());
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2) {
        return a(e2);
    }

    @DexIgnore
    @Override // com.fossil.dz3
    public iy3<dz3.a<E>> entrySet() {
        iy3<dz3.a<E>> iy3 = this.b;
        if (iy3 != null) {
            return iy3;
        }
        iy3<dz3.a<E>> a2 = a();
        this.b = a2;
        return a2;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, java.util.Collection, java.lang.Iterable
    public j04<E> iterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.dz3
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean setCount(E e2, int i, int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2, E e3) {
        return a(e2, e3);
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2, E e3, E e4) {
        return a(e2, e3, e4);
    }

    @DexIgnore
    public final iy3<dz3.a<E>> a() {
        return isEmpty() ? iy3.of() : new c(this, null);
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2, E e3, E e4, E e5) {
        return a(e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2, E e3, E e4, E e5, E e6) {
        return a(e2, e3, e4, e5, e6);
    }

    @DexIgnore
    public static <E> hy3<E> copyOf(Iterator<? extends E> it) {
        sy3 create = sy3.create();
        qy3.a(create, it);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> hy3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E... eArr) {
        b bVar = new b();
        bVar.a((Object) e2);
        bVar.a((Object) e3);
        bVar.a((Object) e4);
        bVar.a((Object) e5);
        bVar.a((Object) e6);
        bVar.a((Object) e7);
        bVar.a((Object[]) eArr);
        return bVar.a();
    }
}
