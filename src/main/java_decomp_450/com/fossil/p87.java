package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p87 extends RuntimeException {
    @DexIgnore
    public p87() {
    }

    @DexIgnore
    public p87(String str) {
        super(str);
    }

    @DexIgnore
    public p87(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public p87(Throwable th) {
        super(th);
    }
}
