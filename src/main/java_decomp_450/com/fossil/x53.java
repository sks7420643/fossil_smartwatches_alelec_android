package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x53 implements Parcelable.Creator<w53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ w53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        boolean z = true;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = Integer.MAX_VALUE;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                z = j72.i(parcel, a);
            } else if (a2 == 2) {
                j = j72.s(parcel, a);
            } else if (a2 == 3) {
                f = j72.n(parcel, a);
            } else if (a2 == 4) {
                j2 = j72.s(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                i = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new w53(z, j, f, j2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ w53[] newArray(int i) {
        return new w53[i];
    }
}
