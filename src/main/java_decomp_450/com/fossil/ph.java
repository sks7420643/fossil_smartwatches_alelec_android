package com.fossil;

import android.util.SparseArray;
import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ph extends ih {
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public ph(long j, RenderScript renderScript) {
        super(j, renderScript);
        new SparseArray();
        new SparseArray();
        new SparseArray();
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public long a(hh hhVar) {
        if (hhVar == null) {
            return 0;
        }
        sh e = hhVar.e();
        long b = e.g().b(((ih) this).c);
        RenderScript renderScript = ((ih) this).c;
        long a = renderScript.a(hhVar.a(renderScript), e.a(((ih) this).c, b), e.h() * e.g().e());
        hhVar.a(a);
        return a;
    }

    @DexIgnore
    public void a(int i, hh hhVar, hh hhVar2, kh khVar) {
        if (hhVar == null && hhVar2 == null) {
            throw new mh("At least one of ain or aout is required to be non-null.");
        }
        long j = 0;
        long a = hhVar != null ? hhVar.a(((ih) this).c) : 0;
        if (hhVar2 != null) {
            j = hhVar2.a(((ih) this).c);
        }
        if (khVar != null) {
            khVar.a();
            throw null;
        } else if (this.d) {
            long a2 = a(hhVar);
            long a3 = a(hhVar2);
            RenderScript renderScript = ((ih) this).c;
            renderScript.a(a(renderScript), i, a2, a3, null, this.d);
        } else {
            RenderScript renderScript2 = ((ih) this).c;
            renderScript2.a(a(renderScript2), i, a, j, null, this.d);
        }
    }

    @DexIgnore
    public void a(int i, float f) {
        RenderScript renderScript = ((ih) this).c;
        renderScript.a(a(renderScript), i, f, this.d);
    }

    @DexIgnore
    public void a(int i, ih ihVar) {
        long j = 0;
        if (this.d) {
            long a = a((hh) ihVar);
            RenderScript renderScript = ((ih) this).c;
            renderScript.a(a(renderScript), i, ihVar == null ? 0 : a, this.d);
            return;
        }
        RenderScript renderScript2 = ((ih) this).c;
        long a2 = a(renderScript2);
        if (ihVar != null) {
            j = ihVar.a(((ih) this).c);
        }
        renderScript2.a(a2, i, j, this.d);
    }
}
