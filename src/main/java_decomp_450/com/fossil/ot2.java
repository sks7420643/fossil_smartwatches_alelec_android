package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot2<K, V> extends ws2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ transient ss2<K, V> c;
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient int e;

    @DexIgnore
    public ot2(ss2<K, V> ss2, Object[] objArr, int i, int i2) {
        this.c = ss2;
        this.d = objArr;
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            if (value == null || !value.equals(this.c.get(key))) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<Map.Entry<K, V>> iterator() {
        return (yt2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final os2<Map.Entry<K, V>> zzd() {
        return new nt2(this);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }
}
