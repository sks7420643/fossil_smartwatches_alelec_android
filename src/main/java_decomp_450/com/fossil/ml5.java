package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml5 extends ll5 {
    @DexIgnore
    public /* final */ List<String> d;
    @DexIgnore
    public String e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ml5(String str, String str2, List<String> list, String str3) {
        super(str, str2);
        ee7.b(str, "tagName");
        ee7.b(str2, "title");
        ee7.b(list, "values");
        ee7.b(str3, "btnText");
        this.d = list;
        this.e = str3;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final List<String> e() {
        return this.d;
    }
}
