package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs2 extends gs2<K> {
    @DexIgnore
    public /* final */ /* synthetic */ ds2 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cs2(ds2 ds2) {
        super(ds2, null);
        this.e = ds2;
    }

    @DexIgnore
    @Override // com.fossil.gs2
    public final K a(int i) {
        return (K) this.e.zzb[i];
    }
}
