package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ta2> CREATOR; // = new wa2();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ na2 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public ta2(String str, IBinder iBinder, boolean z, boolean z2) {
        this.a = str;
        this.b = a(iBinder);
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public static na2 a(IBinder iBinder) {
        byte[] bArr;
        if (iBinder == null) {
            return null;
        }
        try {
            ab2 zzb = t82.a(iBinder).zzb();
            if (zzb == null) {
                bArr = null;
            } else {
                bArr = (byte[]) cb2.g(zzb);
            }
            if (bArr != null) {
                return new qa2(bArr);
            }
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
            return null;
        } catch (RemoteException e) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e);
            return null;
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a, false);
        na2 na2 = this.b;
        if (na2 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            na2 = null;
        } else {
            na2.asBinder();
        }
        k72.a(parcel, 2, (IBinder) na2, false);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public ta2(String str, na2 na2, boolean z, boolean z2) {
        this.a = str;
        this.b = na2;
        this.c = z;
        this.d = z2;
    }
}
