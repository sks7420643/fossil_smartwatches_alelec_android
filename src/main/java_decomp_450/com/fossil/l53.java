package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l53 implements Parcelable.Creator<LocationAvailability> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        u53[] u53Arr = null;
        int i = 1000;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i2 = j72.q(parcel, a);
            } else if (a2 == 2) {
                i3 = j72.q(parcel, a);
            } else if (a2 == 3) {
                j = j72.s(parcel, a);
            } else if (a2 == 4) {
                i = j72.q(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                u53Arr = (u53[]) j72.b(parcel, a, u53.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new LocationAvailability(i, i2, i3, j, u53Arr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationAvailability[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
