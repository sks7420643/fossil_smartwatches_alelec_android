package com.fossil;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tx3<E> extends zx3<E> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ vx3<?> collection;

        @DexIgnore
        public a(vx3<?> vx3) {
            this.collection = vx3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.collection.asList();
        }
    }

    @DexIgnore
    @Override // com.fossil.zx3
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.zx3
    public boolean contains(Object obj) {
        return delegateCollection().contains(obj);
    }

    @DexIgnore
    public abstract vx3<E> delegateCollection();

    @DexIgnore
    public boolean isEmpty() {
        return delegateCollection().isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return delegateCollection().isPartialView();
    }

    @DexIgnore
    public int size() {
        return delegateCollection().size();
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.zx3
    public Object writeReplace() {
        return new a(delegateCollection());
    }
}
