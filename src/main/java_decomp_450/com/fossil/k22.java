package com.fossil;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k22 extends a52 {
    @DexIgnore
    public /* final */ o4<p12<?>> f; // = new o4<>();
    @DexIgnore
    public u12 g;

    @DexIgnore
    public k22(x12 x12) {
        super(x12);
        ((LifecycleCallback) this).a.a("ConnectionlessLifecycleHelper", this);
    }

    @DexIgnore
    public static void a(Activity activity, u12 u12, p12<?> p12) {
        x12 a = LifecycleCallback.a(activity);
        k22 k22 = (k22) a.a("ConnectionlessLifecycleHelper", k22.class);
        if (k22 == null) {
            k22 = new k22(a);
        }
        k22.g = u12;
        a72.a(p12, "ApiKey cannot be null");
        k22.f.add(p12);
        u12.a(k22);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void c() {
        super.c();
        i();
    }

    @DexIgnore
    @Override // com.fossil.a52, com.google.android.gms.common.api.internal.LifecycleCallback
    public void d() {
        super.d();
        i();
    }

    @DexIgnore
    @Override // com.fossil.a52, com.google.android.gms.common.api.internal.LifecycleCallback
    public void e() {
        super.e();
        this.g.b(this);
    }

    @DexIgnore
    @Override // com.fossil.a52
    public final void f() {
        this.g.c();
    }

    @DexIgnore
    public final o4<p12<?>> h() {
        return this.f;
    }

    @DexIgnore
    public final void i() {
        if (!this.f.isEmpty()) {
            this.g.a(this);
        }
    }

    @DexIgnore
    @Override // com.fossil.a52
    public final void a(i02 i02, int i) {
        this.g.a(i02, i);
    }
}
