package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.util.Log;
import com.google.android.datatransport.runtime.backends.TransportBackendDiscovery;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hv1 implements bv1 {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ fv1 b;
    @DexIgnore
    public /* final */ Map<String, jv1> c;

    @DexIgnore
    public hv1(Context context, fv1 fv1) {
        this(new a(context), fv1);
    }

    @DexIgnore
    @Override // com.fossil.bv1
    public synchronized jv1 a(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        av1 a2 = this.a.a(str);
        if (a2 == null) {
            return null;
        }
        jv1 create = a2.create(this.b.a(str));
        this.c.put(str, create);
        return create;
    }

    @DexIgnore
    public hv1(a aVar, fv1 fv1) {
        this.c = new HashMap();
        this.a = aVar;
        this.b = fv1;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public Map<String, String> b; // = null;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public static Bundle b(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("BackendRegistry", "Context has no PackageManager.");
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, TransportBackendDiscovery.class), 128);
                if (serviceInfo != null) {
                    return serviceInfo.metaData;
                }
                Log.w("BackendRegistry", "TransportBackendDiscovery has no service info.");
                return null;
            } catch (PackageManager.NameNotFoundException unused) {
                Log.w("BackendRegistry", "Application info not found.");
                return null;
            }
        }

        @DexIgnore
        public av1 a(String str) {
            String str2 = a().get(str);
            if (str2 == null) {
                return null;
            }
            try {
                return (av1) Class.forName(str2).asSubclass(av1.class).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException e) {
                Log.w("BackendRegistry", String.format("Class %s is not found.", str2), e);
                return null;
            } catch (IllegalAccessException e2) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e2);
                return null;
            } catch (InstantiationException e3) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s.", str2), e3);
                return null;
            } catch (NoSuchMethodException e4) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e4);
                return null;
            } catch (InvocationTargetException e5) {
                Log.w("BackendRegistry", String.format("Could not instantiate %s", str2), e5);
                return null;
            }
        }

        @DexIgnore
        public final Map<String, String> a() {
            if (this.b == null) {
                this.b = a(this.a);
            }
            return this.b;
        }

        @DexIgnore
        public final Map<String, String> a(Context context) {
            Bundle b2 = b(context);
            if (b2 == null) {
                Log.w("BackendRegistry", "Could not retrieve metadata, returning empty list of transport backends.");
                return Collections.emptyMap();
            }
            HashMap hashMap = new HashMap();
            for (String str : b2.keySet()) {
                Object obj = b2.get(str);
                if ((obj instanceof String) && str.startsWith("backend:")) {
                    for (String str2 : ((String) obj).split(",", -1)) {
                        String trim = str2.trim();
                        if (!trim.isEmpty()) {
                            hashMap.put(trim, str.substring(8));
                        }
                    }
                }
            }
            return hashMap;
        }
    }
}
