package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.y62;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lc2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lc2> CREATOR; // = new oc2();
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ uc2 g;
    @DexIgnore
    public /* final */ Long h;

    @DexIgnore
    public lc2(long j, long j2, String str, String str2, String str3, int i, uc2 uc2, Long l) {
        this.a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = i;
        this.g = uc2;
        this.h = l;
    }

    @DexIgnore
    public long a(TimeUnit timeUnit) {
        return timeUnit.convert(this.b, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public long b(TimeUnit timeUnit) {
        return timeUnit.convert(this.a, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof lc2)) {
            return false;
        }
        lc2 lc2 = (lc2) obj;
        return this.a == lc2.a && this.b == lc2.b && y62.a(this.c, lc2.c) && y62.a(this.d, lc2.d) && y62.a(this.e, lc2.e) && y62.a(this.g, lc2.g) && this.f == lc2.f;
    }

    @DexIgnore
    public String g() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(Long.valueOf(this.a), Long.valueOf(this.b), this.d);
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a(SampleRaw.COLUMN_START_TIME, Long.valueOf(this.a));
        a2.a(SampleRaw.COLUMN_END_TIME, Long.valueOf(this.b));
        a2.a("name", this.c);
        a2.a("identifier", this.d);
        a2.a("description", this.e);
        a2.a(Constants.ACTIVITY, Integer.valueOf(this.f));
        a2.a("application", this.g);
        return a2.toString();
    }

    @DexIgnore
    public String v() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, v(), false);
        k72.a(parcel, 4, g(), false);
        k72.a(parcel, 5, e(), false);
        k72.a(parcel, 7, this.f);
        k72.a(parcel, 8, (Parcelable) this.g, i, false);
        k72.a(parcel, 9, this.h, false);
        k72.a(parcel, a2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public long a; // = 0;
        @DexIgnore
        public long b; // = 0;
        @DexIgnore
        public String c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public String e; // = "";
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public Long g;

        @DexIgnore
        public a a(long j, TimeUnit timeUnit) {
            a72.b(j >= 0, "End time should be positive.");
            this.b = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a b(long j, TimeUnit timeUnit) {
            a72.b(j > 0, "Start time should be positive.");
            this.a = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a c(String str) {
            a72.a(str != null && TextUtils.getTrimmedLength(str) > 0);
            this.d = str;
            return this;
        }

        @DexIgnore
        public a d(String str) {
            a72.a(str.length() <= 100, "Session name cannot exceed %d characters", 100);
            this.c = str;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.f = zj2.a(str);
            return this;
        }

        @DexIgnore
        public a b(String str) {
            a72.a(str.length() <= 1000, "Session description cannot exceed %d characters", 1000);
            this.e = str;
            return this;
        }

        @DexIgnore
        public lc2 a() {
            boolean z = true;
            a72.b(this.a > 0, "Start time should be specified.");
            long j = this.b;
            if (j != 0 && j <= this.a) {
                z = false;
            }
            a72.b(z, "End time should be later than start time.");
            if (this.d == null) {
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                long j2 = this.a;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 20);
                sb.append(str);
                sb.append(j2);
                this.d = sb.toString();
            }
            return new lc2(this);
        }
    }

    @DexIgnore
    public lc2(a aVar) {
        this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, null, aVar.g);
    }
}
