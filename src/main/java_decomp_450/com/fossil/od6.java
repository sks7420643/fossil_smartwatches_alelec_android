package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od6 implements MembersInjector<SleepOverviewFragment> {
    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, ld6 ld6) {
        sleepOverviewFragment.g = ld6;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, ce6 ce6) {
        sleepOverviewFragment.h = ce6;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, wd6 wd6) {
        sleepOverviewFragment.i = wd6;
    }
}
