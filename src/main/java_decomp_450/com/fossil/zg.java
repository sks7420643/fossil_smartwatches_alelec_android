package com.fossil;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zg {
    @DexIgnore
    public /* final */ RecyclerView.m a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Rect c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends zg {
        @DexIgnore
        public a(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int a() {
            return ((zg) this).a.r();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int b() {
            return ((zg) this).a.r() - ((zg) this).a.p();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int c(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((zg) this).a.g(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int d(View view) {
            return ((zg) this).a.f(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int e(View view) {
            ((zg) this).a.a(view, true, ((zg) this).c);
            return ((zg) this).c.right;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int f() {
            return ((zg) this).a.o();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int g() {
            return (((zg) this).a.r() - ((zg) this).a.o()) - ((zg) this).a.p();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public void a(int i) {
            ((zg) this).a.e(i);
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int b(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((zg) this).a.h(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int f(View view) {
            ((zg) this).a.a(view, true, ((zg) this).c);
            return ((zg) this).c.left;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int a(View view) {
            return ((zg) this).a.i(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).rightMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int c() {
            return ((zg) this).a.p();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int d() {
            return ((zg) this).a.s();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int e() {
            return ((zg) this).a.i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends zg {
        @DexIgnore
        public b(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int a() {
            return ((zg) this).a.h();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int b() {
            return ((zg) this).a.h() - ((zg) this).a.n();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int c(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((zg) this).a.h(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int d(View view) {
            return ((zg) this).a.j(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int e(View view) {
            ((zg) this).a.a(view, true, ((zg) this).c);
            return ((zg) this).c.bottom;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int f() {
            return ((zg) this).a.q();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int g() {
            return (((zg) this).a.h() - ((zg) this).a.q()) - ((zg) this).a.n();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public void a(int i) {
            ((zg) this).a.f(i);
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int b(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((zg) this).a.g(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int f(View view) {
            ((zg) this).a.a(view, true, ((zg) this).c);
            return ((zg) this).c.top;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int a(View view) {
            return ((zg) this).a.e(view) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin;
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int c() {
            return ((zg) this).a.n();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int d() {
            return ((zg) this).a.i();
        }

        @DexIgnore
        @Override // com.fossil.zg
        public int e() {
            return ((zg) this).a.s();
        }
    }

    @DexIgnore
    public /* synthetic */ zg(RecyclerView.m mVar, a aVar) {
        this(mVar);
    }

    @DexIgnore
    public static zg a(RecyclerView.m mVar, int i) {
        if (i == 0) {
            return a(mVar);
        }
        if (i == 1) {
            return b(mVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    @DexIgnore
    public static zg b(RecyclerView.m mVar) {
        return new b(mVar);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract int a(View view);

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract int b(View view);

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract int c(View view);

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract int d(View view);

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public abstract int e(View view);

    @DexIgnore
    public abstract int f();

    @DexIgnore
    public abstract int f(View view);

    @DexIgnore
    public abstract int g();

    @DexIgnore
    public int h() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return g() - this.b;
    }

    @DexIgnore
    public void i() {
        this.b = g();
    }

    @DexIgnore
    public zg(RecyclerView.m mVar) {
        this.b = RecyclerView.UNDEFINED_DURATION;
        this.c = new Rect();
        this.a = mVar;
    }

    @DexIgnore
    public static zg a(RecyclerView.m mVar) {
        return new a(mVar);
    }
}
