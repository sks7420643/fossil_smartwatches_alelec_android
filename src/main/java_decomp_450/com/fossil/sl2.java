package com.fossil;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl2 extends g63 {
    @DexIgnore
    public /* final */ y12<c53> a;

    @DexIgnore
    public sl2(y12<c53> y12) {
        this.a = y12;
    }

    @DexIgnore
    public final synchronized void E() {
        this.a.a();
    }

    @DexIgnore
    @Override // com.fossil.f63
    public final void a(LocationAvailability locationAvailability) {
        this.a.a(new ul2(this, locationAvailability));
    }

    @DexIgnore
    @Override // com.fossil.f63
    public final void a(LocationResult locationResult) {
        this.a.a(new tl2(this, locationResult));
    }
}
