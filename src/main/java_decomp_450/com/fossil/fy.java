package com.fossil;

import com.fossil.gw;
import com.fossil.gy;
import com.fossil.m00;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy<Transcode> {
    @DexIgnore
    public /* final */ List<m00.a<?>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<yw> b; // = new ArrayList();
    @DexIgnore
    public cw c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public gy.e h;
    @DexIgnore
    public ax i;
    @DexIgnore
    public Map<Class<?>, ex<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public yw n;
    @DexIgnore
    public ew o;
    @DexIgnore
    public iy p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.lang.Class<R> */
    /* JADX WARN: Multi-variable type inference failed */
    public <R> void a(cw cwVar, Object obj, yw ywVar, int i2, int i3, iy iyVar, Class<?> cls, Class<R> cls2, ew ewVar, ax axVar, Map<Class<?>, ex<?>> map, boolean z, boolean z2, gy.e eVar) {
        this.c = cwVar;
        this.d = obj;
        this.n = ywVar;
        this.e = i2;
        this.f = i3;
        this.p = iyVar;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = ewVar;
        this.i = axVar;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public az b() {
        return this.c.a();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    @DexIgnore
    public nz d() {
        return this.h.a();
    }

    @DexIgnore
    public iy e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<m00.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List a2 = this.c.f().a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                m00.a<?> a3 = ((m00) a2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a3 != null) {
                    this.a.add(a3);
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public Class<?> h() {
        return this.d.getClass();
    }

    @DexIgnore
    public ax i() {
        return this.i;
    }

    @DexIgnore
    public ew j() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> k() {
        return this.c.f().c(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public yw l() {
        return this.n;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.Class<Transcode>, java.lang.Class<?> */
    public Class<?> m() {
        return (Class<Transcode>) this.k;
    }

    @DexIgnore
    public int n() {
        return this.e;
    }

    @DexIgnore
    public boolean o() {
        return this.r;
    }

    @DexIgnore
    public <Z> ex<Z> b(Class<Z> cls) {
        ex<Z> exVar;
        ex<Z> exVar2 = (ex<Z>) this.j.get(cls);
        if (exVar2 == null) {
            Iterator<Map.Entry<Class<?>, ex<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<Class<?>, ex<?>> next = it.next();
                if (next.getKey().isAssignableFrom(cls)) {
                    exVar2 = (ex<Z>) next.getValue();
                    break;
                }
            }
        }
        if (exVar != null) {
            return exVar;
        }
        if (!this.j.isEmpty() || !this.q) {
            return f10.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public List<yw> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<m00.a<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                m00.a<?> aVar = g2.get(i2);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public boolean b(uy<?> uyVar) {
        return this.c.f().b(uyVar);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public <Data> sy<Data, ?, Transcode> a(Class<Data> cls) {
        return this.c.f().b(cls, this.g, this.k);
    }

    @DexIgnore
    public <Z> dx<Z> a(uy<Z> uyVar) {
        return this.c.f().a((uy) uyVar);
    }

    @DexIgnore
    public List<m00<File, ?>> a(File file) throws gw.c {
        return this.c.f().a(file);
    }

    @DexIgnore
    public boolean a(yw ywVar) {
        List<m00.a<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).a.equals(ywVar)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public <X> vw<X> a(X x) throws gw.e {
        return this.c.f().c(x);
    }
}
