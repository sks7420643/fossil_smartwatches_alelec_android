package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx3<E> extends qx3<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Queue<E> delegate;
    @DexIgnore
    public /* final */ int maxSize;

    @DexIgnore
    public jx3(int i) {
        jw3.a(i >= 0, "maxSize (%s) must >= 0", i);
        this.delegate = new ArrayDeque(i);
        this.maxSize = i;
    }

    @DexIgnore
    public static <E> jx3<E> create(int i) {
        return new jx3<>(i);
    }

    @DexIgnore
    @Override // java.util.Collection, com.fossil.mx3, java.util.Queue
    @CanIgnoreReturnValue
    public boolean add(E e) {
        jw3.a(e);
        if (this.maxSize == 0) {
            return true;
        }
        if (size() == this.maxSize) {
            this.delegate.remove();
        }
        this.delegate.add(e);
        return true;
    }

    @DexIgnore
    @Override // java.util.Collection, com.fossil.mx3
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        int size = collection.size();
        if (size < this.maxSize) {
            return standardAddAll(collection);
        }
        clear();
        return py3.a((Collection) this, py3.a(collection, size - this.maxSize));
    }

    @DexIgnore
    @Override // com.fossil.mx3
    public boolean contains(Object obj) {
        Queue<E> delegate2 = delegate();
        jw3.a(obj);
        return delegate2.contains(obj);
    }

    @DexIgnore
    @Override // java.util.Queue, com.fossil.qx3
    @CanIgnoreReturnValue
    public boolean offer(E e) {
        return add(e);
    }

    @DexIgnore
    public int remainingCapacity() {
        return this.maxSize - size();
    }

    @DexIgnore
    @Override // com.fossil.mx3
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        Queue<E> delegate2 = delegate();
        jw3.a(obj);
        return delegate2.remove(obj);
    }

    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3, com.fossil.qx3, com.fossil.qx3, com.fossil.qx3
    public Queue<E> delegate() {
        return this.delegate;
    }
}
