package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st2<E> extends ws2<E> {
    @DexIgnore
    public static /* final */ st2<Object> zza; // = new st2<>(new Object[0], 0, null, 0, 0);
    @DexIgnore
    public /* final */ transient Object[] c;
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public /* final */ transient int g;

    @DexIgnore
    public st2(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.c = objArr;
        this.d = objArr2;
        this.e = i2;
        this.f = i;
        this.g = i3;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean contains(@NullableDecl Object obj) {
        Object[] objArr = this.d;
        if (obj == null || objArr == null) {
            return false;
        }
        int a = ms2.a(obj);
        while (true) {
            int i = a & this.e;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            a = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final int hashCode() {
        return this.f;
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<E> iterator() {
        return (yt2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final os2<E> zzd() {
        return os2.zza(this.c, this.g);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final Object[] zze() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzg() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.c, 0, objArr, i, this.g);
        return i + this.g;
    }
}
