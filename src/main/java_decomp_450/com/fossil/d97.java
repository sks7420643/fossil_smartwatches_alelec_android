package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d97 implements Collection<c97>, ye7 {
    @DexIgnore
    public /* final */ long[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xa7 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ long[] b;

        @DexIgnore
        public a(long[] jArr) {
            ee7.b(jArr, "array");
            this.b = jArr;
        }

        @DexIgnore
        @Override // com.fossil.xa7
        public long a() {
            int i = this.a;
            long[] jArr = this.b;
            if (i < jArr.length) {
                this.a = i + 1;
                long j = jArr[i];
                c97.c(j);
                return j;
            }
            throw new NoSuchElementException(String.valueOf(this.a));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(long[] jArr, Object obj) {
        return (obj instanceof d97) && ee7.a(jArr, ((d97) obj).b());
    }

    @DexIgnore
    public static int b(long[] jArr) {
        if (jArr != null) {
            return Arrays.hashCode(jArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(long[] jArr) {
        return jArr.length == 0;
    }

    @DexIgnore
    public static xa7 d(long[] jArr) {
        return new a(jArr);
    }

    @DexIgnore
    public static String e(long[] jArr) {
        return "ULongArray(storage=" + Arrays.toString(jArr) + ")";
    }

    @DexIgnore
    public boolean a(long j) {
        return a(this.a, j);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(c97 c97) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends c97> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ long[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof c97) {
            return a(((c97) obj).a());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<c97>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.xa7' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<c97> iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(long[] jArr) {
        return jArr.length;
    }

    @DexIgnore
    public static boolean a(long[] jArr, long j) {
        return t97.a(jArr, j);
    }

    @DexIgnore
    public static boolean a(long[] jArr, Collection<c97> collection) {
        boolean z;
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof c97) || !t97.a(jArr, t.a())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }
}
