package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sa7 extends ra7 {
    @DexIgnore
    public static final <T> Set<T> a() {
        return ia7.INSTANCE;
    }

    @DexIgnore
    public static final <T> LinkedHashSet<T> a(T... tArr) {
        ee7.b(tArr, MessengerShareContentUtility.ELEMENTS);
        LinkedHashSet<T> linkedHashSet = new LinkedHashSet<>(na7.a(tArr.length));
        t97.b((Object[]) tArr, (Collection) linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Set<T> a(Set<? extends T> set) {
        ee7.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return set;
        }
        return ra7.a(set.iterator().next());
    }
}
