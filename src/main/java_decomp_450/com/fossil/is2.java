package com.fossil;

import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class is2 extends zr2<K, V> {
    @DexIgnore
    @NullableDecl
    public /* final */ K a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ /* synthetic */ ds2 c;

    @DexIgnore
    public is2(ds2 ds2, int i) {
        this.c = ds2;
        this.a = (K) ds2.zzb[i];
        this.b = i;
    }

    @DexIgnore
    public final void a() {
        int i = this.b;
        if (i == -1 || i >= this.c.size() || !mr2.a(this.a, this.c.zzb[this.b])) {
            this.b = this.c.b(this.a);
        }
    }

    @DexIgnore
    @Override // java.util.Map.Entry, com.fossil.zr2
    @NullableDecl
    public final K getKey() {
        return this.a;
    }

    @DexIgnore
    @Override // java.util.Map.Entry, com.fossil.zr2
    @NullableDecl
    public final V getValue() {
        Map zzb = this.c.zzb();
        if (zzb != null) {
            return (V) zzb.get(this.a);
        }
        a();
        int i = this.b;
        if (i == -1) {
            return null;
        }
        return (V) this.c.zzc[i];
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        Map zzb = this.c.zzb();
        if (zzb != null) {
            return (V) zzb.put(this.a, v);
        }
        a();
        int i = this.b;
        if (i == -1) {
            this.c.put(this.a, v);
            return null;
        }
        Object[] objArr = this.c.zzc;
        V v2 = (V) objArr[i];
        objArr[i] = v;
        return v2;
    }
}
