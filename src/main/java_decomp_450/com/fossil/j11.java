package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Base64;
import com.misfit.frameworks.common.constants.Constants;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j11 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ b31 a;
    @DexIgnore
    public /* final */ /* synthetic */ File b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList d;

    @DexIgnore
    public j11(b31 b31, File file, int i, ArrayList arrayList) {
        this.a = b31;
        this.b = file;
        this.c = i;
        this.d = arrayList;
    }

    @DexIgnore
    public final void run() {
        Boolean bool;
        byte[] bArr;
        Context a2 = u31.g.a();
        ActivityManager activityManager = (ActivityManager) (a2 != null ? a2.getSystemService(Constants.ACTIVITY) : null);
        if (activityManager != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);
            bool = Boolean.valueOf(memoryInfo.lowMemory);
        } else {
            bool = null;
        }
        if (ee7.a((Object) bool, (Object) false)) {
            File file = this.b;
            try {
                DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
                bArr = new byte[((int) file.length())];
                try {
                    dataInputStream.readFully(bArr);
                } catch (IOException e) {
                    wl0.h.a(e);
                    bArr = null;
                } catch (Throwable th) {
                    try {
                        dataInputStream.close();
                    } catch (IOException unused) {
                    }
                    throw th;
                }
                try {
                    dataInputStream.close();
                } catch (IOException unused2) {
                }
            } catch (FileNotFoundException e2) {
                wl0.h.a(e2);
                bArr = null;
            }
            if (bArr != null) {
                ak1 ak1 = ak1.h;
                ci1 ci1 = ci1.j;
                b31 b31 = this.a;
                ak1.a(new wr1("gps_log", ci1, ((zk0) b31).w.u, yz0.a(((zk0) b31).y), ((zk0) this.a).z, true, null, null, null, yz0.a(new JSONObject(), r51.W2, Base64.encodeToString(bArr, 2)), 448));
            }
        }
        this.b.delete();
        if (this.c == this.d.size() - 1) {
            xp1.a(ak1.h, 0, 1, null);
        }
    }
}
