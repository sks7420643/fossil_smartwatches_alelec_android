package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz2 implements qz2 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.sdk.collection.validate_param_names_alphabetical", true);

    @DexIgnore
    @Override // com.fossil.qz2
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
