package com.fossil;

import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yi5<T> extends zi5<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public yi5(int i, ServerError serverError, Throwable th, String str, T t) {
        super(null);
        this.a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
        this.e = t;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yi5)) {
            return false;
        }
        yi5 yi5 = (yi5) obj;
        return this.a == yi5.a && ee7.a(this.b, yi5.b) && ee7.a(this.c, yi5.c) && ee7.a(this.d, yi5.d) && ee7.a(this.e, yi5.e);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a * 31;
        ServerError serverError = this.b;
        int i2 = 0;
        int hashCode = (i + (serverError != null ? serverError.hashCode() : 0)) * 31;
        Throwable th = this.c;
        int hashCode2 = (hashCode + (th != null ? th.hashCode() : 0)) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        T t = this.e;
        if (t != null) {
            i2 = t.hashCode();
        }
        return hashCode3 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ", data=" + ((Object) this.e) + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ yi5(int i, ServerError serverError, Throwable th, String str, Object obj, int i2, zd7 zd7) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str, (i2 & 16) != 0 ? null : obj);
    }
}
