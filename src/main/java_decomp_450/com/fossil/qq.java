package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq {
    @DexIgnore
    public static /* final */ int accessibility_action_clickable_span; // = 2131361817;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_0; // = 2131361818;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_1; // = 2131361819;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_10; // = 2131361820;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_11; // = 2131361821;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_12; // = 2131361822;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_13; // = 2131361823;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_14; // = 2131361824;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_15; // = 2131361825;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_16; // = 2131361826;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_17; // = 2131361827;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_18; // = 2131361828;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_19; // = 2131361829;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_2; // = 2131361830;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_20; // = 2131361831;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_21; // = 2131361832;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_22; // = 2131361833;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_23; // = 2131361834;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_24; // = 2131361835;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_25; // = 2131361836;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_26; // = 2131361837;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_27; // = 2131361838;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_28; // = 2131361839;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_29; // = 2131361840;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_3; // = 2131361841;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_30; // = 2131361842;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_31; // = 2131361843;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_4; // = 2131361844;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_5; // = 2131361845;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_6; // = 2131361846;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_7; // = 2131361847;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_8; // = 2131361848;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_9; // = 2131361849;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362019;
    @DexIgnore
    public static /* final */ int coil_bitmap; // = 2131362126;
    @DexIgnore
    public static /* final */ int coil_request_manager; // = 2131362127;
    @DexIgnore
    public static /* final */ int dialog_button; // = 2131362196;
    @DexIgnore
    public static /* final */ int forever; // = 2131362303;
    @DexIgnore
    public static /* final */ int icon; // = 2131362579;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362580;
    @DexIgnore
    public static /* final */ int info; // = 2131362603;
    @DexIgnore
    public static /* final */ int italic; // = 2131362619;
    @DexIgnore
    public static /* final */ int line1; // = 2131362752;
    @DexIgnore
    public static /* final */ int line3; // = 2131362754;
    @DexIgnore
    public static /* final */ int normal; // = 2131362844;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362845;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362846;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362847;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362949;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362950;
    @DexIgnore
    public static /* final */ int tag_accessibility_actions; // = 2131363119;
    @DexIgnore
    public static /* final */ int tag_accessibility_clickable_spans; // = 2131363120;
    @DexIgnore
    public static /* final */ int tag_accessibility_heading; // = 2131363121;
    @DexIgnore
    public static /* final */ int tag_accessibility_pane_title; // = 2131363122;
    @DexIgnore
    public static /* final */ int tag_screen_reader_focusable; // = 2131363123;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363124;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363125;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363126;
    @DexIgnore
    public static /* final */ int text; // = 2131363132;
    @DexIgnore
    public static /* final */ int text2; // = 2131363133;
    @DexIgnore
    public static /* final */ int time; // = 2131363154;
    @DexIgnore
    public static /* final */ int title; // = 2131363156;
}
