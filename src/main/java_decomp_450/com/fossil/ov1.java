package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ov1 implements Runnable {
    @DexIgnore
    public /* final */ qv1 a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ ht1 c;
    @DexIgnore
    public /* final */ ku1 d;

    @DexIgnore
    public ov1(qv1 qv1, pu1 pu1, ht1 ht1, ku1 ku1) {
        this.a = qv1;
        this.b = pu1;
        this.c = ht1;
        this.d = ku1;
    }

    @DexIgnore
    public static Runnable a(qv1 qv1, pu1 pu1, ht1 ht1, ku1 ku1) {
        return new ov1(qv1, pu1, ht1, ku1);
    }

    @DexIgnore
    public void run() {
        qv1.a(this.a, this.b, this.c, this.d);
    }
}
