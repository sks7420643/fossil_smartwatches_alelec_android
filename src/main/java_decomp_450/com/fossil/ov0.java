package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ ut0 CREATOR; // = new ut0(null);
    @DexIgnore
    public /* final */ yr0 a;
    @DexIgnore
    public /* final */ short b;

    @DexIgnore
    public ov0(yr0 yr0, short s) {
        this.a = yr0;
        this.b = s;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.t0, yz0.a(this.a)), r51.y0, yz0.a(this.b));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ov0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ov0 ov0 = (ov0) obj;
            return this.a == ov0.a && this.b == ov0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame");
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
