package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ zo4 b;
    @DexIgnore
    public /* final */ ap4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRepository", f = "NotificationRepository.kt", l = {20}, m = "fetchNotifications")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bp4 bp4, fb7 fb7) {
            super(fb7);
            this.this$0 = bp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore
    public bp4(zo4 zo4, ap4 ap4) {
        ee7.b(zo4, "local");
        ee7.b(ap4, "remote");
        this.b = zo4;
        this.c = ap4;
        String name = bp4.class.getName();
        ee7.a((Object) name, "NotificationRepository::class.java.name");
        this.a = name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.ao4>>> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.bp4.a
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.bp4$a r0 = (com.fossil.bp4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.bp4$a r0 = new com.fossil.bp4$a
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0036
            if (r2 != r3) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.bp4 r0 = (com.fossil.bp4) r0
            com.fossil.t87.a(r8)
            goto L_0x0048
        L_0x002e:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0036:
            com.fossil.t87.a(r8)
            com.fossil.ap4 r8 = r7.c
            r2 = 0
            r0.L$0 = r7
            r0.label = r3
            java.lang.Object r8 = com.fossil.ap4.a(r8, r2, r0, r3, r4)
            if (r8 != r1) goto L_0x0047
            return r1
        L_0x0047:
            r0 = r7
        L_0x0048:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x009d
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r1 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            if (r1 == 0) goto L_0x008e
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "notification: "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r2.e(r3, r5)
            boolean r8 = r8.b()
            if (r8 != 0) goto L_0x0083
            com.fossil.zo4 r8 = r0.b
            java.util.List r0 = r1.get_items()
            r8.a(r0)
        L_0x0083:
            com.fossil.ko4 r8 = new com.fossil.ko4
            java.util.List r0 = r1.get_items()
            r1 = 2
            r8.<init>(r0, r4, r1, r4)
            goto L_0x00bc
        L_0x008e:
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = "response is null"
            r0.<init>(r1, r2)
            r8.<init>(r0)
            goto L_0x00bc
        L_0x009d:
            boolean r0 = r8 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x00bd
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r2 = r8.a()
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00b5
            java.lang.String r4 = r8.getMessage()
        L_0x00b5:
            r1.<init>(r2, r4)
            r0.<init>(r1)
            r8 = r0
        L_0x00bc:
            return r8
        L_0x00bd:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bp4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<ao4>> b() {
        return this.b.b();
    }

    @DexIgnore
    public final void a() {
        this.b.a();
    }
}
