package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.hv3;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sv3 extends tv3 {
    @DexIgnore
    public static /* final */ boolean o; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.e e; // = new b(((tv3) this).a);
    @DexIgnore
    public /* final */ TextInputLayout.f f; // = new c();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public long i; // = Long.MAX_VALUE;
    @DexIgnore
    public StateListDrawable j;
    @DexIgnore
    public dv3 k;
    @DexIgnore
    public AccessibilityManager l;
    @DexIgnore
    public ValueAnimator m;
    @DexIgnore
    public ValueAnimator n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sv3$a$a")
        /* renamed from: com.fossil.sv3$a$a  reason: collision with other inner class name */
        public class RunnableC0179a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ AutoCompleteTextView a;

            @DexIgnore
            public RunnableC0179a(AutoCompleteTextView autoCompleteTextView) {
                this.a = autoCompleteTextView;
            }

            @DexIgnore
            public void run() {
                boolean isPopupShowing = this.a.isPopupShowing();
                sv3.this.a(isPopupShowing);
                boolean unused = sv3.this.g = isPopupShowing;
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            sv3 sv3 = sv3.this;
            AutoCompleteTextView a2 = sv3.a(((tv3) sv3).a.getEditText());
            a2.post(new RunnableC0179a(a2));
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TextInputLayout.e {
        @DexIgnore
        public b(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        @DexIgnore
        @Override // com.fossil.g9, com.google.android.material.textfield.TextInputLayout.e
        public void a(View view, oa oaVar) {
            super.a(view, oaVar);
            oaVar.a((CharSequence) Spinner.class.getName());
            if (oaVar.x()) {
                oaVar.d((CharSequence) null);
            }
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void c(View view, AccessibilityEvent accessibilityEvent) {
            super.c(view, accessibilityEvent);
            sv3 sv3 = sv3.this;
            AutoCompleteTextView a = sv3.a(((tv3) sv3).a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && sv3.this.l.isTouchExplorationEnabled()) {
                sv3.this.d(a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements TextInputLayout.f {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView a2 = sv3.this.a(textInputLayout.getEditText());
            sv3.this.b(a2);
            sv3.this.a(a2);
            sv3.this.c(a2);
            a2.setThreshold(0);
            a2.removeTextChangedListener(sv3.this.d);
            a2.addTextChangedListener(sv3.this.d);
            textInputLayout.setErrorIconDrawable((Drawable) null);
            textInputLayout.setTextInputAccessibilityDelegate(sv3.this.e);
            textInputLayout.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            sv3.this.d((AutoCompleteTextView) ((tv3) sv3.this).a.getEditText());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ AutoCompleteTextView a;

        @DexIgnore
        public e(AutoCompleteTextView autoCompleteTextView) {
            this.a = autoCompleteTextView;
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                if (sv3.this.d()) {
                    boolean unused = sv3.this.g = false;
                }
                sv3.this.d(this.a);
                view.performClick();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements View.OnFocusChangeListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            ((tv3) sv3.this).a.setEndIconActivated(z);
            if (!z) {
                sv3.this.a(false);
                boolean unused = sv3.this.g = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements AutoCompleteTextView.OnDismissListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onDismiss() {
            boolean unused = sv3.this.g = true;
            long unused2 = sv3.this.i = System.currentTimeMillis();
            sv3.this.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends AnimatorListenerAdapter {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            sv3 sv3 = sv3.this;
            ((tv3) sv3).c.setChecked(sv3.h);
            sv3.this.n.start();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ((tv3) sv3.this).c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore
    public sv3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.tv3
    public boolean a(int i2) {
        return i2 != 0;
    }

    @DexIgnore
    @Override // com.fossil.tv3
    public boolean b() {
        return true;
    }

    @DexIgnore
    public final void c(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnTouchListener(new e(autoCompleteTextView));
        autoCompleteTextView.setOnFocusChangeListener(new f());
        if (o) {
            autoCompleteTextView.setOnDismissListener(new g());
        }
    }

    @DexIgnore
    public final void d(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView != null) {
            if (d()) {
                this.g = false;
            }
            if (!this.g) {
                if (o) {
                    a(!this.h);
                } else {
                    this.h = !this.h;
                    ((tv3) this).c.toggle();
                }
                if (this.h) {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.showDropDown();
                    return;
                }
                autoCompleteTextView.dismissDropDown();
                return;
            }
            this.g = false;
        }
    }

    @DexIgnore
    public final void b(AutoCompleteTextView autoCompleteTextView) {
        if (o) {
            int boxBackgroundMode = ((tv3) this).a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.k);
            } else if (boxBackgroundMode == 1) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.j);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.tv3
    public void a() {
        float dimensionPixelOffset = (float) ((tv3) this).b.getResources().getDimensionPixelOffset(lr3.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = (float) ((tv3) this).b.getResources().getDimensionPixelOffset(lr3.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = ((tv3) this).b.getResources().getDimensionPixelOffset(lr3.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        dv3 a2 = a(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        dv3 a3 = a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.k = a2;
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.j = stateListDrawable;
        stateListDrawable.addState(new int[]{16842922}, a2);
        this.j.addState(new int[0], a3);
        ((tv3) this).a.setEndIconDrawable(t0.c(((tv3) this).b, o ? mr3.mtrl_dropdown_arrow : mr3.mtrl_ic_arrow_drop_down));
        TextInputLayout textInputLayout = ((tv3) this).a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(rr3.exposed_dropdown_menu_content_description));
        ((tv3) this).a.setEndIconOnClickListener(new d());
        ((tv3) this).a.a(this.f);
        c();
        da.h(((tv3) this).c, 2);
        this.l = (AccessibilityManager) ((tv3) this).b.getSystemService("accessibility");
    }

    @DexIgnore
    public final void c() {
        this.n = a(67, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        ValueAnimator a2 = a(50, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.m = a2;
        a2.addListener(new h());
    }

    @DexIgnore
    public final void b(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, dv3 dv3) {
        LayerDrawable layerDrawable;
        int a2 = vs3.a(autoCompleteTextView, jr3.colorSurface);
        dv3 dv32 = new dv3(dv3.n());
        int a3 = vs3.a(i2, a2, 0.1f);
        dv32.a(new ColorStateList(iArr, new int[]{a3, 0}));
        if (o) {
            dv32.setTint(a2);
            ColorStateList colorStateList = new ColorStateList(iArr, new int[]{a3, a2});
            dv3 dv33 = new dv3(dv3.n());
            dv33.setTint(-1);
            layerDrawable = new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, dv32, dv33), dv3});
        } else {
            layerDrawable = new LayerDrawable(new Drawable[]{dv32, dv3});
        }
        da.a(autoCompleteTextView, layerDrawable);
    }

    @DexIgnore
    public final boolean d() {
        long currentTimeMillis = System.currentTimeMillis() - this.i;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    @DexIgnore
    public final void a(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView.getKeyListener() == null) {
            int boxBackgroundMode = ((tv3) this).a.getBoxBackgroundMode();
            dv3 boxBackground = ((tv3) this).a.getBoxBackground();
            int a2 = vs3.a(autoCompleteTextView, jr3.colorControlHighlight);
            int[][] iArr = {new int[]{16842919}, new int[0]};
            if (boxBackgroundMode == 2) {
                b(autoCompleteTextView, a2, iArr, boxBackground);
            } else if (boxBackgroundMode == 1) {
                a(autoCompleteTextView, a2, iArr, boxBackground);
            }
        }
    }

    @DexIgnore
    public final void a(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, dv3 dv3) {
        int boxBackgroundColor = ((tv3) this).a.getBoxBackgroundColor();
        int[] iArr2 = {vs3.a(i2, boxBackgroundColor, 0.1f), boxBackgroundColor};
        if (o) {
            da.a(autoCompleteTextView, new RippleDrawable(new ColorStateList(iArr, iArr2), dv3, dv3));
            return;
        }
        dv3 dv32 = new dv3(dv3.n());
        dv32.a(new ColorStateList(iArr, iArr2));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{dv3, dv32});
        int u = da.u(autoCompleteTextView);
        int paddingTop = autoCompleteTextView.getPaddingTop();
        int t = da.t(autoCompleteTextView);
        int paddingBottom = autoCompleteTextView.getPaddingBottom();
        da.a(autoCompleteTextView, layerDrawable);
        da.b(autoCompleteTextView, u, paddingTop, t, paddingBottom);
    }

    @DexIgnore
    public final dv3 a(float f2, float f3, float f4, int i2) {
        hv3.b n2 = hv3.n();
        n2.d(f2);
        n2.e(f2);
        n2.b(f3);
        n2.c(f3);
        hv3 a2 = n2.a();
        dv3 a3 = dv3.a(((tv3) this).b, f4);
        a3.setShapeAppearanceModel(a2);
        a3.a(0, i2, 0, i2);
        return a3;
    }

    @DexIgnore
    public final AutoCompleteTextView a(EditText editText) {
        if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        }
        throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
    }

    @DexIgnore
    public final void a(boolean z) {
        if (this.h != z) {
            this.h = z;
            this.n.cancel();
            this.m.start();
        }
    }

    @DexIgnore
    public final ValueAnimator a(int i2, float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(ur3.a);
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new i());
        return ofFloat;
    }
}
