package com.fossil;

import com.fossil.fo7;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp7 implements ip7 {
    @DexIgnore
    public static /* final */ List<String> f; // = ro7.a("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority");
    @DexIgnore
    public static /* final */ List<String> g; // = ro7.a("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade");
    @DexIgnore
    public /* final */ Interceptor.Chain a;
    @DexIgnore
    public /* final */ fp7 b;
    @DexIgnore
    public /* final */ yp7 c;
    @DexIgnore
    public aq7 d;
    @DexIgnore
    public /* final */ jo7 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends dr7 {
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public long c; // = 0;

        @DexIgnore
        public a(sr7 sr7) {
            super(sr7);
        }

        @DexIgnore
        public final void a(IOException iOException) {
            if (!this.b) {
                this.b = true;
                xp7 xp7 = xp7.this;
                xp7.b.a(false, xp7, this.c, iOException);
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7, com.fossil.dr7
        public long b(yq7 yq7, long j) throws IOException {
            try {
                long b2 = a().b(yq7, j);
                if (b2 > 0) {
                    this.c += b2;
                }
                return b2;
            } catch (IOException e) {
                a(e);
                throw e;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, com.fossil.dr7, java.lang.AutoCloseable
        public void close() throws IOException {
            super.close();
            a(null);
        }
    }

    @DexIgnore
    public xp7(OkHttpClient okHttpClient, Interceptor.Chain chain, fp7 fp7, yp7 yp7) {
        jo7 jo7;
        this.a = chain;
        this.b = fp7;
        this.c = yp7;
        if (okHttpClient.x().contains(jo7.H2_PRIOR_KNOWLEDGE)) {
            jo7 = jo7.H2_PRIOR_KNOWLEDGE;
        } else {
            jo7 = jo7.HTTP_2;
        }
        this.e = jo7;
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public qr7 a(lo7 lo7, long j) {
        return this.d.d();
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void b() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void cancel() {
        aq7 aq7 = this.d;
        if (aq7 != null) {
            aq7.c(tp7.CANCEL);
        }
    }

    @DexIgnore
    public static List<up7> b(lo7 lo7) {
        fo7 c2 = lo7.c();
        ArrayList arrayList = new ArrayList(c2.b() + 4);
        arrayList.add(new up7(up7.f, lo7.e()));
        arrayList.add(new up7(up7.g, op7.a(lo7.g())));
        String a2 = lo7.a("Host");
        if (a2 != null) {
            arrayList.add(new up7(up7.i, a2));
        }
        arrayList.add(new up7(up7.h, lo7.g().n()));
        int b2 = c2.b();
        for (int i = 0; i < b2; i++) {
            br7 encodeUtf8 = br7.encodeUtf8(c2.a(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new up7(encodeUtf8, c2.b(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void a(lo7 lo7) throws IOException {
        if (this.d == null) {
            aq7 a2 = this.c.a(b(lo7), lo7.a() != null);
            this.d = a2;
            a2.h().a((long) this.a.a(), TimeUnit.MILLISECONDS);
            this.d.l().a((long) this.a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void a() throws IOException {
        this.d.d().close();
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public Response.a a(boolean z) throws IOException {
        Response.a a2 = a(this.d.j(), this.e);
        if (!z || po7.a.a(a2) != 100) {
            return a2;
        }
        return null;
    }

    @DexIgnore
    public static Response.a a(fo7 fo7, jo7 jo7) throws IOException {
        fo7.a aVar = new fo7.a();
        int b2 = fo7.b();
        qp7 qp7 = null;
        for (int i = 0; i < b2; i++) {
            String a2 = fo7.a(i);
            String b3 = fo7.b(i);
            if (a2.equals(":status")) {
                qp7 = qp7.a("HTTP/1.1 " + b3);
            } else if (!g.contains(a2)) {
                po7.a.a(aVar, a2, b3);
            }
        }
        if (qp7 != null) {
            Response.a aVar2 = new Response.a();
            aVar2.a(jo7);
            aVar2.a(qp7.b);
            aVar2.a(qp7.c);
            aVar2.a(aVar.a());
            return aVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public mo7 a(Response response) throws IOException {
        fp7 fp7 = this.b;
        fp7.f.e(fp7.e);
        return new np7(response.b("Content-Type"), kp7.a(response), ir7.a(new a(this.d.e())));
    }
}
