package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sq3> CREATOR; // = new tq3();
    @DexIgnore
    public byte a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public sq3(byte b2, byte b3, String str) {
        this.a = b2;
        this.b = b3;
        this.c = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || sq3.class != obj.getClass()) {
            return false;
        }
        sq3 sq3 = (sq3) obj;
        return this.a == sq3.a && this.b == sq3.b && this.c.equals(sq3.c);
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.a + 31) * 31) + this.b) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public final String toString() {
        byte b2 = this.a;
        byte b3 = this.b;
        String str = this.c;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 73);
        sb.append("AmsEntityUpdateParcelable{, mEntityId=");
        sb.append((int) b2);
        sb.append(", mAttributeId=");
        sb.append((int) b3);
        sb.append(", mValue='");
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        k72.a(parcel, 3, this.b);
        k72.a(parcel, 4, this.c, false);
        k72.a(parcel, a2);
    }
}
