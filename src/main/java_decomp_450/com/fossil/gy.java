package com.fossil;

import android.os.Build;
import android.util.Log;
import com.fossil.ey;
import com.fossil.gw;
import com.fossil.hy;
import com.fossil.w50;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class gy<R> implements ey.a, Runnable, Comparable<gy<?>>, w50.f {
    public Object A;
    public Thread B;
    public yw C;
    public yw D;
    public Object E;
    public sw F;
    public ix<?> G;
    public volatile ey H;
    public volatile boolean I;
    public volatile boolean J;
    public final fy<R> a = new fy<>();
    public final List<Throwable> b = new ArrayList();
    public final y50 c = y50.b();
    public final e d;
    public final b9<gy<?>> e;
    public final d<?> f = new d<>();
    public final f g = new f();
    public cw h;
    public yw i;
    public ew j;
    public my p;
    public int q;
    public int r;
    public iy s;
    public ax t;
    public b<R> u;
    public int v;
    public h w;
    public g x;
    public long y;
    public boolean z;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;
        public static final /* synthetic */ int[] c;

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0043 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0074 */
        /*
        static {
            /*
                com.fossil.uw[] r0 = com.fossil.uw.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.gy.a.c = r0
                r1 = 1
                com.fossil.uw r2 = com.fossil.uw.SOURCE     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.gy.a.c     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.uw r3 = com.fossil.uw.TRANSFORMED     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                com.fossil.gy$h[] r2 = com.fossil.gy.h.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.fossil.gy.a.b = r2
                com.fossil.gy$h r3 = com.fossil.gy.h.RESOURCE_CACHE     // Catch:{ NoSuchFieldError -> 0x002e }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x002e }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x002e }
            L_0x002e:
                int[] r2 = com.fossil.gy.a.b     // Catch:{ NoSuchFieldError -> 0x0038 }
                com.fossil.gy$h r3 = com.fossil.gy.h.DATA_CACHE     // Catch:{ NoSuchFieldError -> 0x0038 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0038 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0038 }
            L_0x0038:
                r2 = 3
                int[] r3 = com.fossil.gy.a.b     // Catch:{ NoSuchFieldError -> 0x0043 }
                com.fossil.gy$h r4 = com.fossil.gy.h.SOURCE     // Catch:{ NoSuchFieldError -> 0x0043 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0043 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0043 }
            L_0x0043:
                int[] r3 = com.fossil.gy.a.b     // Catch:{ NoSuchFieldError -> 0x004e }
                com.fossil.gy$h r4 = com.fossil.gy.h.FINISHED     // Catch:{ NoSuchFieldError -> 0x004e }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004e }
                r5 = 4
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x004e }
            L_0x004e:
                int[] r3 = com.fossil.gy.a.b     // Catch:{ NoSuchFieldError -> 0x0059 }
                com.fossil.gy$h r4 = com.fossil.gy.h.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x0059 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0059 }
                r5 = 5
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0059 }
            L_0x0059:
                com.fossil.gy$g[] r3 = com.fossil.gy.g.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.fossil.gy.a.a = r3
                com.fossil.gy$g r4 = com.fossil.gy.g.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x006a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x006a }
                r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x006a }
            L_0x006a:
                int[] r1 = com.fossil.gy.a.a     // Catch:{ NoSuchFieldError -> 0x0074 }
                com.fossil.gy$g r3 = com.fossil.gy.g.SWITCH_TO_SOURCE_SERVICE     // Catch:{ NoSuchFieldError -> 0x0074 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0074 }
                r1[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0074 }
            L_0x0074:
                int[] r0 = com.fossil.gy.a.a     // Catch:{ NoSuchFieldError -> 0x007e }
                com.fossil.gy$g r1 = com.fossil.gy.g.DECODE_DATA     // Catch:{ NoSuchFieldError -> 0x007e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007e }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007e }
            L_0x007e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gy.a.<clinit>():void");
        }
        */
    }

    public interface b<R> {
        void a(gy<?> gyVar);

        void a(py pyVar);

        void a(uy<R> uyVar, sw swVar);
    }

    public final class c<Z> implements hy.a<Z> {
        public final sw a;

        public c(sw swVar) {
            this.a = swVar;
        }

        @Override // com.fossil.hy.a
        public uy<Z> a(uy<Z> uyVar) {
            return gy.this.a(this.a, uyVar);
        }
    }

    public interface e {
        nz a();
    }

    public enum g {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    public enum h {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    public gy(e eVar, b9<gy<?>> b9Var) {
        this.d = eVar;
        this.e = b9Var;
    }

    public gy<R> a(cw cwVar, Object obj, my myVar, yw ywVar, int i2, int i3, Class<?> cls, Class<R> cls2, ew ewVar, iy iyVar, Map<Class<?>, ex<?>> map, boolean z2, boolean z3, boolean z4, ax axVar, b<R> bVar, int i4) {
        this.a.a(cwVar, obj, ywVar, i2, i3, iyVar, cls, cls2, ewVar, axVar, map, z2, z3, this.d);
        this.h = cwVar;
        this.i = ywVar;
        this.j = ewVar;
        this.p = myVar;
        this.q = i2;
        this.r = i3;
        this.s = iyVar;
        this.z = z4;
        this.t = axVar;
        this.u = bVar;
        this.v = i4;
        this.x = g.INITIALIZE;
        this.A = obj;
        return this;
    }

    @Override // com.fossil.ey.a
    public void b() {
        this.x = g.SWITCH_TO_SOURCE_SERVICE;
        this.u.a((gy<?>) this);
    }

    public void c() {
        this.J = true;
        ey eyVar = this.H;
        if (eyVar != null) {
            eyVar.cancel();
        }
    }

    public final void d() {
        if (Log.isLoggable("DecodeJob", 2)) {
            long j2 = this.y;
            a("Retrieved data", j2, "data: " + this.E + ", cache key: " + this.C + ", fetcher: " + this.G);
        }
        uy<R> uyVar = null;
        try {
            uyVar = a(this.G, this.E, this.F);
        } catch (py e2) {
            e2.setLoggingDetails(this.D, this.F);
            this.b.add(e2);
        }
        if (uyVar != null) {
            b(uyVar, this.F);
        } else {
            k();
        }
    }

    public final ey e() {
        int i2 = a.b[this.w.ordinal()];
        if (i2 == 1) {
            return new vy(this.a, this);
        }
        if (i2 == 2) {
            return new by(this.a, this);
        }
        if (i2 == 3) {
            return new yy(this.a, this);
        }
        if (i2 == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.w);
    }

    public final int f() {
        return this.j.ordinal();
    }

    public final void g() {
        m();
        this.u.a(new py("Failed to load resource", new ArrayList(this.b)));
        i();
    }

    public final void h() {
        if (this.g.a()) {
            j();
        }
    }

    public final void i() {
        if (this.g.b()) {
            j();
        }
    }

    public final void j() {
        this.g.c();
        this.f.a();
        this.a.a();
        this.I = false;
        this.h = null;
        this.i = null;
        this.t = null;
        this.j = null;
        this.p = null;
        this.u = null;
        this.w = null;
        this.H = null;
        this.B = null;
        this.C = null;
        this.E = null;
        this.F = null;
        this.G = null;
        this.y = 0;
        this.J = false;
        this.A = null;
        this.b.clear();
        this.e.a(this);
    }

    public final void k() {
        this.B = Thread.currentThread();
        this.y = q50.a();
        boolean z2 = false;
        while (!this.J && this.H != null && !(z2 = this.H.a())) {
            this.w = a(this.w);
            this.H = e();
            if (this.w == h.SOURCE) {
                b();
                return;
            }
        }
        if ((this.w == h.FINISHED || this.J) && !z2) {
            g();
        }
    }

    public final void l() {
        int i2 = a.a[this.x.ordinal()];
        if (i2 == 1) {
            this.w = a(h.INITIALIZE);
            this.H = e();
            k();
        } else if (i2 == 2) {
            k();
        } else if (i2 == 3) {
            d();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.x);
        }
    }

    public final void m() {
        Throwable th;
        this.c.a();
        if (this.I) {
            if (this.b.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.b;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.I = true;
    }

    public boolean n() {
        h a2 = a(h.INITIALIZE);
        return a2 == h.RESOURCE_CACHE || a2 == h.DATA_CACHE;
    }

    public void run() {
        x50.a("DecodeJob#run(model=%s)", this.A);
        ix<?> ixVar = this.G;
        try {
            if (this.J) {
                g();
                if (ixVar != null) {
                    ixVar.a();
                }
                x50.a();
                return;
            }
            l();
            if (ixVar != null) {
                ixVar.a();
            }
            x50.a();
        } catch (ay e2) {
            throw e2;
        } catch (Throwable th) {
            if (ixVar != null) {
                ixVar.a();
            }
            x50.a();
            throw th;
        }
    }

    public static class f {
        public boolean a;
        public boolean b;
        public boolean c;

        public synchronized boolean a() {
            this.b = true;
            return a(false);
        }

        public synchronized boolean b(boolean z) {
            this.a = true;
            return a(z);
        }

        public synchronized void c() {
            this.b = false;
            this.a = false;
            this.c = false;
        }

        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.a;
        }

        public synchronized boolean b() {
            this.c = true;
            return a(false);
        }
    }

    public static class d<Z> {
        public yw a;
        public dx<Z> b;
        public ty<Z> c;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.dx<X> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.ty<X> */
        /* JADX WARN: Multi-variable type inference failed */
        public <X> void a(yw ywVar, dx<X> dxVar, ty<X> tyVar) {
            this.a = ywVar;
            this.b = dxVar;
            this.c = tyVar;
        }

        public boolean b() {
            return this.c != null;
        }

        public void a(e eVar, ax axVar) {
            x50.a("DecodeJob.encode");
            try {
                eVar.a().a(this.a, new dy(this.b, this.c, axVar));
            } finally {
                this.c.f();
                x50.a();
            }
        }

        public void a() {
            this.a = null;
            this.b = null;
            this.c = null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.uy<R> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: com.fossil.ty */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: com.fossil.uy<R> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.ty */
    /* JADX DEBUG: Multi-variable search result rejected for r3v7, resolved type: com.fossil.uy<R> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.ty */
    /* JADX WARN: Multi-variable type inference failed */
    public final void b(uy<R> uyVar, sw swVar) {
        if (uyVar instanceof qy) {
            ((qy) uyVar).a();
        }
        ty tyVar = 0;
        if (this.f.b()) {
            uyVar = ty.b(uyVar);
            tyVar = uyVar;
        }
        a((uy) uyVar, swVar);
        this.w = h.ENCODE;
        try {
            if (this.f.b()) {
                this.f.a(this.d, this.t);
            }
            h();
        } finally {
            if (tyVar != 0) {
                tyVar.f();
            }
        }
    }

    public void a(boolean z2) {
        if (this.g.b(z2)) {
            j();
        }
    }

    /* renamed from: a */
    public int compareTo(gy<?> gyVar) {
        int f2 = f() - gyVar.f();
        return f2 == 0 ? this.v - gyVar.v : f2;
    }

    public final void a(uy<R> uyVar, sw swVar) {
        m();
        this.u.a(uyVar, swVar);
    }

    public final h a(h hVar) {
        int i2 = a.b[hVar.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                return this.z ? h.FINISHED : h.SOURCE;
            }
            if (i2 == 3 || i2 == 4) {
                return h.FINISHED;
            }
            if (i2 != 5) {
                throw new IllegalArgumentException("Unrecognized stage: " + hVar);
            } else if (this.s.b()) {
                return h.RESOURCE_CACHE;
            } else {
                return a(h.RESOURCE_CACHE);
            }
        } else if (this.s.a()) {
            return h.DATA_CACHE;
        } else {
            return a(h.DATA_CACHE);
        }
    }

    @Override // com.fossil.ey.a
    public void a(yw ywVar, Object obj, ix<?> ixVar, sw swVar, yw ywVar2) {
        this.C = ywVar;
        this.E = obj;
        this.G = ixVar;
        this.F = swVar;
        this.D = ywVar2;
        if (Thread.currentThread() != this.B) {
            this.x = g.DECODE_DATA;
            this.u.a((gy<?>) this);
            return;
        }
        x50.a("DecodeJob.decodeFromRetrievedData");
        try {
            d();
        } finally {
            x50.a();
        }
    }

    @Override // com.fossil.ey.a
    public void a(yw ywVar, Exception exc, ix<?> ixVar, sw swVar) {
        ixVar.a();
        py pyVar = new py("Fetching data failed", exc);
        pyVar.setLoggingDetails(ywVar, swVar, ixVar.getDataClass());
        this.b.add(pyVar);
        if (Thread.currentThread() != this.B) {
            this.x = g.SWITCH_TO_SOURCE_SERVICE;
            this.u.a((gy<?>) this);
            return;
        }
        k();
    }

    public final <Data> uy<R> a(ix<?> ixVar, Data data, sw swVar) throws py {
        if (data == null) {
            ixVar.a();
            return null;
        }
        try {
            long a2 = q50.a();
            uy<R> a3 = a((Object) data, swVar);
            if (Log.isLoggable("DecodeJob", 2)) {
                a("Decoded result " + a3, a2);
            }
            return a3;
        } finally {
            ixVar.a();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Class<?> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.fossil.sy<Data, ?, R>, com.fossil.sy<Data, ResourceType, R> */
    public final <Data> uy<R> a(Data data, sw swVar) throws py {
        return a(data, swVar, (sy<Data, ?, R>) this.a.a((Class) data.getClass()));
    }

    public final ax a(sw swVar) {
        ax axVar = this.t;
        if (Build.VERSION.SDK_INT < 26) {
            return axVar;
        }
        boolean z2 = swVar == sw.RESOURCE_DISK_CACHE || this.a.o();
        Boolean bool = (Boolean) axVar.a(s10.i);
        if (bool != null && (!bool.booleanValue() || z2)) {
            return axVar;
        }
        ax axVar2 = new ax();
        axVar2.a(this.t);
        axVar2.a(s10.i, Boolean.valueOf(z2));
        return axVar2;
    }

    public final <Data, ResourceType> uy<R> a(Data data, sw swVar, sy<Data, ResourceType, R> syVar) throws py {
        ax a2 = a(swVar);
        jx<Data> b2 = this.h.f().b((Object) data);
        try {
            return syVar.a(b2, a2, this.q, this.r, new c(swVar));
        } finally {
            b2.a();
        }
    }

    public final void a(String str, long j2) {
        a(str, j2, (String) null);
    }

    public final void a(String str, long j2, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(q50.a(j2));
        sb.append(", load key: ");
        sb.append(this.p);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    @Override // com.fossil.w50.f
    public y50 a() {
        return this.c;
    }

    public <Z> uy<Z> a(sw swVar, uy<Z> uyVar) {
        ex<Z> exVar;
        uy<Z> uyVar2;
        uw uwVar;
        yw ywVar;
        Class<?> cls = uyVar.get().getClass();
        dx dxVar = null;
        if (swVar != sw.RESOURCE_DISK_CACHE) {
            ex<Z> b2 = this.a.b(cls);
            exVar = b2;
            uyVar2 = b2.a(this.h, uyVar, this.q, this.r);
        } else {
            uyVar2 = uyVar;
            exVar = null;
        }
        if (!uyVar.equals(uyVar2)) {
            uyVar.b();
        }
        if (this.a.b((uy<?>) uyVar2)) {
            dxVar = this.a.a((uy) uyVar2);
            uwVar = dxVar.a(this.t);
        } else {
            uwVar = uw.NONE;
        }
        if (!this.s.a(!this.a.a(this.C), swVar, uwVar)) {
            return uyVar2;
        }
        if (dxVar != null) {
            int i2 = a.c[uwVar.ordinal()];
            if (i2 == 1) {
                ywVar = new cy(this.C, this.i);
            } else if (i2 == 2) {
                ywVar = new wy(this.a.b(), this.C, this.i, this.q, this.r, exVar, cls, this.t);
            } else {
                throw new IllegalArgumentException("Unknown strategy: " + uwVar);
            }
            ty b3 = ty.b(uyVar2);
            this.f.a(ywVar, dxVar, b3);
            return b3;
        }
        throw new gw.d(uyVar2.get().getClass());
    }
}
