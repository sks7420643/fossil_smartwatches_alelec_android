package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.util.Log;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class s30 extends Fragment {
    @DexIgnore
    public /* final */ i30 a;
    @DexIgnore
    public /* final */ u30 b;
    @DexIgnore
    public /* final */ Set<s30> c;
    @DexIgnore
    public iw d;
    @DexIgnore
    public s30 e;
    @DexIgnore
    public Fragment f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements u30 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.u30
        public Set<iw> a() {
            Set<s30> a2 = s30.this.a();
            HashSet hashSet = new HashSet(a2.size());
            for (s30 s30 : a2) {
                if (s30.d() != null) {
                    hashSet.add(s30.d());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + s30.this + "}";
        }
    }

    @DexIgnore
    public s30() {
        this(new i30());
    }

    @DexIgnore
    public void a(iw iwVar) {
        this.d = iwVar;
    }

    @DexIgnore
    public i30 b() {
        return this.a;
    }

    @DexIgnore
    @TargetApi(17)
    public final Fragment c() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.f;
    }

    @DexIgnore
    public iw d() {
        return this.d;
    }

    @DexIgnore
    public u30 e() {
        return this.b;
    }

    @DexIgnore
    public final void f() {
        s30 s30 = this.e;
        if (s30 != null) {
            s30.b(this);
            this.e = null;
        }
    }

    @DexIgnore
    @Override // android.app.Fragment
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.a.a();
        f();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        f();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.a.b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.a.c();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{parent=" + c() + "}";
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public s30(i30 i30) {
        this.b = new a();
        this.c = new HashSet();
        this.a = i30;
    }

    @DexIgnore
    public final void a(s30 s30) {
        this.c.add(s30);
    }

    @DexIgnore
    public final void b(s30 s30) {
        this.c.remove(s30);
    }

    @DexIgnore
    @TargetApi(17)
    public Set<s30> a() {
        if (equals(this.e)) {
            return Collections.unmodifiableSet(this.c);
        }
        if (this.e == null || Build.VERSION.SDK_INT < 17) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet();
        for (s30 s30 : this.e.a()) {
            if (a(s30.getParentFragment())) {
                hashSet.add(s30);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public void b(Fragment fragment) {
        this.f = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    @DexIgnore
    @TargetApi(17)
    public final boolean a(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        f();
        s30 b2 = aw.a(activity).h().b(activity);
        this.e = b2;
        if (!equals(b2)) {
            this.e.a(this);
        }
    }
}
