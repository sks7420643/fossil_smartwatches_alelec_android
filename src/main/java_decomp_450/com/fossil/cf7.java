package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf7<T> implements df7<Object, T> {
    @DexIgnore
    public T a;

    @DexIgnore
    @Override // com.fossil.df7
    public T a(Object obj, zf7<?> zf7) {
        ee7.b(zf7, "property");
        T t = this.a;
        if (t != null) {
            return t;
        }
        throw new IllegalStateException("Property " + zf7.getName() + " should be initialized before get.");
    }

    @DexIgnore
    @Override // com.fossil.df7
    public void a(Object obj, zf7<?> zf7, T t) {
        ee7.b(zf7, "property");
        ee7.b(t, "value");
        this.a = t;
    }
}
