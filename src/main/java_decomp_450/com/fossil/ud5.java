package com.fossil;

import com.facebook.share.internal.VideoUploader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ud5 {
    @DexIgnore
    public static /* final */ ud5 a; // = new ud5();

    @DexIgnore
    public final int a() {
        return 30;
    }

    @DexIgnore
    public final int a(int i) {
        if (i < 18) {
            return 540;
        }
        return (18 <= i && 65 >= i) ? 480 : 450;
    }

    @DexIgnore
    public final r87<Integer, Integer> a(fb5 fb5, int i) {
        int i2;
        ee7.b(fb5, "gender");
        int i3 = td5.a[fb5.ordinal()];
        int i4 = 60;
        int i5 = 70;
        if (i3 == 1) {
            i2 = 165;
            if (i < 18) {
                i4 = 55;
            } else if (i >= 35) {
                i4 = 70;
            }
        } else if (i3 == 2) {
            i2 = 175;
            if (i < 18) {
                i5 = 65;
            } else if (i >= 35) {
                i5 = 80;
            }
            i4 = i5;
        } else if (i3 == 3) {
            i2 = 170;
            if (i >= 18) {
                i4 = i < 35 ? 65 : 75;
            }
        } else {
            throw new p87();
        }
        return new r87<>(Integer.valueOf(i2), Integer.valueOf(i4));
    }

    @DexIgnore
    public final int b() {
        return VideoUploader.RETRY_DELAY_UNIT_MS;
    }

    @DexIgnore
    public final int a(int i, int i2, int i3, fb5 fb5) {
        double d;
        ee7.b(fb5, "gender");
        double d2 = i < 18 ? 0.0d : (18 <= i && 30 >= i) ? 21.5650154d : (30 <= i && 50 >= i) ? 25.291792d : 33.3115727d;
        int i4 = td5.b[fb5.ordinal()];
        if (i4 == 1) {
            d = -171.5010333d;
        } else if (i4 == 2) {
            d = -159.3516885d;
        } else if (i4 == 3) {
            d = -64.4165128d;
        } else {
            throw new p87();
        }
        return af7.a(d2 + 147.7007883d + (((double) i2) * 2.3639882d) + (((double) i3) * 0.2297702d) + d);
    }
}
