package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ix<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(Exception exc);

        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    void a();

    @DexIgnore
    void a(ew ewVar, a<? super T> aVar);

    @DexIgnore
    sw b();

    @DexIgnore
    void cancel();

    @DexIgnore
    Class<T> getDataClass();
}
