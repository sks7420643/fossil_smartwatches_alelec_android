package com.fossil;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh3 implements ki3 {
    @DexIgnore
    public static volatile oh3 G;
    @DexIgnore
    public volatile Boolean A;
    @DexIgnore
    public Boolean B;
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public int D;
    @DexIgnore
    public AtomicInteger E; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ long F;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ xm3 f;
    @DexIgnore
    public /* final */ ym3 g;
    @DexIgnore
    public /* final */ wg3 h;
    @DexIgnore
    public /* final */ jg3 i;
    @DexIgnore
    public /* final */ hh3 j;
    @DexIgnore
    public /* final */ il3 k;
    @DexIgnore
    public /* final */ jm3 l;
    @DexIgnore
    public /* final */ hg3 m;
    @DexIgnore
    public /* final */ n92 n;
    @DexIgnore
    public /* final */ zj3 o;
    @DexIgnore
    public /* final */ ti3 p;
    @DexIgnore
    public /* final */ fb3 q;
    @DexIgnore
    public /* final */ qj3 r;
    @DexIgnore
    public fg3 s;
    @DexIgnore
    public ek3 t;
    @DexIgnore
    public ob3 u;
    @DexIgnore
    public cg3 v;
    @DexIgnore
    public bh3 w;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public Boolean y;
    @DexIgnore
    public long z;

    @DexIgnore
    public oh3(qi3 qi3) {
        long j2;
        Bundle bundle;
        boolean z2 = false;
        a72.a(qi3);
        xm3 xm3 = new xm3(qi3.a);
        this.f = xm3;
        zf3.a = xm3;
        this.a = qi3.a;
        this.b = qi3.b;
        this.c = qi3.c;
        this.d = qi3.d;
        this.e = qi3.h;
        this.A = qi3.e;
        qn2 qn2 = qi3.g;
        if (!(qn2 == null || (bundle = qn2.g) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.B = (Boolean) obj;
            }
            Object obj2 = qn2.g.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.C = (Boolean) obj2;
            }
        }
        tq2.a(this.a);
        n92 d2 = q92.d();
        this.n = d2;
        Long l2 = qi3.i;
        if (l2 != null) {
            j2 = l2.longValue();
        } else {
            j2 = d2.b();
        }
        this.F = j2;
        this.g = new ym3(this);
        wg3 wg3 = new wg3(this);
        wg3.o();
        this.h = wg3;
        jg3 jg3 = new jg3(this);
        jg3.o();
        this.i = jg3;
        jm3 jm3 = new jm3(this);
        jm3.o();
        this.l = jm3;
        hg3 hg3 = new hg3(this);
        hg3.o();
        this.m = hg3;
        this.q = new fb3(this);
        zj3 zj3 = new zj3(this);
        zj3.x();
        this.o = zj3;
        ti3 ti3 = new ti3(this);
        ti3.x();
        this.p = ti3;
        il3 il3 = new il3(this);
        il3.x();
        this.k = il3;
        qj3 qj3 = new qj3(this);
        qj3.o();
        this.r = qj3;
        hh3 hh3 = new hh3(this);
        hh3.o();
        this.j = hh3;
        qn2 qn22 = qi3.g;
        if (!(qn22 == null || qn22.b == 0)) {
            z2 = true;
        }
        boolean z3 = !z2;
        if (this.a.getApplicationContext() instanceof Application) {
            ti3 u2 = u();
            if (u2.f().getApplicationContext() instanceof Application) {
                Application application = (Application) u2.f().getApplicationContext();
                if (u2.c == null) {
                    u2.c = new pj3(u2, null);
                }
                if (z3) {
                    application.unregisterActivityLifecycleCallbacks(u2.c);
                    application.registerActivityLifecycleCallbacks(u2.c);
                    u2.e().B().a("Registered activity lifecycle callback");
                }
            }
        } else {
            e().w().a("Application context is not an Application");
        }
        this.j.a(new qh3(this, qi3));
    }

    @DexIgnore
    public final String A() {
        return this.c;
    }

    @DexIgnore
    public final String B() {
        return this.d;
    }

    @DexIgnore
    public final boolean C() {
        return this.e;
    }

    @DexIgnore
    public final zj3 D() {
        b(this.o);
        return this.o;
    }

    @DexIgnore
    public final ek3 E() {
        b(this.t);
        return this.t;
    }

    @DexIgnore
    public final ob3 F() {
        b(this.u);
        return this.u;
    }

    @DexIgnore
    public final cg3 G() {
        b(this.v);
        return this.v;
    }

    @DexIgnore
    public final fb3 H() {
        fb3 fb3 = this.q;
        if (fb3 != null) {
            return fb3;
        }
        throw new IllegalStateException("Component not created");
    }

    @DexIgnore
    public final void a(qi3 qi3) {
        mg3 mg3;
        String str;
        c().g();
        ob3 ob3 = new ob3(this);
        ob3.o();
        this.u = ob3;
        cg3 cg3 = new cg3(this, qi3.f);
        cg3.x();
        this.v = cg3;
        fg3 fg3 = new fg3(this);
        fg3.x();
        this.s = fg3;
        ek3 ek3 = new ek3(this);
        ek3.x();
        this.t = ek3;
        this.l.p();
        this.h.p();
        this.w = new bh3(this);
        this.v.y();
        e().z().a("App measurement initialized, version", Long.valueOf(this.g.n()));
        e().z().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String A2 = cg3.A();
        if (TextUtils.isEmpty(this.b)) {
            if (v().d(A2)) {
                mg3 = e().z();
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                mg3 z2 = e().z();
                String valueOf = String.valueOf(A2);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
                mg3 = z2;
            }
            mg3.a(str);
        }
        e().A().a("Debug-level message logging enabled");
        if (this.D != this.E.get()) {
            e().t().a("Not all components initialized", Integer.valueOf(this.D), Integer.valueOf(this.E.get()));
        }
        this.x = true;
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final xm3 b() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final hh3 c() {
        b(this.j);
        return this.j;
    }

    @DexIgnore
    public final boolean d() {
        return this.A != null && this.A.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final jg3 e() {
        b(this.i);
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final Context f() {
        return this.a;
    }

    @DexIgnore
    public final boolean g() {
        return h() == 0;
    }

    @DexIgnore
    public final int h() {
        c().g();
        if (this.g.p()) {
            return 1;
        }
        Boolean bool = this.C;
        if (bool != null && bool.booleanValue()) {
            return 2;
        }
        Boolean x2 = p().x();
        if (x2 == null) {
            ym3 ym3 = this.g;
            ym3.b();
            Boolean e2 = ym3.e("firebase_analytics_collection_enabled");
            if (e2 == null) {
                Boolean bool2 = this.B;
                if (bool2 != null) {
                    if (bool2.booleanValue()) {
                        return 0;
                    }
                    return 5;
                } else if (v12.b()) {
                    return 6;
                } else {
                    if (!this.g.a(wb3.T) || this.A == null || this.A.booleanValue()) {
                        return 0;
                    }
                    return 7;
                }
            } else if (e2.booleanValue()) {
                return 0;
            } else {
                return 4;
            }
        } else if (x2.booleanValue()) {
            return 0;
        } else {
            return 3;
        }
    }

    @DexIgnore
    public final void i() {
    }

    @DexIgnore
    public final void j() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    @DexIgnore
    public final void k() {
        this.E.incrementAndGet();
    }

    @DexIgnore
    public final boolean l() {
        if (this.x) {
            c().g();
            Boolean bool = this.y;
            if (bool == null || this.z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.n.c() - this.z) > 1000)) {
                this.z = this.n.c();
                boolean z2 = true;
                Boolean valueOf = Boolean.valueOf(v().c("android.permission.INTERNET") && v().c("android.permission.ACCESS_NETWORK_STATE") && (ja2.b(this.a).a() || this.g.u() || (gh3.a(this.a) && jm3.a(this.a, false))));
                this.y = valueOf;
                if (valueOf.booleanValue()) {
                    if (!v().a(G().B(), G().C(), G().D()) && TextUtils.isEmpty(G().C())) {
                        z2 = false;
                    }
                    this.y = Boolean.valueOf(z2);
                }
            }
            return this.y.booleanValue();
        }
        throw new IllegalStateException("AppMeasurement is not initialized");
    }

    @DexIgnore
    public final void m() {
        c().g();
        b(n());
        String A2 = G().A();
        Pair<String, Boolean> a2 = p().a(A2);
        if (!this.g.q().booleanValue() || ((Boolean) a2.second).booleanValue() || TextUtils.isEmpty((CharSequence) a2.first)) {
            e().A().a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
        } else if (!n().s()) {
            e().w().a("Network is not available for Deferred Deep Link request. Skipping");
        } else {
            URL a3 = v().a(G().l().n(), A2, (String) a2.first, p().y.a() - 1);
            qj3 n2 = n();
            nh3 nh3 = new nh3(this);
            n2.g();
            n2.n();
            a72.a(a3);
            a72.a(nh3);
            n2.c().b(new sj3(n2, A2, a3, null, null, nh3));
        }
    }

    @DexIgnore
    public final qj3 n() {
        b(this.r);
        return this.r;
    }

    @DexIgnore
    public final ym3 o() {
        return this.g;
    }

    @DexIgnore
    public final wg3 p() {
        a((ii3) this.h);
        return this.h;
    }

    @DexIgnore
    public final jg3 q() {
        jg3 jg3 = this.i;
        if (jg3 == null || !jg3.r()) {
            return null;
        }
        return this.i;
    }

    @DexIgnore
    public final il3 r() {
        b(this.k);
        return this.k;
    }

    @DexIgnore
    public final bh3 s() {
        return this.w;
    }

    @DexIgnore
    public final hh3 t() {
        return this.j;
    }

    @DexIgnore
    public final ti3 u() {
        b(this.p);
        return this.p;
    }

    @DexIgnore
    public final jm3 v() {
        a((ii3) this.l);
        return this.l;
    }

    @DexIgnore
    public final hg3 w() {
        a((ii3) this.m);
        return this.m;
    }

    @DexIgnore
    public final fg3 x() {
        b(this.s);
        return this.s;
    }

    @DexIgnore
    public final boolean y() {
        return TextUtils.isEmpty(this.b);
    }

    @DexIgnore
    public final String z() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public final n92 zzm() {
        return this.n;
    }

    @DexIgnore
    public static void b(hi3 hi3) {
        if (hi3 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!hi3.r()) {
            String valueOf = String.valueOf(hi3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static void b(kh3 kh3) {
        if (kh3 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!kh3.v()) {
            String valueOf = String.valueOf(kh3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final void a() {
        c().g();
        if (p().e.a() == 0) {
            p().e.a(this.n.b());
        }
        if (Long.valueOf(p().j.a()).longValue() == 0) {
            e().B().a("Persisting first open", Long.valueOf(this.F));
            p().j.a(this.F);
        }
        if (this.g.a(wb3.R0)) {
            u().h.b();
        }
        if (l()) {
            if (!TextUtils.isEmpty(G().B()) || !TextUtils.isEmpty(G().C())) {
                v();
                if (jm3.a(G().B(), p().t(), G().C(), p().u())) {
                    e().z().a("Rechecking which service to use due to a GMP App Id change");
                    p().w();
                    x().A();
                    this.t.G();
                    this.t.E();
                    p().j.a(this.F);
                    p().l.a(null);
                }
                p().c(G().B());
                p().d(G().C());
            }
            u().a(p().l.a());
            if (w03.a() && this.g.a(wb3.v0) && !v().w() && !TextUtils.isEmpty(p().z.a())) {
                e().w().a("Remote config removed with active feature rollouts");
                p().z.a(null);
            }
            if (!TextUtils.isEmpty(G().B()) || !TextUtils.isEmpty(G().C())) {
                boolean g2 = g();
                if (!p().z() && !this.g.p()) {
                    p().c(!g2);
                }
                if (g2) {
                    u().H();
                }
                r().d.a();
                E().a(new AtomicReference<>());
                if (g23.a() && this.g.a(wb3.N0)) {
                    E().a(p().C.a());
                }
            }
        } else if (g()) {
            if (!v().c("android.permission.INTERNET")) {
                e().t().a("App is missing INTERNET permission");
            }
            if (!v().c("android.permission.ACCESS_NETWORK_STATE")) {
                e().t().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!ja2.b(this.a).a() && !this.g.u()) {
                if (!gh3.a(this.a)) {
                    e().t().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!jm3.a(this.a, false)) {
                    e().t().a("AppMeasurementService not registered/enabled");
                }
            }
            e().t().a("Uploading is not possible. App measurement disabled");
        }
        p().t.a(this.g.a(wb3.a0));
    }

    @DexIgnore
    public static oh3 a(Context context, qn2 qn2, Long l2) {
        Bundle bundle;
        if (qn2 != null && (qn2.e == null || qn2.f == null)) {
            qn2 = new qn2(qn2.a, qn2.b, qn2.c, qn2.d, null, null, qn2.g);
        }
        a72.a(context);
        a72.a(context.getApplicationContext());
        if (G == null) {
            synchronized (oh3.class) {
                if (G == null) {
                    G = new oh3(new qi3(context, qn2, l2));
                }
            }
        } else if (!(qn2 == null || (bundle = qn2.g) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            G.a(qn2.g.getBoolean("dataCollectionDefaultEnabled"));
        }
        return G;
    }

    @DexIgnore
    public static void a(ii3 ii3) {
        if (ii3 == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.A = Boolean.valueOf(z2);
    }

    @DexIgnore
    public final void a(hi3 hi3) {
        this.D++;
    }

    @DexIgnore
    public final void a(kh3 kh3) {
        this.D++;
    }

    @DexIgnore
    public final /* synthetic */ void a(String str, int i2, Throwable th, byte[] bArr, Map map) {
        List<ResolveInfo> queryIntentActivities;
        boolean z2 = true;
        if (!((i2 == 200 || i2 == 204 || i2 == 304) && th == null)) {
            e().w().a("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i2), th);
            return;
        }
        p().x.a(true);
        if (bArr.length == 0) {
            e().A().a("Deferred Deep Link response empty.");
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            String optString = jSONObject.optString("deeplink", "");
            String optString2 = jSONObject.optString("gclid", "");
            double optDouble = jSONObject.optDouble("timestamp", 0.0d);
            if (TextUtils.isEmpty(optString)) {
                e().A().a("Deferred Deep Link is empty.");
                return;
            }
            jm3 v2 = v();
            v2.a();
            if (TextUtils.isEmpty(optString) || (queryIntentActivities = v2.f().getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                z2 = false;
            }
            if (!z2) {
                e().w().a("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("gclid", optString2);
            bundle.putString("_cis", "ddp");
            this.p.a("auto", "_cmp", bundle);
            jm3 v3 = v();
            if (!TextUtils.isEmpty(optString) && v3.a(optString, optDouble)) {
                v3.f().sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
            }
        } catch (JSONException e2) {
            e().t().a("Failed to parse the Deferred Deep Link response. exception", e2);
        }
    }
}
