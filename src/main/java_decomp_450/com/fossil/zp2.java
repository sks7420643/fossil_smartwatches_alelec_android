package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp2 extends bw2<zp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ zp2 zzj;
    @DexIgnore
    public static volatile wx2<zp2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public long zzg;
    @DexIgnore
    public float zzh;
    @DexIgnore
    public double zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<zp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(zp2.zzj);
        }

        @DexIgnore
        public final a a(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).a(j);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).b(str);
            return this;
        }

        @DexIgnore
        public final a o() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).y();
            return this;
        }

        @DexIgnore
        public final a p() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).z();
            return this;
        }

        @DexIgnore
        public final a zza() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).x();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final a b(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).b(j);
            return this;
        }

        @DexIgnore
        public final a a(double d) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((zp2) ((bw2.a) this).b).a(d);
            return this;
        }
    }

    /*
    static {
        zp2 zp2 = new zp2();
        zzj = zp2;
        bw2.a(zp2.class, zp2);
    }
    */

    @DexIgnore
    public static a A() {
        return (a) zzj.e();
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 1;
        this.zzd = j;
    }

    @DexIgnore
    public final void b(String str) {
        str.getClass();
        this.zzc |= 4;
        this.zzf = str;
    }

    @DexIgnore
    public final long p() {
        return this.zzd;
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String s() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final long u() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean v() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final double w() {
        return this.zzi;
    }

    @DexIgnore
    public final void x() {
        this.zzc &= -5;
        this.zzf = zzj.zzf;
    }

    @DexIgnore
    public final void y() {
        this.zzc &= -9;
        this.zzg = 0;
    }

    @DexIgnore
    public final void z() {
        this.zzc &= -33;
        this.zzi = 0.0d;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final void b(long j) {
        this.zzc |= 8;
        this.zzg = j;
    }

    @DexIgnore
    public final void a(double d) {
        this.zzc |= 32;
        this.zzi = d;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new zp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1008\u0002\u0004\u1002\u0003\u0005\u1001\u0004\u0006\u1000\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                wx2<zp2> wx2 = zzk;
                if (wx2 == null) {
                    synchronized (zp2.class) {
                        wx2 = zzk;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzj);
                            zzk = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
