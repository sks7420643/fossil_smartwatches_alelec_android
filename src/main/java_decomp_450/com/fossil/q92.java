package com.fossil;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q92 implements n92 {
    @DexIgnore
    public static /* final */ q92 a; // = new q92();

    @DexIgnore
    public static n92 d() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.n92
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    @Override // com.fossil.n92
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    @Override // com.fossil.n92
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
