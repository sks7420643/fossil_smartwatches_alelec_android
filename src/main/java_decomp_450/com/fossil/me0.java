package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class me0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<me0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public me0 createFromParcel(Parcel parcel) {
            return new me0(parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public me0[] newArray(int i) {
            return new me0[i];
        }
    }

    @DexIgnore
    public me0(int i, int i2, int i3) throws IllegalArgumentException {
        this.a = i;
        this.b = i2;
        this.c = i3;
        boolean z = true;
        if (i >= 0 && 359 >= i) {
            int i4 = this.b;
            if (i4 >= 0 && 120 >= i4) {
                if (!(this.c < 0 ? false : z)) {
                    throw new IllegalArgumentException(yh0.a(yh0.b("z index("), this.c, ") must be larger than ", "[0]."));
                }
                return;
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("distanceFromCenter("), this.b, ") is out of ", "range [0, 120]."));
        }
        throw new IllegalArgumentException(yh0.a(yh0.b("angle("), this.a, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("angle", this.a).put("distance", this.b).put("z_index", this.c);
        ee7.a((Object) put, "JSONObject()\n           \u2026Constant.Z_INDEX, zIndex)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(me0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            me0 me0 = (me0) obj;
            return this.a == me0.a && this.b == me0.b && this.c == me0.c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.a;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.b;
    }

    @DexIgnore
    public final int getZIndex() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
