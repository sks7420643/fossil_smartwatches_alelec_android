package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface uz {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(uy<?> uyVar);
    }

    @DexIgnore
    uy<?> a(yw ywVar);

    @DexIgnore
    uy<?> a(yw ywVar, uy<?> uyVar);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(a aVar);
}
