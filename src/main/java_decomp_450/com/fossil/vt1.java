package com.fossil;

import com.fossil.pt1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vt1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public a a(int i) {
            a(Integer.valueOf(i));
            return this;
        }

        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(tt1 tt1);

        @DexIgnore
        public abstract a a(yt1 yt1);

        @DexIgnore
        public abstract a a(Integer num);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract a a(List<ut1> list);

        @DexIgnore
        public abstract vt1 a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public a b(String str) {
            a(str);
            return this;
        }
    }

    @DexIgnore
    public static a h() {
        return new pt1.b();
    }

    @DexIgnore
    public abstract tt1 a();

    @DexIgnore
    public abstract List<ut1> b();

    @DexIgnore
    public abstract Integer c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract yt1 e();

    @DexIgnore
    public abstract long f();

    @DexIgnore
    public abstract long g();
}
