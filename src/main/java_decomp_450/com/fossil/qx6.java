package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx6<T> {
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public /* final */ lb5 a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Integer c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> qx6<T> a(int i, String str, T t) {
            ee7.b(str, "msg");
            return new qx6<>(lb5.ERROR, t, Integer.valueOf(i), str);
        }

        @DexIgnore
        public final <T> qx6<T> b(T t) {
            return new qx6<>(lb5.NETWORK_LOADING, t, null, null);
        }

        @DexIgnore
        public final <T> qx6<T> c(T t) {
            return new qx6<>(lb5.SUCCESS, t, null, null);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final <T> qx6<T> a(T t) {
            return new qx6<>(lb5.DATABASE_LOADING, t, null, null);
        }
    }

    @DexIgnore
    public qx6(lb5 lb5, T t, Integer num, String str) {
        ee7.b(lb5, "status");
        this.a = lb5;
        this.b = t;
        this.c = num;
        this.d = str;
    }

    @DexIgnore
    public final lb5 a() {
        return this.a;
    }

    @DexIgnore
    public final T b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.b;
    }

    @DexIgnore
    public final lb5 d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qx6)) {
            return false;
        }
        qx6 qx6 = (qx6) obj;
        return ee7.a(this.a, qx6.a) && ee7.a(this.b, qx6.b) && ee7.a(this.c, qx6.c) && ee7.a(this.d, qx6.d);
    }

    @DexIgnore
    public int hashCode() {
        lb5 lb5 = this.a;
        int i = 0;
        int hashCode = (lb5 != null ? lb5.hashCode() : 0) * 31;
        T t = this.b;
        int hashCode2 = (hashCode + (t != null ? t.hashCode() : 0)) * 31;
        Integer num = this.c;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Resource(status=" + this.a + ", data=" + ((Object) this.b) + ", code=" + this.c + ", message=" + this.d + ")";
    }
}
