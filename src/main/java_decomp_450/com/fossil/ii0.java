package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii0 {
    @DexIgnore
    public static /* final */ ii0 a; // = new ii0();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        boolean z = true;
        for (String str : strArr) {
            z = z && v6.a(context, str) == 0;
        }
        return z;
    }
}
