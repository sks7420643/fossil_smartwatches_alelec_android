package com.fossil;

import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.view.KeyEvent;
import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jk5 extends hk5 {
    @DexIgnore
    public /* final */ String c; // = "NewMusicController";
    @DexIgnore
    public /* final */ MediaController.Callback d;
    @DexIgnore
    public int e;
    @DexIgnore
    public ik5 f;
    @DexIgnore
    public /* final */ MediaController g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends MediaController.Callback {
        @DexIgnore
        public /* final */ /* synthetic */ jk5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(jk5 jk5, String str) {
            this.a = jk5;
            this.b = str;
        }

        @DexIgnore
        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            super.onMetadataChanged(mediaMetadata);
            if (mediaMetadata != null) {
                String a2 = vc5.a(mediaMetadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a3 = vc5.a(mediaMetadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a4 = vc5.a(mediaMetadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a5 = this.a.c;
                local.d(a5, "SystemCallback of " + this.b + " - onMetadataChanged title=" + a2 + ", artist=" + a3);
                ik5 ik5 = new ik5(this.a.c(), this.b, a2, a3, a4);
                if (!ee7.a(ik5, this.a.e())) {
                    ik5 e = this.a.e();
                    this.a.a(ik5);
                    this.a.a(e, ik5);
                }
            }
        }

        @DexIgnore
        public void onPlaybackStateChanged(PlaybackState playbackState) {
            super.onPlaybackStateChanged(playbackState);
            int state = playbackState != null ? playbackState.getState() : 0;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            StringBuilder sb = new StringBuilder();
            sb.append("SystemCallback of ");
            sb.append(this.b);
            sb.append(" - onPlaybackStateChanged , state = ");
            sb.append(playbackState != null ? Integer.valueOf(playbackState.getState()) : null);
            sb.append(" lastState ");
            sb.append(this.a.f());
            local.d(a2, sb.toString());
            if (state != this.a.f()) {
                int f = this.a.f();
                this.a.a(state);
                jk5 jk5 = this.a;
                jk5.a(f, state, jk5);
            }
        }

        @DexIgnore
        public void onSessionDestroyed() {
            super.onSessionDestroyed();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            local.d(a2, "SystemCallback of " + this.b + " onSessionDestroyed");
            jk5 jk5 = this.a;
            jk5.a((hk5) jk5);
        }

        @DexIgnore
        public void onSessionEvent(String str, Bundle bundle) {
            ee7.b(str, Constants.EVENT);
            super.onSessionEvent(str, bundle);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            local.d(a2, "SystemCallback of " + this.b + " .onSessionEvent, event=" + str + ", extras=" + bundle);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public jk5(android.media.session.MediaController r7, java.lang.String r8) {
        /*
            r6 = this;
            java.lang.String r0 = "mSystemMediaController"
            com.fossil.ee7.b(r7, r0)
            java.lang.String r0 = "appName"
            com.fossil.ee7.b(r8, r0)
            java.lang.String r0 = r7.getPackageName()
            java.lang.String r1 = "mSystemMediaController.packageName"
            com.fossil.ee7.a(r0, r1)
            r6.<init>(r8, r0)
            r6.g = r7
            java.lang.String r7 = "NewMusicController"
            r6.c = r7
            com.fossil.jk5$a r7 = new com.fossil.jk5$a
            r7.<init>(r6, r8)
            r6.d = r7
            com.fossil.ik5 r7 = new com.fossil.ik5
            java.lang.String r1 = r6.c()
            java.lang.String r3 = "Unknown"
            java.lang.String r4 = "Unknown"
            java.lang.String r5 = "Unknown"
            r0 = r7
            r2 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r6.f = r7
            android.media.session.MediaController r7 = r6.g
            android.media.session.MediaController$Callback r8 = r6.d
            r7.registerCallback(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jk5.<init>(android.media.session.MediaController, java.lang.String):void");
    }

    @DexIgnore
    public void a(int i, int i2, hk5 hk5) {
        ee7.b(hk5, "controller");
    }

    @DexIgnore
    public void a(ik5 ik5, ik5 ik52) {
        ee7.b(ik5, "oldMetadata");
        ee7.b(ik52, "newMetadata");
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public ik5 b() {
        String str;
        String str2;
        String str3;
        MediaMetadata metadata = this.g.getMetadata();
        if (metadata != null) {
            String a2 = vc5.a(metadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            String a3 = vc5.a(metadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            str = vc5.a(metadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            str3 = a2;
            str2 = a3;
        } else {
            str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            str2 = str3;
            str = str2;
        }
        ik5 ik5 = new ik5(c(), a(), str3, str2, str);
        if (!ee7.a(ik5, this.f)) {
            this.f = ik5;
        }
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public int d() {
        PlaybackState playbackState = this.g.getPlaybackState();
        Integer valueOf = playbackState != null ? Integer.valueOf(playbackState.getState()) : null;
        int i = this.e;
        if ((valueOf == null || valueOf.intValue() != i) && valueOf != null) {
            this.e = valueOf.intValue();
        }
        return this.e;
    }

    @DexIgnore
    public final ik5 e() {
        return this.f;
    }

    @DexIgnore
    public final int f() {
        return this.e;
    }

    @DexIgnore
    public final void a(int i) {
        this.e = i;
    }

    @DexIgnore
    public final void a(ik5 ik5) {
        ee7.b(ik5, "<set-?>");
        this.f = ik5;
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public boolean a(KeyEvent keyEvent) {
        ee7.b(keyEvent, "keyEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, "dispatchMediaButtonEvent of " + c() + " keyEvent=" + keyEvent);
        return this.g.dispatchMediaButtonEvent(keyEvent);
    }

    @DexIgnore
    public void a(hk5 hk5) {
        ee7.b(hk5, "controller");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, ".onSessionDestroyed of " + c());
        this.g.unregisterCallback(this.d);
    }
}
