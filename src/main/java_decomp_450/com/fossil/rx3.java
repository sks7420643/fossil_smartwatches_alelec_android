package com.fossil;

import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rx3<E> extends mx3<E> implements Set<E> {
    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract Set<E> delegate();

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return yz3.a(this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return yz3.a(this);
    }

    @DexIgnore
    @Override // com.fossil.mx3
    public boolean standardRemoveAll(Collection<?> collection) {
        jw3.a(collection);
        return yz3.a((Set<?>) this, collection);
    }
}
