package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i86 implements Factory<h86> {
    @DexIgnore
    public static h86 a(f86 f86, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new h86(f86, userRepository, summariesRepository, portfolioApp);
    }
}
