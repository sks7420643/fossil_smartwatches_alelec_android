package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cy6;
import com.fossil.el4;
import com.fossil.un6;
import com.fossil.zn6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn6 extends ho5 implements View.OnClickListener, cy6.g, un6.b {
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<o75> g;
    @DexIgnore
    public un6 h;
    @DexIgnore
    public zn6 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wn6 a() {
            return new wn6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<zn6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ wn6 a;

        @DexIgnore
        public b(wn6 wn6) {
            this.a = wn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(zn6.b bVar) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                if (bVar != null) {
                    int i = xn6.a[bVar.ordinal()];
                    if (i == 1) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "Data is changed, wait for user confirm to save");
                        bx6 bx6 = bx6.c;
                        FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                        ee7.a((Object) childFragmentManager, "childFragmentManager");
                        bx6.V(childFragmentManager);
                        return;
                    } else if (i == 2 || i == 3) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "close view");
                        activity.finish();
                        return;
                    } else if (i == 4) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "FAIL_DEVICE_DISCONNECT");
                        bx6 bx62 = bx6.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        ee7.a((Object) childFragmentManager2, "childFragmentManager");
                        bx62.n(childFragmentManager2);
                        return;
                    }
                }
                FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "unknown error");
                String c = PortfolioApp.g0.c().c();
                TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
                Context requireContext = this.a.requireContext();
                ee7.a((Object) requireContext, "requireContext()");
                TroubleshootingActivity.a.a(aVar, requireContext, c, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<el4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ wn6 a;

        @DexIgnore
        public c(wn6 wn6) {
            this.a = wn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.b bVar) {
            if (!bVar.a().isEmpty()) {
                wn6 wn6 = this.a;
                Object[] array = bVar.a().toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    wn6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<el4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ wn6 a;

        @DexIgnore
        public d(wn6 wn6) {
            this.a = wn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutSettingFragment", "loadingState start " + aVar.a() + " stop " + aVar.b());
                if (aVar.b()) {
                    this.a.a();
                } else if (aVar.a()) {
                    this.a.b();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends WorkoutSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ wn6 a;

        @DexIgnore
        public e(wn6 wn6) {
            this.a = wn6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WorkoutSetting> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingFragment", "update workoutSettingList, " + list);
            un6 a2 = wn6.a(this.a);
            ee7.a((Object) list, "it");
            a2.a(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ un6 a(wn6 wn6) {
        un6 un6 = wn6.h;
        if (un6 != null) {
            return un6;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.un6.b
    public void b(WorkoutSetting workoutSetting, boolean z) {
        ee7.b(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onEnableChange(), workout = " + workoutSetting + ", value=" + z);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        zn6 zn6 = this.i;
        if (zn6 != null) {
            un6 un6 = this.h;
            if (un6 != null) {
                zn6.a(un6.c());
                return true;
            }
            ee7.d("mAdapter");
            throw null;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        if (isActive() && view != null && view.getId() == 2131361851) {
            zn6 zn6 = this.i;
            if (zn6 != null) {
                un6 un6 = this.h;
                if (un6 != null) {
                    zn6.a(un6.c());
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        o75 o75 = (o75) qb.a(layoutInflater, 2131558643, viewGroup, false, a1());
        this.g = new qw6<>(this, o75);
        RTLImageView rTLImageView = o75.q;
        ee7.a((Object) rTLImageView, "binding.acivBack");
        bf5.a(rTLImageView, this);
        this.h = new un6();
        RecyclerView recyclerView = o75.s;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        un6 un6 = this.h;
        if (un6 != null) {
            recyclerView.setAdapter(un6);
            un6 un62 = this.h;
            if (un62 != null) {
                un62.a(this);
                qw6<o75> qw6 = this.g;
                if (qw6 != null) {
                    o75 a2 = qw6.a();
                    if (a2 != null) {
                        ee7.a((Object) a2, "mBinding.get()!!");
                        return a2.d();
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mAdapter");
            throw null;
        }
        ee7.d("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.g0.c().f().d().a(this);
        FragmentActivity requireActivity = requireActivity();
        rj4 rj4 = this.j;
        if (rj4 != null) {
            he a2 = je.a(requireActivity, rj4).a(zn6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(re\u2026ingViewModel::class.java)");
            zn6 zn6 = (zn6) a2;
            this.i = zn6;
            if (zn6 != null) {
                zn6.e().a(getViewLifecycleOwner(), new b(this));
                zn6 zn62 = this.i;
                if (zn62 != null) {
                    zn62.d().a(getViewLifecycleOwner(), new c(this));
                    zn6 zn63 = this.i;
                    if (zn63 != null) {
                        zn63.b().a(getViewLifecycleOwner(), new d(this));
                        zn6 zn64 = this.i;
                        if (zn64 != null) {
                            zn64.f().a(getViewLifecycleOwner(), new e(this));
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else {
            ee7.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        ee7.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363307 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363229) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363307) {
                zn6 zn6 = this.i;
                if (zn6 != null) {
                    un6 un6 = this.h;
                    if (un6 != null) {
                        zn6.b(un6.c());
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mViewModel");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.un6.b
    public void a(WorkoutSetting workoutSetting, boolean z) {
        ee7.b(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onConfirmationChange(), workout = " + workoutSetting + ", value=" + z);
    }
}
