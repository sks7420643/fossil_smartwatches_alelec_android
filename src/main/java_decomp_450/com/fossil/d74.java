package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d74 {
    @DexIgnore
    public static /* final */ short[] h; // = {10, 20, 30, 60, 120, 300};
    @DexIgnore
    public /* final */ j74 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ f44 d;
    @DexIgnore
    public /* final */ c74 e;
    @DexIgnore
    public /* final */ a f;
    @DexIgnore
    public Thread g;

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean a();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        d74 a(y74 y74);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        File[] a();

        @DexIgnore
        File[] b();
    }

    @DexIgnore
    public d74(String str, String str2, f44 f44, c74 c74, j74 j74, a aVar) {
        if (j74 != null) {
            this.a = j74;
            this.b = str;
            this.c = str2;
            this.d = f44;
            this.e = c74;
            this.f = aVar;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends p34 {
        @DexIgnore
        public /* final */ List<g74> a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ float c;

        @DexIgnore
        public d(List<g74> list, boolean z, float f) {
            this.a = list;
            this.b = z;
            this.c = f;
        }

        @DexIgnore
        @Override // com.fossil.p34
        public void a() {
            try {
                a(this.a, this.b);
            } catch (Exception e) {
                z24.a().b("An unexpected error occurred while attempting to upload crash reports.", e);
            }
            Thread unused = d74.this.g = (Thread) null;
        }

        @DexIgnore
        public final void a(List<g74> list, boolean z) {
            z24 a2 = z24.a();
            a2.a("Starting report processing in " + this.c + " second(s)...");
            float f = this.c;
            if (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    Thread.sleep((long) (f * 1000.0f));
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            if (!d74.this.f.a()) {
                int i = 0;
                while (list.size() > 0 && !d74.this.f.a()) {
                    z24 a3 = z24.a();
                    a3.a("Attempting to send " + list.size() + " report(s)");
                    ArrayList arrayList = new ArrayList();
                    for (g74 g74 : list) {
                        if (!d74.this.a(g74, z)) {
                            arrayList.add(g74);
                        }
                    }
                    if (arrayList.size() > 0) {
                        int i2 = i + 1;
                        long j = (long) d74.h[Math.min(i, d74.h.length - 1)];
                        z24 a4 = z24.a();
                        a4.a("Report submission: scheduling delayed retry in " + j + " seconds");
                        try {
                            Thread.sleep(j * 1000);
                            i = i2;
                        } catch (InterruptedException unused2) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    }
                    list = arrayList;
                }
            }
        }
    }

    @DexIgnore
    public synchronized void a(List<g74> list, boolean z, float f2) {
        if (this.g != null) {
            z24.a().a("Report upload has already been started.");
            return;
        }
        Thread thread = new Thread(new d(list, z, f2), "Crashlytics Report Uploader");
        this.g = thread;
        thread.start();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062 A[Catch:{ Exception -> 0x0069 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.fossil.g74 r6, boolean r7) {
        /*
            r5 = this;
            r0 = 0
            com.fossil.e74 r1 = new com.fossil.e74     // Catch:{ Exception -> 0x0069 }
            java.lang.String r2 = r5.b     // Catch:{ Exception -> 0x0069 }
            java.lang.String r3 = r5.c     // Catch:{ Exception -> 0x0069 }
            r1.<init>(r2, r3, r6)     // Catch:{ Exception -> 0x0069 }
            com.fossil.f44 r2 = r5.d     // Catch:{ Exception -> 0x0069 }
            com.fossil.f44 r3 = com.fossil.f44.ALL     // Catch:{ Exception -> 0x0069 }
            r4 = 1
            if (r2 != r3) goto L_0x001b
            com.fossil.z24 r7 = com.fossil.z24.a()     // Catch:{ Exception -> 0x0069 }
            java.lang.String r1 = "Send to Reports Endpoint disabled. Removing Reports Endpoint report."
            r7.a(r1)     // Catch:{ Exception -> 0x0069 }
            goto L_0x0032
        L_0x001b:
            com.fossil.f44 r2 = r5.d     // Catch:{ Exception -> 0x0069 }
            com.fossil.f44 r3 = com.fossil.f44.JAVA_ONLY     // Catch:{ Exception -> 0x0069 }
            if (r2 != r3) goto L_0x0034
            com.fossil.g74$a r2 = r6.getType()     // Catch:{ Exception -> 0x0069 }
            com.fossil.g74$a r3 = com.fossil.g74.a.JAVA     // Catch:{ Exception -> 0x0069 }
            if (r2 != r3) goto L_0x0034
            com.fossil.z24 r7 = com.fossil.z24.a()     // Catch:{ Exception -> 0x0069 }
            java.lang.String r1 = "Send to Reports Endpoint for non-native reports disabled. Removing Reports Uploader report."
            r7.a(r1)     // Catch:{ Exception -> 0x0069 }
        L_0x0032:
            r7 = 1
            goto L_0x0060
        L_0x0034:
            com.fossil.j74 r2 = r5.a     // Catch:{ Exception -> 0x0069 }
            boolean r7 = r2.a(r1, r7)     // Catch:{ Exception -> 0x0069 }
            com.fossil.z24 r1 = com.fossil.z24.a()     // Catch:{ Exception -> 0x0069 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0069 }
            r2.<init>()     // Catch:{ Exception -> 0x0069 }
            java.lang.String r3 = "Crashlytics Reports Endpoint upload "
            r2.append(r3)     // Catch:{ Exception -> 0x0069 }
            if (r7 == 0) goto L_0x004d
            java.lang.String r3 = "complete: "
            goto L_0x004f
        L_0x004d:
            java.lang.String r3 = "FAILED: "
        L_0x004f:
            r2.append(r3)     // Catch:{ Exception -> 0x0069 }
            java.lang.String r3 = r6.b()     // Catch:{ Exception -> 0x0069 }
            r2.append(r3)     // Catch:{ Exception -> 0x0069 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0069 }
            r1.c(r2)     // Catch:{ Exception -> 0x0069 }
        L_0x0060:
            if (r7 == 0) goto L_0x0082
            com.fossil.c74 r7 = r5.e     // Catch:{ Exception -> 0x0069 }
            r7.a(r6)     // Catch:{ Exception -> 0x0069 }
            r0 = 1
            goto L_0x0082
        L_0x0069:
            r7 = move-exception
            com.fossil.z24 r1 = com.fossil.z24.a()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error occurred sending report "
            r2.append(r3)
            r2.append(r6)
            java.lang.String r6 = r2.toString()
            r1.b(r6, r7)
        L_0x0082:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d74.a(com.fossil.g74, boolean):boolean");
    }
}
