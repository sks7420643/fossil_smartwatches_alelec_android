package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ho {
    @DexIgnore
    public static ho e;
    @DexIgnore
    public bo a;
    @DexIgnore
    public co b;
    @DexIgnore
    public fo c;
    @DexIgnore
    public go d;

    @DexIgnore
    public ho(Context context, vp vpVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = new bo(applicationContext, vpVar);
        this.b = new co(applicationContext, vpVar);
        this.c = new fo(applicationContext, vpVar);
        this.d = new go(applicationContext, vpVar);
    }

    @DexIgnore
    public static synchronized ho a(Context context, vp vpVar) {
        ho hoVar;
        synchronized (ho.class) {
            if (e == null) {
                e = new ho(context, vpVar);
            }
            hoVar = e;
        }
        return hoVar;
    }

    @DexIgnore
    public co b() {
        return this.b;
    }

    @DexIgnore
    public fo c() {
        return this.c;
    }

    @DexIgnore
    public go d() {
        return this.d;
    }

    @DexIgnore
    public bo a() {
        return this.a;
    }
}
