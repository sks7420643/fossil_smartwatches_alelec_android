package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bd2 implements Parcelable.Creator<hc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ hc2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str = j72.e(parcel, a);
            } else if (a2 == 2) {
                str2 = j72.e(parcel, a);
            } else if (a2 == 4) {
                str3 = j72.e(parcel, a);
            } else if (a2 == 5) {
                i = j72.q(parcel, a);
            } else if (a2 != 6) {
                j72.v(parcel, a);
            } else {
                i2 = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new hc2(str, str2, str3, i, i2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ hc2[] newArray(int i) {
        return new hc2[i];
    }
}
