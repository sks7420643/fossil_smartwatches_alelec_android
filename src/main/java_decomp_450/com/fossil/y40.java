package com.fossil;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y40 extends z40<Drawable> {
    @DexIgnore
    public y40(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: e */
    public void c(Drawable drawable) {
        ((ImageView) ((d50) this).a).setImageDrawable(drawable);
    }
}
