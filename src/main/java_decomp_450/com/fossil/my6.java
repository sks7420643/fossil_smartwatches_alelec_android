package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my6 extends ac {
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public HashMap b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final my6 a(String str) {
            ee7.b(str, "title");
            my6 my6 = new my6();
            Bundle bundle = new Bundle();
            bundle.putString("DIALOG_TITLE", str);
            my6.setArguments(bundle);
            return my6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public void Z0() {
        HashMap hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "create with arguments " + getArguments());
        Bundle arguments = getArguments();
        if (arguments != null) {
            String string = arguments.getString("DIALOG_TITLE");
            if (string == null) {
                string = "";
            }
            this.a = string;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558581, viewGroup, false);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Window window;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131363342);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131362962);
        String b2 = eh5.l.a().b("nonBrandSurface");
        String b3 = eh5.l.a().b("primaryColor");
        FlexibleProgressBar flexibleProgressBar = (FlexibleProgressBar) view.findViewById(2131362929);
        if (b3 != null) {
            ee7.a((Object) flexibleProgressBar, "progressBar");
            flexibleProgressBar.getIndeterminateDrawable().setTint(Color.parseColor(b3));
        }
        if (b2 != null) {
            viewGroup.setBackgroundColor(Color.parseColor(b2));
        }
        if (TextUtils.isEmpty(this.a)) {
            ee7.a((Object) flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(8);
        } else {
            ee7.a((Object) flexibleTextView, "mTvTitle");
            flexibleTextView.setVisibility(0);
            flexibleTextView.setText(this.a);
        }
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
    }
}
