package com.fossil;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow3 {
    @DexIgnore
    public static /* final */ Object a;

    /*
    static {
        Object b = b();
        a = b;
        if (b != null) {
            a();
        }
        if (a != null) {
            c();
        }
    }
    */

    @DexIgnore
    @Deprecated
    public static <X extends Throwable> void a(Throwable th, Class<X> cls) throws Throwable {
        if (th != null) {
            c(th, cls);
        }
    }

    @DexIgnore
    public static void b(Throwable th) {
        jw3.a(th);
        if (th instanceof RuntimeException) {
            throw ((RuntimeException) th);
        } else if (th instanceof Error) {
            throw ((Error) th);
        }
    }

    @DexIgnore
    public static <X extends Throwable> void c(Throwable th, Class<X> cls) throws Throwable {
        jw3.a(th);
        if (cls.isInstance(th)) {
            throw cls.cast(th);
        }
    }

    @DexIgnore
    @Deprecated
    public static void a(Throwable th) {
        if (th != null) {
            b(th);
        }
    }

    @DexIgnore
    public static Method a() {
        return a("getStackTraceElement", Throwable.class, Integer.TYPE);
    }

    @DexIgnore
    public static Method a(String str, Class<?>... clsArr) throws ThreadDeath {
        try {
            return Class.forName("sun.misc.JavaLangAccess", false, null).getMethod(str, clsArr);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Method c() {
        return a("getStackTraceDepth", Throwable.class);
    }

    @DexIgnore
    public static <X extends Throwable> void b(Throwable th, Class<X> cls) throws Throwable {
        a(th, cls);
        a(th);
    }

    @DexIgnore
    public static Object b() {
        try {
            return Class.forName("sun.misc.SharedSecrets", false, null).getMethod("getJavaLangAccess", new Class[0]).invoke(null, new Object[0]);
        } catch (ThreadDeath e) {
            throw e;
        } catch (Throwable unused) {
            return null;
        }
    }
}
