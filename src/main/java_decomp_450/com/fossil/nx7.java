package com.fossil;

import android.os.Handler;
import android.os.Looper;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx7 {
    @DexIgnore
    public static /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MethodChannel.Result a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result a;

        @DexIgnore
        public b(MethodChannel.Result result) {
            this.a = result;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.a;
            if (result != null) {
                result.notImplemented();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public c(MethodChannel.Result result, Object obj) {
            this.a = result;
            this.b = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.a;
            if (result != null) {
                result.success(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MethodChannel.Result a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ Object d;

        @DexIgnore
        public d(MethodChannel.Result result, String str, String str2, Object obj) {
            this.a = result;
            this.b = str;
            this.c = str2;
            this.d = obj;
        }

        @DexIgnore
        public final void run() {
            MethodChannel.Result result = this.a;
            if (result != null) {
                result.error(this.b, this.c, this.d);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public nx7(MethodChannel.Result result) {
        this.a = result;
    }

    @DexIgnore
    public final MethodChannel.Result a() {
        return this.a;
    }

    @DexIgnore
    public final void b() {
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new b(result));
    }

    @DexIgnore
    public final void a(Object obj) {
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new c(result, obj));
    }

    @DexIgnore
    public static /* synthetic */ void a(nx7 nx7, String str, String str2, Object obj, int i, Object obj2) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            obj = null;
        }
        nx7.a(str, str2, obj);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj) {
        ee7.b(str, "code");
        MethodChannel.Result result = this.a;
        this.a = null;
        b.post(new d(result, str, str2, obj));
    }
}
