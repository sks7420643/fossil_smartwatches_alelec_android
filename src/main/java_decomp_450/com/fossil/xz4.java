package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xz4 extends wz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public /* final */ NestedScrollView E;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362033, 1);
        H.put(2131363215, 2);
        H.put(2131363408, 3);
        H.put(2131362666, 4);
        H.put(2131362072, 5);
        H.put(2131363285, 6);
        H.put(2131363417, 7);
        H.put(2131362674, 8);
        H.put(2131362055, 9);
        H.put(2131363248, 10);
        H.put(2131363415, 11);
        H.put(2131362673, 12);
        H.put(2131363067, 13);
        H.put(2131362245, 14);
    }
    */

    @DexIgnore
    public xz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 15, G, H));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public xz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[5], (FlexibleButton) objArr[14], (ImageView) objArr[4], (ImageView) objArr[12], (ImageView) objArr[8], (OverviewSleepDayChart) objArr[13], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[11], (View) objArr[7]);
        this.F = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.E = nestedScrollView;
        nestedScrollView.setTag(null);
        a(view);
        f();
    }
}
