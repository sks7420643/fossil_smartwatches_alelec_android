package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cy3<K, V> extends wx3<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends b<K, V> {
        @DexIgnore
        public /* final */ transient cy3<K, V> b;

        @DexIgnore
        public a(K k, V v, cy3<K, V> cy3, cy3<K, V> cy32) {
            super(k, v, cy3);
            this.b = cy32;
        }

        @DexIgnore
        @Override // com.fossil.cy3
        public cy3<K, V> getNextInValueBucket() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends cy3<K, V> {
        @DexIgnore
        public /* final */ transient cy3<K, V> a;

        @DexIgnore
        public b(K k, V v, cy3<K, V> cy3) {
            super(k, v);
            this.a = cy3;
        }

        @DexIgnore
        @Override // com.fossil.cy3
        public final cy3<K, V> getNextInKeyBucket() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.cy3
        public final boolean isReusable() {
            return false;
        }
    }

    @DexIgnore
    public cy3(K k, V v) {
        super(k, v);
        bx3.a(k, v);
    }

    @DexIgnore
    public static <K, V> cy3<K, V>[] createEntryArray(int i) {
        return new cy3[i];
    }

    @DexIgnore
    public cy3<K, V> getNextInKeyBucket() {
        return null;
    }

    @DexIgnore
    public cy3<K, V> getNextInValueBucket() {
        return null;
    }

    @DexIgnore
    public boolean isReusable() {
        return true;
    }

    @DexIgnore
    public cy3(cy3<K, V> cy3) {
        super(cy3.getKey(), cy3.getValue());
    }
}
