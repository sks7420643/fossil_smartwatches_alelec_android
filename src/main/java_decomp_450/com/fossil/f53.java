package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f53 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<f53> CREATOR; // = new r53();
    @DexIgnore
    public /* final */ List<LocationRequest> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public p53 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<LocationRequest> a; // = new ArrayList<>();
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public final a a(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.a.add(locationRequest);
            }
            return this;
        }

        @DexIgnore
        public final f53 a() {
            return new f53(this.a, this.b, this.c, null);
        }
    }

    @DexIgnore
    public f53(List<LocationRequest> list, boolean z, boolean z2, p53 p53) {
        this.a = list;
        this.b = z;
        this.c = z2;
        this.d = p53;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.c(parcel, 1, Collections.unmodifiableList(this.a), false);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 5, (Parcelable) this.d, i, false);
        k72.a(parcel, a2);
    }
}
