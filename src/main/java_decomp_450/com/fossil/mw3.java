package com.fossil;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw3<T> extends hw3<T> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ T reference;

    @DexIgnore
    public mw3(T t) {
        this.reference = t;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public Set<T> asSet() {
        return Collections.singleton(this.reference);
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public boolean equals(Object obj) {
        if (obj instanceof mw3) {
            return this.reference.equals(((mw3) obj).reference);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T get() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public boolean isPresent() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T or(T t) {
        jw3.a(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T orNull() {
        return this.reference;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public String toString() {
        return "Optional.of(" + ((Object) this.reference) + ")";
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public <V> hw3<V> transform(cw3<? super T, V> cw3) {
        V apply = cw3.apply(this.reference);
        jw3.a(apply, "the Function passed to Optional.transform() must not return null.");
        return new mw3(apply);
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public hw3<T> or(hw3<? extends T> hw3) {
        jw3.a(hw3);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.hw3
    public T or(nw3<? extends T> nw3) {
        jw3.a(nw3);
        return this.reference;
    }
}
