package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yb2 extends IInterface {
    @DexIgnore
    ab2 a(ab2 ab2, String str, int i, ab2 ab22) throws RemoteException;

    @DexIgnore
    ab2 b(ab2 ab2, String str, int i, ab2 ab22) throws RemoteException;
}
