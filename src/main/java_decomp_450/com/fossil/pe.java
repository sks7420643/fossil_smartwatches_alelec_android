package com.fossil;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class pe<Params, Progress, Result> {
    public static final ThreadFactory f = new a();
    public static final BlockingQueue<Runnable> g = new LinkedBlockingQueue(10);
    public static final Executor h = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, g, f);
    public static f i;
    public final h<Params, Result> a = new b();
    public final FutureTask<Result> b = new c(this.a);
    public volatile g c = g.PENDING;
    public final AtomicBoolean d = new AtomicBoolean();
    public final AtomicBoolean e = new AtomicBoolean();

    public static class a implements ThreadFactory {
        public final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.a.getAndIncrement());
        }
    }

    public class b extends h<Params, Result> {
        public b() {
        }

        @Override // java.util.concurrent.Callable
        public Result call() throws Exception {
            pe.this.e.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = (Result) pe.this.a((Object[]) ((h) this).a);
                Binder.flushPendingCommands();
                pe.this.d(result);
                return result;
            } catch (Throwable th) {
                pe.this.d(result);
                throw th;
            }
        }
    }

    public class c extends FutureTask<Result> {
        public c(Callable callable) {
            super(callable);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: com.fossil.pe */
        /* JADX WARN: Multi-variable type inference failed */
        public void done() {
            try {
                pe.this.e(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
            } catch (CancellationException unused) {
                pe.this.e(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    }

    public static /* synthetic */ class d {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /*
        static {
            /*
                com.fossil.pe$g[] r0 = com.fossil.pe.g.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.pe.d.a = r0
                com.fossil.pe$g r1 = com.fossil.pe.g.RUNNING     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.pe.d.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.pe$g r1 = com.fossil.pe.g.FINISHED     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pe.d.<clinit>():void");
        }
        */
    }

    public static class e<Data> {
        public final pe a;
        public final Data[] b;

        public e(pe peVar, Data... dataArr) {
            this.a = peVar;
            this.b = dataArr;
        }
    }

    public static class f extends Handler {
        public f() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            e eVar = (e) message.obj;
            int i = message.what;
            if (i == 1) {
                eVar.a.a(eVar.b[0]);
            } else if (i == 2) {
                eVar.a.b((Object[]) eVar.b);
            }
        }
    }

    public enum g {
        PENDING,
        RUNNING,
        FINISHED
    }

    public static abstract class h<Params, Result> implements Callable<Result> {
        public Params[] a;
    }

    public static Handler d() {
        f fVar;
        synchronized (pe.class) {
            if (i == null) {
                i = new f();
            }
            fVar = i;
        }
        return fVar;
    }

    public abstract Result a(Params... paramsArr);

    public final boolean a() {
        return this.d.get();
    }

    public void b() {
    }

    public void b(Result result) {
        b();
    }

    public void b(Progress... progressArr) {
    }

    public void c() {
    }

    public void c(Result result) {
    }

    public void e(Result result) {
        if (!this.e.get()) {
            d(result);
        }
    }

    public final boolean a(boolean z) {
        this.d.set(true);
        return this.b.cancel(z);
    }

    public final pe<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.c != g.PENDING) {
            int i2 = d.a[this.c.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 != 2) {
                throw new IllegalStateException("We should never reach this state");
            } else {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        } else {
            this.c = g.RUNNING;
            c();
            this.a.a = paramsArr;
            executor.execute(this.b);
            return this;
        }
    }

    public Result d(Result result) {
        d().obtainMessage(1, new e(this, result)).sendToTarget();
        return result;
    }

    public void a(Result result) {
        if (a()) {
            b(result);
        } else {
            c(result);
        }
        this.c = g.FINISHED;
    }
}
