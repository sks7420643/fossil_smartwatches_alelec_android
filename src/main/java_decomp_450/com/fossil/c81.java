package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c81 implements Parcelable.Creator<x91> {
    @DexIgnore
    public /* synthetic */ c81(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public x91 createFromParcel(Parcel parcel) {
        return new x91(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public x91[] newArray(int i) {
        return new x91[i];
    }
}
