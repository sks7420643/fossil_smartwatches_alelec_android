package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lc5 extends mc5 {
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public lc5(String str, int i) {
        super(str);
        this.b = i;
    }

    @DexIgnore
    public int b() {
        return this.b;
    }
}
