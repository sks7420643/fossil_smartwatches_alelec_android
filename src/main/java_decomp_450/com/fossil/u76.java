package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u76 {
    @DexIgnore
    public /* final */ o76 a;
    @DexIgnore
    public /* final */ f86 b;
    @DexIgnore
    public /* final */ z76 c;

    @DexIgnore
    public u76(o76 o76, f86 f86, z76 z76) {
        ee7.b(o76, "mActiveTimeOverviewDayView");
        ee7.b(f86, "mActiveTimeOverviewWeekView");
        ee7.b(z76, "mActiveTimeOverviewMonthView");
        this.a = o76;
        this.b = f86;
        this.c = z76;
    }

    @DexIgnore
    public final o76 a() {
        return this.a;
    }

    @DexIgnore
    public final z76 b() {
        return this.c;
    }

    @DexIgnore
    public final f86 c() {
        return this.b;
    }
}
