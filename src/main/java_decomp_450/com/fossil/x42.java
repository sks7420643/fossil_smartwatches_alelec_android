package com.fossil;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.a12;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x42 extends a52 {
    @DexIgnore
    public /* final */ SparseArray<a> f; // = new SparseArray<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements a12.c {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ a12 b;
        @DexIgnore
        public /* final */ a12.c c;

        @DexIgnore
        public a(int i, a12 a12, a12.c cVar) {
            this.a = i;
            this.b = a12;
            this.c = cVar;
            a12.a(this);
        }

        @DexIgnore
        @Override // com.fossil.a22
        public final void a(i02 i02) {
            String valueOf = String.valueOf(i02);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            x42.this.b(i02, this.a);
        }
    }

    @DexIgnore
    public x42(x12 x12) {
        super(x12);
        ((LifecycleCallback) this).a.a("AutoManageHelper", this);
    }

    @DexIgnore
    public static x42 b(w12 w12) {
        x12 a2 = LifecycleCallback.a(w12);
        x42 x42 = (x42) a2.a("AutoManageHelper", x42.class);
        if (x42 != null) {
            return x42;
        }
        return new x42(a2);
    }

    @DexIgnore
    public final void a(int i, a12 a12, a12.c cVar) {
        a72.a(a12, "GoogleApiClient instance cannot be null");
        boolean z = this.f.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        a72.b(z, sb.toString());
        z42 z42 = ((a52) this).c.get();
        boolean z2 = ((a52) this).b;
        String valueOf = String.valueOf(z42);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.f.put(i, new a(i, a12, cVar));
        if (((a52) this).b && z42 == null) {
            String valueOf2 = String.valueOf(a12);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            a12.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.a52, com.google.android.gms.common.api.internal.LifecycleCallback
    public void d() {
        super.d();
        boolean z = ((a52) this).b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (((a52) this).c.get() == null) {
            for (int i = 0; i < this.f.size(); i++) {
                a b = b(i);
                if (b != null) {
                    b.b.c();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a52, com.google.android.gms.common.api.internal.LifecycleCallback
    public void e() {
        super.e();
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.b.d();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a52
    public final void f() {
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.b.c();
            }
        }
    }

    @DexIgnore
    public final a b(int i) {
        if (this.f.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.f;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    @DexIgnore
    public final void a(int i) {
        a aVar = this.f.get(i);
        this.f.remove(i);
        if (aVar != null) {
            aVar.b.b(aVar);
            aVar.b.d();
        }
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                printWriter.append((CharSequence) str).append("GoogleApiClient #").print(b.a);
                printWriter.println(":");
                b.b.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.a52
    public final void a(i02 i02, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.f.get(i);
        if (aVar != null) {
            a(i);
            a12.c cVar = aVar.c;
            if (cVar != null) {
                cVar.a(i02);
            }
        }
    }
}
