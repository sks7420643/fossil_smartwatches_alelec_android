package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.fossil.f60;
import com.fossil.l60;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm0 {
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<km1> a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<km1> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<km1> c; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<km1> d; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ Handler e;
    @DexIgnore
    public static /* final */ BroadcastReceiver f; // = new xl1();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new al0();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new cj0();
    @DexIgnore
    public static /* final */ xm0 i; // = new xm0();

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            e = new Handler(myLooper);
            BroadcastReceiver broadcastReceiver = f;
            String[] strArr = {"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"};
            Context a2 = u31.g.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(strArr[i2]);
                }
                qe.a(a2).a(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = g;
            String[] strArr2 = {"com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED"};
            Context a3 = u31.g.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(strArr2[i3]);
                }
                qe.a(a3).a(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = h;
            String[] strArr3 = {"com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED"};
            Context a4 = u31.g.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(strArr3[i4]);
                }
                qe.a(a4).a(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final long a(boolean z) {
        return 0;
    }

    @DexIgnore
    public final void a(f60.c cVar) {
        if (zj1.a[cVar.ordinal()] == 1) {
            for (T t : a) {
                xm0 xm0 = i;
                ee7.a((Object) t, "it");
                xm0.a((km1) t);
            }
            for (T t2 : b) {
                xm0 xm02 = i;
                ee7.a((Object) t2, "it");
                a(xm02, t2, 0, 2);
            }
        }
    }

    @DexIgnore
    public final boolean b(km1 km1) {
        return a.contains(km1);
    }

    @DexIgnore
    public final boolean c(km1 km1) {
        return b.contains(km1);
    }

    @DexIgnore
    public final void d(km1 km1) {
        b.remove(km1);
    }

    @DexIgnore
    public final boolean e(km1 km1) {
        a.add(km1);
        a(km1);
        return true;
    }

    @DexIgnore
    public final boolean f(km1 km1) {
        a.remove(km1);
        return true;
    }

    @DexIgnore
    public final void a(km1 km1, l60.d dVar) {
        int i2 = zj1.b[dVar.ordinal()];
        if (i2 == 2) {
            a(km1, 0);
        } else if (i2 == 3) {
            qr0.b.b("HID_EXPONENT_BACK_OFF_TAG");
            a(km1, 0);
        }
    }

    @DexIgnore
    public final void a(km1 km1) {
        if (a.contains(km1) && km1.u == l60.c.DISCONNECTED) {
            e.postDelayed(new wn1(km1), 200);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(xm0 xm0, km1 km1, long j, int i2) {
        if ((i2 & 2) != 0) {
            j = 0;
        }
        xm0.a(km1, j);
    }

    @DexIgnore
    public final void a(km1 km1, long j) {
        e.postDelayed(new fh0(km1), j);
    }
}
