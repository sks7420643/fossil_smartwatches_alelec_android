package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph6 implements Factory<oh6> {
    @DexIgnore
    public static oh6 a(WeakReference<nh6> weakReference, PortfolioApp portfolioApp, mn5 mn5, on5 on5, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, qn5 qn5, fp4 fp4, ro4 ro4, ch5 ch5) {
        return new oh6(weakReference, portfolioApp, mn5, on5, deviceRepository, userRepository, summariesRepository, sleepSummariesRepository, qn5, fp4, ro4, ch5);
    }
}
