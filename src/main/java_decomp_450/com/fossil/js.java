package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js extends is {
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public js(boolean z) {
        super(null);
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.is
    public boolean a(rt rtVar) {
        ee7.b(rtVar, "size");
        return this.b;
    }
}
