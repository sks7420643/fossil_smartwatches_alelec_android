package com.fossil;

import android.os.Bundle;
import com.fossil.h37;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g37 extends u27 {
    @DexIgnore
    public h37 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public g37(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.u27
    public void a(Bundle bundle) {
        super.a(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = h37.a.a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.u27
    public boolean a() {
        h37 h37 = this.c;
        if (h37 == null) {
            return false;
        }
        return h37.a();
    }

    @DexIgnore
    @Override // com.fossil.u27
    public int b() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.u27
    public void b(Bundle bundle) {
        Bundle a = h37.a.a(this.c);
        super.b(a);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(a);
    }
}
