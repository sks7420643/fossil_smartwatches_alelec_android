package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy5 implements Factory<ny5> {
    @DexIgnore
    public static ny5 a(hy5 hy5, int i, ArrayList<bt5> arrayList, LoaderManager loaderManager, rl4 rl4, qy5 qy5) {
        return new ny5(hy5, i, arrayList, loaderManager, rl4, qy5);
    }
}
