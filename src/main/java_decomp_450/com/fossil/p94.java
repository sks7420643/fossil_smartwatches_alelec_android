package com.fossil;

import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p94 implements fo3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public p94(Context context, Intent intent) {
        this.a = context;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.fo3
    public final Object then(no3 no3) {
        return s94.a(this.a, this.b, no3);
    }
}
