package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bd implements Closeable, yi7 {
    @DexIgnore
    public /* final */ ib7 a;

    @DexIgnore
    public bd(ib7 ib7) {
        ee7.b(ib7, "context");
        this.a = ib7;
    }

    @DexIgnore
    @Override // com.fossil.yi7
    public ib7 a() {
        return this.a;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        nk7.a(a(), null, 1, null);
    }
}
