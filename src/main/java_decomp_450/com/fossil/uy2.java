package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uy2<T, B> {
    @DexIgnore
    public abstract B a();

    @DexIgnore
    public abstract T a(Object obj);

    @DexIgnore
    public abstract void a(B b, int i, long j);

    @DexIgnore
    public abstract void a(B b, int i, tu2 tu2);

    @DexIgnore
    public abstract void a(T t, oz2 oz2) throws IOException;

    @DexIgnore
    public abstract void a(Object obj, T t);

    @DexIgnore
    public abstract void b(Object obj);

    @DexIgnore
    public abstract void b(T t, oz2 oz2) throws IOException;

    @DexIgnore
    public abstract void b(Object obj, B b);

    @DexIgnore
    public abstract int c(T t);

    @DexIgnore
    public abstract T c(T t, T t2);

    @DexIgnore
    public abstract int d(T t);
}
