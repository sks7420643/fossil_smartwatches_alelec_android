package com.fossil;

import com.fossil.xi;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ii implements xi.c {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ xi.c c;

    @DexIgnore
    public ii(String str, File file, xi.c cVar) {
        this.a = str;
        this.b = file;
        this.c = cVar;
    }

    @DexIgnore
    @Override // com.fossil.xi.c
    public xi create(xi.b bVar) {
        return new hi(bVar.a, this.a, this.b, bVar.c.a, this.c.create(bVar));
    }
}
