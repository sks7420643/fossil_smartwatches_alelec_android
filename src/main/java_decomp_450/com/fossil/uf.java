package com.fossil;

import com.fossil.lf;
import com.fossil.pf;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uf<T> extends lf<Integer, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Value> extends jf<Integer, Value> {
        @DexIgnore
        public /* final */ uf<Value> a;

        @DexIgnore
        public a(uf<Value> ufVar) {
            this.a = ufVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void dispatchLoadInitial(Integer num, int i, int i2, boolean z, Executor executor, pf.a<Value> aVar) {
            Integer num2;
            if (num == null) {
                num2 = 0;
            } else {
                i = Math.max(i / i2, 2) * i2;
                num2 = Integer.valueOf(Math.max(0, ((num.intValue() - (i / 2)) / i2) * i2));
            }
            this.a.dispatchLoadInitial(false, num2.intValue(), i, i2, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.lf
        public void addInvalidatedCallback(lf.c cVar) {
            this.a.addInvalidatedCallback(cVar);
        }

        @DexIgnore
        @Override // com.fossil.jf
        public void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
            this.a.dispatchLoadRange(1, i + 1, i2, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.jf
        public void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
            int i3 = i - 1;
            if (i3 < 0) {
                this.a.dispatchLoadRange(2, i3, 0, executor, aVar);
                return;
            }
            int min = Math.min(i2, i3 + 1);
            this.a.dispatchLoadRange(2, (i3 - min) + 1, min, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.lf
        public void invalidate() {
            this.a.invalidate();
        }

        @DexIgnore
        @Override // com.fossil.lf
        public boolean isInvalid() {
            return this.a.isInvalid();
        }

        @DexIgnore
        @Override // com.fossil.lf
        public <ToValue> lf<Integer, ToValue> map(t3<Value, ToValue> t3Var) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.fossil.lf
        public <ToValue> lf<Integer, ToValue> mapByPage(t3<List<Value>, List<ToValue>> t3Var) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.fossil.lf
        public void removeInvalidatedCallback(lf.c cVar) {
            this.a.removeInvalidatedCallback(cVar);
        }

        @DexIgnore
        @Override // com.fossil.jf
        public Integer getKey(int i, Value value) {
            return Integer.valueOf(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<T> {
        @DexIgnore
        public abstract void a(List<T> list, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> extends b<T> {
        @DexIgnore
        public /* final */ lf.d<T> a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public c(uf ufVar, boolean z, int i, pf.a<T> aVar) {
            this.a = new lf.d<>(ufVar, 0, null, aVar);
            this.b = z;
            this.c = i;
            if (i < 1) {
                throw new IllegalArgumentException("Page size must be non-negative");
            }
        }

        @DexIgnore
        @Override // com.fossil.uf.b
        public void a(List<T> list, int i, int i2) {
            if (!this.a.a()) {
                lf.d.a(list, i, i2);
                if (list.size() + i != i2 && list.size() % this.c != 0) {
                    throw new IllegalArgumentException("PositionalDataSource requires initial load size to be a multiple of page size to support internal tiling. loadSize " + list.size() + ", position " + i + ", totalCount " + i2 + ", pageSize " + this.c);
                } else if (this.b) {
                    this.a.a(new pf<>(list, i, (i2 - i) - list.size(), 0));
                } else {
                    this.a.a(new pf<>(list, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(int i, int i2, int i3, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {
        @DexIgnore
        public abstract void a(List<T> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<T> extends e<T> {
        @DexIgnore
        public lf.d<T> a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(uf ufVar, int i, int i2, Executor executor, pf.a<T> aVar) {
            this.a = new lf.d<>(ufVar, i, executor, aVar);
            this.b = i2;
        }

        @DexIgnore
        @Override // com.fossil.uf.e
        public void a(List<T> list) {
            if (!this.a.a()) {
                this.a.a(new pf<>(list, 0, 0, this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public g(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    @DexIgnore
    public static int computeInitialLoadPosition(d dVar, int i) {
        int i2 = dVar.a;
        int i3 = dVar.b;
        int i4 = dVar.c;
        return Math.max(0, Math.min(((((i - i3) + i4) - 1) / i4) * i4, (i2 / i4) * i4));
    }

    @DexIgnore
    public static int computeInitialLoadSize(d dVar, int i, int i2) {
        return Math.min(i2 - i, dVar.b);
    }

    @DexIgnore
    public final void dispatchLoadInitial(boolean z, int i, int i2, int i3, Executor executor, pf.a<T> aVar) {
        c cVar = new c(this, z, i3, aVar);
        loadInitial(new d(i, i2, i3, z), cVar);
        cVar.a.a(executor);
    }

    @DexIgnore
    public final void dispatchLoadRange(int i, int i2, int i3, Executor executor, pf.a<T> aVar) {
        f fVar = new f(this, i, i2, executor, aVar);
        if (i3 == 0) {
            fVar.a(Collections.emptyList());
        } else {
            loadRange(new g(i2, i3), fVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isContiguous() {
        return false;
    }

    @DexIgnore
    public abstract void loadInitial(d dVar, b<T> bVar);

    @DexIgnore
    public abstract void loadRange(g gVar, e<T> eVar);

    @DexIgnore
    public jf<Integer, T> wrapAsContiguousWithoutPlaceholders() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.lf
    public final <V> uf<V> map(t3<T, V> t3Var) {
        return mapByPage((t3) lf.createListFunction(t3Var));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public final <V> uf<V> mapByPage(t3<List<T>, List<V>> t3Var) {
        return new zf(this, t3Var);
    }
}
