package com.fossil;

import com.fossil.jx;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i20 implements jx<ByteBuffer> {
    @DexIgnore
    public /* final */ ByteBuffer a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements jx.a<ByteBuffer> {
        @DexIgnore
        @Override // com.fossil.jx.a
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }

        @DexIgnore
        public jx<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new i20(byteBuffer);
        }
    }

    @DexIgnore
    public i20(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @DexIgnore
    @Override // com.fossil.jx
    public void a() {
    }

    @DexIgnore
    @Override // com.fossil.jx
    public ByteBuffer b() {
        this.a.position(0);
        return this.a;
    }
}
