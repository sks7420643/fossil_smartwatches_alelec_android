package com.fossil;

import androidx.loader.app.LoaderManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jy5 {
    @DexIgnore
    public /* final */ hy5 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<bt5> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public jy5(hy5 hy5, int i, ArrayList<bt5> arrayList, LoaderManager loaderManager) {
        ee7.b(hy5, "mView");
        ee7.b(loaderManager, "mLoaderManager");
        this.a = hy5;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<bt5> a() {
        ArrayList<bt5> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final hy5 d() {
        return this.a;
    }
}
