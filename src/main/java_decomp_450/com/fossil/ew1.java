package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ew1 implements Runnable {
    @DexIgnore
    public /* final */ jw1 a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Runnable d;

    @DexIgnore
    public ew1(jw1 jw1, pu1 pu1, int i, Runnable runnable) {
        this.a = jw1;
        this.b = pu1;
        this.c = i;
        this.d = runnable;
    }

    @DexIgnore
    public static Runnable a(jw1 jw1, pu1 pu1, int i, Runnable runnable) {
        return new ew1(jw1, pu1, i, runnable);
    }

    @DexIgnore
    public void run() {
        jw1.a(this.a, this.b, this.c, this.d);
    }
}
