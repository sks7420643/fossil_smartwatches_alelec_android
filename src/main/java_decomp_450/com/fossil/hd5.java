package com.fossil;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd5 {
    @DexIgnore
    public static kd5 a(Context context) {
        return (kd5) aw.d(context);
    }

    @DexIgnore
    public static kd5 a(Fragment fragment) {
        return (kd5) aw.a(fragment);
    }

    @DexIgnore
    public static kd5 a(View view) {
        return (kd5) aw.a(view);
    }
}
