package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pd6 {
    @DexIgnore
    public /* final */ jd6 a;
    @DexIgnore
    public /* final */ ae6 b;
    @DexIgnore
    public /* final */ ud6 c;

    @DexIgnore
    public pd6(jd6 jd6, ae6 ae6, ud6 ud6) {
        ee7.b(jd6, "mSleepOverviewDayView");
        ee7.b(ae6, "mSleepOverviewWeekView");
        ee7.b(ud6, "mSleepOverviewMonthView");
        this.a = jd6;
        this.b = ae6;
        this.c = ud6;
    }

    @DexIgnore
    public final jd6 a() {
        return this.a;
    }

    @DexIgnore
    public final ud6 b() {
        return this.c;
    }

    @DexIgnore
    public final ae6 c() {
        return this.b;
    }
}
