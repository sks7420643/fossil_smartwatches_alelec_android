package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv7<T> implements tu7<T, RequestBody> {
    @DexIgnore
    public static /* final */ ho7 c; // = ho7.a("application/json; charset=UTF-8");
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public kv7(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    @Override // com.fossil.tu7
    public RequestBody a(T t) throws IOException {
        yq7 yq7 = new yq7();
        JsonWriter a2 = this.a.a((Writer) new OutputStreamWriter(yq7.s(), d));
        this.b.write(a2, t);
        a2.close();
        return RequestBody.a(c, yq7.o());
    }
}
