package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t5 extends i5 {
    @DexIgnore
    public ArrayList<i5> k0; // = new ArrayList<>();

    @DexIgnore
    @Override // com.fossil.i5
    public void E() {
        this.k0.clear();
        super.E();
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void I() {
        super.I();
        ArrayList<i5> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                i5 i5Var = this.k0.get(i);
                i5Var.b(h(), i());
                if (!(i5Var instanceof j5)) {
                    i5Var.I();
                }
            }
        }
    }

    @DexIgnore
    public j5 K() {
        i5 l = l();
        j5 j5Var = this instanceof j5 ? (j5) this : null;
        while (l != null) {
            i5 l2 = l.l();
            if (l instanceof j5) {
                j5Var = (j5) l;
            }
            l = l2;
        }
        return j5Var;
    }

    @DexIgnore
    public void L() {
        I();
        ArrayList<i5> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                i5 i5Var = this.k0.get(i);
                if (i5Var instanceof t5) {
                    ((t5) i5Var).L();
                }
            }
        }
    }

    @DexIgnore
    public void M() {
        this.k0.clear();
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void a(w4 w4Var) {
        super.a(w4Var);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).a(w4Var);
        }
    }

    @DexIgnore
    public void b(i5 i5Var) {
        this.k0.add(i5Var);
        if (i5Var.l() != null) {
            ((t5) i5Var.l()).c(i5Var);
        }
        i5Var.a((i5) this);
    }

    @DexIgnore
    public void c(i5 i5Var) {
        this.k0.remove(i5Var);
        i5Var.a((i5) null);
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void b(int i, int i2) {
        super.b(i, i2);
        int size = this.k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.k0.get(i3).b(p(), q());
        }
    }
}
