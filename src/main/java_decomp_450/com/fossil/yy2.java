package com.fossil;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy2 implements ListIterator<String> {
    @DexIgnore
    public ListIterator<String> a; // = this.c.a.listIterator(this.b);
    @DexIgnore
    public /* final */ /* synthetic */ int b;
    @DexIgnore
    public /* final */ /* synthetic */ vy2 c;

    @DexIgnore
    public yy2(vy2 vy2, int i) {
        this.c = vy2;
        this.b = i;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.a.hasPrevious();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator, java.util.ListIterator
    public final /* synthetic */ String next() {
        return this.a.next();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.a.nextIndex();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.a.previous();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.a.previousIndex();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }
}
