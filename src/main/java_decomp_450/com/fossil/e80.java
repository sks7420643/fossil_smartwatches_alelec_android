package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ int f; // = 65535;
    @DexIgnore
    public static /* final */ int g; // = 65535;
    @DexIgnore
    public static /* final */ int h; // = 65535;
    @DexIgnore
    public static /* final */ int i; // = 65535;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<e80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final e80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 12) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new e80(yz0.b(order.getShort(1)), yz0.b(order.getShort(3)), yz0.b(order.getShort(5)), yz0.b(order.getShort(7)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 12"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public e80 createFromParcel(Parcel parcel) {
            return new e80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public e80[] newArray(int i) {
            return new e80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public e80 m13createFromParcel(Parcel parcel) {
            return new e80(parcel, null);
        }
    }

    /*
    static {
        ve7 ve7 = ve7.a;
    }
    */

    @DexIgnore
    public e80(int i2, int i3, int i4, int i5) throws IllegalArgumentException {
        super(o80.CYCLING_CADENCE);
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).put((byte) 2).putShort((short) this.b).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).putShort(0).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int i2 = f;
        int i3 = this.b;
        boolean z = true;
        if (i3 >= 0 && i2 >= i3) {
            int i4 = g;
            int i5 = this.c;
            if (i5 >= 0 && i4 >= i5) {
                int i6 = h;
                int i7 = this.d;
                if (i7 >= 0 && i6 >= i7) {
                    int i8 = i;
                    int i9 = this.e;
                    if (i9 < 0 || i8 < i9) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder b2 = yh0.b("cog(");
                        b2.append(this.e);
                        b2.append(") is out of range ");
                        b2.append("[0, ");
                        throw new IllegalArgumentException(yh0.a(b2, i, "]."));
                    }
                    return;
                }
                StringBuilder b3 = yh0.b("chainring(");
                b3.append(this.d);
                b3.append(") is out of range ");
                b3.append("[0, ");
                throw new IllegalArgumentException(yh0.a(b3, h, "]."));
            }
            StringBuilder b4 = yh0.b("tire size(");
            b4.append(this.c);
            b4.append(") is out of range ");
            b4.append("[0, ");
            throw new IllegalArgumentException(yh0.a(b4, g, "]."));
        }
        StringBuilder b5 = yh0.b("diameter(");
        b5.append(this.b);
        b5.append(") is out of range ");
        b5.append("[0, ");
        throw new IllegalArgumentException(yh0.a(b5, f, "]."));
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(e80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            e80 e80 = (e80) obj;
            return this.b == e80.b && this.c == e80.c && this.d == e80.d && this.e == e80.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CyclingCadenceConfig");
    }

    @DexIgnore
    public final int getChainring() {
        return this.d;
    }

    @DexIgnore
    public final int getCog() {
        return this.e;
    }

    @DexIgnore
    public final int getDiameterInMillimeter() {
        return this.b;
    }

    @DexIgnore
    public final int getTireSizeInMillimeter() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.A5, Integer.valueOf(this.b));
            yz0.a(jSONObject, r51.B5, Integer.valueOf(this.c));
            yz0.a(jSONObject, r51.C5, Integer.valueOf(this.d));
            yz0.a(jSONObject, r51.D5, Integer.valueOf(this.e));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ e80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        e();
    }
}
