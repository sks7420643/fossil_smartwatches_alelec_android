package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mm2> CREATOR; // = new nm2();
    @DexIgnore
    public static /* final */ List<i62> d; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ w53 e; // = new w53();
    @DexIgnore
    public w53 a;
    @DexIgnore
    public List<i62> b;
    @DexIgnore
    public String c;

    @DexIgnore
    public mm2(w53 w53, List<i62> list, String str) {
        this.a = w53;
        this.b = list;
        this.c = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof mm2)) {
            return false;
        }
        mm2 mm2 = (mm2) obj;
        return y62.a(this.a, mm2.a) && y62.a(this.b, mm2.b) && y62.a(this.c, mm2.c);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) this.a, i, false);
        k72.c(parcel, 2, this.b, false);
        k72.a(parcel, 3, this.c, false);
        k72.a(parcel, a2);
    }
}
