package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"AddedAbstractMethod"})
public abstract class rm {
    @DexIgnore
    public static rm a(Context context) {
        return dn.a(context);
    }

    @DexIgnore
    public abstract lm a(String str, dm dmVar, List<km> list);

    @DexIgnore
    public abstract lm a(List<? extends sm> list);

    @DexIgnore
    public static void a(Context context, zl zlVar) {
        dn.a(context, zlVar);
    }

    @DexIgnore
    public final lm a(sm smVar) {
        return a(Collections.singletonList(smVar));
    }

    @DexIgnore
    public lm a(String str, dm dmVar, km kmVar) {
        return a(str, dmVar, Collections.singletonList(kmVar));
    }
}
