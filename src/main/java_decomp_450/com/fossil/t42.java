package com.fossil;

import android.os.RemoteException;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t42 extends e42<Void> {
    @DexIgnore
    public /* final */ d42 c;

    @DexIgnore
    public t42(d42 d42, oo3<Void> oo3) {
        super(3, oo3);
        this.c = d42;
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final /* bridge */ /* synthetic */ void a(j22 j22, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final k02[] b(u12.a<?> aVar) {
        return this.c.a.c();
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final boolean c(u12.a<?> aVar) {
        return this.c.a.d();
    }

    @DexIgnore
    @Override // com.fossil.e42
    public final void d(u12.a<?> aVar) throws RemoteException {
        this.c.a.a(aVar.r(), ((e42) this).b);
        if (this.c.a.b() != null) {
            aVar.k().put(this.c.a.b(), this.c);
        }
    }
}
