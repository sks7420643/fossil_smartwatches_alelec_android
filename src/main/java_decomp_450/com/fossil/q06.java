package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.so5;
import com.fossil.zl4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q06 extends go5 implements a16, zl4.b {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<ky4> f;
    @DexIgnore
    public z06 g;
    @DexIgnore
    public so5 h;
    @DexIgnore
    public zl4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final q06 a() {
            return new q06();
        }

        @DexIgnore
        public final String b() {
            return q06.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements so5.b {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;

        @DexIgnore
        public b(q06 q06) {
            this.a = q06;
        }

        @DexIgnore
        @Override // com.fossil.so5.b
        public void a(String str) {
            ee7.b(str, "address");
            q06.d(this.a).a(nh7.d((CharSequence) str).toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ ky4 b;

        @DexIgnore
        public c(View view, ky4 ky4) {
            this.a = view;
            this.b = ky4;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.x.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ky4 ky4 = this.b;
                ee7.a((Object) ky4, "binding");
                ky4.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = q06.r.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.b.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;

        @DexIgnore
        public d(q06 q06, ky4 ky4) {
            this.a = q06;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            zl4 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText(null);
                ee7.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = r06.r.b();
                local.i(b, "Autocomplete item selected: " + ((Object) fullText));
                this.a.j = fullText.toString();
                if (this.a.j != null) {
                    String b2 = this.a.j;
                    if (b2 != null) {
                        if (b2.length() > 0) {
                            z06 d = q06.d(this.a);
                            String b3 = this.a.j;
                            if (b3 == null) {
                                ee7.a();
                                throw null;
                            } else if (b3 != null) {
                                d.a(nh7.d((CharSequence) b3).toString());
                            } else {
                                throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ky4 a;

        @DexIgnore
        public e(q06 q06, ky4 ky4) {
            this.a = ky4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.a.r;
            ee7.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;

        @DexIgnore
        public f(q06 q06, ky4 ky4) {
            this.a = q06;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            ee7.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.e1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;

        @DexIgnore
        public g(q06 q06) {
            this.a = q06;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;
        @DexIgnore
        public /* final */ /* synthetic */ ky4 b;

        @DexIgnore
        public h(q06 q06, ky4 ky4) {
            this.a = q06;
            this.b = ky4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j = "";
            this.b.q.setText("");
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q06 a;

        @DexIgnore
        public i(q06 q06) {
            this.a = q06;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
            Editable text;
            MapPickerActivity.a aVar = MapPickerActivity.y;
            q06 q06 = this.a;
            ky4 ky4 = (ky4) q06.c(q06).a();
            if (ky4 == null || (flexibleAutoCompleteTextView = ky4.q) == null || (text = flexibleAutoCompleteTextView.getText()) == null || (str = text.toString()) == null) {
                str = "";
            }
            MapPickerActivity.a.a(aVar, q06, 0.0d, 0.0d, str, 6, null);
        }
    }

    /*
    static {
        String simpleName = q06.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 c(q06 q06) {
        qw6<ky4> qw6 = q06.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ z06 d(q06 q06) {
        z06 z06 = q06.g;
        if (z06 != null) {
            return z06;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Y(String str) {
        ee7.b(str, "address");
        qw6<ky4> qw6 = this.f;
        if (qw6 != null) {
            ky4 a2 = qw6.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.j = str;
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.a16
    public void e(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        ee7.b(list, "recentSearchedList");
        f1();
        if (!list.isEmpty()) {
            so5 so5 = this.h;
            if (so5 != null) {
                so5.a(ea7.d((Collection) list));
            }
            qw6<ky4> qw6 = this.f;
            if (qw6 != null) {
                ky4 a2 = qw6.a();
                if (a2 != null && (linearLayout2 = a2.z) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<ky4> qw62 = this.f;
        if (qw62 != null) {
            ky4 a3 = qw62.a();
            if (a3 != null && (linearLayout = a3.z) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        qw6<ky4> qw6 = this.f;
        String str = null;
        if (qw6 == null) {
            ee7.d("mBinding");
            throw null;
        } else if (qw6.a() == null) {
            return true;
        } else {
            z06 z06 = this.g;
            if (z06 != null) {
                String str2 = this.j;
                if (str2 != null) {
                    if (str2 != null) {
                        str = nh7.d((CharSequence) str2).toString();
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                z06.a(str);
                return true;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void f1() {
        qw6<ky4> qw6 = this.f;
        if (qw6 != null) {
            ky4 a2 = qw6.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.z;
                ee7.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g1() {
        qw6<ky4> qw6 = this.f;
        if (qw6 != null) {
            ky4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.g0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886354, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                ee7.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.z;
                ee7.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zl4.b
    public void m(boolean z) {
        qw6<ky4> qw6 = this.f;
        if (qw6 != null) {
            ky4 a2 = qw6.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.j = null;
                        g1();
                        return;
                    }
                }
                f1();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                Y(string);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ky4 ky4 = (ky4) qb.a(layoutInflater, 2131558516, viewGroup, false, a1());
        String b2 = eh5.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            ky4.x.setBackgroundColor(Color.parseColor(b2));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = ky4.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(v6.c(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new d(this, ky4));
        flexibleAutoCompleteTextView.addTextChangedListener(new e(this, ky4));
        flexibleAutoCompleteTextView.setOnKeyListener(new f(this, ky4));
        ky4.u.setOnClickListener(new g(this));
        ky4.r.setOnClickListener(new h(this, ky4));
        ky4.v.setOnClickListener(new i(this));
        so5 so5 = new so5();
        so5.a(new b(this));
        this.h = so5;
        RecyclerView recyclerView = ky4.B;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        ee7.a((Object) ky4, "binding");
        View d2 = ky4.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, ky4));
        this.f = new qw6<>(this, ky4);
        return ky4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        z06 z06 = this.g;
        if (z06 != null) {
            z06.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        z06 z06 = this.g;
        if (z06 != null) {
            z06.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.a16
    public void p(String str) {
        ee7.b(str, "address");
        if (isActive()) {
            qw6<ky4> qw6 = this.f;
            if (qw6 != null) {
                ky4 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    ee7.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = nh7.d(text);
                    boolean z = true;
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.a16
    public void setTitle(String str) {
        ee7.b(str, "title");
        qw6<ky4> qw6 = this.f;
        if (qw6 != null) {
            ky4 a2 = qw6.a();
            FlexibleTextView flexibleTextView = a2 != null ? a2.t : null;
            if (flexibleTextView != null) {
                ee7.a((Object) flexibleTextView, "mBinding.get()?.ftvTitle!!");
                flexibleTextView.setText(str);
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.a16
    public void y(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(z06 z06) {
        ee7.b(z06, "presenter");
        this.g = z06;
    }

    @DexIgnore
    @Override // com.fossil.a16
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            zl4 zl4 = new zl4(requireContext, placesClient);
            this.i = zl4;
            if (zl4 != null) {
                zl4.a(this);
            }
            qw6<ky4> qw6 = this.f;
            if (qw6 != null) {
                ky4 a2 = qw6.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.i);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    ee7.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = nh7.d(text);
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        a2.q.setSelection(d2.length());
                        return;
                    }
                    f1();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
