package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck4 implements Factory<PortfolioApp> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public ck4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static ck4 a(wj4 wj4) {
        return new ck4(wj4);
    }

    @DexIgnore
    public static PortfolioApp b(wj4 wj4) {
        PortfolioApp b = wj4.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public PortfolioApp get() {
        return b(this.a);
    }
}
