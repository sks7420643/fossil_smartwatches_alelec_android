package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct4 extends he {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<String, String>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ServerError> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ kn7 j; // = mn7.a(false, 1, null);
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public /* final */ LiveData<List<Object>> l;
    @DexIgnore
    public /* final */ xo4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public hj7<ko4<List<un4>>> a;
        @DexIgnore
        public hj7<ko4<List<un4>>> b;
        @DexIgnore
        public hj7<ko4<List<un4>>> c;
        @DexIgnore
        public hj7<ko4<List<un4>>> d;

        @DexIgnore
        public a(hj7<ko4<List<un4>>> hj7, hj7<ko4<List<un4>>> hj72, hj7<ko4<List<un4>>> hj73, hj7<ko4<List<un4>>> hj74) {
            ee7.b(hj7, "sentRequestFriend");
            ee7.b(hj72, NativeProtocol.AUDIENCE_FRIENDS);
            ee7.b(hj73, "receivedRequestFriends");
            ee7.b(hj74, "blockedFriends");
            this.a = hj7;
            this.b = hj72;
            this.c = hj73;
            this.d = hj74;
        }

        @DexIgnore
        public final hj7<ko4<List<un4>>> a() {
            return this.d;
        }

        @DexIgnore
        public final hj7<ko4<List<un4>>> b() {
            return this.b;
        }

        @DexIgnore
        public final hj7<ko4<List<un4>>> c() {
            return this.c;
        }

        @DexIgnore
        public final hj7<ko4<List<un4>>> d() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return ee7.a(this.a, aVar.a) && ee7.a(this.b, aVar.b) && ee7.a(this.c, aVar.c) && ee7.a(this.d, aVar.d);
        }

        @DexIgnore
        public int hashCode() {
            hj7<ko4<List<un4>>> hj7 = this.a;
            int i = 0;
            int hashCode = (hj7 != null ? hj7.hashCode() : 0) * 31;
            hj7<ko4<List<un4>>> hj72 = this.b;
            int hashCode2 = (hashCode + (hj72 != null ? hj72.hashCode() : 0)) * 31;
            hj7<ko4<List<un4>>> hj73 = this.c;
            int hashCode3 = (hashCode2 + (hj73 != null ? hj73.hashCode() : 0)) * 31;
            hj7<ko4<List<un4>>> hj74 = this.d;
            if (hj74 != null) {
                i = hj74.hashCode();
            }
            return hashCode3 + i;
        }

        @DexIgnore
        public String toString() {
            return "AllFriendRequest(sentRequestFriend=" + this.a + ", friends=" + this.b + ", receivedRequestFriends=" + this.c + ", blockedFriends=" + this.d + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ct4 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$1", f = "BCFriendTabViewModel.kt", l = {243}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $allFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, MutableLiveData mutableLiveData, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$allFriends = mutableLiveData;
                this.$it = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$allFriends, this.$it, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX INFO: finally extract failed */
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                kn7 kn7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    kn7 b = this.this$0.a.j;
                    this.L$0 = yi7;
                    this.L$1 = b;
                    this.label = 1;
                    if (b.a(null, this) == a) {
                        return a;
                    }
                    kn7 = b;
                } else if (i == 1) {
                    kn7 = (kn7) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                try {
                    MutableLiveData mutableLiveData = this.$allFriends;
                    ct4 ct4 = this.this$0.a;
                    List list = this.$it;
                    ee7.a((Object) list, "it");
                    mutableLiveData.a(ct4.a(list));
                    i97 i97 = i97.a;
                    kn7.a(null);
                    return i97.a;
                } catch (Throwable th) {
                    kn7.a(null);
                    throw th;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$2", f = "BCFriendTabViewModel.kt", l = {86, 86}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                ct4 ct4;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi72 = this.p$;
                    this.this$0.a.b.a(w87.a(pb7.a(true), null));
                    ct4 = this.this$0.a;
                    this.L$0 = yi72;
                    this.L$1 = ct4;
                    this.label = 1;
                    Object a2 = ct4.a(this);
                    if (a2 == a) {
                        return a;
                    }
                    yi7 = yi72;
                    obj = a2;
                } else if (i == 1) {
                    ct4 = (ct4) this.L$1;
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi73 = (yi7) this.L$0;
                    t87.a(obj);
                    this.this$0.a.b.a(w87.a(pb7.a(false), null));
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = yi7;
                this.label = 2;
                if (ct4.a((a) obj, this) == a) {
                    return a;
                }
                this.this$0.a.b.a(w87.a(pb7.a(false), null));
                return i97.a;
            }
        }

        @DexIgnore
        public c(ct4 ct4) {
            this.a = ct4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<Object>> apply(List<un4> list) {
            MutableLiveData<List<Object>> mutableLiveData = new MutableLiveData<>();
            if (list.isEmpty()) {
                if (!this.a.k) {
                    this.a.a.a((Object) true);
                }
                mutableLiveData.a(w97.a());
            } else {
                this.a.a.a((Object) false);
                this.a.b.a(w87.a(false, null));
                ik7 unused = xh7.b(ie.a(this.a), qj7.a(), null, new a(this, mutableLiveData, list, null), 2, null);
            }
            if (this.a.k) {
                this.a.k = false;
                ik7 unused2 = xh7.b(ie.a(this.a), null, null, new b(this, null), 3, null);
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$block$1", f = "BCFriendTabViewModel.kt", l = {181}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ct4 ct4, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$friend, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                xo4 a2 = this.this$0.m;
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.a(un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.a() != null) {
                this.this$0.g.a(ko4.a());
            }
            this.this$0.b.a(w87.a(null, pb7.a(false)));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$cancelRequest$1", f = "BCFriendTabViewModel.kt", l = {205}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ct4 ct4, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$friend, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                xo4 a2 = this.this$0.m;
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.b(un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.a() != null) {
                this.this$0.g.a(ko4.a());
            }
            this.this$0.b.a(w87.a(null, pb7.a(false)));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {145, 145}, m = "fetchAll")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ct4 ct4, fb7 fb7) {
            super(fb7);
            this.this$0 = ct4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super hj7<? extends a>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super a>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ct4$g$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$blockedFriends$1", f = "BCFriendTabViewModel.kt", l = {160}, m = "invokeSuspend")
            /* renamed from: com.fossil.ct4$g$a$a  reason: collision with other inner class name */
            public static final class C0030a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0030a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0030a aVar = new C0030a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                    return ((C0030a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        xo4 a2 = this.this$0.this$0.this$0.m;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = a2.b(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$friends$1", f = "BCFriendTabViewModel.kt", l = {152}, m = "invokeSuspend")
            public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        xo4 a2 = this.this$0.this$0.this$0.m;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = a2.c(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$receivedRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {156}, m = "invokeSuspend")
            public static final class c extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        xo4 a2 = this.this$0.this$0.this$0.m;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = a2.d(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$sentRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {148}, m = "invokeSuspend")
            public static final class d extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    d dVar = new d(this.this$0, fb7);
                    dVar.p$ = (yi7) obj;
                    return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                    return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        xo4 a2 = this.this$0.this$0.this$0.m;
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = a2.e(this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super a> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    return new a(xh7.a(yi7, null, null, new d(this, null), 3, null), xh7.a(yi7, null, null, new b(this, null), 3, null), xh7.a(yi7, null, null, new c(this, null), 3, null), xh7.a(yi7, null, null, new C0030a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ct4 ct4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super hj7<? extends a>> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return xh7.a(this.p$, qj7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {107, 108, 109, 110}, m = "handleFriendResult")
    public static final class h extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ct4 ct4, fb7 fb7) {
            super(fb7);
            this.this$0 = ct4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((a) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$refresh$1", f = "BCFriendTabViewModel.kt", l = {96, 96}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(ct4 ct4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            ct4 ct4;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi72 = this.p$;
                ct4 = this.this$0;
                this.L$0 = yi72;
                this.L$1 = ct4;
                this.label = 1;
                Object a2 = ct4.a(this);
                if (a2 == a) {
                    return a;
                }
                yi7 = yi72;
                obj = a2;
            } else if (i == 1) {
                ct4 = (ct4) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                yi7 yi73 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.b.a(w87.a(pb7.a(false), null));
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.L$0 = yi7;
            this.label = 2;
            if (ct4.a((a) obj, this) == a) {
                return a;
            }
            this.this$0.b.a(w87.a(pb7.a(false), null));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$responseToFriendRequest$1", f = "BCFriendTabViewModel.kt", l = {219}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $confirmation;
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(ct4 ct4, boolean z, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
            this.$confirmation = z;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, this.$confirmation, this.$friend, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                xo4 a2 = this.this$0.m;
                boolean z = this.$confirmation;
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.a(z, un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.c() != null) {
                this.this$0.f.a(w87.a(this.$confirmation ? "bc_accept_friend_request" : "bc_reject_friend_request", ((un4) ko4.c()).d()));
            } else {
                this.this$0.g.a(ko4.a());
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unblock$1", f = "BCFriendTabViewModel.kt", l = {193}, m = "invokeSuspend")
    public static final class k extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(ct4 ct4, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            k kVar = new k(this.this$0, this.$friend, fb7);
            kVar.p$ = (yi7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((k) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                xo4 a2 = this.this$0.m;
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.e(un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.a() != null) {
                this.this$0.g.a(ko4.a());
            }
            this.this$0.b.a(w87.a(null, pb7.a(false)));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unfriend$1", f = "BCFriendTabViewModel.kt", l = {170}, m = "invokeSuspend")
    public static final class l extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ un4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ct4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(ct4 ct4, un4 un4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ct4;
            this.$friend = un4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            l lVar = new l(this.this$0, this.$friend, fb7);
            lVar.p$ = (yi7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((l) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b.a(w87.a(null, pb7.a(true)));
                xo4 a2 = this.this$0.m;
                un4 un4 = this.$friend;
                this.L$0 = yi7;
                this.label = 1;
                obj = a2.d(un4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (ko4.a() != null) {
                this.this$0.g.a(ko4.a());
            }
            this.this$0.b.a(w87.a(null, pb7.a(false)));
            return i97.a;
        }
    }

    /*
    static {
        new b(null);
        String simpleName = ct4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCFriendTabViewModel::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public ct4(ch5 ch5, xo4 xo4) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(xo4, "friendRepository");
        this.m = xo4;
        LiveData<List<Object>> b2 = ge.b(au4.a(this.m.e()), new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026\n        allFriends\n    }");
        this.l = b2;
    }

    @DexIgnore
    public final LiveData<ServerError> h() {
        return this.g;
    }

    @DexIgnore
    public final void i() {
        ik7 unused = xh7.b(ie.a(this), null, null, new i(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<String> b() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Boolean> d() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, Boolean>> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> f() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<r87<String, String>> g() {
        return this.f;
    }

    @DexIgnore
    public final void b(un4 un4) {
        ee7.b(un4, "friend");
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new e(this, un4, null), 2, null);
    }

    @DexIgnore
    public final void c(un4 un4) {
        ee7.b(un4, "friend");
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new k(this, un4, null), 2, null);
    }

    @DexIgnore
    public final void d(un4 un4) {
        ee7.b(un4, "friend");
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new l(this, un4, null), 2, null);
    }

    @DexIgnore
    public final LiveData<List<Object>> a() {
        return this.l;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0113, code lost:
        if (r1 != null) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0153, code lost:
        if (r1 != null) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0195, code lost:
        if (r1 != null) goto L_0x019c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01db, code lost:
        if (r1 != null) goto L_0x01e2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0138 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x017a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x017b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01be A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0282 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x02c5  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.ct4.a r19, com.fossil.fb7<? super com.fossil.i97> r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r20
            boolean r2 = r1 instanceof com.fossil.ct4.h
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.ct4$h r2 = (com.fossil.ct4.h) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.ct4$h r2 = new com.fossil.ct4$h
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 4
            r6 = 3
            r7 = 2
            r8 = 1
            r9 = 0
            if (r4 == 0) goto L_0x00ba
            if (r4 == r8) goto L_0x009e
            if (r4 == r7) goto L_0x007f
            if (r4 == r6) goto L_0x005e
            if (r4 != r5) goto L_0x0056
            boolean r3 = r2.Z$2
            boolean r4 = r2.Z$1
            boolean r5 = r2.Z$0
            java.lang.Object r6 = r2.L$5
            com.fossil.oe7 r6 = (com.fossil.oe7) r6
            java.lang.Object r7 = r2.L$4
            com.fossil.oe7 r7 = (com.fossil.oe7) r7
            java.lang.Object r10 = r2.L$3
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$2
            com.fossil.oe7 r11 = (com.fossil.oe7) r11
            java.lang.Object r12 = r2.L$1
            com.fossil.ct4$a r12 = (com.fossil.ct4.a) r12
            java.lang.Object r2 = r2.L$0
            com.fossil.ct4 r2 = (com.fossil.ct4) r2
            com.fossil.t87.a(r1)
            goto L_0x01c7
        L_0x0056:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x005e:
            boolean r4 = r2.Z$1
            boolean r6 = r2.Z$0
            java.lang.Object r7 = r2.L$5
            com.fossil.oe7 r7 = (com.fossil.oe7) r7
            java.lang.Object r10 = r2.L$4
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$3
            com.fossil.oe7 r11 = (com.fossil.oe7) r11
            java.lang.Object r12 = r2.L$2
            com.fossil.oe7 r12 = (com.fossil.oe7) r12
            java.lang.Object r13 = r2.L$1
            com.fossil.ct4$a r13 = (com.fossil.ct4.a) r13
            java.lang.Object r14 = r2.L$0
            com.fossil.ct4 r14 = (com.fossil.ct4) r14
            com.fossil.t87.a(r1)
            goto L_0x0181
        L_0x007f:
            boolean r4 = r2.Z$0
            java.lang.Object r7 = r2.L$5
            com.fossil.oe7 r7 = (com.fossil.oe7) r7
            java.lang.Object r10 = r2.L$4
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$3
            com.fossil.oe7 r11 = (com.fossil.oe7) r11
            java.lang.Object r12 = r2.L$2
            com.fossil.oe7 r12 = (com.fossil.oe7) r12
            java.lang.Object r13 = r2.L$1
            com.fossil.ct4$a r13 = (com.fossil.ct4.a) r13
            java.lang.Object r14 = r2.L$0
            com.fossil.ct4 r14 = (com.fossil.ct4) r14
            com.fossil.t87.a(r1)
            goto L_0x013f
        L_0x009e:
            java.lang.Object r4 = r2.L$5
            com.fossil.oe7 r4 = (com.fossil.oe7) r4
            java.lang.Object r10 = r2.L$4
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$3
            com.fossil.oe7 r11 = (com.fossil.oe7) r11
            java.lang.Object r12 = r2.L$2
            com.fossil.oe7 r12 = (com.fossil.oe7) r12
            java.lang.Object r13 = r2.L$1
            com.fossil.ct4$a r13 = (com.fossil.ct4.a) r13
            java.lang.Object r14 = r2.L$0
            com.fossil.ct4 r14 = (com.fossil.ct4) r14
            com.fossil.t87.a(r1)
            goto L_0x00ff
        L_0x00ba:
            com.fossil.t87.a(r1)
            com.fossil.oe7 r1 = new com.fossil.oe7
            r1.<init>()
            r1.element = r9
            com.fossil.oe7 r4 = new com.fossil.oe7
            r4.<init>()
            r4.element = r9
            com.fossil.oe7 r10 = new com.fossil.oe7
            r10.<init>()
            r10.element = r9
            com.fossil.oe7 r11 = new com.fossil.oe7
            r11.<init>()
            r11.element = r9
            com.fossil.hj7 r12 = r19.d()
            r2.L$0 = r0
            r13 = r19
            r2.L$1 = r13
            r2.L$2 = r1
            r2.L$3 = r4
            r2.L$4 = r10
            r2.L$5 = r11
            r2.label = r8
            java.lang.Object r12 = r12.c(r2)
            if (r12 != r3) goto L_0x00f4
            return r3
        L_0x00f4:
            r14 = r0
            r16 = r12
            r12 = r1
            r1 = r16
            r17 = r11
            r11 = r4
            r4 = r17
        L_0x00ff:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r1.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0116
            boolean r1 = r1.isEmpty()
            r12.element = r1
            java.lang.Boolean r1 = com.fossil.pb7.a(r9)
            if (r1 == 0) goto L_0x0116
            goto L_0x011a
        L_0x0116:
            java.lang.Boolean r1 = com.fossil.pb7.a(r8)
        L_0x011a:
            boolean r1 = r1.booleanValue()
            com.fossil.hj7 r15 = r13.b()
            r2.L$0 = r14
            r2.L$1 = r13
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r10
            r2.L$5 = r4
            r2.Z$0 = r1
            r2.label = r7
            java.lang.Object r7 = r15.c(r2)
            if (r7 != r3) goto L_0x0139
            return r3
        L_0x0139:
            r16 = r4
            r4 = r1
            r1 = r7
            r7 = r16
        L_0x013f:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r1.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0156
            boolean r1 = r1.isEmpty()
            r11.element = r1
            java.lang.Boolean r1 = com.fossil.pb7.a(r9)
            if (r1 == 0) goto L_0x0156
            goto L_0x015a
        L_0x0156:
            java.lang.Boolean r1 = com.fossil.pb7.a(r8)
        L_0x015a:
            boolean r1 = r1.booleanValue()
            com.fossil.hj7 r15 = r13.c()
            r2.L$0 = r14
            r2.L$1 = r13
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r10
            r2.L$5 = r7
            r2.Z$0 = r4
            r2.Z$1 = r1
            r2.label = r6
            java.lang.Object r6 = r15.c(r2)
            if (r6 != r3) goto L_0x017b
            return r3
        L_0x017b:
            r16 = r4
            r4 = r1
            r1 = r6
            r6 = r16
        L_0x0181:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r1.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0198
            boolean r1 = r1.isEmpty()
            r10.element = r1
            java.lang.Boolean r1 = com.fossil.pb7.a(r9)
            if (r1 == 0) goto L_0x0198
            goto L_0x019c
        L_0x0198:
            java.lang.Boolean r1 = com.fossil.pb7.a(r8)
        L_0x019c:
            boolean r1 = r1.booleanValue()
            com.fossil.hj7 r15 = r13.a()
            r2.L$0 = r14
            r2.L$1 = r13
            r2.L$2 = r12
            r2.L$3 = r11
            r2.L$4 = r10
            r2.L$5 = r7
            r2.Z$0 = r6
            r2.Z$1 = r4
            r2.Z$2 = r1
            r2.label = r5
            java.lang.Object r2 = r15.c(r2)
            if (r2 != r3) goto L_0x01bf
            return r3
        L_0x01bf:
            r3 = r1
            r1 = r2
            r5 = r6
            r6 = r7
            r7 = r10
            r10 = r11
            r11 = r12
            r2 = r14
        L_0x01c7:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r1.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x01de
            boolean r1 = r1.isEmpty()
            r6.element = r1
            java.lang.Boolean r1 = com.fossil.pb7.a(r9)
            if (r1 == 0) goto L_0x01de
            goto L_0x01e2
        L_0x01de:
            java.lang.Boolean r1 = com.fossil.pb7.a(r8)
        L_0x01e2:
            boolean r1 = r1.booleanValue()
            androidx.lifecycle.LiveData<java.util.List<java.lang.Object>> r12 = r2.l
            java.lang.Object r12 = r12.a()
            if (r12 == 0) goto L_0x020d
            androidx.lifecycle.LiveData<java.util.List<java.lang.Object>> r12 = r2.l
            java.lang.Object r12 = r12.a()
            java.util.List r12 = (java.util.List) r12
            if (r12 == 0) goto L_0x0208
            boolean r12 = r12.isEmpty()
            r12 = r12 ^ r8
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            if (r12 == 0) goto L_0x0208
            boolean r12 = r12.booleanValue()
            goto L_0x0209
        L_0x0208:
            r12 = 0
        L_0x0209:
            if (r12 == 0) goto L_0x020d
            r12 = 1
            goto L_0x020e
        L_0x020d:
            r12 = 0
        L_0x020e:
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.fossil.ct4.n
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r9 = "handleFriendResult - isSentEmpty: "
            r15.append(r9)
            boolean r9 = r11.element
            r15.append(r9)
            java.lang.String r9 = " - isFriendEmpty: "
            r15.append(r9)
            boolean r9 = r10.element
            r15.append(r9)
            java.lang.String r9 = " - isReceivedEmpty: "
            r15.append(r9)
            boolean r9 = r7.element
            r15.append(r9)
            r9 = 32
            r15.append(r9)
            java.lang.String r9 = "- isSentError: "
            r15.append(r9)
            r15.append(r5)
            java.lang.String r9 = " - isFriendError: "
            r15.append(r9)
            r15.append(r4)
            java.lang.String r9 = " - isReceivedError: "
            r15.append(r9)
            r15.append(r3)
            java.lang.String r9 = " - hadFriendData: "
            r15.append(r9)
            r15.append(r12)
            java.lang.String r9 = r15.toString()
            r13.e(r14, r9)
            boolean r9 = r11.element
            if (r9 == 0) goto L_0x027d
            boolean r9 = r10.element
            if (r9 == 0) goto L_0x027d
            boolean r7 = r7.element
            if (r7 == 0) goto L_0x027d
            boolean r6 = r6.element
            if (r6 == 0) goto L_0x027d
            if (r12 != 0) goto L_0x027d
            r2.h = r8
            r6 = 0
            r2.i = r6
            goto L_0x028e
        L_0x027d:
            r6 = 0
            r2.h = r6
            if (r4 != 0) goto L_0x028b
            if (r3 != 0) goto L_0x028b
            if (r5 != 0) goto L_0x028b
            if (r1 == 0) goto L_0x0289
            goto L_0x028b
        L_0x0289:
            r6 = 0
            goto L_0x028c
        L_0x028b:
            r6 = 1
        L_0x028c:
            r2.i = r6
        L_0x028e:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.fossil.ct4.n
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "handleFriendResult - isEmpty: "
            r4.append(r5)
            boolean r5 = r2.h
            r4.append(r5)
            java.lang.String r5 = " - isError: "
            r4.append(r5)
            boolean r5 = r2.i
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r2.a
            boolean r3 = r2.h
            java.lang.Boolean r3 = com.fossil.pb7.a(r3)
            r1.a(r3)
            boolean r1 = r2.i
            if (r1 == 0) goto L_0x02db
            if (r12 == 0) goto L_0x02d2
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r2.c
            r2 = 0
            java.lang.Boolean r2 = com.fossil.pb7.a(r2)
            r1.a(r2)
            goto L_0x02db
        L_0x02d2:
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r2.c
            java.lang.Boolean r2 = com.fossil.pb7.a(r8)
            r1.a(r2)
        L_0x02db:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ct4.a(com.fossil.ct4$a, com.fossil.fb7):java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0061 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0062 A[PHI: r6 
      PHI: (r6v2 java.lang.Object) = (r6v5 java.lang.Object), (r6v1 java.lang.Object) binds: [B:19:0x005f, B:10:0x0028] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.ct4.a> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.ct4.f
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.ct4$f r0 = (com.fossil.ct4.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ct4$f r0 = new com.fossil.ct4$f
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0040
            if (r2 == r4) goto L_0x0038
            if (r2 != r3) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.ct4 r0 = (com.fossil.ct4) r0
            com.fossil.t87.a(r6)
            goto L_0x0062
        L_0x0030:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0038:
            java.lang.Object r2 = r0.L$0
            com.fossil.ct4 r2 = (com.fossil.ct4) r2
            com.fossil.t87.a(r6)
            goto L_0x0055
        L_0x0040:
            com.fossil.t87.a(r6)
            com.fossil.ct4$g r6 = new com.fossil.ct4$g
            r2 = 0
            r6.<init>(r5, r2)
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r6 = com.fossil.zi7.a(r6, r0)
            if (r6 != r1) goto L_0x0054
            return r1
        L_0x0054:
            r2 = r5
        L_0x0055:
            com.fossil.hj7 r6 = (com.fossil.hj7) r6
            r0.L$0 = r2
            r0.label = r3
            java.lang.Object r6 = r6.c(r0)
            if (r6 != r1) goto L_0x0062
            return r1
        L_0x0062:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ct4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(un4 un4) {
        ee7.b(un4, "friend");
        ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new d(this, un4, null), 2, null);
    }

    @DexIgnore
    public final void a(un4 un4, boolean z) {
        ee7.b(un4, "friend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = n;
        local.e(str, "responseToFriendRequest - friend: " + un4 + " - confirmation: " + z);
        this.e.a(un4.d());
        if (xw6.b(PortfolioApp.g0.c())) {
            ik7 unused = xh7.b(ie.a(this), qj7.b(), null, new j(this, z, un4, null), 2, null);
        } else {
            this.d.a((Boolean) true);
        }
    }

    @DexIgnore
    public final List<Object> a(List<un4> list) {
        if (list.isEmpty()) {
            return w97.a();
        }
        return uc5.b(list);
    }
}
