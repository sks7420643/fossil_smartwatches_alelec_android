package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.cy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr4 extends go5 implements cy6.g {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public gr4 g;
    @DexIgnore
    public qw6<o45> h;
    @DexIgnore
    public mn4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public iq4 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return dr4.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final dr4 a(mn4 mn4, String str) {
            dr4 dr4 = new dr4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", mn4);
            bundle.putString("challenge_history_id_extra", str);
            dr4.setArguments(bundle);
            return dr4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public b(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dr4 dr4) {
            super(1);
            this.this$0 = dr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.g1().l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public d(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            o45 a2 = this.a.f1().a();
            if (!(a2 == null || (flexibleTextView = a2.t) == null)) {
                flexibleTextView.setVisibility(8);
            }
            this.a.g1().m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Object> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        @Override // com.fossil.zd
        public final void onChanged(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<r87<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public f(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, Boolean> r87) {
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = r87.getFirst();
            if (first != null) {
                boolean booleanValue = first.booleanValue();
                o45 a2 = this.a.f1().a();
                if (!(a2 == null || (swipeRefreshLayout = a2.z) == null)) {
                    swipeRefreshLayout.setRefreshing(booleanValue);
                }
            }
            Boolean second = r87.getSecond();
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public g(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mn4 mn4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dr4.s.a();
            local.e(a2, "challengeLive - oldChallenge: " + this.a.i + " - new : " + mn4);
            this.a.i = mn4;
            dr4 dr4 = this.a;
            ee7.a((Object) mn4, "it");
            dr4.b(mn4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public h(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dr4.s.a();
            local.e(a2, "highlightPlayersLive - " + list);
            iq4 b = dr4.b(this.a);
            ee7.a((Object) list, "it");
            b.a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<r87<? extends Integer, ? extends jn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public i(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Integer, jn4> r87) {
            Integer t;
            int intValue = r87.getFirst().intValue();
            Integer h = r87.getSecond().h();
            int intValue2 = h != null ? h.intValue() : 0;
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886312);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026urrentRankNumberInNumber)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2), Integer.valueOf(intValue)}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = dr4.s.a();
            local.e(a3, "current - rank: " + intValue2 + " - numberOfPlayers: " + intValue);
            o45 a4 = this.a.f1().a();
            if (a4 != null) {
                FlexibleTextView flexibleTextView = a4.v;
                ee7.a((Object) flexibleTextView, "ftvRank");
                flexibleTextView.setText(xe5.a(xe5.b, String.valueOf(intValue2), format, 0, 4, null));
                mn4 a5 = this.a.i;
                if (ee7.a((Object) "activity_reach_goal", (Object) (a5 != null ? a5.u() : null))) {
                    mn4 a6 = this.a.i;
                    int intValue3 = (a6 == null || (t = a6.t()) == null) ? 0 : t.intValue();
                    Integer p = r87.getSecond().p();
                    int intValue4 = p != null ? p.intValue() : 0;
                    we7 we72 = we7.a;
                    String a7 = ig5.a(PortfolioApp.g0.c(), 2131887273);
                    ee7.a((Object) a7, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                    String format2 = String.format(a7, Arrays.copyOf(new Object[]{Integer.valueOf(intValue3 - intValue4), Integer.valueOf(intValue3)}, 2));
                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    TimerTextView timerTextView = a4.r;
                    ee7.a((Object) timerTextView, "ftvCountDown");
                    timerTextView.setTag(Integer.valueOf(intValue3));
                    TimerTextView timerTextView2 = a4.r;
                    ee7.a((Object) timerTextView2, "ftvCountDown");
                    timerTextView2.setText(format2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public j(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            if (r87.getFirst().booleanValue()) {
                qe5.c.b();
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            bx6 bx6 = bx6.c;
            ServerError serverError = (ServerError) r87.getSecond();
            String str = null;
            Integer code = serverError != null ? serverError.getCode() : null;
            ServerError serverError2 = (ServerError) r87.getSecond();
            if (serverError2 != null) {
                str = serverError2.getMessage();
            }
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(code, str, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<v87<? extends Boolean, ? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public k(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<Boolean, Boolean, Boolean> v87) {
            boolean booleanValue = v87.getFirst().booleanValue();
            boolean booleanValue2 = v87.getSecond().booleanValue();
            boolean booleanValue3 = v87.getThird().booleanValue();
            if (booleanValue) {
                this.a.j1();
            } else if (booleanValue2) {
                dr4 dr4 = this.a;
                mn4 a2 = dr4.i;
                String f = a2 != null ? a2.f() : null;
                if (f != null) {
                    dr4.Y(f);
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (booleanValue3) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.d(childFragmentManager);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<List<? extends dn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public l(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<dn4> list) {
            dr4 dr4 = this.a;
            ee7.a((Object) list, "it");
            dr4.x(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        public m(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            o45 a2 = this.a.f1().a();
            if (a2 == null) {
                return;
            }
            if (r87.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = a2.t;
                ee7.a((Object) flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.t;
            ee7.a((Object) flexibleTextView2, "ftvError");
            String a3 = ig5.a(flexibleTextView2.getContext(), 2131886227);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, a3, 0).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements zd<v87<? extends String, ? extends String, ? extends Integer>> {
        @DexIgnore
        public static /* final */ n a; // = new n();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<String, String, Integer> v87) {
            um4.a.a("bc_left_challenge_after_start", v87.getFirst(), v87.getSecond(), v87.getThird().intValue(), PortfolioApp.g0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements er5 {
        @DexIgnore
        public /* final */ /* synthetic */ dr4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public o(dr4 dr4) {
            this.a = dr4;
        }

        @DexIgnore
        @Override // com.fossil.er5
        public void a(dn4 dn4) {
            ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            Object a2 = dn4.a();
            if (!(a2 instanceof tt4)) {
                a2 = null;
            }
            tt4 tt4 = (tt4) a2;
            if (tt4 != null) {
                this.a.g1().a(tt4);
            }
        }
    }

    /*
    static {
        String simpleName = dr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCOverviewLeaderBoardFra\u2026nt::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ iq4 b(dr4 dr4) {
        iq4 iq4 = dr4.p;
        if (iq4 != null) {
            return iq4;
        }
        ee7.d("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        Date date;
        int i2;
        Integer t;
        mn4 mn4 = this.i;
        if (mn4 == null || (date = mn4.m()) == null) {
            date = new Date();
        }
        mn4 mn42 = this.i;
        String str2 = null;
        if (ee7.a((Object) "activity_reach_goal", (Object) (mn42 != null ? mn42.u() : null))) {
            mn4 mn43 = this.i;
            i2 = (mn43 == null || (t = mn43.t()) == null) ? 15000 : t.intValue();
        } else {
            i2 = -1;
        }
        mn4 mn44 = this.i;
        if (mn44 != null) {
            str2 = mn44.p();
        }
        BCLeaderBoardActivity.y.a(this, str, date.getTime(), i2, str2);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final qw6<o45> f1() {
        qw6<o45> qw6 = this.h;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final gr4 g1() {
        gr4 gr4 = this.g;
        if (gr4 != null) {
            return gr4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        Date date;
        int i2;
        Integer t;
        qw6<o45> qw6 = this.h;
        if (qw6 != null) {
            o45 a2 = qw6.a();
            if (a2 != null) {
                a2.x.setOnClickListener(new b(this));
                ImageView imageView = a2.w;
                ee7.a((Object) imageView, "imgOption");
                du4.a(imageView, new c(this));
                a2.z.setOnRefreshListener(new d(this));
                mn4 mn4 = this.i;
                if (mn4 == null || (date = mn4.m()) == null) {
                    date = new Date();
                }
                mn4 mn42 = this.i;
                if (ee7.a((Object) "activity_reach_goal", (Object) (mn42 != null ? mn42.u() : null))) {
                    mn4 mn43 = this.i;
                    i2 = (mn43 == null || (t = mn43.t()) == null) ? 15000 : t.intValue();
                } else {
                    i2 = -1;
                }
                this.p = new iq4(date.getTime(), i2);
                RecyclerView recyclerView = a2.y;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                iq4 iq4 = this.p;
                if (iq4 != null) {
                    recyclerView.setAdapter(iq4);
                } else {
                    ee7.d("leaderBoardAdapter");
                    throw null;
                }
            }
        } else {
            ee7.d("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void i1() {
        gr4 gr4 = this.g;
        if (gr4 != null) {
            gr4.h().a(getViewLifecycleOwner(), new f(this));
            gr4 gr42 = this.g;
            if (gr42 != null) {
                gr42.a().a(getViewLifecycleOwner(), new g(this));
                gr4 gr43 = this.g;
                if (gr43 != null) {
                    gr43.d().a(getViewLifecycleOwner(), new h(this));
                    gr4 gr44 = this.g;
                    if (gr44 != null) {
                        gr44.b().a(getViewLifecycleOwner(), new i(this));
                        gr4 gr45 = this.g;
                        if (gr45 != null) {
                            gr45.g().a(getViewLifecycleOwner(), new j(this));
                            gr4 gr46 = this.g;
                            if (gr46 != null) {
                                gr46.j().a(getViewLifecycleOwner(), new k(this));
                                gr4 gr47 = this.g;
                                if (gr47 != null) {
                                    gr47.i().a(getViewLifecycleOwner(), new l(this));
                                    gr4 gr48 = this.g;
                                    if (gr48 != null) {
                                        gr48.c().a(getViewLifecycleOwner(), new m(this));
                                        gr4 gr49 = this.g;
                                        if (gr49 != null) {
                                            gr49.f().a(getViewLifecycleOwner(), n.a);
                                            gr4 gr410 = this.g;
                                            if (gr410 != null) {
                                                LiveData<Object> e2 = gr410.e();
                                                if (e2 != null) {
                                                    e2.a(getViewLifecycleOwner(), e.a);
                                                    return;
                                                }
                                                return;
                                            }
                                            ee7.d("viewModel");
                                            throw null;
                                        }
                                        ee7.d("viewModel");
                                        throw null;
                                    }
                                    ee7.d("viewModel");
                                    throw null;
                                }
                                ee7.d("viewModel");
                                throw null;
                            }
                            ee7.d("viewModel");
                            throw null;
                        }
                        ee7.d("viewModel");
                        throw null;
                    }
                    ee7.d("viewModel");
                    throw null;
                }
                ee7.d("viewModel");
                throw null;
            }
            ee7.d("viewModel");
            throw null;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        BCWaitingChallengeDetailActivity.z.a(this, this.i, "joined_challenge", -1, true);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 15 && i3 == -1 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.i = arguments != null ? (mn4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.j = arguments2 != null ? arguments2.getString("challenge_history_id_extra") : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.e(str, "onCreate - challenge: " + this.i + " - historyId: " + this.j);
        PortfolioApp.g0.c().f().m().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(gr4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.g = (gr4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        o45 o45 = (o45) qb.a(layoutInflater, 2131558599, viewGroup, false, a1());
        this.h = new qw6<>(this, o45);
        ee7.a((Object) o45, "binding");
        return o45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.j == null) {
            gr4 gr4 = this.g;
            if (gr4 != null) {
                gr4.m();
            } else {
                ee7.d("viewModel");
                throw null;
            }
        }
        qd5 c2 = qd5.f.c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a("bc_leader_board", activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        gr4 gr4 = this.g;
        if (gr4 != null) {
            gr4.a(this.i, this.j);
            h1();
            i1();
            mn4 mn4 = this.i;
            if (mn4 != null) {
                b(mn4);
                return;
            }
            return;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void x(List<dn4> list) {
        bn4 b2 = bn4.z.b();
        String a2 = ig5.a(requireContext(), 2131886318);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b2.setTitle(a2);
        b2.x(list);
        b2.a(new o(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, bn4.z.a());
    }

    @DexIgnore
    public final void b(mn4 mn4) {
        String str;
        String a2;
        FLogger.INSTANCE.getLocal().e(r, "updateView - challenge: " + mn4);
        qw6<o45> qw6 = this.h;
        if (qw6 != null) {
            o45 a3 = qw6.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.A;
                ee7.a((Object) flexibleTextView, "tvTitle");
                flexibleTextView.setText(mn4.g());
                String u = mn4.u();
                if (u != null) {
                    int hashCode = u.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && u.equals("activity_reach_goal")) {
                            a3.r.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099689));
                            Integer t = mn4.t();
                            int intValue = t != null ? t.intValue() : 0;
                            TimerTextView timerTextView = a3.r;
                            ee7.a((Object) timerTextView, "ftvCountDown");
                            Object tag = timerTextView.getTag();
                            if (!(tag instanceof Integer)) {
                                tag = null;
                            }
                            if (((Integer) tag) == null) {
                                TimerTextView timerTextView2 = a3.r;
                                ee7.a((Object) timerTextView2, "ftvCountDown");
                                timerTextView2.setTag(mn4.t());
                                we7 we7 = we7.a;
                                String a4 = ig5.a(PortfolioApp.g0.c(), 2131887273);
                                ee7.a((Object) a4, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                                String format = String.format(a4, Arrays.copyOf(new Object[]{Integer.valueOf(intValue), Integer.valueOf(intValue)}, 2));
                                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                                TimerTextView timerTextView3 = a3.r;
                                ee7.a((Object) timerTextView3, "ftvCountDown");
                                timerTextView3.setText(format);
                            }
                            TimerTextView timerTextView4 = a3.u;
                            ee7.a((Object) timerTextView4, "ftvLeft");
                            timerTextView4.setVisibility(0);
                        }
                    } else if (u.equals("activity_best_result")) {
                        a3.r.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099677));
                        if (ee7.a((Object) "completed", (Object) mn4.p())) {
                            TimerTextView timerTextView5 = a3.r;
                            ee7.a((Object) timerTextView5, "ftvCountDown");
                            timerTextView5.setText(ig5.a(PortfolioApp.g0.c(), 2131887276));
                        } else {
                            TimerTextView timerTextView6 = a3.r;
                            Date e2 = mn4.e();
                            timerTextView6.setTime(e2 != null ? e2.getTime() : 0);
                            a3.r.setDisplayType(ut4.HOUR_MIN_SEC);
                        }
                    }
                }
                String w = PortfolioApp.g0.c().w();
                eo4 i2 = mn4.i();
                if (ee7.a((Object) w, (Object) (i2 != null ? i2.b() : null))) {
                    a2 = ig5.a(PortfolioApp.g0.c(), 2131886246);
                } else {
                    fu4 fu4 = fu4.a;
                    eo4 i3 = mn4.i();
                    String a5 = i3 != null ? i3.a() : null;
                    eo4 i4 = mn4.i();
                    String c2 = i4 != null ? i4.c() : null;
                    eo4 i5 = mn4.i();
                    if (i5 == null || (str = i5.d()) == null) {
                        str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                    }
                    a2 = fu4.a(a5, c2, str);
                }
                Date b2 = mn4.b();
                if (b2 != null) {
                    String a6 = zd5.a(b2);
                    we7 we72 = we7.a;
                    String a7 = ig5.a(PortfolioApp.g0.c(), 2131886308);
                    ee7.a((Object) a7, "LanguageHelper.getString\u2026bel__CreatedByNameOnDate)");
                    String format2 = String.format(a7, Arrays.copyOf(new Object[]{a2, a6}, 2));
                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView2 = a3.s;
                    ee7.a((Object) flexibleTextView2, "ftvCreated");
                    xe5 xe5 = xe5.b;
                    ee7.a((Object) a2, Constants.PROFILE_KEY_LAST_NAME);
                    flexibleTextView2.setText(xe5.a(xe5, a2, format2, 0, 4, null));
                    return;
                }
                ee7.a();
                throw null;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363307) {
            gr4 gr4 = this.g;
            if (gr4 != null) {
                gr4.k();
            } else {
                ee7.d("viewModel");
                throw null;
            }
        }
    }
}
