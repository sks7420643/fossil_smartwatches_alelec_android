package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.fossil.eb2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e72 extends eb2<x62> {
    @DexIgnore
    public static /* final */ e72 c; // = new e72();

    @DexIgnore
    public e72() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    @DexIgnore
    public static View b(Context context, int i, int i2) throws eb2.a {
        return c.a(context, i, i2);
    }

    @DexIgnore
    public final View a(Context context, int i, int i2) throws eb2.a {
        try {
            d72 d72 = new d72(i, i2, null);
            return (View) cb2.g(((x62) a(context)).a(cb2.a(context), d72));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new eb2.a(sb.toString(), e);
        }
    }

    @DexIgnore
    @Override // com.fossil.eb2
    public final x62 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        if (queryLocalInterface instanceof x62) {
            return (x62) queryLocalInterface;
        }
        return new e82(iBinder);
    }
}
