package com.fossil;

import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.NotificationType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class tg5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;

    /*
    static {
        int[] iArr = new int[NotificationSource.values().length];
        a = iArr;
        iArr[NotificationSource.TEXT.ordinal()] = 1;
        a[NotificationSource.CALL.ordinal()] = 2;
        int[] iArr2 = new int[NotificationSource.values().length];
        b = iArr2;
        iArr2[NotificationSource.CALL.ordinal()] = 1;
        b[NotificationSource.TEXT.ordinal()] = 2;
        int[] iArr3 = new int[NotificationType.values().length];
        c = iArr3;
        iArr3[NotificationType.CALL.ordinal()] = 1;
        c[NotificationType.SMS.ordinal()] = 2;
        c[NotificationType.APP_FILTER.ordinal()] = 3;
    }
    */
}
