package com.fossil;

import android.os.Looper;
import com.fossil.h62;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s22 implements h62.c {
    @DexIgnore
    public /* final */ WeakReference<q22> a;
    @DexIgnore
    public /* final */ v02<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public s22(q22 q22, v02<?> v02, boolean z) {
        this.a = new WeakReference<>(q22);
        this.b = v02;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.h62.c
    public final void a(i02 i02) {
        q22 q22 = this.a.get();
        if (q22 != null) {
            a72.b(Looper.myLooper() == q22.a.s.f(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            q22.b.lock();
            try {
                if (q22.b(0)) {
                    if (!i02.x()) {
                        q22.b(i02, this.b, this.c);
                    }
                    if (q22.d()) {
                        q22.e();
                    }
                    q22.b.unlock();
                }
            } finally {
                q22.b.unlock();
            }
        }
    }
}
