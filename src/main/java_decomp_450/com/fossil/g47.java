package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g47 extends e47 {
    @DexIgnore
    public static /* final */ a47 m;

    /*
    static {
        a47 a47 = new a47();
        m = a47;
        a47.a("A9VH9B8L4GX4");
    }
    */

    @DexIgnore
    public g47(Context context) {
        super(context, 0, m);
    }

    @DexIgnore
    @Override // com.fossil.e47
    public f47 a() {
        return f47.i;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public boolean a(JSONObject jSONObject) {
        a67.a(jSONObject, "actky", w37.b(((e47) this).j));
        return true;
    }
}
