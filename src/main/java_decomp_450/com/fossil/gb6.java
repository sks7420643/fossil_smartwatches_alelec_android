package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gb6 implements MembersInjector<GoalTrackingOverviewFragment> {
    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, db6 db6) {
        goalTrackingOverviewFragment.g = db6;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, ub6 ub6) {
        goalTrackingOverviewFragment.h = ub6;
    }

    @DexIgnore
    public static void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment, ob6 ob6) {
        goalTrackingOverviewFragment.i = ob6;
    }
}
