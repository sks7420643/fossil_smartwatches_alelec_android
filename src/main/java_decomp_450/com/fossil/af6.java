package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class af6 extends rf<WorkoutSession, c> {
    @DexIgnore
    public /* final */ String c; // = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public b d;
    @DexIgnore
    public d e;
    @DexIgnore
    public ob5 f;
    @DexIgnore
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(WorkoutSession workoutSession);

        @DexIgnore
        void b(WorkoutSession workoutSession);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public WorkoutSession a;
        @DexIgnore
        public /* final */ qa5 b;
        @DexIgnore
        public /* final */ /* synthetic */ af6 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession b;

            @DexIgnore
            public a(c cVar, WorkoutSession workoutSession) {
                this.a = cVar;
                this.b = workoutSession;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.c.d != null) {
                    b b2 = this.a.c.d;
                    if (b2 != null) {
                        b2.a(this.b);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession b;

            @DexIgnore
            public b(c cVar, WorkoutSession workoutSession) {
                this.a = cVar;
                this.b = workoutSession;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.c.d != null) {
                    b b2 = this.a.c.d;
                    if (b2 != null) {
                        b2.b(this.b);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(af6 af6, qa5 qa5) {
            super(qa5.d());
            ee7.b(qa5, "mBinding");
            this.c = af6;
            this.b = qa5;
            String a2 = af6.c;
            if (a2 != null) {
                this.b.q.setBackgroundColor(Color.parseColor(a2));
                ImageView imageView = this.b.H;
                ee7.a((Object) imageView, "mBinding.ivWorkout");
                Drawable background = imageView.getBackground();
                if (background instanceof ShapeDrawable) {
                    Paint paint = ((ShapeDrawable) background).getPaint();
                    ee7.a((Object) paint, "drawable.paint");
                    paint.setColor(Color.parseColor(a2));
                } else if (background instanceof GradientDrawable) {
                    ((GradientDrawable) background).setColor(Color.parseColor(a2));
                } else if (background instanceof ColorDrawable) {
                    ((ColorDrawable) background).setColor(Color.parseColor(a2));
                }
            }
        }

        @DexIgnore
        public final void a(WorkoutSession workoutSession) {
            int i;
            ee7.b(workoutSession, "value");
            this.a = workoutSession;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutAdapter", "build - value=" + workoutSession);
            af6 af6 = this.c;
            View d = this.b.d();
            ee7.a((Object) d, "mBinding.root");
            Context context = d.getContext();
            ee7.a((Object) context, "mBinding.root.context");
            WorkoutSession workoutSession2 = this.a;
            if (workoutSession2 != null) {
                e a2 = af6.a(context, workoutSession2);
                ac5 editedType = workoutSession.getEditedType();
                if (editedType == null) {
                    editedType = workoutSession.getWorkoutType();
                }
                ub5 editedMode = workoutSession.getEditedMode();
                if (editedMode == null) {
                    editedMode = workoutSession.getMode();
                }
                this.b.E.setImageResource(0);
                this.b.H.setImageResource(a2.i());
                FlexibleTextView flexibleTextView = this.b.y;
                ee7.a((Object) flexibleTextView, "mBinding.ftvWorkoutTitle");
                flexibleTextView.setText(a2.n());
                FlexibleTextView flexibleTextView2 = this.b.y;
                ee7.a((Object) flexibleTextView2, "mBinding.ftvWorkoutTitle");
                flexibleTextView2.setSelected(true);
                FlexibleTextView flexibleTextView3 = this.b.x;
                ee7.a((Object) flexibleTextView3, "mBinding.ftvWorkoutTime");
                flexibleTextView3.setText(a2.m());
                FlexibleTextView flexibleTextView4 = this.b.r;
                ee7.a((Object) flexibleTextView4, "mBinding.ftvActiveTime");
                flexibleTextView4.setText(a2.a());
                FlexibleTextView flexibleTextView5 = this.b.t;
                ee7.a((Object) flexibleTextView5, "mBinding.ftvDistance");
                flexibleTextView5.setText(a2.d());
                FlexibleTextView flexibleTextView6 = this.b.w;
                ee7.a((Object) flexibleTextView6, "mBinding.ftvSteps");
                flexibleTextView6.setText(a2.l());
                FlexibleTextView flexibleTextView7 = this.b.s;
                ee7.a((Object) flexibleTextView7, "mBinding.ftvCalories");
                flexibleTextView7.setText(a2.c());
                FlexibleTextView flexibleTextView8 = this.b.u;
                ee7.a((Object) flexibleTextView8, "mBinding.ftvHeartRateAvg");
                flexibleTextView8.setText(a2.e());
                FlexibleTextView flexibleTextView9 = this.b.v;
                ee7.a((Object) flexibleTextView9, "mBinding.ftvHeartRateMax");
                flexibleTextView9.setText(a2.f());
                ImageView imageView = this.b.E;
                ee7.a((Object) imageView, "mBinding.ivMap");
                bf5.a(imageView, new a(this, workoutSession));
                if (!TextUtils.isEmpty(workoutSession.getScreenShotUri())) {
                    ImageView imageView2 = this.b.E;
                    ee7.a((Object) imageView2, "mBinding.ivMap");
                    imageView2.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = this.b.I;
                    ee7.a((Object) flexibleProgressBar, "mBinding.progressBar");
                    flexibleProgressBar.setVisibility(8);
                    ImageView imageView3 = this.b.E;
                    ee7.a((Object) imageView3, "mBinding.ivMap");
                    kd5 a3 = hd5.a(imageView3.getContext());
                    String screenShotUri = workoutSession.getScreenShotUri();
                    if (screenShotUri != null) {
                        ee7.a((Object) a3.a(screenShotUri).a(this.b.E), "GlideApp.with(mBinding.i\u2026    .into(mBinding.ivMap)");
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
                    if (workoutGpsPoints == null || !(!workoutGpsPoints.isEmpty())) {
                        ImageView imageView4 = this.b.E;
                        ee7.a((Object) imageView4, "mBinding.ivMap");
                        imageView4.setVisibility(8);
                        FlexibleProgressBar flexibleProgressBar2 = this.b.I;
                        ee7.a((Object) flexibleProgressBar2, "mBinding.progressBar");
                        flexibleProgressBar2.setVisibility(8);
                    } else {
                        ImageView imageView5 = this.b.E;
                        ee7.a((Object) imageView5, "mBinding.ivMap");
                        imageView5.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar3 = this.b.I;
                        ee7.a((Object) flexibleProgressBar3, "mBinding.progressBar");
                        flexibleProgressBar3.setVisibility(0);
                    }
                }
                this.b.F.setOnClickListener(new b(this, workoutSession));
                if (TextUtils.isEmpty(this.c.g)) {
                    i = v6.a(PortfolioApp.g0.c(), 2131099812);
                } else {
                    i = Color.parseColor(this.c.g);
                }
                ColorStateList valueOf = ColorStateList.valueOf(i);
                ee7.a((Object) valueOf, "ColorStateList.valueOf(color)");
                int i2 = cf6.b[this.c.e.ordinal()];
                if (i2 == 1) {
                    this.b.z.setColorFilter(i);
                    this.b.r.setTextColor(i);
                    ImageView imageView6 = this.b.F;
                    ee7.a((Object) imageView6, "mBinding.ivMore");
                    imageView6.setVisibility(8);
                } else if (i2 == 2) {
                    Drawable c2 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231065);
                    Drawable c3 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231082);
                    Drawable c4 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231063);
                    this.b.G.setImageDrawable(c2);
                    this.b.D.setImageDrawable(c3);
                    this.b.C.setImageDrawable(c4);
                    if (editedType != null) {
                        int i3 = cf6.a[editedType.ordinal()];
                        if (i3 == 1 || i3 == 2 || i3 == 3) {
                            Drawable c5 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231115);
                            Drawable c6 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231096);
                            this.b.C.setImageDrawable(c5);
                            FlexibleTextView flexibleTextView10 = this.b.u;
                            ee7.a((Object) flexibleTextView10, "mBinding.ftvHeartRateAvg");
                            flexibleTextView10.setText(a2.g());
                            this.b.D.setImageDrawable(c6);
                            FlexibleTextView flexibleTextView11 = this.b.v;
                            ee7.a((Object) flexibleTextView11, "mBinding.ftvHeartRateMax");
                            flexibleTextView11.setText(a2.h());
                        } else if (i3 == 4 && editedMode == ub5.OUTDOOR) {
                            Drawable c7 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131230999);
                            Drawable c8 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231097);
                            Drawable c9 = v6.c(PortfolioApp.g0.c().getApplicationContext(), 2131231040);
                            this.b.C.setImageDrawable(c7);
                            FlexibleTextView flexibleTextView12 = this.b.u;
                            ee7.a((Object) flexibleTextView12, "mBinding.ftvHeartRateAvg");
                            flexibleTextView12.setText(a2.j());
                            this.b.D.setImageDrawable(c8);
                            FlexibleTextView flexibleTextView13 = this.b.v;
                            ee7.a((Object) flexibleTextView13, "mBinding.ftvHeartRateMax");
                            flexibleTextView13.setText(a2.k());
                            this.b.G.setImageDrawable(c9);
                            FlexibleTextView flexibleTextView14 = this.b.w;
                            ee7.a((Object) flexibleTextView14, "mBinding.ftvSteps");
                            flexibleTextView14.setText(a2.b());
                        }
                    }
                    this.b.G.setColorFilter(i);
                    this.b.w.setTextColor(i);
                } else if (i2 == 3) {
                    this.b.A.setColorFilter(i);
                    this.b.s.setTextColor(i);
                    ImageView imageView7 = this.b.F;
                    ee7.a((Object) imageView7, "mBinding.ivMore");
                    imageView7.setVisibility(8);
                } else if (i2 == 4) {
                    this.b.C.setColorFilter(i);
                    this.b.D.setColorFilter(i);
                    this.b.u.setTextColor(i);
                    this.b.v.setTextColor(i);
                    ImageView imageView8 = this.b.F;
                    ee7.a((Object) imageView8, "mBinding.ivMore");
                    imageView8.setVisibility(8);
                }
                ImageView imageView9 = this.b.H;
                ee7.a((Object) imageView9, "mBinding.ivWorkout");
                imageView9.setImageTintList(valueOf);
                return;
            }
            ee7.d("mValue");
            throw null;
        }
    }

    @DexIgnore
    public enum d {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        HEART_RATE
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public String l;
        @DexIgnore
        public String m;
        @DexIgnore
        public String n;

        @DexIgnore
        public e(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13) {
            ee7.b(str, "mTitle");
            ee7.b(str2, "mTime");
            ee7.b(str3, "mActiveMinute");
            ee7.b(str4, "mDistance");
            ee7.b(str5, "mSteps");
            ee7.b(str6, "mCalories");
            ee7.b(str7, "mHeartRateAvg");
            ee7.b(str8, "mHeartRateMax");
            ee7.b(str9, "mPaceAvg");
            ee7.b(str10, "mPaceMax");
            ee7.b(str11, "mSpeedAvg");
            ee7.b(str12, "mSpeedMax");
            ee7.b(str13, "mCadence");
            this.a = i2;
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = str5;
            this.g = str6;
            this.h = str7;
            this.i = str8;
            this.j = str9;
            this.k = str10;
            this.l = str11;
            this.m = str12;
            this.n = str13;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.n;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final String e() {
            return this.h;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            return this.a == eVar.a && ee7.a(this.b, eVar.b) && ee7.a(this.c, eVar.c) && ee7.a(this.d, eVar.d) && ee7.a(this.e, eVar.e) && ee7.a(this.f, eVar.f) && ee7.a(this.g, eVar.g) && ee7.a(this.h, eVar.h) && ee7.a(this.i, eVar.i) && ee7.a(this.j, eVar.j) && ee7.a(this.k, eVar.k) && ee7.a(this.l, eVar.l) && ee7.a(this.m, eVar.m) && ee7.a(this.n, eVar.n);
        }

        @DexIgnore
        public final String f() {
            return this.i;
        }

        @DexIgnore
        public final String g() {
            return this.j;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = this.a * 31;
            String str = this.b;
            int i3 = 0;
            int hashCode = (i2 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.d;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.e;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.f;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.g;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            String str7 = this.h;
            int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
            String str8 = this.i;
            int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
            String str9 = this.j;
            int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
            String str10 = this.k;
            int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
            String str11 = this.l;
            int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
            String str12 = this.m;
            int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
            String str13 = this.n;
            if (str13 != null) {
                i3 = str13.hashCode();
            }
            return hashCode12 + i3;
        }

        @DexIgnore
        public final int i() {
            return this.a;
        }

        @DexIgnore
        public final String j() {
            return this.l;
        }

        @DexIgnore
        public final String k() {
            return this.m;
        }

        @DexIgnore
        public final String l() {
            return this.f;
        }

        @DexIgnore
        public final String m() {
            return this.c;
        }

        @DexIgnore
        public final String n() {
            return this.b;
        }

        @DexIgnore
        public String toString() {
            return "WorkoutModel(mResIconId=" + this.a + ", mTitle=" + this.b + ", mTime=" + this.c + ", mActiveMinute=" + this.d + ", mDistance=" + this.e + ", mSteps=" + this.f + ", mCalories=" + this.g + ", mHeartRateAvg=" + this.h + ", mHeartRateMax=" + this.i + ", mPaceAvg=" + this.j + ", mPaceMax=" + this.k + ", mSpeedAvg=" + this.l + ", mSpeedMax=" + this.m + ", mCadence=" + this.n + ")";
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public af6(d dVar, ob5 ob5, WorkoutSessionDifference workoutSessionDifference, String str) {
        super(workoutSessionDifference);
        ee7.b(dVar, "mWorkoutItem");
        ee7.b(ob5, "mDistanceUnit");
        ee7.b(workoutSessionDifference, "workoutSessionDiff");
        this.e = dVar;
        this.f = ob5;
        this.g = str;
    }

    @DexIgnore
    public final r87<Integer, Integer> b(float f2) {
        int i = (int) (f2 / ((float) 60));
        return new r87<>(Integer.valueOf(i), Integer.valueOf((int) (f2 - ((float) (i * 60)))));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        qa5 a2 = qa5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemWorkoutBinding.infla\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public final void a(ob5 ob5, qf<WorkoutSession> qfVar) {
        ee7.b(ob5, "distanceUnit");
        ee7.b(qfVar, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutAdapter", "setData - distanceUnit=" + ob5 + ", data=" + qfVar.size());
        this.f = ob5;
        b(qfVar);
    }

    @DexIgnore
    public final void a(b bVar) {
        this.d = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        WorkoutSession workoutSession = (WorkoutSession) getItem(i);
        if (workoutSession != null) {
            cVar.a(workoutSession);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:105:0x046a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.af6.e a(android.content.Context r33, com.portfolio.platform.data.model.diana.workout.WorkoutSession r34) {
        /*
            r32 = this;
            r0 = r32
            r1 = r33
            com.fossil.ac5 r2 = r34.getEditedType()
            if (r2 == 0) goto L_0x000b
            goto L_0x000f
        L_0x000b:
            com.fossil.ac5 r2 = r34.getWorkoutType()
        L_0x000f:
            com.fossil.ub5 r3 = r34.getEditedMode()
            if (r3 == 0) goto L_0x0016
            goto L_0x001a
        L_0x0016:
            com.fossil.ub5 r3 = r34.getMode()
        L_0x001a:
            org.joda.time.DateTime r4 = r34.getEditedStartTime()
            long r4 = r4.getMillis()
            int r6 = r34.getTimezoneOffsetInSecond()
            java.lang.String r10 = com.fossil.zd5.a(r4, r6)
            org.joda.time.DateTime r4 = r34.getEditedEndTime()
            r5 = 0
            if (r4 == 0) goto L_0x04fc
            long r6 = r4.getMillis()
            org.joda.time.DateTime r4 = r34.getEditedStartTime()
            if (r4 == 0) goto L_0x04f8
            long r8 = r4.getMillis()
            long r6 = r6 - r8
            r4 = 1000(0x3e8, float:1.401E-42)
            long r8 = (long) r4
            long r6 = r6 / r8
            int r4 = (int) r6
            com.fossil.v87 r4 = com.fossil.zd5.d(r4)
            java.lang.String r6 = "DateHelper.getTimeValues(duration.toInt())"
            com.fossil.ee7.a(r4, r6)
            java.lang.Object r6 = r4.getFirst()
            java.lang.Number r6 = (java.lang.Number) r6
            int r6 = r6.intValue()
            r7 = 0
            int r6 = com.fossil.ee7.a(r6, r7)
            r8 = 3
            r9 = 2
            java.lang.String r11 = "java.lang.String.format(format, *args)"
            r12 = 1
            if (r6 <= 0) goto L_0x0092
            com.fossil.we7 r6 = com.fossil.we7.a
            r6 = 2131886652(0x7f12023c, float:1.9407889E38)
            java.lang.String r6 = com.fossil.ig5.a(r1, r6)
            java.lang.String r13 = "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)"
            com.fossil.ee7.a(r6, r13)
            java.lang.Object[] r13 = new java.lang.Object[r8]
            java.lang.Object r14 = r4.getFirst()
            r13[r7] = r14
            java.lang.Object r14 = r4.getSecond()
            r13[r12] = r14
            java.lang.Object r4 = r4.getThird()
            r13[r9] = r4
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r13, r8)
            java.lang.String r4 = java.lang.String.format(r6, r4)
            com.fossil.ee7.a(r4, r11)
            goto L_0x00eb
        L_0x0092:
            java.lang.Object r6 = r4.getSecond()
            java.lang.Number r6 = (java.lang.Number) r6
            int r6 = r6.intValue()
            int r6 = com.fossil.ee7.a(r6, r7)
            if (r6 <= 0) goto L_0x00ca
            com.fossil.we7 r6 = com.fossil.we7.a
            r6 = 2131886653(0x7f12023d, float:1.940789E38)
            java.lang.String r6 = com.fossil.ig5.a(r1, r6)
            java.lang.String r13 = "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)"
            com.fossil.ee7.a(r6, r13)
            java.lang.Object[] r13 = new java.lang.Object[r9]
            java.lang.Object r14 = r4.getSecond()
            r13[r7] = r14
            java.lang.Object r4 = r4.getThird()
            r13[r12] = r4
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r13, r9)
            java.lang.String r4 = java.lang.String.format(r6, r4)
            com.fossil.ee7.a(r4, r11)
            goto L_0x00eb
        L_0x00ca:
            com.fossil.we7 r6 = com.fossil.we7.a
            r6 = 2131886654(0x7f12023e, float:1.9407893E38)
            java.lang.String r6 = com.fossil.ig5.a(r1, r6)
            java.lang.String r13 = "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)"
            com.fossil.ee7.a(r6, r13)
            java.lang.Object[] r13 = new java.lang.Object[r12]
            java.lang.Object r4 = r4.getThird()
            r13[r7] = r4
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r13, r12)
            java.lang.String r4 = java.lang.String.format(r6, r4)
            com.fossil.ee7.a(r4, r11)
        L_0x00eb:
            com.fossil.af5 r6 = com.fossil.af5.a
            java.lang.Double r13 = r34.getTotalDistance()
            if (r13 == 0) goto L_0x00fc
            double r13 = r13.doubleValue()
            float r5 = (float) r13
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
        L_0x00fc:
            com.fossil.ob5 r13 = r0.f
            java.lang.String r5 = r6.a(r5, r13)
            com.fossil.ob5 r6 = r0.f
            com.fossil.ob5 r13 = com.fossil.ob5.IMPERIAL
            java.lang.String r14 = " "
            if (r6 != r13) goto L_0x0124
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r5)
            r6.append(r14)
            r5 = 2131886559(0x7f1201df, float:1.94077E38)
            java.lang.String r5 = com.fossil.ig5.a(r1, r5)
            r6.append(r5)
            java.lang.String r5 = r6.toString()
            goto L_0x013d
        L_0x0124:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r5)
            r6.append(r14)
            r5 = 2131886558(0x7f1201de, float:1.9407698E38)
            java.lang.String r5 = com.fossil.ig5.a(r1, r5)
            r6.append(r5)
            java.lang.String r5 = r6.toString()
        L_0x013d:
            com.fossil.we7 r6 = com.fossil.we7.a
            r6 = 2131886636(0x7f12022c, float:1.9407856E38)
            java.lang.String r6 = com.fossil.ig5.a(r1, r6)
            java.lang.String r13 = "LanguageHelper.getString\u2026sToday_Text__NumberSteps)"
            com.fossil.ee7.a(r6, r13)
            java.lang.Object[] r13 = new java.lang.Object[r12]
            com.fossil.af5 r15 = com.fossil.af5.a
            java.lang.Integer r9 = r34.getTotalSteps()
            java.lang.String r9 = r15.b(r9)
            r13[r7] = r9
            java.lang.Object[] r9 = java.util.Arrays.copyOf(r13, r12)
            java.lang.String r13 = java.lang.String.format(r6, r9)
            com.fossil.ee7.a(r13, r11)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.fossil.af5 r9 = com.fossil.af5.a
            java.lang.Float r15 = r34.getTotalCalorie()
            java.lang.String r9 = r9.a(r15)
            r6.append(r9)
            r6.append(r14)
            r9 = 2131886555(0x7f1201db, float:1.9407692E38)
            java.lang.String r9 = com.fossil.ig5.a(r1, r9)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.Float r15 = r34.getAverageHeartRate()
            r17 = 0
            if (r15 == 0) goto L_0x0199
            float r15 = r15.floatValue()
            goto L_0x019a
        L_0x0199:
            r15 = 0
        L_0x019a:
            java.lang.String r15 = com.fossil.re5.b(r15, r7)
            r9.append(r15)
            r9.append(r14)
            r15 = 2131886556(0x7f1201dc, float:1.9407694E38)
            java.lang.String r15 = com.fossil.ig5.a(r1, r15)
            r9.append(r15)
            java.lang.String r15 = r9.toString()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.Integer r18 = r34.getMaxHeartRate()
            if (r18 == 0) goto L_0x01c3
            int r12 = r18.intValue()
            float r12 = (float) r12
            goto L_0x01c4
        L_0x01c3:
            r12 = 0
        L_0x01c4:
            java.lang.String r12 = com.fossil.re5.b(r12, r7)
            r9.append(r12)
            r9.append(r14)
            r12 = 2131886557(0x7f1201dd, float:1.9407696E38)
            java.lang.String r12 = com.fossil.ig5.a(r1, r12)
            r9.append(r12)
            java.lang.String r17 = r9.toString()
            com.fossil.cc5$a r9 = com.fossil.cc5.Companion
            com.fossil.cc5 r12 = r9.a(r2, r3)
            com.fossil.r87 r9 = r9.a(r12)
            java.lang.Object r12 = r9.getSecond()
            java.lang.Number r12 = (java.lang.Number) r12
            int r12 = r12.intValue()
            java.lang.String r12 = com.fossil.ig5.a(r1, r12)
            com.fossil.af6$d r14 = r0.e
            com.fossil.af6$d r7 = com.fossil.af6.d.STEPS
            java.lang.String r20 = "--"
            if (r14 != r7) goto L_0x04af
            r7 = 2131886659(0x7f120243, float:1.9407903E38)
            java.lang.String r7 = com.fossil.ig5.a(r1, r7)
            java.lang.String r14 = "LanguageHelper.getString\u2026ctivityGPS_Text__AvgPace)"
            com.fossil.ee7.a(r7, r14)
            java.lang.String r14 = "null cannot be cast to non-null type java.lang.String"
            if (r7 == 0) goto L_0x04a9
            java.lang.String r7 = r7.toLowerCase()
            java.lang.String r8 = "(this as java.lang.String).toLowerCase()"
            com.fossil.ee7.a(r7, r8)
            r22 = r15
            r15 = 2131886661(0x7f120245, float:1.9407907E38)
            java.lang.String r15 = com.fossil.ig5.a(r1, r15)
            r23 = r6
            java.lang.String r6 = "LanguageHelper.getString\u2026tivityGPS_Text__BestPace)"
            com.fossil.ee7.a(r15, r6)
            if (r15 == 0) goto L_0x04a3
            java.lang.String r6 = r15.toLowerCase()
            com.fossil.ee7.a(r6, r8)
            r15 = 2131886662(0x7f120246, float:1.940791E38)
            java.lang.String r15 = com.fossil.ig5.a(r1, r15)
            r24 = r13
            java.lang.String r13 = "LanguageHelper.getString\u2026tivityGPS_Text__MaxSpeed)"
            com.fossil.ee7.a(r15, r13)
            if (r15 == 0) goto L_0x049d
            java.lang.String r13 = r15.toLowerCase()
            com.fossil.ee7.a(r13, r8)
            r15 = 2131886660(0x7f120244, float:1.9407905E38)
            java.lang.String r15 = com.fossil.ig5.a(r1, r15)
            r25 = r5
            java.lang.String r5 = "LanguageHelper.getString\u2026tivityGPS_Text__AvgSpeed)"
            com.fossil.ee7.a(r15, r5)
            if (r15 == 0) goto L_0x0497
            java.lang.String r5 = r15.toLowerCase()
            com.fossil.ee7.a(r5, r8)
            r8 = 2131886589(0x7f1201fd, float:1.9407761E38)
            java.lang.String r8 = com.fossil.ig5.a(r1, r8)
            r14 = 2131886663(0x7f120247, float:1.9407911E38)
            java.lang.String r14 = com.fossil.ig5.a(r1, r14)
            r15 = 2131886664(0x7f120248, float:1.9407913E38)
            java.lang.String r15 = com.fossil.ig5.a(r1, r15)
            r26 = r4
            r4 = 2131886665(0x7f120249, float:1.9407915E38)
            java.lang.String r4 = com.fossil.ig5.a(r1, r4)
            r27 = r4
            r4 = 2131886666(0x7f12024a, float:1.9407917E38)
            java.lang.String r1 = com.fossil.ig5.a(r1, r4)
            com.fossil.ac5 r4 = com.fossil.ac5.RUNNING
            if (r2 == r4) goto L_0x029c
            com.fossil.ac5 r4 = com.fossil.ac5.HIKING
            if (r2 == r4) goto L_0x029c
            com.fossil.ac5 r4 = com.fossil.ac5.WALKING
            if (r2 == r4) goto L_0x029c
            com.fossil.ac5 r4 = com.fossil.ac5.CYCLING
            if (r2 != r4) goto L_0x0294
            goto L_0x029c
        L_0x0294:
            r31 = r9
            r28 = r10
            r29 = r12
            goto L_0x04bf
        L_0x029c:
            com.portfolio.platform.data.model.diana.workout.WorkoutPace r4 = r34.getPace()
            r33 = r1
            if (r4 == 0) goto L_0x042d
            com.fossil.ob5 r1 = r0.f
            r28 = r10
            com.fossil.ob5 r10 = com.fossil.ob5.IMPERIAL
            r29 = r12
            java.lang.String r12 = "%s %s %s"
            if (r1 != r10) goto L_0x0378
            java.lang.Float r1 = r4.getAverage()
            r10 = 1059001362(0x3f1f1412, float:0.6214)
            if (r1 == 0) goto L_0x0314
            float r1 = r1.floatValue()
            float r14 = r1 / r10
            com.fossil.we7 r30 = com.fossil.we7.a
            r31 = r9
            r10 = 3
            java.lang.Object[] r9 = new java.lang.Object[r10]
            java.lang.String r14 = r0.a(r14)
            r10 = 0
            r9[r10] = r14
            r14 = 1
            r9[r14] = r8
            r16 = 2
            r9[r16] = r7
            r7 = 3
            java.lang.Object[] r9 = java.util.Arrays.copyOf(r9, r7)
            java.lang.String r7 = java.lang.String.format(r12, r9)
            com.fossil.ee7.a(r7, r11)
            float r9 = (float) r10
            int r9 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r9 <= 0) goto L_0x030f
            float r9 = (float) r14
            float r9 = r9 / r1
            r1 = 3600(0xe10, float:5.045E-42)
            float r10 = (float) r1
            float r9 = r9 * r10
            r1 = 1059001362(0x3f1f1412, float:0.6214)
            float r9 = r9 * r1
            java.lang.String r1 = com.fossil.re5.a(r9, r14)
            com.fossil.we7 r9 = com.fossil.we7.a
            r10 = 3
            java.lang.Object[] r9 = new java.lang.Object[r10]
            r18 = 0
            r9[r18] = r1
            r9[r14] = r15
            r1 = 2
            r9[r1] = r5
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r9, r10)
            java.lang.String r1 = java.lang.String.format(r12, r1)
            com.fossil.ee7.a(r1, r11)
            goto L_0x0311
        L_0x030f:
            r1 = r20
        L_0x0311:
            com.fossil.i97 r5 = com.fossil.i97.a
            goto L_0x0319
        L_0x0314:
            r31 = r9
            r1 = r20
            r7 = r1
        L_0x0319:
            java.lang.Float r4 = r4.getBest()
            if (r4 == 0) goto L_0x0429
            float r4 = r4.floatValue()
            r5 = 1059001362(0x3f1f1412, float:0.6214)
            float r9 = r4 / r5
            com.fossil.we7 r5 = com.fossil.we7.a
            r10 = 3
            java.lang.Object[] r5 = new java.lang.Object[r10]
            java.lang.String r9 = r0.a(r9)
            r14 = 0
            r5[r14] = r9
            r9 = 1
            r5[r9] = r8
            r8 = 2
            r5[r8] = r6
            java.lang.Object[] r5 = java.util.Arrays.copyOf(r5, r10)
            java.lang.String r5 = java.lang.String.format(r12, r5)
            com.fossil.ee7.a(r5, r11)
            float r6 = (float) r14
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x0372
            float r6 = (float) r9
            float r6 = r6 / r4
            r4 = 3600(0xe10, float:5.045E-42)
            float r4 = (float) r4
            float r6 = r6 * r4
            r4 = 1059001362(0x3f1f1412, float:0.6214)
            float r6 = r6 * r4
            java.lang.String r4 = com.fossil.re5.a(r6, r9)
            com.fossil.we7 r6 = com.fossil.we7.a
            r8 = 3
            java.lang.Object[] r6 = new java.lang.Object[r8]
            r6[r14] = r4
            r6[r9] = r15
            r4 = 2
            r6[r4] = r13
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r6, r8)
            java.lang.String r4 = java.lang.String.format(r12, r4)
            com.fossil.ee7.a(r4, r11)
            goto L_0x0374
        L_0x0372:
            r4 = r20
        L_0x0374:
            com.fossil.i97 r6 = com.fossil.i97.a
            goto L_0x0438
        L_0x0378:
            r31 = r9
            java.lang.Float r1 = r4.getAverage()
            if (r1 == 0) goto L_0x03d2
            float r1 = r1.floatValue()
            com.fossil.we7 r9 = com.fossil.we7.a
            r10 = 3
            java.lang.Object[] r9 = new java.lang.Object[r10]
            java.lang.String r15 = r0.a(r1)
            r10 = 0
            r9[r10] = r15
            r15 = 1
            r9[r15] = r8
            r16 = 2
            r9[r16] = r7
            r7 = 3
            java.lang.Object[] r9 = java.util.Arrays.copyOf(r9, r7)
            java.lang.String r7 = java.lang.String.format(r12, r9)
            com.fossil.ee7.a(r7, r11)
            float r9 = (float) r10
            int r9 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r9 <= 0) goto L_0x03cd
            float r9 = (float) r15
            float r9 = r9 / r1
            r1 = 3600(0xe10, float:5.045E-42)
            float r10 = (float) r1
            float r9 = r9 * r10
            java.lang.String r1 = com.fossil.re5.a(r9, r15)
            com.fossil.we7 r9 = com.fossil.we7.a
            r10 = 3
            java.lang.Object[] r9 = new java.lang.Object[r10]
            r18 = 0
            r9[r18] = r1
            r9[r15] = r14
            r1 = 2
            r9[r1] = r5
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r9, r10)
            java.lang.String r1 = java.lang.String.format(r12, r1)
            com.fossil.ee7.a(r1, r11)
            goto L_0x03cf
        L_0x03cd:
            r1 = r20
        L_0x03cf:
            com.fossil.i97 r5 = com.fossil.i97.a
            goto L_0x03d5
        L_0x03d2:
            r1 = r20
            r7 = r1
        L_0x03d5:
            java.lang.Float r4 = r4.getBest()
            if (r4 == 0) goto L_0x0429
            float r4 = r4.floatValue()
            com.fossil.we7 r5 = com.fossil.we7.a
            r9 = 3
            java.lang.Object[] r5 = new java.lang.Object[r9]
            java.lang.String r10 = r0.a(r4)
            r15 = 0
            r5[r15] = r10
            r10 = 1
            r5[r10] = r8
            r8 = 2
            r5[r8] = r6
            java.lang.Object[] r5 = java.util.Arrays.copyOf(r5, r9)
            java.lang.String r5 = java.lang.String.format(r12, r5)
            com.fossil.ee7.a(r5, r11)
            float r6 = (float) r15
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x0424
            float r6 = (float) r10
            float r6 = r6 / r4
            r4 = 3600(0xe10, float:5.045E-42)
            float r4 = (float) r4
            float r6 = r6 * r4
            java.lang.String r4 = com.fossil.re5.a(r6, r10)
            com.fossil.we7 r6 = com.fossil.we7.a
            r8 = 3
            java.lang.Object[] r6 = new java.lang.Object[r8]
            r6[r15] = r4
            r6[r10] = r14
            r4 = 2
            r6[r4] = r13
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r6, r8)
            java.lang.String r4 = java.lang.String.format(r12, r4)
            com.fossil.ee7.a(r4, r11)
            goto L_0x0426
        L_0x0424:
            r4 = r20
        L_0x0426:
            com.fossil.i97 r6 = com.fossil.i97.a
            goto L_0x0438
        L_0x0429:
            r4 = r20
            r5 = r4
            goto L_0x0438
        L_0x042d:
            r31 = r9
            r28 = r10
            r29 = r12
            r1 = r20
            r4 = r1
            r5 = r4
            r7 = r5
        L_0x0438:
            com.fossil.ub5 r6 = com.fossil.ub5.OUTDOOR
            if (r3 != r6) goto L_0x048e
            if (r2 != 0) goto L_0x043f
            goto L_0x0450
        L_0x043f:
            int[] r3 = com.fossil.bf6.a
            int r2 = r2.ordinal()
            r2 = r3[r2]
            r3 = 1
            if (r2 == r3) goto L_0x0456
            r3 = 2
            if (r2 == r3) goto L_0x0456
            r3 = 3
            if (r2 == r3) goto L_0x0453
        L_0x0450:
            java.lang.String r2 = ""
            goto L_0x0458
        L_0x0453:
            r2 = r27
            goto L_0x0458
        L_0x0456:
            r2 = r33
        L_0x0458:
            com.portfolio.platform.data.model.diana.workout.WorkoutCadence r3 = r34.getCadence()
            if (r3 == 0) goto L_0x048e
            java.lang.Integer r3 = r3.getAverage()
            if (r3 == 0) goto L_0x048e
            int r3 = r3.intValue()
            if (r3 <= 0) goto L_0x048c
            com.fossil.we7 r3 = com.fossil.we7.a
            r6 = 2
            java.lang.Object[] r3 = new java.lang.Object[r6]
            com.portfolio.platform.data.model.diana.workout.WorkoutCadence r8 = r34.getCadence()
            java.lang.Integer r8 = r8.getAverage()
            r9 = 0
            r3[r9] = r8
            r8 = 1
            r3[r8] = r2
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r3, r6)
            java.lang.String r3 = "%d %s"
            java.lang.String r2 = java.lang.String.format(r3, r2)
            com.fossil.ee7.a(r2, r11)
            r20 = r2
        L_0x048c:
            com.fossil.i97 r2 = com.fossil.i97.a
        L_0x048e:
            r19 = r1
            r18 = r5
            r21 = r20
            r20 = r7
            goto L_0x04c7
        L_0x0497:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r14)
            throw r1
        L_0x049d:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r14)
            throw r1
        L_0x04a3:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r14)
            throw r1
        L_0x04a9:
            com.fossil.x87 r1 = new com.fossil.x87
            r1.<init>(r14)
            throw r1
        L_0x04af:
            r26 = r4
            r25 = r5
            r23 = r6
            r31 = r9
            r28 = r10
            r29 = r12
            r24 = r13
            r22 = r15
        L_0x04bf:
            r4 = r20
            r18 = r4
            r19 = r18
            r21 = r19
        L_0x04c7:
            com.fossil.af6$e r1 = new com.fossil.af6$e
            java.lang.Object r2 = r31.getFirst()
            java.lang.Number r2 = (java.lang.Number) r2
            int r8 = r2.intValue()
            java.lang.String r2 = "title"
            r3 = r29
            com.fossil.ee7.a(r3, r2)
            java.lang.String r2 = "time"
            r5 = r28
            com.fossil.ee7.a(r5, r2)
            r7 = r1
            r9 = r3
            r10 = r5
            r11 = r26
            r12 = r25
            r13 = r24
            r14 = r23
            r15 = r22
            r16 = r17
            r17 = r20
            r20 = r4
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            return r1
        L_0x04f8:
            com.fossil.ee7.a()
            throw r5
        L_0x04fc:
            com.fossil.ee7.a()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.af6.a(android.content.Context, com.portfolio.platform.data.model.diana.workout.WorkoutSession):com.fossil.af6$e");
    }

    @DexIgnore
    public final String a(float f2) {
        if (f2 <= ((float) 0)) {
            return "--";
        }
        r87<Integer, Integer> b2 = b(f2);
        we7 we7 = we7.a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{b2.getFirst(), b2.getSecond()}, 2));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
