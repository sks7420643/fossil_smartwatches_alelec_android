package com.fossil;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.facebook.places.model.PlaceFields;
import com.fossil.a12;
import com.google.android.gms.location.LocationRequest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nj4 implements a12.b, a12.c, d53 {
    @DexIgnore
    public static /* final */ String h; // = "nj4";
    @DexIgnore
    public static nj4 i;
    @DexIgnore
    public a12 a;
    @DexIgnore
    public CopyOnWriteArrayList<b> b; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Context c;
    @DexIgnore
    public Timer d;
    @DexIgnore
    public TimerTask e;
    @DexIgnore
    public double f;
    @DexIgnore
    public double g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TimerTask {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            SecureRandom secureRandom = new SecureRandom();
            nj4 nj4 = nj4.this;
            double unused = nj4.f = nj4.f + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            nj4 nj42 = nj4.this;
            double unused2 = nj42.g = nj42.g + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Location location = new Location("");
            location.setLatitude(nj4.this.f + 10.7604877d);
            location.setLongitude(nj4.this.g + 106.698541d);
            Iterator it = nj4.this.b.iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(location, 1);
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Location location, int i);
    }

    @DexIgnore
    public final void d() {
        Timer timer = this.d;
        if (timer != null) {
            timer.cancel();
        }
        TimerTask timerTask = this.e;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.d53
    public void onLocationChanged(Location location) {
        String str = h;
        Log.d(str, "Inside " + h + ".onLocationUpdated - location=" + location);
        CopyOnWriteArrayList<b> copyOnWriteArrayList = this.b;
        if (copyOnWriteArrayList != null) {
            Iterator<b> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().a(location, 1);
            }
        }
    }

    @DexIgnore
    public final void c() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.k(0);
        locationRequest.l(1000);
        locationRequest.b(100);
        e53.d.a(this.a, locationRequest, this);
    }

    @DexIgnore
    public static synchronized nj4 a(Context context) {
        nj4 nj4;
        synchronized (nj4.class) {
            if (i == null) {
                i = new nj4();
            }
            i.c = context.getApplicationContext();
            nj4 = i;
        }
        return nj4;
    }

    @DexIgnore
    public final void b() {
        d();
        this.d = new Timer();
        a aVar = new a();
        this.e = aVar;
        this.d.schedule(aVar, 0, 1000);
    }

    @DexIgnore
    public void a(b bVar) {
        if (!mj4.a(this.c)) {
            bVar.a(null, -1);
            return;
        }
        String str = h;
        Log.i(str, "Register Location Service - callback=" + bVar + ", size=" + this.b.size());
        this.b.add(bVar);
        if (lj4.a(this.c, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            b();
            return;
        }
        if (this.a == null) {
            a12.a aVar = new a12.a(this.c);
            aVar.a(e53.c);
            aVar.a((a12.b) this);
            aVar.a((a12.c) this);
            this.a = aVar.a();
        }
        int a2 = a();
        if (a2 != 0) {
            bVar.a(null, a2);
        } else {
            this.a.c();
        }
    }

    @DexIgnore
    public void b(b bVar) {
        this.b.remove(bVar);
        String str = h;
        Log.i(str, "Unregister Location Service - callback=" + bVar + ", size=" + this.b.size());
        if (lj4.a(this.c, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            d();
            return;
        }
        a12 a12 = this.a;
        if (a12 != null && a12.g()) {
            e53.d.a(this.a, this);
            this.a.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.t12
    public void b(Bundle bundle) {
        Log.i(h, "MFLocationService is connected");
        c();
    }

    @DexIgnore
    public int a() {
        boolean z;
        boolean z2;
        if (p02.f(this.c) != 0) {
            return -2;
        }
        LocationManager locationManager = (LocationManager) this.c.getSystemService(PlaceFields.LOCATION);
        try {
            z = locationManager.isProviderEnabled("gps");
            try {
                z2 = locationManager.isProviderEnabled("network");
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            z = false;
            z2 = false;
            if (!z) {
            }
            return 0;
        }
        if (!z || z2) {
            return 0;
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.t12
    public void a(int i2) {
        String str = h;
        Log.i(str, "MFLocationService is suspended - i=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.a22
    public void a(i02 i02) {
        Log.e(h, "MFLocationService is failed to connect");
    }
}
