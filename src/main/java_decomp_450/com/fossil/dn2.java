package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn2 extends tm2 implements bn2 {
    @DexIgnore
    public dn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.ICircleDelegate");
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final int a() throws RemoteException {
        Parcel a = a(18, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final boolean b(bn2 bn2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bn2);
        Parcel a = a(17, zza);
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final String getId() throws RemoteException {
        Parcel a = a(2, zza());
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void remove() throws RemoteException {
        b(1, zza());
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setCenter(LatLng latLng) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLng);
        b(3, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setFillColor(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        b(11, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setRadius(double d) throws RemoteException {
        Parcel zza = zza();
        zza.writeDouble(d);
        b(5, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setStrokeColor(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        b(9, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setStrokeWidth(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(7, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(15, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void setZIndex(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(13, zza);
    }

    @DexIgnore
    @Override // com.fossil.bn2
    public final void a(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(19, zza);
    }
}
