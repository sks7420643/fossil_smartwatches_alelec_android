package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am7 {
    @DexIgnore
    public static /* final */ Object a; // = new lm7("CONDITION_FALSE");

    /*
    static {
        new lm7("LIST_EMPTY");
    }
    */

    @DexIgnore
    public static final Object a() {
        return a;
    }

    @DexIgnore
    public static final bm7 a(Object obj) {
        bm7 bm7;
        im7 im7 = (im7) (!(obj instanceof im7) ? null : obj);
        if (im7 != null && (bm7 = im7.a) != null) {
            return bm7;
        }
        if (obj != null) {
            return (bm7) obj;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
