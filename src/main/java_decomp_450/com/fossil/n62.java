package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.h62;
import com.fossil.o62;
import com.fossil.v02;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n62<T extends IInterface> extends h62<T> implements v02.f, o62.a {
    @DexIgnore
    public /* final */ j62 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public n62(android.content.Context r10, android.os.Looper r11, int r12, com.fossil.j62 r13, com.fossil.t12 r14, com.fossil.a22 r15) {
        /*
            r9 = this;
            com.fossil.p62 r3 = com.fossil.p62.a(r10)
            com.fossil.l02 r4 = com.fossil.l02.a()
            com.fossil.a72.a(r14)
            r7 = r14
            com.fossil.t12 r7 = (com.fossil.t12) r7
            com.fossil.a72.a(r15)
            r8 = r15
            com.fossil.a22 r8 = (com.fossil.a22) r8
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n62.<init>(android.content.Context, android.os.Looper, int, com.fossil.j62, com.fossil.t12, com.fossil.a22):void");
    }

    @DexIgnore
    public static h62.a a(t12 t12) {
        if (t12 == null) {
            return null;
        }
        return new d82(t12);
    }

    @DexIgnore
    public final j62 H() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    public final Set<Scope> b(Set<Scope> set) {
        Set<Scope> a = a(set);
        for (Scope scope : a) {
            if (!set.contains(scope)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a;
    }

    @DexIgnore
    @Override // com.fossil.v02.f
    public Set<Scope> e() {
        return n() ? this.C : Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62
    public int k() {
        return super.k();
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final Account u() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final Set<Scope> z() {
        return this.C;
    }

    @DexIgnore
    public static h62.b a(a22 a22) {
        if (a22 == null) {
            return null;
        }
        return new c82(a22);
    }

    @DexIgnore
    @Deprecated
    public n62(Context context, Looper looper, int i, j62 j62, a12.b bVar, a12.c cVar) {
        this(context, looper, i, j62, (t12) bVar, (a22) cVar);
    }

    @DexIgnore
    public n62(Context context, Looper looper, p62 p62, l02 l02, int i, j62 j62, t12 t12, a22 a22) {
        super(context, looper, p62, l02, i, a(t12), a(a22), j62.g());
        this.B = j62;
        this.D = j62.a();
        this.C = b(j62.d());
    }
}
