package com.fossil;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g40 {
    @DexIgnore
    public static /* final */ sy<?, ?, ?> c; // = new sy<>(Object.class, Object.class, Object.class, Collections.singletonList(new hy(Object.class, Object.class, Object.class, Collections.emptyList(), new h30(), null)), null);
    @DexIgnore
    public /* final */ n4<t50, sy<?, ?, ?>> a; // = new n4<>();
    @DexIgnore
    public /* final */ AtomicReference<t50> b; // = new AtomicReference<>();

    @DexIgnore
    public boolean a(sy<?, ?, ?> syVar) {
        return c.equals(syVar);
    }

    @DexIgnore
    public final t50 b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        t50 andSet = this.b.getAndSet(null);
        if (andSet == null) {
            andSet = new t50();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public <Data, TResource, Transcode> sy<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        sy<Data, TResource, Transcode> syVar;
        t50 b2 = b(cls, cls2, cls3);
        synchronized (this.a) {
            syVar = (sy<Data, TResource, Transcode>) this.a.get(b2);
        }
        this.b.set(b2);
        return syVar;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, sy<?, ?, ?> syVar) {
        synchronized (this.a) {
            n4<t50, sy<?, ?, ?>> n4Var = this.a;
            t50 t50 = new t50(cls, cls2, cls3);
            if (syVar == null) {
                syVar = c;
            }
            n4Var.put(t50, syVar);
        }
    }
}
