package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class do1 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ em1 CREATOR; // = new em1(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public do1(String str, byte[] bArr) {
        this.a = str;
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.P4, this.a), r51.W2, yz0.a(this.b, (String) null, 1));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeByteArray(this.b);
    }
}
