package com.fossil;

import com.fossil.r80;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class k21 extends ce7 implements gd7<byte[], r80> {
    @DexIgnore
    public k21(r80.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final uf7 getOwner() {
        return te7.a(r80.b.class);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/HeartRateModeConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public r80 invoke(byte[] bArr) {
        return ((r80.b) ((vd7) this).receiver).a(bArr);
    }
}
