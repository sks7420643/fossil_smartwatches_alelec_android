package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ o43 f;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mo2(sn2 sn2, String str, o43 o43) {
        super(sn2);
        this.g = sn2;
        this.e = str;
        this.f = o43;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.g.h.getMaxUserProperties(this.e, this.f);
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void b() {
        this.f.a(null);
    }
}
