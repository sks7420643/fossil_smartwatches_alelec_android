package com.fossil;

import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl7 {
    @DexIgnore
    public static /* final */ xl7 a; // = new xl7();

    @DexIgnore
    public final List<MainDispatcherFactory> a() {
        MainDispatcherFactory mainDispatcherFactory;
        if (!yl7.a()) {
            return a(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
        try {
            ArrayList arrayList = new ArrayList(2);
            MainDispatcherFactory mainDispatcherFactory2 = null;
            try {
                mainDispatcherFactory = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.android.AndroidDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException unused) {
                mainDispatcherFactory = null;
            }
            if (mainDispatcherFactory != null) {
                arrayList.add(mainDispatcherFactory);
            }
            try {
                mainDispatcherFactory2 = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.test.internal.TestMainDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException unused2) {
            }
            if (mainDispatcherFactory2 == null) {
                return arrayList;
            }
            arrayList.add(mainDispatcherFactory2);
            return arrayList;
        } catch (Throwable unused3) {
            return a(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
    }

    @DexIgnore
    public final <S> List<S> b(Class<S> cls, ClassLoader classLoader) {
        ArrayList<URL> list = Collections.list(classLoader.getResources("META-INF/services/" + cls.getName()));
        ee7.a((Object) list, "java.util.Collections.list(this)");
        ArrayList arrayList = new ArrayList();
        for (URL url : list) {
            ba7.a((Collection) arrayList, (Iterable) a.a(url));
        }
        Set<String> q = ea7.q(arrayList);
        if (!q.isEmpty()) {
            ArrayList arrayList2 = new ArrayList(x97.a(q, 10));
            for (String str : q) {
                arrayList2.add(a.a(str, classLoader, cls));
            }
            return arrayList2;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }

    @DexIgnore
    public final <S> List<S> a(Class<S> cls, ClassLoader classLoader) {
        try {
            return b(cls, classLoader);
        } catch (Throwable unused) {
            return ea7.n(ServiceLoader.load(cls, classLoader));
        }
    }

    @DexIgnore
    public final <S> S a(String str, ClassLoader classLoader, Class<S> cls) {
        Class<?> cls2 = Class.forName(str, false, classLoader);
        if (cls.isAssignableFrom(cls2)) {
            return cls.cast(cls2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        }
        throw new IllegalArgumentException(("Expected service of class " + cls + ", but found " + cls2).toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004d, code lost:
        com.fossil.hc7.a(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0053, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0058, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        com.fossil.i87.a(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        throw r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0077, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0078, code lost:
        com.fossil.hc7.a(r0, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007b, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> a(java.net.URL r6) {
        /*
            r5 = this;
            java.lang.String r0 = r6.toString()
            r1 = 0
            r2 = 2
            r3 = 0
            java.lang.String r4 = "jar"
            boolean r4 = com.fossil.mh7.c(r0, r4, r1, r2, r3)
            if (r4 == 0) goto L_0x005d
            java.lang.String r6 = "jar:file:"
            java.lang.String r6 = com.fossil.nh7.a(r0, r6, r3, r2, r3)
            r4 = 33
            java.lang.String r6 = com.fossil.nh7.b(r6, r4, r3, r2, r3)
            java.lang.String r4 = "!/"
            java.lang.String r0 = com.fossil.nh7.a(r0, r4, r3, r2, r3)
            java.util.jar.JarFile r2 = new java.util.jar.JarFile
            r2.<init>(r6, r1)
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ all -> 0x0051 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ all -> 0x0051 }
            java.util.zip.ZipEntry r4 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0051 }
            r4.<init>(r0)     // Catch:{ all -> 0x0051 }
            java.io.InputStream r0 = r2.getInputStream(r4)     // Catch:{ all -> 0x0051 }
            java.lang.String r4 = "UTF-8"
            r1.<init>(r0, r4)     // Catch:{ all -> 0x0051 }
            r6.<init>(r1)     // Catch:{ all -> 0x0051 }
            com.fossil.xl7 r0 = com.fossil.xl7.a     // Catch:{ all -> 0x004a }
            java.util.List r0 = r0.a(r6)     // Catch:{ all -> 0x004a }
            com.fossil.hc7.a(r6, r3)
            r2.close()     // Catch:{ all -> 0x0048 }
            return r0
        L_0x0048:
            r6 = move-exception
            throw r6
        L_0x004a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004c }
        L_0x004c:
            r1 = move-exception
            com.fossil.hc7.a(r6, r0)
            throw r1
        L_0x0051:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x0053 }
        L_0x0053:
            r0 = move-exception
            r2.close()     // Catch:{ all -> 0x0058 }
            throw r0
        L_0x0058:
            r0 = move-exception
            com.fossil.i87.a(r6, r0)
            throw r6
        L_0x005d:
            java.io.BufferedReader r0 = new java.io.BufferedReader
            java.io.InputStreamReader r1 = new java.io.InputStreamReader
            java.io.InputStream r6 = r6.openStream()
            r1.<init>(r6)
            r0.<init>(r1)
            com.fossil.xl7 r6 = com.fossil.xl7.a     // Catch:{ all -> 0x0075 }
            java.util.List r6 = r6.a(r0)     // Catch:{ all -> 0x0075 }
            com.fossil.hc7.a(r0, r3)
            return r6
        L_0x0075:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x0077 }
        L_0x0077:
            r1 = move-exception
            com.fossil.hc7.a(r0, r6)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xl7.a(java.net.URL):java.util.List");
    }

    @DexIgnore
    public final List<String> a(BufferedReader bufferedReader) {
        boolean z;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return ea7.n(linkedHashSet);
            }
            String b = nh7.b(readLine, "#", (String) null, 2, (Object) null);
            if (b != null) {
                String obj = nh7.d((CharSequence) b).toString();
                boolean z2 = false;
                int i = 0;
                while (true) {
                    if (i >= obj.length()) {
                        z = true;
                        break;
                    }
                    char charAt = obj.charAt(i);
                    if (!(charAt == '.' || Character.isJavaIdentifierPart(charAt))) {
                        z = false;
                        break;
                    }
                    i++;
                }
                if (z) {
                    if (obj.length() > 0) {
                        z2 = true;
                    }
                    if (z2) {
                        linkedHashSet.add(obj);
                    }
                } else {
                    throw new IllegalArgumentException(("Illegal service provider class name: " + obj).toString());
                }
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }
}
