package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface hy5 extends dl4<gy5> {
    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(ArrayList<bt5> arrayList);

    @DexIgnore
    void a(List<bt5> list, FilterQueryProvider filterQueryProvider, int i);

    @DexIgnore
    void s();

    @DexIgnore
    void u0();
}
