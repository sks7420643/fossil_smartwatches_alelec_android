package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b15 extends a15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        I.put(2131363342, 2);
        I.put(2131363022, 3);
        I.put(2131362682, 4);
        I.put(2131362396, 5);
        I.put(2131362395, 6);
        I.put(2131362115, 7);
        I.put(2131362224, 8);
        I.put(2131362235, 9);
        I.put(2131362237, 10);
        I.put(2131362226, 11);
        I.put(2131362436, 12);
        I.put(2131362399, 13);
        I.put(2131362274, 14);
        I.put(2131361936, 15);
    }
    */

    @DexIgnore
    public b15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 16, H, I));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public b15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[15], (ConstraintLayout) objArr[7], (FlexibleEditText) objArr[8], (FlexibleEditText) objArr[11], (FlexibleEditText) objArr[9], (FlexibleEditText) objArr[10], (FlexibleButton) objArr[14], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (RTLImageView) objArr[1], (RTLImageView) objArr[4], (ConstraintLayout) objArr[0], (ScrollView) objArr[3], (FlexibleTextView) objArr[2]);
        this.G = -1;
        ((a15) this).D.setTag(null);
        a(view);
        f();
    }
}
