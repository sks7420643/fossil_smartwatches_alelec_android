package com.fossil;

import java.net.InetSocketAddress;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no7 {
    @DexIgnore
    public /* final */ nn7 a;
    @DexIgnore
    public /* final */ Proxy b;
    @DexIgnore
    public /* final */ InetSocketAddress c;

    @DexIgnore
    public no7(nn7 nn7, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (nn7 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.a = nn7;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    @DexIgnore
    public nn7 a() {
        return this.a;
    }

    @DexIgnore
    public Proxy b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.a.i != null && this.b.type() == Proxy.Type.HTTP;
    }

    @DexIgnore
    public InetSocketAddress d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof no7) {
            no7 no7 = (no7) obj;
            return no7.a.equals(this.a) && no7.b.equals(this.b) && no7.c.equals(this.c);
        }
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Route{" + this.c + "}";
    }
}
