package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cd2 implements Parcelable.Creator<ic2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ic2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        Boolean bool = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                str = j72.e(parcel, a);
            } else if (a2 == 2) {
                i = j72.q(parcel, a);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                bool = j72.j(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new ic2(str, i, bool);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ic2[] newArray(int i) {
        return new ic2[i];
    }
}
