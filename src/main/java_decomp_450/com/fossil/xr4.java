package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr4 extends go5 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<o25> f;
    @DexIgnore
    public /* final */ List<Fragment> g; // = new ArrayList();
    @DexIgnore
    public int h;
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xr4.p;
        }

        @DexIgnore
        public final xr4 b() {
            return new xr4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ xr4 a;

        @DexIgnore
        public b(xr4 xr4) {
            this.a = xr4;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            if (gVar != null) {
                this.a.n(gVar.c());
            }
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ xr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xr4 xr4) {
            super(1);
            this.this$0 = xr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCFindFriendsActivity.y.a(this.this$0, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ xr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xr4 xr4) {
            super(1);
            this.this$0 = xr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCNotificationActivity.y.a(this.this$0);
        }
    }

    /*
    static {
        String simpleName = xr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCTabsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<o25> qw6 = this.f;
        if (qw6 != null) {
            o25 a2 = qw6.a();
            if (a2 != null) {
                a2.u.a((TabLayout.d) new b(this));
                RTLImageView rTLImageView = a2.r;
                ee7.a((Object) rTLImageView, "imgSearch");
                du4.a(rTLImageView, new c(this));
                RTLImageView rTLImageView2 = a2.q;
                ee7.a((Object) rTLImageView2, "imgNotification");
                du4.a(rTLImageView2, new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().d(p, "Inside .initTabs");
        Fragment b2 = getChildFragmentManager().b(bs4.r.a());
        Fragment b3 = getChildFragmentManager().b(zs4.q.a());
        Fragment b4 = getChildFragmentManager().b(it4.r.a());
        if (b2 == null) {
            b2 = bs4.r.b();
        }
        if (b3 == null) {
            b3 = zs4.q.b();
        }
        if (b4 == null) {
            b4 = it4.r.b();
        }
        this.g.clear();
        this.g.add(b2);
        this.g.add(b3);
        this.g.add(b4);
        wr4 wr4 = new wr4(this.g, this);
        qw6<o25> qw6 = this.f;
        if (qw6 != null) {
            o25 a2 = qw6.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.t;
                ee7.a((Object) viewPager2, "this");
                viewPager2.setAdapter(wr4);
                viewPager2.setUserInputEnabled(false);
                viewPager2.setOffscreenPageLimit(this.g.size());
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.g.get(i2));
        this.h = i2;
        qw6<o25> qw6 = this.f;
        if (qw6 != null) {
            o25 a2 = qw6.a();
            if (a2 != null) {
                a2.t.a(this.h, true);
                Fragment fragment = this.g.get(i2);
                if (fragment instanceof zs4) {
                    RTLImageView rTLImageView = a2.r;
                    ee7.a((Object) rTLImageView, "imgSearch");
                    rTLImageView.setVisibility(0);
                    this.i = "bc_friend_tab";
                } else if (fragment instanceof it4) {
                    RTLImageView rTLImageView2 = a2.r;
                    ee7.a((Object) rTLImageView2, "imgSearch");
                    rTLImageView2.setVisibility(4);
                    this.i = "bc_history_tab";
                } else {
                    this.i = "";
                    RTLImageView rTLImageView3 = a2.r;
                    ee7.a((Object) rTLImageView3, "imgSearch");
                    rTLImageView3.setVisibility(4);
                }
                qd5 c2 = qd5.f.c();
                String str2 = this.i;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    c2.a(str2, activity);
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.app.Activity");
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        o25 o25 = (o25) qb.a(layoutInflater, 2131558572, viewGroup, false, a1());
        this.f = new qw6<>(this, o25);
        ee7.a((Object) o25, "binding");
        return o25.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qd5 c2 = qd5.f.c();
        String str = this.i;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a(str, activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        g1();
    }
}
