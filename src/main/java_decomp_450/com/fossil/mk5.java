package com.fossil;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.px6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk5 {
    @DexIgnore
    public static /* final */ HashMap<String, c> o; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, j> p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ d b; // = new d(this.k);
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, Long> c; // = new ConcurrentHashMap<>();
    @DexIgnore
    public /* final */ n87 d; // = o87.a(o.INSTANCE);
    @DexIgnore
    public /* final */ n87 e; // = o87.a(p.INSTANCE);
    @DexIgnore
    public int f; // = 1380;
    @DexIgnore
    public int g; // = 1140;
    @DexIgnore
    public h h;
    @DexIgnore
    public f i;
    @DexIgnore
    public /* final */ Queue<NotificationBaseObj> j; // = mz3.a(jx3.create(20));
    @DexIgnore
    public /* final */ ch5 k;
    @DexIgnore
    public /* final */ DNDSettingsDatabase l;
    @DexIgnore
    public /* final */ QuickResponseRepository m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HashMap<String, c> a() {
            if (mk5.o.isEmpty()) {
                int i = 1;
                for (T t : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = t.getPackageName();
                    if (!ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        mk5.o.put(t.getPackageName(), new c(i, t, false, false));
                    } else {
                        mk5.o.put(t.getPackageName(), new c(i, t, false, true));
                    }
                    i++;
                }
            }
            return mk5.o;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        ACTIVE,
        SILENT,
        SKIP
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public c(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            ee7.b(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public long h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public int k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(zd7 zd7) {
                this();
            }
        }

        /*
        static {
            new a(null);
        }
        */

        @DexIgnore
        public e() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
            this.g = "";
        }

        @DexIgnore
        public final void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final void c(String str) {
            ee7.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void d(String str) {
            ee7.b(str, "value");
            this.e = a(str);
        }

        @DexIgnore
        public final String e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            if (!ee7.a((Object) this.a, (Object) eVar.a) || this.h != eVar.h) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String g() {
            return this.f;
        }

        @DexIgnore
        public final String h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final long i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.l;
        }

        @DexIgnore
        public final boolean k() {
            return this.m;
        }

        @DexIgnore
        public final boolean l() {
            return this.j;
        }

        @DexIgnore
        public final boolean m() {
            return this.i;
        }

        @DexIgnore
        public final String a() {
            return this.g;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.k;
        }

        @DexIgnore
        public final void e(String str) {
            ee7.b(str, "value");
            this.f = a(str);
        }

        @DexIgnore
        public final String f() {
            return this.e;
        }

        @DexIgnore
        public final void a(long j2) {
            this.h = j2;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.j = z;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.l = z;
        }

        @DexIgnore
        public final boolean b(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final boolean c(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexIgnore
        public final boolean a(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!nh7.a((CharSequence) tag, (CharSequence) "sms", true) && !nh7.a((CharSequence) tag, (CharSequence) "mms", true) && !nh7.a((CharSequence) tag, (CharSequence) "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public e(StatusBarNotification statusBarNotification) {
            this();
            String str;
            Bundle bundle;
            DianaNotificationObj.AApplicationName a2;
            ee7.b(statusBarNotification, "sbn");
            this.a = a((long) statusBarNotification.getId(), statusBarNotification.getTag());
            String packageName = statusBarNotification.getPackageName();
            ee7.a((Object) packageName, "sbn.packageName");
            this.c = packageName;
            c cVar = mk5.q.a().get(this.c);
            String d2 = (cVar == null || (a2 = cVar.a()) == null || (d2 = a2.getAppName()) == null) ? PortfolioApp.g0.c().d(this.c) : d2;
            CharSequence charSequence = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            String obj = (charSequence != null ? charSequence : d2).toString();
            this.d = obj;
            d(obj);
            CharSequence charSequence2 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            String str2 = "";
            if (!(charSequence2 == null || mh7.a(charSequence2))) {
                Notification notification = statusBarNotification.getNotification();
                str = String.valueOf((notification == null || (bundle = notification.extras) == null) ? null : bundle.getCharSequence("android.bigText"));
            } else {
                CharSequence charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.text");
                str = (charSequence3 == null ? str2 : charSequence3).toString();
            }
            e(str);
            String str3 = statusBarNotification.getNotification().category;
            this.g = str3 != null ? str3 : str2;
            this.i = c(statusBarNotification);
            this.j = b(statusBarNotification);
            this.h = statusBarNotification.getNotification().when;
            this.k = statusBarNotification.getNotification().priority;
            this.l = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            if (a(statusBarNotification)) {
                this.m = true;
                CharSequence charSequence4 = statusBarNotification.getNotification().tickerText;
                if (charSequence4 != null) {
                    int length = charSequence4.length();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            i2 = -1;
                            break;
                        } else if (ee7.a((Object) String.valueOf(charSequence4.charAt(i2)), (Object) ":")) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        String obj2 = charSequence4.subSequence(0, i2).toString();
                        if (obj2 != null) {
                            d(nh7.d((CharSequence) obj2).toString());
                            String obj3 = charSequence4.subSequence(i2 + 1, charSequence4.length()).toString();
                            if (obj3 != null) {
                                e(nh7.d((CharSequence) obj3).toString());
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                        throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new ah7("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final String a(long j2, String str) {
            return j2 + ':' + str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends ContentObserver {
        @DexIgnore
        public b a; // = new b(this, 0, null, 0, 0, 15, null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    f fVar = this.this$0;
                    b b = fVar.a();
                    if (b == null) {
                        b = new b(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    fVar.a = b;
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class b {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public b(f fVar, long j, String str, int i, long j2) {
                ee7.b(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ b(f fVar, long j, String str, int i, long j2, int i2, zd7 zd7) {
                this(fVar, (i2 & 1) != 0 ? -1 : j, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    b b = this.this$0.a();
                    if (b != null) {
                        b a = this.this$0.a;
                        if (a != null) {
                            if (a.a() < b.a()) {
                                if (b.c() == 3) {
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String d = mk5.this.d();
                                    local.d(d, "processMissedCall - number = " + b.b());
                                    mk5.this.a(b.b(), new Date(), g.MISSED);
                                } else if (b.c() == 5 || b.c() == 6 || b.c() == 1) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String d2 = mk5.this.d();
                                    local2.d(d2, "processHangUpCall - number = " + b.b());
                                    mk5.this.a(b.b(), new Date(), g.PICKED);
                                }
                            }
                            this.this$0.a = b;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f() {
            super(null);
            px6.a aVar = px6.a;
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            if (aVar.d(applicationContext)) {
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            b();
        }

        @DexIgnore
        public final void b() {
            px6.a aVar = px6.a;
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            if (!aVar.d(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(mk5.this.d(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new c(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            b();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0067, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0068, code lost:
            com.fossil.hc7.a(r4, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x006c, code lost:
            throw r0;
         */
        @DexIgnore
        @android.annotation.SuppressLint({"MissingPermission"})
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.mk5.f.b a() {
            /*
                r19 = this;
                java.lang.String r0 = "type"
                java.lang.String r1 = "date"
                java.lang.String r2 = "_id"
                java.lang.String r3 = "number"
                java.lang.String[] r6 = new java.lang.String[]{r2, r3, r1, r0}
                java.lang.String r9 = "date DESC LIMIT 1"
                r10 = 0
                com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x006e }
                com.portfolio.platform.PortfolioApp r4 = r4.c()     // Catch:{ Exception -> 0x006e }
                android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ Exception -> 0x006e }
                android.net.Uri r5 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x006e }
                r7 = 0
                r8 = 0
                android.database.Cursor r4 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x006e }
                if (r4 == 0) goto L_0x006d
                boolean r5 = r4.moveToFirst()     // Catch:{ all -> 0x0064 }
                if (r5 == 0) goto L_0x005b
                int r2 = r4.getColumnIndex(r2)     // Catch:{ all -> 0x0064 }
                long r13 = r4.getLong(r2)     // Catch:{ all -> 0x0064 }
                int r2 = r4.getColumnIndex(r3)     // Catch:{ all -> 0x0064 }
                java.lang.String r15 = r4.getString(r2)     // Catch:{ all -> 0x0064 }
                int r0 = r4.getColumnIndex(r0)     // Catch:{ all -> 0x0064 }
                int r16 = r4.getInt(r0)     // Catch:{ all -> 0x0064 }
                int r0 = r4.getColumnIndex(r1)     // Catch:{ all -> 0x0064 }
                long r17 = r4.getLong(r0)     // Catch:{ all -> 0x0064 }
                r4.close()     // Catch:{ all -> 0x0064 }
                com.fossil.mk5$f$b r0 = new com.fossil.mk5$f$b     // Catch:{ all -> 0x0064 }
                com.fossil.ee7.a(r15, r3)     // Catch:{ all -> 0x0064 }
                r11 = r0
                r12 = r19
                r11.<init>(r12, r13, r15, r16, r17)     // Catch:{ all -> 0x0064 }
                com.fossil.hc7.a(r4, r10)
                return r0
            L_0x005b:
                r4.close()
                com.fossil.i97 r0 = com.fossil.i97.a
                com.fossil.hc7.a(r4, r10)
                goto L_0x006d
            L_0x0064:
                r0 = move-exception
                r1 = r0
                throw r1     // Catch:{ all -> 0x0067 }
            L_0x0067:
                r0 = move-exception
                r2 = r0
                com.fossil.hc7.a(r4, r1)
                throw r2
            L_0x006d:
                return r10
            L_0x006e:
                r0 = move-exception
                r0.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                r2 = r19
                com.fossil.mk5 r3 = com.fossil.mk5.this
                java.lang.String r3 = r3.d()
                java.lang.String r0 = r0.getMessage()
                r1.e(r3, r0)
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.f.a():com.fossil.mk5$f$b");
        }
    }

    @DexIgnore
    public enum g {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends e {
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public final h clone() {
            h hVar = new h();
            hVar.c(e());
            hVar.a(b());
            hVar.b(c());
            hVar.d(f());
            hVar.e(g());
            hVar.f(h());
            hVar.a(i());
            hVar.a(j());
            hVar.c(m());
            hVar.b(l());
            hVar.n = this.n;
            hVar.o = this.o;
            return hVar;
        }

        @DexIgnore
        public final void d(boolean z) {
            this.o = z;
        }

        @DexIgnore
        public final boolean n() {
            return this.o;
        }

        @DexIgnore
        public final String o() {
            return this.n;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public h(String str, String str2, String str3, String str4, Date date) {
            this();
            ee7.b(str, "packageName");
            ee7.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            ee7.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            ee7.b(str4, "message");
            ee7.b(date, GoalPhase.COLUMN_START_DATE);
            c(a(date.getTime(), null));
            b(str);
            f(str2);
            d(str2);
            e(str4);
            a(date.getTime());
            this.n = str3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends e {
        @DexIgnore
        public String n;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public final String n() {
            return this.n;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public i(String str, String str2, String str3, long j) {
            this();
            ee7.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            ee7.b(str2, "message");
            ee7.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            c(a(j, null));
            b(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            f(str);
            d(str);
            e(str2);
            this.n = str3;
            a(j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {455}, m = "checkConditionsToSendToDevice")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(mk5 mk5, fb7 fb7) {
            super(fb7);
            this.this$0 = mk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {386, 391, 398}, m = "handleNewLogic")
    public static final class l extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(mk5 mk5, fb7 fb7) {
            super(fb7);
            this.this$0 = mk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((e) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {334}, m = "handleNotificationAdded")
    public static final class m extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(mk5 mk5, fb7 fb7) {
            super(fb7);
            this.this$0 = mk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b((e) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$initializeInCalls$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class n extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(mk5 mk5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            n nVar = new n(this.this$0, fb7);
            nVar.p$ = (yi7) obj;
            return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((n) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0099, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x009a, code lost:
            com.fossil.hc7.a(r1, r9);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x009d, code lost:
            throw r0;
         */
        @DexIgnore
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                com.fossil.nb7.a()
                int r0 = r8.label
                if (r0 != 0) goto L_0x00b8
                com.fossil.t87.a(r9)
                com.fossil.mk5 r9 = r8.this$0
                java.util.concurrent.ConcurrentHashMap r9 = r9.c
                r9.clear()
                com.fossil.px6$a r9 = com.fossil.px6.a
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                android.content.Context r0 = r0.getApplicationContext()
                java.lang.String r1 = "PortfolioApp.instance.applicationContext"
                com.fossil.ee7.a(r0, r1)
                boolean r9 = r9.d(r0)
                if (r9 != 0) goto L_0x002d
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            L_0x002d:
                java.lang.String r9 = "date"
                java.lang.String r0 = "number"
                java.lang.String r1 = "type"
                java.lang.String[] r4 = new java.lang.String[]{r0, r9, r1}
                java.lang.String r7 = "date DESC"
                long r1 = java.lang.System.currentTimeMillis()
                r5 = 900000(0xdbba0, double:4.44659E-318)
                long r1 = r1 - r5
                java.lang.String r5 = "date >= ? AND type IN(?,?)"
                r3 = 3
                java.lang.String[] r6 = new java.lang.String[r3]
                r3 = 0
                java.lang.String r1 = java.lang.String.valueOf(r1)
                r6[r3] = r1
                r1 = 1
                java.lang.String r2 = "1"
                r6[r1] = r2
                r1 = 2
                java.lang.String r2 = "3"
                r6[r1] = r2
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x009e }
                com.portfolio.platform.PortfolioApp r1 = r1.c()     // Catch:{ Exception -> 0x009e }
                android.content.ContentResolver r2 = r1.getContentResolver()     // Catch:{ Exception -> 0x009e }
                android.net.Uri r3 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x009e }
                android.database.Cursor r1 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x009e }
                if (r1 == 0) goto L_0x00b5
                r2 = 0
            L_0x006a:
                boolean r3 = r1.moveToNext()     // Catch:{ all -> 0x0097 }
                if (r3 == 0) goto L_0x008e
                int r3 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x0097 }
                java.lang.String r3 = r1.getString(r3)     // Catch:{ all -> 0x0097 }
                int r4 = r1.getColumnIndex(r9)     // Catch:{ all -> 0x0097 }
                long r4 = r1.getLong(r4)     // Catch:{ all -> 0x0097 }
                com.fossil.mk5 r6 = r8.this$0     // Catch:{ all -> 0x0097 }
                java.util.concurrent.ConcurrentHashMap r6 = r6.c     // Catch:{ all -> 0x0097 }
                java.lang.Long r4 = com.fossil.pb7.a(r4)     // Catch:{ all -> 0x0097 }
                r6.putIfAbsent(r3, r4)     // Catch:{ all -> 0x0097 }
                goto L_0x006a
            L_0x008e:
                r1.close()     // Catch:{ all -> 0x0097 }
                com.fossil.i97 r9 = com.fossil.i97.a     // Catch:{ all -> 0x0097 }
                com.fossil.hc7.a(r1, r2)
                goto L_0x00b5
            L_0x0097:
                r9 = move-exception
                throw r9     // Catch:{ all -> 0x0099 }
            L_0x0099:
                r0 = move-exception
                com.fossil.hc7.a(r1, r9)
                throw r0
            L_0x009e:
                r9 = move-exception
                r9.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.mk5 r1 = r8.this$0
                java.lang.String r1 = r1.d()
                java.lang.String r9 = r9.getMessage()
                r0.e(r1, r9)
            L_0x00b5:
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            L_0x00b8:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.n.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends fe7 implements vc7<AudioManager> {
        @DexIgnore
        public static /* final */ o INSTANCE; // = new o();

        @DexIgnore
        public o() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final AudioManager invoke() {
            Object systemService = PortfolioApp.g0.c().getSystemService("audio");
            if (systemService != null) {
                return (AudioManager) systemService;
            }
            throw new x87("null cannot be cast to non-null type android.media.AudioManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends fe7 implements vc7<NotificationManager> {
        @DexIgnore
        public static /* final */ p INSTANCE; // = new p();

        @DexIgnore
        public p() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final NotificationManager invoke() {
            Object systemService = PortfolioApp.g0.c().getSystemService("notification");
            if (systemService != null) {
                return (NotificationManager) systemService;
            }
            throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class q extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(mk5 mk5, StatusBarNotification statusBarNotification, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mk5;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            q qVar = new q(this.this$0, this.$sbn, fb7);
            qVar.p$ = (yi7) obj;
            return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((q) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                e eVar = new e(this.$sbn);
                c cVar = mk5.q.a().get(eVar.c());
                if (cVar != null) {
                    eVar.d(cVar.a().getAppName());
                }
                this.this$0.c(new e(this.$sbn));
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$1", f = "DianaNotificationComponent.kt", l = {193}, m = "invokeSuspend")
    public static final class r extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(mk5 mk5, StatusBarNotification statusBarNotification, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mk5;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            r rVar = new r(this.this$0, this.$sbn, fb7);
            rVar.p$ = (yi7) obj;
            return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((r) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                e eVar = new e(this.$sbn);
                String str = this.$sbn.getNotification().category;
                mk5 mk5 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = eVar;
                this.label = 1;
                if (mk5.a(eVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                e eVar2 = (e) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", f = "DianaNotificationComponent.kt", l = {143, 169}, m = "invokeSuspend")
    public static final class s extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startTime;
        @DexIgnore
        public /* final */ /* synthetic */ g $state;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(mk5 mk5, g gVar, String str, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mk5;
            this.$state = gVar;
            this.$phoneNumber = str;
            this.$startTime = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            s sVar = new s(this.this$0, this.$state, this.$phoneNumber, this.$startTime, fb7);
            sVar.p$ = (yi7) obj;
            return sVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((s) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = this.this$0.d();
                local.d(d, "process phone call state " + this.$state + " phoneNumber " + this.$phoneNumber);
                int i2 = nk5.a[this.$state.ordinal()];
                if (i2 == 1) {
                    String a2 = this.this$0.d(this.$phoneNumber);
                    h hVar = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), a2, this.$phoneNumber, "Incoming Call", this.$startTime);
                    this.this$0.a(this.$startTime.getTime());
                    hVar.d(this.this$0.c.containsKey(this.$phoneNumber));
                    this.this$0.c.put(this.$phoneNumber, pb7.a(this.$startTime.getTime()));
                    if (!this.this$0.a(this.$phoneNumber, true)) {
                        return i97.a;
                    }
                    mk5 mk5 = this.this$0;
                    Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                    ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    mk5.d(applicationContext);
                    mk5 mk52 = this.this$0;
                    Context applicationContext2 = PortfolioApp.g0.c().getApplicationContext();
                    ee7.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                    mk52.c(applicationContext2);
                    h b = this.this$0.h;
                    if (b != null) {
                        this.this$0.c(b);
                    }
                    this.this$0.h = hVar.clone();
                    mk5 mk53 = this.this$0;
                    this.L$0 = yi7;
                    this.L$1 = a2;
                    this.L$2 = hVar;
                    this.label = 1;
                    if (mk53.b(hVar, this) == a) {
                        return a;
                    }
                } else if (i2 == 2) {
                    h b2 = this.this$0.h;
                    if (b2 != null && ee7.a((Object) b2.o(), (Object) this.$phoneNumber)) {
                        h hVar2 = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hVar2.c(b2.e());
                        hVar2.a(b2.b());
                        this.this$0.c(hVar2);
                        this.this$0.h = (h) null;
                    }
                    mk5 mk54 = this.this$0;
                    Context applicationContext3 = PortfolioApp.g0.c().getApplicationContext();
                    ee7.a((Object) applicationContext3, "PortfolioApp.instance.applicationContext");
                    mk54.d(applicationContext3);
                } else if (i2 == 3) {
                    if (!this.this$0.a(this.$phoneNumber, true)) {
                        return i97.a;
                    }
                    h b3 = this.this$0.h;
                    if (b3 != null && ee7.a((Object) b3.o(), (Object) this.$phoneNumber)) {
                        h hVar3 = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hVar3.a(b3.b());
                        hVar3.c(b3.e());
                        this.this$0.c(hVar3);
                        this.this$0.h = (h) null;
                    }
                    String a3 = this.this$0.d(this.$phoneNumber);
                    mk5 mk55 = this.this$0;
                    String packageName = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName();
                    String str = this.$phoneNumber;
                    Calendar instance = Calendar.getInstance();
                    ee7.a((Object) instance, "Calendar.getInstance()");
                    Date time = instance.getTime();
                    ee7.a((Object) time, "Calendar.getInstance().time");
                    h hVar4 = new h(packageName, a3, str, "Missed Call", time);
                    this.L$0 = yi7;
                    this.L$1 = a3;
                    this.label = 2;
                    if (mk55.b(hVar4, this) == a) {
                        return a;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                h hVar5 = (h) this.L$2;
                String str2 = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else if (i == 2) {
                String str3 = (String) this.L$1;
                yi7 yi73 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mk5 mk56 = this.this$0;
            Context applicationContext4 = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext4, "PortfolioApp.instance.applicationContext");
            mk56.d(applicationContext4);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", f = "DianaNotificationComponent.kt", l = {182}, m = "invokeSuspend")
    public static final class t extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $content;
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ long $receivedTime;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(mk5 mk5, String str, String str2, long j, fb7 fb7) {
            super(2, fb7);
            this.this$0 = mk5;
            this.$phoneNumber = str;
            this.$content = str2;
            this.$receivedTime = j;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            t tVar = new t(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, fb7);
            tVar.p$ = (yi7) obj;
            return tVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((t) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                FLogger.INSTANCE.getLocal().d(this.this$0.d(), "Process SMS/MMS by old solution");
                if (!this.this$0.a(this.$phoneNumber, false)) {
                    return i97.a;
                }
                String a2 = this.this$0.d(this.$phoneNumber);
                mk5 mk5 = this.this$0;
                i iVar = new i(a2, this.$content, this.$phoneNumber, this.$receivedTime);
                this.L$0 = yi7;
                this.L$1 = a2;
                this.label = 1;
                if (mk5.b(iVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        HashMap<String, j> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new j("com.facebook.orca", "SMS"));
        p = hashMap;
    }
    */

    @DexIgnore
    public mk5(ch5 ch5, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        ee7.b(quickResponseRepository, "mQuickResponseRepository");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = ch5;
        this.l = dNDSettingsDatabase;
        this.m = quickResponseRepository;
        this.n = notificationSettingsDatabase;
        String simpleName = mk5.class.getSimpleName();
        ee7.a((Object) simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
        q.a();
    }

    @DexIgnore
    public final AudioManager b() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final NotificationManager c() {
        return (NotificationManager) this.e.getValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0020;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(java.lang.String r7) {
        /*
            r6 = this;
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r6.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            r1 = 0
            com.portfolio.platform.data.model.NotificationSettingsModel r0 = r0.getNotificationSettingsWithIsCallNoLiveData(r1)
            r2 = 1
            if (r0 == 0) goto L_0x001f
            int r0 = r0.getSettingsType()
            if (r0 == 0) goto L_0x0020
            if (r0 == r2) goto L_0x001a
            r3 = 2
            if (r0 == r3) goto L_0x0021
            goto L_0x0020
        L_0x001a:
            boolean r1 = r6.b(r7)
            goto L_0x0021
        L_0x001f:
            r0 = 0
        L_0x0020:
            r1 = 1
        L_0x0021:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isAllowMessageFromContactName() - from sender "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r7 = " is "
            r4.append(r7)
            r4.append(r1)
            java.lang.String r7 = ", type = type = "
            r4.append(r7)
            r4.append(r0)
            java.lang.String r7 = r4.toString()
            r2.d(r3, r7)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.c(java.lang.String):boolean");
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final ik7 e() {
        return xh7.b(zi7.a(qj7.b()), null, null, new n(this, null), 3, null);
    }

    @DexIgnore
    public final boolean f() {
        if ((c().getNotificationPolicy().priorityCategories & 32) == 0) {
            FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Don't allow any alarms");
            return true;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Allow alarms");
        return false;
    }

    @DexIgnore
    public final boolean g() {
        return b().getStreamVolume(4) == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0104, code lost:
        if (r6 < r0) goto L_0x012d;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean h() {
        /*
            r9 = this;
            com.fossil.ch5 r0 = r9.k
            boolean r0 = r0.J()
            if (r0 == 0) goto L_0x012d
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r0, r1)
            java.util.Date r0 = r0.getTime()
            java.util.Date r0 = com.fossil.zd5.q(r0)
            java.lang.String r2 = "DateHelper.getStartOfDay\u2026endar.getInstance().time)"
            com.fossil.ee7.a(r0, r2)
            long r2 = r0.getTime()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            com.fossil.ee7.a(r0, r1)
            java.util.Date r0 = r0.getTime()
            java.lang.String r1 = "Calendar.getInstance().time"
            com.fossil.ee7.a(r0, r1)
            long r0 = r0.getTime()
            long r0 = r0 - r2
            r2 = 60000(0xea60, float:8.4078E-41)
            long r2 = (long) r2
            long r0 = r0 / r2
            com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase r2 = r9.l
            com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao r2 = r2.getDNDScheduledTimeDao()
            java.util.List r2 = r2.getListDNDScheduledTimeModel()
            boolean r3 = r2.isEmpty()
            r4 = 1
            r3 = r3 ^ r4
            if (r3 == 0) goto L_0x006e
            java.util.Iterator r2 = r2.iterator()
        L_0x0052:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x006e
            java.lang.Object r3 = r2.next()
            com.portfolio.platform.data.model.DNDScheduledTimeModel r3 = (com.portfolio.platform.data.model.DNDScheduledTimeModel) r3
            int r5 = r3.component2()
            int r3 = r3.component3()
            if (r3 != 0) goto L_0x006b
            r9.f = r5
            goto L_0x0052
        L_0x006b:
            r9.g = r5
            goto L_0x0052
        L_0x006e:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r9.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "currentMinutesOfDay = "
            r5.append(r6)
            int r6 = (int) r0
            java.lang.String r6 = r9.a(r6)
            r5.append(r6)
            java.lang.String r6 = " -- mDNDStartTime = "
            r5.append(r6)
            int r6 = r9.f
            java.lang.String r6 = r9.a(r6)
            r5.append(r6)
            java.lang.String r6 = " -- mDNDEndTime = "
            r5.append(r6)
            int r6 = r9.g
            java.lang.String r6 = r9.a(r6)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r2.d(r3, r5)
            int r2 = r9.g
            int r3 = r9.f
            java.lang.String r5 = "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification "
            if (r2 <= r3) goto L_0x00db
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r9.a
            java.lang.String r6 = "case mDNDEndTime greater than mDNDStartTime"
            r2.d(r3, r6)
            int r2 = r9.f
            long r2 = (long) r2
            int r6 = r9.g
            long r6 = (long) r6
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 <= 0) goto L_0x00cb
            goto L_0x012d
        L_0x00cb:
            int r2 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r2 < 0) goto L_0x012d
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            r0.d(r1, r5)
            return r4
        L_0x00db:
            if (r2 >= r3) goto L_0x0112
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r9.a
            java.lang.String r6 = "case mDNDEndTime smaller than mDNDStartTime"
            r2.d(r3, r6)
            int r2 = r9.f
            long r2 = (long) r2
            r6 = 1440(0x5a0, float:2.018E-42)
            long r6 = (long) r6
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 > 0) goto L_0x00f8
            int r2 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x0106
        L_0x00f8:
            r2 = 0
            int r6 = r9.g
            long r6 = (long) r6
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 <= 0) goto L_0x0102
            goto L_0x012d
        L_0x0102:
            int r2 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r2 < 0) goto L_0x012d
        L_0x0106:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            r0.d(r1, r5)
            return r4
        L_0x0112:
            if (r2 != r3) goto L_0x012d
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            java.lang.String r2 = "case mDNDEndTime equals mDNDStartTime"
            r0.d(r1, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            r0.d(r1, r5)
            return r4
        L_0x012d:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.a
            java.lang.String r2 = "isDNDScheduleTimeValid() - DND Schedule is not valid - show notification "
            r0.d(r1, r2)
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.h():boolean");
    }

    @DexIgnore
    public final boolean i() {
        return b().getStreamVolume(5) == 0;
    }

    @DexIgnore
    public final boolean j() {
        return b().getStreamVolume(2) == 0;
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onDeviceConnected, notificationList = " + this.j);
        Queue<NotificationBaseObj> queue = this.j;
        ee7.a((Object) queue, "mDeviceNotifications");
        synchronized (queue) {
            for (NotificationBaseObj notificationBaseObj : this.j) {
                PortfolioApp c2 = PortfolioApp.g0.c();
                String a2 = a();
                ee7.a((Object) notificationBaseObj, "notification");
                c2.b(a2, notificationBaseObj);
            }
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(this.a, "onServiceConnected");
        e();
    }

    @DexIgnore
    public final void m() {
        this.c.clear();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ List<e> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.r();
        @DexIgnore
        public /* final */ ch5 c;

        @DexIgnore
        public d(ch5 ch5) {
            ee7.b(ch5, "mSharedPref");
            this.c = ch5;
        }

        @DexIgnore
        public final void a(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.d(i);
        }

        @DexIgnore
        public final int a() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        @DexIgnore
        public final synchronized e a(e eVar) {
            T t;
            boolean z;
            ee7.b(eVar, "notificationStatus");
            if (this.a.contains(eVar)) {
                eVar = null;
            } else if (mh7.b(eVar.c(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.g0.c()), true)) {
                Iterator<T> it = this.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    T t2 = t;
                    if (!mh7.b(t2.g(), eVar.g(), true) || (t2.i() != eVar.i() && eVar.i() - t2.i() > 1)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    t3.c(eVar.e());
                    t3.a(eVar.i());
                    return null;
                }
                eVar.a(a());
                this.a.add(eVar);
                a(a() + 1);
            } else {
                eVar.a(a());
                this.a.add(eVar);
                a(a() + 1);
            }
            return eVar;
        }

        @DexIgnore
        public final synchronized List<e> a(String str, String str2) {
            ArrayList arrayList;
            ee7.b(str, "realId");
            ee7.b(str2, "packageName");
            List<e> list = this.a;
            arrayList = new ArrayList();
            for (T t : list) {
                T t2 = t;
                if (ee7.a(t2.e(), str) && ee7.a(t2.c(), str2)) {
                    arrayList.add(t);
                }
            }
            this.a.removeAll(arrayList);
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j {
        @DexIgnore
        public String a;

        @DexIgnore
        public j() {
            this.a = "";
        }

        @DexIgnore
        public final boolean a(String str) {
            ee7.b(str, "realId");
            return nh7.a((CharSequence) str, (CharSequence) this.a, true);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public j(String str, String str2) {
            this();
            ee7.b(str, "packageName");
            ee7.b(str2, "tag");
            this.a = str2;
        }
    }

    @DexIgnore
    public final void d(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            f fVar = this.i;
            if (fVar != null) {
                context.getContentResolver().unregisterContentObserver(fVar);
                this.i = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    public final ik7 b(StatusBarNotification statusBarNotification) {
        ee7.b(statusBarNotification, "sbn");
        return xh7.b(zi7.a(qj7.a()), null, null, new r(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final boolean b(e eVar, c cVar) {
        if (!cVar.b() && eVar.d() <= -2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final synchronized void c(e eVar) {
        ee7.b(eVar, "notification");
        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved");
        for (T t2 : this.b.a(eVar.e(), eVar.c())) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t2));
            DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t2.b(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", eVar.c(), "", NotificationBaseObj.ANotificationType.REMOVED), t2.h(), t2.f(), -1, t2.g(), w97.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t2.i()), null, null, 1536, null);
            c(t2.b());
            PortfolioApp.g0.c().b(a(), dianaNotificationObj);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(com.fossil.mk5.e r11, com.fossil.fb7<? super com.fossil.i97> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.mk5.m
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.mk5$m r0 = (com.fossil.mk5.m) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.mk5$m r0 = new com.fossil.mk5$m
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0042
            if (r2 != r3) goto L_0x003a
            java.lang.Object r11 = r0.L$3
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$2
            com.fossil.mk5$c r11 = (com.fossil.mk5.c) r11
            java.lang.Object r11 = r0.L$1
            com.fossil.mk5$e r11 = (com.fossil.mk5.e) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.mk5 r11 = (com.fossil.mk5) r11
            com.fossil.t87.a(r12)
            goto L_0x0160
        L_0x003a:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x0042:
            com.fossil.t87.a(r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r2 = r10.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "handleNotificationAdded "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r12.d(r2, r4)
            com.fossil.mk5$a r12 = com.fossil.mk5.q
            java.util.HashMap r12 = r12.a()
            java.lang.String r2 = r11.c()
            java.lang.Object r12 = r12.get(r2)
            com.fossil.mk5$c r12 = (com.fossil.mk5.c) r12
            com.fossil.ah5$a r2 = com.fossil.ah5.p
            com.fossil.ah5 r2 = r2.a()
            com.fossil.wearables.fsl.appfilter.AppFilterProvider r2 = r2.a()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r4 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_DIANA
            int r4 = r4.getValue()
            java.util.List r2 = r2.getAllAppFilters(r4)
            r4 = 0
            r5 = 0
            if (r12 != 0) goto L_0x0104
            java.lang.String r12 = "appNotificationFilters"
            com.fossil.ee7.a(r2, r12)
            java.util.Iterator r12 = r2.iterator()
        L_0x0092:
            boolean r6 = r12.hasNext()
            if (r6 == 0) goto L_0x00bb
            java.lang.Object r6 = r12.next()
            r7 = r6
            com.fossil.wearables.fsl.appfilter.AppFilter r7 = (com.fossil.wearables.fsl.appfilter.AppFilter) r7
            java.lang.String r8 = "it"
            com.fossil.ee7.a(r7, r8)
            java.lang.String r7 = r7.getType()
            java.lang.String r8 = r11.c()
            boolean r7 = com.fossil.ee7.a(r7, r8)
            java.lang.Boolean r7 = com.fossil.pb7.a(r7)
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0092
            goto L_0x00bc
        L_0x00bb:
            r6 = r5
        L_0x00bc:
            com.fossil.wearables.fsl.appfilter.AppFilter r6 = (com.fossil.wearables.fsl.appfilter.AppFilter) r6
            if (r6 != 0) goto L_0x00d3
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = r10.a
            java.lang.String r1 = "App is not selected by user"
            r12.d(r0, r1)
            r10.a(r11, r5)
            com.fossil.i97 r11 = com.fossil.i97.a
            return r11
        L_0x00d3:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r6 = r10.a
            java.lang.String r7 = "App is selected by user but not in list icon supported"
            r12.d(r6, r7)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r12 = new com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            java.lang.String r7 = r11.c()
            java.lang.String r6 = r6.d(r7)
            java.lang.String r7 = r11.c()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r8 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.NOTIFICATION
            java.lang.String r9 = "general_white.bin"
            r12.<init>(r6, r7, r9, r8)
            com.fossil.mk5$c r6 = new com.fossil.mk5$c
            r7 = 17
            r6.<init>(r7, r12, r4, r3)
            r12 = r6
            goto L_0x0105
        L_0x0104:
            r4 = 1
        L_0x0105:
            boolean r6 = r11.l()
            if (r6 != 0) goto L_0x0111
            boolean r6 = r11.m()
            if (r6 != 0) goto L_0x0146
        L_0x0111:
            boolean r6 = r10.b(r11, r12)
            if (r6 == 0) goto L_0x0146
            boolean r6 = r11.j()
            if (r6 != 0) goto L_0x0146
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = r10.a
            java.lang.String r7 = "processNotificationAdd() supported............"
            r5.d(r6, r7)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r5 = r12.a()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r5 = r5.getNotificationType()
            r10.a(r11, r5)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r11 = r10.a(r11, r12, r4, r0)
            if (r11 != r1) goto L_0x0160
            return r1
        L_0x0146:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r10.a
            java.lang.String r2 = "processNotificationAdd() app not supported............"
            r0.d(r1, r2)
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r12 = r12.a()
            if (r12 == 0) goto L_0x015d
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r5 = r12.getNotificationType()
        L_0x015d:
            r10.a(r11, r5)
        L_0x0160:
            com.fossil.i97 r11 = com.fossil.i97.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.b(com.fossil.mk5$e, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final String a() {
        return PortfolioApp.g0.c().c();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5 A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String d(java.lang.String r11) {
        /*
            r10 = this;
            com.fossil.px6$a r0 = com.fossil.px6.a
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            boolean r0 = r0.e(r1)
            java.lang.String r1 = "readDisplayNameFromPhoneNumber(), number="
            if (r0 == 0) goto L_0x00c0
            r0 = 0
            android.net.Uri r2 = android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI     // Catch:{ Exception -> 0x0094 }
            java.lang.String r3 = android.net.Uri.encode(r11)     // Catch:{ Exception -> 0x0094 }
            android.net.Uri r5 = android.net.Uri.withAppendedPath(r2, r3)     // Catch:{ Exception -> 0x0094 }
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0094 }
            com.portfolio.platform.PortfolioApp r2 = r2.c()     // Catch:{ Exception -> 0x0094 }
            android.content.ContentResolver r4 = r2.getContentResolver()     // Catch:{ Exception -> 0x0094 }
            java.lang.String r2 = "display_name"
            java.lang.String[] r6 = new java.lang.String[]{r2}     // Catch:{ Exception -> 0x0094 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r0 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0094 }
            if (r0 == 0) goto L_0x006d
            int r2 = r0.getCount()     // Catch:{ Exception -> 0x0094 }
            if (r2 <= 0) goto L_0x006d
            r0.moveToFirst()     // Catch:{ Exception -> 0x0094 }
            r2 = 0
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0094 }
            java.lang.String r3 = "c.getString(0)"
            com.fossil.ee7.a(r2, r3)     // Catch:{ Exception -> 0x0094 }
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x006b }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x006b }
            java.lang.String r4 = r10.a     // Catch:{ Exception -> 0x006b }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006b }
            r5.<init>()     // Catch:{ Exception -> 0x006b }
            r5.append(r1)     // Catch:{ Exception -> 0x006b }
            r5.append(r11)     // Catch:{ Exception -> 0x006b }
            java.lang.String r11 = ", displayName="
            r5.append(r11)     // Catch:{ Exception -> 0x006b }
            r5.append(r2)     // Catch:{ Exception -> 0x006b }
            java.lang.String r11 = r5.toString()     // Catch:{ Exception -> 0x006b }
            r3.d(r4, r11)     // Catch:{ Exception -> 0x006b }
            r11 = r2
            goto L_0x008c
        L_0x006b:
            r11 = move-exception
            goto L_0x0097
        L_0x006d:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r10.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            r4.append(r11)
            java.lang.String r1 = ", no display name."
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.d(r3, r1)
        L_0x008c:
            if (r0 == 0) goto L_0x00df
            r0.close()
            goto L_0x00df
        L_0x0092:
            r11 = move-exception
            goto L_0x00ba
        L_0x0094:
            r1 = move-exception
            r2 = r11
            r11 = r1
        L_0x0097:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0092 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x0092 }
            java.lang.String r3 = r10.a     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            r4.<init>()     // Catch:{ all -> 0x0092 }
            java.lang.String r5 = "readDisplayNameFromPhoneNumber(), exception while read display name, ex="
            r4.append(r5)     // Catch:{ all -> 0x0092 }
            r4.append(r11)     // Catch:{ all -> 0x0092 }
            java.lang.String r11 = r4.toString()     // Catch:{ all -> 0x0092 }
            r1.d(r3, r11)     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x00b8
            r0.close()
        L_0x00b8:
            r11 = r2
            goto L_0x00df
        L_0x00ba:
            if (r0 == 0) goto L_0x00bf
            r0.close()
        L_0x00bf:
            throw r11
        L_0x00c0:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = r10.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r3.append(r11)
            java.lang.String r1 = ", no read contact permission."
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r0.d(r2, r1)
        L_0x00df:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.d(java.lang.String):java.lang.String");
    }

    @DexIgnore
    public final void a(long j2) {
        Iterator<Map.Entry<String, Long>> it = this.c.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Long> next = it.next();
            ee7.a((Object) next, "iterator.next()");
            Long value = next.getValue();
            ee7.a((Object) value, "item.value");
            if (j2 - value.longValue() >= 900000) {
                it.remove();
            }
        }
    }

    @DexIgnore
    public final ik7 a(String str, Date date, g gVar) {
        ee7.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        ee7.b(date, SampleRaw.COLUMN_START_TIME);
        ee7.b(gVar, "state");
        return xh7.b(zi7.a(qj7.a()), null, null, new s(this, gVar, str, date, null), 3, null);
    }

    @DexIgnore
    public final ik7 a(String str, String str2, long j2) {
        ee7.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        ee7.b(str2, "content");
        return xh7.b(zi7.a(qj7.a()), null, null, new t(this, str, str2, j2, null), 3, null);
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final ik7 a(StatusBarNotification statusBarNotification) {
        ee7.b(statusBarNotification, "sbn");
        return xh7.b(zi7.a(qj7.a()), null, null, new q(this, statusBarNotification, null), 3, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r7 != 2) goto L_0x0020;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r6, boolean r7) {
        /*
            r5 = this;
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r5.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            com.portfolio.platform.data.model.NotificationSettingsModel r7 = r0.getNotificationSettingsWithIsCallNoLiveData(r7)
            r0 = 0
            r1 = 1
            if (r7 == 0) goto L_0x001f
            int r7 = r7.getSettingsType()
            if (r7 == 0) goto L_0x0020
            if (r7 == r1) goto L_0x001a
            r2 = 2
            if (r7 == r2) goto L_0x0021
            goto L_0x0020
        L_0x001a:
            boolean r0 = r5.a(r6)
            goto L_0x0021
        L_0x001f:
            r7 = 0
        L_0x0020:
            r0 = 1
        L_0x0021:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r5.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isAllowCallOrMessageEvent() - from sender "
            r3.append(r4)
            r3.append(r6)
            java.lang.String r6 = " is "
            r3.append(r6)
            r3.append(r0)
            java.lang.String r6 = " because type = type = "
            r3.append(r6)
            r3.append(r7)
            java.lang.String r6 = r3.toString()
            r1.d(r2, r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.a(java.lang.String, boolean):boolean");
    }

    @DexIgnore
    public final void c(Context context) {
        if (px6.a.d(context)) {
            this.i = new f();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            f fVar = this.i;
            if (fVar != null) {
                contentResolver.registerContentObserver(uri, false, fVar);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            if (contactGroup.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.mk5.e r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.fossil.mk5.l
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.mk5$l r0 = (com.fossil.mk5.l) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.mk5$l r0 = new com.fossil.mk5$l
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 1
            if (r2 == 0) goto L_0x0069
            if (r2 == r5) goto L_0x004e
            if (r2 == r4) goto L_0x004e
            if (r2 != r3) goto L_0x0046
            java.lang.Object r14 = r0.L$4
            com.fossil.mk5$c r14 = (com.fossil.mk5.c) r14
            java.lang.Object r1 = r0.L$3
            com.fossil.oe7 r1 = (com.fossil.oe7) r1
            java.lang.Object r1 = r0.L$2
            com.fossil.se7 r1 = (com.fossil.se7) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.mk5$e r1 = (com.fossil.mk5.e) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.mk5 r0 = (com.fossil.mk5) r0
            com.fossil.t87.a(r15)
            r7 = r14
            r14 = r1
            goto L_0x027a
        L_0x0046:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x004e:
            java.lang.Object r14 = r0.L$4
            com.fossil.mk5$c r14 = (com.fossil.mk5.c) r14
            java.lang.Object r1 = r0.L$3
            com.fossil.oe7 r1 = (com.fossil.oe7) r1
            java.lang.Object r1 = r0.L$2
            com.fossil.se7 r1 = (com.fossil.se7) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.mk5$e r1 = (com.fossil.mk5.e) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.mk5 r0 = (com.fossil.mk5) r0
            com.fossil.t87.a(r15)
            r7 = r14
            r14 = r1
            goto L_0x0287
        L_0x0069:
            com.fossil.t87.a(r15)
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r2 = r13.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "handleNewLogic "
            r6.append(r7)
            r6.append(r14)
            java.lang.String r6 = r6.toString()
            r15.d(r2, r6)
            com.fossil.se7 r15 = new com.fossil.se7
            r15.<init>()
            com.fossil.oe7 r2 = new com.fossil.oe7
            r2.<init>()
            r2.element = r5
            java.lang.String r6 = r14.c()
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r7 = android.provider.Telephony.Sms.getDefaultSmsPackage(r7)
            boolean r6 = com.fossil.ee7.a(r6, r7)
            r7 = 0
            if (r6 == 0) goto L_0x00ee
            java.util.HashMap<java.lang.String, com.fossil.mk5$j> r6 = com.fossil.mk5.p
            java.lang.String r8 = r14.c()
            java.lang.Object r6 = r6.get(r8)
            com.fossil.mk5$j r6 = (com.fossil.mk5.j) r6
            if (r6 == 0) goto L_0x00d4
            java.lang.String r8 = r14.e()
            boolean r6 = r6.a(r8)
            if (r6 != 0) goto L_0x00d4
            com.fossil.mk5$a r6 = com.fossil.mk5.q
            java.util.HashMap r6 = r6.a()
            java.lang.String r8 = r14.c()
            java.lang.Object r6 = r6.get(r8)
            com.fossil.mk5$c r6 = (com.fossil.mk5.c) r6
            r15.element = r6
            goto L_0x0100
        L_0x00d4:
            com.fossil.mk5$a r6 = com.fossil.mk5.q
            java.util.HashMap r6 = r6.a()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r8 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r8 = r8.getMESSAGES()
            java.lang.String r8 = r8.getPackageName()
            java.lang.Object r6 = r6.get(r8)
            com.fossil.mk5$c r6 = (com.fossil.mk5.c) r6
            r15.element = r6
            r6 = 1
            goto L_0x0101
        L_0x00ee:
            com.fossil.mk5$a r6 = com.fossil.mk5.q
            java.util.HashMap r6 = r6.a()
            java.lang.String r8 = r14.c()
            java.lang.Object r6 = r6.get(r8)
            com.fossil.mk5$c r6 = (com.fossil.mk5.c) r6
            r15.element = r6
        L_0x0100:
            r6 = 0
        L_0x0101:
            T r8 = r15.element
            com.fossil.mk5$c r8 = (com.fossil.mk5.c) r8
            if (r8 != 0) goto L_0x01a9
            com.fossil.ah5$a r8 = com.fossil.ah5.p
            com.fossil.ah5 r8 = r8.a()
            com.fossil.wearables.fsl.appfilter.AppFilterProvider r8 = r8.a()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r9 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_DIANA
            int r9 = r9.getValue()
            java.util.List r8 = r8.getAllAppFilters(r9)
            java.lang.String r9 = "appNotificationFilters"
            com.fossil.ee7.a(r8, r9)
            java.util.Iterator r8 = r8.iterator()
        L_0x0124:
            boolean r9 = r8.hasNext()
            r10 = 0
            if (r9 == 0) goto L_0x014e
            java.lang.Object r9 = r8.next()
            r11 = r9
            com.fossil.wearables.fsl.appfilter.AppFilter r11 = (com.fossil.wearables.fsl.appfilter.AppFilter) r11
            java.lang.String r12 = "it"
            com.fossil.ee7.a(r11, r12)
            java.lang.String r11 = r11.getType()
            java.lang.String r12 = r14.c()
            boolean r11 = com.fossil.ee7.a(r11, r12)
            java.lang.Boolean r11 = com.fossil.pb7.a(r11)
            boolean r11 = r11.booleanValue()
            if (r11 == 0) goto L_0x0124
            goto L_0x014f
        L_0x014e:
            r9 = r10
        L_0x014f:
            com.fossil.wearables.fsl.appfilter.AppFilter r9 = (com.fossil.wearables.fsl.appfilter.AppFilter) r9
            if (r9 != 0) goto L_0x0176
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r13.a
            java.lang.String r2 = "App is not selected by user"
            r0.d(r1, r2)
            T r15 = r15.element
            com.fossil.mk5$c r15 = (com.fossil.mk5.c) r15
            if (r15 == 0) goto L_0x0170
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r15 = r15.a()
            if (r15 == 0) goto L_0x0170
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r10 = r15.getNotificationType()
        L_0x0170:
            r13.a(r14, r10)
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x0176:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = r13.a
            java.lang.String r10 = "App is selected by user but no supported icon"
            r8.d(r9, r10)
            r2.element = r7
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r8 = new com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r9 = r9.c()
            java.lang.String r10 = r14.c()
            java.lang.String r9 = r9.d(r10)
            java.lang.String r10 = r14.c()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r11 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.NOTIFICATION
            java.lang.String r12 = "general_white.bin"
            r8.<init>(r9, r10, r12, r11)
            com.fossil.mk5$c r9 = new com.fossil.mk5$c
            r10 = 17
            r9.<init>(r10, r8, r7, r5)
            r15.element = r9
        L_0x01a9:
            T r7 = r15.element
            com.fossil.mk5$c r7 = (com.fossil.mk5.c) r7
            if (r7 == 0) goto L_0x02a3
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r8 = r7.a()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r9 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r9 = r9.getPHONE_INCOMING_CALL()
            boolean r8 = com.fossil.ee7.a(r8, r9)
            if (r8 != 0) goto L_0x0293
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r8 = r7.a()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r9 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r9 = r9.getPHONE_MISSED_CALL()
            boolean r8 = com.fossil.ee7.a(r8, r9)
            if (r8 == 0) goto L_0x01d1
            goto L_0x0293
        L_0x01d1:
            if (r6 == 0) goto L_0x0210
            T r8 = r15.element
            com.fossil.mk5$c r8 = (com.fossil.mk5.c) r8
            boolean r8 = r13.b(r14, r8)
            if (r8 == 0) goto L_0x0210
            java.lang.String r8 = r14.f()
            boolean r8 = r13.c(r8)
            if (r8 == 0) goto L_0x0210
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = r13.a
            java.lang.String r6 = "processNotificationAdd() supported - handle for sms - new solution"
            r3.d(r4, r6)
            T r3 = r15.element
            com.fossil.mk5$c r3 = (com.fossil.mk5.c) r3
            boolean r4 = r2.element
            r0.L$0 = r13
            r0.L$1 = r14
            r0.L$2 = r15
            r0.L$3 = r2
            r0.L$4 = r7
            r0.label = r5
            java.lang.Object r15 = r13.a(r14, r3, r4, r0)
            if (r15 != r1) goto L_0x020d
            return r1
        L_0x020d:
            r0 = r13
            goto L_0x0287
        L_0x0210:
            if (r6 != 0) goto L_0x0254
            boolean r5 = r14.l()
            if (r5 != 0) goto L_0x021e
            boolean r5 = r14.m()
            if (r5 != 0) goto L_0x0254
        L_0x021e:
            T r5 = r15.element
            com.fossil.mk5$c r5 = (com.fossil.mk5.c) r5
            boolean r5 = r13.b(r14, r5)
            if (r5 == 0) goto L_0x0254
            boolean r5 = r14.j()
            if (r5 != 0) goto L_0x0254
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r5 = r13.a
            java.lang.String r6 = "processNotificationAdd() supported............"
            r3.d(r5, r6)
            T r3 = r15.element
            com.fossil.mk5$c r3 = (com.fossil.mk5.c) r3
            boolean r5 = r2.element
            r0.L$0 = r13
            r0.L$1 = r14
            r0.L$2 = r15
            r0.L$3 = r2
            r0.L$4 = r7
            r0.label = r4
            java.lang.Object r15 = r13.a(r14, r3, r5, r0)
            if (r15 != r1) goto L_0x020d
            return r1
        L_0x0254:
            java.lang.String r4 = r14.c()
            java.lang.String r5 = "com.google.android.googlequicksearchbox"
            boolean r4 = com.fossil.ee7.a(r4, r5)
            if (r4 == 0) goto L_0x0279
            T r4 = r15.element
            com.fossil.mk5$c r4 = (com.fossil.mk5.c) r4
            boolean r5 = r2.element
            r0.L$0 = r13
            r0.L$1 = r14
            r0.L$2 = r15
            r0.L$3 = r2
            r0.L$4 = r7
            r0.label = r3
            java.lang.Object r15 = r13.a(r14, r4, r5, r0)
            if (r15 != r1) goto L_0x0279
            return r1
        L_0x0279:
            r0 = r13
        L_0x027a:
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r1 = r0.a
            java.lang.String r2 = "processNotificationAdd() app not supported............"
            r15.d(r1, r2)
        L_0x0287:
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r15 = r7.a()
            com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType r15 = r15.getNotificationType()
            r0.a(r14, r15)
            goto L_0x02a3
        L_0x0293:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = r13.a
            java.lang.String r0 = "processNotificationAdd() incoming call or miss called not supported............"
            r14.d(r15, r0)
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x02a3:
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.a(com.fossil.mk5$e, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final boolean b(String str) {
        List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            ee7.a((Object) contactGroup, "contactGroup");
            List<Contact> contacts = contactGroup.getContacts();
            ee7.a((Object) contacts, "contactGroup.contacts");
            if (!contacts.isEmpty()) {
                Contact contact = contactGroup.getContacts().get(0);
                ee7.a((Object) contact, "contactGroup.contacts[0]");
                if (mh7.b(contact.getDisplayName(), str, true)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void c(int i2) {
        FLogger.INSTANCE.getLocal().d(this.a, "removeNotification");
        Queue<NotificationBaseObj> queue = this.j;
        ee7.a((Object) queue, "mDeviceNotifications");
        synchronized (queue) {
            Iterator<NotificationBaseObj> it = this.j.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getUid() == i2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.a;
                        local.d(str, "notification removed, id = " + i2);
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object b(e eVar, c cVar, boolean z, fb7<? super i97> fb7) {
        String str;
        String str2;
        String str3;
        Boolean bool;
        FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - isSupportedIcon " + z + " appName " + cVar.a().getAppName() + " sender = " + eVar.f() + " - text = " + eVar.g());
        if (TextUtils.isEmpty(eVar.g())) {
            FLogger.INSTANCE.getLocal().d(this.a, "Skip this notification from " + eVar.c() + " since content is empty");
            return i97.a;
        }
        if (z) {
            str = eVar.f();
        } else {
            str = cVar.a().getAppName();
        }
        if (z) {
            str2 = eVar.g();
        } else {
            str2 = eVar.f() + ':' + eVar.g();
        }
        ArrayList arrayList = new ArrayList();
        if (!be5.o.m() || !ee7.a((Object) eVar.c(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName())) {
            arrayList.add(NotificationBaseObj.ANotificationFlag.IMPORTANT);
        } else {
            arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACCEPT_CALL);
            arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REJECT_CALL);
        }
        DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
        long j2 = -1;
        String packageName = cVar.a().getPackageName();
        if (ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) || ee7.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName())) {
            if (eVar instanceof h) {
                str3 = ((h) eVar).o();
            } else {
                str3 = eVar instanceof i ? ((i) eVar).n() : "";
            }
            FLogger.INSTANCE.getLocal().d(this.a, "Receive phone notification, number " + str3 + " isQuickResponseEnable " + this.k.T());
            Boolean T = this.k.T();
            ee7.a((Object) T, "mSharedPreferencesManager.isQuickResponseEnabled");
            if (T.booleanValue()) {
                if (str3 != null) {
                    bool = pb7.a(str3.length() > 0);
                } else {
                    bool = null;
                }
                if (bool == null) {
                    ee7.a();
                    throw null;
                } else if (bool.booleanValue()) {
                    arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REPLY_MESSAGE);
                    j2 = this.m.upsertQuickResponseSender(new QuickResponseSender(str3));
                    FLogger.INSTANCE.getLocal().d(this.a, "Update sender with number " + str3 + " id " + j2);
                }
            }
        }
        DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(eVar.b(), cVar.a().getNotificationType(), cVar.a(), eVar.h(), str, (int) j2, str2, arrayList, pb7.a(eVar.i()), null, null, 1536, null);
        b a2 = a(eVar, cVar);
        if (a2 == b.SILENT) {
            FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - silent notification");
            dianaNotificationObj.toSilentNotification();
        } else if (a2 == b.SKIP) {
            FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - skip notification");
            return i97.a;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "sendDianaNotification " + dianaNotificationObj);
        this.j.offer(dianaNotificationObj);
        PortfolioApp.g0.c().b(a(), dianaNotificationObj);
        return i97.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.mk5.e r5, com.fossil.mk5.c r6, boolean r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r4 = this;
            boolean r0 = r8 instanceof com.fossil.mk5.k
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.mk5$k r0 = (com.fossil.mk5.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.mk5$k r0 = new com.fossil.mk5$k
            r0.<init>(r4, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r5 = r0.L$3
            com.fossil.mk5$e r5 = (com.fossil.mk5.e) r5
            boolean r5 = r0.Z$0
            java.lang.Object r5 = r0.L$2
            com.fossil.mk5$c r5 = (com.fossil.mk5.c) r5
            java.lang.Object r5 = r0.L$1
            com.fossil.mk5$e r5 = (com.fossil.mk5.e) r5
            java.lang.Object r5 = r0.L$0
            com.fossil.mk5 r5 = (com.fossil.mk5) r5
            com.fossil.t87.a(r8)
            goto L_0x008c
        L_0x003b:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0043:
            com.fossil.t87.a(r8)
            com.fossil.mk5$d r8 = r4.b
            com.fossil.mk5$e r8 = r8.a(r5)
            if (r8 == 0) goto L_0x007f
            boolean r2 = r4.h()
            if (r2 != 0) goto L_0x007c
            java.lang.String r2 = r8.f()
            boolean r2 = com.fossil.mh7.a(r2)
            if (r2 == 0) goto L_0x0069
            java.lang.String r2 = r8.g()
            boolean r2 = com.fossil.mh7.a(r2)
            if (r2 == 0) goto L_0x0069
            goto L_0x007c
        L_0x0069:
            r0.L$0 = r4
            r0.L$1 = r5
            r0.L$2 = r6
            r0.Z$0 = r7
            r0.L$3 = r8
            r0.label = r3
            java.lang.Object r5 = r4.b(r8, r6, r7, r0)
            if (r5 != r1) goto L_0x008c
            return r1
        L_0x007c:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        L_0x007f:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = r4.a
            java.lang.String r7 = "processNotificationAdd() - this notification existed."
            r5.d(r6, r7)
        L_0x008c:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.a(com.fossil.mk5$e, com.fossil.mk5$c, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final b a(e eVar, c cVar) {
        FLogger.INSTANCE.getLocal().d(this.a, "applyDND()");
        if (Build.VERSION.SDK_INT >= 23) {
            Object systemService = PortfolioApp.g0.c().getSystemService("notification");
            if (systemService != null) {
                NotificationManager notificationManager = (NotificationManager) systemService;
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "currentInterruptionFilter = " + currentInterruptionFilter);
                if (currentInterruptionFilter != 2) {
                    if (currentInterruptionFilter == 3) {
                        return b.SILENT;
                    }
                    if (currentInterruptionFilter == 4) {
                        return (g() || !ee7.a(eVar.a(), Alarm.TABLE_NAME)) ? b.SILENT : b.ACTIVE;
                    }
                    if (ee7.a((Object) cVar.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || ee7.a((Object) cVar.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                        return j() ? b.SILENT : b.ACTIVE;
                    }
                    if (ee7.a((Object) eVar.a(), (Object) Alarm.TABLE_NAME)) {
                        return g() ? b.SILENT : b.ACTIVE;
                    }
                } else if (ee7.a((Object) cVar.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || ee7.a((Object) cVar.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                    if (!(eVar instanceof h)) {
                        return a(eVar) ? b.SILENT : b.ACTIVE;
                    }
                    if ((notificationManager.getNotificationPolicy().priorityCategories & 16) != 0 && ((h) eVar).n()) {
                        return !j() ? b.ACTIVE : b.SILENT;
                    }
                    if (!a(eVar)) {
                        return !j() ? b.ACTIVE : b.SILENT;
                    }
                    return b.SILENT;
                } else if (ee7.a((Object) cVar.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName()) || eVar.k()) {
                    if (!b(eVar)) {
                        return !i() ? b.ACTIVE : b.SILENT;
                    }
                    return b.SILENT;
                } else if (ee7.a((Object) eVar.a(), (Object) Alarm.TABLE_NAME)) {
                    return (f() || g()) ? b.SILENT : b.ACTIVE;
                }
            } else {
                throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
            }
        }
        return i() ? b.SILENT : b.ACTIVE;
    }

    @DexIgnore
    public final boolean b(e eVar) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND()");
        Object systemService = PortfolioApp.g0.c().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 4) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Don't allow any messages");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityMessageSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                return !a(eVar, a(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                return !a(eVar, b(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Allow messages");
                return false;
            }
        } else {
            throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationRemoved, id = " + i2);
        c(i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006c, code lost:
        if (0 == 0) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006f, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0051, code lost:
        if (r3 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0053, code lost:
        r3.close();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.r87<java.lang.String, java.lang.String>> b(android.content.Context r11) {
        /*
            r10 = this;
            java.lang.String r0 = "data1"
            java.lang.String r1 = "display_name"
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r10.a
            java.lang.String r4 = "getStaredContacts()"
            r2.d(r3, r4)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r3 = 0
            android.content.ContentResolver r4 = r11.getContentResolver()     // Catch:{ Exception -> 0x0059 }
            android.net.Uri r5 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0059 }
            java.lang.String r11 = "contact_id"
            java.lang.String[] r6 = new java.lang.String[]{r11, r1, r0}     // Catch:{ Exception -> 0x0059 }
            java.lang.String r7 = "starred = ?"
            java.lang.String r11 = "1"
            java.lang.String[] r8 = new java.lang.String[]{r11}     // Catch:{ Exception -> 0x0059 }
            r9 = 0
            android.database.Cursor r3 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0059 }
            if (r3 == 0) goto L_0x0051
        L_0x0032:
            boolean r11 = r3.moveToNext()     // Catch:{ Exception -> 0x0059 }
            if (r11 == 0) goto L_0x0051
            int r11 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r11 = r3.getString(r11)     // Catch:{ Exception -> 0x0059 }
            int r4 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0059 }
            com.fossil.r87 r5 = new com.fossil.r87     // Catch:{ Exception -> 0x0059 }
            r5.<init>(r11, r4)     // Catch:{ Exception -> 0x0059 }
            r2.add(r5)     // Catch:{ Exception -> 0x0059 }
            goto L_0x0032
        L_0x0051:
            if (r3 == 0) goto L_0x006f
        L_0x0053:
            r3.close()
            goto L_0x006f
        L_0x0057:
            r11 = move-exception
            goto L_0x0070
        L_0x0059:
            r11 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0057 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = r10.a     // Catch:{ all -> 0x0057 }
            java.lang.String r4 = r11.getMessage()     // Catch:{ all -> 0x0057 }
            r0.e(r1, r4)     // Catch:{ all -> 0x0057 }
            r11.printStackTrace()     // Catch:{ all -> 0x0057 }
            if (r3 == 0) goto L_0x006f
            goto L_0x0053
        L_0x006f:
            return r2
        L_0x0070:
            if (r3 == 0) goto L_0x0075
            r3.close()
        L_0x0075:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.b(android.content.Context):java.util.List");
    }

    @DexIgnore
    public final boolean a(e eVar) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND()");
        Object systemService = PortfolioApp.g0.c().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 8) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Don't allow any calls");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityCallSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                return !a(eVar, a(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.g0.c().getApplicationContext();
                ee7.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                return !a(eVar, b(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Allow calls");
                return false;
            }
        } else {
            throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.fossil.mk5.e r4, java.util.List<com.fossil.r87<java.lang.String, java.lang.String>> r5) {
        /*
            r3 = this;
            java.lang.String r0 = r4.f()
            java.lang.String r4 = r4.h()
            java.util.Iterator r5 = r5.iterator()
        L_0x000c:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L_0x003f
            java.lang.Object r1 = r5.next()
            com.fossil.r87 r1 = (com.fossil.r87) r1
            java.lang.Object r2 = r1.getFirst()
            java.lang.String r2 = (java.lang.String) r2
            boolean r2 = com.fossil.ee7.a(r2, r0)
            if (r2 != 0) goto L_0x0030
            java.lang.Object r1 = r1.getSecond()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = com.fossil.ee7.a(r1, r4)
            if (r1 == 0) goto L_0x000c
        L_0x0030:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r3.a
            java.lang.String r0 = "isSenderInContactList() - Found contact"
            r4.d(r5, r0)
            r4 = 1
            return r4
        L_0x003f:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r3.a
            java.lang.String r0 = "isSenderInContactList() - Not found contact"
            r4.d(r5, r0)
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.a(com.fossil.mk5$e, java.util.List):boolean");
    }

    @DexIgnore
    public final void a(e eVar, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + eVar.c());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + eVar.e());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + eVar.f());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + eVar.h());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + eVar.g());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + eVar.m());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + eVar.l());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + eVar.i());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + eVar.d());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + eVar.j());
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        T t2;
        boolean z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationSentResult, id = " + i2 + ", isSuccess = " + z);
        if (z) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.d(str2, "onNotificationSentResult, to silent notification, id = " + i2);
            Queue<NotificationBaseObj> queue = this.j;
            ee7.a((Object) queue, "mDeviceNotifications");
            Iterator<T> it = queue.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (t2.getUid() == i2) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            T t3 = t2;
            if (t3 != null) {
                t3.toExistedNotification();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0066, code lost:
        if (0 == 0) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0069, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x004b, code lost:
        if (r3 != null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x004d, code lost:
        r3.close();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.r87<java.lang.String, java.lang.String>> a(android.content.Context r11) {
        /*
            r10 = this;
            java.lang.String r0 = "data1"
            java.lang.String r1 = "display_name"
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r10.a
            java.lang.String r4 = "getAllContacts()"
            r2.d(r3, r4)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r3 = 0
            android.content.ContentResolver r4 = r11.getContentResolver()     // Catch:{ Exception -> 0x0053 }
            android.net.Uri r5 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0053 }
            java.lang.String r11 = "contact_id"
            java.lang.String[] r6 = new java.lang.String[]{r11, r1, r0}     // Catch:{ Exception -> 0x0053 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r3 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0053 }
            if (r3 == 0) goto L_0x004b
        L_0x002c:
            boolean r11 = r3.moveToNext()     // Catch:{ Exception -> 0x0053 }
            if (r11 == 0) goto L_0x004b
            int r11 = r3.getColumnIndex(r1)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r11 = r3.getString(r11)     // Catch:{ Exception -> 0x0053 }
            int r4 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0053 }
            com.fossil.r87 r5 = new com.fossil.r87     // Catch:{ Exception -> 0x0053 }
            r5.<init>(r11, r4)     // Catch:{ Exception -> 0x0053 }
            r2.add(r5)     // Catch:{ Exception -> 0x0053 }
            goto L_0x002c
        L_0x004b:
            if (r3 == 0) goto L_0x0069
        L_0x004d:
            r3.close()
            goto L_0x0069
        L_0x0051:
            r11 = move-exception
            goto L_0x006a
        L_0x0053:
            r11 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0051 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0051 }
            java.lang.String r1 = r10.a     // Catch:{ all -> 0x0051 }
            java.lang.String r4 = r11.getMessage()     // Catch:{ all -> 0x0051 }
            r0.e(r1, r4)     // Catch:{ all -> 0x0051 }
            r11.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r3 == 0) goto L_0x0069
            goto L_0x004d
        L_0x0069:
            return r2
        L_0x006a:
            if (r3 == 0) goto L_0x006f
            r3.close()
        L_0x006f:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mk5.a(android.content.Context):java.util.List");
    }
}
