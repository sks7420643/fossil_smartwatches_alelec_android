package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr7 extends br7 {
    @DexIgnore
    public /* final */ transient byte[][] c;
    @DexIgnore
    public /* final */ transient int[] d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pr7(byte[][] bArr, int[] iArr) {
        super(br7.EMPTY.getData$okio());
        ee7.b(bArr, "segments");
        ee7.b(iArr, "directory");
        this.c = bArr;
        this.d = iArr;
    }

    @DexIgnore
    private final Object writeReplace() {
        br7 a = a();
        if (a != null) {
            return a;
        }
        throw new x87("null cannot be cast to non-null type java.lang.Object");
    }

    @DexIgnore
    public final br7 a() {
        return new br7(toByteArray());
    }

    @DexIgnore
    @Override // com.fossil.br7
    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
        ee7.a((Object) asReadOnlyBuffer, "ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    @DexIgnore
    @Override // com.fossil.br7
    public String base64() {
        return a().base64();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public String base64Url() {
        return a().base64Url();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public br7 digest$okio(String str) {
        ee7.b(str, "algorithm");
        MessageDigest instance = MessageDigest.getInstance(str);
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory$okio()[length + i];
            int i4 = getDirectory$okio()[i];
            instance.update(getSegments$okio()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
        byte[] digest = instance.digest();
        ee7.a((Object) digest, "digest.digest()");
        return new br7(digest);
    }

    @DexIgnore
    @Override // com.fossil.br7
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof br7) {
            br7 br7 = (br7) obj;
            return br7.size() == size() && rangeEquals(0, br7, 0, size());
        }
    }

    @DexIgnore
    public final int[] getDirectory$okio() {
        return this.d;
    }

    @DexIgnore
    public final byte[][] getSegments$okio() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.br7
    public int getSize$okio() {
        return getDirectory$okio()[getSegments$okio().length - 1];
    }

    @DexIgnore
    @Override // com.fossil.br7
    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio != 0) {
            return hashCode$okio;
        }
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 1;
        int i3 = 0;
        while (i < length) {
            int i4 = getDirectory$okio()[length + i];
            int i5 = getDirectory$okio()[i];
            byte[] bArr = getSegments$okio()[i];
            int i6 = (i5 - i3) + i4;
            while (i4 < i6) {
                i2 = (i2 * 31) + bArr[i4];
                i4++;
            }
            i++;
            i3 = i5;
        }
        setHashCode$okio(i2);
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.br7
    public String hex() {
        return a().hex();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public br7 hmac$okio(String str, br7 br7) {
        ee7.b(str, "algorithm");
        ee7.b(br7, "key");
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(br7.toByteArray(), str));
            int length = getSegments$okio().length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int i3 = getDirectory$okio()[length + i];
                int i4 = getDirectory$okio()[i];
                instance.update(getSegments$okio()[i], i3, i4 - i2);
                i++;
                i2 = i4;
            }
            byte[] doFinal = instance.doFinal();
            ee7.a((Object) doFinal, "mac.doFinal()");
            return new br7(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.br7
    public int indexOf(byte[] bArr, int i) {
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return a().indexOf(bArr, i);
    }

    @DexIgnore
    @Override // com.fossil.br7
    public byte[] internalArray$okio() {
        return toByteArray();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public byte internalGet$okio(int i) {
        int i2;
        vq7.a((long) getDirectory$okio()[getSegments$okio().length - 1], (long) i, 1);
        int a = wr7.a(this, i);
        if (a == 0) {
            i2 = 0;
        } else {
            i2 = getDirectory$okio()[a - 1];
        }
        return getSegments$okio()[a][(i - i2) + getDirectory$okio()[getSegments$okio().length + a]];
    }

    @DexIgnore
    @Override // com.fossil.br7
    public int lastIndexOf(byte[] bArr, int i) {
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return a().lastIndexOf(bArr, i);
    }

    @DexIgnore
    @Override // com.fossil.br7
    public boolean rangeEquals(int i, br7 br7, int i2, int i3) {
        int i4;
        ee7.b(br7, FacebookRequestErrorClassification.KEY_OTHER);
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int i5 = i3 + i;
        int a = wr7.a(this, i);
        while (i < i5) {
            if (a == 0) {
                i4 = 0;
            } else {
                i4 = getDirectory$okio()[a - 1];
            }
            int i6 = getDirectory$okio()[getSegments$okio().length + a];
            int min = Math.min(i5, (getDirectory$okio()[a] - i4) + i4) - i;
            if (!br7.rangeEquals(i2, getSegments$okio()[a], i6 + (i - i4), min)) {
                return false;
            }
            i2 += min;
            i += min;
            a++;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.br7
    public String string(Charset charset) {
        ee7.b(charset, "charset");
        return a().string(charset);
    }

    @DexIgnore
    @Override // com.fossil.br7
    public br7 substring(int i, int i2) {
        int i3 = 0;
        if (i >= 0) {
            if (i2 <= size()) {
                int i4 = i2 - i;
                if (!(i4 >= 0)) {
                    throw new IllegalArgumentException(("endIndex=" + i2 + " < beginIndex=" + i).toString());
                } else if (i == 0 && i2 == size()) {
                    return this;
                } else {
                    if (i == i2) {
                        return br7.EMPTY;
                    }
                    int a = wr7.a(this, i);
                    int a2 = wr7.a(this, i2 - 1);
                    byte[][] bArr = (byte[][]) s97.a(getSegments$okio(), a, a2 + 1);
                    int[] iArr = new int[(bArr.length * 2)];
                    if (a <= a2) {
                        int i5 = a;
                        int i6 = 0;
                        while (true) {
                            iArr[i6] = Math.min(getDirectory$okio()[i5] - i, i4);
                            int i7 = i6 + 1;
                            iArr[i6 + bArr.length] = getDirectory$okio()[getSegments$okio().length + i5];
                            if (i5 == a2) {
                                break;
                            }
                            i5++;
                            i6 = i7;
                        }
                    }
                    if (a != 0) {
                        i3 = getDirectory$okio()[a - 1];
                    }
                    int length = bArr.length;
                    iArr[length] = iArr[length] + (i - i3);
                    return new pr7(bArr, iArr);
                }
            } else {
                throw new IllegalArgumentException(("endIndex=" + i2 + " > length(" + size() + ')').toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex=" + i + " < 0").toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.br7
    public br7 toAsciiLowercase() {
        return a().toAsciiLowercase();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public br7 toAsciiUppercase() {
        return a().toAsciiUppercase();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public byte[] toByteArray() {
        byte[] bArr = new byte[size()];
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = getDirectory$okio()[length + i];
            int i5 = getDirectory$okio()[i];
            int i6 = i5 - i2;
            s97.a(getSegments$okio()[i], bArr, i3, i4, i4 + i6);
            i3 += i6;
            i++;
            i2 = i5;
        }
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.br7
    public String toString() {
        return a().toString();
    }

    @DexIgnore
    @Override // com.fossil.br7
    public void write(OutputStream outputStream) throws IOException {
        ee7.b(outputStream, "out");
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory$okio()[length + i];
            int i4 = getDirectory$okio()[i];
            outputStream.write(getSegments$okio()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
    }

    @DexIgnore
    @Override // com.fossil.br7
    public void write$okio(yq7 yq7, int i, int i2) {
        int i3;
        ee7.b(yq7, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        int i4 = i2 + i;
        int a = wr7.a(this, i);
        while (i < i4) {
            if (a == 0) {
                i3 = 0;
            } else {
                i3 = getDirectory$okio()[a - 1];
            }
            int i5 = getDirectory$okio()[getSegments$okio().length + a];
            int min = Math.min(i4, (getDirectory$okio()[a] - i3) + i3) - i;
            int i6 = i5 + (i - i3);
            nr7 nr7 = new nr7(getSegments$okio()[a], i6, i6 + min, true, false);
            nr7 nr72 = yq7.a;
            if (nr72 == null) {
                nr7.g = nr7;
                nr7.f = nr7;
                yq7.a = nr7;
            } else if (nr72 != null) {
                nr7 nr73 = nr72.g;
                if (nr73 != null) {
                    nr73.a(nr7);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
            i += min;
            a++;
        }
        yq7.j(yq7.x() + ((long) size()));
    }

    @DexIgnore
    @Override // com.fossil.br7
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        int i4;
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int i5 = i3 + i;
        int a = wr7.a(this, i);
        while (i < i5) {
            if (a == 0) {
                i4 = 0;
            } else {
                i4 = getDirectory$okio()[a - 1];
            }
            int i6 = getDirectory$okio()[getSegments$okio().length + a];
            int min = Math.min(i5, (getDirectory$okio()[a] - i4) + i4) - i;
            if (!vq7.a(getSegments$okio()[a], i6 + (i - i4), bArr, i2, min)) {
                return false;
            }
            i2 += min;
            i += min;
            a++;
        }
        return true;
    }
}
