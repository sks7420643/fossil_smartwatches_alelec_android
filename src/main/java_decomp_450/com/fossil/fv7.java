package com.fossil;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv7<T> {
    @DexIgnore
    public /* final */ Response a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ mo7 c;

    @DexIgnore
    public fv7(Response response, T t, mo7 mo7) {
        this.a = response;
        this.b = t;
        this.c = mo7;
    }

    @DexIgnore
    public static <T> fv7<T> a(T t, Response response) {
        jv7.a(response, "rawResponse == null");
        if (response.l()) {
            return new fv7<>(response, t, null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    @DexIgnore
    public int b() {
        return this.a.e();
    }

    @DexIgnore
    public mo7 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.l();
    }

    @DexIgnore
    public String e() {
        return this.a.n();
    }

    @DexIgnore
    public Response f() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static <T> fv7<T> a(mo7 mo7, Response response) {
        jv7.a(mo7, "body == null");
        jv7.a(response, "rawResponse == null");
        if (!response.l()) {
            return new fv7<>(response, null, mo7);
        }
        throw new IllegalArgumentException("rawResponse should not be successful response");
    }

    @DexIgnore
    public T a() {
        return this.b;
    }
}
