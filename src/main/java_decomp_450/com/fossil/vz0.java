package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz0 {
    @DexIgnore
    public /* synthetic */ vz0(zd7 zd7) {
    }

    @DexIgnore
    public final byte[] a(j70[] j70Arr) {
        ByteBuffer allocate = ByteBuffer.allocate(j70Arr.length * 5);
        for (j70 j70 : j70Arr) {
            allocate.put(j70.b());
        }
        byte[] array = allocate.array();
        ee7.a((Object) array, "array.array()");
        return array;
    }
}
