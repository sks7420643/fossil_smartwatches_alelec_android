package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<b93> CREATOR; // = new x93();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ y83 b;
    @DexIgnore
    public /* final */ Float c;

    @DexIgnore
    public b93(int i, y83 y83, Float f) {
        a72.a(i != 3 || (y83 != null && (f != null && (f.floatValue() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f.floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1)) > 0)), String.format("Invalid Cap: type=%s bitmapDescriptor=%s bitmapRefWidth=%s", Integer.valueOf(i), y83, f));
        this.a = i;
        this.b = y83;
        this.c = f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b93)) {
            return false;
        }
        b93 b93 = (b93) obj;
        return this.a == b93.a && y62.a(this.b, b93.b) && y62.a(this.c, b93.c);
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(Integer.valueOf(this.a), this.b, this.c);
    }

    @DexIgnore
    public String toString() {
        int i = this.a;
        StringBuilder sb = new StringBuilder(23);
        sb.append("[Cap: type=");
        sb.append(i);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder;
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        y83 y83 = this.b;
        if (y83 == null) {
            iBinder = null;
        } else {
            iBinder = y83.a().asBinder();
        }
        k72.a(parcel, 3, iBinder, false);
        k72.a(parcel, 4, this.c, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b93(int r2, android.os.IBinder r3, java.lang.Float r4) {
        /*
            r1 = this;
            if (r3 != 0) goto L_0x0004
            r3 = 0
            goto L_0x000e
        L_0x0004:
            com.fossil.ab2 r3 = com.fossil.ab2.a.a(r3)
            com.fossil.y83 r0 = new com.fossil.y83
            r0.<init>(r3)
            r3 = r0
        L_0x000e:
            r1.<init>(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.b93.<init>(int, android.os.IBinder, java.lang.Float):void");
    }

    @DexIgnore
    public b93(y83 y83, float f) {
        this(3, y83, Float.valueOf(f));
    }

    @DexIgnore
    public b93(int i) {
        this(i, (y83) null, (Float) null);
    }
}
