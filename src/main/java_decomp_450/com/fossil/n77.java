package com.fossil;

import android.app.Activity;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n77 extends Activity implements u77 {
    @DexIgnore
    public t77<Object> a;

    @DexIgnore
    @Override // com.fossil.u77
    public m77<Object> a() {
        return this.a;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        l77.a(this);
        super.onCreate(bundle);
    }
}
