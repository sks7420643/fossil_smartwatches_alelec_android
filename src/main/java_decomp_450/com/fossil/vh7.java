package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vh7 {
    @DexIgnore
    public static final <T> hj7<T> a(yi7 yi7, ib7 ib7, bj7 bj7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) {
        return xh7.a(yi7, ib7, bj7, kd7);
    }

    @DexIgnore
    public static final ik7 b(yi7 yi7, ib7 ib7, bj7 bj7, kd7<? super yi7, ? super fb7<? super i97>, ? extends Object> kd7) {
        return xh7.b(yi7, ib7, bj7, kd7);
    }

    @DexIgnore
    public static final <T> T a(ib7 ib7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) throws InterruptedException {
        return (T) wh7.a(ib7, kd7);
    }

    @DexIgnore
    public static final <T> Object a(ib7 ib7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7, fb7<? super T> fb7) {
        return xh7.a(ib7, kd7, fb7);
    }
}
