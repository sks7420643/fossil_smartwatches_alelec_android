package com.fossil;

import com.fossil.i00;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface g00 {
    @DexIgnore
    public static final g00 a = new i00.a().a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g00 {
        @DexIgnore
        @Override // com.fossil.g00
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    Map<String, String> a();
}
