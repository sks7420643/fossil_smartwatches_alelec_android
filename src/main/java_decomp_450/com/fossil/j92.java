package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j92 {
    @DexIgnore
    public static byte[] a(Context context, String str) throws PackageManager.NameNotFoundException {
        MessageDigest a;
        PackageInfo b = ja2.b(context).b(str, 64);
        Signature[] signatureArr = b.signatures;
        if (signatureArr == null || signatureArr.length != 1 || (a = a("SHA1")) == null) {
            return null;
        }
        return a.digest(b.signatures[0].toByteArray());
    }

    @DexIgnore
    public static MessageDigest a(String str) {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance(str);
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }
}
