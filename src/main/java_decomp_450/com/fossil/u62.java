package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface u62 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends dg2 implements u62 {
        @DexIgnore
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @DexIgnore
        @Override // com.fossil.dg2
        public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) fg2.a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                a(parcel.readInt(), (Bundle) fg2.a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                a(parcel.readInt(), parcel.readStrongBinder(), (n82) fg2.a(parcel, n82.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    @DexIgnore
    void a(int i, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(int i, IBinder iBinder, n82 n82) throws RemoteException;
}
