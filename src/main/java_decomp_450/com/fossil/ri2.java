package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri2 extends gi2 implements si2 {
    @DexIgnore
    public ri2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
    }

    @DexIgnore
    @Override // com.fossil.si2
    public final void a(od2 od2) throws RemoteException {
        Parcel zza = zza();
        ej2.a(zza, od2);
        a(2, zza);
    }
}
