package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v13 implements s13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.client.sessions.check_on_reset_and_enable2", true);
        dr2.a("measurement.client.sessions.check_on_startup", true);
        dr2.a("measurement.client.sessions.start_session_before_view_screen", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.s13
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.s13
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
