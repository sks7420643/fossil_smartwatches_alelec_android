package com.fossil;

import android.text.TextUtils;
import com.fossil.be5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs6 extends fl4<es6, fs6, ds6> {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ CategoryRepository h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;
    @DexIgnore
    public /* final */ RingStyleRepository j;
    @DexIgnore
    public /* final */ nw5 k;
    @DexIgnore
    public /* final */ vu5 l;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase m;
    @DexIgnore
    public /* final */ ch5 n;
    @DexIgnore
    public /* final */ WatchLocalizationRepository o;
    @DexIgnore
    public /* final */ AlarmsRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1", f = "GetDianaDeviceSettingUseCase.kt", l = {65, 66, 67, 68, 69, 70, 71, 72, 73, 80, 96, 114, 124}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gs6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gs6 gs6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gs6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:109:0x036c  */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x03de  */
        /* JADX WARNING: Removed duplicated region for block: B:122:0x0412  */
        /* JADX WARNING: Removed duplicated region for block: B:126:0x041a  */
        /* JADX WARNING: Removed duplicated region for block: B:128:0x041e  */
        /* JADX WARNING: Removed duplicated region for block: B:130:0x0422  */
        /* JADX WARNING: Removed duplicated region for block: B:132:0x0426  */
        /* JADX WARNING: Removed duplicated region for block: B:134:0x042a  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x042e  */
        /* JADX WARNING: Removed duplicated region for block: B:138:0x0432  */
        /* JADX WARNING: Removed duplicated region for block: B:140:0x0436  */
        /* JADX WARNING: Removed duplicated region for block: B:142:0x043a  */
        /* JADX WARNING: Removed duplicated region for block: B:145:0x0219 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00e6 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00f5  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x010f  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0129  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0143  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x0177  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0196 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01a5  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0203  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x0246  */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0284  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x02b1  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r6 = r16
                java.lang.Object r7 = com.fossil.nb7.a()
                int r0 = r6.label
                r1 = 1
                r2 = 0
                r8 = 0
                switch(r0) {
                    case 0: goto L_0x00bf;
                    case 1: goto L_0x00b7;
                    case 2: goto L_0x00af;
                    case 3: goto L_0x00a7;
                    case 4: goto L_0x009e;
                    case 5: goto L_0x0095;
                    case 6: goto L_0x008c;
                    case 7: goto L_0x0083;
                    case 8: goto L_0x007a;
                    case 9: goto L_0x0070;
                    case 10: goto L_0x005f;
                    case 11: goto L_0x004e;
                    case 12: goto L_0x0035;
                    case 13: goto L_0x0016;
                    default: goto L_0x000e;
                }
            L_0x000e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0016:
                java.lang.Object r0 = r6.L$5
                com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r0 = (com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings) r0
                java.lang.Object r0 = r6.L$4
                com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings r0 = (com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings) r0
                java.lang.Object r0 = r6.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r6.L$2
                com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r0
                java.lang.Object r0 = r6.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                r0 = r17
                goto L_0x03c5
            L_0x0035:
                java.lang.Object r0 = r6.L$4
                com.fossil.oe7 r0 = (com.fossil.oe7) r0
                java.lang.Object r0 = r6.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r6.L$2
                com.portfolio.platform.data.model.diana.preset.DianaPreset r1 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r1
                java.lang.Object r2 = r6.L$1
                java.util.ArrayList r2 = (java.util.ArrayList) r2
                java.lang.Object r3 = r6.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r17)
                goto L_0x034d
            L_0x004e:
                java.lang.Object r0 = r6.L$2
                com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r0
                java.lang.Object r3 = r6.L$1
                java.util.ArrayList r3 = (java.util.ArrayList) r3
                java.lang.Object r4 = r6.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r17)
                goto L_0x0285
            L_0x005f:
                java.lang.Object r0 = r6.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r6.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r3 = r6.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r17)
                goto L_0x01f2
            L_0x0070:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
            L_0x0077:
                r3 = r0
                goto L_0x0197
            L_0x007a:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x0184
            L_0x0083:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x0169
            L_0x008c:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x014f
            L_0x0095:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x0135
            L_0x009e:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x011b
            L_0x00a7:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x0101
            L_0x00af:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x00e7
            L_0x00b7:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)
                goto L_0x00d5
            L_0x00bf:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r0 = r6.p$
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.WatchLocalizationRepository r3 = r3.o
                r6.L$0 = r0
                r6.label = r1
                java.lang.Object r3 = r3.getWatchLocalizationFromServer(r2, r6)
                if (r3 != r7) goto L_0x00d5
                return r7
            L_0x00d5:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.CategoryRepository r3 = r3.h
                r6.L$0 = r0
                r4 = 2
                r6.label = r4
                java.lang.Object r3 = r3.downloadCategories(r6)
                if (r3 != r7) goto L_0x00e7
                return r7
            L_0x00e7:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.ComplicationRepository r3 = r3.f
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x043a
                r6.L$0 = r0
                r5 = 3
                r6.label = r5
                java.lang.Object r3 = r3.downloadAllComplication(r4, r6)
                if (r3 != r7) goto L_0x0101
                return r7
            L_0x0101:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.WatchAppRepository r3 = r3.e
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x0436
                r6.L$0 = r0
                r5 = 4
                r6.label = r5
                java.lang.Object r3 = r3.downloadWatchApp(r4, r6)
                if (r3 != r7) goto L_0x011b
                return r7
            L_0x011b:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r3 = r3.g
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x0432
                r6.L$0 = r0
                r5 = 5
                r6.label = r5
                java.lang.Object r3 = r3.downloadPresetList(r4, r6)
                if (r3 != r7) goto L_0x0135
                return r7
            L_0x0135:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r3 = r3.g
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x042e
                r6.L$0 = r0
                r5 = 6
                r6.label = r5
                java.lang.Object r3 = r3.downloadRecommendPresetList(r4, r6)
                if (r3 != r7) goto L_0x014f
                return r7
            L_0x014f:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.WatchFaceRepository r3 = r3.i
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x042a
                r6.L$0 = r0
                r5 = 7
                r6.label = r5
                java.lang.Object r3 = r3.getWatchFacesFromServer(r4, r6)
                if (r3 != r7) goto L_0x0169
                return r7
            L_0x0169:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.RingStyleRepository r3 = r3.j
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x0426
                r6.L$0 = r0
                r5 = 8
                r6.label = r5
                java.lang.Object r3 = r3.downloadRingStyleList(r4, r1, r6)
                if (r3 != r7) goto L_0x0184
                return r7
            L_0x0184:
                com.fossil.gs6 r3 = r6.this$0
                com.portfolio.platform.data.source.AlarmsRepository r3 = r3.p
                r6.L$0 = r0
                r4 = 9
                r6.label = r4
                java.lang.Object r3 = r3.downloadAlarms(r6)
                if (r3 != r7) goto L_0x0077
                return r7
            L_0x0197:
                com.fossil.gs6 r0 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r0 = r0.g
                com.fossil.gs6 r4 = r6.this$0
                java.lang.String r4 = r4.d
                if (r4 == 0) goto L_0x0422
                java.util.ArrayList r0 = r0.getPresetList(r4)
                boolean r4 = r0.isEmpty()
                if (r4 == 0) goto L_0x01f2
                com.fossil.gs6 r4 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r4 = r4.g
                com.fossil.gs6 r5 = r6.this$0
                java.lang.String r5 = r5.d
                if (r5 == 0) goto L_0x01f5
                java.util.List r4 = r4.getRecommendPresetList(r5)
                java.util.Iterator r5 = r4.iterator()
            L_0x01c5:
                boolean r9 = r5.hasNext()
                if (r9 == 0) goto L_0x01db
                java.lang.Object r9 = r5.next()
                com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset r9 = (com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset) r9
                com.portfolio.platform.data.model.diana.preset.DianaPreset$Companion r10 = com.portfolio.platform.data.model.diana.preset.DianaPreset.Companion
                com.portfolio.platform.data.model.diana.preset.DianaPreset r9 = r10.cloneFromDefaultPreset(r9)
                r0.add(r9)
                goto L_0x01c5
            L_0x01db:
                com.fossil.gs6 r5 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r5 = r5.g
                r6.L$0 = r3
                r6.L$1 = r0
                r6.L$2 = r4
                r4 = 10
                r6.label = r4
                java.lang.Object r4 = r5.upsertPresetList(r0, r6)
                if (r4 != r7) goto L_0x01f2
                return r7
            L_0x01f2:
                r4 = r3
                r3 = r0
                goto L_0x01f9
            L_0x01f5:
                com.fossil.ee7.a()
                throw r8
            L_0x01f9:
                java.util.Iterator r0 = r3.iterator()
            L_0x01fd:
                boolean r5 = r0.hasNext()
                if (r5 == 0) goto L_0x0219
                java.lang.Object r5 = r0.next()
                r9 = r5
                com.portfolio.platform.data.model.diana.preset.DianaPreset r9 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r9
                boolean r9 = r9.isActive()
                java.lang.Boolean r9 = com.fossil.pb7.a(r9)
                boolean r9 = r9.booleanValue()
                if (r9 == 0) goto L_0x01fd
                goto L_0x021a
            L_0x0219:
                r5 = r8
            L_0x021a:
                com.portfolio.platform.data.model.diana.preset.DianaPreset r5 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r5
                if (r5 != 0) goto L_0x0244
                boolean r0 = r3.isEmpty()
                if (r0 == 0) goto L_0x0244
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gs6.q
                java.lang.String r2 = "activePreset is null, preset list is emptyObjectObject?"
                r0.d(r1, r2)
                com.fossil.gs6 r0 = r6.this$0
                com.fossil.ds6 r1 = new com.fossil.ds6
                r2 = 600(0x258, float:8.41E-43)
                java.lang.String r3 = ""
                r1.<init>(r2, r3)
                r0.a(r1)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x0244:
                if (r5 != 0) goto L_0x0284
                java.lang.Object r0 = r3.get(r2)
                com.portfolio.platform.data.model.diana.preset.DianaPreset r0 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r0
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                java.lang.String r9 = com.fossil.gs6.q
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "Active preset is null ,pick "
                r10.append(r11)
                r10.append(r0)
                java.lang.String r10 = r10.toString()
                r5.d(r9, r10)
                r0.setActive(r1)
                com.fossil.gs6 r5 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r5 = r5.g
                r6.L$0 = r4
                r6.L$1 = r3
                r6.L$2 = r0
                r9 = 11
                r6.label = r9
                java.lang.Object r5 = r5.upsertPreset(r0, r6)
                if (r5 != r7) goto L_0x0285
                return r7
            L_0x0284:
                r0 = r5
            L_0x0285:
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                java.lang.String r9 = com.fossil.gs6.q
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "activePreset="
                r10.append(r11)
                r10.append(r0)
                java.lang.String r10 = r10.toString()
                r5.d(r9, r10)
                com.fossil.gs6 r5 = r6.this$0
                com.portfolio.platform.data.source.WatchFaceRepository r5 = r5.i
                com.fossil.gs6 r9 = r6.this$0
                java.lang.String r9 = r9.d
                if (r9 == 0) goto L_0x041e
                com.fossil.qb5 r10 = com.fossil.qb5.BACKGROUND
                java.util.List r5 = r5.getWatchFacesWithType(r9, r10)
                boolean r9 = r5.isEmpty()
                r9 = r9 ^ r1
                if (r9 == 0) goto L_0x0351
                com.fossil.oe7 r9 = new com.fossil.oe7
                r9.<init>()
                r9.element = r2
                java.util.Iterator r10 = r3.iterator()
            L_0x02c9:
                boolean r11 = r10.hasNext()
                if (r11 == 0) goto L_0x032a
                java.lang.Object r11 = r10.next()
                com.portfolio.platform.data.model.diana.preset.DianaPreset r11 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r11
                com.fossil.gs6 r12 = r6.this$0
                com.portfolio.platform.data.source.WatchFaceRepository r12 = r12.i
                java.lang.String r13 = r11.getWatchFaceId()
                com.portfolio.platform.data.model.diana.preset.WatchFace r12 = r12.getWatchFaceWithId(r13)
                if (r12 != 0) goto L_0x02c9
                com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
                java.lang.String r13 = com.fossil.gs6.q
                java.lang.StringBuilder r14 = new java.lang.StringBuilder
                r14.<init>()
                java.lang.String r15 = "Watchface "
                r14.append(r15)
                java.lang.String r15 = r11.getWatchFaceId()
                r14.append(r15)
                java.lang.String r15 = " is not exist, choose first watch face "
                r14.append(r15)
                java.lang.Object r15 = r5.get(r2)
                com.portfolio.platform.data.model.diana.preset.WatchFace r15 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r15
                r14.append(r15)
                java.lang.String r15 = " for user"
                r14.append(r15)
                java.lang.String r14 = r14.toString()
                r12.d(r13, r14)
                java.lang.Object r12 = r5.get(r2)
                com.portfolio.platform.data.model.diana.preset.WatchFace r12 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r12
                java.lang.String r12 = r12.getId()
                r11.setWatchFaceId(r12)
                r9.element = r1
                goto L_0x02c9
            L_0x032a:
                boolean r1 = r9.element
                if (r1 == 0) goto L_0x0351
                com.fossil.gs6 r1 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r1 = r1.g
                r6.L$0 = r4
                r6.L$1 = r3
                r6.L$2 = r0
                r6.L$3 = r5
                r6.L$4 = r9
                r2 = 12
                r6.label = r2
                java.lang.Object r1 = r1.upsertPresetList(r3, r6)
                if (r1 != r7) goto L_0x0349
                return r7
            L_0x0349:
                r1 = r0
                r2 = r3
                r3 = r4
                r0 = r5
            L_0x034d:
                r5 = r0
                r0 = r1
                r4 = r3
                r3 = r2
            L_0x0351:
                java.util.ArrayList r1 = r0.getComplications()
                com.google.gson.Gson r2 = new com.google.gson.Gson
                r2.<init>()
                com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings r1 = com.fossil.xc5.a(r1, r2)
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                com.fossil.gs6 r9 = r6.this$0
                java.lang.String r9 = r9.d
                if (r9 == 0) goto L_0x041a
                r2.a(r1, r9)
                java.util.ArrayList r2 = r0.getWatchapps()
                com.google.gson.Gson r9 = new com.google.gson.Gson
                r9.<init>()
                com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings r2 = com.fossil.xc5.b(r2, r9)
                com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r9 = r9.c()
                com.fossil.gs6 r10 = r6.this$0
                java.lang.String r10 = r10.d
                if (r10 == 0) goto L_0x0416
                r9.a(r2, r10)
                com.fossil.nx6 r9 = com.fossil.nx6.b
                com.fossil.gs6 r10 = r6.this$0
                com.fossil.nw5 r10 = r10.k
                com.fossil.gs6 r11 = r6.this$0
                com.fossil.vu5 r11 = r11.l
                com.fossil.gs6 r12 = r6.this$0
                com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r12 = r12.m
                com.fossil.gs6 r13 = r6.this$0
                com.fossil.ch5 r13 = r13.n
                r6.L$0 = r4
                r6.L$1 = r3
                r6.L$2 = r0
                r6.L$3 = r5
                r6.L$4 = r1
                r6.L$5 = r2
                r0 = 13
                r6.label = r0
                r0 = r9
                r1 = r10
                r2 = r11
                r3 = r12
                r4 = r13
                r5 = r16
                java.lang.Object r0 = r0.a(r1, r2, r3, r4, r5)
                if (r0 != r7) goto L_0x03c5
                return r7
            L_0x03c5:
                java.util.List r0 = (java.util.List) r0
                com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
                long r2 = java.lang.System.currentTimeMillis()
                r1.<init>(r0, r2)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                com.fossil.gs6 r2 = r6.this$0
                java.lang.String r2 = r2.d
                if (r2 == 0) goto L_0x0412
                r0.a(r1, r2)
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r1 = com.fossil.gs6.q
                java.lang.String r2 = "start set localization"
                r0.e(r1, r2)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                com.fossil.gs6 r1 = r6.this$0
                java.lang.String r1 = r1.d
                if (r1 == 0) goto L_0x040e
                r0.p(r1)
                com.fossil.gs6 r0 = r6.this$0
                com.fossil.fs6 r1 = new com.fossil.fs6
                r1.<init>()
                r0.a(r1)
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            L_0x040e:
                com.fossil.ee7.a()
                throw r8
            L_0x0412:
                com.fossil.ee7.a()
                throw r8
            L_0x0416:
                com.fossil.ee7.a()
                throw r8
            L_0x041a:
                com.fossil.ee7.a()
                throw r8
            L_0x041e:
                com.fossil.ee7.a()
                throw r8
            L_0x0422:
                com.fossil.ee7.a()
                throw r8
            L_0x0426:
                com.fossil.ee7.a()
                throw r8
            L_0x042a:
                com.fossil.ee7.a()
                throw r8
            L_0x042e:
                com.fossil.ee7.a()
                throw r8
            L_0x0432:
                com.fossil.ee7.a()
                throw r8
            L_0x0436:
                com.fossil.ee7.a()
                throw r8
            L_0x043a:
                com.fossil.ee7.a()
                throw r8
                switch-data {0->0x00bf, 1->0x00b7, 2->0x00af, 3->0x00a7, 4->0x009e, 5->0x0095, 6->0x008c, 7->0x0083, 8->0x007a, 9->0x0070, 10->0x005f, 11->0x004e, 12->0x0035, 13->0x0016, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gs6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = gs6.class.getSimpleName();
        ee7.a((Object) simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public gs6(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, CategoryRepository categoryRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, nw5 nw5, vu5 vu5, NotificationSettingsDatabase notificationSettingsDatabase, ch5 ch5, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository) {
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(categoryRepository, "mCategoryRepository");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        ee7.b(ringStyleRepository, "mRingStyleRepository");
        ee7.b(nw5, "mGetApp");
        ee7.b(vu5, "mGetAllContactGroups");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        ee7.b(ch5, "mSharePref");
        ee7.b(watchLocalizationRepository, "mWatchLocalizationRepository");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.e = watchAppRepository;
        this.f = complicationRepository;
        this.g = dianaPresetRepository;
        this.h = categoryRepository;
        this.i = watchFaceRepository;
        this.j = ringStyleRepository;
        this.k = nw5;
        this.l = vu5;
        this.m = notificationSettingsDatabase;
        this.n = ch5;
        this.o = watchLocalizationRepository;
        this.p = alarmsRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(es6 es6, fb7 fb7) {
        return a(es6, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return q;
    }

    @DexIgnore
    public final ik7 d() {
        return xh7.b(b(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public Object a(es6 es6, fb7<? super i97> fb7) {
        if (es6 == null) {
            a(new ds6(600, ""));
            return i97.a;
        }
        this.d = es6.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "download device setting of " + this.d);
        if (!xw6.b(PortfolioApp.g0.c())) {
            a(new ds6(601, ""));
            return i97.a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            be5.a aVar = be5.o;
            String str2 = this.d;
            if (str2 == null) {
                ee7.a();
                throw null;
            } else if (aVar.e(str2)) {
                d();
                return i97.a;
            }
        }
        a(new ds6(600, ""));
        return i97.a;
    }
}
