package com.fossil;

import android.content.Context;
import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bh5;
import com.fossil.ci;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase;
import java.io.File;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pg5 {
    @DexIgnore
    public static kn7 a; // = mn7.a(false, 1, null);
    @DexIgnore
    public static volatile UserDatabase b;
    @DexIgnore
    public static volatile FitnessDatabase c;
    @DexIgnore
    public static volatile SleepDatabase d;
    @DexIgnore
    public static volatile GoalTrackingDatabase e;
    @DexIgnore
    public static volatile ThirdPartyDatabase f;
    @DexIgnore
    public static volatile AlarmDatabase g;
    @DexIgnore
    public static volatile WorkoutSettingDatabase h;
    @DexIgnore
    public static /* final */ pg5 i; // = new pg5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {281}, m = "getAlarmDatabase")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {270}, m = "getFitnessDatabase")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {248}, m = "getGoalTrackingDatabase")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {259}, m = "getSleepDatabase")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {292}, m = "getThirdPartyDatabase")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {237}, m = "getUserDatabase")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {310}, m = "getWorkoutSettingDatabase")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(pg5 pg5, fb7 fb7) {
            super(fb7);
            this.this$0 = pg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager$provideEncryptedDatabase$2", f = "EncryptionDatabaseManager.kt", l = {320, 52}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        public h(fb7 fb7) {
            super(2, fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                java.lang.String r2 = "provideEncryptedDatabase provided already"
                r3 = 2
                r4 = 1
                r5 = 0
                java.lang.String r6 = "EncryptionDM"
                if (r1 == 0) goto L_0x0036
                if (r1 == r4) goto L_0x002a
                if (r1 != r3) goto L_0x0022
                java.lang.Object r0 = r7.L$1
                com.fossil.kn7 r0 = (com.fossil.kn7) r0
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)     // Catch:{ all -> 0x001f }
                goto L_0x0078
            L_0x001f:
                r8 = move-exception
                goto L_0x0113
            L_0x0022:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x002a:
                java.lang.Object r1 = r7.L$1
                com.fossil.kn7 r1 = (com.fossil.kn7) r1
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r8)
                goto L_0x005c
            L_0x0036:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r8 = r7.p$
                com.fossil.pg5 r1 = com.fossil.pg5.i
                boolean r1 = r1.b()
                if (r1 == 0) goto L_0x0117
                java.lang.String r1 = "provideEncryptedDatabase start lock"
                android.util.Log.d(r6, r1)
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.kn7 r1 = com.fossil.pg5.a
                r7.L$0 = r8
                r7.L$1 = r1
                r7.label = r4
                java.lang.Object r4 = r1.a(r5, r7)
                if (r4 != r0) goto L_0x005b
                return r0
            L_0x005b:
                r4 = r8
            L_0x005c:
                com.fossil.pg5 r8 = com.fossil.pg5.i     // Catch:{ all -> 0x0111 }
                boolean r8 = r8.b()     // Catch:{ all -> 0x0111 }
                if (r8 == 0) goto L_0x00fb
                com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x0111 }
                com.portfolio.platform.PortfolioApp r8 = r8.c()     // Catch:{ all -> 0x0111 }
                r7.L$0 = r4     // Catch:{ all -> 0x0111 }
                r7.L$1 = r1     // Catch:{ all -> 0x0111 }
                r7.label = r3     // Catch:{ all -> 0x0111 }
                java.lang.Object r8 = r8.e(r7)     // Catch:{ all -> 0x0111 }
                if (r8 != r0) goto L_0x0077
                return r0
            L_0x0077:
                r0 = r1
            L_0x0078:
                char[] r8 = (char[]) r8
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.portfolio.platform.data.model.room.UserDatabase r1 = com.fossil.pg5.b
                if (r1 != 0) goto L_0x00a3
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.model.room.UserDatabase r2 = r2.f(r8)
                com.fossil.pg5.b = r2
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r2 = r2.c(r8)
                com.fossil.pg5.e = r2
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r2 = r2.e(r8)
                com.fossil.pg5.f = r2
            L_0x00a3:
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = r2.b(r8)
                com.fossil.pg5.c = r2
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.sleep.SleepDatabase r2 = r2.d(r8)
                com.fossil.pg5.d = r2
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.alarm.AlarmDatabase r2 = r2.a(r8)
                com.fossil.pg5.g = r2
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.fossil.pg5 r2 = com.fossil.pg5.i
                com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r8 = r2.g(r8)
                com.fossil.pg5.h = r8
                com.fossil.ah5$a r8 = com.fossil.ah5.p
                com.fossil.ah5 r8 = r8.a()
                com.fossil.pg5 r1 = com.fossil.pg5.i
                com.portfolio.platform.data.model.room.UserDatabase r1 = com.fossil.pg5.b
                if (r1 == 0) goto L_0x00f0
                com.portfolio.platform.data.model.room.UserDao r1 = r1.userDao()
                if (r1 == 0) goto L_0x00f0
                com.portfolio.platform.data.model.MFUser r1 = r1.getCurrentUser()
                if (r1 == 0) goto L_0x00f0
                java.lang.String r1 = r1.getUserId()
                if (r1 == 0) goto L_0x00f0
                goto L_0x00f2
            L_0x00f0:
                java.lang.String r1 = ""
            L_0x00f2:
                r8.b(r1)
                java.lang.String r8 = "provideEncryptedDatabase done "
                android.util.Log.d(r6, r8)
                goto L_0x00ff
            L_0x00fb:
                android.util.Log.d(r6, r2)
                r0 = r1
            L_0x00ff:
                java.lang.String r8 = "provideEncryptedDatabase release log"
                int r8 = android.util.Log.d(r6, r8)
                java.lang.Integer r8 = com.fossil.pb7.a(r8)
                r0.a(r5)
                int r8 = r8.intValue()
                goto L_0x011b
            L_0x0111:
                r8 = move-exception
                r0 = r1
            L_0x0113:
                r0.a(r5)
                throw r8
            L_0x0117:
                int r8 = android.util.Log.d(r6, r2)
            L_0x011b:
                java.lang.Integer r8 = com.fossil.pb7.a(r8)
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public final /* synthetic */ Object h(fb7<? super Integer> fb7) {
        return vh7.a(qj7.b(), new h(null), fb7);
    }

    @DexIgnore
    public final SleepDatabase d(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_sleep.db";
        bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), str2);
        Log.d("EncryptionDM", "getSleepDatabase isEncrypted " + a2 + " dbName " + str2);
        if (a2 == bh5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getSleepDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            bh5 bh5 = bh5.a;
            PortfolioApp c2 = PortfolioApp.g0.c();
            File databasePath = PortfolioApp.g0.c().getDatabasePath(str2);
            ee7.a((Object) databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            bh5.a((Context) c2, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            ci.a a3 = bi.a(PortfolioApp.g0.c(), SleepDatabase.class, str2);
            a3.a(supportFactory);
            a3.a(SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9());
            a3.d();
            a3.c();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (SleepDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final ThirdPartyDatabase e(char[] cArr) {
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), "thirdParty.db");
            Log.d("EncryptionDM", "getThirdPartyDatabase isEncrypted " + a2 + " dbName thirdParty.db");
            if (a2 == bh5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getThirdPartyDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                bh5 bh5 = bh5.a;
                PortfolioApp c2 = PortfolioApp.g0.c();
                File databasePath = PortfolioApp.g0.c().getDatabasePath("thirdParty.db");
                ee7.a((Object) databasePath, "PortfolioApp.instance.ge\u2026HIRD_PARTY_DATABASE_NAME)");
                bh5.a((Context) c2, databasePath, (char[]) cArr.clone());
            }
            ci.a a3 = bi.a(PortfolioApp.g0.c(), ThirdPartyDatabase.class, "thirdParty.db");
            a3.a(supportFactory);
            a3.d();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (ThirdPartyDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final UserDatabase f(char[] cArr) {
        if (bh5.a.a(PortfolioApp.g0.c(), "user.db") == bh5.a.UNENCRYPTED) {
            bh5 bh5 = bh5.a;
            PortfolioApp c2 = PortfolioApp.g0.c();
            File databasePath = PortfolioApp.g0.c().getDatabasePath("user.db");
            ee7.a((Object) databasePath, "PortfolioApp.instance.ge\u2026tants.USER_DATABASE_NAME)");
            bh5.a((Context) c2, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            ci.a a2 = bi.a(PortfolioApp.g0.c(), UserDatabase.class, "user.db");
            a2.a(UserDatabase.Companion.getMIGRATION_FROM_3_TO_4(), UserDatabase.Companion.getMIGRATION_FROM_4_TO_5());
            a2.d();
            a2.c();
            a2.a(supportFactory);
            ci b2 = a2.b();
            ee7.a((Object) b2, "Room\n                .da\u2026\n                .build()");
            return (UserDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingDatabase g(char[] cArr) {
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), "workoutSetting.db");
            Log.d("EncryptionDM", "getWorkoutSetting isEncrypted " + a2 + " dbName workoutSetting.db");
            if (a2 == bh5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getWorkoutSetting encrypt fitness db with passphrase " + ((Object) cArr));
                bh5 bh5 = bh5.a;
                PortfolioApp c2 = PortfolioApp.g0.c();
                File databasePath = PortfolioApp.g0.c().getDatabasePath("workoutSetting.db");
                ee7.a((Object) databasePath, "PortfolioApp.instance.ge\u2026UT_SETTING_DATABASE_NAME)");
                bh5.a((Context) c2, databasePath, (char[]) cArr.clone());
            }
            ci.a a3 = bi.a(PortfolioApp.g0.c(), WorkoutSettingDatabase.class, "workoutSetting.db");
            a3.a(supportFactory);
            a3.a(WorkoutSettingDatabase.Companion.getMIGRATION_FROM_1_TO_2());
            a3.d();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (WorkoutSettingDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final boolean b() {
        return b == null || c == null || d == null || e == null || f == null || g == null || h == null;
    }

    @DexIgnore
    public final void c() {
        Log.d("EncryptionDM", "resetDatabasePrefix");
        c = null;
        d = null;
        g = null;
        b = null;
        h = null;
        e = null;
        f = null;
    }

    @DexIgnore
    public final FitnessDatabase b(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_fitness.db";
        bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), str2);
        Log.d("EncryptionDM", "getFitnessDatabase isEncrypted " + a2 + " dbName " + str2);
        if (a2 == bh5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getFitnessDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            bh5 bh5 = bh5.a;
            PortfolioApp c2 = PortfolioApp.g0.c();
            File databasePath = PortfolioApp.g0.c().getDatabasePath(str2);
            ee7.a((Object) databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            bh5.a((Context) c2, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            ci.a a3 = bi.a(PortfolioApp.g0.c(), FitnessDatabase.class, str2);
            a3.a(supportFactory);
            a3.a(FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21());
            a3.a(FitnessDatabase.Companion.getMIGRATION_FROM_21_TO_22());
            a3.d();
            a3.c();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (FitnessDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final AlarmDatabase a(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_alarm.db";
        bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), str2);
        Log.d("EncryptionDM", "getAlarmDatabase isEncrypted " + a2 + " dbName " + str2);
        if (a2 == bh5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getAlarmDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            bh5 bh5 = bh5.a;
            PortfolioApp c2 = PortfolioApp.g0.c();
            File databasePath = PortfolioApp.g0.c().getDatabasePath(str2);
            ee7.a((Object) databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            bh5.a((Context) c2, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            ci.a a3 = bi.a(PortfolioApp.g0.c(), AlarmDatabase.class, str2);
            a3.a(supportFactory);
            a3.a(AlarmDatabase.Companion.migrating3Or4To5(str, 3), AlarmDatabase.Companion.migrating3Or4To5(str, 4), AlarmDatabase.Companion.getMIGRATION_FROM_5_TO_6());
            a3.d();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (AlarmDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDatabase c(char[] cArr) {
        if (cArr != null) {
            SupportFactory supportFactory = new SupportFactory(SQLiteDatabase.getBytes((char[]) cArr.clone()));
            bh5.a a2 = bh5.a.a(PortfolioApp.g0.c(), "goalTracking.db");
            Log.d("EncryptionDM", "getGoalTrackingDatabase isEncrypted " + a2 + " dbName goalTracking.db");
            if (a2 == bh5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getGoalTrackingDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                bh5 bh5 = bh5.a;
                PortfolioApp c2 = PortfolioApp.g0.c();
                File databasePath = PortfolioApp.g0.c().getDatabasePath("goalTracking.db");
                ee7.a((Object) databasePath, "PortfolioApp.instance.ge\u2026L_TRACKING_DATABASE_NAME)");
                bh5.a((Context) c2, databasePath, (char[]) cArr.clone());
            }
            ci.a a3 = bi.a(PortfolioApp.g0.c(), GoalTrackingDatabase.class, "goalTracking.db");
            a3.a(supportFactory);
            a3.d();
            ci b2 = a3.b();
            ee7.a((Object) b2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (GoalTrackingDatabase) b2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.fb7<? super com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.e
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$e r0 = (com.fossil.pg5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$e r0 = new com.fossil.pg5$e
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r7 = com.fossil.pg5.f
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getThirdPartyDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getThirdPartyDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r7 = com.fossil.pg5.f
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r7 = com.fossil.pg5.f
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.e(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.fb7<? super com.portfolio.platform.data.model.room.UserDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.f
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$f r0 = (com.fossil.pg5.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$f r0 = new com.fossil.pg5$f
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.model.room.UserDatabase r7 = com.fossil.pg5.b
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getUserDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getUserDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.model.room.UserDatabase r7 = com.fossil.pg5.b
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.model.room.UserDatabase r7 = com.fossil.pg5.b
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.f(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.fossil.fb7<? super com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.g
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$g r0 = (com.fossil.pg5.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$g r0 = new com.fossil.pg5$g
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r7 = com.fossil.pg5.h
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getWorkoutSettingDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getWorkoutSettingDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r7 = com.fossil.pg5.h
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r7 = com.fossil.pg5.h
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.g(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.fb7<? super com.portfolio.platform.data.source.local.sleep.SleepDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.d
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$d r0 = (com.fossil.pg5.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$d r0 = new com.fossil.pg5$d
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r7 = com.fossil.pg5.d
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getSleepDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getSleepDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r7 = com.fossil.pg5.d
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r7 = com.fossil.pg5.d
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.d(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.portfolio.platform.data.source.local.fitness.FitnessDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.b
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$b r0 = (com.fossil.pg5.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$b r0 = new com.fossil.pg5$b
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = com.fossil.pg5.c
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getFitnessDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getFitnessDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = com.fossil.pg5.c
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r7 = com.fossil.pg5.c
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.fb7<? super com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.c
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$c r0 = (com.fossil.pg5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$c r0 = new com.fossil.pg5$c
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r7 = com.fossil.pg5.e
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getGoalTrackingDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getGoalTrackingDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r7 = com.fossil.pg5.e
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r7 = com.fossil.pg5.e
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.c(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.portfolio.platform.data.source.local.alarm.AlarmDatabase> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.pg5.a
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.pg5$a r0 = (com.fossil.pg5.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pg5$a r0 = new com.fossil.pg5$a
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = "EncryptionDM"
            r5 = 1
            if (r2 == 0) goto L_0x0038
            if (r2 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.pg5 r0 = (com.fossil.pg5) r0
            com.fossil.t87.a(r7)
            goto L_0x004f
        L_0x0030:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0038:
            com.fossil.t87.a(r7)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r7 = com.fossil.pg5.g
            if (r7 != 0) goto L_0x005d
            java.lang.String r7 = "getAlarmDatabase start"
            android.util.Log.d(r4, r7)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r7 = r6.h(r0)
            if (r7 != r1) goto L_0x004f
            return r1
        L_0x004f:
            java.lang.String r7 = "getAlarmDatabase done"
            android.util.Log.d(r4, r7)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r7 = com.fossil.pg5.g
            if (r7 == 0) goto L_0x0059
            goto L_0x0061
        L_0x0059:
            com.fossil.ee7.a()
            throw r3
        L_0x005d:
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r7 = com.fossil.pg5.g
            if (r7 == 0) goto L_0x0062
        L_0x0061:
            return r7
        L_0x0062:
            com.fossil.ee7.a()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pg5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final UserDatabase a() {
        return b;
    }
}
