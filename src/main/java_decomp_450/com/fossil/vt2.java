package com.fossil;

import java.util.Comparator;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt2 {
    @DexIgnore
    public static boolean a(Comparator<?> comparator, Iterable<?> iterable) {
        Object obj;
        or2.a(comparator);
        or2.a(iterable);
        if (iterable instanceof SortedSet) {
            obj = ((SortedSet) iterable).comparator();
            if (obj == null) {
                obj = ht2.zza;
            }
        } else if (!(iterable instanceof wt2)) {
            return false;
        } else {
            obj = ((wt2) iterable).comparator();
        }
        return comparator.equals(obj);
    }
}
