package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.StatFs;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku {
    @DexIgnore
    public static /* final */ ku a; // = new ku();

    @DexIgnore
    public final int a(int i, int i2, Bitmap.Config config) {
        return i * i2 * iu.a(config);
    }

    @DexIgnore
    public final File b(Context context) {
        ee7.b(context, "context");
        File file = new File(context.getCacheDir(), "image_cache");
        file.mkdirs();
        return file;
    }

    @DexIgnore
    public final long a(File file) {
        ee7.b(file, "cacheDirectory");
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            return qf7.a((long) (0.02d * ((double) (Build.VERSION.SDK_INT > 18 ? statFs.getBlockCountLong() : (long) statFs.getBlockCount())) * ((double) (Build.VERSION.SDK_INT > 18 ? statFs.getBlockSizeLong() : (long) statFs.getBlockSize()))), 10485760, 262144000);
        } catch (Exception unused) {
            return 10485760;
        }
    }

    @DexIgnore
    public final double b() {
        return Build.VERSION.SDK_INT >= 26 ? 0.25d : 0.5d;
    }

    @DexIgnore
    public final Bitmap.Config a() {
        return Build.VERSION.SDK_INT >= 26 ? Bitmap.Config.HARDWARE : Bitmap.Config.ARGB_8888;
    }

    @DexIgnore
    public final long a(Context context, double d) {
        ee7.b(context, "context");
        Object a2 = v6.a(context, ActivityManager.class);
        if (a2 != null) {
            ActivityManager activityManager = (ActivityManager) a2;
            int largeMemoryClass = (context.getApplicationInfo().flags & 1048576) != 0 ? activityManager.getLargeMemoryClass() : activityManager.getMemoryClass();
            double d2 = (double) 1024;
            return (long) (d * ((double) largeMemoryClass) * d2 * d2);
        }
        throw new IllegalStateException(("System service of type " + ActivityManager.class + " was not found.").toString());
    }

    @DexIgnore
    public final double a(Context context) {
        ee7.b(context, "context");
        Object a2 = v6.a(context, ActivityManager.class);
        if (a2 != null) {
            return Build.VERSION.SDK_INT < 19 || ((ActivityManager) a2).isLowRamDevice() ? 0.15d : 0.25d;
        }
        throw new IllegalStateException(("System service of type " + ActivityManager.class + " was not found.").toString());
    }
}
