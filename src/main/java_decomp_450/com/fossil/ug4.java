package com.fossil;

import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug4 implements sg4 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("ISO-8859-1");

    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) {
        int i3;
        int i4;
        Charset charset;
        Charset charset2 = a;
        int i5 = 33;
        if (map != null) {
            if (map.containsKey(og4.CHARACTER_SET)) {
                charset2 = Charset.forName(map.get(og4.CHARACTER_SET).toString());
            }
            if (map.containsKey(og4.ERROR_CORRECTION)) {
                i5 = Integer.parseInt(map.get(og4.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(og4.AZTEC_LAYERS)) {
                charset = charset2;
                i4 = i5;
                i3 = Integer.parseInt(map.get(og4.AZTEC_LAYERS).toString());
                return a(str, mg4, i, i2, charset, i4, i3);
            }
            charset = charset2;
            i4 = i5;
        } else {
            charset = charset2;
            i4 = 33;
        }
        i3 = 0;
        return a(str, mg4, i, i2, charset, i4, i3);
    }

    @DexIgnore
    public static dh4 a(String str, mg4 mg4, int i, int i2, Charset charset, int i3, int i4) {
        if (mg4 == mg4.AZTEC) {
            return a(xg4.a(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got " + mg4);
    }

    @DexIgnore
    public static dh4 a(vg4 vg4, int i, int i2) {
        dh4 a2 = vg4.a();
        if (a2 != null) {
            int h = a2.h();
            int f = a2.f();
            int max = Math.max(i, h);
            int max2 = Math.max(i2, f);
            int min = Math.min(max / h, max2 / f);
            int i3 = (max - (h * min)) / 2;
            int i4 = (max2 - (f * min)) / 2;
            dh4 dh4 = new dh4(max, max2);
            int i5 = 0;
            while (i5 < f) {
                int i6 = i3;
                int i7 = 0;
                while (i7 < h) {
                    if (a2.a(i7, i5)) {
                        dh4.a(i6, i4, min, min);
                    }
                    i7++;
                    i6 += min;
                }
                i5++;
                i4 += min;
            }
            return dh4;
        }
        throw new IllegalStateException();
    }
}
