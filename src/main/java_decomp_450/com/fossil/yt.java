package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yt {
    @DexIgnore
    Object a(rq rqVar, Bitmap bitmap, rt rtVar, fb7<? super Bitmap> fb7);

    @DexIgnore
    String key();
}
