package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nm3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 b;

    @DexIgnore
    public qk3(ek3 ek3, nm3 nm3) {
        this.b = ek3;
        this.a = nm3;
    }

    @DexIgnore
    public final void run() {
        bg3 d = this.b.d;
        if (d == null) {
            this.b.e().t().a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            d.d(this.a);
            this.b.J();
        } catch (RemoteException e) {
            this.b.e().t().a("Failed to send measurementEnabled to the service", e);
        }
    }
}
