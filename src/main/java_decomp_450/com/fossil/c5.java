package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c5 {
    @DexIgnore
    public static int k; // = 1;
    @DexIgnore
    public String a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public float e;
    @DexIgnore
    public float[] f; // = new float[7];
    @DexIgnore
    public a g;
    @DexIgnore
    public v4[] h; // = new v4[8];
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j; // = 0;

    @DexIgnore
    public enum a {
        UNRESTRICTED,
        CONSTANT,
        SLACK,
        ERROR,
        UNKNOWN
    }

    @DexIgnore
    public c5(a aVar, String str) {
        this.g = aVar;
    }

    @DexIgnore
    public static void b() {
        k++;
    }

    @DexIgnore
    public final void a(v4 v4Var) {
        int i2 = 0;
        while (true) {
            int i3 = this.i;
            if (i2 >= i3) {
                v4[] v4VarArr = this.h;
                if (i3 >= v4VarArr.length) {
                    this.h = (v4[]) Arrays.copyOf(v4VarArr, v4VarArr.length * 2);
                }
                v4[] v4VarArr2 = this.h;
                int i4 = this.i;
                v4VarArr2[i4] = v4Var;
                this.i = i4 + 1;
                return;
            } else if (this.h[i2] != v4Var) {
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void c(v4 v4Var) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            v4[] v4VarArr = this.h;
            v4VarArr[i3].d.a(v4VarArr[i3], v4Var, false);
        }
        this.i = 0;
    }

    @DexIgnore
    public String toString() {
        return "" + this.a;
    }

    @DexIgnore
    public final void b(v4 v4Var) {
        int i2 = this.i;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.h[i3] == v4Var) {
                for (int i4 = 0; i4 < (i2 - i3) - 1; i4++) {
                    v4[] v4VarArr = this.h;
                    int i5 = i3 + i4;
                    v4VarArr[i5] = v4VarArr[i5 + 1];
                }
                this.i--;
                return;
            }
        }
    }

    @DexIgnore
    public void a() {
        this.a = null;
        this.g = a.UNKNOWN;
        this.d = 0;
        this.b = -1;
        this.c = -1;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = 0;
        this.j = 0;
    }

    @DexIgnore
    public void a(a aVar, String str) {
        this.g = aVar;
    }
}
