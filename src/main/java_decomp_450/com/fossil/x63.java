package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface x63 extends IInterface {
    @DexIgnore
    ab2 a(ab2 ab2, ab2 ab22, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(ab2 ab2, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(o73 o73) throws RemoteException;

    @DexIgnore
    void e() throws RemoteException;

    @DexIgnore
    void onCreate(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onDestroy() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;

    @DexIgnore
    void onResume() throws RemoteException;

    @DexIgnore
    void onSaveInstanceState(Bundle bundle) throws RemoteException;

    @DexIgnore
    void onStart() throws RemoteException;

    @DexIgnore
    void onStop() throws RemoteException;
}
