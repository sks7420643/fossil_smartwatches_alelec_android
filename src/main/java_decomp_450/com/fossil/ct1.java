package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ct1<T> {
    @DexIgnore
    public static <T> ct1<T> a(T t) {
        return new at1(null, t, dt1.DEFAULT);
    }

    @DexIgnore
    public static <T> ct1<T> b(T t) {
        return new at1(null, t, dt1.VERY_LOW);
    }

    @DexIgnore
    public static <T> ct1<T> c(T t) {
        return new at1(null, t, dt1.HIGHEST);
    }

    @DexIgnore
    public abstract Integer a();

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public abstract dt1 c();
}
