package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vy4 extends uy4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H;
    @DexIgnore
    public long F;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        H.put(2131363342, 2);
        H.put(2131362111, 3);
        H.put(2131362732, 4);
        H.put(2131363346, 5);
        H.put(2131362003, 6);
        H.put(2131362067, 7);
        H.put(2131362699, 8);
        H.put(2131363279, 9);
        H.put(2131361997, 10);
        H.put(2131362063, 11);
        H.put(2131362690, 12);
        H.put(2131363274, 13);
        H.put(2131361994, 14);
    }
    */

    @DexIgnore
    public vy4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 15, G, H));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public vy4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleSwitchCompat) objArr[14], (FlexibleSwitchCompat) objArr[10], (FlexibleSwitchCompat) objArr[6], (ConstraintLayout) objArr[11], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[3], (RTLImageView) objArr[1], (RTLImageView) objArr[12], (RTLImageView) objArr[8], (RTLImageView) objArr[4], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5]);
        this.F = -1;
        ((uy4) this).A.setTag(null);
        a(view);
        f();
    }
}
