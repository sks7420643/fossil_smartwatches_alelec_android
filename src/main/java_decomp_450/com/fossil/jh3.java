package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jh3 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ hh3 b;

    @DexIgnore
    public jh3(hh3 hh3, String str) {
        this.b = hh3;
        a72.a((Object) str);
        this.a = str;
    }

    @DexIgnore
    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.b.e().t().a(this.a, th);
    }
}
