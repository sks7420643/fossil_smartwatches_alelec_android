package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm5 implements Factory<mm5> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;

    @DexIgnore
    public nm5(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static nm5 a(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5) {
        return new nm5(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static mm5 a(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        return new mm5(deviceRepository, hybridPresetRepository, dianaPresetRepository, portfolioApp, watchFaceRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public mm5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
