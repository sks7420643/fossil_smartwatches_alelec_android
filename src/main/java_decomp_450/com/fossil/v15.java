package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v15 extends u15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        R.put(2131362636, 2);
        R.put(2131363342, 3);
        R.put(2131362083, 4);
        R.put(2131362637, 5);
        R.put(2131362374, 6);
        R.put(2131362373, 7);
        R.put(2131362751, 8);
        R.put(2131362049, 9);
        R.put(2131362544, 10);
        R.put(2131362488, 11);
        R.put(2131362487, 12);
        R.put(2131363056, 13);
        R.put(2131362453, 14);
        R.put(2131362452, 15);
        R.put(2131362463, 16);
        R.put(2131362705, 17);
        R.put(2131361889, 18);
        R.put(2131362044, 19);
        R.put(2131362180, 20);
        R.put(2131362764, 21);
        R.put(2131363386, 22);
        R.put(2131362465, 23);
        R.put(2131362974, 24);
    }
    */

    @DexIgnore
    public v15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 25, Q, R));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.P != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.P = 1;
        }
        g();
    }

    @DexIgnore
    public v15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (AppBarLayout) objArr[18], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[1], (TodayHeartRateChart) objArr[20], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[11], (Guideline) objArr[10], (RTLImageView) objArr[2], (ImageView) objArr[5], (ImageView) objArr[17], (View) objArr[8], (LinearLayout) objArr[21], (ConstraintLayout) objArr[0], (RecyclerView) objArr[24], (View) objArr[13], (FlexibleTextView) objArr[3], (View) objArr[22]);
        this.P = -1;
        ((u15) this).K.setTag(null);
        a(view);
        f();
    }
}
