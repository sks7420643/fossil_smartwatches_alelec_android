package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ie {
    @DexIgnore
    public static final yi7 a(he heVar) {
        ee7.b(heVar, "$this$viewModelScope");
        yi7 yi7 = (yi7) heVar.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (yi7 != null) {
            return yi7;
        }
        Object tagIfAbsent = heVar.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new bd(dl7.a(null, 1, null).plus(qj7.c().g())));
        ee7.a(tagIfAbsent, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (yi7) tagIfAbsent;
    }
}
