package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa2 extends va2 {
    @DexIgnore
    public /* final */ Callable<String> e;

    @DexIgnore
    public xa2(Callable<String> callable) {
        super(false, null, null);
        this.e = callable;
    }

    @DexIgnore
    @Override // com.fossil.va2
    public final String a() {
        try {
            return this.e.call();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
