package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.workers.PushPendingDataWorker;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b07 implements Factory<PushPendingDataWorker.b> {
    @DexIgnore
    public /* final */ Provider<ActivitiesRepository> a;
    @DexIgnore
    public /* final */ Provider<SummariesRepository> b;
    @DexIgnore
    public /* final */ Provider<SleepSessionsRepository> c;
    @DexIgnore
    public /* final */ Provider<SleepSummariesRepository> d;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> e;
    @DexIgnore
    public /* final */ Provider<HeartRateSampleRepository> f;
    @DexIgnore
    public /* final */ Provider<HeartRateSummaryRepository> g;
    @DexIgnore
    public /* final */ Provider<FitnessDataRepository> h;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> i;
    @DexIgnore
    public /* final */ Provider<ch5> j;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> k;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> l;
    @DexIgnore
    public /* final */ Provider<ThirdPartyRepository> m;
    @DexIgnore
    public /* final */ Provider<ro4> n;
    @DexIgnore
    public /* final */ Provider<xo4> o;
    @DexIgnore
    public /* final */ Provider<FileRepository> p;
    @DexIgnore
    public /* final */ Provider<UserRepository> q;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> r;
    @DexIgnore
    public /* final */ Provider<to4> s;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> t;

    @DexIgnore
    public b07(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<ch5> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<ro4> provider14, Provider<xo4> provider15, Provider<FileRepository> provider16, Provider<UserRepository> provider17, Provider<WorkoutSettingRepository> provider18, Provider<to4> provider19, Provider<PortfolioApp> provider20) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
        this.o = provider15;
        this.p = provider16;
        this.q = provider17;
        this.r = provider18;
        this.s = provider19;
        this.t = provider20;
    }

    @DexIgnore
    public static b07 a(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<ch5> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<ro4> provider14, Provider<xo4> provider15, Provider<FileRepository> provider16, Provider<UserRepository> provider17, Provider<WorkoutSettingRepository> provider18, Provider<to4> provider19, Provider<PortfolioApp> provider20) {
        return new b07(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    public static PushPendingDataWorker.b b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<ch5> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<ro4> provider14, Provider<xo4> provider15, Provider<FileRepository> provider16, Provider<UserRepository> provider17, Provider<WorkoutSettingRepository> provider18, Provider<to4> provider19, Provider<PortfolioApp> provider20) {
        return new PushPendingDataWorker.b(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public PushPendingDataWorker.b get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t);
    }
}
