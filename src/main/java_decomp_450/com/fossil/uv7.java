package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv7 implements tu7<mo7, Short> {
    @DexIgnore
    public static /* final */ uv7 a; // = new uv7();

    @DexIgnore
    public Short a(mo7 mo7) throws IOException {
        return Short.valueOf(mo7.string());
    }
}
