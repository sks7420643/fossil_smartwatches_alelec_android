package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n86 extends k86 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public ob5 g;
    @DexIgnore
    public /* final */ l86 h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ pj4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$1", f = "DashboardActivityPresenter.kt", l = {85, 93}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ n86 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements te5.a {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // com.fossil.te5.a
            public final void a(te5.g gVar) {
                ee7.b(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardActivityPresenter", "XXX-  onStatusChange status=" + gVar);
                if (gVar.a()) {
                    this.a.this$0.h.d();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.n86$b$b")
        /* renamed from: com.fossil.n86$b$b  reason: collision with other inner class name */
        public static final class C0133b<T> implements zd<qf<ActivitySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0133b(MFUser mFUser, b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<ActivitySummary> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("XXX-  getSummariesPaging observer size=");
                sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
                local.d("DashboardActivityPresenter", sb.toString());
                if (qfVar != null) {
                    this.a.this$0.h.a(qfVar);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$initDataSource$1$user$1", f = "DashboardActivityPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.k;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(n86 n86, fb7 fb7) {
            super(2, fb7);
            this.this$0 = n86;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00f4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0118  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0123  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 0
                r3 = 2
                r4 = 1
                java.lang.String r5 = "DashboardActivityPresenter"
                if (r1 == 0) goto L_0x003a
                if (r1 == r4) goto L_0x0032
                if (r1 != r3) goto L_0x002a
                java.lang.Object r0 = r13.L$4
                com.fossil.n86 r0 = (com.fossil.n86) r0
                java.lang.Object r1 = r13.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r13.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r3 = r13.L$1
                com.portfolio.platform.data.model.MFUser r3 = (com.portfolio.platform.data.model.MFUser) r3
                java.lang.Object r3 = r13.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r14)
                goto L_0x00c9
            L_0x002a:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x0032:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x0055
            L_0x003a:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r1 = r13.p$
                com.fossil.n86 r14 = r13.this$0
                com.fossil.ti7 r14 = r14.c()
                com.fossil.n86$b$c r6 = new com.fossil.n86$b$c
                r6.<init>(r13, r2)
                r13.L$0 = r1
                r13.label = r4
                java.lang.Object r14 = com.fossil.vh7.a(r14, r6, r13)
                if (r14 != r0) goto L_0x0055
                return r0
            L_0x0055:
                com.portfolio.platform.data.model.MFUser r14 = (com.portfolio.platform.data.model.MFUser) r14
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "user "
                r6.append(r7)
                r6.append(r14)
                java.lang.String r6 = r6.toString()
                r4.d(r5, r6)
                if (r14 == 0) goto L_0x0145
                java.lang.String r4 = r14.getCreatedAt()
                java.util.Date r8 = com.fossil.zd5.d(r4)
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "XXX- get summaries with createdDate "
                r6.append(r7)
                r6.append(r8)
                java.lang.String r6 = r6.toString()
                r4.d(r5, r6)
                com.fossil.n86 r4 = r13.this$0
                com.portfolio.platform.data.source.SummariesRepository r6 = r4.i
                com.fossil.n86 r7 = r13.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r7 = r7.j
                java.lang.String r9 = "createdDate"
                com.fossil.ee7.a(r8, r9)
                com.fossil.n86 r9 = r13.this$0
                com.fossil.pj4 r9 = r9.l
                com.fossil.n86$b$a r10 = new com.fossil.n86$b$a
                r10.<init>(r13)
                r13.L$0 = r1
                r13.L$1 = r14
                r13.L$2 = r14
                r13.L$3 = r8
                r13.L$4 = r4
                r13.label = r3
                r11 = r13
                java.lang.Object r1 = r6.getSummariesPaging(r7, r8, r9, r10, r11)
                if (r1 != r0) goto L_0x00c5
                return r0
            L_0x00c5:
                r0 = r4
                r12 = r1
                r1 = r14
                r14 = r12
            L_0x00c9:
                com.portfolio.platform.data.Listing r14 = (com.portfolio.platform.data.Listing) r14
                r0.f = r14
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r3 = "listing "
                r0.append(r3)
                com.fossil.n86 r3 = r13.this$0
                com.portfolio.platform.data.Listing r3 = r3.f
                r0.append(r3)
                java.lang.String r3 = " pageList "
                r0.append(r3)
                com.fossil.n86 r3 = r13.this$0
                com.portfolio.platform.data.Listing r3 = r3.f
                if (r3 == 0) goto L_0x00f8
                androidx.lifecycle.LiveData r2 = r3.getPagedList()
            L_0x00f8:
                r0.append(r2)
                java.lang.String r0 = r0.toString()
                r14.d(r5, r0)
                com.fossil.n86 r14 = r13.this$0
                com.fossil.l86 r14 = r14.h
                com.fossil.n86 r0 = r13.this$0
                com.portfolio.platform.data.Listing r0 = r0.f
                if (r0 == 0) goto L_0x012b
                androidx.lifecycle.LiveData r0 = r0.getPagedList()
                if (r0 == 0) goto L_0x012b
                if (r14 == 0) goto L_0x0123
                com.fossil.m86 r14 = (com.fossil.m86) r14
                com.fossil.n86$b$b r2 = new com.fossil.n86$b$b
                r2.<init>(r1, r13)
                r0.a(r14, r2)
                goto L_0x012b
            L_0x0123:
                com.fossil.x87 r14 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment"
                r14.<init>(r0)
                throw r14
            L_0x012b:
                com.fossil.n86 r14 = r13.this$0
                com.fossil.l86 r14 = r14.h
                com.portfolio.platform.data.model.MFUser$UnitGroup r0 = r1.getUnitGroup()
                java.lang.String r0 = r0.getDistance()
                com.fossil.ob5 r0 = com.fossil.ob5.fromString(r0)
                java.lang.String r1 = "Unit.fromString(userInfo.unitGroup.distance)"
                com.fossil.ee7.a(r0, r1)
                r14.b(r0)
            L_0x0145:
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.n86.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1", f = "DashboardActivityPresenter.kt", l = {42}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ n86 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter$start$1$currentUser$1", f = "DashboardActivityPresenter.kt", l = {42}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository h = this.this$0.this$0.k;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = h.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(n86 n86, fb7 fb7) {
            super(2, fb7);
            this.this$0 = n86;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                if (this.this$0.g == null) {
                    this.this$0.g = ob5.fromString(mFUser.getUnitGroup().getDistance());
                } else {
                    ob5 fromString = ob5.fromString(mFUser.getUnitGroup().getDistance());
                    ee7.a((Object) fromString, "Unit.fromString(distance)");
                    if (this.this$0.g != fromString) {
                        this.this$0.g = fromString;
                        this.this$0.h.b(fromString);
                    }
                }
            }
            Boolean w = zd5.w(this.this$0.e);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActivityPresenter", "start isDateTodayDate " + w + " listingPage " + this.this$0.f);
            if (!w.booleanValue()) {
                this.this$0.e = new Date();
                Listing b = this.this$0.f;
                if (b != null) {
                    b.getRefresh();
                }
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public n86(l86 l86, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        ee7.b(l86, "mView");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(pj4, "mAppExecutors");
        this.h = l86;
        this.i = summariesRepository;
        this.j = fitnessDataRepository;
        this.k = userRepository;
        this.l = pj4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
    }

    @DexIgnore
    @Override // com.fossil.k86
    public void j() {
        vc7<i97> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.k86
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.k86
    public void i() {
        LiveData<qf<ActivitySummary>> pagedList;
        try {
            FLogger.INSTANCE.getLocal().d("DashboardActivityPresenter", "remove data source observer");
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                l86 l86 = this.h;
                if (l86 != null) {
                    pagedList.a((m86) l86);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
                }
            }
            this.i.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("DashboardActivityPresenter", sb.toString());
        }
    }
}
