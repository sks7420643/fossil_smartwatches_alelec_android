package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eh3 implements ServiceConnection {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ bh3 b;

    @DexIgnore
    public eh3(bh3 bh3, String str) {
        this.b = bh3;
        this.a = str;
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.a.e().w().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            xq2 a2 = zt2.a(iBinder);
            if (a2 == null) {
                this.b.a.e().w().a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.a.e().B().a("Install Referrer Service connected");
            this.b.a.c().a(new dh3(this, a2, this));
        } catch (Exception e) {
            this.b.a.e().w().a("Exception occurred while calling Install Referrer API", e);
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.a.e().B().a("Install Referrer Service disconnected");
    }
}
