package com.fossil;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx5 {
    @DexIgnore
    public /* final */ kx5 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public mx5(kx5 kx5, LoaderManager loaderManager) {
        ee7.b(kx5, "mView");
        ee7.b(loaderManager, "mLoaderManager");
        this.a = kx5;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final kx5 b() {
        return this.a;
    }
}
