package com.fossil;

import java.io.File;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rc7 extends qc7 {
    @DexIgnore
    public static final boolean c(File file) {
        ee7.b(file, "$this$deleteRecursively");
        Iterator it = qc7.b(file).iterator();
        while (true) {
            boolean z = true;
            while (true) {
                if (!it.hasNext()) {
                    return z;
                }
                File file2 = (File) it.next();
                if (file2.delete() || !file2.exists()) {
                    if (z) {
                    }
                }
                z = false;
            }
        }
    }

    @DexIgnore
    public static final String d(File file) {
        ee7.b(file, "$this$extension");
        String name = file.getName();
        ee7.a((Object) name, "name");
        return nh7.a(name, '.', "");
    }
}
