package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w06 extends dl4<v06> {
    @DexIgnore
    void G(String str);

    @DexIgnore
    void S(boolean z);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void a(CommuteTimeSetting commuteTimeSetting);

    @DexIgnore
    void b();

    @DexIgnore
    void c(String str, String str2);

    @DexIgnore
    void e(List<String> list);

    @DexIgnore
    void h(boolean z);

    @DexIgnore
    void r(String str);

    @DexIgnore
    void t(String str);
}
