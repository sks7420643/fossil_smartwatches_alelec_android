package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax3<F, T> extends jz3<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ cw3<F, ? extends T> function;
    @DexIgnore
    public /* final */ jz3<T> ordering;

    @DexIgnore
    public ax3(cw3<F, ? extends T> cw3, jz3<T> jz3) {
        jw3.a(cw3);
        this.function = cw3;
        jw3.a(jz3);
        this.ordering = jz3;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.jz3<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(F f, F f2) {
        return this.ordering.compare(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ax3)) {
            return false;
        }
        ax3 ax3 = (ax3) obj;
        if (!this.function.equals(ax3.function) || !this.ordering.equals(ax3.ordering)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return gw3.a(this.function, this.ordering);
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".onResultOf(" + this.function + ")";
    }
}
