package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ ti3 b;

    @DexIgnore
    public fj3(ti3 ti3, AtomicReference atomicReference) {
        this.b = ti3;
        this.a = atomicReference;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                this.a.set(Long.valueOf(this.b.l().a(this.b.p().A(), wb3.M)));
                this.a.notify();
            } catch (Throwable th) {
                this.a.notify();
                throw th;
            }
        }
    }
}
