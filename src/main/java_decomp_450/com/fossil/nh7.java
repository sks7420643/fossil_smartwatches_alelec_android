package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nh7 extends mh7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements kd7<CharSequence, Integer, r87<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ char[] $delimiters;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(char[] cArr, boolean z) {
            super(2);
            this.$delimiters = cArr;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final r87<Integer, Integer> invoke(CharSequence charSequence, int i) {
            ee7.b(charSequence, "$receiver");
            int a = nh7.a(charSequence, this.$delimiters, i, this.$ignoreCase);
            if (a < 0) {
                return null;
            }
            return w87.a(Integer.valueOf(a), 1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public /* bridge */ /* synthetic */ r87<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements kd7<CharSequence, Integer, r87<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $delimitersList;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(List list, boolean z) {
            super(2);
            this.$delimitersList = list;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final r87<Integer, Integer> invoke(CharSequence charSequence, int i) {
            ee7.b(charSequence, "$receiver");
            r87 a = nh7.b(charSequence, (Collection) this.$delimitersList, i, this.$ignoreCase, false);
            if (a != null) {
                return w87.a(a.getFirst(), Integer.valueOf(((String) a.getSecond()).length()));
            }
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public /* bridge */ /* synthetic */ r87<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<lf7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(lf7 lf7) {
            ee7.b(lf7, "it");
            return nh7.a(this.$this_splitToSequence, lf7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements gd7<lf7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(lf7 lf7) {
            ee7.b(lf7, "it");
            return nh7.a(this.$this_splitToSequence, lf7);
        }
    }

    @DexIgnore
    public static final lf7 b(CharSequence charSequence) {
        ee7.b(charSequence, "$this$indices");
        return new lf7(0, charSequence.length() - 1);
    }

    @DexIgnore
    public static final int c(CharSequence charSequence) {
        ee7.b(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    @DexIgnore
    public static final CharSequence d(CharSequence charSequence) {
        ee7.b(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z = false;
        while (i <= length) {
            boolean a2 = qg7.a(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!a2) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!a2) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    @DexIgnore
    public static final CharSequence a(CharSequence charSequence, int i, char c2) {
        ee7.b(charSequence, "$this$padStart");
        if (i < 0) {
            throw new IllegalArgumentException("Desired length " + i + " is less than zero.");
        } else if (i <= charSequence.length()) {
            return charSequence.subSequence(0, charSequence.length());
        } else {
            StringBuilder sb = new StringBuilder(i);
            int length = i - charSequence.length();
            int i2 = 1;
            if (1 <= length) {
                while (true) {
                    sb.append(c2);
                    if (i2 == length) {
                        break;
                    }
                    i2++;
                }
            }
            sb.append(charSequence);
            return sb;
        }
    }

    @DexIgnore
    public static /* synthetic */ String b(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return b(str, c2, str2);
    }

    @DexIgnore
    public static /* synthetic */ String c(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return c(str, c2, str2);
    }

    @DexIgnore
    public static final String b(String str, char c2, String str2) {
        ee7.b(str, "$this$substringBefore");
        ee7.b(str2, "missingDelimiterValue");
        int a2 = a((CharSequence) str, c2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str2;
        }
        String substring = str.substring(0, a2);
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String c(String str, char c2, String str2) {
        ee7.b(str, "$this$substringBeforeLast");
        ee7.b(str2, "missingDelimiterValue");
        int b2 = b((CharSequence) str, c2, 0, false, 6, (Object) null);
        if (b2 == -1) {
            return str2;
        }
        String substring = str.substring(0, b2);
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String b(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return b(str, str2, str3);
    }

    @DexIgnore
    public static /* synthetic */ String c(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return c(str, str2, str3);
    }

    @DexIgnore
    public static final String b(String str, String str2, String str3) {
        ee7.b(str, "$this$substringBefore");
        ee7.b(str2, "delimiter");
        ee7.b(str3, "missingDelimiterValue");
        int a2 = a((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str3;
        }
        String substring = str.substring(0, a2);
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String c(String str, String str2, String str3) {
        ee7.b(str, "$this$substringBeforeLast");
        ee7.b(str2, "delimiter");
        ee7.b(str3, "missingDelimiterValue");
        int b2 = b((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (b2 == -1) {
            return str3;
        }
        String substring = str.substring(0, b2);
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final String a(String str, int i, char c2) {
        ee7.b(str, "$this$padStart");
        return a((CharSequence) str, i, c2).toString();
    }

    @DexIgnore
    public static final boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        ee7.b(charSequence, "$this$endsWith");
        ee7.b(charSequence2, "suffix");
        if (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            return a(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z);
        }
        return mh7.a((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static final boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        ee7.b(charSequence, "$this$startsWith");
        ee7.b(charSequence2, "prefix");
        if (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            return a(charSequence, 0, charSequence2, 0, charSequence2.length(), z);
        }
        return mh7.c((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static final String a(CharSequence charSequence, lf7 lf7) {
        ee7.b(charSequence, "$this$substring");
        ee7.b(lf7, "range");
        return charSequence.subSequence(lf7.c().intValue(), lf7.b().intValue() + 1).toString();
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return a(str, str2, str3);
    }

    @DexIgnore
    public static final String a(String str, String str2, String str3) {
        ee7.b(str, "$this$substringAfter");
        ee7.b(str2, "delimiter");
        ee7.b(str3, "missingDelimiterValue");
        int a2 = a((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str3;
        }
        String substring = str.substring(a2 + str2.length(), str.length());
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, char[] cArr, int i, boolean z) {
        ee7.b(charSequence, "$this$lastIndexOfAny");
        ee7.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int b2 = qf7.b(i, c(charSequence)); b2 >= 0; b2--) {
                char charAt = charSequence.charAt(b2);
                int length = cArr.length;
                boolean z2 = false;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (rg7.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return b2;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(t97.a(cArr), i);
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return a(str, c2, str2);
    }

    @DexIgnore
    public static final String a(String str, char c2, String str2) {
        ee7.b(str, "$this$substringAfterLast");
        ee7.b(str2, "missingDelimiterValue");
        int b2 = b((CharSequence) str, c2, 0, false, 6, (Object) null);
        if (b2 == -1) {
            return str2;
        }
        String substring = str.substring(b2 + 1, str.length());
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String a(String str, CharSequence charSequence) {
        ee7.b(str, "$this$removePrefix");
        ee7.b(charSequence, "prefix");
        if (!c((CharSequence) str, charSequence, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length());
        ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @DexIgnore
    public static final String a(String str, CharSequence charSequence, CharSequence charSequence2) {
        ee7.b(str, "$this$removeSurrounding");
        ee7.b(charSequence, "prefix");
        ee7.b(charSequence2, "suffix");
        if (str.length() < charSequence.length() + charSequence2.length() || !c((CharSequence) str, charSequence, false, 2, (Object) null) || !b((CharSequence) str, charSequence2, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final r87<Integer, String> b(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        T t;
        T t2;
        if (z || collection.size() != 1) {
            jf7 lf7 = !z2 ? new lf7(qf7.a(i, 0), charSequence.length()) : qf7.c(qf7.b(i, c(charSequence)), 0);
            if (charSequence instanceof String) {
                int first = lf7.getFirst();
                int last = lf7.getLast();
                int a2 = lf7.a();
                if (a2 < 0 ? first >= last : first <= last) {
                    while (true) {
                        Iterator<T> it = collection.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it.next();
                            T t3 = t2;
                            if (mh7.a(t3, 0, (String) charSequence, first, t3.length(), z)) {
                                break;
                            }
                        }
                        T t4 = t2;
                        if (t4 == null) {
                            if (first == last) {
                                break;
                            }
                            first += a2;
                        } else {
                            return w87.a(Integer.valueOf(first), t4);
                        }
                    }
                }
            } else {
                int first2 = lf7.getFirst();
                int last2 = lf7.getLast();
                int a3 = lf7.a();
                if (a3 < 0 ? first2 >= last2 : first2 <= last2) {
                    while (true) {
                        Iterator<T> it2 = collection.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it2.next();
                            T t5 = t;
                            if (a(t5, 0, charSequence, first2, t5.length(), z)) {
                                break;
                            }
                        }
                        T t6 = t;
                        if (t6 == null) {
                            if (first2 == last2) {
                                break;
                            }
                            first2 += a3;
                        } else {
                            return w87.a(Integer.valueOf(first2), t6);
                        }
                    }
                }
            }
            return null;
        }
        String str = (String) ea7.j(collection);
        int a4 = !z2 ? a(charSequence, str, i, false, 4, (Object) null) : b(charSequence, str, i, false, 4, (Object) null);
        if (a4 < 0) {
            return null;
        }
        return w87.a(Integer.valueOf(a4), str);
    }

    @DexIgnore
    public static final boolean a(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        ee7.b(charSequence, "$this$regionMatchesImpl");
        ee7.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!rg7.a(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        ee7.b(charSequence, "$this$indexOfAny");
        ee7.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int a2 = qf7.a(i, 0);
            int c2 = c(charSequence);
            if (a2 > c2) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(a2);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (rg7.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return a2;
                }
                if (a2 == c2) {
                    return -1;
                }
                a2++;
            }
        } else {
            return ((String) charSequence).indexOf(t97.a(cArr), i);
        }
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        return a(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        jf7 jf7;
        if (!z2) {
            jf7 = new lf7(qf7.a(i, 0), qf7.b(i2, charSequence.length()));
        } else {
            jf7 = qf7.c(qf7.b(i, c(charSequence)), qf7.a(i2, 0));
        }
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int first = jf7.getFirst();
            int last = jf7.getLast();
            int a2 = jf7.a();
            if (a2 >= 0) {
                if (first > last) {
                    return -1;
                }
            } else if (first < last) {
                return -1;
            }
            while (!a(charSequence2, 0, charSequence, first, charSequence2.length(), z)) {
                if (first == last) {
                    return -1;
                }
                first += a2;
            }
            return first;
        }
        int first2 = jf7.getFirst();
        int last2 = jf7.getLast();
        int a3 = jf7.a();
        if (a3 >= 0) {
            if (first2 > last2) {
                return -1;
            }
        } else if (first2 < last2) {
            return -1;
        }
        while (!mh7.a((String) charSequence2, 0, (String) charSequence, first2, charSequence2.length(), z)) {
            if (first2 == last2) {
                return -1;
            }
            first2 += a3;
        }
        return first2;
    }

    @DexIgnore
    public static /* synthetic */ int b(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, c2, i, z);
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, char c2, int i, boolean z) {
        ee7.b(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c2, i);
        }
        return b(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int b(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, str, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, c2, i, z);
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, String str, int i, boolean z) {
        ee7.b(charSequence, "$this$lastIndexOf");
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        if (z || !(charSequence instanceof String)) {
            return a(charSequence, (CharSequence) str, i, 0, z, true);
        }
        return ((String) charSequence).lastIndexOf(str, i);
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, char c2, int i, boolean z) {
        ee7.b(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c2, i);
        }
        return a(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static /* synthetic */ hg7 b(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return b(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, str, i, z);
    }

    @DexIgnore
    public static final hg7<String> b(CharSequence charSequence, String[] strArr, boolean z, int i) {
        ee7.b(charSequence, "$this$splitToSequence");
        ee7.b(strArr, "delimiters");
        return og7.c(a(charSequence, strArr, 0, z, i, 2, (Object) null), new c(charSequence));
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, String str, int i, boolean z) {
        ee7.b(charSequence, "$this$indexOf");
        ee7.b(str, LegacyTokenHelper.TYPE_STRING);
        if (z || !(charSequence instanceof String)) {
            return a(charSequence, str, i, charSequence.length(), z, false, 16, null);
        }
        return ((String) charSequence).indexOf(str, i);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        ee7.b(charSequence, "$this$contains");
        ee7.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (charSequence2 instanceof String) {
            if (a(charSequence, (String) charSequence2, 0, z, 2, (Object) null) >= 0) {
                return true;
            }
        } else if (a(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, null) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static /* synthetic */ hg7 a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, cArr, i, z, i2);
    }

    @DexIgnore
    public static final hg7<lf7> a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new tg7(charSequence, i, i2, new a(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ hg7 a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, strArr, i, z, i2);
    }

    @DexIgnore
    public static final hg7<lf7> a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new tg7(charSequence, i, i2, new b(s97.b(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ List a(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final List<String> a(CharSequence charSequence, String[] strArr, boolean z, int i) {
        ee7.b(charSequence, "$this$split");
        ee7.b(strArr, "delimiters");
        boolean z2 = true;
        if (strArr.length == 1) {
            String str = strArr[0];
            if (str.length() != 0) {
                z2 = false;
            }
            if (!z2) {
                return a(charSequence, str, z, i);
            }
        }
        Iterable<lf7> b2 = og7.b(a(charSequence, strArr, 0, z, i, 2, (Object) null));
        ArrayList arrayList = new ArrayList(x97.a(b2, 10));
        for (lf7 lf7 : b2) {
            arrayList.add(a(charSequence, lf7));
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ hg7 a(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, cArr, z, i);
    }

    @DexIgnore
    public static final hg7<String> a(CharSequence charSequence, char[] cArr, boolean z, int i) {
        ee7.b(charSequence, "$this$splitToSequence");
        ee7.b(cArr, "delimiters");
        return og7.c(a(charSequence, cArr, 0, z, i, 2, (Object) null), new d(charSequence));
    }

    @DexIgnore
    public static final List<String> a(CharSequence charSequence, String str, boolean z, int i) {
        int i2 = 0;
        if (i >= 0) {
            int a2 = a(charSequence, str, 0, z);
            if (a2 == -1 || i == 1) {
                return v97.a(charSequence.toString());
            }
            boolean z2 = i > 0;
            int i3 = 10;
            if (z2) {
                i3 = qf7.b(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            do {
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                if (z2 && arrayList.size() == i - 1) {
                    break;
                }
                a2 = a(charSequence, str, i2, z);
            } while (a2 != -1);
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }
}
