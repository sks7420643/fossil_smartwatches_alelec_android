package com.fossil;

import java.io.Serializable;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> implements kw3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Collection<?> target;

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean apply(T t) {
            try {
                return this.target.contains(t);
            } catch (ClassCastException | NullPointerException unused) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.target.equals(((b) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.in(" + this.target + ")";
        }

        @DexIgnore
        public b(Collection<?> collection) {
            jw3.a(collection);
            this.target = collection;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> implements kw3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean apply(T t) {
            return this.target.equals(t);
        }

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean equals(Object obj) {
            if (obj instanceof c) {
                return this.target.equals(((c) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.equalTo(" + ((Object) this.target) + ")";
        }

        @DexIgnore
        public c(T t) {
            this.target = t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> implements kw3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ kw3<T> predicate;

        @DexIgnore
        public d(kw3<T> kw3) {
            jw3.a(kw3);
            this.predicate = kw3;
        }

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean apply(T t) {
            return !this.predicate.apply(t);
        }

        @DexIgnore
        @Override // com.fossil.kw3
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.predicate.equals(((d) obj).predicate);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return ~this.predicate.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.not(" + this.predicate + ")";
        }
    }

    @DexIgnore
    public enum e implements kw3<Object> {
        ALWAYS_TRUE {
            @DexIgnore
            @Override // com.fossil.kw3
            public boolean apply(Object obj) {
                return true;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        },
        ALWAYS_FALSE {
            @DexIgnore
            @Override // com.fossil.kw3
            public boolean apply(Object obj) {
                return false;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        },
        IS_NULL {
            @DexIgnore
            @Override // com.fossil.kw3
            public boolean apply(Object obj) {
                return obj == null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.isNull()";
            }
        },
        NOT_NULL {
            @DexIgnore
            @Override // com.fossil.kw3
            public boolean apply(Object obj) {
                return obj != null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.notNull()";
            }
        };

        @DexIgnore
        public <T> kw3<T> withNarrowedType() {
            return this;
        }
    }

    /*
    static {
        ew3.a(',');
    }
    */

    @DexIgnore
    public static <T> kw3<T> a() {
        return e.IS_NULL.withNarrowedType();
    }

    @DexIgnore
    public static <T> kw3<T> a(kw3<T> kw3) {
        return new d(kw3);
    }

    @DexIgnore
    public static <T> kw3<T> a(T t) {
        return t == null ? a() : new c(t);
    }

    @DexIgnore
    public static <T> kw3<T> a(Collection<? extends T> collection) {
        return new b(collection);
    }
}
