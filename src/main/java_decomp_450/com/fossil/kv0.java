package com.fossil;

import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv0 {
    @DexIgnore
    public /* final */ LinkedList<eo0> a; // = new LinkedList<>();
    @DexIgnore
    public eo0 b;
    @DexIgnore
    public /* final */ ri1 c;

    @DexIgnore
    public kv0(ri1 ri1) {
        this.c = ri1;
    }

    @DexIgnore
    public final void a(eo0 eo0) {
        eo0 poll;
        le0 le0 = le0.DEBUG;
        yz0.a(eo0.h);
        try {
            synchronized (this.a) {
                this.a.add(c(eo0), eo0);
                i97 i97 = i97.a;
            }
        } catch (IndexOutOfBoundsException unused) {
            this.a.add(eo0);
        }
        if (this.b == null && (poll = this.a.poll()) != null) {
            b(poll);
        }
    }

    @DexIgnore
    public final void b(eo0 eo0) {
        this.b = eo0;
        if (eo0 != null) {
            eo0.g = new ur0(this);
            ri1 ri1 = this.c;
            if (!eo0.c) {
                le0 le0 = le0.DEBUG;
                qy0<s91> c2 = eo0.c();
                if (c2 != null) {
                    c2.a(eo0);
                }
                eo0.a(ri1);
            }
        }
    }

    @DexIgnore
    public final int c(eo0 eo0) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (eo0.b().compareTo((Enum) this.a.get(i).b()) > 0) {
                    return i;
                }
            }
            return this.a.size();
        }
    }

    @DexIgnore
    public final void a(oi0 oi0) {
        LinkedList linkedList = new LinkedList(this.a);
        this.a.clear();
        eo0 eo0 = this.b;
        if (eo0 != null) {
            eo0.a(oi0);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            ((eo0) it.next()).a(oi0);
        }
    }
}
