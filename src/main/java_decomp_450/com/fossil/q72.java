package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q72 extends m72 {
    @DexIgnore
    public /* final */ s12<Status> a;

    @DexIgnore
    public q72(s12<Status> s12) {
        this.a = s12;
    }

    @DexIgnore
    @Override // com.fossil.u72
    public final void d(int i) throws RemoteException {
        this.a.a(new Status(i));
    }
}
