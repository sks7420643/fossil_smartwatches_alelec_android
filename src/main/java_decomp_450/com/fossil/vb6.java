package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb6 implements Factory<ub6> {
    @DexIgnore
    public static ub6 a(sb6 sb6, UserRepository userRepository, ch5 ch5, GoalTrackingRepository goalTrackingRepository) {
        return new ub6(sb6, userRepository, ch5, goalTrackingRepository);
    }
}
