package com.fossil;

import android.os.Bundle;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ie6 extends cl4 {
    @DexIgnore
    public abstract void a(Bundle bundle);

    @DexIgnore
    public abstract void a(Date date);

    @DexIgnore
    public abstract void b(Date date);

    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE h();

    @DexIgnore
    public abstract ob5 i();

    @DexIgnore
    public abstract rd j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
