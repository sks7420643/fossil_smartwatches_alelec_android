package com.fossil;

import androidx.renderscript.RenderScript;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sh extends ih {
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public jh k;

    @DexIgnore
    public enum b {
        POSITIVE_X(0),
        NEGATIVE_X(1),
        POSITIVE_Y(2),
        NEGATIVE_Y(3),
        POSITIVE_Z(4),
        NEGATIVE_Z(5);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    @DexIgnore
    public sh(long j2, RenderScript renderScript) {
        super(j2, renderScript);
    }

    @DexIgnore
    public long a(RenderScript renderScript, long j2) {
        return renderScript.a(j2, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public void e() {
        boolean l = l();
        int h2 = h();
        int i2 = i();
        int j2 = j();
        int i3 = k() ? 6 : 1;
        if (h2 == 0) {
            h2 = 1;
        }
        if (i2 == 0) {
            i2 = 1;
        }
        if (j2 == 0) {
            j2 = 1;
        }
        int i4 = h2 * i2 * j2 * i3;
        while (l && (h2 > 1 || i2 > 1 || j2 > 1)) {
            if (h2 > 1) {
                h2 >>= 1;
            }
            if (i2 > 1) {
                i2 >>= 1;
            }
            if (j2 > 1) {
                j2 >>= 1;
            }
            i4 += h2 * i2 * j2 * i3;
        }
        this.j = i4;
    }

    @DexIgnore
    public int f() {
        return this.j;
    }

    @DexIgnore
    public jh g() {
        return this.k;
    }

    @DexIgnore
    public int h() {
        return this.d;
    }

    @DexIgnore
    public int i() {
        return this.e;
    }

    @DexIgnore
    public int j() {
        return this.f;
    }

    @DexIgnore
    public boolean k() {
        return this.h;
    }

    @DexIgnore
    public boolean l() {
        return this.g;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public RenderScript a;
        @DexIgnore
        public int b; // = 1;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public int g;
        @DexIgnore
        public jh h;

        @DexIgnore
        public a(RenderScript renderScript, jh jhVar) {
            jhVar.a();
            this.a = renderScript;
            this.h = jhVar;
        }

        @DexIgnore
        public a a(int i) {
            if (i >= 1) {
                this.b = i;
                return this;
            }
            throw new mh("Values of less than 1 for Dimension X are not valid.");
        }

        @DexIgnore
        public a b(int i) {
            if (i >= 1) {
                this.c = i;
                return this;
            }
            throw new mh("Values of less than 1 for Dimension Y are not valid.");
        }

        @DexIgnore
        public a a(boolean z) {
            this.e = z;
            return this;
        }

        @DexIgnore
        public sh a() {
            if (this.d > 0) {
                if (this.b < 1 || this.c < 1) {
                    throw new nh("Both X and Y dimension required when Z is present.");
                } else if (this.f) {
                    throw new nh("Cube maps not supported with 3D types.");
                }
            }
            if (this.c > 0 && this.b < 1) {
                throw new nh("X dimension required when Y is present.");
            } else if (this.f && this.c < 1) {
                throw new nh("Cube maps require 2D Types.");
            } else if (this.g == 0 || (this.d == 0 && !this.f && !this.e)) {
                RenderScript renderScript = this.a;
                sh shVar = new sh(renderScript.b(this.h.a(renderScript), this.b, this.c, this.d, this.e, this.f, this.g), this.a);
                shVar.k = this.h;
                shVar.d = this.b;
                shVar.e = this.c;
                shVar.f = this.d;
                shVar.g = this.e;
                shVar.h = this.f;
                shVar.i = this.g;
                shVar.e();
                return shVar;
            } else {
                throw new nh("YUV only supports basic 2D.");
            }
        }
    }
}
