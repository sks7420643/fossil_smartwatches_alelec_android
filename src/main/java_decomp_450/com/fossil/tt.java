package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tt<T extends View> extends st {
    @DexIgnore
    public static final a a = a.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ a a; // = new a();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tt$a$a")
        /* renamed from: com.fossil.tt$a$a  reason: collision with other inner class name */
        public static final class C0188a implements tt<T> {
            @DexIgnore
            public /* final */ T b;
            @DexIgnore
            public /* final */ boolean c;
            @DexIgnore
            public /* final */ /* synthetic */ View d;

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: android.view.View */
            /* JADX WARN: Multi-variable type inference failed */
            public C0188a(View view, boolean z) {
                this.d = view;
                this.b = view;
                this.c = z;
            }

            @DexIgnore
            @Override // com.fossil.st
            public Object a(fb7<? super rt> fb7) {
                return b.a(this, fb7);
            }

            @DexIgnore
            /* JADX WARN: Type inference failed for: r0v0, types: [T, T extends android.view.View] */
            @Override // com.fossil.tt
            public T getView() {
                return this.b;
            }

            @DexIgnore
            @Override // com.fossil.tt
            public boolean a() {
                return this.c;
            }
        }

        @DexIgnore
        public static /* synthetic */ tt a(a aVar, View view, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = true;
            }
            return aVar.a(view, z);
        }

        @DexIgnore
        public final <T extends View> tt<T> a(T t, boolean z) {
            ee7.b(t, "view");
            return new C0188a(t, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public boolean a;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver b;
            @DexIgnore
            public /* final */ /* synthetic */ ai7 c;
            @DexIgnore
            public /* final */ /* synthetic */ tt d;

            @DexIgnore
            public a(ViewTreeObserver viewTreeObserver, ai7 ai7, tt ttVar) {
                this.b = viewTreeObserver;
                this.c = ai7;
                this.d = ttVar;
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (!this.a) {
                    this.a = true;
                    tt ttVar = this.d;
                    ViewTreeObserver viewTreeObserver = this.b;
                    ee7.a((Object) viewTreeObserver, "viewTreeObserver");
                    b.b(ttVar, viewTreeObserver, this);
                    ot otVar = new ot(qf7.a(b.d(this.d, false), 1), qf7.a(b.c(this.d, false), 1));
                    ai7 ai7 = this.c;
                    s87.a aVar = s87.Companion;
                    ai7.resumeWith(s87.m60constructorimpl(otVar));
                }
                return true;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tt$b$b")
        /* renamed from: com.fossil.tt$b$b  reason: collision with other inner class name */
        public static final class C0189b extends fe7 implements gd7<Throwable, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ a $preDrawListener;
            @DexIgnore
            public /* final */ /* synthetic */ ViewTreeObserver $viewTreeObserver;
            @DexIgnore
            public /* final */ /* synthetic */ tt this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0189b(ViewTreeObserver viewTreeObserver, a aVar, tt ttVar) {
                super(1);
                this.$viewTreeObserver = viewTreeObserver;
                this.$preDrawListener = aVar;
                this.this$0 = ttVar;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
                invoke(th);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                tt ttVar = this.this$0;
                ViewTreeObserver viewTreeObserver = this.$viewTreeObserver;
                ee7.a((Object) viewTreeObserver, "viewTreeObserver");
                b.b(ttVar, viewTreeObserver, this.$preDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> int c(tt<T> ttVar, boolean z) {
            ViewGroup.LayoutParams layoutParams = ttVar.getView().getLayoutParams();
            return a(ttVar, layoutParams != null ? layoutParams.height : -1, ttVar.getView().getHeight(), ttVar.a() ? ttVar.getView().getPaddingTop() + ttVar.getView().getPaddingBottom() : 0, z);
        }

        @DexIgnore
        public static <T extends View> int d(tt<T> ttVar, boolean z) {
            ViewGroup.LayoutParams layoutParams = ttVar.getView().getLayoutParams();
            return a(ttVar, layoutParams != null ? layoutParams.width : -1, ttVar.getView().getWidth(), ttVar.a() ? ttVar.getView().getPaddingLeft() + ttVar.getView().getPaddingRight() : 0, z);
        }

        @DexIgnore
        public static <T extends View> void b(tt<T> ttVar, ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnPreDrawListener onPreDrawListener) {
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(onPreDrawListener);
            } else {
                ttVar.getView().getViewTreeObserver().removeOnPreDrawListener(onPreDrawListener);
            }
        }

        @DexIgnore
        public static <T extends View> Object a(tt<T> ttVar, fb7<? super rt> fb7) {
            boolean isLayoutRequested = ttVar.getView().isLayoutRequested();
            int d = d(ttVar, isLayoutRequested);
            int c = c(ttVar, isLayoutRequested);
            if (d > 0 && c > 0) {
                return new ot(d, c);
            }
            bi7 bi7 = new bi7(mb7.a(fb7), 1);
            ViewTreeObserver viewTreeObserver = ttVar.getView().getViewTreeObserver();
            a aVar = new a(viewTreeObserver, bi7, ttVar);
            viewTreeObserver.addOnPreDrawListener(aVar);
            bi7.a((gd7<? super Throwable, i97>) new C0189b(viewTreeObserver, aVar, ttVar));
            Object g = bi7.g();
            if (g == nb7.a()) {
                vb7.c(fb7);
            }
            return g;
        }

        @DexIgnore
        public static <T extends View> int a(tt<T> ttVar, int i, int i2, int i3, boolean z) {
            int i4 = i - i3;
            if (i4 > 0) {
                return i4;
            }
            int i5 = i2 - i3;
            if (i5 > 0) {
                return i5;
            }
            if (z || i != -2) {
                return -1;
            }
            if (cu.c.a() && cu.c.b() <= 4) {
                Log.println(4, "ViewSizeResolver", "A View's width and/or height is set to WRAP_CONTENT. Falling back to the size of the display.");
            }
            Context context = ttVar.getView().getContext();
            ee7.a((Object) context, "view.context");
            Resources resources = context.getResources();
            ee7.a((Object) resources, "view.context.resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            return Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    T getView();
}
