package com.fossil;

import com.fossil.m60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class jo1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[m60.b.values().length];
        a = iArr;
        iArr[m60.b.SERIAL_NUMBER.ordinal()] = 1;
        a[m60.b.HARDWARE_REVISION.ordinal()] = 2;
        a[m60.b.FIRMWARE_VERSION.ordinal()] = 3;
        a[m60.b.MODEL_NUMBER.ordinal()] = 4;
        a[m60.b.HEART_RATE_SERIAL_NUMBER.ordinal()] = 5;
        a[m60.b.BOOTLOADER_VERSION.ordinal()] = 6;
        a[m60.b.WATCH_APP_VERSION.ordinal()] = 7;
        a[m60.b.FONT_VERSION.ordinal()] = 8;
        a[m60.b.LUTS_VERSION.ordinal()] = 9;
        a[m60.b.SUPPORTED_FILES_VERSION.ordinal()] = 10;
        a[m60.b.CURRENT_FILES_VERSION.ordinal()] = 11;
        a[m60.b.BOND_REQUIREMENT.ordinal()] = 12;
        a[m60.b.SUPPORTED_DEVICE_CONFIGS.ordinal()] = 13;
        a[m60.b.DEVICE_SECURITY_VERSION.ordinal()] = 14;
        a[m60.b.SOCKET_INFO.ordinal()] = 15;
        a[m60.b.LOCALE.ordinal()] = 16;
        a[m60.b.LOCALE_VERSION.ordinal()] = 17;
        a[m60.b.MICRO_APP_SYSTEM_VERSION.ordinal()] = 18;
        a[m60.b.UNKNOWN.ordinal()] = 19;
    }
    */
}
