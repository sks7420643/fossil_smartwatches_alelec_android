package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public mo4(String str, String str2, String str3, String str4, String str5, boolean z) {
        ee7.b(str, "id");
        ee7.b(str2, "socialId");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = z;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof mo4)) {
            return false;
        }
        mo4 mo4 = (mo4) obj;
        return ee7.a(this.a, mo4.a) && ee7.a(this.b, mo4.b) && ee7.a(this.c, mo4.c) && ee7.a(this.d, mo4.d) && ee7.a(this.e, mo4.e) && this.f == mo4.f;
    }

    @DexIgnore
    public final boolean f() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        int i2 = (hashCode4 + i) * 31;
        boolean z = this.f;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public String toString() {
        return "SuggestedFriend(id=" + this.a + ", socialId=" + this.b + ", first=" + this.c + ", last=" + this.d + ", picture=" + this.e + ", isChecked=" + this.f + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ mo4(String str, String str2, String str3, String str4, String str5, boolean z, int i, zd7 zd7) {
        this(str, str2, str3, str4, str5, (i & 32) != 0 ? false : z);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.f = z;
    }
}
