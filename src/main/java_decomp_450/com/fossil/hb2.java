package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.za2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb2 implements za2.a {
    @DexIgnore
    public /* final */ /* synthetic */ FrameLayout a;
    @DexIgnore
    public /* final */ /* synthetic */ LayoutInflater b;
    @DexIgnore
    public /* final */ /* synthetic */ ViewGroup c;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ za2 e;

    @DexIgnore
    public hb2(za2 za2, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.e = za2;
        this.a = frameLayout;
        this.b = layoutInflater;
        this.c = viewGroup;
        this.d = bundle;
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final void a(bb2 bb2) {
        this.a.removeAllViews();
        this.a.addView(this.e.a.a(this.b, this.c, this.d));
    }

    @DexIgnore
    @Override // com.fossil.za2.a
    public final int getState() {
        return 2;
    }
}
