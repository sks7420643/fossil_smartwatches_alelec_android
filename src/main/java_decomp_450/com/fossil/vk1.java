package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk1 {
    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x04d4  */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x06c2  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x06cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.km1 r29, com.fossil.xp0 r30) {
        /*
            r28 = this;
            r0 = r29
            r1 = r30
            java.lang.String r2 = "total_duration"
            java.lang.String r3 = "pause_run_seq"
            java.lang.String r4 = "type"
            java.lang.String r5 = "state"
            com.fossil.ru0 r6 = r1.a
            int[] r7 = com.fossil.w91.d
            int r6 = r6.ordinal()
            r6 = r7[r6]
            r7 = 2
            r8 = 1
            switch(r6) {
                case 1: goto L_0x0113;
                case 2: goto L_0x010e;
                case 3: goto L_0x0106;
                case 4: goto L_0x001b;
                case 5: goto L_0x00ef;
                case 6: goto L_0x00da;
                case 7: goto L_0x00c2;
                case 8: goto L_0x0061;
                case 9: goto L_0x005c;
                case 10: goto L_0x004e;
                case 11: goto L_0x003d;
                case 12: goto L_0x001d;
                default: goto L_0x001b;
            }
        L_0x001b:
            goto L_0x06e0
        L_0x001d:
            r2 = r1
            com.fossil.t71 r2 = (com.fossil.t71) r2
            com.fossil.lb0 r11 = new com.fossil.lb0
            byte r4 = r1.b
            com.fossil.wf0 r5 = r2.d
            com.fossil.vf0 r6 = r2.e
            com.fossil.yf0 r7 = r2.f
            byte[] r8 = r2.g
            int r9 = r2.h
            int r10 = r2.i
            r3 = r11
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r11)
            goto L_0x06e0
        L_0x003d:
            r2 = r1
            com.fossil.l01 r2 = (com.fossil.l01) r2
            com.fossil.ie0 r2 = r29.a(r30)
            com.fossil.xi1 r3 = new com.fossil.xi1
            r3.<init>(r0, r1)
            r2.c(r3)
            goto L_0x06e0
        L_0x004e:
            com.fossil.ie0 r2 = r29.a(r30)
            com.fossil.ah1 r3 = new com.fossil.ah1
            r3.<init>(r0, r1)
            r2.c(r3)
            goto L_0x06e0
        L_0x005c:
            r29.a(r30)
            goto L_0x06e0
        L_0x0061:
            r2 = r1
            com.fossil.pi1 r2 = (com.fossil.pi1) r2
            com.fossil.rg0 r3 = r2.d
            com.fossil.pg0 r3 = r3.getMicroAppId()
            int[] r4 = com.fossil.w91.c
            int r3 = r3.ordinal()
            r3 = r4[r3]
            if (r3 == r8) goto L_0x008a
            if (r3 == r7) goto L_0x0078
            goto L_0x06e0
        L_0x0078:
            com.fossil.cc0 r3 = new com.fossil.cc0
            byte r1 = r1.b
            com.fossil.rg0 r2 = r2.d
            r3.<init>(r1, r2)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r3)
            goto L_0x06e0
        L_0x008a:
            com.fossil.rg0 r3 = r2.d
            com.fossil.qg0 r3 = r3.getVariant()
            int[] r4 = com.fossil.w91.b
            int r3 = r3.ordinal()
            r3 = r4[r3]
            if (r3 == r8) goto L_0x00b0
            if (r3 == r7) goto L_0x009e
            goto L_0x06e0
        L_0x009e:
            com.fossil.wb0 r3 = new com.fossil.wb0
            byte r1 = r1.b
            com.fossil.rg0 r2 = r2.d
            r3.<init>(r1, r2)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r3)
            goto L_0x06e0
        L_0x00b0:
            com.fossil.vb0 r3 = new com.fossil.vb0
            byte r1 = r1.b
            com.fossil.rg0 r2 = r2.d
            r3.<init>(r1, r2)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r3)
            goto L_0x06e0
        L_0x00c2:
            boolean r2 = r29.z()
            if (r2 != 0) goto L_0x00c9
            return
        L_0x00c9:
            r2 = r1
            com.fossil.bx0 r2 = (com.fossil.bx0) r2
            com.fossil.ie0 r2 = r0.a(r2)
            com.fossil.kd1 r3 = new com.fossil.kd1
            r3.<init>(r1, r0)
            r2.c(r3)
            goto L_0x06e0
        L_0x00da:
            r2 = r1
            com.fossil.lm1 r2 = (com.fossil.lm1) r2
            com.fossil.mb0 r3 = new com.fossil.mb0
            byte r1 = r1.b
            com.fossil.k90 r2 = r2.d
            r3.<init>(r1, r2)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r3)
            goto L_0x06e0
        L_0x00ef:
            r2 = r1
            com.fossil.ik0 r2 = (com.fossil.ik0) r2
            com.fossil.fb0 r3 = new com.fossil.fb0
            byte r1 = r1.b
            int r4 = r2.e
            com.fossil.q90 r2 = r2.d
            r3.<init>(r1, r4, r2)
            com.fossil.l60$b r1 = r0.v
            if (r1 == 0) goto L_0x06e0
            r1.onEventReceived(r0, r3)
            goto L_0x06e0
        L_0x0106:
            r0.b(r8)
            r29.a(r30)
            goto L_0x06e0
        L_0x010e:
            r29.a(r30)
            goto L_0x06e0
        L_0x0113:
            com.fossil.we1 r1 = (com.fossil.we1) r1
            com.fossil.t11 r6 = com.fossil.t11.a
            r1.a(r7)
            byte r6 = r1.b
            int r15 = r1.d
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            org.json.JSONObject r9 = r1.e
            java.util.Iterator r14 = r9.keys()
            java.lang.String r9 = "jsonRequestEvent.requestedApps.keys()"
            com.fossil.ee7.a(r14, r9)
        L_0x012e:
            boolean r9 = r14.hasNext()
            java.lang.String r10 = "null cannot be cast to non-null type kotlin.Array<T>"
            java.lang.String r11 = ""
            if (r9 == 0) goto L_0x04ec
            java.lang.Object r9 = r14.next()
            java.lang.String r9 = (java.lang.String) r9
            org.json.JSONObject r7 = r1.e
            org.json.JSONObject r7 = r7.optJSONObject(r9)
            if (r7 == 0) goto L_0x04e5
            if (r9 != 0) goto L_0x0154
        L_0x0148:
            r21 = r1
            r18 = r2
            r8 = r13
            r19 = r14
            r0 = r15
        L_0x0150:
            r2 = 0
        L_0x0151:
            r13 = 2
            goto L_0x04d1
        L_0x0154:
            int r16 = r9.hashCode()
            java.lang.String r12 = "action"
            switch(r16) {
                case -1119206702: goto L_0x04ab;
                case -755744823: goto L_0x045e;
                case -186117117: goto L_0x0445;
                case -94314940: goto L_0x02dc;
                case 65217736: goto L_0x02a5;
                case 126482114: goto L_0x0287;
                case 427103396: goto L_0x021d;
                case 970116257: goto L_0x0175;
                case 1255636834: goto L_0x015e;
                default: goto L_0x015d;
            }
        L_0x015d:
            goto L_0x0148
        L_0x015e:
            java.lang.String r7 = "weatherApp._.config.locations"
            boolean r7 = r9.equals(r7)
            if (r7 == 0) goto L_0x0148
            com.fossil.gc0 r12 = new com.fossil.gc0
            r12.<init>(r6, r15)
            r21 = r1
            r18 = r2
            r8 = r13
            r19 = r14
            r0 = r15
            goto L_0x045b
        L_0x0175:
            java.lang.String r12 = "master._.config.app_status"
            boolean r9 = r9.equals(r12)
            if (r9 == 0) goto L_0x0148
            java.util.EnumMap r9 = new java.util.EnumMap
            java.lang.Class<com.fossil.cg0> r12 = com.fossil.cg0.class
            r9.<init>(r12)
            com.fossil.cg0[] r12 = com.fossil.cg0.values()
            int r8 = r12.length
            r21 = r1
            r1 = 0
        L_0x018c:
            if (r1 >= r8) goto L_0x0208
            r16 = r8
            r8 = r12[r1]
            r19 = r12
            java.lang.String r12 = r8.a()
            org.json.JSONArray r12 = r7.optJSONArray(r12)
            if (r12 == 0) goto L_0x01f3
            r22 = r13
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            r23 = r14
            int r14 = r12.length()
            r0 = 0
        L_0x01ac:
            if (r0 >= r14) goto L_0x01d3
            r24 = r14
            com.fossil.pa0$a r14 = com.fossil.pa0.c
            r25 = r2
            java.lang.String r2 = r12.optString(r0, r11)
            r26 = r12
            java.lang.String r12 = "watchAppArray.optString(i, \"\")"
            com.fossil.ee7.a(r2, r12)
            com.fossil.pa0 r2 = r14.a(r2)
            com.fossil.pa0 r12 = com.fossil.pa0.EMPTY
            if (r2 == r12) goto L_0x01ca
            r13.add(r2)
        L_0x01ca:
            int r0 = r0 + 1
            r14 = r24
            r2 = r25
            r12 = r26
            goto L_0x01ac
        L_0x01d3:
            r25 = r2
            boolean r0 = r13.isEmpty()
            r2 = 1
            r0 = r0 ^ r2
            if (r0 == 0) goto L_0x01f0
            r0 = 0
            com.fossil.pa0[] r2 = new com.fossil.pa0[r0]
            java.lang.Object[] r0 = r13.toArray(r2)
            if (r0 == 0) goto L_0x01ea
            r9.put(r8, r0)
            goto L_0x01f0
        L_0x01ea:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r10)
            throw r0
        L_0x01f0:
            com.fossil.i97 r0 = com.fossil.i97.a
            goto L_0x01f9
        L_0x01f3:
            r25 = r2
            r22 = r13
            r23 = r14
        L_0x01f9:
            int r1 = r1 + 1
            r0 = r29
            r8 = r16
            r12 = r19
            r13 = r22
            r14 = r23
            r2 = r25
            goto L_0x018c
        L_0x0208:
            r25 = r2
            r22 = r13
            r23 = r14
            boolean r0 = r9.isEmpty()
            r1 = 1
            r0 = r0 ^ r1
            if (r0 == 0) goto L_0x043c
            com.fossil.qb0 r12 = new com.fossil.qb0
            r12.<init>(r6, r15, r9)
            goto L_0x029c
        L_0x021d:
            r21 = r1
            r25 = r2
            r22 = r13
            r23 = r14
            java.lang.String r0 = "buddyChallengeApp"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x043c
            com.fossil.sw0 r0 = com.fossil.ky0.f     // Catch:{ JSONException -> 0x027e }
            com.fossil.r51 r1 = com.fossil.r51.d     // Catch:{ JSONException -> 0x027e }
            java.lang.String r1 = com.fossil.yz0.a(r1)     // Catch:{ JSONException -> 0x027e }
            java.lang.String r1 = r7.optString(r1, r11)     // Catch:{ JSONException -> 0x027e }
            java.lang.String r2 = "dataJson.optString(JSONKey.TYPE.lowerCaseName, \"\")"
            com.fossil.ee7.a(r1, r2)     // Catch:{ JSONException -> 0x027e }
            com.fossil.ky0 r0 = r0.a(r1)     // Catch:{ JSONException -> 0x027e }
            if (r0 == 0) goto L_0x043c
            int[] r1 = com.fossil.w91.f     // Catch:{ JSONException -> 0x027e }
            int r0 = r0.ordinal()     // Catch:{ JSONException -> 0x027e }
            r0 = r1[r0]     // Catch:{ JSONException -> 0x027e }
            r1 = 1
            if (r0 == r1) goto L_0x0268
            r1 = 2
            if (r0 == r1) goto L_0x0261
            r1 = 3
            if (r0 != r1) goto L_0x025b
            com.fossil.tb0 r0 = new com.fossil.tb0     // Catch:{ JSONException -> 0x027e }
            r0.<init>(r6, r15)     // Catch:{ JSONException -> 0x027e }
            goto L_0x0266
        L_0x025b:
            com.fossil.p87 r0 = new com.fossil.p87     // Catch:{ JSONException -> 0x027e }
            r0.<init>()     // Catch:{ JSONException -> 0x027e }
            throw r0     // Catch:{ JSONException -> 0x027e }
        L_0x0261:
            com.fossil.sb0 r0 = new com.fossil.sb0     // Catch:{ JSONException -> 0x027e }
            r0.<init>(r6, r15)     // Catch:{ JSONException -> 0x027e }
        L_0x0266:
            r12 = r0
            goto L_0x029c
        L_0x0268:
            com.fossil.r51 r0 = com.fossil.r51.D4     // Catch:{ JSONException -> 0x027e }
            java.lang.String r0 = com.fossil.yz0.a(r0)     // Catch:{ JSONException -> 0x027e }
            java.lang.String r0 = r7.getString(r0)     // Catch:{ JSONException -> 0x027e }
            java.lang.String r1 = "dataJson\n               \u2026ALLENGE_ID.lowerCaseName)"
            com.fossil.ee7.a(r0, r1)     // Catch:{ JSONException -> 0x027e }
            com.fossil.rb0 r1 = new com.fossil.rb0     // Catch:{ JSONException -> 0x027e }
            r1.<init>(r6, r15, r0)     // Catch:{ JSONException -> 0x027e }
            r12 = r1
            goto L_0x029c
        L_0x027e:
            r0 = r15
            r8 = r22
            r19 = r23
            r18 = r25
            goto L_0x0150
        L_0x0287:
            r21 = r1
            r25 = r2
            r22 = r13
            r23 = r14
            java.lang.String r0 = "weatherInfo"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x043c
            com.fossil.fc0 r12 = new com.fossil.fc0
            r12.<init>(r6, r15)
        L_0x029c:
            r0 = r15
            r8 = r22
            r19 = r23
        L_0x02a1:
            r18 = r25
            goto L_0x045b
        L_0x02a5:
            r21 = r1
            r25 = r2
            r22 = r13
            r23 = r14
            java.lang.String r0 = "iftttApp._.config.buttons"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x043c
            com.fossil.zg0$a r0 = com.fossil.zg0.b
            java.lang.String r1 = "button_evt"
            java.lang.String r1 = r7.optString(r1)
            java.lang.String r2 = "dataJson.optString(\"button_evt\")"
            com.fossil.ee7.a(r1, r2)
            com.fossil.zg0 r0 = r0.a(r1)
            if (r0 == 0) goto L_0x02da
            java.lang.String r1 = "alias"
            java.lang.String r2 = "Unknown"
            java.lang.String r2 = r7.optString(r1, r2)
            com.fossil.zb0 r7 = new com.fossil.zb0
            com.fossil.ee7.a(r2, r1)
            r7.<init>(r6, r15, r0, r2)
            r12 = r7
            goto L_0x029c
        L_0x02da:
            r12 = 0
            goto L_0x029c
        L_0x02dc:
            r21 = r1
            r25 = r2
            r22 = r13
            r23 = r14
            java.lang.String r0 = "workoutApp"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x043c
            boolean r0 = r7.has(r5)
            java.lang.String r1 = "session_id"
            if (r0 == 0) goto L_0x039d
            boolean r0 = r7.has(r1)     // Catch:{ JSONException -> 0x0395 }
            if (r0 == 0) goto L_0x039d
            java.lang.String r0 = r7.optString(r5, r11)     // Catch:{ JSONException -> 0x038d }
            java.lang.String r2 = "dataJson.optString(UIScriptConstant.STATE, \"\")"
            com.fossil.ee7.a(r0, r2)     // Catch:{ JSONException -> 0x038d }
            com.fossil.fitness.WorkoutState r0 = com.fossil.yz0.b(r0)     // Catch:{ JSONException -> 0x038d }
            if (r0 == 0) goto L_0x0386
            java.lang.String r2 = "activity"
            r8 = 0
            int r2 = r7.optInt(r2, r8)     // Catch:{ JSONException -> 0x038d }
            com.fossil.fitness.WorkoutType r12 = com.fossil.yz0.c(r2)     // Catch:{ JSONException -> 0x038d }
            if (r12 == 0) goto L_0x037e
            boolean r2 = r7.has(r1)     // Catch:{ JSONException -> 0x038d }
            if (r2 == 0) goto L_0x037e
            long r13 = r7.optLong(r1)     // Catch:{ JSONException -> 0x038d }
            java.lang.String r1 = "gps"
            java.lang.String r2 = "off"
            java.lang.String r1 = r7.optString(r1, r2)     // Catch:{ JSONException -> 0x038d }
            java.lang.String r2 = "on"
            boolean r1 = com.fossil.ee7.a(r1, r2)     // Catch:{ JSONException -> 0x038d }
            int[] r2 = com.fossil.w91.g     // Catch:{ JSONException -> 0x038d }
            int r0 = r0.ordinal()     // Catch:{ JSONException -> 0x038d }
            r0 = r2[r0]     // Catch:{ JSONException -> 0x038d }
            r2 = 1
            if (r0 == r2) goto L_0x0368
            r2 = 2
            if (r0 == r2) goto L_0x0358
            r2 = 3
            if (r0 == r2) goto L_0x0351
            r1 = 4
            if (r0 == r1) goto L_0x034a
            r0 = r15
            r8 = r22
            r19 = r23
            r2 = 0
            goto L_0x03ec
        L_0x034a:
            com.fossil.pb0 r0 = new com.fossil.pb0
            r0.<init>(r6, r13, r15)
            goto L_0x0266
        L_0x0351:
            com.fossil.ob0 r0 = new com.fossil.ob0
            r0.<init>(r6, r13, r15)
            goto L_0x0266
        L_0x0358:
            com.fossil.bc0 r0 = new com.fossil.bc0
            r9 = r0
            r10 = r6
            r11 = r15
            r2 = 0
            r8 = r22
            r19 = r23
            r7 = r15
            r15 = r1
            r9.<init>(r10, r11, r12, r13, r15)     // Catch:{ JSONException -> 0x037b }
            goto L_0x0377
        L_0x0368:
            r7 = r15
            r8 = r22
            r19 = r23
            r2 = 0
            com.fossil.ec0 r0 = new com.fossil.ec0     // Catch:{ JSONException -> 0x037b }
            r9 = r0
            r10 = r6
            r11 = r7
            r15 = r1
            r9.<init>(r10, r11, r12, r13, r15)     // Catch:{ JSONException -> 0x037b }
        L_0x0377:
            r12 = r0
            r0 = r7
            goto L_0x02a1
        L_0x037b:
            r0 = r7
            goto L_0x0438
        L_0x037e:
            r8 = r22
            r19 = r23
            r2 = 0
            r0 = r15
            goto L_0x03ec
        L_0x0386:
            r8 = r22
            r19 = r23
            r2 = 0
            r0 = r15
            goto L_0x03ec
        L_0x038d:
            r8 = r22
            r19 = r23
            r2 = 0
            r0 = r15
            goto L_0x0438
        L_0x0395:
            r0 = r15
            r8 = r22
            r19 = r23
            r2 = 0
            goto L_0x0438
        L_0x039d:
            r0 = r15
            r8 = r22
            r19 = r23
            r2 = 0
            boolean r9 = r7.has(r4)     // Catch:{ JSONException -> 0x0438 }
            if (r9 == 0) goto L_0x03ef
            boolean r9 = r7.has(r1)     // Catch:{ JSONException -> 0x0438 }
            if (r9 == 0) goto L_0x03ef
            com.fossil.b01 r9 = com.fossil.v11.e     // Catch:{ JSONException -> 0x0438 }
            java.lang.String r10 = r7.optString(r4, r11)     // Catch:{ JSONException -> 0x0438 }
            java.lang.String r11 = "dataJson.optString(UIScriptConstant.TYPE, \"\")"
            com.fossil.ee7.a(r10, r11)     // Catch:{ JSONException -> 0x0438 }
            com.fossil.v11 r9 = r9.a(r10)     // Catch:{ JSONException -> 0x0438 }
            if (r9 == 0) goto L_0x03ec
            boolean r10 = r7.has(r1)     // Catch:{ JSONException -> 0x0438 }
            if (r10 == 0) goto L_0x03ec
            long r10 = r7.optLong(r1)     // Catch:{ JSONException -> 0x0438 }
            int[] r1 = com.fossil.w91.h     // Catch:{ JSONException -> 0x0438 }
            int r7 = r9.ordinal()     // Catch:{ JSONException -> 0x0438 }
            r1 = r1[r7]     // Catch:{ JSONException -> 0x0438 }
            r7 = 1
            if (r1 == r7) goto L_0x03e4
            r7 = 2
            if (r1 != r7) goto L_0x03de
            com.fossil.hc0 r1 = new com.fossil.hc0     // Catch:{ JSONException -> 0x0438 }
            r1.<init>(r6, r0, r10)     // Catch:{ JSONException -> 0x0438 }
            goto L_0x03e9
        L_0x03de:
            com.fossil.p87 r1 = new com.fossil.p87     // Catch:{ JSONException -> 0x0438 }
            r1.<init>()     // Catch:{ JSONException -> 0x0438 }
            throw r1     // Catch:{ JSONException -> 0x0438 }
        L_0x03e4:
            com.fossil.jc0 r1 = new com.fossil.jc0     // Catch:{ JSONException -> 0x0438 }
            r1.<init>(r6, r0, r10)     // Catch:{ JSONException -> 0x0438 }
        L_0x03e9:
            r12 = r1
            goto L_0x02a1
        L_0x03ec:
            r12 = r2
            goto L_0x02a1
        L_0x03ef:
            boolean r9 = r7.has(r3)     // Catch:{ JSONException -> 0x0438 }
            if (r9 == 0) goto L_0x0438
            boolean r9 = r7.has(r1)     // Catch:{ JSONException -> 0x0438 }
            if (r9 == 0) goto L_0x0438
            r14 = r25
            boolean r9 = r7.has(r14)     // Catch:{ JSONException -> 0x0434 }
            if (r9 == 0) goto L_0x0434
            long r12 = r7.optLong(r1)     // Catch:{ JSONException -> 0x0434 }
            long r15 = r7.optLong(r14)     // Catch:{ JSONException -> 0x0434 }
            org.json.JSONArray r1 = r7.getJSONArray(r3)     // Catch:{ JSONException -> 0x0434 }
            int r7 = r1.length()     // Catch:{ JSONException -> 0x0434 }
            int[] r7 = new int[r7]     // Catch:{ JSONException -> 0x0434 }
            int r9 = r1.length()     // Catch:{ JSONException -> 0x0434 }
            r10 = 0
        L_0x041a:
            if (r10 >= r9) goto L_0x0425
            int r11 = r1.optInt(r10)     // Catch:{ JSONException -> 0x0434 }
            r7[r10] = r11     // Catch:{ JSONException -> 0x0434 }
            int r10 = r10 + 1
            goto L_0x041a
        L_0x0425:
            com.fossil.ic0 r1 = new com.fossil.ic0     // Catch:{ JSONException -> 0x0434 }
            r9 = r1
            r10 = r6
            r11 = r0
            r18 = r14
            r14 = r15
            r16 = r7
            r9.<init>(r10, r11, r12, r14, r16)     // Catch:{ JSONException -> 0x0151 }
            r12 = r1
            goto L_0x045b
        L_0x0434:
            r18 = r14
            goto L_0x0151
        L_0x0438:
            r18 = r25
            goto L_0x0151
        L_0x043c:
            r0 = r15
            r8 = r22
            r19 = r23
            r18 = r25
            goto L_0x0150
        L_0x0445:
            r21 = r1
            r18 = r2
            r8 = r13
            r19 = r14
            r0 = r15
            r2 = 0
            java.lang.String r1 = "chanceOfRainSSE._.config.info"
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x0151
            com.fossil.ub0 r12 = new com.fossil.ub0
            r12.<init>(r6, r0)
        L_0x045b:
            r13 = 2
            goto L_0x04d2
        L_0x045e:
            r21 = r1
            r18 = r2
            r8 = r13
            r19 = r14
            r0 = r15
            r2 = 0
            java.lang.String r1 = "commuteApp._.config.commute_info"
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x0151
            java.lang.String r1 = "dest"
            java.lang.String r1 = r7.optString(r1)
            com.fossil.tf0$a r9 = com.fossil.tf0.c
            java.lang.String r7 = r7.optString(r12)
            java.lang.String r10 = "dataJson.optString(UIScriptConstant.ACTION)"
            com.fossil.ee7.a(r7, r10)
            com.fossil.tf0 r7 = r9.a(r7)
            if (r1 == 0) goto L_0x0151
            if (r7 != 0) goto L_0x048a
            goto L_0x0151
        L_0x048a:
            int[] r2 = com.fossil.w91.e
            int r9 = r7.ordinal()
            r2 = r2[r9]
            r9 = 1
            if (r2 == r9) goto L_0x04a4
            r13 = 2
            if (r2 != r13) goto L_0x049e
            com.fossil.ib0 r12 = new com.fossil.ib0
            r12.<init>(r6, r1, r7)
            goto L_0x04d2
        L_0x049e:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        L_0x04a4:
            r13 = 2
            com.fossil.xb0 r12 = new com.fossil.xb0
            r12.<init>(r6, r0, r1)
            goto L_0x04d2
        L_0x04ab:
            r21 = r1
            r18 = r2
            r8 = r13
            r19 = r14
            r0 = r15
            r2 = 0
            r13 = 2
            java.lang.String r1 = "ringMyPhone"
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x04d1
            java.lang.String r1 = r7.optString(r12)
            if (r1 == 0) goto L_0x04d1
            com.fossil.zf0$a r7 = com.fossil.zf0.b
            com.fossil.zf0 r1 = r7.a(r1)
            if (r1 == 0) goto L_0x04d1
            com.fossil.dc0 r12 = new com.fossil.dc0
            r12.<init>(r6, r0, r1)
            goto L_0x04d2
        L_0x04d1:
            r12 = r2
        L_0x04d2:
            if (r12 == 0) goto L_0x04d7
            r8.add(r12)
        L_0x04d7:
            r15 = r0
            r13 = r8
            r2 = r18
            r14 = r19
            r1 = r21
            r7 = 2
            r8 = 1
            r0 = r29
            goto L_0x012e
        L_0x04e5:
            r8 = r13
            r0 = r29
            r7 = 2
            r8 = 1
            goto L_0x012e
        L_0x04ec:
            r8 = r13
            r1 = 0
            r2 = 0
            com.fossil.bb0[] r0 = new com.fossil.bb0[r1]
            java.lang.Object[] r0 = r8.toArray(r0)
            if (r0 == 0) goto L_0x06da
            com.fossil.bb0[] r0 = (com.fossil.bb0[]) r0
            int r1 = r0.length
            r3 = 0
        L_0x04fb:
            if (r3 >= r1) goto L_0x06e0
            r4 = r0[r3]
            com.fossil.oe7 r5 = new com.fossil.oe7
            r5.<init>()
            r6 = 1
            r5.element = r6
            com.fossil.oe7 r7 = new com.fossil.oe7
            r7.<init>()
            r8 = 0
            r7.element = r8
            boolean r8 = r4 instanceof com.fossil.ec0
            if (r8 == 0) goto L_0x0547
            r5.element = r6
            r6 = r29
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x052a
            long r9 = r8.getCurrentSessionId()
            r12 = r4
            com.fossil.ec0 r12 = (com.fossil.ec0) r12
            long r12 = r12.getSessionId()
            int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r14 == 0) goto L_0x052f
        L_0x052a:
            if (r8 == 0) goto L_0x052f
            r8.dispose()
        L_0x052f:
            com.fossil.m60 r8 = r6.t
            java.lang.String r12 = r8.getSerialNumber()
            r8 = r4
            com.fossil.ec0 r8 = (com.fossil.ec0) r8
            long r13 = r8.getSessionId()
            com.fossil.fitness.WorkoutState r15 = com.fossil.fitness.WorkoutState.START
            r16 = 0
            com.fossil.fitness.WorkoutSessionManager r8 = com.fossil.fitness.WorkoutSessionManager.initialize(r12, r13, r15, r16)
            r6.s = r8
            goto L_0x056c
        L_0x0547:
            r6 = r29
            boolean r8 = r4 instanceof com.fossil.bc0
            if (r8 == 0) goto L_0x0571
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x0569
            long r9 = r8.getCurrentSessionId()
            r12 = r4
            com.fossil.bc0 r12 = (com.fossil.bc0) r12
            long r12 = r12.getSessionId()
            int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r14 != 0) goto L_0x0569
            r9 = 1
            r5.element = r9
            com.fossil.fitness.WorkoutState r9 = com.fossil.fitness.WorkoutState.RESUME
            r8.onWorkoutStateChanged(r9)
            goto L_0x056c
        L_0x0569:
            r8 = 0
            r5.element = r8
        L_0x056c:
            r12 = r2
        L_0x056d:
            r8 = 0
            r9 = 1
            goto L_0x06c0
        L_0x0571:
            boolean r8 = r4 instanceof com.fossil.ob0
            if (r8 == 0) goto L_0x05a5
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x059e
            long r9 = r8.getCurrentSessionId()
            r12 = r4
            com.fossil.ob0 r12 = (com.fossil.ob0) r12
            long r13 = r12.getSessionId()
            int r15 = (r9 > r13 ? 1 : (r9 == r13 ? 0 : -1))
            if (r15 != 0) goto L_0x059e
            r9 = 1
            r5.element = r9
            com.fossil.fitness.WorkoutState r9 = com.fossil.fitness.WorkoutState.PAUSE
            r8.onWorkoutStateChanged(r9)
            com.fossil.gf0 r8 = new com.fossil.gf0
            com.fossil.df0 r9 = new com.fossil.df0
            com.fossil.uf0 r10 = com.fossil.uf0.SUCCESS
            r9.<init>(r11, r10)
            r8.<init>(r12, r9)
        L_0x059c:
            r12 = r8
            goto L_0x05a2
        L_0x059e:
            r8 = 0
            r5.element = r8
        L_0x05a1:
            r12 = r2
        L_0x05a2:
            r9 = 1
            goto L_0x0670
        L_0x05a5:
            boolean r8 = r4 instanceof com.fossil.pb0
            if (r8 == 0) goto L_0x05d5
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x05d1
            long r9 = r8.getCurrentSessionId()
            r12 = r4
            com.fossil.pb0 r12 = (com.fossil.pb0) r12
            long r13 = r12.getSessionId()
            int r15 = (r9 > r13 ? 1 : (r9 == r13 ? 0 : -1))
            if (r15 != 0) goto L_0x05d1
            r9 = 1
            r5.element = r9
            com.fossil.fitness.WorkoutState r9 = com.fossil.fitness.WorkoutState.END
            r8.onWorkoutStateChanged(r9)
            com.fossil.lf0 r8 = new com.fossil.lf0
            com.fossil.df0 r9 = new com.fossil.df0
            com.fossil.uf0 r10 = com.fossil.uf0.SUCCESS
            r9.<init>(r11, r10)
            r8.<init>(r12, r9)
            goto L_0x059c
        L_0x05d1:
            r8 = 0
            r5.element = r8
            goto L_0x05a1
        L_0x05d5:
            boolean r8 = r4 instanceof com.fossil.qb0
            if (r8 == 0) goto L_0x05e9
            com.fossil.mf0 r12 = new com.fossil.mf0
            r8 = r4
            com.fossil.qb0 r8 = (com.fossil.qb0) r8
            com.fossil.df0 r9 = new com.fossil.df0
            com.fossil.uf0 r10 = com.fossil.uf0.SUCCESS
            r9.<init>(r11, r10)
            r12.<init>(r8, r9)
            goto L_0x056d
        L_0x05e9:
            boolean r8 = r4 instanceof com.fossil.hc0
            if (r8 == 0) goto L_0x062b
            r8 = 0
            r5.element = r8
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x0627
            long r9 = r8.getCurrentSessionId()
            r12 = r4
            com.fossil.hc0 r12 = (com.fossil.hc0) r12
            long r13 = r12.getSessionId()
            int r15 = (r9 > r13 ? 1 : (r9 == r13 ? 0 : -1))
            if (r15 == 0) goto L_0x0607
            r8 = 0
            r9 = 1
            goto L_0x06bf
        L_0x0607:
            r9 = 1
            r7.element = r9
            com.fossil.fitness.LocationDistance r8 = r8.getAccumulateDistance()
            java.lang.String r10 = "workoutSessionManager.accumulateDistance"
            com.fossil.ee7.a(r8, r10)
            com.fossil.pf0 r10 = new com.fossil.pf0
            com.fossil.b81 r13 = new com.fossil.b81
            double r14 = r8.getDistanceInCentimeters()
            int r8 = r8.getDuration()
            r13.<init>(r14, r8)
            r10.<init>(r12, r2, r13)
            r12 = r10
            goto L_0x0670
        L_0x0627:
            r9 = 1
        L_0x0628:
            r8 = 0
            goto L_0x06bf
        L_0x062b:
            r9 = 1
            boolean r8 = r4 instanceof com.fossil.ic0
            if (r8 == 0) goto L_0x0672
            r8 = 0
            r5.element = r8
            com.fossil.fitness.WorkoutSessionManager r8 = r6.s
            if (r8 == 0) goto L_0x066f
            long r12 = r8.getCurrentSessionId()
            r10 = r4
            com.fossil.ic0 r10 = (com.fossil.ic0) r10
            long r14 = r10.getSessionId()
            int r16 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r16 == 0) goto L_0x0647
            goto L_0x066f
        L_0x0647:
            long r12 = r10.d()
            java.util.ArrayList r14 = r10.e()
            boolean r8 = r8.correctGpsState(r12, r14)
            if (r8 == 0) goto L_0x0662
            com.fossil.qf0 r12 = new com.fossil.qf0
            com.fossil.df0 r8 = new com.fossil.df0
            com.fossil.uf0 r13 = com.fossil.uf0.SUCCESS
            r8.<init>(r11, r13)
            r12.<init>(r10, r8)
            goto L_0x0670
        L_0x0662:
            com.fossil.qf0 r12 = new com.fossil.qf0
            com.fossil.df0 r8 = new com.fossil.df0
            com.fossil.uf0 r13 = com.fossil.uf0.ERROR
            r8.<init>(r11, r13)
            r12.<init>(r10, r8)
            goto L_0x0670
        L_0x066f:
            r12 = r2
        L_0x0670:
            r8 = 0
            goto L_0x06c0
        L_0x0672:
            boolean r8 = r4 instanceof com.fossil.jc0
            if (r8 == 0) goto L_0x0628
            r8 = 0
            r5.element = r8
            com.fossil.fitness.WorkoutSessionManager r10 = r6.s
            if (r10 == 0) goto L_0x06bf
            long r12 = r10.getCurrentSessionId()
            r10 = r4
            com.fossil.jc0 r10 = (com.fossil.jc0) r10
            long r14 = r10.getSessionId()
            int r16 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r16 == 0) goto L_0x068d
            goto L_0x06bf
        L_0x068d:
            com.fossil.es1 r17 = com.fossil.es1.a
            android.location.Location[] r18 = r10.d()
            r19 = 240(0xf0, float:3.36E-43)
            r20 = 240(0xf0, float:3.36E-43)
            r21 = 50
            r22 = 50
            r23 = 20
            r24 = 20
            r25 = 0
            r26 = -1
            r27 = 1073741824(0x40000000, float:2.0)
            com.fossil.za0 r12 = r17.a(r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            if (r12 == 0) goto L_0x06b2
            com.fossil.rf0 r13 = new com.fossil.rf0
            r13.<init>(r10, r12)
            r12 = r13
            goto L_0x06c0
        L_0x06b2:
            com.fossil.rf0 r12 = new com.fossil.rf0
            com.fossil.df0 r13 = new com.fossil.df0
            com.fossil.uf0 r14 = com.fossil.uf0.ERROR
            r13.<init>(r11, r14)
            r12.<init>(r10, r13)
            goto L_0x06c0
        L_0x06bf:
            r12 = r2
        L_0x06c0:
            if (r12 == 0) goto L_0x06cf
            com.fossil.he0 r10 = r6.a(r12)
            com.fossil.qb1 r12 = new com.fossil.qb1
            r12.<init>(r5, r4, r7, r6)
            r10.e(r12)
            goto L_0x06d6
        L_0x06cf:
            boolean r5 = r5.element
            if (r5 == 0) goto L_0x06d6
            r6.a(r4)
        L_0x06d6:
            int r3 = r3 + 1
            goto L_0x04fb
        L_0x06da:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r10)
            throw r0
        L_0x06e0:
            return
            switch-data {1->0x0113, 2->0x010e, 3->0x0106, 4->0x001b, 5->0x00ef, 6->0x00da, 7->0x00c2, 8->0x0061, 9->0x005c, 10->0x004e, 11->0x003d, 12->0x001d, }
            switch-data {-1119206702->0x04ab, -755744823->0x045e, -186117117->0x0445, -94314940->0x02dc, 65217736->0x02a5, 126482114->0x0287, 427103396->0x021d, 970116257->0x0175, 1255636834->0x015e, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vk1.a(com.fossil.km1, com.fossil.xp0):void");
    }
}
