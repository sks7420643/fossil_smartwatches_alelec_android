package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u66 {
    @DexIgnore
    public /* final */ l86 a;
    @DexIgnore
    public /* final */ h76 b;
    @DexIgnore
    public /* final */ q96 c;
    @DexIgnore
    public /* final */ yb6 d;
    @DexIgnore
    public /* final */ cd6 e;
    @DexIgnore
    public /* final */ ua6 f;

    @DexIgnore
    public u66(l86 l86, h76 h76, q96 q96, yb6 yb6, cd6 cd6, ua6 ua6) {
        ee7.b(l86, "mActivityView");
        ee7.b(h76, "mActiveTimeView");
        ee7.b(q96, "mCaloriesView");
        ee7.b(yb6, "mHeartrateView");
        ee7.b(cd6, "mSleepView");
        ee7.b(ua6, "mGoalTrackingView");
        this.a = l86;
        this.b = h76;
        this.c = q96;
        this.d = yb6;
        this.e = cd6;
        this.f = ua6;
    }

    @DexIgnore
    public final h76 a() {
        return this.b;
    }

    @DexIgnore
    public final l86 b() {
        return this.a;
    }

    @DexIgnore
    public final q96 c() {
        return this.c;
    }

    @DexIgnore
    public final ua6 d() {
        return this.f;
    }

    @DexIgnore
    public final yb6 e() {
        return this.d;
    }

    @DexIgnore
    public final cd6 f() {
        return this.e;
    }
}
