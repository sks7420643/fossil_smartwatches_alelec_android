package com.fossil;

import com.fossil.vx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class by3<K, V> implements Map<K, V>, Serializable {
    @DexIgnore
    public static /* final */ Map.Entry<?, ?>[] EMPTY_ENTRY_ARRAY; // = new Map.Entry[0];
    @DexIgnore
    @LazyInit
    public transient iy3<Map.Entry<K, V>> a;
    @DexIgnore
    @LazyInit
    public transient iy3<K> b;
    @DexIgnore
    @LazyInit
    public transient vx3<V> c;
    @DexIgnore
    @LazyInit
    public transient jy3<K, V> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends j04<K> {
        @DexIgnore
        public /* final */ /* synthetic */ j04 a;

        @DexIgnore
        public a(by3 by3, j04 j04) {
            this.a = j04;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public K next() {
            return (K) ((Map.Entry) this.a.next()).getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> {
        @DexIgnore
        public Comparator<? super V> a;
        @DexIgnore
        public cy3<K, V>[] b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b() {
            this(4);
        }

        @DexIgnore
        public final void a(int i) {
            cy3<K, V>[] cy3Arr = this.b;
            if (i > cy3Arr.length) {
                this.b = (cy3[]) iz3.a((Object[]) cy3Arr, vx3.b.a(cy3Arr.length, i));
                this.d = false;
            }
        }

        @DexIgnore
        public b(int i) {
            this.b = new cy3[i];
            this.c = 0;
            this.d = false;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> a(K k, V v) {
            a(this.c + 1);
            cy3<K, V> entryOf = by3.entryOf(k, v);
            cy3<K, V>[] cy3Arr = this.b;
            int i = this.c;
            this.c = i + 1;
            cy3Arr[i] = entryOf;
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.by3$b<K, V> */
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public b<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            return a(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> a(Map<? extends K, ? extends V> map) {
            return a(map.entrySet());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public b<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            if (iterable instanceof Collection) {
                a(this.c + ((Collection) iterable).size());
            }
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                a(entry);
            }
            return this;
        }

        @DexIgnore
        public by3<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return by3.of();
            }
            boolean z = true;
            if (i == 1) {
                return by3.of(this.b[0].getKey(), this.b[0].getValue());
            }
            if (this.a != null) {
                if (this.d) {
                    this.b = (cy3[]) iz3.a((Object[]) this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, jz3.from(this.a).onResultOf(yy3.c()));
            }
            if (this.c != this.b.length) {
                z = false;
            }
            this.d = z;
            return qz3.fromEntryArray(this.c, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends by3<K, V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends dy3<K, V> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.dy3
            public by3<K, V> map() {
                return c.this;
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
            public j04<Map.Entry<K, V>> iterator() {
                return c.this.entryIterator();
            }
        }

        @DexIgnore
        @Override // com.fossil.by3
        public iy3<Map.Entry<K, V>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        public abstract j04<Map.Entry<K, V>> entryIterator();

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3, com.fossil.by3
        public /* bridge */ /* synthetic */ Set entrySet() {
            return by3.super.entrySet();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3, com.fossil.by3
        public /* bridge */ /* synthetic */ Set keySet() {
            return by3.super.keySet();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3, com.fossil.by3
        public /* bridge */ /* synthetic */ Collection values() {
            return by3.super.values();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends c<K, iy3<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends j04<Map.Entry<K, iy3<V>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Iterator a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.by3$d$a$a")
            /* renamed from: com.fossil.by3$d$a$a  reason: collision with other inner class name */
            public class C0021a extends uw3<K, iy3<V>> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry a;

                @DexIgnore
                public C0021a(a aVar, Map.Entry entry) {
                    this.a = entry;
                }

                @DexIgnore
                @Override // java.util.Map.Entry, com.fossil.uw3
                public K getKey() {
                    return (K) this.a.getKey();
                }

                @DexIgnore
                @Override // java.util.Map.Entry, com.fossil.uw3
                public iy3<V> getValue() {
                    return iy3.of(this.a.getValue());
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.a = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.a.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public Map.Entry<K, iy3<V>> next() {
                return new C0021a(this, (Map.Entry) this.a.next());
            }
        }

        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.by3
        public boolean containsKey(Object obj) {
            return by3.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.by3.c
        public j04<Map.Entry<K, iy3<V>>> entryIterator() {
            return new a(this, by3.this.entrySet().iterator());
        }

        @DexIgnore
        @Override // com.fossil.by3
        public int hashCode() {
            return by3.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.by3
        public boolean isHashCodeFast() {
            return by3.this.isHashCodeFast();
        }

        @DexIgnore
        @Override // com.fossil.by3
        public boolean isPartialView() {
            return by3.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return by3.this.size();
        }

        @DexIgnore
        public /* synthetic */ d(by3 by3, a aVar) {
            this();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3
        public iy3<V> get(Object obj) {
            Object obj2 = by3.this.get(obj);
            if (obj2 == null) {
                return null;
            }
            return iy3.of(obj2);
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3.c, com.fossil.by3, com.fossil.by3
        public iy3<K> keySet() {
            return by3.this.keySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] keys;
        @DexIgnore
        public /* final */ Object[] values;

        @DexIgnore
        public e(by3<?, ?> by3) {
            this.keys = new Object[by3.size()];
            this.values = new Object[by3.size()];
            Iterator it = by3.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.keys[i] = entry.getKey();
                this.values[i] = entry.getValue();
                i++;
            }
        }

        @DexIgnore
        public Object createMap(b<Object, Object> bVar) {
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return bVar.a();
                }
                bVar.a(objArr[i], this.values[i]);
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new b<>(this.keys.length));
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> by3<K, V> a(EnumMap<K, ? extends V> enumMap) {
        EnumMap enumMap2 = new EnumMap((EnumMap) enumMap);
        for (Map.Entry<K, V> entry : enumMap2.entrySet()) {
            bx3.a(entry.getKey(), entry.getValue());
        }
        return xx3.asImmutable(enumMap2);
    }

    @DexIgnore
    public static <K, V> b<K, V> builder() {
        return new b<>();
    }

    @DexIgnore
    public static void checkNoConflict(boolean z, String str, Map.Entry<?, ?> entry, Map.Entry<?, ?> entry2) {
        if (!z) {
            throw new IllegalArgumentException("Multiple entries with same " + str + ": " + entry + " and " + entry2);
        }
    }

    @DexIgnore
    public static <K, V> by3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if ((map instanceof by3) && !(map instanceof ly3)) {
            by3<K, V> by3 = (by3) map;
            if (!by3.isPartialView()) {
                return by3;
            }
        } else if (map instanceof EnumMap) {
            return a((EnumMap) map);
        }
        return copyOf(map.entrySet());
    }

    @DexIgnore
    public static <K, V> cy3<K, V> entryOf(K k, V v) {
        return new cy3<>(k, v);
    }

    @DexIgnore
    public static <K, V> by3<K, V> of() {
        return ux3.of();
    }

    @DexIgnore
    public jy3<K, V> asMultimap() {
        if (isEmpty()) {
            return jy3.of();
        }
        jy3<K, V> jy3 = this.d;
        if (jy3 != null) {
            return jy3;
        }
        jy3<K, V> jy32 = new jy3<>(new d(this, null), size(), null);
        this.d = jy32;
        return jy32;
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    @DexIgnore
    public abstract iy3<Map.Entry<K, V>> createEntrySet();

    @DexIgnore
    public iy3<K> createKeySet() {
        return isEmpty() ? iy3.of() : new ey3(this);
    }

    @DexIgnore
    public vx3<V> createValues() {
        return new fy3(this);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return yy3.c(this, obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public abstract V get(Object obj);

    @DexIgnore
    public int hashCode() {
        return yz3.a(entrySet());
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    public j04<K> keyIterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @CanIgnoreReturnValue
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return yy3.a(this);
    }

    @DexIgnore
    public Object writeReplace() {
        return new e(this);
    }

    @DexIgnore
    public static <K, V> by3<K, V> of(K k, V v) {
        return ux3.of((Object) k, (Object) v);
    }

    @DexIgnore
    @Override // java.util.Map
    public iy3<Map.Entry<K, V>> entrySet() {
        iy3<Map.Entry<K, V>> iy3 = this.a;
        if (iy3 != null) {
            return iy3;
        }
        iy3<Map.Entry<K, V>> createEntrySet = createEntrySet();
        this.a = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    @Override // java.util.Map
    public iy3<K> keySet() {
        iy3<K> iy3 = this.b;
        if (iy3 != null) {
            return iy3;
        }
        iy3<K> createKeySet = createKeySet();
        this.b = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    @Override // java.util.Map
    public vx3<V> values() {
        vx3<V> vx3 = this.c;
        if (vx3 != null) {
            return vx3;
        }
        vx3<V> createValues = createValues();
        this.c = createValues;
        return createValues;
    }

    @DexIgnore
    public static <K, V> by3<K, V> of(K k, V v, K k2, V v2) {
        return qz3.fromEntries(entryOf(k, v), entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> by3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return qz3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> by3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return qz3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> by3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return qz3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> by3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) py3.a((Iterable) iterable, (Object[]) EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return qz3.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }
}
