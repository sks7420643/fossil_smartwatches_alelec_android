package com.fossil;

import java.nio.ByteBuffer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a81 extends k60 {
    @DexIgnore
    public static /* final */ c61 d; // = new c61(null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore
    public a81(short s) {
        this.c = s;
        ByteBuffer putShort = ByteBuffer.allocate(2).putShort(this.c);
        ee7.a((Object) putShort, "ByteBuffer.allocate(2).putShort(fileHandle)");
        this.a = putShort.get(0);
        this.b = putShort.get(1);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.H2, yz0.a(this.a)), r51.s4, yz0.a(this.b)), r51.y0, yz0.a(this.c)), r51.e4, d.a(this.c));
    }

    @DexIgnore
    public a81(byte b2, byte b3) {
        this(ByteBuffer.allocate(2).put(b2).put(b3).getShort(0));
    }
}
