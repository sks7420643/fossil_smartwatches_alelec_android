package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ int b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ x47 e;

    @DexIgnore
    public y47(x47 x47, List list, int i, boolean z, boolean z2) {
        this.e = x47;
        this.a = list;
        this.b = i;
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public void run() {
        this.e.a(this.a, this.b, this.c);
        if (this.d) {
            this.a.clear();
        }
    }
}
