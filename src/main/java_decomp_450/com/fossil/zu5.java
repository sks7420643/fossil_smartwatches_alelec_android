package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu5 extends RecyclerView.g<c> {
    @DexIgnore
    public ArrayList<QuickResponseMessage> a;
    @DexIgnore
    public b b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(QuickResponseMessage quickResponseMessage);

        @DexIgnore
        void g(int i, String str);

        @DexIgnore
        void j0();
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public zu5(ArrayList<QuickResponseMessage> arrayList, b bVar, int i) {
        ee7.b(arrayList, "mData");
        ee7.b(bVar, "mListener");
        this.a = arrayList;
        this.b = bVar;
        this.c = i;
    }

    @DexIgnore
    public final void a(List<QuickResponseMessage> list) {
        ee7.b(list, "data");
        this.a.clear();
        for (T t : list) {
            QuickResponseMessage quickResponseMessage = new QuickResponseMessage(t.getResponse());
            quickResponseMessage.setId(t.getId());
            this.a.add(quickResponseMessage);
        }
        notifyDataSetChanged();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "notifyDataSetChanged " + this.a);
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final b d() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public RTLImageView a;
        @DexIgnore
        public FlexibleEditText b;
        @DexIgnore
        public ConstraintLayout c;
        @DexIgnore
        public View d;
        @DexIgnore
        public QuickResponseMessage e;
        @DexIgnore
        public RTLImageView f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public /* final */ /* synthetic */ zu5 j;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements TextWatcher {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void afterTextChanged(Editable editable) {
                if (this.a.b.isEnabled()) {
                    int length = String.valueOf(editable).length();
                    if (length >= this.a.j.c()) {
                        this.a.g = true;
                        if (!TextUtils.isEmpty(this.a.h)) {
                            this.a.b.setBackgroundResource(2131230861);
                            this.a.b.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.a.h)));
                            return;
                        }
                        this.a.b.setBackgroundResource(2131230861);
                        this.a.b.setBackgroundTintList(ColorStateList.valueOf(-65536));
                    } else if (length != 0) {
                        this.a.a();
                    } else {
                        this.a.b.setHint(ig5.a(PortfolioApp.g0.c(), 2131886126));
                    }
                }
            }

            @DexIgnore
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @DexIgnore
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (this.a.b.hasFocus()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onTextChanged id ");
                    QuickResponseMessage b = this.a.e;
                    sb.append(b != null ? Integer.valueOf(b.getId()) : null);
                    sb.append(" message ");
                    QuickResponseMessage b2 = this.a.e;
                    sb.append(b2 != null ? b2.getResponse() : null);
                    local.d("QuickResponseAdapter", sb.toString());
                    b d = this.a.j.d();
                    QuickResponseMessage b3 = this.a.e;
                    Integer valueOf = b3 != null ? Integer.valueOf(b3.getId()) : null;
                    if (valueOf != null) {
                        d.g(valueOf.intValue(), String.valueOf(charSequence));
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnFocusChangeListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onFocusChange(View view, boolean z) {
                if (!z) {
                    this.a.a();
                    this.a.b.setEnabled(false);
                    this.a.j.d().j0();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zu5 zu5, View view) {
            super(view);
            ee7.b(view, "view");
            this.j = zu5;
            View findViewById = view.findViewById(2131362714);
            if (findViewById != null) {
                this.a = (RTLImageView) findViewById;
                View findViewById2 = view.findViewById(2131362280);
                if (findViewById2 != null) {
                    this.b = (FlexibleEditText) findViewById2;
                    View findViewById3 = view.findViewById(nr3.container);
                    if (findViewById3 != null) {
                        this.c = (ConstraintLayout) findViewById3;
                        View findViewById4 = view.findViewById(2131362751);
                        if (findViewById4 != null) {
                            this.d = findViewById4;
                            View findViewById5 = view.findViewById(2131361938);
                            ee7.a((Object) findViewById5, "view.findViewById(R.id.bt_edit)");
                            this.f = (RTLImageView) findViewById5;
                            this.h = eh5.l.a().b("error");
                            this.i = eh5.l.a().b("nonBrandSurface");
                            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
                            String b3 = eh5.l.a().b("nonBrandSeparatorLine");
                            if (!TextUtils.isEmpty(b2)) {
                                this.c.setBackgroundColor(Color.parseColor(b2));
                            }
                            if (!TextUtils.isEmpty(b3)) {
                                this.d.setBackgroundColor(Color.parseColor(b3));
                            }
                            this.b.setEnabled(false);
                            this.a.setOnClickListener(this);
                            this.f.setOnClickListener(this);
                            this.b.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(zu5.c())});
                            this.b.addTextChangedListener(new a(this));
                            this.b.setOnFocusChangeListener(new b(this));
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type android.view.View");
                    }
                    throw new x87("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout");
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleEditText");
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.RTLImageView");
        }

        @DexIgnore
        public void onClick(View view) {
            Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
            if (valueOf != null && valueOf.intValue() == 2131362714) {
                QuickResponseMessage quickResponseMessage = this.e;
                if (quickResponseMessage != null) {
                    this.j.d().a(quickResponseMessage);
                }
            } else if (valueOf != null && valueOf.intValue() == 2131361938) {
                this.b.setEnabled(true);
                this.b.requestFocus();
                this.b.a();
                FlexibleEditText flexibleEditText = this.b;
                Editable text = flexibleEditText.getText();
                Integer valueOf2 = text != null ? Integer.valueOf(text.length()) : null;
                if (valueOf2 != null) {
                    flexibleEditText.setSelection(valueOf2.intValue());
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public final void a() {
            if (this.g) {
                if (!TextUtils.isEmpty(this.i)) {
                    this.b.setBackground(null);
                    this.b.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.i)));
                } else {
                    this.b.setBackground(null);
                    this.b.setBackgroundTintList(ColorStateList.valueOf(-1));
                }
                this.g = false;
            }
        }

        @DexIgnore
        public final void a(QuickResponseMessage quickResponseMessage) {
            ee7.b(quickResponseMessage, "response");
            this.e = quickResponseMessage;
            if (quickResponseMessage.getResponse().length() > 0) {
                this.b.setText(quickResponseMessage.getResponse());
                quickResponseMessage.getResponse();
                return;
            }
            this.b.setHint(ig5.a(PortfolioApp.g0.c(), 2131886126));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558699, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026_response, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "holder");
        QuickResponseMessage quickResponseMessage = this.a.get(i);
        ee7.a((Object) quickResponseMessage, "mData[position]");
        cVar.a(quickResponseMessage);
    }
}
