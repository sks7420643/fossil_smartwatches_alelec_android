package com.fossil;

import android.util.Log;
import com.fossil.v02;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o52 implements ho3<Map<p12<?>, String>> {
    @DexIgnore
    public /* final */ /* synthetic */ m52 a;

    @DexIgnore
    public o52(m52 m52) {
        this.a = m52;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3<Map<p12<?>, String>> no3) {
        this.a.f.lock();
        try {
            if (this.a.s) {
                if (no3.e()) {
                    Map unused = this.a.t = new n4(this.a.a.size());
                    for (k52 k52 : this.a.a.values()) {
                        this.a.t.put(k52.a(), i02.e);
                    }
                } else if (no3.a() instanceof x02) {
                    x02 x02 = (x02) no3.a();
                    if (this.a.q) {
                        Map unused2 = this.a.t = new n4(this.a.a.size());
                        for (k52 k522 : this.a.a.values()) {
                            p12 a2 = k522.a();
                            i02 connectionResult = x02.getConnectionResult((z02<? extends v02.d>) k522);
                            if (this.a.a(k522, connectionResult)) {
                                this.a.t.put(a2, new i02(16));
                            } else {
                                this.a.t.put(a2, connectionResult);
                            }
                        }
                    } else {
                        Map unused3 = this.a.t = x02.zaj();
                    }
                    i02 unused4 = this.a.w = this.a.k();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", no3.a());
                    Map unused5 = this.a.t = Collections.emptyMap();
                    i02 unused6 = this.a.w = new i02(8);
                }
                if (this.a.u != null) {
                    this.a.t.putAll(this.a.u);
                    i02 unused7 = this.a.w = this.a.k();
                }
                if (this.a.w == null) {
                    this.a.i();
                    this.a.j();
                } else {
                    boolean unused8 = this.a.s = false;
                    this.a.e.a(this.a.w);
                }
                this.a.i.signalAll();
                this.a.f.unlock();
            }
        } finally {
            this.a.f.unlock();
        }
    }
}
