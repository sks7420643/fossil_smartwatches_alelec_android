package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Picasso;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m07<T> {
    @DexIgnore
    public /* final */ Picasso a;
    @DexIgnore
    public /* final */ g17 b;
    @DexIgnore
    public /* final */ WeakReference<T> c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ Drawable h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<M> extends WeakReference<M> {
        @DexIgnore
        public /* final */ m07 a;

        @DexIgnore
        public a(m07 m07, M m, ReferenceQueue<? super M> referenceQueue) {
            super(m, referenceQueue);
            this.a = m07;
        }
    }

    @DexIgnore
    public m07(Picasso picasso, T t, g17 g17, int i2, int i3, int i4, Drawable drawable, String str, Object obj, boolean z) {
        a aVar;
        this.a = picasso;
        this.b = g17;
        if (t == null) {
            aVar = null;
        } else {
            aVar = new a(this, t, picasso.k);
        }
        this.c = aVar;
        this.e = i2;
        this.f = i3;
        this.d = z;
        this.g = i4;
        this.h = drawable;
        this.i = str;
        this.j = obj == null ? this : obj;
    }

    @DexIgnore
    public void a() {
        this.l = true;
    }

    @DexIgnore
    public abstract void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom);

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public String c() {
        return this.i;
    }

    @DexIgnore
    public int d() {
        return this.e;
    }

    @DexIgnore
    public int e() {
        return this.f;
    }

    @DexIgnore
    public Picasso f() {
        return this.a;
    }

    @DexIgnore
    public Picasso.e g() {
        return this.b.r;
    }

    @DexIgnore
    public g17 h() {
        return this.b;
    }

    @DexIgnore
    public Object i() {
        return this.j;
    }

    @DexIgnore
    public T j() {
        WeakReference<T> weakReference = this.c;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    @DexIgnore
    public boolean k() {
        return this.l;
    }

    @DexIgnore
    public boolean l() {
        return this.k;
    }
}
