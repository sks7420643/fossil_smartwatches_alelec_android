package com.fossil;

import com.facebook.internal.AnalyticsEvents;
import com.fossil.bm7;
import com.fossil.ib7;
import com.fossil.ik7;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pk7 implements ik7, ii7, xk7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(pk7.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _parentHandle;
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> extends bi7<T> {
        @DexIgnore
        public /* final */ pk7 h;

        @DexIgnore
        public a(fb7<? super T> fb7, pk7 pk7) {
            super(fb7, 1);
            this.h = pk7;
        }

        @DexIgnore
        @Override // com.fossil.bi7
        public Throwable a(ik7 ik7) {
            Throwable d;
            Object h2 = this.h.h();
            if ((h2 instanceof c) && (d = ((c) h2).d()) != null) {
                return d;
            }
            if (h2 instanceof li7) {
                return ((li7) h2).a;
            }
            return ik7.b();
        }

        @DexIgnore
        @Override // com.fossil.bi7
        public String k() {
            return "AwaitContinuation";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ok7<ik7> {
        @DexIgnore
        public /* final */ pk7 e;
        @DexIgnore
        public /* final */ c f;
        @DexIgnore
        public /* final */ hi7 g;
        @DexIgnore
        public /* final */ Object h;

        @DexIgnore
        public b(pk7 pk7, c cVar, hi7 hi7, Object obj) {
            super(hi7.e);
            this.e = pk7;
            this.f = cVar;
            this.g = hi7;
            this.h = obj;
        }

        @DexIgnore
        @Override // com.fossil.pi7
        public void b(Throwable th) {
            this.e.a(this.f, this.g, this.h);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
            b(th);
            return i97.a;
        }

        @DexIgnore
        @Override // com.fossil.bm7
        public String toString() {
            return "ChildCompletion[" + this.g + ", " + this.h + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements dk7 {
        @DexIgnore
        public volatile Object _exceptionsHolder; // = null;
        @DexIgnore
        public volatile int _isCompleting;
        @DexIgnore
        public volatile Object _rootCause;
        @DexIgnore
        public /* final */ uk7 a;

        @DexIgnore
        public c(uk7 uk7, boolean z, Throwable th) {
            this.a = uk7;
            this._isCompleting = z ? 1 : 0;
            this._rootCause = th;
        }

        @DexIgnore
        @Override // com.fossil.dk7
        public uk7 a() {
            return this.a;
        }

        @DexIgnore
        public final List<Throwable> b(Throwable th) {
            ArrayList<Throwable> arrayList;
            Object c = c();
            if (c == null) {
                arrayList = b();
            } else if (c instanceof Throwable) {
                ArrayList<Throwable> b = b();
                b.add(c);
                arrayList = b;
            } else if (c instanceof ArrayList) {
                arrayList = (ArrayList) c;
            } else {
                throw new IllegalStateException(("State is " + c).toString());
            }
            Throwable d = d();
            if (d != null) {
                arrayList.add(0, d);
            }
            if (th != null && (!ee7.a(th, d))) {
                arrayList.add(th);
            }
            a(qk7.e);
            return arrayList;
        }

        @DexIgnore
        public final void c(Throwable th) {
            this._rootCause = th;
        }

        @DexIgnore
        public final Throwable d() {
            return (Throwable) this._rootCause;
        }

        @DexIgnore
        public final boolean e() {
            return d() != null;
        }

        @DexIgnore
        /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
        public final boolean f() {
            return this._isCompleting;
        }

        @DexIgnore
        public final boolean g() {
            return c() == qk7.e;
        }

        @DexIgnore
        @Override // com.fossil.dk7
        public boolean isActive() {
            return d() == null;
        }

        @DexIgnore
        public String toString() {
            return "Finishing[cancelling=" + e() + ", completing=" + f() + ", rootCause=" + d() + ", exceptions=" + c() + ", list=" + a() + ']';
        }

        @DexIgnore
        public final void a(boolean z) {
            this._isCompleting = z ? 1 : 0;
        }

        @DexIgnore
        public final Object c() {
            return this._exceptionsHolder;
        }

        @DexIgnore
        public final void a(Object obj) {
            this._exceptionsHolder = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: java.util.ArrayList<java.lang.Throwable> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(Throwable th) {
            Throwable d = d();
            if (d == null) {
                c(th);
            } else if (th != d) {
                Object c = c();
                if (c == null) {
                    a((Object) th);
                } else if (c instanceof Throwable) {
                    if (th != c) {
                        ArrayList<Throwable> b = b();
                        b.add(c);
                        b.add(th);
                        a(b);
                    }
                } else if (c instanceof ArrayList) {
                    ((ArrayList) c).add(th);
                } else {
                    throw new IllegalStateException(("State is " + c).toString());
                }
            }
        }

        @DexIgnore
        public final ArrayList<Throwable> b() {
            return new ArrayList<>(4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends bm7.a {
        @DexIgnore
        public /* final */ /* synthetic */ pk7 d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bm7 bm7, bm7 bm72, pk7 pk7, Object obj) {
            super(bm72);
            this.d = pk7;
            this.e = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(bm7 bm7) {
            if (this.d.h() == this.e) {
                return null;
            }
            return am7.a();
        }
    }

    @DexIgnore
    public pk7(boolean z) {
        this._state = z ? qk7.g : qk7.f;
        this._parentHandle = null;
    }

    @DexIgnore
    public void a(Object obj) {
    }

    @DexIgnore
    public final boolean b(dk7 dk7, Object obj) {
        if (dj7.a()) {
            if (!((dk7 instanceof uj7) || (dk7 instanceof ok7))) {
                throw new AssertionError();
            }
        }
        if (dj7.a() && !(!(obj instanceof li7))) {
            throw new AssertionError();
        } else if (!a.compareAndSet(this, dk7, qk7.a(obj))) {
            return false;
        } else {
            g((Throwable) null);
            i(obj);
            a(dk7, obj);
            return true;
        }
    }

    @DexIgnore
    public String c() {
        return "Job was cancelled";
    }

    @DexIgnore
    public final boolean c(Throwable th) {
        if (j()) {
            return true;
        }
        boolean z = th instanceof CancellationException;
        gi7 g = g();
        if (g == null || g == vk7.a) {
            return z;
        }
        if (g.a(th) || z) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean d() {
        return true;
    }

    @DexIgnore
    public boolean d(Throwable th) {
        if (th instanceof CancellationException) {
            return true;
        }
        if (!c((Object) th) || !d()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.xk7
    public CancellationException e() {
        Throwable th;
        Object h = h();
        CancellationException cancellationException = null;
        if (h instanceof c) {
            th = ((c) h).d();
        } else if (h instanceof li7) {
            th = ((li7) h).a;
        } else if (!(h instanceof dk7)) {
            th = null;
        } else {
            throw new IllegalStateException(("Cannot be cancelling child in this state: " + h).toString());
        }
        if (th instanceof CancellationException) {
            cancellationException = th;
        }
        CancellationException cancellationException2 = cancellationException;
        if (cancellationException2 != null) {
            return cancellationException2;
        }
        return new jk7("Parent job is " + k(h), th, this);
    }

    @DexIgnore
    public boolean e(Throwable th) {
        return false;
    }

    @DexIgnore
    public final Throwable f(Object obj) {
        if (!(obj instanceof li7)) {
            obj = null;
        }
        li7 li7 = (li7) obj;
        if (li7 != null) {
            return li7.a;
        }
        return null;
    }

    @DexIgnore
    public boolean f() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <R> R fold(R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
        return (R) ik7.a.a(this, r, kd7);
    }

    @DexIgnore
    public final gi7 g() {
        return (gi7) this._parentHandle;
    }

    @DexIgnore
    public void g(Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.ib7, com.fossil.ib7.b
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        return (E) ik7.a.a(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7.b
    public final ib7.c<?> getKey() {
        return ik7.o;
    }

    @DexIgnore
    public final Object h() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof hm7)) {
                return obj;
            }
            ((hm7) obj).a(this);
        }
    }

    @DexIgnore
    public void i(Object obj) {
    }

    @DexIgnore
    public final boolean i() {
        return !(h() instanceof dk7);
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public boolean isActive() {
        Object h = h();
        return (h instanceof dk7) && ((dk7) h).isActive();
    }

    @DexIgnore
    public final int j(Object obj) {
        if (obj instanceof uj7) {
            if (((uj7) obj).isActive()) {
                return 0;
            }
            if (!a.compareAndSet(this, obj, qk7.g)) {
                return -1;
            }
            l();
            return 1;
        } else if (!(obj instanceof ck7)) {
            return 0;
        } else {
            if (!a.compareAndSet(this, obj, ((ck7) obj).a())) {
                return -1;
            }
            l();
            return 1;
        }
    }

    @DexIgnore
    public boolean j() {
        return false;
    }

    @DexIgnore
    public String k() {
        return ej7.a(this);
    }

    @DexIgnore
    public void l() {
    }

    @DexIgnore
    public final String m() {
        return k() + '{' + k(h()) + '}';
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 minusKey(ib7.c<?> cVar) {
        return ik7.a.b(this, cVar);
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 plus(ib7 ib7) {
        return ik7.a.a(this, ib7);
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public final boolean start() {
        int j;
        do {
            j = j(h());
            if (j == 0) {
                return false;
            }
        } while (j != 1);
        return true;
    }

    @DexIgnore
    public String toString() {
        return m() + '@' + ej7.b(this);
    }

    @DexIgnore
    public void f(Throwable th) {
        throw th;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        if (r0 == null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0041, code lost:
        a(((com.fossil.pk7.c) r2).a(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004e, code lost:
        return com.fossil.qk7.a();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(java.lang.Object r7) {
        /*
            r6 = this;
            r0 = 0
            r1 = r0
        L_0x0002:
            java.lang.Object r2 = r6.h()
            boolean r3 = r2 instanceof com.fossil.pk7.c
            if (r3 == 0) goto L_0x0052
            monitor-enter(r2)
            r3 = r2
            com.fossil.pk7$c r3 = (com.fossil.pk7.c) r3     // Catch:{ all -> 0x004f }
            boolean r3 = r3.g()     // Catch:{ all -> 0x004f }
            if (r3 == 0) goto L_0x001a
            com.fossil.lm7 r7 = com.fossil.qk7.d     // Catch:{ all -> 0x004f }
            monitor-exit(r2)
            return r7
        L_0x001a:
            r3 = r2
            com.fossil.pk7$c r3 = (com.fossil.pk7.c) r3
            boolean r3 = r3.e()
            if (r7 != 0) goto L_0x0025
            if (r3 != 0) goto L_0x0032
        L_0x0025:
            if (r1 == 0) goto L_0x0028
            goto L_0x002c
        L_0x0028:
            java.lang.Throwable r1 = r6.e(r7)
        L_0x002c:
            r7 = r2
            com.fossil.pk7$c r7 = (com.fossil.pk7.c) r7
            r7.a(r1)
        L_0x0032:
            r7 = r2
            com.fossil.pk7$c r7 = (com.fossil.pk7.c) r7
            java.lang.Throwable r7 = r7.d()
            r1 = r3 ^ 1
            if (r1 == 0) goto L_0x003e
            r0 = r7
        L_0x003e:
            monitor-exit(r2)
            if (r0 == 0) goto L_0x004a
            com.fossil.pk7$c r2 = (com.fossil.pk7.c) r2
            com.fossil.uk7 r7 = r2.a()
            r6.a(r7, r0)
        L_0x004a:
            com.fossil.lm7 r7 = com.fossil.qk7.a
            return r7
        L_0x004f:
            r7 = move-exception
            monitor-exit(r2)
            throw r7
        L_0x0052:
            boolean r3 = r2 instanceof com.fossil.dk7
            if (r3 == 0) goto L_0x00a6
            if (r1 == 0) goto L_0x0059
            goto L_0x005d
        L_0x0059:
            java.lang.Throwable r1 = r6.e(r7)
        L_0x005d:
            r3 = r2
            com.fossil.dk7 r3 = (com.fossil.dk7) r3
            boolean r4 = r3.isActive()
            if (r4 == 0) goto L_0x0071
            boolean r2 = r6.a(r3, r1)
            if (r2 == 0) goto L_0x0002
            com.fossil.lm7 r7 = com.fossil.qk7.a
            return r7
        L_0x0071:
            com.fossil.li7 r3 = new com.fossil.li7
            r4 = 0
            r5 = 2
            r3.<init>(r1, r4, r5, r0)
            java.lang.Object r3 = r6.b(r2, r3)
            com.fossil.lm7 r4 = com.fossil.qk7.a
            if (r3 == r4) goto L_0x008b
            com.fossil.lm7 r2 = com.fossil.qk7.c
            if (r3 != r2) goto L_0x008a
            goto L_0x0002
        L_0x008a:
            return r3
        L_0x008b:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r0 = "Cannot happen in "
            r7.append(r0)
            r7.append(r2)
            java.lang.String r7 = r7.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r7 = r7.toString()
            r0.<init>(r7)
            throw r0
        L_0x00a6:
            com.fossil.lm7 r7 = com.fossil.qk7.d
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk7.g(java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    public final String k(Object obj) {
        if (obj instanceof c) {
            c cVar = (c) obj;
            if (cVar.e()) {
                return "Cancelling";
            }
            if (cVar.f()) {
                return "Completing";
            }
            return "Active";
        } else if (!(obj instanceof dk7)) {
            return obj instanceof li7 ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_CANCELLED : AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_COMPLETED;
        } else {
            if (((dk7) obj).isActive()) {
                return "Active";
            }
            return "New";
        }
    }

    @DexIgnore
    public final void a(gi7 gi7) {
        this._parentHandle = gi7;
    }

    @DexIgnore
    public final Object d(fb7<Object> fb7) {
        Object h;
        do {
            h = h();
            if (!(h instanceof dk7)) {
                if (!(h instanceof li7)) {
                    return qk7.b(h);
                }
                Throwable th = ((li7) h).a;
                if (!dj7.d()) {
                    throw th;
                } else if (!(fb7 instanceof sb7)) {
                    throw th;
                } else {
                    throw km7.b(th, (sb7) fb7);
                }
            }
        } while (j(h) < 0);
        return e(fb7);
    }

    @DexIgnore
    public final void a(ik7 ik7) {
        if (dj7.a()) {
            if (!(g() == null)) {
                throw new AssertionError();
            }
        }
        if (ik7 == null) {
            a((gi7) vk7.a);
            return;
        }
        ik7.start();
        gi7 a2 = ik7.a(this);
        a(a2);
        if (i()) {
            a2.dispose();
            a((gi7) vk7.a);
        }
    }

    @DexIgnore
    public final Object h(Object obj) {
        Object b2;
        do {
            b2 = b(h(), obj);
            if (b2 == qk7.a) {
                throw new IllegalStateException("Job " + this + " is already complete or completing, " + "but is being completed with " + obj, f(obj));
            }
        } while (b2 == qk7.c);
        return b2;
    }

    @DexIgnore
    public final boolean c(Object obj) {
        Object a2 = qk7.a;
        if (f() && (a2 = d(obj)) == qk7.b) {
            return true;
        }
        if (a2 == qk7.a) {
            a2 = g(obj);
        }
        if (a2 == qk7.a || a2 == qk7.b) {
            return true;
        }
        if (a2 == qk7.d) {
            return false;
        }
        a(a2);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public final CancellationException b() {
        Object h = h();
        if (h instanceof c) {
            Throwable d2 = ((c) h).d();
            if (d2 != null) {
                CancellationException a2 = a(d2, ej7.a(this) + " is cancelling");
                if (a2 != null) {
                    return a2;
                }
            }
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (h instanceof dk7) {
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (h instanceof li7) {
            return a(this, ((li7) h).a, null, 1, null);
        } else {
            return new jk7(ej7.a(this) + " has completed normally", null, this);
        }
    }

    @DexIgnore
    public final Throwable e(Object obj) {
        if (obj != null ? obj instanceof Throwable : true) {
            if (obj != null) {
                return (Throwable) obj;
            }
            return new jk7(c(), null, this);
        } else if (obj != null) {
            return ((xk7) obj).e();
        } else {
            throw new x87("null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
        }
    }

    @DexIgnore
    public final /* synthetic */ Object e(fb7<Object> fb7) {
        a aVar = new a(mb7.a(fb7), this);
        di7.a(aVar, b((gd7<? super Throwable, i97>) new zk7(this, aVar)));
        Object g = aVar.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    public final Object a(c cVar, Object obj) {
        boolean e;
        Throwable a2;
        boolean z = true;
        if (dj7.a()) {
            if (!(h() == cVar)) {
                throw new AssertionError();
            }
        }
        if (dj7.a() && !(!cVar.g())) {
            throw new AssertionError();
        } else if (!dj7.a() || cVar.f()) {
            li7 li7 = (li7) (!(obj instanceof li7) ? null : obj);
            Throwable th = li7 != null ? li7.a : null;
            synchronized (cVar) {
                e = cVar.e();
                List<Throwable> b2 = cVar.b(th);
                a2 = a(cVar, (List<? extends Throwable>) b2);
                if (a2 != null) {
                    a(a2, b2);
                }
            }
            if (!(a2 == null || a2 == th)) {
                obj = new li7(a2, false, 2, null);
            }
            if (a2 != null) {
                if (!c(a2) && !e(a2)) {
                    z = false;
                }
                if (z) {
                    if (obj != null) {
                        ((li7) obj).b();
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                    }
                }
            }
            if (!e) {
                g(a2);
            }
            i(obj);
            boolean compareAndSet = a.compareAndSet(this, cVar, qk7.a(obj));
            if (!dj7.a() || compareAndSet) {
                a((dk7) cVar, obj);
                return obj;
            }
            throw new AssertionError();
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public final rj7 b(gd7<? super Throwable, i97> gd7) {
        return a(false, true, gd7);
    }

    @DexIgnore
    public final Object d(Object obj) {
        Object b2;
        do {
            Object h = h();
            if (!(h instanceof dk7) || ((h instanceof c) && ((c) h).f())) {
                return qk7.a;
            }
            b2 = b(h, new li7(e(obj), false, 2, null));
        } while (b2 == qk7.c);
        return b2;
    }

    @DexIgnore
    public void b(Throwable th) {
        c((Object) th);
    }

    @DexIgnore
    public final uk7 b(dk7 dk7) {
        uk7 a2 = dk7.a();
        if (a2 != null) {
            return a2;
        }
        if (dk7 instanceof uj7) {
            return new uk7();
        }
        if (dk7 instanceof ok7) {
            a((ok7) dk7);
            return null;
        }
        throw new IllegalStateException(("State should have list: " + dk7).toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x006d, code lost:
        if (r2 == null) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006f, code lost:
        a(r0, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0072, code lost:
        r7 = a(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0076, code lost:
        if (r7 == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007c, code lost:
        if (b(r1, r7, r8) == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0080, code lost:
        return com.fossil.qk7.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0085, code lost:
        return a(r1, r8);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.dk7 r7, java.lang.Object r8) {
        /*
            r6 = this;
            com.fossil.uk7 r0 = r6.b(r7)
            if (r0 == 0) goto L_0x0089
            boolean r1 = r7 instanceof com.fossil.pk7.c
            r2 = 0
            if (r1 != 0) goto L_0x000d
            r1 = r2
            goto L_0x000e
        L_0x000d:
            r1 = r7
        L_0x000e:
            com.fossil.pk7$c r1 = (com.fossil.pk7.c) r1
            if (r1 == 0) goto L_0x0013
            goto L_0x0019
        L_0x0013:
            com.fossil.pk7$c r1 = new com.fossil.pk7$c
            r3 = 0
            r1.<init>(r0, r3, r2)
        L_0x0019:
            monitor-enter(r1)
            boolean r3 = r1.f()     // Catch:{ all -> 0x0086 }
            if (r3 == 0) goto L_0x0026
            com.fossil.lm7 r7 = com.fossil.qk7.a     // Catch:{ all -> 0x0086 }
            monitor-exit(r1)
            return r7
        L_0x0026:
            r3 = 1
            r1.a(r3)
            if (r1 == r7) goto L_0x003a
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r4 = com.fossil.pk7.a
            boolean r4 = r4.compareAndSet(r6, r7, r1)
            if (r4 != 0) goto L_0x003a
            com.fossil.lm7 r7 = com.fossil.qk7.c
            monitor-exit(r1)
            return r7
        L_0x003a:
            boolean r4 = com.fossil.dj7.a()
            if (r4 == 0) goto L_0x004e
            boolean r4 = r1.g()
            r4 = r4 ^ r3
            if (r4 == 0) goto L_0x0048
            goto L_0x004e
        L_0x0048:
            java.lang.AssertionError r7 = new java.lang.AssertionError
            r7.<init>()
            throw r7
        L_0x004e:
            boolean r4 = r1.e()
            boolean r5 = r8 instanceof com.fossil.li7
            if (r5 != 0) goto L_0x0058
            r5 = r2
            goto L_0x0059
        L_0x0058:
            r5 = r8
        L_0x0059:
            com.fossil.li7 r5 = (com.fossil.li7) r5
            if (r5 == 0) goto L_0x0062
            java.lang.Throwable r5 = r5.a
            r1.a(r5)
        L_0x0062:
            java.lang.Throwable r5 = r1.d()
            r3 = r3 ^ r4
            if (r3 == 0) goto L_0x006a
            r2 = r5
        L_0x006a:
            com.fossil.i97 r3 = com.fossil.i97.a
            monitor-exit(r1)
            if (r2 == 0) goto L_0x0072
            r6.a(r0, r2)
        L_0x0072:
            com.fossil.hi7 r7 = r6.a(r7)
            if (r7 == 0) goto L_0x0081
            boolean r7 = r6.b(r1, r7, r8)
            if (r7 == 0) goto L_0x0081
            com.fossil.lm7 r7 = com.fossil.qk7.b
            return r7
        L_0x0081:
            java.lang.Object r7 = r6.a(r1, r8)
            return r7
        L_0x0086:
            r7 = move-exception
            monitor-exit(r1)
            throw r7
        L_0x0089:
            com.fossil.lm7 r7 = com.fossil.qk7.c
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk7.c(com.fossil.dk7, java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    public final Object b(Object obj, Object obj2) {
        if (!(obj instanceof dk7)) {
            return qk7.a;
        }
        if ((!(obj instanceof uj7) && !(obj instanceof ok7)) || (obj instanceof hi7) || (obj2 instanceof li7)) {
            return c((dk7) obj, obj2);
        }
        if (b((dk7) obj, obj2)) {
            return obj2;
        }
        return qk7.c;
    }

    @DexIgnore
    public final boolean b(c cVar, hi7 hi7, Object obj) {
        while (ik7.a.a(hi7.e, false, false, new b(this, cVar, hi7, obj), 1, null) == vk7.a) {
            hi7 = a((bm7) hi7);
            if (hi7 == null) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Throwable a(c cVar, List<? extends Throwable> list) {
        T t;
        boolean z;
        T t2 = null;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (!(t instanceof CancellationException)) {
                    break;
                }
            }
            T t3 = t;
            if (t3 != null) {
                return t3;
            }
            Throwable th = (Throwable) list.get(0);
            if (th instanceof il7) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    T t4 = next;
                    if (t4 == th || !(t4 instanceof il7)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t2 = next;
                        break;
                    }
                }
                T t5 = t2;
                if (t5 != null) {
                    return t5;
                }
            }
            return th;
        } else if (cVar.e()) {
            return new jk7(c(), null, this);
        } else {
            return null;
        }
    }

    @DexIgnore
    public final void b(uk7 uk7, Throwable th) {
        Object b2 = uk7.b();
        if (b2 != null) {
            qi7 qi7 = null;
            for (bm7 bm7 = (bm7) b2; !ee7.a(bm7, uk7); bm7 = bm7.c()) {
                if (bm7 instanceof ok7) {
                    ok7 ok7 = (ok7) bm7;
                    try {
                        ok7.b(th);
                    } catch (Throwable th2) {
                        if (qi7 != null) {
                            i87.a(qi7, th2);
                            if (qi7 != null) {
                            }
                        }
                        qi7 = new qi7("Exception in completion handler " + ok7 + " for " + this, th2);
                        i97 i97 = i97.a;
                    }
                }
            }
            if (qi7 != null) {
                f((Throwable) qi7);
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final void a(Throwable th, List<? extends Throwable> list) {
        if (list.size() > 1) {
            Set newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(list.size()));
            Throwable b2 = !dj7.d() ? th : km7.b(th);
            for (Throwable th2 : list) {
                if (dj7.d()) {
                    th2 = km7.b(th2);
                }
                if (th2 != th && th2 != b2 && !(th2 instanceof CancellationException) && newSetFromMap.add(th2)) {
                    i87.a(th, th2);
                }
            }
        }
    }

    @DexIgnore
    public final void b(ok7<?> ok7) {
        Object h;
        do {
            h = h();
            if (h instanceof ok7) {
                if (h != ok7) {
                    return;
                }
            } else if ((h instanceof dk7) && ((dk7) h).a() != null) {
                ok7.g();
                return;
            } else {
                return;
            }
        } while (!a.compareAndSet(this, h, qk7.g));
    }

    @DexIgnore
    public final void a(dk7 dk7, Object obj) {
        gi7 g = g();
        if (g != null) {
            g.dispose();
            a((gi7) vk7.a);
        }
        Throwable th = null;
        if (!(obj instanceof li7)) {
            obj = null;
        }
        li7 li7 = (li7) obj;
        if (li7 != null) {
            th = li7.a;
        }
        if (dk7 instanceof ok7) {
            try {
                ((ok7) dk7).b(th);
            } catch (Throwable th2) {
                f((Throwable) new qi7("Exception in completion handler " + dk7 + " for " + this, th2));
            }
        } else {
            uk7 a2 = dk7.a();
            if (a2 != null) {
                b(a2, th);
            }
        }
    }

    @DexIgnore
    public final void a(uk7 uk7, Throwable th) {
        g(th);
        Object b2 = uk7.b();
        if (b2 != null) {
            qi7 qi7 = null;
            for (bm7 bm7 = (bm7) b2; !ee7.a(bm7, uk7); bm7 = bm7.c()) {
                if (bm7 instanceof kk7) {
                    ok7 ok7 = (ok7) bm7;
                    try {
                        ok7.b(th);
                    } catch (Throwable th2) {
                        if (qi7 != null) {
                            i87.a(qi7, th2);
                            if (qi7 != null) {
                            }
                        }
                        qi7 = new qi7("Exception in completion handler " + ok7 + " for " + this, th2);
                        i97 i97 = i97.a;
                    }
                }
            }
            if (qi7 != null) {
                f((Throwable) qi7);
            }
            c(th);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public static /* synthetic */ CancellationException a(pk7 pk7, Throwable th, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = null;
            }
            return pk7.a(th, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }

    @DexIgnore
    public final CancellationException a(Throwable th, String str) {
        CancellationException cancellationException = (CancellationException) (!(th instanceof CancellationException) ? null : th);
        if (cancellationException == null) {
            if (str == null) {
                str = c();
            }
            cancellationException = new jk7(str, th, this);
        }
        return cancellationException;
    }

    @DexIgnore
    public final ok7<?> a(gd7<? super Throwable, i97> gd7, boolean z) {
        boolean z2 = true;
        kk7 kk7 = null;
        if (z) {
            if (gd7 instanceof kk7) {
                kk7 = gd7;
            }
            kk7 kk72 = kk7;
            if (kk72 != null) {
                if (dj7.a()) {
                    if (((ok7) kk72).d != this) {
                        z2 = false;
                    }
                    if (!z2) {
                        throw new AssertionError();
                    }
                }
                if (kk72 != null) {
                    return kk72;
                }
            }
            return new gk7(this, gd7);
        }
        if (gd7 instanceof ok7) {
            kk7 = gd7;
        }
        ok7<?> ok7 = kk7;
        if (ok7 != null) {
            if (dj7.a()) {
                if (ok7.d != this || (ok7 instanceof kk7)) {
                    z2 = false;
                }
                if (!z2) {
                    throw new AssertionError();
                }
            }
            if (ok7 != null) {
                return ok7;
            }
        }
        return new hk7(this, gd7);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [com.fossil.ck7] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.uj7 r3) {
        /*
            r2 = this;
            com.fossil.uk7 r0 = new com.fossil.uk7
            r0.<init>()
            boolean r1 = r3.isActive()
            if (r1 == 0) goto L_0x000c
            goto L_0x0012
        L_0x000c:
            com.fossil.ck7 r1 = new com.fossil.ck7
            r1.<init>(r0)
            r0 = r1
        L_0x0012:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = com.fossil.pk7.a
            r1.compareAndSet(r2, r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk7.a(com.fossil.uj7):void");
    }

    @DexIgnore
    public final void a(ok7<?> ok7) {
        ok7.a(new uk7());
        a.compareAndSet(this, ok7, ok7.c());
    }

    @DexIgnore
    @Override // com.fossil.ii7
    public final void a(xk7 xk7) {
        c(xk7);
    }

    @DexIgnore
    public final boolean a(Throwable th) {
        return c((Object) th);
    }

    @DexIgnore
    public final boolean a(dk7 dk7, Throwable th) {
        if (dj7.a() && !(!(dk7 instanceof c))) {
            throw new AssertionError();
        } else if (!dj7.a() || dk7.isActive()) {
            uk7 b2 = b(dk7);
            if (b2 == null) {
                return false;
            }
            if (!a.compareAndSet(this, dk7, new c(b2, false, th))) {
                return false;
            }
            a(b2, th);
            return true;
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final hi7 a(dk7 dk7) {
        hi7 hi7 = (hi7) (!(dk7 instanceof hi7) ? null : dk7);
        if (hi7 != null) {
            return hi7;
        }
        uk7 a2 = dk7.a();
        if (a2 != null) {
            return a((bm7) a2);
        }
        return null;
    }

    @DexIgnore
    public final void a(c cVar, hi7 hi7, Object obj) {
        if (dj7.a()) {
            if (!(h() == cVar)) {
                throw new AssertionError();
            }
        }
        hi7 a2 = a((bm7) hi7);
        if (a2 == null || !b(cVar, a2, obj)) {
            a(a(cVar, obj));
        }
    }

    @DexIgnore
    public final hi7 a(bm7 bm7) {
        while (bm7.f()) {
            bm7 = bm7.d();
        }
        while (true) {
            bm7 = bm7.c();
            if (!bm7.f()) {
                if (bm7 instanceof hi7) {
                    return (hi7) bm7;
                }
                if (bm7 instanceof uk7) {
                    return null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public final gi7 a(ii7 ii7) {
        rj7 a2 = ik7.a.a(this, true, false, new hi7(this, ii7), 2, null);
        if (a2 != null) {
            return (gi7) a2;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public final rj7 a(boolean z, boolean z2, gd7<? super Throwable, i97> gd7) {
        Throwable th;
        Throwable th2 = null;
        ok7<?> ok7 = null;
        while (true) {
            Object h = h();
            if (h instanceof uj7) {
                uj7 uj7 = (uj7) h;
                if (uj7.isActive()) {
                    if (ok7 == null) {
                        ok7 = a(gd7, z);
                    }
                    if (a.compareAndSet(this, h, ok7)) {
                        return ok7;
                    }
                } else {
                    a(uj7);
                }
            } else if (h instanceof dk7) {
                uk7 a2 = ((dk7) h).a();
                if (a2 != null) {
                    rj7 rj7 = vk7.a;
                    if (!z || !(h instanceof c)) {
                        th = null;
                    } else {
                        synchronized (h) {
                            th = ((c) h).d();
                            if (th == null || ((gd7 instanceof hi7) && !((c) h).f())) {
                                if (ok7 == null) {
                                    ok7 = a(gd7, z);
                                }
                                if (a(h, a2, ok7)) {
                                    if (th == null) {
                                        return ok7;
                                    }
                                    rj7 = ok7;
                                }
                            }
                            i97 i97 = i97.a;
                        }
                    }
                    if (th != null) {
                        if (z2) {
                            gd7.invoke(th);
                        }
                        return rj7;
                    }
                    if (ok7 == null) {
                        ok7 = a(gd7, z);
                    }
                    if (a(h, a2, ok7)) {
                        return ok7;
                    }
                } else if (h != null) {
                    a((ok7) h);
                } else {
                    throw new x87("null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>");
                }
            } else {
                if (z2) {
                    if (!(h instanceof li7)) {
                        h = null;
                    }
                    li7 li7 = (li7) h;
                    if (li7 != null) {
                        th2 = li7.a;
                    }
                    gd7.invoke(th2);
                }
                return vk7.a;
            }
        }
    }

    @DexIgnore
    public final boolean a(Object obj, uk7 uk7, ok7<?> ok7) {
        int a2;
        d dVar = new d(ok7, ok7, this, obj);
        do {
            a2 = uk7.d().a(ok7, uk7, dVar);
            if (a2 == 1) {
                return true;
            }
        } while (a2 != 2);
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ik7
    public void a(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new jk7(c(), null, this);
        }
        b(cancellationException);
    }
}
