package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im7 {
    @DexIgnore
    public /* final */ bm7 a;

    @DexIgnore
    public im7(bm7 bm7) {
        this.a = bm7;
    }

    @DexIgnore
    public String toString() {
        return "Removed[" + this.a + ']';
    }
}
