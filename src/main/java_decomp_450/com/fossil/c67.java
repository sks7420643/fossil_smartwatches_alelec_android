package com.fossil;

import android.content.Context;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c67 {
    @DexIgnore
    public static volatile c67 c;
    @DexIgnore
    public Timer a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public c67(Context context) {
        this.b = context.getApplicationContext();
        this.a = new Timer(false);
    }

    @DexIgnore
    public static c67 a(Context context) {
        if (c == null) {
            synchronized (c67.class) {
                if (c == null) {
                    c = new c67(context);
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void a() {
        if (w37.o() == x37.PERIOD) {
            long l = (long) (w37.l() * 60 * 1000);
            if (w37.q()) {
                k57 b2 = v57.b();
                b2.e("setupPeriodTimer delay:" + l);
            }
            a(new d67(this), l);
        }
    }

    @DexIgnore
    public void a(TimerTask timerTask, long j) {
        if (this.a != null) {
            if (w37.q()) {
                k57 b2 = v57.b();
                b2.e("setupPeriodTimer schedule delay:" + j);
            }
            this.a.schedule(timerTask, j);
        } else if (w37.q()) {
            v57.b().g("setupPeriodTimer schedule timer == null");
        }
    }
}
