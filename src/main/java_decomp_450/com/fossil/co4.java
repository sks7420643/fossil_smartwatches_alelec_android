package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface co4 {
    @DexIgnore
    void a();

    @DexIgnore
    Long[] a(List<ao4> list);

    @DexIgnore
    LiveData<List<ao4>> b();
}
