package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij5 implements Factory<hj5> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public ij5(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static ij5 a(Provider<PortfolioApp> provider, Provider<AuthApiGuestService> provider2, Provider<ch5> provider3) {
        return new ij5(provider, provider2, provider3);
    }

    @DexIgnore
    public static hj5 a(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, ch5 ch5) {
        return new hj5(portfolioApp, authApiGuestService, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public hj5 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
