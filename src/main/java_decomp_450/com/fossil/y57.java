package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y57 {
    @DexIgnore
    public static int a; // = -1;

    @DexIgnore
    public static boolean a() {
        int i = a;
        if (i == 1) {
            return true;
        }
        if (i == 0) {
            return false;
        }
        String[] strArr = {"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
        int i2 = 0;
        while (i2 < 6) {
            try {
                if (new File(strArr[i2] + "su").exists()) {
                    a = 1;
                    return true;
                }
                i2++;
            } catch (Exception unused) {
            }
        }
        a = 0;
        return false;
    }
}
