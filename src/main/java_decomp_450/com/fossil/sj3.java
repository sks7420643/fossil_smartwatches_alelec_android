package com.fossil;

import java.net.URL;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sj3 implements Runnable {
    @DexIgnore
    public /* final */ URL a;
    @DexIgnore
    public /* final */ tj3 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Map<String, String> d; // = null;
    @DexIgnore
    public /* final */ /* synthetic */ qj3 e;

    @DexIgnore
    public sj3(qj3 qj3, String str, URL url, byte[] bArr, Map<String, String> map, tj3 tj3) {
        this.e = qj3;
        a72.b(str);
        a72.a(url);
        a72.a(tj3);
        this.a = url;
        this.b = tj3;
        this.c = str;
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, Exception exc, byte[] bArr, Map map) {
        this.b.a(this.c, i, exc, bArr, map);
    }

    @DexIgnore
    public final void b(int i, Exception exc, byte[] bArr, Map<String, List<String>> map) {
        this.e.c().a(new vj3(this, i, exc, bArr, map));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            com.fossil.qj3 r0 = r7.e
            r0.d()
            r0 = 0
            r1 = 0
            com.fossil.qj3 r2 = r7.e     // Catch:{ IOException -> 0x006c, all -> 0x0060 }
            java.net.URL r3 = r7.a     // Catch:{ IOException -> 0x006c, all -> 0x0060 }
            java.net.HttpURLConnection r2 = r2.a(r3)     // Catch:{ IOException -> 0x006c, all -> 0x0060 }
            java.util.Map<java.lang.String, java.lang.String> r3 = r7.d     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            if (r3 == 0) goto L_0x0039
            java.util.Map<java.lang.String, java.lang.String> r3 = r7.d     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.util.Set r3 = r3.entrySet()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
        L_0x001d:
            boolean r4 = r3.hasNext()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            if (r4 == 0) goto L_0x0039
            java.lang.Object r4 = r3.next()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.lang.Object r5 = r4.getKey()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.lang.Object r4 = r4.getValue()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            r2.addRequestProperty(r5, r4)     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            goto L_0x001d
        L_0x0039:
            int r1 = r2.getResponseCode()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.util.Map r3 = r2.getHeaderFields()     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            com.fossil.qj3 r4 = r7.e     // Catch:{ IOException -> 0x0055, all -> 0x0050 }
            byte[] r4 = com.fossil.qj3.a(r2)     // Catch:{ IOException -> 0x0055, all -> 0x0050 }
            if (r2 == 0) goto L_0x004c
            r2.disconnect()
        L_0x004c:
            r7.b(r1, r0, r4, r3)
            return
        L_0x0050:
            r4 = move-exception
            r6 = r4
            r4 = r3
            r3 = r6
            goto L_0x0063
        L_0x0055:
            r4 = move-exception
            r6 = r4
            r4 = r3
            r3 = r6
            goto L_0x006f
        L_0x005a:
            r3 = move-exception
            r4 = r0
            goto L_0x0063
        L_0x005d:
            r3 = move-exception
            r4 = r0
            goto L_0x006f
        L_0x0060:
            r3 = move-exception
            r2 = r0
            r4 = r2
        L_0x0063:
            if (r2 == 0) goto L_0x0068
            r2.disconnect()
        L_0x0068:
            r7.b(r1, r0, r0, r4)
            throw r3
        L_0x006c:
            r3 = move-exception
            r2 = r0
            r4 = r2
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.disconnect()
        L_0x0074:
            r7.b(r1, r3, r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sj3.run():void");
    }
}
