package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface l83 extends IInterface {
    @DexIgnore
    b73 a(ab2 ab2, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    y63 a(ab2 ab2, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    void a(ab2 ab2, int i) throws RemoteException;

    @DexIgnore
    x63 f(ab2 ab2) throws RemoteException;

    @DexIgnore
    v63 zze() throws RemoteException;

    @DexIgnore
    ym2 zzf() throws RemoteException;
}
