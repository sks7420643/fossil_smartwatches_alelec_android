package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.zx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.WechatToken;
import com.portfolio.platform.data.source.remote.WechatApiService;
import java.net.SocketTimeoutException;
import okhttp3.Interceptor;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kh5 implements k37 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public WechatApiService a; // = ((WechatApiService) kj5.g.a(WechatApiService.class));
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kh5.c;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ kh5 a;
        @DexIgnore
        public /* final */ /* synthetic */ mh5 b;

        @DexIgnore
        public b(kh5 kh5, mh5 mh5) {
            this.a = kh5;
            this.b = mh5;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.b()) {
                this.b.a(500, null, "");
            }
        }
    }

    /*
    static {
        String simpleName = kh5.class.getSimpleName();
        ee7.a((Object) simpleName, "MFLoginWechatManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public kh5() {
        kj5.g.a("https://api.weixin.qq.com/sns/");
        kj5.g.a((Interceptor) null);
    }

    @DexIgnore
    public final WechatApiService a() {
        return this.a;
    }

    @DexIgnore
    public final void a(String str) {
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "appId");
        zx6.a().a(str);
    }

    @DexIgnore
    public final void a(Intent intent) {
        ee7.b(intent, "intent");
        zx6.a().a(intent, this);
    }

    @DexIgnore
    public final void a(Activity activity, mh5 mh5) {
        ee7.b(activity, Constants.ACTIVITY);
        ee7.b(mh5, Constants.CALLBACK);
        zx6.a().a(activity);
        zx6.a().a(activity.getIntent(), this);
        this.b = false;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new b(this, mh5), 120000);
        zx6.a().a(new c(this, handler, mh5));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements zx6.a {
        @DexIgnore
        public /* final */ /* synthetic */ kh5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ mh5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1", f = "MFLoginWechatManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $authToken;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kh5$c$a$a")
            /* renamed from: com.fossil.kh5$c$a$a  reason: collision with other inner class name */
            public static final class C0097a implements ru7<WechatToken> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public C0097a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ru7
                public void onFailure(Call<WechatToken> call, Throwable th) {
                    ee7.b(call, "call");
                    ee7.b(th, "t");
                    FLogger.INSTANCE.getLocal().d(kh5.d.a(), "getWechatToken onFailure");
                    if (th instanceof SocketTimeoutException) {
                        this.a.this$0.c.a(MFNetworkReturnCode.CLIENT_TIMEOUT, null, "");
                    } else {
                        this.a.this$0.c.a(601, null, "");
                    }
                }

                @DexIgnore
                @Override // com.fossil.ru7
                public void onResponse(Call<WechatToken> call, fv7<WechatToken> fv7) {
                    ee7.b(call, "call");
                    ee7.b(fv7, "response");
                    if (fv7.d()) {
                        FLogger.INSTANCE.getLocal().d(kh5.d.a(), "getWechatToken isSuccessful");
                        WechatToken a2 = fv7.a();
                        if (a2 != null) {
                            ee7.a((Object) a2, "response.body()!!");
                            WechatToken wechatToken = a2;
                            this.a.this$0.a.a(wechatToken.getOpenId());
                            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                            signUpSocialAuth.setToken(wechatToken.getAccessToken());
                            signUpSocialAuth.setService("wechat");
                            this.a.this$0.c.a(signUpSocialAuth);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(kh5.d.a(), "getWechatToken isNotSuccessful");
                    this.a.this$0.c.a(600, null, fv7.e());
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$authToken = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$authToken, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                String str;
                String e;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Access a = ng5.a().a(PortfolioApp.g0.c());
                    WechatApiService a2 = this.this$0.a.a();
                    String str2 = "";
                    if (a == null || (str = a.getD()) == null) {
                        str = str2;
                    }
                    if (!(a == null || (e = a.getE()) == null)) {
                        str2 = e;
                    }
                    a2.getWechatToken(str, str2, this.$authToken, "authorization_code").a(new C0097a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public c(kh5 kh5, Handler handler, mh5 mh5) {
            this.a = kh5;
            this.b = handler;
            this.c = mh5;
        }

        @DexIgnore
        @Override // com.fossil.zx6.a
        public void a(String str) {
            ee7.b(str, "authToken");
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kh5.d.a();
            local.d(a2, "Wechat onAuthSuccess: " + str);
            FLogger.INSTANCE.getLocal().d(kh5.d.a(), "Wechat step 1: Login using wechat success");
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, str, null), 3, null);
        }

        @DexIgnore
        @Override // com.fossil.zx6.a
        public void b() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(kh5.d.a(), "Wechat onAuthAppNotInstalled");
            this.c.a(600, null, "");
        }

        @DexIgnore
        @Override // com.fossil.zx6.a
        public void c() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(kh5.d.a(), "Wechat onAuthCancel");
            this.c.a(2, null, "");
        }

        @DexIgnore
        @Override // com.fossil.zx6.a
        public void a() {
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(kh5.d.a(), "Wechat onAuthFailed");
            this.c.a(600, null, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.k37
    public void a(u27 u27) {
        ee7.b(u27, "baseReq");
        zx6.a().a(u27);
    }

    @DexIgnore
    @Override // com.fossil.k37
    public void a(v27 v27) {
        ee7.b(v27, "baseResp");
        zx6.a().a(v27);
    }
}
