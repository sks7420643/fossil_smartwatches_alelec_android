package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.be5;
import com.fossil.do5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be6 extends go5 implements ae6 {
    @DexIgnore
    public qw6<i65> f;
    @DexIgnore
    public zd6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "SleepOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        i65 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        qw6<i65> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            be5.a aVar = be5.o;
            zd6 zd6 = this.g;
            if (aVar.a(zd6 != null ? zd6.h() : null)) {
                overviewSleepWeekChart.a("dianaSleepTab", "nonBrandNonReachGoal");
            } else {
                overviewSleepWeekChart.a("hybridSleepTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        i65 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onCreateView");
        this.f = new qw6<>(this, (i65) qb.a(layoutInflater, 2131558626, viewGroup, false, a1()));
        f1();
        qw6<i65> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onResume");
        f1();
        zd6 zd6 = this.g;
        if (zd6 != null) {
            zd6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onStop");
        zd6 zd6 = this.g;
        if (zd6 != null) {
            zd6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.ae6
    public void a(do5 do5) {
        i65 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        ee7.b(do5, "baseModel");
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "showWeekDetails");
        qw6<i65> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            do5.a aVar = do5.a;
            ee7.a((Object) overviewSleepWeekChart, "it");
            Context context = overviewSleepWeekChart.getContext();
            ee7.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewSleepWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewSleepWeekChart.a(do5);
        }
    }

    @DexIgnore
    public void a(zd6 zd6) {
        ee7.b(zd6, "presenter");
        this.g = zd6;
    }
}
