package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rc6 extends go5 implements qc6 {
    @DexIgnore
    public qw6<a25> f;
    @DexIgnore
    public pc6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewHeartRateCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ rc6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(rc6 rc6) {
            this.a = rc6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.c
        public void a(Calendar calendar) {
            ee7.b(calendar, "calendar");
            pc6 a2 = rc6.a(this.a);
            Date time = calendar.getTime();
            ee7.a((Object) time, "calendar.time");
            a2.a(time);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewHeartRateCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ rc6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(rc6 rc6) {
            this.a = rc6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.b
        public void a(int i, Calendar calendar) {
            ee7.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.A;
                Date time = calendar.getTime();
                ee7.a((Object) time, "it.time");
                ee7.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ pc6 a(rc6 rc6) {
        pc6 pc6 = rc6.g;
        if (pc6 != null) {
            return pc6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        a25 a25 = (a25) qb.a(layoutInflater, 2131558565, viewGroup, false, a1());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = a25.q;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        a25.q.setOnCalendarMonthChanged(new b(this));
        a25.q.setOnCalendarItemClickListener(new c(this));
        qw6<a25> qw6 = new qw6<>(this, a25);
        this.f = qw6;
        if (qw6 != null) {
            a25 a2 = qw6.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        pc6 pc6 = this.g;
        if (pc6 != null) {
            pc6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        pc6 pc6 = this.g;
        if (pc6 != null) {
            pc6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(pc6 pc6) {
        ee7.b(pc6, "presenter");
        this.g = pc6;
    }

    @DexIgnore
    @Override // com.fossil.qc6
    public void a(TreeMap<Long, Integer> treeMap) {
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar;
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar2;
        ee7.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        qw6<a25> qw6 = this.f;
        if (qw6 != null) {
            a25 a2 = qw6.a();
            if (!(a2 == null || (recyclerViewHeartRateCalendar2 = a2.q) == null)) {
                recyclerViewHeartRateCalendar2.setData(treeMap);
            }
            qw6<a25> qw62 = this.f;
            if (qw62 != null) {
                a25 a3 = qw62.a();
                if (a3 != null && (recyclerViewHeartRateCalendar = a3.q) != null) {
                    recyclerViewHeartRateCalendar.setEnableButtonNextAndPrevMonth(true);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qc6
    public void a(Date date, Date date2) {
        ee7.b(date, "selectDate");
        ee7.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        qw6<a25> qw6 = this.f;
        if (qw6 != null) {
            a25 a2 = qw6.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                ee7.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                ee7.a((Object) instance2, "startCalendar");
                instance2.setTime(zd5.q(date2));
                ee7.a((Object) instance3, "endCalendar");
                instance3.setTime(zd5.l(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
