package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wi1<K, V> {
    @DexIgnore
    public static /* final */ zg1 a; // = new zg1(null);

    @DexIgnore
    public final byte[] a(short s, r60 r60, K k) throws f41 {
        r60 r602;
        y71<K>[] a2 = a();
        int length = a2.length;
        y71<K> y71 = null;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            y71<K> y712 = a2[i];
            if (ee7.a(y712.a, r60)) {
                y71 = y712;
                break;
            }
            if (y712.a.getMajor() == r60.getMajor()) {
                if (y712.a.getMinor() >= ((y71 == null || (r602 = y71.a) == null) ? 0 : r602.getMinor())) {
                    y71 = y712;
                }
            }
            i++;
        }
        if (y71 != null) {
            return y71.a(s, k);
        }
        throw new f41(l21.UNSUPPORTED_VERSION, "Not support version " + r60 + '.', null, 4);
    }

    @DexIgnore
    public abstract y71<K>[] a();

    @DexIgnore
    public abstract uk1<V>[] b();

    @DexIgnore
    public final uk1<V> a(r60 r60) {
        r60 r602;
        uk1<V>[] b = b();
        uk1<V> uk1 = null;
        for (uk1<V> uk12 : b) {
            if (ee7.a(uk12.a, r60)) {
                return uk12;
            }
            if (uk12.a.getMajor() == r60.getMajor()) {
                if (uk12.a.getMinor() >= ((uk1 == null || (r602 = uk1.a) == null) ? 0 : r602.getMinor())) {
                    uk1 = uk12;
                }
            }
        }
        return uk1;
    }

    @DexIgnore
    public final V a(byte[] bArr) throws f41 {
        try {
            r60 r60 = new r60(bArr[2], bArr[3]);
            uk1<V> a2 = a(r60);
            if (a2 == null) {
                l21 l21 = l21.UNSUPPORTED_VERSION;
                throw new f41(l21, "Not support version " + r60 + '.', null, 4);
            } else if (a2.b(bArr)) {
                return a2.a(bArr);
            } else {
                throw new f41(l21.INVALID_FILE_DATA, "Invalid file.", null, 4);
            }
        } catch (Exception e) {
            throw new f41(l21.INVALID_FILE_DATA, "Invalid file data.", e);
        }
    }
}
