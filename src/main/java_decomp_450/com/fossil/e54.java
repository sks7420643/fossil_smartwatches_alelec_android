package com.fossil;

import com.fossil.v54;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e54 extends v54.c.b {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    @Override // com.fossil.v54.c.b
    public byte[] a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54.c.b
    public String b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.c.b)) {
            return false;
        }
        v54.c.b bVar = (v54.c.b) obj;
        if (this.a.equals(bVar.b())) {
            if (Arrays.equals(this.b, bVar instanceof e54 ? ((e54) bVar).b : bVar.a())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "File{filename=" + this.a + ", contents=" + Arrays.toString(this.b) + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.c.b.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.v54.c.b.a
        public v54.c.b.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null filename");
        }

        @DexIgnore
        @Override // com.fossil.v54.c.b.a
        public v54.c.b.a a(byte[] bArr) {
            if (bArr != null) {
                this.b = bArr;
                return this;
            }
            throw new NullPointerException("Null contents");
        }

        @DexIgnore
        @Override // com.fossil.v54.c.b.a
        public v54.c.b a() {
            String str = "";
            if (this.a == null) {
                str = str + " filename";
            }
            if (this.b == null) {
                str = str + " contents";
            }
            if (str.isEmpty()) {
                return new e54(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public e54(String str, byte[] bArr) {
        this.a = str;
        this.b = bArr;
    }
}
