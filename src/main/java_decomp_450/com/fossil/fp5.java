package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.be5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp5 extends RecyclerView.g<c> {
    @DexIgnore
    public List<r87<ShineDevice, String>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ iw b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, c cVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ View d;
        @DexIgnore
        public ShineDevice e;
        @DexIgnore
        public /* final */ /* synthetic */ fp5 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                ee7.b(str, "serial");
                ee7.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "set image for serial=" + str + " path " + str2);
                this.a.f.b.a(str2).a(((r40) new r40().b(2131231316)).h()).a(this.a.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                ee7.b(str, "fastPairId");
                ee7.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "setImageCallback() for wearOS, fastPairId=" + str);
                this.a.f.b.a(str2).a(((r40) new r40().b(2131231078)).h()).a(this.a.c);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fp5 fp5, View view) {
            super(view);
            ee7.b(view, "view");
            this.f = fp5;
            View findViewById = view.findViewById(2131362386);
            ee7.a((Object) findViewById, "view.findViewById(R.id.ftv_device_serial)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362385);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.ftv_device_name)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362662);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.iv_device_image)");
            this.c = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131363024);
            ee7.a((Object) findViewById4, "view.findViewById(R.id.scan_device_container)");
            this.d = findViewById4;
            findViewById4.setOnClickListener(this);
        }

        @DexIgnore
        public void onClick(View view) {
            b a2;
            ee7.b(view, "view");
            if (this.f.getItemCount() > getAdapterPosition() && getAdapterPosition() != -1 && (a2 = this.f.c) != null) {
                a2.a(view, this, getAdapterPosition());
            }
        }

        @DexIgnore
        public final void a(ShineDevice shineDevice, String str) {
            ee7.b(shineDevice, "shineDevice");
            ee7.b(str, "deviceName");
            this.e = shineDevice;
            this.b.setText(str);
            String serial = shineDevice.getSerial();
            ee7.a((Object) serial, "shineDevice.serial");
            boolean z = true;
            if (serial.length() > 0) {
                FlexibleTextView flexibleTextView = this.a;
                we7 we7 = we7.a;
                Locale locale = Locale.US;
                ee7.a((Object) locale, "Locale.US");
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131887166);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026 R.string.Serial_pattern)");
                String format = String.format(locale, a2, Arrays.copyOf(new Object[]{shineDevice.getSerial()}, 1));
                ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                flexibleTextView.setText(format);
                this.a.setVisibility(0);
            } else {
                this.a.setVisibility(4);
            }
            String serial2 = shineDevice.getSerial();
            ee7.a((Object) serial2, "shineDevice.serial");
            if (serial2.length() == 0) {
                String fastPairId = shineDevice.getFastPairId();
                ee7.a((Object) fastPairId, "shineDevice.fastPairId");
                if (fastPairId.length() != 0) {
                    z = false;
                }
                if (z) {
                    this.c.setImageDrawable(PortfolioApp.g0.c().getDrawable(2131231078));
                    return;
                }
            }
            a(shineDevice);
        }

        @DexIgnore
        public final void a(ShineDevice shineDevice) {
            String serial = shineDevice.getSerial();
            ee7.a((Object) serial, "shineDevice.serial");
            if (serial.length() > 0) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                String serial2 = shineDevice.getSerial();
                ee7.a((Object) serial2, "shineDevice.serial");
                CloudImageHelper.ItemImage type = with.setSerialNumber(serial2).setSerialPrefix(be5.o.b(shineDevice.getSerial())).setType(Constants.DeviceType.TYPE_LARGE);
                ImageView imageView = this.c;
                be5.a aVar = be5.o;
                String serial3 = shineDevice.getSerial();
                ee7.a((Object) serial3, "shineDevice.serial");
                type.setPlaceHolder(imageView, aVar.b(serial3, be5.b.LARGE)).setImageCallback(new a(this)).download();
                return;
            }
            CloudImageHelper.ItemImage with2 = CloudImageHelper.Companion.getInstance().with();
            String fastPairId = shineDevice.getFastPairId();
            ee7.a((Object) fastPairId, "shineDevice.fastPairId");
            with2.setFastPairId(fastPairId).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder(this.c, 2131231078).setImageCallback(new b(this)).downloadForWearOS();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<r87<? extends ShineDevice, ? extends String>> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        /* renamed from: a */
        public final int compare(r87<ShineDevice, String> r87, r87<ShineDevice, String> r872) {
            int i;
            if (r87 == null || r872 == null) {
                int i2 = -1;
                int i3 = r87 == null ? -1 : 1;
                if (r872 != null) {
                    i2 = 1;
                }
                i = i3 - i2;
            } else {
                i = r87.getFirst().getRssi() - r872.getFirst().getRssi();
            }
            return -i;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public fp5(iw iwVar, b bVar) {
        ee7.b(iwVar, "mRequestManager");
        this.b = iwVar;
        this.c = bVar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void a(List<r87<ShineDevice, String>> list) {
        ee7.b(list, "data");
        this.a.clear();
        aa7.a(list, d.a);
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558709, viewGroup, false);
        ee7.a((Object) inflate, "v");
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.width = (int) (((float) viewGroup.getMeasuredWidth()) * 0.5f);
        inflate.setLayoutParams(layoutParams);
        return new c(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        ee7.b(cVar, "viewHolder");
        cVar.a(this.a.get(i).getFirst(), this.a.get(i).getSecond());
    }
}
