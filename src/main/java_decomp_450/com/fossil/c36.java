package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c36 extends he {
    @DexIgnore
    public MutableLiveData<e> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<a> c; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<fz5> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ FilterResult a;

        @DexIgnore
        public a(FilterResult filterResult) {
            ee7.b(filterResult, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.a = filterResult;
        }

        @DexIgnore
        public final FilterResult a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && ee7.a(this.a, ((a) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            FilterResult filterResult = this.a;
            if (filterResult != null) {
                return filterResult.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "ApplyFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ List<FilterResult> a;

        @DexIgnore
        public b(List<FilterResult> list) {
            ee7.b(list, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.a = list;
        }

        @DexIgnore
        public final List<FilterResult> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && ee7.a(this.a, ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<FilterResult> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "BuildFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ float[] b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public /* final */ boolean h;

        @DexIgnore
        public d(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
            ee7.b(bitmap, "bitmap");
            ee7.b(fArr, "points");
            this.a = bitmap;
            this.b = fArr;
            this.c = i;
            this.d = z;
            this.e = i2;
            this.f = i3;
            this.g = z2;
            this.h = z3;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.f;
        }

        @DexIgnore
        public final Bitmap c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final boolean e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.h;
        }

        @DexIgnore
        public final float[] h() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public e() {
            this(false, null, 3, null);
        }

        @DexIgnore
        public e(boolean z, String str) {
            ee7.b(str, "data");
            this.a = z;
            this.b = str;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            return this.a == eVar.a && ee7.a(this.b, eVar.b);
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = i * 31;
            String str = this.b;
            return i3 + (str != null ? str.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "CropImageResult(isSuccess=" + this.a + ", data=" + this.b + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ e(boolean z, String str, int i, zd7 zd7) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? "" : str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1", f = "EditPhotoViewModel.kt", l = {76}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filter;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    EInkImageFilter create = EInkImageFilter.create();
                    ee7.a((Object) create, "EInkImageFilter.create()");
                    OutputSettings outputSettings = new OutputSettings(this.this$0.$bitmap.getWidth(), this.this$0.$bitmap.getHeight(), Format.RAW, false, false);
                    f fVar = this.this$0;
                    FilterResult apply = create.apply(fVar.$bitmap, fVar.$filter, false, false, outputSettings);
                    ee7.a((Object) apply, "algorithm.apply(bitmap, \u2026, false, false, settings)");
                    this.this$0.this$0.a().a(new a(apply));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(c36 c36, Bitmap bitmap, FilterType filterType, fb7 fb7) {
            super(2, fb7);
            this.this$0 = c36;
            this.$bitmap = bitmap;
            this.$filter = filterType;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$bitmap, this.$filter, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1", f = "EditPhotoViewModel.kt", l = {59}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filterList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1$filters$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ArrayList<FilterResult>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ArrayList<FilterResult>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    d dVar = this.this$0.$cropImageInfo;
                    Bitmap a = jr5.a(jr5.a(dVar.c(), dVar.h(), dVar.d(), dVar.e(), dVar.a(), dVar.b(), dVar.f(), dVar.g()).a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    ee7.a((Object) create, "EInkImageFilter.create()");
                    ee7.a((Object) a, "bitmap");
                    return create.applyFilters(a, this.this$0.$filterList, false, false, new OutputSettings(a.getWidth(), a.getHeight(), Format.RAW, false, false));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(c36 c36, d dVar, ArrayList arrayList, fb7 fb7) {
            super(2, fb7);
            this.this$0 = c36;
            this.$cropImageInfo = dVar;
            this.$filterList = arrayList;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$cropImageInfo, this.$filterList, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ee7.a(obj, "withContext(IO) {\n      \u2026          }\n            }");
            this.this$0.b().a(new b((ArrayList) obj));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1", f = "EditPhotoViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ d $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filterType;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ c36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    d dVar = this.this$0.$cropImageInfo;
                    Bitmap a = jr5.a(jr5.a(dVar.c(), dVar.h(), dVar.d(), dVar.e(), dVar.a(), dVar.b(), dVar.f(), dVar.g()).a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    ee7.a((Object) create, "EInkImageFilter.create()");
                    create.apply(a, this.this$0.$filterType, false, false, OutputSettings.BACKGROUND);
                    jr5.a(this.this$0.$context, FileUtils.getDirectory(this.this$0.$context, FileType.WATCH_FACE) + File.separator, "previewPhoto", a);
                    this.this$0.this$0.d().a(new e(true, null, 2, null));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(c36 c36, d dVar, FilterType filterType, Context context, fb7 fb7) {
            super(2, fb7);
            this.this$0 = c36;
            this.$cropImageInfo = dVar;
            this.$filterType = filterType;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$cropImageInfo, this.$filterType, this.$context, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new c(null);
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public final MutableLiveData<a> a() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<fz5> c() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<e> d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        zi7.a(ie.a(this), null, 1, null);
        super.onCleared();
    }

    @DexIgnore
    public final void a(ArrayList<fz5> arrayList) {
        this.d = arrayList;
    }

    @DexIgnore
    public final void a(d dVar, FilterType filterType) {
        ee7.b(dVar, "cropImageInfo");
        ee7.b(filterType, "filterType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoViewModel", "cropImage(), filterType = " + filterType);
        ik7 unused = xh7.b(ie.a(this), null, null, new h(this, dVar, filterType, PortfolioApp.g0.c().getApplicationContext(), null), 3, null);
    }

    @DexIgnore
    public final void a(d dVar, ArrayList<FilterType> arrayList) {
        ee7.b(dVar, "cropImageInfo");
        ee7.b(arrayList, "filterList");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "buildFilterList()");
        ik7 unused = xh7.b(ie.a(this), null, null, new g(this, dVar, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void a(Bitmap bitmap, FilterType filterType) {
        ee7.b(bitmap, "bitmap");
        ee7.b(filterType, "filter");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "applyFilter()");
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, bitmap, filterType, null), 3, null);
    }
}
