package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y52<T> implements z52<T> {
    @DexIgnore
    public /* final */ DataHolder a;

    @DexIgnore
    public y52(DataHolder dataHolder) {
        this.a = dataHolder;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        release();
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<T> iterator() {
        return new a62(this);
    }

    @DexIgnore
    @Override // com.fossil.e12
    public void release() {
        DataHolder dataHolder = this.a;
        if (dataHolder != null) {
            dataHolder.close();
        }
    }
}
