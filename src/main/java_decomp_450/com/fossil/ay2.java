package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay2 implements hx2 {
    @DexIgnore
    public /* final */ jx2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public ay2(jx2 jx2, String str, Object[] objArr) {
        this.a = jx2;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < '\ud800') {
            this.d = charAt;
            return;
        }
        char c2 = charAt & '\u1fff';
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= '\ud800') {
                c2 |= (charAt2 & '\u1fff') << i;
                i += 13;
                i2 = i3;
            } else {
                this.d = c2 | (charAt2 << i);
                return;
            }
        }
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object[] b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.hx2
    public final int zza() {
        return (this.d & 1) == 1 ? bw2.f.i : bw2.f.j;
    }

    @DexIgnore
    @Override // com.fossil.hx2
    public final boolean zzb() {
        return (this.d & 2) == 2;
    }

    @DexIgnore
    @Override // com.fossil.hx2
    public final jx2 zzc() {
        return this.a;
    }
}
