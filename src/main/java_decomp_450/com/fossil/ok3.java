package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ub3 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ r43 c;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 d;

    @DexIgnore
    public ok3(ek3 ek3, ub3 ub3, String str, r43 r43) {
        this.d = ek3;
        this.a = ub3;
        this.b = str;
        this.c = r43;
    }

    @DexIgnore
    public final void run() {
        try {
            bg3 d2 = this.d.d;
            if (d2 == null) {
                this.d.e().t().a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            byte[] a2 = d2.a(this.a, this.b);
            this.d.J();
            this.d.j().a(this.c, a2);
        } catch (RemoteException e) {
            this.d.e().t().a("Failed to send event to the service to bundle", e);
        } finally {
            this.d.j().a(this.c, (byte[]) null);
        }
    }
}
