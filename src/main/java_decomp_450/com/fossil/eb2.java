package com.fossil;

import android.content.Context;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eb2<T> {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public T b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Exception {
        @DexIgnore
        public a(String str) {
            super(str);
        }

        @DexIgnore
        public a(String str, Throwable th) {
            super(str, th);
        }
    }

    @DexIgnore
    public eb2(String str) {
        this.a = str;
    }

    @DexIgnore
    public final T a(Context context) throws a {
        if (this.b == null) {
            a72.a(context);
            Context c = q02.c(context);
            if (c != null) {
                try {
                    this.b = a((IBinder) c.getClassLoader().loadClass(this.a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new a("Could not load creator class.", e);
                } catch (InstantiationException e2) {
                    throw new a("Could not instantiate creator.", e2);
                } catch (IllegalAccessException e3) {
                    throw new a("Could not access creator.", e3);
                }
            } else {
                throw new a("Could not get remote context.");
            }
        }
        return this.b;
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);
}
