package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vv0 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.FILE_CONFIG, ul0.TRANSFER_DATA}));
    @DexIgnore
    public /* final */ long D; // = ((long) this.N.length);
    @DexIgnore
    public long E;
    @DexIgnore
    public float F;
    @DexIgnore
    public long G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public /* final */ x91 M; // = b21.x.m();
    @DexIgnore
    public /* final */ byte[] N;
    @DexIgnore
    public /* final */ boolean O;
    @DexIgnore
    public /* final */ short P;
    @DexIgnore
    public /* final */ float Q;

    @DexIgnore
    public vv0(ri1 ri1, en0 en0, byte[] bArr, boolean z, short s, float f, String str) {
        super(ri1, en0, wm0.j0, str, false, 16);
        this.N = bArr;
        this.O = z;
        this.P = s;
        this.Q = f;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(yz0.a(yz0.a(super.i(), r51.I, Long.valueOf(ik1.a.a(this.N, ng1.CRC32))), r51.v0, Boolean.valueOf(this.O)), r51.y0, yz0.a(this.P));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void j() {
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
    }

    @DexIgnore
    public final void m() {
        long j = this.I + this.J;
        zk0.a(this, new xj1(j, Math.min(6144L, this.D - j), this.D, this.P, ((zk0) this).w, 0, 32), new ym1(this), new xo1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public final void n() {
        zk0.a(this, new tr1(this.P, ((zk0) this).w, 0, 4), new oo0(this), new kq0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public final void o() {
        long j = this.H;
        zk0.a(this, new zh0(0, j, this.D, this.P, ((zk0) this).w, 0, 32), new fs0(this, ik1.a.a(this.N, (int) 0, (int) j, ng1.CRC32)), new bu0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new mh1(((zk0) this).w, ((zk0) this).x, this.M, ((zk0) this).z), new wq1(this), new us1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }
}
