package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.h5;
import java.util.ArrayList;

public class i5 {
    public static float j0 = 0.5f;
    public h5[] A;
    public ArrayList<h5> B;
    public b[] C;
    public i5 D;
    public int E;
    public int F;
    public float G;
    public int H;
    public int I;
    public int J;
    public int K;
    public int L;
    public int M;
    public int N;
    public int O;
    public int P;
    public int Q;
    public int R;
    public int S;
    public int T;
    public int U;
    public float V;
    public float W;
    public Object X;
    public int Y;
    public String Z;
    public int a = -1;
    public String a0;
    public int b = -1;
    public boolean b0;
    public q5 c;
    public boolean c0;
    public q5 d;
    public boolean d0;
    public int e = 0;
    public int e0;
    public int f = 0;
    public int f0;
    public int[] g = new int[2];
    public float[] g0;
    public int h = 0;
    public i5[] h0;
    public int i = 0;
    public i5[] i0;
    public float j = 1.0f;
    public int k = 0;
    public int l = 0;
    public float m = 1.0f;
    public int n = -1;
    public float o = 1.0f;
    public k5 p = null;
    public int[] q = {Integer.MAX_VALUE, Integer.MAX_VALUE};
    public float r = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    public h5 s = new h5(this, h5.d.LEFT);
    public h5 t = new h5(this, h5.d.TOP);
    public h5 u = new h5(this, h5.d.RIGHT);
    public h5 v = new h5(this, h5.d.BOTTOM);
    public h5 w = new h5(this, h5.d.BASELINE);
    public h5 x = new h5(this, h5.d.CENTER_X);
    public h5 y = new h5(this, h5.d.CENTER_Y);
    public h5 z;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|(2:1|2)|3|(2:5|6)|7|9|10|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|1|2|3|(2:5|6)|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0044 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x004e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0058 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0083 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x008f */
        /*
        static {
            /*
                com.fossil.i5$b[] r0 = com.fossil.i5.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.i5.a.b = r0
                r1 = 1
                com.fossil.i5$b r2 = com.fossil.i5.b.FIXED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.i5.a.b     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.i5$b r3 = com.fossil.i5.b.WRAP_CONTENT     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                r2 = 3
                int[] r3 = com.fossil.i5.a.b     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.i5$b r4 = com.fossil.i5.b.MATCH_PARENT     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                r3 = 4
                int[] r4 = com.fossil.i5.a.b     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.i5$b r5 = com.fossil.i5.b.MATCH_CONSTRAINT     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                com.fossil.h5$d[] r4 = com.fossil.h5.d.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.fossil.i5.a.a = r4
                com.fossil.h5$d r5 = com.fossil.h5.d.LEFT     // Catch:{ NoSuchFieldError -> 0x0044 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0044 }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x0044 }
            L_0x0044:
                int[] r1 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x004e }
                com.fossil.h5$d r4 = com.fossil.h5.d.TOP     // Catch:{ NoSuchFieldError -> 0x004e }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004e }
                r1[r4] = r0     // Catch:{ NoSuchFieldError -> 0x004e }
            L_0x004e:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x0058 }
                com.fossil.h5$d r1 = com.fossil.h5.d.RIGHT     // Catch:{ NoSuchFieldError -> 0x0058 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0058 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0058 }
            L_0x0058:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.fossil.h5$d r1 = com.fossil.h5.d.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x006d }
                com.fossil.h5$d r1 = com.fossil.h5.d.BASELINE     // Catch:{ NoSuchFieldError -> 0x006d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006d }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006d }
            L_0x006d:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x0083 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_X     // Catch:{ NoSuchFieldError -> 0x0083 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0083 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0083 }
            L_0x0083:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x008f }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_Y     // Catch:{ NoSuchFieldError -> 0x008f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x008f }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x008f }
            L_0x008f:
                int[] r0 = com.fossil.i5.a.a     // Catch:{ NoSuchFieldError -> 0x009b }
                com.fossil.h5$d r1 = com.fossil.h5.d.NONE     // Catch:{ NoSuchFieldError -> 0x009b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009b }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009b }
            L_0x009b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.i5.a.<clinit>():void");
        }
        */
    }

    public enum b {
        FIXED,
        WRAP_CONTENT,
        MATCH_CONSTRAINT,
        MATCH_PARENT
    }

    public i5() {
        h5 h5Var = new h5(this, h5.d.CENTER);
        this.z = h5Var;
        this.A = new h5[]{this.s, this.u, this.t, this.v, this.w, h5Var};
        this.B = new ArrayList<>();
        b bVar = b.FIXED;
        this.C = new b[]{bVar, bVar};
        this.D = null;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        this.Y = 0;
        this.Z = null;
        this.a0 = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
        this.e0 = 0;
        this.f0 = 0;
        this.g0 = new float[]{-1.0f, -1.0f};
        this.h0 = new i5[]{null, null};
        this.i0 = new i5[]{null, null};
        a();
    }

    public boolean A() {
        h5 h5Var = this.s;
        h5 h5Var2 = h5Var.d;
        if (h5Var2 != null && h5Var2.d == h5Var) {
            return true;
        }
        h5 h5Var3 = this.u;
        h5 h5Var4 = h5Var3.d;
        return h5Var4 != null && h5Var4.d == h5Var3;
    }

    public boolean B() {
        h5 h5Var = this.t;
        h5 h5Var2 = h5Var.d;
        if (h5Var2 != null && h5Var2.d == h5Var) {
            return true;
        }
        h5 h5Var3 = this.v;
        h5 h5Var4 = h5Var3.d;
        return h5Var4 != null && h5Var4.d == h5Var3;
    }

    public boolean C() {
        return this.f == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.k == 0 && this.l == 0 && this.C[1] == b.MATCH_CONSTRAINT;
    }

    public boolean D() {
        return this.e == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.h == 0 && this.i == 0 && this.C[0] == b.MATCH_CONSTRAINT;
    }

    public void E() {
        this.s.j();
        this.t.j();
        this.u.j();
        this.v.j();
        this.w.j();
        this.x.j();
        this.y.j();
        this.z.j();
        this.D = null;
        this.r = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.U = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        b[] bVarArr = this.C;
        b bVar = b.FIXED;
        bVarArr[0] = bVar;
        bVarArr[1] = bVar;
        this.X = null;
        this.Y = 0;
        this.a0 = null;
        this.e0 = 0;
        this.f0 = 0;
        float[] fArr = this.g0;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.a = -1;
        this.b = -1;
        int[] iArr = this.q;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.e = 0;
        this.f = 0;
        this.j = 1.0f;
        this.m = 1.0f;
        this.i = Integer.MAX_VALUE;
        this.l = Integer.MAX_VALUE;
        this.h = 0;
        this.k = 0;
        this.n = -1;
        this.o = 1.0f;
        q5 q5Var = this.c;
        if (q5Var != null) {
            q5Var.d();
        }
        q5 q5Var2 = this.d;
        if (q5Var2 != null) {
            q5Var2.d();
        }
        this.p = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
    }

    public void F() {
        i5 l2 = l();
        if (l2 == null || !(l2 instanceof j5) || !((j5) l()).O()) {
            int size = this.B.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.B.get(i2).j();
            }
        }
    }

    public void G() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().d();
        }
    }

    public void H() {
    }

    public void I() {
        int i2 = this.I;
        int i3 = this.J;
        this.M = i2;
        this.N = i3;
    }

    public void J() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].d().g();
        }
    }

    public void a(int i2) {
        n5.a(i2, this);
    }

    public void a(boolean z2) {
    }

    public void b(y4 y4Var) {
        y4Var.a(this.s);
        y4Var.a(this.t);
        y4Var.a(this.u);
        y4Var.a(this.v);
        if (this.Q > 0) {
            y4Var.a(this.w);
        }
    }

    public void b(boolean z2) {
    }

    public ArrayList<h5> c() {
        return this.B;
    }

    public int d(int i2) {
        if (i2 == 0) {
            return t();
        }
        if (i2 == 1) {
            return j();
        }
        return 0;
    }

    public int e() {
        return x() + this.F;
    }

    public Object f() {
        return this.X;
    }

    public String g() {
        return this.Z;
    }

    public int h() {
        return this.M + this.O;
    }

    public int i() {
        return this.N + this.P;
    }

    public void j(int i2) {
        this.q[1] = i2;
    }

    public void k(int i2) {
        this.q[0] = i2;
    }

    public i5 l() {
        return this.D;
    }

    public q5 m() {
        if (this.d == null) {
            this.d = new q5();
        }
        return this.d;
    }

    public q5 n() {
        if (this.c == null) {
            this.c = new q5();
        }
        return this.c;
    }

    public void o(int i2) {
        this.Y = i2;
    }

    public int p() {
        return this.I + this.O;
    }

    public int q() {
        return this.J + this.P;
    }

    public void r(int i2) {
        this.T = i2;
    }

    public int s() {
        return this.Y;
    }

    public int t() {
        if (this.Y == 8) {
            return 0;
        }
        return this.E;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (this.a0 != null) {
            str = "type: " + this.a0 + " ";
        } else {
            str = str2;
        }
        sb.append(str);
        if (this.Z != null) {
            str2 = "id: " + this.Z + " ";
        }
        sb.append(str2);
        sb.append("(");
        sb.append(this.I);
        sb.append(", ");
        sb.append(this.J);
        sb.append(") - (");
        sb.append(this.E);
        sb.append(" x ");
        sb.append(this.F);
        sb.append(") wrap: (");
        sb.append(this.T);
        sb.append(" x ");
        sb.append(this.U);
        sb.append(")");
        return sb.toString();
    }

    public int u() {
        return this.U;
    }

    public int v() {
        return this.T;
    }

    public int w() {
        return this.I;
    }

    public int x() {
        return this.J;
    }

    public boolean y() {
        return this.Q > 0;
    }

    public boolean z() {
        if (((r5) this.s.d()).b == 1 && ((r5) this.u.d()).b == 1 && ((r5) this.t.d()).b == 1 && ((r5) this.v.d()).b == 1) {
            return true;
        }
        return false;
    }

    public void a(w4 w4Var) {
        this.s.a(w4Var);
        this.t.a(w4Var);
        this.u.a(w4Var);
        this.v.a(w4Var);
        this.w.a(w4Var);
        this.z.a(w4Var);
        this.x.a(w4Var);
        this.y.a(w4Var);
    }

    public void c(int i2, int i3) {
        this.I = i2;
        this.J = i3;
    }

    public void e(int i2, int i3) {
        this.J = i2;
        int i4 = i3 - i2;
        this.F = i4;
        int i5 = this.S;
        if (i4 < i5) {
            this.F = i5;
        }
    }

    public final boolean f(int i2) {
        int i3 = i2 * 2;
        h5[] h5VarArr = this.A;
        if (!(h5VarArr[i3].d == null || h5VarArr[i3].d.d == h5VarArr[i3])) {
            int i4 = i3 + 1;
            return h5VarArr[i4].d != null && h5VarArr[i4].d.d == h5VarArr[i4];
        }
    }

    public void g(int i2) {
        this.Q = i2;
    }

    public void h(int i2) {
        this.F = i2;
        int i3 = this.S;
        if (i2 < i3) {
            this.F = i3;
        }
    }

    public void i(int i2) {
        this.e0 = i2;
    }

    public int j() {
        if (this.Y == 8) {
            return 0;
        }
        return this.F;
    }

    public b k() {
        return this.C[0];
    }

    public void l(int i2) {
        if (i2 < 0) {
            this.S = 0;
        } else {
            this.S = i2;
        }
    }

    public int o() {
        return w() + this.E;
    }

    public void p(int i2) {
        this.E = i2;
        int i3 = this.R;
        if (i2 < i3) {
            this.E = i3;
        }
    }

    public void q(int i2) {
        this.U = i2;
    }

    public b r() {
        return this.C[1];
    }

    public void s(int i2) {
        this.I = i2;
    }

    public int d() {
        return this.Q;
    }

    public void t(int i2) {
        this.J = i2;
    }

    public void c(float f2) {
        this.W = f2;
    }

    public void d(int i2, int i3) {
        if (i3 == 0) {
            this.K = i2;
        } else if (i3 == 1) {
            this.L = i2;
        }
    }

    public void m(int i2) {
        if (i2 < 0) {
            this.R = 0;
        } else {
            this.R = i2;
        }
    }

    public void n(int i2) {
        this.f0 = i2;
    }

    public b c(int i2) {
        if (i2 == 0) {
            return k();
        }
        if (i2 == 1) {
            return r();
        }
        return null;
    }

    public void d(float f2) {
        this.g0[1] = f2;
    }

    public int e(int i2) {
        if (i2 == 0) {
            return this.K;
        }
        if (i2 == 1) {
            return this.L;
        }
        return 0;
    }

    public float b(int i2) {
        if (i2 == 0) {
            return this.V;
        }
        if (i2 == 1) {
            return this.W;
        }
        return -1.0f;
    }

    public void c(y4 y4Var) {
        int b2 = y4Var.b(this.s);
        int b3 = y4Var.b(this.t);
        int b4 = y4Var.b(this.u);
        int b5 = y4Var.b(this.v);
        int i2 = b5 - b3;
        if (b4 - b2 < 0 || i2 < 0 || b2 == Integer.MIN_VALUE || b2 == Integer.MAX_VALUE || b3 == Integer.MIN_VALUE || b3 == Integer.MAX_VALUE || b4 == Integer.MIN_VALUE || b4 == Integer.MAX_VALUE || b5 == Integer.MIN_VALUE || b5 == Integer.MAX_VALUE) {
            b5 = 0;
            b2 = 0;
            b3 = 0;
            b4 = 0;
        }
        a(b2, b3, b4, b5);
    }

    public void b(int i2, int i3) {
        this.O = i2;
        this.P = i3;
    }

    public final void a() {
        this.B.add(this.s);
        this.B.add(this.t);
        this.B.add(this.u);
        this.B.add(this.v);
        this.B.add(this.x);
        this.B.add(this.y);
        this.B.add(this.z);
        this.B.add(this.w);
    }

    public void b(int i2, int i3, int i4, float f2) {
        this.f = i2;
        this.k = i3;
        this.l = i4;
        this.m = f2;
        if (f2 < 1.0f && i2 == 0) {
            this.f = 2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(java.lang.String r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 == 0) goto L_0x008e
            int r1 = r9.length()
            if (r1 != 0) goto L_0x000b
            goto L_0x008e
        L_0x000b:
            r1 = -1
            int r2 = r9.length()
            r3 = 44
            int r3 = r9.indexOf(r3)
            r4 = 0
            r5 = 1
            if (r3 <= 0) goto L_0x0037
            int r6 = r2 + -1
            if (r3 >= r6) goto L_0x0037
            java.lang.String r6 = r9.substring(r4, r3)
            java.lang.String r7 = "W"
            boolean r7 = r6.equalsIgnoreCase(r7)
            if (r7 == 0) goto L_0x002c
            r1 = 0
            goto L_0x0035
        L_0x002c:
            java.lang.String r4 = "H"
            boolean r4 = r6.equalsIgnoreCase(r4)
            if (r4 == 0) goto L_0x0035
            r1 = 1
        L_0x0035:
            int r4 = r3 + 1
        L_0x0037:
            r3 = 58
            int r3 = r9.indexOf(r3)
            if (r3 < 0) goto L_0x0075
            int r2 = r2 - r5
            if (r3 >= r2) goto L_0x0075
            java.lang.String r2 = r9.substring(r4, r3)
            int r3 = r3 + r5
            java.lang.String r9 = r9.substring(r3)
            int r3 = r2.length()
            if (r3 <= 0) goto L_0x0084
            int r3 = r9.length()
            if (r3 <= 0) goto L_0x0084
            float r2 = java.lang.Float.parseFloat(r2)     // Catch:{ NumberFormatException -> 0x0084 }
            float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ NumberFormatException -> 0x0084 }
            int r3 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x0084
            int r3 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x0084
            if (r1 != r5) goto L_0x006f
            float r9 = r9 / r2
            float r9 = java.lang.Math.abs(r9)     // Catch:{ NumberFormatException -> 0x0084 }
            goto L_0x0085
        L_0x006f:
            float r2 = r2 / r9
            float r9 = java.lang.Math.abs(r2)     // Catch:{ NumberFormatException -> 0x0084 }
            goto L_0x0085
        L_0x0075:
            java.lang.String r9 = r9.substring(r4)
            int r2 = r9.length()
            if (r2 <= 0) goto L_0x0084
            float r9 = java.lang.Float.parseFloat(r9)
            goto L_0x0085
        L_0x0084:
            r9 = 0
        L_0x0085:
            int r0 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x008d
            r8.G = r9
            r8.H = r1
        L_0x008d:
            return
        L_0x008e:
            r8.G = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i5.b(java.lang.String):void");
    }

    public void a(i5 i5Var) {
        this.D = i5Var;
    }

    public void a(i5 i5Var, float f2, int i2) {
        h5.d dVar = h5.d.CENTER;
        a(dVar, i5Var, dVar, i2, 0);
        this.r = f2;
    }

    public void a(String str) {
        this.Z = str;
    }

    public void a(int i2, int i3, int i4, float f2) {
        this.e = i2;
        this.h = i3;
        this.i = i4;
        this.j = f2;
        if (f2 < 1.0f && i2 == 0) {
            this.e = 2;
        }
    }

    public void a(float f2) {
        this.V = f2;
    }

    public void a(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i4 - i2;
        int i9 = i5 - i3;
        this.I = i2;
        this.J = i3;
        if (this.Y == 8) {
            this.E = 0;
            this.F = 0;
            return;
        }
        if (this.C[0] == b.FIXED && i8 < (i7 = this.E)) {
            i8 = i7;
        }
        if (this.C[1] == b.FIXED && i9 < (i6 = this.F)) {
            i9 = i6;
        }
        this.E = i8;
        this.F = i9;
        int i10 = this.S;
        if (i9 < i10) {
            this.F = i10;
        }
        int i11 = this.E;
        int i12 = this.R;
        if (i11 < i12) {
            this.E = i12;
        }
        this.c0 = true;
    }

    public void b(float f2) {
        this.g0[0] = f2;
    }

    public boolean b() {
        return this.Y != 8;
    }

    public void b(b bVar) {
        this.C[1] = bVar;
        if (bVar == b.WRAP_CONTENT) {
            h(this.U);
        }
    }

    public void a(int i2, int i3, int i4) {
        if (i4 == 0) {
            a(i2, i3);
        } else if (i4 == 1) {
            e(i2, i3);
        }
        this.c0 = true;
    }

    public void a(int i2, int i3) {
        this.I = i2;
        int i4 = i3 - i2;
        this.E = i4;
        int i5 = this.R;
        if (i4 < i5) {
            this.E = i5;
        }
    }

    public void a(Object obj) {
        this.X = obj;
    }

    public void a(h5.d dVar, i5 i5Var, h5.d dVar2, int i2, int i3) {
        a(dVar).a(i5Var.a(dVar2), i2, i3, h5.c.STRONG, 0, true);
    }

    public h5 a(h5.d dVar) {
        switch (a.a[dVar.ordinal()]) {
            case 1:
                return this.s;
            case 2:
                return this.t;
            case 3:
                return this.u;
            case 4:
                return this.v;
            case 5:
                return this.w;
            case 6:
                return this.z;
            case 7:
                return this.x;
            case 8:
                return this.y;
            case 9:
                return null;
            default:
                throw new AssertionError(dVar.name());
        }
    }

    public void a(b bVar) {
        this.C[0] = bVar;
        if (bVar == b.WRAP_CONTENT) {
            p(this.T);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x023b  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x024c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x02ae  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02b7  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x02bd  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x02c5  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0325  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:169:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.fossil.y4 r39) {
        /*
            r38 = this;
            r15 = r38
            r14 = r39
            com.fossil.h5 r0 = r15.s
            com.fossil.c5 r21 = r14.a(r0)
            com.fossil.h5 r0 = r15.u
            com.fossil.c5 r10 = r14.a(r0)
            com.fossil.h5 r0 = r15.t
            com.fossil.c5 r6 = r14.a(r0)
            com.fossil.h5 r0 = r15.v
            com.fossil.c5 r4 = r14.a(r0)
            com.fossil.h5 r0 = r15.w
            com.fossil.c5 r3 = r14.a(r0)
            com.fossil.i5 r0 = r15.D
            r1 = 8
            r2 = 1
            r13 = 0
            if (r0 == 0) goto L_0x00b0
            if (r0 == 0) goto L_0x0036
            com.fossil.i5$b[] r0 = r0.C
            r0 = r0[r13]
            com.fossil.i5$b r5 = com.fossil.i5.b.WRAP_CONTENT
            if (r0 != r5) goto L_0x0036
            r0 = 1
            goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            com.fossil.i5 r5 = r15.D
            if (r5 == 0) goto L_0x0045
            com.fossil.i5$b[] r5 = r5.C
            r5 = r5[r2]
            com.fossil.i5$b r7 = com.fossil.i5.b.WRAP_CONTENT
            if (r5 != r7) goto L_0x0045
            r5 = 1
            goto L_0x0046
        L_0x0045:
            r5 = 0
        L_0x0046:
            boolean r7 = r15.f(r13)
            if (r7 == 0) goto L_0x0055
            com.fossil.i5 r7 = r15.D
            com.fossil.j5 r7 = (com.fossil.j5) r7
            r7.a(r15, r13)
            r7 = 1
            goto L_0x0059
        L_0x0055:
            boolean r7 = r38.A()
        L_0x0059:
            boolean r8 = r15.f(r2)
            if (r8 == 0) goto L_0x0068
            com.fossil.i5 r8 = r15.D
            com.fossil.j5 r8 = (com.fossil.j5) r8
            r8.a(r15, r2)
            r8 = 1
            goto L_0x006c
        L_0x0068:
            boolean r8 = r38.B()
        L_0x006c:
            if (r0 == 0) goto L_0x0089
            int r9 = r15.Y
            if (r9 == r1) goto L_0x0089
            com.fossil.h5 r9 = r15.s
            com.fossil.h5 r9 = r9.d
            if (r9 != 0) goto L_0x0089
            com.fossil.h5 r9 = r15.u
            com.fossil.h5 r9 = r9.d
            if (r9 != 0) goto L_0x0089
            com.fossil.i5 r9 = r15.D
            com.fossil.h5 r9 = r9.u
            com.fossil.c5 r9 = r14.a(r9)
            r14.b(r9, r10, r13, r2)
        L_0x0089:
            if (r5 == 0) goto L_0x00aa
            int r9 = r15.Y
            if (r9 == r1) goto L_0x00aa
            com.fossil.h5 r9 = r15.t
            com.fossil.h5 r9 = r9.d
            if (r9 != 0) goto L_0x00aa
            com.fossil.h5 r9 = r15.v
            com.fossil.h5 r9 = r9.d
            if (r9 != 0) goto L_0x00aa
            com.fossil.h5 r9 = r15.w
            if (r9 != 0) goto L_0x00aa
            com.fossil.i5 r9 = r15.D
            com.fossil.h5 r9 = r9.v
            com.fossil.c5 r9 = r14.a(r9)
            r14.b(r9, r4, r13, r2)
        L_0x00aa:
            r12 = r5
            r16 = r7
            r22 = r8
            goto L_0x00b6
        L_0x00b0:
            r0 = 0
            r12 = 0
            r16 = 0
            r22 = 0
        L_0x00b6:
            int r5 = r15.E
            int r7 = r15.R
            if (r5 >= r7) goto L_0x00bd
            r5 = r7
        L_0x00bd:
            int r7 = r15.F
            int r8 = r15.S
            if (r7 >= r8) goto L_0x00c4
            r7 = r8
        L_0x00c4:
            com.fossil.i5$b[] r8 = r15.C
            r8 = r8[r13]
            com.fossil.i5$b r9 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r8 == r9) goto L_0x00ce
            r8 = 1
            goto L_0x00cf
        L_0x00ce:
            r8 = 0
        L_0x00cf:
            com.fossil.i5$b[] r9 = r15.C
            r9 = r9[r2]
            com.fossil.i5$b r11 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r9 == r11) goto L_0x00d9
            r9 = 1
            goto L_0x00da
        L_0x00d9:
            r9 = 0
        L_0x00da:
            int r11 = r15.H
            r15.n = r11
            float r11 = r15.G
            r15.o = r11
            int r2 = r15.e
            int r13 = r15.f
            r18 = 0
            r19 = 4
            int r11 = (r11 > r18 ? 1 : (r11 == r18 ? 0 : -1))
            if (r11 <= 0) goto L_0x018f
            int r11 = r15.Y
            r1 = 8
            if (r11 == r1) goto L_0x018f
            com.fossil.i5$b[] r1 = r15.C
            r11 = 0
            r1 = r1[r11]
            com.fossil.i5$b r11 = com.fossil.i5.b.MATCH_CONSTRAINT
            r23 = r3
            if (r1 != r11) goto L_0x0102
            if (r2 != 0) goto L_0x0102
            r2 = 3
        L_0x0102:
            com.fossil.i5$b[] r1 = r15.C
            r11 = 1
            r1 = r1[r11]
            com.fossil.i5$b r11 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r1 != r11) goto L_0x010e
            if (r13 != 0) goto L_0x010e
            r13 = 3
        L_0x010e:
            com.fossil.i5$b[] r1 = r15.C
            r11 = 0
            r3 = r1[r11]
            com.fossil.i5$b r11 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r3 != r11) goto L_0x0125
            r3 = 1
            r1 = r1[r3]
            if (r1 != r11) goto L_0x0125
            r1 = 3
            if (r2 != r1) goto L_0x0126
            if (r13 != r1) goto L_0x0126
            r15.a(r0, r12, r8, r9)
            goto L_0x0184
        L_0x0125:
            r1 = 3
        L_0x0126:
            com.fossil.i5$b[] r3 = r15.C
            r8 = 0
            r9 = r3[r8]
            com.fossil.i5$b r11 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r9 != r11) goto L_0x014e
            if (r2 != r1) goto L_0x014e
            r15.n = r8
            float r1 = r15.o
            int r5 = r15.F
            float r5 = (float) r5
            float r1 = r1 * r5
            int r1 = (int) r1
            r8 = 1
            r3 = r3[r8]
            r25 = r1
            if (r3 == r11) goto L_0x014b
            r26 = r7
            r29 = r13
            r27 = 0
            r28 = 4
            goto L_0x019b
        L_0x014b:
            r28 = r2
            goto L_0x0188
        L_0x014e:
            r8 = 1
            com.fossil.i5$b[] r1 = r15.C
            r1 = r1[r8]
            com.fossil.i5$b r3 = com.fossil.i5.b.MATCH_CONSTRAINT
            if (r1 != r3) goto L_0x0184
            r1 = 3
            if (r13 != r1) goto L_0x0184
            r15.n = r8
            int r1 = r15.H
            r3 = -1
            if (r1 != r3) goto L_0x0168
            r1 = 1065353216(0x3f800000, float:1.0)
            float r3 = r15.o
            float r1 = r1 / r3
            r15.o = r1
        L_0x0168:
            float r1 = r15.o
            int r3 = r15.E
            float r3 = (float) r3
            float r1 = r1 * r3
            int r1 = (int) r1
            com.fossil.i5$b[] r3 = r15.C
            r7 = 0
            r3 = r3[r7]
            com.fossil.i5$b r7 = com.fossil.i5.b.MATCH_CONSTRAINT
            r26 = r1
            r28 = r2
            r25 = r5
            if (r3 == r7) goto L_0x018a
            r27 = 0
            r29 = 4
            goto L_0x019b
        L_0x0184:
            r28 = r2
            r25 = r5
        L_0x0188:
            r26 = r7
        L_0x018a:
            r29 = r13
            r27 = 1
            goto L_0x019b
        L_0x018f:
            r23 = r3
            r28 = r2
            r25 = r5
            r26 = r7
            r29 = r13
            r27 = 0
        L_0x019b:
            int[] r1 = r15.g
            r2 = 0
            r1[r2] = r28
            r2 = 1
            r1[r2] = r29
            if (r27 == 0) goto L_0x01af
            int r1 = r15.n
            r2 = -1
            if (r1 == 0) goto L_0x01ac
            if (r1 != r2) goto L_0x01b0
        L_0x01ac:
            r24 = 1
            goto L_0x01b2
        L_0x01af:
            r2 = -1
        L_0x01b0:
            r24 = 0
        L_0x01b2:
            com.fossil.i5$b[] r1 = r15.C
            r3 = 0
            r1 = r1[r3]
            com.fossil.i5$b r3 = com.fossil.i5.b.WRAP_CONTENT
            if (r1 != r3) goto L_0x01c2
            boolean r1 = r15 instanceof com.fossil.j5
            if (r1 == 0) goto L_0x01c2
            r30 = 1
            goto L_0x01c4
        L_0x01c2:
            r30 = 0
        L_0x01c4:
            com.fossil.h5 r1 = r15.z
            boolean r1 = r1.i()
            r3 = 1
            r31 = r1 ^ 1
            int r1 = r15.a
            r13 = 2
            r32 = 0
            if (r1 == r13) goto L_0x023b
            com.fossil.i5 r1 = r15.D
            if (r1 == 0) goto L_0x01e1
            com.fossil.h5 r1 = r1.u
            com.fossil.c5 r1 = r14.a(r1)
            r20 = r1
            goto L_0x01e3
        L_0x01e1:
            r20 = r32
        L_0x01e3:
            com.fossil.i5 r1 = r15.D
            if (r1 == 0) goto L_0x01f0
            com.fossil.h5 r1 = r1.s
            com.fossil.c5 r1 = r14.a(r1)
            r33 = r1
            goto L_0x01f2
        L_0x01f0:
            r33 = r32
        L_0x01f2:
            com.fossil.i5$b[] r1 = r15.C
            r17 = 0
            r5 = r1[r17]
            com.fossil.h5 r7 = r15.s
            com.fossil.h5 r8 = r15.u
            int r9 = r15.I
            int r11 = r15.R
            int[] r1 = r15.q
            r1 = r1[r17]
            r34 = r12
            r12 = r1
            float r1 = r15.V
            r13 = r1
            int r1 = r15.h
            r17 = r1
            int r1 = r15.i
            r18 = r1
            float r1 = r15.j
            r19 = r1
            r35 = r0
            r0 = r38
            r1 = r39
            r3 = -1
            r2 = r35
            r36 = r23
            r3 = r33
            r23 = r4
            r4 = r20
            r37 = r6
            r6 = r30
            r30 = r10
            r10 = r25
            r14 = r24
            r15 = r16
            r16 = r28
            r20 = r31
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x0245
        L_0x023b:
            r37 = r6
            r30 = r10
            r34 = r12
            r36 = r23
            r23 = r4
        L_0x0245:
            r15 = r38
            int r0 = r15.b
            r1 = 2
            if (r0 != r1) goto L_0x024d
            return
        L_0x024d:
            com.fossil.i5$b[] r0 = r15.C
            r14 = 1
            r0 = r0[r14]
            com.fossil.i5$b r1 = com.fossil.i5.b.WRAP_CONTENT
            if (r0 != r1) goto L_0x025c
            boolean r0 = r15 instanceof com.fossil.j5
            if (r0 == 0) goto L_0x025c
            r6 = 1
            goto L_0x025d
        L_0x025c:
            r6 = 0
        L_0x025d:
            if (r27 == 0) goto L_0x0269
            int r0 = r15.n
            if (r0 == r14) goto L_0x0266
            r1 = -1
            if (r0 != r1) goto L_0x0269
        L_0x0266:
            r16 = 1
            goto L_0x026b
        L_0x0269:
            r16 = 0
        L_0x026b:
            int r0 = r15.Q
            if (r0 <= 0) goto L_0x02a4
            com.fossil.h5 r0 = r15.w
            com.fossil.p5 r0 = r0.d()
            int r0 = r0.b
            if (r0 != r14) goto L_0x0285
            com.fossil.h5 r0 = r15.w
            com.fossil.p5 r0 = r0.d()
            r10 = r39
            r0.a(r10)
            goto L_0x02a6
        L_0x0285:
            r10 = r39
            int r0 = r38.d()
            r1 = 6
            r2 = r36
            r4 = r37
            r10.a(r2, r4, r0, r1)
            com.fossil.h5 r0 = r15.w
            com.fossil.h5 r0 = r0.d
            if (r0 == 0) goto L_0x02a8
            com.fossil.c5 r0 = r10.a(r0)
            r3 = 0
            r10.a(r2, r0, r3, r1)
            r20 = 0
            goto L_0x02aa
        L_0x02a4:
            r10 = r39
        L_0x02a6:
            r4 = r37
        L_0x02a8:
            r20 = r31
        L_0x02aa:
            com.fossil.i5 r0 = r15.D
            if (r0 == 0) goto L_0x02b7
            com.fossil.h5 r0 = r0.v
            com.fossil.c5 r0 = r10.a(r0)
            r24 = r0
            goto L_0x02b9
        L_0x02b7:
            r24 = r32
        L_0x02b9:
            com.fossil.i5 r0 = r15.D
            if (r0 == 0) goto L_0x02c5
            com.fossil.h5 r0 = r0.t
            com.fossil.c5 r0 = r10.a(r0)
            r3 = r0
            goto L_0x02c7
        L_0x02c5:
            r3 = r32
        L_0x02c7:
            com.fossil.i5$b[] r0 = r15.C
            r5 = r0[r14]
            com.fossil.h5 r7 = r15.t
            com.fossil.h5 r8 = r15.v
            int r9 = r15.J
            int r11 = r15.S
            int[] r0 = r15.q
            r12 = r0[r14]
            float r13 = r15.W
            int r0 = r15.k
            r17 = r0
            int r0 = r15.l
            r18 = r0
            float r0 = r15.m
            r19 = r0
            r0 = r38
            r1 = r39
            r2 = r34
            r25 = r4
            r4 = r24
            r10 = r26
            r14 = r16
            r15 = r22
            r16 = r29
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            if (r27 == 0) goto L_0x0325
            r6 = 6
            r7 = r38
            int r0 = r7.n
            r1 = 1
            if (r0 != r1) goto L_0x0314
            float r5 = r7.o
            r0 = r39
            r1 = r23
            r2 = r25
            r3 = r30
            r4 = r21
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0327
        L_0x0314:
            float r5 = r7.o
            r6 = 6
            r0 = r39
            r1 = r30
            r2 = r21
            r3 = r23
            r4 = r25
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0327
        L_0x0325:
            r7 = r38
        L_0x0327:
            com.fossil.h5 r0 = r7.z
            boolean r0 = r0.i()
            if (r0 == 0) goto L_0x034f
            com.fossil.h5 r0 = r7.z
            com.fossil.h5 r0 = r0.g()
            com.fossil.i5 r0 = r0.c()
            float r1 = r7.r
            r2 = 1119092736(0x42b40000, float:90.0)
            float r1 = r1 + r2
            double r1 = (double) r1
            double r1 = java.lang.Math.toRadians(r1)
            float r1 = (float) r1
            com.fossil.h5 r2 = r7.z
            int r2 = r2.b()
            r3 = r39
            r3.a(r7, r0, r1, r2)
        L_0x034f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i5.a(com.fossil.y4):void");
    }

    public void a(boolean z2, boolean z3, boolean z4, boolean z5) {
        if (this.n == -1) {
            if (z4 && !z5) {
                this.n = 0;
            } else if (!z4 && z5) {
                this.n = 1;
                if (this.H == -1) {
                    this.o = 1.0f / this.o;
                }
            }
        }
        if (this.n == 0 && (!this.t.i() || !this.v.i())) {
            this.n = 1;
        } else if (this.n == 1 && (!this.s.i() || !this.u.i())) {
            this.n = 0;
        }
        if (this.n == -1 && (!this.t.i() || !this.v.i() || !this.s.i() || !this.u.i())) {
            if (this.t.i() && this.v.i()) {
                this.n = 0;
            } else if (this.s.i() && this.u.i()) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (z2 && !z3) {
                this.n = 0;
            } else if (!z2 && z3) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (this.h > 0 && this.k == 0) {
                this.n = 0;
            } else if (this.h == 0 && this.k > 0) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1 && z2 && z3) {
            this.o = 1.0f / this.o;
            this.n = 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:153:0x0292  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02d7  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x02e6  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0307  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0310  */
    /* JADX WARNING: Removed duplicated region for block: B:186:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01cd A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.y4 r24, boolean r25, com.fossil.c5 r26, com.fossil.c5 r27, com.fossil.i5.b r28, boolean r29, com.fossil.h5 r30, com.fossil.h5 r31, int r32, int r33, int r34, int r35, float r36, boolean r37, boolean r38, int r39, int r40, int r41, float r42, boolean r43) {
        /*
            r23 = this;
            r0 = r23
            r10 = r24
            r11 = r26
            r12 = r27
            r13 = r30
            r14 = r31
            r1 = r34
            r2 = r35
            com.fossil.c5 r15 = r10.a(r13)
            com.fossil.c5 r9 = r10.a(r14)
            com.fossil.h5 r3 = r30.g()
            com.fossil.c5 r8 = r10.a(r3)
            com.fossil.h5 r3 = r31.g()
            com.fossil.c5 r7 = r10.a(r3)
            boolean r3 = r10.g
            r6 = 1
            r4 = 6
            r5 = 0
            if (r3 == 0) goto L_0x0066
            com.fossil.p5 r3 = r30.d()
            int r3 = r3.b
            if (r3 != r6) goto L_0x0066
            com.fossil.p5 r3 = r31.d()
            int r3 = r3.b
            if (r3 != r6) goto L_0x0066
            com.fossil.z4 r1 = com.fossil.y4.j()
            if (r1 == 0) goto L_0x0050
            com.fossil.z4 r1 = com.fossil.y4.j()
            long r2 = r1.r
            r6 = 1
            long r2 = r2 + r6
            r1.r = r2
        L_0x0050:
            com.fossil.p5 r1 = r30.d()
            r1.a(r10)
            com.fossil.p5 r1 = r31.d()
            r1.a(r10)
            if (r38 != 0) goto L_0x0065
            if (r25 == 0) goto L_0x0065
            r10.b(r12, r9, r5, r4)
        L_0x0065:
            return
        L_0x0066:
            com.fossil.z4 r3 = com.fossil.y4.j()
            if (r3 == 0) goto L_0x0078
            com.fossil.z4 r3 = com.fossil.y4.j()
            long r4 = r3.z
            r16 = 1
            long r4 = r4 + r16
            r3.z = r4
        L_0x0078:
            boolean r16 = r30.i()
            boolean r17 = r31.i()
            com.fossil.h5 r3 = r0.z
            boolean r19 = r3.i()
            if (r16 == 0) goto L_0x008a
            r3 = 1
            goto L_0x008b
        L_0x008a:
            r3 = 0
        L_0x008b:
            if (r17 == 0) goto L_0x008f
            int r3 = r3 + 1
        L_0x008f:
            if (r19 == 0) goto L_0x0093
            int r3 = r3 + 1
        L_0x0093:
            r5 = r3
            if (r37 == 0) goto L_0x0098
            r3 = 3
            goto L_0x009a
        L_0x0098:
            r3 = r39
        L_0x009a:
            int[] r20 = com.fossil.i5.a.b
            int r21 = r28.ordinal()
            r4 = r20[r21]
            r14 = 2
            r13 = 4
            if (r4 == r6) goto L_0x00ad
            if (r4 == r14) goto L_0x00ad
            r14 = 3
            if (r4 == r14) goto L_0x00ad
            if (r4 == r13) goto L_0x00af
        L_0x00ad:
            r4 = 0
            goto L_0x00b3
        L_0x00af:
            if (r3 != r13) goto L_0x00b2
            goto L_0x00ad
        L_0x00b2:
            r4 = 1
        L_0x00b3:
            int r14 = r0.Y
            r13 = 8
            if (r14 != r13) goto L_0x00bc
            r4 = 0
            r13 = 0
            goto L_0x00bf
        L_0x00bc:
            r13 = r4
            r4 = r33
        L_0x00bf:
            if (r43 == 0) goto L_0x00da
            if (r16 != 0) goto L_0x00cd
            if (r17 != 0) goto L_0x00cd
            if (r19 != 0) goto L_0x00cd
            r14 = r32
            r10.a(r15, r14)
            goto L_0x00da
        L_0x00cd:
            if (r16 == 0) goto L_0x00da
            if (r17 != 0) goto L_0x00da
            int r14 = r30.b()
            r6 = 6
            r10.a(r15, r8, r14, r6)
            goto L_0x00db
        L_0x00da:
            r6 = 6
        L_0x00db:
            if (r13 != 0) goto L_0x0107
            if (r29 == 0) goto L_0x00f4
            r6 = 0
            r14 = 3
            r10.a(r9, r15, r6, r14)
            r4 = 6
            if (r1 <= 0) goto L_0x00ea
            r10.b(r9, r15, r1, r4)
        L_0x00ea:
            r6 = 2147483647(0x7fffffff, float:NaN)
            if (r2 >= r6) goto L_0x00f2
            r10.c(r9, r15, r2, r4)
        L_0x00f2:
            r6 = 6
            goto L_0x00f8
        L_0x00f4:
            r14 = 3
            r10.a(r9, r15, r4, r6)
        L_0x00f8:
            r14 = r40
            r32 = r3
            r0 = r5
            r1 = r7
            r22 = r8
            r21 = r13
            r2 = 2
            r13 = r41
            goto L_0x01e4
        L_0x0107:
            r14 = 3
            r2 = -2
            r14 = r40
            r6 = r41
            if (r14 != r2) goto L_0x0110
            r14 = r4
        L_0x0110:
            if (r6 != r2) goto L_0x0113
            r6 = r4
        L_0x0113:
            r2 = 6
            if (r14 <= 0) goto L_0x011d
            r10.b(r9, r15, r14, r2)
            int r4 = java.lang.Math.max(r4, r14)
        L_0x011d:
            if (r6 <= 0) goto L_0x0126
            r10.c(r9, r15, r6, r2)
            int r4 = java.lang.Math.min(r4, r6)
        L_0x0126:
            r2 = 1
            if (r3 != r2) goto L_0x014f
            if (r25 == 0) goto L_0x013b
            r2 = 6
            r10.a(r9, r15, r4, r2)
            r32 = r3
            r0 = r5
            r1 = r7
            r22 = r8
            r33 = r13
            r8 = r4
            r13 = r6
            goto L_0x01c8
        L_0x013b:
            r2 = 6
            if (r38 == 0) goto L_0x0146
            r33 = r13
            r13 = 4
            r10.a(r9, r15, r4, r13)
            goto L_0x01c0
        L_0x0146:
            r33 = r13
            r2 = 1
            r13 = 4
            r10.a(r9, r15, r4, r2)
            goto L_0x01c0
        L_0x014f:
            r33 = r13
            r2 = 2
            r13 = 4
            if (r3 != r2) goto L_0x01c0
            com.fossil.h5$d r2 = r30.h()
            com.fossil.h5$d r13 = com.fossil.h5.d.TOP
            if (r2 == r13) goto L_0x0181
            com.fossil.h5$d r2 = r30.h()
            com.fossil.h5$d r13 = com.fossil.h5.d.BOTTOM
            if (r2 != r13) goto L_0x0166
            goto L_0x0181
        L_0x0166:
            com.fossil.i5 r2 = r0.D
            com.fossil.h5$d r13 = com.fossil.h5.d.LEFT
            com.fossil.h5 r2 = r2.a(r13)
            com.fossil.c5 r2 = r10.a(r2)
            com.fossil.i5 r13 = r0.D
            r29 = r2
            com.fossil.h5$d r2 = com.fossil.h5.d.RIGHT
            com.fossil.h5 r2 = r13.a(r2)
            com.fossil.c5 r2 = r10.a(r2)
            goto L_0x019b
        L_0x0181:
            com.fossil.i5 r2 = r0.D
            com.fossil.h5$d r13 = com.fossil.h5.d.TOP
            com.fossil.h5 r2 = r2.a(r13)
            com.fossil.c5 r2 = r10.a(r2)
            com.fossil.i5 r13 = r0.D
            r29 = r2
            com.fossil.h5$d r2 = com.fossil.h5.d.BOTTOM
            com.fossil.h5 r2 = r13.a(r2)
            com.fossil.c5 r2 = r10.a(r2)
        L_0x019b:
            r21 = r29
            r13 = r2
            com.fossil.v4 r2 = r24.c()
            r29 = r2
            r18 = 1
            r20 = 6
            r0 = r3
            r3 = r9
            r22 = r8
            r8 = r4
            r4 = r15
            r32 = r0
            r0 = r5
            r5 = r13
            r13 = r6
            r6 = r21
            r1 = r7
            r7 = r42
            r2.a(r3, r4, r5, r6, r7)
            r10.a(r2)
            r5 = 0
            goto L_0x01ca
        L_0x01c0:
            r32 = r3
            r0 = r5
            r13 = r6
            r1 = r7
            r22 = r8
            r8 = r4
        L_0x01c8:
            r5 = r33
        L_0x01ca:
            r2 = 2
            if (r5 == 0) goto L_0x01e2
            if (r0 == r2) goto L_0x01e2
            if (r37 != 0) goto L_0x01e2
            int r3 = java.lang.Math.max(r14, r8)
            if (r13 <= 0) goto L_0x01db
            int r3 = java.lang.Math.min(r13, r3)
        L_0x01db:
            r4 = 6
            r10.a(r9, r15, r3, r4)
            r21 = 0
            goto L_0x01e4
        L_0x01e2:
            r21 = r5
        L_0x01e4:
            if (r43 == 0) goto L_0x0317
            if (r38 == 0) goto L_0x01ea
            goto L_0x0317
        L_0x01ea:
            r0 = 5
            if (r16 != 0) goto L_0x01f9
            if (r17 != 0) goto L_0x01f9
            if (r19 != 0) goto L_0x01f9
            if (r25 == 0) goto L_0x030b
            r2 = 0
            r10.b(r12, r9, r2, r0)
            goto L_0x030b
        L_0x01f9:
            r2 = 0
            if (r16 == 0) goto L_0x0205
            if (r17 != 0) goto L_0x0205
            if (r25 == 0) goto L_0x030b
            r10.b(r12, r9, r2, r0)
            goto L_0x030b
        L_0x0205:
            if (r16 != 0) goto L_0x0219
            if (r17 == 0) goto L_0x0219
            int r3 = r31.b()
            int r3 = -r3
            r4 = 6
            r10.a(r9, r1, r3, r4)
            if (r25 == 0) goto L_0x030b
            r10.b(r15, r11, r2, r0)
            goto L_0x030b
        L_0x0219:
            if (r16 == 0) goto L_0x030b
            if (r17 == 0) goto L_0x030b
            if (r21 == 0) goto L_0x0285
            r8 = r1
            r7 = 6
            if (r25 == 0) goto L_0x0228
            if (r34 != 0) goto L_0x0228
            r10.b(r9, r15, r2, r7)
        L_0x0228:
            if (r32 != 0) goto L_0x0252
            if (r13 > 0) goto L_0x0232
            if (r14 <= 0) goto L_0x022f
            goto L_0x0232
        L_0x022f:
            r4 = 6
            r6 = 0
            goto L_0x0234
        L_0x0232:
            r4 = 4
            r6 = 1
        L_0x0234:
            int r1 = r30.b()
            r5 = r22
            r10.a(r15, r5, r1, r4)
            int r1 = r31.b()
            int r1 = -r1
            r10.a(r9, r8, r1, r4)
            if (r13 > 0) goto L_0x024c
            if (r14 <= 0) goto L_0x024a
            goto L_0x024c
        L_0x024a:
            r1 = 0
            goto L_0x024d
        L_0x024c:
            r1 = 1
        L_0x024d:
            r13 = r6
            r14 = 1
            r16 = 5
            goto L_0x025d
        L_0x0252:
            r4 = r32
            r5 = r22
            r14 = 1
            if (r4 != r14) goto L_0x0260
            r1 = 1
            r13 = 1
            r16 = 6
        L_0x025d:
            r6 = r23
            goto L_0x0290
        L_0x0260:
            r1 = 3
            r6 = r23
            if (r4 != r1) goto L_0x0283
            if (r37 != 0) goto L_0x0270
            int r1 = r6.n
            r2 = -1
            if (r1 == r2) goto L_0x0270
            if (r13 > 0) goto L_0x0270
            r4 = 6
            goto L_0x0271
        L_0x0270:
            r4 = 4
        L_0x0271:
            int r1 = r30.b()
            r10.a(r15, r5, r1, r4)
            int r1 = r31.b()
            int r1 = -r1
            r10.a(r9, r8, r1, r4)
            r1 = 1
            r13 = 1
            goto L_0x028e
        L_0x0283:
            r1 = 0
            goto L_0x028d
        L_0x0285:
            r8 = r1
            r5 = r22
            r7 = 6
            r14 = 1
            r6 = r23
            r1 = 1
        L_0x028d:
            r13 = 0
        L_0x028e:
            r16 = 5
        L_0x0290:
            if (r1 == 0) goto L_0x02d7
            int r4 = r30.b()
            int r17 = r31.b()
            r1 = r24
            r2 = r15
            r3 = r5
            r18 = r5
            r5 = r36
            r6 = r8
            r19 = 6
            r7 = r9
            r14 = r8
            r0 = r18
            r12 = 6
            r8 = r17
            r12 = r9
            r9 = r16
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9)
            r1 = r30
            com.fossil.h5 r2 = r1.d
            com.fossil.i5 r2 = r2.b
            boolean r2 = r2 instanceof com.fossil.e5
            r3 = r31
            com.fossil.h5 r4 = r3.d
            com.fossil.i5 r4 = r4.b
            boolean r4 = r4 instanceof com.fossil.e5
            if (r2 == 0) goto L_0x02cd
            if (r4 != 0) goto L_0x02cd
            r6 = r25
            r2 = 6
            r4 = 5
            r18 = 1
            goto L_0x02e4
        L_0x02cd:
            if (r2 != 0) goto L_0x02de
            if (r4 == 0) goto L_0x02de
            r18 = r25
            r2 = 5
            r4 = 6
            r6 = 1
            goto L_0x02e4
        L_0x02d7:
            r1 = r30
            r3 = r31
            r0 = r5
            r14 = r8
            r12 = r9
        L_0x02de:
            r6 = r25
            r18 = r6
            r2 = 5
            r4 = 5
        L_0x02e4:
            if (r13 == 0) goto L_0x02e8
            r2 = 6
            r4 = 6
        L_0x02e8:
            if (r21 != 0) goto L_0x02ec
            if (r6 != 0) goto L_0x02ee
        L_0x02ec:
            if (r13 == 0) goto L_0x02f5
        L_0x02ee:
            int r1 = r30.b()
            r10.b(r15, r0, r1, r4)
        L_0x02f5:
            if (r21 != 0) goto L_0x02f9
            if (r18 != 0) goto L_0x02fb
        L_0x02f9:
            if (r13 == 0) goto L_0x0303
        L_0x02fb:
            int r0 = r31.b()
            int r0 = -r0
            r10.c(r12, r14, r0, r2)
        L_0x0303:
            r0 = 6
            r1 = 0
            if (r25 == 0) goto L_0x030e
            r10.b(r15, r11, r1, r0)
            goto L_0x030e
        L_0x030b:
            r12 = r9
            r0 = 6
            r1 = 0
        L_0x030e:
            if (r25 == 0) goto L_0x0316
            r2 = r27
            r3 = 6
            r10.b(r2, r12, r1, r3)
        L_0x0316:
            return
        L_0x0317:
            r2 = r12
            r1 = 0
            r3 = 6
            r4 = 2
            r12 = r9
            if (r0 >= r4) goto L_0x0326
            if (r25 == 0) goto L_0x0326
            r10.b(r15, r11, r1, r3)
            r10.b(r2, r12, r1, r3)
        L_0x0326:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.i5.a(com.fossil.y4, boolean, com.fossil.c5, com.fossil.c5, com.fossil.i5$b, boolean, com.fossil.h5, com.fossil.h5, int, int, int, int, float, boolean, boolean, int, int, int, float, boolean):void");
    }
}
