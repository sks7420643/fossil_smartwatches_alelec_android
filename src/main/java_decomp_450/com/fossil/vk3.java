package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 e;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 f;

    @DexIgnore
    public vk3(ek3 ek3, AtomicReference atomicReference, String str, String str2, String str3, nm3 nm3) {
        this.f = ek3;
        this.a = atomicReference;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = nm3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                bg3 d2 = this.f.d;
                if (d2 == null) {
                    this.f.e().t().a("(legacy) Failed to get conditional properties; not connected to service", jg3.a(this.b), this.c, this.d);
                    this.a.set(Collections.emptyList());
                    return;
                }
                if (TextUtils.isEmpty(this.b)) {
                    this.a.set(d2.a(this.c, this.d, this.e));
                } else {
                    this.a.set(d2.a(this.b, this.c, this.d));
                }
                this.f.J();
                this.a.notify();
            } catch (RemoteException e2) {
                this.f.e().t().a("(legacy) Failed to get conditional properties; remote exception", jg3.a(this.b), this.c, e2);
                this.a.set(Collections.emptyList());
            } finally {
                this.a.notify();
            }
        }
    }
}
