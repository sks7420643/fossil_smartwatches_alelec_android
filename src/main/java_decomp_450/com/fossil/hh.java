package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.Surface;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.jh;
import com.fossil.sh;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class hh extends ih {
    public static BitmapFactory.Options n;
    public sh d;
    public Bitmap e;
    public int f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int k;
    public long l;
    public boolean m;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /*
        static {
            /*
                android.graphics.Bitmap$Config[] r0 = android.graphics.Bitmap.Config.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.hh.a.a = r0
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ALPHA_8     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.hh.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.hh.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.hh.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_4444     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.hh.a.<clinit>():void");
        }
        */
    }

    public enum b {
        MIPMAP_NONE(0),
        MIPMAP_FULL(1),
        MIPMAP_ON_SYNC_TO_TEXTURE(2);
        
        public int mID;

        public b(int i) {
            this.mID = i;
        }
    }

    /*
    static {
        BitmapFactory.Options options = new BitmapFactory.Options();
        n = options;
        options.inScaled = false;
    }
    */

    public hh(long j2, RenderScript renderScript, sh shVar, int i2) {
        super(j2, renderScript);
        sh.b bVar = sh.b.POSITIVE_X;
        if ((i2 & -228) != 0) {
            throw new mh("Unknown usage specified.");
        } else if ((i2 & 32) == 0 || (i2 & -36) == 0) {
            this.d = shVar;
            this.f = i2;
            this.l = 0;
            this.m = false;
            if (shVar != null) {
                this.g = shVar.f() * this.d.g().e();
                a(shVar);
            }
            if (RenderScript.x) {
                try {
                    RenderScript.z.invoke(RenderScript.y, Integer.valueOf(this.g));
                } catch (Exception e2) {
                    Log.e("RenderScript_jni", "Couldn't invoke registerNativeAllocation:" + e2);
                    throw new oh("Couldn't invoke registerNativeAllocation:" + e2);
                }
            }
        } else {
            throw new mh("Invalid usage combination.");
        }
    }

    public void a(long j2) {
        this.l = j2;
    }

    public void b(Bitmap bitmap) {
        ((ih) this).c.j();
        d(bitmap);
        e(bitmap);
        RenderScript renderScript = ((ih) this).c;
        renderScript.b(a(renderScript), bitmap);
    }

    public final void c(Bitmap bitmap) {
        this.e = bitmap;
    }

    public final void d(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config != null) {
            int i2 = a.a[config.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (this.d.g().f != jh.b.PIXEL_RGBA || this.d.g().e() != 2) {
                                throw new mh("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                            }
                        }
                    } else if (this.d.g().f != jh.b.PIXEL_RGB || this.d.g().e() != 2) {
                        throw new mh("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                    }
                } else if (this.d.g().f != jh.b.PIXEL_RGBA || this.d.g().e() != 4) {
                    throw new mh("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                }
            } else if (this.d.g().f != jh.b.PIXEL_A) {
                throw new mh("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
            }
        } else {
            throw new mh("Bitmap has an unsupported format for this operation");
        }
    }

    public sh e() {
        return this.d;
    }

    @Override // com.fossil.ih
    public void finalize() throws Throwable {
        if (RenderScript.x) {
            RenderScript.A.invoke(RenderScript.y, Integer.valueOf(this.g));
        }
        super.finalize();
    }

    public final void a(sh shVar) {
        this.h = shVar.h();
        this.i = shVar.i();
        this.j = shVar.j();
        int i2 = this.h;
        this.k = i2;
        int i3 = this.i;
        if (i3 > 1) {
            this.k = i2 * i3;
        }
        int i4 = this.j;
        if (i4 > 1) {
            this.k *= i4;
        }
    }

    public final void e(Bitmap bitmap) {
        if (this.h != bitmap.getWidth() || this.i != bitmap.getHeight()) {
            throw new mh("Cannot update allocation from bitmap, sizes mismatch");
        }
    }

    @Override // com.fossil.ih
    public void b() {
        if (this.l != 0) {
            boolean z = false;
            synchronized (this) {
                if (!this.m) {
                    this.m = true;
                    z = true;
                }
            }
            if (z) {
                ReentrantReadWriteLock.ReadLock readLock = ((ih) this).c.k.readLock();
                readLock.lock();
                if (((ih) this).c.c()) {
                    ((ih) this).c.a(this.l);
                }
                readLock.unlock();
                this.l = 0;
            }
        }
        if ((this.f & 96) != 0) {
            a((Surface) null);
        }
        super.b();
    }

    public void a(Bitmap bitmap) {
        ((ih) this).c.j();
        if (bitmap.getConfig() == null) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a(createBitmap);
            return;
        }
        e(bitmap);
        d(bitmap);
        RenderScript renderScript = ((ih) this).c;
        renderScript.a(a(renderScript), bitmap);
    }

    public static hh a(RenderScript renderScript, sh shVar, b bVar, int i2) {
        renderScript.j();
        if (shVar.a(renderScript) == 0) {
            throw new nh("Bad Type");
        } else if (renderScript.i() || (i2 & 32) == 0) {
            long a2 = renderScript.a(shVar.a(renderScript), bVar.mID, i2, 0L);
            if (a2 != 0) {
                return new hh(a2, renderScript, shVar, i2);
            }
            throw new oh("Allocation creation failed.");
        } else {
            throw new oh("USAGE_IO not supported, Allocation creation failed.");
        }
    }

    public static hh a(RenderScript renderScript, sh shVar) {
        return a(renderScript, shVar, b.MIPMAP_NONE, 1);
    }

    public static jh a(RenderScript renderScript, Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == Bitmap.Config.ALPHA_8) {
            return jh.c(renderScript);
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return jh.d(renderScript);
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return jh.e(renderScript);
        }
        if (config == Bitmap.Config.RGB_565) {
            return jh.f(renderScript);
        }
        throw new nh("Bad bitmap type: " + config);
    }

    public static sh a(RenderScript renderScript, Bitmap bitmap, b bVar) {
        sh.a aVar = new sh.a(renderScript, a(renderScript, bitmap));
        aVar.a(bitmap.getWidth());
        aVar.b(bitmap.getHeight());
        aVar.a(bVar == b.MIPMAP_FULL);
        return aVar.a();
    }

    public static hh a(RenderScript renderScript, Bitmap bitmap, b bVar, int i2) {
        renderScript.j();
        if (bitmap.getConfig() != null) {
            sh a2 = a(renderScript, bitmap, bVar);
            if (bVar == b.MIPMAP_NONE && a2.g().a(jh.e(renderScript)) && i2 == 131) {
                long a3 = renderScript.a(a2.a(renderScript), bVar.mID, bitmap, i2);
                if (a3 != 0) {
                    hh hhVar = new hh(a3, renderScript, a2, i2);
                    hhVar.c(bitmap);
                    return hhVar;
                }
                throw new oh("Load failed.");
            }
            long b2 = renderScript.b(a2.a(renderScript), bVar.mID, bitmap, i2);
            if (b2 != 0) {
                return new hh(b2, renderScript, a2, i2);
            }
            throw new oh("Load failed.");
        } else if ((i2 & 128) == 0) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            return a(renderScript, createBitmap, bVar, i2);
        } else {
            throw new mh("USAGE_SHARED cannot be used with a Bitmap that has a null config.");
        }
    }

    public void a(Surface surface) {
        ((ih) this).c.j();
        if ((this.f & 64) != 0) {
            RenderScript renderScript = ((ih) this).c;
            renderScript.a(a(renderScript), surface);
            return;
        }
        throw new nh("Allocation is not USAGE_IO_OUTPUT.");
    }
}
