package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ EnumMap<cg0, pa0[]> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qb0 createFromParcel(Parcel parcel) {
            return new qb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qb0[] newArray(int i) {
            return new qb0[i];
        }
    }

    @DexIgnore
    public qb0(byte b, int i, EnumMap<cg0, pa0[]> enumMap) {
        super(cb0.WATCH_APP_LIFE_CYCLE, b);
        this.c = i;
        this.d = enumMap;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(super.a(), r51.p, Integer.valueOf(this.c));
        for (Map.Entry<K, pa0[]> entry : this.d.entrySet()) {
            JSONArray jSONArray = new JSONArray();
            pa0[] value = entry.getValue();
            ee7.a((Object) value, "entry.value");
            for (pa0 pa0 : value) {
                jSONArray.put(pa0.a());
            }
            if (jSONArray.length() > 0) {
                a2.put(entry.getKey().a(), jSONArray);
            }
        }
        return a2;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(qb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            qb0 qb0 = (qb0) obj;
            return this.c == qb0.c && !(ee7.a(this.d, qb0.d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.WatchAppLifeCycleNotification");
    }

    @DexIgnore
    public final EnumMap<cg0, pa0[]> getWatchAppStatus() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = Integer.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ qb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.c = parcel.readInt();
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            this.d = (EnumMap) readSerializable;
            return;
        }
        throw new x87("null cannot be cast to non-null type java.util.EnumMap<com.fossil.blesdk.model.enumerate.WatchAppStatus, kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchAppId>>");
    }
}
