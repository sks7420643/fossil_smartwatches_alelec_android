package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class nk7 {
    @DexIgnore
    public static final ki7 a(ik7 ik7) {
        return new lk7(ik7);
    }

    @DexIgnore
    public static final void b(ik7 ik7) {
        if (!ik7.isActive()) {
            throw ik7.b();
        }
    }

    @DexIgnore
    public static /* synthetic */ ki7 a(ik7 ik7, int i, Object obj) {
        if ((i & 1) != 0) {
            ik7 = null;
        }
        return mk7.a(ik7);
    }

    @DexIgnore
    public static final rj7 a(ik7 ik7, rj7 rj7) {
        return ik7.b(new tj7(ik7, rj7));
    }

    @DexIgnore
    public static /* synthetic */ void a(ib7 ib7, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        mk7.a(ib7, cancellationException);
    }

    @DexIgnore
    public static final void a(ib7 ib7, CancellationException cancellationException) {
        ik7 ik7 = (ik7) ib7.get(ik7.o);
        if (ik7 != null) {
            ik7.a(cancellationException);
        }
    }

    @DexIgnore
    public static final void a(ib7 ib7) {
        ik7 ik7 = (ik7) ib7.get(ik7.o);
        if (ik7 != null) {
            mk7.b(ik7);
            return;
        }
        throw new IllegalStateException(("Context cannot be checked for liveness because it does not have a job: " + ib7).toString());
    }
}
