package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s43 extends ln2 implements q43 {
    @DexIgnore
    public s43(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeLong(j);
        b(23, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, bundle);
        b(9, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeLong(j);
        b(24, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void generateEventId(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(22, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getAppInstanceId(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(20, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getCachedAppInstanceId(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(19, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getConditionalUserProperties(String str, String str2, r43 r43) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, r43);
        b(10, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getCurrentScreenClass(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(17, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getCurrentScreenName(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(16, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getGmpAppId(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(21, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getMaxUserProperties(String str, r43 r43) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        jo2.a(E, r43);
        b(6, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getTestFlag(r43 r43, int i) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        E.writeInt(i);
        b(38, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void getUserProperties(String str, String str2, boolean z, r43 r43) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, z);
        jo2.a(E, r43);
        b(5, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void initForTests(Map map) throws RemoteException {
        Parcel E = E();
        E.writeMap(map);
        b(37, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void initialize(ab2 ab2, qn2 qn2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        jo2.a(E, qn2);
        E.writeLong(j);
        b(1, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void isDataCollectionEnabled(r43 r43) throws RemoteException {
        Parcel E = E();
        jo2.a(E, r43);
        b(40, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, bundle);
        jo2.a(E, z);
        jo2.a(E, z2);
        E.writeLong(j);
        b(2, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void logEventAndBundle(String str, String str2, Bundle bundle, r43 r43, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, bundle);
        jo2.a(E, r43);
        E.writeLong(j);
        b(3, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void logHealthData(int i, String str, ab2 ab2, ab2 ab22, ab2 ab23) throws RemoteException {
        Parcel E = E();
        E.writeInt(i);
        E.writeString(str);
        jo2.a(E, ab2);
        jo2.a(E, ab22);
        jo2.a(E, ab23);
        b(33, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityCreated(ab2 ab2, Bundle bundle, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        jo2.a(E, bundle);
        E.writeLong(j);
        b(27, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityDestroyed(ab2 ab2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeLong(j);
        b(28, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityPaused(ab2 ab2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeLong(j);
        b(29, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityResumed(ab2 ab2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeLong(j);
        b(30, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivitySaveInstanceState(ab2 ab2, r43 r43, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        jo2.a(E, r43);
        E.writeLong(j);
        b(31, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityStarted(ab2 ab2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeLong(j);
        b(25, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void onActivityStopped(ab2 ab2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeLong(j);
        b(26, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void performAction(Bundle bundle, r43 r43, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, bundle);
        jo2.a(E, r43);
        E.writeLong(j);
        b(32, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void registerOnMeasurementEventListener(nn2 nn2) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nn2);
        b(35, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void resetAnalyticsData(long j) throws RemoteException {
        Parcel E = E();
        E.writeLong(j);
        b(12, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, bundle);
        E.writeLong(j);
        b(8, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setCurrentScreen(ab2 ab2, String str, String str2, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, ab2);
        E.writeString(str);
        E.writeString(str2);
        E.writeLong(j);
        b(15, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setDataCollectionEnabled(boolean z) throws RemoteException {
        Parcel E = E();
        jo2.a(E, z);
        b(39, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setDefaultEventParameters(Bundle bundle) throws RemoteException {
        Parcel E = E();
        jo2.a(E, bundle);
        b(42, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setEventInterceptor(nn2 nn2) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nn2);
        b(34, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setInstanceIdProvider(on2 on2) throws RemoteException {
        Parcel E = E();
        jo2.a(E, on2);
        b(18, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setMeasurementEnabled(boolean z, long j) throws RemoteException {
        Parcel E = E();
        jo2.a(E, z);
        E.writeLong(j);
        b(11, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setMinimumSessionDuration(long j) throws RemoteException {
        Parcel E = E();
        E.writeLong(j);
        b(13, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setSessionTimeoutDuration(long j) throws RemoteException {
        Parcel E = E();
        E.writeLong(j);
        b(14, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setUserId(String str, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeLong(j);
        b(7, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void setUserProperty(String str, String str2, ab2 ab2, boolean z, long j) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        E.writeString(str2);
        jo2.a(E, ab2);
        jo2.a(E, z);
        E.writeLong(j);
        b(4, E);
    }

    @DexIgnore
    @Override // com.fossil.q43
    public final void unregisterOnMeasurementEventListener(nn2 nn2) throws RemoteException {
        Parcel E = E();
        jo2.a(E, nn2);
        b(36, E);
    }
}
