package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za1 implements Parcelable.Creator<tc1> {
    @DexIgnore
    public /* synthetic */ za1(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public tc1 createFromParcel(Parcel parcel) {
        Object[] createTypedArray = parcel.createTypedArray(jg0.CREATOR);
        if (createTypedArray != null) {
            ee7.a((Object) createTypedArray, "parcel.createTypedArray(MicroAppMapping.CREATOR)!!");
            jg0[] jg0Arr = (jg0[]) createTypedArray;
            Parcelable readParcelable = parcel.readParcelable(r60.class.getClassLoader());
            if (readParcelable != null) {
                return new tc1(jg0Arr, (r60) readParcelable);
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public tc1[] newArray(int i) {
        return new tc1[i];
    }
}
