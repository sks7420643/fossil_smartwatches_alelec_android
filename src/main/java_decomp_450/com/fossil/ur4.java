package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur4 implements Factory<tr4> {
    @DexIgnore
    public /* final */ Provider<xo4> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;

    @DexIgnore
    public ur4(Provider<xo4> provider, Provider<ro4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ur4 a(Provider<xo4> provider, Provider<ro4> provider2) {
        return new ur4(provider, provider2);
    }

    @DexIgnore
    public static tr4 a(xo4 xo4, ro4 ro4) {
        return new tr4(xo4, ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public tr4 get() {
        return a(this.a.get(), this.b.get());
    }
}
