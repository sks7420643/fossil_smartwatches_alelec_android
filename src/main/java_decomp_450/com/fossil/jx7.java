package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jx7 extends kx7<Bitmap> {
    @DexIgnore
    public Bitmap d;

    @DexIgnore
    public jx7(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void a(Bitmap bitmap, f50<? super Bitmap> f50) {
        ee7.b(bitmap, "resource");
        this.d = bitmap;
    }

    @DexIgnore
    @Override // com.fossil.q30, com.fossil.kx7
    public void onDestroy() {
        Bitmap bitmap;
        super.onDestroy();
        Bitmap bitmap2 = this.d;
        if (bitmap2 != null && !bitmap2.isRecycled() && (bitmap = this.d) != null) {
            bitmap.recycle();
        }
    }
}
