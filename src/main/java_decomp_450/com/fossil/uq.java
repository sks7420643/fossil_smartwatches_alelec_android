package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface uq {
    @DexIgnore
    public static final a a = a.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ a a; // = new a();

        @DexIgnore
        public final uq a() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 23) {
                return new wq();
            }
            if (i >= 19) {
                return new vq();
            }
            return new tq();
        }
    }

    @DexIgnore
    Bitmap a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    String b(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String b(Bitmap bitmap);

    @DexIgnore
    Bitmap removeLast();
}
