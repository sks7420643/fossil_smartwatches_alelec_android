package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xr7 extends js7 {
    @DexIgnore
    public gs7 group;
    @DexIgnore
    public fs7 option;

    @DexIgnore
    public xr7(String str) {
        super(str);
    }

    @DexIgnore
    public fs7 getOption() {
        return this.option;
    }

    @DexIgnore
    public gs7 getOptionGroup() {
        return this.group;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public xr7(com.fossil.gs7 r3, com.fossil.fs7 r4) {
        /*
            r2 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "The option '"
            r0.append(r1)
            java.lang.String r1 = r4.getKey()
            r0.append(r1)
            java.lang.String r1 = "' was specified but an option from this group "
            r0.append(r1)
            java.lang.String r1 = "has already been selected: '"
            r0.append(r1)
            java.lang.String r1 = r3.getSelected()
            r0.append(r1)
            java.lang.String r1 = "'"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.group = r3
            r2.option = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xr7.<init>(com.fossil.gs7, com.fossil.fs7):void");
    }
}
