package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p97<T> implements Collection<T>, ye7 {
    @DexIgnore
    public /* final */ T[] a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public p97(T[] tArr, boolean z) {
        ee7.b(tArr, "values");
        this.a = tArr;
        this.b = z;
    }

    @DexIgnore
    public int a() {
        return this.a.length;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return t97.a(this.a, obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.a.length == 0;
    }

    @DexIgnore
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<T> iterator() {
        return td7.a(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public final Object[] toArray() {
        return v97.a(this.a, this.b);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }
}
