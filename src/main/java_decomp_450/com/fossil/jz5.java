package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz5 implements Factory<r26> {
    @DexIgnore
    public static r26 a(hz5 hz5) {
        r26 b = hz5.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
