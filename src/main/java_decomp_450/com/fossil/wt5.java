package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt5 implements Factory<vt5> {
    @DexIgnore
    public static vt5 a(qt5 qt5, rl4 rl4, vu5 vu5, xu5 xu5, yu5 yu5, nw5 nw5, ch5 ch5, NotificationSettingsDao notificationSettingsDao, dm5 dm5, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new vt5(qt5, rl4, vu5, xu5, yu5, nw5, ch5, notificationSettingsDao, dm5, quickResponseRepository, notificationSettingsDatabase);
    }
}
