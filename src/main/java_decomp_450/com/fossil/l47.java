package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ int b;

    @DexIgnore
    public l47(Context context, int i) {
        this.a = context;
        this.b = i;
    }

    @DexIgnore
    public final void run() {
        try {
            z37.f(this.a);
            x47.b(this.a).a(this.b);
        } catch (Throwable th) {
            z37.m.a(th);
            z37.a(this.a, th);
        }
    }
}
