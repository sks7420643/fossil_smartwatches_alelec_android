package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt2<E> extends ws2<E> {
    @DexIgnore
    public /* final */ transient E c;
    @DexIgnore
    public transient int d;

    @DexIgnore
    public tt2(E e) {
        or2.a(e);
        this.c = e;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean contains(Object obj) {
        return this.c.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = this.c.hashCode();
        this.d = hashCode;
        return hashCode;
    }

    @DexIgnore
    public final int size() {
        return 1;
    }

    @DexIgnore
    public final String toString() {
        String obj = this.c.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final boolean zza() {
        return this.d != 0;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<E> iterator() {
        return new gt2(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ws2
    public final os2<E> zzd() {
        return os2.zza(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return false;
    }

    @DexIgnore
    public tt2(E e, int i) {
        this.c = e;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        objArr[i] = this.c;
        return i + 1;
    }
}
