package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d36 implements Factory<c36> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ d36 a; // = new d36();
    }

    @DexIgnore
    public static d36 a() {
        return a.a;
    }

    @DexIgnore
    public static c36 b() {
        return new c36();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public c36 get() {
        return b();
    }
}
