package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class id5 extends r40 implements Cloneable {
    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 I() {
        super.I();
        return this;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 J() {
        return (id5) super.J();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 K() {
        return (id5) super.K();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 L() {
        return (id5) super.L();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 d() {
        return (id5) super.d();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 h() {
        return (id5) super.h();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 b(boolean z) {
        return (id5) super.b(z);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40, com.fossil.k40, java.lang.Object
    public r40 clone() {
        return (id5) super.clone();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 b(int i) {
        return (id5) super.b(i);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(float f) {
        return (id5) super.a(f);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(iy iyVar) {
        return (id5) super.a(iyVar);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(ew ewVar) {
        return (id5) super.a(ewVar);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(boolean z) {
        return (id5) super.a(z);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(int i, int i2) {
        return (id5) super.a(i, i2);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(yw ywVar) {
        return (id5) super.a(ywVar);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public <Y> r40 a(zw<Y> zwVar, Y y) {
        return (id5) super.a((zw) zwVar, (Object) y);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(Class<?> cls) {
        return (id5) super.a(cls);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(r10 r10) {
        return (id5) super.a(r10);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(ex<Bitmap> exVar) {
        return (id5) super.a(exVar);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.id5' to match base method */
    @Override // com.fossil.k40
    public r40 a(k40<?> k40) {
        return (id5) super.a(k40);
    }
}
