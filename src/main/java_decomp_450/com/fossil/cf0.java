package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cf0 extends k60 implements Parcelable {
    @DexIgnore
    public /* final */ yb0 a;
    @DexIgnore
    public /* final */ df0 b;

    @DexIgnore
    public cf0(yb0 yb0, df0 df0) {
        this.a = yb0;
        this.b = df0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        Object obj;
        Object obj2;
        JSONObject jSONObject = new JSONObject();
        try {
            r51 r51 = r51.n;
            yb0 yb0 = this.a;
            if (yb0 == null || (obj = yb0.a()) == null) {
                obj = JSONObject.NULL;
            }
            yz0.a(jSONObject, r51, obj);
            r51 r512 = r51.o;
            df0 df0 = this.b;
            if (df0 == null || (obj2 = df0.a()) == null) {
                obj2 = JSONObject.NULL;
            }
            yz0.a(jSONObject, r512, obj2);
            yz0.a(jSONObject, r51.W2, b());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract byte[] a(short s, r60 r60);

    @DexIgnore
    public JSONObject b() {
        return new JSONObject();
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            cf0 cf0 = (cf0) obj;
            return !(ee7.a(this.a, cf0.a) ^ true) && !(ee7.a(this.b, cf0.b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.DeviceData");
    }

    @DexIgnore
    public final df0 getDeviceMessage() {
        return this.b;
    }

    @DexIgnore
    public final yb0 getDeviceRequest() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        yb0 yb0 = this.a;
        int i = 0;
        int hashCode = yb0 != null ? yb0.hashCode() : 0;
        df0 df0 = this.b;
        if (df0 != null) {
            i = df0.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }
}
