package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleImageButton s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ImageView u;

    @DexIgnore
    public y95(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleImageButton flexibleImageButton, ConstraintLayout constraintLayout, ImageView imageView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleImageButton;
        this.t = constraintLayout;
        this.u = imageView;
    }

    @DexIgnore
    public static y95 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static y95 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (y95) ViewDataBinding.a(layoutInflater, 2131558702, viewGroup, z, obj);
    }
}
