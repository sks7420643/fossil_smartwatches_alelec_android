package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ua0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<nf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public nf0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(fc0.class.getClassLoader());
            if (readParcelable != null) {
                fc0 fc0 = (fc0) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(df0.class.getClassLoader());
                if (readParcelable2 != null) {
                    df0 df0 = (df0) readParcelable2;
                    Parcelable readParcelable3 = parcel.readParcelable(ua0.class.getClassLoader());
                    if (readParcelable3 != null) {
                        return new nf0(fc0, df0, (ua0) readParcelable3);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public nf0[] newArray(int i) {
            return new nf0[i];
        }
    }

    @DexIgnore
    public nf0(fc0 fc0, ua0 ua0) {
        super(fc0, null);
        this.c = ua0;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("weatherInfo", this.c.b());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(super.b(), this.c.a());
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(nf0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.c, ((nf0) obj).c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherComplicationData");
    }

    @DexIgnore
    public final ua0 getWeatherInfo() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public nf0(fc0 fc0, df0 df0, ua0 ua0) {
        super(fc0, df0);
        this.c = ua0;
    }

    @DexIgnore
    public nf0(ua0 ua0) {
        this(null, null, ua0);
    }
}
