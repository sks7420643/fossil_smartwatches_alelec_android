package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt4 {
    @DexIgnore
    public /* final */ oy6.b a;
    @DexIgnore
    public /* final */ st4 b; // = st4.d.a();
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ gr5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ia5 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gt4$a$a")
        /* renamed from: com.fossil.gt4$a$a  reason: collision with other inner class name */
        public static final class C0069a extends fe7 implements gd7<View, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ st4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ oy6.b $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ un4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ gr5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0069a(a aVar, un4 un4, oy6.b bVar, st4 st4, gr5 gr5) {
                super(1);
                this.this$0 = aVar;
                this.$friend$inlined = un4;
                this.$drawableBuilder$inlined = bVar;
                this.$colorGenerator$inlined = st4;
                this.$listener$inlined = gr5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(View view) {
                invoke(view);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "accept: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.b(this.$friend$inlined);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends fe7 implements gd7<View, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ st4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ oy6.b $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ un4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ gr5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, un4 un4, oy6.b bVar, st4 st4, gr5 gr5) {
                super(1);
                this.this$0 = aVar;
                this.$friend$inlined = un4;
                this.$drawableBuilder$inlined = bVar;
                this.$colorGenerator$inlined = st4;
                this.$listener$inlined = gr5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(View view) {
                invoke(view);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "reject: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.a(this.$friend$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ia5 ia5) {
            super(ia5.d());
            ee7.b(ia5, "binding");
            this.a = ia5;
        }

        @DexIgnore
        public final void a(un4 un4, gr5 gr5, oy6.b bVar, st4 st4) {
            ee7.b(un4, "friend");
            ee7.b(gr5, "listener");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            ia5 ia5 = this.a;
            String a2 = fu4.a.a(un4.b(), un4.e(), un4.i());
            we7 we7 = we7.a;
            FlexibleTextView flexibleTextView = ia5.v;
            ee7.a((Object) flexibleTextView, "tvFriendRequested");
            String a3 = ig5.a(flexibleTextView.getContext(), 2131886292);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026rnameWantsToBeYourFriend)");
            String format = String.format(a3, Arrays.copyOf(new Object[]{a2}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = ia5.v;
            ee7.a((Object) flexibleTextView2, "tvFriendRequested");
            flexibleTextView2.setText(xe5.a(xe5.b, a2, format, 0, 4, null));
            ImageView imageView = ia5.s;
            ee7.a((Object) imageView, "ivAvatar");
            rt4.a(imageView, un4.h(), a2, bVar, st4);
            FlexibleButton flexibleButton = ia5.q;
            ee7.a((Object) flexibleButton, "btnAccept");
            du4.a(flexibleButton, new C0069a(this, un4, bVar, st4, gr5));
            FlexibleButton flexibleButton2 = ia5.r;
            ee7.a((Object) flexibleButton2, "btnDecline");
            du4.a(flexibleButton2, new b(this, un4, bVar, st4, gr5));
        }
    }

    @DexIgnore
    public gt4(int i, gr5 gr5) {
        ee7.b(gr5, "listener");
        this.c = i;
        this.d = gr5;
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.a = b2;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean a(List<? extends Object> list, int i) {
        ee7.b(list, "items");
        Object obj = list.get(i);
        return (obj instanceof un4) && ((un4) obj).c() == 2;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        ee7.b(viewGroup, "parent");
        ia5 a2 = ia5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemSentRequestFriendBin\u2026.context), parent, false)");
        return new a(a2);
    }

    @DexIgnore
    public void a(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        ee7.b(list, "items");
        ee7.b(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof un4)) {
            obj = null;
        }
        un4 un4 = (un4) obj;
        if (!(viewHolder instanceof a)) {
            viewHolder = null;
        }
        a aVar = (a) viewHolder;
        if (un4 != null && aVar != null) {
            aVar.a(un4, this.d, this.a, this.b);
        }
    }
}
