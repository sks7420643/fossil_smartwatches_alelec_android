package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.mm6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm6 extends go5 implements gy6, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public mm6 g;
    @DexIgnore
    public qw6<uz4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jm6.q;
        }

        @DexIgnore
        public final String b() {
            return jm6.p;
        }

        @DexIgnore
        public final String c() {
            return jm6.r;
        }

        @DexIgnore
        public final String d() {
            return jm6.s;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<mm6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public b(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mm6.b bVar) {
            if (bVar != null) {
                Integer d = bVar.d();
                if (d != null) {
                    this.a.q(d.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.p(c.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.o(b.intValue());
                }
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.n(a2.intValue());
                }
                Integer f = bVar.f();
                if (f != null) {
                    this.a.s(f.intValue());
                }
                Integer e = bVar.e();
                if (e != null) {
                    this.a.r(e.intValue());
                }
                Integer h = bVar.h();
                if (h != null) {
                    this.a.u(h.intValue());
                }
                Integer g = bVar.g();
                if (g != null) {
                    this.a.t(g.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public c(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 401);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public d(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, Action.ActivityTracker.TAG_ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public e(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, MFNetworkReturnCode.WRONG_PASSWORD);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public f(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 404);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm6 a;

        @DexIgnore
        public g(jm6 jm6) {
            this.a = jm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.i(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = jm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeRingChartFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363307) {
            mm6 mm6 = this.g;
            if (mm6 != null) {
                mm6.a(cn6.q.a(), p, q, r, s);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        we7 we7 = we7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3 + " hexColor=" + format);
        mm6 mm6 = this.g;
        if (mm6 != null) {
            mm6.a(i2, Color.parseColor(format));
            switch (i2) {
                case 401:
                    p = format;
                    return;
                case Action.ActivityTracker.TAG_ACTIVITY /*{ENCODED_INT: 402}*/:
                    q = format;
                    return;
                case MFNetworkReturnCode.WRONG_PASSWORD /*{ENCODED_INT: 403}*/:
                    r = format;
                    return;
                case 404:
                    s = format;
                    return;
                default:
                    return;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.A.setProgressRingColorPreview(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.I.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        uz4 uz4 = (uz4) qb.a(LayoutInflater.from(getContext()), 2131558536, null, false, a1());
        PortfolioApp.g0.c().f().a(new lm6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(mm6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            mm6 mm6 = (mm6) a2;
            this.g = mm6;
            if (mm6 != null) {
                mm6.b().a(getViewLifecycleOwner(), new b(this));
                mm6 mm62 = this.g;
                if (mm62 != null) {
                    mm62.c();
                    this.h = new qw6<>(this, uz4);
                    ee7.a((Object) uz4, "binding");
                    return uz4.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        p = null;
        q = null;
        r = null;
        s = null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        mm6 mm6 = this.g;
        if (mm6 != null) {
            mm6.c();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.B.setProgress(1.0f);
                a2.A.setProgress(1.0f);
                a2.C.setProgress(1.0f);
                a2.D.setProgress(1.0f);
                a2.x.setOnClickListener(new c(this));
                a2.w.setOnClickListener(new d(this));
                a2.y.setOnClickListener(new e(this));
                a2.z.setOnClickListener(new f(this));
                a2.v.setOnClickListener(new g(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.B.setProgressRingColorPreview(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void q(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.J.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void r(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.C.setProgressRingColorPreview(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.K.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.D.setProgressRingColorPreview(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void u(int i2) {
        qw6<uz4> qw6 = this.h;
        if (qw6 != null) {
            uz4 a2 = qw6.a();
            if (a2 != null) {
                a2.L.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }
}
