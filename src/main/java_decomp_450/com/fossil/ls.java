package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ks;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls extends ts implements ks {
    @DexIgnore
    public /* final */ vt a;
    @DexIgnore
    public /* final */ ds b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.memory.InvalidatableTargetDelegate", f = "TargetDelegate.kt", l = {220}, m = "error")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ls this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ls lsVar, fb7 fb7) {
            super(fb7);
            this.this$0 = lsVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.memory.InvalidatableTargetDelegate", f = "TargetDelegate.kt", l = {203}, m = "success")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ls this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ls lsVar, fb7 fb7) {
            super(fb7);
            this.this$0 = lsVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Drawable) null, false, (zt) null, (fb7<? super i97>) this);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ls(vt vtVar, ds dsVar) {
        super(null);
        ee7.b(vtVar, "target");
        ee7.b(dsVar, "referenceCounter");
        this.a = vtVar;
        this.b = dsVar;
    }

    @DexIgnore
    @Override // com.fossil.ks
    public ds a() {
        return this.b;
    }

    @DexIgnore
    public void c(Bitmap bitmap) {
        ks.a.a(this, bitmap);
    }

    @DexIgnore
    @Override // com.fossil.ts
    public void a(BitmapDrawable bitmapDrawable, Drawable drawable) {
        c(bitmapDrawable != null ? bitmapDrawable.getBitmap() : null);
        this.a.c(drawable);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.ts
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(android.graphics.drawable.Drawable r6, boolean r7, com.fossil.zt r8, com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r5 = this;
            boolean r0 = r9 instanceof com.fossil.ls.b
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.ls$b r0 = (com.fossil.ls.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ls$b r0 = new com.fossil.ls$b
            r0.<init>(r5, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r6 = r0.L$3
            com.fossil.vt r6 = (com.fossil.vt) r6
            java.lang.Object r6 = r0.L$2
            com.fossil.zt r6 = (com.fossil.zt) r6
            boolean r6 = r0.Z$0
            java.lang.Object r6 = r0.L$1
            android.graphics.drawable.Drawable r6 = (android.graphics.drawable.Drawable) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.ls r6 = (com.fossil.ls) r6
            com.fossil.t87.a(r9)
            goto L_0x00ac
        L_0x003b:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0043:
            com.fossil.t87.a(r9)
            android.graphics.Bitmap r9 = com.fossil.us.b(r6)
            r5.c(r9)
            com.fossil.vt r9 = r5.a
            if (r8 != 0) goto L_0x0055
            r9.a(r6)
            goto L_0x00ac
        L_0x0055:
            boolean r2 = r9 instanceof com.fossil.bu
            if (r2 != 0) goto L_0x0091
            r7 = 5
            com.fossil.cu r0 = com.fossil.cu.c
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x008d
            com.fossil.cu r0 = com.fossil.cu.c
            int r0 = r0.b()
            if (r0 > r7) goto L_0x008d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Ignoring '"
            r0.append(r1)
            r0.append(r8)
            java.lang.String r8 = "' as '"
            r0.append(r8)
            r0.append(r9)
            java.lang.String r8 = "' does not implement coil.transition.TransitionTarget."
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            java.lang.String r0 = "TargetDelegate"
            android.util.Log.println(r7, r0, r8)
        L_0x008d:
            r9.a(r6)
            goto L_0x00ac
        L_0x0091:
            r2 = r9
            com.fossil.bu r2 = (com.fossil.bu) r2
            com.fossil.au$b r4 = new com.fossil.au$b
            r4.<init>(r6, r7)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.Z$0 = r7
            r0.L$2 = r8
            r0.L$3 = r9
            r0.label = r3
            java.lang.Object r6 = r8.a(r2, r4, r0)
            if (r6 != r1) goto L_0x00ac
            return r1
        L_0x00ac:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ls.a(android.graphics.drawable.Drawable, boolean, com.fossil.zt, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @Override // com.fossil.ts
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(android.graphics.drawable.Drawable r6, com.fossil.zt r7, com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof com.fossil.ls.a
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.ls$a r0 = (com.fossil.ls.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ls$a r0 = new com.fossil.ls$a
            r0.<init>(r5, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r6 = r0.L$3
            com.fossil.vt r6 = (com.fossil.vt) r6
            java.lang.Object r6 = r0.L$2
            com.fossil.zt r6 = (com.fossil.zt) r6
            java.lang.Object r6 = r0.L$1
            android.graphics.drawable.Drawable r6 = (android.graphics.drawable.Drawable) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.ls r6 = (com.fossil.ls) r6
            com.fossil.t87.a(r8)
            goto L_0x00a1
        L_0x0039:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0041:
            com.fossil.t87.a(r8)
            com.fossil.vt r8 = r5.a
            if (r7 != 0) goto L_0x004c
            r8.b(r6)
            goto L_0x00a1
        L_0x004c:
            boolean r2 = r8 instanceof com.fossil.bu
            if (r2 != 0) goto L_0x0088
            r0 = 5
            com.fossil.cu r1 = com.fossil.cu.c
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x0084
            com.fossil.cu r1 = com.fossil.cu.c
            int r1 = r1.b()
            if (r1 > r0) goto L_0x0084
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Ignoring '"
            r1.append(r2)
            r1.append(r7)
            java.lang.String r7 = "' as '"
            r1.append(r7)
            r1.append(r8)
            java.lang.String r7 = "' does not implement coil.transition.TransitionTarget."
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            java.lang.String r1 = "TargetDelegate"
            android.util.Log.println(r0, r1, r7)
        L_0x0084:
            r8.b(r6)
            goto L_0x00a1
        L_0x0088:
            r2 = r8
            com.fossil.bu r2 = (com.fossil.bu) r2
            com.fossil.au$a r4 = new com.fossil.au$a
            r4.<init>(r6)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.L$2 = r7
            r0.L$3 = r8
            r0.label = r3
            java.lang.Object r6 = r7.a(r2, r4, r0)
            if (r6 != r1) goto L_0x00a1
            return r1
        L_0x00a1:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ls.a(android.graphics.drawable.Drawable, com.fossil.zt, com.fossil.fb7):java.lang.Object");
    }
}
