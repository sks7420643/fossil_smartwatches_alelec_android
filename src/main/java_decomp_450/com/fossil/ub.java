package com.fossil;

import android.content.res.AssetManager;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.facebook.share.internal.VideoUploader;
import com.facebook.stetho.dumpapp.Framer;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ub {
    @DexIgnore
    public static /* final */ byte[] A; // = {-119, 80, 78, 71, 13, 10, 26, 10};
    @DexIgnore
    public static /* final */ byte[] B; // = {101, 88, 73, 102};
    @DexIgnore
    public static /* final */ byte[] C; // = {73, 72, 68, 82};
    @DexIgnore
    public static /* final */ byte[] D; // = {73, 69, 78, 68};
    @DexIgnore
    public static /* final */ byte[] E; // = {82, 73, 70, 70};
    @DexIgnore
    public static /* final */ byte[] F; // = {87, 69, 66, 80};
    @DexIgnore
    public static /* final */ byte[] G; // = {69, 88, 73, 70};
    @DexIgnore
    public static SimpleDateFormat H;
    @DexIgnore
    public static /* final */ String[] I; // = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
    @DexIgnore
    public static /* final */ int[] J; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    @DexIgnore
    public static /* final */ byte[] K; // = {65, 83, 67, 73, 73, 0, 0, 0};
    @DexIgnore
    public static /* final */ d[] L; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ImageWidth", 256, 3, 4), new d("ImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("Orientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", Action.Presenter.NEXT, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("SensorTopBorder", 4, 4), new d("SensorLeftBorder", 5, 4), new d("SensorBottomBorder", 6, 4), new d("SensorRightBorder", 7, 4), new d("ISO", 23, 3), new d("JpgFromRaw", 46, 7), new d("Xmp", 700, 1)};
    @DexIgnore
    public static /* final */ d[] M; // = {new d("ExposureTime", 33434, 5), new d("FNumber", 33437, 5), new d("ExposureProgram", 34850, 3), new d("SpectralSensitivity", 34852, 2), new d("PhotographicSensitivity", 34855, 3), new d("OECF", 34856, 7), new d("SensitivityType", 34864, 3), new d("StandardOutputSensitivity", 34865, 4), new d("RecommendedExposureIndex", 34866, 4), new d("ISOSpeed", 34867, 4), new d("ISOSpeedLatitudeyyy", 34868, 4), new d("ISOSpeedLatitudezzz", 34869, 4), new d("ExifVersion", 36864, 2), new d("DateTimeOriginal", 36867, 2), new d("DateTimeDigitized", 36868, 2), new d("OffsetTime", 36880, 2), new d("OffsetTimeOriginal", 36881, 2), new d("OffsetTimeDigitized", 36882, 2), new d("ComponentsConfiguration", 37121, 7), new d("CompressedBitsPerPixel", 37122, 5), new d("ShutterSpeedValue", 37377, 10), new d("ApertureValue", 37378, 5), new d("BrightnessValue", 37379, 10), new d("ExposureBiasValue", 37380, 10), new d("MaxApertureValue", 37381, 5), new d("SubjectDistance", 37382, 5), new d("MeteringMode", 37383, 3), new d("LightSource", 37384, 3), new d("Flash", 37385, 3), new d("FocalLength", 37386, 5), new d("SubjectArea", 37396, 3), new d("MakerNote", 37500, 7), new d("UserComment", 37510, 7), new d("SubSecTime", 37520, 2), new d("SubSecTimeOriginal", 37521, 2), new d("SubSecTimeDigitized", 37522, 2), new d("FlashpixVersion", 40960, 7), new d("ColorSpace", 40961, 3), new d("PixelXDimension", 40962, 3, 4), new d("PixelYDimension", 40963, 3, 4), new d("RelatedSoundFile", 40964, 2), new d("InteroperabilityIFDPointer", 40965, 4), new d("FlashEnergy", 41483, 5), new d("SpatialFrequencyResponse", 41484, 7), new d("FocalPlaneXResolution", 41486, 5), new d("FocalPlaneYResolution", 41487, 5), new d("FocalPlaneResolutionUnit", 41488, 3), new d("SubjectLocation", 41492, 3), new d("ExposureIndex", 41493, 5), new d("SensingMethod", 41495, 3), new d("FileSource", 41728, 7), new d("SceneType", 41729, 7), new d("CFAPattern", 41730, 7), new d("CustomRendered", 41985, 3), new d("ExposureMode", 41986, 3), new d("WhiteBalance", 41987, 3), new d("DigitalZoomRatio", 41988, 5), new d("FocalLengthIn35mmFilm", 41989, 3), new d("SceneCaptureType", 41990, 3), new d("GainControl", 41991, 3), new d("Contrast", 41992, 3), new d("Saturation", 41993, 3), new d("Sharpness", 41994, 3), new d("DeviceSettingDescription", 41995, 7), new d("SubjectDistanceRange", 41996, 3), new d("ImageUniqueID", 42016, 2), new d("CameraOwnerName", 42032, 2), new d("BodySerialNumber", 42033, 2), new d("LensSpecification", 42034, 5), new d("LensMake", 42035, 2), new d("LensModel", 42036, 2), new d("Gamma", 42240, 5), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d[] N; // = {new d("GPSVersionID", 0, 1), new d("GPSLatitudeRef", 1, 2), new d("GPSLatitude", 2, 5), new d("GPSLongitudeRef", 3, 2), new d("GPSLongitude", 4, 5), new d("GPSAltitudeRef", 5, 1), new d("GPSAltitude", 6, 5), new d("GPSTimeStamp", 7, 5), new d("GPSSatellites", 8, 2), new d("GPSStatus", 9, 2), new d("GPSMeasureMode", 10, 2), new d("GPSDOP", 11, 5), new d("GPSSpeedRef", 12, 2), new d("GPSSpeed", 13, 5), new d("GPSTrackRef", 14, 2), new d("GPSTrack", 15, 5), new d("GPSImgDirectionRef", 16, 2), new d("GPSImgDirection", 17, 5), new d("GPSMapDatum", 18, 2), new d("GPSDestLatitudeRef", 19, 2), new d("GPSDestLatitude", 20, 5), new d("GPSDestLongitudeRef", 21, 2), new d("GPSDestLongitude", 22, 5), new d("GPSDestBearingRef", 23, 2), new d("GPSDestBearing", 24, 5), new d("GPSDestDistanceRef", 25, 2), new d("GPSDestDistance", 26, 5), new d("GPSProcessingMethod", 27, 7), new d("GPSAreaInformation", 28, 7), new d("GPSDateStamp", 29, 2), new d("GPSDifferential", 30, 3), new d("GPSHPositioningError", 31, 5)};
    @DexIgnore
    public static /* final */ d[] O; // = {new d("InteroperabilityIndex", 1, 2)};
    @DexIgnore
    public static /* final */ d[] P; // = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ThumbnailImageWidth", 256, 3, 4), new d("ThumbnailImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("ThumbnailOrientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", Action.Presenter.NEXT, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
    @DexIgnore
    public static /* final */ d Q; // = new d("StripOffsets", 273, 3);
    @DexIgnore
    public static /* final */ d[] R; // = {new d("ThumbnailImage", 256, 7), new d("CameraSettingsIFDPointer", 8224, 4), new d("ImageProcessingIFDPointer", 8256, 4)};
    @DexIgnore
    public static /* final */ d[] S; // = {new d("PreviewImageStart", 257, 4), new d("PreviewImageLength", 258, 4)};
    @DexIgnore
    public static /* final */ d[] T; // = {new d("AspectFrame", 4371, 3)};
    @DexIgnore
    public static /* final */ d[] U;
    @DexIgnore
    public static /* final */ d[][] V;
    @DexIgnore
    public static /* final */ d[] W; // = {new d("SubIFDPointer", 330, 4), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("InteroperabilityIFDPointer", 40965, 4), new d("CameraSettingsIFDPointer", 8224, 1), new d("ImageProcessingIFDPointer", 8256, 1)};
    @DexIgnore
    public static /* final */ HashMap<Integer, d>[] X;
    @DexIgnore
    public static /* final */ HashMap<String, d>[] Y;
    @DexIgnore
    public static /* final */ HashSet<String> Z; // = new HashSet<>(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
    @DexIgnore
    public static /* final */ HashMap<Integer, Integer> a0; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Charset b0;
    @DexIgnore
    public static /* final */ byte[] c0;
    @DexIgnore
    public static /* final */ byte[] d0; // = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(b0);
    @DexIgnore
    public static /* final */ boolean r; // = Log.isLoggable("ExifInterface", 3);
    @DexIgnore
    public static /* final */ int[] s; // = {8, 8, 8};
    @DexIgnore
    public static /* final */ int[] t; // = {8};
    @DexIgnore
    public static /* final */ byte[] u; // = {-1, -40, -1};
    @DexIgnore
    public static /* final */ byte[] v; // = {102, 116, 121, 112};
    @DexIgnore
    public static /* final */ byte[] w; // = {109, 105, 102, Framer.STDOUT_FRAME_PREFIX};
    @DexIgnore
    public static /* final */ byte[] x; // = {104, 101, 105, 99};
    @DexIgnore
    public static /* final */ byte[] y; // = {79, 76, 89, 77, 80, 0};
    @DexIgnore
    public static /* final */ byte[] z; // = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};
    @DexIgnore
    public String a;
    @DexIgnore
    public FileDescriptor b;
    @DexIgnore
    public AssetManager.AssetInputStream c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ HashMap<String, c>[] f;
    @DexIgnore
    public Set<Integer> g;
    @DexIgnore
    public ByteOrder h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends MediaDataSource {
        @DexIgnore
        public long a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;

        @DexIgnore
        public a(ub ubVar, b bVar) {
            this.b = bVar;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @DexIgnore
        @Override // android.media.MediaDataSource
        public long getSize() throws IOException {
            return -1;
        }

        @DexIgnore
        @Override // android.media.MediaDataSource
        public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
            if (i2 == 0) {
                return 0;
            }
            if (j < 0) {
                return -1;
            }
            try {
                if (this.a != j) {
                    if (this.a >= 0 && j >= this.a + ((long) this.b.available())) {
                        return -1;
                    }
                    this.b.e(j);
                    this.a = j;
                }
                if (i2 > this.b.available()) {
                    i2 = this.b.available();
                }
                int read = this.b.read(bArr, i, i2);
                if (read >= 0) {
                    this.a += (long) read;
                    return read;
                }
            } catch (IOException unused) {
            }
            this.a = -1;
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends InputStream implements DataInput {
        @DexIgnore
        public static /* final */ ByteOrder e; // = ByteOrder.LITTLE_ENDIAN;
        @DexIgnore
        public static /* final */ ByteOrder f; // = ByteOrder.BIG_ENDIAN;
        @DexIgnore
        public DataInputStream a;
        @DexIgnore
        public ByteOrder b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public b(InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        public void a(ByteOrder byteOrder) {
            this.b = byteOrder;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() throws IOException {
            return this.a.available();
        }

        @DexIgnore
        public long b() throws IOException {
            return ((long) readInt()) & 4294967295L;
        }

        @DexIgnore
        public void e(long j) throws IOException {
            int i = this.d;
            if (((long) i) > j) {
                this.d = 0;
                this.a.reset();
                this.a.mark(this.c);
            } else {
                j -= (long) i;
            }
            int i2 = (int) j;
            if (skipBytes(i2) != i2) {
                throw new IOException("Couldn't seek up to the byteCount");
            }
        }

        @DexIgnore
        public int peek() {
            return this.d;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() throws IOException {
            this.d++;
            return this.a.read();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public boolean readBoolean() throws IOException {
            this.d++;
            return this.a.readBoolean();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public byte readByte() throws IOException {
            int i = this.d + 1;
            this.d = i;
            if (i <= this.c) {
                int read = this.a.read();
                if (read >= 0) {
                    return (byte) read;
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public char readChar() throws IOException {
            this.d += 2;
            return this.a.readChar();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(readLong());
        }

        @DexIgnore
        @Override // java.io.DataInput
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(readInt());
        }

        @DexIgnore
        @Override // java.io.DataInput
        public void readFully(byte[] bArr, int i, int i2) throws IOException {
            int i3 = this.d + i2;
            this.d = i3;
            if (i3 > this.c) {
                throw new EOFException();
            } else if (this.a.read(bArr, i, i2) != i2) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readInt() throws IOException {
            int i = this.d + 4;
            this.d = i;
            if (i <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                if ((read | read2 | read3 | read4) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public String readLine() throws IOException {
            Log.d("ExifInterface", "Currently unsupported");
            return null;
        }

        @DexIgnore
        @Override // java.io.DataInput
        public long readLong() throws IOException {
            int i = this.d + 8;
            this.d = i;
            if (i <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                int read5 = this.a.read();
                int read6 = this.a.read();
                int read7 = this.a.read();
                int read8 = this.a.read();
                if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (((long) read8) << 56) + (((long) read7) << 48) + (((long) read6) << 40) + (((long) read5) << 32) + (((long) read4) << 24) + (((long) read3) << 16) + (((long) read2) << 8) + ((long) read);
                    }
                    if (byteOrder == f) {
                        return (((long) read) << 56) + (((long) read2) << 48) + (((long) read3) << 40) + (((long) read4) << 32) + (((long) read5) << 24) + (((long) read6) << 16) + (((long) read7) << 8) + ((long) read8);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public short readShort() throws IOException {
            int i = this.d + 2;
            this.d = i;
            if (i <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (short) ((read2 << 8) + read);
                    }
                    if (byteOrder == f) {
                        return (short) ((read << 8) + read2);
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public String readUTF() throws IOException {
            this.d += 2;
            return this.a.readUTF();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readUnsignedByte() throws IOException {
            this.d++;
            return this.a.readUnsignedByte();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int readUnsignedShort() throws IOException {
            int i = this.d + 2;
            this.d = i;
            if (i <= this.c) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.b;
                    if (byteOrder == e) {
                        return (read2 << 8) + read;
                    }
                    if (byteOrder == f) {
                        return (read << 8) + read2;
                    }
                    throw new IOException("Invalid byte order: " + this.b);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @DexIgnore
        @Override // java.io.DataInput
        public int skipBytes(int i) throws IOException {
            int min = Math.min(i, this.c - this.d);
            int i2 = 0;
            while (i2 < min) {
                i2 += this.a.skipBytes(min - i2);
            }
            this.d += i2;
            return i2;
        }

        @DexIgnore
        public b(InputStream inputStream, ByteOrder byteOrder) throws IOException {
            this.b = ByteOrder.BIG_ENDIAN;
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            this.a = dataInputStream;
            int available = dataInputStream.available();
            this.c = available;
            this.d = 0;
            this.a.mark(available);
            this.b = byteOrder;
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.a.read(bArr, i, i2);
            this.d += read;
            return read;
        }

        @DexIgnore
        @Override // java.io.DataInput
        public void readFully(byte[] bArr) throws IOException {
            int length = this.d + bArr.length;
            this.d = length;
            if (length > this.c) {
                throw new EOFException();
            } else if (this.a.read(bArr, 0, bArr.length) != bArr.length) {
                throw new IOException("Couldn't read up to the length of buffer");
            }
        }

        @DexIgnore
        public b(byte[] bArr) throws IOException {
            this(new ByteArrayInputStream(bArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ byte[] c;

        @DexIgnore
        public c(int i, int i2, byte[] bArr) {
            this(i, i2, -1, bArr);
        }

        @DexIgnore
        public static c a(int[] iArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ub.J[3] * iArr.length)]);
            wrap.order(byteOrder);
            for (int i : iArr) {
                wrap.putShort((short) i);
            }
            return new c(3, iArr.length, wrap.array());
        }

        @DexIgnore
        public int b(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            } else if (d instanceof String) {
                return Integer.parseInt((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (int) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
            }
        }

        @DexIgnore
        public String c(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                return null;
            }
            if (d instanceof String) {
                return (String) d;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (d instanceof long[]) {
                long[] jArr = (long[]) d;
                while (i < jArr.length) {
                    sb.append(jArr[i]);
                    i++;
                    if (i != jArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof int[]) {
                int[] iArr = (int[]) d;
                while (i < iArr.length) {
                    sb.append(iArr[i]);
                    i++;
                    if (i != iArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (d instanceof double[]) {
                double[] dArr = (double[]) d;
                while (i < dArr.length) {
                    sb.append(dArr[i]);
                    i++;
                    if (i != dArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (!(d instanceof e[])) {
                return null;
            } else {
                e[] eVarArr = (e[]) d;
                while (i < eVarArr.length) {
                    sb.append(eVarArr[i].a);
                    sb.append('/');
                    sb.append(eVarArr[i].b);
                    i++;
                    if (i != eVarArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:138:0x019e A[SYNTHETIC, Splitter:B:138:0x019e] */
        /* JADX WARNING: Removed duplicated region for block: B:146:0x01ab A[SYNTHETIC, Splitter:B:146:0x01ab] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object d(java.nio.ByteOrder r11) {
            /*
                r10 = this;
                java.lang.String r0 = "IOException occurred while closing InputStream"
                java.lang.String r1 = "ExifInterface"
                r2 = 0
                com.fossil.ub$b r3 = new com.fossil.ub$b     // Catch:{ IOException -> 0x0195, all -> 0x0193 }
                byte[] r4 = r10.c     // Catch:{ IOException -> 0x0195, all -> 0x0193 }
                r3.<init>(r4)     // Catch:{ IOException -> 0x0195, all -> 0x0193 }
                r3.a(r11)     // Catch:{ IOException -> 0x0191 }
                int r11 = r10.a     // Catch:{ IOException -> 0x0191 }
                r4 = 1
                r5 = 0
                switch(r11) {
                    case 1: goto L_0x014c;
                    case 2: goto L_0x00fd;
                    case 3: goto L_0x00e3;
                    case 4: goto L_0x00c9;
                    case 5: goto L_0x00a6;
                    case 6: goto L_0x014c;
                    case 7: goto L_0x00fd;
                    case 8: goto L_0x008c;
                    case 9: goto L_0x0072;
                    case 10: goto L_0x004d;
                    case 11: goto L_0x0032;
                    case 12: goto L_0x0018;
                    default: goto L_0x0016;
                }     // Catch:{ IOException -> 0x0191 }
            L_0x0016:
                goto L_0x0188
            L_0x0018:
                int r11 = r10.b     // Catch:{ IOException -> 0x0191 }
                double[] r11 = new double[r11]     // Catch:{ IOException -> 0x0191 }
            L_0x001c:
                int r4 = r10.b     // Catch:{ IOException -> 0x0191 }
                if (r5 >= r4) goto L_0x0029
                double r6 = r3.readDouble()     // Catch:{ IOException -> 0x0191 }
                r11[r5] = r6     // Catch:{ IOException -> 0x0191 }
                int r5 = r5 + 1
                goto L_0x001c
            L_0x0029:
                r3.close()     // Catch:{ IOException -> 0x002d }
                goto L_0x0031
            L_0x002d:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0031:
                return r11
            L_0x0032:
                int r11 = r10.b
                double[] r11 = new double[r11]
            L_0x0036:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x0044
                float r4 = r3.readFloat()
                double r6 = (double) r4
                r11[r5] = r6
                int r5 = r5 + 1
                goto L_0x0036
            L_0x0044:
                r3.close()     // Catch:{ IOException -> 0x0048 }
                goto L_0x004c
            L_0x0048:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x004c:
                return r11
            L_0x004d:
                int r11 = r10.b
                com.fossil.ub$e[] r11 = new com.fossil.ub.e[r11]
            L_0x0051:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x0069
                int r4 = r3.readInt()
                long r6 = (long) r4
                int r4 = r3.readInt()
                long r8 = (long) r4
                com.fossil.ub$e r4 = new com.fossil.ub$e
                r4.<init>(r6, r8)
                r11[r5] = r4
                int r5 = r5 + 1
                goto L_0x0051
            L_0x0069:
                r3.close()     // Catch:{ IOException -> 0x006d }
                goto L_0x0071
            L_0x006d:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0071:
                return r11
            L_0x0072:
                int r11 = r10.b
                int[] r11 = new int[r11]
            L_0x0076:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x0083
                int r4 = r3.readInt()
                r11[r5] = r4
                int r5 = r5 + 1
                goto L_0x0076
            L_0x0083:
                r3.close()     // Catch:{ IOException -> 0x0087 }
                goto L_0x008b
            L_0x0087:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x008b:
                return r11
            L_0x008c:
                int r11 = r10.b
                int[] r11 = new int[r11]
            L_0x0090:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x009d
                short r4 = r3.readShort()
                r11[r5] = r4
                int r5 = r5 + 1
                goto L_0x0090
            L_0x009d:
                r3.close()     // Catch:{ IOException -> 0x00a1 }
                goto L_0x00a5
            L_0x00a1:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00a5:
                return r11
            L_0x00a6:
                int r11 = r10.b
                com.fossil.ub$e[] r11 = new com.fossil.ub.e[r11]
            L_0x00aa:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x00c0
                long r6 = r3.b()
                long r8 = r3.b()
                com.fossil.ub$e r4 = new com.fossil.ub$e
                r4.<init>(r6, r8)
                r11[r5] = r4
                int r5 = r5 + 1
                goto L_0x00aa
            L_0x00c0:
                r3.close()     // Catch:{ IOException -> 0x00c4 }
                goto L_0x00c8
            L_0x00c4:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00c8:
                return r11
            L_0x00c9:
                int r11 = r10.b
                long[] r11 = new long[r11]
            L_0x00cd:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x00da
                long r6 = r3.b()
                r11[r5] = r6
                int r5 = r5 + 1
                goto L_0x00cd
            L_0x00da:
                r3.close()     // Catch:{ IOException -> 0x00de }
                goto L_0x00e2
            L_0x00de:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00e2:
                return r11
            L_0x00e3:
                int r11 = r10.b
                int[] r11 = new int[r11]
            L_0x00e7:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x00f4
                int r4 = r3.readUnsignedShort()
                r11[r5] = r4
                int r5 = r5 + 1
                goto L_0x00e7
            L_0x00f4:
                r3.close()     // Catch:{ IOException -> 0x00f8 }
                goto L_0x00fc
            L_0x00f8:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x00fc:
                return r11
            L_0x00fd:
                int r11 = r10.b
                byte[] r6 = com.fossil.ub.K
                int r6 = r6.length
                if (r11 < r6) goto L_0x011e
                r11 = 0
            L_0x0105:
                byte[] r6 = com.fossil.ub.K
                int r6 = r6.length
                if (r11 >= r6) goto L_0x0119
                byte[] r6 = r10.c
                byte r6 = r6[r11]
                byte[] r7 = com.fossil.ub.K
                byte r7 = r7[r11]
                if (r6 == r7) goto L_0x0116
                r4 = 0
                goto L_0x0119
            L_0x0116:
                int r11 = r11 + 1
                goto L_0x0105
            L_0x0119:
                if (r4 == 0) goto L_0x011e
                byte[] r11 = com.fossil.ub.K
                int r5 = r11.length
            L_0x011e:
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
            L_0x0123:
                int r4 = r10.b
                if (r5 >= r4) goto L_0x013f
                byte[] r4 = r10.c
                byte r4 = r4[r5]
                if (r4 != 0) goto L_0x012e
                goto L_0x013f
            L_0x012e:
                r6 = 32
                if (r4 < r6) goto L_0x0137
                char r4 = (char) r4
                r11.append(r4)
                goto L_0x013c
            L_0x0137:
                r4 = 63
                r11.append(r4)
            L_0x013c:
                int r5 = r5 + 1
                goto L_0x0123
            L_0x013f:
                java.lang.String r11 = r11.toString()
                r3.close()     // Catch:{ IOException -> 0x0147 }
                goto L_0x014b
            L_0x0147:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x014b:
                return r11
            L_0x014c:
                byte[] r11 = r10.c
                int r11 = r11.length
                if (r11 != r4) goto L_0x0176
                byte[] r11 = r10.c
                byte r11 = r11[r5]
                if (r11 < 0) goto L_0x0176
                byte[] r11 = r10.c
                byte r11 = r11[r5]
                if (r11 > r4) goto L_0x0176
                java.lang.String r11 = new java.lang.String
                char[] r4 = new char[r4]
                byte[] r6 = r10.c
                byte r6 = r6[r5]
                int r6 = r6 + 48
                char r6 = (char) r6
                r4[r5] = r6
                r11.<init>(r4)
                r3.close()     // Catch:{ IOException -> 0x0171 }
                goto L_0x0175
            L_0x0171:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0175:
                return r11
            L_0x0176:
                java.lang.String r11 = new java.lang.String
                byte[] r4 = r10.c
                java.nio.charset.Charset r5 = com.fossil.ub.b0
                r11.<init>(r4, r5)
                r3.close()     // Catch:{ IOException -> 0x0183 }
                goto L_0x0187
            L_0x0183:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x0187:
                return r11
            L_0x0188:
                r3.close()     // Catch:{ IOException -> 0x018c }
                goto L_0x0190
            L_0x018c:
                r11 = move-exception
                android.util.Log.e(r1, r0, r11)
            L_0x0190:
                return r2
            L_0x0191:
                r11 = move-exception
                goto L_0x0197
            L_0x0193:
                r11 = move-exception
                goto L_0x01a9
            L_0x0195:
                r11 = move-exception
                r3 = r2
            L_0x0197:
                java.lang.String r4 = "IOException occurred during reading a value"
                android.util.Log.w(r1, r4, r11)     // Catch:{ all -> 0x01a7 }
                if (r3 == 0) goto L_0x01a6
                r3.close()     // Catch:{ IOException -> 0x01a2 }
                goto L_0x01a6
            L_0x01a2:
                r11 = move-exception
                android.util.Log.e(r1, r0, r11)
            L_0x01a6:
                return r2
            L_0x01a7:
                r11 = move-exception
                r2 = r3
            L_0x01a9:
                if (r2 == 0) goto L_0x01b3
                r2.close()     // Catch:{ IOException -> 0x01af }
                goto L_0x01b3
            L_0x01af:
                r2 = move-exception
                android.util.Log.e(r1, r0, r2)
            L_0x01b3:
                throw r11
                switch-data {1->0x014c, 2->0x00fd, 3->0x00e3, 4->0x00c9, 5->0x00a6, 6->0x014c, 7->0x00fd, 8->0x008c, 9->0x0072, 10->0x004d, 11->0x0032, 12->0x0018, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.c.d(java.nio.ByteOrder):java.lang.Object");
        }

        @DexIgnore
        public String toString() {
            return "(" + ub.I[this.a] + ", data length:" + this.c.length + ")";
        }

        @DexIgnore
        public c(int i, int i2, long j, byte[] bArr) {
            this.a = i;
            this.b = i2;
            this.c = bArr;
        }

        @DexIgnore
        public static c a(int i, ByteOrder byteOrder) {
            return a(new int[]{i}, byteOrder);
        }

        @DexIgnore
        public static c a(long[] jArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ub.J[4] * jArr.length)]);
            wrap.order(byteOrder);
            for (long j : jArr) {
                wrap.putInt((int) j);
            }
            return new c(4, jArr.length, wrap.array());
        }

        @DexIgnore
        public static c a(long j, ByteOrder byteOrder) {
            return a(new long[]{j}, byteOrder);
        }

        @DexIgnore
        public static c a(String str) {
            byte[] bytes = (str + (char) 0).getBytes(ub.b0);
            return new c(2, bytes.length, bytes);
        }

        @DexIgnore
        public static c a(e[] eVarArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[(ub.J[5] * eVarArr.length)]);
            wrap.order(byteOrder);
            for (e eVar : eVarArr) {
                wrap.putInt((int) eVar.a);
                wrap.putInt((int) eVar.b);
            }
            return new c(5, eVarArr.length, wrap.array());
        }

        @DexIgnore
        public static c a(e eVar, ByteOrder byteOrder) {
            return a(new e[]{eVar}, byteOrder);
        }

        @DexIgnore
        public double a(ByteOrder byteOrder) {
            Object d = d(byteOrder);
            if (d == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            } else if (d instanceof String) {
                return Double.parseDouble((String) d);
            } else {
                if (d instanceof long[]) {
                    long[] jArr = (long[]) d;
                    if (jArr.length == 1) {
                        return (double) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof int[]) {
                    int[] iArr = (int[]) d;
                    if (iArr.length == 1) {
                        return (double) iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof double[]) {
                    double[] dArr = (double[]) d;
                    if (dArr.length == 1) {
                        return dArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (d instanceof e[]) {
                    e[] eVarArr = (e[]) d;
                    if (eVarArr.length == 1) {
                        return eVarArr[0].a();
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a double value");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public e(long j, long j2) {
            if (j2 == 0) {
                this.a = 0;
                this.b = 1;
                return;
            }
            this.a = j;
            this.b = j2;
        }

        @DexIgnore
        public double a() {
            return ((double) this.a) / ((double) this.b);
        }

        @DexIgnore
        public String toString() {
            return this.a + "/" + this.b;
        }
    }

    /*
    static {
        Arrays.asList(1, 6, 3, 8);
        Arrays.asList(2, 7, 4, 5);
        "VP8X".getBytes(Charset.defaultCharset());
        "VP8L".getBytes(Charset.defaultCharset());
        "VP8 ".getBytes(Charset.defaultCharset());
        "ANIM".getBytes(Charset.defaultCharset());
        "ANMF".getBytes(Charset.defaultCharset());
        "XMP ".getBytes(Charset.defaultCharset());
        d[] dVarArr = {new d("ColorSpace", 55, 3)};
        U = dVarArr;
        d[] dVarArr2 = L;
        V = new d[][]{dVarArr2, M, N, O, P, dVarArr2, R, S, T, dVarArr};
        new d("JPEGInterchangeFormat", 513, 4);
        new d("JPEGInterchangeFormatLength", 514, 4);
        d[][] dVarArr3 = V;
        X = new HashMap[dVarArr3.length];
        Y = new HashMap[dVarArr3.length];
        Charset forName = Charset.forName("US-ASCII");
        b0 = forName;
        c0 = "Exif\u0000\u0000".getBytes(forName);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        H = simpleDateFormat;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i2 = 0; i2 < V.length; i2++) {
            X[i2] = new HashMap<>();
            Y[i2] = new HashMap<>();
            d[] dVarArr4 = V[i2];
            for (d dVar : dVarArr4) {
                X[i2].put(Integer.valueOf(dVar.a), dVar);
                Y[i2].put(dVar.b, dVar);
            }
        }
        a0.put(Integer.valueOf(W[0].a), 5);
        a0.put(Integer.valueOf(W[1].a), 1);
        a0.put(Integer.valueOf(W[2].a), 2);
        a0.put(Integer.valueOf(W[3].a), 3);
        a0.put(Integer.valueOf(W[4].a), 7);
        a0.put(Integer.valueOf(W[5].a), 8);
        Pattern.compile(".*[1-9].*");
        Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
    }
    */

    @DexIgnore
    public ub(String str) throws IOException {
        this.f = new HashMap[V.length];
        this.g = new HashSet(V.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (str != null) {
            c(str);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }

    @DexIgnore
    public static boolean h(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = u;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public String a(String str) {
        if (str != null) {
            c b2 = b(str);
            if (b2 != null) {
                if (!Z.contains(str)) {
                    return b2.c(this.h);
                }
                if (str.equals("GPSTimeStamp")) {
                    int i2 = b2.a;
                    if (i2 == 5 || i2 == 10) {
                        e[] eVarArr = (e[]) b2.d(this.h);
                        if (eVarArr == null || eVarArr.length != 3) {
                            Log.w("ExifInterface", "Invalid GPS Timestamp array. array=" + Arrays.toString(eVarArr));
                            return null;
                        }
                        return String.format("%02d:%02d:%02d", Integer.valueOf((int) (((float) eVarArr[0].a) / ((float) eVarArr[0].b))), Integer.valueOf((int) (((float) eVarArr[1].a) / ((float) eVarArr[1].b))), Integer.valueOf((int) (((float) eVarArr[2].a) / ((float) eVarArr[2].b))));
                    }
                    Log.w("ExifInterface", "GPS Timestamp format is not rational. format=" + b2.a);
                    return null;
                }
                try {
                    return Double.toString(b2.a(this.h));
                } catch (NumberFormatException unused) {
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public final c b(String str) {
        if (str != null) {
            if ("ISOSpeedRatings".equals(str)) {
                if (r) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                str = "PhotographicSensitivity";
            }
            for (int i2 = 0; i2 < V.length; i2++) {
                c cVar = this.f[i2].get(str);
                if (cVar != null) {
                    return cVar;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    @DexIgnore
    public int c() {
        switch (a("Orientation", 1)) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 8:
                return 270;
            case 6:
            case 7:
                return 90;
            default:
                return 0;
        }
    }

    @DexIgnore
    public boolean d() {
        int a2 = a("Orientation", 1);
        return a2 == 2 || a2 == 7 || a2 == 4 || a2 == 5;
    }

    @DexIgnore
    public final void e() {
        for (int i2 = 0; i2 < this.f.length; i2++) {
            Log.d("ExifInterface", "The size of tag group[" + i2 + "]: " + this.f[i2].size());
            for (Map.Entry<String, c> entry : this.f[i2].entrySet()) {
                c value = entry.getValue();
                Log.d("ExifInterface", "tagName: " + entry.getKey() + ", tagType: " + value.toString() + ", tagValue: '" + value.c(this.h) + "'");
            }
        }
    }

    @DexIgnore
    public final boolean f(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = E;
            if (i2 >= bArr2.length) {
                int i3 = 0;
                while (true) {
                    byte[] bArr3 = F;
                    if (i3 >= bArr3.length) {
                        return true;
                    }
                    if (bArr[E.length + i3 + 4] != bArr3[i3]) {
                        return false;
                    }
                    i3++;
                }
            } else if (bArr[i2] != bArr2[i2]) {
                return false;
            } else {
                i2++;
            }
        }
    }

    @DexIgnore
    public final void g(b bVar) throws IOException {
        bVar.skipBytes(c0.length);
        byte[] bArr = new byte[bVar.available()];
        bVar.readFully(bArr);
        this.m = c0.length;
        a(bArr, 0);
    }

    @DexIgnore
    public final ByteOrder i(b bVar) throws IOException {
        short readShort = bVar.readShort();
        if (readShort == 18761) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        } else if (readShort == 19789) {
            if (r) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        } else {
            throw new IOException("Invalid byte order: " + Integer.toHexString(readShort));
        }
    }

    @DexIgnore
    public final void j(b bVar) throws IOException {
        HashMap<String, c> hashMap = this.f[4];
        c cVar = hashMap.get("Compression");
        if (cVar != null) {
            int b2 = cVar.b(this.h);
            this.l = b2;
            if (b2 != 1) {
                if (b2 == 6) {
                    a(bVar, hashMap);
                    return;
                } else if (b2 != 7) {
                    return;
                }
            }
            if (a((HashMap) hashMap)) {
                b(bVar, hashMap);
                return;
            }
            return;
        }
        this.l = 6;
        a(bVar, hashMap);
    }

    @DexIgnore
    public final void c(String str) throws IOException {
        if (str != null) {
            FileInputStream fileInputStream = null;
            this.c = null;
            this.a = str;
            try {
                FileInputStream fileInputStream2 = new FileInputStream(str);
                try {
                    if (a(fileInputStream2.getFD())) {
                        this.b = fileInputStream2.getFD();
                    } else {
                        this.b = null;
                    }
                    a((InputStream) fileInputStream2);
                    a((Closeable) fileInputStream2);
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    a((Closeable) fileInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                a((Closeable) fileInputStream);
                throw th;
            }
        } else {
            throw new NullPointerException("filename cannot be null");
        }
    }

    @DexIgnore
    public final boolean d(byte[] bArr) throws IOException {
        byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i2 = 0; i2 < bytes.length; i2++) {
            if (bArr[i2] != bytes[i2]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final void h(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getWebpAttributes starting with: " + bVar);
        }
        bVar.a(ByteOrder.LITTLE_ENDIAN);
        bVar.skipBytes(E.length);
        int readInt = bVar.readInt() + 8;
        int skipBytes = bVar.skipBytes(F.length) + 8;
        while (true) {
            try {
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == 4) {
                    int readInt2 = bVar.readInt();
                    int i2 = skipBytes + 4 + 4;
                    if (Arrays.equals(G, bArr)) {
                        byte[] bArr2 = new byte[readInt2];
                        if (bVar.read(bArr2) == readInt2) {
                            this.m = i2;
                            a(bArr2, 0);
                            this.m = i2;
                            return;
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + g(bArr));
                    }
                    if (readInt2 % 2 == 1) {
                        readInt2++;
                    }
                    int i3 = i2 + readInt2;
                    if (i3 != readInt) {
                        if (i3 <= readInt) {
                            int skipBytes2 = bVar.skipBytes(readInt2);
                            if (skipBytes2 == readInt2) {
                                skipBytes = i2 + skipBytes2;
                            } else {
                                throw new IOException("Encountered WebP file with invalid chunk size");
                            }
                        } else {
                            throw new IOException("Encountered WebP file with invalid chunk size");
                        }
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt WebP file.");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public d(String str, int i, int i2) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = -1;
        }

        @DexIgnore
        public boolean a(int i) {
            int i2;
            int i3 = this.c;
            if (i3 == 7 || i == 7 || i3 == i || (i2 = this.d) == i) {
                return true;
            }
            if ((i3 == 4 || i2 == 4) && i == 3) {
                return true;
            }
            if ((this.c == 9 || this.d == 9) && i == 8) {
                return true;
            }
            if ((this.c == 12 || this.d == 12) && i == 11) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public d(String str, int i, int i2, int i3) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = i3;
        }
    }

    @DexIgnore
    public final void d(b bVar) throws IOException {
        bVar.skipBytes(84);
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[4];
        bVar.read(bArr);
        bVar.skipBytes(4);
        bVar.read(bArr2);
        int i2 = ByteBuffer.wrap(bArr).getInt();
        int i3 = ByteBuffer.wrap(bArr2).getInt();
        a(bVar, i2, 5);
        bVar.e((long) i3);
        bVar.a(ByteOrder.BIG_ENDIAN);
        int readInt = bVar.readInt();
        if (r) {
            Log.d("ExifInterface", "numberOfDirectoryEntry: " + readInt);
        }
        for (int i4 = 0; i4 < readInt; i4++) {
            int readUnsignedShort = bVar.readUnsignedShort();
            int readUnsignedShort2 = bVar.readUnsignedShort();
            if (readUnsignedShort == Q.a) {
                short readShort = bVar.readShort();
                short readShort2 = bVar.readShort();
                c a2 = c.a((int) readShort, this.h);
                c a3 = c.a((int) readShort2, this.h);
                this.f[0].put("ImageLength", a2);
                this.f[0].put("ImageWidth", a3);
                if (r) {
                    Log.d("ExifInterface", "Updated to length: " + ((int) readShort) + ", width: " + ((int) readShort2));
                    return;
                }
                return;
            }
            bVar.skipBytes(readUnsignedShort2);
        }
    }

    @DexIgnore
    public final void f(b bVar) throws IOException {
        e(bVar);
        if (this.f[0].get("JpgFromRaw") != null) {
            a(bVar, this.q, 5);
        }
        c cVar = this.f[0].get("ISO");
        c cVar2 = this.f[1].get("PhotographicSensitivity");
        if (cVar != null && cVar2 == null) {
            this.f[1].put("PhotographicSensitivity", cVar);
        }
    }

    @DexIgnore
    public static String g(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i2])));
        }
        return sb.toString();
    }

    @DexIgnore
    public ub(InputStream inputStream) throws IOException {
        this(inputStream, false);
    }

    @DexIgnore
    public double[] b() {
        String a2 = a("GPSLatitude");
        String a3 = a("GPSLatitudeRef");
        String a4 = a("GPSLongitude");
        String a5 = a("GPSLongitudeRef");
        if (a2 == null || a3 == null || a4 == null || a5 == null) {
            return null;
        }
        try {
            return new double[]{a(a2, a3), a(a4, a5)};
        } catch (IllegalArgumentException unused) {
            Log.w("ExifInterface", "Latitude/longitude values are not parsable. " + String.format("latValue=%s, latRef=%s, lngValue=%s, lngRef=%s", a2, a3, a4, a5));
            return null;
        }
    }

    @DexIgnore
    public ub(InputStream inputStream, boolean z2) throws IOException {
        this.f = new HashMap[V.length];
        this.g = new HashSet(V.length);
        this.h = ByteOrder.BIG_ENDIAN;
        if (inputStream != null) {
            this.a = null;
            if (z2) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                if (!b(bufferedInputStream)) {
                    Log.w("ExifInterface", "Given data does not follow the structure of an Exif-only data.");
                    return;
                }
                this.e = true;
                this.c = null;
                this.b = null;
                inputStream = bufferedInputStream;
            } else if (inputStream instanceof AssetManager.AssetInputStream) {
                this.c = (AssetManager.AssetInputStream) inputStream;
                this.b = null;
            } else {
                if (inputStream instanceof FileInputStream) {
                    FileInputStream fileInputStream = (FileInputStream) inputStream;
                    if (a(fileInputStream.getFD())) {
                        this.c = null;
                        this.b = fileInputStream.getFD();
                    }
                }
                this.c = null;
                this.b = null;
            }
            a(inputStream);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(byte[] r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 0
            r1 = 0
            com.fossil.ub$b r2 = new com.fossil.ub$b     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            java.nio.ByteOrder r4 = r3.i(r2)     // Catch:{ Exception -> 0x0020, all -> 0x001d }
            r3.h = r4     // Catch:{ Exception -> 0x0020, all -> 0x001d }
            r2.a(r4)     // Catch:{ Exception -> 0x0020, all -> 0x001d }
            short r4 = r2.readShort()     // Catch:{ Exception -> 0x0020, all -> 0x001d }
            r1 = 85
            if (r4 != r1) goto L_0x0019
            r0 = 1
        L_0x0019:
            r2.close()
            return r0
        L_0x001d:
            r4 = move-exception
            r1 = r2
            goto L_0x0023
        L_0x0020:
            r1 = r2
            goto L_0x002a
        L_0x0022:
            r4 = move-exception
        L_0x0023:
            if (r1 == 0) goto L_0x0028
            r1.close()
        L_0x0028:
            throw r4
        L_0x0029:
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()
        L_0x002f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.e(byte[]):boolean");
    }

    @DexIgnore
    public final boolean c(byte[] bArr) throws IOException {
        int i2 = 0;
        while (true) {
            byte[] bArr2 = A;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public final void c(b bVar) throws IOException {
        if (r) {
            Log.d("ExifInterface", "getPngAttributes starting with: " + bVar);
        }
        bVar.a(ByteOrder.BIG_ENDIAN);
        bVar.skipBytes(A.length);
        int length = A.length + 0;
        while (true) {
            try {
                int readInt = bVar.readInt();
                int i2 = length + 4;
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == 4) {
                    int i3 = i2 + 4;
                    if (i3 == 16) {
                        if (!Arrays.equals(bArr, C)) {
                            throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                        }
                    }
                    if (!Arrays.equals(bArr, D)) {
                        if (Arrays.equals(bArr, B)) {
                            byte[] bArr2 = new byte[readInt];
                            if (bVar.read(bArr2) == readInt) {
                                int readInt2 = bVar.readInt();
                                CRC32 crc32 = new CRC32();
                                crc32.update(bArr);
                                crc32.update(bArr2);
                                if (((int) crc32.getValue()) == readInt2) {
                                    this.m = i3;
                                    a(bArr2, 0);
                                    f();
                                    return;
                                }
                                throw new IOException("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: " + readInt2 + ", calculated CRC value: " + crc32.getValue());
                            }
                            throw new IOException("Failed to read given length for given PNG chunk type: " + g(bArr));
                        }
                        int i4 = readInt + 4;
                        bVar.skipBytes(i4);
                        length = i3 + i4;
                    } else {
                        return;
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing PNG chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt PNG file.");
            }
        }
    }

    @DexIgnore
    public final void f() throws IOException {
        a(0, 5);
        a(0, 4);
        a(5, 4);
        c cVar = this.f[1].get("PixelXDimension");
        c cVar2 = this.f[1].get("PixelYDimension");
        if (!(cVar == null || cVar2 == null)) {
            this.f[0].put("ImageWidth", cVar);
            this.f[0].put("ImageLength", cVar2);
        }
        if (this.f[4].isEmpty() && b(this.f[5])) {
            HashMap<String, c>[] hashMapArr = this.f;
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap<>();
        }
        if (!b(this.f[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
    }

    @DexIgnore
    public final void e(b bVar) throws IOException {
        c cVar;
        a(bVar, bVar.available());
        b(bVar, 0);
        d(bVar, 0);
        d(bVar, 5);
        d(bVar, 4);
        f();
        if (this.d == 8 && (cVar = this.f[1].get("MakerNote")) != null) {
            b bVar2 = new b(cVar.c);
            bVar2.a(this.h);
            bVar2.e(6);
            b(bVar2, 9);
            c cVar2 = this.f[9].get("ColorSpace");
            if (cVar2 != null) {
                this.f[1].put("ColorSpace", cVar2);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean b(byte[] r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 0
            r1 = 0
            com.fossil.ub$b r2 = new com.fossil.ub$b     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x002d, all -> 0x0026 }
            java.nio.ByteOrder r4 = r3.i(r2)     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            r3.h = r4     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            r2.a(r4)     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            short r4 = r2.readShort()     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            r1 = 20306(0x4f52, float:2.8455E-41)
            if (r4 == r1) goto L_0x001c
            r1 = 21330(0x5352, float:2.989E-41)
            if (r4 != r1) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            r2.close()
            return r0
        L_0x0021:
            r4 = move-exception
            r1 = r2
            goto L_0x0027
        L_0x0024:
            r1 = r2
            goto L_0x002e
        L_0x0026:
            r4 = move-exception
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            throw r4
        L_0x002d:
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()
        L_0x0033:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.b(byte[]):boolean");
    }

    @DexIgnore
    public int a(String str, int i2) {
        if (str != null) {
            c b2 = b(str);
            if (b2 == null) {
                return i2;
            }
            try {
                return b2.b(this.h);
            } catch (NumberFormatException unused) {
                return i2;
            }
        } else {
            throw new NullPointerException("tag shouldn't be null");
        }
    }

    @DexIgnore
    public final void a(InputStream inputStream) {
        if (inputStream != null) {
            int i2 = 0;
            while (i2 < V.length) {
                try {
                    this.f[i2] = new HashMap<>();
                    i2++;
                } catch (IOException e2) {
                    if (r) {
                        Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", e2);
                    }
                    a();
                    if (!r) {
                        return;
                    }
                } catch (Throwable th) {
                    a();
                    if (r) {
                        e();
                    }
                    throw th;
                }
            }
            if (!this.e) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, VideoUploader.RETRY_DELAY_UNIT_MS);
                this.d = a(bufferedInputStream);
                inputStream = bufferedInputStream;
            }
            b bVar = new b(inputStream);
            if (!this.e) {
                switch (this.d) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 11:
                        e(bVar);
                        break;
                    case 4:
                        a(bVar, 0, 0);
                        break;
                    case 7:
                        b(bVar);
                        break;
                    case 9:
                        d(bVar);
                        break;
                    case 10:
                        f(bVar);
                        break;
                    case 12:
                        a(bVar);
                        break;
                    case 13:
                        c(bVar);
                        break;
                    case 14:
                        h(bVar);
                        break;
                }
            } else {
                g(bVar);
            }
            j(bVar);
            a();
            if (!r) {
                return;
            }
            e();
            return;
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }

    @DexIgnore
    public static boolean b(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(c0.length);
        byte[] bArr = new byte[c0.length];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        int i2 = 0;
        while (true) {
            byte[] bArr2 = c0;
            if (i2 >= bArr2.length) {
                return true;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    public final void b(b bVar) throws IOException {
        e(bVar);
        c cVar = this.f[1].get("MakerNote");
        if (cVar != null) {
            b bVar2 = new b(cVar.c);
            bVar2.a(this.h);
            byte[] bArr = new byte[y.length];
            bVar2.readFully(bArr);
            bVar2.e(0);
            byte[] bArr2 = new byte[z.length];
            bVar2.readFully(bArr2);
            if (Arrays.equals(bArr, y)) {
                bVar2.e(8);
            } else if (Arrays.equals(bArr2, z)) {
                bVar2.e(12);
            }
            b(bVar2, 6);
            c cVar2 = this.f[7].get("PreviewImageStart");
            c cVar3 = this.f[7].get("PreviewImageLength");
            if (!(cVar2 == null || cVar3 == null)) {
                this.f[5].put("JPEGInterchangeFormat", cVar2);
                this.f[5].put("JPEGInterchangeFormatLength", cVar3);
            }
            c cVar4 = this.f[8].get("AspectFrame");
            if (cVar4 != null) {
                int[] iArr = (int[]) cVar4.d(this.h);
                if (iArr == null || iArr.length != 4) {
                    Log.w("ExifInterface", "Invalid aspect frame values. frame=" + Arrays.toString(iArr));
                } else if (iArr[2] > iArr[0] && iArr[3] > iArr[1]) {
                    int i2 = (iArr[2] - iArr[0]) + 1;
                    int i3 = (iArr[3] - iArr[1]) + 1;
                    if (i2 < i3) {
                        int i4 = i2 + i3;
                        i3 = i4 - i3;
                        i2 = i4 - i3;
                    }
                    c a2 = c.a(i2, this.h);
                    c a3 = c.a(i3, this.h);
                    this.f[0].put("ImageWidth", a2);
                    this.f[0].put("ImageLength", a3);
                }
            }
        }
    }

    @DexIgnore
    public final void d(b bVar, int i2) throws IOException {
        c cVar;
        c cVar2;
        c cVar3 = this.f[i2].get("DefaultCropSize");
        c cVar4 = this.f[i2].get("SensorTopBorder");
        c cVar5 = this.f[i2].get("SensorLeftBorder");
        c cVar6 = this.f[i2].get("SensorBottomBorder");
        c cVar7 = this.f[i2].get("SensorRightBorder");
        if (cVar3 != null) {
            if (cVar3.a == 5) {
                e[] eVarArr = (e[]) cVar3.d(this.h);
                if (eVarArr == null || eVarArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(eVarArr));
                    return;
                }
                cVar2 = c.a(eVarArr[0], this.h);
                cVar = c.a(eVarArr[1], this.h);
            } else {
                int[] iArr = (int[]) cVar3.d(this.h);
                if (iArr == null || iArr.length != 2) {
                    Log.w("ExifInterface", "Invalid crop size values. cropSize=" + Arrays.toString(iArr));
                    return;
                }
                cVar2 = c.a(iArr[0], this.h);
                cVar = c.a(iArr[1], this.h);
            }
            this.f[i2].put("ImageWidth", cVar2);
            this.f[i2].put("ImageLength", cVar);
        } else if (cVar4 == null || cVar5 == null || cVar6 == null || cVar7 == null) {
            c(bVar, i2);
        } else {
            int b2 = cVar4.b(this.h);
            int b3 = cVar6.b(this.h);
            int b4 = cVar7.b(this.h);
            int b5 = cVar5.b(this.h);
            if (b3 > b2 && b4 > b5) {
                c a2 = c.a(b3 - b2, this.h);
                c a3 = c.a(b4 - b5, this.h);
                this.f[i2].put("ImageLength", a2);
                this.f[i2].put("ImageWidth", a3);
            }
        }
    }

    @DexIgnore
    public final void c(b bVar, int i2) throws IOException {
        c cVar;
        c cVar2 = this.f[i2].get("ImageLength");
        c cVar3 = this.f[i2].get("ImageWidth");
        if ((cVar2 == null || cVar3 == null) && (cVar = this.f[i2].get("JPEGInterchangeFormat")) != null) {
            a(bVar, cVar.b(this.h), i2);
        }
    }

    @DexIgnore
    public static boolean a(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.lseek(fileDescriptor, 0, OsConstants.SEEK_CUR);
                return true;
            } catch (Exception unused) {
                if (r) {
                    Log.d("ExifInterface", "The file descriptor for the given input is not seekable");
                }
            }
        }
        return false;
    }

    @DexIgnore
    public static double a(String str, String str2) {
        try {
            String[] split = str.split(",", -1);
            String[] split2 = split[0].split("/", -1);
            String[] split3 = split[1].split("/", -1);
            String[] split4 = split[2].split("/", -1);
            double parseDouble = (Double.parseDouble(split2[0].trim()) / Double.parseDouble(split2[1].trim())) + ((Double.parseDouble(split3[0].trim()) / Double.parseDouble(split3[1].trim())) / 60.0d) + ((Double.parseDouble(split4[0].trim()) / Double.parseDouble(split4[1].trim())) / 3600.0d);
            if (!str2.equals(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX)) {
                if (!str2.equals("W")) {
                    if (!str2.equals("N")) {
                        if (!str2.equals("E")) {
                            throw new IllegalArgumentException();
                        }
                    }
                    return parseDouble;
                }
            }
            return -parseDouble;
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException unused) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02fc  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x015b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.fossil.ub.b r28, int r29) throws java.io.IOException {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            r2 = r29
            java.util.Set<java.lang.Integer> r3 = r0.g
            int r4 = r1.d
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3.add(r4)
            int r3 = r1.d
            r4 = 2
            int r3 = r3 + r4
            int r5 = r1.c
            if (r3 <= r5) goto L_0x001a
            return
        L_0x001a:
            short r3 = r28.readShort()
            boolean r5 = com.fossil.ub.r
            java.lang.String r6 = "ExifInterface"
            if (r5 == 0) goto L_0x0038
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "numberOfDirectoryEntry: "
            r5.append(r7)
            r5.append(r3)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x0038:
            int r5 = r1.d
            int r7 = r3 * 12
            int r5 = r5 + r7
            int r7 = r1.c
            if (r5 > r7) goto L_0x0433
            if (r3 > 0) goto L_0x0045
            goto L_0x0433
        L_0x0045:
            r5 = 0
            r7 = 0
        L_0x0047:
            r8 = 5
            if (r7 >= r3) goto L_0x03a2
            int r13 = r28.readUnsignedShort()
            int r14 = r28.readUnsignedShort()
            int r15 = r28.readInt()
            int r9 = r28.peek()
            long r9 = (long) r9
            r18 = 4
            long r9 = r9 + r18
            java.util.HashMap<java.lang.Integer, com.fossil.ub$d>[] r20 = com.fossil.ub.X
            r12 = r20[r2]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            java.lang.Object r4 = r12.get(r4)
            com.fossil.ub$d r4 = (com.fossil.ub.d) r4
            boolean r12 = com.fossil.ub.r
            r11 = 3
            if (r12 == 0) goto L_0x00a3
            java.lang.Object[] r8 = new java.lang.Object[r8]
            java.lang.Integer r12 = java.lang.Integer.valueOf(r29)
            r8[r5] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r13)
            r20 = 1
            r8[r20] = r12
            if (r4 == 0) goto L_0x0087
            java.lang.String r12 = r4.b
            goto L_0x0088
        L_0x0087:
            r12 = 0
        L_0x0088:
            r22 = 2
            r8[r22] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r14)
            r8[r11] = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r15)
            r21 = 4
            r8[r21] = r12
            java.lang.String r12 = "ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d"
            java.lang.String r8 = java.lang.String.format(r12, r8)
            android.util.Log.d(r6, r8)
        L_0x00a3:
            r8 = 7
            if (r4 != 0) goto L_0x00c2
            boolean r12 = com.fossil.ub.r
            if (r12 == 0) goto L_0x00be
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r5 = "Skip the tag entry since tag number is not defined: "
            r12.append(r5)
            r12.append(r13)
            java.lang.String r5 = r12.toString()
            android.util.Log.d(r6, r5)
        L_0x00be:
            r24 = r9
            goto L_0x0149
        L_0x00c2:
            if (r14 <= 0) goto L_0x012f
            int[] r5 = com.fossil.ub.J
            int r5 = r5.length
            if (r14 < r5) goto L_0x00ca
            goto L_0x012f
        L_0x00ca:
            boolean r5 = r4.a(r14)
            if (r5 != 0) goto L_0x00f7
            boolean r5 = com.fossil.ub.r
            if (r5 == 0) goto L_0x00be
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r12 = "Skip the tag entry since data format ("
            r5.append(r12)
            java.lang.String[] r12 = com.fossil.ub.I
            r12 = r12[r14]
            r5.append(r12)
            java.lang.String r12 = ") is unexpected for tag: "
            r5.append(r12)
            java.lang.String r12 = r4.b
            r5.append(r12)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
            goto L_0x00be
        L_0x00f7:
            if (r14 != r8) goto L_0x00fb
            int r14 = r4.c
        L_0x00fb:
            long r11 = (long) r15
            int[] r23 = com.fossil.ub.J
            r5 = r23[r14]
            r24 = r9
            long r8 = (long) r5
            long r8 = r8 * r11
            r11 = 0
            int r5 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r5 < 0) goto L_0x0115
            r11 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r5 <= 0) goto L_0x0113
            goto L_0x0115
        L_0x0113:
            r5 = 1
            goto L_0x014c
        L_0x0115:
            boolean r5 = com.fossil.ub.r
            if (r5 == 0) goto L_0x012d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r11 = "Skip the tag entry since the number of components is invalid: "
            r5.append(r11)
            r5.append(r15)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x012d:
            r5 = 0
            goto L_0x014c
        L_0x012f:
            r24 = r9
            boolean r5 = com.fossil.ub.r
            if (r5 == 0) goto L_0x0149
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "Skip the tag entry since data format is invalid: "
            r5.append(r8)
            r5.append(r14)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r6, r5)
        L_0x0149:
            r5 = 0
            r8 = 0
        L_0x014c:
            if (r5 != 0) goto L_0x015b
            r11 = r24
            r1.e(r11)
            r23 = r3
            r13 = r6
            r24 = r7
        L_0x0158:
            r7 = 2
            goto L_0x0395
        L_0x015b:
            r11 = r24
            java.lang.String r5 = "Compression"
            int r23 = (r8 > r18 ? 1 : (r8 == r18 ? 0 : -1))
            if (r23 <= 0) goto L_0x022a
            int r10 = r28.readInt()
            boolean r19 = com.fossil.ub.r
            r23 = r3
            if (r19 == 0) goto L_0x0184
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r24 = r7
            java.lang.String r7 = "seek to data offset: "
            r3.append(r7)
            r3.append(r10)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r6, r3)
            goto L_0x0186
        L_0x0184:
            r24 = r7
        L_0x0186:
            int r3 = r0.d
            r7 = 7
            if (r3 != r7) goto L_0x01e6
            java.lang.String r3 = r4.b
            java.lang.String r7 = "MakerNote"
            boolean r3 = r7.equals(r3)
            if (r3 == 0) goto L_0x0198
            r0.n = r10
            goto L_0x01e1
        L_0x0198:
            r3 = 6
            if (r2 != r3) goto L_0x01e1
            java.lang.String r7 = r4.b
            java.lang.String r3 = "ThumbnailImage"
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x01e1
            r0.o = r10
            r0.p = r15
            java.nio.ByteOrder r3 = r0.h
            r7 = 6
            com.fossil.ub$c r3 = com.fossil.ub.c.a(r7, r3)
            int r7 = r0.o
            r19 = r14
            r18 = r15
            long r14 = (long) r7
            java.nio.ByteOrder r7 = r0.h
            com.fossil.ub$c r7 = com.fossil.ub.c.a(r14, r7)
            int r14 = r0.p
            long r14 = (long) r14
            java.nio.ByteOrder r2 = r0.h
            com.fossil.ub$c r2 = com.fossil.ub.c.a(r14, r2)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r14 = r0.f
            r15 = 4
            r14 = r14[r15]
            r14.put(r5, r3)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r3 = r0.f
            r3 = r3[r15]
            java.lang.String r14 = "JPEGInterchangeFormat"
            r3.put(r14, r7)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r3 = r0.f
            r3 = r3[r15]
            java.lang.String r7 = "JPEGInterchangeFormatLength"
            r3.put(r7, r2)
            goto L_0x01fa
        L_0x01e1:
            r19 = r14
            r18 = r15
            goto L_0x01fa
        L_0x01e6:
            r19 = r14
            r18 = r15
            r2 = 10
            if (r3 != r2) goto L_0x01fa
            java.lang.String r2 = r4.b
            java.lang.String r3 = "JpgFromRaw"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x01fa
            r0.q = r10
        L_0x01fa:
            long r2 = (long) r10
            long r14 = r2 + r8
            int r7 = r1.c
            r25 = r4
            r26 = r5
            long r4 = (long) r7
            int r7 = (r14 > r4 ? 1 : (r14 == r4 ? 0 : -1))
            if (r7 > 0) goto L_0x020c
            r1.e(r2)
            goto L_0x0236
        L_0x020c:
            boolean r2 = com.fossil.ub.r
            if (r2 == 0) goto L_0x0224
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Skip the tag entry since data offset is invalid: "
            r2.append(r3)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r6, r2)
        L_0x0224:
            r1.e(r11)
            r13 = r6
            goto L_0x0158
        L_0x022a:
            r23 = r3
            r25 = r4
            r26 = r5
            r24 = r7
            r19 = r14
            r18 = r15
        L_0x0236:
            java.util.HashMap<java.lang.Integer, java.lang.Integer> r2 = com.fossil.ub.a0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r13)
            java.lang.Object r2 = r2.get(r3)
            java.lang.Integer r2 = (java.lang.Integer) r2
            boolean r3 = com.fossil.ub.r
            if (r3 == 0) goto L_0x0262
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "nextIfdType: "
            r3.append(r4)
            r3.append(r2)
            java.lang.String r4 = " byteCount: "
            r3.append(r4)
            r3.append(r8)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r6, r3)
        L_0x0262:
            r3 = 8
            if (r2 == 0) goto L_0x0316
            r4 = -1
            r14 = r19
            r7 = 3
            if (r14 == r7) goto L_0x028a
            r7 = 4
            if (r14 == r7) goto L_0x0285
            if (r14 == r3) goto L_0x0280
            r3 = 9
            if (r14 == r3) goto L_0x027b
            r3 = 13
            if (r14 == r3) goto L_0x027b
            goto L_0x028f
        L_0x027b:
            int r3 = r28.readInt()
            goto L_0x028e
        L_0x0280:
            short r3 = r28.readShort()
            goto L_0x028e
        L_0x0285:
            long r4 = r28.b()
            goto L_0x028f
        L_0x028a:
            int r3 = r28.readUnsignedShort()
        L_0x028e:
            long r4 = (long) r3
        L_0x028f:
            boolean r3 = com.fossil.ub.r
            r7 = 2
            if (r3 == 0) goto L_0x02ad
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.Long r8 = java.lang.Long.valueOf(r4)
            r9 = 0
            r3[r9] = r8
            r10 = r25
            java.lang.String r8 = r10.b
            r9 = 1
            r3[r9] = r8
            java.lang.String r8 = "Offset: %d, tagName: %s"
            java.lang.String r3 = java.lang.String.format(r8, r3)
            android.util.Log.d(r6, r3)
        L_0x02ad:
            r8 = 0
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x02f8
            int r3 = r1.c
            long r8 = (long) r3
            int r3 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x02f8
            java.util.Set<java.lang.Integer> r3 = r0.g
            int r8 = (int) r4
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            boolean r3 = r3.contains(r8)
            if (r3 != 0) goto L_0x02d2
            r1.e(r4)
            int r2 = r2.intValue()
            r0.b(r1, r2)
            goto L_0x0310
        L_0x02d2:
            boolean r3 = com.fossil.ub.r
            if (r3 == 0) goto L_0x0310
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r8 = "Skip jump into the IFD since it has already been read: IfdType "
            r3.append(r8)
            r3.append(r2)
            java.lang.String r2 = " (at "
            r3.append(r2)
            r3.append(r4)
            java.lang.String r2 = ")"
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            android.util.Log.d(r6, r2)
            goto L_0x0310
        L_0x02f8:
            boolean r2 = com.fossil.ub.r
            if (r2 == 0) goto L_0x0310
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Skip jump into the IFD since its offset is invalid: "
            r2.append(r3)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r6, r2)
        L_0x0310:
            r1.e(r11)
            r13 = r6
            goto L_0x0395
        L_0x0316:
            r14 = r19
            r10 = r25
            r2 = r26
            r7 = 2
            int r4 = r28.peek()
            int r13 = r0.m
            int r4 = r4 + r13
            int r9 = (int) r8
            byte[] r8 = new byte[r9]
            r1.readFully(r8)
            com.fossil.ub$c r9 = new com.fossil.ub$c
            r13 = r6
            long r5 = (long) r4
            r4 = r18
            r15 = r9
            r16 = r14
            r17 = r4
            r18 = r5
            r20 = r8
            r15.<init>(r16, r17, r18, r20)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r4 = r0.f
            r4 = r4[r29]
            java.lang.String r5 = r10.b
            r4.put(r5, r9)
            java.lang.String r4 = r10.b
            java.lang.String r5 = "DNGVersion"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x0352
            r4 = 3
            r0.d = r4
        L_0x0352:
            java.lang.String r4 = r10.b
            java.lang.String r5 = "Make"
            boolean r4 = r5.equals(r4)
            if (r4 != 0) goto L_0x0366
            java.lang.String r4 = r10.b
            java.lang.String r5 = "Model"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x0374
        L_0x0366:
            java.nio.ByteOrder r4 = r0.h
            java.lang.String r4 = r9.c(r4)
            java.lang.String r5 = "PENTAX"
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0387
        L_0x0374:
            java.lang.String r4 = r10.b
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0389
            java.nio.ByteOrder r2 = r0.h
            int r2 = r9.b(r2)
            r4 = 65535(0xffff, float:9.1834E-41)
            if (r2 != r4) goto L_0x0389
        L_0x0387:
            r0.d = r3
        L_0x0389:
            int r2 = r28.peek()
            long r2 = (long) r2
            int r4 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r4 == 0) goto L_0x0395
            r1.e(r11)
        L_0x0395:
            int r2 = r24 + 1
            short r2 = (short) r2
            r7 = r2
            r6 = r13
            r3 = r23
            r4 = 2
            r5 = 0
            r2 = r29
            goto L_0x0047
        L_0x03a2:
            r13 = r6
            int r2 = r28.peek()
            r3 = 4
            int r2 = r2 + r3
            int r3 = r1.c
            if (r2 > r3) goto L_0x0433
            int r2 = r28.readInt()
            boolean r3 = com.fossil.ub.r
            if (r3 == 0) goto L_0x03ca
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            r5 = 0
            r3[r5] = r4
            java.lang.String r4 = "nextIfdOffset: %d"
            java.lang.String r3 = java.lang.String.format(r4, r3)
            r4 = r13
            android.util.Log.d(r4, r3)
            goto L_0x03cb
        L_0x03ca:
            r4 = r13
        L_0x03cb:
            long r5 = (long) r2
            r9 = 0
            int r3 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x041b
            int r3 = r1.c
            if (r2 >= r3) goto L_0x041b
            java.util.Set<java.lang.Integer> r3 = r0.g
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)
            boolean r3 = r3.contains(r7)
            if (r3 != 0) goto L_0x0402
            r1.e(r5)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r2 = r0.f
            r3 = 4
            r2 = r2[r3]
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x03f4
            r0.b(r1, r3)
            goto L_0x0433
        L_0x03f4:
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r2 = r0.f
            r2 = r2[r8]
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x0433
            r0.b(r1, r8)
            goto L_0x0433
        L_0x0402:
            boolean r1 = com.fossil.ub.r
            if (r1 == 0) goto L_0x0433
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Stop reading file since re-reading an IFD may cause an infinite loop: "
            r1.append(r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r4, r1)
            goto L_0x0433
        L_0x041b:
            boolean r1 = com.fossil.ub.r
            if (r1 == 0) goto L_0x0433
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Stop reading file since a wrong offset may cause an infinite loop: "
            r1.append(r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r4, r1)
        L_0x0433:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.b(com.fossil.ub$b, int):void");
    }

    @DexIgnore
    public final int a(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(VideoUploader.RETRY_DELAY_UNIT_MS);
        byte[] bArr = new byte[VideoUploader.RETRY_DELAY_UNIT_MS];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        if (h(bArr)) {
            return 4;
        }
        if (d(bArr)) {
            return 9;
        }
        if (a(bArr)) {
            return 12;
        }
        if (b(bArr)) {
            return 7;
        }
        if (e(bArr)) {
            return 10;
        }
        if (c(bArr)) {
            return 13;
        }
        return f(bArr) ? 14 : 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0092 A[Catch:{ all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(byte[] r15) throws java.io.IOException {
        /*
            r14 = this;
            r0 = 0
            r1 = 0
            com.fossil.ub$b r2 = new com.fossil.ub$b     // Catch:{ Exception -> 0x008d }
            r2.<init>(r15)     // Catch:{ Exception -> 0x008d }
            int r1 = r2.readInt()     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            long r3 = (long) r1     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            r1 = 4
            byte[] r5 = new byte[r1]     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            r2.read(r5)     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            byte[] r6 = com.fossil.ub.v     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            boolean r5 = java.util.Arrays.equals(r5, r6)     // Catch:{ Exception -> 0x0088, all -> 0x0085 }
            if (r5 != 0) goto L_0x001e
            r2.close()
            return r0
        L_0x001e:
            r5 = 16
            r7 = 8
            r9 = 1
            int r11 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            if (r11 != 0) goto L_0x0034
            long r3 = r2.readLong()
            int r11 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r11 >= 0) goto L_0x0035
            r2.close()
            return r0
        L_0x0034:
            r5 = r7
        L_0x0035:
            int r11 = r15.length
            long r11 = (long) r11
            int r13 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r13 <= 0) goto L_0x003d
            int r15 = r15.length
            long r3 = (long) r15
        L_0x003d:
            long r3 = r3 - r5
            int r15 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r15 >= 0) goto L_0x0046
            r2.close()
            return r0
        L_0x0046:
            byte[] r15 = new byte[r1]
            r5 = 0
            r7 = 0
            r8 = 0
        L_0x004c:
            r11 = 4
            long r11 = r3 / r11
            int r13 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r13 >= 0) goto L_0x0081
            int r11 = r2.read(r15)
            if (r11 == r1) goto L_0x005e
            r2.close()
            return r0
        L_0x005e:
            int r11 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r11 != 0) goto L_0x0063
            goto L_0x007f
        L_0x0063:
            byte[] r11 = com.fossil.ub.w
            boolean r11 = java.util.Arrays.equals(r15, r11)
            r12 = 1
            if (r11 == 0) goto L_0x006e
            r7 = 1
            goto L_0x0077
        L_0x006e:
            byte[] r11 = com.fossil.ub.x
            boolean r11 = java.util.Arrays.equals(r15, r11)
            if (r11 == 0) goto L_0x0077
            r8 = 1
        L_0x0077:
            if (r7 == 0) goto L_0x007f
            if (r8 == 0) goto L_0x007f
            r2.close()
            return r12
        L_0x007f:
            long r5 = r5 + r9
            goto L_0x004c
        L_0x0081:
            r2.close()
            goto L_0x009e
        L_0x0085:
            r15 = move-exception
            r1 = r2
            goto L_0x009f
        L_0x0088:
            r15 = move-exception
            r1 = r2
            goto L_0x008e
        L_0x008b:
            r15 = move-exception
            goto L_0x009f
        L_0x008d:
            r15 = move-exception
        L_0x008e:
            boolean r2 = com.fossil.ub.r     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x0099
            java.lang.String r2 = "ExifInterface"
            java.lang.String r3 = "Exception parsing HEIF file type box."
            android.util.Log.d(r2, r3, r15)     // Catch:{ all -> 0x008b }
        L_0x0099:
            if (r1 == 0) goto L_0x009e
            r1.close()
        L_0x009e:
            return r0
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.close()
        L_0x00a4:
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.a(byte[]):boolean");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c2 A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.ub.b r20, int r21, int r22) throws java.io.IOException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            r3 = r22
            boolean r4 = com.fossil.ub.r
            java.lang.String r5 = "ExifInterface"
            if (r4 == 0) goto L_0x0022
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "getJpegAttributes starting with: "
            r4.append(r6)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r5, r4)
        L_0x0022:
            java.nio.ByteOrder r4 = java.nio.ByteOrder.BIG_ENDIAN
            r1.a(r4)
            long r6 = (long) r2
            r1.e(r6)
            byte r4 = r20.readByte()
            java.lang.String r6 = "Invalid marker: "
            r7 = -1
            if (r4 != r7) goto L_0x01d6
            r8 = 1
            int r2 = r2 + r8
            byte r9 = r20.readByte()
            r10 = -40
            if (r9 != r10) goto L_0x01bb
            int r2 = r2 + r8
        L_0x003f:
            byte r4 = r20.readByte()
            if (r4 != r7) goto L_0x019e
            int r2 = r2 + r8
            byte r4 = r20.readByte()
            boolean r6 = com.fossil.ub.r
            if (r6 == 0) goto L_0x0068
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = "Found JPEG segment indicator: "
            r6.append(r9)
            r9 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r9 = java.lang.Integer.toHexString(r9)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            android.util.Log.d(r5, r6)
        L_0x0068:
            int r2 = r2 + r8
            r6 = -39
            if (r4 == r6) goto L_0x0198
            r6 = -38
            if (r4 != r6) goto L_0x0073
            goto L_0x0198
        L_0x0073:
            int r6 = r20.readUnsignedShort()
            int r6 = r6 + -2
            int r2 = r2 + 2
            boolean r9 = com.fossil.ub.r
            if (r9 == 0) goto L_0x00a8
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "JPEG segment: "
            r9.append(r10)
            r10 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r10 = java.lang.Integer.toHexString(r10)
            r9.append(r10)
            java.lang.String r10 = " (length: "
            r9.append(r10)
            int r10 = r6 + 2
            r9.append(r10)
            java.lang.String r10 = ")"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            android.util.Log.d(r5, r9)
        L_0x00a8:
            java.lang.String r9 = "Invalid length"
            if (r6 < 0) goto L_0x0192
            r10 = -31
            r11 = 0
            if (r4 == r10) goto L_0x0127
            r10 = -2
            if (r4 == r10) goto L_0x00fc
            switch(r4) {
                case -64: goto L_0x00c2;
                case -63: goto L_0x00c2;
                case -62: goto L_0x00c2;
                case -61: goto L_0x00c2;
                default: goto L_0x00b7;
            }
        L_0x00b7:
            switch(r4) {
                case -59: goto L_0x00c2;
                case -58: goto L_0x00c2;
                case -57: goto L_0x00c2;
                default: goto L_0x00ba;
            }
        L_0x00ba:
            switch(r4) {
                case -55: goto L_0x00c2;
                case -54: goto L_0x00c2;
                case -53: goto L_0x00c2;
                default: goto L_0x00bd;
            }
        L_0x00bd:
            switch(r4) {
                case -51: goto L_0x00c2;
                case -50: goto L_0x00c2;
                case -49: goto L_0x00c2;
                default: goto L_0x00c0;
            }
        L_0x00c0:
            goto L_0x0177
        L_0x00c2:
            int r4 = r1.skipBytes(r8)
            if (r4 != r8) goto L_0x00f4
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r4 = r0.f
            r4 = r4[r3]
            int r10 = r20.readUnsignedShort()
            long r10 = (long) r10
            java.nio.ByteOrder r12 = r0.h
            com.fossil.ub$c r10 = com.fossil.ub.c.a(r10, r12)
            java.lang.String r11 = "ImageLength"
            r4.put(r11, r10)
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r4 = r0.f
            r4 = r4[r3]
            int r10 = r20.readUnsignedShort()
            long r10 = (long) r10
            java.nio.ByteOrder r12 = r0.h
            com.fossil.ub$c r10 = com.fossil.ub.c.a(r10, r12)
            java.lang.String r11 = "ImageWidth"
            r4.put(r11, r10)
            int r6 = r6 + -5
            goto L_0x0177
        L_0x00f4:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid SOFx"
            r1.<init>(r2)
            throw r1
        L_0x00fc:
            byte[] r4 = new byte[r6]
            int r10 = r1.read(r4)
            if (r10 != r6) goto L_0x011f
            java.lang.String r6 = "UserComment"
            java.lang.String r10 = r0.a(r6)
            if (r10 != 0) goto L_0x0176
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r10 = r0.f
            r10 = r10[r8]
            java.lang.String r12 = new java.lang.String
            java.nio.charset.Charset r13 = com.fossil.ub.b0
            r12.<init>(r4, r13)
            com.fossil.ub$c r4 = com.fossil.ub.c.a(r12)
            r10.put(r6, r4)
            goto L_0x0176
        L_0x011f:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid exif"
            r1.<init>(r2)
            throw r1
        L_0x0127:
            byte[] r4 = new byte[r6]
            r1.readFully(r4)
            int r10 = r2 + r6
            byte[] r12 = com.fossil.ub.c0
            boolean r12 = a(r4, r12)
            if (r12 == 0) goto L_0x0145
            byte[] r12 = com.fossil.ub.c0
            int r13 = r12.length
            int r2 = r2 + r13
            int r12 = r12.length
            byte[] r4 = java.util.Arrays.copyOfRange(r4, r12, r6)
            r0.m = r2
            r0.a(r4, r3)
            goto L_0x0175
        L_0x0145:
            byte[] r12 = com.fossil.ub.d0
            boolean r12 = a(r4, r12)
            if (r12 == 0) goto L_0x0175
            byte[] r12 = com.fossil.ub.d0
            int r13 = r12.length
            int r2 = r2 + r13
            int r12 = r12.length
            byte[] r4 = java.util.Arrays.copyOfRange(r4, r12, r6)
            java.lang.String r6 = "Xmp"
            java.lang.String r12 = r0.a(r6)
            if (r12 != 0) goto L_0x0175
            java.util.HashMap<java.lang.String, com.fossil.ub$c>[] r12 = r0.f
            r12 = r12[r11]
            com.fossil.ub$c r15 = new com.fossil.ub$c
            r14 = 1
            int r13 = r4.length
            long r7 = (long) r2
            r2 = r13
            r13 = r15
            r11 = r15
            r15 = r2
            r16 = r7
            r18 = r4
            r13.<init>(r14, r15, r16, r18)
            r12.put(r6, r11)
        L_0x0175:
            r2 = r10
        L_0x0176:
            r6 = 0
        L_0x0177:
            if (r6 < 0) goto L_0x018c
            int r4 = r1.skipBytes(r6)
            if (r4 != r6) goto L_0x0184
            int r2 = r2 + r6
            r7 = -1
            r8 = 1
            goto L_0x003f
        L_0x0184:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r2 = "Invalid JPEG segment"
            r1.<init>(r2)
            throw r1
        L_0x018c:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>(r9)
            throw r1
        L_0x0192:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>(r9)
            throw r1
        L_0x0198:
            java.nio.ByteOrder r2 = r0.h
            r1.a(r2)
            return
        L_0x019e:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid marker:"
            r2.append(r3)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01bb:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01d6:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            r3 = r4 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
            switch-data {-64->0x00c2, -63->0x00c2, -62->0x00c2, -61->0x00c2, }
            switch-data {-59->0x00c2, -58->0x00c2, -57->0x00c2, }
            switch-data {-55->0x00c2, -54->0x00c2, -53->0x00c2, }
            switch-data {-51->0x00c2, -50->0x00c2, -49->0x00c2, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ub.a(com.fossil.ub$b, int, int):void");
    }

    @DexIgnore
    public final void a(b bVar) throws IOException {
        String str;
        String str2;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mediaMetadataRetriever.setDataSource(new a(this, bVar));
            } else if (this.b != null) {
                mediaMetadataRetriever.setDataSource(this.b);
            } else if (this.a != null) {
                mediaMetadataRetriever.setDataSource(this.a);
            } else {
                mediaMetadataRetriever.release();
                return;
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(33);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(34);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(26);
            String extractMetadata4 = mediaMetadataRetriever.extractMetadata(17);
            String str3 = null;
            if ("yes".equals(extractMetadata3)) {
                str3 = mediaMetadataRetriever.extractMetadata(29);
                str2 = mediaMetadataRetriever.extractMetadata(30);
                str = mediaMetadataRetriever.extractMetadata(31);
            } else if ("yes".equals(extractMetadata4)) {
                str3 = mediaMetadataRetriever.extractMetadata(18);
                str2 = mediaMetadataRetriever.extractMetadata(19);
                str = mediaMetadataRetriever.extractMetadata(24);
            } else {
                str2 = null;
                str = null;
            }
            if (str3 != null) {
                this.f[0].put("ImageWidth", c.a(Integer.parseInt(str3), this.h));
            }
            if (str2 != null) {
                this.f[0].put("ImageLength", c.a(Integer.parseInt(str2), this.h));
            }
            if (str != null) {
                int i2 = 1;
                int parseInt = Integer.parseInt(str);
                if (parseInt == 90) {
                    i2 = 6;
                } else if (parseInt == 180) {
                    i2 = 3;
                } else if (parseInt == 270) {
                    i2 = 8;
                }
                this.f[0].put("Orientation", c.a(i2, this.h));
            }
            if (!(extractMetadata == null || extractMetadata2 == null)) {
                int parseInt2 = Integer.parseInt(extractMetadata);
                int parseInt3 = Integer.parseInt(extractMetadata2);
                if (parseInt3 > 6) {
                    bVar.e((long) parseInt2);
                    byte[] bArr = new byte[6];
                    if (bVar.read(bArr) == 6) {
                        int i3 = parseInt2 + 6;
                        int i4 = parseInt3 - 6;
                        if (Arrays.equals(bArr, c0)) {
                            byte[] bArr2 = new byte[i4];
                            if (bVar.read(bArr2) == i4) {
                                this.m = i3;
                                a(bArr2, 0);
                            } else {
                                throw new IOException("Can't read exif");
                            }
                        } else {
                            throw new IOException("Invalid identifier");
                        }
                    } else {
                        throw new IOException("Can't read identifier");
                    }
                } else {
                    throw new IOException("Invalid exif length");
                }
            }
            if (r) {
                Log.d("ExifInterface", "Heif meta: " + str3 + "x" + str2 + ", rotation " + str);
            }
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    @DexIgnore
    public final void b(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("StripOffsets");
        c cVar2 = (c) hashMap.get("StripByteCounts");
        if (cVar != null && cVar2 != null) {
            long[] a2 = a(cVar.d(this.h));
            long[] a3 = a(cVar2.d(this.h));
            if (a2 == null || a2.length == 0) {
                Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
            } else if (a3 == null || a3.length == 0) {
                Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
            } else if (a2.length != a3.length) {
                Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
            } else {
                long j2 = 0;
                for (long j3 : a3) {
                    j2 += j3;
                }
                int i2 = (int) j2;
                byte[] bArr = new byte[i2];
                this.i = true;
                int i3 = 0;
                int i4 = 0;
                for (int i5 = 0; i5 < a2.length; i5++) {
                    int i6 = (int) a2[i5];
                    int i7 = (int) a3[i5];
                    if (i5 < a2.length - 1 && ((long) (i6 + i7)) != a2[i5 + 1]) {
                        this.i = false;
                    }
                    int i8 = i6 - i3;
                    if (i8 < 0) {
                        Log.d("ExifInterface", "Invalid strip offset value");
                    }
                    bVar.e((long) i8);
                    int i9 = i3 + i8;
                    byte[] bArr2 = new byte[i7];
                    bVar.read(bArr2);
                    i3 = i9 + i7;
                    System.arraycopy(bArr2, 0, bArr, i4, i7);
                    i4 += i7;
                }
                if (this.i) {
                    this.j = ((int) a2[0]) + this.m;
                    this.k = i2;
                }
            }
        }
    }

    @DexIgnore
    public final void a(byte[] bArr, int i2) throws IOException {
        b bVar = new b(bArr);
        a(bVar, bArr.length);
        b(bVar, i2);
    }

    @DexIgnore
    public final void a() {
        String a2 = a("DateTimeOriginal");
        if (a2 != null && a("DateTime") == null) {
            this.f[0].put("DateTime", c.a(a2));
        }
        if (a("ImageWidth") == null) {
            this.f[0].put("ImageWidth", c.a(0L, this.h));
        }
        if (a("ImageLength") == null) {
            this.f[0].put("ImageLength", c.a(0L, this.h));
        }
        if (a("Orientation") == null) {
            this.f[0].put("Orientation", c.a(0L, this.h));
        }
        if (a("LightSource") == null) {
            this.f[1].put("LightSource", c.a(0L, this.h));
        }
    }

    @DexIgnore
    public final boolean b(HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("ImageLength");
        c cVar2 = (c) hashMap.get("ImageWidth");
        if (cVar == null || cVar2 == null) {
            return false;
        }
        return cVar.b(this.h) <= 512 && cVar2.b(this.h) <= 512;
    }

    @DexIgnore
    public final void a(b bVar, int i2) throws IOException {
        ByteOrder i3 = i(bVar);
        this.h = i3;
        bVar.a(i3);
        int readUnsignedShort = bVar.readUnsignedShort();
        int i4 = this.d;
        if (i4 == 7 || i4 == 10 || readUnsignedShort == 42) {
            int readInt = bVar.readInt();
            if (readInt < 8 || readInt >= i2) {
                throw new IOException("Invalid first Ifd offset: " + readInt);
            }
            int i5 = readInt - 8;
            if (i5 > 0 && bVar.skipBytes(i5) != i5) {
                throw new IOException("Couldn't jump to first Ifd: " + i5);
            }
            return;
        }
        throw new IOException("Invalid start code: " + Integer.toHexString(readUnsignedShort));
    }

    @DexIgnore
    public final void a(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("JPEGInterchangeFormat");
        c cVar2 = (c) hashMap.get("JPEGInterchangeFormatLength");
        if (cVar != null && cVar2 != null) {
            int b2 = cVar.b(this.h);
            int b3 = cVar2.b(this.h);
            if (this.d == 7) {
                b2 += this.n;
            }
            int min = Math.min(b3, bVar.a() - b2);
            if (b2 > 0 && min > 0) {
                int i2 = this.m + b2;
                this.j = i2;
                this.k = min;
                if (this.a == null && this.c == null && this.b == null) {
                    bVar.e((long) i2);
                    bVar.readFully(new byte[min]);
                }
            }
            if (r) {
                Log.d("ExifInterface", "Setting thumbnail attributes with offset: " + b2 + ", length: " + min);
            }
        }
    }

    @DexIgnore
    public final boolean a(HashMap hashMap) throws IOException {
        c cVar;
        int b2;
        c cVar2 = (c) hashMap.get("BitsPerSample");
        if (cVar2 != null) {
            int[] iArr = (int[]) cVar2.d(this.h);
            if (Arrays.equals(s, iArr)) {
                return true;
            }
            if (this.d == 3 && (cVar = (c) hashMap.get("PhotometricInterpretation")) != null && (((b2 = cVar.b(this.h)) == 1 && Arrays.equals(iArr, t)) || (b2 == 6 && Arrays.equals(iArr, s)))) {
                return true;
            }
        }
        if (!r) {
            return false;
        }
        Log.d("ExifInterface", "Unsupported data type value");
        return false;
    }

    @DexIgnore
    public final void a(int i2, int i3) throws IOException {
        if (!this.f[i2].isEmpty() && !this.f[i3].isEmpty()) {
            c cVar = this.f[i2].get("ImageLength");
            c cVar2 = this.f[i2].get("ImageWidth");
            c cVar3 = this.f[i3].get("ImageLength");
            c cVar4 = this.f[i3].get("ImageWidth");
            if (cVar == null || cVar2 == null) {
                if (r) {
                    Log.d("ExifInterface", "First image does not contain valid size information");
                }
            } else if (cVar3 != null && cVar4 != null) {
                int b2 = cVar.b(this.h);
                int b3 = cVar2.b(this.h);
                int b4 = cVar3.b(this.h);
                int b5 = cVar4.b(this.h);
                if (b2 < b4 && b3 < b5) {
                    HashMap<String, c>[] hashMapArr = this.f;
                    HashMap<String, c> hashMap = hashMapArr[i2];
                    hashMapArr[i2] = hashMapArr[i3];
                    hashMapArr[i3] = hashMap;
                }
            } else if (r) {
                Log.d("ExifInterface", "Second image does not contain valid size information");
            }
        } else if (r) {
            Log.d("ExifInterface", "Cannot perform swap since only one image data exists");
        }
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public static long[] a(Object obj) {
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            long[] jArr = new long[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                jArr[i2] = (long) iArr[i2];
            }
            return jArr;
        } else if (obj instanceof long[]) {
            return (long[]) obj;
        } else {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length < bArr2.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }
}
