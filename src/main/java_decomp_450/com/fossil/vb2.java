package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vb2 extends eg2 implements wb2 {
    @DexIgnore
    public vb2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    @Override // com.fossil.wb2
    public final int A() throws RemoteException {
        Parcel a = a(6, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.wb2
    public final ab2 a(ab2 ab2, String str, int i) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        zza.writeInt(i);
        Parcel a = a(2, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.wb2
    public final int b(ab2 ab2, String str, boolean z) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        fg2.a(zza, z);
        Parcel a = a(3, zza);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.wb2
    public final int a(ab2 ab2, String str, boolean z) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        fg2.a(zza, z);
        Parcel a = a(5, zza);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.wb2
    public final ab2 b(ab2 ab2, String str, int i) throws RemoteException {
        Parcel zza = zza();
        fg2.a(zza, ab2);
        zza.writeString(str);
        zza.writeInt(i);
        Parcel a = a(4, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
