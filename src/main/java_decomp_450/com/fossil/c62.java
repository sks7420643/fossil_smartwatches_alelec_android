package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c62<T> extends y52<T> {
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public ArrayList<Integer> c;

    @DexIgnore
    public c62(DataHolder dataHolder) {
        super(dataHolder);
    }

    @DexIgnore
    public final int a(int i) {
        if (i >= 0 && i < this.c.size()) {
            return this.c.get(i).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public abstract T a(int i, int i2);

    @DexIgnore
    public String b() {
        return null;
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final void e() {
        synchronized (this) {
            if (!this.b) {
                int count = ((y52) this).a.getCount();
                ArrayList<Integer> arrayList = new ArrayList<>();
                this.c = arrayList;
                if (count > 0) {
                    arrayList.add(0);
                    String c2 = c();
                    String c3 = ((y52) this).a.c(c2, 0, ((y52) this).a.a(0));
                    int i = 1;
                    while (i < count) {
                        int a = ((y52) this).a.a(i);
                        String c4 = ((y52) this).a.c(c2, i, a);
                        if (c4 != null) {
                            if (!c4.equals(c3)) {
                                this.c.add(Integer.valueOf(i));
                                c3 = c4;
                            }
                            i++;
                        } else {
                            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 78);
                            sb.append("Missing value for markerColumn: ");
                            sb.append(c2);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(a);
                            throw new NullPointerException(sb.toString());
                        }
                    }
                }
                this.b = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0063, code lost:
        if (((com.fossil.y52) r6).a.c(r4, r7, r3) == null) goto L_0x0067;
     */
    @DexIgnore
    @Override // com.fossil.z52
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T get(int r7) {
        /*
            r6 = this;
            r6.e()
            int r0 = r6.a(r7)
            r1 = 0
            if (r7 < 0) goto L_0x0067
            java.util.ArrayList<java.lang.Integer> r2 = r6.c
            int r2 = r2.size()
            if (r7 != r2) goto L_0x0013
            goto L_0x0067
        L_0x0013:
            java.util.ArrayList<java.lang.Integer> r2 = r6.c
            int r2 = r2.size()
            r3 = 1
            int r2 = r2 - r3
            if (r7 != r2) goto L_0x0030
            com.google.android.gms.common.data.DataHolder r2 = r6.a
            int r2 = r2.getCount()
            java.util.ArrayList<java.lang.Integer> r4 = r6.c
            java.lang.Object r4 = r4.get(r7)
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            goto L_0x004a
        L_0x0030:
            java.util.ArrayList<java.lang.Integer> r2 = r6.c
            int r4 = r7 + 1
            java.lang.Object r2 = r2.get(r4)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            java.util.ArrayList<java.lang.Integer> r4 = r6.c
            java.lang.Object r4 = r4.get(r7)
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
        L_0x004a:
            int r2 = r2 - r4
            if (r2 != r3) goto L_0x0066
            int r7 = r6.a(r7)
            com.google.android.gms.common.data.DataHolder r3 = r6.a
            int r3 = r3.a(r7)
            java.lang.String r4 = r6.b()
            if (r4 == 0) goto L_0x0066
            com.google.android.gms.common.data.DataHolder r5 = r6.a
            java.lang.String r7 = r5.c(r4, r7, r3)
            if (r7 != 0) goto L_0x0066
            goto L_0x0067
        L_0x0066:
            r1 = r2
        L_0x0067:
            java.lang.Object r7 = r6.a(r0, r1)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.c62.get(int):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.z52
    public int getCount() {
        e();
        return this.c.size();
    }
}
