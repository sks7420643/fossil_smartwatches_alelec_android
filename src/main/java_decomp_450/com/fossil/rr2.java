package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr2<T> extends pr2<T> {
    @DexIgnore
    public /* final */ T zza;

    @DexIgnore
    public rr2(T t) {
        this.zza = t;
    }

    @DexIgnore
    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof rr2) {
            return this.zza.equals(((rr2) obj).zza);
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return this.zza.hashCode() + 1502476572;
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.pr2
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.pr2
    public final T zzb() {
        return this.zza;
    }
}
