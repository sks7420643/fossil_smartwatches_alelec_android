package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp4 implements Factory<vp4> {
    @DexIgnore
    public /* final */ Provider<xo4> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public wp4(Provider<xo4> provider, Provider<ro4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static wp4 a(Provider<xo4> provider, Provider<ro4> provider2, Provider<ch5> provider3) {
        return new wp4(provider, provider2, provider3);
    }

    @DexIgnore
    public static vp4 a(xo4 xo4, ro4 ro4, ch5 ch5) {
        return new vp4(xo4, ro4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vp4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
