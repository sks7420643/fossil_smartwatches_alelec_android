package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc2 implements Parcelable.Creator<lc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ lc2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        uc2 uc2 = null;
        Long l = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    j = j72.s(parcel, a);
                    break;
                case 2:
                    j2 = j72.s(parcel, a);
                    break;
                case 3:
                    str = j72.e(parcel, a);
                    break;
                case 4:
                    str2 = j72.e(parcel, a);
                    break;
                case 5:
                    str3 = j72.e(parcel, a);
                    break;
                case 6:
                default:
                    j72.v(parcel, a);
                    break;
                case 7:
                    i = j72.q(parcel, a);
                    break;
                case 8:
                    uc2 = (uc2) j72.a(parcel, a, uc2.CREATOR);
                    break;
                case 9:
                    l = j72.t(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new lc2(j, j2, str, str2, str3, i, uc2, l);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ lc2[] newArray(int i) {
        return new lc2[i];
    }
}
