package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo1 extends zk0 {
    @DexIgnore
    public /* final */ HashMap<d81, Object> C;

    @DexIgnore
    public uo1(ri1 ri1, en0 en0, HashMap<d81, Object> hashMap) {
        super(ri1, en0, wm0.g0, null, false, 24);
        this.C = hashMap;
    }

    @DexIgnore
    public final void b(eu0 eu0) {
        if (eu0.b == is0.SUCCESS) {
            a(eu0);
            return;
        }
        ((zk0) this).v = eu0;
        zk0.a(this, new ce1(((zk0) this).w), zi1.a, xk1.a, (kd7) null, new vm1(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new tk0(((zk0) this).w, ((zk0) this).x, ((zk0) this).z), new gf1(this), new ch1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public static final /* synthetic */ void a(uo1 uo1) {
        ri1 ri1 = ((zk0) uo1).w;
        Long l = (Long) uo1.C.get(d81.CONNECTION_TIME_OUT);
        zk0.a(uo1, new k11(ri1, l != null ? l.longValue() : 60000), y91.a, sb1.a, (kd7) null, new md1(uo1), (gd7) null, 40, (Object) null);
    }
}
