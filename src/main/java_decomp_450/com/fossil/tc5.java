package com.fossil;

import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewTreeObserver.OnWindowFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditText a;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager b;

        @DexIgnore
        public a(EditText editText, InputMethodManager inputMethodManager) {
            this.a = editText;
            this.b = inputMethodManager;
        }

        @DexIgnore
        public void onWindowFocusChanged(boolean z) {
            if (z) {
                tc5.c(this.a, this.b);
                this.a.getViewTreeObserver().removeOnWindowFocusChangeListener(this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ EditText a;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager b;

        @DexIgnore
        public b(EditText editText, InputMethodManager inputMethodManager) {
            this.a = editText;
            this.b = inputMethodManager;
        }

        @DexIgnore
        public final void run() {
            this.b.showSoftInput(this.a, 1);
        }
    }

    @DexIgnore
    public static final void b(EditText editText, InputMethodManager inputMethodManager) {
        ee7.b(editText, "$this$focusAndShowKeyboard");
        ee7.b(inputMethodManager, "imm");
        editText.requestFocus();
        if (editText.hasWindowFocus()) {
            c(editText, inputMethodManager);
        } else {
            editText.getViewTreeObserver().addOnWindowFocusChangeListener(new a(editText, inputMethodManager));
        }
    }

    @DexIgnore
    public static final void c(EditText editText, InputMethodManager inputMethodManager) {
        if (editText.isFocused()) {
            editText.post(new b(editText, inputMethodManager));
        }
    }
}
