package com.fossil;

import android.database.Cursor;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im4 implements hm4 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<nm4> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<nm4> {
        @DexIgnore
        public a(im4 im4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, nm4 nm4) {
            if (nm4.a() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, nm4.a());
            }
            if (nm4.b() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, nm4.b());
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `flag` (`id`,`variantKey`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(im4 im4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM flag";
        }
    }

    @DexIgnore
    public im4(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
        new b(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.hm4
    public Long[] a(List<nm4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.hm4
    public nm4 a(String str) {
        fi b2 = fi.b("SELECT * FROM flag WHERE id = ? LIMIT 1", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        nm4 nm4 = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "variantKey");
            if (a2.moveToFirst()) {
                nm4 = new nm4(a2.getString(b3), a2.getString(b4));
            }
            return nm4;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
