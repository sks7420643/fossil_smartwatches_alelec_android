package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v5 {
    @DexIgnore
    public static /* final */ int bottom; // = 2131361915;
    @DexIgnore
    public static /* final */ int end; // = 2131362215;
    @DexIgnore
    public static /* final */ int gone; // = 2131362539;
    @DexIgnore
    public static /* final */ int invisible; // = 2131362618;
    @DexIgnore
    public static /* final */ int left; // = 2131362749;
    @DexIgnore
    public static /* final */ int packed; // = 2131362875;
    @DexIgnore
    public static /* final */ int parent; // = 2131362879;
    @DexIgnore
    public static /* final */ int percent; // = 2131362890;
    @DexIgnore
    public static /* final */ int right; // = 2131362948;
    @DexIgnore
    public static /* final */ int spread; // = 2131363082;
    @DexIgnore
    public static /* final */ int spread_inside; // = 2131363083;
    @DexIgnore
    public static /* final */ int start; // = 2131363091;
    @DexIgnore
    public static /* final */ int top; // = 2131363165;
    @DexIgnore
    public static /* final */ int wrap; // = 2131363472;
}
