package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q5 extends r5 {
    @DexIgnore
    public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    public void a(int i) {
        if (((r5) this).b == 0 || this.c != ((float) i)) {
            this.c = (float) i;
            if (((r5) this).b == 1) {
                b();
            }
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.r5
    public void d() {
        super.d();
        this.c = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void f() {
        ((r5) this).b = 2;
    }
}
