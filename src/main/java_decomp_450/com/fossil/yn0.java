package com.fossil;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn0 {
    @DexIgnore
    public static /* final */ fo1 a; // = fo1.CBC_PKCS5_PADDING;
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[16];
    @DexIgnore
    public static byte[] c;
    @DexIgnore
    public static byte[] d;
    @DexIgnore
    public static /* final */ Object e; // = new Object();
    @DexIgnore
    @TargetApi(23)
    public static byte[] f;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static /* final */ yn0 h;

    /*
    static {
        SharedPreferences a2;
        yn0 yn0 = new yn0();
        h = yn0;
        if (c == null && (a2 = yz0.a(j91.f)) != null) {
            String string = a2.getString("text_encryption_secret_key", null);
            if (string == null) {
                string = a2.getString("text_encryption_key", null);
            }
            if (string != null) {
                c = Base64.decode(string, 0);
            }
        }
        yn0.a();
        yn0.b();
    }
    */

    @DexIgnore
    public final void a() {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new fk0(null), 3, null);
    }

    @DexIgnore
    public final void b() {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new cm0(null), 3, null);
    }

    @DexIgnore
    public final String a(String str) {
        for (byte[] bArr : t97.e(new byte[][]{f, d, c})) {
            try {
                fq1 fq1 = fq1.a;
                fo1 fo1 = a;
                byte[] bArr2 = b;
                byte[] decode = Base64.decode(str, 2);
                ee7.a((Object) decode, "Base64.decode(encryptedText, Base64.NO_WRAP)");
                return new String(fq1.a(fo1, bArr, bArr2, decode), b21.x.c());
            } catch (Exception unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String b(String str) {
        byte[] bArr;
        if (Build.VERSION.SDK_INT >= 23) {
            b();
            bArr = f;
        } else {
            a();
            bArr = d;
        }
        if (bArr != null) {
            try {
                fo1 fo1 = a;
                byte[] bArr2 = b;
                byte[] bytes = str.getBytes(b21.x.c());
                ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                gm1 gm1 = gm1.ENCRYPT;
                SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
                Cipher instance = Cipher.getInstance(fo1.a);
                instance.init(gm1.a, secretKeySpec, new IvParameterSpec(bArr2));
                byte[] doFinal = instance.doFinal(bytes);
                ee7.a((Object) doFinal, "cipher.doFinal(data)");
                return Base64.encodeToString(doFinal, 2);
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
