package com.fossil;

import com.fossil.o14;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v14 {
    @DexIgnore
    public o14.b a;
    @DexIgnore
    public eb3 b;
    @DexIgnore
    public u14 c;

    @DexIgnore
    public v14(eb3 eb3, o14.b bVar) {
        this.a = bVar;
        this.b = eb3;
        u14 u14 = new u14(this);
        this.c = u14;
        this.b.a(u14);
    }
}
