package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl1 extends sj1 {
    @DexIgnore
    public /* final */ w90 A;

    @DexIgnore
    public nl1(w90 w90, ri1 ri1) {
        super(qa1.b0, ri1);
        this.A = w90;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.c4, this.A.a());
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        qk1 qk1 = qk1.ASYNC;
        w90 w90 = this.A;
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(8).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(cy0.b.a);
        order.put(ru0.APP_NOTIFICATION_EVENT.a);
        order.putInt(w90.getNotificationUid());
        order.put(w90.getAction().a());
        order.put(w90.getActionStatus().a());
        byte[] array = order.array();
        ee7.a((Object) array, "appNotificationEventData.array()");
        return new ze1(qk1, array, ((v81) this).y.w);
    }
}
