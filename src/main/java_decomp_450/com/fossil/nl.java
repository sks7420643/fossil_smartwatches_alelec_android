package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseIntArray;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nl extends ml {
    @DexIgnore
    public /* final */ SparseIntArray d;
    @DexIgnore
    public /* final */ Parcel e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;

    @DexIgnore
    public nl(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new n4(), new n4(), new n4());
    }

    @DexIgnore
    @Override // com.fossil.ml
    public boolean a(int i2) {
        while (this.j < this.g) {
            int i3 = this.k;
            if (i3 == i2) {
                return true;
            }
            if (String.valueOf(i3).compareTo(String.valueOf(i2)) > 0) {
                return false;
            }
            this.e.setDataPosition(this.j);
            int readInt = this.e.readInt();
            this.k = this.e.readInt();
            this.j += readInt;
        }
        if (this.k == i2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void b(int i2) {
        a();
        this.i = i2;
        this.d.put(i2, this.e.dataPosition());
        c(0);
        c(i2);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void c(int i2) {
        this.e.writeInt(i2);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public boolean d() {
        return this.e.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.ml
    public byte[] e() {
        int readInt = this.e.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.e.readByteArray(bArr);
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.ml
    public CharSequence f() {
        return (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this.e);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public int g() {
        return this.e.readInt();
    }

    @DexIgnore
    @Override // com.fossil.ml
    public <T extends Parcelable> T h() {
        return (T) this.e.readParcelable(nl.class.getClassLoader());
    }

    @DexIgnore
    @Override // com.fossil.ml
    public String i() {
        return this.e.readString();
    }

    @DexIgnore
    public nl(Parcel parcel, int i2, int i3, String str, n4<String, Method> n4Var, n4<String, Method> n4Var2, n4<String, Class> n4Var3) {
        super(n4Var, n4Var2, n4Var3);
        this.d = new SparseIntArray();
        this.i = -1;
        this.j = 0;
        this.k = -1;
        this.e = parcel;
        this.f = i2;
        this.g = i3;
        this.j = i2;
        this.h = str;
    }

    @DexIgnore
    @Override // com.fossil.ml
    public ml b() {
        Parcel parcel = this.e;
        int dataPosition = parcel.dataPosition();
        int i2 = this.j;
        if (i2 == this.f) {
            i2 = this.g;
        }
        return new nl(parcel, dataPosition, i2, this.h + "  ", ((ml) this).a, ((ml) this).b, ((ml) this).c);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void b(String str) {
        this.e.writeString(str);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void a() {
        int i2 = this.i;
        if (i2 >= 0) {
            int i3 = this.d.get(i2);
            int dataPosition = this.e.dataPosition();
            this.e.setDataPosition(i3);
            this.e.writeInt(dataPosition - i3);
            this.e.setDataPosition(dataPosition);
        }
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.e.writeInt(bArr.length);
            this.e.writeByteArray(bArr);
            return;
        }
        this.e.writeInt(-1);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void a(Parcelable parcelable) {
        this.e.writeParcelable(parcelable, 0);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void a(boolean z) {
        this.e.writeInt(z ? 1 : 0);
    }

    @DexIgnore
    @Override // com.fossil.ml
    public void a(CharSequence charSequence) {
        TextUtils.writeToParcel(charSequence, this.e, 0);
    }
}
