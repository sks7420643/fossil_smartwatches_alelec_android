package com.fossil;

import com.fossil.ik7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ok7<J extends ik7> extends pi7 implements rj7, dk7 {
    @DexIgnore
    public /* final */ J d;

    @DexIgnore
    public ok7(J j) {
        this.d = j;
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public uk7 a() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.rj7
    public void dispose() {
        J j = this.d;
        if (j != null) {
            ((pk7) j).b((ok7<?>) this);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public boolean isActive() {
        return true;
    }
}
