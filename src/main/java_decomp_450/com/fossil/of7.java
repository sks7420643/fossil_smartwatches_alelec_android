package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class of7 extends mf7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        new of7(1, 0);
    }
    */

    @DexIgnore
    public of7(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean a(long j) {
        return getFirst() <= j && j <= getLast();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof of7) {
            if (!isEmpty() || !((of7) obj).isEmpty()) {
                of7 of7 = (of7) obj;
                if (!(getFirst() == of7.getFirst() && getLast() == of7.getLast())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (getFirst() ^ (getFirst() >>> 32))) + (getLast() ^ (getLast() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return getFirst() > getLast();
    }

    @DexIgnore
    public String toString() {
        return getFirst() + ".." + getLast();
    }
}
