package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le5 {
    @DexIgnore
    public static /* final */ le5 a; // = new le5();

    @DexIgnore
    public final void a(View view, Context context) {
        ee7.b(view, "view");
        ee7.b(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @DexIgnore
    public final void b(View view, Context context) {
        ee7.b(view, "view");
        ee7.b(context, "context");
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(view, 2);
        }
    }
}
