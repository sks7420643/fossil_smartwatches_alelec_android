package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga0 extends na0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ga0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ga0 createFromParcel(Parcel parcel) {
            return new ga0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ga0[] newArray(int i) {
            return new ga0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public ga0 m21createFromParcel(Parcel parcel) {
            return new ga0(parcel, (zd7) null);
        }
    }

    @DexIgnore
    public ga0(ah0 ah0) {
        super(pa0.COMMUTE, null, ah0, 2);
    }

    @DexIgnore
    @Override // com.fossil.na0
    public JSONObject b() {
        JSONObject put = super.b().put("commuteApp._.config.destinations", yz0.a(getDataConfig().getDestinations()));
        ee7.a((Object) put, "super.getDataConfigJSONO\u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public final ah0 getDataConfig() {
        bh0 bh0 = ((na0) this).c;
        if (bh0 != null) {
            return (ah0) bh0;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public ga0(ah0 ah0, zg0 zg0) {
        super(pa0.COMMUTE, zg0, ah0);
    }

    @DexIgnore
    public /* synthetic */ ga0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
