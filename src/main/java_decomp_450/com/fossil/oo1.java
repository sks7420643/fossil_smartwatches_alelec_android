package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class oo1 extends Enum<oo1> {
    @DexIgnore
    public static /* final */ oo1 b;
    @DexIgnore
    public static /* final */ /* synthetic */ oo1[] c;
    @DexIgnore
    public /* final */ short a;

    /*
    static {
        oo1 oo1 = new oo1("ACTIVITY_FILE", 0, pb1.ACTIVITY_FILE.a);
        b = oo1;
        c = new oo1[]{oo1, new oo1("HARDWARE_LOG", 1, pb1.HARDWARE_LOG.a)};
    }
    */

    @DexIgnore
    public oo1(String str, int i, short s) {
        this.a = s;
    }

    @DexIgnore
    public static oo1 valueOf(String str) {
        return (oo1) Enum.valueOf(oo1.class, str);
    }

    @DexIgnore
    public static oo1[] values() {
        return (oo1[]) c.clone();
    }
}
