package com.fossil;

import com.fossil.cz3;
import com.fossil.yy3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vw3<K, V> implements zy3<K, V> {
    @DexIgnore
    public transient Collection<Map.Entry<K, V>> a;
    @DexIgnore
    public transient Set<K> b;
    @DexIgnore
    public transient dz3<K> c;
    @DexIgnore
    public transient Collection<V> d;
    @DexIgnore
    public transient Map<K, Collection<V>> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends cz3.c<K, V> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.cz3.c
        public zy3<K, V> a() {
            return vw3.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return vw3.this.entryIterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends vw3<K, V>.b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public c(vw3 vw3) {
            super();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return yz3.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            return yz3.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AbstractCollection<V> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void clear() {
            vw3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return vw3.this.containsValue(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return vw3.this.valueIterator();
        }

        @DexIgnore
        public int size() {
            return vw3.this.size();
        }
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public Map<K, Collection<V>> asMap() {
        Map<K, Collection<V>> map = this.e;
        if (map != null) {
            return map;
        }
        Map<K, Collection<V>> createAsMap = createAsMap();
        this.e = createAsMap;
        return createAsMap;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public boolean containsEntry(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.contains(obj2);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        for (Collection<V> collection : asMap().values()) {
            if (collection.contains(obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract Map<K, Collection<V>> createAsMap();

    @DexIgnore
    public Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof xz3) {
            return new c();
        }
        return new b();
    }

    @DexIgnore
    public Set<K> createKeySet() {
        return new yy3.e(asMap());
    }

    @DexIgnore
    public dz3<K> createKeys() {
        return new cz3.d(this);
    }

    @DexIgnore
    public Collection<V> createValues() {
        return new d();
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public Collection<Map.Entry<K, V>> entries() {
        Collection<Map.Entry<K, V>> collection = this.a;
        if (collection != null) {
            return collection;
        }
        Collection<Map.Entry<K, V>> createEntries = createEntries();
        this.a = createEntries;
        return createEntries;
    }

    @DexIgnore
    public abstract Iterator<Map.Entry<K, V>> entryIterator();

    @DexIgnore
    public boolean equals(Object obj) {
        return cz3.a(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return asMap().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public Set<K> keySet() {
        Set<K> set = this.b;
        if (set != null) {
            return set;
        }
        Set<K> createKeySet = createKeySet();
        this.b = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    public dz3<K> keys() {
        dz3<K> dz3 = this.c;
        if (dz3 != null) {
            return dz3;
        }
        dz3<K> createKeys = createKeys();
        this.c = createKeys;
        return createKeys;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    @CanIgnoreReturnValue
    public abstract boolean put(K k, V v);

    @DexIgnore
    @Override // com.fossil.zy3
    @CanIgnoreReturnValue
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        jw3.a(iterable);
        if (iterable instanceof Collection) {
            Collection<? extends V> collection = (Collection) iterable;
            if (collection.isEmpty() || !get(k).addAll(collection)) {
                return false;
            }
            return true;
        }
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext() || !qy3.a(get(k), it)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        Collection<V> collection = asMap().get(obj);
        return collection != null && collection.remove(obj2);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public abstract Collection<V> replaceValues(K k, Iterable<? extends V> iterable);

    @DexIgnore
    public String toString() {
        return asMap().toString();
    }

    @DexIgnore
    public abstract Iterator<V> valueIterator();

    @DexIgnore
    public Collection<V> values() {
        Collection<V> collection = this.d;
        if (collection != null) {
            return collection;
        }
        Collection<V> createValues = createValues();
        this.d = createValues;
        return createValues;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.vw3<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @CanIgnoreReturnValue
    public boolean putAll(zy3<? extends K, ? extends V> zy3) {
        boolean z = false;
        for (Map.Entry<? extends K, ? extends V> entry : zy3.entries()) {
            z |= put(entry.getKey(), entry.getValue());
        }
        return z;
    }
}
