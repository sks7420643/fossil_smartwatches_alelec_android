package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hb implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hb> CREATOR; // = new b();
    @DexIgnore
    public static /* final */ hb b; // = new a();
    @DexIgnore
    public /* final */ Parcelable a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends hb {
        @DexIgnore
        public a() {
            super((a) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Parcelable.ClassLoaderCreator<hb> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public hb[] newArray(int i) {
            return new hb[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.ClassLoaderCreator
        public hb createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return hb.b;
            }
            throw new IllegalStateException("superState must be null");
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public hb createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, (ClassLoader) null);
        }
    }

    @DexIgnore
    public /* synthetic */ hb(a aVar) {
        this();
    }

    @DexIgnore
    public final Parcelable a() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
    }

    @DexIgnore
    public hb() {
        this.a = null;
    }

    @DexIgnore
    public hb(Parcelable parcelable) {
        if (parcelable != null) {
            this.a = parcelable == b ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public hb(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.a = readParcelable == null ? b : readParcelable;
    }
}
