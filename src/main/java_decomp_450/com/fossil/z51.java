package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum z51 {
    SUCCESS(0),
    GATT_NULL(1),
    CHARACTERISTIC_NOT_FOUND(2),
    DESCRIPTOR_NOT_FOUND(3),
    START_FAIL(4),
    GATT_ERROR(5),
    BLUETOOTH_OFF(6),
    HID_PROXY_NOT_CONNECTED(256),
    HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
    HID_INPUT_DEVICE_DISABLED(258),
    HID_UNKNOWN_ERROR(511);
    
    @DexIgnore
    public static /* final */ c41 m; // = new c41(null);

    @DexIgnore
    public z51(int i) {
    }
}
