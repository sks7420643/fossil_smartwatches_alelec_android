package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.MetaData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Data> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<MetaData> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<MetaData> {
    }

    @DexIgnore
    public final String a(Data data) {
        ee7.b(data, "data");
        String a2 = new Gson().a(data, new a().getType());
        ee7.a((Object) a2, "Gson().toJson(data, type)");
        return a2;
    }

    @DexIgnore
    public final MetaData b(String str) {
        ee7.b(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (MetaData) new Gson().a(str, new c().getType());
    }

    @DexIgnore
    public final Data a(String str) {
        ee7.b(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Data) new Gson().a(str, new b().getType());
    }

    @DexIgnore
    public final String a(MetaData metaData) {
        ee7.b(metaData, "metaData");
        String a2 = new Gson().a(metaData, new d().getType());
        ee7.a((Object) a2, "Gson().toJson(metaData, type)");
        return a2;
    }
}
