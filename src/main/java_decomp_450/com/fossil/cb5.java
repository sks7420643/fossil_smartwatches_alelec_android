package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum cb5 {
    WALKING("walking"),
    RUNNING("running"),
    BIKING("biking");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public cb5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static cb5 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            cb5[] values = values();
            for (cb5 cb5 : values) {
                if (str.equalsIgnoreCase(cb5.value)) {
                    return cb5;
                }
            }
        }
        return WALKING;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
