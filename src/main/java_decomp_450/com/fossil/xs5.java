package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl4;
import com.fossil.mt5;
import com.fossil.nw5;
import com.fossil.ql4;
import com.fossil.qq5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs5 extends rs5 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.g0.c().d();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> f; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public List<at5> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ ss5 l;
    @DexIgnore
    public /* final */ rl4 m;
    @DexIgnore
    public /* final */ pd5 n;
    @DexIgnore
    public /* final */ nw5 o;
    @DexIgnore
    public /* final */ vu5 p;
    @DexIgnore
    public /* final */ mt5 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ qq5 s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ ch5 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xs5.w;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<qq5.d, qq5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ xs5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(xs5 xs5, Alarm alarm) {
            this.a = xs5;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qq5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xs5.x.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.l.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(qq5.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.l.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = xs5.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.l.z();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.b());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            ss5 o = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                o.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.d<mt5.c, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ xs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(xs5 xs5) {
            this.a = xs5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mt5.c cVar) {
            FLogger.INSTANCE.getLocal().d(xs5.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886497);
            ss5 o = this.a.l;
            ee7.a((Object) a2, "notificationAppOverView");
            o.l(a2);
            this.a.l();
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            FLogger.INSTANCE.getLocal().d(xs5.x.a(), ".Inside mSaveAppsNotification onError");
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886942);
            ss5 o = this.a.l;
            ee7.a((Object) a2, "notificationAppOverView");
            o.l(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", f = "HomeAlertsPresenter.kt", l = {435, 437}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ xs5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {356, 362, 371, 379}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xs5$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.xs5$d$a$a  reason: collision with other inner class name */
            public static final class C0248a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $notificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xs5$d$a$a$a")
                /* renamed from: com.fossil.xs5$d$a$a$a  reason: collision with other inner class name */
                public static final class RunnableC0249a implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0248a a;

                    @DexIgnore
                    public RunnableC0249a(C0248a aVar) {
                        this.a = aVar;
                    }

                    @DexIgnore
                    public final void run() {
                        this.a.this$0.this$0.this$0.r.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0248a(a aVar, List list, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$notificationSettings = list;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0248a aVar = new C0248a(this.this$0, this.$notificationSettings, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0248a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.this$0.r.runInTransaction(new RunnableC0249a(this));
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ se7 $contactMessageAppFilters;
                @DexIgnore
                public /* final */ /* synthetic */ List $listNotificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, List list, se7 se7, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$listNotificationSettings = list;
                    this.$contactMessageAppFilters = se7;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                            int component2 = notificationSettingsModel.component2();
                            if (notificationSettingsModel.component3()) {
                                String a = this.this$0.this$0.this$0.a(component2);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = xs5.x.a();
                                local.d(a2, "CALL settingsTypeName=" + a);
                                if (component2 == 0) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = xs5.x.a();
                                    local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size = this.this$0.this$0.this$0.k.size();
                                    for (int i = 0; i < size; i++) {
                                        ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.this$0.k.get(i);
                                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                        String a4 = xs5.x.a();
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("mListAppNotificationFilter add PHONE item - ");
                                        sb.append(i);
                                        sb.append(" name = ");
                                        Contact contact = contactGroup.getContacts().get(0);
                                        ee7.a((Object) contact, "item.contacts[0]");
                                        sb.append(contact.getDisplayName());
                                        local3.d(a4, sb.toString());
                                        DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                        FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                                        List<Contact> contacts = contactGroup.getContacts();
                                        ee7.a((Object) contacts, "item.contacts");
                                        if (!contacts.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                            Contact contact2 = contactGroup.getContacts().get(0);
                                            ee7.a((Object) contact2, "item.contacts[0]");
                                            appNotificationFilter.setSender(contact2.getDisplayName());
                                            this.$contactMessageAppFilters.element.add(appNotificationFilter);
                                        }
                                    }
                                }
                            } else {
                                String a5 = this.this$0.this$0.this$0.a(component2);
                                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                String a6 = xs5.x.a();
                                local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                                if (component2 == 0) {
                                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                    String a7 = xs5.x.a();
                                    local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size2 = this.this$0.this$0.this$0.k.size();
                                    for (int i2 = 0; i2 < size2; i2++) {
                                        ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.this$0.k.get(i2);
                                        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                        String a8 = xs5.x.a();
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                        sb2.append(i2);
                                        sb2.append(" name = ");
                                        Contact contact3 = contactGroup2.getContacts().get(0);
                                        ee7.a((Object) contact3, "item.contacts[0]");
                                        sb2.append(contact3.getDisplayName());
                                        local6.d(a8, sb2.toString());
                                        DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                        FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                                        List<Contact> contacts2 = contactGroup2.getContacts();
                                        ee7.a((Object) contacts2, "item.contacts");
                                        if (!contacts2.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                                            Contact contact4 = contactGroup2.getContacts().get(0);
                                            ee7.a((Object) contact4, "item.contacts[0]");
                                            appNotificationFilter2.setSender(contact4.getDisplayName());
                                            this.$contactMessageAppFilters.element.add(appNotificationFilter2);
                                        }
                                    }
                                }
                            }
                        }
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class c extends zb7 implements kd7<yi7, fb7<? super List<? extends NotificationSettingsModel>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<? extends NotificationSettingsModel>> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        return this.this$0.this$0.this$0.r.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends AppNotificationFilter>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00fc  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x013d  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                    r14 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r14.label
                    r2 = 4
                    r3 = 3
                    r4 = 2
                    r5 = 1
                    r6 = 0
                    if (r1 == 0) goto L_0x005f
                    if (r1 == r5) goto L_0x0053
                    if (r1 == r4) goto L_0x003f
                    if (r1 == r3) goto L_0x001e
                    if (r1 != r2) goto L_0x0016
                    goto L_0x002a
                L_0x0016:
                    java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r15.<init>(r0)
                    throw r15
                L_0x001e:
                    java.lang.Object r0 = r14.L$6
                    com.portfolio.platform.data.model.NotificationSettingsModel r0 = (com.portfolio.platform.data.model.NotificationSettingsModel) r0
                    java.lang.Object r0 = r14.L$5
                    com.portfolio.platform.data.model.NotificationSettingsModel r0 = (com.portfolio.platform.data.model.NotificationSettingsModel) r0
                    java.lang.Object r0 = r14.L$4
                    java.util.List r0 = (java.util.List) r0
                L_0x002a:
                    java.lang.Object r0 = r14.L$3
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r14.L$2
                    com.fossil.fl4$c r0 = (com.fossil.fl4.c) r0
                    java.lang.Object r0 = r14.L$1
                    com.fossil.se7 r0 = (com.fossil.se7) r0
                    java.lang.Object r1 = r14.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r15)
                    goto L_0x013b
                L_0x003f:
                    java.lang.Object r1 = r14.L$2
                    com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
                    java.lang.Object r4 = r14.L$1
                    com.fossil.se7 r4 = (com.fossil.se7) r4
                    java.lang.Object r7 = r14.L$0
                    com.fossil.yi7 r7 = (com.fossil.yi7) r7
                    com.fossil.t87.a(r15)
                    r13 = r4
                    r4 = r1
                    r1 = r13
                    goto L_0x00f4
                L_0x0053:
                    java.lang.Object r1 = r14.L$1
                    com.fossil.se7 r1 = (com.fossil.se7) r1
                    java.lang.Object r7 = r14.L$0
                    com.fossil.yi7 r7 = (com.fossil.yi7) r7
                    com.fossil.t87.a(r15)
                    goto L_0x0083
                L_0x005f:
                    com.fossil.t87.a(r15)
                    com.fossil.yi7 r15 = r14.p$
                    com.fossil.se7 r1 = new com.fossil.se7
                    r1.<init>()
                    r1.element = r6
                    com.fossil.xs5$d r7 = r14.this$0
                    com.fossil.xs5 r7 = r7.this$0
                    com.fossil.vu5 r7 = r7.p
                    r14.L$0 = r15
                    r14.L$1 = r1
                    r14.label = r5
                    java.lang.Object r7 = com.fossil.gl4.a(r7, r6, r14)
                    if (r7 != r0) goto L_0x0080
                    return r0
                L_0x0080:
                    r13 = r7
                    r7 = r15
                    r15 = r13
                L_0x0083:
                    com.fossil.fl4$c r15 = (com.fossil.fl4.c) r15
                    boolean r8 = r15 instanceof com.fossil.vu5.d
                    if (r8 == 0) goto L_0x017f
                    com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                    com.fossil.xs5$a r9 = com.fossil.xs5.x
                    java.lang.String r9 = r9.a()
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder
                    r10.<init>()
                    java.lang.String r11 = "GetAllContactGroup onSuccess, size = "
                    r10.append(r11)
                    r11 = r15
                    com.fossil.vu5$d r11 = (com.fossil.vu5.d) r11
                    java.util.List r12 = r11.a()
                    int r12 = r12.size()
                    r10.append(r12)
                    java.lang.String r10 = r10.toString()
                    r8.d(r9, r10)
                    java.util.ArrayList r8 = new java.util.ArrayList
                    r8.<init>()
                    r1.element = r8
                    com.fossil.xs5$d r8 = r14.this$0
                    com.fossil.xs5 r8 = r8.this$0
                    java.util.List r8 = r8.k
                    r8.clear()
                    com.fossil.xs5$d r8 = r14.this$0
                    com.fossil.xs5 r8 = r8.this$0
                    java.util.List r9 = r11.a()
                    java.util.List r9 = com.fossil.ea7.d(r9)
                    r8.k = r9
                    com.fossil.xs5$d r8 = r14.this$0
                    com.fossil.xs5 r8 = r8.this$0
                    com.fossil.ti7 r8 = r8.c()
                    com.fossil.xs5$d$a$c r9 = new com.fossil.xs5$d$a$c
                    r9.<init>(r14, r6)
                    r14.L$0 = r7
                    r14.L$1 = r1
                    r14.L$2 = r15
                    r14.label = r4
                    java.lang.Object r4 = com.fossil.vh7.a(r8, r9, r14)
                    if (r4 != r0) goto L_0x00f1
                    return r0
                L_0x00f1:
                    r13 = r4
                    r4 = r15
                    r15 = r13
                L_0x00f4:
                    java.util.List r15 = (java.util.List) r15
                    boolean r8 = r15.isEmpty()
                    if (r8 == 0) goto L_0x013d
                    java.util.ArrayList r2 = new java.util.ArrayList
                    r2.<init>()
                    com.portfolio.platform.data.model.NotificationSettingsModel r8 = new com.portfolio.platform.data.model.NotificationSettingsModel
                    r9 = 0
                    java.lang.String r10 = "AllowCallsFrom"
                    r8.<init>(r10, r9, r5)
                    com.portfolio.platform.data.model.NotificationSettingsModel r5 = new com.portfolio.platform.data.model.NotificationSettingsModel
                    java.lang.String r10 = "AllowMessagesFrom"
                    r5.<init>(r10, r9, r9)
                    r2.add(r8)
                    r2.add(r5)
                    com.fossil.xs5$d r9 = r14.this$0
                    com.fossil.xs5 r9 = r9.this$0
                    com.fossil.ti7 r9 = r9.c()
                    com.fossil.xs5$d$a$a r10 = new com.fossil.xs5$d$a$a
                    r10.<init>(r14, r2, r6)
                    r14.L$0 = r7
                    r14.L$1 = r1
                    r14.L$2 = r4
                    r14.L$3 = r15
                    r14.L$4 = r2
                    r14.L$5 = r8
                    r14.L$6 = r5
                    r14.label = r3
                    java.lang.Object r15 = com.fossil.vh7.a(r9, r10, r14)
                    if (r15 != r0) goto L_0x013a
                    return r0
                L_0x013a:
                    r0 = r1
                L_0x013b:
                    r1 = r0
                    goto L_0x0194
                L_0x013d:
                    com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                    com.fossil.xs5$a r5 = com.fossil.xs5.x
                    java.lang.String r5 = r5.a()
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder
                    r8.<init>()
                    java.lang.String r9 = "listNotificationSettings.size = "
                    r8.append(r9)
                    int r9 = r15.size()
                    r8.append(r9)
                    java.lang.String r8 = r8.toString()
                    r3.d(r5, r8)
                    com.fossil.xs5$d r3 = r14.this$0
                    com.fossil.xs5 r3 = r3.this$0
                    com.fossil.ti7 r3 = r3.b()
                    com.fossil.xs5$d$a$b r5 = new com.fossil.xs5$d$a$b
                    r5.<init>(r14, r15, r1, r6)
                    r14.L$0 = r7
                    r14.L$1 = r1
                    r14.L$2 = r4
                    r14.L$3 = r15
                    r14.label = r2
                    java.lang.Object r15 = com.fossil.vh7.a(r3, r5, r14)
                    if (r15 != r0) goto L_0x013a
                    return r0
                L_0x017f:
                    boolean r15 = r15 instanceof com.fossil.vu5.b
                    if (r15 == 0) goto L_0x0194
                    com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                    com.fossil.xs5$a r0 = com.fossil.xs5.x
                    java.lang.String r0 = r0.a()
                    java.lang.String r2 = "GetAllContactGroup onError"
                    r15.d(r0, r2)
                L_0x0194:
                    T r15 = r1.element
                    java.util.List r15 = (java.util.List) r15
                    return r15
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.xs5.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends AppNotificationFilter>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return mx6.a(this.this$0.this$0.j(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xs5 xs5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = xs5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0107  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r0 = r16
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 2
                r4 = 1
                if (r2 == 0) goto L_0x0053
                if (r2 == r4) goto L_0x0032
                if (r2 != r3) goto L_0x002a
                java.lang.Object r1 = r0.L$3
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                java.lang.Object r1 = r0.L$2
                com.fossil.hj7 r1 = (com.fossil.hj7) r1
                long r1 = r0.J$0
                java.lang.Object r3 = r0.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r0.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r17)
                r5 = r1
                r2 = r17
                goto L_0x0103
            L_0x002a:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0032:
                java.lang.Object r2 = r0.L$4
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r4 = r0.L$3
                com.fossil.hj7 r4 = (com.fossil.hj7) r4
                java.lang.Object r5 = r0.L$2
                com.fossil.hj7 r5 = (com.fossil.hj7) r5
                long r6 = r0.J$0
                java.lang.Object r8 = r0.L$1
                java.util.List r8 = (java.util.List) r8
                java.lang.Object r9 = r0.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r17)
                r10 = r9
                r9 = r5
                r5 = r6
                r7 = r4
                r4 = r17
                goto L_0x00ea
            L_0x0053:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r9 = r0.p$
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                long r5 = java.lang.System.currentTimeMillis()
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.xs5$a r8 = com.fossil.xs5.x
                java.lang.String r8 = r8.a()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "filter notification, time start="
                r10.append(r11)
                r10.append(r5)
                java.lang.String r10 = r10.toString()
                r7.d(r8, r10)
                r2.clear()
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.xs5$a r8 = com.fossil.xs5.x
                java.lang.String r8 = r8.a()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "mListAppWrapper.size = "
                r10.append(r11)
                com.fossil.xs5 r11 = r0.this$0
                java.util.List r11 = r11.j()
                int r11 = r11.size()
                r10.append(r11)
                java.lang.String r10 = r10.toString()
                r7.d(r8, r10)
                com.fossil.xs5 r7 = r0.this$0
                com.fossil.ti7 r11 = r7.b()
                r12 = 0
                com.fossil.xs5$d$b r13 = new com.fossil.xs5$d$b
                r7 = 0
                r13.<init>(r0, r7)
                r14 = 2
                r15 = 0
                r10 = r9
                com.fossil.hj7 r8 = com.fossil.xh7.a(r10, r11, r12, r13, r14, r15)
                com.fossil.xs5 r10 = r0.this$0
                com.fossil.ti7 r11 = r10.b()
                com.fossil.xs5$d$a r13 = new com.fossil.xs5$d$a
                r13.<init>(r0, r7)
                r10 = r9
                com.fossil.hj7 r7 = com.fossil.xh7.a(r10, r11, r12, r13, r14, r15)
                r0.L$0 = r9
                r0.L$1 = r2
                r0.J$0 = r5
                r0.L$2 = r8
                r0.L$3 = r7
                r0.L$4 = r2
                r0.label = r4
                java.lang.Object r4 = r8.c(r0)
                if (r4 != r1) goto L_0x00e7
                return r1
            L_0x00e7:
                r10 = r9
                r9 = r8
                r8 = r2
            L_0x00ea:
                java.util.Collection r4 = (java.util.Collection) r4
                r2.addAll(r4)
                r0.L$0 = r10
                r0.L$1 = r8
                r0.J$0 = r5
                r0.L$2 = r9
                r0.L$3 = r7
                r0.label = r3
                java.lang.Object r2 = r7.c(r0)
                if (r2 != r1) goto L_0x0102
                return r1
            L_0x0102:
                r3 = r8
            L_0x0103:
                java.util.List r2 = (java.util.List) r2
                if (r2 == 0) goto L_0x016d
                r3.addAll(r2)
                com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r1 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
                long r7 = java.lang.System.currentTimeMillis()
                r1.<init>(r3, r7)
                com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r2 = r2.c()
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                java.lang.String r3 = r3.c()
                long r1 = r2.b(r1, r3)
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.xs5$a r4 = com.fossil.xs5.x
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "filter notification, time end= "
                r7.append(r8)
                r7.append(r1)
                java.lang.String r7 = r7.toString()
                r3.d(r4, r7)
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.xs5$a r4 = com.fossil.xs5.x
                java.lang.String r4 = r4.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "delayTime ="
                r7.append(r8)
                long r1 = r1 - r5
                r7.append(r1)
                java.lang.String r1 = " milliseconds"
                r7.append(r1)
                java.lang.String r1 = r7.toString()
                r3.d(r4, r1)
            L_0x016d:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xs5.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ xs5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", f = "HomeAlertsPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xs5$e$a$a")
            /* renamed from: com.fossil.xs5$e$a$a  reason: collision with other inner class name */
            public static final class C0250a implements fl4.e<nw5.a, fl4.a> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public C0250a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                /* renamed from: a */
                public void onSuccess(nw5.a aVar) {
                    String str;
                    ee7.b(aVar, "responseValue");
                    FLogger.INSTANCE.getLocal().d(xs5.x.a(), "GetApps onSuccess");
                    ArrayList<at5> arrayList = new ArrayList();
                    arrayList.addAll(aVar.a());
                    if (this.a.this$0.a.u.F()) {
                        ArrayList arrayList2 = new ArrayList();
                        for (at5 at5 : arrayList) {
                            InstalledApp installedApp = at5.getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected == null) {
                                ee7.a();
                                throw null;
                            } else if (!isSelected.booleanValue()) {
                                arrayList2.add(at5);
                            }
                        }
                        this.a.this$0.a.j().clear();
                        this.a.this$0.a.j().addAll(arrayList);
                        if (!arrayList2.isEmpty()) {
                            this.a.this$0.a.a(arrayList2);
                            return;
                        }
                        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886497);
                        ss5 o = this.a.this$0.a.l;
                        ee7.a((Object) a2, "notificationAppOverView");
                        o.l(a2);
                        xs5 xs5 = this.a.this$0.a;
                        if (xs5.a(xs5.j(), arrayList)) {
                            this.a.this$0.a.l();
                            return;
                        }
                        return;
                    }
                    this.a.this$0.a.j().clear();
                    int i = 0;
                    for (at5 at52 : arrayList) {
                        InstalledApp installedApp2 = at52.getInstalledApp();
                        Boolean isSelected2 = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected2 == null) {
                            ee7.a();
                            throw null;
                        } else if (isSelected2.booleanValue()) {
                            this.a.this$0.a.j().add(at52);
                            i++;
                        }
                    }
                    if (i == aVar.a().size()) {
                        str = ig5.a(PortfolioApp.g0.c(), 2131886497);
                    } else if (i == 0) {
                        str = ig5.a(PortfolioApp.g0.c(), 2131886942);
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append(i);
                        sb.append('/');
                        sb.append(arrayList.size());
                        str = sb.toString();
                    }
                    ss5 o2 = this.a.this$0.a.l;
                    ee7.a((Object) str, "notificationAppOverView");
                    o2.l(str);
                }

                @DexIgnore
                public void a(fl4.a aVar) {
                    ee7.b(aVar, "errorValue");
                    FLogger.INSTANCE.getLocal().d(xs5.x.a(), "GetApps onError");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b<T> implements zd<List<? extends DNDScheduledTimeModel>> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xs5$e$a$b$a")
                @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", f = "HomeAlertsPresenter.kt", l = {166}, m = "invokeSuspend")
                /* renamed from: com.fossil.xs5$e$a$b$a  reason: collision with other inner class name */
                public static final class C0251a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ b this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.xs5$e$a$b$a$a")
                    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
                    /* renamed from: com.fossil.xs5$e$a$b$a$a  reason: collision with other inner class name */
                    public static final class C0252a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public yi7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ C0251a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0252a(C0251a aVar, fb7 fb7) {
                            super(2, fb7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.ob7
                        public final fb7<i97> create(Object obj, fb7<?> fb7) {
                            ee7.b(fb7, "completion");
                            C0252a aVar = new C0252a(this.this$0, fb7);
                            aVar.p$ = (yi7) obj;
                            return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.kd7
                        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                            return ((C0252a) create(yi7, fb7)).invokeSuspend(i97.a);
                        }

                        @DexIgnore
                        @Override // com.fossil.ob7
                        public final Object invokeSuspend(Object obj) {
                            nb7.a();
                            if (this.label == 0) {
                                t87.a(obj);
                                this.this$0.this$0.a.this$0.a.v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$0.$lDndScheduledTimeModel);
                                return i97.a;
                            }
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0251a(b bVar, List list, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = bVar;
                        this.$lDndScheduledTimeModel = list;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0251a aVar = new C0251a(this.this$0, this.$lDndScheduledTimeModel, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0251a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            ti7 c = this.this$0.a.this$0.a.c();
                            C0252a aVar = new C0252a(this, null);
                            this.L$0 = yi7;
                            this.label = 1;
                            if (vh7.a(c, aVar, this) == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return i97.a;
                    }
                }

                @DexIgnore
                public b(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                /* renamed from: a */
                public final void onChanged(List<DNDScheduledTimeModel> list) {
                    if (list == null || list.isEmpty()) {
                        ArrayList arrayList = new ArrayList();
                        DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                        DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                        arrayList.add(dNDScheduledTimeModel);
                        arrayList.add(dNDScheduledTimeModel2);
                        ik7 unused = xh7.b(this.a.this$0.a.e(), null, null, new C0251a(this, arrayList, null), 3, null);
                        return;
                    }
                    for (T t : list) {
                        if (t.getScheduledTimeType() == 0) {
                            this.a.this$0.a.l.b(this.a.this$0.a.b(t.getMinutes()));
                        } else {
                            this.a.this$0.a.l.a(this.a.this$0.a.b(t.getMinutes()));
                        }
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", f = "HomeAlertsPresenter.kt", l = {86, 91}, m = "invokeSuspend")
            public static final class c extends zb7 implements kd7<yi7, fb7<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    c cVar = new c(this.this$0, fb7);
                    cVar.p$ = (yi7) obj;
                    return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<Alarm>> fb7) {
                    return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        pd5 d = this.this$0.this$0.a.n;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (d.a(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.i) {
                        this.this$0.this$0.a.i = true;
                        pd5 d2 = this.this$0.this$0.a.n;
                        Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                        ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        d2.d(applicationContext);
                    }
                    AlarmsRepository e = this.this$0.this$0.a.t;
                    this.L$0 = yi7;
                    this.label = 2;
                    obj = e.getAllAlarmIgnoreDeletePinType(this);
                    return obj == a ? a : obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class d<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return bb7.a(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 c2 = this.this$0.a.c();
                    c cVar = new c(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj2 = vh7.a(c2, cVar, this);
                    if (obj2 == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<Alarm> list = (List) obj2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = xs5.x.a();
                local.d(a2, "GetAlarms onSuccess: size = " + list.size());
                this.this$0.a.g.clear();
                for (Alarm alarm : list) {
                    this.this$0.a.g.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                }
                ArrayList f = this.this$0.a.g;
                if (f.size() > 1) {
                    aa7.a(f, new d());
                }
                this.this$0.a.l.f(this.this$0.a.g);
                this.this$0.a.o.a((fl4.b) null, new C0250a(this));
                xs5 xs5 = this.this$0.a;
                xs5.h = xs5.u.J();
                this.this$0.a.l.z(this.this$0.a.h);
                this.this$0.a.f.a((LifecycleOwner) this.this$0.a.l, new b(this));
                return i97.a;
            }
        }

        @DexIgnore
        public e(xs5 xs5) {
            this.a = xs5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.l.a(true);
            } else {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ xs5 a;

        @DexIgnore
        public f(xs5 xs5) {
            this.a = xs5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ss5 unused = this.a.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<List<? extends DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ xs5 a;

        @DexIgnore
        public g(xs5 xs5) {
            this.a = xs5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DNDScheduledTimeModel> list) {
            ss5 unused = this.a.l;
        }
    }

    /*
    static {
        String simpleName = xs5.class.getSimpleName();
        ee7.a((Object) simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public xs5(ss5 ss5, rl4 rl4, pd5 pd5, nw5 nw5, vu5 vu5, mt5 mt5, NotificationSettingsDatabase notificationSettingsDatabase, qq5 qq5, AlarmsRepository alarmsRepository, ch5 ch5, DNDSettingsDatabase dNDSettingsDatabase) {
        ee7.b(ss5, "mView");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(pd5, "mAlarmHelper");
        ee7.b(nw5, "mGetApps");
        ee7.b(vu5, "mGetAllContactGroup");
        ee7.b(mt5, "mSaveAppsNotification");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        ee7.b(qq5, "mSetAlarms");
        ee7.b(alarmsRepository, "mAlarmRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = ss5;
        this.m = rl4;
        this.n = pd5;
        this.o = nw5;
        this.p = vu5;
        this.q = mt5;
        this.r = notificationSettingsDatabase;
        this.s = qq5;
        this.t = alarmsRepository;
        this.u = ch5;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    @k07
    public final void onSetAlarmEventEndComplete(ec5 ec5) {
        if (ec5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + ec5);
            if (ec5.b()) {
                String a2 = ec5.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (ee7.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.l.t();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.s.f();
        PortfolioApp.g0.b(this);
        LiveData<String> liveData = this.e;
        ss5 ss5 = this.l;
        if (ss5 != null) {
            liveData.a((ts5) ss5, new e(this));
            nj5.d.a(CommunicateMode.SET_LIST_ALARM);
            ss5 ss52 = this.l;
            ss52.d(true ^ xg5.a(xg5.b, ((ts5) ss52).getContext(), xg5.a.NOTIFICATION_DIANA, false, false, false, (Integer) null, 56, (Object) null));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.e.b(new f(this));
        this.f.b(new g(this));
        this.s.g();
        PortfolioApp.g0.c(this);
    }

    @DexIgnore
    @Override // com.fossil.rs5
    public void h() {
        xg5 xg5 = xg5.b;
        ss5 ss5 = this.l;
        if (ss5 != null) {
            xg5.a(xg5, ((ts5) ss5).getContext(), xg5.a.NOTIFICATION_DIANA, false, false, false, (Integer) null, 60, (Object) null);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.rs5
    public void i() {
        boolean z = !this.h;
        this.h = z;
        this.u.e(z);
        this.l.z(this.h);
    }

    @DexIgnore
    public final List<at5> j() {
        return this.j;
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.d(PortfolioApp.g0.c());
        String a2 = this.e.a();
        if (a2 != null) {
            PortfolioApp c2 = PortfolioApp.g0.c();
            ee7.a((Object) a2, "it");
            c2.j(a2);
        }
    }

    @DexIgnore
    public final void l() {
        xg5 xg5 = xg5.b;
        ss5 ss5 = this.l;
        if (ss5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
        } else if (xg5.a(xg5, ((ts5) ss5).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
            ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
        }
    }

    @DexIgnore
    public void m() {
        this.l.a(this);
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        ee7.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (ee7.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                if (!z) {
                    break;
                }
                k();
            }
        }
        this.l.f(this.g);
    }

    @DexIgnore
    public final boolean a(List<at5> list, List<at5> list2) {
        Boolean bool;
        T t2;
        ee7.b(list, "listDatabaseAppWrapper");
        ee7.b(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        for (T t3 : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                bool = null;
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                InstalledApp installedApp = t2.getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = t3.getInstalledApp();
                if (ee7.a((Object) identifier, (Object) (installedApp2 != null ? installedApp2.getIdentifier() : null))) {
                    break;
                }
            }
            T t4 = t2;
            if (t4 != null) {
                InstalledApp installedApp3 = t4.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = t3.getInstalledApp();
                if (installedApp4 != null) {
                    bool = installedApp4.isSelected();
                }
                if (!(!ee7.a(isSelected, bool))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.rs5
    public void a(Alarm alarm) {
        String a2 = this.e.a();
        if (a2 == null || a2.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            ss5 ss5 = this.l;
            String a3 = this.e.a();
            if (a3 != null) {
                ee7.a((Object) a3, "mActiveSerial.value!!");
                ss5.a(a3, this.g, alarm);
                return;
            }
            ee7.a();
            throw null;
        } else {
            this.l.r();
        }
    }

    @DexIgnore
    public final SpannableString b(int i2) {
        int i3 = i2 / 60;
        int i4 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.g0.c())) {
            StringBuilder sb = new StringBuilder();
            we7 we7 = we7.a;
            Locale locale = Locale.US;
            ee7.a((Object) locale, "Locale.US");
            String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            we7 we72 = we7.a;
            Locale locale2 = Locale.US;
            ee7.a((Object) locale2, "Locale.US");
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
            ee7.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        }
        int i5 = 12;
        if (i2 < 720) {
            if (i3 == 0) {
                i3 = 12;
            }
            xe5 xe5 = xe5.b;
            StringBuilder sb2 = new StringBuilder();
            we7 we73 = we7.a;
            Locale locale3 = Locale.US;
            ee7.a((Object) locale3, "Locale.US");
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            ee7.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            we7 we74 = we7.a;
            Locale locale4 = Locale.US;
            ee7.a((Object) locale4, "Locale.US");
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
            ee7.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886102);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return xe5.a(sb3, a2, 1.0f);
        }
        if (i3 > 12) {
            i5 = i3 - 12;
        }
        xe5 xe52 = xe5.b;
        StringBuilder sb4 = new StringBuilder();
        we7 we75 = we7.a;
        Locale locale5 = Locale.US;
        ee7.a((Object) locale5, "Locale.US");
        String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
        ee7.a((Object) format5, "java.lang.String.format(locale, format, *args)");
        sb4.append(format5);
        sb4.append(':');
        we7 we76 = we7.a;
        Locale locale6 = Locale.US;
        ee7.a((Object) locale6, "Locale.US");
        String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
        ee7.a((Object) format6, "java.lang.String.format(locale, format, *args)");
        sb4.append(format6);
        String sb5 = sb4.toString();
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886104);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
        return xe52.a(sb5, a3, 1.0f);
    }

    @DexIgnore
    @Override // com.fossil.rs5
    public void a(Alarm alarm, boolean z) {
        ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String a2 = this.e.a();
        if (!(a2 == null || a2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            qq5 qq5 = this.s;
            String a3 = this.e.a();
            if (a3 != null) {
                ee7.a((Object) a3, "mActiveSerial.value!!");
                qq5.a(new qq5.c(a3, this.g, alarm), new b(this, alarm));
                return;
            }
            ee7.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    public final void a(List<at5> list) {
        ee7.b(list, "listAppWrapperNotEnabled");
        for (at5 at5 : list) {
            InstalledApp installedApp = at5.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(true);
            }
        }
        this.m.a(this.q, new mt5.b(list), new c(this));
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886089);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886091);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886090);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }
}
