package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b55 extends a55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362109, 1);
        Q.put(2131361851, 2);
        Q.put(2131363342, 3);
        Q.put(2131362549, 4);
        Q.put(2131362320, 5);
        Q.put(2131363161, 6);
        Q.put(2131363147, 7);
        Q.put(2131363151, 8);
        Q.put(2131363468, 9);
        Q.put(2131362331, 10);
        Q.put(2131363164, 11);
        Q.put(2131363149, 12);
        Q.put(2131363153, 13);
        Q.put(2131362202, 14);
        Q.put(2131362319, 15);
        Q.put(2131363160, 16);
        Q.put(2131363146, 17);
        Q.put(2131363150, 18);
        Q.put(2131363128, 19);
        Q.put(2131362327, 20);
        Q.put(2131363163, 21);
        Q.put(2131363148, 22);
        Q.put(2131363152, 23);
    }
    */

    @DexIgnore
    public b55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 24, P, Q));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public b55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[2], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[14], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[10], (ConstraintLayout) objArr[4], (RelativeLayout) objArr[0], (ConstraintLayout) objArr[19], (TabItem) objArr[17], (TabItem) objArr[7], (TabItem) objArr[22], (TabItem) objArr[12], (TabItem) objArr[18], (TabItem) objArr[8], (TabItem) objArr[23], (TabItem) objArr[13], (TabLayout) objArr[16], (TabLayout) objArr[6], (TabLayout) objArr[21], (TabLayout) objArr[11], (FlexibleTextView) objArr[3], (ConstraintLayout) objArr[9]);
        this.O = -1;
        ((a55) this).y.setTag(null);
        a(view);
        f();
    }
}
