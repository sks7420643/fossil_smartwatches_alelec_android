package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my2 implements Comparable<my2>, Map.Entry<K, V> {
    @DexIgnore
    public /* final */ K a;
    @DexIgnore
    public V b;
    @DexIgnore
    public /* final */ /* synthetic */ dy2 c;

    @DexIgnore
    public my2(dy2 dy2, Map.Entry<K, V> entry) {
        this(dy2, entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(my2 my2) {
        return ((Comparable) getKey()).compareTo((Comparable) my2.getKey());
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return a(this.a, entry.getKey()) && a(this.b, entry.getValue());
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.a;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.b;
    }

    @DexIgnore
    public final int hashCode() {
        K k = this.a;
        int i = 0;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.b;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        this.c.f();
        V v2 = this.b;
        this.b = v;
        return v2;
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        return sb.toString();
    }

    @DexIgnore
    public my2(dy2 dy2, K k, V v) {
        this.c = dy2;
        this.a = k;
        this.b = v;
    }
}
