package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class d41 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[o80.values().length];
        a = iArr;
        iArr[o80.BIOMETRIC_PROFILE.ordinal()] = 1;
        a[o80.DAILY_STEP.ordinal()] = 2;
        a[o80.DAILY_STEP_GOAL.ordinal()] = 3;
        a[o80.DAILY_CALORIE.ordinal()] = 4;
        a[o80.DAILY_CALORIE_GOAL.ordinal()] = 5;
        a[o80.DAILY_TOTAL_ACTIVE_MINUTE.ordinal()] = 6;
        a[o80.DAILY_ACTIVE_MINUTE_GOAL.ordinal()] = 7;
        a[o80.DAILY_DISTANCE.ordinal()] = 8;
        a[o80.INACTIVE_NUDGE.ordinal()] = 9;
        a[o80.VIBE_STRENGTH.ordinal()] = 10;
        a[o80.DO_NOT_DISTURB_SCHEDULE.ordinal()] = 11;
        a[o80.TIME.ordinal()] = 12;
        a[o80.BATTERY.ordinal()] = 13;
        a[o80.HEART_RATE_MODE.ordinal()] = 14;
        a[o80.DAILY_SLEEP.ordinal()] = 15;
        a[o80.DISPLAY_UNIT.ordinal()] = 16;
        a[o80.SECOND_TIMEZONE_OFFSET.ordinal()] = 17;
        a[o80.CURRENT_HEART_RATE.ordinal()] = 18;
        a[o80.HELLAS_BATTERY.ordinal()] = 19;
        a[o80.AUTO_WORKOUT_DETECTION.ordinal()] = 20;
        a[o80.CYCLING_CADENCE.ordinal()] = 21;
    }
    */
}
