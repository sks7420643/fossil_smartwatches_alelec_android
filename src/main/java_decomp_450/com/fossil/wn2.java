package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ o43 g;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wn2(sn2 sn2, String str, String str2, o43 o43) {
        super(sn2);
        this.h = sn2;
        this.e = str;
        this.f = str2;
        this.g = o43;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.h.h.getConditionalUserProperties(this.e, this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void b() {
        this.g.a(null);
    }
}
