package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh6 {
    @DexIgnore
    public /* final */ Date a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public lh6(Date date, long j) {
        ee7.b(date, "date");
        this.a = date;
        this.b = j;
    }

    @DexIgnore
    public final Date a() {
        return this.a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lh6)) {
            return false;
        }
        lh6 lh6 = (lh6) obj;
        return ee7.a(this.a, lh6.a) && this.b == lh6.b;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.a;
        return ((date != null ? date.hashCode() : 0) * 31) + c.a(this.b);
    }

    @DexIgnore
    public String toString() {
        return "ActivityDailyBest(date=" + this.a + ", value=" + this.b + ")";
    }
}
