package com.fossil;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import com.fossil.ix;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qx<T> implements ix<T> {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ ContentResolver b;
    @DexIgnore
    public T c;

    @DexIgnore
    public qx(ContentResolver contentResolver, Uri uri) {
        this.b = contentResolver;
        this.a = uri;
    }

    @DexIgnore
    public abstract T a(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;

    @DexIgnore
    @Override // com.fossil.ix
    public final void a(ew ewVar, ix.a<? super T> aVar) {
        try {
            T a2 = a(this.a, this.b);
            this.c = a2;
            aVar.a((Object) a2);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.ix
    public sw b() {
        return sw.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.ix
    public void a() {
        T t = this.c;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
