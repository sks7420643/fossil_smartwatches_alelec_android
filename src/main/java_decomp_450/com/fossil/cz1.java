package com.fossil;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz1 implements ry1 {
    @DexIgnore
    @Override // com.fossil.ry1
    public final c12<Status> a(a12 a12) {
        return ez1.b(a12, a12.e(), false);
    }

    @DexIgnore
    @Override // com.fossil.ry1
    public final uy1 a(Intent intent) {
        return ez1.a(intent);
    }
}
