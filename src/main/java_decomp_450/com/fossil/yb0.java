package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yb0 extends bb0 {
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public yb0(cb0 cb0, byte b, int i) {
        super(cb0, b);
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(super.a(), r51.p, Integer.valueOf(this.c));
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(getClass(), obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((yb0) obj).c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.DeviceRequest");
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        return (super.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }

    @DexIgnore
    public yb0(Parcel parcel) {
        super(parcel);
        this.c = parcel.readInt();
    }
}
