package com.fossil;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.Utility;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm3 extends hi3 {
    @DexIgnore
    public static /* final */ String[] g; // = {"firebase_", "google_", "ga_"};
    @DexIgnore
    public static /* final */ String[] h; // = {"_err"};
    @DexIgnore
    public SecureRandom c;
    @DexIgnore
    public /* final */ AtomicLong d; // = new AtomicLong(0);
    @DexIgnore
    public int e;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public jm3(oh3 oh3) {
        super(oh3);
    }

    @DexIgnore
    public static boolean h(String str) {
        a72.b(str);
        if (str.charAt(0) != '_' || str.equals("_ep")) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean i(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public static boolean j(String str) {
        for (String str2 : h) {
            if (str2.equals(str)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean k(String str) {
        a72.a((Object) str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    @DexIgnore
    public static MessageDigest x() {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final Bundle a(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString("source", str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString("medium", str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e2) {
            e().w().a("Install referrer url isn't a hierarchical URI", e2);
            return null;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        if (str2 == null) {
            e().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            e().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        e().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            e().v().a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    public final Object c(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return a(g(str), obj, true, false);
        }
        return a(g(str), obj, false, false);
    }

    @DexIgnore
    public final boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String s = l().s();
        b();
        return s.equals(str);
    }

    @DexIgnore
    public final int e(String str) {
        if (!a("event param", str)) {
            return 3;
        }
        if (!a("event param", (String[]) null, str)) {
            return 14;
        }
        if (!a("event param", 40, str)) {
            return 3;
        }
        return 0;
    }

    @DexIgnore
    public final int f(String str) {
        if (!b("event param", str)) {
            return 3;
        }
        if (!a("event param", (String[]) null, str)) {
            return 14;
        }
        if (!a("event param", 40, str)) {
            return 3;
        }
        return 0;
    }

    @DexIgnore
    public final int g(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        if (FieldType.FOREIGN_ID_FIELD_SUFFIX.equals(str)) {
            return 256;
        }
        return (!l().a(wb3.l0) || !"_lgclid".equals(str)) ? 36 : 100;
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final void m() {
        g();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                e().w().a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    @DexIgnore
    @Override // com.fossil.hi3
    public final boolean q() {
        return true;
    }

    @DexIgnore
    public final long s() {
        long andIncrement;
        long j;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                long nextLong = new Random(System.nanoTime() ^ zzm().b()).nextLong();
                int i = this.e + 1;
                this.e = i;
                j = nextLong + ((long) i);
            }
            return j;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1, 1);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    @DexIgnore
    public final SecureRandom t() {
        g();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    @DexIgnore
    public final int u() {
        if (this.f == null) {
            this.f = Integer.valueOf(m02.a().b(f()) / 1000);
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final String v() {
        byte[] bArr = new byte[16];
        t().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    @DexIgnore
    public final boolean w() {
        try {
            f().getClassLoader().loadClass("com.google.firebase.remoteconfig.FirebaseRemoteConfig");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public static boolean c(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null || !serviceInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        g();
        if (ja2.b(f()).a(str) == 0) {
            return true;
        }
        e().A().a("Permission not granted", str);
        return false;
    }

    @DexIgnore
    public static boolean c(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    @DexIgnore
    public final int b(String str) {
        if (!b("user property", str)) {
            return 6;
        }
        if (!a("user property", pi3.a, str)) {
            return 15;
        }
        if (!a("user property", 24, str)) {
            return 6;
        }
        return 0;
    }

    @DexIgnore
    public final boolean b(String str, String str2, int i, Object obj) {
        int i2;
        if (obj instanceof Parcelable[]) {
            i2 = ((Parcelable[]) obj).length;
        } else {
            if (obj instanceof ArrayList) {
                i2 = ((ArrayList) obj).size();
            }
            return true;
        }
        if (i2 > i) {
            e().y().a("Parameter array is too long; discarded. Value kind, name, array length", str, str2, Integer.valueOf(i2));
            return false;
        }
        return true;
    }

    @DexIgnore
    public static Bundle[] b(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            return (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
        } else if (!(obj instanceof ArrayList)) {
            return null;
        } else {
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    @DexIgnore
    public static boolean a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    @DexIgnore
    public static boolean b(Bundle bundle, int i) {
        if (bundle == null || bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    @DexIgnore
    public final int b(String str, Object obj) {
        boolean z;
        if ("_ldl".equals(str)) {
            z = a("user property referrer", str, g(str), obj);
        } else {
            z = a("user property", str, g(str), obj);
        }
        return z ? 0 : 7;
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        if (str2 == null) {
            e().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            e().v().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                e().v().a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    e().v().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean b(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo b = ja2.b(context).b(str, 64);
            if (b == null || b.signatures == null || b.signatures.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(b.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (CertificateException e2) {
            e().t().a("Error obtaining certificate", e2);
            return true;
        } catch (PackageManager.NameNotFoundException e3) {
            e().t().a("Package name not found", e3);
            return true;
        }
    }

    @DexIgnore
    public static Bundle b(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String str : bundle2.keySet()) {
            Object obj = bundle2.get(str);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        if (parcelableArr[i] instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final boolean a(String str, String[] strArr, String str2) {
        boolean z;
        if (str2 == null) {
            e().v().a("Name is required and can't be null. Type", str);
            return false;
        }
        a72.a((Object) str2);
        String[] strArr2 = g;
        int length = strArr2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            e().v().a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        } else if (strArr == null || !a(str2, strArr)) {
            return true;
        } else {
            e().v().a("Name is reserved. Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, int i, String str2) {
        if (str2 == null) {
            e().v().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            e().v().a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    @DexIgnore
    public final int a(String str) {
        if (!b(Constants.EVENT, str)) {
            return 2;
        }
        if (!a(Constants.EVENT, ni3.a, str)) {
            return 13;
        }
        if (!a(Constants.EVENT, 40, str)) {
            return 2;
        }
        return 0;
    }

    @DexIgnore
    public static boolean a(Object obj) {
        return (obj instanceof Parcelable[]) || (obj instanceof ArrayList) || (obj instanceof Bundle);
    }

    @DexIgnore
    public static ArrayList<Bundle> b(List<wm3> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (wm3 wm3 : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", wm3.a);
            bundle.putString("origin", wm3.b);
            bundle.putLong("creation_timestamp", wm3.d);
            bundle.putString("name", wm3.c.b);
            ji3.a(bundle, wm3.c.zza());
            bundle.putBoolean("active", wm3.e);
            String str = wm3.f;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            ub3 ub3 = wm3.g;
            if (ub3 != null) {
                bundle.putString("timed_out_event_name", ub3.a);
                tb3 tb3 = wm3.g.b;
                if (tb3 != null) {
                    bundle.putBundle("timed_out_event_params", tb3.zzb());
                }
            }
            bundle.putLong("trigger_timeout", wm3.h);
            ub3 ub32 = wm3.i;
            if (ub32 != null) {
                bundle.putString("triggered_event_name", ub32.a);
                tb3 tb32 = wm3.i.b;
                if (tb32 != null) {
                    bundle.putBundle("triggered_event_params", tb32.zzb());
                }
            }
            bundle.putLong("triggered_timestamp", wm3.c.c);
            bundle.putLong("time_to_live", wm3.j);
            ub3 ub33 = wm3.p;
            if (ub33 != null) {
                bundle.putString("expired_event_name", ub33.a);
                tb3 tb33 = wm3.p.b;
                if (tb33 != null) {
                    bundle.putBundle("expired_event_params", tb33.zzb());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean a(String str, String str2, int i, Object obj) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
                return false;
            }
            String valueOf = String.valueOf(obj);
            if (valueOf.codePointCount(0, valueOf.length()) > i) {
                e().y().a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean a(Bundle bundle, int i) {
        int i2 = 0;
        if (bundle.size() <= i) {
            return false;
        }
        for (String str : new TreeSet(bundle.keySet())) {
            i2++;
            if (i2 > i) {
                bundle.remove(str);
            }
        }
        return true;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, Bundle bundle, List<String> list, boolean z) {
        int i;
        int i2;
        String str4;
        int i3;
        String str5;
        if (bundle != null) {
            boolean a = l().a(wb3.I0);
            if (a) {
                i = 0;
            } else {
                i = l().m();
            }
            int i4 = 0;
            for (String str6 : new TreeSet(bundle.keySet())) {
                if (list == null || !list.contains(str6)) {
                    i2 = z ? e(str6) : 0;
                    if (i2 == 0) {
                        i2 = f(str6);
                    }
                } else {
                    i2 = 0;
                }
                if (i2 != 0) {
                    a(bundle, i2, str6, str6, i2 == 3 ? str6 : null);
                    bundle.remove(str6);
                } else {
                    if (a(bundle.get(str6))) {
                        e().y().a("Nested Bundle parameters are not allowed; discarded. event name, param name, child param name", str2, str3, str6);
                        i3 = 22;
                        str4 = str6;
                    } else {
                        str4 = str6;
                        i3 = a(str, str2, str6, bundle.get(str6), bundle, list, z, false);
                    }
                    if (i3 != 0 && !"_ev".equals(str4)) {
                        a(bundle, i3, str4, str4, bundle.get(str4));
                        bundle.remove(str4);
                    } else if (h(str4) && (!a || !a(str4, mi3.d))) {
                        int i5 = i4 + 1;
                        if (i5 > i) {
                            if (a) {
                                str5 = "Item cannot contain custom parameters";
                            } else {
                                StringBuilder sb = new StringBuilder(63);
                                sb.append("Child bundles can't contain more than ");
                                sb.append(i);
                                sb.append(" custom params");
                                str5 = sb.toString();
                            }
                            e().v().a(str5, i().a(str2), i().a(bundle));
                            b(bundle, a ? 23 : 5);
                            bundle.remove(str4);
                        }
                        i4 = i5;
                    }
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            if (k(str)) {
                return true;
            }
            if (((ii3) this).a.y()) {
                e().v().a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", jg3.a(str));
            }
            return false;
        } else if (f23.a() && l().a(wb3.o0) && !TextUtils.isEmpty(str3)) {
            return true;
        } else {
            if (TextUtils.isEmpty(str2)) {
                if (((ii3) this).a.y()) {
                    e().v().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
                }
                return false;
            } else if (k(str2)) {
                return true;
            } else {
                e().v().a("Invalid admob_app_id. Analytics disabled.", jg3.a(str2));
                return false;
            }
        }
    }

    @DexIgnore
    public static boolean a(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    @DexIgnore
    public final Object a(int i, Object obj, boolean z, boolean z2) {
        Bundle a;
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return a(String.valueOf(obj), i, z);
            }
            if (!p03.a() || !l().a(wb3.H0) || !l().a(wb3.G0) || !z2 || (!(obj instanceof Bundle[]) && !(obj instanceof Parcelable[]))) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            for (Parcelable parcelable : parcelableArr) {
                if ((parcelable instanceof Bundle) && (a = a((Bundle) parcelable)) != null && !a.isEmpty()) {
                    arrayList.add(a);
                }
            }
            return arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    @DexIgnore
    public static String a(String str, int i, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0166 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(java.lang.String r18, java.lang.String r19, java.lang.String r20, java.lang.Object r21, android.os.Bundle r22, java.util.List<java.lang.String> r23, boolean r24, boolean r25) {
        /*
            r17 = this;
            r7 = r17
            r8 = r20
            r0 = r21
            r1 = r22
            r17.g()
            boolean r2 = com.fossil.p03.a()
            r3 = 17
            java.lang.String r4 = "param"
            r9 = 0
            if (r2 == 0) goto L_0x007f
            com.fossil.ym3 r2 = r17.l()
            com.fossil.yf3<java.lang.Boolean> r5 = com.fossil.wb3.I0
            boolean r2 = r2.a(r5)
            if (r2 == 0) goto L_0x007f
            boolean r2 = a(r21)
            if (r2 == 0) goto L_0x008a
            if (r25 == 0) goto L_0x007c
            java.lang.String[] r2 = com.fossil.mi3.c
            boolean r2 = a(r8, r2)
            if (r2 != 0) goto L_0x0035
            r0 = 20
            return r0
        L_0x0035:
            com.fossil.oh3 r2 = r7.a
            com.fossil.ek3 r2 = r2.E()
            boolean r2 = r2.H()
            if (r2 != 0) goto L_0x0044
            r0 = 25
            return r0
        L_0x0044:
            r2 = 200(0xc8, float:2.8E-43)
            boolean r5 = r7.b(r4, r8, r2, r0)
            if (r5 != 0) goto L_0x008a
            boolean r5 = r0 instanceof android.os.Parcelable[]
            if (r5 == 0) goto L_0x0060
            r5 = r0
            android.os.Parcelable[] r5 = (android.os.Parcelable[]) r5
            int r6 = r5.length
            if (r6 <= r2) goto L_0x0079
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r5, r2)
            android.os.Parcelable[] r2 = (android.os.Parcelable[]) r2
            r1.putParcelableArray(r8, r2)
            goto L_0x0079
        L_0x0060:
            boolean r5 = r0 instanceof java.util.ArrayList
            if (r5 == 0) goto L_0x0079
            r5 = r0
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            int r6 = r5.size()
            if (r6 <= r2) goto L_0x0079
            java.util.ArrayList r6 = new java.util.ArrayList
            java.util.List r2 = r5.subList(r9, r2)
            r6.<init>(r2)
            r1.putParcelableArrayList(r8, r6)
        L_0x0079:
            r10 = 17
            goto L_0x008b
        L_0x007c:
            r0 = 21
            return r0
        L_0x007f:
            if (r25 == 0) goto L_0x008a
            r1 = 1000(0x3e8, float:1.401E-42)
            boolean r1 = r7.b(r4, r8, r1, r0)
            if (r1 != 0) goto L_0x008a
            return r3
        L_0x008a:
            r10 = 0
        L_0x008b:
            com.fossil.ym3 r1 = r17.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.R
            r11 = r18
            boolean r1 = r1.e(r11, r2)
            if (r1 == 0) goto L_0x009f
            boolean r1 = i(r19)
            if (r1 != 0) goto L_0x00a5
        L_0x009f:
            boolean r1 = i(r20)
            if (r1 == 0) goto L_0x00a8
        L_0x00a5:
            r1 = 256(0x100, float:3.59E-43)
            goto L_0x00aa
        L_0x00a8:
            r1 = 100
        L_0x00aa:
            boolean r1 = r7.a(r4, r8, r1, r0)
            if (r1 == 0) goto L_0x00b1
            return r10
        L_0x00b1:
            if (r25 == 0) goto L_0x0167
            boolean r1 = com.fossil.p03.a()
            r12 = 1
            if (r1 == 0) goto L_0x00c8
            com.fossil.ym3 r1 = r17.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.H0
            boolean r1 = r1.a(r2)
            if (r1 == 0) goto L_0x00c8
            r13 = 1
            goto L_0x00c9
        L_0x00c8:
            r13 = 0
        L_0x00c9:
            boolean r1 = r0 instanceof android.os.Bundle
            if (r1 == 0) goto L_0x00e4
            if (r13 == 0) goto L_0x00e1
            r4 = r0
            android.os.Bundle r4 = (android.os.Bundle) r4
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r5 = r23
            r6 = r24
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x00e1:
            r9 = 1
            goto L_0x0164
        L_0x00e4:
            boolean r1 = r0 instanceof android.os.Parcelable[]
            if (r1 == 0) goto L_0x0123
            r14 = r0
            android.os.Parcelable[] r14 = (android.os.Parcelable[]) r14
            int r15 = r14.length
            r6 = 0
        L_0x00ed:
            if (r6 >= r15) goto L_0x00e1
            r0 = r14[r6]
            boolean r1 = r0 instanceof android.os.Bundle
            if (r1 != 0) goto L_0x0107
            com.fossil.jg3 r1 = r17.e()
            com.fossil.mg3 r1 = r1.y()
            java.lang.Class r0 = r0.getClass()
            java.lang.String r2 = "All Parcelable[] elements must be of type Bundle. Value type, name"
            r1.a(r2, r0, r8)
            goto L_0x0164
        L_0x0107:
            if (r13 == 0) goto L_0x011e
            r4 = r0
            android.os.Bundle r4 = (android.os.Bundle) r4
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r5 = r23
            r16 = r6
            r6 = r24
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0120
        L_0x011e:
            r16 = r6
        L_0x0120:
            int r6 = r16 + 1
            goto L_0x00ed
        L_0x0123:
            boolean r1 = r0 instanceof java.util.ArrayList
            if (r1 == 0) goto L_0x0164
            r14 = r0
            java.util.ArrayList r14 = (java.util.ArrayList) r14
            int r15 = r14.size()
            r0 = 0
        L_0x012f:
            if (r0 >= r15) goto L_0x00e1
            java.lang.Object r1 = r14.get(r0)
            int r16 = r0 + 1
            boolean r0 = r1 instanceof android.os.Bundle
            if (r0 != 0) goto L_0x014d
            com.fossil.jg3 r0 = r17.e()
            com.fossil.mg3 r0 = r0.y()
            java.lang.Class r1 = r1.getClass()
            java.lang.String r2 = "All ArrayList elements must be of type Bundle. Value type, name"
            r0.a(r2, r1, r8)
            goto L_0x0164
        L_0x014d:
            if (r13 == 0) goto L_0x0161
            r4 = r1
            android.os.Bundle r4 = (android.os.Bundle) r4
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r5 = r23
            r6 = r24
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x0161:
            r0 = r16
            goto L_0x012f
        L_0x0164:
            if (r9 == 0) goto L_0x0167
            return r10
        L_0x0167:
            r0 = 4
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jm3.a(java.lang.String, java.lang.String, java.lang.String, java.lang.Object, android.os.Bundle, java.util.List, boolean, boolean):int");
    }

    @DexIgnore
    public final Object a(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return a(256, obj, true, true);
        }
        if (!i(str)) {
            i = 100;
        }
        return a(i, obj, false, true);
    }

    @DexIgnore
    public final Bundle a(String str, String str2, Bundle bundle, List<String> list, boolean z, boolean z2) {
        Set<String> set;
        int i;
        int i2;
        Bundle bundle2;
        String str3;
        int i3;
        boolean z3 = p03.a() && l().a(wb3.I0);
        boolean a = z3 ? a(str2, ni3.c) : z2;
        if (bundle == null) {
            return null;
        }
        Bundle bundle3 = new Bundle(bundle);
        int m = l().m();
        if (l().e(str, wb3.b0)) {
            set = new TreeSet<>(bundle.keySet());
        } else {
            set = bundle.keySet();
        }
        int i4 = 0;
        for (String str4 : set) {
            if (list == null || !list.contains(str4)) {
                i = z ? e(str4) : 0;
                if (i == 0) {
                    i = f(str4);
                }
            } else {
                i = 0;
            }
            if (i != 0) {
                a(bundle3, i, str4, str4, i == 3 ? str4 : null);
                bundle3.remove(str4);
                i2 = m;
                bundle2 = bundle3;
            } else {
                i2 = m;
                int a2 = a(str, str2, str4, bundle.get(str4), bundle3, list, z, a);
                if (!z3 || a2 != 17) {
                    str3 = str4;
                    bundle2 = bundle3;
                    if (a2 != 0 && !"_ev".equals(str3)) {
                        a(bundle2, a2, a2 == 21 ? str2 : str3, str3, bundle.get(str3));
                        bundle2.remove(str3);
                    }
                } else {
                    str3 = str4;
                    bundle2 = bundle3;
                    a(bundle2, a2, str3, str3, (Object) false);
                }
                if (h(str3)) {
                    int i5 = i4 + 1;
                    i3 = i2;
                    if (i5 > i3) {
                        StringBuilder sb = new StringBuilder(48);
                        sb.append("Event can't contain more than ");
                        sb.append(i3);
                        sb.append(" params");
                        e().v().a(sb.toString(), i().a(str2), i().a(bundle));
                        b(bundle2, 5);
                        bundle2.remove(str3);
                        i4 = i5;
                        m = i3;
                        bundle3 = bundle2;
                    } else {
                        i4 = i5;
                    }
                } else {
                    i3 = i2;
                }
                m = i3;
                bundle3 = bundle2;
            }
            bundle3 = bundle2;
            m = i2;
        }
        return bundle3;
    }

    @DexIgnore
    public final void a(og3 og3, int i) {
        int i2 = 0;
        for (String str : new TreeSet(og3.d.keySet())) {
            if (h(str) && (i2 = i2 + 1) > i) {
                StringBuilder sb = new StringBuilder(48);
                sb.append("Event can't contain more than ");
                sb.append(i);
                sb.append(" params");
                e().v().a(sb.toString(), i().a(og3.a), i().a(og3.d));
                b(og3.d, 5);
                og3.d.remove(str);
            }
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, Bundle bundle2) {
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                if (!bundle.containsKey(str)) {
                    j().a(bundle, str, bundle2.get(str));
                }
            }
        }
    }

    @DexIgnore
    public static void a(Bundle bundle, int i, String str, String str2, Object obj) {
        if (b(bundle, i)) {
            bundle.putString("_ev", a(str, 40, true));
            if (obj != null) {
                a72.a(bundle);
                if (obj == null) {
                    return;
                }
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    bundle.putLong("_el", (long) String.valueOf(obj).length());
                }
            }
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (p03.a() && l().a(wb3.H0) && l().a(wb3.G0) && (obj instanceof Bundle[])) {
                bundle.putParcelableArray(str, (Bundle[]) obj);
            } else if (str != null) {
                e().y().a("Not putting event parameter. Invalid value type. name, type", i().b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    @DexIgnore
    public final void a(int i, String str, String str2, int i2) {
        a((String) null, i, str, str2, i2);
    }

    @DexIgnore
    public final void a(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        b(bundle, i);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        ((ii3) this).a.b();
        ((ii3) this).a.u().a("auto", "_err", bundle);
    }

    @DexIgnore
    public static long a(byte[] bArr) {
        a72.a(bArr);
        int i = 0;
        a72.b(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    @DexIgnore
    public static boolean a(Context context, boolean z) {
        a72.a(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return c(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return c(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    @DexIgnore
    public static boolean a(Boolean bool, Boolean bool2) {
        if (bool == null && bool2 == null) {
            return true;
        }
        if (bool == null) {
            return false;
        }
        return bool.equals(bool2);
    }

    @DexIgnore
    public static boolean a(List<String> list, List<String> list2) {
        if (list == null && list2 == null) {
            return true;
        }
        if (list == null) {
            return false;
        }
        return list.equals(list2);
    }

    @DexIgnore
    public final Bundle a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object a = a(str, bundle.get(str));
                if (a == null) {
                    e().y().a("Param value can't be null", i().b(str));
                } else {
                    a(bundle2, str, a);
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final ub3 a(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (a(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            bundle2.putString("_o", str3);
            return new ub3(str2, new tb3(a(a(str, str2, bundle2, o92.a("_o"), false, false))), str3, j);
        }
        e().t().a("Invalid conditional property event name", i().c(str2));
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final long a(Context context, String str) {
        g();
        a72.a(context);
        a72.b(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest x = x();
        if (x == null) {
            e().t().a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!b(context, str)) {
                    PackageInfo b = ja2.b(context).b(f().getPackageName(), 64);
                    if (b.signatures != null && b.signatures.length > 0) {
                        return a(x.digest(b.signatures[0].toByteArray()));
                    }
                    e().w().a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                e().t().a("Package name not found", e2);
            }
        }
        return 0;
    }

    @DexIgnore
    public static byte[] a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public static boolean a(String str, String[] strArr) {
        a72.a(strArr);
        for (String str2 : strArr) {
            if (c(str, str2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int a(int i) {
        return m02.a().a(f(), q02.a);
    }

    @DexIgnore
    public static long a(long j, long j2) {
        return (j + (j2 * 60000)) / LogBuilder.MAX_INTERVAL;
    }

    @DexIgnore
    public final void a(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            e().w().a("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }

    @DexIgnore
    public final void a(r43 r43, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning string value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(r43 r43, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning long value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(r43 r43, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning int value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(r43 r43, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning byte array to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(r43 r43, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning boolean value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void a(r43 r43, Bundle bundle) {
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning bundle value to wrapper", e2);
        }
    }

    @DexIgnore
    public static Bundle a(List<em3> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (em3 em3 : list) {
            String str = em3.e;
            if (str != null) {
                bundle.putString(em3.b, str);
            } else {
                Long l = em3.d;
                if (l != null) {
                    bundle.putLong(em3.b, l.longValue());
                } else {
                    Double d2 = em3.g;
                    if (d2 != null) {
                        bundle.putDouble(em3.b, d2.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    @DexIgnore
    public final void a(r43 r43, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            r43.a(bundle);
        } catch (RemoteException e2) {
            ((ii3) this).a.e().w().a("Error returning bundle list to wrapper", e2);
        }
    }

    @DexIgnore
    public final URL a(long j, String str, String str2, long j2) {
        try {
            a72.b(str2);
            a72.b(str);
            String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", Long.valueOf(j), Integer.valueOf(u())), str2, str, Long.valueOf(j2));
            if (str.equals(l().t())) {
                format = format.concat("&ddl_test=1");
            }
            return new URL(format);
        } catch (IllegalArgumentException | MalformedURLException e2) {
            e().t().a("Failed to create BOW URL for Deferred Deep Link. exception", e2.getMessage());
            return null;
        }
    }

    @DexIgnore
    @SuppressLint({"ApplySharedPref"})
    public final boolean a(String str, double d2) {
        try {
            SharedPreferences.Editor edit = f().getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
            edit.putString("deeplink", str);
            edit.putLong("timestamp", Double.doubleToRawLongBits(d2));
            return edit.commit();
        } catch (Exception e2) {
            e().t().a("Failed to persist Deferred Deep Link. exception", e2);
            return false;
        }
    }

    @DexIgnore
    public static long a(tb3 tb3) {
        long j = 0;
        if (tb3 == null) {
            return 0;
        }
        Iterator<String> it = tb3.iterator();
        while (it.hasNext()) {
            Object b = tb3.b(it.next());
            if (b instanceof Parcelable[]) {
                j += (long) ((Parcelable[]) b).length;
            }
        }
        return j;
    }
}
