package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm3 implements Parcelable.Creator<nm3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nm3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        String str8 = null;
        long j6 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    str = j72.e(parcel, a);
                    break;
                case 3:
                    str2 = j72.e(parcel, a);
                    break;
                case 4:
                    str3 = j72.e(parcel, a);
                    break;
                case 5:
                    str4 = j72.e(parcel, a);
                    break;
                case 6:
                    j = j72.s(parcel, a);
                    break;
                case 7:
                    j2 = j72.s(parcel, a);
                    break;
                case 8:
                    str5 = j72.e(parcel, a);
                    break;
                case 9:
                    z = j72.i(parcel, a);
                    break;
                case 10:
                    z2 = j72.i(parcel, a);
                    break;
                case 11:
                    j6 = j72.s(parcel, a);
                    break;
                case 12:
                    str6 = j72.e(parcel, a);
                    break;
                case 13:
                    j3 = j72.s(parcel, a);
                    break;
                case 14:
                    j4 = j72.s(parcel, a);
                    break;
                case 15:
                    i = j72.q(parcel, a);
                    break;
                case 16:
                    z3 = j72.i(parcel, a);
                    break;
                case 17:
                    z4 = j72.i(parcel, a);
                    break;
                case 18:
                    z5 = j72.i(parcel, a);
                    break;
                case 19:
                    str7 = j72.e(parcel, a);
                    break;
                case 20:
                default:
                    j72.v(parcel, a);
                    break;
                case 21:
                    bool = j72.j(parcel, a);
                    break;
                case 22:
                    j5 = j72.s(parcel, a);
                    break;
                case 23:
                    arrayList = j72.g(parcel, a);
                    break;
                case 24:
                    str8 = j72.e(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new nm3(str, str2, str3, str4, j, j2, str5, z, z2, j6, str6, j3, j4, i, z3, z4, z5, str7, bool, j5, arrayList, str8);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nm3[] newArray(int i) {
        return new nm3[i];
    }
}
