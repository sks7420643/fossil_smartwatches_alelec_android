package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cm2 implements Parcelable.Creator<bm2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ bm2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        List<i62> list = bm2.h;
        LocationRequest locationRequest = null;
        String str = null;
        String str2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 != 1) {
                switch (a2) {
                    case 5:
                        list = j72.c(parcel, a, i62.CREATOR);
                        continue;
                    case 6:
                        str = j72.e(parcel, a);
                        continue;
                    case 7:
                        z = j72.i(parcel, a);
                        continue;
                    case 8:
                        z2 = j72.i(parcel, a);
                        continue;
                    case 9:
                        z3 = j72.i(parcel, a);
                        continue;
                    case 10:
                        str2 = j72.e(parcel, a);
                        continue;
                    default:
                        j72.v(parcel, a);
                        continue;
                }
            } else {
                locationRequest = (LocationRequest) j72.a(parcel, a, LocationRequest.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new bm2(locationRequest, list, str, z, z2, z3, str2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ bm2[] newArray(int i) {
        return new bm2[i];
    }
}
