package com.fossil;

import android.util.SparseArray;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class yt1 extends Enum<yt1> {
    @DexIgnore
    public static /* final */ SparseArray<yt1> a;
    @DexIgnore
    public static /* final */ yt1 zza; // = new yt1(MessengerShareContentUtility.PREVIEW_DEFAULT, 0, 0);
    @DexIgnore
    public static /* final */ yt1 zzb; // = new yt1("UNMETERED_ONLY", 1, 1);
    @DexIgnore
    public static /* final */ yt1 zzc; // = new yt1("UNMETERED_OR_DAILY", 2, 2);
    @DexIgnore
    public static /* final */ yt1 zzd; // = new yt1("FAST_IF_RADIO_AWAKE", 3, 3);
    @DexIgnore
    public static /* final */ yt1 zze; // = new yt1("NEVER", 4, 4);
    @DexIgnore
    public static /* final */ yt1 zzf; // = new yt1("UNRECOGNIZED", 5, -1);

    /*
    static {
        SparseArray<yt1> sparseArray = new SparseArray<>();
        a = sparseArray;
        sparseArray.put(0, zza);
        a.put(1, zzb);
        a.put(2, zzc);
        a.put(3, zzd);
        a.put(4, zze);
        a.put(-1, zzf);
    }
    */

    @DexIgnore
    public yt1(String str, int i, int i2) {
    }
}
