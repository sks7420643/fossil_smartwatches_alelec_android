package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj5 implements Factory<uj5> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;
    @DexIgnore
    public /* final */ Provider<xo4> c;
    @DexIgnore
    public /* final */ Provider<ch5> d;

    @DexIgnore
    public vj5(Provider<PortfolioApp> provider, Provider<ro4> provider2, Provider<xo4> provider3, Provider<ch5> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static vj5 a(Provider<PortfolioApp> provider, Provider<ro4> provider2, Provider<xo4> provider3, Provider<ch5> provider4) {
        return new vj5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static uj5 a(PortfolioApp portfolioApp, ro4 ro4, xo4 xo4, ch5 ch5) {
        return new uj5(portfolioApp, ro4, xo4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public uj5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
