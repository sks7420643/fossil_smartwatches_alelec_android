package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b54 extends v54 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ v54.d h;
    @DexIgnore
    public /* final */ v54.c i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public v54.d g;
        @DexIgnore
        public v54.c h;

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a a(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a b(String str) {
            if (str != null) {
                this.f = str;
                return this;
            }
            throw new NullPointerException("Null displayVersion");
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a c(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null gmpAppId");
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a d(String str) {
            if (str != null) {
                this.d = str;
                return this;
            }
            throw new NullPointerException("Null installationUuid");
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a e(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null sdkVersion");
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a a(String str) {
            if (str != null) {
                this.e = str;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }

        @DexIgnore
        public b(v54 v54) {
            this.a = v54.g();
            this.b = v54.c();
            this.c = Integer.valueOf(v54.f());
            this.d = v54.d();
            this.e = v54.a();
            this.f = v54.b();
            this.g = v54.h();
            this.h = v54.e();
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a a(v54.d dVar) {
            this.g = dVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54.a a(v54.c cVar) {
            this.h = cVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.a
        public v54 a() {
            String str = "";
            if (this.a == null) {
                str = str + " sdkVersion";
            }
            if (this.b == null) {
                str = str + " gmpAppId";
            }
            if (this.c == null) {
                str = str + " platform";
            }
            if (this.d == null) {
                str = str + " installationUuid";
            }
            if (this.e == null) {
                str = str + " buildVersion";
            }
            if (this.f == null) {
                str = str + " displayVersion";
            }
            if (str.isEmpty()) {
                return new b54(this.a, this.b, this.c.intValue(), this.d, this.e, this.f, this.g, this.h);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54
    public String a() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public String b() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public String c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public String d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public v54.c e() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        v54.d dVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54)) {
            return false;
        }
        v54 v54 = (v54) obj;
        if (this.b.equals(v54.g()) && this.c.equals(v54.c()) && this.d == v54.f() && this.e.equals(v54.d()) && this.f.equals(v54.a()) && this.g.equals(v54.b()) && ((dVar = this.h) != null ? dVar.equals(v54.h()) : v54.h() == null)) {
            v54.c cVar = this.i;
            if (cVar == null) {
                if (v54.e() == null) {
                    return true;
                }
            } else if (cVar.equals(v54.e())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public int f() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public String g() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public v54.d h() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((this.b.hashCode() ^ 1000003) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d) * 1000003) ^ this.e.hashCode()) * 1000003) ^ this.f.hashCode()) * 1000003) ^ this.g.hashCode()) * 1000003;
        v54.d dVar = this.h;
        int i2 = 0;
        int hashCode2 = (hashCode ^ (dVar == null ? 0 : dVar.hashCode())) * 1000003;
        v54.c cVar = this.i;
        if (cVar != null) {
            i2 = cVar.hashCode();
        }
        return hashCode2 ^ i2;
    }

    @DexIgnore
    @Override // com.fossil.v54
    public v54.a j() {
        return new b(this);
    }

    @DexIgnore
    public String toString() {
        return "CrashlyticsReport{sdkVersion=" + this.b + ", gmpAppId=" + this.c + ", platform=" + this.d + ", installationUuid=" + this.e + ", buildVersion=" + this.f + ", displayVersion=" + this.g + ", session=" + this.h + ", ndkPayload=" + this.i + "}";
    }

    @DexIgnore
    public b54(String str, String str2, int i2, String str3, String str4, String str5, v54.d dVar, v54.c cVar) {
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = dVar;
        this.i = cVar;
    }
}
