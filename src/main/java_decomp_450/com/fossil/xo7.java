package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xo7 extends cr7 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public xo7(qr7 qr7) {
        super(qr7);
    }

    @DexIgnore
    @Override // com.fossil.qr7, com.fossil.cr7
    public void a(yq7 yq7, long j) throws IOException {
        if (this.b) {
            yq7.skip(j);
            return;
        }
        try {
            super.a(yq7, j);
        } catch (IOException e) {
            this.b = true;
            a(e);
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        throw null;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.qr7, com.fossil.cr7, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.b) {
            try {
                super.close();
            } catch (IOException e) {
                this.b = true;
                a(e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qr7, com.fossil.cr7, java.io.Flushable
    public void flush() throws IOException {
        if (!this.b) {
            try {
                super.flush();
            } catch (IOException e) {
                this.b = true;
                a(e);
            }
        }
    }
}
