package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.internal.Utility;
import com.google.maps.internal.UrlSigner;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class br7 implements Serializable, Comparable<br7> {
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public static /* final */ br7 EMPTY; // = new br7(new byte[0]);
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public transient int a;
    @DexIgnore
    public transient String b;
    @DexIgnore
    public /* final */ byte[] data;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final br7 a(ByteBuffer byteBuffer) {
            ee7.b(byteBuffer, "$this$toByteString");
            byte[] bArr = new byte[byteBuffer.remaining()];
            byteBuffer.get(bArr);
            return new br7(bArr);
        }

        @DexIgnore
        public final br7 b(String str) {
            ee7.b(str, "$this$decodeHex");
            if (str.length() % 2 == 0) {
                int length = str.length() / 2;
                byte[] bArr = new byte[length];
                for (int i = 0; i < length; i++) {
                    int i2 = i * 2;
                    bArr[i] = (byte) ((vr7.b(str.charAt(i2)) << 4) + vr7.b(str.charAt(i2 + 1)));
                }
                return new br7(bArr);
            }
            throw new IllegalArgumentException(("Unexpected hex string: " + str).toString());
        }

        @DexIgnore
        public final br7 c(String str) {
            ee7.b(str, "$this$encodeUtf8");
            br7 br7 = new br7(uq7.a(str));
            br7.setUtf8$okio(str);
            return br7;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final br7 a(String str, Charset charset) {
            ee7.b(str, "$this$encode");
            ee7.b(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return new br7(bytes);
        }

        @DexIgnore
        public final br7 a(InputStream inputStream, int i) throws IOException {
            ee7.b(inputStream, "$this$readByteString");
            int i2 = 0;
            if (i >= 0) {
                byte[] bArr = new byte[i];
                while (i2 < i) {
                    int read = inputStream.read(bArr, i2, i - i2);
                    if (read != -1) {
                        i2 += read;
                    } else {
                        throw new EOFException();
                    }
                }
                return new br7(bArr);
            }
            throw new IllegalArgumentException(("byteCount < 0: " + i).toString());
        }

        @DexIgnore
        public final br7 a(byte... bArr) {
            ee7.b(bArr, "data");
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
            return new br7(copyOf);
        }

        @DexIgnore
        public final br7 a(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "$this$toByteString");
            vq7.a((long) bArr.length, (long) i, (long) i2);
            return new br7(s97.a(bArr, i, i2 + i));
        }

        @DexIgnore
        public final br7 a(String str) {
            ee7.b(str, "$this$decodeBase64");
            byte[] a = tq7.a(str);
            if (a != null) {
                return new br7(a);
            }
            return null;
        }
    }

    @DexIgnore
    public br7(byte[] bArr) {
        ee7.b(bArr, "data");
        this.data = bArr;
    }

    @DexIgnore
    public static final br7 decodeBase64(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public static final br7 decodeHex(String str) {
        return Companion.b(str);
    }

    @DexIgnore
    public static final br7 encodeString(String str, Charset charset) {
        return Companion.a(str, charset);
    }

    @DexIgnore
    public static final br7 encodeUtf8(String str) {
        return Companion.c(str);
    }

    @DexIgnore
    public static /* synthetic */ int indexOf$default(br7 br7, br7 br72, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return br7.indexOf(br72, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    @DexIgnore
    public static /* synthetic */ int indexOf$default(br7 br7, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return br7.indexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    @DexIgnore
    public static /* synthetic */ int lastIndexOf$default(br7 br7, br7 br72, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = br7.size();
            }
            return br7.lastIndexOf(br72, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    @DexIgnore
    public static /* synthetic */ int lastIndexOf$default(br7 br7, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = br7.size();
            }
            return br7.lastIndexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    @DexIgnore
    public static final br7 of(ByteBuffer byteBuffer) {
        return Companion.a(byteBuffer);
    }

    @DexIgnore
    public static final br7 of(byte... bArr) {
        return Companion.a(bArr);
    }

    @DexIgnore
    public static final br7 of(byte[] bArr, int i, int i2) {
        return Companion.a(bArr, i, i2);
    }

    @DexIgnore
    public static final br7 read(InputStream inputStream, int i) throws IOException {
        return Companion.a(inputStream, i);
    }

    @DexIgnore
    private final void readObject(ObjectInputStream objectInputStream) throws IOException {
        br7 a2 = Companion.a(objectInputStream, objectInputStream.readInt());
        Field declaredField = br7.class.getDeclaredField("data");
        ee7.a((Object) declaredField, "field");
        declaredField.setAccessible(true);
        declaredField.set(this, a2.data);
    }

    @DexIgnore
    public static /* synthetic */ br7 substring$default(br7 br7, int i, int i2, int i3, Object obj) {
        if (obj == null) {
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = br7.size();
            }
            return br7.substring(i, i2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: substring");
    }

    @DexIgnore
    private final void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    @DexIgnore
    /* renamed from: -deprecated_getByte  reason: not valid java name */
    public final byte m5deprecated_getByte(int i) {
        return getByte(i);
    }

    @DexIgnore
    /* renamed from: -deprecated_size  reason: not valid java name */
    public final int m6deprecated_size() {
        return size();
    }

    @DexIgnore
    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(this.data).asReadOnlyBuffer();
        ee7.a((Object) asReadOnlyBuffer, "ByteBuffer.wrap(data).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    @DexIgnore
    public String base64() {
        return tq7.a(getData$okio(), null, 1, null);
    }

    @DexIgnore
    public String base64Url() {
        return tq7.a(getData$okio(), tq7.a());
    }

    @DexIgnore
    public br7 digest$okio(String str) {
        ee7.b(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        ee7.a((Object) digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new br7(digest);
    }

    @DexIgnore
    public final boolean endsWith(br7 br7) {
        ee7.b(br7, "suffix");
        return rangeEquals(size() - br7.size(), br7, 0, br7.size());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof br7) {
            br7 br7 = (br7) obj;
            return br7.size() == getData$okio().length && br7.rangeEquals(0, getData$okio(), 0, getData$okio().length);
        }
    }

    @DexIgnore
    public final byte getByte(int i) {
        return internalGet$okio(i);
    }

    @DexIgnore
    public final byte[] getData$okio() {
        return this.data;
    }

    @DexIgnore
    public final int getHashCode$okio() {
        return this.a;
    }

    @DexIgnore
    public int getSize$okio() {
        return getData$okio().length;
    }

    @DexIgnore
    public final String getUtf8$okio() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio != 0) {
            return hashCode$okio;
        }
        int hashCode = Arrays.hashCode(getData$okio());
        setHashCode$okio(hashCode);
        return hashCode;
    }

    @DexIgnore
    public String hex() {
        char[] cArr = new char[(getData$okio().length * 2)];
        byte[] data$okio = getData$okio();
        int i = 0;
        for (byte b2 : data$okio) {
            int i2 = i + 1;
            cArr[i] = vr7.a()[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = vr7.a()[b2 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new String(cArr);
    }

    @DexIgnore
    public br7 hmac$okio(String str, br7 br7) {
        ee7.b(str, "algorithm");
        ee7.b(br7, "key");
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(br7.toByteArray(), str));
            byte[] doFinal = instance.doFinal(this.data);
            ee7.a((Object) doFinal, "mac.doFinal(data)");
            return new br7(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public br7 hmacSha1(br7 br7) {
        ee7.b(br7, "key");
        return hmac$okio(UrlSigner.ALGORITHM_HMAC_SHA1, br7);
    }

    @DexIgnore
    public br7 hmacSha256(br7 br7) {
        ee7.b(br7, "key");
        return hmac$okio("HmacSHA256", br7);
    }

    @DexIgnore
    public br7 hmacSha512(br7 br7) {
        ee7.b(br7, "key");
        return hmac$okio("HmacSHA512", br7);
    }

    @DexIgnore
    public final int indexOf(br7 br7) {
        return indexOf$default(this, br7, 0, 2, (Object) null);
    }

    @DexIgnore
    public final int indexOf(br7 br7, int i) {
        ee7.b(br7, FacebookRequestErrorClassification.KEY_OTHER);
        return indexOf(br7.internalArray$okio(), i);
    }

    @DexIgnore
    public int indexOf(byte[] bArr) {
        return indexOf$default(this, bArr, 0, 2, (Object) null);
    }

    @DexIgnore
    public byte[] internalArray$okio() {
        return getData$okio();
    }

    @DexIgnore
    public byte internalGet$okio(int i) {
        return getData$okio()[i];
    }

    @DexIgnore
    public final int lastIndexOf(br7 br7) {
        return lastIndexOf$default(this, br7, 0, 2, (Object) null);
    }

    @DexIgnore
    public final int lastIndexOf(br7 br7, int i) {
        ee7.b(br7, FacebookRequestErrorClassification.KEY_OTHER);
        return lastIndexOf(br7.internalArray$okio(), i);
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr) {
        return lastIndexOf$default(this, bArr, 0, 2, (Object) null);
    }

    @DexIgnore
    public br7 md5() {
        return digest$okio(Utility.HASH_ALGORITHM_MD5);
    }

    @DexIgnore
    public boolean rangeEquals(int i, br7 br7, int i2, int i3) {
        ee7.b(br7, FacebookRequestErrorClassification.KEY_OTHER);
        return br7.rangeEquals(i2, getData$okio(), i, i3);
    }

    @DexIgnore
    public final void setHashCode$okio(int i) {
        this.a = i;
    }

    @DexIgnore
    public final void setUtf8$okio(String str) {
        this.b = str;
    }

    @DexIgnore
    public br7 sha1() {
        return digest$okio(Utility.HASH_ALGORITHM_SHA1);
    }

    @DexIgnore
    public br7 sha256() {
        return digest$okio("SHA-256");
    }

    @DexIgnore
    public br7 sha512() {
        return digest$okio("SHA-512");
    }

    @DexIgnore
    public final int size() {
        return getSize$okio();
    }

    @DexIgnore
    public final boolean startsWith(br7 br7) {
        ee7.b(br7, "prefix");
        return rangeEquals(0, br7, 0, br7.size());
    }

    @DexIgnore
    public String string(Charset charset) {
        ee7.b(charset, "charset");
        return new String(this.data, charset);
    }

    @DexIgnore
    public br7 substring() {
        return substring$default(this, 0, 0, 3, null);
    }

    @DexIgnore
    public br7 substring(int i) {
        return substring$default(this, i, 0, 2, null);
    }

    @DexIgnore
    public br7 substring(int i, int i2) {
        boolean z = true;
        if (i >= 0) {
            if (i2 <= getData$okio().length) {
                if (i2 - i < 0) {
                    z = false;
                }
                if (z) {
                    return (i == 0 && i2 == getData$okio().length) ? this : new br7(s97.a(getData$okio(), i, i2));
                }
                throw new IllegalArgumentException("endIndex < beginIndex".toString());
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
        throw new IllegalArgumentException("beginIndex < 0".toString());
    }

    @DexIgnore
    public br7 toAsciiLowercase() {
        byte b2;
        int i = 0;
        while (i < getData$okio().length) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 65;
            if (b3 < b4 || b3 > (b2 = (byte) 90)) {
                i++;
            } else {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) (b3 + 32);
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) (b5 + 32);
                    }
                }
                return new br7(copyOf);
            }
        }
        return this;
    }

    @DexIgnore
    public br7 toAsciiUppercase() {
        byte b2;
        int i = 0;
        while (i < getData$okio().length) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 97;
            if (b3 < b4 || b3 > (b2 = (byte) 122)) {
                i++;
            } else {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) (b3 - 32);
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) (b5 - 32);
                    }
                }
                return new br7(copyOf);
            }
        }
        return this;
    }

    @DexIgnore
    public byte[] toByteArray() {
        byte[] data$okio = getData$okio();
        byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
        ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        return copyOf;
    }

    @DexIgnore
    public String toString() {
        br7 br7;
        boolean z = true;
        if (getData$okio().length == 0) {
            return "[size=0]";
        }
        int a2 = vr7.b(getData$okio(), 64);
        if (a2 != -1) {
            String utf8 = utf8();
            if (utf8 != null) {
                String substring = utf8.substring(0, a2);
                ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                String a3 = mh7.a(mh7.a(mh7.a(substring, "\\", "\\\\", false, 4, (Object) null), "\n", "\\n", false, 4, (Object) null), "\r", "\\r", false, 4, (Object) null);
                if (a2 < utf8.length()) {
                    return "[size=" + getData$okio().length + " text=" + a3 + "\u2026]";
                }
                return "[text=" + a3 + ']';
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        } else if (getData$okio().length <= 64) {
            return "[hex=" + hex() + ']';
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[size=");
            sb.append(getData$okio().length);
            sb.append(" hex=");
            if (64 > getData$okio().length) {
                z = false;
            }
            if (z) {
                if (64 == getData$okio().length) {
                    br7 = this;
                } else {
                    br7 = new br7(s97.a(getData$okio(), 0, 64));
                }
                sb.append(br7.hex());
                sb.append("\u2026]");
                return sb.toString();
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
    }

    @DexIgnore
    public String utf8() {
        String utf8$okio = getUtf8$okio();
        if (utf8$okio != null) {
            return utf8$okio;
        }
        String a2 = uq7.a(internalArray$okio());
        setUtf8$okio(a2);
        return a2;
    }

    @DexIgnore
    public void write(OutputStream outputStream) throws IOException {
        ee7.b(outputStream, "out");
        outputStream.write(this.data);
    }

    @DexIgnore
    public void write$okio(yq7 yq7, int i, int i2) {
        ee7.b(yq7, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        vr7.a(this, yq7, i, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0032 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0030 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compareTo(com.fossil.br7 r10) {
        /*
            r9 = this;
            java.lang.String r0 = "other"
            com.fossil.ee7.b(r10, r0)
            int r0 = r9.size()
            int r1 = r10.size()
            int r2 = java.lang.Math.min(r0, r1)
            r3 = 0
            r4 = 0
        L_0x0013:
            r5 = -1
            r6 = 1
            if (r4 >= r2) goto L_0x002b
            byte r7 = r9.getByte(r4)
            r7 = r7 & 255(0xff, float:3.57E-43)
            byte r8 = r10.getByte(r4)
            r8 = r8 & 255(0xff, float:3.57E-43)
            if (r7 != r8) goto L_0x0028
            int r4 = r4 + 1
            goto L_0x0013
        L_0x0028:
            if (r7 >= r8) goto L_0x0032
            goto L_0x0030
        L_0x002b:
            if (r0 != r1) goto L_0x002e
            goto L_0x0033
        L_0x002e:
            if (r0 >= r1) goto L_0x0032
        L_0x0030:
            r3 = -1
            goto L_0x0033
        L_0x0032:
            r3 = 1
        L_0x0033:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.br7.compareTo(com.fossil.br7):int");
    }

    @DexIgnore
    public final boolean endsWith(byte[] bArr) {
        ee7.b(bArr, "suffix");
        return rangeEquals(size() - bArr.length, bArr, 0, bArr.length);
    }

    @DexIgnore
    public int indexOf(byte[] bArr, int i) {
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        int length = getData$okio().length - bArr.length;
        int max = Math.max(i, 0);
        if (max <= length) {
            while (!vq7.a(getData$okio(), max, bArr, 0, bArr.length)) {
                if (max != length) {
                    max++;
                }
            }
            return max;
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr, int i) {
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        for (int min = Math.min(i, getData$okio().length - bArr.length); min >= 0; min--) {
            if (vq7.a(getData$okio(), min, bArr, 0, bArr.length)) {
                return min;
            }
        }
        return -1;
    }

    @DexIgnore
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        ee7.b(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return i >= 0 && i <= getData$okio().length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && vq7.a(getData$okio(), i, bArr, i2, i3);
    }

    @DexIgnore
    public final boolean startsWith(byte[] bArr) {
        ee7.b(bArr, "prefix");
        return rangeEquals(0, bArr, 0, bArr.length);
    }
}
