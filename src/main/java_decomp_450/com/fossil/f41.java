package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f41 extends Exception {
    @DexIgnore
    public f41(l21 l21, String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ f41(l21 l21, String str, Throwable th, int i) {
        super((i & 2) != 0 ? null : str, (i & 4) != 0 ? null : th);
    }
}
