package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s93 extends b93 {
    @DexIgnore
    public s93() {
        super(1);
    }

    @DexIgnore
    @Override // com.fossil.b93
    public final String toString() {
        return "[SquareCap]";
    }
}
