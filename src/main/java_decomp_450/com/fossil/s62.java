package com.fossil;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s62 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends dg2 implements s62 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.s62$a$a")
        /* renamed from: com.fossil.s62$a$a  reason: collision with other inner class name */
        public static class C0163a extends eg2 implements s62 {
            @DexIgnore
            public C0163a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            @Override // com.fossil.s62
            public final Account c() throws RemoteException {
                Parcel a = a(2, zza());
                Account account = (Account) fg2.a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        @DexIgnore
        public static s62 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof s62) {
                return (s62) queryLocalInterface;
            }
            return new C0163a(iBinder);
        }
    }

    @DexIgnore
    Account c() throws RemoteException;
}
