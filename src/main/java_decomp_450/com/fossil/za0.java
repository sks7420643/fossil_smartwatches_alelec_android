package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class za0 extends dg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<za0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public za0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                    return new za0(readString, createByteArray, b.values()[parcel.readInt()]);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public za0[] newArray(int i) {
            return new za0[i];
        }
    }

    @DexIgnore
    public enum b {
        VERTICAL("vertical"),
        HORIZONTAL(MessengerShareContentUtility.IMAGE_RATIO_HORIZONTAL);
        
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            this.a = str;
        }
    }

    @DexIgnore
    public za0(String str, byte[] bArr, b bVar) {
        super(str, bArr);
        this.d = bVar;
    }

    @DexIgnore
    @Override // com.fossil.dg0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(super.a(), r51.M5, yz0.a(this.d));
    }

    @DexIgnore
    public final b f() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.dg0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
