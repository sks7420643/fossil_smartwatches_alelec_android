package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y51 extends pk1 {
    @DexIgnore
    public byte[] l; // = new byte[0];

    @DexIgnore
    public y51(qk1 qk1, cx0 cx0) {
        super(aq0.e, qk1, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.a(((pk1) this).k);
        ((pk1) this).j = true;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return (s91 instanceof mb1) && ((mb1) s91).b == ((pk1) this).k;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.d;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.l = ((mb1) s91).c;
    }
}
