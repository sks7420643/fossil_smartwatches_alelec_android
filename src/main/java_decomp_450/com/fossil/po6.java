package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po6 implements MembersInjector<no6> {
    @DexIgnore
    public static void a(no6 no6, tn5 tn5) {
        no6.g = tn5;
    }

    @DexIgnore
    public static void a(no6 no6, wn5 wn5) {
        no6.h = wn5;
    }

    @DexIgnore
    public static void a(no6 no6, rn5 rn5) {
        no6.i = rn5;
    }

    @DexIgnore
    public static void a(no6 no6, yl5 yl5) {
        no6.j = yl5;
    }

    @DexIgnore
    public static void a(no6 no6, ad5 ad5) {
        no6.k = ad5;
    }

    @DexIgnore
    public static void a(no6 no6, UserRepository userRepository) {
        no6.l = userRepository;
    }

    @DexIgnore
    public static void a(no6 no6, DeviceRepository deviceRepository) {
        no6.m = deviceRepository;
    }

    @DexIgnore
    public static void a(no6 no6, ch5 ch5) {
        no6.n = ch5;
    }

    @DexIgnore
    public static void a(no6 no6, in5 in5) {
        no6.o = in5;
    }

    @DexIgnore
    public static void a(no6 no6, jn5 jn5) {
        no6.p = jn5;
    }

    @DexIgnore
    public static void a(no6 no6, rl4 rl4) {
        no6.q = rl4;
    }

    @DexIgnore
    public static void a(no6 no6, kn5 kn5) {
        no6.r = kn5;
    }

    @DexIgnore
    public static void a(no6 no6, ln5 ln5) {
        no6.s = ln5;
    }

    @DexIgnore
    public static void a(no6 no6, bn5 bn5) {
        no6.t = bn5;
    }

    @DexIgnore
    public static void a(no6 no6, an5 an5) {
        no6.u = an5;
    }

    @DexIgnore
    public static void a(no6 no6, un5 un5) {
        no6.v = un5;
    }

    @DexIgnore
    public static void a(no6 no6, lh5 lh5) {
        no6.w = lh5;
    }

    @DexIgnore
    public static void a(no6 no6, vn5 vn5) {
        no6.x = vn5;
    }

    @DexIgnore
    public static void a(no6 no6, yn5 yn5) {
        no6.y = yn5;
    }

    @DexIgnore
    public static void a(no6 no6, xn5 xn5) {
        no6.z = xn5;
    }

    @DexIgnore
    public static void a(no6 no6, yv6 yv6) {
        no6.A = yv6;
    }

    @DexIgnore
    public static void a(no6 no6, qd5 qd5) {
        no6.B = qd5;
    }

    @DexIgnore
    public static void a(no6 no6, SummariesRepository summariesRepository) {
        no6.C = summariesRepository;
    }

    @DexIgnore
    public static void a(no6 no6, SleepSummariesRepository sleepSummariesRepository) {
        no6.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(no6 no6, GoalTrackingRepository goalTrackingRepository) {
        no6.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(no6 no6, ym5 ym5) {
        no6.F = ym5;
    }

    @DexIgnore
    public static void a(no6 no6, zm5 zm5) {
        no6.G = zm5;
    }

    @DexIgnore
    public static void a(no6 no6, fw6 fw6) {
        no6.H = fw6;
    }

    @DexIgnore
    public static void a(no6 no6, WatchLocalizationRepository watchLocalizationRepository) {
        no6.I = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(no6 no6, AlarmsRepository alarmsRepository) {
        no6.J = alarmsRepository;
    }

    @DexIgnore
    public static void a(no6 no6, fp4 fp4) {
        no6.K = fp4;
    }

    @DexIgnore
    public static void a(no6 no6, to4 to4) {
        no6.L = to4;
    }

    @DexIgnore
    public static void a(no6 no6, lm4 lm4) {
        no6.M = lm4;
    }

    @DexIgnore
    public static void a(no6 no6, WorkoutSettingRepository workoutSettingRepository) {
        no6.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void a(no6 no6) {
        no6.G();
    }
}
