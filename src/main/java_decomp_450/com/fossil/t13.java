package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t13 implements tr2<s13> {
    @DexIgnore
    public static t13 b; // = new t13();
    @DexIgnore
    public /* final */ tr2<s13> a;

    @DexIgnore
    public t13(tr2<s13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((s13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((s13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ s13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public t13() {
        this(sr2.a(new v13()));
    }
}
