package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv1 {
    @DexIgnore
    public static String a(String str) {
        return "TransportRuntime." + str;
    }

    @DexIgnore
    public static void b(String str, String str2, Object obj) {
        Log.w(a(str), String.format(str2, obj));
    }

    @DexIgnore
    public static void a(String str, String str2, Object obj) {
        Log.d(a(str), String.format(str2, obj));
    }

    @DexIgnore
    public static void a(String str, String str2, Object... objArr) {
        Log.d(a(str), String.format(str2, objArr));
    }

    @DexIgnore
    public static void a(String str, String str2) {
        Log.i(a(str), str2);
    }

    @DexIgnore
    public static void a(String str, String str2, Throwable th) {
        Log.e(a(str), str2, th);
    }
}
