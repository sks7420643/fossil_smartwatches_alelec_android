package com.fossil;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dh3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xq2 a;
    @DexIgnore
    public /* final */ /* synthetic */ ServiceConnection b;
    @DexIgnore
    public /* final */ /* synthetic */ eh3 c;

    @DexIgnore
    public dh3(eh3 eh3, xq2 xq2, ServiceConnection serviceConnection) {
        this.c = eh3;
        this.a = xq2;
        this.b = serviceConnection;
    }

    @DexIgnore
    public final void run() {
        eh3 eh3 = this.c;
        bh3 bh3 = eh3.b;
        String a2 = eh3.a;
        xq2 xq2 = this.a;
        ServiceConnection serviceConnection = this.b;
        Bundle a3 = bh3.a(a2, xq2);
        bh3.a.c().g();
        if (a3 != null) {
            long j = a3.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                bh3.a.e().w().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a3.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    bh3.a.e().t().a("No referrer defined in Install Referrer response");
                } else {
                    bh3.a.e().B().a("InstallReferrer API result", string);
                    jm3 v = bh3.a.v();
                    String valueOf = String.valueOf(string);
                    Bundle a4 = v.a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a4 == null) {
                        bh3.a.e().t().a("No campaign params defined in Install Referrer result");
                    } else {
                        String string2 = a4.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a3.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                bh3.a.e().t().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a4.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == bh3.a.p().k.a()) {
                            bh3.a.b();
                            bh3.a.e().B().a("Install Referrer campaign has already been logged");
                        } else if (!h13.a() || !bh3.a.o().a(wb3.B0) || bh3.a.g()) {
                            bh3.a.p().k.a(j);
                            bh3.a.b();
                            bh3.a.e().B().a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a4.putString("_cis", "referrer API");
                            bh3.a.u().a("auto", "_cmp", a4);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            e92.a().a(bh3.a.f(), serviceConnection);
        }
    }
}
