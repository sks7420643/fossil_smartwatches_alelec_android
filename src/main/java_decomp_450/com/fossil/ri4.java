package com.fossil;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri4 implements sg4 {
    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        int i3;
        int i4;
        if (mg4 == mg4.PDF_417) {
            wi4 wi4 = new wi4();
            int i5 = 30;
            int i6 = 2;
            if (map != null) {
                if (map.containsKey(og4.PDF417_COMPACT)) {
                    wi4.a(Boolean.valueOf(map.get(og4.PDF417_COMPACT).toString()).booleanValue());
                }
                if (map.containsKey(og4.PDF417_COMPACTION)) {
                    wi4.a(ui4.valueOf(map.get(og4.PDF417_COMPACTION).toString()));
                }
                if (map.containsKey(og4.PDF417_DIMENSIONS)) {
                    vi4 vi4 = (vi4) map.get(og4.PDF417_DIMENSIONS);
                    wi4.a(vi4.a(), vi4.c(), vi4.b(), vi4.d());
                }
                if (map.containsKey(og4.MARGIN)) {
                    i5 = Integer.parseInt(map.get(og4.MARGIN).toString());
                }
                if (map.containsKey(og4.ERROR_CORRECTION)) {
                    i6 = Integer.parseInt(map.get(og4.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(og4.CHARACTER_SET)) {
                    wi4.a(Charset.forName(map.get(og4.CHARACTER_SET).toString()));
                }
                i3 = i5;
                i4 = i6;
            } else {
                i4 = 2;
                i3 = 30;
            }
            return a(wi4, str, i4, i, i2, i3);
        }
        throw new IllegalArgumentException("Can only encode PDF_417, but got " + mg4);
    }

    @DexIgnore
    public static dh4 a(wi4 wi4, String str, int i, int i2, int i3, int i4) throws tg4 {
        boolean z;
        wi4.a(str, i);
        byte[][] a = wi4.a().a(1, 4);
        if ((i3 > i2) ^ (a[0].length < a.length)) {
            a = a(a);
            z = true;
        } else {
            z = false;
        }
        int length = i2 / a[0].length;
        int length2 = i3 / a.length;
        if (length >= length2) {
            length = length2;
        }
        if (length <= 1) {
            return a(a, i4);
        }
        byte[][] a2 = wi4.a().a(length, length << 2);
        if (z) {
            a2 = a(a2);
        }
        return a(a2, i4);
    }

    @DexIgnore
    public static dh4 a(byte[][] bArr, int i) {
        int i2 = i * 2;
        dh4 dh4 = new dh4(bArr[0].length + i2, bArr.length + i2);
        dh4.d();
        int f = (dh4.f() - i) - 1;
        int i3 = 0;
        while (i3 < bArr.length) {
            for (int i4 = 0; i4 < bArr[0].length; i4++) {
                if (bArr[i3][i4] == 1) {
                    dh4.b(i4 + i, f);
                }
            }
            i3++;
            f--;
        }
        return dh4;
    }

    @DexIgnore
    public static byte[][] a(byte[][] bArr) {
        int length = bArr[0].length;
        int[] iArr = new int[2];
        iArr[1] = bArr.length;
        iArr[0] = length;
        byte[][] bArr2 = (byte[][]) Array.newInstance(byte.class, iArr);
        for (int i = 0; i < bArr.length; i++) {
            int length2 = (bArr.length - i) - 1;
            for (int i2 = 0; i2 < bArr[0].length; i2++) {
                bArr2[i2][length2] = bArr[i][i2];
            }
        }
        return bArr2;
    }
}
