package com.fossil;

import com.fossil.tt1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt1 extends tt1 {
    @DexIgnore
    public /* final */ tt1.b a;
    @DexIgnore
    public /* final */ jt1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tt1.a {
        @DexIgnore
        public tt1.b a;
        @DexIgnore
        public jt1 b;

        @DexIgnore
        @Override // com.fossil.tt1.a
        public tt1.a a(tt1.b bVar) {
            this.a = bVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tt1.a
        public tt1.a a(jt1 jt1) {
            this.b = jt1;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.tt1.a
        public tt1 a() {
            return new nt1(this.a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ nt1(tt1.b bVar, jt1 jt1, a aVar) {
        this.a = bVar;
        this.b = jt1;
    }

    @DexIgnore
    @Override // com.fossil.tt1
    public jt1 a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.tt1
    public tt1.b b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof tt1)) {
            return false;
        }
        tt1.b bVar = this.a;
        if (bVar != null ? bVar.equals(((nt1) obj).a) : ((nt1) obj).a == null) {
            jt1 jt1 = this.b;
            if (jt1 == null) {
                if (((nt1) obj).b == null) {
                    return true;
                }
            } else if (jt1.equals(((nt1) obj).b)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        tt1.b bVar = this.a;
        int i = 0;
        int hashCode = ((bVar == null ? 0 : bVar.hashCode()) ^ 1000003) * 1000003;
        jt1 jt1 = this.b;
        if (jt1 != null) {
            i = jt1.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "ClientInfo{clientType=" + this.a + ", androidClientInfo=" + this.b + "}";
    }
}
