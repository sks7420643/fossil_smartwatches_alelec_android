package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pl0 implements mw0 {
    SUCCESS((byte) 0),
    UNKNOWN((byte) 1);
    
    @DexIgnore
    public static /* final */ rj0 f; // = new rj0(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public pl0(byte b2) {
        this.b = b2;
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public boolean a() {
        return this == SUCCESS;
    }

    @DexIgnore
    @Override // com.fossil.mw0
    public String getLogName() {
        return this.a;
    }
}
