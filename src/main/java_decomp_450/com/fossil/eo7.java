package com.fossil;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo7 {
    @DexIgnore
    public /* final */ oo7 a;
    @DexIgnore
    public /* final */ tn7 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public eo7(oo7 oo7, tn7 tn7, List<Certificate> list, List<Certificate> list2) {
        this.a = oo7;
        this.b = tn7;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static eo7 a(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            tn7 a2 = tn7.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                oo7 forJavaName = oo7.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException unused) {
                    certificateArr = null;
                }
                if (certificateArr != null) {
                    list = ro7.a(certificateArr);
                } else {
                    list = Collections.emptyList();
                }
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                if (localCertificates != null) {
                    list2 = ro7.a(localCertificates);
                } else {
                    list2 = Collections.emptyList();
                }
                return new eo7(forJavaName, a2, list, list2);
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public List<Certificate> b() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> c() {
        return this.c;
    }

    @DexIgnore
    public oo7 d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof eo7)) {
            return false;
        }
        eo7 eo7 = (eo7) obj;
        if (!this.a.equals(eo7.a) || !this.b.equals(eo7.b) || !this.c.equals(eo7.c) || !this.d.equals(eo7.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public static eo7 a(oo7 oo7, tn7 tn7, List<Certificate> list, List<Certificate> list2) {
        if (oo7 == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (tn7 != null) {
            return new eo7(oo7, tn7, ro7.a(list), ro7.a(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public tn7 a() {
        return this.b;
    }
}
