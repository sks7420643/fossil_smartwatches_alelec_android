package com.fossil;

import com.fossil.ew6;
import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp6 extends pp6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ qp6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ ew6 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return tp6.i;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<ew6.d, ew6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser a;
        @DexIgnore
        public /* final */ /* synthetic */ tp6 b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public b(MFUser mFUser, tp6 tp6, boolean z) {
            this.a = mFUser;
            this.b = tp6;
            this.c = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ew6.d dVar) {
            ee7.b(dVar, "responseValue");
            this.b.a(this.a, this.c);
        }

        @DexIgnore
        public void a(ew6.b bVar) {
            ee7.b(bVar, "errorValue");
            this.b.a(this.a, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tp6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository c = this.this$0.this$0.g;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.updateUser(mFUser, true, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(tp6 tp6, MFUser mFUser, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tp6;
            this.$user = mFUser;
            this.$isDefaultValuesUsed = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$user, this.$isDefaultValuesUsed, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.$user.getUseDefaultBiometric()) {
                    this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
                }
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.f();
            this.this$0.f.b0();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tp6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository c = this.this$0.this$0.g;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(tp6 tp6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tp6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L_0x0020
                if (r1 != r2) goto L_0x0018
                java.lang.Object r0 = r6.L$1
                com.fossil.tp6 r0 = (com.fossil.tp6) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0046
            L_0x0018:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0020:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r7 = r6.p$
                com.fossil.tp6 r1 = r6.this$0
                com.portfolio.platform.data.model.MFUser r1 = r1.e
                if (r1 != 0) goto L_0x004b
                com.fossil.tp6 r1 = r6.this$0
                com.fossil.ti7 r4 = r1.b()
                com.fossil.tp6$d$a r5 = new com.fossil.tp6$d$a
                r5.<init>(r6, r3)
                r6.L$0 = r7
                r6.L$1 = r1
                r6.label = r2
                java.lang.Object r7 = com.fossil.vh7.a(r4, r5, r6)
                if (r7 != r0) goto L_0x0045
                return r0
            L_0x0045:
                r0 = r1
            L_0x0046:
                com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
                r0.e = r7
            L_0x004b:
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.tp6$a r0 = com.fossil.tp6.j
                java.lang.String r0 = r0.a()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "start with currentUser="
                r1.append(r2)
                com.fossil.tp6 r2 = r6.this$0
                com.portfolio.platform.data.model.MFUser r2 = r2.e
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r7.d(r0, r1)
                com.fossil.tp6 r7 = r6.this$0
                com.portfolio.platform.data.model.MFUser r7 = r7.e
                if (r7 == 0) goto L_0x0133
                int r0 = r7.getHeightInCentimeters()
                if (r0 == 0) goto L_0x0085
                int r0 = r7.getWeightInGrams()
                if (r0 != 0) goto L_0x00c9
            L_0x0085:
                com.fossil.ud5 r0 = com.fossil.ud5.a
                com.fossil.fb5$a r1 = com.fossil.fb5.Companion
                java.lang.String r2 = r7.getGender()
                if (r2 == 0) goto L_0x012f
                com.fossil.fb5 r1 = r1.a(r2)
                java.lang.String r2 = r7.getBirthday()
                if (r2 == 0) goto L_0x012b
                int r2 = r7.getAge(r2)
                com.fossil.r87 r0 = r0.a(r1, r2)
                int r1 = r7.getHeightInCentimeters()
                if (r1 != 0) goto L_0x00b4
                java.lang.Object r1 = r0.getFirst()
                java.lang.Number r1 = (java.lang.Number) r1
                int r1 = r1.intValue()
                r7.setHeightInCentimeters(r1)
            L_0x00b4:
                int r1 = r7.getWeightInGrams()
                if (r1 != 0) goto L_0x00c9
                java.lang.Object r0 = r0.getSecond()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r0 = r0 * 1000
                r7.setWeightInGrams(r0)
            L_0x00c9:
                int r0 = r7.getHeightInCentimeters()
                if (r0 <= 0) goto L_0x00fa
                com.fossil.tp6 r0 = r6.this$0
                com.fossil.qp6 r0 = r0.f
                int r1 = r7.getHeightInCentimeters()
                com.portfolio.platform.data.model.MFUser$UnitGroup r2 = r7.getUnitGroup()
                if (r2 == 0) goto L_0x00f6
                java.lang.String r2 = r2.getHeight()
                if (r2 == 0) goto L_0x00f2
                com.fossil.ob5 r2 = com.fossil.ob5.fromString(r2)
                java.lang.String r4 = "Unit.fromString(it.unitGroup!!.height!!)"
                com.fossil.ee7.a(r2, r4)
                r0.a(r1, r2)
                goto L_0x00fa
            L_0x00f2:
                com.fossil.ee7.a()
                throw r3
            L_0x00f6:
                com.fossil.ee7.a()
                throw r3
            L_0x00fa:
                int r0 = r7.getWeightInGrams()
                if (r0 <= 0) goto L_0x0133
                com.fossil.tp6 r0 = r6.this$0
                com.fossil.qp6 r0 = r0.f
                int r1 = r7.getWeightInGrams()
                com.portfolio.platform.data.model.MFUser$UnitGroup r7 = r7.getUnitGroup()
                if (r7 == 0) goto L_0x0127
                java.lang.String r7 = r7.getWeight()
                if (r7 == 0) goto L_0x0123
                com.fossil.ob5 r7 = com.fossil.ob5.fromString(r7)
                java.lang.String r2 = "Unit.fromString(it.unitGroup!!.weight!!)"
                com.fossil.ee7.a(r7, r2)
                r0.b(r1, r7)
                goto L_0x0133
            L_0x0123:
                com.fossil.ee7.a()
                throw r3
            L_0x0127:
                com.fossil.ee7.a()
                throw r3
            L_0x012b:
                com.fossil.ee7.a()
                throw r3
            L_0x012f:
                com.fossil.ee7.a()
                throw r3
            L_0x0133:
                com.fossil.tp6 r7 = r6.this$0
                com.fossil.qp6 r7 = r7.f
                r7.e()
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.tp6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = tp6.class.getSimpleName();
        ee7.a((Object) simpleName, "OnboardingHeightWeightPr\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public tp6(qp6 qp6, UserRepository userRepository, ew6 ew6) {
        ee7.b(qp6, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(ew6, "mGetRecommendedGoalUseCase");
        this.f = qp6;
        this.g = userRepository;
        this.h = ew6;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.pp6
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onWeightChanged weightInGram=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.pp6
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onHeightChanged heightInCentimeters=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.pp6
    public void b(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitWeightChanged unit=" + ob5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setWeight(value);
                qp6 qp6 = this.f;
                int weightInGrams = mFUser.getWeightInGrams();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String weight = unitGroup2.getWeight();
                    if (weight != null) {
                        ob5 fromString = ob5.fromString(weight);
                        ee7.a((Object) fromString, "Unit.fromString(it.unitGroup!!.weight!!)");
                        qp6.b(weightInGrams, fromString);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pp6
    public void a(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitHeightChanged unit=" + ob5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setHeight(value);
                qp6 qp6 = this.f;
                int heightInCentimeters = mFUser.getHeightInCentimeters();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String height = unitGroup2.getHeight();
                    if (height != null) {
                        ob5 fromString = ob5.fromString(height);
                        ee7.a((Object) fromString, "Unit.fromString(it.unitGroup!!.height!!)");
                        qp6.a(heightInCentimeters, fromString);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pp6
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "completeOnboarding currentUser=" + this.e);
        this.f.g();
        MFUser mFUser = this.e;
        if (mFUser != null) {
            String birthday = mFUser.getBirthday();
            String str2 = null;
            if (birthday != null) {
                int age = mFUser.getAge(birthday);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                StringBuilder sb = new StringBuilder();
                sb.append("completeOnboarding update heightUnit=");
                MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                sb.append(unitGroup != null ? unitGroup.getHeight() : null);
                sb.append(", weightUnit=");
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    str2 = unitGroup2.getWeight();
                }
                sb.append(str2);
                sb.append(',');
                sb.append(" height=");
                sb.append(mFUser.getHeightInCentimeters());
                sb.append(", weight=");
                sb.append(mFUser.getWeightInGrams());
                local2.d(str3, sb.toString());
                this.h.a(new ew6.c(age, mFUser.getHeightInCentimeters(), mFUser.getWeightInGrams(), fb5.Companion.a(mFUser.getGender())), new b(mFUser, this, z));
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(MFUser mFUser, boolean z) {
        ee7.b(mFUser, "user");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onSetUpHeightWeightComplete register date: " + mFUser.getRegisterDate());
        ik7 unused = xh7.b(e(), null, null, new c(this, mFUser, z, null), 3, null);
    }
}
