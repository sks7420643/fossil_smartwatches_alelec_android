package com.fossil;

import com.fossil.dv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu1 extends dv1 {
    @DexIgnore
    public /* final */ dv1.a a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public yu1(dv1.a aVar, long j) {
        if (aVar != null) {
            this.a = aVar;
            this.b = j;
            return;
        }
        throw new NullPointerException("Null status");
    }

    @DexIgnore
    @Override // com.fossil.dv1
    public long a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.dv1
    public dv1.a b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof dv1)) {
            return false;
        }
        dv1 dv1 = (dv1) obj;
        if (!this.a.equals(dv1.b()) || this.b != dv1.a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "BackendResponse{status=" + this.a + ", nextRequestWaitMillis=" + this.b + "}";
    }
}
