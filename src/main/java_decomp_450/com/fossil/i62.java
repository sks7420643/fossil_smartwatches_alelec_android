package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i62 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<i62> CREATOR; // = new z72();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public i62(int i, String str) {
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof i62)) {
            i62 i62 = (i62) obj;
            return i62.a == this.a && y62.a(i62.b, this.b);
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        int i = this.a;
        String str = this.b;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b, false);
        k72.a(parcel, a2);
    }
}
