package com.fossil;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ag6 extends dl4<zf6> {
    @DexIgnore
    void a(int i, List<fz6> list, List<v87<Integer, r87<Integer, Float>, String>> list2);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void a(boolean z, ob5 ob5, qf<WorkoutSession> qfVar);

    @DexIgnore
    void c(int i, int i2);
}
