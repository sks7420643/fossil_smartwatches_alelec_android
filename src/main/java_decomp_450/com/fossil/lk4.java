package com.fossil;

import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk4 implements Factory<GuestApiService> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public lk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static lk4 a(wj4 wj4) {
        return new lk4(wj4);
    }

    @DexIgnore
    public static GuestApiService b(wj4 wj4) {
        GuestApiService i = wj4.i();
        c87.a(i, "Cannot return null from a non-@Nullable @Provides method");
        return i;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public GuestApiService get() {
        return b(this.a);
    }
}
