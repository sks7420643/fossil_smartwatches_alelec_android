package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.h5;
import com.fossil.i5;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d5 {
    @DexIgnore
    public static void a(j5 j5Var) {
        if ((j5Var.N() & 32) != 32) {
            b(j5Var);
            return;
        }
        j5Var.D0 = true;
        j5Var.x0 = false;
        j5Var.y0 = false;
        j5Var.z0 = false;
        ArrayList<i5> arrayList = ((t5) j5Var).k0;
        List<k5> list = j5Var.w0;
        boolean z = j5Var.k() == i5.b.WRAP_CONTENT;
        boolean z2 = j5Var.r() == i5.b.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (i5 i5Var : arrayList) {
            i5Var.p = null;
            i5Var.d0 = false;
            i5Var.G();
        }
        for (i5 i5Var2 : arrayList) {
            if (i5Var2.p == null && !a(i5Var2, list, z3)) {
                b(j5Var);
                j5Var.D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (k5 k5Var : list) {
            i = Math.max(i, a(k5Var, 0));
            i2 = Math.max(i2, a(k5Var, 1));
        }
        if (z) {
            j5Var.a(i5.b.FIXED);
            j5Var.p(i);
            j5Var.x0 = true;
            j5Var.y0 = true;
            j5Var.A0 = i;
        }
        if (z2) {
            j5Var.b(i5.b.FIXED);
            j5Var.h(i2);
            j5Var.x0 = true;
            j5Var.z0 = true;
            j5Var.B0 = i2;
        }
        a(list, 0, j5Var.t());
        a(list, 1, j5Var.j());
    }

    @DexIgnore
    public static void b(j5 j5Var) {
        j5Var.w0.clear();
        j5Var.w0.add(0, new k5(((t5) j5Var).k0));
    }

    @DexIgnore
    public static boolean a(i5 i5Var, List<k5> list, boolean z) {
        k5 k5Var = new k5(new ArrayList(), true);
        list.add(k5Var);
        return a(i5Var, k5Var, list, z);
    }

    @DexIgnore
    public static boolean a(i5 i5Var, k5 k5Var, List<k5> list, boolean z) {
        h5 h5Var;
        h5 h5Var2;
        h5 h5Var3;
        i5 i5Var2;
        h5 h5Var4;
        h5 h5Var5;
        h5 h5Var6;
        h5 h5Var7;
        i5 i5Var3;
        h5 h5Var8;
        if (i5Var == null) {
            return true;
        }
        i5Var.c0 = false;
        j5 j5Var = (j5) i5Var.l();
        k5 k5Var2 = i5Var.p;
        if (k5Var2 == null) {
            i5Var.b0 = true;
            k5Var.a.add(i5Var);
            i5Var.p = k5Var;
            if (i5Var.s.d == null && i5Var.u.d == null && i5Var.t.d == null && i5Var.v.d == null && i5Var.w.d == null && i5Var.z.d == null) {
                a(j5Var, i5Var, k5Var);
                if (z) {
                    return false;
                }
            }
            if (!(i5Var.t.d == null || i5Var.v.d == null)) {
                j5Var.r();
                i5.b bVar = i5.b.WRAP_CONTENT;
                if (z) {
                    a(j5Var, i5Var, k5Var);
                    return false;
                } else if (!(i5Var.t.d.b == i5Var.l() && i5Var.v.d.b == i5Var.l())) {
                    a(j5Var, i5Var, k5Var);
                }
            }
            if (!(i5Var.s.d == null || i5Var.u.d == null)) {
                j5Var.k();
                i5.b bVar2 = i5.b.WRAP_CONTENT;
                if (z) {
                    a(j5Var, i5Var, k5Var);
                    return false;
                } else if (!(i5Var.s.d.b == i5Var.l() && i5Var.u.d.b == i5Var.l())) {
                    a(j5Var, i5Var, k5Var);
                }
            }
            if (((i5Var.k() == i5.b.MATCH_CONSTRAINT) ^ (i5Var.r() == i5.b.MATCH_CONSTRAINT)) && i5Var.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                a(i5Var);
            } else if (i5Var.k() == i5.b.MATCH_CONSTRAINT || i5Var.r() == i5.b.MATCH_CONSTRAINT) {
                a(j5Var, i5Var, k5Var);
                if (z) {
                    return false;
                }
            }
            if (((i5Var.s.d == null && i5Var.u.d == null) || (((h5Var5 = i5Var.s.d) != null && h5Var5.b == i5Var.D && i5Var.u.d == null) || (((h5Var6 = i5Var.u.d) != null && h5Var6.b == i5Var.D && i5Var.s.d == null) || ((h5Var7 = i5Var.s.d) != null && h5Var7.b == (i5Var3 = i5Var.D) && (h5Var8 = i5Var.u.d) != null && h5Var8.b == i5Var3)))) && i5Var.z.d == null && !(i5Var instanceof l5) && !(i5Var instanceof m5)) {
                k5Var.f.add(i5Var);
            }
            if (((i5Var.t.d == null && i5Var.v.d == null) || (((h5Var = i5Var.t.d) != null && h5Var.b == i5Var.D && i5Var.v.d == null) || (((h5Var2 = i5Var.v.d) != null && h5Var2.b == i5Var.D && i5Var.t.d == null) || ((h5Var3 = i5Var.t.d) != null && h5Var3.b == (i5Var2 = i5Var.D) && (h5Var4 = i5Var.v.d) != null && h5Var4.b == i5Var2)))) && i5Var.z.d == null && i5Var.w.d == null && !(i5Var instanceof l5) && !(i5Var instanceof m5)) {
                k5Var.g.add(i5Var);
            }
            if (i5Var instanceof m5) {
                a(j5Var, i5Var, k5Var);
                if (z) {
                    return false;
                }
                m5 m5Var = (m5) i5Var;
                for (int i = 0; i < m5Var.l0; i++) {
                    if (!a(m5Var.k0[i], k5Var, list, z)) {
                        return false;
                    }
                }
            }
            int length = i5Var.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                h5 h5Var9 = i5Var.A[i2];
                h5 h5Var10 = h5Var9.d;
                if (!(h5Var10 == null || h5Var10.b == i5Var.l())) {
                    if (h5Var9.c == h5.d.CENTER) {
                        a(j5Var, i5Var, k5Var);
                        if (z) {
                            return false;
                        }
                    } else {
                        a(h5Var9);
                    }
                    if (!a(h5Var9.d.b, k5Var, list, z)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (k5Var2 != k5Var) {
            k5Var.a.addAll(k5Var2.a);
            k5Var.f.addAll(i5Var.p.f);
            k5Var.g.addAll(i5Var.p.g);
            if (!i5Var.p.d) {
                k5Var.d = false;
            }
            list.remove(i5Var.p);
            for (i5 i5Var4 : i5Var.p.a) {
                i5Var4.p = k5Var;
            }
        }
        return true;
    }

    @DexIgnore
    public static void a(j5 j5Var, i5 i5Var, k5 k5Var) {
        k5Var.d = false;
        j5Var.D0 = false;
        i5Var.b0 = false;
    }

    @DexIgnore
    public static int a(k5 k5Var, int i) {
        int i2 = i * 2;
        List<i5> a = k5Var.a(i);
        int size = a.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            i5 i5Var = a.get(i4);
            h5[] h5VarArr = i5Var.A;
            int i5 = i2 + 1;
            i3 = Math.max(i3, a(i5Var, i, h5VarArr[i5].d == null || !(h5VarArr[i2].d == null || h5VarArr[i5].d == null), 0));
        }
        k5Var.e[i] = i3;
        return i3;
    }

    @DexIgnore
    public static int a(i5 i5Var, int i, boolean z, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int t;
        int i8;
        int i9;
        int i10;
        int i11 = 0;
        if (!i5Var.b0) {
            return 0;
        }
        boolean z2 = i5Var.w.d != null && i == 1;
        if (z) {
            i6 = i5Var.d();
            i5 = i5Var.j() - i5Var.d();
            i4 = i * 2;
            i3 = i4 + 1;
        } else {
            i6 = i5Var.j() - i5Var.d();
            i5 = i5Var.d();
            i3 = i * 2;
            i4 = i3 + 1;
        }
        h5[] h5VarArr = i5Var.A;
        if (h5VarArr[i3].d == null || h5VarArr[i4].d != null) {
            i7 = 1;
        } else {
            i7 = -1;
            i3 = i4;
            i4 = i3;
        }
        int i12 = z2 ? i2 - i6 : i2;
        int b = (i5Var.A[i4].b() * i7) + a(i5Var, i);
        int i13 = i12 + b;
        int t2 = (i == 0 ? i5Var.t() : i5Var.j()) * i7;
        Iterator<r5> it = ((r5) i5Var.A[i4].d()).a.iterator();
        while (it.hasNext()) {
            i11 = Math.max(i11, a(((p5) it.next()).c.b, i, z, i13));
        }
        int i14 = 0;
        for (Iterator<r5> it2 = ((r5) i5Var.A[i3].d()).a.iterator(); it2.hasNext(); it2 = it2) {
            i14 = Math.max(i14, a(((p5) it2.next()).c.b, i, z, t2 + i13));
        }
        if (z2) {
            i11 -= i6;
            t = i14 + i5;
        } else {
            t = i14 + ((i == 0 ? i5Var.t() : i5Var.j()) * i7);
        }
        int i15 = 1;
        if (i == 1) {
            Iterator<r5> it3 = ((r5) i5Var.w.d()).a.iterator();
            int i16 = 0;
            while (it3.hasNext()) {
                p5 p5Var = (p5) it3.next();
                if (i7 == i15) {
                    i16 = Math.max(i16, a(p5Var.c.b, i, z, i6 + i13));
                    i10 = i3;
                } else {
                    i10 = i3;
                    i16 = Math.max(i16, a(p5Var.c.b, i, z, (i5 * i7) + i13));
                }
                it3 = it3;
                i3 = i10;
                i15 = 1;
            }
            i8 = i3;
            i9 = (((r5) i5Var.w.d()).a.size() <= 0 || z2) ? i16 : i7 == 1 ? i16 + i6 : i16 - i5;
        } else {
            i8 = i3;
            i9 = 0;
        }
        int max = b + Math.max(i11, Math.max(t, i9));
        int i17 = t2 + i13;
        if (i7 == -1) {
            i17 = i13;
            i13 = i17;
        }
        if (z) {
            n5.a(i5Var, i, i13);
            i5Var.a(i13, i17, i);
        } else {
            i5Var.p.a(i5Var, i);
            i5Var.d(i13, i);
        }
        if (i5Var.c(i) == i5.b.MATCH_CONSTRAINT && i5Var.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            i5Var.p.a(i5Var, i);
        }
        h5[] h5VarArr2 = i5Var.A;
        if (!(h5VarArr2[i4].d == null || h5VarArr2[i8].d == null)) {
            i5 l = i5Var.l();
            h5[] h5VarArr3 = i5Var.A;
            if (h5VarArr3[i4].d.b == l && h5VarArr3[i8].d.b == l) {
                i5Var.p.a(i5Var, i);
            }
        }
        return max;
    }

    @DexIgnore
    public static void a(h5 h5Var) {
        p5 d = h5Var.d();
        h5 h5Var2 = h5Var.d;
        if (h5Var2 != null && h5Var2.d != h5Var) {
            h5Var2.d().a(d);
        }
    }

    @DexIgnore
    public static void a(List<k5> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (i5 i5Var : list.get(i3).b(i)) {
                if (i5Var.b0) {
                    a(i5Var, i, i2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(i5 i5Var, int i, int i2) {
        int i3 = i * 2;
        h5[] h5VarArr = i5Var.A;
        h5 h5Var = h5VarArr[i3];
        h5 h5Var2 = h5VarArr[i3 + 1];
        if ((h5Var.d == null || h5Var2.d == null) ? false : true) {
            n5.a(i5Var, i, a(i5Var, i) + h5Var.b());
        } else if (i5Var.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || i5Var.c(i) != i5.b.MATCH_CONSTRAINT) {
            int e = i2 - i5Var.e(i);
            int d = e - i5Var.d(i);
            i5Var.a(d, e, i);
            n5.a(i5Var, i, d);
        } else {
            int a = a(i5Var);
            int i4 = (int) i5Var.A[i3].d().g;
            h5Var2.d().f = h5Var.d();
            h5Var2.d().g = (float) a;
            ((r5) h5Var2.d()).b = 1;
            i5Var.a(i4, i4 + a, i);
        }
    }

    @DexIgnore
    public static int a(i5 i5Var, int i) {
        i5 i5Var2;
        h5 h5Var;
        int i2 = i * 2;
        h5[] h5VarArr = i5Var.A;
        h5 h5Var2 = h5VarArr[i2];
        h5 h5Var3 = h5VarArr[i2 + 1];
        h5 h5Var4 = h5Var2.d;
        if (h5Var4 == null || h5Var4.b != (i5Var2 = i5Var.D) || (h5Var = h5Var3.d) == null || h5Var.b != i5Var2) {
            return 0;
        }
        return (int) (((float) (((i5Var2.d(i) - h5Var2.b()) - h5Var3.b()) - i5Var.d(i))) * (i == 0 ? i5Var.V : i5Var.W));
    }

    @DexIgnore
    public static int a(i5 i5Var) {
        float f;
        float f2;
        if (i5Var.k() == i5.b.MATCH_CONSTRAINT) {
            if (i5Var.H == 0) {
                f2 = ((float) i5Var.j()) * i5Var.G;
            } else {
                f2 = ((float) i5Var.j()) / i5Var.G;
            }
            int i = (int) f2;
            i5Var.p(i);
            return i;
        } else if (i5Var.r() != i5.b.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (i5Var.H == 1) {
                f = ((float) i5Var.t()) * i5Var.G;
            } else {
                f = ((float) i5Var.t()) / i5Var.G;
            }
            int i2 = (int) f;
            i5Var.h(i2);
            return i2;
        }
    }
}
