package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ml2 extends el2 implements kl2 {
    @DexIgnore
    public ml2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    @Override // com.fossil.kl2
    public final void a(hl2 hl2) throws RemoteException {
        Parcel E = E();
        jm2.a(E, hl2);
        c(1, E);
    }
}
