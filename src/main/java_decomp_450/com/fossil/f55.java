package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f55 extends e55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362163, 1);
        E.put(2131362636, 2);
        E.put(2131362613, 3);
        E.put(2131362230, 4);
        E.put(2131362612, 5);
        E.put(2131362229, 6);
        E.put(2131363266, 7);
        E.put(2131363265, 8);
        E.put(2131362616, 9);
        E.put(2131362233, 10);
        E.put(2131363015, 11);
    }
    */

    @DexIgnore
    public f55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public f55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[4], (FlexibleTextInputEditText) objArr[10], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[3], (FlexibleTextInputLayout) objArr[9], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (ProgressButton) objArr[11], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[7]);
        this.C = -1;
        ((e55) this).y.setTag(null);
        a(view);
        f();
    }
}
