package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r60 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<if0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public if0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(cc0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<Ri\u2026class.java.classLoader)!!");
                cc0 cc0 = (cc0) readParcelable;
                df0 df0 = (df0) parcel.readParcelable(df0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(r60.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new if0(cc0, df0, (r60) readParcelable2);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public if0[] newArray(int i) {
            return new if0[i];
        }
    }

    @DexIgnore
    public if0(cc0 cc0, r60 r60) {
        super(cc0, null);
        this.c = r60;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        try {
            qs1 qs1 = qs1.d;
            yb0 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return qs1.a(s, r60, new s31(((ac0) deviceRequest).d(), new r60(this.c.getMajor(), this.c.getMinor())).getData());
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (f41 e) {
            wl0.h.a(e);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(super.b(), r51.r3, this.c.toString());
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(if0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.c, ((if0) obj).c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData");
    }

    @DexIgnore
    public final r60 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public if0(cc0 cc0, df0 df0, r60 r60) {
        super(cc0, df0);
        this.c = r60;
    }
}
