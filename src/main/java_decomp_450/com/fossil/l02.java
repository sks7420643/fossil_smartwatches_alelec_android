package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.o6;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l02 extends m02 {
    @DexIgnore
    public static /* final */ Object d; // = new Object();
    @DexIgnore
    public static /* final */ l02 e; // = new l02();
    @DexIgnore
    public String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"HandlerLeak"})
    public class a extends bg2 {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int c = l02.this.c(this.a);
            if (l02.this.c(c)) {
                l02.this.c(this.a, c);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m02
    public static l02 a() {
        return e;
    }

    @DexIgnore
    public boolean b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, i2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @DexIgnore
    public void c(Context context, int i) {
        a(context, i, (String) null, a(context, i, 0, "n"));
    }

    @DexIgnore
    public final void d(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000);
    }

    @DexIgnore
    public Dialog a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return a(activity, i, l62.a(activity, a(activity, i, "d"), i2), onCancelListener);
    }

    @DexIgnore
    public final String b() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    @DexIgnore
    @Override // com.fossil.m02
    public int c(Context context) {
        return super.c(context);
    }

    @DexIgnore
    @Override // com.fossil.m02
    public final boolean c(int i) {
        return super.c(i);
    }

    @DexIgnore
    public boolean a(Activity activity, int i, int i2) {
        return b(activity, i, i2, null);
    }

    @DexIgnore
    public final boolean a(Activity activity, x12 x12, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, l62.a(x12, a(activity, i, "d"), 2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m02
    public final String b(int i) {
        return super.b(i);
    }

    @DexIgnore
    public final boolean a(Context context, i02 i02, int i) {
        PendingIntent a2 = a(context, i02);
        if (a2 == null) {
            return false;
        }
        a(context, i02.e(), (String) null, GoogleApiActivity.a(context, a2, i));
        return true;
    }

    @DexIgnore
    public static Dialog a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(k62.b(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        a(activity, create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    @DexIgnore
    public final x32 a(Context context, z32 z32) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        x32 x32 = new x32(z32);
        context.registerReceiver(x32, intentFilter);
        x32.a(context);
        if (a(context, "com.google.android.gms")) {
            return x32;
        }
        z32.a();
        x32.a();
        return null;
    }

    @DexIgnore
    @Override // com.fossil.m02
    public int a(Context context, int i) {
        return super.a(context, i);
    }

    @DexIgnore
    @Override // com.fossil.m02
    public Intent a(Context context, int i, String str) {
        return super.a(context, i, str);
    }

    @DexIgnore
    @Override // com.fossil.m02
    public PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    @DexIgnore
    public PendingIntent a(Context context, i02 i02) {
        if (i02.w()) {
            return i02.v();
        }
        return a(context, i02.e(), 0);
    }

    @DexIgnore
    public static Dialog a(Context context, int i, l62 l62, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(k62.b(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String a2 = k62.a(context, i);
        if (a2 != null) {
            builder.setPositiveButton(a2, l62);
        }
        String e2 = k62.e(context, i);
        if (e2 != null) {
            builder.setTitle(e2);
        }
        return builder.create();
    }

    @DexIgnore
    public static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            t02.a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        j02.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @DexIgnore
    @TargetApi(20)
    public final void a(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        Log.w("GoogleApiAvailability", String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", Integer.valueOf(i), null), new IllegalArgumentException());
        if (i == 18) {
            d(context);
        } else if (pendingIntent != null) {
            String d2 = k62.d(context, i);
            String c2 = k62.c(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            o6.e eVar = new o6.e(context);
            eVar.b(true);
            eVar.a(true);
            eVar.b((CharSequence) d2);
            o6.c cVar = new o6.c();
            cVar.a(c2);
            eVar.a(cVar);
            if (r92.a(context)) {
                a72.b(v92.g());
                eVar.f(context.getApplicationInfo().icon);
                eVar.e(2);
                if (r92.b(context)) {
                    eVar.a(e02.common_full_open_on_phone, resources.getString(f02.common_open_on_phone), pendingIntent);
                } else {
                    eVar.a(pendingIntent);
                }
            } else {
                eVar.f(17301642);
                eVar.c(resources.getString(f02.common_google_play_services_notification_ticker));
                eVar.a(System.currentTimeMillis());
                eVar.a(pendingIntent);
                eVar.a((CharSequence) c2);
            }
            if (v92.j()) {
                a72.b(v92.j());
                String b = b();
                if (b == null) {
                    b = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(b);
                    String b2 = k62.b(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(b, b2, 4));
                    } else if (!b2.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(b2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                eVar.b(b);
            }
            Notification a2 = eVar.a();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                q02.d.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, a2);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
