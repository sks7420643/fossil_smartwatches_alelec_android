package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.fossil.za0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es1 {
    @DexIgnore
    public static /* final */ es1 a; // = new es1();

    @DexIgnore
    public final za0 a(Location[] locationArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, float f) {
        Location location;
        Location location2;
        Location[] locationArr2;
        Location location3;
        Location location4;
        double d;
        double d2;
        int i9;
        double d3;
        int i10;
        double d4;
        double d5;
        za0.b bVar;
        Location[] locationArr3 = locationArr;
        int i11 = i2;
        if (locationArr3.length <= 1) {
            return null;
        }
        Location[] locationArr4 = (Location[]) locationArr.clone();
        if (locationArr3.length == 0) {
            location = null;
        } else {
            location = locationArr3[0];
            int f2 = t97.f(locationArr);
            if (f2 != 0) {
                double longitude = location.getLongitude();
                if (1 <= f2) {
                    int i12 = 1;
                    while (true) {
                        Location location5 = locationArr3[i12];
                        double longitude2 = location5.getLongitude();
                        if (Double.compare(longitude, longitude2) > 0) {
                            longitude = longitude2;
                            location = location5;
                        }
                        if (i12 == f2) {
                            break;
                        }
                        i12++;
                    }
                }
            }
        }
        if (location != null) {
            double longitude3 = location.getLongitude();
            if (locationArr3.length == 0) {
                location2 = null;
            } else {
                location2 = locationArr3[0];
                int f3 = t97.f(locationArr);
                if (f3 != 0) {
                    double latitude = location2.getLatitude();
                    if (1 <= f3) {
                        int i13 = 1;
                        while (true) {
                            Location location6 = locationArr3[i13];
                            double latitude2 = location6.getLatitude();
                            if (Double.compare(latitude, latitude2) > 0) {
                                latitude = latitude2;
                                location2 = location6;
                            }
                            if (i13 == f3) {
                                break;
                            }
                            i13++;
                            f3 = f3;
                        }
                    }
                }
            }
            if (location2 != null) {
                double latitude3 = location2.getLatitude();
                if (locationArr3.length == 0) {
                    locationArr2 = locationArr4;
                    location3 = null;
                } else {
                    location3 = locationArr3[0];
                    int f4 = t97.f(locationArr);
                    if (f4 != 0) {
                        double longitude4 = location3.getLongitude();
                        if (1 <= f4) {
                            int i14 = 1;
                            while (true) {
                                Location location7 = locationArr3[i14];
                                locationArr2 = locationArr4;
                                double longitude5 = location7.getLongitude();
                                if (Double.compare(longitude4, longitude5) < 0) {
                                    longitude4 = longitude5;
                                    location3 = location7;
                                }
                                if (i14 == f4) {
                                    break;
                                }
                                i14++;
                                locationArr4 = locationArr2;
                            }
                        }
                    }
                    locationArr2 = locationArr4;
                }
                if (location3 != null) {
                    double longitude6 = location3.getLongitude();
                    if (locationArr3.length == 0) {
                        location4 = null;
                    } else {
                        location4 = locationArr3[0];
                        int f5 = t97.f(locationArr);
                        if (f5 != 0) {
                            double latitude4 = location4.getLatitude();
                            if (1 <= f5) {
                                int i15 = 1;
                                while (true) {
                                    Location location8 = locationArr3[i15];
                                    double latitude5 = location8.getLatitude();
                                    if (Double.compare(latitude4, latitude5) < 0) {
                                        latitude4 = latitude5;
                                        location4 = location8;
                                    }
                                    if (i15 == f5) {
                                        break;
                                    }
                                    i15++;
                                    locationArr3 = locationArr;
                                }
                            }
                        }
                    }
                    if (location4 != null) {
                        double d6 = longitude6 - longitude3;
                        double latitude6 = location4.getLatitude() - latitude3;
                        int i16 = (d6 > latitude6 ? 1 : (d6 == latitude6 ? 0 : -1));
                        if (i16 > 0) {
                            d3 = (double) i5;
                            double d7 = ((((double) i11) / 2.0d) - d3) - ((double) i6);
                            i11 /= 2;
                            i10 = i;
                            d = latitude3;
                            i9 = i16;
                            d2 = (double) i3;
                            d4 = d7;
                            d5 = (double) ((i - i3) - i4);
                        } else {
                            double d8 = (double) i5;
                            d = latitude3;
                            d2 = d8;
                            double d9 = ((((double) i) / 2.0d) - d8) - ((double) i6);
                            i10 = i / 2;
                            i9 = i16;
                            d3 = (double) i3;
                            d5 = d9;
                            d4 = (double) ((i11 - i3) - i4);
                        }
                        double d10 = d6 / d5;
                        double d11 = latitude6 / d4;
                        double max = Math.max(d10, d11);
                        Location[] locationArr5 = locationArr2;
                        int length = locationArr5.length;
                        int i17 = 0;
                        while (i17 < length) {
                            Location location9 = locationArr5[i17];
                            location9.setLongitude(location9.getLongitude() - longitude3);
                            location9.setLatitude(location9.getLatitude() - d);
                            location9.setLongitude(location9.getLongitude() / max);
                            location9.setLatitude(location9.getLatitude() / max);
                            if (d10 > d11) {
                                location9.setLatitude(((d4 - (latitude6 / max)) / ((double) 2)) + location9.getLatitude());
                            } else {
                                location9.setLongitude(((d5 - (d6 / max)) / ((double) 2)) + location9.getLongitude());
                            }
                            location9.setLatitude(d4 - location9.getLatitude());
                            location9.setLongitude(location9.getLongitude() + d2);
                            location9.setLatitude(location9.getLatitude() + d3);
                            i17++;
                            locationArr5 = locationArr5;
                        }
                        Bitmap createBitmap = Bitmap.createBitmap(i10, i11, Bitmap.Config.ARGB_8888);
                        ee7.a((Object) createBitmap, "Bitmap\n                .\u2026 Bitmap.Config.ARGB_8888)");
                        Paint paint = new Paint();
                        paint.setColor(i8);
                        paint.setStrokeWidth(f);
                        Canvas canvas = new Canvas(createBitmap);
                        canvas.drawColor(i7);
                        int length2 = locationArr5.length - 1;
                        int i18 = 0;
                        while (i18 < length2) {
                            int i19 = i18 + 1;
                            canvas.drawLine((float) locationArr5[i18].getLongitude(), (float) locationArr5[i18].getLatitude(), (float) locationArr5[i19].getLongitude(), (float) locationArr5[i19].getLatitude(), paint);
                            i18 = i19;
                        }
                        OutputSettings outputSettings = new OutputSettings(i10, i11, Format.RLE, false, false);
                        String valueOf = String.valueOf(createBitmap.hashCode());
                        FilterResult apply = EInkImageFilter.create().apply(createBitmap, FilterType.DIRECT_MAPPING, false, false, outputSettings);
                        ee7.a((Object) apply, "EInkImageFilter.create()\u2026se, false, routeSettings)");
                        byte[] data = apply.getData();
                        ee7.a((Object) data, "EInkImageFilter.create()\u2026alse, routeSettings).data");
                        if (i9 > 0) {
                            bVar = za0.b.HORIZONTAL;
                        } else {
                            bVar = za0.b.VERTICAL;
                        }
                        return new za0(valueOf, data, bVar);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }
}
