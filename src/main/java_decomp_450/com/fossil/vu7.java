package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vu7 extends RuntimeException {
    @DexIgnore
    public /* final */ transient fv7<?> a;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String message;

    @DexIgnore
    public vu7(fv7<?> fv7) {
        super(a(fv7));
        this.code = fv7.b();
        this.message = fv7.e();
        this.a = fv7;
    }

    @DexIgnore
    public static String a(fv7<?> fv7) {
        jv7.a(fv7, "response == null");
        return "HTTP " + fv7.b() + " " + fv7.e();
    }

    @DexIgnore
    public int code() {
        return this.code;
    }

    @DexIgnore
    public String message() {
        return this.message;
    }

    @DexIgnore
    public fv7<?> response() {
        return this.a;
    }
}
