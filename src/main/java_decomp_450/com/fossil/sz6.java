package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.qg;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sz6 extends qg.i {
    @DexIgnore
    public sz6(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
        ee7.b(canvas, "c");
        ee7.b(recyclerView, "recyclerView");
        ee7.b(viewHolder, "viewHolder");
        super.a(canvas, recyclerView, viewHolder, f, f2, i, z);
        View view = viewHolder.itemView;
        ee7.a((Object) view, "viewHolder.itemView");
        ColorDrawable colorDrawable = new ColorDrawable(-65536);
        float f3 = (float) 0;
        if (f < f3) {
            colorDrawable.setBounds((view.getRight() + Math.round(f)) - 0, view.getTop(), view.getRight(), view.getBottom());
        } else {
            colorDrawable.setBounds(0, 0, 0, 0);
        }
        colorDrawable.draw(canvas);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((float) PortfolioApp.g0.c().getResources().getDimensionPixelSize(2131165720));
        float f4 = (float) 2;
        float bottom = ((float) (view.getBottom() + view.getTop())) / f4;
        String a = ig5.a(PortfolioApp.g0.c(), 2131887339);
        ee7.a((Object) a, "LanguageHelper.getString\u2026ute_time_app_CTA__Delete)");
        if (a != null) {
            String upperCase = a.toUpperCase();
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            canvas.drawText(upperCase, ((((float) view.getRight()) + f) - f3) + ((float) PortfolioApp.g0.c().getResources().getDimensionPixelSize(2131165399)), bottom - ((paint.descent() + paint.ascent()) / f4), paint);
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.qg.f
    public boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        ee7.b(recyclerView, "recyclerView");
        ee7.b(viewHolder, "viewHolder");
        ee7.b(viewHolder2, "target");
        return false;
    }
}
