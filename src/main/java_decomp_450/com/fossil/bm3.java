package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm3 implements Callable<String> {
    @DexIgnore
    public /* final */ /* synthetic */ nm3 a;
    @DexIgnore
    public /* final */ /* synthetic */ xl3 b;

    @DexIgnore
    public bm3(xl3 xl3, nm3 nm3) {
        this.b = xl3;
        this.a = nm3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ String call() throws Exception {
        kg3 c = this.b.c(this.a);
        if (c != null) {
            return c.m();
        }
        this.b.e().w().a("App info was null when attempting to get app instance id");
        return null;
    }
}
