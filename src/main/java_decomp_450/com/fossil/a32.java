package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a32 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ q22 a;

    @DexIgnore
    public a32(q22 q22) {
        this.a = q22;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void run() {
        this.a.b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.a.b.unlock();
            }
        } catch (RuntimeException e) {
            this.a.a.a(e);
        } finally {
            this.a.b.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ a32(q22 q22, t22 t22) {
        this(q22);
    }
}
