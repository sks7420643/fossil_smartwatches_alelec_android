package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.p62;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q82 extends p62 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<p62.a, p82> d; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ e92 g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ long i;

    @DexIgnore
    public q82(Context context) {
        this.e = context.getApplicationContext();
        this.f = new kg2(context.getMainLooper(), this);
        this.g = e92.a();
        this.h = 5000;
        this.i = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    @Override // com.fossil.p62
    public final boolean a(p62.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d2;
        a72.a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            p82 p82 = this.d.get(aVar);
            if (p82 == null) {
                p82 = new p82(this, aVar);
                p82.a(serviceConnection, serviceConnection, str);
                p82.a(str);
                this.d.put(aVar, p82);
            } else {
                this.f.removeMessages(0, aVar);
                if (!p82.a(serviceConnection)) {
                    p82.a(serviceConnection, serviceConnection, str);
                    int c = p82.c();
                    if (c == 1) {
                        serviceConnection.onServiceConnected(p82.b(), p82.a());
                    } else if (c == 2) {
                        p82.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = p82.d();
        }
        return d2;
    }

    @DexIgnore
    @Override // com.fossil.p62
    public final void b(p62.a aVar, ServiceConnection serviceConnection, String str) {
        a72.a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            p82 p82 = this.d.get(aVar);
            if (p82 == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (p82.a(serviceConnection)) {
                p82.a(serviceConnection, str);
                if (p82.e()) {
                    this.f.sendMessageDelayed(this.f.obtainMessage(0, aVar), this.h);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 0) {
            synchronized (this.d) {
                p62.a aVar = (p62.a) message.obj;
                p82 p82 = this.d.get(aVar);
                if (p82 != null && p82.e()) {
                    if (p82.d()) {
                        p82.b("GmsClientSupervisor");
                    }
                    this.d.remove(aVar);
                }
            }
            return true;
        } else if (i2 != 1) {
            return false;
        } else {
            synchronized (this.d) {
                p62.a aVar2 = (p62.a) message.obj;
                p82 p822 = this.d.get(aVar2);
                if (p822 != null && p822.c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = p822.b();
                    if (b == null) {
                        b = aVar2.a();
                    }
                    if (b == null) {
                        b = new ComponentName(aVar2.b(), "unknown");
                    }
                    p822.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
