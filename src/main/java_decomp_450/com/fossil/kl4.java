package com.fossil;

import com.fossil.ng;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl4 extends ng.d<GoalTrackingData> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        ee7.b(goalTrackingData, "oldItem");
        ee7.b(goalTrackingData2, "newItem");
        return ee7.a(goalTrackingData, goalTrackingData2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        ee7.b(goalTrackingData, "oldItem");
        ee7.b(goalTrackingData2, "newItem");
        return ee7.a((Object) goalTrackingData.getId(), (Object) goalTrackingData2.getId());
    }
}
