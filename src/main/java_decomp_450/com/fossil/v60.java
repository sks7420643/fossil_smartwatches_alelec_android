package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v60 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ go0 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<v60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final v60[] a(byte[] bArr) {
            Object obj;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr.length - 1) - 2) {
                go0 a = go0.f.a(bArr[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(s97.a(bArr, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                ee7.a((Object) order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int b = yz0.b(order.getShort());
                byte[] a2 = s97.a(bArr, i3, i3 + b);
                if (a == null) {
                    obj = null;
                } else {
                    int i4 = nk0.b[a.ordinal()];
                    if (i4 == 1) {
                        obj = w60.CREATOR.a(a2);
                    } else if (i4 == 2) {
                        obj = f70.CREATOR.a(a2);
                    } else if (i4 == 3) {
                        obj = x60.CREATOR.a(a2);
                    } else {
                        throw new p87();
                    }
                }
                if (obj != null) {
                    arrayList.add(obj);
                }
                i += b + 3;
            }
            Object[] array = arrayList.toArray(new v60[0]);
            if (array != null) {
                return (v60[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public v60 createFromParcel(Parcel parcel) {
            go0 go0 = go0.values()[parcel.readInt()];
            parcel.setDataPosition(parcel.dataPosition() - 4);
            int i = nk0.a[go0.ordinal()];
            if (i == 1) {
                return w60.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return f70.CREATOR.createFromParcel(parcel);
            }
            if (i == 3) {
                return x60.CREATOR.createFromParcel(parcel);
            }
            throw new p87();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public v60[] newArray(int i) {
            return new v60[i];
        }
    }

    @DexIgnore
    public v60(go0 go0) {
        this.a = go0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(new JSONObject(), r51.d, yz0.a(this.a));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] c = c();
        byte[] array = ByteBuffer.allocate(c.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a).putShort((short) c.length).put(c).array();
        ee7.a((Object) array, "ByteBuffer.allocate(SUB_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((v60) obj).a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.AlarmSubEntry");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
    }
}
