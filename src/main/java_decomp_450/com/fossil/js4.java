package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js4 extends he {
    @DexIgnore
    public /* final */ MutableLiveData<Object> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gu4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ch5 c;
    @DexIgnore
    public /* final */ ro4 d;
    @DexIgnore
    public /* final */ xo4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1", f = "BCCreateSubTabViewModel.kt", l = {33, 38}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ js4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.js4$a$a")
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$challenge$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.js4$a$a  reason: collision with other inner class name */
        public static final class C0088a extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0088a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0088a aVar = new C0088a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
                return ((C0088a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.d.a(new String[]{"running", "waiting"}, new Date());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$countFriend$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return pb7.a(this.this$0.this$0.e.b());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(js4 js4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = js4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0074  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00a3  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002b
                if (r1 == r4) goto L_0x0023
                if (r1 != r3) goto L_0x001b
                java.lang.Object r0 = r8.L$1
                com.fossil.mn4 r0 = (com.fossil.mn4) r0
                java.lang.Object r0 = r8.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r9)
                goto L_0x006c
            L_0x001b:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x0023:
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x0044
            L_0x002b:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r1 = r8.p$
                com.fossil.ti7 r9 = com.fossil.qj7.b()
                com.fossil.js4$a$a r5 = new com.fossil.js4$a$a
                r5.<init>(r8, r2)
                r8.L$0 = r1
                r8.label = r4
                java.lang.Object r9 = com.fossil.vh7.a(r9, r5, r8)
                if (r9 != r0) goto L_0x0044
                return r0
            L_0x0044:
                com.fossil.mn4 r9 = (com.fossil.mn4) r9
                if (r9 != 0) goto L_0x00bd
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                boolean r5 = r5.e()
                if (r5 == 0) goto L_0x00b1
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.js4$a$b r7 = new com.fossil.js4$a$b
                r7.<init>(r8, r2)
                r8.L$0 = r1
                r8.L$1 = r9
                r8.Z$0 = r5
                r8.label = r3
                java.lang.Object r9 = com.fossil.vh7.a(r6, r7, r8)
                if (r9 != r0) goto L_0x006c
                return r0
            L_0x006c:
                java.lang.Number r9 = (java.lang.Number) r9
                int r9 = r9.intValue()
                if (r9 >= r4) goto L_0x00a3
                com.fossil.js4 r9 = r8.this$0
                com.fossil.ch5 r9 = r9.c
                boolean r9 = r9.E()
                if (r9 == 0) goto L_0x008e
                com.fossil.js4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.a
                java.lang.Boolean r0 = com.fossil.pb7.a(r4)
                r9.a(r0)
                goto L_0x00c8
            L_0x008e:
                com.fossil.js4 r9 = r8.this$0
                com.fossil.ch5 r9 = r9.c
                r9.e0()
                com.fossil.js4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.b
                com.fossil.gu4 r0 = com.fossil.gu4.SUGGEST_FIND_FRIEND
                r9.a(r0)
                goto L_0x00c8
            L_0x00a3:
                com.fossil.js4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.a
                java.lang.Boolean r0 = com.fossil.pb7.a(r4)
                r9.a(r0)
                goto L_0x00c8
            L_0x00b1:
                com.fossil.js4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.b
                com.fossil.gu4 r0 = com.fossil.gu4.NEED_ACTIVE_DEVICE
                r9.a(r0)
                goto L_0x00c8
            L_0x00bd:
                com.fossil.js4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.b
                com.fossil.gu4 r0 = com.fossil.gu4.JOIN_TOO_MUCH
                r9.a(r0)
            L_0x00c8:
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.js4.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public js4(ch5 ch5, ro4 ro4, xo4 xo4) {
        ee7.b(ch5, "shared");
        ee7.b(ro4, "challengeRepository");
        ee7.b(xo4, "friendRepository");
        this.c = ch5;
        this.d = ro4;
        this.e = xo4;
    }

    @DexIgnore
    public final LiveData<Object> a() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<gu4> b() {
        return this.b;
    }

    @DexIgnore
    public final void c() {
        ik7 unused = xh7.b(ie.a(this), null, null, new a(this, null), 3, null);
    }
}
