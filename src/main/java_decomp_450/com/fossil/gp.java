package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.lm;
import com.fossil.qm;
import java.util.LinkedList;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gp implements Runnable {
    @DexIgnore
    public /* final */ wm a; // = new wm();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gp {
        @DexIgnore
        public /* final */ /* synthetic */ dn b;
        @DexIgnore
        public /* final */ /* synthetic */ UUID c;

        @DexIgnore
        public a(dn dnVar, UUID uuid) {
            this.b = dnVar;
            this.c = uuid;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.gp
        public void b() {
            WorkDatabase f = this.b.f();
            f.beginTransaction();
            try {
                a(this.b, this.c.toString());
                f.setTransactionSuccessful();
                f.endTransaction();
                a(this.b);
            } catch (Throwable th) {
                f.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gp {
        @DexIgnore
        public /* final */ /* synthetic */ dn b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public b(dn dnVar, String str, boolean z) {
            this.b = dnVar;
            this.c = str;
            this.d = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.gp
        public void b() {
            WorkDatabase f = this.b.f();
            f.beginTransaction();
            try {
                for (String str : f.f().c(this.c)) {
                    a(this.b, str);
                }
                f.setTransactionSuccessful();
                f.endTransaction();
                if (this.d) {
                    a(this.b);
                }
            } catch (Throwable th) {
                f.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public lm a() {
        return this.a;
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public void run() {
        try {
            b();
            this.a.a(lm.a);
        } catch (Throwable th) {
            this.a.a(new lm.b.a(th));
        }
    }

    @DexIgnore
    public void a(dn dnVar, String str) {
        a(dnVar.f(), str);
        dnVar.d().f(str);
        for (ym ymVar : dnVar.e()) {
            ymVar.a(str);
        }
    }

    @DexIgnore
    public void a(dn dnVar) {
        zm.a(dnVar.b(), dnVar.f(), dnVar.e());
    }

    @DexIgnore
    public final void a(WorkDatabase workDatabase, String str) {
        ap f = workDatabase.f();
        lo a2 = workDatabase.a();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            qm.a d = f.d(str2);
            if (!(d == qm.a.SUCCEEDED || d == qm.a.FAILED)) {
                f.a(qm.a.CANCELLED, str2);
            }
            linkedList.addAll(a2.a(str2));
        }
    }

    @DexIgnore
    public static gp a(UUID uuid, dn dnVar) {
        return new a(dnVar, uuid);
    }

    @DexIgnore
    public static gp a(String str, dn dnVar, boolean z) {
        return new b(dnVar, str, z);
    }
}
