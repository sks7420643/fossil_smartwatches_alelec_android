package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls5 implements Factory<c56> {
    @DexIgnore
    public static c56 a(gs5 gs5) {
        c56 e = gs5.e();
        c87.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
