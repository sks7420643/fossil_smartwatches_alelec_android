package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bf5 {
    @DexIgnore
    public static final void a(View view, View.OnClickListener onClickListener) {
        ee7.b(view, "$this$setOnSingleClickListener");
        if (onClickListener != null) {
            view.setOnClickListener(new se5(onClickListener));
            if (onClickListener != null) {
                return;
            }
        }
        view.setOnClickListener(null);
        i97 i97 = i97.a;
    }
}
