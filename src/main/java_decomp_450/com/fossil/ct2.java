package com.fossil;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ct2<E> extends bt2<E> implements wt2<E>, NavigableSet<E> {
    @DexIgnore
    public transient ct2<E> c;
    @DexIgnore
    public /* final */ transient Comparator<? super E> zza;

    @DexIgnore
    public ct2(Comparator<? super E> comparator) {
        this.zza = comparator;
    }

    @DexIgnore
    public static <E> rt2<E> zza(Comparator<? super E> comparator) {
        return ht2.zza.equals(comparator) ? (rt2<E>) rt2.zzb : new rt2<>(os2.zza(), comparator);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) et2.a((ct2) tailSet(e, true), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.wt2
    public Comparator<? super E> comparator() {
        return this.zza;
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet descendingSet() {
        ct2<E> ct2 = this.c;
        if (ct2 != null) {
            return ct2;
        }
        ct2<E> zzi = zzi();
        this.c = zzi;
        zzi.c = this;
        return zzi;
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return (E) ((yt2) iterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) dt2.a((yt2) ((ct2) headSet(e, true)).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet headSet(Object obj) {
        return (ct2) headSet(obj, false);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) et2.a((ct2) tailSet(e, false), null);
    }

    @DexIgnore
    @Override // com.fossil.ps2, java.util.AbstractCollection, java.util.Collection, com.fossil.ws2, java.util.Set, java.util.NavigableSet, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return iterator();
    }

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return (E) ((yt2) descendingIterator()).next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) dt2.a((yt2) ((ct2) headSet(e, false)).descendingIterator(), null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet subSet(Object obj, Object obj2) {
        return (ct2) subSet(obj, true, obj2, false);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public /* synthetic */ SortedSet tailSet(Object obj) {
        return (ct2) tailSet(obj, true);
    }

    @DexIgnore
    public abstract ct2<E> zza(E e, boolean z);

    @DexIgnore
    public abstract ct2<E> zza(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    public abstract ct2<E> zzb(E e, boolean z);

    @DexIgnore
    public abstract ct2<E> zzi();

    @DexIgnore
    /* renamed from: zzj */
    public abstract yt2<E> descendingIterator();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet headSet(Object obj, boolean z) {
        or2.a(obj);
        return zza(obj, z);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet subSet(Object obj, boolean z, Object obj2, boolean z2) {
        or2.a(obj);
        or2.a(obj2);
        if (this.zza.compare(obj, obj2) <= 0) {
            return zza(obj, z, obj2, z2);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.NavigableSet
    public /* synthetic */ NavigableSet tailSet(Object obj, boolean z) {
        or2.a(obj);
        return zzb(obj, z);
    }

    @DexIgnore
    public final int zza(Object obj, Object obj2) {
        return this.zza.compare(obj, obj2);
    }
}
