package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.eb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u14 implements eb3.a {
    @DexIgnore
    public /* final */ /* synthetic */ v14 a;

    @DexIgnore
    public u14(v14 v14) {
        this.a = v14;
    }

    @DexIgnore
    @Override // com.fossil.ri3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (str != null && !str.equals(CrashDumperPlugin.NAME) && q14.b(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("name", str2);
            bundle2.putLong("timestampInMillis", j);
            bundle2.putBundle(NativeProtocol.WEB_DIALOG_PARAMS, bundle);
            this.a.a.a(3, bundle2);
        }
    }
}
