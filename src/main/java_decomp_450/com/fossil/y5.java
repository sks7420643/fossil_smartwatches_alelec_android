package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y5 {
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131952123;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131952124;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131952126;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131952129;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131952131;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131952375;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131952376;
    @DexIgnore
    public static /* final */ int Widget_Support_CoordinatorLayout; // = 2131952479;
}
