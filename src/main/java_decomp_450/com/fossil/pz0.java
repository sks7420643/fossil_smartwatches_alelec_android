package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pz0 extends fe7 implements kd7<zk0, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ b31 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pz0(b31 b31) {
        super(2);
        this.a = b31;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(zk0 zk0, Float f) {
        this.a.a(f.floatValue());
        return i97.a;
    }
}
