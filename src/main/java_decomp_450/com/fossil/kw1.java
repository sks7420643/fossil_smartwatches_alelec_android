package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw1 implements Factory<jw1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<bv1> b;
    @DexIgnore
    public /* final */ Provider<sw1> c;
    @DexIgnore
    public /* final */ Provider<pw1> d;
    @DexIgnore
    public /* final */ Provider<Executor> e;
    @DexIgnore
    public /* final */ Provider<ay1> f;
    @DexIgnore
    public /* final */ Provider<by1> g;

    @DexIgnore
    public kw1(Provider<Context> provider, Provider<bv1> provider2, Provider<sw1> provider3, Provider<pw1> provider4, Provider<Executor> provider5, Provider<ay1> provider6, Provider<by1> provider7) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
    }

    @DexIgnore
    public static kw1 a(Provider<Context> provider, Provider<bv1> provider2, Provider<sw1> provider3, Provider<pw1> provider4, Provider<Executor> provider5, Provider<ay1> provider6, Provider<by1> provider7) {
        return new kw1(provider, provider2, provider3, provider4, provider5, provider6, provider7);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public jw1 get() {
        return new jw1(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }
}
