package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f0 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131887195;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131887196;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131887197;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131887198;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131887199;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131887200;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131887201;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131887202;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131887203;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131887204;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131887205;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131887206;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131887207;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131887208;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131887209;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131887210;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131887211;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131887212;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131887213;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887214;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887215;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887216;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887217;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887218;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887219;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887220;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887221;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131887518;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887534;
}
