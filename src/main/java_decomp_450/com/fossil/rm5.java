package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm5 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ GoogleApiService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public b(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            ee7.b(str, "address");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress", f = "GetAddress.kt", l = {20}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(rm5 rm5, fb7 fb7) {
            super(fb7);
            this.this$0 = rm5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1", f = "GetAddress.kt", l = {20}, m = "invokeSuspend")
    public static final class f extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ rm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(rm5 rm5, b bVar, fb7 fb7) {
            super(1, fb7);
            this.this$0 = rm5;
            this.$requestValues = bVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new f(this.this$0, this.$requestValues, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((f) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GoogleApiService a2 = this.this$0.d;
                StringBuilder sb = new StringBuilder();
                b bVar = this.$requestValues;
                Double d = null;
                sb.append(bVar != null ? pb7.a(bVar.a()) : null);
                sb.append(',');
                b bVar2 = this.$requestValues;
                if (bVar2 != null) {
                    d = pb7.a(bVar2.b());
                }
                sb.append(d);
                String sb2 = sb.toString();
                Locale a3 = ig5.a();
                ee7.a((Object) a3, "LanguageHelper.getLocale()");
                String language = a3.getLanguage();
                ee7.a((Object) language, "LanguageHelper.getLocale().language");
                this.label = 1;
                obj = a2.getAddress(sb2, language, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public rm5(GoogleApiService googleApiService) {
        ee7.b(googleApiService, "mGoogleApiService");
        this.d = googleApiService;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.rm5.b r7, com.fossil.fb7<java.lang.Object> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.fossil.rm5.e
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.rm5$e r0 = (com.fossil.rm5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.rm5$e r0 = new com.fossil.rm5$e
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x003a
            if (r2 != r3) goto L_0x0032
            java.lang.Object r7 = r0.L$1
            com.fossil.rm5$b r7 = (com.fossil.rm5.b) r7
            java.lang.Object r7 = r0.L$0
            com.fossil.rm5 r7 = (com.fossil.rm5) r7
            com.fossil.t87.a(r8)
            goto L_0x005c
        L_0x0032:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003a:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = "GetAddress"
            java.lang.String r5 = "executeUseCase"
            r8.d(r2, r5)
            com.fossil.rm5$f r8 = new com.fossil.rm5$f
            r8.<init>(r6, r7, r4)
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r8 = com.fossil.aj5.a(r8, r0)
            if (r8 != r1) goto L_0x005c
            return r1
        L_0x005c:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r7 = r8 instanceof com.fossil.bj5
            if (r7 == 0) goto L_0x00a8
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r7 = r8.a()
            com.fossil.ie4 r7 = (com.fossil.ie4) r7
            if (r7 == 0) goto L_0x0073
            java.lang.String r8 = "results"
            com.fossil.de4 r7 = r7.b(r8)
            goto L_0x0074
        L_0x0073:
            r7 = r4
        L_0x0074:
            if (r7 == 0) goto L_0x00a2
            int r8 = r7.size()
            if (r8 <= 0) goto L_0x00a2
            r8 = 0
            com.google.gson.JsonElement r7 = r7.get(r8)
            com.fossil.ie4 r7 = (com.fossil.ie4) r7
            if (r7 == 0) goto L_0x008b
            java.lang.String r8 = "formatted_address"
            com.google.gson.JsonElement r4 = r7.a(r8)
        L_0x008b:
            if (r4 == 0) goto L_0x009c
            com.fossil.rm5$d r7 = new com.fossil.rm5$d
            java.lang.String r8 = r4.f()
            java.lang.String r0 = "value.asString"
            com.fossil.ee7.a(r8, r0)
            r7.<init>(r8)
            goto L_0x00b1
        L_0x009c:
            com.fossil.rm5$c r7 = new com.fossil.rm5$c
            r7.<init>()
            goto L_0x00b1
        L_0x00a2:
            com.fossil.rm5$c r7 = new com.fossil.rm5$c
            r7.<init>()
            goto L_0x00b1
        L_0x00a8:
            boolean r7 = r8 instanceof com.fossil.yi5
            if (r7 == 0) goto L_0x00b2
            com.fossil.rm5$c r7 = new com.fossil.rm5$c
            r7.<init>()
        L_0x00b1:
            return r7
        L_0x00b2:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rm5.a(com.fossil.rm5$b, com.fossil.fb7):java.lang.Object");
    }
}
