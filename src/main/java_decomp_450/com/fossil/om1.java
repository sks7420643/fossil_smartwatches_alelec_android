package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om1 {
    @DexIgnore
    public /* synthetic */ om1(zd7 zd7) {
    }

    @DexIgnore
    public final qk1 a(UUID uuid) {
        if (ee7.a(uuid, b21.x.h())) {
            return qk1.MODEL_NUMBER;
        }
        if (ee7.a(uuid, b21.x.i())) {
            return qk1.SERIAL_NUMBER;
        }
        if (ee7.a(uuid, b21.x.g())) {
            return qk1.FIRMWARE_VERSION;
        }
        if (ee7.a(uuid, b21.x.j())) {
            return qk1.SOFTWARE_REVISION;
        }
        if (ee7.a(uuid, b21.x.p())) {
            return qk1.DC;
        }
        if (ee7.a(uuid, b21.x.q())) {
            return qk1.FTC;
        }
        if (ee7.a(uuid, b21.x.r())) {
            return qk1.FTD;
        }
        if (ee7.a(uuid, b21.x.n())) {
            return qk1.ASYNC;
        }
        if (ee7.a(uuid, b21.x.s())) {
            return qk1.FTD_1;
        }
        if (ee7.a(uuid, b21.x.o())) {
            return qk1.AUTHENTICATION;
        }
        if (ee7.a(uuid, b21.x.l())) {
            return qk1.HEART_RATE;
        }
        return qk1.UNKNOWN;
    }
}
