package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nj5;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul5 extends fl4<km5, lm5, jm5> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public /* final */ b d; // = new b(this);
    @DexIgnore
    public /* final */ MicroAppRepository e;
    @DexIgnore
    public /* final */ ch5 f;
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ HybridPresetRepository i;
    @DexIgnore
    public /* final */ NotificationsRepository j;
    @DexIgnore
    public /* final */ qd5 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ ul5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ul5 ul5) {
            this.a = ul5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, this.a.h.c(), true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                if (intExtra == 1) {
                    FLogger.INSTANCE.getLocal().d(ul5.m, "sync success, remove device now");
                    nj5.d.b(this, CommunicateMode.SYNC);
                    this.a.a(new lm5());
                } else if (intExtra == 2 || intExtra == 4) {
                    nj5.d.b(this, CommunicateMode.SYNC);
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = ul5.m;
                    local.d(d, "sync fail due to " + intExtra2);
                    if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                        this.a.a(new jm5(nb5.FAIL_DUE_TO_LACK_PERMISSION, integerArrayListExtra));
                    } else {
                        this.a.a(new jm5(nb5.FAIL_DUE_TO_SYNC_FAIL, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase", f = "HybridSyncUseCase.kt", l = {85, 93, 132}, m = "run")
    public static final class c extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ul5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ul5 ul5, fb7 fb7) {
            super(fb7);
            this.this$0 = ul5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((km5) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$run$isAA$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        public d(fb7 fb7) {
            super(2, fb7);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return pb7.a(ng5.a().a(PortfolioApp.g0.c()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase$saveNotificationSettingToDevice$1", f = "HybridSyncUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ SparseArray $data;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMovemberModel;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ul5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ul5 ul5, SparseArray sparseArray, boolean z, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ul5;
            this.$data = sparseArray;
            this.$isMovemberModel = z;
            this.$serialNumber = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$data, this.$isMovemberModel, this.$serialNumber, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                AppNotificationFilterSettings a = nx6.b.a(this.$data, this.$isMovemberModel);
                PortfolioApp.g0.c().a(a, this.$serialNumber);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String c = this.this$0.c();
                local.d(c, "saveNotificationSettingToDevice, total: " + a.getNotificationFilters().size() + " items");
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ul5.class.getSimpleName();
        ee7.a((Object) simpleName, "HybridSyncUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public ul5(MicroAppRepository microAppRepository, ch5 ch5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, qd5 qd5, AlarmsRepository alarmsRepository) {
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(portfolioApp, "mApp");
        ee7.b(hybridPresetRepository, "mPresetRepository");
        ee7.b(notificationsRepository, "mNotificationsRepository");
        ee7.b(qd5, "mAnalyticsHelper");
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.e = microAppRepository;
        this.f = ch5;
        this.g = deviceRepository;
        this.h = portfolioApp;
        this.i = hybridPresetRepository;
        this.j = notificationsRepository;
        this.k = qd5;
        this.l = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "HybridSyncUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(km5 km5, fb7 fb7) {
        return a(km5, (fb7<Object>) fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x02a5  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x02e1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.km5 r18, com.fossil.fb7<java.lang.Object> r19) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.fossil.ul5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.ul5$c r3 = (com.fossil.ul5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.ul5$c r3 = new com.fossil.ul5$c
            r3.<init>(r1, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 3
            java.lang.String r7 = "Inside "
            java.lang.String r8 = "Error inside "
            r9 = 1
            r10 = 2
            r11 = 0
            if (r5 == 0) goto L_0x0089
            if (r5 == r9) goto L_0x0076
            if (r5 == r10) goto L_0x0065
            if (r5 != r6) goto L_0x005d
            boolean r0 = r3.Z$2
            java.lang.Object r0 = r3.L$4
            com.portfolio.platform.data.model.SKUModel r0 = (com.portfolio.platform.data.model.SKUModel) r0
            java.lang.Object r0 = r3.L$3
            android.util.SparseArray r0 = (android.util.SparseArray) r0
            boolean r0 = r3.Z$1
            java.lang.Object r0 = r3.L$2
            r4 = r0
            com.misfit.frameworks.buttonservice.model.UserProfile r4 = (com.misfit.frameworks.buttonservice.model.UserProfile) r4
            boolean r0 = r3.Z$0
            int r5 = r3.I$0
            java.lang.Object r0 = r3.L$1
            r6 = r0
            com.fossil.km5 r6 = (com.fossil.km5) r6
            java.lang.Object r0 = r3.L$0
            r3 = r0
            com.fossil.ul5 r3 = (com.fossil.ul5) r3
            com.fossil.t87.a(r2)     // Catch:{ Exception -> 0x005a }
            goto L_0x02a1
        L_0x005a:
            r0 = move-exception
            goto L_0x0384
        L_0x005d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0065:
            boolean r0 = r3.Z$0
            int r5 = r3.I$0
            java.lang.Object r12 = r3.L$1
            com.fossil.km5 r12 = (com.fossil.km5) r12
            java.lang.Object r13 = r3.L$0
            com.fossil.ul5 r13 = (com.fossil.ul5) r13
            com.fossil.t87.a(r2)
            goto L_0x0117
        L_0x0076:
            int r0 = r3.I$0
            java.lang.Object r5 = r3.L$1
            com.fossil.km5 r5 = (com.fossil.km5) r5
            java.lang.Object r12 = r3.L$0
            com.fossil.ul5 r12 = (com.fossil.ul5) r12
            com.fossil.t87.a(r2)
            r16 = r5
            r5 = r0
            r0 = r16
            goto L_0x00b8
        L_0x0089:
            com.fossil.t87.a(r2)
            if (r0 != 0) goto L_0x0096
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r11)
            return r0
        L_0x0096:
            int r2 = r18.b()
            com.fossil.ti7 r5 = com.fossil.qj7.b()
            com.fossil.ul5$d r12 = new com.fossil.ul5$d
            r12.<init>(r11)
            r3.L$0 = r1
            r3.L$1 = r0
            r3.I$0 = r2
            r3.label = r9
            java.lang.Object r5 = com.fossil.vh7.a(r5, r12, r3)
            if (r5 != r4) goto L_0x00b2
            return r4
        L_0x00b2:
            r12 = r1
            r16 = r5
            r5 = r2
            r2 = r16
        L_0x00b8:
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x00f9
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r12.c()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r8)
            java.lang.String r5 = r12.c()
            r4.append(r5)
            java.lang.String r5 = ".startDeviceSync - FAIL_DUE_TO_UAA"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            java.lang.String r0 = r0.a()
            r2 = 6
            java.lang.Integer r2 = com.fossil.pb7.a(r2)
            r12.a(r0, r2)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_UAA
            r0.<init>(r2, r11)
            return r0
        L_0x00f9:
            com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r13 = r13.c()
            r3.L$0 = r12
            r3.L$1 = r0
            r3.I$0 = r5
            r3.Z$0 = r2
            r3.label = r10
            java.lang.Object r13 = r13.d(r3)
            if (r13 != r4) goto L_0x0110
            return r4
        L_0x0110:
            r16 = r12
            r12 = r0
            r0 = r2
            r2 = r13
            r13 = r16
        L_0x0117:
            com.misfit.frameworks.buttonservice.model.UserProfile r2 = (com.misfit.frameworks.buttonservice.model.UserProfile) r2
            if (r2 != 0) goto L_0x014b
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.ul5.m
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r8)
            java.lang.String r4 = com.fossil.ul5.m
            r3.append(r4)
            java.lang.String r4 = ".startDeviceSync - user is null"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.e(r2, r3)
            java.lang.String r0 = r12.a()
            a(r13, r0, r11, r10, r11)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r11)
            return r0
        L_0x014b:
            java.lang.String r14 = r12.a()
            boolean r14 = android.text.TextUtils.isEmpty(r14)
            if (r14 == 0) goto L_0x0185
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.ul5.m
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r8)
            java.lang.String r4 = com.fossil.ul5.m
            r3.append(r4)
            java.lang.String r4 = ".startDeviceSync - serial is null"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.e(r2, r3)
            java.lang.String r0 = r12.a()
            a(r13, r0, r11, r10, r11)
            com.fossil.jm5 r0 = new com.fossil.jm5
            com.fossil.nb5 r2 = com.fossil.nb5.FAIL_DUE_TO_INVALID_REQUEST
            r0.<init>(r2, r11)
            return r0
        L_0x0185:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r10 = com.fossil.ul5.m
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r7)
            java.lang.String r14 = com.fossil.ul5.m
            r11.append(r14)
            java.lang.String r14 = ".startDeviceSync - serial="
            r11.append(r14)
            java.lang.String r14 = r12.a()
            r11.append(r14)
            java.lang.String r14 = ","
            r11.append(r14)
            java.lang.String r14 = " weightInKg="
            r11.append(r14)
            com.misfit.frameworks.buttonservice.model.UserBiometricData r14 = r2.getUserBiometricData()
            float r14 = r14.getWeightInKilogram()
            r11.append(r14)
            java.lang.String r14 = ", heightInMeter="
            r11.append(r14)
            com.misfit.frameworks.buttonservice.model.UserBiometricData r14 = r2.getUserBiometricData()
            float r14 = r14.getHeightInMeter()
            r11.append(r14)
            java.lang.String r14 = ", goal="
            r11.append(r14)
            long r14 = r2.getGoalSteps()
            r11.append(r14)
            java.lang.String r14 = ", isNewDevice="
            r11.append(r14)
            boolean r14 = r12.c()
            r11.append(r14)
            java.lang.String r14 = ", SyncMode="
            r11.append(r14)
            r11.append(r5)
            java.lang.String r11 = r11.toString()
            r8.d(r10, r11)
            r2.setOriginalSyncMode(r5)
            com.fossil.ch5 r8 = r13.f
            java.lang.String r10 = r12.a()
            boolean r8 = r8.i(r10)
            r10 = 13
            if (r8 == 0) goto L_0x0205
            if (r5 != r10) goto L_0x0207
        L_0x0205:
            r5 = 13
        L_0x0207:
            if (r5 == r10) goto L_0x022c
            com.portfolio.platform.PortfolioApp$a r11 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r11 = r11.c()
            java.lang.String r14 = r12.a()
            boolean r11 = r11.g(r14)
            if (r11 == 0) goto L_0x022c
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.ul5.m
            java.lang.String r3 = "Device is syncing, returning..."
            r0.e(r2, r3)
            com.fossil.lm5 r0 = new com.fossil.lm5
            r0.<init>()
            return r0
        L_0x022c:
            if (r5 != r10) goto L_0x03ab
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0380 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()     // Catch:{ Exception -> 0x0380 }
            java.lang.String r11 = com.fossil.ul5.m     // Catch:{ Exception -> 0x0380 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0380 }
            r14.<init>()     // Catch:{ Exception -> 0x0380 }
            r14.append(r7)     // Catch:{ Exception -> 0x0380 }
            java.lang.String r15 = com.fossil.ul5.m     // Catch:{ Exception -> 0x0380 }
            r14.append(r15)     // Catch:{ Exception -> 0x0380 }
            java.lang.String r15 = ".startDeviceSync - Start full-sync"
            r14.append(r15)     // Catch:{ Exception -> 0x0380 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0380 }
            r10.d(r11, r14)     // Catch:{ Exception -> 0x0380 }
            com.portfolio.platform.data.source.NotificationsRepository r10 = r13.j     // Catch:{ Exception -> 0x0380 }
            java.lang.String r11 = r12.a()     // Catch:{ Exception -> 0x0380 }
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r14 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM     // Catch:{ Exception -> 0x0380 }
            int r14 = r14.getValue()     // Catch:{ Exception -> 0x0380 }
            android.util.SparseArray r10 = r10.getAllNotificationsByHour(r11, r14)     // Catch:{ Exception -> 0x0380 }
            com.portfolio.platform.data.source.DeviceRepository r11 = r13.g     // Catch:{ Exception -> 0x0380 }
            com.fossil.be5$a r14 = com.fossil.be5.o     // Catch:{ Exception -> 0x0380 }
            java.lang.String r15 = r12.a()     // Catch:{ Exception -> 0x0380 }
            java.lang.String r14 = r14.b(r15)     // Catch:{ Exception -> 0x0380 }
            com.portfolio.platform.data.model.SKUModel r11 = r11.getSkuModelBySerialPrefix(r14)     // Catch:{ Exception -> 0x0380 }
            com.fossil.nx6 r14 = com.fossil.nx6.b     // Catch:{ Exception -> 0x0380 }
            java.lang.String r15 = r12.a()     // Catch:{ Exception -> 0x0380 }
            boolean r14 = r14.a(r11, r15)     // Catch:{ Exception -> 0x0380 }
            java.lang.String r15 = r12.a()     // Catch:{ Exception -> 0x0380 }
            r13.a(r10, r15, r14)     // Catch:{ Exception -> 0x0380 }
            com.portfolio.platform.data.source.AlarmsRepository r15 = r13.l     // Catch:{ Exception -> 0x0380 }
            r3.L$0 = r13     // Catch:{ Exception -> 0x0380 }
            r3.L$1 = r12     // Catch:{ Exception -> 0x0380 }
            r3.I$0 = r5     // Catch:{ Exception -> 0x0380 }
            r3.Z$0 = r0     // Catch:{ Exception -> 0x0380 }
            r3.L$2 = r2     // Catch:{ Exception -> 0x0380 }
            r3.Z$1 = r8     // Catch:{ Exception -> 0x0380 }
            r3.L$3 = r10     // Catch:{ Exception -> 0x0380 }
            r3.L$4 = r11     // Catch:{ Exception -> 0x0380 }
            r3.Z$2 = r14     // Catch:{ Exception -> 0x0380 }
            r3.label = r6     // Catch:{ Exception -> 0x0380 }
            java.lang.Object r0 = r15.getActiveAlarms(r3)     // Catch:{ Exception -> 0x0380 }
            if (r0 != r4) goto L_0x029d
            return r4
        L_0x029d:
            r4 = r2
            r6 = r12
            r3 = r13
            r2 = r0
        L_0x02a1:
            java.util.List r2 = (java.util.List) r2
            if (r2 != 0) goto L_0x02aa
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
        L_0x02aa:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.util.List r2 = com.fossil.rc5.a(r2)
            r0.a(r2)
            com.portfolio.platform.data.source.HybridPresetRepository r0 = r3.i
            java.lang.String r2 = r6.a()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r0.getActivePresetBySerial(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r8 = r3.c()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "startDeviceSync activePreset="
            r10.append(r11)
            r10.append(r2)
            java.lang.String r10 = r10.toString()
            r0.d(r8, r10)
            if (r2 == 0) goto L_0x03a8
            java.util.ArrayList r0 = r2.getButtons()
            java.util.Iterator r8 = r0.iterator()
        L_0x02e9:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x035f
            java.lang.Object r0 = r8.next()
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
            java.lang.String r10 = r0.getAppId()
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r11 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
            java.lang.String r11 = r11.getValue()
            boolean r10 = com.fossil.ee7.a(r10, r11)
            if (r10 == 0) goto L_0x02e9
            java.lang.String r0 = r0.getSettings()
            boolean r10 = android.text.TextUtils.isEmpty(r0)
            if (r10 != 0) goto L_0x02e9
            com.google.gson.Gson r10 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0336 }
            r10.<init>()     // Catch:{ Exception -> 0x0336 }
            java.lang.Class<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> r11 = com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class
            java.lang.Object r0 = r10.a(r0, r11)     // Catch:{ Exception -> 0x0336 }
            com.portfolio.platform.data.model.setting.SecondTimezoneSetting r0 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r0     // Catch:{ Exception -> 0x0336 }
            if (r0 == 0) goto L_0x02e9
            java.lang.String r10 = r0.getTimeZoneId()     // Catch:{ Exception -> 0x0336 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ Exception -> 0x0336 }
            if (r10 != 0) goto L_0x02e9
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0336 }
            com.portfolio.platform.PortfolioApp r10 = r10.c()     // Catch:{ Exception -> 0x0336 }
            java.lang.String r0 = r0.getTimeZoneId()     // Catch:{ Exception -> 0x0336 }
            r10.o(r0)     // Catch:{ Exception -> 0x0336 }
            goto L_0x02e9
        L_0x0336:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r11 = com.fossil.ul5.m
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            r12.append(r7)
            java.lang.String r13 = com.fossil.ul5.m
            r12.append(r13)
            java.lang.String r13 = ".startDeviceSync - parse secondTimezone, ex="
            r12.append(r13)
            r12.append(r0)
            java.lang.String r12 = r12.toString()
            r10.e(r11, r12)
            r0.printStackTrace()
            goto L_0x02e9
        L_0x035f:
            java.lang.String r0 = r6.a()
            com.portfolio.platform.data.source.DeviceRepository r7 = r3.g
            com.portfolio.platform.data.source.MicroAppRepository r8 = r3.e
            java.util.List r0 = com.fossil.xc5.a(r2, r0, r7, r8)
            boolean r2 = r0.isEmpty()
            r2 = r2 ^ r9
            if (r2 == 0) goto L_0x03a8
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r7 = r6.a()
            r2.b(r7, r0)
            goto L_0x03a8
        L_0x0380:
            r0 = move-exception
            r4 = r2
            r6 = r12
            r3 = r13
        L_0x0384:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r7 = r2.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r8 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r9 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            java.lang.String r10 = r6.a()
            java.lang.String r11 = com.fossil.ul5.m
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r12 = "[Sync] Exception when prepare settings "
            r2.append(r12)
            r2.append(r0)
            java.lang.String r12 = r2.toString()
            r7.i(r8, r9, r10, r11, r12)
        L_0x03a8:
            r13 = r3
            r2 = r4
            r12 = r6
        L_0x03ab:
            boolean r0 = r12.c()
            java.lang.String r3 = r12.a()
            r13.a(r5, r0, r2, r3)
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ul5.a(com.fossil.km5, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final ik7 a(SparseArray<List<BaseFeatureModel>> sparseArray, String str, boolean z) {
        return xh7.b(zi7.a(qj7.a()), null, null, new e(this, sparseArray, z, str, null), 3, null);
    }

    @DexIgnore
    public final void a(int i2, boolean z, UserProfile userProfile, String str) {
        if (a() != null) {
            nj5.d.a(this.d, CommunicateMode.SYNC);
        }
        userProfile.setNewDevice(z);
        userProfile.setSyncMode(i2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, ".startDeviceSync - syncMode=" + i2);
        if (!this.f.b0()) {
            SKUModel skuModelBySerialPrefix = this.g.getSkuModelBySerialPrefix(be5.o.b(str));
            this.f.c((Boolean) true);
            this.k.a(i2, skuModelBySerialPrefix);
            if5 b2 = qd5.f.b("sync_session");
            qd5.f.a("sync_session", b2);
            b2.d();
        }
        if (!PortfolioApp.g0.c().a(str, userProfile)) {
            a(this, str, null, 2, null);
        } else if (i2 == 13) {
            this.f.a(str, System.currentTimeMillis(), false);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(ul5 ul5, String str, Integer num, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            num = 2;
        }
        ul5.a(str, num);
    }

    @DexIgnore
    public final void a(String str, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "broadcastSyncFail serial=" + str);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", num);
        intent.putExtra("SERIAL", str);
    }
}
