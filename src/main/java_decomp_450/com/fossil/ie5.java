package com.fossil;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.a12;
import com.fossil.bc2;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ie5 {
    @DexIgnore
    public static /* final */ String c; // = "ie5";
    @DexIgnore
    public static a12 d;
    @DexIgnore
    public static d e;
    @DexIgnore
    public ch5 a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements a12.c {
        @DexIgnore
        public a(ie5 ie5) {
        }

        @DexIgnore
        @Override // com.fossil.a22
        public void a(i02 i02) {
            d dVar = ie5.e;
            if (dVar != null) {
                dVar.a(i02);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements a12.b {
        @DexIgnore
        public b(ie5 ie5) {
        }

        @DexIgnore
        @Override // com.fossil.t12
        public void a(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(ie5.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(ie5.c, "Connection lost.  Reason: Service Disconnected");
            }
        }

        @DexIgnore
        @Override // com.fossil.t12
        public void b(Bundle bundle) {
            d dVar = ie5.e;
            if (dVar != null) {
                dVar.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            c12<Status> a2 = ac2.d.a(ie5.d);
            d dVar = ie5.e;
            if (dVar != null) {
                dVar.a(a2);
            }
            ie5.this.a.h(false);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(c12<Status> c12);

        @DexIgnore
        void a(i02 i02);
    }

    @DexIgnore
    public ie5(Context context, Executor executor, ch5 ch5) {
        if (d == null) {
            a12.a aVar = new a12.a(context);
            aVar.a(ac2.b, new Scope[0]);
            aVar.a(ac2.a, new Scope[0]);
            aVar.a(ac2.c, new Scope[0]);
            aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            aVar.a(new b(this));
            aVar.a(new a(this));
            d = aVar.a();
        }
        this.a = ch5;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.c();
        this.a.h(true);
    }

    @DexIgnore
    public void b() {
        d.d();
        this.a.h(false);
    }

    @DexIgnore
    public final bc2 c() {
        bc2.a b2 = bc2.b();
        b2.a(DataType.j, 1);
        return b2.a();
    }

    @DexIgnore
    public boolean d() {
        return qy1.a(qy1.a(PortfolioApp.c0), c());
    }

    @DexIgnore
    public boolean e() {
        return d.g() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.a.k();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new c());
    }

    @DexIgnore
    public void h() {
        try {
            b();
            qy1.a(PortfolioApp.c0, new GoogleSignInOptions.a(GoogleSignInOptions.u).a()).j();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void i() {
        this.a.h(false);
    }

    @DexIgnore
    public void a(d dVar) {
        e = dVar;
    }

    @DexIgnore
    public void a(Fragment fragment) {
        qy1.a(fragment, 1, qy1.a(PortfolioApp.c0), c());
    }
}
