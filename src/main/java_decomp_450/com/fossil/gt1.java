package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gt1 {
    @DexIgnore
    <T> ft1<T> a(String str, Class<T> cls, bt1 bt1, et1<T, byte[]> et1);

    @DexIgnore
    @Deprecated
    <T> ft1<T> a(String str, Class<T> cls, et1<T, byte[]> et1);
}
