package com.fossil;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s16 extends n16 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> e; // = new ArrayList<>();
    @DexIgnore
    public Ringtone f;
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ o16 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1$1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends Ringtone>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Ringtone>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return rd5.g.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(s16 s16, fb7 fb7) {
            super(2, fb7);
            this.this$0 = s16;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            ArrayList arrayList;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.e.isEmpty()) {
                    ArrayList b = this.this$0.e;
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(null);
                    this.L$0 = yi7;
                    this.L$1 = b;
                    this.label = 1;
                    obj = vh7.a(a3, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                    arrayList = b;
                }
                this.this$0.j().p(this.this$0.e);
                return i97.a;
            } else if (i == 1) {
                arrayList = (ArrayList) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList.addAll((Collection) obj);
            if (this.this$0.f == null) {
                s16 s16 = this.this$0;
                s16.f = (Ringtone) s16.e.get(0);
            }
            this.this$0.j().p(this.this$0.e);
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public s16(o16 o16) {
        ee7.b(o16, "mView");
        this.h = o16;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.n16
    public Ringtone h() {
        Ringtone ringtone = this.f;
        if (ringtone != null) {
            return ringtone;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.n16
    public void i() {
        dh5.o.a().b();
    }

    @DexIgnore
    public final o16 j() {
        return this.h;
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void b(Ringtone ringtone) {
        ee7.b(ringtone, Constants.RINGTONE);
        this.f = ringtone;
    }

    @DexIgnore
    @Override // com.fossil.n16
    public void a(Ringtone ringtone) {
        ee7.b(ringtone, Constants.RINGTONE);
        dh5.o.a().a(ringtone);
        this.f = ringtone;
    }

    @DexIgnore
    public void a(String str) {
        ee7.b(str, MicroAppSetting.SETTING);
        try {
            this.f = (Ringtone) this.g.a(str, Ringtone.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e2);
        }
    }
}
