package com.fossil;

import android.content.Context;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k53 extends z02<v02.d.C0203d> {
    @DexIgnore
    public k53(Context context) {
        super(context, e53.c, (v02.d) null, new o12());
    }

    @DexIgnore
    public no3<g53> a(f53 f53) {
        return z62.a(e53.e.a(b(), f53), new g53());
    }
}
