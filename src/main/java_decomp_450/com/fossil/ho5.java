package com.fossil;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.cy6;
import com.fossil.xg5;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ho5 extends go5 implements cy6.g {
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        ee7.a((Object) ho5.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a(ib5... ib5Arr) {
        ee7.b(ib5Arr, "permissionCodes");
        ArrayList<ib5> arrayList = new ArrayList<>();
        for (ib5 ib5 : ib5Arr) {
            arrayList.add(ib5);
        }
        xg5.b.a(getContext(), arrayList, w97.d(xg5.a.PAIR_DEVICE));
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i, Intent intent) {
        ee7.b(str, "tag");
        if (getActivity() instanceof cl5) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).a(str, i, intent);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
