package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ea1 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ aj0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ea1(aj0 aj0) {
        super(1);
        this.a = aj0;
    }

    @DexIgnore
    public final void a(zk0 zk0) {
        this.a.U = System.currentTimeMillis();
        aj0 aj0 = this.a;
        m60 m60 = ((s21) zk0).C;
        aj0.X = m60;
        aj0.W = m60.getFirmwareVersion();
        aj0 aj02 = this.a;
        String str = aj02.a0;
        if (str == null || mh7.b(aj02.W, str, true)) {
            aj0 aj03 = this.a;
            aj03.Z = false;
            aj03.a(eu0.a(((zk0) aj03).v, null, is0.SUCCESS, null, 5));
            return;
        }
        aj0 aj04 = this.a;
        aj04.Z = false;
        aj04.a(eu0.a(((zk0) aj04).v, null, is0.TARGET_FIRMWARE_NOT_MATCHED, null, 5));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(zk0 zk0) {
        a(zk0);
        return i97.a;
    }
}
