package com.fossil;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.p62;
import com.fossil.u62;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h62<T extends IInterface> {
    @DexIgnore
    public static /* final */ k02[] A; // = new k02[0];
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public int d;
    @DexIgnore
    public long e;
    @DexIgnore
    public s82 f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ p62 h;
    @DexIgnore
    public /* final */ m02 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ Object k;
    @DexIgnore
    public /* final */ Object l;
    @DexIgnore
    public w62 m;
    @DexIgnore
    public c n;
    @DexIgnore
    public T o;
    @DexIgnore
    public /* final */ ArrayList<h<?>> p;
    @DexIgnore
    public i q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ a s;
    @DexIgnore
    public /* final */ b t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ String v;
    @DexIgnore
    public i02 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public volatile n82 y;
    @DexIgnore
    public AtomicInteger z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);

        @DexIgnore
        void b(Bundle bundle);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(i02 i02);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(i02 i02);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.h62.c
        public void a(i02 i02) {
            if (i02.x()) {
                h62 h62 = h62.this;
                h62.a((s62) null, h62.z());
            } else if (h62.this.t != null) {
                h62.this.t.a(i02);
            }
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f extends h<Boolean> {
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Bundle e;

        @DexIgnore
        public f(int i, Bundle bundle) {
            super(true);
            this.d = i;
            this.e = bundle;
        }

        @DexIgnore
        public abstract void a(i02 i02);

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.h62.h
        public final /* synthetic */ void a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool == null) {
                h62.this.c(1, null);
                return;
            }
            int i = this.d;
            if (i != 0) {
                if (i != 10) {
                    h62.this.c(1, null);
                    Bundle bundle = this.e;
                    if (bundle != null) {
                        pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
                    }
                    a(new i02(this.d, pendingIntent));
                    return;
                }
                h62.this.c(1, null);
                throw new IllegalStateException(String.format("A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. ", getClass().getSimpleName(), h62.this.p(), h62.this.i()));
            } else if (!e()) {
                h62.this.c(1, null);
                a(new i02(8, null));
            }
        }

        @DexIgnore
        @Override // com.fossil.h62.h
        public final void c() {
        }

        @DexIgnore
        public abstract boolean e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends kg2 {
        @DexIgnore
        public g(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public static void a(Message message) {
            h hVar = (h) message.obj;
            hVar.c();
            hVar.b();
        }

        @DexIgnore
        public static boolean b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            i02 i02;
            i02 i022;
            if (h62.this.z.get() == message.arg1) {
                int i = message.what;
                if ((i == 1 || i == 7 || ((i == 4 && !h62.this.t()) || message.what == 5)) && !h62.this.f()) {
                    a(message);
                    return;
                }
                int i2 = message.what;
                PendingIntent pendingIntent = null;
                if (i2 == 4) {
                    i02 unused = h62.this.w = new i02(message.arg2);
                    if (!h62.this.G() || h62.this.x) {
                        if (h62.this.w != null) {
                            i022 = h62.this.w;
                        } else {
                            i022 = new i02(8);
                        }
                        h62.this.n.a(i022);
                        h62.this.a(i022);
                        return;
                    }
                    h62.this.c(3, null);
                } else if (i2 == 5) {
                    if (h62.this.w != null) {
                        i02 = h62.this.w;
                    } else {
                        i02 = new i02(8);
                    }
                    h62.this.n.a(i02);
                    h62.this.a(i02);
                } else if (i2 == 3) {
                    Object obj = message.obj;
                    if (obj instanceof PendingIntent) {
                        pendingIntent = (PendingIntent) obj;
                    }
                    i02 i023 = new i02(message.arg2, pendingIntent);
                    h62.this.n.a(i023);
                    h62.this.a(i023);
                } else if (i2 == 6) {
                    h62.this.c(5, null);
                    if (h62.this.s != null) {
                        h62.this.s.a(message.arg2);
                    }
                    h62.this.a(message.arg2);
                    boolean unused2 = h62.this.a(5, 1, (IInterface) null);
                } else if (i2 == 2 && !h62.this.c()) {
                    a(message);
                } else if (b(message)) {
                    ((h) message.obj).d();
                } else {
                    int i3 = message.what;
                    StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(i3);
                    Log.wtf("GmsClient", sb.toString(), new Exception());
                }
            } else if (b(message)) {
                a(message);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<TListener> {
        @DexIgnore
        public TListener a;
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore
        public h(TListener tlistener) {
            this.a = tlistener;
        }

        @DexIgnore
        public final void a() {
            synchronized (this) {
                this.a = null;
            }
        }

        @DexIgnore
        public abstract void a(TListener tlistener);

        @DexIgnore
        public final void b() {
            a();
            synchronized (h62.this.p) {
                h62.this.p.remove(this);
            }
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public final void d() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    c();
                    throw e;
                }
            } else {
                c();
            }
            synchronized (this) {
                this.b = true;
            }
            b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements ServiceConnection {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public i(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            w62 w62;
            if (iBinder == null) {
                h62.this.c(16);
                return;
            }
            synchronized (h62.this.l) {
                h62 h62 = h62.this;
                if (iBinder == null) {
                    w62 = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof w62)) {
                        w62 = new v62(iBinder);
                    } else {
                        w62 = (w62) queryLocalInterface;
                    }
                }
                w62 unused = h62.m = w62;
            }
            h62.this.a(0, (Bundle) null, this.a);
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (h62.this.l) {
                w62 unused = h62.this.m = (w62) null;
            }
            Handler handler = h62.this.j;
            handler.sendMessage(handler.obtainMessage(6, this.a, 1));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends u62.a {
        @DexIgnore
        public h62 a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public j(h62 h62, int i) {
            this.a = h62;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.u62
        public final void a(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @DexIgnore
        @Override // com.fossil.u62
        public final void a(int i, IBinder iBinder, Bundle bundle) {
            a72.a(this.a, "onPostInitComplete can be called only once per call to getRemoteService");
            this.a.a(i, iBinder, bundle, this.b);
            this.a = null;
        }

        @DexIgnore
        @Override // com.fossil.u62
        public final void a(int i, IBinder iBinder, n82 n82) {
            a72.a(this.a, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            a72.a(n82);
            this.a.a(n82);
            a(i, iBinder, n82.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends f {
        @DexIgnore
        public /* final */ IBinder g;

        @DexIgnore
        public k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.g = iBinder;
        }

        @DexIgnore
        @Override // com.fossil.h62.f
        public final void a(i02 i02) {
            if (h62.this.t != null) {
                h62.this.t.a(i02);
            }
            h62.this.a(i02);
        }

        @DexIgnore
        @Override // com.fossil.h62.f
        public final boolean e() {
            try {
                String interfaceDescriptor = this.g.getInterfaceDescriptor();
                if (!h62.this.i().equals(interfaceDescriptor)) {
                    String i = h62.this.i();
                    StringBuilder sb = new StringBuilder(String.valueOf(i).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(i);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface a = h62.this.a(this.g);
                if (a == null || (!h62.this.a(2, 4, a) && !h62.this.a(3, 4, a))) {
                    return false;
                }
                i02 unused = h62.this.w = (i02) null;
                Bundle q = h62.this.q();
                if (h62.this.s == null) {
                    return true;
                }
                h62.this.s.b(q);
                return true;
            } catch (RemoteException unused2) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends f {
        @DexIgnore
        public l(int i, Bundle bundle) {
            super(i, null);
        }

        @DexIgnore
        @Override // com.fossil.h62.f
        public final void a(i02 i02) {
            if (!h62.this.t() || !h62.this.G()) {
                h62.this.n.a(i02);
                h62.this.a(i02);
                return;
            }
            h62.this.c(16);
        }

        @DexIgnore
        @Override // com.fossil.h62.f
        public final boolean e() {
            h62.this.n.a(i02.e);
            return true;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public h62(android.content.Context r10, android.os.Looper r11, int r12, com.fossil.h62.a r13, com.fossil.h62.b r14, java.lang.String r15) {
        /*
            r9 = this;
            com.fossil.p62 r3 = com.fossil.p62.a(r10)
            com.fossil.m02 r4 = com.fossil.m02.a()
            com.fossil.a72.a(r13)
            r6 = r13
            com.fossil.h62$a r6 = (com.fossil.h62.a) r6
            com.fossil.a72.a(r14)
            r7 = r14
            com.fossil.h62$b r7 = (com.fossil.h62.b) r7
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r8 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.h62.<init>(android.content.Context, android.os.Looper, int, com.fossil.h62$a, com.fossil.h62$b, java.lang.String):void");
    }

    @DexIgnore
    public final T A() throws DeadObjectException {
        T t2;
        synchronized (this.k) {
            if (this.r != 5) {
                s();
                a72.b(this.o != null, "Client is connected but service is null");
                t2 = this.o;
            } else {
                throw new DeadObjectException();
            }
        }
        return t2;
    }

    @DexIgnore
    public String B() {
        return "com.google.android.gms";
    }

    @DexIgnore
    public boolean C() {
        return false;
    }

    @DexIgnore
    public boolean D() {
        return false;
    }

    @DexIgnore
    public final String E() {
        String str = this.v;
        return str == null ? this.g.getClass().getName() : str;
    }

    @DexIgnore
    public final boolean F() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 3;
        }
        return z2;
    }

    @DexIgnore
    public final boolean G() {
        if (this.x || TextUtils.isEmpty(i()) || TextUtils.isEmpty(y())) {
            return false;
        }
        try {
            Class.forName(i());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);

    @DexIgnore
    public final void a(n82 n82) {
        this.y = n82;
    }

    @DexIgnore
    public void b(int i2) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(6, this.z.get(), i2));
    }

    @DexIgnore
    public void b(int i2, T t2) {
    }

    @DexIgnore
    public final void c(int i2, T t2) {
        s82 s82;
        boolean z2 = false;
        if ((i2 == 4) == (t2 != null)) {
            z2 = true;
        }
        a72.a(z2);
        synchronized (this.k) {
            this.r = i2;
            this.o = t2;
            b(i2, t2);
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.q == null || this.f == null)) {
                        String d2 = this.f.d();
                        String a2 = this.f.a();
                        StringBuilder sb = new StringBuilder(String.valueOf(d2).length() + 70 + String.valueOf(a2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(d2);
                        sb.append(" on ");
                        sb.append(a2);
                        Log.e("GmsClient", sb.toString());
                        this.h.a(this.f.d(), this.f.a(), this.f.c(), this.q, E(), this.f.b());
                        this.z.incrementAndGet();
                    }
                    this.q = new i(this.z.get());
                    if (this.r != 3 || y() == null) {
                        s82 = new s82(B(), p(), false, p62.a(), C());
                    } else {
                        s82 = new s82(w().getPackageName(), y(), true, p62.a(), false);
                    }
                    this.f = s82;
                    if (!s82.b() || k() >= 17895000) {
                        if (!this.h.a(new p62.a(this.f.d(), this.f.a(), this.f.c(), this.f.b()), this.q, E())) {
                            String d3 = this.f.d();
                            String a3 = this.f.a();
                            StringBuilder sb2 = new StringBuilder(String.valueOf(d3).length() + 34 + String.valueOf(a3).length());
                            sb2.append("unable to connect to service: ");
                            sb2.append(d3);
                            sb2.append(" on ");
                            sb2.append(a3);
                            Log.e("GmsClient", sb2.toString());
                            a(16, (Bundle) null, this.z.get());
                        }
                    } else {
                        String valueOf = String.valueOf(this.f.d());
                        throw new IllegalStateException(valueOf.length() != 0 ? "Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: ".concat(valueOf) : new String("Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: "));
                    }
                } else if (i2 == 4) {
                    a(t2);
                }
            } else if (this.q != null) {
                this.h.a(this.f.d(), this.f.a(), this.f.c(), this.q, E(), this.f.b());
                this.q = null;
            }
        }
    }

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public boolean f() {
        boolean z2;
        synchronized (this.k) {
            if (this.r != 2) {
                if (this.r != 3) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    @DexIgnore
    public String g() {
        s82 s82;
        if (c() && (s82 = this.f) != null) {
            return s82.a();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    @DexIgnore
    public abstract String i();

    @DexIgnore
    public boolean j() {
        return true;
    }

    @DexIgnore
    public int k() {
        return m02.a;
    }

    @DexIgnore
    public final k02[] l() {
        n82 n82 = this.y;
        if (n82 == null) {
            return null;
        }
        return n82.b;
    }

    @DexIgnore
    public Intent m() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    @DexIgnore
    public boolean n() {
        return false;
    }

    @DexIgnore
    public IBinder o() {
        synchronized (this.l) {
            if (this.m == null) {
                return null;
            }
            return this.m.asBinder();
        }
    }

    @DexIgnore
    public abstract String p();

    @DexIgnore
    public Bundle q() {
        return null;
    }

    @DexIgnore
    public void r() {
        int a2 = this.i.a(this.g, k());
        if (a2 != 0) {
            c(1, null);
            a(new d(), a2, (PendingIntent) null);
            return;
        }
        a(new d());
    }

    @DexIgnore
    public final void s() {
        if (!c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @DexIgnore
    public boolean t() {
        return false;
    }

    @DexIgnore
    public Account u() {
        return null;
    }

    @DexIgnore
    public k02[] v() {
        return A;
    }

    @DexIgnore
    public final Context w() {
        return this.g;
    }

    @DexIgnore
    public Bundle x() {
        return new Bundle();
    }

    @DexIgnore
    public String y() {
        return null;
    }

    @DexIgnore
    public Set<Scope> z() {
        return Collections.emptySet();
    }

    @DexIgnore
    public void a(T t2) {
        this.c = System.currentTimeMillis();
    }

    @DexIgnore
    public void a(int i2) {
        this.a = i2;
        this.b = System.currentTimeMillis();
    }

    @DexIgnore
    public void a(i02 i02) {
        this.d = i02.e();
        this.e = System.currentTimeMillis();
    }

    @DexIgnore
    public h62(Context context, Looper looper, p62 p62, m02 m02, int i2, a aVar, b bVar, String str) {
        this.k = new Object();
        this.l = new Object();
        this.p = new ArrayList<>();
        this.r = 1;
        this.w = null;
        this.x = false;
        this.y = null;
        this.z = new AtomicInteger(0);
        a72.a(context, "Context must not be null");
        this.g = context;
        a72.a(looper, "Looper must not be null");
        a72.a(p62, "Supervisor must not be null");
        this.h = p62;
        a72.a(m02, "API availability must not be null");
        this.i = m02;
        this.j = new g(looper);
        this.u = i2;
        this.s = aVar;
        this.t = bVar;
        this.v = str;
    }

    @DexIgnore
    public final boolean a(int i2, int i3, T t2) {
        synchronized (this.k) {
            if (this.r != i2) {
                return false;
            }
            c(i3, t2);
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        a72.a(cVar, "Connection progress callbacks cannot be null.");
        this.n = cVar;
        c(2, null);
    }

    @DexIgnore
    public void a() {
        this.z.incrementAndGet();
        synchronized (this.p) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.p.get(i2).a();
            }
            this.p.clear();
        }
        synchronized (this.l) {
            this.m = null;
        }
        c(1, null);
    }

    @DexIgnore
    public void a(c cVar, int i2, PendingIntent pendingIntent) {
        a72.a(cVar, "Connection progress callbacks cannot be null.");
        this.n = cVar;
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(3, this.z.get(), i2, pendingIntent));
    }

    @DexIgnore
    public void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
    }

    @DexIgnore
    public final void a(int i2, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new l(i2, null)));
    }

    @DexIgnore
    public void a(s62 s62, Set<Scope> set) {
        Bundle x2 = x();
        m62 m62 = new m62(this.u);
        m62.d = this.g.getPackageName();
        m62.g = x2;
        if (set != null) {
            m62.f = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (n()) {
            m62.h = u() != null ? u() : new Account("<<default account>>", "com.google");
            if (s62 != null) {
                m62.e = s62.asBinder();
            }
        } else if (D()) {
            m62.h = u();
        }
        m62.i = A;
        m62.j = v();
        try {
            synchronized (this.l) {
                if (this.m != null) {
                    this.m.a(new j(this, this.z.get()), m62);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            b(1);
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            a(8, (IBinder) null, (Bundle) null, this.z.get());
        }
    }

    @DexIgnore
    public boolean c() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 4;
        }
        return z2;
    }

    @DexIgnore
    public final void c(int i2) {
        int i3;
        if (F()) {
            i3 = 5;
            this.x = true;
        } else {
            i3 = 4;
        }
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(i3, this.z.get(), 16));
    }

    @DexIgnore
    public void a(e eVar) {
        eVar.a();
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i2;
        T t2;
        w62 w62;
        synchronized (this.k) {
            i2 = this.r;
            t2 = this.o;
        }
        synchronized (this.l) {
            w62 = this.m;
        }
        printWriter.append((CharSequence) str).append("mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append(" mService=");
        if (t2 == null) {
            printWriter.append("null");
        } else {
            printWriter.append((CharSequence) i()).append("@").append((CharSequence) Integer.toHexString(System.identityHashCode(t2.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (w62 == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(w62.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.c > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append("lastConnectedTime=");
            long j2 = this.c;
            String format = simpleDateFormat.format(new Date(this.c));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.b > 0) {
            printWriter.append((CharSequence) str).append("lastSuspendedCause=");
            int i3 = this.a;
            if (i3 == 1) {
                printWriter.append("CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 != 2) {
                printWriter.append((CharSequence) String.valueOf(i3));
            } else {
                printWriter.append("CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j3 = this.b;
            String format2 = simpleDateFormat.format(new Date(this.b));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.e > 0) {
            printWriter.append((CharSequence) str).append("lastFailedStatus=").append((CharSequence) y02.getStatusCodeString(this.d));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j4 = this.e;
            String format3 = simpleDateFormat.format(new Date(this.e));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }
}
