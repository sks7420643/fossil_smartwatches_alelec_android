package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t53 implements Parcelable.Creator<i53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ i53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    z = j72.i(parcel, a);
                    break;
                case 2:
                    z2 = j72.i(parcel, a);
                    break;
                case 3:
                    z3 = j72.i(parcel, a);
                    break;
                case 4:
                    z4 = j72.i(parcel, a);
                    break;
                case 5:
                    z5 = j72.i(parcel, a);
                    break;
                case 6:
                    z6 = j72.i(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new i53(z, z2, z3, z4, z5, z6);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ i53[] newArray(int i) {
        return new i53[i];
    }
}
