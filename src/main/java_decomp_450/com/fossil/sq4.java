package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq4 extends he {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ro4 e;
    @DexIgnore
    public /* final */ xo4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel", f = "BCMemberInChallengeViewModel.kt", l = {82, 82}, m = "loadInvitedPlayers")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sq4 sq4, fb7 fb7) {
            super(fb7);
            this.this$0 = sq4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super hj7<? extends r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sq4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sq4$b$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCMemberInChallengeViewModel.kt", l = {91}, m = "invokeSuspend")
            /* renamed from: com.fossil.sq4$b$a$a  reason: collision with other inner class name */
            public static final class C0175a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0175a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0175a aVar = new C0175a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
                    return ((C0175a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ro4 a2 = this.this$0.this$0.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = ro4.a(a2, str, new String[]{"waiting", "running", "completed", "left_after_start"}, 0, this, 4, null);
                            if (obj == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sq4$b$a$b")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCMemberInChallengeViewModel.kt", l = {86}, m = "invokeSuspend")
            /* renamed from: com.fossil.sq4$b$a$b  reason: collision with other inner class name */
            public static final class C0176b extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0176b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0176b bVar = new C0176b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
                    return ((C0176b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        b bVar = this.this$0.this$0;
                        if (!bVar.$isWaiting) {
                            return new ko4(w97.a(), null, 2, null);
                        }
                        ro4 a2 = bVar.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = ro4.a(a2, str, new String[]{"invited", "invitation_expired"}, 0, this, 4, null);
                            if (obj == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (ko4) obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    return w87.a(xh7.a(yi7, null, null, new C0176b(this, null), 3, null), xh7.a(yi7, null, null, new C0175a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sq4 sq4, boolean z, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sq4;
            this.$isWaiting = z;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$isWaiting, this.$challengeId, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super hj7<? extends r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return xh7.a(this.p$, qj7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1", f = "BCMemberInChallengeViewModel.kt", l = {38, 39, 40, 42, 45}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sq4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $any;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $joined;
            @DexIgnore
            public /* final */ /* synthetic */ List $pending;
            @DexIgnore
            public /* final */ /* synthetic */ ko4 $resultJoined;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, List list2, List list3, List list4, ko4 ko4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$joined = list;
                this.$friends = list2;
                this.$pending = list3;
                this.$any = list4;
                this.$resultJoined = ko4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$joined, this.$friends, this.$pending, this.$any, this.$resultJoined, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List list = this.$joined;
                    if (list != null) {
                        r87<List<xn4>, List<xn4>> c = nt4.c(this.$friends, list);
                        List<xn4> first = c.getFirst();
                        List<xn4> second = c.getSecond();
                        List list2 = this.$pending;
                        List<xn4> b = list2 != null ? nt4.b(list2) : null;
                        if (!first.isEmpty()) {
                            this.$any.add(xe5.b.e(first.size()));
                            this.$any.addAll(first);
                        }
                        if (!second.isEmpty()) {
                            this.$any.add(xe5.b.f(second.size()));
                            this.$any.addAll(second);
                        }
                        if (b != null && (!b.isEmpty())) {
                            this.$any.add(xe5.b.g(b.size()));
                            this.$any.addAll(b);
                        }
                        this.this$0.this$0.c.a(this.$any);
                    } else if (this.this$0.this$0.c.a() == null) {
                        this.this$0.this$0.d.a(w87.a(pb7.a(true), this.$resultJoined.a()));
                    } else {
                        this.this$0.this$0.d.a(w87.a(pb7.a(false), this.$resultJoined.a()));
                    }
                    this.this$0.this$0.b.a(pb7.a(false));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$friends$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<un4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<un4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.f.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sq4 sq4, String str, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sq4;
            this.$challengeId = str;
            this.$isWaiting = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, this.$isWaiting, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x010b A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0133 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0134  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x017d A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r22) {
            /*
                r21 = this;
                r8 = r21
                java.lang.Object r9 = com.fossil.nb7.a()
                int r0 = r8.label
                r10 = 5
                r1 = 4
                r2 = 3
                r3 = 2
                r4 = 1
                if (r0 == 0) goto L_0x00a4
                if (r0 == r4) goto L_0x0095
                if (r0 == r3) goto L_0x0081
                if (r0 == r2) goto L_0x0067
                if (r0 == r1) goto L_0x0042
                if (r0 != r10) goto L_0x003a
                java.lang.Object r0 = r8.L$6
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r8.L$5
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r8.L$4
                com.fossil.ko4 r0 = (com.fossil.ko4) r0
                java.lang.Object r0 = r8.L$3
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r0 = r8.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r8.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r8.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r22)
                goto L_0x017e
            L_0x003a:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0042:
                java.lang.Object r0 = r8.L$5
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r8.L$4
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r2 = r8.L$3
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r3 = r8.L$2
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r8.L$1
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r5 = r8.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r22)
                r11 = r0
                r12 = r1
                r13 = r2
                r14 = r3
                r15 = r4
                r7 = r5
                r1 = r22
                goto L_0x013a
            L_0x0067:
                java.lang.Object r0 = r8.L$3
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r2 = r8.L$2
                java.util.List r2 = (java.util.List) r2
                java.lang.Object r3 = r8.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r8.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r22)
                r5 = r4
                r4 = r3
                r3 = r2
                r2 = r22
                goto L_0x0111
            L_0x0081:
                java.lang.Object r0 = r8.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r3 = r8.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r8.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r22)
                r5 = r4
                r4 = r3
                r3 = r22
                goto L_0x00f3
            L_0x0095:
                java.lang.Object r0 = r8.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r4 = r8.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r22)
                r5 = r4
                r4 = r22
                goto L_0x00d7
            L_0x00a4:
                com.fossil.t87.a(r22)
                com.fossil.yi7 r0 = r8.p$
                com.fossil.sq4 r5 = r8.this$0
                androidx.lifecycle.MutableLiveData r5 = r5.b
                java.lang.Boolean r6 = com.fossil.pb7.a(r4)
                r5.a(r6)
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                com.fossil.ti7 r6 = com.fossil.qj7.b()
                com.fossil.sq4$c$b r7 = new com.fossil.sq4$c$b
                r11 = 0
                r7.<init>(r8, r11)
                r8.L$0 = r0
                r8.L$1 = r5
                r8.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r6, r7, r8)
                if (r4 != r9) goto L_0x00d2
                return r9
            L_0x00d2:
                r20 = r5
                r5 = r0
                r0 = r20
            L_0x00d7:
                java.util.List r4 = (java.util.List) r4
                com.fossil.sq4 r6 = r8.this$0
                java.lang.String r7 = r8.$challengeId
                boolean r11 = r8.$isWaiting
                r8.L$0 = r5
                r8.L$1 = r0
                r8.L$2 = r4
                r8.label = r3
                java.lang.Object r3 = r6.a(r7, r11, r8)
                if (r3 != r9) goto L_0x00ee
                return r9
            L_0x00ee:
                r20 = r4
                r4 = r0
                r0 = r20
            L_0x00f3:
                com.fossil.r87 r3 = (com.fossil.r87) r3
                java.lang.Object r6 = r3.getSecond()
                com.fossil.hj7 r6 = (com.fossil.hj7) r6
                r8.L$0 = r5
                r8.L$1 = r4
                r8.L$2 = r0
                r8.L$3 = r3
                r8.label = r2
                java.lang.Object r2 = r6.c(r8)
                if (r2 != r9) goto L_0x010c
                return r9
            L_0x010c:
                r20 = r3
                r3 = r0
                r0 = r20
            L_0x0111:
                com.fossil.ko4 r2 = (com.fossil.ko4) r2
                java.lang.Object r6 = r2.c()
                java.util.List r6 = (java.util.List) r6
                java.lang.Object r7 = r0.getFirst()
                com.fossil.hj7 r7 = (com.fossil.hj7) r7
                r8.L$0 = r5
                r8.L$1 = r4
                r8.L$2 = r3
                r8.L$3 = r0
                r8.L$4 = r2
                r8.L$5 = r6
                r8.label = r1
                java.lang.Object r1 = r7.c(r8)
                if (r1 != r9) goto L_0x0134
                return r9
            L_0x0134:
                r13 = r0
                r12 = r2
                r14 = r3
                r15 = r4
                r7 = r5
                r11 = r6
            L_0x013a:
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r0 = r1.c()
                r6 = r0
                java.util.List r6 = (java.util.List) r6
                com.fossil.ti7 r5 = com.fossil.qj7.a()
                com.fossil.sq4$c$a r4 = new com.fossil.sq4$c$a
                r16 = 0
                r0 = r4
                r1 = r21
                r2 = r11
                r3 = r14
                r17 = r4
                r4 = r6
                r18 = r5
                r5 = r15
                r10 = r6
                r6 = r12
                r19 = r9
                r9 = r7
                r7 = r16
                r0.<init>(r1, r2, r3, r4, r5, r6, r7)
                r8.L$0 = r9
                r8.L$1 = r15
                r8.L$2 = r14
                r8.L$3 = r13
                r8.L$4 = r12
                r8.L$5 = r11
                r8.L$6 = r10
                r0 = 5
                r8.label = r0
                r1 = r17
                r0 = r18
                java.lang.Object r0 = com.fossil.vh7.a(r0, r1, r8)
                r1 = r19
                if (r0 != r1) goto L_0x017e
                return r1
            L_0x017e:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sq4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public sq4(ro4 ro4, xo4 xo4) {
        ee7.b(ro4, "challengeRepository");
        ee7.b(xo4, "friendRepository");
        this.e = ro4;
        this.f = xo4;
        String simpleName = sq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCMemberInChallengeViewM\u2026el::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> a() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> c() {
        LiveData<List<Object>> a2 = ge.a(this.c);
        ee7.a((Object) a2, "Transformations.distinctUntilChanged(this)");
        return a2;
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "loadMembers - challengeId: " + str + " - isWaiting: " + z);
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, z, null), 3, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0075 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076 A[PHI: r8 
      PHI: (r8v2 java.lang.Object) = (r8v5 java.lang.Object), (r8v1 java.lang.Object) binds: [B:19:0x0073, B:10:0x0028] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r6, boolean r7, com.fossil.fb7<? super com.fossil.r87<? extends com.fossil.hj7<com.fossil.ko4<java.util.List<com.fossil.jn4>>>, ? extends com.fossil.hj7<com.fossil.ko4<java.util.List<com.fossil.jn4>>>>> r8) {
        /*
            r5 = this;
            boolean r0 = r8 instanceof com.fossil.sq4.a
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.sq4$a r0 = (com.fossil.sq4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.sq4$a r0 = new com.fossil.sq4$a
            r0.<init>(r5, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x004c
            if (r2 == r4) goto L_0x003e
            if (r2 != r3) goto L_0x0036
            boolean r6 = r0.Z$0
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.sq4 r6 = (com.fossil.sq4) r6
            com.fossil.t87.a(r8)
            goto L_0x0076
        L_0x0036:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003e:
            boolean r7 = r0.Z$0
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r2 = r0.L$0
            com.fossil.sq4 r2 = (com.fossil.sq4) r2
            com.fossil.t87.a(r8)
            goto L_0x0065
        L_0x004c:
            com.fossil.t87.a(r8)
            com.fossil.sq4$b r8 = new com.fossil.sq4$b
            r2 = 0
            r8.<init>(r5, r7, r6, r2)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.Z$0 = r7
            r0.label = r4
            java.lang.Object r8 = com.fossil.dl7.a(r8, r0)
            if (r8 != r1) goto L_0x0064
            return r1
        L_0x0064:
            r2 = r5
        L_0x0065:
            com.fossil.hj7 r8 = (com.fossil.hj7) r8
            r0.L$0 = r2
            r0.L$1 = r6
            r0.Z$0 = r7
            r0.label = r3
            java.lang.Object r8 = r8.c(r0)
            if (r8 != r1) goto L_0x0076
            return r1
        L_0x0076:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sq4.a(java.lang.String, boolean, com.fossil.fb7):java.lang.Object");
    }
}
