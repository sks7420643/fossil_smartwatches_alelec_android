package com.fossil;

import com.fossil.ey;
import com.fossil.ix;
import com.fossil.m00;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vy implements ey, ix.a<Object> {
    @DexIgnore
    public /* final */ ey.a a;
    @DexIgnore
    public /* final */ fy<?> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public yw e;
    @DexIgnore
    public List<m00<File, ?>> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public volatile m00.a<?> h;
    @DexIgnore
    public File i;
    @DexIgnore
    public wy j;

    @DexIgnore
    public vy(fy<?> fyVar, ey.a aVar) {
        this.b = fyVar;
        this.a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ey
    public boolean a() {
        List<yw> c2 = this.b.c();
        boolean z = false;
        if (c2.isEmpty()) {
            return false;
        }
        List<Class<?>> k = this.b.k();
        if (!k.isEmpty()) {
            while (true) {
                if (this.f == null || !b()) {
                    int i2 = this.d + 1;
                    this.d = i2;
                    if (i2 >= k.size()) {
                        int i3 = this.c + 1;
                        this.c = i3;
                        if (i3 >= c2.size()) {
                            return false;
                        }
                        this.d = 0;
                    }
                    yw ywVar = c2.get(this.c);
                    Class<?> cls = k.get(this.d);
                    this.j = new wy(this.b.b(), ywVar, this.b.l(), this.b.n(), this.b.f(), this.b.b(cls), cls, this.b.i());
                    File a2 = this.b.d().a(this.j);
                    this.i = a2;
                    if (a2 != null) {
                        this.e = ywVar;
                        this.f = this.b.a(a2);
                        this.g = 0;
                    }
                } else {
                    this.h = null;
                    while (!z && b()) {
                        List<m00<File, ?>> list = this.f;
                        int i4 = this.g;
                        this.g = i4 + 1;
                        this.h = list.get(i4).a(this.i, this.b.n(), this.b.f(), this.b.i());
                        if (this.h != null && this.b.c(this.h.c.getDataClass())) {
                            this.h.c.a(this.b.j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (File.class.equals(this.b.m())) {
            return false;
        } else {
            throw new IllegalStateException("Failed to find any load path from " + this.b.h() + " to " + this.b.m());
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.f.size();
    }

    @DexIgnore
    @Override // com.fossil.ey
    public void cancel() {
        m00.a<?> aVar = this.h;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.ix.a
    public void a(Object obj) {
        this.a.a(this.e, obj, this.h.c, sw.RESOURCE_DISK_CACHE, this.j);
    }

    @DexIgnore
    @Override // com.fossil.ix.a
    public void a(Exception exc) {
        this.a.a(this.j, exc, this.h.c, sw.RESOURCE_DISK_CACHE);
    }
}
