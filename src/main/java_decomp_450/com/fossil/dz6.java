package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz6 {
    @DexIgnore
    public short a;
    @DexIgnore
    public short b;

    @DexIgnore
    public final short a() {
        return this.a;
    }

    @DexIgnore
    public final short b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dz6)) {
            return false;
        }
        dz6 dz6 = (dz6) obj;
        return this.a == dz6.a && this.b == dz6.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateDWMModel(minValue=" + ((int) this.a) + ", maxValue=" + ((int) this.b) + ")";
    }
}
