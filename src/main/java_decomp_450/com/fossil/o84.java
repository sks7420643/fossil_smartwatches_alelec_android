package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o84 implements p84 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public String c;

    @DexIgnore
    public o84(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.p84
    public String a() {
        if (!this.b) {
            this.c = t34.l(this.a);
            this.b = true;
        }
        String str = this.c;
        if (str != null) {
            return str;
        }
        return null;
    }
}
