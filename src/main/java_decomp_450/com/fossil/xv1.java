package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xv1 implements pw1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ sw1 b;
    @DexIgnore
    public AlarmManager c;
    @DexIgnore
    public /* final */ dw1 d;
    @DexIgnore
    public /* final */ by1 e;

    @DexIgnore
    public xv1(Context context, sw1 sw1, by1 by1, dw1 dw1) {
        this(context, sw1, (AlarmManager) context.getSystemService(Alarm.TABLE_NAME), by1, dw1);
    }

    @DexIgnore
    public boolean a(Intent intent) {
        return PendingIntent.getBroadcast(this.a, 0, intent, 536870912) != null;
    }

    @DexIgnore
    @Override // com.fossil.pw1
    public void a(pu1 pu1, int i) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", pu1.a());
        builder.appendQueryParameter("priority", String.valueOf(hy1.a(pu1.c())));
        if (pu1.b() != null) {
            builder.appendQueryParameter(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(pu1.b(), 0));
        }
        Intent intent = new Intent(this.a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (a(intent)) {
            kv1.a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", pu1);
            return;
        }
        long b2 = this.b.b(pu1);
        long a2 = this.d.a(pu1.c(), b2, i);
        kv1.a("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", pu1, Long.valueOf(a2), Long.valueOf(b2), Integer.valueOf(i));
        this.c.set(3, this.e.a() + a2, PendingIntent.getBroadcast(this.a, 0, intent, 0));
    }

    @DexIgnore
    public xv1(Context context, sw1 sw1, AlarmManager alarmManager, by1 by1, dw1 dw1) {
        this.a = context;
        this.b = sw1;
        this.c = alarmManager;
        this.e = by1;
        this.d = dw1;
    }
}
