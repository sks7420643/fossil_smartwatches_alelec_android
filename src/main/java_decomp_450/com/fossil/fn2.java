package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fn2 extends wm2 implements en2 {
    @DexIgnore
    public static en2 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IMarkerDelegate");
        if (queryLocalInterface instanceof en2) {
            return (en2) queryLocalInterface;
        }
        return new gn2(iBinder);
    }
}
