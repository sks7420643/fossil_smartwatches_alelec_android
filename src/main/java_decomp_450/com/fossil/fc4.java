package com.fossil;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import org.joda.time.DateTimeFieldType;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fc4 {
    @DexIgnore
    public static /* final */ String[] c; // = {vt7.ANY_MARKER, "FCM", "GCM", ""};
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public fc4(l14 l14) {
        this.a = l14.b().getSharedPreferences("com.google.android.gms.appid", 0);
        this.b = a(l14);
    }

    @DexIgnore
    public static String a(l14 l14) {
        String c2 = l14.d().c();
        if (c2 != null) {
            return c2;
        }
        String b2 = l14.d().b();
        if (!b2.startsWith("1:") && !b2.startsWith("2:")) {
            return b2;
        }
        String[] split = b2.split(":");
        if (split.length != 4) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public final String b() {
        String string;
        synchronized (this.a) {
            string = this.a.getString("|S|id", null);
        }
        return string;
    }

    @DexIgnore
    public final String c() {
        synchronized (this.a) {
            String string = this.a.getString("|S||P|", null);
            if (string == null) {
                return null;
            }
            PublicKey b2 = b(string);
            if (b2 == null) {
                return null;
            }
            return a(b2);
        }
    }

    @DexIgnore
    public String d() {
        synchronized (this.a) {
            String[] strArr = c;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                String string = this.a.getString(a(this.b, strArr[i]), null);
                if (string == null || string.isEmpty()) {
                    i++;
                } else {
                    if (string.startsWith("{")) {
                        string = a(string);
                    }
                    return string;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public final PublicKey b(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 8)));
        } catch (IllegalArgumentException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            Log.w("ContentValues", "Invalid key stored " + e);
            return null;
        }
    }

    @DexIgnore
    public final String a(String str, String str2) {
        return "|T|" + str + "|" + str2;
    }

    @DexIgnore
    public final String a(String str) {
        try {
            return new JSONObject(str).getString("token");
        } catch (JSONException unused) {
            return null;
        }
    }

    @DexIgnore
    public String a() {
        synchronized (this.a) {
            String b2 = b();
            if (b2 != null) {
                return b2;
            }
            return c();
        }
    }

    @DexIgnore
    public static String a(PublicKey publicKey) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(publicKey.getEncoded());
            digest[0] = (byte) (((digest[0] & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) + 112) & 255);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException unused) {
            Log.w("ContentValues", "Unexpected error, device missing required algorithms");
            return null;
        }
    }
}
