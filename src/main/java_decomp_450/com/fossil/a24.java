package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class a24 implements f24 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public a24(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static f24 a(Object obj) {
        return new a24(obj);
    }

    @DexIgnore
    @Override // com.fossil.f24
    public Object a(d24 d24) {
        Object obj = this.a;
        c24.b(obj, d24);
        return obj;
    }
}
