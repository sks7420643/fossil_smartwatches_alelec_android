package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr4 implements Factory<gr4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;

    @DexIgnore
    public hr4(Provider<ro4> provider, Provider<ch5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static hr4 a(Provider<ro4> provider, Provider<ch5> provider2) {
        return new hr4(provider, provider2);
    }

    @DexIgnore
    public static gr4 a(ro4 ro4, ch5 ch5) {
        return new gr4(ro4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public gr4 get() {
        return a(this.a.get(), this.b.get());
    }
}
