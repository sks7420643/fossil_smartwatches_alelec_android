package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qf0 createFromParcel(Parcel parcel) {
            return new qf0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qf0[] newArray(int i) {
            return new qf0[i];
        }
    }

    @DexIgnore
    public qf0(ic0 ic0, df0 df0) {
        super(ic0, df0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            df0 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.a());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    wl0.h.a(e);
                }
                String jSONObject4 = jSONObject2.toString();
                ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = b21.x.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            ee7.a();
            throw null;
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ qf0(android.os.Parcel r3, com.fossil.zd7 r4) {
        /*
            r2 = this;
            java.lang.Class<com.fossil.ic0> r4 = com.fossil.ic0.class
            java.lang.ClassLoader r4 = r4.getClassLoader()
            android.os.Parcelable r4 = r3.readParcelable(r4)
            r0 = 0
            if (r4 == 0) goto L_0x002a
            java.lang.String r1 = "parcel.readParcelable<Wo\u2026class.java.classLoader)!!"
            com.fossil.ee7.a(r4, r1)
            com.fossil.ic0 r4 = (com.fossil.ic0) r4
            java.lang.Class<com.fossil.df0> r1 = com.fossil.df0.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r3 = r3.readParcelable(r1)
            if (r3 == 0) goto L_0x0026
            com.fossil.df0 r3 = (com.fossil.df0) r3
            r2.<init>(r4, r3)
            return
        L_0x0026:
            com.fossil.ee7.a()
            throw r0
        L_0x002a:
            com.fossil.ee7.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qf0.<init>(android.os.Parcel, com.fossil.zd7):void");
    }
}
