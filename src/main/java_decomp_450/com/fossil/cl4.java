package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cl4 {
    @DexIgnore
    public /* final */ ti7 a; // = qj7.b();
    @DexIgnore
    public /* final */ ti7 b; // = qj7.a();
    @DexIgnore
    public /* final */ tk7 c; // = qj7.c();
    @DexIgnore
    public /* final */ yi7 d; // = zi7.a();

    @DexIgnore
    public final ti7 b() {
        return this.b;
    }

    @DexIgnore
    public final ti7 c() {
        return this.a;
    }

    @DexIgnore
    public final tk7 d() {
        return this.c;
    }

    @DexIgnore
    public final yi7 e() {
        return this.d;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public void g() {
    }
}
