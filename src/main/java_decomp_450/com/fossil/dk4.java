package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dk4 implements Factory<AuthApiGuestService> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public dk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static dk4 a(wj4 wj4) {
        return new dk4(wj4);
    }

    @DexIgnore
    public static AuthApiGuestService b(wj4 wj4) {
        AuthApiGuestService d = wj4.d();
        c87.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AuthApiGuestService get() {
        return b(this.a);
    }
}
