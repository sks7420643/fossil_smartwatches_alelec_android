package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a87<T> implements Factory<T> {
    @DexIgnore
    public /* final */ T a;

    /*
    static {
        new a87(null);
    }
    */

    @DexIgnore
    public a87(T t) {
        this.a = t;
    }

    @DexIgnore
    public static <T> Factory<T> a(T t) {
        c87.a(t, "instance cannot be null");
        return new a87(t);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        return this.a;
    }
}
