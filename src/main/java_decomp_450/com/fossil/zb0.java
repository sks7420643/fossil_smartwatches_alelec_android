package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zb0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ zg0 d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public zb0 createFromParcel(Parcel parcel) {
            return new zb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public zb0[] newArray(int i) {
            return new zb0[i];
        }
    }

    @DexIgnore
    public zb0(byte b, int i, zg0 zg0, String str) {
        super(cb0.IFTTT_APP, b, i);
        this.d = zg0;
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(zb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            zb0 zb0 = (zb0) obj;
            if (this.d != zb0.d) {
                return false;
            }
            String str = this.e;
            String str2 = zb0.e;
            if (str != null) {
                return !str.contentEquals(str2);
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest");
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        int hashCode = this.d.hashCode();
        return this.e.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
    }

    @DexIgnore
    public /* synthetic */ zb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = zg0.values()[parcel.readInt()];
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
        } else {
            ee7.a();
            throw null;
        }
    }
}
