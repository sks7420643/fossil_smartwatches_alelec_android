package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls0 extends zk0 {
    @DexIgnore
    public /* final */ vg0[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ls0(ri1 ri1, en0 en0, vg0[] vg0Arr, String str, int i) {
        super(ri1, en0, wm0.B0, (i & 8) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = vg0Arr;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        boolean z = false;
        for (vg0 vg0 : this.C) {
            for (x90 x90 : vg0.getReplyMessageGroup().getReplyMessages()) {
                da0 messageIcon = x90.getMessageIcon();
                if (!messageIcon.e()) {
                    copyOnWriteArrayList.addIfAbsent(messageIcon);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new gg0[0]);
        if (array != null) {
            gg0[] gg0Arr = (gg0[]) array;
            if (gg0Arr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    zr0 zr0 = zr0.d;
                    r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.NOTIFICATION.a));
                    if (r60 == null) {
                        r60 = b21.x.d();
                    }
                    zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.C0, true, 1795, zr0.a(gg0Arr, (short) 1795, r60), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new dj0(this), new bl0(this), new ym0(this), (gd7) null, (gd7) null, 48, (Object) null);
                } catch (f41 e) {
                    wl0.h.a(e);
                    a(eu0.a(((zk0) this).v, null, is0.INCOMPATIBLE_FIRMWARE, null, 5));
                }
            } else {
                m();
            }
        } else {
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.g5, yz0.a(this.C));
    }

    @DexIgnore
    public final void m() {
        short b = gq0.b.b(((zk0) this).w.u, pb1.REPLY_MESSAGES_FILE);
        try {
            ri0 ri0 = ri0.d;
            r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.REPLY_MESSAGES_FILE.a));
            if (r60 == null) {
                r60 = new r60((byte) 2, (byte) 0);
            }
            zk0.a(this, new s81(((zk0) this).w, ((zk0) this).x, wm0.D0, true, b, ri0.a(b, r60, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((zk0) this).z, 64), new uo0(this), new qq0(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
        } catch (f41 e) {
            wl0.h.a(e);
            a(eu0.a(((zk0) this).v, null, is0.UNSUPPORTED_FORMAT, null, 5));
        }
    }
}
