package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b21 {
    @DexIgnore
    public static /* final */ Charset a; // = sg7.a;
    @DexIgnore
    public static /* final */ Locale b;
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ UUID d; // = UUID.fromString("3dda0001-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ UUID f; // = UUID.fromString("3dda0002-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID g; // = UUID.fromString("3dda0003-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID h; // = UUID.fromString("3dda0004-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID i; // = UUID.fromString("3dda0005-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID j; // = UUID.fromString("3dda0006-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID k; // = UUID.fromString("3dda0007-957f-7d4a-34a6-74696673696d");
    @DexIgnore
    public static /* final */ UUID l; // = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID m; // = UUID.fromString("00002a25-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID n; // = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID o; // = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID p; // = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ UUID q; // = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    @DexIgnore
    public static /* final */ f31 r; // = f31.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY;
    @DexIgnore
    public static /* final */ r60 s; // = new r60((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ r60 t; // = new r60((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ r60 u; // = new r60((byte) 1, (byte) 0);
    @DexIgnore
    public static /* final */ r60 v; // = new r60((byte) 0, (byte) 0);
    @DexIgnore
    public static /* final */ x91 w; // = new x91(12, 12, 0, 600);
    @DexIgnore
    public static /* final */ b21 x; // = new b21();

    /*
    static {
        Locale locale = Locale.ROOT;
        ee7.a((Object) locale, "Locale.ROOT");
        b = locale;
        String property = System.getProperty("line.separator");
        if (property == null) {
            property = "\n";
        }
        c = property;
        UUID.fromString("00001805-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002a2b-0000-1000-8000-00805f9b34fb");
        UUID.fromString("00002a0f-0000-1000-8000-00805f9b34fb");
        UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
        String a2 = mh7.a("3dda0001-957f-7d4a-34a6-74696673696d", ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "", false, 4, (Object) null);
        Locale locale2 = b;
        if (a2 != null) {
            String upperCase = a2.toUpperCase(locale2);
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            e = upperCase;
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }
    */

    @DexIgnore
    public final UUID a() {
        return p;
    }

    @DexIgnore
    public final f31 b() {
        return r;
    }

    @DexIgnore
    public final Charset c() {
        return a;
    }

    @DexIgnore
    public final r60 d() {
        return v;
    }

    @DexIgnore
    public final Locale e() {
        return b;
    }

    @DexIgnore
    public final r60 f() {
        return s;
    }

    @DexIgnore
    public final UUID g() {
        return n;
    }

    @DexIgnore
    public final UUID h() {
        return l;
    }

    @DexIgnore
    public final UUID i() {
        return m;
    }

    @DexIgnore
    public final UUID j() {
        return o;
    }

    @DexIgnore
    public final r60 k() {
        return t;
    }

    @DexIgnore
    public final UUID l() {
        return q;
    }

    @DexIgnore
    public final x91 m() {
        return w;
    }

    @DexIgnore
    public final UUID n() {
        return j;
    }

    @DexIgnore
    public final UUID o() {
        return i;
    }

    @DexIgnore
    public final UUID p() {
        return f;
    }

    @DexIgnore
    public final UUID q() {
        return g;
    }

    @DexIgnore
    public final UUID r() {
        return h;
    }

    @DexIgnore
    public final UUID s() {
        return k;
    }

    @DexIgnore
    public final UUID t() {
        return d;
    }

    @DexIgnore
    public final String u() {
        return e;
    }

    @DexIgnore
    public final String v() {
        return c;
    }

    @DexIgnore
    public final r60 w() {
        return u;
    }
}
