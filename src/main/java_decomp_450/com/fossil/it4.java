package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.LimitSlopeSwipeRefresh;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it4 extends go5 implements fr5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<g25> f;
    @DexIgnore
    public kt4 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public mt4 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return it4.q;
        }

        @DexIgnore
        public final it4 b() {
            return new it4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ g25 a;
        @DexIgnore
        public /* final */ /* synthetic */ it4 b;

        @DexIgnore
        public b(g25 g25, it4 it4) {
            this.a = g25;
            this.b = it4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i) {
            super.b(i);
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886235);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1), Integer.valueOf(this.b.f1().getItemCount())}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView = this.a.s;
            ee7.a((Object) flexibleTextView, "ftvIndicator");
            flexibleTextView.setText(format);
            it4.c(this.b).a(i, this.b.f1().getItemCount());
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ g25 a;
        @DexIgnore
        public /* final */ /* synthetic */ it4 b;

        @DexIgnore
        public c(g25 g25, it4 it4) {
            this.a = g25;
            this.b = it4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            it4.c(this.b).b(this.b.j, this.b.f1().getItemCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<List<? extends oo4>> {
        @DexIgnore
        public /* final */ /* synthetic */ it4 a;

        @DexIgnore
        public d(it4 it4) {
            this.a = it4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<oo4> list) {
            mt4 f1 = this.a.f1();
            ee7.a((Object) list, "it");
            f1.a(list);
            we7 we7 = we7.a;
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886235);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(this.a.j + 1), Integer.valueOf(list.size())}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            g25 g25 = (g25) it4.a(this.a).a();
            if (g25 != null) {
                FlexibleTextView flexibleTextView = g25.s;
                ee7.a((Object) flexibleTextView, "ftvIndicator");
                if (format != null) {
                    String upperCase = format.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    if (!list.isEmpty()) {
                        FlexibleTextView flexibleTextView2 = g25.r;
                        ee7.a((Object) flexibleTextView2, "ftvError");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = g25.s;
                        ee7.a((Object) flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(0);
                        FlexibleTextView flexibleTextView4 = g25.t;
                        ee7.a((Object) flexibleTextView4, "ftvNotice");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ it4 a;

        @DexIgnore
        public e(it4 it4) {
            this.a = it4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            g25 g25 = (g25) it4.a(this.a).a();
            if (g25 != null) {
                ee7.a((Object) bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = g25.q;
                    ee7.a((Object) flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    ViewPager2 viewPager2 = g25.v;
                    ee7.a((Object) viewPager2, "subTabs");
                    viewPager2.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = g25.s;
                    ee7.a((Object) flexibleTextView2, "ftvIndicator");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = g25.t;
                    ee7.a((Object) flexibleTextView3, "ftvNotice");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = g25.q;
                ee7.a((Object) flexibleTextView4, "ftvEmpty");
                flexibleTextView4.setVisibility(8);
                ViewPager2 viewPager22 = g25.v;
                ee7.a((Object) viewPager22, "subTabs");
                viewPager22.setVisibility(0);
                FlexibleTextView flexibleTextView5 = g25.s;
                ee7.a((Object) flexibleTextView5, "ftvIndicator");
                flexibleTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView6 = g25.t;
                ee7.a((Object) flexibleTextView6, "ftvNotice");
                flexibleTextView6.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ it4 a;

        @DexIgnore
        public f(it4 it4) {
            this.a = it4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            g25 g25 = (g25) it4.a(this.a).a();
            if (g25 != null) {
                LimitSlopeSwipeRefresh limitSlopeSwipeRefresh = g25.w;
                ee7.a((Object) limitSlopeSwipeRefresh, "swipe");
                ee7.a((Object) bool, "it");
                limitSlopeSwipeRefresh.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ it4 a;

        @DexIgnore
        public g(it4 it4) {
            this.a = it4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            if (r87 != null) {
                boolean booleanValue = r87.getFirst().booleanValue();
                ServerError serverError = (ServerError) r87.getSecond();
                g25 g25 = (g25) it4.a(this.a).a();
                if (g25 != null) {
                    FlexibleTextView flexibleTextView = g25.q;
                    ee7.a((Object) flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(8);
                    if (booleanValue) {
                        FlexibleTextView flexibleTextView2 = g25.t;
                        ee7.a((Object) flexibleTextView2, "ftvNotice");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = g25.s;
                        ee7.a((Object) flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(8);
                        FlexibleTextView flexibleTextView4 = g25.r;
                        ee7.a((Object) flexibleTextView4, "ftvError");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = g25.r;
                    ee7.a((Object) flexibleTextView5, "ftvError");
                    flexibleTextView5.setVisibility(8);
                    if (this.a.isResumed()) {
                        FlexibleTextView flexibleTextView6 = g25.r;
                        ee7.a((Object) flexibleTextView6, "ftvError");
                        String a2 = ig5.a(flexibleTextView6.getContext(), 2131886227);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            Toast.makeText(activity, a2, 1).show();
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String simpleName = it4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCHistoryFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(it4 it4) {
        qw6<g25> qw6 = it4.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ kt4 c(it4 it4) {
        kt4 kt4 = it4.g;
        if (kt4 != null) {
            return kt4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final mt4 f1() {
        mt4 mt4 = this.i;
        if (mt4 != null) {
            return mt4;
        }
        ee7.d("historyAdapter");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        qw6<g25> qw6 = this.f;
        if (qw6 != null) {
            g25 a2 = qw6.a();
            if (a2 != null) {
                this.i = new mt4(this);
                ViewPager2 viewPager2 = a2.v;
                ee7.a((Object) viewPager2, "this");
                mt4 mt4 = this.i;
                if (mt4 != null) {
                    viewPager2.setAdapter(mt4);
                    viewPager2.setOffscreenPageLimit(5);
                    a2.v.a(new b(a2, this));
                    a2.w.setOnRefreshListener(new c(a2, this));
                    return;
                }
                ee7.d("historyAdapter");
                throw null;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        kt4 kt4 = this.g;
        if (kt4 != null) {
            kt4.c().a(getViewLifecycleOwner(), new d(this));
            kt4 kt42 = this.g;
            if (kt42 != null) {
                kt42.a().a(getViewLifecycleOwner(), new e(this));
                kt4 kt43 = this.g;
                if (kt43 != null) {
                    kt43.d().a(getViewLifecycleOwner(), new f(this));
                    kt4 kt44 = this.g;
                    if (kt44 != null) {
                        kt44.b().a(getViewLifecycleOwner(), new g(this));
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().A().a(this);
        rj4 rj4 = this.h;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(kt4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026oryViewModel::class.java)");
            this.g = (kt4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        g25 g25 = (g25) qb.a(layoutInflater, 2131558568, viewGroup, false, a1());
        this.f = new qw6<>(this, g25);
        ee7.a((Object) g25, "binding");
        return g25.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        g1();
        h1();
    }

    @DexIgnore
    @Override // com.fossil.fr5
    public void a(mn4 mn4) {
        ee7.b(mn4, "challenge");
        BCOverviewLeaderBoardActivity.y.a(this, mn4, mn4.f());
    }
}
