package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class sh3 implements Runnable {
    @DexIgnore
    public /* final */ ph3 a;
    @DexIgnore
    public /* final */ nm3 b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public sh3(ph3 ph3, nm3 nm3, Bundle bundle) {
        this.a = ph3;
        this.b = nm3;
        this.c = bundle;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
