package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.bw2;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx2<T> implements cy2<T> {
    @DexIgnore
    public static /* final */ int[] q; // = new int[0];
    @DexIgnore
    public static /* final */ Unsafe r; // = bz2.c();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ jx2 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int[] i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ rx2 l;
    @DexIgnore
    public /* final */ sw2 m;
    @DexIgnore
    public /* final */ uy2<?, ?> n;
    @DexIgnore
    public /* final */ pv2<?> o;
    @DexIgnore
    public /* final */ gx2 p;

    @DexIgnore
    public nx2(int[] iArr, Object[] objArr, int i2, int i3, jx2 jx2, boolean z, boolean z2, int[] iArr2, int i4, int i5, rx2 rx2, sw2 sw2, uy2<?, ?> uy2, pv2<?> pv2, gx2 gx2) {
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        boolean z3 = jx2 instanceof bw2;
        this.g = z;
        this.f = pv2 != null && pv2.a(jx2);
        this.h = false;
        this.i = iArr2;
        this.j = i4;
        this.k = i5;
        this.l = rx2;
        this.m = sw2;
        this.n = uy2;
        this.o = pv2;
        this.e = jx2;
        this.p = gx2;
    }

    @DexIgnore
    public static <T> nx2<T> a(Class<T> cls, hx2 hx2, rx2 rx2, sw2 sw2, uy2<?, ?> uy2, pv2<?> pv2, gx2 gx2) {
        int i2;
        char c2;
        int[] iArr;
        char c3;
        char c4;
        char c5;
        char c6;
        int i3;
        int i4;
        int i5;
        char c7;
        int i6;
        String str;
        Object[] objArr;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z;
        int i11;
        Field field;
        int i12;
        char charAt;
        int i13;
        char c8;
        Field field2;
        Field field3;
        int i14;
        char charAt2;
        int i15;
        char charAt3;
        int i16;
        char charAt4;
        int i17;
        char charAt5;
        int i18;
        char charAt6;
        int i19;
        char charAt7;
        int i20;
        char charAt8;
        int i21;
        char charAt9;
        int i22;
        char charAt10;
        int i23;
        char charAt11;
        int i24;
        char charAt12;
        int i25;
        char charAt13;
        if (hx2 instanceof ay2) {
            ay2 ay2 = (ay2) hx2;
            char c9 = 0;
            boolean z2 = ay2.zza() == bw2.f.j;
            String a2 = ay2.a();
            int length = a2.length();
            if (a2.charAt(0) >= '\ud800') {
                int i26 = 1;
                while (true) {
                    i2 = i26 + 1;
                    if (a2.charAt(i26) < '\ud800') {
                        break;
                    }
                    i26 = i2;
                }
            } else {
                i2 = 1;
            }
            int i27 = i2 + 1;
            char charAt14 = a2.charAt(i2);
            if (charAt14 >= '\ud800') {
                char c10 = charAt14 & '\u1fff';
                int i28 = 13;
                while (true) {
                    i25 = i27 + 1;
                    charAt13 = a2.charAt(i27);
                    if (charAt13 < '\ud800') {
                        break;
                    }
                    c10 |= (charAt13 & '\u1fff') << i28;
                    i28 += 13;
                    i27 = i25;
                }
                charAt14 = c10 | (charAt13 << i28);
                i27 = i25;
            }
            if (charAt14 == 0) {
                iArr = q;
                i3 = 0;
                c6 = 0;
                c5 = 0;
                c4 = 0;
                c3 = 0;
                c2 = 0;
            } else {
                int i29 = i27 + 1;
                char charAt15 = a2.charAt(i27);
                if (charAt15 >= '\ud800') {
                    char c11 = charAt15 & '\u1fff';
                    int i30 = 13;
                    while (true) {
                        i24 = i29 + 1;
                        charAt12 = a2.charAt(i29);
                        if (charAt12 < '\ud800') {
                            break;
                        }
                        c11 |= (charAt12 & '\u1fff') << i30;
                        i30 += 13;
                        i29 = i24;
                    }
                    charAt15 = c11 | (charAt12 << i30);
                    i29 = i24;
                }
                int i31 = i29 + 1;
                char charAt16 = a2.charAt(i29);
                if (charAt16 >= '\ud800') {
                    char c12 = charAt16 & '\u1fff';
                    int i32 = 13;
                    while (true) {
                        i23 = i31 + 1;
                        charAt11 = a2.charAt(i31);
                        if (charAt11 < '\ud800') {
                            break;
                        }
                        c12 |= (charAt11 & '\u1fff') << i32;
                        i32 += 13;
                        i31 = i23;
                    }
                    charAt16 = c12 | (charAt11 << i32);
                    i31 = i23;
                }
                int i33 = i31 + 1;
                c6 = a2.charAt(i31);
                if (c6 >= '\ud800') {
                    char c13 = c6 & '\u1fff';
                    int i34 = 13;
                    while (true) {
                        i22 = i33 + 1;
                        charAt10 = a2.charAt(i33);
                        if (charAt10 < '\ud800') {
                            break;
                        }
                        c13 |= (charAt10 & '\u1fff') << i34;
                        i34 += 13;
                        i33 = i22;
                    }
                    c6 = c13 | (charAt10 << i34);
                    i33 = i22;
                }
                int i35 = i33 + 1;
                c5 = a2.charAt(i33);
                if (c5 >= '\ud800') {
                    char c14 = c5 & '\u1fff';
                    int i36 = 13;
                    while (true) {
                        i21 = i35 + 1;
                        charAt9 = a2.charAt(i35);
                        if (charAt9 < '\ud800') {
                            break;
                        }
                        c14 |= (charAt9 & '\u1fff') << i36;
                        i36 += 13;
                        i35 = i21;
                    }
                    c5 = c14 | (charAt9 << i36);
                    i35 = i21;
                }
                int i37 = i35 + 1;
                c4 = a2.charAt(i35);
                if (c4 >= '\ud800') {
                    char c15 = c4 & '\u1fff';
                    int i38 = 13;
                    while (true) {
                        i20 = i37 + 1;
                        charAt8 = a2.charAt(i37);
                        if (charAt8 < '\ud800') {
                            break;
                        }
                        c15 |= (charAt8 & '\u1fff') << i38;
                        i38 += 13;
                        i37 = i20;
                    }
                    c4 = c15 | (charAt8 << i38);
                    i37 = i20;
                }
                int i39 = i37 + 1;
                c3 = a2.charAt(i37);
                if (c3 >= '\ud800') {
                    char c16 = c3 & '\u1fff';
                    int i40 = 13;
                    while (true) {
                        i19 = i39 + 1;
                        charAt7 = a2.charAt(i39);
                        if (charAt7 < '\ud800') {
                            break;
                        }
                        c16 |= (charAt7 & '\u1fff') << i40;
                        i40 += 13;
                        i39 = i19;
                    }
                    c3 = c16 | (charAt7 << i40);
                    i39 = i19;
                }
                int i41 = i39 + 1;
                char charAt17 = a2.charAt(i39);
                if (charAt17 >= '\ud800') {
                    char c17 = charAt17 & '\u1fff';
                    int i42 = 13;
                    while (true) {
                        i18 = i41 + 1;
                        charAt6 = a2.charAt(i41);
                        if (charAt6 < '\ud800') {
                            break;
                        }
                        c17 |= (charAt6 & '\u1fff') << i42;
                        i42 += 13;
                        i41 = i18;
                    }
                    charAt17 = c17 | (charAt6 << i42);
                    i41 = i18;
                }
                int i43 = i41 + 1;
                c2 = a2.charAt(i41);
                if (c2 >= '\ud800') {
                    char c18 = c2 & '\u1fff';
                    int i44 = i43;
                    int i45 = 13;
                    while (true) {
                        i17 = i44 + 1;
                        charAt5 = a2.charAt(i44);
                        if (charAt5 < '\ud800') {
                            break;
                        }
                        c18 |= (charAt5 & '\u1fff') << i45;
                        i45 += 13;
                        i44 = i17;
                    }
                    c2 = c18 | (charAt5 << i45);
                    i43 = i17;
                }
                i3 = (charAt15 << 1) + charAt16;
                iArr = new int[(c2 + c3 + charAt17)];
                c9 = charAt15;
                i27 = i43;
            }
            Unsafe unsafe = r;
            Object[] b2 = ay2.b();
            Class<?> cls2 = ay2.zzc().getClass();
            int[] iArr2 = new int[(c4 * 3)];
            Object[] objArr2 = new Object[(c4 << 1)];
            int i46 = c2 + c3;
            int i47 = i3;
            char c19 = c2;
            int i48 = i27;
            int i49 = i46;
            int i50 = 0;
            int i51 = 0;
            while (i48 < length) {
                int i52 = i48 + 1;
                int charAt18 = a2.charAt(i48);
                if (charAt18 >= 55296) {
                    int i53 = charAt18 & 8191;
                    int i54 = i52;
                    int i55 = 13;
                    while (true) {
                        i16 = i54 + 1;
                        charAt4 = a2.charAt(i54);
                        i4 = length;
                        if (charAt4 < '\ud800') {
                            break;
                        }
                        i53 |= (charAt4 & '\u1fff') << i55;
                        i55 += 13;
                        i54 = i16;
                        length = i4;
                    }
                    charAt18 = i53 | (charAt4 << i55);
                    i5 = i16;
                } else {
                    i4 = length;
                    i5 = i52;
                }
                int i56 = i5 + 1;
                char charAt19 = a2.charAt(i5);
                if (charAt19 >= '\ud800') {
                    char c20 = charAt19 & '\u1fff';
                    int i57 = i56;
                    int i58 = 13;
                    while (true) {
                        i15 = i57 + 1;
                        charAt3 = a2.charAt(i57);
                        c7 = c2;
                        if (charAt3 < '\ud800') {
                            break;
                        }
                        c20 |= (charAt3 & '\u1fff') << i58;
                        i58 += 13;
                        i57 = i15;
                        c2 = c7;
                    }
                    charAt19 = c20 | (charAt3 << i58);
                    i6 = i15;
                } else {
                    c7 = c2;
                    i6 = i56;
                }
                char c21 = charAt19 & '\u00ff';
                if ((charAt19 & '\u0400') != 0) {
                    iArr[i50] = i51;
                    i50++;
                }
                if (c21 >= '3') {
                    int i59 = i6 + 1;
                    char charAt20 = a2.charAt(i6);
                    char c22 = '\ud800';
                    if (charAt20 >= '\ud800') {
                        char c23 = charAt20 & '\u1fff';
                        int i60 = 13;
                        while (true) {
                            i14 = i59 + 1;
                            charAt2 = a2.charAt(i59);
                            if (charAt2 < c22) {
                                break;
                            }
                            c23 |= (charAt2 & '\u1fff') << i60;
                            i60 += 13;
                            i59 = i14;
                            c22 = '\ud800';
                        }
                        charAt20 = c23 | (charAt2 << i60);
                        i59 = i14;
                    }
                    int i61 = c21 - '3';
                    if (i61 == 9 || i61 == 17) {
                        c8 = 1;
                        objArr2[((i51 / 3) << 1) + 1] = b2[i47];
                        i47++;
                    } else {
                        if (i61 == 12 && !z2) {
                            objArr2[((i51 / 3) << 1) + 1] = b2[i47];
                            i47++;
                        }
                        c8 = 1;
                    }
                    int i62 = charAt20 << c8;
                    Object obj = b2[i62];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = a(cls2, (String) obj);
                        b2[i62] = field2;
                    }
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i63 = i62 + 1;
                    Object obj2 = b2[i63];
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = a(cls2, (String) obj2);
                        b2[i63] = field3;
                    }
                    str = a2;
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    z = z2;
                    objArr = objArr2;
                    i8 = objectFieldOffset;
                    i7 = i59;
                    i10 = 0;
                } else {
                    int i64 = i47 + 1;
                    Field a3 = a(cls2, (String) b2[i47]);
                    if (c21 == '\t' || c21 == 17) {
                        objArr2[((i51 / 3) << 1) + 1] = a3.getType();
                    } else {
                        if (c21 == 27 || c21 == '1') {
                            i13 = i64 + 1;
                            objArr2[((i51 / 3) << 1) + 1] = b2[i64];
                        } else if (c21 == '\f' || c21 == 30 || c21 == ',') {
                            if (!z2) {
                                i13 = i64 + 1;
                                objArr2[((i51 / 3) << 1) + 1] = b2[i64];
                            }
                        } else if (c21 == '2') {
                            int i65 = c19 + 1;
                            iArr[c19] = i51;
                            int i66 = (i51 / 3) << 1;
                            i13 = i64 + 1;
                            objArr2[i66] = b2[i64];
                            if ((charAt19 & '\u0800') != 0) {
                                i64 = i13 + 1;
                                objArr2[i66 + 1] = b2[i13];
                                c19 = i65;
                            } else {
                                c19 = i65;
                            }
                        }
                        i11 = i13;
                        i8 = (int) unsafe.objectFieldOffset(a3);
                        if ((charAt19 & '\u1000') == '\u1000' || c21 > 17) {
                            str = a2;
                            z = z2;
                            objArr = objArr2;
                            i9 = 1048575;
                            i7 = i6;
                            i10 = 0;
                        } else {
                            int i67 = i6 + 1;
                            char charAt21 = a2.charAt(i6);
                            if (charAt21 >= '\ud800') {
                                char c24 = charAt21 & '\u1fff';
                                int i68 = 13;
                                while (true) {
                                    i12 = i67 + 1;
                                    charAt = a2.charAt(i67);
                                    if (charAt < '\ud800') {
                                        break;
                                    }
                                    c24 |= (charAt & '\u1fff') << i68;
                                    i68 += 13;
                                    i67 = i12;
                                }
                                charAt21 = c24 | (charAt << i68);
                                i67 = i12;
                            }
                            int i69 = (c9 << 1) + (charAt21 / ' ');
                            Object obj3 = b2[i69];
                            str = a2;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = a(cls2, (String) obj3);
                                b2[i69] = field;
                            }
                            z = z2;
                            objArr = objArr2;
                            i10 = charAt21 % ' ';
                            i7 = i67;
                            i9 = (int) unsafe.objectFieldOffset(field);
                        }
                        if (c21 >= 18 && c21 <= '1') {
                            iArr[i49] = i8;
                            i49++;
                        }
                        i47 = i11;
                    }
                    i11 = i64;
                    i8 = (int) unsafe.objectFieldOffset(a3);
                    if ((charAt19 & '\u1000') == '\u1000') {
                    }
                    str = a2;
                    z = z2;
                    objArr = objArr2;
                    i9 = 1048575;
                    i7 = i6;
                    i10 = 0;
                    iArr[i49] = i8;
                    i49++;
                    i47 = i11;
                }
                int i70 = i51 + 1;
                iArr2[i51] = charAt18;
                int i71 = i70 + 1;
                iArr2[i70] = ((charAt19 & '\u0100') != 0 ? SQLiteDatabase.CREATE_IF_NECESSARY : 0) | ((charAt19 & '\u0200') != 0 ? 536870912 : 0) | (c21 << 20) | i8;
                int i72 = i71 + 1;
                iArr2[i71] = (i10 << 20) | i9;
                i48 = i7;
                c9 = c9;
                c5 = c5;
                objArr2 = objArr;
                c2 = c7;
                c6 = c6;
                z2 = z;
                i51 = i72;
                length = i4;
                a2 = str;
            }
            return new nx2<>(iArr2, objArr2, c6, c5, ay2.zzc(), z2, false, iArr, c2, i46, rx2, sw2, uy2, pv2, gx2);
        }
        ((ny2) hx2).zza();
        throw null;
    }

    @DexIgnore
    public static <T> boolean f(T t, long j2) {
        return ((Boolean) bz2.f(t, j2)).booleanValue();
    }

    @DexIgnore
    public final void b(T t, T t2, int i2) {
        int d2 = d(i2);
        int i3 = this.a[i2];
        long j2 = (long) (d2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = bz2.f(t, j2);
            Object f3 = bz2.f(t2, j2);
            if (f2 != null && f3 != null) {
                bz2.a(t, j2, ew2.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                bz2.a(t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    @DexIgnore
    public final fw2 c(int i2) {
        return (fw2) this.b[((i2 / 3) << 1) + 1];
    }

    @DexIgnore
    public final int d(int i2) {
        return this.a[i2 + 1];
    }

    @DexIgnore
    public final int e(int i2) {
        return this.a[i2 + 2];
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final T zza() {
        return (T) this.l.zza(this.e);
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.a.length; i2 += 3) {
                int d2 = d(i2);
                long j2 = (long) (1048575 & d2);
                int i3 = this.a[i2];
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.e(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 1:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.d(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 2:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 3:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 4:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 5:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 6:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 7:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.c(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 8:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.f(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 9:
                        a(t, t2, i2);
                        break;
                    case 10:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.f(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 11:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 12:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 13:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 14:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 15:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 16:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            bz2.a((Object) t, j2, bz2.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 17:
                        a(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.m.a(t, t2, j2);
                        break;
                    case 50:
                        ey2.a(this.p, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 60:
                        b(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            bz2.a(t, j2, bz2.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 68:
                        b(t, t2, i2);
                        break;
                }
            }
            ey2.a(this.n, t, t2);
            if (this.f) {
                ey2.a(this.o, t, t2);
                return;
            }
            return;
        }
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final void zzc(T t) {
        int i2;
        int i3 = this.j;
        while (true) {
            i2 = this.k;
            if (i3 >= i2) {
                break;
            }
            long d2 = (long) (d(this.i[i3]) & 1048575);
            Object f2 = bz2.f(t, d2);
            if (f2 != null) {
                bz2.a(t, d2, this.p.b(f2));
            }
            i3++;
        }
        int length = this.i.length;
        while (i2 < length) {
            this.m.a(t, (long) this.i[i2]);
            i2++;
        }
        this.n.b(t);
        if (this.f) {
            this.o.c(t);
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.fossil.cy2] */
    /* JADX WARN: Type inference failed for: r1v21 */
    /* JADX WARN: Type inference failed for: r1v23, types: [com.fossil.cy2] */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.cy2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzd(T r19) {
        /*
            r18 = this;
            r6 = r18
            r7 = r19
            r8 = 1048575(0xfffff, float:1.469367E-39)
            r9 = 0
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r1 = 0
            r10 = 0
        L_0x000d:
            int r2 = r6.j
            r11 = 1
            if (r10 >= r2) goto L_0x012e
            int[] r2 = r6.i
            r12 = r2[r10]
            int[] r2 = r6.a
            r13 = r2[r12]
            int r14 = r6.d(r12)
            int[] r2 = r6.a
            int r3 = r12 + 2
            r2 = r2[r3]
            r3 = r2 & r8
            int r2 = r2 >>> 20
            int r15 = r11 << r2
            if (r3 == r0) goto L_0x003a
            if (r3 == r8) goto L_0x0035
            sun.misc.Unsafe r0 = com.fossil.nx2.r
            long r1 = (long) r3
            int r1 = r0.getInt(r7, r1)
        L_0x0035:
            r17 = r1
            r16 = r3
            goto L_0x003e
        L_0x003a:
            r16 = r0
            r17 = r1
        L_0x003e:
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r0 & r14
            if (r0 == 0) goto L_0x0045
            r0 = 1
            goto L_0x0046
        L_0x0045:
            r0 = 0
        L_0x0046:
            if (r0 == 0) goto L_0x0059
            r0 = r18
            r1 = r19
            r2 = r12
            r3 = r16
            r4 = r17
            r5 = r15
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x0059
            return r9
        L_0x0059:
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r0 = r0 & r14
            int r0 = r0 >>> 20
            r1 = 9
            if (r0 == r1) goto L_0x010b
            r1 = 17
            if (r0 == r1) goto L_0x010b
            r1 = 27
            if (r0 == r1) goto L_0x00df
            r1 = 60
            if (r0 == r1) goto L_0x00ce
            r1 = 68
            if (r0 == r1) goto L_0x00ce
            r1 = 49
            if (r0 == r1) goto L_0x00df
            r1 = 50
            if (r0 == r1) goto L_0x007c
            goto L_0x0126
        L_0x007c:
            com.fossil.gx2 r0 = r6.p
            r1 = r14 & r8
            long r1 = (long) r1
            java.lang.Object r1 = com.fossil.bz2.f(r7, r1)
            java.util.Map r0 = r0.zzc(r1)
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x00cb
            java.lang.Object r1 = r6.b(r12)
            com.fossil.gx2 r2 = r6.p
            com.fossil.ex2 r1 = r2.zzb(r1)
            com.fossil.iz2 r1 = r1.c
            com.fossil.pz2 r1 = r1.zza()
            com.fossil.pz2 r2 = com.fossil.pz2.MESSAGE
            if (r1 != r2) goto L_0x00cb
            r1 = 0
            java.util.Collection r0 = r0.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x00ac:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00cb
            java.lang.Object r2 = r0.next()
            if (r1 != 0) goto L_0x00c4
            com.fossil.yx2 r1 = com.fossil.yx2.a()
            java.lang.Class r3 = r2.getClass()
            com.fossil.cy2 r1 = r1.a(r3)
        L_0x00c4:
            boolean r2 = r1.zzd(r2)
            if (r2 != 0) goto L_0x00ac
            r11 = 0
        L_0x00cb:
            if (r11 != 0) goto L_0x0126
            return r9
        L_0x00ce:
            boolean r0 = r6.a(r7, r13, r12)
            if (r0 == 0) goto L_0x0126
            com.fossil.cy2 r0 = r6.a(r12)
            boolean r0 = a(r7, r14, r0)
            if (r0 != 0) goto L_0x0126
            return r9
        L_0x00df:
            r0 = r14 & r8
            long r0 = (long) r0
            java.lang.Object r0 = com.fossil.bz2.f(r7, r0)
            java.util.List r0 = (java.util.List) r0
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x0108
            com.fossil.cy2 r1 = r6.a(r12)
            r2 = 0
        L_0x00f3:
            int r3 = r0.size()
            if (r2 >= r3) goto L_0x0108
            java.lang.Object r3 = r0.get(r2)
            boolean r3 = r1.zzd(r3)
            if (r3 != 0) goto L_0x0105
            r11 = 0
            goto L_0x0108
        L_0x0105:
            int r2 = r2 + 1
            goto L_0x00f3
        L_0x0108:
            if (r11 != 0) goto L_0x0126
            return r9
        L_0x010b:
            r0 = r18
            r1 = r19
            r2 = r12
            r3 = r16
            r4 = r17
            r5 = r15
            boolean r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0126
            com.fossil.cy2 r0 = r6.a(r12)
            boolean r0 = a(r7, r14, r0)
            if (r0 != 0) goto L_0x0126
            return r9
        L_0x0126:
            int r10 = r10 + 1
            r0 = r16
            r1 = r17
            goto L_0x000d
        L_0x012e:
            boolean r0 = r6.f
            if (r0 == 0) goto L_0x013f
            com.fossil.pv2<?> r0 = r6.o
            com.fossil.qv2 r0 = r0.a(r7)
            boolean r0 = r0.e()
            if (r0 != 0) goto L_0x013f
            return r9
        L_0x013f:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.zzd(java.lang.Object):boolean");
    }

    @DexIgnore
    public static <T> float c(T t, long j2) {
        return ((Float) bz2.f(t, j2)).floatValue();
    }

    @DexIgnore
    public static <T> int d(T t, long j2) {
        return ((Integer) bz2.f(t, j2)).intValue();
    }

    @DexIgnore
    public static <T> long e(T t, long j2) {
        return ((Long) bz2.f(t, j2)).longValue();
    }

    @DexIgnore
    public final int f(int i2) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return b(i2, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.fossil.ey2.a(com.fossil.bz2.f(r10, r6), com.fossil.bz2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.fossil.bz2.b(r10, r6) == com.fossil.bz2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.fossil.bz2.b(r10, r6) == com.fossil.bz2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.fossil.ey2.a(com.fossil.bz2.f(r10, r6), com.fossil.bz2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.fossil.ey2.a(com.fossil.bz2.f(r10, r6), com.fossil.bz2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.fossil.ey2.a(com.fossil.bz2.f(r10, r6), com.fossil.bz2.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.fossil.bz2.c(r10, r6) == com.fossil.bz2.c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.fossil.bz2.b(r10, r6) == com.fossil.bz2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.fossil.bz2.a(r10, r6) == com.fossil.bz2.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.fossil.bz2.b(r10, r6) == com.fossil.bz2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.fossil.bz2.b(r10, r6) == com.fossil.bz2.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.fossil.bz2.d(r10, r6)) == java.lang.Float.floatToIntBits(com.fossil.bz2.d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.fossil.bz2.e(r10, r6)) == java.lang.Double.doubleToLongBits(com.fossil.bz2.e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.fossil.ey2.a(com.fossil.bz2.f(r10, r6), com.fossil.bz2.f(r11, r6)) != false) goto L_0x01c2;
     */
    @DexIgnore
    @Override // com.fossil.cy2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.a
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.d(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.e(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.fossil.bz2.a(r10, r4)
            int r4 = com.fossil.bz2.a(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r5 = com.fossil.bz2.f(r11, r6)
            boolean r4 = com.fossil.ey2.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r4 = com.fossil.bz2.f(r11, r6)
            boolean r3 = com.fossil.ey2.a(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r4 = com.fossil.bz2.f(r11, r6)
            boolean r3 = com.fossil.ey2.a(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r5 = com.fossil.bz2.f(r11, r6)
            boolean r4 = com.fossil.ey2.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.fossil.bz2.b(r10, r6)
            long r6 = com.fossil.bz2.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.fossil.bz2.b(r10, r6)
            long r6 = com.fossil.bz2.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r5 = com.fossil.bz2.f(r11, r6)
            boolean r4 = com.fossil.ey2.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r5 = com.fossil.bz2.f(r11, r6)
            boolean r4 = com.fossil.ey2.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.fossil.bz2.f(r10, r6)
            java.lang.Object r5 = com.fossil.bz2.f(r11, r6)
            boolean r4 = com.fossil.ey2.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.fossil.bz2.c(r10, r6)
            boolean r5 = com.fossil.bz2.c(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.fossil.bz2.b(r10, r6)
            long r6 = com.fossil.bz2.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.fossil.bz2.a(r10, r6)
            int r5 = com.fossil.bz2.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.fossil.bz2.b(r10, r6)
            long r6 = com.fossil.bz2.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.fossil.bz2.b(r10, r6)
            long r6 = com.fossil.bz2.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.fossil.bz2.d(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.fossil.bz2.d(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.fossil.bz2.e(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.fossil.bz2.e(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.fossil.uy2<?, ?> r0 = r9.n
            java.lang.Object r0 = r0.a(r10)
            com.fossil.uy2<?, ?> r2 = r9.n
            java.lang.Object r2 = r2.a(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.f
            if (r0 == 0) goto L_0x01f1
            com.fossil.pv2<?> r0 = r9.o
            com.fossil.qv2 r10 = r0.a(r10)
            com.fossil.pv2<?> r0 = r9.o
            com.fossil.qv2 r11 = r0.a(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
            switch-data {0->0x01a7, 1->0x018e, 2->0x017b, 3->0x0168, 4->0x0157, 5->0x0144, 6->0x0132, 7->0x0120, 8->0x010a, 9->0x00f4, 10->0x00de, 11->0x00cc, 12->0x00ba, 13->0x00a8, 14->0x0094, 15->0x0082, 16->0x006e, 17->0x0058, 18->0x004a, 19->0x004a, 20->0x004a, 21->0x004a, 22->0x004a, 23->0x004a, 24->0x004a, 25->0x004a, 26->0x004a, 27->0x004a, 28->0x004a, 29->0x004a, 30->0x004a, 31->0x004a, 32->0x004a, 33->0x004a, 34->0x004a, 35->0x004a, 36->0x004a, 37->0x004a, 38->0x004a, 39->0x004a, 40->0x004a, 41->0x004a, 42->0x004a, 43->0x004a, 44->0x004a, 45->0x004a, 46->0x004a, 47->0x004a, 48->0x004a, 49->0x004a, 50->0x003c, 51->0x001c, 52->0x001c, 53->0x001c, 54->0x001c, 55->0x001c, 56->0x001c, 57->0x001c, 58->0x001c, 59->0x001c, 60->0x001c, 61->0x001c, 62->0x001c, 63->0x001c, 64->0x001c, 65->0x001c, 66->0x001c, 67->0x001c, 68->0x001c, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.zza(java.lang.Object, java.lang.Object):boolean");
    }

    @DexIgnore
    public final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:167:0x046e  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0474  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(T r17, com.fossil.oz2 r18) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            boolean r3 = r0.f
            if (r3 == 0) goto L_0x0023
            com.fossil.pv2<?> r3 = r0.o
            com.fossil.qv2 r3 = r3.a(r1)
            com.fossil.dy2<T extends com.fossil.sv2<T>, java.lang.Object> r5 = r3.a
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.c()
            java.lang.Object r3 = r3.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            goto L_0x0024
        L_0x0023:
            r3 = 0
        L_0x0024:
            int[] r5 = r0.a
            int r5 = r5.length
            sun.misc.Unsafe r6 = com.fossil.nx2.r
            r7 = 1048575(0xfffff, float:1.469367E-39)
            r9 = 0
            r10 = 1048575(0xfffff, float:1.469367E-39)
            r11 = 0
        L_0x0031:
            if (r9 >= r5) goto L_0x046b
            int r12 = r0.d(r9)
            int[] r13 = r0.a
            r14 = r13[r9]
            r15 = 267386880(0xff00000, float:2.3665827E-29)
            r15 = r15 & r12
            int r15 = r15 >>> 20
            boolean r4 = r0.g
            r8 = 1
            if (r4 != 0) goto L_0x005c
            r4 = 17
            if (r15 > r4) goto L_0x005c
            int r4 = r9 + 2
            r4 = r13[r4]
            r13 = r4 & r7
            if (r13 == r10) goto L_0x0057
            long r10 = (long) r13
            int r11 = r6.getInt(r1, r10)
            r10 = r13
        L_0x0057:
            int r4 = r4 >>> 20
            int r4 = r8 << r4
            goto L_0x005d
        L_0x005c:
            r4 = 0
        L_0x005d:
            if (r3 != 0) goto L_0x0464
            r12 = r12 & r7
            long r12 = (long) r12
            switch(r15) {
                case 0: goto L_0x0455;
                case 1: goto L_0x0449;
                case 2: goto L_0x043d;
                case 3: goto L_0x0431;
                case 4: goto L_0x0425;
                case 5: goto L_0x0419;
                case 6: goto L_0x040d;
                case 7: goto L_0x0401;
                case 8: goto L_0x03f5;
                case 9: goto L_0x03e4;
                case 10: goto L_0x03d5;
                case 11: goto L_0x03c8;
                case 12: goto L_0x03bb;
                case 13: goto L_0x03ae;
                case 14: goto L_0x03a1;
                case 15: goto L_0x0394;
                case 16: goto L_0x0387;
                case 17: goto L_0x0376;
                case 18: goto L_0x0366;
                case 19: goto L_0x0356;
                case 20: goto L_0x0346;
                case 21: goto L_0x0336;
                case 22: goto L_0x0326;
                case 23: goto L_0x0316;
                case 24: goto L_0x0306;
                case 25: goto L_0x02f6;
                case 26: goto L_0x02e7;
                case 27: goto L_0x02d4;
                case 28: goto L_0x02c5;
                case 29: goto L_0x02b5;
                case 30: goto L_0x02a5;
                case 31: goto L_0x0295;
                case 32: goto L_0x0285;
                case 33: goto L_0x0275;
                case 34: goto L_0x0265;
                case 35: goto L_0x0256;
                case 36: goto L_0x0247;
                case 37: goto L_0x0238;
                case 38: goto L_0x0229;
                case 39: goto L_0x021a;
                case 40: goto L_0x020b;
                case 41: goto L_0x01fc;
                case 42: goto L_0x01ed;
                case 43: goto L_0x01de;
                case 44: goto L_0x01cf;
                case 45: goto L_0x01c0;
                case 46: goto L_0x01b1;
                case 47: goto L_0x01a2;
                case 48: goto L_0x0193;
                case 49: goto L_0x0180;
                case 50: goto L_0x0177;
                case 51: goto L_0x0168;
                case 52: goto L_0x0159;
                case 53: goto L_0x014a;
                case 54: goto L_0x013b;
                case 55: goto L_0x012c;
                case 56: goto L_0x011d;
                case 57: goto L_0x010e;
                case 58: goto L_0x00ff;
                case 59: goto L_0x00f0;
                case 60: goto L_0x00dd;
                case 61: goto L_0x00cd;
                case 62: goto L_0x00bf;
                case 63: goto L_0x00b1;
                case 64: goto L_0x00a3;
                case 65: goto L_0x0095;
                case 66: goto L_0x0087;
                case 67: goto L_0x0079;
                case 68: goto L_0x0067;
                default: goto L_0x0064;
            }
        L_0x0064:
            r15 = 0
            goto L_0x0460
        L_0x0067:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.cy2 r8 = r0.a(r9)
            r2.b(r14, r4, r8)
            goto L_0x0064
        L_0x0079:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            long r12 = e(r1, r12)
            r2.zze(r14, r12)
            goto L_0x0064
        L_0x0087:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x0064
        L_0x0095:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            long r12 = e(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x0064
        L_0x00a3:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0064
        L_0x00b1:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zzb(r14, r4)
            goto L_0x0064
        L_0x00bf:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zze(r14, r4)
            goto L_0x0064
        L_0x00cd:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.tu2 r4 = (com.fossil.tu2) r4
            r2.a(r14, r4)
            goto L_0x0064
        L_0x00dd:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.cy2 r8 = r0.a(r9)
            r2.a(r14, r4, r8)
            goto L_0x0064
        L_0x00f0:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            java.lang.Object r4 = r6.getObject(r1, r12)
            a(r14, r4, r2)
            goto L_0x0064
        L_0x00ff:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            boolean r4 = f(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0064
        L_0x010e:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zzd(r14, r4)
            goto L_0x0064
        L_0x011d:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            long r12 = e(r1, r12)
            r2.zzd(r14, r12)
            goto L_0x0064
        L_0x012c:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            int r4 = d(r1, r12)
            r2.zzc(r14, r4)
            goto L_0x0064
        L_0x013b:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            long r12 = e(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x0064
        L_0x014a:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            long r12 = e(r1, r12)
            r2.zza(r14, r12)
            goto L_0x0064
        L_0x0159:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            float r4 = c(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0064
        L_0x0168:
            boolean r4 = r0.a(r1, r14, r9)
            if (r4 == 0) goto L_0x0064
            double r12 = b(r1, r12)
            r2.zza(r14, r12)
            goto L_0x0064
        L_0x0177:
            java.lang.Object r4 = r6.getObject(r1, r12)
            r0.a(r2, r14, r4, r9)
            goto L_0x0064
        L_0x0180:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.cy2 r12 = r0.a(r9)
            com.fossil.ey2.b(r4, r8, r2, r12)
            goto L_0x0064
        L_0x0193:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.e(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01a2:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.j(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01b1:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.g(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01c0:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.l(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01cf:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.m(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01de:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.i(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01ed:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.n(r4, r12, r2, r8)
            goto L_0x0064
        L_0x01fc:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.k(r4, r12, r2, r8)
            goto L_0x0064
        L_0x020b:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.f(r4, r12, r2, r8)
            goto L_0x0064
        L_0x021a:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.h(r4, r12, r2, r8)
            goto L_0x0064
        L_0x0229:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.d(r4, r12, r2, r8)
            goto L_0x0064
        L_0x0238:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.c(r4, r12, r2, r8)
            goto L_0x0064
        L_0x0247:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.b(r4, r12, r2, r8)
            goto L_0x0064
        L_0x0256:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r12 = r6.getObject(r1, r12)
            java.util.List r12 = (java.util.List) r12
            com.fossil.ey2.a(r4, r12, r2, r8)
            goto L_0x0064
        L_0x0265:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            r14 = 0
            com.fossil.ey2.e(r4, r8, r2, r14)
            goto L_0x0064
        L_0x0275:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.j(r4, r8, r2, r14)
            goto L_0x0064
        L_0x0285:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.g(r4, r8, r2, r14)
            goto L_0x0064
        L_0x0295:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.l(r4, r8, r2, r14)
            goto L_0x0064
        L_0x02a5:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.m(r4, r8, r2, r14)
            goto L_0x0064
        L_0x02b5:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.i(r4, r8, r2, r14)
            goto L_0x0064
        L_0x02c5:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.b(r4, r8, r2)
            goto L_0x0064
        L_0x02d4:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.cy2 r12 = r0.a(r9)
            com.fossil.ey2.a(r4, r8, r2, r12)
            goto L_0x0064
        L_0x02e7:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.a(r4, r8, r2)
            goto L_0x0064
        L_0x02f6:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            r15 = 0
            com.fossil.ey2.n(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0306:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.k(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0316:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.f(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0326:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.h(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0336:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.d(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0346:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.c(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0356:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.b(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0366:
            r15 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r6.getObject(r1, r12)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.a(r4, r8, r2, r15)
            goto L_0x0460
        L_0x0376:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.cy2 r8 = r0.a(r9)
            r2.b(r14, r4, r8)
            goto L_0x0460
        L_0x0387:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            long r12 = r6.getLong(r1, r12)
            r2.zze(r14, r12)
            goto L_0x0460
        L_0x0394:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x0460
        L_0x03a1:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            long r12 = r6.getLong(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x0460
        L_0x03ae:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0460
        L_0x03bb:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zzb(r14, r4)
            goto L_0x0460
        L_0x03c8:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zze(r14, r4)
            goto L_0x0460
        L_0x03d5:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.tu2 r4 = (com.fossil.tu2) r4
            r2.a(r14, r4)
            goto L_0x0460
        L_0x03e4:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            java.lang.Object r4 = r6.getObject(r1, r12)
            com.fossil.cy2 r8 = r0.a(r9)
            r2.a(r14, r4, r8)
            goto L_0x0460
        L_0x03f5:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            java.lang.Object r4 = r6.getObject(r1, r12)
            a(r14, r4, r2)
            goto L_0x0460
        L_0x0401:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            boolean r4 = com.fossil.bz2.c(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0460
        L_0x040d:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zzd(r14, r4)
            goto L_0x0460
        L_0x0419:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            long r12 = r6.getLong(r1, r12)
            r2.zzd(r14, r12)
            goto L_0x0460
        L_0x0425:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            int r4 = r6.getInt(r1, r12)
            r2.zzc(r14, r4)
            goto L_0x0460
        L_0x0431:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            long r12 = r6.getLong(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x0460
        L_0x043d:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            long r12 = r6.getLong(r1, r12)
            r2.zza(r14, r12)
            goto L_0x0460
        L_0x0449:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            float r4 = com.fossil.bz2.d(r1, r12)
            r2.zza(r14, r4)
            goto L_0x0460
        L_0x0455:
            r15 = 0
            r4 = r4 & r11
            if (r4 == 0) goto L_0x0460
            double r12 = com.fossil.bz2.e(r1, r12)
            r2.zza(r14, r12)
        L_0x0460:
            int r9 = r9 + 3
            goto L_0x0031
        L_0x0464:
            com.fossil.pv2<?> r1 = r0.o
            r1.a(r3)
            r4 = 0
            throw r4
        L_0x046b:
            r4 = 0
            if (r3 != 0) goto L_0x0474
            com.fossil.uy2<?, ?> r3 = r0.n
            a(r3, r1, r2)
            return
        L_0x0474:
            com.fossil.pv2<?> r1 = r0.o
            r1.a(r2, r3)
            throw r4
            switch-data {0->0x0455, 1->0x0449, 2->0x043d, 3->0x0431, 4->0x0425, 5->0x0419, 6->0x040d, 7->0x0401, 8->0x03f5, 9->0x03e4, 10->0x03d5, 11->0x03c8, 12->0x03bb, 13->0x03ae, 14->0x03a1, 15->0x0394, 16->0x0387, 17->0x0376, 18->0x0366, 19->0x0356, 20->0x0346, 21->0x0336, 22->0x0326, 23->0x0316, 24->0x0306, 25->0x02f6, 26->0x02e7, 27->0x02d4, 28->0x02c5, 29->0x02b5, 30->0x02a5, 31->0x0295, 32->0x0285, 33->0x0275, 34->0x0265, 35->0x0256, 36->0x0247, 37->0x0238, 38->0x0229, 39->0x021a, 40->0x020b, 41->0x01fc, 42->0x01ed, 43->0x01de, 44->0x01cf, 45->0x01c0, 46->0x01b1, 47->0x01a2, 48->0x0193, 49->0x0180, 50->0x0177, 51->0x0168, 52->0x0159, 53->0x014a, 54->0x013b, 55->0x012c, 56->0x011d, 57->0x010e, 58->0x00ff, 59->0x00f0, 60->0x00dd, 61->0x00cd, 62->0x00bf, 63->0x00b1, 64->0x00a3, 65->0x0095, 66->0x0087, 67->0x0079, 68->0x0067, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.b(java.lang.Object, com.fossil.oz2):void");
    }

    @DexIgnore
    @Override // com.fossil.cy2
    public final int zza(T t) {
        int i2;
        int i3;
        int length = this.a.length;
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5 += 3) {
            int d2 = d(i5);
            int i6 = this.a[i5];
            long j2 = (long) (1048575 & d2);
            int i7 = 37;
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    i3 = i4 * 53;
                    i2 = ew2.a(Double.doubleToLongBits(bz2.e(t, j2)));
                    i4 = i3 + i2;
                    break;
                case 1:
                    i3 = i4 * 53;
                    i2 = Float.floatToIntBits(bz2.d(t, j2));
                    i4 = i3 + i2;
                    break;
                case 2:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 3:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 4:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 5:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 6:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 7:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.c(t, j2));
                    i4 = i3 + i2;
                    break;
                case 8:
                    i3 = i4 * 53;
                    i2 = ((String) bz2.f(t, j2)).hashCode();
                    i4 = i3 + i2;
                    break;
                case 9:
                    Object f2 = bz2.f(t, j2);
                    if (f2 != null) {
                        i7 = f2.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 10:
                    i3 = i4 * 53;
                    i2 = bz2.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 11:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 12:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 13:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 14:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 15:
                    i3 = i4 * 53;
                    i2 = bz2.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 16:
                    i3 = i4 * 53;
                    i2 = ew2.a(bz2.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 17:
                    Object f3 = bz2.f(t, j2);
                    if (f3 != null) {
                        i7 = f3.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i4 * 53;
                    i2 = bz2.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 50:
                    i3 = i4 * 53;
                    i2 = bz2.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 51:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(Double.doubleToLongBits(b(t, j2)));
                        i4 = i3 + i2;
                        break;
                    }
                case 52:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = Float.floatToIntBits(c(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 53:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 54:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 55:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 56:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 57:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 58:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(f(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 59:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ((String) bz2.f(t, j2)).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 60:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = bz2.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 61:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = bz2.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 62:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 63:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 64:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 65:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 66:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 67:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ew2.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 68:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = bz2.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
            }
        }
        int hashCode = (i4 * 53) + this.n.a(t).hashCode();
        return this.f ? (hashCode * 53) + this.o.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.fossil.cy2
    public final int zzb(T t) {
        int i2;
        int i3;
        long j2;
        int i4;
        int b2;
        int i5;
        int i6;
        int i7;
        int i8;
        int b3;
        int i9;
        int i10;
        int i11;
        int i12 = 267386880;
        if (this.g) {
            Unsafe unsafe = r;
            int i13 = 0;
            int i14 = 0;
            while (i13 < this.a.length) {
                int d2 = d(i13);
                int i15 = (d2 & i12) >>> 20;
                int i16 = this.a[i13];
                long j3 = (long) (d2 & 1048575);
                int i17 = (i15 < vv2.DOUBLE_LIST_PACKED.zza() || i15 > vv2.SINT64_LIST_PACKED.zza()) ? 0 : this.a[i13 + 2] & 1048575;
                switch (i15) {
                    case 0:
                        if (a((Object) t, i13)) {
                            b3 = iv2.b(i16, 0.0d);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 1:
                        if (a((Object) t, i13)) {
                            b3 = iv2.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 2:
                        if (a((Object) t, i13)) {
                            b3 = iv2.d(i16, bz2.b(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 3:
                        if (a((Object) t, i13)) {
                            b3 = iv2.e(i16, bz2.b(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 4:
                        if (a((Object) t, i13)) {
                            b3 = iv2.f(i16, bz2.a(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 5:
                        if (a((Object) t, i13)) {
                            b3 = iv2.g(i16, 0L);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 6:
                        if (a((Object) t, i13)) {
                            b3 = iv2.i(i16, 0);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 7:
                        if (a((Object) t, i13)) {
                            b3 = iv2.b(i16, true);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 8:
                        if (a((Object) t, i13)) {
                            Object f2 = bz2.f(t, j3);
                            if (!(f2 instanceof tu2)) {
                                b3 = iv2.b(i16, (String) f2);
                                break;
                            } else {
                                b3 = iv2.c(i16, (tu2) f2);
                                break;
                            }
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 9:
                        if (a((Object) t, i13)) {
                            b3 = ey2.a(i16, bz2.f(t, j3), a(i13));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 10:
                        if (a((Object) t, i13)) {
                            b3 = iv2.c(i16, (tu2) bz2.f(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 11:
                        if (a((Object) t, i13)) {
                            b3 = iv2.g(i16, bz2.a(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 12:
                        if (a((Object) t, i13)) {
                            b3 = iv2.k(i16, bz2.a(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 13:
                        if (a((Object) t, i13)) {
                            b3 = iv2.j(i16, 0);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 14:
                        if (a((Object) t, i13)) {
                            b3 = iv2.h(i16, 0L);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 15:
                        if (a((Object) t, i13)) {
                            b3 = iv2.h(i16, bz2.a(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 16:
                        if (a((Object) t, i13)) {
                            b3 = iv2.f(i16, bz2.b(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 17:
                        if (a((Object) t, i13)) {
                            b3 = iv2.c(i16, (jx2) bz2.f(t, j3), a(i13));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 18:
                        b3 = ey2.i(i16, a(t, j3), false);
                        break;
                    case 19:
                        b3 = ey2.h(i16, a(t, j3), false);
                        break;
                    case 20:
                        b3 = ey2.a(i16, (List<Long>) a(t, j3), false);
                        break;
                    case 21:
                        b3 = ey2.b(i16, (List<Long>) a(t, j3), false);
                        break;
                    case 22:
                        b3 = ey2.e(i16, a(t, j3), false);
                        break;
                    case 23:
                        b3 = ey2.i(i16, a(t, j3), false);
                        break;
                    case 24:
                        b3 = ey2.h(i16, a(t, j3), false);
                        break;
                    case 25:
                        b3 = ey2.j(i16, a(t, j3), false);
                        break;
                    case 26:
                        b3 = ey2.a(i16, a(t, j3));
                        break;
                    case 27:
                        b3 = ey2.a(i16, a(t, j3), a(i13));
                        break;
                    case 28:
                        b3 = ey2.b(i16, a(t, j3));
                        break;
                    case 29:
                        b3 = ey2.f(i16, a(t, j3), false);
                        break;
                    case 30:
                        b3 = ey2.d(i16, a(t, j3), false);
                        break;
                    case 31:
                        b3 = ey2.h(i16, a(t, j3), false);
                        break;
                    case 32:
                        b3 = ey2.i(i16, a(t, j3), false);
                        break;
                    case 33:
                        b3 = ey2.g(i16, a(t, j3), false);
                        break;
                    case 34:
                        b3 = ey2.c(i16, a(t, j3), false);
                        break;
                    case 35:
                        i10 = ey2.i((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 36:
                        i10 = ey2.h((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 37:
                        i10 = ey2.a((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 38:
                        i10 = ey2.b((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 39:
                        i10 = ey2.e((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 40:
                        i10 = ey2.i((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 41:
                        i10 = ey2.h((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 42:
                        i10 = ey2.j((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 43:
                        i10 = ey2.f((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 44:
                        i10 = ey2.d((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 45:
                        i10 = ey2.h((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 46:
                        i10 = ey2.i((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 47:
                        i10 = ey2.g((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 48:
                        i10 = ey2.c((List) unsafe.getObject(t, j3));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i17, i10);
                            }
                            i11 = iv2.e(i16);
                            i9 = iv2.g(i10);
                            b3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 49:
                        b3 = ey2.b(i16, (List<jx2>) a(t, j3), a(i13));
                        break;
                    case 50:
                        b3 = this.p.zza(i16, bz2.f(t, j3), b(i13));
                        break;
                    case 51:
                        if (a(t, i16, i13)) {
                            b3 = iv2.b(i16, 0.0d);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 52:
                        if (a(t, i16, i13)) {
                            b3 = iv2.b(i16, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 53:
                        if (a(t, i16, i13)) {
                            b3 = iv2.d(i16, e(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 54:
                        if (a(t, i16, i13)) {
                            b3 = iv2.e(i16, e(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 55:
                        if (a(t, i16, i13)) {
                            b3 = iv2.f(i16, d(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 56:
                        if (a(t, i16, i13)) {
                            b3 = iv2.g(i16, 0L);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 57:
                        if (a(t, i16, i13)) {
                            b3 = iv2.i(i16, 0);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 58:
                        if (a(t, i16, i13)) {
                            b3 = iv2.b(i16, true);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 59:
                        if (a(t, i16, i13)) {
                            Object f3 = bz2.f(t, j3);
                            if (!(f3 instanceof tu2)) {
                                b3 = iv2.b(i16, (String) f3);
                                break;
                            } else {
                                b3 = iv2.c(i16, (tu2) f3);
                                break;
                            }
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 60:
                        if (a(t, i16, i13)) {
                            b3 = ey2.a(i16, bz2.f(t, j3), a(i13));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 61:
                        if (a(t, i16, i13)) {
                            b3 = iv2.c(i16, (tu2) bz2.f(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 62:
                        if (a(t, i16, i13)) {
                            b3 = iv2.g(i16, d(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 63:
                        if (a(t, i16, i13)) {
                            b3 = iv2.k(i16, d(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 64:
                        if (a(t, i16, i13)) {
                            b3 = iv2.j(i16, 0);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 65:
                        if (a(t, i16, i13)) {
                            b3 = iv2.h(i16, 0L);
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 66:
                        if (a(t, i16, i13)) {
                            b3 = iv2.h(i16, d(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 67:
                        if (a(t, i16, i13)) {
                            b3 = iv2.f(i16, e(t, j3));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    case 68:
                        if (a(t, i16, i13)) {
                            b3 = iv2.c(i16, (jx2) bz2.f(t, j3), a(i13));
                            break;
                        } else {
                            continue;
                            i13 += 3;
                            i12 = 267386880;
                        }
                    default:
                        i13 += 3;
                        i12 = 267386880;
                }
                i14 += b3;
                i13 += 3;
                i12 = 267386880;
            }
            return i14 + a((uy2) this.n, (Object) t);
        }
        Unsafe unsafe2 = r;
        int i18 = 0;
        int i19 = 1048575;
        int i20 = 0;
        for (int i21 = 0; i21 < this.a.length; i21 += 3) {
            int d3 = d(i21);
            int[] iArr = this.a;
            int i22 = iArr[i21];
            int i23 = (d3 & 267386880) >>> 20;
            if (i23 <= 17) {
                int i24 = iArr[i21 + 2];
                int i25 = i24 & 1048575;
                i2 = 1 << (i24 >>> 20);
                if (i25 != i19) {
                    i20 = unsafe2.getInt(t, (long) i25);
                    i19 = i25;
                }
                i3 = i24;
            } else {
                i3 = (!this.h || i23 < vv2.DOUBLE_LIST_PACKED.zza() || i23 > vv2.SINT64_LIST_PACKED.zza()) ? 0 : this.a[i21 + 2] & 1048575;
                i2 = 0;
            }
            long j4 = (long) (d3 & 1048575);
            switch (i23) {
                case 0:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i18 += iv2.b(i22, 0.0d);
                        break;
                    }
                    break;
                case 1:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i18 += iv2.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        break;
                    }
                case 2:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i4 = iv2.d(i22, unsafe2.getLong(t, j4));
                        i18 += i4;
                    }
                    break;
                case 3:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i4 = iv2.e(i22, unsafe2.getLong(t, j4));
                        i18 += i4;
                    }
                    break;
                case 4:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i4 = iv2.f(i22, unsafe2.getInt(t, j4));
                        i18 += i4;
                    }
                    break;
                case 5:
                    j2 = 0;
                    if ((i20 & i2) != 0) {
                        i4 = iv2.g(i22, 0L);
                        i18 += i4;
                    }
                    break;
                case 6:
                    if ((i20 & i2) != 0) {
                        i18 += iv2.i(i22, 0);
                        j2 = 0;
                        break;
                    }
                    j2 = 0;
                case 7:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.b(i22, true);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 8:
                    if ((i20 & i2) != 0) {
                        Object object = unsafe2.getObject(t, j4);
                        if (object instanceof tu2) {
                            b2 = iv2.c(i22, (tu2) object);
                        } else {
                            b2 = iv2.b(i22, (String) object);
                        }
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 9:
                    if ((i20 & i2) != 0) {
                        b2 = ey2.a(i22, unsafe2.getObject(t, j4), a(i21));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 10:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.c(i22, (tu2) unsafe2.getObject(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 11:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.g(i22, unsafe2.getInt(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 12:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.k(i22, unsafe2.getInt(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 13:
                    if ((i20 & i2) != 0) {
                        i5 = iv2.j(i22, 0);
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 14:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.h(i22, 0L);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 15:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.h(i22, unsafe2.getInt(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 16:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.f(i22, unsafe2.getLong(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 17:
                    if ((i20 & i2) != 0) {
                        b2 = iv2.c(i22, (jx2) unsafe2.getObject(t, j4), a(i21));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 18:
                    b2 = ey2.i(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 19:
                    b2 = ey2.h(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 20:
                    b2 = ey2.a(i22, (List<Long>) ((List) unsafe2.getObject(t, j4)), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 21:
                    b2 = ey2.b(i22, (List<Long>) ((List) unsafe2.getObject(t, j4)), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 22:
                    b2 = ey2.e(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 23:
                    b2 = ey2.i(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 24:
                    b2 = ey2.h(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 25:
                    b2 = ey2.j(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 26:
                    b2 = ey2.a(i22, (List) unsafe2.getObject(t, j4));
                    i18 += b2;
                    j2 = 0;
                    break;
                case 27:
                    b2 = ey2.a(i22, (List<?>) ((List) unsafe2.getObject(t, j4)), a(i21));
                    i18 += b2;
                    j2 = 0;
                    break;
                case 28:
                    b2 = ey2.b(i22, (List) unsafe2.getObject(t, j4));
                    i18 += b2;
                    j2 = 0;
                    break;
                case 29:
                    b2 = ey2.f(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 30:
                    b2 = ey2.d(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 31:
                    b2 = ey2.h(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 32:
                    b2 = ey2.i(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 33:
                    b2 = ey2.g(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 34:
                    b2 = ey2.c(i22, (List) unsafe2.getObject(t, j4), false);
                    i18 += b2;
                    j2 = 0;
                    break;
                case 35:
                    i8 = ey2.i((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 36:
                    i8 = ey2.h((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 37:
                    i8 = ey2.a((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 38:
                    i8 = ey2.b((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 39:
                    i8 = ey2.e((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 40:
                    i8 = ey2.i((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 41:
                    i8 = ey2.h((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 42:
                    i8 = ey2.j((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 43:
                    i8 = ey2.f((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 44:
                    i8 = ey2.d((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 45:
                    i8 = ey2.h((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 46:
                    i8 = ey2.i((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 47:
                    i8 = ey2.g((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 48:
                    i8 = ey2.c((List) unsafe2.getObject(t, j4));
                    if (i8 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t, (long) i3, i8);
                        }
                        i7 = iv2.e(i22);
                        i6 = iv2.g(i8);
                        i5 = i7 + i6 + i8;
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 49:
                    b2 = ey2.b(i22, (List) unsafe2.getObject(t, j4), a(i21));
                    i18 += b2;
                    j2 = 0;
                    break;
                case 50:
                    b2 = this.p.zza(i22, unsafe2.getObject(t, j4), b(i21));
                    i18 += b2;
                    j2 = 0;
                    break;
                case 51:
                    if (a(t, i22, i21)) {
                        b2 = iv2.b(i22, 0.0d);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 52:
                    if (a(t, i22, i21)) {
                        i5 = iv2.b(i22, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 53:
                    if (a(t, i22, i21)) {
                        b2 = iv2.d(i22, e(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 54:
                    if (a(t, i22, i21)) {
                        b2 = iv2.e(i22, e(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 55:
                    if (a(t, i22, i21)) {
                        b2 = iv2.f(i22, d(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 56:
                    if (a(t, i22, i21)) {
                        b2 = iv2.g(i22, 0L);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 57:
                    if (a(t, i22, i21)) {
                        i5 = iv2.i(i22, 0);
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 58:
                    if (a(t, i22, i21)) {
                        b2 = iv2.b(i22, true);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 59:
                    if (a(t, i22, i21)) {
                        Object object2 = unsafe2.getObject(t, j4);
                        if (object2 instanceof tu2) {
                            b2 = iv2.c(i22, (tu2) object2);
                        } else {
                            b2 = iv2.b(i22, (String) object2);
                        }
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 60:
                    if (a(t, i22, i21)) {
                        b2 = ey2.a(i22, unsafe2.getObject(t, j4), a(i21));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 61:
                    if (a(t, i22, i21)) {
                        b2 = iv2.c(i22, (tu2) unsafe2.getObject(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 62:
                    if (a(t, i22, i21)) {
                        b2 = iv2.g(i22, d(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 63:
                    if (a(t, i22, i21)) {
                        b2 = iv2.k(i22, d(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 64:
                    if (a(t, i22, i21)) {
                        i5 = iv2.j(i22, 0);
                        i18 += i5;
                    }
                    j2 = 0;
                    break;
                case 65:
                    if (a(t, i22, i21)) {
                        b2 = iv2.h(i22, 0L);
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 66:
                    if (a(t, i22, i21)) {
                        b2 = iv2.h(i22, d(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 67:
                    if (a(t, i22, i21)) {
                        b2 = iv2.f(i22, e(t, j4));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                case 68:
                    if (a(t, i22, i21)) {
                        b2 = iv2.c(i22, (jx2) unsafe2.getObject(t, j4), a(i21));
                        i18 += b2;
                    }
                    j2 = 0;
                    break;
                default:
                    j2 = 0;
                    break;
            }
        }
        int i26 = 0;
        int a2 = i18 + a((uy2) this.n, (Object) t);
        if (!this.f) {
            return a2;
        }
        qv2<?> a3 = this.o.a((Object) t);
        for (int i27 = 0; i27 < a3.a.c(); i27++) {
            Map.Entry<T, Object> a4 = a3.a.a(i27);
            i26 += qv2.b((sv2<?>) a4.getKey(), a4.getValue());
        }
        for (Map.Entry<T, Object> entry : a3.a.d()) {
            i26 += qv2.b((sv2<?>) entry.getKey(), entry.getValue());
        }
        return a2 + i26;
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @DexIgnore
    public final void a(T t, T t2, int i2) {
        long d2 = (long) (d(i2) & 1048575);
        if (a((Object) t2, i2)) {
            Object f2 = bz2.f(t, d2);
            Object f3 = bz2.f(t2, d2);
            if (f2 != null && f3 != null) {
                bz2.a(t, d2, ew2.a(f2, f3));
                b((Object) t, i2);
            } else if (f3 != null) {
                bz2.a(t, d2, f3);
                b((Object) t, i2);
            }
        }
    }

    @DexIgnore
    public static <UT, UB> int a(uy2<UT, UB> uy2, T t) {
        return uy2.d(uy2.a(t));
    }

    @DexIgnore
    public static List<?> a(Object obj, long j2) {
        return (List) bz2.f(obj, j2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04bc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x04bd  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x096d  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0973  */
    @Override // com.fossil.cy2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r13, com.fossil.oz2 r14) throws java.io.IOException {
        /*
            r12 = this;
            int r0 = r14.zza()
            int r1 = com.fossil.bw2.f.l
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x04c3
            com.fossil.uy2<?, ?> r0 = r12.n
            a(r0, r13, r14)
            boolean r0 = r12.f
            if (r0 == 0) goto L_0x0032
            com.fossil.pv2<?> r0 = r12.o
            com.fossil.qv2 r0 = r0.a(r13)
            com.fossil.dy2<T extends com.fossil.sv2<T>, java.lang.Object> r1 = r0.a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.d()
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            goto L_0x0033
        L_0x0032:
            r0 = r3
        L_0x0033:
            int[] r1 = r12.a
            int r1 = r1.length
            int r1 = r1 + -3
        L_0x0038:
            if (r1 < 0) goto L_0x04ba
            int r7 = r12.d(r1)
            int[] r8 = r12.a
            r9 = r8[r1]
            if (r0 != 0) goto L_0x04b4
            r10 = r7 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04a1;
                case 1: goto L_0x0491;
                case 2: goto L_0x0481;
                case 3: goto L_0x0471;
                case 4: goto L_0x0461;
                case 5: goto L_0x0451;
                case 6: goto L_0x0441;
                case 7: goto L_0x0430;
                case 8: goto L_0x041f;
                case 9: goto L_0x040a;
                case 10: goto L_0x03f7;
                case 11: goto L_0x03e6;
                case 12: goto L_0x03d5;
                case 13: goto L_0x03c4;
                case 14: goto L_0x03b3;
                case 15: goto L_0x03a2;
                case 16: goto L_0x0391;
                case 17: goto L_0x037c;
                case 18: goto L_0x036d;
                case 19: goto L_0x035e;
                case 20: goto L_0x034f;
                case 21: goto L_0x0340;
                case 22: goto L_0x0331;
                case 23: goto L_0x0322;
                case 24: goto L_0x0313;
                case 25: goto L_0x0304;
                case 26: goto L_0x02f5;
                case 27: goto L_0x02e2;
                case 28: goto L_0x02d3;
                case 29: goto L_0x02c4;
                case 30: goto L_0x02b5;
                case 31: goto L_0x02a6;
                case 32: goto L_0x0297;
                case 33: goto L_0x0288;
                case 34: goto L_0x0279;
                case 35: goto L_0x026a;
                case 36: goto L_0x025b;
                case 37: goto L_0x024c;
                case 38: goto L_0x023d;
                case 39: goto L_0x022e;
                case 40: goto L_0x021f;
                case 41: goto L_0x0210;
                case 42: goto L_0x0201;
                case 43: goto L_0x01f2;
                case 44: goto L_0x01e3;
                case 45: goto L_0x01d4;
                case 46: goto L_0x01c5;
                case 47: goto L_0x01b6;
                case 48: goto L_0x01a7;
                case 49: goto L_0x0194;
                case 50: goto L_0x0189;
                case 51: goto L_0x0178;
                case 52: goto L_0x0167;
                case 53: goto L_0x0156;
                case 54: goto L_0x0145;
                case 55: goto L_0x0134;
                case 56: goto L_0x0123;
                case 57: goto L_0x0112;
                case 58: goto L_0x0101;
                case 59: goto L_0x00f0;
                case 60: goto L_0x00db;
                case 61: goto L_0x00c8;
                case 62: goto L_0x00b7;
                case 63: goto L_0x00a6;
                case 64: goto L_0x0095;
                case 65: goto L_0x0084;
                case 66: goto L_0x0073;
                case 67: goto L_0x0062;
                case 68: goto L_0x004d;
                default: goto L_0x004b;
            }
        L_0x004b:
            goto L_0x04b0
        L_0x004d:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.cy2 r8 = r12.a(r1)
            r14.b(r9, r7, r8)
            goto L_0x04b0
        L_0x0062:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = e(r13, r7)
            r14.zze(r9, r7)
            goto L_0x04b0
        L_0x0073:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zzf(r9, r7)
            goto L_0x04b0
        L_0x0084:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = e(r13, r7)
            r14.zzb(r9, r7)
            goto L_0x04b0
        L_0x0095:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x00a6:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zzb(r9, r7)
            goto L_0x04b0
        L_0x00b7:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zze(r9, r7)
            goto L_0x04b0
        L_0x00c8:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.tu2 r7 = (com.fossil.tu2) r7
            r14.a(r9, r7)
            goto L_0x04b0
        L_0x00db:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.cy2 r8 = r12.a(r1)
            r14.a(r9, r7, r8)
            goto L_0x04b0
        L_0x00f0:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            a(r9, r7, r14)
            goto L_0x04b0
        L_0x0101:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            boolean r7 = f(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0112:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zzd(r9, r7)
            goto L_0x04b0
        L_0x0123:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = e(r13, r7)
            r14.zzd(r9, r7)
            goto L_0x04b0
        L_0x0134:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = d(r13, r7)
            r14.zzc(r9, r7)
            goto L_0x04b0
        L_0x0145:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = e(r13, r7)
            r14.zzc(r9, r7)
            goto L_0x04b0
        L_0x0156:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = e(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0167:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            float r7 = c(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0178:
            boolean r8 = r12.a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            double r7 = b(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0189:
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            r12.a(r14, r9, r7, r1)
            goto L_0x04b0
        L_0x0194:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.cy2 r9 = r12.a(r1)
            com.fossil.ey2.b(r8, r7, r14, r9)
            goto L_0x04b0
        L_0x01a7:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.e(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01b6:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.j(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01c5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.g(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01d4:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.l(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01e3:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.m(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01f2:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.i(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0201:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.n(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0210:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.k(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x021f:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.f(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x022e:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.h(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x023d:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.d(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x024c:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.c(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x025b:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.b(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x026a:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.a(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0279:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.e(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0288:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.j(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0297:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.g(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02a6:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.l(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02b5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.m(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02c4:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.i(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02d3:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.b(r8, r7, r14)
            goto L_0x04b0
        L_0x02e2:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.cy2 r9 = r12.a(r1)
            com.fossil.ey2.a(r8, r7, r14, r9)
            goto L_0x04b0
        L_0x02f5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.a(r8, r7, r14)
            goto L_0x04b0
        L_0x0304:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.n(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0313:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.k(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0322:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.f(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0331:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.h(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0340:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.d(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x034f:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.c(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x035e:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.b(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x036d:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.fossil.ey2.a(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x037c:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.cy2 r8 = r12.a(r1)
            r14.b(r9, r7, r8)
            goto L_0x04b0
        L_0x0391:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.fossil.bz2.b(r13, r7)
            r14.zze(r9, r7)
            goto L_0x04b0
        L_0x03a2:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zzf(r9, r7)
            goto L_0x04b0
        L_0x03b3:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.fossil.bz2.b(r13, r7)
            r14.zzb(r9, r7)
            goto L_0x04b0
        L_0x03c4:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x03d5:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zzb(r9, r7)
            goto L_0x04b0
        L_0x03e6:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zze(r9, r7)
            goto L_0x04b0
        L_0x03f7:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.tu2 r7 = (com.fossil.tu2) r7
            r14.a(r9, r7)
            goto L_0x04b0
        L_0x040a:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            com.fossil.cy2 r8 = r12.a(r1)
            r14.a(r9, r7, r8)
            goto L_0x04b0
        L_0x041f:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.fossil.bz2.f(r13, r7)
            a(r9, r7, r14)
            goto L_0x04b0
        L_0x0430:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            boolean r7 = com.fossil.bz2.c(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0441:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zzd(r9, r7)
            goto L_0x04b0
        L_0x0451:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.fossil.bz2.b(r13, r7)
            r14.zzd(r9, r7)
            goto L_0x04b0
        L_0x0461:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.fossil.bz2.a(r13, r7)
            r14.zzc(r9, r7)
            goto L_0x04b0
        L_0x0471:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.fossil.bz2.b(r13, r7)
            r14.zzc(r9, r7)
            goto L_0x04b0
        L_0x0481:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.fossil.bz2.b(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x0491:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            float r7 = com.fossil.bz2.d(r13, r7)
            r14.zza(r9, r7)
            goto L_0x04b0
        L_0x04a1:
            boolean r8 = r12.a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            double r7 = com.fossil.bz2.e(r13, r7)
            r14.zza(r9, r7)
        L_0x04b0:
            int r1 = r1 + -3
            goto L_0x0038
        L_0x04b4:
            com.fossil.pv2<?> r13 = r12.o
            r13.a(r0)
            throw r3
        L_0x04ba:
            if (r0 != 0) goto L_0x04bd
            return
        L_0x04bd:
            com.fossil.pv2<?> r13 = r12.o
            r13.a(r14, r0)
            throw r3
        L_0x04c3:
            boolean r0 = r12.g
            if (r0 == 0) goto L_0x0979
            boolean r0 = r12.f
            if (r0 == 0) goto L_0x04e4
            com.fossil.pv2<?> r0 = r12.o
            com.fossil.qv2 r0 = r0.a(r13)
            com.fossil.dy2<T extends com.fossil.sv2<T>, java.lang.Object> r1 = r0.a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x04e4
            java.util.Iterator r0 = r0.c()
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            goto L_0x04e5
        L_0x04e4:
            r0 = r3
        L_0x04e5:
            int[] r1 = r12.a
            int r1 = r1.length
            r7 = 0
        L_0x04e9:
            if (r7 >= r1) goto L_0x096b
            int r8 = r12.d(r7)
            int[] r9 = r12.a
            r10 = r9[r7]
            if (r0 != 0) goto L_0x0965
            r11 = r8 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0952;
                case 1: goto L_0x0942;
                case 2: goto L_0x0932;
                case 3: goto L_0x0922;
                case 4: goto L_0x0912;
                case 5: goto L_0x0902;
                case 6: goto L_0x08f2;
                case 7: goto L_0x08e1;
                case 8: goto L_0x08d0;
                case 9: goto L_0x08bb;
                case 10: goto L_0x08a8;
                case 11: goto L_0x0897;
                case 12: goto L_0x0886;
                case 13: goto L_0x0875;
                case 14: goto L_0x0864;
                case 15: goto L_0x0853;
                case 16: goto L_0x0842;
                case 17: goto L_0x082d;
                case 18: goto L_0x081e;
                case 19: goto L_0x080f;
                case 20: goto L_0x0800;
                case 21: goto L_0x07f1;
                case 22: goto L_0x07e2;
                case 23: goto L_0x07d3;
                case 24: goto L_0x07c4;
                case 25: goto L_0x07b5;
                case 26: goto L_0x07a6;
                case 27: goto L_0x0793;
                case 28: goto L_0x0784;
                case 29: goto L_0x0775;
                case 30: goto L_0x0766;
                case 31: goto L_0x0757;
                case 32: goto L_0x0748;
                case 33: goto L_0x0739;
                case 34: goto L_0x072a;
                case 35: goto L_0x071b;
                case 36: goto L_0x070c;
                case 37: goto L_0x06fd;
                case 38: goto L_0x06ee;
                case 39: goto L_0x06df;
                case 40: goto L_0x06d0;
                case 41: goto L_0x06c1;
                case 42: goto L_0x06b2;
                case 43: goto L_0x06a3;
                case 44: goto L_0x0694;
                case 45: goto L_0x0685;
                case 46: goto L_0x0676;
                case 47: goto L_0x0667;
                case 48: goto L_0x0658;
                case 49: goto L_0x0645;
                case 50: goto L_0x063a;
                case 51: goto L_0x0629;
                case 52: goto L_0x0618;
                case 53: goto L_0x0607;
                case 54: goto L_0x05f6;
                case 55: goto L_0x05e5;
                case 56: goto L_0x05d4;
                case 57: goto L_0x05c3;
                case 58: goto L_0x05b2;
                case 59: goto L_0x05a1;
                case 60: goto L_0x058c;
                case 61: goto L_0x0579;
                case 62: goto L_0x0568;
                case 63: goto L_0x0557;
                case 64: goto L_0x0546;
                case 65: goto L_0x0535;
                case 66: goto L_0x0524;
                case 67: goto L_0x0513;
                case 68: goto L_0x04fe;
                default: goto L_0x04fc;
            }
        L_0x04fc:
            goto L_0x0961
        L_0x04fe:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.cy2 r9 = r12.a(r7)
            r14.b(r10, r8, r9)
            goto L_0x0961
        L_0x0513:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = e(r13, r8)
            r14.zze(r10, r8)
            goto L_0x0961
        L_0x0524:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zzf(r10, r8)
            goto L_0x0961
        L_0x0535:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = e(r13, r8)
            r14.zzb(r10, r8)
            goto L_0x0961
        L_0x0546:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0557:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zzb(r10, r8)
            goto L_0x0961
        L_0x0568:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zze(r10, r8)
            goto L_0x0961
        L_0x0579:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.tu2 r8 = (com.fossil.tu2) r8
            r14.a(r10, r8)
            goto L_0x0961
        L_0x058c:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.cy2 r9 = r12.a(r7)
            r14.a(r10, r8, r9)
            goto L_0x0961
        L_0x05a1:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            a(r10, r8, r14)
            goto L_0x0961
        L_0x05b2:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            boolean r8 = f(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x05c3:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zzd(r10, r8)
            goto L_0x0961
        L_0x05d4:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = e(r13, r8)
            r14.zzd(r10, r8)
            goto L_0x0961
        L_0x05e5:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = d(r13, r8)
            r14.zzc(r10, r8)
            goto L_0x0961
        L_0x05f6:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = e(r13, r8)
            r14.zzc(r10, r8)
            goto L_0x0961
        L_0x0607:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = e(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0618:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            float r8 = c(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0629:
            boolean r9 = r12.a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            double r8 = b(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x063a:
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            r12.a(r14, r10, r8, r7)
            goto L_0x0961
        L_0x0645:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.cy2 r10 = r12.a(r7)
            com.fossil.ey2.b(r9, r8, r14, r10)
            goto L_0x0961
        L_0x0658:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.e(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0667:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.j(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0676:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.g(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0685:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.l(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0694:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.m(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06a3:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.i(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06b2:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.n(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06c1:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.k(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06d0:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.f(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06df:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.h(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06ee:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.d(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06fd:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.c(r9, r8, r14, r4)
            goto L_0x0961
        L_0x070c:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.b(r9, r8, r14, r4)
            goto L_0x0961
        L_0x071b:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.a(r9, r8, r14, r4)
            goto L_0x0961
        L_0x072a:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.e(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0739:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.j(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0748:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.g(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0757:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.l(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0766:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.m(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0775:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.i(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0784:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.b(r9, r8, r14)
            goto L_0x0961
        L_0x0793:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.cy2 r10 = r12.a(r7)
            com.fossil.ey2.a(r9, r8, r14, r10)
            goto L_0x0961
        L_0x07a6:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.a(r9, r8, r14)
            goto L_0x0961
        L_0x07b5:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.n(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07c4:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.k(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07d3:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.f(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07e2:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.h(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07f1:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.d(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0800:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.c(r9, r8, r14, r5)
            goto L_0x0961
        L_0x080f:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.b(r9, r8, r14, r5)
            goto L_0x0961
        L_0x081e:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.fossil.ey2.a(r9, r8, r14, r5)
            goto L_0x0961
        L_0x082d:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.cy2 r9 = r12.a(r7)
            r14.b(r10, r8, r9)
            goto L_0x0961
        L_0x0842:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.fossil.bz2.b(r13, r8)
            r14.zze(r10, r8)
            goto L_0x0961
        L_0x0853:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zzf(r10, r8)
            goto L_0x0961
        L_0x0864:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.fossil.bz2.b(r13, r8)
            r14.zzb(r10, r8)
            goto L_0x0961
        L_0x0875:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0886:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zzb(r10, r8)
            goto L_0x0961
        L_0x0897:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zze(r10, r8)
            goto L_0x0961
        L_0x08a8:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.tu2 r8 = (com.fossil.tu2) r8
            r14.a(r10, r8)
            goto L_0x0961
        L_0x08bb:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            com.fossil.cy2 r9 = r12.a(r7)
            r14.a(r10, r8, r9)
            goto L_0x0961
        L_0x08d0:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.fossil.bz2.f(r13, r8)
            a(r10, r8, r14)
            goto L_0x0961
        L_0x08e1:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            boolean r8 = com.fossil.bz2.c(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x08f2:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zzd(r10, r8)
            goto L_0x0961
        L_0x0902:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.fossil.bz2.b(r13, r8)
            r14.zzd(r10, r8)
            goto L_0x0961
        L_0x0912:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.fossil.bz2.a(r13, r8)
            r14.zzc(r10, r8)
            goto L_0x0961
        L_0x0922:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.fossil.bz2.b(r13, r8)
            r14.zzc(r10, r8)
            goto L_0x0961
        L_0x0932:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.fossil.bz2.b(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0942:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            float r8 = com.fossil.bz2.d(r13, r8)
            r14.zza(r10, r8)
            goto L_0x0961
        L_0x0952:
            boolean r9 = r12.a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            double r8 = com.fossil.bz2.e(r13, r8)
            r14.zza(r10, r8)
        L_0x0961:
            int r7 = r7 + 3
            goto L_0x04e9
        L_0x0965:
            com.fossil.pv2<?> r13 = r12.o
            r13.a(r0)
            throw r3
        L_0x096b:
            if (r0 != 0) goto L_0x0973
            com.fossil.uy2<?, ?> r0 = r12.n
            a(r0, r13, r14)
            return
        L_0x0973:
            com.fossil.pv2<?> r13 = r12.o
            r13.a(r14, r0)
            throw r3
        L_0x0979:
            r12.b(r13, r14)
            return
            switch-data {0->0x04a1, 1->0x0491, 2->0x0481, 3->0x0471, 4->0x0461, 5->0x0451, 6->0x0441, 7->0x0430, 8->0x041f, 9->0x040a, 10->0x03f7, 11->0x03e6, 12->0x03d5, 13->0x03c4, 14->0x03b3, 15->0x03a2, 16->0x0391, 17->0x037c, 18->0x036d, 19->0x035e, 20->0x034f, 21->0x0340, 22->0x0331, 23->0x0322, 24->0x0313, 25->0x0304, 26->0x02f5, 27->0x02e2, 28->0x02d3, 29->0x02c4, 30->0x02b5, 31->0x02a6, 32->0x0297, 33->0x0288, 34->0x0279, 35->0x026a, 36->0x025b, 37->0x024c, 38->0x023d, 39->0x022e, 40->0x021f, 41->0x0210, 42->0x0201, 43->0x01f2, 44->0x01e3, 45->0x01d4, 46->0x01c5, 47->0x01b6, 48->0x01a7, 49->0x0194, 50->0x0189, 51->0x0178, 52->0x0167, 53->0x0156, 54->0x0145, 55->0x0134, 56->0x0123, 57->0x0112, 58->0x0101, 59->0x00f0, 60->0x00db, 61->0x00c8, 62->0x00b7, 63->0x00a6, 64->0x0095, 65->0x0084, 66->0x0073, 67->0x0062, 68->0x004d, }
            switch-data {0->0x0952, 1->0x0942, 2->0x0932, 3->0x0922, 4->0x0912, 5->0x0902, 6->0x08f2, 7->0x08e1, 8->0x08d0, 9->0x08bb, 10->0x08a8, 11->0x0897, 12->0x0886, 13->0x0875, 14->0x0864, 15->0x0853, 16->0x0842, 17->0x082d, 18->0x081e, 19->0x080f, 20->0x0800, 21->0x07f1, 22->0x07e2, 23->0x07d3, 24->0x07c4, 25->0x07b5, 26->0x07a6, 27->0x0793, 28->0x0784, 29->0x0775, 30->0x0766, 31->0x0757, 32->0x0748, 33->0x0739, 34->0x072a, 35->0x071b, 36->0x070c, 37->0x06fd, 38->0x06ee, 39->0x06df, 40->0x06d0, 41->0x06c1, 42->0x06b2, 43->0x06a3, 44->0x0694, 45->0x0685, 46->0x0676, 47->0x0667, 48->0x0658, 49->0x0645, 50->0x063a, 51->0x0629, 52->0x0618, 53->0x0607, 54->0x05f6, 55->0x05e5, 56->0x05d4, 57->0x05c3, 58->0x05b2, 59->0x05a1, 60->0x058c, 61->0x0579, 62->0x0568, 63->0x0557, 64->0x0546, 65->0x0535, 66->0x0524, 67->0x0513, 68->0x04fe, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.a(java.lang.Object, com.fossil.oz2):void");
    }

    @DexIgnore
    public final Object b(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    @DexIgnore
    public static <T> double b(T t, long j2) {
        return ((Double) bz2.f(t, j2)).doubleValue();
    }

    @DexIgnore
    public final void b(T t, int i2) {
        int e2 = e(i2);
        long j2 = (long) (1048575 & e2);
        if (j2 != 1048575) {
            bz2.a((Object) t, j2, (1 << (e2 >>> 20)) | bz2.a(t, j2));
        }
    }

    @DexIgnore
    public final void b(T t, int i2, int i3) {
        bz2.a((Object) t, (long) (e(i3) & 1048575), i2);
    }

    @DexIgnore
    public final int b(int i2, int i3) {
        int length = (this.a.length / 3) - 1;
        while (i3 <= length) {
            int i4 = (length + i3) >>> 1;
            int i5 = i4 * 3;
            int i6 = this.a[i5];
            if (i2 == i6) {
                return i5;
            }
            if (i2 < i6) {
                length = i4 - 1;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }

    @DexIgnore
    public final <K, V> void a(oz2 oz2, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            oz2.a(i2, this.p.zzb(b(i3)), this.p.zzc(obj));
        }
    }

    @DexIgnore
    public static <UT, UB> void a(uy2<UT, UB> uy2, T t, oz2 oz2) throws IOException {
        uy2.a(uy2.a(t), oz2);
    }

    @DexIgnore
    public static ty2 a(Object obj) {
        bw2 bw2 = (bw2) obj;
        ty2 ty2 = bw2.zzb;
        if (ty2 != ty2.d()) {
            return ty2;
        }
        ty2 e2 = ty2.e();
        bw2.zzb = e2;
        return e2;
    }

    @DexIgnore
    public static int a(byte[] bArr, int i2, int i3, iz2 iz2, Class<?> cls, ou2 ou2) throws IOException {
        switch (qx2.a[iz2.ordinal()]) {
            case 1:
                int b2 = pu2.b(bArr, i2, ou2);
                ou2.c = Boolean.valueOf(ou2.b != 0);
                return b2;
            case 2:
                return pu2.e(bArr, i2, ou2);
            case 3:
                ou2.c = Double.valueOf(pu2.c(bArr, i2));
                return i2 + 8;
            case 4:
            case 5:
                ou2.c = Integer.valueOf(pu2.a(bArr, i2));
                return i2 + 4;
            case 6:
            case 7:
                ou2.c = Long.valueOf(pu2.b(bArr, i2));
                return i2 + 8;
            case 8:
                ou2.c = Float.valueOf(pu2.d(bArr, i2));
                return i2 + 4;
            case 9:
            case 10:
            case 11:
                int a2 = pu2.a(bArr, i2, ou2);
                ou2.c = Integer.valueOf(ou2.a);
                return a2;
            case 12:
            case 13:
                int b3 = pu2.b(bArr, i2, ou2);
                ou2.c = Long.valueOf(ou2.b);
                return b3;
            case 14:
                return pu2.a(yx2.a().a((Class) cls), bArr, i2, i3, ou2);
            case 15:
                int a3 = pu2.a(bArr, i2, ou2);
                ou2.c = Integer.valueOf(gv2.a(ou2.a));
                return a3;
            case 16:
                int b4 = pu2.b(bArr, i2, ou2);
                ou2.c = Long.valueOf(gv2.a(ou2.b));
                return b4;
            case 17:
                return pu2.d(bArr, i2, ou2);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0422 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01eb  */
    public final int a(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.fossil.ou2 r30) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = com.fossil.nx2.r
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.fossil.jw2 r11 = (com.fossil.jw2) r11
            boolean r12 = r11.zza()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.fossil.jw2 r11 = r11.zza(r12)
            sun.misc.Unsafe r12 = com.fossil.nx2.r
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03e4;
                case 19: goto L_0x03a6;
                case 20: goto L_0x0365;
                case 21: goto L_0x0365;
                case 22: goto L_0x034b;
                case 23: goto L_0x030c;
                case 24: goto L_0x02cd;
                case 25: goto L_0x0276;
                case 26: goto L_0x01c3;
                case 27: goto L_0x01a9;
                case 28: goto L_0x0151;
                case 29: goto L_0x034b;
                case 30: goto L_0x0119;
                case 31: goto L_0x02cd;
                case 32: goto L_0x030c;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e4;
                case 36: goto L_0x03a6;
                case 37: goto L_0x0365;
                case 38: goto L_0x0365;
                case 39: goto L_0x034b;
                case 40: goto L_0x030c;
                case 41: goto L_0x02cd;
                case 42: goto L_0x0276;
                case 43: goto L_0x034b;
                case 44: goto L_0x0119;
                case 45: goto L_0x02cd;
                case 46: goto L_0x030c;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0422
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0422
            com.fossil.cy2 r1 = r0.a(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.fossil.pu2.a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0422
            int r8 = com.fossil.pu2.a(r3, r4, r7)
            int r9 = r7.a
            if (r2 != r9) goto L_0x0422
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.fossil.pu2.a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.fossil.pu2.b(r3, r1, r7)
            long r4 = r7.b
            long r4 = com.fossil.gv2.a(r4)
            r11.a(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0423
        L_0x009e:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0422
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            int r1 = com.fossil.pu2.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = com.fossil.gv2.a(r8)
            r11.a(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.pu2.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = com.fossil.gv2.a(r8)
            r11.a(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.fossil.cw2 r11 = (com.fossil.cw2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.fossil.pu2.a(r3, r1, r7)
            int r4 = r7.a
            int r4 = com.fossil.gv2.a(r4)
            r11.a(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0423
        L_0x00eb:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0422
            com.fossil.cw2 r11 = (com.fossil.cw2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = com.fossil.gv2.a(r4)
            r11.a(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = com.fossil.gv2.a(r4)
            r11.a(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.fossil.pu2.a(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0422
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.fossil.pu2.a(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            com.fossil.bw2 r1 = (com.fossil.bw2) r1
            com.fossil.ty2 r3 = r1.zzb
            com.fossil.ty2 r4 = com.fossil.ty2.d()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.fossil.fw2 r4 = r0.c(r8)
            com.fossil.uy2<?, ?> r5 = r0.n
            r6 = r22
            java.lang.Object r3 = com.fossil.ey2.a(r6, r11, r4, r3, r5)
            com.fossil.ty2 r3 = (com.fossil.ty2) r3
            if (r3 == 0) goto L_0x014e
            r1.zzb = r3
        L_0x014e:
            r1 = r2
            goto L_0x0423
        L_0x0151:
            if (r6 != r10) goto L_0x0422
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x01a4
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x019f
            if (r4 != 0) goto L_0x0167
            com.fossil.tu2 r4 = com.fossil.tu2.zza
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.fossil.tu2 r6 = com.fossil.tu2.zza(r3, r1, r4)
            r11.add(r6)
        L_0x016e:
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x019a
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0195
            if (r4 != 0) goto L_0x018d
            com.fossil.tu2 r4 = com.fossil.tu2.zza
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.fossil.tu2 r6 = com.fossil.tu2.zza(r3, r1, r4)
            r11.add(r6)
            goto L_0x016e
        L_0x0195:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x019a:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x019f:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x01a4:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x01a9:
            if (r6 != r10) goto L_0x0422
            com.fossil.cy2 r1 = r0.a(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.fossil.pu2.a(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0423
        L_0x01c3:
            if (r6 != r10) goto L_0x0422
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x0216
            int r4 = com.fossil.pu2.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0211
            if (r6 != 0) goto L_0x01de
            r11.add(r1)
            goto L_0x01e9
        L_0x01de:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.fossil.ew2.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01e8:
            int r4 = r4 + r6
        L_0x01e9:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.pu2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.pu2.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x020c
            if (r6 != 0) goto L_0x0201
            r11.add(r1)
            goto L_0x01e9
        L_0x0201:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.fossil.ew2.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01e8
        L_0x020c:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x0211:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x0216:
            int r4 = com.fossil.pu2.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0271
            if (r6 != 0) goto L_0x0224
            r11.add(r1)
            goto L_0x0237
        L_0x0224:
            int r8 = r4 + r6
            boolean r9 = com.fossil.dz2.a(r3, r4, r8)
            if (r9 == 0) goto L_0x026c
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.fossil.ew2.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x0236:
            r4 = r8
        L_0x0237:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.pu2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.pu2.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0267
            if (r6 != 0) goto L_0x024f
            r11.add(r1)
            goto L_0x0237
        L_0x024f:
            int r8 = r4 + r6
            boolean r9 = com.fossil.dz2.a(r3, r4, r8)
            if (r9 == 0) goto L_0x0262
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.fossil.ew2.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x0236
        L_0x0262:
            com.fossil.iw2 r1 = com.fossil.iw2.zzh()
            throw r1
        L_0x0267:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x026c:
            com.fossil.iw2 r1 = com.fossil.iw2.zzh()
            throw r1
        L_0x0271:
            com.fossil.iw2 r1 = com.fossil.iw2.zzb()
            throw r1
        L_0x0276:
            r1 = 0
            if (r6 != r10) goto L_0x029e
            com.fossil.ru2 r11 = (com.fossil.ru2) r11
            int r2 = com.fossil.pu2.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = r4 + r2
        L_0x0282:
            if (r2 >= r4) goto L_0x0295
            int r2 = com.fossil.pu2.b(r3, r2, r7)
            long r5 = r7.b
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0290
            r5 = 1
            goto L_0x0291
        L_0x0290:
            r5 = 0
        L_0x0291:
            r11.a(r5)
            goto L_0x0282
        L_0x0295:
            if (r2 != r4) goto L_0x0299
            goto L_0x014e
        L_0x0299:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x029e:
            if (r6 != 0) goto L_0x0422
            com.fossil.ru2 r11 = (com.fossil.ru2) r11
            int r4 = com.fossil.pu2.b(r3, r4, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02ae
            r6 = 1
            goto L_0x02af
        L_0x02ae:
            r6 = 0
        L_0x02af:
            r11.a(r6)
        L_0x02b2:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.fossil.pu2.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0422
            int r4 = com.fossil.pu2.b(r3, r6, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02c8
            r6 = 1
            goto L_0x02c9
        L_0x02c8:
            r6 = 0
        L_0x02c9:
            r11.a(r6)
            goto L_0x02b2
        L_0x02cd:
            if (r6 != r10) goto L_0x02ed
            com.fossil.cw2 r11 = (com.fossil.cw2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x02d8:
            if (r1 >= r2) goto L_0x02e4
            int r4 = com.fossil.pu2.a(r3, r1)
            r11.a(r4)
            int r1 = r1 + 4
            goto L_0x02d8
        L_0x02e4:
            if (r1 != r2) goto L_0x02e8
            goto L_0x0423
        L_0x02e8:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x02ed:
            if (r6 != r9) goto L_0x0422
            com.fossil.cw2 r11 = (com.fossil.cw2) r11
            int r1 = com.fossil.pu2.a(r18, r19)
            r11.a(r1)
        L_0x02f8:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.pu2.a(r3, r4)
            r11.a(r1)
            goto L_0x02f8
        L_0x030c:
            if (r6 != r10) goto L_0x032c
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x0317:
            if (r1 >= r2) goto L_0x0323
            long r4 = com.fossil.pu2.b(r3, r1)
            r11.a(r4)
            int r1 = r1 + 8
            goto L_0x0317
        L_0x0323:
            if (r1 != r2) goto L_0x0327
            goto L_0x0423
        L_0x0327:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x032c:
            if (r6 != r13) goto L_0x0422
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            long r8 = com.fossil.pu2.b(r18, r19)
            r11.a(r8)
        L_0x0337:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            long r8 = com.fossil.pu2.b(r3, r4)
            r11.a(r8)
            goto L_0x0337
        L_0x034b:
            if (r6 != r10) goto L_0x0353
            int r1 = com.fossil.pu2.a(r3, r4, r11, r7)
            goto L_0x0423
        L_0x0353:
            if (r6 != 0) goto L_0x0422
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.fossil.pu2.a(r21, r22, r23, r24, r25, r26)
            goto L_0x0423
        L_0x0365:
            if (r6 != r10) goto L_0x0385
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x0370:
            if (r1 >= r2) goto L_0x037c
            int r1 = com.fossil.pu2.b(r3, r1, r7)
            long r4 = r7.b
            r11.a(r4)
            goto L_0x0370
        L_0x037c:
            if (r1 != r2) goto L_0x0380
            goto L_0x0423
        L_0x0380:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x0385:
            if (r6 != 0) goto L_0x0422
            com.fossil.ww2 r11 = (com.fossil.ww2) r11
            int r1 = com.fossil.pu2.b(r3, r4, r7)
            long r8 = r7.b
            r11.a(r8)
        L_0x0392:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            int r1 = com.fossil.pu2.b(r3, r4, r7)
            long r8 = r7.b
            r11.a(r8)
            goto L_0x0392
        L_0x03a6:
            if (r6 != r10) goto L_0x03c5
            com.fossil.wv2 r11 = (com.fossil.wv2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03b1:
            if (r1 >= r2) goto L_0x03bd
            float r4 = com.fossil.pu2.d(r3, r1)
            r11.a(r4)
            int r1 = r1 + 4
            goto L_0x03b1
        L_0x03bd:
            if (r1 != r2) goto L_0x03c0
            goto L_0x0423
        L_0x03c0:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x03c5:
            if (r6 != r9) goto L_0x0422
            com.fossil.wv2 r11 = (com.fossil.wv2) r11
            float r1 = com.fossil.pu2.d(r18, r19)
            r11.a(r1)
        L_0x03d0:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            float r1 = com.fossil.pu2.d(r3, r4)
            r11.a(r1)
            goto L_0x03d0
        L_0x03e4:
            if (r6 != r10) goto L_0x0403
            com.fossil.mv2 r11 = (com.fossil.mv2) r11
            int r1 = com.fossil.pu2.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03ef:
            if (r1 >= r2) goto L_0x03fb
            double r4 = com.fossil.pu2.c(r3, r1)
            r11.a(r4)
            int r1 = r1 + 8
            goto L_0x03ef
        L_0x03fb:
            if (r1 != r2) goto L_0x03fe
            goto L_0x0423
        L_0x03fe:
            com.fossil.iw2 r1 = com.fossil.iw2.zza()
            throw r1
        L_0x0403:
            if (r6 != r13) goto L_0x0422
            com.fossil.mv2 r11 = (com.fossil.mv2) r11
            double r8 = com.fossil.pu2.c(r18, r19)
            r11.a(r8)
        L_0x040e:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.fossil.pu2.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0423
            double r8 = com.fossil.pu2.c(r3, r4)
            r11.a(r8)
            goto L_0x040e
        L_0x0422:
            r1 = r4
        L_0x0423:
            return r1
            switch-data {18->0x03e4, 19->0x03a6, 20->0x0365, 21->0x0365, 22->0x034b, 23->0x030c, 24->0x02cd, 25->0x0276, 26->0x01c3, 27->0x01a9, 28->0x0151, 29->0x034b, 30->0x0119, 31->0x02cd, 32->0x030c, 33->0x00cc, 34->0x007f, 35->0x03e4, 36->0x03a6, 37->0x0365, 38->0x0365, 39->0x034b, 40->0x030c, 41->0x02cd, 42->0x0276, 43->0x034b, 44->0x0119, 45->0x02cd, 46->0x030c, 47->0x00cc, 48->0x007f, 49->0x003f, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.a(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.fossil.ou2):int");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r10v4, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r10v11, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r10v12, resolved type: byte */
    /* JADX WARN: Multi-variable type inference failed */
    public final <K, V> int a(T t, byte[] bArr, int i2, int i3, int i4, long j2, ou2 ou2) throws IOException {
        Unsafe unsafe = r;
        Object b2 = b(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.p.zzd(object)) {
            Object a2 = this.p.a(b2);
            this.p.zza(a2, object);
            unsafe.putObject(t, j2, a2);
            object = a2;
        }
        ex2<?, ?> zzb = this.p.zzb(b2);
        Map<?, ?> zza = this.p.zza(object);
        int a3 = pu2.a(bArr, i2, ou2);
        int i5 = ou2.a;
        if (i5 < 0 || i5 > i3 - a3) {
            throw iw2.zza();
        }
        int i6 = i5 + a3;
        K k2 = zzb.b;
        V v = zzb.d;
        while (a3 < i6) {
            int i7 = a3 + 1;
            byte b3 = bArr[a3];
            int i8 = b3;
            if (b3 < 0) {
                i7 = pu2.a(b3, bArr, i7, ou2);
                i8 = ou2.a;
            }
            int i9 = i8 >>> 3;
            int i10 = i8 & 7;
            if (i9 != 1) {
                if (i9 == 2 && i10 == zzb.c.zzb()) {
                    a3 = a(bArr, i7, i3, zzb.c, zzb.d.getClass(), ou2);
                    v = (V) ou2.c;
                }
            } else if (i10 == zzb.a.zzb()) {
                a3 = a(bArr, i7, i3, zzb.a, (Class<?>) null, ou2);
                k2 = (K) ou2.c;
            }
            a3 = pu2.a(i8, bArr, i7, i3, ou2);
        }
        if (a3 == i6) {
            zza.put(k2, v);
            return i6;
        }
        throw iw2.zzg();
    }

    @DexIgnore
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, ou2 ou2) throws IOException {
        int i10;
        Unsafe unsafe = r;
        long j3 = (long) (this.a[i9 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Double.valueOf(pu2.c(bArr, i2)));
                    i10 = i2 + 8;
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 52:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Float.valueOf(pu2.d(bArr, i2)));
                    i10 = i2 + 4;
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 53:
            case 54:
                if (i6 == 0) {
                    i10 = pu2.b(bArr, i2, ou2);
                    unsafe.putObject(t, j2, Long.valueOf(ou2.b));
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 55:
            case 62:
                if (i6 == 0) {
                    i10 = pu2.a(bArr, i2, ou2);
                    unsafe.putObject(t, j2, Integer.valueOf(ou2.a));
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 56:
            case 65:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Long.valueOf(pu2.b(bArr, i2)));
                    i10 = i2 + 8;
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 57:
            case 64:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Integer.valueOf(pu2.a(bArr, i2)));
                    i10 = i2 + 4;
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 58:
                if (i6 == 0) {
                    i10 = pu2.b(bArr, i2, ou2);
                    unsafe.putObject(t, j2, Boolean.valueOf(ou2.b != 0));
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 59:
                if (i6 == 2) {
                    int a2 = pu2.a(bArr, i2, ou2);
                    int i11 = ou2.a;
                    if (i11 == 0) {
                        unsafe.putObject(t, j2, "");
                    } else if ((i7 & 536870912) == 0 || dz2.a(bArr, a2, a2 + i11)) {
                        unsafe.putObject(t, j2, new String(bArr, a2, i11, ew2.a));
                        a2 += i11;
                    } else {
                        throw iw2.zzh();
                    }
                    unsafe.putInt(t, j3, i5);
                    return a2;
                }
                return i2;
            case 60:
                if (i6 == 2) {
                    int a3 = pu2.a(a(i9), bArr, i2, i3, ou2);
                    Object object = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t, j2, ou2.c);
                    } else {
                        unsafe.putObject(t, j2, ew2.a(object, ou2.c));
                    }
                    unsafe.putInt(t, j3, i5);
                    return a3;
                }
                return i2;
            case 61:
                if (i6 == 2) {
                    i10 = pu2.e(bArr, i2, ou2);
                    unsafe.putObject(t, j2, ou2.c);
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 63:
                if (i6 == 0) {
                    int a4 = pu2.a(bArr, i2, ou2);
                    int i12 = ou2.a;
                    fw2 c2 = c(i9);
                    if (c2 == null || c2.zza(i12)) {
                        unsafe.putObject(t, j2, Integer.valueOf(i12));
                        i10 = a4;
                        unsafe.putInt(t, j3, i5);
                        return i10;
                    }
                    a(t).a(i4, Long.valueOf((long) i12));
                    return a4;
                }
                return i2;
            case 66:
                if (i6 == 0) {
                    i10 = pu2.a(bArr, i2, ou2);
                    unsafe.putObject(t, j2, Integer.valueOf(gv2.a(ou2.a)));
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 67:
                if (i6 == 0) {
                    i10 = pu2.b(bArr, i2, ou2);
                    unsafe.putObject(t, j2, Long.valueOf(gv2.a(ou2.b)));
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            case 68:
                if (i6 == 3) {
                    i10 = pu2.a(a(i9), bArr, i2, i3, (i4 & -8) | 4, ou2);
                    Object object2 = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t, j2, ou2.c);
                    } else {
                        unsafe.putObject(t, j2, ew2.a(object2, ou2.c));
                    }
                    unsafe.putInt(t, j3, i5);
                    return i10;
                }
                return i2;
            default:
                return i2;
        }
    }

    @DexIgnore
    public final cy2 a(int i2) {
        int i3 = (i2 / 3) << 1;
        cy2 cy2 = (cy2) this.b[i3];
        if (cy2 != null) {
            return cy2;
        }
        cy2<T> a2 = yx2.a().a((Class) ((Class) this.b[i3 + 1]));
        this.b[i3] = a2;
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:112:0x03bb, code lost:
        if (r0 == r3) goto L_0x0424;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x03fe, code lost:
        if (r0 == r15) goto L_0x0424;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(T r28, byte[] r29, int r30, int r31, int r32, com.fossil.ou2 r33) throws java.io.IOException {
        /*
            r27 = this;
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            r9 = r33
            sun.misc.Unsafe r10 = com.fossil.nx2.r
            r16 = 0
            r0 = r30
            r1 = -1
            r2 = 0
            r3 = 0
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
        L_0x0019:
            if (r0 >= r13) goto L_0x04ce
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x002a
            int r0 = com.fossil.pu2.a(r0, r12, r3, r9)
            int r3 = r9.a
            r4 = r3
            r3 = r0
            goto L_0x002b
        L_0x002a:
            r4 = r0
        L_0x002b:
            int r0 = r4 >>> 3
            r7 = r4 & 7
            r8 = 3
            if (r0 <= r1) goto L_0x0038
            int r2 = r2 / r8
            int r1 = r15.a(r0, r2)
            goto L_0x003c
        L_0x0038:
            int r1 = r15.f(r0)
        L_0x003c:
            r2 = r1
            r1 = -1
            if (r2 != r1) goto L_0x004f
            r30 = r0
            r2 = r3
            r8 = r4
            r22 = r5
            r26 = r10
            r7 = r11
            r17 = 0
            r18 = -1
            goto L_0x0428
        L_0x004f:
            int[] r1 = r15.a
            int r19 = r2 + 1
            r8 = r1[r19]
            r19 = 267386880(0xff00000, float:2.3665827E-29)
            r19 = r8 & r19
            int r11 = r19 >>> 20
            r19 = r4
            r17 = 1048575(0xfffff, float:1.469367E-39)
            r4 = r8 & r17
            long r12 = (long) r4
            r4 = 17
            r20 = r8
            if (r11 > r4) goto L_0x0325
            int r4 = r2 + 2
            r1 = r1[r4]
            int r4 = r1 >>> 20
            r8 = 1
            int r22 = r8 << r4
            r4 = 1048575(0xfffff, float:1.469367E-39)
            r1 = r1 & r4
            if (r1 == r6) goto L_0x0085
            if (r6 == r4) goto L_0x007e
            long r8 = (long) r6
            r10.putInt(r14, r8, r5)
        L_0x007e:
            long r5 = (long) r1
            int r5 = r10.getInt(r14, r5)
            r8 = r1
            goto L_0x0086
        L_0x0085:
            r8 = r6
        L_0x0086:
            r6 = r5
            r1 = 5
            switch(r11) {
                case 0: goto L_0x02ee;
                case 1: goto L_0x02d2;
                case 2: goto L_0x02a9;
                case 3: goto L_0x02a9;
                case 4: goto L_0x028c;
                case 5: goto L_0x0265;
                case 6: goto L_0x023d;
                case 7: goto L_0x0210;
                case 8: goto L_0x01e6;
                case 9: goto L_0x01ab;
                case 10: goto L_0x018d;
                case 11: goto L_0x028c;
                case 12: goto L_0x014e;
                case 13: goto L_0x023d;
                case 14: goto L_0x0265;
                case 15: goto L_0x012d;
                case 16: goto L_0x00f9;
                case 17: goto L_0x009c;
                default: goto L_0x008b;
            }
        L_0x008b:
            r12 = r29
            r13 = r33
            r9 = r0
            r11 = r2
            r30 = r8
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            goto L_0x0316
        L_0x009c:
            r5 = 3
            if (r7 != r5) goto L_0x00e7
            int r1 = r0 << 3
            r5 = r1 | 4
            com.fossil.cy2 r1 = r15.a(r2)
            r9 = r0
            r0 = r1
            r18 = -1
            r1 = r29
            r11 = r2
            r2 = r3
            r3 = r31
            r7 = r19
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r4 = r5
            r5 = r33
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x00c9
            r4 = r33
            java.lang.Object r1 = r4.c
            r10.putObject(r14, r12, r1)
            goto L_0x00d8
        L_0x00c9:
            r4 = r33
            java.lang.Object r1 = r10.getObject(r14, r12)
            java.lang.Object r2 = r4.c
            java.lang.Object r1 = com.fossil.ew2.a(r1, r2)
            r10.putObject(r14, r12, r1)
        L_0x00d8:
            r5 = r6 | r22
            r12 = r29
            r13 = r31
            r3 = r7
            r6 = r8
            r1 = r9
            r2 = r11
            r11 = r32
            r9 = r4
            goto L_0x0019
        L_0x00e7:
            r9 = r0
            r11 = r2
            r7 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            r30 = r8
            r8 = r7
            goto L_0x0316
        L_0x00f9:
            r4 = r33
            r9 = r0
            r11 = r2
            r5 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            if (r7 != 0) goto L_0x0125
            r1 = r12
            r12 = r29
            int r7 = com.fossil.pu2.b(r12, r3, r4)
            r20 = r1
            long r0 = r4.b
            long r23 = com.fossil.gv2.a(r0)
            r0 = r10
            r2 = r20
            r1 = r28
            r13 = r4
            r30 = r8
            r8 = r5
            r4 = r23
            r0.putLong(r1, r2, r4)
            goto L_0x02cc
        L_0x0125:
            r12 = r29
            r13 = r4
            r30 = r8
            r8 = r5
            goto L_0x0316
        L_0x012d:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != 0) goto L_0x0316
            int r0 = com.fossil.pu2.a(r12, r3, r13)
            int r1 = r13.a
            int r1 = com.fossil.gv2.a(r1)
            r10.putInt(r14, r4, r1)
            goto L_0x030a
        L_0x014e:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != 0) goto L_0x0316
            int r0 = com.fossil.pu2.a(r12, r3, r13)
            int r1 = r13.a
            com.fossil.fw2 r2 = r15.c(r11)
            if (r2 == 0) goto L_0x0188
            boolean r2 = r2.zza(r1)
            if (r2 == 0) goto L_0x0173
            goto L_0x0188
        L_0x0173:
            com.fossil.ty2 r2 = a(r28)
            long r3 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            r2.a(r8, r1)
            r5 = r6
            r3 = r8
            r1 = r9
            r2 = r11
            r9 = r13
            r6 = r30
            goto L_0x0312
        L_0x0188:
            r10.putInt(r14, r4, r1)
            goto L_0x030a
        L_0x018d:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r0 = 2
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != r0) goto L_0x0316
            int r0 = com.fossil.pu2.e(r12, r3, r13)
            java.lang.Object r1 = r13.c
            r10.putObject(r14, r4, r1)
            goto L_0x030a
        L_0x01ab:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r0 = 2
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != r0) goto L_0x01e2
            com.fossil.cy2 r0 = r15.a(r11)
            r2 = r31
            int r0 = com.fossil.pu2.a(r0, r12, r3, r2, r13)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x01d3
            java.lang.Object r1 = r13.c
            r10.putObject(r14, r4, r1)
            goto L_0x025a
        L_0x01d3:
            java.lang.Object r1 = r10.getObject(r14, r4)
            java.lang.Object r3 = r13.c
            java.lang.Object r1 = com.fossil.ew2.a(r1, r3)
            r10.putObject(r14, r4, r1)
            goto L_0x025a
        L_0x01e2:
            r2 = r31
            goto L_0x0316
        L_0x01e6:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r0 = 2
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r2 = r31
            r13 = r33
            if (r7 != r0) goto L_0x0316
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r20 & r0
            if (r0 != 0) goto L_0x0206
            int r0 = com.fossil.pu2.c(r12, r3, r13)
            goto L_0x020a
        L_0x0206:
            int r0 = com.fossil.pu2.d(r12, r3, r13)
        L_0x020a:
            java.lang.Object r1 = r13.c
            r10.putObject(r14, r4, r1)
            goto L_0x025a
        L_0x0210:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r2 = r31
            r13 = r33
            if (r7 != 0) goto L_0x0316
            int r0 = com.fossil.pu2.b(r12, r3, r13)
            r3 = r0
            long r0 = r13.b
            r20 = 0
            int r7 = (r0 > r20 ? 1 : (r0 == r20 ? 0 : -1))
            if (r7 == 0) goto L_0x0233
            r0 = 1
            goto L_0x0234
        L_0x0233:
            r0 = 0
        L_0x0234:
            com.fossil.bz2.a(r14, r4, r0)
            r5 = r6 | r22
            r6 = r30
            r0 = r3
            goto L_0x025e
        L_0x023d:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r2 = r31
            r13 = r33
            if (r7 != r1) goto L_0x0316
            int r0 = com.fossil.pu2.a(r12, r3)
            r10.putInt(r14, r4, r0)
            int r0 = r3 + 4
        L_0x025a:
            r5 = r6 | r22
            r6 = r30
        L_0x025e:
            r3 = r8
            r1 = r9
            r9 = r13
            r13 = r2
            r2 = r11
            goto L_0x04ca
        L_0x0265:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r0 = 1
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r2 = r31
            r13 = r33
            if (r7 != r0) goto L_0x0316
            long r20 = com.fossil.pu2.b(r12, r3)
            r0 = r10
            r1 = r28
            r7 = r3
            r2 = r4
            r4 = r20
            r0.putLong(r1, r2, r4)
            int r0 = r7 + 8
            goto L_0x030a
        L_0x028c:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != 0) goto L_0x0316
            int r0 = com.fossil.pu2.a(r12, r3, r13)
            int r1 = r13.a
            r10.putInt(r14, r4, r1)
            goto L_0x030a
        L_0x02a9:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != 0) goto L_0x0316
            int r7 = com.fossil.pu2.b(r12, r3, r13)
            long r2 = r13.b
            r0 = r10
            r1 = r28
            r20 = r2
            r2 = r4
            r4 = r20
            r0.putLong(r1, r2, r4)
        L_0x02cc:
            r5 = r6 | r22
            r6 = r30
            r0 = r7
            goto L_0x030e
        L_0x02d2:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != r1) goto L_0x0316
            float r0 = com.fossil.pu2.d(r12, r3)
            com.fossil.bz2.a(r14, r4, r0)
            int r0 = r3 + 4
            goto L_0x030a
        L_0x02ee:
            r9 = r0
            r11 = r2
            r30 = r8
            r4 = r12
            r8 = r19
            r0 = 1
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r33
            if (r7 != r0) goto L_0x0316
            double r0 = com.fossil.pu2.c(r12, r3)
            com.fossil.bz2.a(r14, r4, r0)
            int r0 = r3 + 8
        L_0x030a:
            r5 = r6 | r22
            r6 = r30
        L_0x030e:
            r3 = r8
            r1 = r9
            r2 = r11
            r9 = r13
        L_0x0312:
            r13 = r31
            goto L_0x04ca
        L_0x0316:
            r7 = r32
            r2 = r3
            r22 = r6
            r26 = r10
            r17 = r11
            r6 = r30
            r30 = r9
            goto L_0x0428
        L_0x0325:
            r4 = r2
            r1 = r12
            r8 = r19
            r18 = -1
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r12 = r29
            r13 = r9
            r9 = r0
            r0 = 27
            if (r11 != r0) goto L_0x038c
            r0 = 2
            if (r7 != r0) goto L_0x037d
            java.lang.Object r0 = r10.getObject(r14, r1)
            com.fossil.jw2 r0 = (com.fossil.jw2) r0
            boolean r7 = r0.zza()
            if (r7 != 0) goto L_0x0357
            int r7 = r0.size()
            if (r7 != 0) goto L_0x034e
            r7 = 10
            goto L_0x0350
        L_0x034e:
            int r7 = r7 << 1
        L_0x0350:
            com.fossil.jw2 r0 = r0.zza(r7)
            r10.putObject(r14, r1, r0)
        L_0x0357:
            r7 = r0
            com.fossil.cy2 r0 = r15.a(r4)
            r1 = r8
            r2 = r29
            r17 = r4
            r4 = r31
            r22 = r5
            r5 = r7
            r23 = r6
            r6 = r33
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5, r6)
            r11 = r32
            r3 = r8
            r1 = r9
            r9 = r13
            r2 = r17
            r5 = r22
            r6 = r23
            r13 = r31
            goto L_0x0019
        L_0x037d:
            r17 = r4
            r22 = r5
            r23 = r6
            r15 = r3
            r19 = r8
            r30 = r9
            r26 = r10
            goto L_0x0401
        L_0x038c:
            r17 = r4
            r22 = r5
            r23 = r6
            r0 = 49
            if (r11 > r0) goto L_0x03d9
            r6 = r20
            long r5 = (long) r6
            r0 = r27
            r24 = r1
            r1 = r28
            r2 = r29
            r4 = r3
            r15 = r4
            r4 = r31
            r20 = r5
            r5 = r8
            r6 = r9
            r19 = r8
            r8 = r17
            r30 = r9
            r26 = r10
            r9 = r20
            r12 = r24
            r14 = r33
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x03bf
            goto L_0x0424
        L_0x03bf:
            r15 = r27
            r14 = r28
            r12 = r29
            r1 = r30
            r13 = r31
            r11 = r32
            r9 = r33
            r2 = r17
            r3 = r19
            r5 = r22
            r6 = r23
            r10 = r26
            goto L_0x0019
        L_0x03d9:
            r24 = r1
            r15 = r3
            r19 = r8
            r30 = r9
            r26 = r10
            r6 = r20
            r0 = 50
            if (r11 != r0) goto L_0x0409
            r0 = 2
            if (r7 != r0) goto L_0x0401
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r5 = r17
            r6 = r24
            r8 = r33
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x03bf
            goto L_0x0424
        L_0x0401:
            r7 = r32
            r2 = r15
        L_0x0404:
            r8 = r19
            r6 = r23
            goto L_0x0428
        L_0x0409:
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r5 = r19
            r8 = r6
            r6 = r30
            r9 = r11
            r10 = r24
            r12 = r17
            r13 = r33
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x04b2
        L_0x0424:
            r7 = r32
            r2 = r0
            goto L_0x0404
        L_0x0428:
            if (r8 != r7) goto L_0x043a
            if (r7 != 0) goto L_0x042d
            goto L_0x043a
        L_0x042d:
            r1 = 1048575(0xfffff, float:1.469367E-39)
            r9 = r27
            r12 = r28
            r0 = r2
            r3 = r8
            r5 = r22
            goto L_0x04da
        L_0x043a:
            r9 = r27
            boolean r0 = r9.f
            if (r0 == 0) goto L_0x048b
            r10 = r33
            com.fossil.nv2 r0 = r10.d
            com.fossil.nv2 r1 = com.fossil.nv2.a()
            if (r0 == r1) goto L_0x0486
            com.fossil.jx2 r0 = r9.e
            com.fossil.nv2 r1 = r10.d
            r11 = r30
            com.fossil.bw2$d r0 = r1.a(r0, r11)
            if (r0 != 0) goto L_0x0476
            com.fossil.ty2 r4 = a(r28)
            r0 = r8
            r1 = r29
            r3 = r31
            r5 = r33
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5)
            r14 = r28
            r12 = r29
            r13 = r31
            r3 = r8
            r15 = r9
            r9 = r10
            r1 = r11
            r2 = r17
            r5 = r22
            r10 = r26
            goto L_0x04af
        L_0x0476:
            r12 = r28
            r0 = r12
            com.fossil.bw2$b r0 = (com.fossil.bw2.b) r0
            r0.zza()
            com.fossil.qv2<com.fossil.bw2$e> r0 = r0.zzc
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x0486:
            r12 = r28
            r11 = r30
            goto L_0x0491
        L_0x048b:
            r12 = r28
            r11 = r30
            r10 = r33
        L_0x0491:
            com.fossil.ty2 r4 = a(r28)
            r0 = r8
            r1 = r29
            r3 = r31
            r5 = r33
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5)
            r13 = r31
            r3 = r8
            r15 = r9
            r9 = r10
            r1 = r11
            r14 = r12
            r2 = r17
            r5 = r22
            r10 = r26
            r12 = r29
        L_0x04af:
            r11 = r7
            goto L_0x0019
        L_0x04b2:
            r11 = r30
            r8 = r19
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r9 = r33
            r3 = r8
            r1 = r11
            r2 = r17
            r5 = r22
            r6 = r23
            r10 = r26
        L_0x04ca:
            r11 = r32
            goto L_0x0019
        L_0x04ce:
            r22 = r5
            r23 = r6
            r26 = r10
            r7 = r11
            r12 = r14
            r9 = r15
            r1 = 1048575(0xfffff, float:1.469367E-39)
        L_0x04da:
            if (r6 == r1) goto L_0x04e2
            long r1 = (long) r6
            r4 = r26
            r4.putInt(r12, r1, r5)
        L_0x04e2:
            r1 = 0
            int r2 = r9.j
        L_0x04e5:
            int r4 = r9.k
            if (r2 >= r4) goto L_0x04f8
            int[] r4 = r9.i
            r4 = r4[r2]
            com.fossil.uy2<?, ?> r5 = r9.n
            java.lang.Object r1 = r9.a(r12, r4, r1, r5)
            com.fossil.ty2 r1 = (com.fossil.ty2) r1
            int r2 = r2 + 1
            goto L_0x04e5
        L_0x04f8:
            if (r1 == 0) goto L_0x04ff
            com.fossil.uy2<?, ?> r2 = r9.n
            r2.b(r12, r1)
        L_0x04ff:
            if (r7 != 0) goto L_0x050b
            r1 = r31
            if (r0 != r1) goto L_0x0506
            goto L_0x0511
        L_0x0506:
            com.fossil.iw2 r0 = com.fossil.iw2.zzg()
            throw r0
        L_0x050b:
            r1 = r31
            if (r0 > r1) goto L_0x0512
            if (r3 != r7) goto L_0x0512
        L_0x0511:
            return r0
        L_0x0512:
            com.fossil.iw2 r0 = com.fossil.iw2.zzg()
            throw r0
            switch-data {0->0x02ee, 1->0x02d2, 2->0x02a9, 3->0x02a9, 4->0x028c, 5->0x0265, 6->0x023d, 7->0x0210, 8->0x01e6, 9->0x01ab, 10->0x018d, 11->0x028c, 12->0x014e, 13->0x023d, 14->0x0265, 15->0x012d, 16->0x00f9, 17->0x009c, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.a(java.lang.Object, byte[], int, int, int, com.fossil.ou2):int");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v11, types: [int] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02dc, code lost:
        if (r0 == r4) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0323, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0346, code lost:
        if (r0 == r15) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0348, code lost:
        r2 = r0;
     */
    @DexIgnore
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.cy2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r31, byte[] r32, int r33, int r34, com.fossil.ou2 r35) throws java.io.IOException {
        /*
            r30 = this;
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            boolean r0 = r15.g
            if (r0 == 0) goto L_0x038d
            sun.misc.Unsafe r9 = com.fossil.nx2.r
            r10 = -1
            r16 = 0
            r8 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r33
            r1 = -1
            r2 = 0
            r6 = 0
            r7 = 1048575(0xfffff, float:1.469367E-39)
        L_0x001e:
            if (r0 >= r13) goto L_0x0370
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0030
            int r0 = com.fossil.pu2.a(r0, r12, r3, r11)
            int r3 = r11.a
            r4 = r0
            r17 = r3
            goto L_0x0033
        L_0x0030:
            r17 = r0
            r4 = r3
        L_0x0033:
            int r5 = r17 >>> 3
            r3 = r17 & 7
            if (r5 <= r1) goto L_0x0040
            int r2 = r2 / 3
            int r0 = r15.a(r5, r2)
            goto L_0x0044
        L_0x0040:
            int r0 = r15.f(r5)
        L_0x0044:
            r2 = r0
            if (r2 != r10) goto L_0x0052
            r2 = r4
            r25 = r5
            r29 = r9
            r18 = 0
        L_0x004e:
            r20 = -1
            goto L_0x034a
        L_0x0052:
            int[] r0 = r15.a
            int r1 = r2 + 1
            r1 = r0[r1]
            r18 = 267386880(0xff00000, float:2.3665827E-29)
            r18 = r1 & r18
            int r10 = r18 >>> 20
            r33 = r5
            r5 = r1 & r8
            r18 = r9
            long r8 = (long) r5
            r5 = 17
            r21 = r1
            if (r10 > r5) goto L_0x0242
            int r5 = r2 + 2
            r0 = r0[r5]
            int r5 = r0 >>> 20
            r1 = 1
            int r23 = r1 << r5
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r5
            r20 = r2
            if (r0 == r7) goto L_0x0092
            if (r7 == r5) goto L_0x0085
            long r1 = (long) r7
            r7 = r18
            r7.putInt(r14, r1, r6)
            goto L_0x0087
        L_0x0085:
            r7 = r18
        L_0x0087:
            if (r0 == r5) goto L_0x008f
            long r1 = (long) r0
            int r1 = r7.getInt(r14, r1)
            r6 = r1
        L_0x008f:
            r2 = r7
            r7 = r0
            goto L_0x0094
        L_0x0092:
            r2 = r18
        L_0x0094:
            r0 = 5
            switch(r10) {
                case 0: goto L_0x021d;
                case 1: goto L_0x0206;
                case 2: goto L_0x01e4;
                case 3: goto L_0x01e4;
                case 4: goto L_0x01cd;
                case 5: goto L_0x01ab;
                case 6: goto L_0x0194;
                case 7: goto L_0x0174;
                case 8: goto L_0x0151;
                case 9: goto L_0x0124;
                case 10: goto L_0x010c;
                case 11: goto L_0x01cd;
                case 12: goto L_0x00f5;
                case 13: goto L_0x0194;
                case 14: goto L_0x01ab;
                case 15: goto L_0x00da;
                case 16: goto L_0x00a5;
                default: goto L_0x0098;
            }
        L_0x0098:
            r25 = r33
            r5 = r4
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            goto L_0x0239
        L_0x00a5:
            if (r3 != 0) goto L_0x00cc
            int r10 = com.fossil.pu2.b(r12, r4, r11)
            long r0 = r11.b
            long r17 = com.fossil.gv2.a(r0)
            r0 = r2
            r1 = r31
            r4 = r20
            r20 = r7
            r7 = r2
            r2 = r8
            r25 = r33
            r8 = r4
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r4 = r17
            r0.putLong(r1, r2, r4)
            r6 = r6 | r23
            r9 = r7
            r2 = r8
            r0 = r10
            goto L_0x028c
        L_0x00cc:
            r25 = r33
            r8 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            r5 = r4
            r10 = r8
            goto L_0x0239
        L_0x00da:
            r25 = r33
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != 0) goto L_0x01ca
            int r0 = com.fossil.pu2.a(r12, r4, r11)
            int r1 = r11.a
            int r1 = com.fossil.gv2.a(r1)
            r7.putInt(r14, r8, r1)
            goto L_0x0234
        L_0x00f5:
            r25 = r33
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != 0) goto L_0x01ca
            int r0 = com.fossil.pu2.a(r12, r4, r11)
            int r1 = r11.a
            r7.putInt(r14, r8, r1)
            goto L_0x0234
        L_0x010c:
            r25 = r33
            r10 = r20
            r0 = 2
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x01ca
            int r0 = com.fossil.pu2.e(r12, r4, r11)
            java.lang.Object r1 = r11.c
            r7.putObject(r14, r8, r1)
            goto L_0x0234
        L_0x0124:
            r25 = r33
            r10 = r20
            r0 = 2
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x01ca
            com.fossil.cy2 r0 = r15.a(r10)
            int r0 = com.fossil.pu2.a(r0, r12, r4, r13, r11)
            java.lang.Object r1 = r7.getObject(r14, r8)
            if (r1 != 0) goto L_0x0146
            java.lang.Object r1 = r11.c
            r7.putObject(r14, r8, r1)
            goto L_0x0234
        L_0x0146:
            java.lang.Object r2 = r11.c
            java.lang.Object r1 = com.fossil.ew2.a(r1, r2)
            r7.putObject(r14, r8, r1)
            goto L_0x0234
        L_0x0151:
            r25 = r33
            r10 = r20
            r0 = 2
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x01ca
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r21 & r0
            if (r0 != 0) goto L_0x0169
            int r0 = com.fossil.pu2.c(r12, r4, r11)
            goto L_0x016d
        L_0x0169:
            int r0 = com.fossil.pu2.d(r12, r4, r11)
        L_0x016d:
            java.lang.Object r1 = r11.c
            r7.putObject(r14, r8, r1)
            goto L_0x0234
        L_0x0174:
            r25 = r33
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != 0) goto L_0x01ca
            int r0 = com.fossil.pu2.b(r12, r4, r11)
            long r1 = r11.b
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x018e
            r1 = 1
            goto L_0x018f
        L_0x018e:
            r1 = 0
        L_0x018f:
            com.fossil.bz2.a(r14, r8, r1)
            goto L_0x0234
        L_0x0194:
            r25 = r33
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x01ca
            int r0 = com.fossil.pu2.a(r12, r4)
            r7.putInt(r14, r8, r0)
            int r0 = r4 + 4
            goto L_0x0234
        L_0x01ab:
            r25 = r33
            r10 = r20
            r0 = 1
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x01ca
            long r17 = com.fossil.pu2.b(r12, r4)
            r0 = r7
            r1 = r31
            r2 = r8
            r8 = r4
            r4 = r17
            r0.putLong(r1, r2, r4)
            int r0 = r8 + 8
            goto L_0x0234
        L_0x01ca:
            r5 = r4
            goto L_0x0239
        L_0x01cd:
            r25 = r33
            r5 = r4
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != 0) goto L_0x0239
            int r0 = com.fossil.pu2.a(r12, r5, r11)
            int r1 = r11.a
            r7.putInt(r14, r8, r1)
            goto L_0x0234
        L_0x01e4:
            r25 = r33
            r5 = r4
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != 0) goto L_0x0239
            int r17 = com.fossil.pu2.b(r12, r5, r11)
            long r4 = r11.b
            r0 = r7
            r1 = r31
            r2 = r8
            r0.putLong(r1, r2, r4)
            r6 = r6 | r23
            r9 = r7
            r2 = r10
            r0 = r17
            goto L_0x028c
        L_0x0206:
            r25 = r33
            r5 = r4
            r10 = r20
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x0239
            float r0 = com.fossil.pu2.d(r12, r5)
            com.fossil.bz2.a(r14, r8, r0)
            int r0 = r5 + 4
            goto L_0x0234
        L_0x021d:
            r25 = r33
            r5 = r4
            r10 = r20
            r0 = 1
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r7
            r7 = r2
            if (r3 != r0) goto L_0x0239
            double r0 = com.fossil.pu2.c(r12, r5)
            com.fossil.bz2.a(r14, r8, r0)
            int r0 = r5 + 8
        L_0x0234:
            r6 = r6 | r23
            r9 = r7
            r2 = r10
            goto L_0x028c
        L_0x0239:
            r2 = r5
            r29 = r7
            r18 = r10
            r7 = r20
            goto L_0x004e
        L_0x0242:
            r25 = r33
            r5 = r4
            r20 = r7
            r7 = r18
            r26 = 1048575(0xfffff, float:1.469367E-39)
            r4 = r2
            r0 = 27
            if (r10 != r0) goto L_0x029f
            r0 = 2
            if (r3 != r0) goto L_0x0292
            java.lang.Object r0 = r7.getObject(r14, r8)
            com.fossil.jw2 r0 = (com.fossil.jw2) r0
            boolean r1 = r0.zza()
            if (r1 != 0) goto L_0x0272
            int r1 = r0.size()
            if (r1 != 0) goto L_0x0269
            r1 = 10
            goto L_0x026b
        L_0x0269:
            int r1 = r1 << 1
        L_0x026b:
            com.fossil.jw2 r0 = r0.zza(r1)
            r7.putObject(r14, r8, r0)
        L_0x0272:
            r8 = r0
            com.fossil.cy2 r0 = r15.a(r4)
            r1 = r17
            r2 = r32
            r3 = r5
            r18 = r4
            r4 = r34
            r5 = r8
            r8 = r6
            r6 = r35
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5, r6)
            r9 = r7
            r6 = r8
            r2 = r18
        L_0x028c:
            r7 = r20
            r1 = r25
            goto L_0x036a
        L_0x0292:
            r18 = r4
            r15 = r5
            r27 = r6
            r29 = r7
            r28 = r20
            r20 = -1
            goto L_0x0326
        L_0x029f:
            r18 = r4
            r0 = 49
            if (r10 > r0) goto L_0x02f4
            r1 = r21
            long r1 = (long) r1
            r0 = r30
            r21 = r1
            r1 = r31
            r2 = r32
            r4 = r3
            r3 = r5
            r33 = r4
            r4 = r34
            r15 = r5
            r5 = r17
            r27 = r6
            r6 = r25
            r28 = r20
            r20 = r7
            r7 = r33
            r23 = r8
            r9 = 1048575(0xfffff, float:1.469367E-39)
            r8 = r18
            r19 = r10
            r29 = r20
            r20 = -1
            r9 = r21
            r11 = r19
            r12 = r23
            r14 = r35
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x02e0
            goto L_0x0348
        L_0x02e0:
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r2 = r18
            r1 = r25
            r6 = r27
            r7 = r28
            goto L_0x0368
        L_0x02f4:
            r33 = r3
            r15 = r5
            r27 = r6
            r29 = r7
            r23 = r8
            r19 = r10
            r28 = r20
            r1 = r21
            r20 = -1
            r0 = 50
            r9 = r19
            if (r9 != r0) goto L_0x032c
            r7 = r33
            r0 = 2
            if (r7 != r0) goto L_0x0326
            r0 = r30
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r5 = r18
            r6 = r23
            r8 = r35
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x02e0
            goto L_0x0348
        L_0x0326:
            r2 = r15
        L_0x0327:
            r6 = r27
            r7 = r28
            goto L_0x034a
        L_0x032c:
            r7 = r33
            r0 = r30
            r8 = r1
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r5 = r17
            r6 = r25
            r10 = r23
            r12 = r18
            r13 = r35
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x02e0
        L_0x0348:
            r2 = r0
            goto L_0x0327
        L_0x034a:
            com.fossil.ty2 r4 = a(r31)
            r0 = r17
            r1 = r32
            r3 = r34
            r5 = r35
            int r0 = com.fossil.pu2.a(r0, r1, r2, r3, r4, r5)
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r2 = r18
            r1 = r25
        L_0x0368:
            r9 = r29
        L_0x036a:
            r8 = 1048575(0xfffff, float:1.469367E-39)
            r10 = -1
            goto L_0x001e
        L_0x0370:
            r27 = r6
            r29 = r9
            r1 = 1048575(0xfffff, float:1.469367E-39)
            if (r7 == r1) goto L_0x0383
            long r1 = (long) r7
            r3 = r31
            r6 = r27
            r4 = r29
            r4.putInt(r3, r1, r6)
        L_0x0383:
            r4 = r34
            if (r0 != r4) goto L_0x0388
            return
        L_0x0388:
            com.fossil.iw2 r0 = com.fossil.iw2.zzg()
            throw r0
        L_0x038d:
            r4 = r13
            r3 = r14
            r5 = 0
            r0 = r30
            r1 = r31
            r2 = r32
            r3 = r33
            r4 = r34
            r6 = r35
            r0.a(r1, r2, r3, r4, r5, r6)
            return
            switch-data {0->0x021d, 1->0x0206, 2->0x01e4, 3->0x01e4, 4->0x01cd, 5->0x01ab, 6->0x0194, 7->0x0174, 8->0x0151, 9->0x0124, 10->0x010c, 11->0x01cd, 12->0x00f5, 13->0x0194, 14->0x01ab, 15->0x00da, 16->0x00a5, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nx2.a(java.lang.Object, byte[], int, int, com.fossil.ou2):void");
    }

    @DexIgnore
    public final <UT, UB> UB a(Object obj, int i2, UB ub, uy2<UT, UB> uy2) {
        fw2 c2;
        int i3 = this.a[i2];
        Object f2 = bz2.f(obj, (long) (d(i2) & 1048575));
        return (f2 == null || (c2 = c(i2)) == null) ? ub : (UB) a(i2, i3, (Map<K, V>) this.p.zza(f2), c2, ub, uy2);
    }

    @DexIgnore
    public final <K, V, UT, UB> UB a(int i2, int i3, Map<K, V> map, fw2 fw2, UB ub, uy2<UT, UB> uy2) {
        UB ub2;
        ex2<?, ?> zzb = this.p.zzb(b(i2));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!fw2.zza(next.getValue().intValue())) {
                if (ub2 == null) {
                    ub2 = uy2.a();
                }
                cv2 zzc = tu2.zzc(bx2.a(zzb, next.getKey(), next.getValue()));
                try {
                    bx2.a(zzc.b(), zzb, next.getKey(), next.getValue());
                    uy2.a(ub2, i3, zzc.a());
                    it.remove();
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
        return ub2;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.cy2 */
    /* JADX WARN: Multi-variable type inference failed */
    public static boolean a(Object obj, int i2, cy2 cy2) {
        return cy2.zzd(bz2.f(obj, (long) (i2 & 1048575)));
    }

    @DexIgnore
    public static void a(int i2, Object obj, oz2 oz2) throws IOException {
        if (obj instanceof String) {
            oz2.zza(i2, (String) obj);
        } else {
            oz2.a(i2, (tu2) obj);
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3, int i4, int i5) {
        if (i3 == 1048575) {
            return a((Object) t, i2);
        }
        return (i4 & i5) != 0;
    }

    @DexIgnore
    public final boolean a(T t, int i2) {
        int e2 = e(i2);
        long j2 = (long) (e2 & 1048575);
        if (j2 == 1048575) {
            int d2 = d(i2);
            long j3 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    return bz2.e(t, j3) != 0.0d;
                case 1:
                    return bz2.d(t, j3) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return bz2.b(t, j3) != 0;
                case 3:
                    return bz2.b(t, j3) != 0;
                case 4:
                    return bz2.a(t, j3) != 0;
                case 5:
                    return bz2.b(t, j3) != 0;
                case 6:
                    return bz2.a(t, j3) != 0;
                case 7:
                    return bz2.c(t, j3);
                case 8:
                    Object f2 = bz2.f(t, j3);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof tu2) {
                        return !tu2.zza.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return bz2.f(t, j3) != null;
                case 10:
                    return !tu2.zza.equals(bz2.f(t, j3));
                case 11:
                    return bz2.a(t, j3) != 0;
                case 12:
                    return bz2.a(t, j3) != 0;
                case 13:
                    return bz2.a(t, j3) != 0;
                case 14:
                    return bz2.b(t, j3) != 0;
                case 15:
                    return bz2.a(t, j3) != 0;
                case 16:
                    return bz2.b(t, j3) != 0;
                case 17:
                    return bz2.f(t, j3) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return (bz2.a(t, j2) & (1 << (e2 >>> 20))) != 0;
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3) {
        return bz2.a(t, (long) (e(i3) & 1048575)) == i2;
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return b(i2, i3);
    }
}
