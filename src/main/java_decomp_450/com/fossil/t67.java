package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t67 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<t67> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ w67 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<t67> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public t67 createFromParcel(Parcel parcel) {
            return new t67(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public t67[] newArray(int i) {
            return new t67[i];
        }
    }

    @DexIgnore
    public /* synthetic */ t67(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public w67 a() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeParcelable(this.b, i);
        parcel.writeSerializable(this.c);
    }

    @DexIgnore
    public t67(Intent intent, int i, w67 w67) {
        this.b = intent;
        this.a = i;
        this.c = w67;
    }

    @DexIgnore
    public void a(Activity activity) {
        activity.startActivityForResult(this.b, this.a);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        fragment.startActivityForResult(this.b, this.a);
    }

    @DexIgnore
    public t67(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = (Intent) parcel.readParcelable(t67.class.getClassLoader());
        this.c = (w67) parcel.readSerializable();
    }
}
