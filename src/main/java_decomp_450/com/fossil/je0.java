package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class je0 extends k60 {
    @DexIgnore
    public /* final */ ke0 a;

    @DexIgnore
    public je0(ke0 ke0) {
        this.a = ke0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.t4, getErrorCode().getLogName());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public ke0 getErrorCode() {
        return this.a;
    }
}
