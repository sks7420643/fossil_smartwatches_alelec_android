package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x20 {
    @DexIgnore
    public /* final */ nw a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public /* final */ iw d;
    @DexIgnore
    public /* final */ dz e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public hw<Bitmap> i;
    @DexIgnore
    public a j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public a l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public a n;
    @DexIgnore
    public d o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends x40<Bitmap> {
        @DexIgnore
        public /* final */ Handler d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public Bitmap g;

        @DexIgnore
        public a(Handler handler, int i, long j) {
            this.d = handler;
            this.e = i;
            this.f = j;
        }

        @DexIgnore
        @Override // com.fossil.c50
        public /* bridge */ /* synthetic */ void a(Object obj, f50 f50) {
            a((Bitmap) obj, (f50<? super Bitmap>) f50);
        }

        @DexIgnore
        public Bitmap b() {
            return this.g;
        }

        @DexIgnore
        @Override // com.fossil.c50
        public void c(Drawable drawable) {
            this.g = null;
        }

        @DexIgnore
        public void a(Bitmap bitmap, f50<? super Bitmap> f50) {
            this.g = bitmap;
            this.d.sendMessageAtTime(this.d.obtainMessage(1, this), this.f);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Handler.Callback {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                x20.this.a((a) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                x20.this.d.a((c50<?>) ((a) message.obj));
                return false;
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public x20(aw awVar, nw nwVar, int i2, int i3, ex<Bitmap> exVar, Bitmap bitmap) {
        this(awVar.c(), aw.d(awVar.e()), nwVar, null, a(aw.d(awVar.e()), i2, i3), exVar, bitmap);
    }

    @DexIgnore
    public static yw n() {
        return new k50(Double.valueOf(Math.random()));
    }

    @DexIgnore
    public void a(ex<Bitmap> exVar, Bitmap bitmap) {
        u50.a(exVar);
        u50.a(bitmap);
        this.m = bitmap;
        this.i = this.i.a(new r40().a(exVar));
        this.p = v50.a(bitmap);
        this.q = bitmap.getWidth();
        this.r = bitmap.getHeight();
    }

    @DexIgnore
    public void b(b bVar) {
        this.c.remove(bVar);
        if (this.c.isEmpty()) {
            m();
        }
    }

    @DexIgnore
    public Bitmap c() {
        a aVar = this.j;
        return aVar != null ? aVar.b() : this.m;
    }

    @DexIgnore
    public int d() {
        a aVar = this.j;
        if (aVar != null) {
            return aVar.e;
        }
        return -1;
    }

    @DexIgnore
    public Bitmap e() {
        return this.m;
    }

    @DexIgnore
    public int f() {
        return this.a.c();
    }

    @DexIgnore
    public int g() {
        return this.r;
    }

    @DexIgnore
    public int h() {
        return this.a.h() + this.p;
    }

    @DexIgnore
    public int i() {
        return this.q;
    }

    @DexIgnore
    public final void j() {
        if (this.f && !this.g) {
            if (this.h) {
                u50.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.a.f();
                this.h = false;
            }
            a aVar = this.n;
            if (aVar != null) {
                this.n = null;
                a(aVar);
                return;
            }
            this.g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.a.d());
            this.a.b();
            this.l = new a(this.b, this.a.g(), uptimeMillis);
            this.i.a((k40<?>) r40.b(n())).a(this.a).a((c50) this.l);
        }
    }

    @DexIgnore
    public final void k() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.a(bitmap);
            this.m = null;
        }
    }

    @DexIgnore
    public final void l() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            j();
        }
    }

    @DexIgnore
    public final void m() {
        this.f = false;
    }

    @DexIgnore
    public ByteBuffer b() {
        return this.a.e().asReadOnlyBuffer();
    }

    @DexIgnore
    public x20(dz dzVar, iw iwVar, nw nwVar, Handler handler, hw<Bitmap> hwVar, ex<Bitmap> exVar, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = iwVar;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = dzVar;
        this.b = handler;
        this.i = hwVar;
        this.a = nwVar;
        a(exVar, bitmap);
    }

    @DexIgnore
    public void a(b bVar) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(bVar)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(bVar);
            if (isEmpty) {
                l();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    public void a() {
        this.c.clear();
        k();
        m();
        a aVar = this.j;
        if (aVar != null) {
            this.d.a((c50<?>) aVar);
            this.j = null;
        }
        a aVar2 = this.l;
        if (aVar2 != null) {
            this.d.a((c50<?>) aVar2);
            this.l = null;
        }
        a aVar3 = this.n;
        if (aVar3 != null) {
            this.d.a((c50<?>) aVar3);
            this.n = null;
        }
        this.a.clear();
        this.k = true;
    }

    @DexIgnore
    public void a(a aVar) {
        d dVar = this.o;
        if (dVar != null) {
            dVar.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f) {
            this.n = aVar;
        } else {
            if (aVar.b() != null) {
                k();
                a aVar2 = this.j;
                this.j = aVar;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            j();
        }
    }

    @DexIgnore
    public static hw<Bitmap> a(iw iwVar, int i2, int i3) {
        return iwVar.b().a(((r40) ((r40) r40.b(iy.a).b(true)).a(true)).a(i2, i3));
    }
}
