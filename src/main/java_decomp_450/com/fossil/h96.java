package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h96 implements Factory<g96> {
    @DexIgnore
    public static g96 a(e96 e96, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new g96(e96, userRepository, summariesRepository, portfolioApp);
    }
}
