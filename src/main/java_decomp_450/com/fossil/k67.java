package com.fossil;

import android.content.Context;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k67 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;

    @DexIgnore
    public k67(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void run() {
        k47.a(z37.r).h();
        v57.a(this.a, true);
        x47.b(this.a);
        h67.b(this.a);
        Thread.UncaughtExceptionHandler unused = z37.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new r47());
        if (w37.o() == x37.APP_LAUNCH) {
            z37.a(this.a, -1);
        }
        if (w37.q()) {
            z37.m.a("Init MTA StatService success.");
        }
    }
}
