package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og5 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ PortfolioApp b;
    @DexIgnore
    public /* final */ DeviceRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {78}, m = "getComplicationRealData")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ og5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(og5 og5, fb7 fb7) {
            super(fb7);
            this.this$0 = og5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {111}, m = "getComplicationRealData")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ og5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(og5 og5, fb7 fb7) {
            super(fb7);
            this.this$0 = og5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.CustomizeRealDataManager$getComplicationRealData$deviceInDb$1", f = "CustomizeRealDataManager.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ og5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(og5 og5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = og5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return this.this$0.c.getDeviceBySerial(this.this$0.b.c());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public og5(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(deviceRepository, "mDeviceRepository");
        this.b = portfolioApp;
        this.c = deviceRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.portfolio.platform.data.model.MFUser r8, java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r9, java.lang.String r10, com.portfolio.platform.data.model.diana.preset.DianaPreset r11, com.fossil.fb7<? super java.lang.String> r12) {
        /*
            r7 = this;
            boolean r0 = r12 instanceof com.fossil.og5.b
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.og5$b r0 = (com.fossil.og5.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.og5$b r0 = new com.fossil.og5$b
            r0.<init>(r7, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 != r3) goto L_0x0042
            java.lang.Object r8 = r0.L$5
            java.util.ArrayList r8 = (java.util.ArrayList) r8
            java.lang.Object r8 = r0.L$4
            com.portfolio.platform.data.model.diana.preset.DianaPreset r8 = (com.portfolio.platform.data.model.diana.preset.DianaPreset) r8
            java.lang.Object r8 = r0.L$3
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r8 = r0.L$2
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r8 = r0.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.og5 r8 = (com.fossil.og5) r8
            com.fossil.t87.a(r12)
            goto L_0x013b
        L_0x0042:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x004a:
            com.fossil.t87.a(r12)
            int r12 = r10.hashCode()
            r2 = 134170930(0x7ff4932, float:3.8411156E-34)
            r4 = 0
            if (r12 == r2) goto L_0x0059
            goto L_0x00fd
        L_0x0059:
            java.lang.String r12 = "second-timezone"
            boolean r2 = r10.equals(r12)
            if (r2 == 0) goto L_0x00fd
            java.util.ArrayList r8 = r11.getComplications()
            java.util.Iterator r8 = r8.iterator()
        L_0x0069:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x0089
            java.lang.Object r9 = r8.next()
            r10 = r9
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r10 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r10
            java.lang.String r10 = r10.getId()
            boolean r10 = com.fossil.ee7.a(r10, r12)
            java.lang.Boolean r10 = com.fossil.pb7.a(r10)
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x0069
            r4 = r9
        L_0x0089:
            com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting r4 = (com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting) r4
            java.lang.String r8 = "- -"
            if (r4 == 0) goto L_0x013e
            java.lang.String r9 = r4.getSettings()
            boolean r9 = com.fossil.sc5.a(r9)
            if (r9 != 0) goto L_0x013e
            com.google.gson.Gson r9 = r7.a     // Catch:{ Exception -> 0x00ef }
            java.lang.String r10 = r4.getSettings()     // Catch:{ Exception -> 0x00ef }
            java.lang.Class<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> r11 = com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class
            java.lang.Object r9 = r9.a(r10, r11)     // Catch:{ Exception -> 0x00ef }
            com.portfolio.platform.data.model.setting.SecondTimezoneSetting r9 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r9     // Catch:{ Exception -> 0x00ef }
            if (r9 == 0) goto L_0x013e
            com.misfit.frameworks.buttonservice.utils.ConversionUtils r10 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE     // Catch:{ Exception -> 0x00ef }
            java.lang.String r9 = r9.getTimeZoneId()     // Catch:{ Exception -> 0x00ef }
            int r9 = r10.getTimezoneRawOffsetById(r9)     // Catch:{ Exception -> 0x00ef }
            double r9 = (double) r9     // Catch:{ Exception -> 0x00ef }
            r11 = 4633641066610819072(0x404e000000000000, double:60.0)
            double r9 = r9 / r11
            int r11 = com.fossil.ye5.a()     // Catch:{ Exception -> 0x00ef }
            float r11 = (float) r11     // Catch:{ Exception -> 0x00ef }
            r12 = 3600(0xe10, float:5.045E-42)
            float r12 = (float) r12     // Catch:{ Exception -> 0x00ef }
            float r11 = r11 / r12
            double r11 = (double) r11     // Catch:{ Exception -> 0x00ef }
            double r9 = r9 - r11
            r11 = 0
            double r11 = (double) r11     // Catch:{ Exception -> 0x00ef }
            r0 = 104(0x68, float:1.46E-43)
            int r1 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r1 <= 0) goto L_0x00df
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef }
            r11.<init>()     // Catch:{ Exception -> 0x00ef }
            r12 = 43
            r11.append(r12)     // Catch:{ Exception -> 0x00ef }
            r11.append(r9)     // Catch:{ Exception -> 0x00ef }
            r11.append(r0)     // Catch:{ Exception -> 0x00ef }
            java.lang.String r8 = r11.toString()     // Catch:{ Exception -> 0x00ef }
            goto L_0x013e
        L_0x00df:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef }
            r11.<init>()     // Catch:{ Exception -> 0x00ef }
            r11.append(r9)     // Catch:{ Exception -> 0x00ef }
            r11.append(r0)     // Catch:{ Exception -> 0x00ef }
            java.lang.String r8 = r11.toString()     // Catch:{ Exception -> 0x00ef }
            goto L_0x013e
        L_0x00ef:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = "CustomizeRealDataManager"
            java.lang.String r11 = "exception when parse 2nd tz setting"
            r9.d(r10, r11)
            goto L_0x013e
        L_0x00fd:
            java.util.ArrayList r12 = new java.util.ArrayList
            r2 = 10
            int r2 = com.fossil.x97.a(r9, r2)
            r12.<init>(r2)
            java.util.Iterator r2 = r9.iterator()
        L_0x010c:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0121
            java.lang.Object r5 = r2.next()
            com.portfolio.platform.data.model.CustomizeRealData r5 = (com.portfolio.platform.data.model.CustomizeRealData) r5
            r6 = 3
            com.portfolio.platform.data.model.CustomizeRealData r5 = com.portfolio.platform.data.model.CustomizeRealData.copy$default(r5, r4, r4, r6, r4)
            r12.add(r5)
            goto L_0x010c
        L_0x0121:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>(r12)
            r0.L$0 = r7
            r0.L$1 = r8
            r0.L$2 = r9
            r0.L$3 = r10
            r0.L$4 = r11
            r0.L$5 = r2
            r0.label = r3
            java.lang.Object r12 = r7.a(r2, r10, r8, r0)
            if (r12 != r1) goto L_0x013b
            return r1
        L_0x013b:
            r8 = r12
            java.lang.String r8 = (java.lang.String) r8
        L_0x013e:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.og5.a(com.portfolio.platform.data.model.MFUser, java.util.List, java.lang.String, com.portfolio.platform.data.model.diana.preset.DianaPreset, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0278  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r10, java.lang.String r11, com.portfolio.platform.data.model.MFUser r12, com.fossil.fb7<? super java.lang.String> r13) {
        /*
            r9 = this;
            boolean r0 = r13 instanceof com.fossil.og5.c
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.og5$c r0 = (com.fossil.og5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.og5$c r0 = new com.fossil.og5$c
            r0.<init>(r9, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 37
            if (r2 == 0) goto L_0x004c
            if (r2 != r3) goto L_0x0044
            java.lang.Object r10 = r0.L$5
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r10 = (com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) r10
            java.lang.Object r10 = r0.L$4
            com.fossil.se7 r10 = (com.fossil.se7) r10
            java.lang.Object r11 = r0.L$3
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            java.lang.Object r11 = r0.L$2
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$1
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.og5 r11 = (com.fossil.og5) r11
            com.fossil.t87.a(r13)
            goto L_0x0262
        L_0x0044:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x004c:
            com.fossil.t87.a(r13)
            com.fossil.se7 r13 = new com.fossil.se7
            r13.<init>()
            java.lang.String r2 = "- -"
            r13.element = r2
            int r2 = r11.hashCode()
            r5 = 0
            switch(r2) {
                case -829740640: goto L_0x028b;
                case -331239923: goto L_0x020b;
                case -168965370: goto L_0x01fd;
                case -85386984: goto L_0x01ef;
                case -48173007: goto L_0x01a6;
                case 3076014: goto L_0x015c;
                case 96634189: goto L_0x013e;
                case 109761319: goto L_0x0130;
                case 1223440372: goto L_0x0070;
                case 1884273159: goto L_0x0062;
                default: goto L_0x0060;
            }
        L_0x0060:
            goto L_0x0297
        L_0x0062:
            java.lang.String r10 = "heart-rate"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            java.lang.String r10 = "68"
            r13.element = r10
            goto L_0x0297
        L_0x0070:
            java.lang.String r0 = "weather"
            boolean r11 = r11.equals(r0)
            if (r11 == 0) goto L_0x0297
            java.util.Iterator r10 = r10.iterator()
        L_0x007c:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x009e
            java.lang.Object r11 = r10.next()
            r0 = r11
            com.portfolio.platform.data.model.CustomizeRealData r0 = (com.portfolio.platform.data.model.CustomizeRealData) r0
            java.lang.String r0 = r0.getId()
            java.lang.String r1 = "temperature"
            boolean r0 = com.fossil.ee7.a(r1, r0)
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x007c
            r5 = r11
        L_0x009e:
            com.portfolio.platform.data.model.CustomizeRealData r5 = (com.portfolio.platform.data.model.CustomizeRealData) r5
            if (r12 == 0) goto L_0x00af
            com.portfolio.platform.data.model.MFUser$UnitGroup r10 = r12.getUnitGroup()
            if (r10 == 0) goto L_0x00af
            java.lang.String r10 = r10.getTemperature()
            if (r10 == 0) goto L_0x00af
            goto L_0x00b5
        L_0x00af:
            com.fossil.ob5 r10 = com.fossil.ob5.METRIC
            java.lang.String r10 = r10.getValue()
        L_0x00b5:
            com.fossil.ob5 r11 = com.fossil.ob5.METRIC
            java.lang.String r11 = r11.getValue()
            boolean r11 = com.fossil.ee7.a(r10, r11)
            java.lang.String r12 = "exception when parse celsius temperature"
            java.lang.String r0 = "CustomizeRealDataManager"
            r1 = 0
            if (r11 == 0) goto L_0x00f3
            if (r5 == 0) goto L_0x0297
            java.lang.String r10 = r5.getValue()     // Catch:{ Exception -> 0x00d1 }
            float r1 = java.lang.Float.parseFloat(r10)     // Catch:{ Exception -> 0x00d1 }
            goto L_0x00da
        L_0x00d1:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            r10.d(r0, r12)
        L_0x00da:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            int r11 = com.fossil.af7.a(r1)
            r10.append(r11)
            r11 = 8451(0x2103, float:1.1842E-41)
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r13.element = r10
            goto L_0x0297
        L_0x00f3:
            com.fossil.ob5 r11 = com.fossil.ob5.IMPERIAL
            java.lang.String r11 = r11.getValue()
            boolean r10 = com.fossil.ee7.a(r10, r11)
            if (r10 == 0) goto L_0x0297
            if (r5 == 0) goto L_0x0297
            java.lang.String r10 = r5.getValue()     // Catch:{ Exception -> 0x010a }
            float r1 = java.lang.Float.parseFloat(r10)     // Catch:{ Exception -> 0x010a }
            goto L_0x0113
        L_0x010a:
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            r10.d(r0, r12)
        L_0x0113:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            float r11 = com.fossil.xd5.a(r1)
            int r11 = com.fossil.af7.a(r11)
            r10.append(r11)
            r11 = 8457(0x2109, float:1.1851E-41)
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r13.element = r10
            goto L_0x0297
        L_0x0130:
            java.lang.String r10 = "steps"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            java.lang.String r10 = "2385"
            r13.element = r10
            goto L_0x0297
        L_0x013e:
            java.lang.String r10 = "empty"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r10 = r10.c()
            r11 = 2131886550(0x7f1201d6, float:1.9407682E38)
            java.lang.String r10 = com.fossil.ig5.a(r10, r11)
            java.lang.String r11 = "LanguageHelper.getString\u2026me_PresetSwiped_CTA__Set)"
            com.fossil.ee7.a(r10, r11)
            r13.element = r10
            goto L_0x0297
        L_0x015c:
            java.lang.String r12 = "date"
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x0297
            java.util.Iterator r10 = r10.iterator()
        L_0x0168:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x018a
            java.lang.Object r11 = r10.next()
            r12 = r11
            com.portfolio.platform.data.model.CustomizeRealData r12 = (com.portfolio.platform.data.model.CustomizeRealData) r12
            java.lang.String r12 = r12.getId()
            java.lang.String r0 = "day"
            boolean r12 = com.fossil.ee7.a(r0, r12)
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            boolean r12 = r12.booleanValue()
            if (r12 == 0) goto L_0x0168
            r5 = r11
        L_0x018a:
            com.portfolio.platform.data.model.CustomizeRealData r5 = (com.portfolio.platform.data.model.CustomizeRealData) r5
            if (r5 == 0) goto L_0x0195
            java.lang.String r10 = r5.getValue()
            if (r10 == 0) goto L_0x0195
            goto L_0x01a2
        L_0x0195:
            java.util.Calendar r10 = java.util.Calendar.getInstance()
            r11 = 5
            int r10 = r10.get(r11)
            java.lang.String r10 = java.lang.String.valueOf(r10)
        L_0x01a2:
            r13.element = r10
            goto L_0x0297
        L_0x01a6:
            java.lang.String r12 = "chance-of-rain"
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x0297
            java.util.Iterator r10 = r10.iterator()
        L_0x01b2:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x01d4
            java.lang.Object r11 = r10.next()
            r12 = r11
            com.portfolio.platform.data.model.CustomizeRealData r12 = (com.portfolio.platform.data.model.CustomizeRealData) r12
            java.lang.String r12 = r12.getId()
            java.lang.String r0 = "chance_of_rain"
            boolean r12 = com.fossil.ee7.a(r0, r12)
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            boolean r12 = r12.booleanValue()
            if (r12 == 0) goto L_0x01b2
            r5 = r11
        L_0x01d4:
            com.portfolio.platform.data.model.CustomizeRealData r5 = (com.portfolio.platform.data.model.CustomizeRealData) r5
            if (r5 == 0) goto L_0x0297
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r5.getValue()
            r10.append(r11)
            r10.append(r4)
            java.lang.String r10 = r10.toString()
            r13.element = r10
            goto L_0x0297
        L_0x01ef:
            java.lang.String r10 = "active-minutes"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            java.lang.String r10 = "12m"
            r13.element = r10
            goto L_0x0297
        L_0x01fd:
            java.lang.String r10 = "calories"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            java.lang.String r10 = "1200"
            r13.element = r10
            goto L_0x0297
        L_0x020b:
            java.lang.String r2 = "battery"
            boolean r2 = r11.equals(r2)
            if (r2 == 0) goto L_0x0297
            com.fossil.be5$a r2 = com.fossil.be5.o
            com.fossil.be5 r2 = r2.e()
            com.portfolio.platform.PortfolioApp r6 = r9.b
            java.lang.String r6 = r6.c()
            com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r2 = r2.a(r6)
            if (r2 == 0) goto L_0x0241
            int r6 = r2.getBatteryLevel()
            if (r6 <= 0) goto L_0x0241
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            int r11 = r2.getBatteryLevel()
            r10.append(r11)
            r10.append(r4)
            java.lang.String r10 = r10.toString()
            r13.element = r10
            goto L_0x0297
        L_0x0241:
            com.fossil.ti7 r6 = com.fossil.qj7.b()
            com.fossil.og5$d r7 = new com.fossil.og5$d
            r7.<init>(r9, r5)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.L$3 = r12
            r0.L$4 = r13
            r0.L$5 = r2
            r0.label = r3
            java.lang.Object r10 = com.fossil.vh7.a(r6, r7, r0)
            if (r10 != r1) goto L_0x025f
            return r1
        L_0x025f:
            r8 = r13
            r13 = r10
            r10 = r8
        L_0x0262:
            com.portfolio.platform.data.model.Device r13 = (com.portfolio.platform.data.model.Device) r13
            if (r13 == 0) goto L_0x0275
            int r11 = r13.getBatteryLevel()
            java.lang.Integer r11 = com.fossil.pb7.a(r11)
            if (r11 == 0) goto L_0x0275
            int r11 = r11.intValue()
            goto L_0x0276
        L_0x0275:
            r11 = 0
        L_0x0276:
            if (r11 <= 0) goto L_0x0289
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            r12.append(r11)
            r12.append(r4)
            java.lang.String r11 = r12.toString()
            r10.element = r11
        L_0x0289:
            r13 = r10
            goto L_0x0297
        L_0x028b:
            java.lang.String r10 = "commute-time"
            boolean r10 = r11.equals(r10)
            if (r10 == 0) goto L_0x0297
            java.lang.String r10 = "34m"
            r13.element = r10
        L_0x0297:
            T r10 = r13.element
            java.lang.String r10 = (java.lang.String) r10
            return r10
            switch-data {-829740640->0x028b, -331239923->0x020b, -168965370->0x01fd, -85386984->0x01ef, -48173007->0x01a6, 3076014->0x015c, 96634189->0x013e, 109761319->0x0130, 1223440372->0x0070, 1884273159->0x0062, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.og5.a(java.util.List, java.lang.String, com.portfolio.platform.data.model.MFUser, com.fossil.fb7):java.lang.Object");
    }
}
