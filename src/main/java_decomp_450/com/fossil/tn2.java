package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn2 implements Parcelable.Creator<qn2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qn2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    j = j72.s(parcel, a);
                    break;
                case 2:
                    j2 = j72.s(parcel, a);
                    break;
                case 3:
                    z = j72.i(parcel, a);
                    break;
                case 4:
                    str = j72.e(parcel, a);
                    break;
                case 5:
                    str2 = j72.e(parcel, a);
                    break;
                case 6:
                    str3 = j72.e(parcel, a);
                    break;
                case 7:
                    bundle = j72.a(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new qn2(j, j2, z, str, str2, str3, bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qn2[] newArray(int i) {
        return new qn2[i];
    }
}
