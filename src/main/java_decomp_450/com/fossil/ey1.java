package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey1 implements Factory<by1> {
    @DexIgnore
    public static /* final */ ey1 a; // = new ey1();

    @DexIgnore
    public static ey1 a() {
        return a;
    }

    @DexIgnore
    public static by1 b() {
        by1 b = cy1.b();
        c87.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public by1 get() {
        return b();
    }
}
