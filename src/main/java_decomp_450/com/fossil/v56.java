package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.a56;
import com.fossil.fl4;
import com.fossil.xg5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v56 extends p56 {
    @DexIgnore
    public j56 e;
    @DexIgnore
    public MutableLiveData<HybridPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ez5> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<ez5> i;
    @DexIgnore
    public /* final */ q56 j;
    @DexIgnore
    public /* final */ a56 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $it;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ v56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPresetAppSetting $buttonMapping;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, HybridPresetAppSetting hybridPresetAppSetting, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$buttonMapping = hybridPresetAppSetting;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$buttonMapping, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Parcelable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return v56.e(this.this$0.this$0).c(this.$buttonMapping.getAppId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v56$b$b")
        /* renamed from: com.fossil.v56$b$b  reason: collision with other inner class name */
        public static final class C0218b implements fl4.e<a56.d, a56.b> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0218b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(a56.d dVar) {
                ee7.b(dVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch success");
                this.a.this$0.j.m();
                this.a.this$0.j.g(true);
            }

            @DexIgnore
            public void a(a56.b bVar) {
                ee7.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch onError");
                this.a.this$0.j.m();
                int b = bVar.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
                if (b != 1101) {
                    if (b == 8888) {
                        this.a.this$0.j.c();
                        return;
                    } else if (!(b == 1112 || b == 1113)) {
                        this.a.this$0.j.p();
                        return;
                    }
                }
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                q56 g = this.a.this$0.j;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    g.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HybridPreset hybridPreset, fb7 fb7, v56 v56, boolean z, HybridPreset hybridPreset2) {
            super(2, fb7);
            this.$it = hybridPreset;
            this.this$0 = v56;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$currentPreset$inlined = hybridPreset2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$it, fb7, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$currentPreset$inlined);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b4, code lost:
            r9 = com.fossil.v56.a(r15.this$0);
            r10 = new com.fossil.v56.b.a(r15, r5, null);
            r15.L$0 = r8;
            r15.L$1 = r7;
            r15.L$2 = r6;
            r15.L$3 = r5;
            r15.L$4 = r1;
            r15.label = 1;
            r9 = com.fossil.vh7.a(r9, r10, r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cf, code lost:
            if (r9 != r0) goto L_0x00d2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d1, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d2, code lost:
            r0 = r15;
            r15 = r9;
            r9 = r8;
            r8 = r7;
            r7 = r6;
            r6 = r5;
            r5 = r1;
            r1 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x016b, code lost:
            r2 = r5;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00f9 A[Catch:{ Exception -> 0x0111 }] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x010e  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01b1  */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x0228  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x0192 A[EDGE_INSN: B:88:0x0192->B:50:0x0192 ?: BREAK  , SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 0
                r3 = 1
                java.lang.String r4 = "HybridCustomizeEditPresenter"
                if (r1 == 0) goto L_0x003c
                if (r1 != r3) goto L_0x0034
                java.lang.Object r1 = r14.L$4
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r5 = r14.L$3
                com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r5 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r5
                java.lang.Object r6 = r14.L$2
                com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r6 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r6
                java.lang.Object r7 = r14.L$1
                java.util.List r7 = (java.util.List) r7
                java.lang.Object r8 = r14.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r15)     // Catch:{ Exception -> 0x002e }
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r1
                r1 = r0
                r0 = r14
                goto L_0x00db
            L_0x002e:
                r15 = move-exception
                r5 = r1
                r1 = r0
                r0 = r14
                goto L_0x0173
            L_0x0034:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x003c:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r15 = r14.p$
                com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r14.$it
                java.util.ArrayList r1 = r1.getButtons()
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x0050:
                boolean r6 = r1.hasNext()
                if (r6 == 0) goto L_0x0075
                java.lang.Object r6 = r1.next()
                r7 = r6
                com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r7 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r7
                com.fossil.pe5 r8 = com.fossil.pe5.c
                java.lang.String r7 = r7.getAppId()
                boolean r7 = r8.d(r7)
                java.lang.Boolean r7 = com.fossil.pb7.a(r7)
                boolean r7 = r7.booleanValue()
                if (r7 == 0) goto L_0x0050
                r5.add(r6)
                goto L_0x0050
            L_0x0075:
                boolean r1 = r5.isEmpty()
                r1 = r1 ^ r3
                if (r1 == 0) goto L_0x0194
                java.util.Iterator r1 = r5.iterator()
                r8 = r15
                r6 = r2
                r7 = r5
                r15 = r14
            L_0x0084:
                boolean r5 = r1.hasNext()
                if (r5 == 0) goto L_0x0192
                java.lang.Object r5 = r1.next()
                com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r5 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r5
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "check setting of "
                r10.append(r11)
                r10.append(r5)
                java.lang.String r10 = r10.toString()
                r9.d(r4, r10)
                java.lang.String r9 = r5.getSettings()     // Catch:{ Exception -> 0x016d }
                boolean r9 = com.fossil.sc5.a(r9)     // Catch:{ Exception -> 0x016d }
                if (r9 == 0) goto L_0x0116
                com.fossil.v56 r9 = r15.this$0     // Catch:{ Exception -> 0x016d }
                com.fossil.ti7 r9 = r9.b()     // Catch:{ Exception -> 0x016d }
                com.fossil.v56$b$a r10 = new com.fossil.v56$b$a     // Catch:{ Exception -> 0x016d }
                r10.<init>(r15, r5, r2)     // Catch:{ Exception -> 0x016d }
                r15.L$0 = r8     // Catch:{ Exception -> 0x016d }
                r15.L$1 = r7     // Catch:{ Exception -> 0x016d }
                r15.L$2 = r6     // Catch:{ Exception -> 0x016d }
                r15.L$3 = r5     // Catch:{ Exception -> 0x016d }
                r15.L$4 = r1     // Catch:{ Exception -> 0x016d }
                r15.label = r3     // Catch:{ Exception -> 0x016d }
                java.lang.Object r9 = com.fossil.vh7.a(r9, r10, r15)     // Catch:{ Exception -> 0x016d }
                if (r9 != r0) goto L_0x00d2
                return r0
            L_0x00d2:
                r13 = r0
                r0 = r15
                r15 = r9
                r9 = r8
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r1
                r1 = r13
            L_0x00db:
                android.os.Parcelable r15 = (android.os.Parcelable) r15     // Catch:{ Exception -> 0x0111 }
                com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0111 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()     // Catch:{ Exception -> 0x0111 }
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0111 }
                r11.<init>()     // Catch:{ Exception -> 0x0111 }
                java.lang.String r12 = "last setting "
                r11.append(r12)     // Catch:{ Exception -> 0x0111 }
                r11.append(r15)     // Catch:{ Exception -> 0x0111 }
                java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0111 }
                r10.d(r4, r11)     // Catch:{ Exception -> 0x0111 }
                if (r15 == 0) goto L_0x010e
                com.fossil.v56 r10 = r0.this$0     // Catch:{ Exception -> 0x0111 }
                com.google.gson.Gson r10 = r10.h     // Catch:{ Exception -> 0x0111 }
                java.lang.String r15 = r10.a(r15)     // Catch:{ Exception -> 0x0111 }
                r6.setSettings(r15)     // Catch:{ Exception -> 0x0111 }
                r15 = r0
                r0 = r1
                r1 = r5
                r6 = r7
                r7 = r8
                r8 = r9
                goto L_0x0084
            L_0x010e:
                r15 = r0
                goto L_0x0192
            L_0x0111:
                r15 = move-exception
                r6 = r7
                r7 = r8
                r8 = r9
                goto L_0x0173
            L_0x0116:
                java.lang.String r9 = r5.getAppId()
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r10 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME
                java.lang.String r10 = r10.getValue()
                boolean r10 = com.fossil.ee7.a(r9, r10)
                if (r10 == 0) goto L_0x0143
                com.fossil.v56 r9 = r15.this$0
                com.google.gson.Gson r9 = r9.h
                java.lang.String r10 = r5.getSettings()
                java.lang.Class<com.portfolio.platform.data.model.setting.CommuteTimeSetting> r11 = com.portfolio.platform.data.model.setting.CommuteTimeSetting.class
                java.lang.Object r9 = r9.a(r10, r11)
                com.portfolio.platform.data.model.setting.CommuteTimeSetting r9 = (com.portfolio.platform.data.model.setting.CommuteTimeSetting) r9
                java.lang.String r9 = r9.getAddress()
                boolean r9 = android.text.TextUtils.isEmpty(r9)
                if (r9 == 0) goto L_0x0084
                goto L_0x016b
            L_0x0143:
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r10 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
                java.lang.String r10 = r10.getValue()
                boolean r9 = com.fossil.ee7.a(r9, r10)
                if (r9 == 0) goto L_0x0084
                com.fossil.v56 r9 = r15.this$0
                com.google.gson.Gson r9 = r9.h
                java.lang.String r10 = r5.getSettings()
                java.lang.Class<com.portfolio.platform.data.model.setting.SecondTimezoneSetting> r11 = com.portfolio.platform.data.model.setting.SecondTimezoneSetting.class
                java.lang.Object r9 = r9.a(r10, r11)
                com.portfolio.platform.data.model.setting.SecondTimezoneSetting r9 = (com.portfolio.platform.data.model.setting.SecondTimezoneSetting) r9
                java.lang.String r9 = r9.getTimeZoneId()
                boolean r9 = android.text.TextUtils.isEmpty(r9)
                if (r9 == 0) goto L_0x0084
            L_0x016b:
                r2 = r5
                goto L_0x0195
            L_0x016d:
                r5 = move-exception
                r13 = r0
                r0 = r15
                r15 = r5
                r5 = r1
                r1 = r13
            L_0x0173:
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "exception when parse setting from json "
                r10.append(r11)
                r10.append(r15)
                java.lang.String r15 = r10.toString()
                r9.e(r4, r15)
                r15 = r0
                r0 = r1
                r1 = r5
                goto L_0x0084
            L_0x0192:
                r2 = r6
                goto L_0x0195
            L_0x0194:
                r15 = r14
            L_0x0195:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = "setPresetToWatch missingSettingMicroApp "
                r1.append(r3)
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.d(r4, r1)
                if (r2 == 0) goto L_0x0228
                java.lang.String r0 = r2.getAppId()
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME
                java.lang.String r1 = r1.getValue()
                boolean r1 = com.fossil.ee7.a(r0, r1)
                if (r1 == 0) goto L_0x01d4
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886362(0x7f12011a, float:1.94073E38)
                java.lang.String r0 = com.fossil.ig5.a(r0, r1)
                java.lang.String r1 = "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)"
                com.fossil.ee7.a(r0, r1)
                goto L_0x0214
            L_0x01d4:
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
                java.lang.String r1 = r1.getValue()
                boolean r1 = com.fossil.ee7.a(r0, r1)
                if (r1 == 0) goto L_0x01f3
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131886524(0x7f1201bc, float:1.940763E38)
                java.lang.String r0 = com.fossil.ig5.a(r0, r1)
                java.lang.String r1 = "LanguageHelper.getString\u2026splayTimezoneOnYourWatch)"
                com.fossil.ee7.a(r0, r1)
                goto L_0x0214
            L_0x01f3:
                com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r1 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE
                java.lang.String r1 = r1.getValue()
                boolean r0 = com.fossil.ee7.a(r0, r1)
                if (r0 == 0) goto L_0x0212
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r1 = 2131887528(0x7f1205a8, float:1.9409666E38)
                java.lang.String r0 = com.fossil.ig5.a(r0, r1)
                java.lang.String r1 = "LanguageHelper.getString\u2026ing_phone_not_configured)"
                com.fossil.ee7.a(r0, r1)
                goto L_0x0214
            L_0x0212:
                java.lang.String r0 = ""
            L_0x0214:
                com.fossil.v56 r15 = r15.this$0
                com.fossil.q56 r15 = r15.j
                java.lang.String r1 = r2.getAppId()
                java.lang.String r2 = r2.getPosition()
                r15.b(r0, r1, r2)
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x0228:
                com.fossil.xg5 r0 = com.fossil.xg5.b
                com.fossil.v56 r1 = r15.this$0
                com.fossil.q56 r1 = r1.j
                if (r1 == 0) goto L_0x0295
                com.fossil.r56 r1 = (com.fossil.r56) r1
                android.content.Context r1 = r1.getContext()
                com.fossil.xg5$a r2 = com.fossil.xg5.a.SET_BLE_COMMAND
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 60
                r8 = 0
                boolean r0 = com.fossil.xg5.a(r0, r1, r2, r3, r4, r5, r6, r7, r8)
                if (r0 != 0) goto L_0x024a
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x024a:
                boolean r0 = r15.$shouldShowSkippedPermissions$inlined
                if (r0 == 0) goto L_0x0274
                com.fossil.xg5 r1 = com.fossil.xg5.b
                com.fossil.v56 r0 = r15.this$0
                com.fossil.q56 r0 = r0.j
                com.fossil.r56 r0 = (com.fossil.r56) r0
                android.content.Context r2 = r0.getContext()
                com.fossil.ve5 r0 = com.fossil.ve5.a
                com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r15.$currentPreset$inlined
                java.util.List r3 = r0.a(r3)
                r4 = 0
                r5 = 0
                r6 = 1
                r7 = 0
                r8 = 44
                r9 = 0
                boolean r0 = com.fossil.xg5.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
                if (r0 != 0) goto L_0x0274
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x0274:
                com.fossil.v56 r0 = r15.this$0
                com.fossil.q56 r0 = r0.j
                r0.k()
                com.fossil.v56 r0 = r15.this$0
                com.fossil.a56 r0 = r0.k
                com.fossil.a56$c r1 = new com.fossil.a56$c
                com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r15.$currentPreset$inlined
                r1.<init>(r2)
                com.fossil.v56$b$b r2 = new com.fossil.v56$b$b
                r2.<init>(r15)
                r0.a(r1, r2)
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x0295:
                com.fossil.x87 r15 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment"
                r15.<init>(r0)
                throw r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.v56.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ v56 a;

        @DexIgnore
        public c(v56 v56) {
            this.a = v56;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe current preset value=" + hybridPreset);
            MutableLiveData b = this.a.f;
            if (hybridPreset != null) {
                b.a(hybridPreset.clone());
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ v56 a;

        @DexIgnore
        public d(v56 v56) {
            this.a = v56;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe selected microApp value=" + str);
            q56 g = this.a.j;
            if (str != null) {
                g.C(str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<ez5> {
        @DexIgnore
        public /* final */ /* synthetic */ v56 a;

        @DexIgnore
        public e(v56 v56) {
            this.a = v56;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ez5 ez5) {
            if (ez5 != null) {
                this.a.j.a(ez5);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ v56 a;

        @DexIgnore
        public f(v56 v56) {
            this.a = v56;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "update preset status isChanged=" + bool);
            q56 g = this.a.j;
            if (bool != null) {
                g.f(bool.booleanValue());
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ v56 a;

        @DexIgnore
        public g(v56 v56) {
            this.a = v56;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<ez5> apply(HybridPreset hybridPreset) {
            String str;
            String str2;
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(hybridPreset.getButtons());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microApps=" + arrayList);
            ArrayList arrayList2 = new ArrayList();
            ve5 ve5 = ve5.a;
            ee7.a((Object) hybridPreset, "preset");
            List<xg5.a> a2 = ve5.a(hybridPreset);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
                String component1 = hybridPresetAppSetting.component1();
                MicroApp b = v56.e(this.a).b(hybridPresetAppSetting.component2());
                if (b != null) {
                    String id = b.getId();
                    String icon = b.getIcon();
                    if (icon != null) {
                        str2 = icon;
                    } else {
                        str2 = "";
                    }
                    arrayList2.add(new fz5(id, str2, ig5.a(PortfolioApp.g0.c(), b.getNameKey(), b.getName()), component1, null, 16, null));
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microAppsDetail=" + arrayList2);
            MutableLiveData c = this.a.g;
            String id2 = hybridPreset.getId();
            String name = hybridPreset.getName();
            if (name != null) {
                str = name;
            } else {
                str = "";
            }
            c.a(new ez5(id2, str, arrayList2, a2, hybridPreset.isActive()));
            return this.a.g;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public v56(q56 q56, a56 a56) {
        ee7.b(q56, "mView");
        ee7.b(a56, "mSetHybridPresetToWatchUseCase");
        this.j = q56;
        this.k = a56;
        LiveData<ez5> b2 = ge.b(this.f, new g(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.i = b2;
    }

    @DexIgnore
    public static final /* synthetic */ j56 e(v56 v56) {
        j56 j56 = v56.e;
        if (j56 != null) {
            return j56;
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void h() {
        j56 j56 = this.e;
        if (j56 != null) {
            boolean g2 = j56.g();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "onUserExit isPresetChanged " + g2);
            if (g2) {
                this.j.o();
            } else {
                this.j.g(false);
            }
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.j.a(this);
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void a(j56 j56) {
        ee7.b(j56, "viewModel");
        this.e = j56;
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void b(String str, String str2) {
        T t;
        ee7.b(str, "fromPosition");
        ee7.b(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!ee7.a((Object) str, (Object) str2)) {
            j56 j56 = this.e;
            T t2 = null;
            if (j56 != null) {
                HybridPreset a2 = j56.a().a();
                if (a2 != null) {
                    HybridPreset clone = a2.clone();
                    Iterator<T> it = clone.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (ee7.a((Object) t.getPosition(), (Object) str)) {
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        T next = it2.next();
                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                            t2 = next;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.setPosition(str2);
                    }
                    if (t4 != null) {
                        t4.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - update preset");
                    a(clone);
                    return;
                }
                return;
            }
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.g0.c().c())) {
            this.j.R();
        }
        this.k.e();
        nj5.d.a(CommunicateMode.SET_LINK_MAPPING);
        j56 j56 = this.e;
        if (j56 != null) {
            MutableLiveData<HybridPreset> a2 = j56.a();
            q56 q56 = this.j;
            if (q56 != null) {
                a2.a((r56) q56, new c(this));
                j56 j562 = this.e;
                if (j562 != null) {
                    j562.e().a((LifecycleOwner) this.j, new d(this));
                    this.i.a((LifecycleOwner) this.j, new e(this));
                    j56 j563 = this.e;
                    if (j563 != null) {
                        j563.b().a((LifecycleOwner) this.j, new f(this));
                    } else {
                        ee7.d("mHybridCustomizeViewModel");
                        throw null;
                    }
                } else {
                    ee7.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            }
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.k.g();
        j56 j56 = this.e;
        if (j56 != null) {
            MutableLiveData<HybridPreset> a2 = j56.a();
            q56 q56 = this.j;
            if (q56 != null) {
                a2.a((r56) q56);
                j56 j562 = this.e;
                if (j562 != null) {
                    j562.e().a((LifecycleOwner) this.j);
                    this.g.a((LifecycleOwner) this.j);
                    return;
                }
                ee7.d("mHybridCustomizeViewModel");
                throw null;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void a(String str) {
        ee7.b(str, "microAppPos");
        j56 j56 = this.e;
        if (j56 != null) {
            j56.e(str);
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void a(String str, String str2) {
        ee7.b(str, "microAppId");
        ee7.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "dropMicroApp - microAppid=" + str + ", toPosition=" + str2);
        j56 j56 = this.e;
        T t = null;
        if (j56 == null) {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        } else if (!j56.d(str)) {
            j56 j562 = this.e;
            if (j562 != null) {
                HybridPreset a2 = j562.a().a();
                if (a2 != null) {
                    Iterator<T> it = a2.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        if (ee7.a((Object) next.getPosition(), (Object) str2)) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAppId(str);
                        t2.setSettings("");
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - update preset");
                    ee7.a((Object) a2, "currentPreset");
                    a(a2);
                    return;
                }
                return;
            }
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.p56
    public void a(boolean z) {
        j56 j56 = this.e;
        if (j56 != null) {
            HybridPreset a2 = j56.a().a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch currentPreset=" + a2);
            if (a2 != null) {
                ik7 unused = xh7.b(e(), null, null, new b(a2, null, this, z, a2), 3, null);
                return;
            }
            return;
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "updateCurrentPreset=" + hybridPreset);
        j56 j56 = this.e;
        if (j56 != null) {
            j56.a(hybridPreset);
        } else {
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        j56 j56 = this.e;
        if (j56 != null) {
            HybridPreset a2 = j56.a().a();
            if (!(a2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", this.h.a(a2));
            }
            j56 j562 = this.e;
            if (j562 != null) {
                HybridPreset c2 = j562.c();
                if (!(c2 == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", this.h.a(c2));
                }
                j56 j563 = this.e;
                if (j563 != null) {
                    String a3 = j563.e().a();
                    if (!(a3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", a3);
                    }
                    return bundle;
                }
                ee7.d("mHybridCustomizeViewModel");
                throw null;
            }
            ee7.d("mHybridCustomizeViewModel");
            throw null;
        }
        ee7.d("mHybridCustomizeViewModel");
        throw null;
    }
}
