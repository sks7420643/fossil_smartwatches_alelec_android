package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia3 implements Parcelable.Creator<u93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ u93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 2) {
                j72.v(parcel, a);
            } else {
                i = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new u93(i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ u93[] newArray(int i) {
        return new u93[i];
    }
}
