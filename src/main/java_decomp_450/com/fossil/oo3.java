package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oo3<TResult> {
    @DexIgnore
    public /* final */ lp3<TResult> a; // = new lp3<>();

    @DexIgnore
    public oo3() {
    }

    @DexIgnore
    public void a(TResult tresult) {
        this.a.a((Object) tresult);
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.b((Object) tresult);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.a.a(exc);
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.b(exc);
    }

    @DexIgnore
    public oo3(do3 do3) {
        do3.a(new jp3(this));
    }

    @DexIgnore
    public no3<TResult> a() {
        return this.a;
    }
}
