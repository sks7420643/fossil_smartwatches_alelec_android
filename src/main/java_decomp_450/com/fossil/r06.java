package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.so5;
import com.fossil.zl4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r06 extends go5 implements w06, zl4.b {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<iy4> f;
    @DexIgnore
    public v06 g;
    @DexIgnore
    public so5 h;
    @DexIgnore
    public zl4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final r06 a() {
            return new r06();
        }

        @DexIgnore
        public final String b() {
            return r06.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements so5.b {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;
        @DexIgnore
        public /* final */ /* synthetic */ iy4 b;

        @DexIgnore
        public b(r06 r06, iy4 iy4) {
            this.a = r06;
            this.b = iy4;
        }

        @DexIgnore
        @Override // com.fossil.so5.b
        public void a(String str) {
            ee7.b(str, "address");
            this.b.q.setText((CharSequence) str, false);
            this.a.Y(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public c(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public d(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public e(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public f(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ iy4 b;

        @DexIgnore
        public g(View view, iy4 iy4) {
            this.a = view;
            this.b = iy4;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.D.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                iy4 iy4 = this.b;
                ee7.a((Object) iy4, "binding");
                iy4.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = r06.r.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.b.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;
        @DexIgnore
        public /* final */ /* synthetic */ iy4 b;

        @DexIgnore
        public h(r06 r06, iy4 iy4) {
            this.a = r06;
            this.b = iy4;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            zl4 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText(null);
                ee7.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = r06.r.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                String spannableString = fullText.toString();
                ee7.a((Object) spannableString, "primaryText.toString()");
                if (!TextUtils.isEmpty(spannableString)) {
                    this.a.Y(spannableString);
                    this.b.q.setText((CharSequence) spannableString, false);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;
        @DexIgnore
        public /* final */ /* synthetic */ iy4 b;

        @DexIgnore
        public i(r06 r06, iy4 iy4) {
            this.a = r06;
            this.b = iy4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            RTLImageView rTLImageView = this.b.t;
            if (!ee7.a((Object) this.a.f1(), (Object) String.valueOf(editable))) {
                this.a.Y(null);
            }
            RTLImageView rTLImageView2 = this.b.t;
            ee7.a((Object) rTLImageView2, "binding.closeIv");
            rTLImageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public j(r06 r06, iy4 iy4) {
            this.a = r06;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            ee7.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.e1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public k(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).b("travel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public l(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.y;
            r06 r06 = this.a;
            CommuteTimeSetting i = r06.b(r06).i();
            if (i == null || (str = i.getAddress()) == null) {
                str = "";
            }
            aVar.a(r06, 0.0d, 0.0d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public m(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;
        @DexIgnore
        public /* final */ /* synthetic */ iy4 b;

        @DexIgnore
        public n(r06 r06, iy4 iy4) {
            this.a = r06;
            this.b = iy4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText((CharSequence) "", false);
            r06.b(this.a).h();
            this.a.g1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r06 a;

        @DexIgnore
        public o(r06 r06) {
            this.a = r06;
        }

        @DexIgnore
        public final void onClick(View view) {
            r06.b(this.a).b("eta");
        }
    }

    /*
    static {
        String simpleName = r06.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ v06 b(r06 r06) {
        v06 v06 = r06.g;
        if (v06 != null) {
            return v06;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void G(String str) {
        a95 a95;
        ee7.b(str, "userWorkDefaultAddress");
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null && (a95 = a2.y) != null) {
                FlexibleTextView flexibleTextView = a95.r;
                ee7.a((Object) flexibleTextView, "workButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) yx6.a(12, requireContext());
                a95.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = a95.t;
                    ee7.a((Object) imageButton, "workButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(v6.a(PortfolioApp.g0.c(), 2131099972)));
                    a95.t.setImageResource(2131231045);
                    return;
                }
                ImageButton imageButton2 = a95.t;
                ee7.a((Object) imageButton2, "workButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(v6.a(PortfolioApp.g0.c(), 2131099809)));
                a95.t.setImageResource(2131231045);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void S(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.J) != null) {
                ee7.a((Object) flexibleSwitchCompat, "it");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        this.j = str;
    }

    @DexIgnore
    public void Z(String str) {
        ee7.b(str, "address");
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.j = str;
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void c(String str, String str2) {
        ee7.b(str, "addressType");
        ee7.b(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                zl4 zl4 = this.i;
                if (zl4 != null) {
                    zl4.a((zl4.b) null);
                }
                CommuteTimeSettingsDefaultAddressActivity.z.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            zl4 zl42 = this.i;
            if (zl42 != null) {
                zl42.a((zl4.b) null);
            }
            CommuteTimeSettingsDefaultAddressActivity.z.a(this, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void e(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        ee7.b(list, "recentSearchedList");
        g1();
        if (!list.isEmpty()) {
            so5 so5 = this.h;
            if (so5 != null) {
                so5.a(ea7.d((Collection) list));
            }
            qw6<iy4> qw6 = this.f;
            if (qw6 != null) {
                iy4 a2 = qw6.a();
                if (a2 != null && (linearLayout2 = a2.G) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<iy4> qw62 = this.f;
        if (qw62 != null) {
            iy4 a3 = qw62.a();
            if (a3 != null && (linearLayout = a3.G) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2 != null) goto L_0x0028;
     */
    @DexIgnore
    @Override // com.fossil.go5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e1() {
        /*
            r10 = this;
            com.fossil.qw6<com.fossil.iy4> r0 = r10.f
            r1 = 0
            if (r0 == 0) goto L_0x007c
            java.lang.Object r0 = r0.a()
            com.fossil.iy4 r0 = (com.fossil.iy4) r0
            if (r0 == 0) goto L_0x007a
            java.lang.String r2 = r10.j
            java.lang.String r3 = "null cannot be cast to non-null type kotlin.CharSequence"
            if (r2 == 0) goto L_0x0026
            if (r2 == 0) goto L_0x0020
            java.lang.CharSequence r2 = com.fossil.nh7.d(r2)
            java.lang.String r2 = r2.toString()
            if (r2 == 0) goto L_0x0026
            goto L_0x0028
        L_0x0020:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r3)
            throw r0
        L_0x0026:
            java.lang.String r2 = ""
        L_0x0028:
            r5 = r2
            com.portfolio.platform.view.RTLImageView r2 = r0.z
            java.lang.String r4 = "it.ivArrivalTime"
            com.fossil.ee7.a(r2, r4)
            float r2 = r2.getAlpha()
            r4 = 1065353216(0x3f800000, float:1.0)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x003d
            java.lang.String r2 = "eta"
            goto L_0x003f
        L_0x003d:
            java.lang.String r2 = "travel"
        L_0x003f:
            r9 = r2
            com.fossil.v06 r4 = r10.g
            if (r4 == 0) goto L_0x0074
            com.portfolio.platform.view.FlexibleAutoCompleteTextView r1 = r0.q
            java.lang.String r2 = "it.autocompletePlaces"
            com.fossil.ee7.a(r1, r2)
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            if (r1 == 0) goto L_0x006e
            java.lang.CharSequence r1 = com.fossil.nh7.d(r1)
            java.lang.String r6 = r1.toString()
            com.fossil.bb5 r7 = com.fossil.bb5.CAR
            com.portfolio.platform.view.FlexibleSwitchCompat r0 = r0.J
            java.lang.String r1 = "it.scAvoidTolls"
            com.fossil.ee7.a(r0, r1)
            boolean r8 = r0.isChecked()
            r4.a(r5, r6, r7, r8, r9)
            goto L_0x007a
        L_0x006e:
            com.fossil.x87 r0 = new com.fossil.x87
            r0.<init>(r3)
            throw r0
        L_0x0074:
            java.lang.String r0 = "mPresenter"
            com.fossil.ee7.d(r0)
            throw r1
        L_0x007a:
            r0 = 1
            return r0
        L_0x007c:
            java.lang.String r0 = "mBinding"
            com.fossil.ee7.d(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.r06.e1():boolean");
    }

    @DexIgnore
    public final String f1() {
        return this.j;
    }

    @DexIgnore
    public void g1() {
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                LinearLayout linearLayout = a2.G;
                ee7.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                a95 a95 = a2.x;
                ee7.a((Object) a95, "it.icHome");
                View d2 = a95.d();
                ee7.a((Object) d2, "it.icHome.root");
                d2.setVisibility(0);
                a95 a952 = a2.y;
                ee7.a((Object) a952, "it.icWork");
                View d3 = a952.d();
                ee7.a((Object) d3, "it.icWork.root");
                d3.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void h(boolean z) {
        if (z) {
            Intent intent = new Intent();
            v06 v06 = this.g;
            if (v06 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", v06.i());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void h1() {
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.g0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886354, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.u;
                ee7.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.G;
                ee7.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                a95 a95 = a2.x;
                ee7.a((Object) a95, "it.icHome");
                View d2 = a95.d();
                ee7.a((Object) d2, "it.icHome.root");
                d2.setVisibility(8);
                a95 a952 = a2.y;
                ee7.a((Object) a952, "it.icWork");
                View d3 = a952.d();
                ee7.a((Object) d3, "it.icWork.root");
                d3.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zl4.b
    public void m(boolean z) {
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.j = null;
                        h1();
                        return;
                    }
                }
                g1();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null) {
            String str = null;
            if (i2 != 100) {
                String str2 = "";
                if (i2 == 111) {
                    String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
                    r(stringExtra != null ? stringExtra : str2);
                    v06 v06 = this.g;
                    if (v06 != null) {
                        if (stringExtra != null) {
                            str2 = stringExtra;
                        }
                        v06.a(str2);
                        return;
                    }
                    ee7.d("mPresenter");
                    throw null;
                } else if (i2 == 112) {
                    String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
                    G(stringExtra2 != null ? stringExtra2 : str2);
                    v06 v062 = this.g;
                    if (v062 != null) {
                        if (stringExtra2 != null) {
                            str2 = stringExtra2;
                        }
                        v062.a(str2);
                        return;
                    }
                    ee7.d("mPresenter");
                    throw null;
                }
            } else {
                Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
                if (bundleExtra != null) {
                    str = bundleExtra.getString("address");
                }
                if (str != null) {
                    Z(str);
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        iy4 iy4 = (iy4) qb.a(layoutInflater, 2131558515, viewGroup, false, a1());
        String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            iy4.F.setBackgroundColor(Color.parseColor(b2));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = iy4.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(v6.c(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new h(this, iy4));
        flexibleAutoCompleteTextView.addTextChangedListener(new i(this, iy4));
        flexibleAutoCompleteTextView.setOnKeyListener(new j(this, iy4));
        iy4.w.setOnClickListener(new m(this));
        a95 a95 = iy4.x;
        a95.u.setImageResource(2131231069);
        FlexibleTextView flexibleTextView = a95.s;
        ee7.a((Object) flexibleTextView, "it.ftvTitle");
        flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131886348));
        a95.q.setOnClickListener(new c(this));
        a95.t.setOnClickListener(new d(this));
        a95 a952 = iy4.y;
        a952.u.setImageResource(2131231070);
        FlexibleTextView flexibleTextView2 = a952.s;
        ee7.a((Object) flexibleTextView2, "it.ftvTitle");
        flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131886349));
        a952.q.setOnClickListener(new e(this));
        a952.t.setOnClickListener(new f(this));
        iy4.t.setOnClickListener(new n(this, iy4));
        so5 so5 = new so5();
        so5.a(new b(this, iy4));
        this.h = so5;
        RecyclerView recyclerView = iy4.I;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        ee7.a((Object) iy4, "binding");
        View d2 = iy4.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new g(d2, iy4));
        iy4.z.setOnClickListener(new o(this));
        iy4.B.setOnClickListener(new k(this));
        iy4.A.setOnClickListener(new l(this));
        this.f = new qw6<>(this, iy4);
        return iy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        v06 v06 = this.g;
        if (v06 != null) {
            v06.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        v06 v06 = this.g;
        if (v06 != null) {
            v06.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void r(String str) {
        a95 a95;
        ee7.b(str, "userHomeDefaultAddress");
        qw6<iy4> qw6 = this.f;
        if (qw6 != null) {
            iy4 a2 = qw6.a();
            if (a2 != null && (a95 = a2.x) != null) {
                FlexibleTextView flexibleTextView = a95.r;
                ee7.a((Object) flexibleTextView, "homeButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) yx6.a(12, requireContext());
                a95.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = a95.t;
                    ee7.a((Object) imageButton, "homeButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(v6.a(PortfolioApp.g0.c(), 2131099971)));
                    a95.t.setImageResource(2131231045);
                    return;
                }
                ImageButton imageButton2 = a95.t;
                ee7.a((Object) imageButton2, "homeButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(v6.a(PortfolioApp.g0.c(), 2131099809)));
                a95.t.setImageResource(2131231045);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void t(String str) {
        ee7.b(str, "userCurrentAddress");
        if (isActive()) {
            qw6<iy4> qw6 = this.f;
            if (qw6 != null) {
                iy4 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    ee7.a((Object) text, "it.autocompletePlaces.text");
                    boolean z = true;
                    if (nh7.d(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText((CharSequence) str, false);
                            this.j = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(v06 v06) {
        ee7.b(v06, "presenter");
        this.g = v06;
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            zl4 zl4 = new zl4(requireContext, placesClient);
            this.i = zl4;
            if (zl4 != null) {
                zl4.a(this);
            }
            qw6<iy4> qw6 = this.f;
            if (qw6 != null) {
                iy4 a2 = qw6.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.i);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    ee7.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = nh7.d(text);
                    if (d2.length() > 0) {
                        a2.q.setText(d2, false);
                        a2.q.setSelection(d2.length());
                        return;
                    }
                    g1();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.w06
    public void a(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(q, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    qw6<iy4> qw6 = this.f;
                    if (qw6 != null) {
                        iy4 a2 = qw6.a();
                        if (a2 != null) {
                            RTLImageView rTLImageView = a2.z;
                            ee7.a((Object) rTLImageView, "it.ivArrivalTime");
                            rTLImageView.setAlpha(1.0f);
                            RTLImageView rTLImageView2 = a2.B;
                            ee7.a((Object) rTLImageView2, "it.ivTravelTime");
                            rTLImageView2.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                qw6<iy4> qw62 = this.f;
                if (qw62 != null) {
                    iy4 a3 = qw62.a();
                    if (a3 != null) {
                        RTLImageView rTLImageView3 = a3.B;
                        ee7.a((Object) rTLImageView3, "it.ivTravelTime");
                        rTLImageView3.setAlpha(1.0f);
                        RTLImageView rTLImageView4 = a3.z;
                        ee7.a((Object) rTLImageView4, "it.ivArrivalTime");
                        rTLImageView4.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
        }
    }
}
