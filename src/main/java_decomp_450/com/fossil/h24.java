package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class h24 implements ob4 {
    @DexIgnore
    public /* final */ k24 a;
    @DexIgnore
    public /* final */ c24 b;

    @DexIgnore
    public h24(k24 k24, c24 c24) {
        this.a = k24;
        this.b = c24;
    }

    @DexIgnore
    public static ob4 a(k24 k24, c24 c24) {
        return new h24(k24, c24);
    }

    @DexIgnore
    @Override // com.fossil.ob4
    public Object get() {
        return this.b.b().a(new u24(this.b, this.a));
    }
}
