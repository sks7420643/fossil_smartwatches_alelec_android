package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vj6 extends go5 implements uj6 {
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public qw6<m45> f;
    @DexIgnore
    public tj6 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final vj6 a() {
            return new vj6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vj6 a;

        @DexIgnore
        public b(vj6 vj6) {
            this.a = vj6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vj6 a;
        @DexIgnore
        public /* final */ /* synthetic */ m45 b;

        @DexIgnore
        public c(vj6 vj6, m45 m45) {
            this.a = vj6;
            this.b = m45;
        }

        @DexIgnore
        public final void onClick(View view) {
            vj6 vj6 = this.a;
            FlexibleSwitchCompat flexibleSwitchCompat = this.b.q;
            ee7.a((Object) flexibleSwitchCompat, "binding.anonymousSwitch");
            vj6.c("Usage_Data", flexibleSwitchCompat.isChecked());
            qd5 a2 = this.a.b1();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.b.q;
            ee7.a((Object) flexibleSwitchCompat2, "binding.anonymousSwitch");
            a2.c(flexibleSwitchCompat2.isChecked());
            tj6 b2 = vj6.b(this.a);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.b.q;
            ee7.a((Object) flexibleSwitchCompat3, "binding.anonymousSwitch");
            b2.a(flexibleSwitchCompat3.isChecked());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vj6 a;
        @DexIgnore
        public /* final */ /* synthetic */ m45 b;

        @DexIgnore
        public d(vj6 vj6, m45 m45) {
            this.a = vj6;
            this.b = m45;
        }

        @DexIgnore
        public final void onClick(View view) {
            vj6 vj6 = this.a;
            FlexibleSwitchCompat flexibleSwitchCompat = this.b.y;
            ee7.a((Object) flexibleSwitchCompat, "binding.scSubcribeEmail");
            vj6.c("Emails", flexibleSwitchCompat.isChecked());
            qd5 a2 = this.a.b1();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.b.y;
            ee7.a((Object) flexibleSwitchCompat2, "binding.scSubcribeEmail");
            a2.b(flexibleSwitchCompat2.isChecked());
            tj6 b2 = vj6.b(this.a);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.b.y;
            ee7.a((Object) flexibleSwitchCompat3, "binding.scSubcribeEmail");
            b2.b(flexibleSwitchCompat3.isChecked());
        }
    }

    /*
    static {
        ee7.a((Object) vj6.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ tj6 b(vj6 vj6) {
        tj6 tj6 = vj6.g;
        if (tj6 != null) {
            return tj6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.uj6
    public void H() {
        String string = getString(2131886857);
        ee7.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        W(string);
    }

    @DexIgnore
    @Override // com.fossil.uj6
    public void K(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<m45> qw6 = this.f;
        if (qw6 != null) {
            m45 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.q) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.uj6
    public void N(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        qw6<m45> qw6 = this.f;
        if (qw6 != null) {
            m45 a2 = qw6.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.uj6
    public void N0() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void c(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        a("profile_optin", hashMap);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        m45 m45 = (m45) qb.a(LayoutInflater.from(getContext()), 2131558598, (ViewGroup) null, false);
        m45.w.setOnClickListener(new b(this));
        FlexibleTextView flexibleTextView = m45.u;
        ee7.a((Object) flexibleTextView, "binding.ftvDescriptionSubcribeEmail");
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887161);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h()}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        m45.q.setOnClickListener(new c(this, m45));
        m45.y.setOnClickListener(new d(this, m45));
        this.f = new qw6<>(this, m45);
        ee7.a((Object) m45, "binding");
        return m45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        tj6 tj6 = this.g;
        if (tj6 != null) {
            tj6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        tj6 tj6 = this.g;
        if (tj6 != null) {
            tj6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.uj6
    public void b(int i2, String str) {
        ee7.b(str, "message");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(tj6 tj6) {
        ee7.b(tj6, "presenter");
        jw3.a(tj6);
        ee7.a((Object) tj6, "checkNotNull(presenter)");
        this.g = tj6;
    }
}
