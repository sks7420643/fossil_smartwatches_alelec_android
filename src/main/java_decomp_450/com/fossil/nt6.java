package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt6 implements MembersInjector<lt6> {
    @DexIgnore
    public static void a(lt6 lt6, jw6 jw6) {
        lt6.e = jw6;
    }

    @DexIgnore
    public static void a(lt6 lt6, mw6 mw6) {
        lt6.f = mw6;
    }

    @DexIgnore
    public static void a(lt6 lt6) {
        lt6.m();
    }
}
