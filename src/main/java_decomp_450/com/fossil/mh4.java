package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mh4 extends th4 {
    @DexIgnore
    public mh4() {
        super(false, 1558, 620, 22, 22, 36, -1, 62);
    }

    @DexIgnore
    @Override // com.fossil.th4
    public int a(int i) {
        return i <= 8 ? 156 : 155;
    }

    @DexIgnore
    @Override // com.fossil.th4
    public int d() {
        return 10;
    }
}
