package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s53 implements Parcelable.Creator<h53> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ h53 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Status status = null;
        i53 i53 = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                status = (Status) j72.a(parcel, a, Status.CREATOR);
            } else if (a2 != 2) {
                j72.v(parcel, a);
            } else {
                i53 = (i53) j72.a(parcel, a, i53.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new h53(status, i53);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ h53[] newArray(int i) {
        return new h53[i];
    }
}
