package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qu3 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ ColorStateList h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ float j;
    @DexIgnore
    public /* final */ float k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public Typeface n;

    @DexIgnore
    public qu3(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, tr3.TextAppearance);
        this.a = obtainStyledAttributes.getDimension(tr3.TextAppearance_android_textSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b = pu3.a(context, obtainStyledAttributes, tr3.TextAppearance_android_textColor);
        this.c = pu3.a(context, obtainStyledAttributes, tr3.TextAppearance_android_textColorHint);
        this.d = pu3.a(context, obtainStyledAttributes, tr3.TextAppearance_android_textColorLink);
        this.e = obtainStyledAttributes.getInt(tr3.TextAppearance_android_textStyle, 0);
        this.f = obtainStyledAttributes.getInt(tr3.TextAppearance_android_typeface, 1);
        int a2 = pu3.a(obtainStyledAttributes, tr3.TextAppearance_fontFamily, tr3.TextAppearance_android_fontFamily);
        this.l = obtainStyledAttributes.getResourceId(a2, 0);
        this.g = obtainStyledAttributes.getString(a2);
        obtainStyledAttributes.getBoolean(tr3.TextAppearance_textAllCaps, false);
        this.h = pu3.a(context, obtainStyledAttributes, tr3.TextAppearance_android_shadowColor);
        this.i = obtainStyledAttributes.getFloat(tr3.TextAppearance_android_shadowDx, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.j = obtainStyledAttributes.getFloat(tr3.TextAppearance_android_shadowDy, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.k = obtainStyledAttributes.getFloat(tr3.TextAppearance_android_shadowRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public Typeface b() {
        a();
        return this.n;
    }

    @DexIgnore
    public void c(Context context, TextPaint textPaint, su3 su3) {
        if (ru3.a()) {
            a(textPaint, a(context));
        } else {
            a(context, textPaint, su3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends su3 {
        @DexIgnore
        public /* final */ /* synthetic */ TextPaint a;
        @DexIgnore
        public /* final */ /* synthetic */ su3 b;

        @DexIgnore
        public b(TextPaint textPaint, su3 su3) {
            this.a = textPaint;
            this.b = su3;
        }

        @DexIgnore
        @Override // com.fossil.su3
        public void a(Typeface typeface, boolean z) {
            qu3.this.a(this.a, typeface);
            this.b.a(typeface, z);
        }

        @DexIgnore
        @Override // com.fossil.su3
        public void a(int i) {
            this.b.a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends c7.a {
        @DexIgnore
        public /* final */ /* synthetic */ su3 a;

        @DexIgnore
        public a(su3 su3) {
            this.a = su3;
        }

        @DexIgnore
        @Override // com.fossil.c7.a
        public void a(Typeface typeface) {
            qu3 qu3 = qu3.this;
            Typeface unused = qu3.n = Typeface.create(typeface, qu3.e);
            boolean unused2 = qu3.this.m = true;
            this.a.a(qu3.this.n, false);
        }

        @DexIgnore
        @Override // com.fossil.c7.a
        public void a(int i) {
            boolean unused = qu3.this.m = true;
            this.a.a(i);
        }
    }

    @DexIgnore
    public void b(Context context, TextPaint textPaint, su3 su3) {
        c(context, textPaint, su3);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.k;
        float f3 = this.i;
        float f4 = this.j;
        ColorStateList colorStateList2 = this.h;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    @DexIgnore
    public Typeface a(Context context) {
        if (this.m) {
            return this.n;
        }
        if (!context.isRestricted()) {
            try {
                Typeface a2 = c7.a(context, this.l);
                this.n = a2;
                if (a2 != null) {
                    this.n = Typeface.create(a2, this.e);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
            } catch (Exception e2) {
                Log.d("TextAppearance", "Error loading font " + this.g, e2);
            }
        }
        a();
        this.m = true;
        return this.n;
    }

    @DexIgnore
    public void a(Context context, su3 su3) {
        if (ru3.a()) {
            a(context);
        } else {
            a();
        }
        if (this.l == 0) {
            this.m = true;
        }
        if (this.m) {
            su3.a(this.n, true);
            return;
        }
        try {
            c7.a(context, this.l, new a(su3), null);
        } catch (Resources.NotFoundException unused) {
            this.m = true;
            su3.a(1);
        } catch (Exception e2) {
            Log.d("TextAppearance", "Error loading font " + this.g, e2);
            this.m = true;
            su3.a(-3);
        }
    }

    @DexIgnore
    public void a(Context context, TextPaint textPaint, su3 su3) {
        a(textPaint, b());
        a(context, new b(textPaint, su3));
    }

    @DexIgnore
    public final void a() {
        String str;
        if (this.n == null && (str = this.g) != null) {
            this.n = Typeface.create(str, this.e);
        }
        if (this.n == null) {
            int i2 = this.f;
            if (i2 == 1) {
                this.n = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.n = Typeface.SERIF;
            } else if (i2 != 3) {
                this.n = Typeface.DEFAULT;
            } else {
                this.n = Typeface.MONOSPACE;
            }
            this.n = Typeface.create(this.n, this.e);
        }
    }

    @DexIgnore
    public void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i2 = (~typeface.getStyle()) & this.e;
        textPaint.setFakeBoldText((i2 & 1) != 0);
        textPaint.setTextSkewX((i2 & 2) != 0 ? -0.25f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        textPaint.setTextSize(this.a);
    }
}
