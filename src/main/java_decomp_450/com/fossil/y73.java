package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y73 extends wm2 implements x73 {
    @DexIgnore
    public y73() {
        super("com.google.android.gms.maps.internal.IOnPolylineClickListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a(um2.a(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
