package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j31 extends e71 {
    @DexIgnore
    public long L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ j31(long j, short s, ri1 ri1, int i, int i2) {
        super(g51.LEGACY_ERASE_SEGMENT, s, qa1.S, ri1, (i2 & 8) != 0 ? 3 : i);
        this.N = j;
        this.M = true;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            long b = yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            this.L = b;
            yz0.a(jSONObject, r51.T2, Long.valueOf(b));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.e71, com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.S2, Long.valueOf(this.N));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), r51.T2, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).array();
        ee7.a((Object) array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.M;
    }
}
