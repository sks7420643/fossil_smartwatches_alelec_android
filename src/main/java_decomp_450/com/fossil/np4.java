package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.rp4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np4 extends go5 implements rp4.a {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<yy4> g;
    @DexIgnore
    public pp4 h;
    @DexIgnore
    public rp4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return np4.p;
        }

        @DexIgnore
        public final np4 b() {
            return new np4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ np4 a;

        @DexIgnore
        public b(np4 np4) {
            this.a = np4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ np4 a;

        @DexIgnore
        public c(np4 np4) {
            this.a = np4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ProgressBar progressBar;
            yy4 yy4 = (yy4) np4.b(this.a).a();
            if (yy4 != null && (progressBar = yy4.s) != null) {
                ee7.a((Object) bool, "it");
                progressBar.setVisibility(bool.booleanValue() ? 0 : 8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<List<sn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ np4 a;

        @DexIgnore
        public d(np4 np4) {
            this.a = np4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<sn4> list) {
            rp4 a2;
            if (list != null && (a2 = this.a.i) != null) {
                a2.a(list);
            }
        }
    }

    /*
    static {
        String simpleName = np4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCCreateChallengeIntroFr\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 b(np4 np4) {
        qw6<yy4> qw6 = np4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(0);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            return true;
        }
        activity2.finish();
        return true;
    }

    @DexIgnore
    public final void f1() {
        qw6<yy4> qw6 = this.g;
        if (qw6 != null) {
            yy4 a2 = qw6.a();
            if (a2 != null) {
                rp4 rp4 = new rp4();
                this.i = rp4;
                if (rp4 != null) {
                    rp4.a(this);
                }
                RecyclerView recyclerView = a2.u;
                ee7.a((Object) recyclerView, "rvTemplates");
                recyclerView.setAdapter(this.i);
                RecyclerView recyclerView2 = a2.u;
                ee7.a((Object) recyclerView2, "rvTemplates");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
                a2.r.setOnClickListener(new b(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        pp4 pp4 = this.h;
        if (pp4 != null) {
            pp4.b().a(getViewLifecycleOwner(), new c(this));
            pp4 pp42 = this.h;
            if (pp42 != null) {
                pp42.c().a(getViewLifecycleOwner(), new d(this));
            } else {
                ee7.d("mViewModelHome");
                throw null;
            }
        } else {
            ee7.d("mViewModelHome");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().k().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(pp4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026troViewModel::class.java)");
            this.h = (pp4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        yy4 yy4 = (yy4) qb.a(layoutInflater, 2131558525, viewGroup, false, a1());
        this.g = new qw6<>(this, yy4);
        ee7.a((Object) yy4, "binding");
        return yy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qd5 c2 = qd5.f.c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a("bc_challenge_mode", activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        pp4 pp4 = this.h;
        if (pp4 != null) {
            pp4.a();
            f1();
            g1();
            return;
        }
        ee7.d("mViewModelHome");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.rp4.a
    public void a(sn4 sn4) {
        ee7.b(sn4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
        BCCreateChallengeInputActivity.y.a(this, sn4);
    }
}
