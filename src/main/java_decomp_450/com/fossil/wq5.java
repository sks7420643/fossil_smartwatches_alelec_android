package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wq5 implements Factory<uq5> {
    @DexIgnore
    public static uq5 a(vq5 vq5) {
        uq5 a = vq5.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
