package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ub3> CREATOR; // = new xb3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ tb3 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public ub3(String str, tb3 tb3, String str2, long j) {
        this.a = str;
        this.b = tb3;
        this.c = str2;
        this.d = j;
    }

    @DexIgnore
    public final String toString() {
        String str = this.c;
        String str2 = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a, false);
        k72.a(parcel, 3, (Parcelable) this.b, i, false);
        k72.a(parcel, 4, this.c, false);
        k72.a(parcel, 5, this.d);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public ub3(ub3 ub3, long j) {
        a72.a(ub3);
        this.a = ub3.a;
        this.b = ub3.b;
        this.c = ub3.c;
        this.d = j;
    }
}
