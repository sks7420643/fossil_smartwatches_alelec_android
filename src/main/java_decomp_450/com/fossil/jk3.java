package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ nm3 a;
    @DexIgnore
    public /* final */ /* synthetic */ r43 b;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 c;

    @DexIgnore
    public jk3(ek3 ek3, nm3 nm3, r43 r43) {
        this.c = ek3;
        this.a = nm3;
        this.b = r43;
    }

    @DexIgnore
    public final void run() {
        try {
            bg3 d = this.c.d;
            if (d == null) {
                this.c.e().t().a("Failed to get app instance id");
                return;
            }
            String a2 = d.a(this.a);
            if (a2 != null) {
                this.c.o().a(a2);
                this.c.k().l.a(a2);
            }
            this.c.J();
            this.c.j().a(this.b, a2);
        } catch (RemoteException e) {
            this.c.e().t().a("Failed to get app instance id", e);
        } finally {
            this.c.j().a(this.b, (String) null);
        }
    }
}
