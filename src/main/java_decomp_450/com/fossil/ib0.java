package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ib0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ tf0 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ib0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ib0 createFromParcel(Parcel parcel) {
            return new ib0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ib0[] newArray(int i) {
            return new ib0[i];
        }
    }

    @DexIgnore
    public ib0(byte b, String str, tf0 tf0) {
        super(cb0.COMMUTE_TIME_WATCH_APP, b);
        this.d = str;
        this.c = tf0;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(ib0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            ib0 ib0 = (ib0) obj;
            return this.c == ib0.c && !(ee7.a(this.d, ib0.d) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.CommuteTimeWatchAppNotification");
    }

    @DexIgnore
    public final tf0 getAction() {
        return this.c;
    }

    @DexIgnore
    public final String getDestination() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
    }

    @DexIgnore
    public /* synthetic */ ib0(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
            this.c = tf0.values()[parcel.readInt()];
            return;
        }
        ee7.a();
        throw null;
    }
}
