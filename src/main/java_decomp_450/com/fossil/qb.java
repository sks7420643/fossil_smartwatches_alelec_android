package com.fossil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qb {
    @DexIgnore
    public static nb a; // = new ob();
    @DexIgnore
    public static pb b; // = null;

    @DexIgnore
    public static pb a() {
        return b;
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z) {
        return (T) a(layoutInflater, i, viewGroup, z, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z, pb pbVar) {
        int i2 = 0;
        boolean z2 = viewGroup != null && z;
        if (z2) {
            i2 = viewGroup.getChildCount();
        }
        return z2 ? (T) a(pbVar, viewGroup, i2, i) : (T) a(pbVar, layoutInflater.inflate(i, viewGroup, z), i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pb pbVar, View[] viewArr, int i) {
        return (T) a.a(pbVar, viewArr, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pb pbVar, View view, int i) {
        return (T) a.a(pbVar, view, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i) {
        return (T) a(activity, i, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i, pb pbVar) {
        activity.setContentView(i);
        return (T) a(pbVar, (ViewGroup) activity.getWindow().getDecorView().findViewById(16908290), 0, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pb pbVar, ViewGroup viewGroup, int i, int i2) {
        int childCount = viewGroup.getChildCount();
        int i3 = childCount - i;
        if (i3 == 1) {
            return (T) a(pbVar, viewGroup.getChildAt(childCount - 1), i2);
        }
        View[] viewArr = new View[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            viewArr[i4] = viewGroup.getChildAt(i4 + i);
        }
        return (T) a(pbVar, viewArr, i2);
    }
}
