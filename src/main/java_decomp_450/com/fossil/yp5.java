package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp5 extends RecyclerView.g<b> implements Filterable {
    @DexIgnore
    public List<at5> a; // = new ArrayList();
    @DexIgnore
    public int b;
    @DexIgnore
    public List<at5> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(at5 at5, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ i85 a;
        @DexIgnore
        public /* final */ /* synthetic */ yp5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List c = this.a.b.c;
                    if (c != null) {
                        InstalledApp installedApp = ((at5) c.get(adapterPosition)).getInstalledApp();
                        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                        if (isSelected != null) {
                            boolean booleanValue = isSelected.booleanValue();
                            if (booleanValue) {
                                List c2 = this.a.b.c;
                                if (c2 == null) {
                                    ee7.a();
                                    throw null;
                                } else if (((at5) c2.get(adapterPosition)).getCurrentHandGroup() != this.a.b.b) {
                                    a d = this.a.b.d;
                                    if (d != null) {
                                        List c3 = this.a.b.c;
                                        if (c3 != null) {
                                            d.a((at5) c3.get(adapterPosition), booleanValue);
                                            return;
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            a d2 = this.a.b.d;
                            if (d2 != null) {
                                List c4 = this.a.b.c;
                                if (c4 != null) {
                                    d2.a((at5) c4.get(adapterPosition), !booleanValue);
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(yp5 yp5, i85 i85) {
            super(i85.d());
            ee7.b(i85, "binding");
            this.b = yp5;
            this.a = i85;
            i85.w.setOnClickListener(new a(this));
            String b2 = eh5.l.a().b("nonBrandSeparatorLine");
            String b3 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b3)) {
                this.a.q.setBackgroundColor(Color.parseColor(b3));
            }
            if (!TextUtils.isEmpty(b2)) {
                this.a.u.setBackgroundColor(Color.parseColor(b2));
            }
        }

        @DexIgnore
        public final void a(at5 at5) {
            ee7.b(at5, "appWrapper");
            ImageView imageView = this.a.t;
            ee7.a((Object) imageView, "binding.ivAppIcon");
            hd5.a(imageView.getContext()).a(at5.getUri()).a(((r40) ((r40) new r40().h()).a(iy.a)).a(true)).h().a(this.a.t);
            FlexibleTextView flexibleTextView = this.a.r;
            ee7.a((Object) flexibleTextView, "binding.ftvAppName");
            InstalledApp installedApp = at5.getInstalledApp();
            flexibleTextView.setText(installedApp != null ? installedApp.getTitle() : null);
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.w;
            ee7.a((Object) flexibleSwitchCompat, "binding.swEnabled");
            InstalledApp installedApp2 = at5.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected != null) {
                flexibleSwitchCompat.setChecked(isSelected.booleanValue());
                if (at5.getCurrentHandGroup() == 0 || at5.getCurrentHandGroup() == this.b.b) {
                    FlexibleTextView flexibleTextView2 = this.a.s;
                    ee7.a((Object) flexibleTextView2, "binding.ftvAssigned");
                    flexibleTextView2.setText("");
                    FlexibleTextView flexibleTextView3 = this.a.s;
                    ee7.a((Object) flexibleTextView3, "binding.ftvAssigned");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = this.a.s;
                ee7.a((Object) flexibleTextView4, "binding.ftvAssigned");
                we7 we7 = we7.a;
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886157);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(at5.getCurrentHandGroup())}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                FlexibleTextView flexibleTextView5 = this.a.s;
                ee7.a((Object) flexibleTextView5, "binding.ftvAssigned");
                flexibleTextView5.setVisibility(0);
                FlexibleSwitchCompat flexibleSwitchCompat2 = this.a.w;
                ee7.a((Object) flexibleSwitchCompat2, "binding.swEnabled");
                flexibleSwitchCompat2.setChecked(false);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ yp5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(yp5 yp5) {
            this.a = yp5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            ee7.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.a;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (at5 at5 : this.a.a) {
                        InstalledApp installedApp = at5.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            ee7.a((Object) str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && nh7.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                            arrayList.add(at5);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            ee7.b(charSequence, "charSequence");
            ee7.b(filterResults, "results");
            this.a.c = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<at5> list = this.c;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        i85 a2 = i85.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemAppHybridNotificatio\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        List<at5> list = this.c;
        if (list != null) {
            bVar.a(list.get(i));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<at5> list, int i) {
        ee7.b(list, "listAppWrapper");
        this.b = i;
        this.a = list;
        this.c = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(a aVar) {
        ee7.b(aVar, "listener");
        this.d = aVar;
    }
}
