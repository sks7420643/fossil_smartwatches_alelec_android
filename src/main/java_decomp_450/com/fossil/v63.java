package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface v63 extends IInterface {
    @DexIgnore
    ab2 a(float f) throws RemoteException;

    @DexIgnore
    ab2 a(float f, float f2) throws RemoteException;

    @DexIgnore
    ab2 a(float f, int i, int i2) throws RemoteException;

    @DexIgnore
    ab2 a(CameraPosition cameraPosition) throws RemoteException;

    @DexIgnore
    ab2 a(LatLng latLng) throws RemoteException;

    @DexIgnore
    ab2 a(LatLng latLng, float f) throws RemoteException;

    @DexIgnore
    ab2 a(LatLngBounds latLngBounds, int i) throws RemoteException;

    @DexIgnore
    ab2 e(float f) throws RemoteException;

    @DexIgnore
    ab2 n() throws RemoteException;

    @DexIgnore
    ab2 z() throws RemoteException;
}
