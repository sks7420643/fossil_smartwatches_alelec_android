package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r52 extends Fragment implements x12 {
    @DexIgnore
    public static WeakHashMap<FragmentActivity, WeakReference<r52>> d; // = new WeakHashMap<>();
    @DexIgnore
    public Map<String, LifecycleCallback> a; // = new n4();
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public Bundle c;

    @DexIgnore
    public static r52 a(FragmentActivity fragmentActivity) {
        r52 r52;
        WeakReference<r52> weakReference = d.get(fragmentActivity);
        if (weakReference != null && (r52 = weakReference.get()) != null) {
            return r52;
        }
        try {
            r52 r522 = (r52) fragmentActivity.getSupportFragmentManager().b("SupportLifecycleFragmentImpl");
            if (r522 == null || r522.isRemoving()) {
                r522 = new r52();
                nc b2 = fragmentActivity.getSupportFragmentManager().b();
                b2.a(r522, "SupportLifecycleFragmentImpl");
                b2.b();
            }
            d.put(fragmentActivity, new WeakReference<>(r522));
            return r522;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    @Override // com.fossil.x12
    public final /* synthetic */ Activity m0() {
        return getActivity();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.a(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = 1;
        this.c = bundle;
        for (Map.Entry<String, LifecycleCallback> entry : this.a.entrySet()) {
            entry.getValue().a(bundle != null ? bundle.getBundle(entry.getKey()) : null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onDestroy() {
        super.onDestroy();
        this.b = 5;
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.b();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onResume() {
        super.onResume();
        this.b = 3;
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.c();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry<String, LifecycleCallback> entry : this.a.entrySet()) {
                Bundle bundle2 = new Bundle();
                entry.getValue().b(bundle2);
                bundle.putBundle(entry.getKey(), bundle2);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onStart() {
        super.onStart();
        this.b = 2;
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onStop() {
        super.onStop();
        this.b = 4;
        for (LifecycleCallback lifecycleCallback : this.a.values()) {
            lifecycleCallback.e();
        }
    }

    @DexIgnore
    @Override // com.fossil.x12
    public final <T extends LifecycleCallback> T a(String str, Class<T> cls) {
        return cls.cast(this.a.get(str));
    }

    @DexIgnore
    @Override // com.fossil.x12
    public final void a(String str, LifecycleCallback lifecycleCallback) {
        if (!this.a.containsKey(str)) {
            this.a.put(str, lifecycleCallback);
            if (this.b > 0) {
                new kg2(Looper.getMainLooper()).post(new s52(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
}
