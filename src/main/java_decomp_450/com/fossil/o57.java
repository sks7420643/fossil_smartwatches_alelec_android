package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o57 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public DisplayMetrics c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public Context o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;

    @DexIgnore
    public o57(Context context) {
        this.b = "2.0.3";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        Context applicationContext = context.getApplicationContext();
        this.o = applicationContext;
        this.c = v57.g(applicationContext);
        this.a = v57.m(this.o);
        this.h = w37.d(this.o);
        this.i = v57.l(this.o);
        this.j = TimeZone.getDefault().getID();
        this.l = v57.r(this.o);
        this.k = v57.s(this.o);
        this.m = this.o.getPackageName();
        if (this.d >= 14) {
            this.p = v57.y(this.o);
        }
        this.q = v57.x(this.o).toString();
        this.r = v57.w(this.o);
        this.s = v57.d();
        this.n = v57.b(this.o);
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        if (thread == null) {
            if (this.c != null) {
                jSONObject.put("sr", this.c.widthPixels + vt7.ANY_MARKER + this.c.heightPixels);
                jSONObject.put("dpi", this.c.xdpi + vt7.ANY_MARKER + this.c.ydpi);
            }
            if (k47.a(this.o).e()) {
                JSONObject jSONObject2 = new JSONObject();
                a67.a(jSONObject2, "bs", a67.d(this.o));
                a67.a(jSONObject2, "ss", a67.e(this.o));
                if (jSONObject2.length() > 0) {
                    a67.a(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray a2 = a67.a(this.o, 10);
            if (a2 != null && a2.length() > 0) {
                a67.a(jSONObject, "wflist", a2.toString());
            }
            str = this.p;
            str2 = "sen";
        } else {
            a67.a(jSONObject, "thn", thread.getName());
            a67.a(jSONObject, "qq", w37.f(this.o));
            a67.a(jSONObject, "cui", w37.c(this.o));
            if (v57.c(this.r) && this.r.split("/").length == 2) {
                a67.a(jSONObject, "fram", this.r.split("/")[0]);
            }
            if (v57.c(this.s) && this.s.split("/").length == 2) {
                a67.a(jSONObject, "from", this.s.split("/")[0]);
            }
            if (x47.b(this.o).a(this.o) != null) {
                jSONObject.put("ui", x47.b(this.o).a(this.o).b());
            }
            str = w37.e(this.o);
            str2 = "mid";
        }
        a67.a(jSONObject, str2, str);
        a67.a(jSONObject, "pcn", v57.t(this.o));
        a67.a(jSONObject, "osn", Build.VERSION.RELEASE);
        a67.a(jSONObject, "av", this.a);
        a67.a(jSONObject, "ch", this.h);
        a67.a(jSONObject, "mf", this.f);
        a67.a(jSONObject, "sv", this.b);
        a67.a(jSONObject, "osd", Build.DISPLAY);
        a67.a(jSONObject, "prod", Build.PRODUCT);
        a67.a(jSONObject, "tags", Build.TAGS);
        a67.a(jSONObject, "id", Build.ID);
        a67.a(jSONObject, "fng", Build.FINGERPRINT);
        a67.a(jSONObject, "lch", this.n);
        a67.a(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        a67.a(jSONObject, "op", this.i);
        a67.a(jSONObject, "lg", this.g);
        a67.a(jSONObject, "md", this.e);
        a67.a(jSONObject, "tz", this.j);
        int i2 = this.l;
        if (i2 != 0) {
            jSONObject.put("jb", i2);
        }
        a67.a(jSONObject, "sd", this.k);
        a67.a(jSONObject, "apn", this.m);
        a67.a(jSONObject, "cpu", this.q);
        a67.a(jSONObject, "abi", Build.CPU_ABI);
        a67.a(jSONObject, "abi2", Build.CPU_ABI2);
        a67.a(jSONObject, "ram", this.r);
        a67.a(jSONObject, "rom", this.s);
    }
}
