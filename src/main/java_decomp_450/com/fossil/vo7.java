package com.fossil;

import com.fossil.fo7;
import com.fossil.lo7;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo7 {
    @DexIgnore
    public /* final */ lo7 a;
    @DexIgnore
    public /* final */ Response b;

    @DexIgnore
    public vo7(lo7 lo7, Response response) {
        this.a = lo7;
        this.b = response;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        if (r3.b().b() == false) goto L_0x0059;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(okhttp3.Response r3, com.fossil.lo7 r4) {
        /*
            int r0 = r3.e()
            r1 = 200(0xc8, float:2.8E-43)
            r2 = 0
            if (r0 == r1) goto L_0x005a
            r1 = 410(0x19a, float:5.75E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 414(0x19e, float:5.8E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 501(0x1f5, float:7.02E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 203(0xcb, float:2.84E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 204(0xcc, float:2.86E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 307(0x133, float:4.3E-43)
            if (r0 == r1) goto L_0x0031
            r1 = 308(0x134, float:4.32E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 404(0x194, float:5.66E-43)
            if (r0 == r1) goto L_0x005a
            r1 = 405(0x195, float:5.68E-43)
            if (r0 == r1) goto L_0x005a
            switch(r0) {
                case 300: goto L_0x005a;
                case 301: goto L_0x005a;
                case 302: goto L_0x0031;
                default: goto L_0x0030;
            }
        L_0x0030:
            goto L_0x0059
        L_0x0031:
            java.lang.String r0 = "Expires"
            java.lang.String r0 = r3.b(r0)
            if (r0 != 0) goto L_0x005a
            com.fossil.pn7 r0 = r3.b()
            int r0 = r0.d()
            r1 = -1
            if (r0 != r1) goto L_0x005a
            com.fossil.pn7 r0 = r3.b()
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x005a
            com.fossil.pn7 r0 = r3.b()
            boolean r0 = r0.b()
            if (r0 == 0) goto L_0x0059
            goto L_0x005a
        L_0x0059:
            return r2
        L_0x005a:
            com.fossil.pn7 r3 = r3.b()
            boolean r3 = r3.i()
            if (r3 != 0) goto L_0x006f
            com.fossil.pn7 r3 = r4.b()
            boolean r3 = r3.i()
            if (r3 != 0) goto L_0x006f
            r2 = 1
        L_0x006f:
            return r2
            switch-data {300->0x005a, 301->0x005a, 302->0x0031, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vo7.a(okhttp3.Response, com.fossil.lo7):boolean");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ lo7 b;
        @DexIgnore
        public /* final */ Response c;
        @DexIgnore
        public Date d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Date h;
        @DexIgnore
        public long i;
        @DexIgnore
        public long j;
        @DexIgnore
        public String k;
        @DexIgnore
        public int l; // = -1;

        @DexIgnore
        public a(long j2, lo7 lo7, Response response) {
            this.a = j2;
            this.b = lo7;
            this.c = response;
            if (response != null) {
                this.i = response.y();
                this.j = response.w();
                fo7 k2 = response.k();
                int b2 = k2.b();
                for (int i2 = 0; i2 < b2; i2++) {
                    String a2 = k2.a(i2);
                    String b3 = k2.b(i2);
                    if (ConfigItem.KEY_DATE.equalsIgnoreCase(a2)) {
                        this.d = jp7.a(b3);
                        this.e = b3;
                    } else if ("Expires".equalsIgnoreCase(a2)) {
                        this.h = jp7.a(b3);
                    } else if ("Last-Modified".equalsIgnoreCase(a2)) {
                        this.f = jp7.a(b3);
                        this.g = b3;
                    } else if ("ETag".equalsIgnoreCase(a2)) {
                        this.k = b3;
                    } else if ("Age".equalsIgnoreCase(a2)) {
                        this.l = kp7.a(b3, -1);
                    }
                }
            }
        }

        @DexIgnore
        public final long a() {
            Date date = this.d;
            long j2 = 0;
            if (date != null) {
                j2 = Math.max(0L, this.j - date.getTime());
            }
            int i2 = this.l;
            if (i2 != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) i2));
            }
            long j3 = this.j;
            return j2 + (j3 - this.i) + (this.a - j3);
        }

        @DexIgnore
        public final long b() {
            long j2;
            long j3;
            pn7 b2 = this.c.b();
            if (b2.d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) b2.d());
            }
            if (this.h != null) {
                Date date = this.d;
                if (date != null) {
                    j3 = date.getTime();
                } else {
                    j3 = this.j;
                }
                long time = this.h.getTime() - j3;
                if (time > 0) {
                    return time;
                }
                return 0;
            } else if (this.f == null || this.c.x().g().l() != null) {
                return 0;
            } else {
                Date date2 = this.d;
                if (date2 != null) {
                    j2 = date2.getTime();
                } else {
                    j2 = this.i;
                }
                long time2 = j2 - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        @DexIgnore
        public vo7 c() {
            vo7 d2 = d();
            return (d2.a == null || !this.b.b().j()) ? d2 : new vo7(null, null);
        }

        @DexIgnore
        public final vo7 d() {
            if (this.c == null) {
                return new vo7(this.b, null);
            }
            if (this.b.d() && this.c.g() == null) {
                return new vo7(this.b, null);
            }
            if (!vo7.a(this.c, this.b)) {
                return new vo7(this.b, null);
            }
            pn7 b2 = this.b.b();
            if (b2.h() || a(this.b)) {
                return new vo7(this.b, null);
            }
            pn7 b3 = this.c.b();
            long a2 = a();
            long b4 = b();
            if (b2.d() != -1) {
                b4 = Math.min(b4, TimeUnit.SECONDS.toMillis((long) b2.d()));
            }
            long j2 = 0;
            long millis = b2.f() != -1 ? TimeUnit.SECONDS.toMillis((long) b2.f()) : 0;
            if (!b3.g() && b2.e() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) b2.e());
            }
            if (!b3.h()) {
                long j3 = millis + a2;
                if (j3 < j2 + b4) {
                    Response.a q = this.c.q();
                    if (j3 >= b4) {
                        q.a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (a2 > LogBuilder.MAX_INTERVAL && e()) {
                        q.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new vo7(null, q.a());
                }
            }
            String str = this.k;
            String str2 = "If-Modified-Since";
            if (str != null) {
                str2 = "If-None-Match";
            } else if (this.f != null) {
                str = this.g;
            } else if (this.d == null) {
                return new vo7(this.b, null);
            } else {
                str = this.e;
            }
            fo7.a a3 = this.b.c().a();
            po7.a.a(a3, str2, str);
            lo7.a f2 = this.b.f();
            f2.a(a3.a());
            return new vo7(f2.a(), this.c);
        }

        @DexIgnore
        public final boolean e() {
            return this.c.b().d() == -1 && this.h == null;
        }

        @DexIgnore
        public static boolean a(lo7 lo7) {
            return (lo7.a("If-Modified-Since") == null && lo7.a("If-None-Match") == null) ? false : true;
        }
    }
}
