package com.fossil;

import java.nio.ByteBuffer;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yx0 {
    @DexIgnore
    public short a; // = -1;
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ qk1 f;

    @DexIgnore
    public yx0(byte[] bArr, int i, qk1 qk1) {
        this.d = bArr;
        this.e = i;
        this.f = qk1;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        ee7.a((Object) wrap, "ByteBuffer.wrap(data)");
        this.b = wrap;
        byte[] bArr2 = this.d;
        this.c = bArr2.length;
        ByteBuffer wrap2 = ByteBuffer.wrap(bArr2);
        ee7.a((Object) wrap2, "ByteBuffer.wrap(data)");
        this.b = wrap2;
    }

    @DexIgnore
    public final byte[] a() {
        int remaining = this.b.remaining();
        if (remaining <= 0) {
            return new byte[0];
        }
        this.a = (short) (this.a + 1);
        byte[] bArr = {(byte) (((yx0) ((qz0) this)).a % 256)};
        int min = Math.min(remaining + 1, this.e);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, min);
        this.b.get(copyOfRange, 1, min - 1);
        ee7.a((Object) copyOfRange, "bytes");
        return copyOfRange;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public final int c() {
        return Math.min((this.a + 1) * (this.e - b()), this.c);
    }
}
