package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l51 {
    @DexIgnore
    public static /* final */ HashMap<Integer, r87<pg0, qg0>> a; // = oa7.b(w87.a(3073, new r87(pg0.RING_PHONE, qg0.STANDARD)), w87.a(6657, new r87(pg0.ALARM, qg0.STANDARD)), w87.a(6658, new r87(pg0.ALARM, qg0.SEQUENCED)), w87.a(7169, new r87(pg0.PROGRESS, qg0.STANDARD)), w87.a(7170, new r87(pg0.PROGRESS, qg0.SWEEP)), w87.a(7681, new r87(pg0.TWENTY_FOUR_HOUR, qg0.STANDARD)), w87.a(7682, new r87(pg0.TWENTY_FOUR_HOUR, qg0.SEQUENCED)), w87.a(1025, new r87(pg0.GOAL_TRACKING, qg0.STANDARD)), w87.a(4097, new r87(pg0.SELFIE, qg0.STANDARD)), w87.a(4609, new r87(pg0.MUSIC_CONTROL, qg0.PLAY_PAUSE)), w87.a(4610, new r87(pg0.MUSIC_CONTROL, qg0.NEXT)), w87.a(4611, new r87(pg0.MUSIC_CONTROL, qg0.PREVIOUS)), w87.a(4612, new r87(pg0.MUSIC_CONTROL, qg0.VOLUME_UP)), w87.a(4613, new r87(pg0.MUSIC_CONTROL, qg0.VOLUME_DOWN)), w87.a(4614, new r87(pg0.MUSIC_CONTROL, qg0.STANDARD)), w87.a(5121, new r87(pg0.DATE, qg0.STANDARD)), w87.a(5122, new r87(pg0.DATE, qg0.SEQUENCED)), w87.a(5633, new r87(pg0.TIME2, qg0.STANDARD)), w87.a(5634, new r87(pg0.TIME2, qg0.SEQUENCED)), w87.a(6145, new r87(pg0.ALERT, qg0.STANDARD)), w87.a(6146, new r87(pg0.ALERT, qg0.SEQUENCED)), w87.a(8193, new r87(pg0.STOPWATCH, qg0.STANDARD)), w87.a(9217, new r87(pg0.COMMUTE_TIME, qg0.TRAVEL)), w87.a(9218, new r87(pg0.COMMUTE_TIME, qg0.ETA)));
    @DexIgnore
    public static /* final */ l51 b; // = new l51();

    @DexIgnore
    public final pg0 a(short s) {
        r87<pg0, qg0> r87 = a.get(Integer.valueOf(s));
        if (r87 != null) {
            return r87.getFirst();
        }
        return pg0.UNDEFINED;
    }

    @DexIgnore
    public final qg0 b(short s) {
        r87<pg0, qg0> r87 = a.get(Integer.valueOf(s));
        if (r87 != null) {
            return r87.getSecond();
        }
        return qg0.UNDEFINED;
    }
}
