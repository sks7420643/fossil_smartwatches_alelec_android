package com.fossil;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cs3 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b; // = 300;
    @DexIgnore
    public TimeInterpolator c; // = null;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public int e; // = 1;

    @DexIgnore
    public cs3(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    @DexIgnore
    public void a(Animator animator) {
        animator.setStartDelay(a());
        animator.setDuration(b());
        animator.setInterpolator(c());
        if (animator instanceof ValueAnimator) {
            ValueAnimator valueAnimator = (ValueAnimator) animator;
            valueAnimator.setRepeatCount(d());
            valueAnimator.setRepeatMode(e());
        }
    }

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public TimeInterpolator c() {
        TimeInterpolator timeInterpolator = this.c;
        return timeInterpolator != null ? timeInterpolator : ur3.b;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cs3)) {
            return false;
        }
        cs3 cs3 = (cs3) obj;
        if (a() == cs3.a() && b() == cs3.b() && d() == cs3.d() && e() == cs3.e()) {
            return c().getClass().equals(cs3.c().getClass());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((int) (a() ^ (a() >>> 32))) * 31) + ((int) (b() ^ (b() >>> 32)))) * 31) + c().getClass().hashCode()) * 31) + d()) * 31) + e();
    }

    @DexIgnore
    public String toString() {
        return '\n' + cs3.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " delay: " + a() + " duration: " + b() + " interpolator: " + c().getClass() + " repeatCount: " + d() + " repeatMode: " + e() + "}\n";
    }

    @DexIgnore
    public static TimeInterpolator b(ValueAnimator valueAnimator) {
        TimeInterpolator interpolator = valueAnimator.getInterpolator();
        if ((interpolator instanceof AccelerateDecelerateInterpolator) || interpolator == null) {
            return ur3.b;
        }
        if (interpolator instanceof AccelerateInterpolator) {
            return ur3.c;
        }
        return interpolator instanceof DecelerateInterpolator ? ur3.d : interpolator;
    }

    @DexIgnore
    public long a() {
        return this.a;
    }

    @DexIgnore
    public static cs3 a(ValueAnimator valueAnimator) {
        cs3 cs3 = new cs3(valueAnimator.getStartDelay(), valueAnimator.getDuration(), b(valueAnimator));
        cs3.d = valueAnimator.getRepeatCount();
        cs3.e = valueAnimator.getRepeatMode();
        return cs3;
    }

    @DexIgnore
    public cs3(long j, long j2, TimeInterpolator timeInterpolator) {
        this.a = j;
        this.b = j2;
        this.c = timeInterpolator;
    }
}
