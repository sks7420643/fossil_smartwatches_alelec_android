package com.fossil;

import java.util.AbstractCollection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls2 extends AbstractCollection<V> {
    @DexIgnore
    public /* final */ /* synthetic */ ds2 a;

    @DexIgnore
    public ls2(ds2 ds2) {
        this.a = ds2;
    }

    @DexIgnore
    public final void clear() {
        this.a.clear();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public final Iterator<V> iterator() {
        return this.a.zzg();
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }
}
