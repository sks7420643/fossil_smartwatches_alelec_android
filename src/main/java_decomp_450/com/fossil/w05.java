package com.fossil;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.WindowInsetsFrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w05 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CropImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ WindowInsetsFrameLayout t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ TextView v;
    @DexIgnore
    public /* final */ TextView w;
    @DexIgnore
    public /* final */ TextView x;

    @DexIgnore
    public w05(Object obj, View view, int i, CropImageView cropImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, WindowInsetsFrameLayout windowInsetsFrameLayout, RecyclerView recyclerView, TextView textView, TextView textView2, TextView textView3) {
        super(obj, view, i);
        this.q = cropImageView;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = windowInsetsFrameLayout;
        this.u = recyclerView;
        this.v = textView;
        this.w = textView2;
        this.x = textView3;
    }
}
