package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jh5 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public GoogleSignInOptions a;
    @DexIgnore
    public sy1 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<cl5> d;
    @DexIgnore
    public mh5 e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements ho3<Void> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        @Override // com.fossil.ho3
        public final void onComplete(no3<Void> no3) {
            ee7.b(no3, "it");
            FLogger.INSTANCE.getLocal().d(jh5.g, "Log out google account completely");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements io3 {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            ee7.b(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = jh5.g;
            local.d(d, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        new a(null);
        String canonicalName = jh5.class.getCanonicalName();
        if (canonicalName != null) {
            ee7.a((Object) canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final void a(WeakReference<cl5> weakReference, mh5 mh5) {
        String str;
        ee7.b(weakReference, Constants.ACTIVITY);
        ee7.b(mh5, Constants.CALLBACK);
        Access a2 = ng5.a().a(PortfolioApp.g0.c());
        if (a2 == null || (str = a2.getA()) == null) {
            str = this.c;
        }
        if (str != null) {
            String obj = nh7.d((CharSequence) str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.u);
            aVar.a(obj);
            aVar.b(obj);
            aVar.b();
            aVar.d();
            GoogleSignInOptions a3 = aVar.a();
            ee7.a((Object) a3, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.a = a3;
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.a;
            if (googleSignInOptions != null) {
                this.b = qy1.a(applicationContext, googleSignInOptions);
                this.e = mh5;
                this.d = weakReference;
                b();
                return;
            }
            ee7.d("gso");
            throw null;
        }
        throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final void b() {
        a(this.d);
        a();
    }

    @DexIgnore
    public final void c() {
        a(this.d);
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            uy1 a2 = my1.f.a(intent);
            a(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            ee7.a((Object) a2, Constants.RESULT);
            sb.append(a2.a());
            local2.d(str2, sb.toString());
            return true;
        }
        a((uy1) null);
        return true;
    }

    @DexIgnore
    public final void a(uy1 uy1) {
        int i = 600;
        if (uy1 == null) {
            c();
            mh5 mh5 = this.e;
            if (mh5 != null) {
                mh5.a(600, null, "");
            }
        } else if (uy1.c()) {
            GoogleSignInAccount b2 = uy1.b();
            if (b2 == null) {
                mh5 mh52 = this.e;
                if (mh52 != null) {
                    mh52.a(600, null, "");
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String g2 = b2.g();
                if (g2 == null) {
                    g2 = "";
                }
                signUpSocialAuth.setEmail(g2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Google user email is " + b2.g());
                String w = b2.w();
                if (w == null) {
                    w = "";
                }
                signUpSocialAuth.setFirstName(w);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "Google user first name is " + b2.w());
                String v = b2.v();
                if (v == null) {
                    v = "";
                }
                signUpSocialAuth.setLastName(v);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = g;
                local3.d(str3, "Google user last name is " + b2.v());
                String z = b2.z();
                if (z == null) {
                    z = "";
                }
                signUpSocialAuth.setToken(z);
                signUpSocialAuth.setClientId(rd5.g.a(""));
                signUpSocialAuth.setService("google");
                mh5 mh53 = this.e;
                if (mh53 != null) {
                    mh53.a(signUpSocialAuth);
                }
                this.f = 0;
                c();
            }
        } else {
            Status a2 = uy1.a();
            ee7.a((Object) a2, "result.status");
            int g3 = a2.g();
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = g;
            local4.d(str4, "login result code from google: " + g3);
            if (g3 == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    a();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().d(g, "why the login session always end up here, bug from Google!!");
                c();
                mh5 mh54 = this.e;
                if (mh54 != null) {
                    mh54.a(600, null, "");
                    return;
                }
                return;
            }
            if (g3 == 12501) {
                i = 2;
            }
            c();
            mh5 mh55 = this.e;
            if (mh55 != null) {
                mh55.a(i, null, "");
            }
        }
    }

    @DexIgnore
    public final void a() {
        sy1 sy1;
        FLogger.INSTANCE.getLocal().d(g, "connectNewAccount");
        WeakReference<cl5> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                ee7.a();
                throw null;
            } else if (!(weakReference.get() == null || (sy1 = this.b) == null)) {
                if (sy1 != null) {
                    Intent i = sy1.i();
                    ee7.a((Object) i, "googleSignInClient!!.signInIntent");
                    WeakReference<cl5> weakReference2 = this.d;
                    if (weakReference2 != null) {
                        cl5 cl5 = weakReference2.get();
                        if (cl5 != null) {
                            cl5.startActivityForResult(i, 922);
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
        }
        mh5 mh5 = this.e;
        if (mh5 != null) {
            mh5.a(600, null, "");
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? weakReference.get() : null) != null && qy1.a(weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            sy1 sy1 = this.b;
            if (sy1 == null) {
                return;
            }
            if (sy1 != null) {
                no3<Void> j = sy1.j();
                if (j != null) {
                    j.a(b.a);
                }
                if (j != null) {
                    j.a(c.a);
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }
    }
}
