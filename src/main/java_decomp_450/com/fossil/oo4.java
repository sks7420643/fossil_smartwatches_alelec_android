package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo4 {
    @DexIgnore
    public mn4 a;
    @DexIgnore
    public jn4 b;
    @DexIgnore
    public jn4 c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public oo4(mn4 mn4, jn4 jn4, jn4 jn42, boolean z) {
        ee7.b(mn4, "challenge");
        this.a = mn4;
        this.b = jn4;
        this.c = jn42;
        this.d = z;
    }

    @DexIgnore
    public final mn4 a() {
        return this.a;
    }

    @DexIgnore
    public final jn4 b() {
        return this.c;
    }

    @DexIgnore
    public final jn4 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof oo4)) {
            return false;
        }
        oo4 oo4 = (oo4) obj;
        return ee7.a(this.a, oo4.a) && ee7.a(this.b, oo4.b) && ee7.a(this.c, oo4.c) && this.d == oo4.d;
    }

    @DexIgnore
    public int hashCode() {
        mn4 mn4 = this.a;
        int i = 0;
        int hashCode = (mn4 != null ? mn4.hashCode() : 0) * 31;
        jn4 jn4 = this.b;
        int hashCode2 = (hashCode + (jn4 != null ? jn4.hashCode() : 0)) * 31;
        jn4 jn42 = this.c;
        if (jn42 != null) {
            i = jn42.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i2 + i3;
    }

    @DexIgnore
    public String toString() {
        return "UIHistory(challenge=" + this.a + ", topPlayer=" + this.b + ", currentPlayer=" + this.c + ", isFirst=" + this.d + ")";
    }
}
