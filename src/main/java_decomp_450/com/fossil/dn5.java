package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn5 implements Factory<cn5> {
    @DexIgnore
    public static cn5 a(NotificationsRepository notificationsRepository) {
        return new cn5(notificationsRepository);
    }
}
