package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lh7 extends kh7 {
    @DexIgnore
    public static final Byte a(String str) {
        ee7.b(str, "$this$toByteOrNull");
        return a(str, 10);
    }

    @DexIgnore
    public static final Integer b(String str) {
        ee7.b(str, "$this$toIntOrNull");
        return b(str, 10);
    }

    @DexIgnore
    public static final Long c(String str) {
        ee7.b(str, "$this$toLongOrNull");
        return c(str, 10);
    }

    @DexIgnore
    public static final Byte a(String str, int i) {
        int intValue;
        ee7.b(str, "$this$toByteOrNull");
        Integer b = b(str, i);
        if (b == null || (intValue = b.intValue()) < -128 || intValue > 127) {
            return null;
        }
        return Byte.valueOf((byte) intValue);
    }

    @DexIgnore
    public static final Integer b(String str, int i) {
        boolean z;
        int i2;
        ee7.b(str, "$this$toIntOrNull");
        qg7.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        int i3 = 0;
        char charAt = str.charAt(0);
        int i4 = -2147483647;
        int i5 = 1;
        if (charAt >= '0') {
            z = false;
            i5 = 0;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                i4 = RecyclerView.UNDEFINED_DURATION;
                z = true;
            } else if (charAt != '+') {
                return null;
            } else {
                z = false;
            }
        }
        int i6 = -59652323;
        while (i5 < length) {
            int a = qg7.a(str.charAt(i5), i);
            if (a < 0) {
                return null;
            }
            if ((i3 < i6 && (i6 != -59652323 || i3 < (i6 = i4 / i))) || (i2 = i3 * i) < i4 + a) {
                return null;
            }
            i3 = i2 - a;
            i5++;
        }
        return z ? Integer.valueOf(i3) : Integer.valueOf(-i3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Long c(java.lang.String r18, int r19) {
        /*
            r0 = r18
            r1 = r19
            java.lang.String r2 = "$this$toLongOrNull"
            com.fossil.ee7.b(r0, r2)
            com.fossil.qg7.a(r19)
            int r2 = r18.length()
            r3 = 0
            if (r2 != 0) goto L_0x0014
            return r3
        L_0x0014:
            r4 = 0
            char r5 = r0.charAt(r4)
            r6 = 48
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r9 = 1
            if (r5 >= r6) goto L_0x0035
            if (r2 != r9) goto L_0x0026
            return r3
        L_0x0026:
            r6 = 45
            if (r5 != r6) goto L_0x002e
            r7 = -9223372036854775808
            r4 = 1
            goto L_0x0036
        L_0x002e:
            r6 = 43
            if (r5 != r6) goto L_0x0034
            r4 = 1
            goto L_0x0035
        L_0x0034:
            return r3
        L_0x0035:
            r9 = 0
        L_0x0036:
            r5 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
            r10 = 0
            r12 = r5
        L_0x003e:
            if (r4 >= r2) goto L_0x006f
            char r14 = r0.charAt(r4)
            int r14 = com.fossil.qg7.a(r14, r1)
            if (r14 >= 0) goto L_0x004b
            return r3
        L_0x004b:
            int r15 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r15 >= 0) goto L_0x005b
            int r15 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r15 != 0) goto L_0x005a
            long r12 = (long) r1
            long r12 = r7 / r12
            int r15 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r15 >= 0) goto L_0x005b
        L_0x005a:
            return r3
        L_0x005b:
            long r5 = (long) r1
            long r10 = r10 * r5
            long r5 = (long) r14
            long r16 = r7 + r5
            int r14 = (r10 > r16 ? 1 : (r10 == r16 ? 0 : -1))
            if (r14 >= 0) goto L_0x0066
            return r3
        L_0x0066:
            long r10 = r10 - r5
            int r4 = r4 + 1
            r5 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
            goto L_0x003e
        L_0x006f:
            if (r9 == 0) goto L_0x0076
            java.lang.Long r0 = java.lang.Long.valueOf(r10)
            goto L_0x007b
        L_0x0076:
            long r0 = -r10
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
        L_0x007b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lh7.c(java.lang.String, int):java.lang.Long");
    }
}
