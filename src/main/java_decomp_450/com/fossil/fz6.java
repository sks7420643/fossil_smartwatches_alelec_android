package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz6 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public fz6(int i, int i2, int i3, int i4, int i5, int i6) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final int c() {
        return this.f;
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof fz6)) {
            return false;
        }
        fz6 fz6 = (fz6) obj;
        return this.a == fz6.a && this.b == fz6.b && this.c == fz6.c && this.d == fz6.d && this.e == fz6.e && this.f == fz6.f;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    @DexIgnore
    public String toString() {
        return "TodayHeartRateModel(value=" + this.a + ", minValue=" + this.b + ", maxValue=" + this.c + ", startTime=" + this.d + ", endTime=" + this.e + ", midTime=" + this.f + ")";
    }
}
