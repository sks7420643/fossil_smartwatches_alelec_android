package com.fossil;

import android.util.Log;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class te5 {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ c[] c; // = {new c(d.INITIAL), new c(d.BEFORE), new c(d.AFTER)};
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<a> d; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(g gVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public e a;
        @DexIgnore
        public b b;
        @DexIgnore
        public Throwable c;
        @DexIgnore
        public f d; // = f.SUCCESS;

        @DexIgnore
        public c(d dVar) {
        }
    }

    @DexIgnore
    public enum d {
        INITIAL,
        BEFORE,
        AFTER
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ b a;
        @DexIgnore
        public /* final */ te5 b;
        @DexIgnore
        public /* final */ d c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                e eVar = e.this;
                eVar.b.a(eVar.c, eVar.a);
            }
        }

        @DexIgnore
        public e(b bVar, te5 te5, d dVar) {
            this.a = bVar;
            this.b = te5;
            this.c = dVar;
        }

        @DexIgnore
        public void a(Executor executor) {
            executor.execute(new a());
        }

        @DexIgnore
        public void run() {
            this.a.run(new b.a(this, this.b));
        }
    }

    @DexIgnore
    public enum f {
        RUNNING,
        SUCCESS,
        FAILED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {
        @DexIgnore
        public /* final */ f a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public /* final */ f c;
        @DexIgnore
        public /* final */ Throwable[] d;

        @DexIgnore
        public g(f fVar, f fVar2, f fVar3, Throwable[] thArr) {
            this.a = fVar;
            this.b = fVar2;
            this.c = fVar3;
            this.d = thArr;
        }

        @DexIgnore
        public boolean a() {
            f fVar = this.a;
            f fVar2 = f.FAILED;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean b() {
            f fVar = this.a;
            f fVar2 = f.RUNNING;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || g.class != obj.getClass()) {
                return false;
            }
            g gVar = (g) obj;
            if (this.a == gVar.a && this.b == gVar.b && this.c == gVar.c) {
                return Arrays.equals(this.d, gVar.d);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d);
        }

        @DexIgnore
        public String toString() {
            return "StatusReport{initial=" + this.a + ", before=" + this.b + ", after=" + this.c + ", mErrors=" + Arrays.toString(this.d) + '}';
        }

        @DexIgnore
        public Throwable a(d dVar) {
            return this.d[dVar.ordinal()];
        }
    }

    @DexIgnore
    public te5(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a(a aVar) {
        return this.d.add(aVar);
    }

    @DexIgnore
    public boolean b(a aVar) {
        return this.d.remove(aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r4 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        new com.fossil.te5.e(r7, r5, r6).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.fossil.te5.d r6, com.fossil.te5.b r7) {
        /*
            r5 = this;
            java.util.concurrent.CopyOnWriteArrayList<com.fossil.te5$a> r0 = r5.d
            boolean r0 = r0.isEmpty()
            r1 = 1
            r0 = r0 ^ r1
            java.lang.Object r2 = r5.a
            monitor-enter(r2)
            com.fossil.te5$c[] r3 = r5.c     // Catch:{ all -> 0x003a }
            int r4 = r6.ordinal()     // Catch:{ all -> 0x003a }
            r3 = r3[r4]     // Catch:{ all -> 0x003a }
            com.fossil.te5$b r4 = r3.b     // Catch:{ all -> 0x003a }
            if (r4 == 0) goto L_0x001a
            r6 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            return r6
        L_0x001a:
            r3.b = r7     // Catch:{ all -> 0x003a }
            com.fossil.te5$f r4 = com.fossil.te5.f.RUNNING     // Catch:{ all -> 0x003a }
            r3.d = r4     // Catch:{ all -> 0x003a }
            r4 = 0
            r3.a = r4     // Catch:{ all -> 0x003a }
            r3.c = r4     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x002b
            com.fossil.te5$g r4 = r5.a()     // Catch:{ all -> 0x003a }
        L_0x002b:
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            if (r4 == 0) goto L_0x0031
            r5.a(r4)
        L_0x0031:
            com.fossil.te5$e r0 = new com.fossil.te5$e
            r0.<init>(r7, r5, r6)
            r0.run()
            return r1
        L_0x003a:
            r6 = move-exception
            monitor-exit(r2)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.te5.a(com.fossil.te5$d, com.fossil.te5$b):boolean");
    }

    @DexIgnore
    public boolean b() {
        int i;
        FLogger.INSTANCE.getLocal().d("PagingRequestHelper", "retryAllFailed");
        int length = d.values().length;
        e[] eVarArr = new e[length];
        synchronized (this.a) {
            for (int i2 = 0; i2 < d.values().length; i2++) {
                eVarArr[i2] = this.c[i2].a;
                this.c[i2].a = null;
            }
        }
        boolean z = false;
        for (i = 0; i < length; i++) {
            e eVar = eVarArr[i];
            if (eVar != null) {
                eVar.a(this.b);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @FunctionalInterface
    public interface b {
        @DexIgnore
        void run(a aVar);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a {
            @DexIgnore
            public /* final */ AtomicBoolean a; // = new AtomicBoolean();
            @DexIgnore
            public /* final */ e b;
            @DexIgnore
            public /* final */ te5 c;

            @DexIgnore
            public a(e eVar, te5 te5) {
                this.b = eVar;
                this.c = te5;
            }

            @DexIgnore
            public final void a() {
                Log.d("PagingRequestHelper", "recordSuccess");
                if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, (Throwable) null);
                    return;
                }
                throw new IllegalStateException("already called recordSuccess or recordFailure");
            }

            @DexIgnore
            public final void a(Throwable th) {
                Log.d("PagingRequestHelper", "recordFailure");
                if (th == null) {
                    throw new IllegalArgumentException("You must provide a throwable describing the error to record the failure");
                } else if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, th);
                } else {
                    throw new IllegalStateException("already called recordSuccess or recordFailure");
                }
            }
        }
    }

    @DexIgnore
    public final g a() {
        c[] cVarArr = this.c;
        return new g(a(d.INITIAL), a(d.BEFORE), a(d.AFTER), new Throwable[]{cVarArr[0].c, cVarArr[1].c, cVarArr[2].c});
    }

    @DexIgnore
    public final f a(d dVar) {
        return this.c[dVar.ordinal()].d;
    }

    @DexIgnore
    public void a(e eVar, Throwable th) {
        g gVar;
        boolean z = th == null;
        boolean isEmpty = true ^ this.d.isEmpty();
        synchronized (this.a) {
            c cVar = this.c[eVar.c.ordinal()];
            gVar = null;
            cVar.b = null;
            cVar.c = th;
            if (z) {
                cVar.a = null;
                cVar.d = f.SUCCESS;
            } else {
                cVar.a = eVar;
                cVar.d = f.FAILED;
            }
            if (isEmpty) {
                gVar = a();
            }
        }
        if (gVar != null) {
            a(gVar);
        }
    }

    @DexIgnore
    public final void a(g gVar) {
        Iterator<a> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().a(gVar);
        }
    }
}
