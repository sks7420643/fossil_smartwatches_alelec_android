package com.fossil;

import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk4 implements Factory<ShortcutApiService> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<hj5> b;
    @DexIgnore
    public /* final */ Provider<lj5> c;

    @DexIgnore
    public uk4(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static uk4 a(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        return new uk4(wj4, provider, provider2);
    }

    @DexIgnore
    public static ShortcutApiService a(wj4 wj4, hj5 hj5, lj5 lj5) {
        ShortcutApiService d = wj4.d(hj5, lj5);
        c87.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ShortcutApiService get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
