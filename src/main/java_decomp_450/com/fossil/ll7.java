package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll7 extends ti7 {
    @DexIgnore
    public static /* final */ ll7 b; // = new ll7();

    @DexIgnore
    @Override // com.fossil.ti7
    public void a(ib7 ib7, Runnable runnable) {
        nl7 nl7 = (nl7) ib7.get(nl7.b);
        if (nl7 != null) {
            nl7.a = true;
            return;
        }
        throw new UnsupportedOperationException("Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls.");
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public boolean b(ib7 ib7) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        return "Unconfined";
    }
}
