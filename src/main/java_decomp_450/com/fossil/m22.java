package com.fossil;

import com.fossil.a12;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m22 extends a12 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public m22(String str) {
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.a12
    public i02 a() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public c12<Status> b() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public void c() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public void d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public boolean g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public void a(a12.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public void b(a12.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    @Override // com.fossil.a12
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }
}
