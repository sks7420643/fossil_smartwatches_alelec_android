package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.qx6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lx6<ResultType, RequestType> {
    @DexIgnore
    public boolean isFromCache; // = true;
    @DexIgnore
    public /* final */ xd<qx6<ResultType>> result;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$1", f = "NetworkBoundResource.kt", l = {34, 36}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lx6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$a$a")
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.lx6$a$a  reason: collision with other inner class name */
        public static final class C0114a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LiveData $dbSource;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$a$a$a")
            /* renamed from: com.fossil.lx6$a$a$a  reason: collision with other inner class name */
            public static final class C0115a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ C0114a a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$a$a$a$a")
                /* renamed from: com.fossil.lx6$a$a$a$a  reason: collision with other inner class name */
                public static final class C0116a<T> implements zd<S> {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0115a a;

                    @DexIgnore
                    public C0116a(C0115a aVar) {
                        this.a = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zd
                    public final void onChanged(ResultType resulttype) {
                        this.a.a.this$0.this$0.setValue(qx6.e.c(resulttype));
                    }
                }

                @DexIgnore
                public C0115a(C0114a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.result.a(this.a.$dbSource);
                    if (this.a.this$0.this$0.shouldFetch(resulttype)) {
                        C0114a aVar = this.a;
                        aVar.this$0.this$0.fetchFromNetwork(aVar.$dbSource);
                        return;
                    }
                    this.a.this$0.this$0.result.a(this.a.$dbSource, new C0116a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0114a(a aVar, LiveData liveData, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
                this.$dbSource = liveData;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0114a aVar = new C0114a(this.this$0, this.$dbSource, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((C0114a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.result.a(this.$dbSource, new C0115a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lx6 lx6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = lx6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                lx6 lx6 = this.this$0;
                this.L$0 = yi7;
                this.label = 1;
                obj = lx6.loadFromDb(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                LiveData liveData = (LiveData) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LiveData liveData2 = (LiveData) obj;
            tk7 c = qj7.c();
            C0114a aVar = new C0114a(this, liveData2, null);
            this.L$0 = yi7;
            this.L$1 = liveData2;
            this.label = 2;
            if (vh7.a(c, aVar, this) == a) {
                return a;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1", f = "NetworkBoundResource.kt", l = {58, 65, 68, 76, 76, 79, 85, 88, 98, 111}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $dbSource;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lx6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$b$a$a")
            /* renamed from: com.fossil.lx6$b$a$a  reason: collision with other inner class name */
            public static final class C0117a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;

                @DexIgnore
                public C0117a(a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(qx6.e.b(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.result.a(this.this$0.$dbSource, new C0117a(this));
                    this.this$0.this$0.result.a(this.this$0.$dbSource);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$b$b")
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$2", f = "NetworkBoundResource.kt", l = {71}, m = "invokeSuspend")
        /* renamed from: com.fossil.lx6$b$b  reason: collision with other inner class name */
        public static final class C0118b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lx6$b$b$a")
            /* renamed from: com.fossil.lx6$b$b$a */
            public static final class a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ C0118b a;

                @DexIgnore
                public a(C0118b bVar) {
                    this.a = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(qx6.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0118b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0118b bVar = new C0118b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((C0118b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                xd xdVar;
                Object a2 = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network response null");
                    xd access$getResult$p = this.this$0.this$0.result;
                    lx6 lx6 = this.this$0.this$0;
                    this.L$0 = yi7;
                    this.L$1 = access$getResult$p;
                    this.label = 1;
                    obj = lx6.loadFromDb(this);
                    if (obj == a2) {
                        return a2;
                    }
                    xdVar = access$getResult$p;
                } else if (i == 1) {
                    xdVar = (xd) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                xdVar.a((LiveData) obj, new a(this));
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$3", f = "NetworkBoundResource.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ zi5 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, zi5 zi5, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$response = zi5;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, this.$response, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                lx6 lx6;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi72 = this.p$;
                    lx6 lx62 = this.this$0.this$0;
                    this.L$0 = yi72;
                    this.L$1 = lx62;
                    this.label = 1;
                    Object processResponse = lx62.processResponse((bj5) this.$response, this);
                    if (processResponse == a) {
                        return a;
                    }
                    yi7 = yi72;
                    obj = processResponse;
                    lx6 = lx62;
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                    lx6 = (lx6) this.L$1;
                } else if (i == 2) {
                    yi7 yi73 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = yi7;
                this.label = 2;
                if (lx6.saveCallResult(obj, this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$4", f = "NetworkBoundResource.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ zi5 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, zi5 zi5, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$response = zi5;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, this.$response, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                lx6 lx6;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi72 = this.p$;
                    lx6 lx62 = this.this$0.this$0;
                    this.L$0 = yi72;
                    this.L$1 = lx62;
                    this.label = 1;
                    Object processResponse = lx62.processResponse((bj5) this.$response, this);
                    if (processResponse == a) {
                        return a;
                    }
                    yi7 = yi72;
                    obj = processResponse;
                    lx6 = lx62;
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                    lx6 = (lx6) this.L$1;
                } else if (i == 2) {
                    yi7 yi73 = (yi7) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = yi7;
                this.label = 2;
                if (lx6.saveCallResult(obj, this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$5", f = "NetworkBoundResource.kt", l = {93}, m = "invokeSuspend")
        public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ e a;

                @DexIgnore
                public a(e eVar) {
                    this.a = eVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(qx6.e.c(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                e eVar = new e(this.this$0, fb7);
                eVar.p$ = (yi7) obj;
                return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                xd xdVar;
                Object a2 = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    FLogger.INSTANCE.getLocal().d("NetworkBoundResource", "set value dbSource fetched from network success");
                    xd access$getResult$p = this.this$0.this$0.result;
                    lx6 lx6 = this.this$0.this$0;
                    this.L$0 = yi7;
                    this.L$1 = access$getResult$p;
                    this.label = 1;
                    obj = lx6.loadFromDb(this);
                    if (obj == a2) {
                        return a2;
                    }
                    xdVar = access$getResult$p;
                } else if (i == 1) {
                    xdVar = (xd) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                xdVar.a((LiveData) obj, new a(this));
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$6", f = "NetworkBoundResource.kt", l = {99}, m = "invokeSuspend")
        public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ f a;

                @DexIgnore
                public a(f fVar) {
                    this.a = fVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    this.a.this$0.this$0.setValue(qx6.e.b(resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public f(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                f fVar = new f(this.this$0, fb7);
                fVar.p$ = (yi7) obj;
                return fVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                xd xdVar;
                Object a2 = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xd access$getResult$p = this.this$0.this$0.result;
                    lx6 lx6 = this.this$0.this$0;
                    this.L$0 = yi7;
                    this.L$1 = access$getResult$p;
                    this.label = 1;
                    obj = lx6.loadFromDb(this);
                    if (obj == a2) {
                        return a2;
                    }
                    xdVar = access$getResult$p;
                } else if (i == 1) {
                    xdVar = (xd) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                xdVar.a((LiveData) obj, new a(this));
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$7", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
        public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ zi5 $response;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements zd<S> {
                @DexIgnore
                public /* final */ /* synthetic */ g a;

                @DexIgnore
                public a(g gVar) {
                    this.a = gVar;
                }

                @DexIgnore
                @Override // com.fossil.zd
                public final void onChanged(ResultType resulttype) {
                    String str;
                    g gVar = this.a;
                    lx6 lx6 = gVar.this$0.this$0;
                    qx6.a aVar = qx6.e;
                    int a2 = ((yi5) gVar.$response).a();
                    ServerError c = ((yi5) this.a.$response).c();
                    if (c == null || (str = c.getUserMessage()) == null) {
                        ServerError c2 = ((yi5) this.a.$response).c();
                        str = c2 != null ? c2.getMessage() : null;
                    }
                    if (str == null) {
                        str = "";
                    }
                    lx6.setValue(aVar.a(a2, str, resulttype));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public g(b bVar, zi5 zi5, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$response = zi5;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                g gVar = new g(this.this$0, this.$response, fb7);
                gVar.p$ = (yi7) obj;
                return gVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.result.a(this.this$0.$dbSource, new a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$response$1", f = "NetworkBoundResource.kt", l = {65}, m = "invokeSuspend")
        public static final class h extends zb7 implements gd7<fb7<? super fv7<RequestType>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public h(b bVar, fb7 fb7) {
                super(1, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(fb7<?> fb7) {
                ee7.b(fb7, "completion");
                return new h(this.this$0, fb7);
            }

            @DexIgnore
            @Override // com.fossil.gd7
            public final Object invoke(Object obj) {
                return ((h) create((fb7) obj)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    lx6 lx6 = this.this$0.this$0;
                    this.label = 1;
                    obj = lx6.createCall(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(lx6 lx6, LiveData liveData, fb7 fb7) {
            super(2, fb7);
            this.this$0 = lx6;
            this.$dbSource = liveData;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$dbSource, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0092 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x009a  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00d9 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00da  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0121  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0175  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x018d  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 0
                switch(r1) {
                    case 0: goto L_0x0068;
                    case 1: goto L_0x0060;
                    case 2: goto L_0x0057;
                    case 3: goto L_0x004a;
                    case 4: goto L_0x0039;
                    case 5: goto L_0x002c;
                    case 6: goto L_0x001f;
                    case 7: goto L_0x0012;
                    case 8: goto L_0x004a;
                    case 9: goto L_0x004a;
                    case 10: goto L_0x004a;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0012:
                java.lang.Object r1 = r6.L$1
                com.fossil.zi5 r1 = (com.fossil.zi5) r1
                java.lang.Object r3 = r6.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r7)
                goto L_0x0155
            L_0x001f:
                java.lang.Object r0 = r6.L$1
                com.fossil.zi5 r0 = (com.fossil.zi5) r0
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x0118
            L_0x002c:
                java.lang.Object r1 = r6.L$1
                com.fossil.zi5 r1 = (com.fossil.zi5) r1
                java.lang.Object r3 = r6.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r7)
                goto L_0x00dc
            L_0x0039:
                java.lang.Object r1 = r6.L$2
                com.fossil.lx6 r1 = (com.fossil.lx6) r1
                java.lang.Object r3 = r6.L$1
                com.fossil.zi5 r3 = (com.fossil.zi5) r3
                java.lang.Object r4 = r6.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r7)
                goto L_0x00cc
            L_0x004a:
                java.lang.Object r0 = r6.L$1
                com.fossil.zi5 r0 = (com.fossil.zi5) r0
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x01c2
            L_0x0057:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
            L_0x005e:
                r4 = r1
                goto L_0x0093
            L_0x0060:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0082
            L_0x0068:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r1 = r6.p$
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.fossil.lx6$b$a r3 = new com.fossil.lx6$b$a
                r3.<init>(r6, r2)
                r6.L$0 = r1
                r4 = 1
                r6.label = r4
                java.lang.Object r7 = com.fossil.vh7.a(r7, r3, r6)
                if (r7 != r0) goto L_0x0082
                return r0
            L_0x0082:
                com.fossil.lx6$b$h r7 = new com.fossil.lx6$b$h
                r7.<init>(r6, r2)
                r6.L$0 = r1
                r3 = 2
                r6.label = r3
                java.lang.Object r7 = com.fossil.aj5.a(r7, r6)
                if (r7 != r0) goto L_0x005e
                return r0
            L_0x0093:
                r3 = r7
                com.fossil.zi5 r3 = (com.fossil.zi5) r3
                boolean r7 = r3 instanceof com.fossil.bj5
                if (r7 == 0) goto L_0x018d
                r7 = r3
                com.fossil.bj5 r7 = (com.fossil.bj5) r7
                java.lang.Object r1 = r7.a()
                if (r1 != 0) goto L_0x00ba
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.fossil.lx6$b$b r1 = new com.fossil.lx6$b$b
                r1.<init>(r6, r2)
                r6.L$0 = r4
                r6.L$1 = r3
                r2 = 3
                r6.label = r2
                java.lang.Object r7 = com.fossil.vh7.a(r7, r1, r6)
                if (r7 != r0) goto L_0x01c2
                return r0
            L_0x00ba:
                com.fossil.lx6 r1 = r6.this$0
                r6.L$0 = r4
                r6.L$1 = r3
                r6.L$2 = r1
                r5 = 4
                r6.label = r5
                java.lang.Object r7 = r1.processResponse(r7, r6)
                if (r7 != r0) goto L_0x00cc
                return r0
            L_0x00cc:
                r6.L$0 = r4
                r6.L$1 = r3
                r5 = 5
                r6.label = r5
                java.lang.Object r7 = r1.processContinueFetching(r7, r6)
                if (r7 != r0) goto L_0x00da
                return r0
            L_0x00da:
                r1 = r3
                r3 = r4
            L_0x00dc:
                java.lang.Boolean r7 = (java.lang.Boolean) r7
                boolean r7 = r7.booleanValue()
                if (r7 == 0) goto L_0x0121
                com.fossil.lx6 r7 = r6.this$0
                boolean r7 = r7.isFromCache
                if (r7 == 0) goto L_0x00f8
                com.fossil.lx6 r7 = r6.this$0
                r4 = r1
                com.fossil.bj5 r4 = (com.fossil.bj5) r4
                boolean r4 = r4.b()
                r7.isFromCache = r4
            L_0x00f8:
                r7 = r1
                com.fossil.bj5 r7 = (com.fossil.bj5) r7
                boolean r7 = r7.b()
                if (r7 != 0) goto L_0x0118
                com.fossil.ti7 r7 = com.fossil.qj7.a()
                com.fossil.lx6$b$c r4 = new com.fossil.lx6$b$c
                r4.<init>(r6, r1, r2)
                r6.L$0 = r3
                r6.L$1 = r1
                r1 = 6
                r6.label = r1
                java.lang.Object r7 = com.fossil.vh7.a(r7, r4, r6)
                if (r7 != r0) goto L_0x0118
                return r0
            L_0x0118:
                com.fossil.lx6 r7 = r6.this$0
                androidx.lifecycle.LiveData r0 = r6.$dbSource
                r7.fetchFromNetwork(r0)
                goto L_0x01c2
            L_0x0121:
                com.fossil.lx6 r7 = r6.this$0
                boolean r7 = r7.isFromCache
                if (r7 == 0) goto L_0x0135
                com.fossil.lx6 r7 = r6.this$0
                r4 = r1
                com.fossil.bj5 r4 = (com.fossil.bj5) r4
                boolean r4 = r4.b()
                r7.isFromCache = r4
            L_0x0135:
                r7 = r1
                com.fossil.bj5 r7 = (com.fossil.bj5) r7
                boolean r7 = r7.b()
                if (r7 != 0) goto L_0x0155
                com.fossil.ti7 r7 = com.fossil.qj7.a()
                com.fossil.lx6$b$d r4 = new com.fossil.lx6$b$d
                r4.<init>(r6, r1, r2)
                r6.L$0 = r3
                r6.L$1 = r1
                r5 = 7
                r6.label = r5
                java.lang.Object r7 = com.fossil.vh7.a(r7, r4, r6)
                if (r7 != r0) goto L_0x0155
                return r0
            L_0x0155:
                com.fossil.lx6 r7 = r6.this$0
                boolean r7 = r7.isFromCache
                if (r7 != 0) goto L_0x0175
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.fossil.lx6$b$e r4 = new com.fossil.lx6$b$e
                r4.<init>(r6, r2)
                r6.L$0 = r3
                r6.L$1 = r1
                r1 = 8
                r6.label = r1
                java.lang.Object r7 = com.fossil.vh7.a(r7, r4, r6)
                if (r7 != r0) goto L_0x01c2
                return r0
            L_0x0175:
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.fossil.lx6$b$f r4 = new com.fossil.lx6$b$f
                r4.<init>(r6, r2)
                r6.L$0 = r3
                r6.L$1 = r1
                r1 = 9
                r6.label = r1
                java.lang.Object r7 = com.fossil.vh7.a(r7, r4, r6)
                if (r7 != r0) goto L_0x01c2
                return r0
            L_0x018d:
                boolean r7 = r3 instanceof com.fossil.yi5
                if (r7 == 0) goto L_0x01c2
                com.fossil.lx6 r7 = r6.this$0
                r1 = r3
                com.fossil.yi5 r1 = (com.fossil.yi5) r1
                java.lang.Throwable r1 = r1.d()
                r7.onFetchFailed(r1)
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                java.lang.String r1 = "NetworkBoundResource"
                java.lang.String r5 = "set value dbSource fetched from network failed"
                r7.d(r1, r5)
                com.fossil.tk7 r7 = com.fossil.qj7.c()
                com.fossil.lx6$b$g r1 = new com.fossil.lx6$b$g
                r1.<init>(r6, r3, r2)
                r6.L$0 = r4
                r6.L$1 = r3
                r2 = 10
                r6.label = r2
                java.lang.Object r7 = com.fossil.vh7.a(r7, r1, r6)
                if (r7 != r0) goto L_0x01c2
                return r0
            L_0x01c2:
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
                switch-data {0->0x0068, 1->0x0060, 2->0x0057, 3->0x004a, 4->0x0039, 5->0x002c, 6->0x001f, 7->0x0012, 8->0x004a, 9->0x004a, 10->0x004a, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.lx6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public lx6() {
        xd<qx6<ResultType>> xdVar = new xd<>();
        this.result = xdVar;
        xdVar.b(qx6.e.a(null));
        this.isFromCache = true;
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    private final void fetchFromNetwork(LiveData<ResultType> liveData) {
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, liveData, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ Object processResponse$suspendImpl(lx6 lx6, bj5 bj5, fb7 fb7) {
        Object a2 = bj5.a();
        if (a2 != null) {
            return a2;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    private final void setValue(qx6<? extends ResultType> qx6) {
        if (!ee7.a(this.result.a(), qx6)) {
            this.result.b((qx6<ResultType>) qx6);
        }
    }

    @DexIgnore
    public final LiveData<qx6<ResultType>> asLiveData() {
        xd<qx6<ResultType>> xdVar = this.result;
        if (xdVar != null) {
            return xdVar;
        }
        throw new x87("null cannot be cast to non-null type androidx.lifecycle.LiveData<com.portfolio.platform.util.Resource<ResultType>>");
    }

    @DexIgnore
    public abstract Object createCall(fb7<? super fv7<RequestType>> fb7);

    @DexIgnore
    public abstract Object loadFromDb(fb7<? super LiveData<ResultType>> fb7);

    @DexIgnore
    public void onFetchFailed(Throwable th) {
    }

    @DexIgnore
    public Object processContinueFetching(RequestType requesttype, fb7<? super Boolean> fb7) {
        return pb7.a(false);
    }

    @DexIgnore
    public Object processResponse(bj5<RequestType> bj5, fb7<? super RequestType> fb7) {
        return processResponse$suspendImpl(this, bj5, fb7);
    }

    @DexIgnore
    public abstract Object saveCallResult(RequestType requesttype, fb7<? super i97> fb7);

    @DexIgnore
    public abstract boolean shouldFetch(ResultType resulttype);
}
