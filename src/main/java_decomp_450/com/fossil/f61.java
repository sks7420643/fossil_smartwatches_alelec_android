package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f61 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.AUTHENTICATION}));
    @DexIgnore
    public /* final */ long D;

    @DexIgnore
    public f61(ri1 ri1, en0 en0, long j) {
        super(ri1, en0, wm0.z0, null, false, 24);
        this.D = j;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        long j = this.D;
        if (j >= 0) {
            de7 de7 = de7.a;
            if (j <= 4294967295L) {
                zk0.a(this, new pj0(((zk0) this).w, j), new zt0(this), new tv0(this), (kd7) null, lx0.a, dz0.a, 8, (Object) null);
                return;
            }
        }
        a(is0.INVALID_PARAMETER);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.K4, Long.valueOf(this.D));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean a(v81 v81) {
        return v81 instanceof br0;
    }
}
