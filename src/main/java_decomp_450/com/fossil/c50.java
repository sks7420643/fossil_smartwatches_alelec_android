package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c50<R> extends q30 {
    @DexIgnore
    n40 a();

    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    void a(b50 b50);

    @DexIgnore
    void a(n40 n40);

    @DexIgnore
    void a(R r, f50<? super R> f50);

    @DexIgnore
    void b(Drawable drawable);

    @DexIgnore
    void b(b50 b50);

    @DexIgnore
    void c(Drawable drawable);
}
