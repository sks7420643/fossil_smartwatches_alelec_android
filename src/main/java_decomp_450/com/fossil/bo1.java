package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo1 implements Parcelable.Creator<bq1> {
    @DexIgnore
    public /* synthetic */ bo1(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public bq1 createFromParcel(Parcel parcel) {
        return new bq1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public bq1[] newArray(int i) {
        return new bq1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public bq1 m4createFromParcel(Parcel parcel) {
        return new bq1(parcel, null);
    }
}
