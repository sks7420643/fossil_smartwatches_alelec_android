package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp2 extends bw2<dp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ dp2 zzj;
    @DexIgnore
    public static volatile wx2<dp2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public bp2 zzf;
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;
    @DexIgnore
    public boolean zzi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<dp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(dp2.zzj);
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((dp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(yo2 yo2) {
            this();
        }
    }

    /*
    static {
        dp2 dp2 = new dp2();
        zzj = dp2;
        bw2.a(dp2.class, dp2);
    }
    */

    @DexIgnore
    public static a w() {
        return (a) zzj.e();
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final int p() {
        return this.zzd;
    }

    @DexIgnore
    public final String q() {
        return this.zze;
    }

    @DexIgnore
    public final bp2 r() {
        bp2 bp2 = this.zzf;
        return bp2 == null ? bp2.w() : bp2;
    }

    @DexIgnore
    public final boolean s() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean t() {
        return this.zzh;
    }

    @DexIgnore
    public final boolean u() {
        return (this.zzc & 32) != 0;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new dp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1008\u0001\u0003\u1009\u0002\u0004\u1007\u0003\u0005\u1007\u0004\u0006\u1007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                wx2<dp2> wx2 = zzk;
                if (wx2 == null) {
                    synchronized (dp2.class) {
                        wx2 = zzk;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzj);
                            zzk = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
