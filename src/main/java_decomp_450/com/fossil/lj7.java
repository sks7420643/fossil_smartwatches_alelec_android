package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj7<T> extends oj7<T> implements sb7, fb7<T> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater i; // = AtomicReferenceFieldUpdater.newUpdater(lj7.class, Object.class, "_reusableCancellableContinuation");
    @DexIgnore
    public volatile Object _reusableCancellableContinuation;
    @DexIgnore
    public Object d; // = mj7.a;
    @DexIgnore
    public /* final */ sb7 e;
    @DexIgnore
    public /* final */ Object f;
    @DexIgnore
    public /* final */ ti7 g;
    @DexIgnore
    public /* final */ fb7<T> h;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.fb7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public lj7(ti7 ti7, fb7<? super T> fb7) {
        super(0);
        this.g = ti7;
        this.h = fb7;
        fb7<T> fb72 = this.h;
        this.e = (sb7) (!(fb72 instanceof sb7) ? null : fb72);
        this.f = pm7.a(getContext());
        this._reusableCancellableContinuation = null;
    }

    @DexIgnore
    @Override // com.fossil.oj7
    public fb7<T> a() {
        return this;
    }

    @DexIgnore
    public final void a(ib7 ib7, T t) {
        this.d = t;
        ((oj7) this).c = 1;
        this.g.b(ib7, this);
    }

    @DexIgnore
    @Override // com.fossil.oj7
    public Object b() {
        Object obj = this.d;
        if (dj7.a()) {
            if (!(obj != mj7.a)) {
                throw new AssertionError();
            }
        }
        this.d = mj7.a;
        return obj;
    }

    @DexIgnore
    public final bi7<T> c() {
        Object obj;
        do {
            obj = this._reusableCancellableContinuation;
            if (obj == null) {
                this._reusableCancellableContinuation = mj7.b;
                return null;
            } else if (!(obj instanceof bi7)) {
                throw new IllegalStateException(("Inconsistent state " + obj).toString());
            }
        } while (!i.compareAndSet(this, obj, mj7.b));
        return (bi7) obj;
    }

    @DexIgnore
    public final bi7<?> d() {
        Object obj = this._reusableCancellableContinuation;
        if (!(obj instanceof bi7)) {
            obj = null;
        }
        return (bi7) obj;
    }

    @DexIgnore
    public final boolean e() {
        return this._reusableCancellableContinuation != null;
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public sb7 getCallerFrame() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        return this.h.getContext();
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public void resumeWith(Object obj) {
        ib7 context = this.h.getContext();
        Object a = mi7.a(obj);
        if (this.g.b(context)) {
            this.d = a;
            ((oj7) this).c = 0;
            this.g.a(context, this);
            return;
        }
        vj7 b = fl7.b.b();
        if (b.k()) {
            this.d = a;
            ((oj7) this).c = 0;
            b.a((oj7<?>) this);
            return;
        }
        b.c(true);
        try {
            ib7 context2 = getContext();
            Object b2 = pm7.b(context2, this.f);
            try {
                this.h.resumeWith(obj);
                i97 i97 = i97.a;
                do {
                } while (b.o());
            } finally {
                pm7.a(context2, b2);
            }
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.g + ", " + ej7.a((fb7<?>) this.h) + ']';
    }

    @DexIgnore
    public final Throwable a(ai7<?> ai7) {
        lm7 lm7;
        do {
            Object obj = this._reusableCancellableContinuation;
            lm7 = mj7.b;
            if (obj != lm7) {
                if (obj == null) {
                    return null;
                }
                if (!(obj instanceof Throwable)) {
                    throw new IllegalStateException(("Inconsistent state " + obj).toString());
                } else if (i.compareAndSet(this, obj, null)) {
                    return (Throwable) obj;
                } else {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            }
        } while (!i.compareAndSet(this, lm7, ai7));
        return null;
    }

    @DexIgnore
    public final boolean a(Throwable th) {
        while (true) {
            Object obj = this._reusableCancellableContinuation;
            if (ee7.a(obj, mj7.b)) {
                if (i.compareAndSet(this, mj7.b, th)) {
                    return true;
                }
            } else if (obj instanceof Throwable) {
                return true;
            } else {
                if (i.compareAndSet(this, obj, null)) {
                    return false;
                }
            }
        }
    }
}
