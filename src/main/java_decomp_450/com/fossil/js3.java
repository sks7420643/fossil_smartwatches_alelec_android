package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class js3 extends cv3 implements Cloneable {
    @DexIgnore
    public float a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;

    @DexIgnore
    public js3(float f, float f2, float f3) {
        this.b = f;
        this.a = f2;
        this.d = f3;
        if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            return;
        }
        throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
    }

    @DexIgnore
    @Override // com.fossil.cv3
    public void a(float f, float f2, float f3, jv3 jv3) {
        float f4 = this.c;
        if (f4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            jv3.a(f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f5 = ((this.b * 2.0f) + f4) / 2.0f;
        float f6 = f3 * this.a;
        float f7 = f2 + this.e;
        float f8 = (this.d * f3) + ((1.0f - f3) * f5);
        if (f8 / f5 >= 1.0f) {
            jv3.a(f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f9 = f5 + f6;
        float f10 = f8 + f6;
        float sqrt = (float) Math.sqrt((double) ((f9 * f9) - (f10 * f10)));
        float f11 = f7 - sqrt;
        float f12 = f7 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan((double) (sqrt / f10)));
        float f13 = 90.0f - degrees;
        jv3.a(f11, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float f14 = f6 * 2.0f;
        jv3.a(f11 - f6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f11 + f6, f14, 270.0f, degrees);
        jv3.a(f7 - f5, (-f5) - f8, f7 + f5, f5 - f8, 180.0f - f13, (f13 * 2.0f) - 180.0f);
        jv3.a(f12 - f6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12 + f6, f14, 270.0f - degrees, degrees);
        jv3.a(f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void b(float f) {
        this.b = f;
    }

    @DexIgnore
    public void c(float f) {
        this.a = f;
    }

    @DexIgnore
    public void d(float f) {
        this.c = f;
    }

    @DexIgnore
    public void e(float f) {
        this.e = f;
    }

    @DexIgnore
    public float f() {
        return this.b;
    }

    @DexIgnore
    public float h() {
        return this.a;
    }

    @DexIgnore
    public float i() {
        return this.c;
    }

    @DexIgnore
    public float j() {
        return this.e;
    }

    @DexIgnore
    public float d() {
        return this.d;
    }

    @DexIgnore
    public void a(float f) {
        this.d = f;
    }
}
