package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vf5 {
    @DexIgnore
    public Vector<zf5> a;
    @DexIgnore
    public Map<String, zf5> b;
    @DexIgnore
    public Map<String, zf5> c;
    @DexIgnore
    public Map<String, zf5> d;
    @DexIgnore
    public Map<String, zf5> e;

    @DexIgnore
    public vf5() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public boolean a(zf5 zf5) {
        if (zf5 != null) {
            b(zf5);
            return this.a.add(zf5);
        }
        throw null;
    }

    @DexIgnore
    public final void b(zf5 zf5) {
        byte[] a2 = zf5.a();
        if (a2 != null) {
            this.b.put(new String(a2), zf5);
        }
        byte[] b2 = zf5.b();
        if (b2 != null) {
            this.c.put(new String(b2), zf5);
        }
        byte[] f = zf5.f();
        if (f != null) {
            this.d.put(new String(f), zf5);
        }
        byte[] e2 = zf5.e();
        if (e2 != null) {
            this.e.put(new String(e2), zf5);
        }
    }

    @DexIgnore
    public void a(int i, zf5 zf5) {
        if (zf5 != null) {
            b(zf5);
            this.a.add(i, zf5);
            return;
        }
        throw null;
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }

    @DexIgnore
    public zf5 a(int i) {
        return this.a.get(i);
    }
}
