package com.fossil;

import android.os.Bundle;
import com.fossil.v02;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class my1 {
    @DexIgnore
    public static /* final */ v02.g<lf2> a; // = new v02.g<>();
    @DexIgnore
    public static /* final */ v02.g<dz1> b; // = new v02.g<>();
    @DexIgnore
    public static /* final */ v02.a<lf2, a> c; // = new b02();
    @DexIgnore
    public static /* final */ v02.a<dz1, GoogleSignInOptions> d; // = new c02();
    @DexIgnore
    public static /* final */ v02<GoogleSignInOptions> e; // = new v02<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ ry1 f; // = new cz1();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a implements v02.d.f {
        @DexIgnore
        public /* final */ boolean a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.my1$a$a")
        @Deprecated
        /* renamed from: com.fossil.my1$a$a  reason: collision with other inner class name */
        public static class C0131a {
            @DexIgnore
            public Boolean a; // = false;

            @DexIgnore
            public a a() {
                return new a(this);
            }
        }

        /*
        static {
            new C0131a().a();
        }
        */

        @DexIgnore
        public a(C0131a aVar) {
            this.a = aVar.a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", null);
            bundle.putBoolean("force_save_dialog", this.a);
            return bundle;
        }
    }

    /*
    static {
        v02<oy1> v02 = ny1.c;
        new v02("Auth.CREDENTIALS_API", c, a);
        py1 py1 = ny1.d;
        new kf2();
    }
    */
}
