package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import com.fossil.cy6;
import com.fossil.el4;
import com.fossil.ui6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri6 extends ho5 implements cy6.g {
    @DexIgnore
    public static /* final */ /* synthetic */ zf7[] s;
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public rj4 g;
    @DexIgnore
    public ui6 h;
    @DexIgnore
    public qw6<i55> i;
    @DexIgnore
    public /* final */ InputMethodManager j;
    @DexIgnore
    public /* final */ df7 p;
    @DexIgnore
    public gb5 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ri6.t;
        }

        @DexIgnore
        public final ri6 b() {
            return new ri6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<dx6<? extends ui6.e>> {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public b(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(dx6<? extends ui6.e> dx6) {
            ui6.e eVar = (ui6.e) dx6.a();
            if (eVar == null) {
                return;
            }
            if (eVar instanceof ui6.e.a) {
                ri6 ri6 = this.a;
                ri6.a(ri6.q);
            } else if (eVar instanceof ui6.e.c) {
                this.a.l1();
            } else if (eVar instanceof ui6.e.d) {
                ui6.e.d dVar = (ui6.e.d) eVar;
                this.a.f(dVar.a(), dVar.b());
            } else if (eVar instanceof ui6.e.b) {
                this.a.g1();
            } else if (eVar instanceof ui6.e.C0199e) {
                ri6 ri62 = this.a;
                Object[] array = ((ui6.e.C0199e) eVar).a().toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    ri62.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<el4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public c(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(el4.a aVar) {
            if (aVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = ri6.u.a();
                local.d(a2, "loadingState start " + aVar.a() + " stop " + aVar.b());
                if (aVar.a()) {
                    this.a.b();
                }
                if (aVar.b()) {
                    this.a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 b;

        @DexIgnore
        public d(i55 i55, ri6 ri6) {
            this.a = i55;
            this.b = ri6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                if (editable.length() > 0) {
                    int a2 = re5.a(editable.toString());
                    if (a2 <= 9 && editable.length() > 1) {
                        this.a.y.setText(String.valueOf(a2));
                    }
                    this.b.n(a2);
                    return;
                }
                this.a.y.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence != null) {
                if (charSequence.length() > 0) {
                    this.a.y.removeTextChangedListener(this);
                    String obj = charSequence.toString();
                    if (nh7.a((CharSequence) obj, (CharSequence) ",", false, 2, (Object) null)) {
                        obj = new ah7(",").replace(obj, "");
                    }
                    this.a.y.setText(re5.d(Integer.parseInt(obj)));
                    FlexibleEditText flexibleEditText = this.a.y;
                    ee7.a((Object) flexibleEditText, "binding.fetGoalsValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        flexibleEditText.setSelection(text.length());
                        this.a.y.addTextChangedListener(this);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                this.a.y.setText(String.valueOf(0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 b;

        @DexIgnore
        public e(i55 i55, ri6 ri6) {
            this.a = i55;
            this.b = ri6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.a.z.setText(String.valueOf(parseInt));
                    }
                    if (parseInt > 16) {
                        this.a.z.setText(String.valueOf(16));
                        bx6 bx6 = bx6.c;
                        FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                        ee7.a((Object) childFragmentManager, "childFragmentManager");
                        bx6.a(childFragmentManager, this.b.q, 16);
                        parseInt = 16;
                    }
                    ri6 ri6 = this.b;
                    FlexibleEditText flexibleEditText = this.a.A;
                    ee7.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        ee7.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                        if (text.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            FlexibleEditText flexibleEditText2 = this.a.A;
                            ee7.a((Object) flexibleEditText2, "binding.fetSleepMinuteValue");
                            i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                        }
                        ri6.b(this.b).a(ri6.e(i, parseInt), this.b.q);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                this.a.z.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 b;

        @DexIgnore
        public f(i55 i55, ri6 ri6) {
            this.a = i55;
            this.b = ri6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.a.A.setText(String.valueOf(parseInt));
                    }
                    if (this.b.q == gb5.TOTAL_SLEEP) {
                        if (parseInt > 59) {
                            this.a.A.setText(String.valueOf(59));
                            parseInt = 59;
                        }
                        ri6 ri6 = this.b;
                        FlexibleEditText flexibleEditText = this.a.A;
                        ee7.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                        Editable text = flexibleEditText.getText();
                        if (text != null) {
                            ee7.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                            if (text.length() <= 0) {
                                z = false;
                            }
                            if (z) {
                                FlexibleEditText flexibleEditText2 = this.a.z;
                                ee7.a((Object) flexibleEditText2, "binding.fetSleepHourValue");
                                i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                            }
                            parseInt = ri6.e(parseInt, i);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    ri6.b(this.b).a(parseInt, this.b.q);
                    return;
                }
                this.a.A.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 b;

        @DexIgnore
        public g(i55 i55, ri6 ri6) {
            this.a = i55;
            this.b = ri6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (this.b.q == gb5.TOTAL_SLEEP) {
                ri6 ri6 = this.b;
                FlexibleEditText flexibleEditText = this.a.A;
                ee7.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                ri6.e(z, flexibleEditText.isFocused());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 b;

        @DexIgnore
        public h(i55 i55, ri6 ri6) {
            this.a = i55;
            this.b = ri6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            ri6 ri6 = this.b;
            FlexibleEditText flexibleEditText = this.a.z;
            ee7.a((Object) flexibleEditText, "binding.fetSleepHourValue");
            ri6.e(flexibleEditText.isFocused(), z);
            if (z) {
                FlexibleEditText flexibleEditText2 = this.a.z;
                ee7.a((Object) flexibleEditText2, "binding.fetSleepHourValue");
                if (Integer.parseInt(String.valueOf(flexibleEditText2.getText())) == 16) {
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    bx6.a(childFragmentManager, gb5.TOTAL_SLEEP, 16);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<ui6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;

        @DexIgnore
        public i(i55 i55) {
            this.a = i55;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ui6.c cVar) {
            this.a.v.setValue(cVar.g());
            this.a.w.setValue(cVar.a());
            this.a.t.setValue(cVar.c());
            this.a.u.setValue(cVar.f());
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.s;
            ee7.a((Object) flexibleSwitchCompat, "binding.cbEnableGoalRing");
            flexibleSwitchCompat.setChecked(cVar.d());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<ui6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;

        @DexIgnore
        public j(i55 i55) {
            this.a = i55;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ui6.c cVar) {
            FLogger.INSTANCE.getLocal().d("Brown", cVar.toString());
            this.a.v.setValue(cVar.g());
            this.a.w.setValue(cVar.c());
            this.a.t.setValue(cVar.f());
            this.a.u.setValue(cVar.e());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public k(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public l(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ri6 ri6 = this.a;
            if (view != null) {
                ri6.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public m(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ri6 ri6 = this.a;
            if (view != null) {
                ri6.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public n(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ri6 ri6 = this.a;
            if (view != null) {
                ri6.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public o(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ri6 ri6 = this.a;
            if (view != null) {
                ri6.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ri6 a;

        @DexIgnore
        public p(ri6 ri6) {
            this.a = ri6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ri6.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ i55 a;

        @DexIgnore
        public q(i55 i55) {
            this.a = i55;
        }

        @DexIgnore
        public final void run() {
            this.a.N.fullScroll(130);
        }
    }

    /*
    static {
        ie7 ie7 = new ie7(te7.a(ri6.class), "mIsDiana", "getMIsDiana()Z");
        te7.a(ie7);
        s = new zf7[]{ie7};
        String simpleName = ri6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileGoalEditFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public ri6() {
        Object systemService = PortfolioApp.g0.c().getSystemService("input_method");
        if (systemService != null) {
            this.j = (InputMethodManager) systemService;
            this.p = bf7.a.a();
            this.q = gb5.TOTAL_STEPS;
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public static final /* synthetic */ ui6 b(ri6 ri6) {
        ui6 ui6 = ri6.h;
        if (ui6 != null) {
            return ui6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        this.p.a(this, s[0], Boolean.valueOf(z));
    }

    @DexIgnore
    public final void U(boolean z) {
        FLogger.INSTANCE.getLocal().d(t, "showSleepGoalEdit");
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.g0.c();
                if (z) {
                    LinearLayout linearLayout = a2.L;
                    ee7.a((Object) linearLayout, "llSleepGoalValue");
                    linearLayout.setVisibility(0);
                    LinearLayout linearLayout2 = a2.K;
                    ee7.a((Object) linearLayout2, "llGoalsValue");
                    linearLayout2.setVisibility(8);
                    a2.z.setTextColor(Color.parseColor(eh5.l.a().b("dianaSleepTab")));
                    a2.A.setTextColor(v6.a(c2, 2131099764));
                    FlexibleEditText flexibleEditText = a2.z;
                    ee7.a((Object) flexibleEditText, "fetSleepHourValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleEditText flexibleEditText2 = a2.A;
                    ee7.a((Object) flexibleEditText2, "fetSleepMinuteValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleTextView flexibleTextView = a2.G;
                    ee7.a((Object) flexibleTextView, "ftvSleepHourUnit");
                    flexibleTextView.setText(getString(2131887062));
                    FlexibleTextView flexibleTextView2 = a2.H;
                    ee7.a((Object) flexibleTextView2, "ftvSleepMinuteUnit");
                    flexibleTextView2.setText(getString(2131887063));
                    int mValue = (f1() ? a2.u : a2.t).getMValue() / 60;
                    CustomEditGoalView customEditGoalView = f1() ? a2.u : a2.t;
                    a2.z.setText(String.valueOf(mValue));
                    a2.A.setText(String.valueOf(customEditGoalView.getMValue() % 60));
                    a(a2.z);
                } else {
                    LinearLayout linearLayout3 = a2.L;
                    ee7.a((Object) linearLayout3, "llSleepGoalValue");
                    linearLayout3.setVisibility(8);
                    LinearLayout linearLayout4 = a2.K;
                    ee7.a((Object) linearLayout4, "llGoalsValue");
                    linearLayout4.setVisibility(0);
                    a(a2.y);
                }
                a2.N.post(new q(a2));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void e(boolean z, boolean z2) {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                String b2 = eh5.l.a().b("dianaSleepTab");
                String b3 = eh5.l.a().b("nonBrandCoolGray");
                int parseColor = Color.parseColor(b2);
                int parseColor2 = Color.parseColor(b3);
                if (z) {
                    a2.z.setTextColor(parseColor);
                    a2.A.setTextColor(parseColor2);
                } else if (z2) {
                    a2.z.setTextColor(parseColor2);
                    a2.A.setTextColor(parseColor);
                } else {
                    a2.z.setTextColor(parseColor);
                    a2.A.setTextColor(parseColor);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        if (getActivity() == null) {
            return true;
        }
        FragmentActivity requireActivity = requireActivity();
        ee7.a((Object) requireActivity, "requireActivity()");
        if (requireActivity.isFinishing()) {
            return true;
        }
        FragmentActivity requireActivity2 = requireActivity();
        ee7.a((Object) requireActivity2, "requireActivity()");
        if (requireActivity2.isDestroyed()) {
            return true;
        }
        ui6 ui6 = this.h;
        if (ui6 != null) {
            ui6.k();
            return true;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void f(int i2, String str) {
        if (isActive()) {
            a();
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public final boolean f1() {
        return ((Boolean) this.p.a(this, s[0])).booleanValue();
    }

    @DexIgnore
    public final void g1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void h1() {
        ui6 ui6 = this.h;
        if (ui6 != null) {
            ui6.f().a(getViewLifecycleOwner(), new b(this));
            ui6 ui62 = this.h;
            if (ui62 != null) {
                ui62.b().a(getViewLifecycleOwner(), new c(this));
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void i1() {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                a2.y.addTextChangedListener(new d(a2, this));
                a2.z.addTextChangedListener(new e(a2, this));
                a2.A.addTextChangedListener(new f(a2, this));
                FlexibleEditText flexibleEditText = a2.z;
                ee7.a((Object) flexibleEditText, "binding.fetSleepHourValue");
                flexibleEditText.setOnFocusChangeListener(new g(a2, this));
                FlexibleEditText flexibleEditText2 = a2.A;
                ee7.a((Object) flexibleEditText2, "binding.fetSleepMinuteValue");
                flexibleEditText2.setOnFocusChangeListener(new h(a2, this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j1() {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                String b2 = eh5.l.a().b("nonBrandSurface");
                if (b2 != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                }
                if (f1()) {
                    ConstraintLayout constraintLayout2 = a2.x;
                    ee7.a((Object) constraintLayout2, "binding.clEnableGoalRing");
                    constraintLayout2.setVisibility(0);
                    a2.v.setType(gb5.TOTAL_STEPS);
                    a2.w.setType(gb5.ACTIVE_TIME);
                    a2.t.setType(gb5.CALORIES);
                    a2.u.setType(gb5.TOTAL_SLEEP);
                    ui6 ui6 = this.h;
                    if (ui6 != null) {
                        ui6.g().a(getViewLifecycleOwner(), new i(a2));
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                } else {
                    ConstraintLayout constraintLayout3 = a2.x;
                    ee7.a((Object) constraintLayout3, "binding.clEnableGoalRing");
                    constraintLayout3.setVisibility(8);
                    a2.v.setType(gb5.TOTAL_STEPS);
                    a2.w.setType(gb5.CALORIES);
                    a2.t.setType(gb5.TOTAL_SLEEP);
                    a2.u.setType(gb5.GOAL_TRACKING);
                    ui6 ui62 = this.h;
                    if (ui62 != null) {
                        ui62.g().a(getViewLifecycleOwner(), new j(a2));
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void k1() {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                a2.J.setOnClickListener(new k(this));
                a2.v.setOnClickListener(new l(this));
                a2.w.setOnClickListener(new m(this));
                a2.t.setOnClickListener(new n(this));
                a2.u.setOnClickListener(new o(this));
                a2.s.setOnCheckedChangeListener(new p(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void l1() {
        if (isActive()) {
            a();
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.V(childFragmentManager);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (r10 <= 999) goto L_0x0042;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void n(int r10) {
        /*
            r9 = this;
            com.fossil.qw6<com.fossil.i55> r0 = r9.i
            r1 = 0
            if (r0 == 0) goto L_0x007d
            java.lang.Object r0 = r0.a()
            com.fossil.i55 r0 = (com.fossil.i55) r0
            if (r0 == 0) goto L_0x007c
            com.fossil.gb5 r2 = r9.q
            int[] r3 = com.fossil.si6.b
            int r2 = r2.ordinal()
            r2 = r3[r2]
            r3 = 999(0x3e7, float:1.4E-42)
            r4 = 480(0x1e0, float:6.73E-43)
            r5 = 4800(0x12c0, float:6.726E-42)
            r6 = 50000(0xc350, float:7.0065E-41)
            r7 = 0
            r8 = 1
            if (r2 == r8) goto L_0x003b
            r6 = 2
            if (r2 == r6) goto L_0x0036
            r5 = 3
            if (r2 == r5) goto L_0x0031
            r4 = 4
            if (r2 == r4) goto L_0x002e
            goto L_0x0042
        L_0x002e:
            if (r10 <= r3) goto L_0x0042
            goto L_0x0040
        L_0x0031:
            if (r10 <= r4) goto L_0x0042
            r3 = 480(0x1e0, float:6.73E-43)
            goto L_0x0040
        L_0x0036:
            if (r10 <= r5) goto L_0x0042
            r3 = 4800(0x12c0, float:6.726E-42)
            goto L_0x0040
        L_0x003b:
            if (r10 <= r6) goto L_0x0042
            r3 = 50000(0xc350, float:7.0065E-41)
        L_0x0040:
            r7 = 1
            goto L_0x0043
        L_0x0042:
            r3 = 0
        L_0x0043:
            java.lang.String r2 = "mViewModel"
            if (r7 == 0) goto L_0x006e
            com.fossil.ui6 r10 = r9.h
            if (r10 == 0) goto L_0x006a
            com.fossil.gb5 r1 = r9.q
            r10.a(r3, r1)
            com.portfolio.platform.view.FlexibleEditText r10 = r0.y
            java.lang.String r0 = com.fossil.re5.d(r3)
            r10.setText(r0)
            com.fossil.bx6 r10 = com.fossil.bx6.c
            androidx.fragment.app.FragmentManager r0 = r9.getChildFragmentManager()
            java.lang.String r1 = "childFragmentManager"
            com.fossil.ee7.a(r0, r1)
            com.fossil.gb5 r1 = r9.q
            r10.a(r0, r1, r3)
            goto L_0x007c
        L_0x006a:
            com.fossil.ee7.d(r2)
            throw r1
        L_0x006e:
            com.fossil.ui6 r0 = r9.h
            if (r0 == 0) goto L_0x0078
            com.fossil.gb5 r1 = r9.q
            r0.a(r10, r1)
            goto L_0x007c
        L_0x0078:
            com.fossil.ee7.d(r2)
            throw r1
        L_0x007c:
            return
        L_0x007d:
            java.lang.String r10 = "mBinding"
            com.fossil.ee7.d(r10)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ri6.n(int):void");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        i55 i55 = (i55) qb.a(layoutInflater, 2131558610, viewGroup, false, a1());
        PortfolioApp.g0.c().f().x().a(this);
        rj4 rj4 = this.g;
        if (rj4 != null) {
            he a2 = new ViewModelProvider(this, rj4).a(ui6.class);
            ee7.a((Object) a2, "ViewModelProvider(this, \u2026ditViewModel::class.java)");
            this.h = (ui6) a2;
            this.i = new qw6<>(this, i55);
            ui6 ui6 = this.h;
            if (ui6 != null) {
                T(ui6.i());
                j1();
                k1();
                i1();
                h1();
                LinearLayout linearLayout = i55.K;
                ee7.a((Object) linearLayout, "binding.llGoalsValue");
                linearLayout.setVisibility(4);
                LinearLayout linearLayout2 = i55.L;
                ee7.a((Object) linearLayout2, "binding.llSleepGoalValue");
                linearLayout2.setVisibility(4);
                V("set_goal_view");
                ee7.a((Object) i55, "binding");
                return i55.d();
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d(t, "onResume");
        super.onResume();
        jf5 c1 = c1();
        if (c1 != null) {
            c1.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        FLogger.INSTANCE.getLocal().d(t, "onStop");
        super.onStop();
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
    }

    @DexIgnore
    public final void b(gb5 gb5) {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null && gb5 != null) {
                int i2 = si6.a[gb5.ordinal()];
                if (i2 == 1) {
                    FlexibleTextView flexibleTextView = a2.E;
                    ee7.a((Object) flexibleTextView, "ftvGoalTitle");
                    flexibleTextView.setText(getString(2131886584));
                    FlexibleTextView flexibleTextView2 = a2.B;
                    ee7.a((Object) flexibleTextView2, "ftvDesc");
                    flexibleTextView2.setText(getString(2131887058));
                    String b2 = eh5.l.a().b("dianaActiveCaloriesTab");
                    if (b2 != null) {
                        a2.y.setTextColor(Color.parseColor(b2));
                    }
                    FlexibleEditText flexibleEditText = a2.y;
                    ee7.a((Object) flexibleEditText, "fetGoalsValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.y.setText(String.valueOf((f1() ? a2.t : a2.w).getValue()));
                    FlexibleTextView flexibleTextView3 = a2.F;
                    ee7.a((Object) flexibleTextView3, "ftvGoalsUnit");
                    flexibleTextView3.setVisibility(8);
                    U(false);
                } else if (i2 == 2) {
                    FlexibleTextView flexibleTextView4 = a2.E;
                    ee7.a((Object) flexibleTextView4, "ftvGoalTitle");
                    flexibleTextView4.setText(getString(2131886592));
                    FlexibleTextView flexibleTextView5 = a2.B;
                    ee7.a((Object) flexibleTextView5, "ftvDesc");
                    flexibleTextView5.setText(getString(2131887060));
                    String b3 = eh5.l.a().b("dianaActiveMinutesTab");
                    if (b3 != null) {
                        a2.y.setTextColor(Color.parseColor(b3));
                    }
                    FlexibleEditText flexibleEditText2 = a2.y;
                    ee7.a((Object) flexibleEditText2, "fetGoalsValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(5)});
                    a2.y.setText(String.valueOf(a2.w.getValue()));
                    FlexibleTextView flexibleTextView6 = a2.F;
                    ee7.a((Object) flexibleTextView6, "ftvGoalsUnit");
                    flexibleTextView6.setVisibility(8);
                    U(false);
                } else if (i2 == 3) {
                    FlexibleTextView flexibleTextView7 = a2.E;
                    ee7.a((Object) flexibleTextView7, "ftvGoalTitle");
                    flexibleTextView7.setText(getString(2131887070));
                    FlexibleTextView flexibleTextView8 = a2.B;
                    ee7.a((Object) flexibleTextView8, "ftvDesc");
                    flexibleTextView8.setText(getString(2131887071));
                    String b4 = eh5.l.a().b("hybridStepsTab");
                    if (b4 != null) {
                        a2.y.setTextColor(Color.parseColor(b4));
                    }
                    FlexibleEditText flexibleEditText3 = a2.y;
                    ee7.a((Object) flexibleEditText3, "fetGoalsValue");
                    flexibleEditText3.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.y.setText(String.valueOf(a2.v.getValue()));
                    FlexibleTextView flexibleTextView9 = a2.F;
                    ee7.a((Object) flexibleTextView9, "ftvGoalsUnit");
                    flexibleTextView9.setVisibility(8);
                    U(false);
                } else if (i2 == 4) {
                    FlexibleTextView flexibleTextView10 = a2.E;
                    ee7.a((Object) flexibleTextView10, "ftvGoalTitle");
                    flexibleTextView10.setText(getString(2131886716));
                    FlexibleTextView flexibleTextView11 = a2.B;
                    ee7.a((Object) flexibleTextView11, "ftvDesc");
                    flexibleTextView11.setText(getString(2131887064));
                    String b5 = eh5.l.a().b("dianaSleepTab");
                    if (b5 != null) {
                        a2.y.setTextColor(Color.parseColor(b5));
                    }
                    U(true);
                } else if (i2 == 5) {
                    FlexibleTextView flexibleTextView12 = a2.E;
                    ee7.a((Object) flexibleTextView12, "ftvGoalTitle");
                    flexibleTextView12.setText(getString(2131886690));
                    FlexibleTextView flexibleTextView13 = a2.B;
                    ee7.a((Object) flexibleTextView13, "ftvDesc");
                    flexibleTextView13.setText(getString(2131887076));
                    String b6 = eh5.l.a().b("hybridGoalTrackingTab");
                    if (b6 != null) {
                        a2.y.setTextColor(Color.parseColor(b6));
                    }
                    FlexibleEditText flexibleEditText4 = a2.y;
                    ee7.a((Object) flexibleEditText4, "fetGoalsValue");
                    flexibleEditText4.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.y.setText(String.valueOf(a2.u.getValue()));
                    FlexibleTextView flexibleTextView14 = a2.F;
                    ee7.a((Object) flexibleTextView14, "ftvGoalsUnit");
                    flexibleTextView14.setVisibility(0);
                    FlexibleTextView flexibleTextView15 = a2.F;
                    ee7.a((Object) flexibleTextView15, "ftvGoalsUnit");
                    flexibleTextView15.setText(getString(2131887075));
                    U(false);
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(gb5 gb5) {
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 != null) {
                if (gb5 != null) {
                    this.q = gb5;
                }
                CustomEditGoalView customEditGoalView = a2.v;
                ee7.a((Object) customEditGoalView, "cegvTopLeft");
                boolean z = true;
                customEditGoalView.setSelected(a2.v.getMGoalType() == gb5);
                CustomEditGoalView customEditGoalView2 = a2.w;
                ee7.a((Object) customEditGoalView2, "cegvTopRight");
                customEditGoalView2.setSelected(a2.w.getMGoalType() == gb5);
                CustomEditGoalView customEditGoalView3 = a2.t;
                ee7.a((Object) customEditGoalView3, "cegvBottomLeft");
                customEditGoalView3.setSelected(a2.t.getMGoalType() == gb5);
                CustomEditGoalView customEditGoalView4 = a2.u;
                ee7.a((Object) customEditGoalView4, "cegvBottomRight");
                if (a2.u.getMGoalType() != gb5) {
                    z = false;
                }
                customEditGoalView4.setSelected(z);
                b(gb5);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final int e(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "updateSleepGoal minute: " + i2 + " hour: " + i3);
        qw6<i55> qw6 = this.i;
        if (qw6 != null) {
            i55 a2 = qw6.a();
            if (a2 == null) {
                return 0;
            }
            int i4 = i2 + (i3 * 60);
            if (i4 <= 960) {
                return i4;
            }
            a2.A.setText("0");
            a2.z.setText(String.valueOf(16));
            return 960;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(EditText editText) {
        if (editText != null) {
            tc5.b(editText, this.j);
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        ee7.b(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            View view = null;
            if (hashCode != -1375614559) {
                if (hashCode != 927377074) {
                    if (hashCode != 1008390942 || !str.equals("NO_INTERNET_CONNECTION")) {
                        return;
                    }
                    if (i2 == 2131363307) {
                        FragmentActivity activity2 = getActivity();
                        if (activity2 != null) {
                            ((cl5) activity2).l();
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                    } else if (i2 == 2131363229) {
                        g1();
                    }
                } else if (str.equals("GOAL_EXCEED_VALUE") && i2 == 2131363307) {
                    FragmentActivity requireActivity = requireActivity();
                    ee7.a((Object) requireActivity, "requireActivity()");
                    View currentFocus = requireActivity.getCurrentFocus();
                    if (currentFocus instanceof EditText) {
                        view = currentFocus;
                    }
                    a((EditText) view);
                }
            } else if (!str.equals("UNSAVED_CHANGE")) {
            } else {
                if (i2 == 2131363307) {
                    ui6 ui6 = this.h;
                    if (ui6 != null) {
                        ui6.l();
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                } else if (i2 == 2131363229 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            }
        }
    }
}
