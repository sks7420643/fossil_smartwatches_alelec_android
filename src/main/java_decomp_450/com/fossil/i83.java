package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i83 {
    @DexIgnore
    public static /* final */ String a; // = "i83";
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context b;
    @DexIgnore
    public static l83 c;

    @DexIgnore
    public static l83 a(Context context) throws n02 {
        l83 l83;
        a72.a(context);
        l83 l832 = c;
        if (l832 != null) {
            return l832;
        }
        int a2 = p02.a(context, 13400000);
        if (a2 == 0) {
            Log.i(a, "Making Creator dynamically");
            IBinder iBinder = (IBinder) a(b(context).getClassLoader(), "com.google.android.gms.maps.internal.CreatorImpl");
            if (iBinder == null) {
                l83 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICreator");
                if (queryLocalInterface instanceof l83) {
                    l83 = (l83) queryLocalInterface;
                } else {
                    l83 = new m83(iBinder);
                }
            }
            c = l83;
            try {
                l83.a(cb2.a(b(context).getResources()), p02.f);
                return c;
            } catch (RemoteException e) {
                throw new r93(e);
            }
        } else {
            throw new n02(a2);
        }
    }

    @DexIgnore
    public static Context b(Context context) {
        Context context2 = b;
        if (context2 != null) {
            return context2;
        }
        Context c2 = c(context);
        b = c2;
        return c2;
    }

    @DexIgnore
    public static Context c(Context context) {
        try {
            return DynamiteModule.a(context, DynamiteModule.i, "com.google.android.gms.maps_dynamite").a();
        } catch (Exception e) {
            Log.e(a, "Failed to load maps module, use legacy", e);
            return p02.c(context);
        }
    }

    @DexIgnore
    public static <T> T a(ClassLoader classLoader, String str) {
        try {
            a72.a(classLoader);
            return (T) a(classLoader.loadClass(str));
        } catch (ClassNotFoundException unused) {
            String valueOf = String.valueOf(str);
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to find dynamic class ".concat(valueOf) : new String("Unable to find dynamic class "));
        }
    }

    @DexIgnore
    public static <T> T a(Class<?> cls) {
        try {
            return (T) cls.newInstance();
        } catch (InstantiationException unused) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to instantiate the dynamic class ".concat(valueOf) : new String("Unable to instantiate the dynamic class "));
        } catch (IllegalAccessException unused2) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf2.length() != 0 ? "Unable to call the default constructor of ".concat(valueOf2) : new String("Unable to call the default constructor of "));
        }
    }
}
