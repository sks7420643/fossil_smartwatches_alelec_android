package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv7 implements tu7<mo7, Integer> {
    @DexIgnore
    public static /* final */ sv7 a; // = new sv7();

    @DexIgnore
    public Integer a(mo7 mo7) throws IOException {
        return Integer.valueOf(mo7.string());
    }
}
