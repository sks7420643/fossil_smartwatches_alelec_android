package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr3 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131887195;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131887196;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131887197;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131887198;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131887199;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131887200;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131887201;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131887202;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131887203;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131887204;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131887205;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131887206;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131887207;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131887208;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131887209;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131887210;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131887211;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131887212;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131887213;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887214;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887215;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887216;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887217;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887218;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887219;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887220;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887221;
    @DexIgnore
    public static /* final */ int appbar_scrolling_view_behavior; // = 2131887235;
    @DexIgnore
    public static /* final */ int bottom_sheet_behavior; // = 2131887264;
    @DexIgnore
    public static /* final */ int character_counter_content_description; // = 2131887284;
    @DexIgnore
    public static /* final */ int character_counter_overflowed_content_description; // = 2131887285;
    @DexIgnore
    public static /* final */ int character_counter_pattern; // = 2131887286;
    @DexIgnore
    public static /* final */ int chip_text; // = 2131887292;
    @DexIgnore
    public static /* final */ int clear_text_end_icon_content_description; // = 2131887294;
    @DexIgnore
    public static /* final */ int error_icon_content_description; // = 2131887359;
    @DexIgnore
    public static /* final */ int exposed_dropdown_menu_content_description; // = 2131887360;
    @DexIgnore
    public static /* final */ int fab_transformation_scrim_behavior; // = 2131887361;
    @DexIgnore
    public static /* final */ int fab_transformation_sheet_behavior; // = 2131887362;
    @DexIgnore
    public static /* final */ int hide_bottom_view_on_scroll_behavior; // = 2131887386;
    @DexIgnore
    public static /* final */ int icon_content_description; // = 2131887389;
    @DexIgnore
    public static /* final */ int mtrl_badge_numberless_content_description; // = 2131887411;
    @DexIgnore
    public static /* final */ int mtrl_chip_close_icon_content_description; // = 2131887412;
    @DexIgnore
    public static /* final */ int mtrl_exceed_max_badge_number_suffix; // = 2131887413;
    @DexIgnore
    public static /* final */ int mtrl_picker_a11y_next_month; // = 2131887414;
    @DexIgnore
    public static /* final */ int mtrl_picker_a11y_prev_month; // = 2131887415;
    @DexIgnore
    public static /* final */ int mtrl_picker_announce_current_selection; // = 2131887416;
    @DexIgnore
    public static /* final */ int mtrl_picker_cancel; // = 2131887417;
    @DexIgnore
    public static /* final */ int mtrl_picker_confirm; // = 2131887418;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_selected; // = 2131887419;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_title; // = 2131887420;
    @DexIgnore
    public static /* final */ int mtrl_picker_date_header_unselected; // = 2131887421;
    @DexIgnore
    public static /* final */ int mtrl_picker_day_of_week_column_header; // = 2131887422;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format; // = 2131887423;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format_example; // = 2131887424;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_format_use; // = 2131887425;
    @DexIgnore
    public static /* final */ int mtrl_picker_invalid_range; // = 2131887426;
    @DexIgnore
    public static /* final */ int mtrl_picker_navigate_to_year_description; // = 2131887427;
    @DexIgnore
    public static /* final */ int mtrl_picker_out_of_range; // = 2131887428;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_only_end_selected; // = 2131887429;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_only_start_selected; // = 2131887430;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_selected; // = 2131887431;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_title; // = 2131887432;
    @DexIgnore
    public static /* final */ int mtrl_picker_range_header_unselected; // = 2131887433;
    @DexIgnore
    public static /* final */ int mtrl_picker_save; // = 2131887434;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_hint; // = 2131887435;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_range_end_hint; // = 2131887436;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_range_start_hint; // = 2131887437;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_day_abbr; // = 2131887438;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_month_abbr; // = 2131887439;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_year_abbr; // = 2131887440;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_calendar_input_mode; // = 2131887441;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_day_selection; // = 2131887442;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_text_input_mode; // = 2131887443;
    @DexIgnore
    public static /* final */ int mtrl_picker_toggle_to_year_selection; // = 2131887444;
    @DexIgnore
    public static /* final */ int password_toggle_content_description; // = 2131887459;
    @DexIgnore
    public static /* final */ int path_password_eye; // = 2131887460;
    @DexIgnore
    public static /* final */ int path_password_eye_mask_strike_through; // = 2131887461;
    @DexIgnore
    public static /* final */ int path_password_eye_mask_visible; // = 2131887462;
    @DexIgnore
    public static /* final */ int path_password_strike_through; // = 2131887463;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131887518;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887534;
}
