package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq7 extends mq7 {
    @DexIgnore
    public /* final */ Method c;
    @DexIgnore
    public /* final */ Method d;

    @DexIgnore
    public jq7(Method method, Method method2) {
        this.c = method;
        this.d = method2;
    }

    @DexIgnore
    public static jq7 f() {
        try {
            return new jq7(SSLParameters.class.getMethod("setApplicationProtocols", String[].class), SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]));
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public void a(SSLSocket sSLSocket, String str, List<jo7> list) {
        try {
            SSLParameters sSLParameters = sSLSocket.getSSLParameters();
            List<String> a = mq7.a(list);
            this.c.invoke(sSLParameters, a.toArray(new String[a.size()]));
            sSLSocket.setSSLParameters(sSLParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw ro7.a("unable to set ssl parameters", (Exception) e);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public String b(SSLSocket sSLSocket) {
        try {
            String str = (String) this.d.invoke(sSLSocket, new Object[0]);
            if (str == null || str.equals("")) {
                return null;
            }
            return str;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw ro7.a("unable to get selected protocols", (Exception) e);
        }
    }
}
