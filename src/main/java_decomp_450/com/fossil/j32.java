package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j32 extends z32 {
    @DexIgnore
    public WeakReference<c32> a;

    @DexIgnore
    public j32(c32 c32) {
        this.a = new WeakReference<>(c32);
    }

    @DexIgnore
    @Override // com.fossil.z32
    public final void a() {
        c32 c32 = this.a.get();
        if (c32 != null) {
            c32.l();
        }
    }
}
