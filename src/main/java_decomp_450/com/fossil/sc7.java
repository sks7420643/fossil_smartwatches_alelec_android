package com.fossil;

import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc7 {
    @DexIgnore
    public static final String a(Reader reader) {
        ee7.b(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        a(reader, stringWriter, 0, 2, null);
        String stringWriter2 = stringWriter.toString();
        ee7.a((Object) stringWriter2, "buffer.toString()");
        return stringWriter2;
    }

    @DexIgnore
    public static /* synthetic */ long a(Reader reader, Writer writer, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return a(reader, writer, i);
    }

    @DexIgnore
    public static final long a(Reader reader, Writer writer, int i) {
        ee7.b(reader, "$this$copyTo");
        ee7.b(writer, "out");
        char[] cArr = new char[i];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += (long) read;
            read = reader.read(cArr);
        }
        return j;
    }
}
