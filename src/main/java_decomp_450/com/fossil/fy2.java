package com.fossil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy2 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int a;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> b;
    @DexIgnore
    public /* final */ /* synthetic */ dy2 c;

    @DexIgnore
    public fy2(dy2 dy2) {
        this.c = dy2;
        this.a = this.c.b.size();
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.a;
        return (i > 0 && i <= this.c.b.size()) || zza().hasNext();
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        if (zza().hasNext()) {
            return (Map.Entry) zza().next();
        }
        List b2 = this.c.b;
        int i = this.a - 1;
        this.a = i;
        return (Map.Entry) b2.get(i);
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> zza() {
        if (this.b == null) {
            this.b = this.c.f.entrySet().iterator();
        }
        return this.b;
    }

    @DexIgnore
    public /* synthetic */ fy2(dy2 dy2, gy2 gy2) {
        this(dy2);
    }
}
