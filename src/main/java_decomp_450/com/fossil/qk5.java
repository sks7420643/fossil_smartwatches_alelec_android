package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk5 implements Factory<pk5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ qk5 a; // = new qk5();
    }

    @DexIgnore
    public static qk5 a() {
        return a.a;
    }

    @DexIgnore
    public static pk5 b() {
        return new pk5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pk5 get() {
        return b();
    }
}
