package com.fossil;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class l62 implements DialogInterface.OnClickListener {
    @DexIgnore
    public static l62 a(Activity activity, Intent intent, int i) {
        return new a82(intent, activity, i);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            a();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static l62 a(x12 x12, Intent intent, int i) {
        return new b82(intent, x12, i);
    }
}
