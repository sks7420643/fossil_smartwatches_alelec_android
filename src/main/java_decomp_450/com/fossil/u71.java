package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u71 {
    @DexIgnore
    public /* synthetic */ u71(zd7 zd7) {
    }

    @DexIgnore
    public final p91 a(int i) {
        if (i == 0) {
            return p91.DISCONNECTED;
        }
        if (i == 1) {
            return p91.CONNECTING;
        }
        if (i == 2) {
            return p91.CONNECTED;
        }
        if (i != 3) {
            return p91.DISCONNECTED;
        }
        return p91.DISCONNECTING;
    }
}
