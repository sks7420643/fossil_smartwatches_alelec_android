package com.fossil;

import com.fossil.g74;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f74 implements g74 {
    @DexIgnore
    public /* final */ File a;

    @DexIgnore
    public f74(File file) {
        this.a = file;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public Map<String, String> a() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public String b() {
        return this.a.getName();
    }

    @DexIgnore
    @Override // com.fossil.g74
    public File c() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public File[] d() {
        return this.a.listFiles();
    }

    @DexIgnore
    @Override // com.fossil.g74
    public String e() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public g74.a getType() {
        return g74.a.NATIVE;
    }

    @DexIgnore
    @Override // com.fossil.g74
    public void remove() {
        File[] d = d();
        for (File file : d) {
            z24.a().a("Removing native report file at " + file.getPath());
            file.delete();
        }
        z24.a().a("Removing native report directory at " + this.a);
        this.a.delete();
    }
}
