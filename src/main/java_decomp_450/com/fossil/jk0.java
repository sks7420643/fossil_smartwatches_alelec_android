package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jk0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ mi0 CREATOR; // = new mi0(null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c; // = ((TimeZone.getDefault().getOffset(this.d) / 1000) / 60);
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public jk0(long j) {
        this.d = j;
        long j2 = (long) 1000;
        long j3 = j / j2;
        this.a = j3;
        this.b = j - (j3 * j2);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.E2, Long.valueOf(this.a)), r51.F2, Long.valueOf(this.b)), r51.u2, Integer.valueOf(this.c));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.d);
    }
}
