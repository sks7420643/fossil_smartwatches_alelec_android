package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz3 extends jz3<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ uz3 INSTANCE; // = new uz3();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S extends Comparable> jz3<S> reverse() {
        return jz3.natural();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural().reverse()";
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        jw3.a(comparable);
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2) {
        return (E) ((Comparable) fz3.INSTANCE.min(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2) {
        return (E) ((Comparable) fz3.INSTANCE.max(e, e2));
    }

    @DexIgnore
    public <E extends Comparable> E max(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) fz3.INSTANCE.min(e, e2, e3, eArr));
    }

    @DexIgnore
    public <E extends Comparable> E min(E e, E e2, E e3, E... eArr) {
        return (E) ((Comparable) fz3.INSTANCE.max(e, e2, e3, eArr));
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E extends Comparable> E max(Iterator<E> it) {
        return (E) ((Comparable) fz3.INSTANCE.min(it));
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E extends Comparable> E min(Iterator<E> it) {
        return (E) ((Comparable) fz3.INSTANCE.max(it));
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E extends Comparable> E max(Iterable<E> iterable) {
        return (E) ((Comparable) fz3.INSTANCE.min(iterable));
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E extends Comparable> E min(Iterable<E> iterable) {
        return (E) ((Comparable) fz3.INSTANCE.max(iterable));
    }
}
