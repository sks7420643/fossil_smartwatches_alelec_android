package com.fossil;

import com.fossil.v02;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b42 {
    @DexIgnore
    <A extends v02.b, T extends r12<? extends i12, A>> T a(T t);

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean a(c22 c22);

    @DexIgnore
    <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    boolean c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    i02 f();
}
