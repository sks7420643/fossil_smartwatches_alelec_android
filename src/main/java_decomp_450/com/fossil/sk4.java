package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk4 implements Factory<ll4> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<is6> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> f;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> g;
    @DexIgnore
    public /* final */ Provider<DeviceDao> h;
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> i;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> j;
    @DexIgnore
    public /* final */ Provider<ad5> k;
    @DexIgnore
    public /* final */ Provider<lm4> l;

    @DexIgnore
    public sk4(wj4 wj4, Provider<ch5> provider, Provider<UserRepository> provider2, Provider<is6> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<ad5> provider10, Provider<lm4> provider11) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
    }

    @DexIgnore
    public static sk4 a(wj4 wj4, Provider<ch5> provider, Provider<UserRepository> provider2, Provider<is6> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<ad5> provider10, Provider<lm4> provider11) {
        return new sk4(wj4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11);
    }

    @DexIgnore
    public static ll4 a(wj4 wj4, ch5 ch5, UserRepository userRepository, is6 is6, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, ad5 ad5, lm4 lm4) {
        ll4 a2 = wj4.a(ch5, userRepository, is6, notificationsRepository, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, ad5, lm4);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ll4 get() {
        return a(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get());
    }
}
