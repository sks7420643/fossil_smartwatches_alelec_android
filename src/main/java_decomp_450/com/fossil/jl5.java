package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.kl5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl5 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<kl5> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<pl5> b;
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public RecyclerView c;
        @DexIgnore
        public /* final */ /* synthetic */ jl5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jl5$a$a")
        /* renamed from: com.fossil.jl5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0086a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0086a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.d();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.d();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jl5 jl5, View view) {
            super(view);
            ee7.b(view, "itemView");
            this.d = jl5;
            View findViewById = view.findViewById(2131363245);
            if (findViewById != null) {
                TextView textView = (TextView) findViewById;
                textView.setOnClickListener(new View$OnClickListenerC0086a(this));
                this.a = textView;
                View findViewById2 = view.findViewById(2131362633);
                if (findViewById2 != null) {
                    ImageView imageView = (ImageView) findViewById2;
                    imageView.setOnClickListener(new b(this));
                    this.b = imageView;
                    View findViewById3 = view.findViewById(2131362984);
                    if (findViewById3 != null) {
                        this.c = (RecyclerView) findViewById3;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final RecyclerView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }

        @DexIgnore
        public final void d() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                ArrayList<pl5> d2 = this.d.d();
                if (d2 != null) {
                    boolean c2 = d2.get(adapterPosition).c();
                    ArrayList<pl5> d3 = this.d.d();
                    if (d3 != null) {
                        d3.get(adapterPosition).a(!c2);
                        this.d.notifyItemChanged(adapterPosition);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final ImageView a() {
            return this.b;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements kl5.e {
        @DexIgnore
        public /* final */ /* synthetic */ jl5 a;

        @DexIgnore
        public c(pl5 pl5, jl5 jl5) {
            this.a = jl5;
        }

        @DexIgnore
        @Override // com.fossil.kl5.e
        public void a(String str, int i, int i2, Object obj, Bundle bundle) {
            ee7.b(str, "tagName");
            b c = this.a.c();
            if (c != null) {
                c.a(str, i, i2, obj, bundle);
            }
        }

        @DexIgnore
        @Override // com.fossil.kl5.e
        public void b(String str, int i, int i2, Object obj, Bundle bundle) {
            ee7.b(str, "tagName");
            b c = this.a.c();
            if (c != null) {
                c.b(str, i, i2, obj, bundle);
            }
        }
    }

    @DexIgnore
    public final void a(ArrayList<pl5> arrayList) {
        this.a.clear();
        if (arrayList != null) {
            for (T t : arrayList) {
                ArrayList<kl5> arrayList2 = this.a;
                kl5 kl5 = new kl5();
                kl5.a(new c(t, this));
                kl5.a(t.a());
                arrayList2.add(kl5);
            }
        }
        this.b = arrayList;
    }

    @DexIgnore
    public final b c() {
        return this.c;
    }

    @DexIgnore
    public final ArrayList<pl5> d() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        ArrayList<pl5> arrayList = this.b;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558677, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        TextView c2 = aVar.c();
        ArrayList<pl5> arrayList = this.b;
        if (arrayList != null) {
            c2.setText(arrayList.get(i).b());
            RecyclerView b2 = aVar.b();
            b2.setLayoutManager(new LinearLayoutManager(b2.getContext()));
            kl5 kl5 = this.a.get(i);
            ee7.a((Object) kl5, "listDebugChildAdapter[position]");
            kl5 kl52 = kl5;
            kl52.a(i);
            b2.setAdapter(kl52);
            ArrayList<pl5> arrayList2 = this.b;
            if (arrayList2 == null) {
                ee7.a();
                throw null;
            } else if (arrayList2.get(i).c()) {
                aVar.a().setImageResource(2131230993);
                aVar.b().setVisibility(0);
            } else {
                aVar.a().setImageResource(2131230992);
                aVar.b().setVisibility(8);
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "itemClickListener");
        this.c = bVar;
    }
}
