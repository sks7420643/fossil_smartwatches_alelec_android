package com.fossil;

import android.os.Bundle;
import com.fossil.a12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b32 implements a12.b, a12.c {
    @DexIgnore
    public /* final */ /* synthetic */ q22 a;

    @DexIgnore
    public b32(q22 q22) {
        this.a = q22;
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void a(int i) {
    }

    @DexIgnore
    @Override // com.fossil.a22
    public final void a(i02 i02) {
        this.a.b.lock();
        try {
            if (this.a.a(i02)) {
                this.a.g();
                this.a.e();
            } else {
                this.a.b(i02);
            }
        } finally {
            this.a.b.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.t12
    public final void b(Bundle bundle) {
        if (this.a.r.k()) {
            this.a.b.lock();
            try {
                if (this.a.k != null) {
                    this.a.k.a(new z22(this.a));
                    this.a.b.unlock();
                }
            } finally {
                this.a.b.unlock();
            }
        } else {
            this.a.k.a(new z22(this.a));
        }
    }

    @DexIgnore
    public /* synthetic */ b32(q22 q22, t22 t22) {
        this(q22);
    }
}
