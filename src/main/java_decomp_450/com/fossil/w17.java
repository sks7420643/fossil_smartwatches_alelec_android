package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w17 extends u17 {
    @DexIgnore
    public /* final */ Map<String, Object> a;
    @DexIgnore
    public /* final */ a b; // = new a(this);
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements a27 {
        @DexIgnore
        public Object a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Object d;

        @DexIgnore
        public a(w17 w17) {
        }

        @DexIgnore
        @Override // com.fossil.a27
        public void error(String str, String str2, Object obj) {
            this.b = str;
            this.c = str2;
            this.d = obj;
        }

        @DexIgnore
        @Override // com.fossil.a27
        public void success(Object obj) {
            this.a = obj;
        }
    }

    @DexIgnore
    public w17(Map<String, Object> map, boolean z) {
        this.a = map;
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.z17
    public <T> T a(String str) {
        return (T) this.a.get(str);
    }

    @DexIgnore
    @Override // com.fossil.z17, com.fossil.v17
    public boolean b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.u17
    public a27 g() {
        return this.b;
    }

    @DexIgnore
    public String h() {
        return (String) this.a.get("method");
    }

    @DexIgnore
    public Map<String, Object> i() {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("code", this.b.b);
        hashMap2.put("message", this.b.c);
        hashMap2.put("data", this.b.d);
        hashMap.put("error", hashMap2);
        return hashMap;
    }

    @DexIgnore
    public Map<String, Object> j() {
        HashMap hashMap = new HashMap();
        hashMap.put(Constants.RESULT, this.b.a);
        return hashMap;
    }

    @DexIgnore
    public void a(MethodChannel.Result result) {
        a aVar = this.b;
        result.error(aVar.b, aVar.c, aVar.d);
    }

    @DexIgnore
    public void b(List<Map<String, Object>> list) {
        if (!b()) {
            list.add(j());
        }
    }

    @DexIgnore
    public void a(List<Map<String, Object>> list) {
        if (!b()) {
            list.add(i());
        }
    }
}
