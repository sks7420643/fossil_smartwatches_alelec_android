package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu0 extends k60 {
    @DexIgnore
    public /* final */ wm0 a;
    @DexIgnore
    public /* final */ is0 b;
    @DexIgnore
    public /* final */ sz0 c;

    @DexIgnore
    public /* synthetic */ eu0(wm0 wm0, is0 is0, sz0 sz0, int i) {
        wm0 = (i & 1) != 0 ? wm0.a : wm0;
        sz0 = (i & 4) != 0 ? new sz0(null, null, ay0.a, null, null, 27) : sz0;
        this.a = wm0;
        this.b = is0;
        this.c = sz0;
    }

    @DexIgnore
    public static /* synthetic */ eu0 a(eu0 eu0, wm0 wm0, is0 is0, sz0 sz0, int i) {
        if ((i & 1) != 0) {
            wm0 = eu0.a;
        }
        if ((i & 2) != 0) {
            is0 = eu0.b;
        }
        if ((i & 4) != 0) {
            sz0 = eu0.c;
        }
        return eu0.a(wm0, is0, sz0);
    }

    @DexIgnore
    public final eu0 a(wm0 wm0, is0 is0, sz0 sz0) {
        return new eu0(wm0, is0, sz0);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(yz0.a(jSONObject, r51.g4, yz0.a(this.a)), r51.M0, yz0.a(this.b));
            if (this.c.c != ay0.a) {
                yz0.a(jSONObject, r51.h4, this.c.a());
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof eu0)) {
            return false;
        }
        eu0 eu0 = (eu0) obj;
        return ee7.a(this.a, eu0.a) && ee7.a(this.b, eu0.b) && ee7.a(this.c, eu0.c);
    }

    @DexIgnore
    public int hashCode() {
        wm0 wm0 = this.a;
        int i = 0;
        int hashCode = (wm0 != null ? wm0.hashCode() : 0) * 31;
        is0 is0 = this.b;
        int hashCode2 = (hashCode + (is0 != null ? is0.hashCode() : 0)) * 31;
        sz0 sz0 = this.c;
        if (sz0 != null) {
            i = sz0.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("Result(phaseId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", requestResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public eu0(wm0 wm0, is0 is0, sz0 sz0) {
        this.a = wm0;
        this.b = is0;
        this.c = sz0;
    }
}
