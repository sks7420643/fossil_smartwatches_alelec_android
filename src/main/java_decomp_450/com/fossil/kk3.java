package com.fossil;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 c;

    @DexIgnore
    public kk3(ek3 ek3, AtomicReference atomicReference, nm3 nm3) {
        this.c = ek3;
        this.a = atomicReference;
        this.b = nm3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                bg3 d = this.c.d;
                if (d == null) {
                    this.c.e().t().a("Failed to get app instance id");
                    return;
                }
                this.a.set(d.a(this.b));
                String str = (String) this.a.get();
                if (str != null) {
                    this.c.o().a(str);
                    this.c.k().l.a(str);
                }
                this.c.J();
                this.a.notify();
            } catch (RemoteException e) {
                this.c.e().t().a("Failed to get app instance id", e);
            } finally {
                this.a.notify();
            }
        }
    }
}
