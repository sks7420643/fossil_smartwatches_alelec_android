package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z17 extends a27 {
    @DexIgnore
    s17 a();

    @DexIgnore
    <T> T a(String str);

    @DexIgnore
    boolean b();

    @DexIgnore
    Boolean c();
}
