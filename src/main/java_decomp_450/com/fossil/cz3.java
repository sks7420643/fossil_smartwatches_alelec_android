package com.fossil;

import com.fossil.dz3;
import com.fossil.ez3;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends rw3<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient nw3<? extends List<V>> factory;

        @DexIgnore
        public a(Map<K, Collection<V>> map, nw3<? extends List<V>> nw3) {
            super(map);
            jw3.a(nw3);
            this.factory = nw3;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (nw3) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.rw3, com.fossil.rw3, com.fossil.sw3
        public List<V> createCollection() {
            return (List) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends xw3<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient nw3<? extends Set<V>> factory;

        @DexIgnore
        public b(Map<K, Collection<V>> map, nw3<? extends Set<V>> nw3) {
            super(map);
            jw3.a(nw3);
            this.factory = nw3;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (nw3) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        @Override // com.fossil.sw3, com.fossil.xw3, com.fossil.xw3
        public Set<V> createCollection() {
            return (Set) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends AbstractCollection<Map.Entry<K, V>> {
        @DexIgnore
        public abstract zy3<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().remove(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends ww3<K> {
        @DexIgnore
        @Weak
        public /* final */ zy3<K, V> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h04<Map.Entry<K, Collection<V>>, dz3.a<K>> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cz3$d$a$a")
            /* renamed from: com.fossil.cz3$d$a$a  reason: collision with other inner class name */
            public class C0032a extends ez3.b<K> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry a;

                @DexIgnore
                public C0032a(a aVar, Map.Entry entry) {
                    this.a = entry;
                }

                @DexIgnore
                @Override // com.fossil.dz3.a
                public int getCount() {
                    return ((Collection) this.a.getValue()).size();
                }

                @DexIgnore
                @Override // com.fossil.dz3.a
                public K getElement() {
                    return (K) this.a.getKey();
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            @Override // com.fossil.h04
            public /* bridge */ /* synthetic */ Object a(Object obj) {
                return a((Map.Entry) ((Map.Entry) obj));
            }

            @DexIgnore
            public dz3.a<K> a(Map.Entry<K, Collection<V>> entry) {
                return new C0032a(this, entry);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends ez3.d<K> {
            @DexIgnore
            public b() {
            }

            @DexIgnore
            @Override // com.fossil.ez3.d
            public dz3<K> a() {
                return d.this;
            }

            @DexIgnore
            @Override // com.fossil.ez3.d
            public boolean contains(Object obj) {
                if (!(obj instanceof dz3.a)) {
                    return false;
                }
                dz3.a aVar = (dz3.a) obj;
                Collection<V> collection = d.this.c.asMap().get(aVar.getElement());
                if (collection == null || collection.size() != aVar.getCount()) {
                    return false;
                }
                return true;
            }

            @DexIgnore
            public boolean isEmpty() {
                return d.this.c.isEmpty();
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<dz3.a<K>> iterator() {
                return d.this.entryIterator();
            }

            @DexIgnore
            @Override // com.fossil.ez3.d
            public boolean remove(Object obj) {
                if (!(obj instanceof dz3.a)) {
                    return false;
                }
                dz3.a aVar = (dz3.a) obj;
                Collection<V> collection = d.this.c.asMap().get(aVar.getElement());
                if (collection == null || collection.size() != aVar.getCount()) {
                    return false;
                }
                collection.clear();
                return true;
            }

            @DexIgnore
            public int size() {
                return d.this.distinctElements();
            }
        }

        @DexIgnore
        public d(zy3<K, V> zy3) {
            this.c = zy3;
        }

        @DexIgnore
        @Override // com.fossil.ww3
        public void clear() {
            this.c.clear();
        }

        @DexIgnore
        @Override // com.fossil.dz3, com.fossil.ww3
        public boolean contains(Object obj) {
            return this.c.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.dz3, com.fossil.ww3
        public int count(Object obj) {
            Collection collection = (Collection) yy3.e(this.c.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.ww3
        public Set<dz3.a<K>> createEntrySet() {
            return new b();
        }

        @DexIgnore
        @Override // com.fossil.ww3
        public int distinctElements() {
            return this.c.asMap().size();
        }

        @DexIgnore
        @Override // com.fossil.dz3, com.fossil.ww3
        public Set<K> elementSet() {
            return this.c.keySet();
        }

        @DexIgnore
        @Override // com.fossil.ww3
        public Iterator<dz3.a<K>> entryIterator() {
            return new a(this, this.c.asMap().entrySet().iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.ww3, java.util.Collection, java.lang.Iterable
        public Iterator<K> iterator() {
            return yy3.a(this.c.entries().iterator());
        }

        @DexIgnore
        @Override // com.fossil.dz3, com.fossil.ww3
        public int remove(Object obj, int i) {
            bx3.a(i, "occurrences");
            if (i == 0) {
                return count(obj);
            }
            Collection collection = (Collection) yy3.e(this.c.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            int size = collection.size();
            if (i >= size) {
                collection.clear();
            } else {
                Iterator it = collection.iterator();
                for (int i2 = 0; i2 < i; i2++) {
                    it.next();
                    it.remove();
                }
            }
            return size;
        }
    }

    @DexIgnore
    public static <K, V> ty3<K, V> a(Map<K, Collection<V>> map, nw3<? extends List<V>> nw3) {
        return new a(map, nw3);
    }

    @DexIgnore
    public static <K, V> xz3<K, V> b(Map<K, Collection<V>> map, nw3<? extends Set<V>> nw3) {
        return new b(map, nw3);
    }

    @DexIgnore
    public static boolean a(zy3<?, ?> zy3, Object obj) {
        if (obj == zy3) {
            return true;
        }
        if (obj instanceof zy3) {
            return zy3.asMap().equals(((zy3) obj).asMap());
        }
        return false;
    }
}
