package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ll5 extends hl5 {
    @DexIgnore
    public String c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ll5(String str, String str2) {
        super(str);
        ee7.b(str, "tagName");
        ee7.b(str2, "title");
        this.c = str2;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }
}
