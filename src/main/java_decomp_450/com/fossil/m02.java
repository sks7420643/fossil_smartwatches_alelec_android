package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m02 {
    @DexIgnore
    public static /* final */ int a; // = q02.a;
    @DexIgnore
    public static /* final */ m02 b; // = new m02();

    @DexIgnore
    public static m02 a() {
        return b;
    }

    @DexIgnore
    public int b(Context context) {
        return q02.b(context);
    }

    @DexIgnore
    public int c(Context context) {
        return a(context, a);
    }

    @DexIgnore
    public int a(Context context, int i) {
        int a2 = q02.a(context, i);
        if (q02.c(context, a2)) {
            return 18;
        }
        return a2;
    }

    @DexIgnore
    public boolean b(Context context, int i) {
        return q02.c(context, i);
    }

    @DexIgnore
    public boolean c(int i) {
        return q02.b(i);
    }

    @DexIgnore
    public String b(int i) {
        return q02.a(i);
    }

    @DexIgnore
    public static String b(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(a);
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (context != null) {
            try {
                sb.append(ja2.b(context).b(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Deprecated
    public Intent a(int i) {
        return a((Context) null, i, (String) null);
    }

    @DexIgnore
    public Intent a(Context context, int i, String str) {
        if (i == 1 || i == 2) {
            if (context == null || !r92.b(context)) {
                return r82.a("com.google.android.gms", b(context, str));
            }
            return r82.a();
        } else if (i != 3) {
            return null;
        } else {
            return r82.a("com.google.android.gms");
        }
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2) {
        return a(context, i, i2, null);
    }

    @DexIgnore
    public PendingIntent a(Context context, int i, int i2, String str) {
        Intent a2 = a(context, i, str);
        if (a2 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, a2, 134217728);
    }

    @DexIgnore
    public void a(Context context) {
        q02.a(context);
    }

    @DexIgnore
    public boolean a(Context context, String str) {
        return q02.a(context, str);
    }
}
