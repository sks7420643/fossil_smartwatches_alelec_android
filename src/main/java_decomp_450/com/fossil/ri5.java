package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.mk5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ri5 extends oi5 {
    @DexIgnore
    public static /* final */ String i; // = ri5.class.getSimpleName();
    @DexIgnore
    public /* final */ HashMap<String, Long> f; // = new HashMap<>();
    @DexIgnore
    public mk5 g;
    @DexIgnore
    public ch5 h;

    @DexIgnore
    @Override // com.fossil.oi5
    public void a(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onMissedCall : " + str);
        if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.c0.c()) && !TextUtils.isEmpty(str)) {
            this.g.a(str, date, mk5.g.PICKED);
        }
    }

    @DexIgnore
    @Override // com.fossil.oi5
    public void a(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    @Override // com.fossil.oi5
    public void b(Context context, String str, Date date) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "Phone Receiver : onIncomingCallStarted : " + str);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.c0.c())) {
            a(str);
        } else {
            a(str, date);
        }
    }

    @DexIgnore
    @Override // com.fossil.oi5
    public void b(Context context, String str, Date date, Date date2) {
    }

    @DexIgnore
    @Override // com.fossil.oi5
    public void c(Context context, String str, Date date) {
    }

    @DexIgnore
    @Override // com.fossil.oi5
    public void d(Context context, String str, Date date) {
    }

    @DexIgnore
    public final void a(String str, Date date) {
        if (!TextUtils.isEmpty(str)) {
            this.g.a(str, date, mk5.g.RINGING);
        }
    }

    @DexIgnore
    public final void a(String str) {
        String b = yx6.b(str);
        boolean I = this.h.I();
        synchronized (this.f) {
            this.f.put(b, Long.valueOf(System.currentTimeMillis()));
        }
        if (I) {
            FLogger.INSTANCE.getLocal().d(i, "Phone Receiver - blocked by DND mode");
            return;
        }
        sg5.i.a().a(new NotificationInfo(NotificationSource.CALL, b, "", ""));
        a();
    }

    @DexIgnore
    public final void a() {
        synchronized (this.f) {
            long currentTimeMillis = System.currentTimeMillis();
            int size = this.f.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "Clean past call. Size = " + size);
            LinkedList<String> linkedList = new LinkedList();
            for (Map.Entry<String, Long> entry : this.f.entrySet()) {
                String key = entry.getKey();
                if (currentTimeMillis - entry.getValue().longValue() > 900000) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = i;
                    local2.d(str2, "Adding key to remove - key = " + key);
                    linkedList.add(key);
                }
            }
            for (String str3 : linkedList) {
                FLogger.INSTANCE.getLocal().d(i, "Dumping old call");
                this.f.remove(str3);
            }
        }
    }
}
