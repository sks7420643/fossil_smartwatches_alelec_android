package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ za0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<rf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public rf0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(jc0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<Wo\u2026class.java.classLoader)!!");
                return new rf0((jc0) readParcelable, (df0) parcel.readParcelable(df0.class.getClassLoader()), (za0) parcel.readParcelable(za0.class.getClassLoader()));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public rf0[] newArray(int i) {
            return new rf0[i];
        }
    }

    @DexIgnore
    public rf0(jc0 jc0, za0 za0) {
        super(jc0, null);
        this.c = za0;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            za0 za0 = this.c;
            if (za0 != null) {
                JSONObject a2 = yz0.a(new JSONObject(), r51.G, za0.getFileName());
                JSONObject jSONObject2 = new JSONObject();
                r51 r51 = r51.G5;
                yb0 deviceRequest2 = getDeviceRequest();
                if (deviceRequest2 != null) {
                    jSONObject.put("workoutApp._.config.images", yz0.a(yz0.a(yz0.a(jSONObject2, r51, Long.valueOf(((jc0) deviceRequest2).getSessionId())), r51.J5, a2), r51.N5, za0.f().a));
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
                }
            }
            if (getDeviceMessage() != null) {
                jSONObject.put("workoutApp._.config.response", getDeviceMessage().a());
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        ee7.a((Object) jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        JSONObject jSONObject;
        JSONObject b = super.b();
        za0 za0 = this.c;
        if (za0 == null || (jSONObject = za0.a()) == null) {
            jSONObject = new JSONObject();
        }
        return yz0.a(b, jSONObject);
    }

    @DexIgnore
    public final za0 c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(rf0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.c, ((rf0) obj).c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WorkoutRouteImageData");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        za0 za0 = this.c;
        return hashCode + (za0 != null ? za0.hashCode() : 0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public rf0(jc0 jc0, df0 df0) {
        super(jc0, df0);
        this.c = null;
    }

    @DexIgnore
    public rf0(jc0 jc0, df0 df0, za0 za0) {
        super(jc0, df0);
        this.c = za0;
    }
}
