package com.fossil;

import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az1 implements Runnable {
    @DexIgnore
    public static /* final */ b92 c; // = new b92("RevokeAccessOperation", new String[0]);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ e22 b; // = new e22(null);

    @DexIgnore
    public az1(String str) {
        a72.b(str);
        this.a = str;
    }

    @DexIgnore
    public static c12<Status> a(String str) {
        if (str == null) {
            return d12.a((i12) new Status(4), (a12) null);
        }
        az1 az1 = new az1(str);
        new Thread(az1).start();
        return az1.b;
    }

    @DexIgnore
    public final void run() {
        Status status = Status.g;
        try {
            String valueOf = String.valueOf(this.a);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(valueOf.length() != 0 ? "https://accounts.google.com/o/oauth2/revoke?token=".concat(valueOf) : new String("https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.e;
            } else {
                c.b("Unable to revoke access!", new Object[0]);
            }
            b92 b92 = c;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Response Code: ");
            sb.append(responseCode);
            b92.a(sb.toString(), new Object[0]);
        } catch (IOException e) {
            b92 b922 = c;
            String valueOf2 = String.valueOf(e.toString());
            b922.b(valueOf2.length() != 0 ? "IOException when revoking access: ".concat(valueOf2) : new String("IOException when revoking access: "), new Object[0]);
        } catch (Exception e2) {
            b92 b923 = c;
            String valueOf3 = String.valueOf(e2.toString());
            b923.b(valueOf3.length() != 0 ? "Exception when revoking access: ".concat(valueOf3) : new String("Exception when revoking access: "), new Object[0]);
        }
        this.b.a((i12) status);
    }
}
