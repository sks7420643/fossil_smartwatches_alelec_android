package com.fossil;

import android.os.Build;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import retrofit.android.AndroidLog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hq7 extends mq7 {
    @DexIgnore
    public /* final */ lq7<Socket> c;
    @DexIgnore
    public /* final */ lq7<Socket> d;
    @DexIgnore
    public /* final */ lq7<Socket> e;
    @DexIgnore
    public /* final */ lq7<Socket> f;
    @DexIgnore
    public /* final */ c g; // = c.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qq7 {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public a(Object obj, Method method) {
            this.a = obj;
            this.b = method;
        }

        @DexIgnore
        @Override // com.fossil.qq7
        public List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                return (List) this.b.invoke(this.a, (X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str);
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return obj instanceof a;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements sq7 {
        @DexIgnore
        public /* final */ X509TrustManager a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public b(X509TrustManager x509TrustManager, Method method) {
            this.b = method;
            this.a = x509TrustManager;
        }

        @DexIgnore
        @Override // com.fossil.sq7
        public X509Certificate a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.b.invoke(this.a, x509Certificate);
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw ro7.a("unable to get issues and signature", (Exception) e);
            } catch (InvocationTargetException unused) {
                return null;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (!this.a.equals(bVar.a) || !this.b.equals(bVar.b)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() + (this.b.hashCode() * 31);
        }
    }

    @DexIgnore
    public hq7(Class<?> cls, lq7<Socket> lq7, lq7<Socket> lq72, lq7<Socket> lq73, lq7<Socket> lq74) {
        this.c = lq7;
        this.d = lq72;
        this.e = lq73;
        this.f = lq74;
    }

    @DexIgnore
    public static mq7 f() {
        Class<?> cls;
        lq7 lq7;
        lq7 lq72;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException unused) {
            try {
                cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
            } catch (ClassNotFoundException unused2) {
                return null;
            }
        }
        lq7 lq73 = new lq7(null, "setUseSessionTickets", Boolean.TYPE);
        lq7 lq74 = new lq7(null, "setHostname", String.class);
        if (g()) {
            lq7 lq75 = new lq7(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
            lq7 = new lq7(null, "setAlpnProtocols", byte[].class);
            lq72 = lq75;
        } else {
            lq72 = null;
            lq7 = null;
        }
        return new hq7(cls, lq73, lq74, lq72, lq7);
    }

    @DexIgnore
    public static boolean g() {
        if (Security.getProvider("GMSCore_OpenSSL") != null) {
            return true;
        }
        try {
            Class.forName("android.net.Network");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e2) {
            if (ro7.a(e2)) {
                throw new IOException(e2);
            }
            throw e2;
        } catch (SecurityException e3) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e3);
            throw iOException;
        } catch (ClassCastException e4) {
            if (Build.VERSION.SDK_INT == 26) {
                IOException iOException2 = new IOException("Exception in connect");
                iOException2.initCause(e4);
                throw iOException2;
            }
            throw e4;
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public String b(SSLSocket sSLSocket) {
        byte[] bArr;
        lq7<Socket> lq7 = this.e;
        if (lq7 == null || !lq7.a(sSLSocket) || (bArr = (byte[]) this.e.d(sSLSocket, new Object[0])) == null) {
            return null;
        }
        return new String(bArr, ro7.i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public c(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }

        @DexIgnore
        public Object a(String str) {
            Method method = this.a;
            if (method != null) {
                try {
                    Object invoke = method.invoke(null, new Object[0]);
                    this.b.invoke(invoke, str);
                    return invoke;
                } catch (Exception unused) {
                }
            }
            return null;
        }

        @DexIgnore
        public boolean a(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.c.invoke(obj, new Object[0]);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        @DexIgnore
        public static c a() {
            Method method;
            Method method2;
            Method method3 = null;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                Method method4 = cls.getMethod("get", new Class[0]);
                method = cls.getMethod("open", String.class);
                method2 = cls.getMethod("warnIfOpen", new Class[0]);
                method3 = method4;
            } catch (Exception unused) {
                method2 = null;
                method = null;
            }
            return new c(method3, method, method2);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public boolean b(String str) {
        if (Build.VERSION.SDK_INT < 23) {
            return super.b(str);
        }
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            return b(str, cls, cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]));
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return super.b(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            throw ro7.a("unable to determine cleartext support", e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public void a(SSLSocket sSLSocket, String str, List<jo7> list) {
        if (str != null) {
            this.c.c(sSLSocket, true);
            this.d.c(sSLSocket, str);
        }
        lq7<Socket> lq7 = this.f;
        if (lq7 != null && lq7.a(sSLSocket)) {
            this.f.d(sSLSocket, mq7.b(list));
        }
    }

    @DexIgnore
    public final boolean b(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", String.class).invoke(obj, str)).booleanValue();
        } catch (NoSuchMethodException unused) {
            return a(str, cls, obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public void a(int i, String str, Throwable th) {
        int min;
        int i2 = 5;
        if (i != 5) {
            i2 = 3;
        }
        if (th != null) {
            str = str + '\n' + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + AndroidLog.LOG_CHUNK_SIZE);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public sq7 b(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
            declaredMethod.setAccessible(true);
            return new b(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException unused) {
            return super.b(x509TrustManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public SSLContext b() {
        boolean z = true;
        try {
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT >= 22) {
                z = false;
            }
        } catch (NoClassDefFoundError unused) {
        }
        if (z) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException unused2) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException("No TLS provider", e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public Object a(String str) {
        return this.g.a(str);
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public void a(String str, Object obj) {
        if (!this.g.a(obj)) {
            a(5, str, (Throwable) null);
        }
    }

    @DexIgnore
    public final boolean a(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (NoSuchMethodException unused) {
            return super.b(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.mq7
    public qq7 a(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new a(cls.getConstructor(X509TrustManager.class).newInstance(x509TrustManager), cls.getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class));
        } catch (Exception unused) {
            return super.a(x509TrustManager);
        }
    }
}
