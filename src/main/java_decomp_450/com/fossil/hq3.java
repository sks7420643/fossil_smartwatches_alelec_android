package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq3 implements Parcelable.Creator<gq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                str = j72.e(parcel, a);
            } else if (a2 == 3) {
                str2 = j72.e(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                str3 = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new gq3(str, str2, str3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ gq3[] newArray(int i) {
        return new gq3[i];
    }
}
