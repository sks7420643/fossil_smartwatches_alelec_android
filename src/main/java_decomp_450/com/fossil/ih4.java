package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ih4 implements sg4 {
    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) {
        ng4 ng4;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (mg4 != mg4.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + mg4);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            uh4 uh4 = uh4.FORCE_NONE;
            ng4 ng42 = null;
            if (map != null) {
                uh4 uh42 = (uh4) map.get(og4.DATA_MATRIX_SHAPE);
                if (uh42 != null) {
                    uh4 = uh42;
                }
                ng4 ng43 = (ng4) map.get(og4.MIN_SIZE);
                if (ng43 == null) {
                    ng43 = null;
                }
                ng4 = (ng4) map.get(og4.MAX_SIZE);
                if (ng4 == null) {
                    ng4 = null;
                }
                ng42 = ng43;
            } else {
                ng4 = null;
            }
            String a = sh4.a(str, uh4, ng42, ng4);
            th4 a2 = th4.a(a.length(), uh4, ng42, ng4, true);
            nh4 nh4 = new nh4(rh4.a(a, a2), a2.f(), a2.e());
            nh4.a();
            return a(nh4, a2);
        }
    }

    @DexIgnore
    public static dh4 a(nh4 nh4, th4 th4) {
        int f = th4.f();
        int e = th4.e();
        ej4 ej4 = new ej4(th4.h(), th4.g());
        int i = 0;
        for (int i2 = 0; i2 < e; i2++) {
            if (i2 % th4.e == 0) {
                int i3 = 0;
                for (int i4 = 0; i4 < th4.h(); i4++) {
                    ej4.a(i3, i, i4 % 2 == 0);
                    i3++;
                }
                i++;
            }
            int i5 = 0;
            for (int i6 = 0; i6 < f; i6++) {
                if (i6 % th4.d == 0) {
                    ej4.a(i5, i, true);
                    i5++;
                }
                ej4.a(i5, i, nh4.a(i6, i2));
                i5++;
                int i7 = th4.d;
                if (i6 % i7 == i7 - 1) {
                    ej4.a(i5, i, i2 % 2 == 0);
                    i5++;
                }
            }
            i++;
            int i8 = th4.e;
            if (i2 % i8 == i8 - 1) {
                int i9 = 0;
                for (int i10 = 0; i10 < th4.h(); i10++) {
                    ej4.a(i9, i, true);
                    i9++;
                }
                i++;
            }
        }
        return a(ej4);
    }

    @DexIgnore
    public static dh4 a(ej4 ej4) {
        int c = ej4.c();
        int b = ej4.b();
        dh4 dh4 = new dh4(c, b);
        dh4.d();
        for (int i = 0; i < c; i++) {
            for (int i2 = 0; i2 < b; i2++) {
                if (ej4.a(i, i2) == 1) {
                    dh4.b(i, i2);
                }
            }
        }
        return dh4;
    }
}
