package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew6 extends fl4<c, d, b> {
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ SleepSummariesRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ fb5 d;

        @DexIgnore
        public c(int i, int i2, int i3, fb5 fb5) {
            ee7.b(fb5, "gender");
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = fb5;
        }

        @DexIgnore
        public final fb5 a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.GetRecommendedGoalUseCase", f = "GetRecommendedGoalUseCase.kt", l = {30, 32, 59, 60, 64, 65}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ew6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ew6 ew6, fb7 fb7) {
            super(fb7);
            this.this$0 = ew6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ew6(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        ee7.b(summariesRepository, "mSummaryRepository");
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSleepSessionsRepository");
        this.d = summariesRepository;
        this.e = sleepSummariesRepository;
        this.f = sleepSessionsRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetRecommendedGoalUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0166 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0167  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01ca  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x023c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0254 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x02a5 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x02c3 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.ew6.c r14, com.fossil.fb7<java.lang.Object> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.fossil.ew6.e
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.ew6$e r0 = (com.fossil.ew6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ew6$e r0 = new com.fossil.ew6$e
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r7 = com.fossil.nb7.a()
            int r1 = r0.label
            r8 = 0
            java.lang.String r9 = "GetRecommendedGoalUseCase"
            switch(r1) {
                case 0: goto L_0x00d1;
                case 1: goto L_0x00c4;
                case 2: goto L_0x00b0;
                case 3: goto L_0x0093;
                case 4: goto L_0x0070;
                case 5: goto L_0x004f;
                case 6: goto L_0x002e;
                default: goto L_0x0026;
            }
        L_0x0026:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x002e:
            java.lang.Object r14 = r0.L$6
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r14 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r14
            java.lang.Object r14 = r0.L$5
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r14 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r14
            java.lang.Object r14 = r0.L$4
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r14 = (com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals) r14
            java.lang.Object r14 = r0.L$3
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            java.lang.Object r14 = r0.L$2
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            java.lang.Object r14 = r0.L$1
            com.fossil.ew6$c r14 = (com.fossil.ew6.c) r14
            java.lang.Object r14 = r0.L$0
            com.fossil.ew6 r14 = (com.fossil.ew6) r14
            com.fossil.t87.a(r15)
            goto L_0x02c4
        L_0x004f:
            java.lang.Object r14 = r0.L$6
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r14 = (com.portfolio.platform.data.model.room.fitness.ActivitySettings) r14
            java.lang.Object r1 = r0.L$5
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r1 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r1
            java.lang.Object r2 = r0.L$4
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r2 = (com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals) r2
            java.lang.Object r3 = r0.L$3
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r4 = r0.L$2
            com.fossil.zi5 r4 = (com.fossil.zi5) r4
            java.lang.Object r5 = r0.L$1
            com.fossil.ew6$c r5 = (com.fossil.ew6.c) r5
            java.lang.Object r6 = r0.L$0
            com.fossil.ew6 r6 = (com.fossil.ew6) r6
            com.fossil.t87.a(r15)
            goto L_0x02a6
        L_0x0070:
            java.lang.Object r14 = r0.L$5
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r14 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r14
            java.lang.Object r1 = r0.L$4
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r1 = (com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals) r1
            java.lang.Object r2 = r0.L$3
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r3 = r0.L$2
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r4 = r0.L$1
            com.fossil.ew6$c r4 = (com.fossil.ew6.c) r4
            java.lang.Object r5 = r0.L$0
            com.fossil.ew6 r5 = (com.fossil.ew6) r5
            com.fossil.t87.a(r15)
        L_0x008b:
            r6 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r14
            goto L_0x0255
        L_0x0093:
            java.lang.Object r14 = r0.L$5
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r14 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r14
            java.lang.Object r1 = r0.L$4
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r1 = (com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals) r1
            java.lang.Object r2 = r0.L$3
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            java.lang.Object r3 = r0.L$2
            com.fossil.zi5 r3 = (com.fossil.zi5) r3
            java.lang.Object r4 = r0.L$1
            com.fossil.ew6$c r4 = (com.fossil.ew6.c) r4
            java.lang.Object r5 = r0.L$0
            com.fossil.ew6 r5 = (com.fossil.ew6) r5
            com.fossil.t87.a(r15)
            goto L_0x023d
        L_0x00b0:
            java.lang.Object r14 = r0.L$2
            com.fossil.zi5 r14 = (com.fossil.zi5) r14
            java.lang.Object r1 = r0.L$1
            com.fossil.ew6$c r1 = (com.fossil.ew6.c) r1
            java.lang.Object r2 = r0.L$0
            com.fossil.ew6 r2 = (com.fossil.ew6) r2
            com.fossil.t87.a(r15)
            r3 = r14
            r4 = r1
            r5 = r2
            goto L_0x016b
        L_0x00c4:
            java.lang.Object r14 = r0.L$1
            com.fossil.ew6$c r14 = (com.fossil.ew6.c) r14
            java.lang.Object r1 = r0.L$0
            com.fossil.ew6 r1 = (com.fossil.ew6) r1
            com.fossil.t87.a(r15)
            r10 = r1
            goto L_0x013e
        L_0x00d1:
            com.fossil.t87.a(r15)
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "height "
            r1.append(r2)
            if (r14 == 0) goto L_0x02d6
            int r2 = r14.b()
            r1.append(r2)
            java.lang.String r2 = " weight "
            r1.append(r2)
            int r2 = r14.d()
            r1.append(r2)
            java.lang.String r2 = " age "
            r1.append(r2)
            int r2 = r14.c()
            r1.append(r2)
            java.lang.String r2 = " gender "
            r1.append(r2)
            com.fossil.fb5 r2 = r14.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r15.d(r9, r1)
            com.portfolio.platform.data.source.SummariesRepository r1 = r13.d
            int r2 = r14.c()
            int r3 = r14.d()
            int r4 = r14.b()
            com.fossil.fb5 r15 = r14.a()
            java.lang.String r5 = r15.getValue()
            r0.L$0 = r13
            r0.L$1 = r14
            r15 = 1
            r0.label = r15
            r6 = r0
            java.lang.Object r15 = r1.downloadRecommendedGoals(r2, r3, r4, r5, r6)
            if (r15 != r7) goto L_0x013d
            return r7
        L_0x013d:
            r10 = r13
        L_0x013e:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            com.portfolio.platform.data.source.SleepSessionsRepository r1 = r10.f
            int r2 = r14.c()
            int r3 = r14.d()
            int r4 = r14.b()
            com.fossil.fb5 r5 = r14.a()
            java.lang.String r5 = r5.getValue()
            r0.L$0 = r10
            r0.L$1 = r14
            r0.L$2 = r15
            r6 = 2
            r0.label = r6
            r6 = r0
            java.lang.Object r1 = r1.downloadRecommendedGoals(r2, r3, r4, r5, r6)
            if (r1 != r7) goto L_0x0167
            return r7
        L_0x0167:
            r4 = r14
            r3 = r15
            r15 = r1
            r5 = r10
        L_0x016b:
            r2 = r15
            com.fossil.zi5 r2 = (com.fossil.zi5) r2
            boolean r14 = r3 instanceof com.fossil.bj5
            if (r14 == 0) goto L_0x018d
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = "init activity recommended goal from server"
            r14.d(r9, r15)
            r14 = r3
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            java.lang.Object r14 = r14.a()
            if (r14 == 0) goto L_0x0189
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r14 = (com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals) r14
            goto L_0x01c5
        L_0x0189:
            com.fossil.ee7.a()
            throw r8
        L_0x018d:
            boolean r14 = r3 instanceof com.fossil.yi5
            if (r14 == 0) goto L_0x02d0
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = "init activity recommended goal from client"
            r14.d(r9, r15)
            com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals r14 = new com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals
            com.fossil.ud5 r15 = com.fossil.ud5.a
            int r15 = r15.b()
            com.fossil.ud5 r1 = com.fossil.ud5.a
            int r6 = r4.c()
            int r10 = r4.d()
            int r10 = r10 / 1000
            int r11 = r4.b()
            com.fossil.fb5 r12 = r4.a()
            int r1 = r1.a(r6, r10, r11, r12)
            com.fossil.ud5 r6 = com.fossil.ud5.a
            int r6 = r6.a()
            r14.<init>(r15, r1, r6)
        L_0x01c5:
            r1 = r14
            boolean r14 = r2 instanceof com.fossil.bj5
            if (r14 == 0) goto L_0x01e5
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = "init sleepRecommendedGoals from server"
            r14.d(r9, r15)
            r14 = r2
            com.fossil.bj5 r14 = (com.fossil.bj5) r14
            java.lang.Object r14 = r14.a()
            if (r14 == 0) goto L_0x01e1
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r14 = (com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal) r14
            goto L_0x0203
        L_0x01e1:
            com.fossil.ee7.a()
            throw r8
        L_0x01e5:
            boolean r14 = r2 instanceof com.fossil.yi5
            if (r14 == 0) goto L_0x02ca
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = "init sleepRecommendedGoals from client"
            r14.d(r9, r15)
            com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal r14 = new com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal
            com.fossil.ud5 r15 = com.fossil.ud5.a
            int r6 = r4.c()
            int r15 = r15.a(r6)
            r14.<init>(r15)
        L_0x0203:
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = "Update recommendActivityGoals "
            r6.append(r8)
            r6.append(r1)
            java.lang.String r8 = " recommendSleepGoal "
            r6.append(r8)
            r6.append(r14)
            java.lang.String r6 = r6.toString()
            r15.d(r9, r6)
            com.portfolio.platform.data.source.SummariesRepository r15 = r5.d
            r0.L$0 = r5
            r0.L$1 = r4
            r0.L$2 = r3
            r0.L$3 = r2
            r0.L$4 = r1
            r0.L$5 = r14
            r6 = 3
            r0.label = r6
            java.lang.Object r15 = r15.upsertRecommendGoals(r1, r0)
            if (r15 != r7) goto L_0x023d
            return r7
        L_0x023d:
            com.portfolio.platform.data.source.SleepSessionsRepository r15 = r5.f
            r0.L$0 = r5
            r0.L$1 = r4
            r0.L$2 = r3
            r0.L$3 = r2
            r0.L$4 = r1
            r0.L$5 = r14
            r6 = 4
            r0.label = r6
            java.lang.Object r15 = r15.upsertRecommendedGoals(r14, r0)
            if (r15 != r7) goto L_0x008b
            return r7
        L_0x0255:
            com.portfolio.platform.data.model.room.fitness.ActivitySettings r14 = new com.portfolio.platform.data.model.room.fitness.ActivitySettings
            int r15 = r2.getRecommendedStepsGoal()
            int r8 = r2.getRecommendedCaloriesGoal()
            int r10 = r2.getRecommendedActiveTimeGoal()
            r14.<init>(r15, r8, r10)
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = "Update ActivitySettings "
            r8.append(r10)
            r8.append(r14)
            java.lang.String r10 = " sleepSetting "
            r8.append(r10)
            int r10 = r1.getRecommendedSleepGoal()
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r15.d(r9, r8)
            com.portfolio.platform.data.source.SummariesRepository r15 = r6.d
            r0.L$0 = r6
            r0.L$1 = r5
            r0.L$2 = r4
            r0.L$3 = r3
            r0.L$4 = r2
            r0.L$5 = r1
            r0.L$6 = r14
            r8 = 5
            r0.label = r8
            java.lang.Object r15 = r15.pushActivitySettingsToServer(r14, r0)
            if (r15 != r7) goto L_0x02a6
            return r7
        L_0x02a6:
            com.portfolio.platform.data.source.SleepSummariesRepository r15 = r6.e
            int r8 = r1.getRecommendedSleepGoal()
            r0.L$0 = r6
            r0.L$1 = r5
            r0.L$2 = r4
            r0.L$3 = r3
            r0.L$4 = r2
            r0.L$5 = r1
            r0.L$6 = r14
            r14 = 6
            r0.label = r14
            java.lang.Object r14 = r15.pushLastSleepGoalToServer(r8, r0)
            if (r14 != r7) goto L_0x02c4
            return r7
        L_0x02c4:
            com.fossil.ew6$d r14 = new com.fossil.ew6$d
            r14.<init>()
            return r14
        L_0x02ca:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        L_0x02d0:
            com.fossil.p87 r14 = new com.fossil.p87
            r14.<init>()
            throw r14
        L_0x02d6:
            com.fossil.ee7.a()
            throw r8
            switch-data {0->0x00d1, 1->0x00c4, 2->0x00b0, 3->0x0093, 4->0x0070, 5->0x004f, 6->0x002e, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ew6.a(com.fossil.ew6$c, com.fossil.fb7):java.lang.Object");
    }
}
