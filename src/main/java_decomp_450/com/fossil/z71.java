package com.fossil;

import com.fossil.u80;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z71 extends ce7 implements gd7<byte[], u80> {
    @DexIgnore
    public z71(u80.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final uf7 getOwner() {
        return te7.a(u80.a.class);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/SecondTimezoneOffsetConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public u80 invoke(byte[] bArr) {
        return ((u80.a) ((vd7) this).receiver).a(bArr);
    }
}
