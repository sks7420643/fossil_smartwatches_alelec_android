package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk0 a;
    @DexIgnore
    public /* final */ /* synthetic */ vc7 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ys1(zk0 zk0, vc7 vc7) {
        super(1);
        this.a = zk0;
        this.b = vc7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        v81 v812 = v81;
        zk0 zk0 = this.a;
        zk0.m++;
        Short sh = null;
        zk0.v = eu0.a(zk0.v, null, is0.G.a(v812.v), v812.v, 1);
        sz0 sz0 = v812.v;
        ay0 ay0 = sz0.c;
        if (ay0 == ay0.f || ay0 == ay0.n || sz0.d.c.a == z51.GATT_NULL) {
            this.a.a(v812.v);
        } else {
            if (v812 instanceof rr1) {
                sh = Short.valueOf(((rr1) v812).L);
            } else if (v812 instanceof nr1) {
                sh = Short.valueOf(((nr1) v812).F);
            }
            if (sh != null) {
                zk0.a(this.a, new sn1(sh.shortValue(), this.a.w, 0, 4), new bp1(this, v812), new ar1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
            } else {
                this.b.invoke();
            }
        }
        return i97.a;
    }
}
