package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b3 {
    @DexIgnore
    public static /* final */ ThreadLocal<TypedValue> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ int[] b; // = {-16842910};
    @DexIgnore
    public static /* final */ int[] c; // = {16842908};
    @DexIgnore
    public static /* final */ int[] d; // = {16842919};
    @DexIgnore
    public static /* final */ int[] e; // = {16842912};
    @DexIgnore
    public static /* final */ int[] f; // = new int[0];
    @DexIgnore
    public static /* final */ int[] g; // = new int[1];

    @DexIgnore
    public static int a(Context context, int i) {
        ColorStateList c2 = c(context, i);
        if (c2 != null && c2.isStateful()) {
            return c2.getColorForState(b, c2.getDefaultColor());
        }
        TypedValue a2 = a();
        context.getTheme().resolveAttribute(16842803, a2, true);
        return a(context, i, a2.getFloat());
    }

    @DexIgnore
    public static int b(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        g3 a2 = g3.a(context, (AttributeSet) null, iArr);
        try {
            return a2.a(0, 0);
        } finally {
            a2.b();
        }
    }

    @DexIgnore
    public static ColorStateList c(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        g3 a2 = g3.a(context, (AttributeSet) null, iArr);
        try {
            return a2.a(0);
        } finally {
            a2.b();
        }
    }

    @DexIgnore
    public static TypedValue a() {
        TypedValue typedValue = a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        a.set(typedValue2);
        return typedValue2;
    }

    @DexIgnore
    public static int a(Context context, int i, float f2) {
        int b2 = b(context, i);
        return e7.c(b2, Math.round(((float) Color.alpha(b2)) * f2));
    }

    @DexIgnore
    public static void a(View view, Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(h0.AppCompatTheme);
        try {
            if (!obtainStyledAttributes.hasValue(h0.AppCompatTheme_windowActionBar)) {
                Log.e("ThemeUtils", "View " + view.getClass() + " is an AppCompat widget that can only be used with a Theme.AppCompat theme (or descendant).");
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }
}
