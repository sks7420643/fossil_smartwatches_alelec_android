package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn3 extends eb2<bn3> {
    @DexIgnore
    public static /* final */ dn3 c; // = new dn3();

    @DexIgnore
    public dn3() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View a(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) cb2.g(((bn3) c.a(context)).a(cb2.a(context), i, i2, str, i3));
            } catch (Exception unused) {
                return new an3(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.eb2
    public final /* synthetic */ bn3 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof bn3 ? (bn3) queryLocalInterface : new cn3(iBinder);
    }
}
