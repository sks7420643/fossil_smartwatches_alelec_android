package com.fossil;

import android.content.res.Resources;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf5 {
    @DexIgnore
    public static byte[] d;
    @DexIgnore
    public static byte[] e;
    @DexIgnore
    public ByteArrayInputStream a; // = null;
    @DexIgnore
    public xf5 b; // = null;
    @DexIgnore
    public vf5 c; // = null;

    @DexIgnore
    public yf5(byte[] bArr) {
        this.a = new ByteArrayInputStream(bArr);
    }

    @DexIgnore
    public static void a(String str) {
    }

    @DexIgnore
    public static boolean a(int i) {
        return (i >= 32 && i <= 126) || (i >= 128 && i <= 255) || i == 9 || i == 10 || i == 13;
    }

    @DexIgnore
    public static boolean b(int i) {
        if (!(i < 33 || i > 126 || i == 34 || i == 44 || i == 47 || i == 123 || i == 125 || i == 40 || i == 41)) {
            switch (i) {
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                    break;
                default:
                    switch (i) {
                        case 91:
                        case 92:
                        case 93:
                            break;
                        default:
                            return true;
                    }
            }
        }
        return false;
    }

    @DexIgnore
    public static byte[] b(ByteArrayInputStream byteArrayInputStream, int i) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        if (1 == i && 34 == read) {
            byteArrayInputStream.mark(1);
        } else if (i == 0 && 127 == read) {
            byteArrayInputStream.mark(1);
        } else {
            byteArrayInputStream.reset();
        }
        return a(byteArrayInputStream, i);
    }

    @DexIgnore
    public static qf5 c(ByteArrayInputStream byteArrayInputStream) {
        int i;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read() & 255;
        if (read == 0) {
            return new qf5("");
        }
        byteArrayInputStream.reset();
        if (read < 32) {
            i(byteArrayInputStream);
            i = g(byteArrayInputStream);
        } else {
            i = 0;
        }
        byte[] b2 = b(byteArrayInputStream, 0);
        if (i == 0) {
            return new qf5(b2);
        }
        try {
            return new qf5(i, b2);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static long d(ByteArrayInputStream byteArrayInputStream) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        if (read > 127) {
            return (long) g(byteArrayInputStream);
        }
        return e(byteArrayInputStream);
    }

    @DexIgnore
    public static long e(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 8) {
            long j = 0;
            for (int i = 0; i < read; i++) {
                j = (j << 8) + ((long) (byteArrayInputStream.read() & 255));
            }
            return j;
        }
        throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
    }

    @DexIgnore
    public static vf5 f(ByteArrayInputStream byteArrayInputStream) {
        if (byteArrayInputStream == null) {
            return null;
        }
        int h = h(byteArrayInputStream);
        vf5 vf5 = new vf5();
        for (int i = 0; i < h; i++) {
            int h2 = h(byteArrayInputStream);
            int h3 = h(byteArrayInputStream);
            zf5 zf5 = new zf5();
            int available = byteArrayInputStream.available();
            if (available <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            byte[] a2 = a(byteArrayInputStream, hashMap);
            if (a2 != null) {
                zf5.e(a2);
            } else {
                zf5.e(wf5.a[0].getBytes());
            }
            byte[] bArr = (byte[]) hashMap.get(151);
            if (bArr != null) {
                zf5.h(bArr);
            }
            Integer num = (Integer) hashMap.get(129);
            if (num != null) {
                zf5.a(num.intValue());
            }
            int available2 = h2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                if (!a(byteArrayInputStream, zf5, available2)) {
                    return null;
                }
            } else if (available2 < 0) {
                return null;
            }
            if (zf5.b() == null && zf5.f() == null && zf5.e() == null && zf5.a() == null) {
                zf5.c(Long.toOctalString(System.currentTimeMillis()).getBytes());
            }
            if (h3 > 0) {
                byte[] bArr2 = new byte[h3];
                String str = new String(zf5.d());
                byteArrayInputStream.read(bArr2, 0, h3);
                if (str.equalsIgnoreCase("application/vnd.wap.multipart.alternative")) {
                    zf5 = f(new ByteArrayInputStream(bArr2)).a(0);
                } else {
                    byte[] c2 = zf5.c();
                    if (c2 != null) {
                        String str2 = new String(c2);
                        if (str2.equalsIgnoreCase("base64")) {
                            bArr2 = nf5.a(bArr2);
                        } else if (str2.equalsIgnoreCase("quoted-printable")) {
                            bArr2 = ag5.a(bArr2);
                        }
                    }
                    if (bArr2 == null) {
                        a("Decode part data error!");
                        return null;
                    }
                    zf5.f(bArr2);
                }
            }
            if (a(zf5) == 0) {
                vf5.a(0, zf5);
            } else {
                vf5.a(zf5);
            }
        }
        return vf5;
    }

    @DexIgnore
    public static int g(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 127;
    }

    @DexIgnore
    public static int h(ByteArrayInputStream byteArrayInputStream) {
        int i = 0;
        int read = byteArrayInputStream.read();
        if (read == -1) {
            return read;
        }
        while ((read & 128) != 0) {
            i = (i << 7) | (read & 127);
            read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
        }
        return (i << 7) | (read & 127);
    }

    @DexIgnore
    public static int i(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 30) {
            return read;
        }
        if (read == 31) {
            return h(byteArrayInputStream);
        }
        throw new RuntimeException("Value length > LENGTH_QUOTE!");
    }

    @DexIgnore
    public rf5 a() {
        ByteArrayInputStream byteArrayInputStream = this.a;
        if (byteArrayInputStream == null) {
            return null;
        }
        xf5 a2 = a(byteArrayInputStream);
        this.b = a2;
        if (a2 == null) {
            return null;
        }
        int d2 = a2.d(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (!a(this.b)) {
            a("check mandatory headers failed!");
            return null;
        }
        if (128 == d2 || 132 == d2) {
            vf5 f = f(this.a);
            this.c = f;
            if (f == null) {
                return null;
            }
        }
        switch (d2) {
            case 128:
                return new fg5(this.b, this.c);
            case 129:
                return new eg5(this.b);
            case 130:
                return new tf5(this.b);
            case 131:
                return new uf5(this.b);
            case 132:
                dg5 dg5 = new dg5(this.b, this.c);
                byte[] b2 = dg5.b();
                if (b2 == null) {
                    return null;
                }
                String str = new String(b2);
                if (str.equals("application/vnd.wap.multipart.mixed") || str.equals("application/vnd.wap.multipart.related") || str.equals("application/vnd.wap.multipart.alternative")) {
                    return dg5;
                }
                if (!str.equals("application/vnd.wap.multipart.alternative")) {
                    return null;
                }
                zf5 a3 = this.c.a(0);
                this.c.a();
                this.c.a(0, a3);
                return dg5;
            case 133:
                return new mf5(this.b);
            case 134:
                return new pf5(this.b);
            case 135:
                return new cg5(this.b);
            case 136:
                return new bg5(this.b);
            default:
                a("Parser doesn't support this message type in this version!");
                return null;
        }
    }

    @DexIgnore
    public static int b(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 255;
    }

    @DexIgnore
    public static int c(ByteArrayInputStream byteArrayInputStream, int i) {
        int read = byteArrayInputStream.read(new byte[i], 0, i);
        if (read < i) {
            return -1;
        }
        return read;
    }

    @DexIgnore
    public xf5 a(ByteArrayInputStream byteArrayInputStream) {
        qf5 qf5;
        byte[] f;
        if (byteArrayInputStream == null) {
            return null;
        }
        xf5 xf5 = new xf5();
        boolean z = true;
        while (z && byteArrayInputStream.available() > 0) {
            byteArrayInputStream.mark(1);
            int b2 = b(byteArrayInputStream);
            if (b2 < 32 || b2 > 127) {
                switch (b2) {
                    case 129:
                    case 130:
                    case 151:
                        qf5 c2 = c(byteArrayInputStream);
                        if (c2 == null) {
                            continue;
                        } else {
                            byte[] f2 = c2.f();
                            if (f2 != null) {
                                String str = new String(f2);
                                int indexOf = str.indexOf(47);
                                if (indexOf > 0) {
                                    str = str.substring(0, indexOf);
                                }
                                try {
                                    c2.b(str.getBytes());
                                } catch (NullPointerException unused) {
                                    a("null pointer error!");
                                    return null;
                                }
                            }
                            try {
                                xf5.a(c2, b2);
                                break;
                            } catch (NullPointerException unused2) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused3) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    case 131:
                    case 139:
                    case 152:
                    case 158:
                    case 183:
                    case 184:
                    case 185:
                    case 189:
                    case FacebookRequestErrorClassification.EC_INVALID_TOKEN:
                        byte[] b3 = b(byteArrayInputStream, 0);
                        if (b3 != null) {
                            try {
                                xf5.a(b3, b2);
                                break;
                            } catch (NullPointerException unused4) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused5) {
                                a(b2 + "is not Text-String header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 132:
                        HashMap hashMap = new HashMap();
                        byte[] a2 = a(byteArrayInputStream, hashMap);
                        if (a2 != null) {
                            try {
                                xf5.a(a2, 132);
                            } catch (NullPointerException unused6) {
                                a("null pointer error!");
                            } catch (RuntimeException unused7) {
                                a(b2 + "is not Text-String header field!");
                                return null;
                            }
                        }
                        e = (byte[]) hashMap.get(153);
                        d = (byte[]) hashMap.get(131);
                        z = false;
                        continue;
                    case 133:
                    case 142:
                    case 159:
                        try {
                            xf5.a(e(byteArrayInputStream), b2);
                            continue;
                        } catch (RuntimeException unused8) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 134:
                    case 143:
                    case 144:
                    case 145:
                    case 146:
                    case 148:
                    case 149:
                    case 153:
                    case 155:
                    case 156:
                    case 162:
                    case 163:
                    case 165:
                    case 167:
                    case 169:
                    case 171:
                    case 177:
                    case 180:
                    case 186:
                    case 187:
                    case 188:
                    case 191:
                        int b4 = b(byteArrayInputStream);
                        try {
                            xf5.a(b4, b2);
                            continue;
                        } catch (kf5 unused9) {
                            a("Set invalid Octet value: " + b4 + " into the header filed: " + b2);
                            return null;
                        } catch (RuntimeException unused10) {
                            a(b2 + "is not Octet header field!");
                            return null;
                        }
                    case 135:
                    case 136:
                    case 157:
                        i(byteArrayInputStream);
                        int b5 = b(byteArrayInputStream);
                        try {
                            long e2 = e(byteArrayInputStream);
                            if (129 == b5) {
                                e2 += System.currentTimeMillis() / 1000;
                            }
                            try {
                                xf5.a(e2, b2);
                                continue;
                            } catch (RuntimeException unused11) {
                                a(b2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused12) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 137:
                        i(byteArrayInputStream);
                        if (128 == b(byteArrayInputStream)) {
                            qf5 = c(byteArrayInputStream);
                            if (!(qf5 == null || (f = qf5.f()) == null)) {
                                String str2 = new String(f);
                                int indexOf2 = str2.indexOf(47);
                                if (indexOf2 > 0) {
                                    str2 = str2.substring(0, indexOf2);
                                }
                                try {
                                    qf5.b(str2.getBytes());
                                } catch (NullPointerException unused13) {
                                    a("null pointer error!");
                                    return null;
                                }
                            }
                        } else {
                            try {
                                qf5 = new qf5("insert-address-token".getBytes());
                            } catch (NullPointerException unused14) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                        try {
                            xf5.b(qf5, 137);
                            continue;
                        } catch (NullPointerException unused15) {
                            a("null pointer error!");
                            break;
                        } catch (RuntimeException unused16) {
                            a(b2 + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    case 138:
                        byteArrayInputStream.mark(1);
                        int b6 = b(byteArrayInputStream);
                        if (b6 >= 128) {
                            if (128 != b6) {
                                if (129 != b6) {
                                    if (130 != b6) {
                                        if (131 != b6) {
                                            break;
                                        } else {
                                            xf5.a("auto".getBytes(), 138);
                                            break;
                                        }
                                    } else {
                                        xf5.a("informational".getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    xf5.a("advertisement".getBytes(), 138);
                                    break;
                                }
                            } else {
                                try {
                                    xf5.a("personal".getBytes(), 138);
                                    continue;
                                } catch (NullPointerException unused17) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused18) {
                                    a(b2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        } else {
                            byteArrayInputStream.reset();
                            byte[] b7 = b(byteArrayInputStream, 0);
                            if (b7 == null) {
                                break;
                            } else {
                                try {
                                    xf5.a(b7, 138);
                                    break;
                                } catch (NullPointerException unused19) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused20) {
                                    a(b2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        }
                    case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL:
                        int b8 = b(byteArrayInputStream);
                        switch (b8) {
                            case 137:
                            case 138:
                            case 139:
                            case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL:
                            case 141:
                            case 142:
                            case 143:
                            case 144:
                            case 145:
                            case 146:
                            case 147:
                            case 148:
                            case 149:
                            case 150:
                            case 151:
                                return null;
                            default:
                                try {
                                    xf5.a(b8, b2);
                                    continue;
                                    continue;
                                } catch (kf5 unused21) {
                                    a("Set invalid Octet value: " + b8 + " into the header filed: " + b2);
                                    return null;
                                } catch (RuntimeException unused22) {
                                    a(b2 + "is not Octet header field!");
                                    return null;
                                }
                        }
                    case 141:
                        int g = g(byteArrayInputStream);
                        try {
                            xf5.a(g, 141);
                            continue;
                        } catch (kf5 unused23) {
                            a("Set invalid Octet value: " + g + " into the header filed: " + b2);
                            return null;
                        } catch (RuntimeException unused24) {
                            a(b2 + "is not Octet header field!");
                            return null;
                        }
                    case 147:
                    case 150:
                    case 154:
                    case 166:
                    case 181:
                    case 182:
                        qf5 c3 = c(byteArrayInputStream);
                        if (c3 != null) {
                            try {
                                xf5.b(c3, b2);
                                break;
                            } catch (NullPointerException unused25) {
                                a("null pointer error!");
                                break;
                            } catch (RuntimeException unused26) {
                                a(b2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 160:
                        i(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            qf5 c4 = c(byteArrayInputStream);
                            if (c4 != null) {
                                try {
                                    xf5.b(c4, 160);
                                    break;
                                } catch (NullPointerException unused27) {
                                    a("null pointer error!");
                                    break;
                                } catch (RuntimeException unused28) {
                                    a(b2 + "is not Encoded-String-Value header field!");
                                    return null;
                                }
                            } else {
                                continue;
                            }
                        } catch (RuntimeException unused29) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 161:
                        i(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            try {
                                xf5.a(e(byteArrayInputStream), 161);
                                continue;
                            } catch (RuntimeException unused30) {
                                a(b2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused31) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 164:
                        i(byteArrayInputStream);
                        b(byteArrayInputStream);
                        c(byteArrayInputStream);
                        continue;
                    case DateTimeConstants.HOURS_PER_WEEK /*{ENCODED_INT: 168}*/:
                    case 174:
                    case 176:
                    default:
                        a("Unknown header");
                        continue;
                    case 170:
                    case 172:
                        i(byteArrayInputStream);
                        b(byteArrayInputStream);
                        try {
                            d(byteArrayInputStream);
                            continue;
                        } catch (RuntimeException unused32) {
                            a(b2 + " is not Integer-Value");
                            return null;
                        }
                    case 173:
                    case 175:
                    case 179:
                        try {
                            xf5.a(d(byteArrayInputStream), b2);
                            continue;
                        } catch (RuntimeException unused33) {
                            a(b2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 178:
                        a(byteArrayInputStream, (HashMap<Integer, Object>) null);
                        continue;
                }
            } else {
                byteArrayInputStream.reset();
                b(byteArrayInputStream, 0);
            }
        }
        return xf5;
    }

    @DexIgnore
    public static byte[] a(ByteArrayInputStream byteArrayInputStream, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read = byteArrayInputStream.read();
        while (-1 != read && read != 0) {
            if (i == 2) {
                if (b(read)) {
                    byteArrayOutputStream.write(read);
                }
            } else if (a(read)) {
                byteArrayOutputStream.write(read);
            }
            read = byteArrayInputStream.read();
        }
        if (byteArrayOutputStream.size() > 0) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    @DexIgnore
    public static void a(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        int available;
        int intValue;
        int available2 = byteArrayInputStream.available();
        int intValue2 = num.intValue();
        while (intValue2 > 0) {
            int read = byteArrayInputStream.read();
            intValue2--;
            if (read != 129) {
                if (read != 131) {
                    if (read == 133 || read == 151) {
                        byte[] b2 = b(byteArrayInputStream, 0);
                        if (!(b2 == null || hashMap == null)) {
                            hashMap.put(151, b2);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    } else {
                        if (read != 153) {
                            if (read != 137) {
                                if (read != 138) {
                                    if (-1 == c(byteArrayInputStream, intValue2)) {
                                        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
                                    } else {
                                        intValue2 = 0;
                                    }
                                }
                            }
                        }
                        byte[] b3 = b(byteArrayInputStream, 0);
                        if (!(b3 == null || hashMap == null)) {
                            hashMap.put(153, b3);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    }
                }
                byteArrayInputStream.mark(1);
                int b4 = b(byteArrayInputStream);
                byteArrayInputStream.reset();
                if (b4 > 127) {
                    int g = g(byteArrayInputStream);
                    String[] strArr = wf5.a;
                    if (g < strArr.length) {
                        hashMap.put(131, strArr[g].getBytes());
                    }
                } else {
                    byte[] b5 = b(byteArrayInputStream, 0);
                    if (!(b5 == null || hashMap == null)) {
                        hashMap.put(131, b5);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            } else {
                byteArrayInputStream.mark(1);
                int b6 = b(byteArrayInputStream);
                byteArrayInputStream.reset();
                if ((b6 <= 32 || b6 >= 127) && b6 != 0) {
                    int d2 = (int) d(byteArrayInputStream);
                    if (hashMap != null) {
                        hashMap.put(129, Integer.valueOf(d2));
                    }
                } else {
                    try {
                        hashMap.put(129, Integer.valueOf(of5.a(new String(b(byteArrayInputStream, 0)))));
                    } catch (UnsupportedEncodingException unused) {
                        hashMap.put(129, 0);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            }
            intValue2 = intValue - (available2 - available);
        }
        if (intValue2 != 0) {
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
        }
    }

    @DexIgnore
    public static byte[] a(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        byte[] bArr;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        int i = read & 255;
        if (i < 32) {
            int i2 = i(byteArrayInputStream);
            int available = byteArrayInputStream.available();
            byteArrayInputStream.mark(1);
            int read2 = byteArrayInputStream.read();
            byteArrayInputStream.reset();
            int i3 = read2 & 255;
            if (i3 >= 32 && i3 <= 127) {
                bArr = b(byteArrayInputStream, 0);
            } else if (i3 > 127) {
                int g = g(byteArrayInputStream);
                String[] strArr = wf5.a;
                if (g < strArr.length) {
                    bArr = strArr[g].getBytes();
                } else {
                    byteArrayInputStream.reset();
                    bArr = b(byteArrayInputStream, 0);
                }
            } else {
                FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt content-type");
                return wf5.a[0].getBytes();
            }
            int available2 = i2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                a(byteArrayInputStream, hashMap, Integer.valueOf(available2));
            }
            if (available2 >= 0) {
                return bArr;
            }
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt MMS message");
            return wf5.a[0].getBytes();
        } else if (i <= 127) {
            return b(byteArrayInputStream, 0);
        } else {
            return wf5.a[g(byteArrayInputStream)].getBytes();
        }
    }

    @DexIgnore
    public static boolean a(ByteArrayInputStream byteArrayInputStream, zf5 zf5, int i) {
        int available;
        int available2 = byteArrayInputStream.available();
        int i2 = i;
        while (i2 > 0) {
            int read = byteArrayInputStream.read();
            i2--;
            if (read > 127) {
                if (read != 142) {
                    if (read != 174) {
                        if (read == 192) {
                            byte[] b2 = b(byteArrayInputStream, 1);
                            if (b2 != null) {
                                zf5.b(b2);
                            }
                            available = byteArrayInputStream.available();
                        } else if (read != 197) {
                            if (-1 == c(byteArrayInputStream, i2)) {
                                FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                                return false;
                            }
                            i2 = 0;
                        }
                    }
                    if (Resources.getSystem().getBoolean(Resources.getSystem().getIdentifier("config_mms_content_disposition_support", "id", "android"))) {
                        int i3 = i(byteArrayInputStream);
                        byteArrayInputStream.mark(1);
                        int available3 = byteArrayInputStream.available();
                        int read2 = byteArrayInputStream.read();
                        if (read2 == 128) {
                            zf5.a(zf5.c);
                        } else if (read2 == 129) {
                            zf5.a(zf5.d);
                        } else if (read2 == 130) {
                            zf5.a(zf5.e);
                        } else {
                            byteArrayInputStream.reset();
                            zf5.a(b(byteArrayInputStream, 0));
                        }
                        if (available3 - byteArrayInputStream.available() < i3) {
                            if (byteArrayInputStream.read() == 152) {
                                zf5.g(b(byteArrayInputStream, 0));
                            }
                            int available4 = available3 - byteArrayInputStream.available();
                            if (available4 < i3) {
                                int i4 = i3 - available4;
                                byteArrayInputStream.read(new byte[i4], 0, i4);
                            }
                        }
                        available = byteArrayInputStream.available();
                    }
                } else {
                    byte[] b3 = b(byteArrayInputStream, 0);
                    if (b3 != null) {
                        zf5.c(b3);
                    }
                    available = byteArrayInputStream.available();
                }
            } else if (read < 32 || read > 127) {
                if (-1 == c(byteArrayInputStream, i2)) {
                    FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                    return false;
                }
                i2 = 0;
            } else {
                byte[] b4 = b(byteArrayInputStream, 0);
                byte[] b5 = b(byteArrayInputStream, 0);
                if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(b4))) {
                    zf5.d(b5);
                }
                available = byteArrayInputStream.available();
            }
            i2 = i - (available2 - available);
        }
        if (i2 == 0) {
            return true;
        }
        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
        return false;
    }

    @DexIgnore
    public static int a(zf5 zf5) {
        byte[] d2;
        byte[] a2;
        if (d == null && e == null) {
            return 1;
        }
        if (e != null && (a2 = zf5.a()) != null && true == Arrays.equals(e, a2)) {
            return 0;
        }
        if (d == null || (d2 = zf5.d()) == null || true != Arrays.equals(d, d2)) {
            return 1;
        }
        return 0;
    }

    @DexIgnore
    public static boolean a(xf5 xf5) {
        if (xf5 == null) {
            return false;
        }
        int d2 = xf5.d(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (xf5.d(141) == 0) {
            return false;
        }
        switch (d2) {
            case 128:
                if (xf5.e(132) == null || xf5.a(137) == null || xf5.e(152) == null) {
                    return false;
                }
                return true;
            case 129:
                if (xf5.d(146) == 0 || xf5.e(152) == null) {
                    return false;
                }
                return true;
            case 130:
                if (xf5.e(131) == null || -1 == xf5.c(136) || xf5.e(138) == null || -1 == xf5.c(142) || xf5.e(152) == null) {
                    return false;
                }
                return true;
            case 131:
                if (xf5.d(149) == 0 || xf5.e(152) == null) {
                    return false;
                }
                return true;
            case 132:
                if (xf5.e(132) == null || -1 == xf5.c(133)) {
                    return false;
                }
                return true;
            case 133:
                if (xf5.e(152) == null) {
                    return false;
                }
                return true;
            case 134:
                if (-1 == xf5.c(133) || xf5.e(139) == null || xf5.d(149) == 0 || xf5.b(151) == null) {
                    return false;
                }
                return true;
            case 135:
                if (xf5.a(137) == null || xf5.e(139) == null || xf5.d(155) == 0 || xf5.b(151) == null) {
                    return false;
                }
                return true;
            case 136:
                if (-1 == xf5.c(133) || xf5.a(137) == null || xf5.e(139) == null || xf5.d(155) == 0 || xf5.b(151) == null) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }
}
