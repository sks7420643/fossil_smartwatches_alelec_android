package com.fossil;

import com.fossil.ay1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class pv1 implements ay1.a {
    @DexIgnore
    public /* final */ qv1 a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ ku1 c;

    @DexIgnore
    public pv1(qv1 qv1, pu1 pu1, ku1 ku1) {
        this.a = qv1;
        this.b = pu1;
        this.c = ku1;
    }

    @DexIgnore
    public static ay1.a a(qv1 qv1, pu1 pu1, ku1 ku1) {
        return new pv1(qv1, pu1, ku1);
    }

    @DexIgnore
    @Override // com.fossil.ay1.a
    public Object a() {
        return qv1.a(this.a, this.b, this.c);
    }
}
