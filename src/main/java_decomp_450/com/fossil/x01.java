package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x01 extends s81 {
    @DexIgnore
    public /* final */ yg0 T;

    @DexIgnore
    public x01(ri1 ri1, en0 en0, yg0 yg0) {
        super(ri1, en0, wm0.w0, true, gq0.b.b(ri1.u, pb1.UI_PACKAGE_FILE), yg0.f(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = yg0;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.J4, this.T.a());
    }
}
