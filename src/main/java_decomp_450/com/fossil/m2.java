package com.fossil;

import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m2 {
    @DexIgnore
    public TextView a;
    @DexIgnore
    public TextClassifier b;

    @DexIgnore
    public m2(TextView textView) {
        e9.a(textView);
        this.a = textView;
    }

    @DexIgnore
    public void a(TextClassifier textClassifier) {
        this.b = textClassifier;
    }

    @DexIgnore
    public TextClassifier a() {
        TextClassifier textClassifier = this.b;
        if (textClassifier != null) {
            return textClassifier;
        }
        TextClassificationManager textClassificationManager = (TextClassificationManager) this.a.getContext().getSystemService(TextClassificationManager.class);
        if (textClassificationManager != null) {
            return textClassificationManager.getTextClassifier();
        }
        return TextClassifier.NO_OP;
    }
}
