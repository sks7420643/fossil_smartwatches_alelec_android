package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr1 extends fe7 implements kd7<zk0, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ dn0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hr1(dn0 dn0) {
        super(2);
        this.a = dn0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(zk0 zk0, Float f) {
        float floatValue = f.floatValue();
        if (floatValue == 1.0f) {
            this.a.a(floatValue);
        } else {
            dn0 dn0 = this.a;
            dn0.a((floatValue * this.a.J) + dn0.I);
        }
        return i97.a;
    }
}
