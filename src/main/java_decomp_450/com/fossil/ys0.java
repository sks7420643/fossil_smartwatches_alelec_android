package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys0 {
    @DexIgnore
    public /* synthetic */ ys0(zd7 zd7) {
    }

    @DexIgnore
    public final tu0 a(byte b) {
        tu0 tu0;
        tu0[] values = tu0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                tu0 = null;
                break;
            }
            tu0 = values[i];
            if (tu0.b == b) {
                break;
            }
            i++;
        }
        return tu0 != null ? tu0 : tu0.h;
    }
}
