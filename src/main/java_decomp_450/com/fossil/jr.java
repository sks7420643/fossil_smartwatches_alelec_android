package com.fossil;

import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr implements pr<Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public jr(Context context) {
        ee7.b(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.rq, java.lang.Object, com.fossil.rt, com.fossil.ir, com.fossil.fb7] */
    @Override // com.fossil.pr
    public /* bridge */ /* synthetic */ Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7 fb7) {
        return a(rqVar, uri, rtVar, irVar, (fb7<? super or>) fb7);
    }

    @DexIgnore
    public String b(Uri uri) {
        ee7.b(uri, "data");
        String uri2 = uri.toString();
        ee7.a((Object) uri2, "data.toString()");
        return uri2;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        return ee7.a(uri.getScheme(), "file") && ee7.a(iu.a(uri), "android_asset");
    }

    @DexIgnore
    public Object a(rq rqVar, Uri uri, rt rtVar, ir irVar, fb7<? super or> fb7) {
        List<String> pathSegments = uri.getPathSegments();
        ee7.a((Object) pathSegments, "data.pathSegments");
        String a2 = ea7.a(ea7.c(pathSegments, 1), "/", null, null, 0, null, null, 62, null);
        InputStream open = this.a.getAssets().open(a2);
        ee7.a((Object) open, "context.assets.open(path)");
        ar7 a3 = ir7.a(ir7.a(open));
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        ee7.a((Object) singleton, "MimeTypeMap.getSingleton()");
        return new vr(a3, iu.a(singleton, a2), br.DISK);
    }
}
