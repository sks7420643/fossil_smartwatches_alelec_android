package com.fossil;

import com.fossil.gy;
import com.fossil.oy;
import com.fossil.w50;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ky<R> implements gy.b<R>, w50.f {
    @DexIgnore
    public static /* final */ c D; // = new c();
    @DexIgnore
    public oy<?> A;
    @DexIgnore
    public gy<R> B;
    @DexIgnore
    public volatile boolean C;
    @DexIgnore
    public /* final */ e a;
    @DexIgnore
    public /* final */ y50 b;
    @DexIgnore
    public /* final */ oy.a c;
    @DexIgnore
    public /* final */ b9<ky<?>> d;
    @DexIgnore
    public /* final */ c e;
    @DexIgnore
    public /* final */ ly f;
    @DexIgnore
    public /* final */ xz g;
    @DexIgnore
    public /* final */ xz h;
    @DexIgnore
    public /* final */ xz i;
    @DexIgnore
    public /* final */ xz j;
    @DexIgnore
    public /* final */ AtomicInteger p;
    @DexIgnore
    public yw q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public uy<?> v;
    @DexIgnore
    public sw w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public py y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ s40 a;

        @DexIgnore
        public a(s40 s40) {
            this.a = s40;
        }

        @DexIgnore
        public void run() {
            synchronized (this.a.b()) {
                synchronized (ky.this) {
                    if (ky.this.a.a(this.a)) {
                        ky.this.a(this.a);
                    }
                    ky.this.c();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ s40 a;

        @DexIgnore
        public b(s40 s40) {
            this.a = s40;
        }

        @DexIgnore
        public void run() {
            synchronized (this.a.b()) {
                synchronized (ky.this) {
                    if (ky.this.a.a(this.a)) {
                        ky.this.A.a();
                        ky.this.b(this.a);
                        ky.this.c(this.a);
                    }
                    ky.this.c();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <R> oy<R> a(uy<R> uyVar, boolean z, yw ywVar, oy.a aVar) {
            return new oy<>(uyVar, z, true, ywVar, aVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ s40 a;
        @DexIgnore
        public /* final */ Executor b;

        @DexIgnore
        public d(s40 s40, Executor executor) {
            this.a = s40;
            this.b = executor;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Iterable<d> {
        @DexIgnore
        public /* final */ List<d> a;

        @DexIgnore
        public e() {
            this(new ArrayList(2));
        }

        @DexIgnore
        public static d c(s40 s40) {
            return new d(s40, p50.a());
        }

        @DexIgnore
        public void a(s40 s40, Executor executor) {
            this.a.add(new d(s40, executor));
        }

        @DexIgnore
        public void b(s40 s40) {
            this.a.remove(c(s40));
        }

        @DexIgnore
        public void clear() {
            this.a.clear();
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.a.isEmpty();
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<d> iterator() {
            return this.a.iterator();
        }

        @DexIgnore
        public int size() {
            return this.a.size();
        }

        @DexIgnore
        public e(List<d> list) {
            this.a = list;
        }

        @DexIgnore
        public boolean a(s40 s40) {
            return this.a.contains(c(s40));
        }

        @DexIgnore
        public e a() {
            return new e(new ArrayList(this.a));
        }
    }

    @DexIgnore
    public ky(xz xzVar, xz xzVar2, xz xzVar3, xz xzVar4, ly lyVar, oy.a aVar, b9<ky<?>> b9Var) {
        this(xzVar, xzVar2, xzVar3, xzVar4, lyVar, aVar, b9Var, D);
    }

    @DexIgnore
    public synchronized ky<R> a(yw ywVar, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.q = ywVar;
        this.r = z2;
        this.s = z3;
        this.t = z4;
        this.u = z5;
        return this;
    }

    @DexIgnore
    public synchronized void b(gy<R> gyVar) {
        this.B = gyVar;
        (gyVar.n() ? this.g : d()).execute(gyVar);
    }

    @DexIgnore
    public synchronized void c(s40 s40) {
        boolean z2;
        this.b.a();
        this.a.b(s40);
        if (this.a.isEmpty()) {
            b();
            if (!this.x) {
                if (!this.z) {
                    z2 = false;
                    if (z2 && this.p.get() == 0) {
                        i();
                    }
                }
            }
            z2 = true;
            i();
        }
    }

    @DexIgnore
    public final xz d() {
        if (this.s) {
            return this.i;
        }
        return this.t ? this.j : this.h;
    }

    @DexIgnore
    public final boolean e() {
        return this.z || this.x || this.C;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.f.a(r4, r1, null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.ky.a(r4, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.fossil.y50 r0 = r4.b     // Catch:{ all -> 0x0066 }
            r0.a()     // Catch:{ all -> 0x0066 }
            boolean r0 = r4.C     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x000f
            r4.i()     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            return
        L_0x000f:
            com.fossil.ky$e r0 = r4.a     // Catch:{ all -> 0x0066 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x005e
            boolean r0 = r4.z     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0056
            r0 = 1
            r4.z = r0     // Catch:{ all -> 0x0066 }
            com.fossil.yw r1 = r4.q     // Catch:{ all -> 0x0066 }
            com.fossil.ky$e r2 = r4.a     // Catch:{ all -> 0x0066 }
            com.fossil.ky$e r2 = r2.a()     // Catch:{ all -> 0x0066 }
            int r3 = r2.size()     // Catch:{ all -> 0x0066 }
            int r3 = r3 + r0
            r4.a(r3)     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            com.fossil.ly r0 = r4.f
            r3 = 0
            r0.a(r4, r1, r3)
            java.util.Iterator r0 = r2.iterator()
        L_0x0039:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0052
            java.lang.Object r1 = r0.next()
            com.fossil.ky$d r1 = (com.fossil.ky.d) r1
            java.util.concurrent.Executor r2 = r1.b
            com.fossil.ky$a r3 = new com.fossil.ky$a
            com.fossil.s40 r1 = r1.a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x0039
        L_0x0052:
            r4.c()
            return
        L_0x0056:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Already failed once"
            r0.<init>(r1)
            throw r0
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Received an exception without any callbacks to notify"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ky.f():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.f.a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.ky.b(r5, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.fossil.y50 r0 = r5.b     // Catch:{ all -> 0x007c }
            r0.a()     // Catch:{ all -> 0x007c }
            boolean r0 = r5.C     // Catch:{ all -> 0x007c }
            if (r0 == 0) goto L_0x0014
            com.fossil.uy<?> r0 = r5.v     // Catch:{ all -> 0x007c }
            r0.b()     // Catch:{ all -> 0x007c }
            r5.i()     // Catch:{ all -> 0x007c }
            monitor-exit(r5)     // Catch:{ all -> 0x007c }
            return
        L_0x0014:
            com.fossil.ky$e r0 = r5.a     // Catch:{ all -> 0x007c }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x0074
            boolean r0 = r5.x     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x006c
            com.fossil.ky$c r0 = r5.e     // Catch:{ all -> 0x007c }
            com.fossil.uy<?> r1 = r5.v     // Catch:{ all -> 0x007c }
            boolean r2 = r5.r     // Catch:{ all -> 0x007c }
            com.fossil.yw r3 = r5.q     // Catch:{ all -> 0x007c }
            com.fossil.oy$a r4 = r5.c     // Catch:{ all -> 0x007c }
            com.fossil.oy r0 = r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x007c }
            r5.A = r0     // Catch:{ all -> 0x007c }
            r0 = 1
            r5.x = r0     // Catch:{ all -> 0x007c }
            com.fossil.ky$e r1 = r5.a     // Catch:{ all -> 0x007c }
            com.fossil.ky$e r1 = r1.a()     // Catch:{ all -> 0x007c }
            int r2 = r1.size()     // Catch:{ all -> 0x007c }
            int r2 = r2 + r0
            r5.a(r2)     // Catch:{ all -> 0x007c }
            com.fossil.yw r0 = r5.q     // Catch:{ all -> 0x007c }
            com.fossil.oy<?> r2 = r5.A     // Catch:{ all -> 0x007c }
            monitor-exit(r5)     // Catch:{ all -> 0x007c }
            com.fossil.ly r3 = r5.f
            r3.a(r5, r0, r2)
            java.util.Iterator r0 = r1.iterator()
        L_0x004f:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0068
            java.lang.Object r1 = r0.next()
            com.fossil.ky$d r1 = (com.fossil.ky.d) r1
            java.util.concurrent.Executor r2 = r1.b
            com.fossil.ky$b r3 = new com.fossil.ky$b
            com.fossil.s40 r1 = r1.a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x004f
        L_0x0068:
            r5.c()
            return
        L_0x006c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Already have resource"
            r0.<init>(r1)
            throw r0
        L_0x0074:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Received a resource without any callbacks to notify"
            r0.<init>(r1)
            throw r0
        L_0x007c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ky.g():void");
    }

    @DexIgnore
    public boolean h() {
        return this.u;
    }

    @DexIgnore
    public final synchronized void i() {
        if (this.q != null) {
            this.a.clear();
            this.q = null;
            this.A = null;
            this.v = null;
            this.z = false;
            this.C = false;
            this.x = false;
            this.B.a(false);
            this.B = null;
            this.y = null;
            this.w = null;
            this.d.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public ky(xz xzVar, xz xzVar2, xz xzVar3, xz xzVar4, ly lyVar, oy.a aVar, b9<ky<?>> b9Var, c cVar) {
        this.a = new e();
        this.b = y50.b();
        this.p = new AtomicInteger();
        this.g = xzVar;
        this.h = xzVar2;
        this.i = xzVar3;
        this.j = xzVar4;
        this.f = lyVar;
        this.c = aVar;
        this.d = b9Var;
        this.e = cVar;
    }

    @DexIgnore
    public void b(s40 s40) {
        try {
            s40.a(this.A, this.w);
        } catch (Throwable th) {
            throw new ay(th);
        }
    }

    @DexIgnore
    public synchronized void a(s40 s40, Executor executor) {
        this.b.a();
        this.a.a(s40, executor);
        boolean z2 = true;
        if (this.x) {
            a(1);
            executor.execute(new b(s40));
        } else if (this.z) {
            a(1);
            executor.execute(new a(s40));
        } else {
            if (this.C) {
                z2 = false;
            }
            u50.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    @DexIgnore
    public void b() {
        if (!e()) {
            this.C = true;
            this.B.c();
            this.f.a(this, this.q);
        }
    }

    @DexIgnore
    public void c() {
        oy<?> oyVar;
        synchronized (this) {
            this.b.a();
            u50.a(e(), "Not yet complete!");
            int decrementAndGet = this.p.decrementAndGet();
            u50.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                oyVar = this.A;
                i();
            } else {
                oyVar = null;
            }
        }
        if (oyVar != null) {
            oyVar.g();
        }
    }

    @DexIgnore
    public void a(s40 s40) {
        try {
            s40.a(this.y);
        } catch (Throwable th) {
            throw new ay(th);
        }
    }

    @DexIgnore
    public synchronized void a(int i2) {
        u50.a(e(), "Not yet complete!");
        if (this.p.getAndAdd(i2) == 0 && this.A != null) {
            this.A.a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.uy<R> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.gy.b
    public void a(uy<R> uyVar, sw swVar) {
        synchronized (this) {
            this.v = uyVar;
            this.w = swVar;
        }
        g();
    }

    @DexIgnore
    @Override // com.fossil.gy.b
    public void a(py pyVar) {
        synchronized (this) {
            this.y = pyVar;
        }
        f();
    }

    @DexIgnore
    @Override // com.fossil.gy.b
    public void a(gy<?> gyVar) {
        d().execute(gyVar);
    }

    @DexIgnore
    @Override // com.fossil.w50.f
    public y50 a() {
        return this.b;
    }
}
