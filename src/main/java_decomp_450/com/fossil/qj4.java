package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj4 implements Factory<pj4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ qj4 a; // = new qj4();
    }

    @DexIgnore
    public static qj4 a() {
        return a.a;
    }

    @DexIgnore
    public static pj4 b() {
        return new pj4();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pj4 get() {
        return b();
    }
}
