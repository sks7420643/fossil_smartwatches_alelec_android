package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn3 implements Parcelable.Creator<tn3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tn3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        i02 i02 = null;
        c72 c72 = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                i02 = (i02) j72.a(parcel, a, i02.CREATOR);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                c72 = (c72) j72.a(parcel, a, c72.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new tn3(i, i02, c72);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ tn3[] newArray(int i) {
        return new tn3[i];
    }
}
