package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl0 extends xp1 {
    @DexIgnore
    public static long g; // = 60000;
    @DexIgnore
    public static /* final */ wl0 h; // = new wl0();

    @DexIgnore
    public wl0() {
        super("sdk_log", 102400, 20971520, "sdk_log", "sdklog", new dh0("", "", ""), 1800, new bi0(), j91.b, true);
    }

    @DexIgnore
    public final void a(Exception exc) {
        try {
            JSONObject jSONObject = new JSONObject();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            exc.printStackTrace(new PrintWriter((OutputStream) byteArrayOutputStream, true));
            r51 r51 = r51.r4;
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            ee7.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
            yz0.a(jSONObject, r51, new String(byteArray, b21.x.c()));
            String localizedMessage = exc.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = exc.toString();
            }
            ci1 ci1 = ci1.i;
            String canonicalName = exc.getClass().getCanonicalName();
            if (canonicalName == null) {
                canonicalName = "";
            }
            String uuid = UUID.randomUUID().toString();
            ee7.a((Object) uuid, "UUID.randomUUID().toString()");
            a(new wr1(localizedMessage, ci1, "", canonicalName, uuid, false, null, null, null, jSONObject, 448));
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    @Override // com.fossil.xp1
    public long b() {
        return g;
    }
}
