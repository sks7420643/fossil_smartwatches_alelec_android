package com.fossil;

import com.facebook.share.internal.ShareConstants;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa7 extends na7 {
    @DexIgnore
    public static final <K, V> Map<K, V> a() {
        ha7 ha7 = ha7.INSTANCE;
        if (ha7 != null) {
            return ha7;
        }
        throw new x87("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    @DexIgnore
    public static final <K, V> HashMap<K, V> b(r87<? extends K, ? extends V>... r87Arr) {
        ee7.b(r87Arr, "pairs");
        HashMap<K, V> hashMap = new HashMap<>(na7.a(r87Arr.length));
        a((Map) hashMap, (r87[]) r87Arr);
        return hashMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> c(r87<? extends K, ? extends V>... r87Arr) {
        ee7.b(r87Arr, "pairs");
        if (r87Arr.length <= 0) {
            return a();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(na7.a(r87Arr.length));
        a(r87Arr, linkedHashMap);
        return linkedHashMap;
    }

    @DexIgnore
    public static final <K, V> void a(Map<? super K, ? super V> map, r87<? extends K, ? extends V>[] r87Arr) {
        ee7.b(map, "$this$putAll");
        ee7.b(r87Arr, "pairs");
        for (r87<? extends K, ? extends V> r87 : r87Arr) {
            map.put((Object) r87.component1(), (Object) r87.component2());
        }
    }

    @DexIgnore
    public static final <K, V> V b(Map<K, ? extends V> map, K k) {
        ee7.b(map, "$this$getValue");
        return (V) ma7.a(map, k);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<K, ? extends V> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <K, V> Map<K, V> c(Map<K, ? extends V> map) {
        ee7.b(map, "$this$optimizeReadOnlyMap");
        int size = map.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return map;
        }
        return na7.a(map);
    }

    @DexIgnore
    public static final <K, V> void a(Map<? super K, ? super V> map, Iterable<? extends r87<? extends K, ? extends V>> iterable) {
        ee7.b(map, "$this$putAll");
        ee7.b(iterable, "pairs");
        Iterator<? extends r87<? extends K, ? extends V>> it = iterable.iterator();
        while (it.hasNext()) {
            r87 r87 = (r87) it.next();
            map.put((Object) r87.component1(), (Object) r87.component2());
        }
    }

    @DexIgnore
    public static final <K, V> Map<K, V> a(Iterable<? extends r87<? extends K, ? extends V>> iterable) {
        ee7.b(iterable, "$this$toMap");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return a();
            }
            if (size != 1) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(na7.a(collection.size()));
                a(iterable, linkedHashMap);
                return linkedHashMap;
            }
            return na7.a((r87) (iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next()));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        a(iterable, linkedHashMap2);
        return c(linkedHashMap2);
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M a(Iterable<? extends r87<? extends K, ? extends V>> iterable, M m) {
        ee7.b(iterable, "$this$toMap");
        ee7.b(m, ShareConstants.DESTINATION);
        a((Map) m, (Iterable) iterable);
        return m;
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M a(r87<? extends K, ? extends V>[] r87Arr, M m) {
        ee7.b(r87Arr, "$this$toMap");
        ee7.b(m, ShareConstants.DESTINATION);
        a((Map) m, (r87[]) r87Arr);
        return m;
    }
}
