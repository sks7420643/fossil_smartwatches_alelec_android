package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.n63;
import com.fossil.to6;
import com.fossil.xg5;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro6 extends ho5 implements n63.b, n63.d {
    @DexIgnore
    public static /* final */ b q; // = new b(null);
    @DexIgnore
    public qw6<g35> g;
    @DexIgnore
    public to6 h;
    @DexIgnore
    public n63 i;
    @DexIgnore
    public rj4 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements n63.a {
        @DexIgnore
        public /* final */ /* synthetic */ g35 a;
        @DexIgnore
        public /* final */ /* synthetic */ LatLng b;

        @DexIgnore
        public a(g35 g35, LatLng latLng) {
            this.a = g35;
            this.b = latLng;
        }

        @DexIgnore
        @Override // com.fossil.n63.a
        public void onCancel() {
            FlexibleImageButton flexibleImageButton = this.a.r;
            ee7.a((Object) flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }

        @DexIgnore
        @Override // com.fossil.n63.a
        public void onFinish() {
            FlexibleImageButton flexibleImageButton = this.a.r;
            ee7.a((Object) flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final ro6 a(double d, double d2, String str) {
            ee7.b(str, "address");
            ro6 ro6 = new ro6();
            Bundle bundle = new Bundle();
            bundle.putDouble("latitude", d);
            bundle.putDouble("longitude", d2);
            bundle.putString("address", str);
            ro6.setArguments(bundle);
            return ro6;
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements p63 {
        @DexIgnore
        public /* final */ /* synthetic */ ro6 a;

        @DexIgnore
        public c(ro6 ro6) {
            this.a = ro6;
        }

        @DexIgnore
        @Override // com.fossil.p63
        public final void onMapReady(n63 n63) {
            this.a.i = n63;
            n63 b = this.a.i;
            if (b != null) {
                b.a((n63.b) this.a);
            }
            n63 b2 = this.a.i;
            if (b2 != null) {
                b2.a((n63.d) this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ro6 a;

        @DexIgnore
        public d(ro6 ro6) {
            this.a = ro6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ro6 a;

        @DexIgnore
        public e(ro6 ro6) {
            this.a = ro6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ro6.c(this.a).c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g35 a;
        @DexIgnore
        public /* final */ /* synthetic */ ro6 b;

        @DexIgnore
        public f(g35 g35, ro6 ro6) {
            this.a = g35;
            this.b = ro6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Intent intent = new Intent();
            FlexibleTextView flexibleTextView = this.a.u;
            ee7.a((Object) flexibleTextView, "binding.tvTitle");
            String obj = flexibleTextView.getText().toString();
            Bundle bundle = new Bundle();
            Location d = ro6.c(this.b).d();
            if (d != null) {
                bundle.putParcelable(PlaceFields.LOCATION, d);
                bundle.putString("address", obj);
            }
            intent.putExtra(Constants.RESULT, bundle);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.setResult(100, intent);
            }
            FragmentActivity activity2 = this.b.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<to6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ro6 a;

        @DexIgnore
        public g(ro6 ro6) {
            this.a = ro6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(to6.b bVar) {
            if (bVar != null) {
                Integer c = bVar.c();
                if (c != null) {
                    this.a.a(c.intValue(), "");
                }
                if (!(bVar.d() == null || bVar.e() == null)) {
                    this.a.a(bVar.d(), bVar.e());
                }
                if (bVar.a() != null) {
                    ro6 ro6 = this.a;
                    String a2 = bVar.a();
                    if (a2 != null) {
                        ro6.v(a2);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                Boolean b = bVar.b();
                if (b != null) {
                    b.booleanValue();
                    this.a.f1();
                }
                Boolean g = bVar.g();
                if (g != null) {
                    if (g.booleanValue()) {
                        this.a.g();
                    } else {
                        this.a.f();
                    }
                }
                Boolean f = bVar.f();
                if (f != null) {
                    f.booleanValue();
                    Boolean f2 = bVar.f();
                    if (f2 == null) {
                        ee7.a();
                        throw null;
                    } else if (!f2.booleanValue()) {
                        this.a.M();
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ to6 c(ro6 ro6) {
        to6 to6 = ro6.h;
        if (to6 != null) {
            return to6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void M() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showLocationPermissionError");
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "hide Dialog Loading");
        a();
    }

    @DexIgnore
    public final void f1() {
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (a2 != null) {
                a2.q.a("flexible_button_primary");
                FlexibleButton flexibleButton = a2.q;
                ee7.a((Object) flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.q;
                ee7.a((Object) flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.q;
                ee7.a((Object) flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(true);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "show Dialog Loading");
        b();
    }

    @DexIgnore
    public final void g1() {
        to6 to6 = this.h;
        if (to6 != null) {
            to6.b().a(getViewLifecycleOwner(), new g(this));
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        if (i2 == 111 && i3 == 2 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.n63.b
    public void onCameraIdle() {
        ImageView imageView;
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (!(a2 == null || (imageView = a2.s) == null)) {
                imageView.setImageResource(2131231095);
            }
            k93 k93 = new k93();
            n63 n63 = this.i;
            CameraPosition b2 = n63 != null ? n63.b() : null;
            if (b2 != null) {
                k93.a(b2.a);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("onCameraIdle: lat= ");
                ee7.a((Object) k93, "markerOptions");
                sb.append(k93.getPosition().a);
                sb.append(", long= ");
                sb.append(k93.getPosition().b);
                local.d("MapPickerFragment", sb.toString());
                if (k93.getPosition().a != 0.0d && k93.getPosition().b != 0.0d) {
                    to6 to6 = this.h;
                    if (to6 != null) {
                        to6.a(k93.getPosition().a, k93.getPosition().b);
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.n63.d
    public void onCameraMoveStarted(int i2) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCameraMoveStarted");
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (a2 != null) {
                a2.s.setImageResource(2131231094);
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                we7 we7 = we7.a;
                String a3 = ig5.a(getContext(), 2131886546);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026on_DropPin_Text__Loading)");
                String format = String.format(a3, Arrays.copyOf(new Object[0], 0));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.a("flexible_button_disabled");
                FlexibleButton flexibleButton = a2.q;
                ee7.a((Object) flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.q;
                ee7.a((Object) flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.q;
                ee7.a((Object) flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(false);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentManager supportFragmentManager;
        ee7.b(layoutInflater, "inflater");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCreateView");
        g35 g35 = (g35) qb.a(LayoutInflater.from(getContext()), 2131558582, viewGroup, false);
        PortfolioApp.g0.c().f().a().a(this);
        rj4 rj4 = this.j;
        Fragment fragment = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(to6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026kerViewModel::class.java)");
            this.h = (to6) a2;
            g35.q.a("flexible_button_disabled");
            FragmentActivity activity = getActivity();
            if (!(activity == null || (supportFragmentManager = activity.getSupportFragmentManager()) == null)) {
                fragment = supportFragmentManager.b(2131362241);
            }
            SupportMapFragment supportMapFragment = (SupportMapFragment) fragment;
            if (supportMapFragment != null) {
                supportMapFragment.a(new c(this));
            }
            g1();
            this.g = new qw6<>(this, g35);
            ee7.a((Object) g35, "binding");
            return g35.d();
        }
        ee7.d("appViewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onResume");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        double d2;
        double d3;
        String string;
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onViewCreated");
        super.onViewCreated(view, bundle);
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                a2.q.setOnClickListener(new f(a2, this));
                String str = "";
                if (getArguments() != null) {
                    double d4 = requireArguments().containsKey("latitude") ? requireArguments().getDouble("latitude") : 0.0d;
                    double d5 = requireArguments().containsKey("longitude") ? requireArguments().getDouble("longitude") : 0.0d;
                    if (requireArguments().containsKey("address") && (string = requireArguments().getString("address")) != null) {
                        str = string;
                    }
                    d3 = d4;
                    d2 = d5;
                } else {
                    d3 = 0.0d;
                    d2 = 0.0d;
                }
                if ((d3 == 0.0d || d2 == 0.0d) && TextUtils.isEmpty(str)) {
                    to6 to6 = this.h;
                    if (to6 == null) {
                        ee7.d("mViewModel");
                        throw null;
                    } else if (!to6.a()) {
                    } else {
                        if (!xg5.a(xg5.b, (Context) requireActivity(), xg5.a.FIND_DEVICE, false, true, false, (Integer) null, 52, (Object) null)) {
                            FLogger.INSTANCE.getLocal().d("MapPickerFragment", "No permission granted");
                            return;
                        }
                        to6 to62 = this.h;
                        if (to62 != null) {
                            to62.c();
                        } else {
                            ee7.d("mViewModel");
                            throw null;
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("MapPickerFragment", "initStartLocation lat " + d3 + " long " + d2 + " address " + str);
                    to6 to63 = this.h;
                    if (to63 != null) {
                        to63.a(d3, d2, str);
                    } else {
                        ee7.d("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void v(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showAddress: address = " + str);
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.tvTitle");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showErrorDialog");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        qw6<g35> qw6 = this.g;
        if (qw6 != null) {
            g35 a2 = qw6.a();
            if (a2 != null && d2 != null && d3 != null) {
                ImageView imageView = a2.s;
                ee7.a((Object) imageView, "binding.imgLocationPinUp");
                imageView.setVisibility(0);
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                n63 n63 = this.i;
                if (n63 != null) {
                    FlexibleImageButton flexibleImageButton = a2.r;
                    ee7.a((Object) flexibleImageButton, "binding.fbCurrentLocation");
                    flexibleImageButton.setEnabled(false);
                    n63.a(m63.a(latLng, 16.0f), new a(a2, latLng));
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
