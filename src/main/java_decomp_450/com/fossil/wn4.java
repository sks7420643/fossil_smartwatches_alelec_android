package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn4 implements vn4 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<un4> b;
    @DexIgnore
    public /* final */ ji c;
    @DexIgnore
    public /* final */ ji d;
    @DexIgnore
    public /* final */ ji e;
    @DexIgnore
    public /* final */ ji f;
    @DexIgnore
    public /* final */ ji g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<un4> {
        @DexIgnore
        public a(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, un4 un4) {
            if (un4.d() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, un4.d());
            }
            if (un4.i() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, un4.i());
            }
            if (un4.b() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, un4.b());
            }
            if (un4.e() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, un4.e());
            }
            if (un4.g() == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindLong(5, (long) un4.g().intValue());
            }
            if (un4.h() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, un4.h());
            }
            ajVar.bindLong(7, un4.a() ? 1 : 0);
            ajVar.bindLong(8, (long) un4.f());
            ajVar.bindLong(9, (long) un4.c());
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `friend` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`confirmation`,`pin`,`friendType`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE friend SET confirmation = ? AND friendType = ? AND pin = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM friend WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ji {
        @DexIgnore
        public d(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 2 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ji {
        @DexIgnore
        public e(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends ji {
        @DexIgnore
        public f(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = -1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends ji {
        @DexIgnore
        public g(wn4 wn4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM friend";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Callable<List<un4>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public h(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<un4> call() throws Exception {
            Integer num;
            Cursor a2 = pi.a(wn4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "socialId");
                int b4 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
                int b5 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
                int b6 = oi.b(a2, "points");
                int b7 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
                int b8 = oi.b(a2, "confirmation");
                int b9 = oi.b(a2, "pin");
                int b10 = oi.b(a2, "friendType");
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    if (a2.isNull(b6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b6));
                    }
                    arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b7), a2.getInt(b8) != 0, a2.getInt(b9), a2.getInt(b10)));
                }
                return arrayList;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    public wn4(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
        new b(this, ciVar);
        this.c = new c(this, ciVar);
        this.d = new d(this, ciVar);
        this.e = new e(this, ciVar);
        this.f = new f(this, ciVar);
        this.g = new g(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public LiveData<List<un4>> b() {
        return this.a.getInvalidationTracker().a(new String[]{"friend"}, false, (Callable) new h(fi.b("SELECT * FROM friend WHERE friendType <> 3 ORDER BY friendType DESC, firstName COLLATE NOCASE ASC", 0)));
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> c() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE friendType = 2 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public int d() {
        int i = 0;
        fi b2 = fi.b("SELECT COUNT(*) FROM friend WHERE friendType = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                i = a2.getInt(0);
            }
            return i;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> e() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE friendType = 1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public void f() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.e.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public void g() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.d.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> h() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE pin = 2", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public void i() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.f.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> j() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE friendType = 0 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> k() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE friendType = 0 OR friendType = 1 OR friendType = -1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public List<un4> l() {
        Integer num;
        fi b2 = fi.b("SELECT * FROM friend WHERE friendType = -1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (a2.isNull(b7)) {
                    num = null;
                } else {
                    num = Integer.valueOf(a2.getInt(b7));
                }
                arrayList.add(new un4(string, string2, string3, string4, num, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public long a(un4 un4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(un4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public un4 b(String str) {
        fi b2 = fi.b("SELECT * FROM friend WHERE id =? LIMIT 1", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        un4 un4 = null;
        Integer valueOf = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "points");
            int b8 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b9 = oi.b(a2, "confirmation");
            int b10 = oi.b(a2, "pin");
            int b11 = oi.b(a2, "friendType");
            if (a2.moveToFirst()) {
                String string = a2.getString(b3);
                String string2 = a2.getString(b4);
                String string3 = a2.getString(b5);
                String string4 = a2.getString(b6);
                if (!a2.isNull(b7)) {
                    valueOf = Integer.valueOf(a2.getInt(b7));
                }
                un4 = new un4(string, string2, string3, string4, valueOf, a2.getString(b8), a2.getInt(b9) != 0, a2.getInt(b10), a2.getInt(b11));
            }
            return un4;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public Long[] a(List<un4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public int a(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.g.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.vn4
    public int a(String[] strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = si.a();
        a2.append("DELETE FROM friend WHERE id in (");
        si.a(a2, strArr.length);
        a2.append(")");
        aj compileStatement = this.a.compileStatement(a2.toString());
        int i = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindString(i, str);
            }
            i++;
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
        }
    }
}
