package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s60 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r60 a;
    @DexIgnore
    public /* final */ r60 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<s60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public s60 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(r60.class.getClassLoader());
            if (readParcelable != null) {
                r60 r60 = (r60) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(r60.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new s60(r60, (r60) readParcelable2);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public s60[] newArray(int i) {
            return new s60[i];
        }
    }

    @DexIgnore
    public s60(r60 r60, r60 r602) {
        this.a = r60;
        this.b = r602;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.B4, this.a), r51.C4, this.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(s60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            s60 s60 = (s60) obj;
            return !(ee7.a(this.a, s60.a) ^ true) && !(ee7.a(this.b, s60.b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.VersionInformation");
    }

    @DexIgnore
    public final r60 getCurrentVersion() {
        return this.a;
    }

    @DexIgnore
    public final r60 getSupportedVersion() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }
}
