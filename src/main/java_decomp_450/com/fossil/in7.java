package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(in7.class, Object.class, "lastScheduledTask");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(in7.class, "producerIndex");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater d; // = AtomicIntegerFieldUpdater.newUpdater(in7.class, "consumerIndex");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater e; // = AtomicIntegerFieldUpdater.newUpdater(in7.class, "blockingTasksInBuffer");
    @DexIgnore
    public /* final */ AtomicReferenceArray<dn7> a; // = new AtomicReferenceArray<>(128);
    @DexIgnore
    public volatile int blockingTasksInBuffer; // = 0;
    @DexIgnore
    public volatile int consumerIndex; // = 0;
    @DexIgnore
    public volatile Object lastScheduledTask; // = null;
    @DexIgnore
    public volatile int producerIndex; // = 0;

    @DexIgnore
    public final int a() {
        return this.producerIndex - this.consumerIndex;
    }

    @DexIgnore
    public final int b() {
        return this.lastScheduledTask != null ? a() + 1 : a();
    }

    @DexIgnore
    public final dn7 c() {
        dn7 dn7 = (dn7) b.getAndSet(this, null);
        return dn7 != null ? dn7 : d();
    }

    @DexIgnore
    public final dn7 d() {
        dn7 andSet;
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (d.compareAndSet(this, i, i + 1) && (andSet = this.a.getAndSet(i2, null)) != null) {
                b(andSet);
                return andSet;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ dn7 a(in7 in7, dn7 dn7, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return in7.a(dn7, z);
    }

    @DexIgnore
    public final long b(in7 in7) {
        boolean z = true;
        if (dj7.a()) {
            if (!(a() == 0)) {
                throw new AssertionError();
            }
        }
        dn7 d2 = in7.d();
        if (d2 == null) {
            return a(in7, false);
        }
        dn7 a2 = a(this, d2, false, 2, null);
        if (!dj7.a()) {
            return -1;
        }
        if (a2 != null) {
            z = false;
        }
        if (z) {
            return -1;
        }
        throw new AssertionError();
    }

    @DexIgnore
    public final dn7 a(dn7 dn7, boolean z) {
        if (z) {
            return a(dn7);
        }
        dn7 dn72 = (dn7) b.getAndSet(this, dn7);
        if (dn72 != null) {
            return a(dn72);
        }
        return null;
    }

    @DexIgnore
    public final long a(in7 in7) {
        if (dj7.a()) {
            if (!(a() == 0)) {
                throw new AssertionError();
            }
        }
        int i = in7.producerIndex;
        AtomicReferenceArray<dn7> atomicReferenceArray = in7.a;
        for (int i2 = in7.consumerIndex; i2 != i; i2++) {
            int i3 = i2 & 127;
            if (in7.blockingTasksInBuffer == 0) {
                break;
            }
            dn7 dn7 = atomicReferenceArray.get(i3);
            if (dn7 != null) {
                if ((dn7.b.c() == 1) && atomicReferenceArray.compareAndSet(i3, dn7, null)) {
                    e.decrementAndGet(in7);
                    a(this, dn7, false, 2, null);
                    return -1;
                }
            }
        }
        return a(in7, true);
    }

    @DexIgnore
    public final boolean b(zm7 zm7) {
        dn7 d2 = d();
        if (d2 == null) {
            return false;
        }
        zm7.a(d2);
        return true;
    }

    @DexIgnore
    public final void b(dn7 dn7) {
        if (dn7 != null) {
            boolean z = false;
            if (dn7.b.c() == 1) {
                int decrementAndGet = e.decrementAndGet(this);
                if (dj7.a()) {
                    if (decrementAndGet >= 0) {
                        z = true;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a(zm7 zm7) {
        dn7 dn7 = (dn7) b.getAndSet(this, null);
        if (dn7 != null) {
            zm7.a(dn7);
        }
        do {
        } while (b(zm7));
    }

    @DexIgnore
    public final long a(in7 in7, boolean z) {
        dn7 dn7;
        do {
            dn7 = (dn7) in7.lastScheduledTask;
            if (dn7 == null) {
                return -2;
            }
            if (z) {
                boolean z2 = true;
                if (dn7.b.c() != 1) {
                    z2 = false;
                }
                if (!z2) {
                    return -2;
                }
            }
            long a2 = gn7.e.a() - dn7.a;
            long j = gn7.a;
            if (a2 < j) {
                return j - a2;
            }
        } while (!b.compareAndSet(in7, dn7, null));
        a(this, dn7, false, 2, null);
        return -1;
    }

    @DexIgnore
    public final dn7 a(dn7 dn7) {
        boolean z = true;
        if (dn7.b.c() != 1) {
            z = false;
        }
        if (z) {
            e.incrementAndGet(this);
        }
        if (a() == 127) {
            return dn7;
        }
        int i = this.producerIndex & 127;
        while (this.a.get(i) != null) {
            Thread.yield();
        }
        this.a.lazySet(i, dn7);
        c.incrementAndGet(this);
        return null;
    }
}
