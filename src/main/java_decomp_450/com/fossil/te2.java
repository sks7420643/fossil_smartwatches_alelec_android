package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class te2 extends Service {
    @DexIgnore
    public /* final */ ExecutorService a; // = pk2.a().a(new ba2("EnhancedIntentService"), 9);
    @DexIgnore
    public Binder b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = 0;

    @DexIgnore
    public final void a(Intent intent) {
        if (intent != null) {
            xc.a(intent);
        }
        synchronized (this.c) {
            int i = this.e - 1;
            this.e = i;
            if (i == 0) {
                stopSelfResult(this.d);
            }
        }
    }

    @DexIgnore
    public abstract void handleIntent(Intent intent);

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.b == null) {
            this.b = new xe2(this);
        }
        return this.b;
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        if (intent == null) {
            a(intent);
            return 2;
        }
        this.a.execute(new ue2(this, intent, intent));
        return 3;
    }
}
