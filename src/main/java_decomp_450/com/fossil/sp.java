package com.fossil;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sp<V> implements i14<V> {
    @DexIgnore
    public static /* final */ boolean d; // = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(sp.class.getName());
    @DexIgnore
    public static /* final */ b f;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public volatile Object a;
    @DexIgnore
    public volatile e b;
    @DexIgnore
    public volatile i c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public abstract void a(i iVar, i iVar2);

        @DexIgnore
        public abstract void a(i iVar, Thread thread);

        @DexIgnore
        public abstract boolean a(sp<?> spVar, e eVar, e eVar2);

        @DexIgnore
        public abstract boolean a(sp<?> spVar, i iVar, i iVar2);

        @DexIgnore
        public abstract boolean a(sp<?> spVar, Object obj, Object obj2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ c c;
        @DexIgnore
        public static /* final */ c d;
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ Throwable b;

        /*
        static {
            if (sp.d) {
                d = null;
                c = null;
                return;
            }
            d = new c(false, null);
            c = new c(true, null);
        }
        */

        @DexIgnore
        public c(boolean z, Throwable th) {
            this.a = z;
            this.b = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public static /* final */ d b; // = new d(new a("Failure occurred while trying to finish a future."));
        @DexIgnore
        public /* final */ Throwable a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Throwable {
            @DexIgnore
            public a(String str) {
                super(str);
            }

            @DexIgnore
            public synchronized Throwable fillInStackTrace() {
                return this;
            }
        }

        @DexIgnore
        public d(Throwable th) {
            sp.d(th);
            this.a = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public static /* final */ e d; // = new e(null, null);
        @DexIgnore
        public /* final */ Runnable a;
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public e c;

        @DexIgnore
        public e(Runnable runnable, Executor executor) {
            this.a = runnable;
            this.b = executor;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends b {
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<i, Thread> a;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<i, i> b;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<sp, i> c;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<sp, e> d;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<sp, Object> e;

        @DexIgnore
        public f(AtomicReferenceFieldUpdater<i, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<i, i> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<sp, i> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<sp, e> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<sp, Object> atomicReferenceFieldUpdater5) {
            super();
            this.a = atomicReferenceFieldUpdater;
            this.b = atomicReferenceFieldUpdater2;
            this.c = atomicReferenceFieldUpdater3;
            this.d = atomicReferenceFieldUpdater4;
            this.e = atomicReferenceFieldUpdater5;
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public void a(i iVar, Thread thread) {
            this.a.lazySet(iVar, thread);
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public void a(i iVar, i iVar2) {
            this.b.lazySet(iVar, iVar2);
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, i iVar, i iVar2) {
            return this.c.compareAndSet(spVar, iVar, iVar2);
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, e eVar, e eVar2) {
            return this.d.compareAndSet(spVar, eVar, eVar2);
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, Object obj, Object obj2) {
            return this.e.compareAndSet(spVar, obj, obj2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<V> implements Runnable {
        @DexIgnore
        public /* final */ sp<V> a;
        @DexIgnore
        public /* final */ i14<? extends V> b;

        @DexIgnore
        public g(sp<V> spVar, i14<? extends V> i14) {
            this.a = spVar;
            this.b = i14;
        }

        @DexIgnore
        public void run() {
            if (this.a.a == this) {
                if (sp.f.a((sp<?>) this.a, (Object) this, sp.b((i14<?>) this.b))) {
                    sp.a((sp<?>) this.a);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends b {
        @DexIgnore
        public h() {
            super();
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public void a(i iVar, Thread thread) {
            iVar.a = thread;
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public void a(i iVar, i iVar2) {
            iVar.b = iVar2;
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, i iVar, i iVar2) {
            synchronized (spVar) {
                if (spVar.c != iVar) {
                    return false;
                }
                spVar.c = iVar2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, e eVar, e eVar2) {
            synchronized (spVar) {
                if (spVar.b != eVar) {
                    return false;
                }
                spVar.b = eVar2;
                return true;
            }
        }

        @DexIgnore
        @Override // com.fossil.sp.b
        public boolean a(sp<?> spVar, Object obj, Object obj2) {
            synchronized (spVar) {
                if (spVar.a != obj) {
                    return false;
                }
                spVar.a = obj2;
                return true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public static /* final */ i c; // = new i(false);
        @DexIgnore
        public volatile Thread a;
        @DexIgnore
        public volatile i b;

        @DexIgnore
        public i(boolean z) {
        }

        @DexIgnore
        public void a(i iVar) {
            sp.f.a(this, iVar);
        }

        @DexIgnore
        public i() {
            sp.f.a(this, Thread.currentThread());
        }

        @DexIgnore
        public void a() {
            Thread thread = this.a;
            if (thread != null) {
                this.a = null;
                LockSupport.unpark(thread);
            }
        }
    }

    /*
    static {
        b bVar;
        try {
            bVar = new f(AtomicReferenceFieldUpdater.newUpdater(i.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(i.class, i.class, "b"), AtomicReferenceFieldUpdater.newUpdater(sp.class, i.class, "c"), AtomicReferenceFieldUpdater.newUpdater(sp.class, e.class, "b"), AtomicReferenceFieldUpdater.newUpdater(sp.class, Object.class, "a"));
            th = null;
        } catch (Throwable th) {
            th = th;
            bVar = new h();
        }
        f = bVar;
        if (th != null) {
            e.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }
    */

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final void a(i iVar) {
        iVar.a = null;
        while (true) {
            i iVar2 = this.c;
            if (iVar2 != i.c) {
                i iVar3 = null;
                while (iVar2 != null) {
                    i iVar4 = iVar2.b;
                    if (iVar2.a != null) {
                        iVar3 = iVar2;
                    } else if (iVar3 != null) {
                        iVar3.b = iVar4;
                        if (iVar3.a == null) {
                        }
                    } else if (!f.a((sp<?>) this, iVar2, iVar4)) {
                    }
                    iVar2 = iVar4;
                }
                return;
            }
            return;
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public boolean b(V v) {
        if (v == null) {
            v = (V) g;
        }
        if (!f.a((sp<?>) this, (Object) null, (Object) v)) {
            return false;
        }
        a((sp<?>) this);
        return true;
    }

    @DexIgnore
    public String c() {
        Object obj = this.a;
        if (obj instanceof g) {
            return "setFuture=[" + c(((g) obj).b) + "]";
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            return "remaining delay=[" + ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS) + " ms]";
        }
    }

    @DexIgnore
    public final boolean cancel(boolean z) {
        c cVar;
        Object obj = this.a;
        if (!(obj == null) && !(obj instanceof g)) {
            return false;
        }
        if (d) {
            cVar = new c(z, new CancellationException("Future.cancel() was called."));
        } else if (z) {
            cVar = c.c;
        } else {
            cVar = c.d;
        }
        boolean z2 = false;
        sp<V> spVar = this;
        while (true) {
            if (f.a((sp<?>) spVar, obj, (Object) cVar)) {
                if (z) {
                    spVar.b();
                }
                a((sp<?>) spVar);
                if (!(obj instanceof g)) {
                    return true;
                }
                i14<? extends V> i14 = ((g) obj).b;
                if (i14 instanceof sp) {
                    spVar = (sp) i14;
                    obj = spVar.a;
                    if (!(obj == null) && !(obj instanceof g)) {
                        return true;
                    }
                    z2 = true;
                } else {
                    i14.cancel(z);
                    return true;
                }
            } else {
                obj = spVar.a;
                if (!(obj instanceof g)) {
                    return z2;
                }
            }
        }
    }

    @DexIgnore
    public final void d() {
        i iVar;
        do {
            iVar = this.c;
        } while (!f.a((sp<?>) this, iVar, i.c));
        while (iVar != null) {
            iVar.a();
            iVar = iVar.b;
        }
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long nanos = timeUnit.toNanos(j);
        if (!Thread.interrupted()) {
            Object obj = this.a;
            if ((obj != null) && (!(obj instanceof g))) {
                return a(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                i iVar = this.c;
                if (iVar != i.c) {
                    i iVar2 = new i();
                    do {
                        iVar2.a(iVar);
                        if (f.a((sp<?>) this, iVar, iVar2)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.a;
                                    if ((obj2 != null) && (!(obj2 instanceof g))) {
                                        return a(obj2);
                                    }
                                    nanos = nanoTime - System.nanoTime();
                                } else {
                                    a(iVar2);
                                    throw new InterruptedException();
                                }
                            } while (nanos >= 1000);
                            a(iVar2);
                        } else {
                            iVar = this.c;
                        }
                    } while (iVar != i.c);
                }
                return a(this.a);
            }
            while (nanos > 0) {
                Object obj3 = this.a;
                if ((obj3 != null) && (!(obj3 instanceof g))) {
                    return a(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String spVar = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String str = "Waited " + j + " " + timeUnit.toString().toLowerCase(Locale.ROOT);
            if (nanos + 1000 < 0) {
                String str2 = str + " (plus ";
                long j2 = -nanos;
                long convert = timeUnit.convert(j2, TimeUnit.NANOSECONDS);
                long nanos2 = j2 - timeUnit.toNanos(convert);
                int i2 = (convert > 0 ? 1 : (convert == 0 ? 0 : -1));
                boolean z = i2 == 0 || nanos2 > 1000;
                if (i2 > 0) {
                    String str3 = str2 + convert + " " + lowerCase;
                    if (z) {
                        str3 = str3 + ",";
                    }
                    str2 = str3 + " ";
                }
                if (z) {
                    str2 = str2 + nanos2 + " nanoseconds ";
                }
                str = str2 + "delay)";
            }
            if (isDone()) {
                throw new TimeoutException(str + " but future completed as timeout expired");
            }
            throw new TimeoutException(str + " for " + spVar);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    public final boolean isCancelled() {
        return this.a instanceof c;
    }

    @DexIgnore
    public final boolean isDone() {
        Object obj = this.a;
        return (!(obj instanceof g)) & (obj != null);
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            a(sb);
        } else {
            try {
                str = c();
            } catch (RuntimeException e2) {
                str = "Exception thrown from implementation: " + e2.getClass();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                a(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public static Object b(i14<?> i14) {
        if (i14 instanceof sp) {
            Object obj = ((sp) i14).a;
            if (!(obj instanceof c)) {
                return obj;
            }
            c cVar = (c) obj;
            if (!cVar.a) {
                return obj;
            }
            if (cVar.b != null) {
                return new c(false, cVar.b);
            }
            return c.d;
        }
        boolean isCancelled = i14.isCancelled();
        if ((!d) && isCancelled) {
            return c.d;
        }
        try {
            Object a2 = a((Future) i14);
            return a2 == null ? g : a2;
        } catch (ExecutionException e2) {
            return new d(e2.getCause());
        } catch (CancellationException e3) {
            if (isCancelled) {
                return new c(false, e3);
            }
            return new d(new IllegalArgumentException("get() threw CancellationException, despite reporting isCancelled() == false: " + i14, e3));
        } catch (Throwable th) {
            return new d(th);
        }
    }

    @DexIgnore
    public static <T> T d(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    public final String c(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public final V a(Object obj) throws ExecutionException {
        if (obj instanceof c) {
            throw a("Task was cancelled.", ((c) obj).b);
        } else if (obj instanceof d) {
            throw new ExecutionException(((d) obj).a);
        } else if (obj == g) {
            return null;
        } else {
            return obj;
        }
    }

    @DexIgnore
    @Override // com.fossil.i14
    public final void a(Runnable runnable, Executor executor) {
        d(runnable);
        d(executor);
        e eVar = this.b;
        if (eVar != e.d) {
            e eVar2 = new e(runnable, executor);
            do {
                eVar2.c = eVar;
                if (!f.a((sp<?>) this, eVar, eVar2)) {
                    eVar = this.b;
                } else {
                    return;
                }
            } while (eVar != e.d);
        }
        b(runnable, executor);
    }

    @DexIgnore
    public static void b(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e2) {
            Logger logger = e;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, (Throwable) e2);
        }
    }

    @DexIgnore
    public boolean a(Throwable th) {
        d(th);
        if (!f.a((sp<?>) this, (Object) null, (Object) new d(th))) {
            return false;
        }
        a((sp<?>) this);
        return true;
    }

    @DexIgnore
    public boolean a(i14<? extends V> i14) {
        g gVar;
        d dVar;
        d(i14);
        Object obj = this.a;
        if (obj == null) {
            if (i14.isDone()) {
                if (!f.a((sp<?>) this, (Object) null, b((i14<?>) i14))) {
                    return false;
                }
                a((sp<?>) this);
                return true;
            }
            gVar = new g(this, i14);
            if (f.a((sp<?>) this, (Object) null, (Object) gVar)) {
                try {
                    i14.a(gVar, tp.INSTANCE);
                } catch (Throwable unused) {
                    dVar = d.b;
                }
                return true;
            }
            obj = this.a;
        }
        if (obj instanceof c) {
            i14.cancel(((c) obj).a);
        }
        return false;
        f.a((sp<?>) this, (Object) gVar, (Object) dVar);
        return true;
    }

    @DexIgnore
    public static <V> V a(Future<V> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException unused) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:13:0x0001 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [com.fossil.sp<?>] */
    /* JADX WARN: Type inference failed for: r4v1, types: [com.fossil.sp] */
    /* JADX WARN: Type inference failed for: r4v6, types: [com.fossil.sp<V>, com.fossil.sp] */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.fossil.sp$b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.fossil.sp<?> r4) {
        /*
            r0 = 0
        L_0x0001:
            r4.d()
            r4.a()
            com.fossil.sp$e r4 = r4.a(r0)
        L_0x000b:
            if (r4 == 0) goto L_0x0033
            com.fossil.sp$e r0 = r4.c
            java.lang.Runnable r1 = r4.a
            boolean r2 = r1 instanceof com.fossil.sp.g
            if (r2 == 0) goto L_0x002c
            com.fossil.sp$g r1 = (com.fossil.sp.g) r1
            com.fossil.sp<V> r4 = r1.a
            java.lang.Object r2 = r4.a
            if (r2 != r1) goto L_0x0031
            com.fossil.i14<? extends V> r2 = r1.b
            java.lang.Object r2 = b(r2)
            com.fossil.sp$b r3 = com.fossil.sp.f
            boolean r1 = r3.a(r4, r1, r2)
            if (r1 == 0) goto L_0x0031
            goto L_0x0001
        L_0x002c:
            java.util.concurrent.Executor r4 = r4.b
            b(r1, r4)
        L_0x0031:
            r4 = r0
            goto L_0x000b
        L_0x0033:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sp.a(com.fossil.sp):void");
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public final V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.a;
            if ((obj2 != null) && (!(obj2 instanceof g))) {
                return a(obj2);
            }
            i iVar = this.c;
            if (iVar != i.c) {
                i iVar2 = new i();
                do {
                    iVar2.a(iVar);
                    if (f.a((sp<?>) this, iVar, iVar2)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.a;
                            } else {
                                a(iVar2);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof g))));
                        return a(obj);
                    }
                    iVar = this.c;
                } while (iVar != i.c);
            }
            return a(this.a);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    public final e a(e eVar) {
        e eVar2;
        do {
            eVar2 = this.b;
        } while (!f.a((sp<?>) this, eVar2, e.d));
        e eVar3 = eVar;
        e eVar4 = eVar2;
        while (eVar4 != null) {
            e eVar5 = eVar4.c;
            eVar4.c = eVar3;
            eVar3 = eVar4;
            eVar4 = eVar5;
        }
        return eVar3;
    }

    @DexIgnore
    public final void a(StringBuilder sb) {
        try {
            Object a2 = a((Future) this);
            sb.append("SUCCESS, result=[");
            sb.append(c(a2));
            sb.append("]");
        } catch (ExecutionException e2) {
            sb.append("FAILURE, cause=[");
            sb.append(e2.getCause());
            sb.append("]");
        } catch (CancellationException unused) {
            sb.append("CANCELLED");
        } catch (RuntimeException e3) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e3.getClass());
            sb.append(" thrown from get()]");
        }
    }

    @DexIgnore
    public static CancellationException a(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }
}
