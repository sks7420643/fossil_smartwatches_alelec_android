package com.fossil;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t97 extends s97 {
    @DexIgnore
    public static final <T> boolean a(T[] tArr, T t) {
        ee7.b(tArr, "$this$contains");
        return b(tArr, t) >= 0;
    }

    @DexIgnore
    public static final boolean b(byte[] bArr, byte b) {
        ee7.b(bArr, "$this$contains");
        return c(bArr, b) >= 0;
    }

    @DexIgnore
    public static final int c(byte[] bArr, byte b) {
        ee7.b(bArr, "$this$indexOf");
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            if (b == bArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final int d(int[] iArr) {
        ee7.b(iArr, "$this$lastIndex");
        return iArr.length - 1;
    }

    @DexIgnore
    public static final <T> List<T> e(T[] tArr) {
        ee7.b(tArr, "$this$filterNotNull");
        ArrayList arrayList = new ArrayList();
        a((Object[]) tArr, (Collection) arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T> int f(T[] tArr) {
        ee7.b(tArr, "$this$lastIndex");
        return tArr.length - 1;
    }

    @DexIgnore
    public static final <T> T g(T[] tArr) {
        ee7.b(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public static final <T> List<T> h(T[] tArr) {
        ee7.b(tArr, "$this$toList");
        int length = tArr.length;
        if (length == 0) {
            return w97.a();
        }
        if (length != 1) {
            return i(tArr);
        }
        return v97.a(tArr[0]);
    }

    @DexIgnore
    public static final <T> List<T> i(T[] tArr) {
        ee7.b(tArr, "$this$toMutableList");
        return new ArrayList(w97.b(tArr));
    }

    @DexIgnore
    public static final <T> Set<T> j(T[] tArr) {
        ee7.b(tArr, "$this$toMutableSet");
        LinkedHashSet linkedHashSet = new LinkedHashSet(na7.a(tArr.length));
        for (T t : tArr) {
            linkedHashSet.add(t);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static final boolean a(short[] sArr, short s) {
        ee7.b(sArr, "$this$contains");
        return b(sArr, s) >= 0;
    }

    @DexIgnore
    public static final <T> int b(T[] tArr, T t) {
        ee7.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (ee7.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> d(T[] tArr) {
        ee7.b(tArr, "$this$distinct");
        return ea7.n(j(tArr));
    }

    @DexIgnore
    public static final List<Integer> e(int[] iArr) {
        ee7.b(iArr, "$this$toList");
        int length = iArr.length;
        if (length == 0) {
            return w97.a();
        }
        if (length != 1) {
            return f(iArr);
        }
        return v97.a(Integer.valueOf(iArr[0]));
    }

    @DexIgnore
    public static final List<Integer> f(int[] iArr) {
        ee7.b(iArr, "$this$toMutableList");
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int i : iArr) {
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(int[] iArr, int i) {
        ee7.b(iArr, "$this$contains");
        return b(iArr, i) >= 0;
    }

    @DexIgnore
    public static final byte[] c(byte[] bArr) {
        ee7.b(bArr, "$this$reversedArray");
        int i = 0;
        if (bArr.length == 0) {
            return bArr;
        }
        byte[] bArr2 = new byte[bArr.length];
        int b = b(bArr);
        if (b >= 0) {
            while (true) {
                bArr2[b - i] = bArr[i];
                if (i == b) {
                    break;
                }
                i++;
            }
        }
        return bArr2;
    }

    @DexIgnore
    public static final boolean a(long[] jArr, long j) {
        ee7.b(jArr, "$this$contains");
        return b(jArr, j) >= 0;
    }

    @DexIgnore
    public static final char a(char[] cArr) {
        ee7.b(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    @DexIgnore
    public static final int b(short[] sArr, short s) {
        ee7.b(sArr, "$this$indexOf");
        int length = sArr.length;
        for (int i = 0; i < length; i++) {
            if (s == sArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr, Comparator<? super T> comparator) {
        ee7.b(tArr, "$this$sortedWith");
        ee7.b(comparator, "comparator");
        return s97.b(b((Object[]) tArr, (Comparator) comparator));
    }

    @DexIgnore
    public static final int b(int[] iArr, int i) {
        ee7.b(iArr, "$this$indexOf");
        int length = iArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (i == iArr[i2]) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final lf7 c(int[] iArr) {
        ee7.b(iArr, "$this$indices");
        return new lf7(0, d(iArr));
    }

    @DexIgnore
    public static final <C extends Collection<? super T>, T> C a(T[] tArr, C c) {
        ee7.b(tArr, "$this$filterNotNullTo");
        ee7.b(c, ShareConstants.DESTINATION);
        for (T t : tArr) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T[] a(T[] tArr) {
        ee7.b(tArr, "$this$sortedArray");
        if (tArr.length == 0) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length);
        ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        T[] tArr2 = (T[]) ((Comparable[]) copyOf);
        if (tArr2 != null) {
            s97.c(tArr2);
            return tArr2;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }

    @DexIgnore
    public static final int b(long[] jArr, long j) {
        ee7.b(jArr, "$this$indexOf");
        int length = jArr.length;
        for (int i = 0; i < length; i++) {
            if (j == jArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final lf7 a(byte[] bArr) {
        ee7.b(bArr, "$this$indices");
        return new lf7(0, b(bArr));
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, Comparator<? super T> comparator) {
        ee7.b(tArr, "$this$sortedArrayWith");
        ee7.b(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] tArr2 = (T[]) Arrays.copyOf(tArr, tArr.length);
        ee7.a((Object) tArr2, "java.util.Arrays.copyOf(this, size)");
        s97.a(tArr2, comparator);
        return tArr2;
    }

    @DexIgnore
    public static final <A extends Appendable> A a(char[] cArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super Character, ? extends CharSequence> gd7) {
        ee7.b(cArr, "$this$joinTo");
        ee7.b(a, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (char c : cArr) {
            i2++;
            if (i2 > 1) {
                a.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            if (gd7 != null) {
                a.append((CharSequence) gd7.invoke(Character.valueOf(c)));
            } else {
                a.append(c);
            }
        }
        if (i >= 0 && i2 > i) {
            a.append(charSequence4);
        }
        a.append(charSequence3);
        return a;
    }

    @DexIgnore
    public static final int b(byte[] bArr) {
        ee7.b(bArr, "$this$lastIndex");
        return bArr.length - 1;
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C b(T[] tArr, C c) {
        ee7.b(tArr, "$this$toCollection");
        ee7.b(c, ShareConstants.DESTINATION);
        for (T t : tArr) {
            c.add(t);
        }
        return c;
    }

    @DexIgnore
    public static /* synthetic */ String a(char[] cArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7 gd7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        if ((i2 & 32) != 0) {
            gd7 = null;
        }
        return a(cArr, charSequence, charSequence6, charSequence5, i3, charSequence4, gd7);
    }

    @DexIgnore
    public static final String a(char[] cArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, gd7<? super Character, ? extends CharSequence> gd7) {
        ee7.b(cArr, "$this$joinToString");
        ee7.b(charSequence, "separator");
        ee7.b(charSequence2, "prefix");
        ee7.b(charSequence3, "postfix");
        ee7.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(cArr, sb, charSequence, charSequence2, charSequence3, i, charSequence4, gd7);
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
