package com.fossil;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import com.fossil.fo7;
import com.fossil.ns;
import com.fossil.qn7;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu {
    @DexIgnore
    public static /* final */ fo7 a; // = new fo7.a().a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements qn7.a {
        @DexIgnore
        public /* final */ /* synthetic */ n87 a;

        @DexIgnore
        public a(n87 n87) {
            this.a = n87;
        }

        @DexIgnore
        @Override // com.fossil.qn7.a
        public final qn7 a(lo7 lo7) {
            return ((qn7.a) this.a.getValue()).a(lo7);
        }
    }

    @DexIgnore
    public static final int a(Bitmap.Config config) {
        if (config == Bitmap.Config.ALPHA_8) {
            return 1;
        }
        if (config == Bitmap.Config.RGB_565 || config == Bitmap.Config.ARGB_4444) {
            return 2;
        }
        return (Build.VERSION.SDK_INT < 26 || config != Bitmap.Config.RGBA_F16) ? 4 : 8;
    }

    @DexIgnore
    public static final boolean b(Bitmap.Config config) {
        ee7.b(config, "$this$isHardware");
        return Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE;
    }

    @DexIgnore
    public static final Bitmap.Config c(Bitmap.Config config) {
        return (config == null || b(config)) ? Bitmap.Config.ARGB_8888 : config;
    }

    @DexIgnore
    public static final int a(Bitmap bitmap) {
        ee7.b(bitmap, "$this$getAllocationByteCountCompat");
        if (!bitmap.isRecycled()) {
            try {
                if (Build.VERSION.SDK_INT >= 19) {
                    return bitmap.getAllocationByteCount();
                }
                return bitmap.getHeight() * bitmap.getRowBytes();
            } catch (Exception unused) {
                return ku.a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
            }
        } else {
            throw new IllegalStateException(("Cannot obtain size for recycled Bitmap: " + bitmap + " [" + bitmap.getWidth() + " x " + bitmap.getHeight() + "] + " + bitmap.getConfig()).toString());
        }
    }

    @DexIgnore
    public static final ns.b a(ns nsVar, String str) {
        ee7.b(nsVar, "$this$getValue");
        if (str != null) {
            return nsVar.a(str);
        }
        return null;
    }

    @DexIgnore
    public static final void a(ns nsVar, String str, Drawable drawable, boolean z) {
        ee7.b(nsVar, "$this$putValue");
        ee7.b(drawable, "value");
        if (str != null) {
            Bitmap bitmap = null;
            if (!(drawable instanceof BitmapDrawable)) {
                drawable = null;
            }
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
            if (bitmap != null) {
                nsVar.a(str, bitmap, z);
            }
        }
    }

    @DexIgnore
    public static final vs a(View view) {
        ee7.b(view, "$this$requestManager");
        Object tag = view.getTag(qq.coil_request_manager);
        if (!(tag instanceof vs)) {
            tag = null;
        }
        vs vsVar = (vs) tag;
        if (vsVar != null) {
            return vsVar;
        }
        vs vsVar2 = new vs();
        view.addOnAttachStateChangeListener(vsVar2);
        view.setTag(qq.coil_request_manager, vsVar2);
        return vsVar2;
    }

    @DexIgnore
    public static final String a(br brVar) {
        ee7.b(brVar, "$this$emoji");
        int i = hu.a[brVar.ordinal()];
        if (i == 1) {
            return "\ud83e\udde0 ";
        }
        if (i == 2) {
            return "\ud83d\udcbe";
        }
        if (i == 3) {
            return "\u2601\ufe0f ";
        }
        throw new p87();
    }

    @DexIgnore
    public static final boolean a(Drawable drawable) {
        ee7.b(drawable, "$this$isVector");
        return (drawable instanceof ll) || (Build.VERSION.SDK_INT > 21 && (drawable instanceof VectorDrawable));
    }

    @DexIgnore
    public static final void a(Closeable closeable) {
        ee7.b(closeable, "$this$closeQuietly");
        try {
            closeable.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    public static final qt a(ImageView imageView) {
        int i;
        ee7.b(imageView, "$this$scale");
        ImageView.ScaleType scaleType = imageView.getScaleType();
        if (scaleType != null && ((i = hu.b[scaleType.ordinal()]) == 1 || i == 2 || i == 3 || i == 4)) {
            return qt.FIT;
        }
        return qt.FILL;
    }

    @DexIgnore
    public static final qn7.a a(vc7<? extends qn7.a> vc7) {
        ee7.b(vc7, "initializer");
        return new a(o87.a(vc7));
    }

    @DexIgnore
    public static final String a(MimeTypeMap mimeTypeMap, String str) {
        ee7.b(mimeTypeMap, "$this$getMimeTypeFromUrl");
        if (str == null || mh7.a(str)) {
            return null;
        }
        return mimeTypeMap.getMimeTypeFromExtension(nh7.a(nh7.a(nh7.c(nh7.c(str, '#', (String) null, 2, (Object) null), '?', (String) null, 2, (Object) null), '/', (String) null, 2, (Object) null), '.', ""));
    }

    @DexIgnore
    public static final String a(Uri uri) {
        ee7.b(uri, "$this$firstPathSegment");
        List<String> pathSegments = uri.getPathSegments();
        ee7.a((Object) pathSegments, "pathSegments");
        return (String) ea7.e((List) pathSegments);
    }

    @DexIgnore
    public static final Drawable a(Resources resources, int i, Resources.Theme theme) {
        ee7.b(resources, "$this$getDrawableCompat");
        Drawable a2 = c7.a(resources, i, theme);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }

    @DexIgnore
    public static final int a(Configuration configuration) {
        ee7.b(configuration, "$this$nightMode");
        return configuration.uiMode & 48;
    }

    @DexIgnore
    public static final fo7 a(fo7 fo7) {
        return fo7 != null ? fo7 : a;
    }

    @DexIgnore
    public static final ht a(ht htVar) {
        return htVar != null ? htVar : ht.b;
    }

    @DexIgnore
    public static final boolean a(it itVar) {
        ee7.b(itVar, "$this$isDiskPreload");
        return (itVar instanceof et) && itVar.u() == null && !itVar.n().getWriteEnabled();
    }

    @DexIgnore
    public static final boolean a() {
        return ee7.a(Looper.myLooper(), Looper.getMainLooper());
    }
}
