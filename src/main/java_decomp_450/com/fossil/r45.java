package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r45 extends q45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362084, 1);
        F.put(2131362246, 2);
        F.put(2131362517, 3);
        F.put(2131362602, 4);
        F.put(2131363450, 5);
        F.put(2131362516, 6);
        F.put(2131362029, 7);
        F.put(2131362685, 8);
        F.put(2131362268, 9);
        F.put(2131362404, 10);
        F.put(2131362423, 11);
        F.put(2131362519, 12);
    }
    */

    @DexIgnore
    public r45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 13, E, F));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public r45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[1], (FlexibleButton) objArr[2], (FlexibleButton) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[12], (TabLayout) objArr[4], (ImageView) objArr[8], (ConstraintLayout) objArr[0], (ViewPager2) objArr[5]);
        this.D = -1;
        ((q45) this).B.setTag(null);
        a(view);
        f();
    }
}
