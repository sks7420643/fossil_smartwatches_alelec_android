package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e57 implements g67 {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ x47 c;

    @DexIgnore
    public e57(x47 x47, List list, boolean z) {
        this.c = x47;
        this.a = list;
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.g67
    public void a() {
        z37.c();
        this.c.a(this.a, this.b, true);
    }

    @DexIgnore
    @Override // com.fossil.g67
    public void b() {
        z37.d();
        this.c.a(this.a, 1, this.b, true);
    }
}
