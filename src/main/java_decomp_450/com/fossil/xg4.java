package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg4 {
    @DexIgnore
    public static /* final */ int[] a; // = {4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

    @DexIgnore
    public static int a(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }

    @DexIgnore
    public static vg4 a(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        boolean z;
        ch4 ch4;
        int i6;
        ch4 a2 = new yg4(bArr).a();
        int i7 = 11;
        int d = ((a2.d() * i) / 100) + 11;
        int d2 = a2.d() + d;
        int i8 = 32;
        int i9 = 0;
        int i10 = 1;
        if (i2 != 0) {
            z = i2 < 0;
            i4 = Math.abs(i2);
            if (z) {
                i8 = 4;
            }
            if (i4 <= i8) {
                i5 = a(i4, z);
                i3 = a[i4];
                int i11 = i5 - (i5 % i3);
                ch4 = a(a2, i3);
                if (ch4.d() + d > i11) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                } else if (z && ch4.d() > (i3 << 6)) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                }
            } else {
                throw new IllegalArgumentException(String.format("Illegal value %s for layers", Integer.valueOf(i2)));
            }
        } else {
            ch4 ch42 = null;
            int i12 = 0;
            int i13 = 0;
            while (i12 <= 32) {
                boolean z2 = i12 <= 3;
                int i14 = z2 ? i12 + 1 : i12;
                int a3 = a(i14, z2);
                if (d2 <= a3) {
                    int[] iArr = a;
                    if (i13 != iArr[i14]) {
                        int i15 = iArr[i14];
                        i13 = i15;
                        ch42 = a(a2, i15);
                    }
                    int i16 = a3 - (a3 % i13);
                    if ((!z2 || ch42.d() <= (i13 << 6)) && ch42.d() + d <= i16) {
                        ch4 = ch42;
                        i3 = i13;
                        z = z2;
                        i4 = i14;
                        i5 = a3;
                    }
                }
                i12++;
                i9 = 0;
                i10 = 1;
            }
            throw new IllegalArgumentException("Data too large for an Aztec code");
        }
        ch4 b = b(ch4, i5, i3);
        int d3 = ch4.d() / i3;
        ch4 a4 = a(z, i4, d3);
        if (!z) {
            i7 = 14;
        }
        int i17 = i7 + (i4 << 2);
        int[] iArr2 = new int[i17];
        int i18 = 2;
        if (z) {
            for (int i19 = 0; i19 < i17; i19++) {
                iArr2[i19] = i19;
            }
            i6 = i17;
        } else {
            int i20 = i17 / 2;
            i6 = i17 + 1 + (((i20 - 1) / 15) * 2);
            int i21 = i6 / 2;
            for (int i22 = 0; i22 < i20; i22++) {
                int i23 = (i22 / 15) + i22;
                iArr2[(i20 - i22) - i10] = (i21 - i23) - 1;
                iArr2[i20 + i22] = i23 + i21 + i10;
            }
        }
        dh4 dh4 = new dh4(i6);
        int i24 = 0;
        int i25 = 0;
        while (i24 < i4) {
            int i26 = ((i4 - i24) << i18) + (z ? 9 : 12);
            int i27 = 0;
            while (i27 < i26) {
                int i28 = i27 << 1;
                while (i9 < i18) {
                    if (b.b(i25 + i28 + i9)) {
                        int i29 = i24 << 1;
                        dh4.b(iArr2[i29 + i9], iArr2[i29 + i27]);
                    }
                    if (b.b((i26 << 1) + i25 + i28 + i9)) {
                        int i30 = i24 << 1;
                        dh4.b(iArr2[i30 + i27], iArr2[((i17 - 1) - i30) - i9]);
                    }
                    if (b.b((i26 << 2) + i25 + i28 + i9)) {
                        int i31 = (i17 - 1) - (i24 << 1);
                        dh4.b(iArr2[i31 - i9], iArr2[i31 - i27]);
                    }
                    if (b.b((i26 * 6) + i25 + i28 + i9)) {
                        int i32 = i24 << 1;
                        dh4.b(iArr2[((i17 - 1) - i32) - i27], iArr2[i32 + i9]);
                    }
                    i9++;
                    i18 = 2;
                }
                i27++;
                i9 = 0;
                i18 = 2;
            }
            i25 += i26 << 3;
            i24++;
            i9 = 0;
            i18 = 2;
        }
        a(dh4, z, i6, a4);
        if (z) {
            a(dh4, i6 / 2, 5);
        } else {
            int i33 = i6 / 2;
            a(dh4, i33, 7);
            int i34 = 0;
            int i35 = 0;
            while (i35 < (i17 / 2) - 1) {
                for (int i36 = i33 & 1; i36 < i6; i36 += 2) {
                    int i37 = i33 - i34;
                    dh4.b(i37, i36);
                    int i38 = i33 + i34;
                    dh4.b(i38, i36);
                    dh4.b(i36, i37);
                    dh4.b(i36, i38);
                }
                i35 += 15;
                i34 += 16;
            }
        }
        vg4 vg4 = new vg4();
        vg4.a(z);
        vg4.c(i6);
        vg4.b(i4);
        vg4.a(d3);
        vg4.a(dh4);
        return vg4;
    }

    @DexIgnore
    public static ch4 b(ch4 ch4, int i, int i2) {
        hh4 hh4 = new hh4(a(i2));
        int i3 = i / i2;
        int[] a2 = a(ch4, i2, i3);
        hh4.a(a2, i3 - (ch4.d() / i2));
        ch4 ch42 = new ch4();
        ch42.a(0, i % i2);
        for (int i4 : a2) {
            ch42.a(i4, i2);
        }
        return ch42;
    }

    @DexIgnore
    public static void a(dh4 dh4, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3 += 2) {
            int i4 = i - i3;
            int i5 = i4;
            while (true) {
                int i6 = i + i3;
                if (i5 > i6) {
                    break;
                }
                dh4.b(i5, i4);
                dh4.b(i5, i6);
                dh4.b(i4, i5);
                dh4.b(i6, i5);
                i5++;
            }
        }
        int i7 = i - i2;
        dh4.b(i7, i7);
        int i8 = i7 + 1;
        dh4.b(i8, i7);
        dh4.b(i7, i8);
        int i9 = i + i2;
        dh4.b(i9, i7);
        dh4.b(i9, i8);
        dh4.b(i9, i9 - 1);
    }

    @DexIgnore
    public static ch4 a(boolean z, int i, int i2) {
        ch4 ch4 = new ch4();
        if (z) {
            ch4.a(i - 1, 2);
            ch4.a(i2 - 1, 6);
            return b(ch4, 28, 4);
        }
        ch4.a(i - 1, 5);
        ch4.a(i2 - 1, 11);
        return b(ch4, 40, 4);
    }

    @DexIgnore
    public static void a(dh4 dh4, boolean z, int i, ch4 ch4) {
        int i2 = i / 2;
        int i3 = 0;
        if (z) {
            while (i3 < 7) {
                int i4 = (i2 - 3) + i3;
                if (ch4.b(i3)) {
                    dh4.b(i4, i2 - 5);
                }
                if (ch4.b(i3 + 7)) {
                    dh4.b(i2 + 5, i4);
                }
                if (ch4.b(20 - i3)) {
                    dh4.b(i4, i2 + 5);
                }
                if (ch4.b(27 - i3)) {
                    dh4.b(i2 - 5, i4);
                }
                i3++;
            }
            return;
        }
        while (i3 < 10) {
            int i5 = (i2 - 5) + i3 + (i3 / 5);
            if (ch4.b(i3)) {
                dh4.b(i5, i2 - 7);
            }
            if (ch4.b(i3 + 10)) {
                dh4.b(i2 + 7, i5);
            }
            if (ch4.b(29 - i3)) {
                dh4.b(i5, i2 + 7);
            }
            if (ch4.b(39 - i3)) {
                dh4.b(i2 - 7, i5);
            }
            i3++;
        }
    }

    @DexIgnore
    public static int[] a(ch4 ch4, int i, int i2) {
        int[] iArr = new int[i2];
        int d = ch4.d() / i;
        for (int i3 = 0; i3 < d; i3++) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                i4 |= ch4.b((i3 * i) + i5) ? 1 << ((i - i5) - 1) : 0;
            }
            iArr[i3] = i4;
        }
        return iArr;
    }

    @DexIgnore
    public static fh4 a(int i) {
        if (i == 4) {
            return fh4.j;
        }
        if (i == 6) {
            return fh4.i;
        }
        if (i == 8) {
            return fh4.l;
        }
        if (i == 10) {
            return fh4.h;
        }
        if (i == 12) {
            return fh4.g;
        }
        throw new IllegalArgumentException("Unsupported word size " + i);
    }

    @DexIgnore
    public static ch4 a(ch4 ch4, int i) {
        ch4 ch42 = new ch4();
        int d = ch4.d();
        int i2 = (1 << i) - 2;
        int i3 = 0;
        while (i3 < d) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                int i6 = i3 + i5;
                if (i6 >= d || ch4.b(i6)) {
                    i4 |= 1 << ((i - 1) - i5);
                }
            }
            int i7 = i4 & i2;
            if (i7 == i2) {
                ch42.a(i7, i);
            } else if (i7 == 0) {
                ch42.a(i4 | 1, i);
            } else {
                ch42.a(i4, i);
                i3 += i;
            }
            i3--;
            i3 += i;
        }
        return ch42;
    }
}
