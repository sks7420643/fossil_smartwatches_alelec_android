package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.zh;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gi<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ ci k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ yh n;
    @DexIgnore
    public /* final */ zh.c o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public /* final */ Runnable t; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (gi.this.r.compareAndSet(false, true)) {
                gi.this.k.getInvalidationTracker().b(gi.this.o);
            }
            do {
                if (gi.this.q.compareAndSet(false, true)) {
                    T t = null;
                    z = false;
                    while (gi.this.p.compareAndSet(true, false)) {
                        try {
                            try {
                                t = gi.this.m.call();
                                z = true;
                            } catch (Exception e) {
                                throw new RuntimeException("Exception while computing database live data.", e);
                            }
                        } finally {
                            gi.this.q.set(false);
                        }
                    }
                    if (z) {
                        gi.this.a(t);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (gi.this.p.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean c = gi.this.c();
            if (gi.this.p.compareAndSet(false, true) && c) {
                gi.this.f().execute(gi.this.s);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends zh.c {
        @DexIgnore
        public c(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            o3.c().b(gi.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public gi(ci ciVar, yh yhVar, boolean z, Callable<T> callable, String[] strArr) {
        this.k = ciVar;
        this.l = z;
        this.m = callable;
        this.n = yhVar;
        this.o = new c(strArr);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void d() {
        super.d();
        this.n.a(this);
        f().execute(this.s);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void e() {
        super.e();
        this.n.b(this);
    }

    @DexIgnore
    public Executor f() {
        if (this.l) {
            return this.k.getTransactionExecutor();
        }
        return this.k.getQueryExecutor();
    }
}
