package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class cy0 extends Enum<cy0> {
    @DexIgnore
    public static /* final */ cy0 b;
    @DexIgnore
    public static /* final */ /* synthetic */ cy0[] c;
    @DexIgnore
    public static /* final */ kw0 d; // = new kw0(null);
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        cy0 cy0 = new cy0("NOTIFY", 1, (byte) 2);
        b = cy0;
        c = new cy0[]{new cy0("REQUEST", 0, (byte) 1), cy0};
    }
    */

    @DexIgnore
    public cy0(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static cy0 valueOf(String str) {
        return (cy0) Enum.valueOf(cy0.class, str);
    }

    @DexIgnore
    public static cy0[] values() {
        return (cy0[]) c.clone();
    }
}
