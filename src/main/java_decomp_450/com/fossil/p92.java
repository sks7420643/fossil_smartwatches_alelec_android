package com.fossil;

import android.content.Context;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p92 {
    @DexIgnore
    public static boolean a(Context context, Throwable th) {
        return a(context, th, 536870912);
    }

    @DexIgnore
    public static boolean a(Context context, Throwable th, int i) {
        try {
            a72.a(context);
            a72.a(th);
            return false;
        } catch (Exception e) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
            return false;
        }
    }
}
