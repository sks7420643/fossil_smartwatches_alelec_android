package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c41 {
    @DexIgnore
    public /* synthetic */ c41(zd7 zd7) {
    }

    @DexIgnore
    public final z51 a(int i) {
        if (i == 0) {
            return z51.SUCCESS;
        }
        if (i == xr0.j.a) {
            return z51.BLUETOOTH_OFF;
        }
        if (i == xr0.e.a) {
            return z51.START_FAIL;
        }
        if (i == xr0.f.a) {
            return z51.HID_PROXY_NOT_CONNECTED;
        }
        if (i == xr0.g.a) {
            return z51.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
        }
        if (i == xr0.h.a) {
            return z51.HID_INPUT_DEVICE_DISABLED;
        }
        if (i == xr0.i.a) {
            return z51.HID_UNKNOWN_ERROR;
        }
        return z51.GATT_ERROR;
    }
}
