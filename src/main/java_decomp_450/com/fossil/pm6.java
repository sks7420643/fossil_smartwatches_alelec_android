package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cy6;
import com.fossil.sg6;
import com.fossil.sm6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm6 extends go5 implements gy6, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public sm6 g;
    @DexIgnore
    public qw6<wz4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return pm6.p;
        }

        @DexIgnore
        public final String b() {
            return pm6.r;
        }

        @DexIgnore
        public final String c() {
            return pm6.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<sm6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ pm6 a;

        @DexIgnore
        public b(pm6 pm6) {
            this.a = pm6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(sm6.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.n(a2.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.q(b.intValue());
                }
                Integer e = bVar.e();
                if (e != null) {
                    this.a.p(e.intValue());
                }
                Integer f = bVar.f();
                if (f != null) {
                    this.a.s(f.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.o(c.intValue());
                }
                Integer d = bVar.d();
                if (d != null) {
                    this.a.r(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm6 a;

        @DexIgnore
        public c(pm6 pm6) {
            this.a = pm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 701);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm6 a;

        @DexIgnore
        public d(pm6 pm6) {
            this.a = pm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 702);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm6 a;

        @DexIgnore
        public e(pm6 pm6) {
            this.a = pm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, 703);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pm6 a;

        @DexIgnore
        public f(pm6 pm6) {
            this.a = pm6;
        }

        @DexIgnore
        public final void onClick(View view) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.i(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = pm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeSleepChartFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363307) {
            sm6 sm6 = this.g;
            if (sm6 != null) {
                sm6.a(cn6.q.a(), p, q, r);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void b(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        we7 we7 = we7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(i3 & 16777215)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        sm6 sm6 = this.g;
        if (sm6 != null) {
            sm6.a(i2, Color.parseColor(format));
            switch (i2) {
                case 701:
                    p = format;
                    return;
                case 702:
                    q = format;
                    return;
                case 703:
                    r = format;
                    return;
                default:
                    return;
            }
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        OverviewSleepDayChart overviewSleepDayChart;
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(0, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(8, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(56, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(104, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(111, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(117, BarChart.f.LOWEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(122, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(200, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(211, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(229, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(247, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(273, BarChart.f.LOWEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(286, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(305, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(316, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(325, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(337, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(410, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(474, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(486, BarChart.f.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(495, BarChart.f.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList a3 = w97.a((Object[]) new ArrayList[]{arrayList});
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.a(390, a3, 0, false, 12, null));
            sg6.b bVar = new sg6.b(new BarChart.c(390, 390, arrayList2), 0.00996016f, 0.6055777f, 0.38446212f, 5, 304, 193, 25200);
            if (a2 != null && (overviewSleepDayChart = a2.x) != null) {
                overviewSleepDayChart.a(bVar.g());
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gy6
    public void j(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void n(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.x.a("awakeSleep", i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.x.a("deepSleep", i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        wz4 wz4 = (wz4) qb.a(LayoutInflater.from(getContext()), 2131558537, null, false, a1());
        PortfolioApp.g0.c().f().a(new rm6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(sm6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            sm6 sm6 = (sm6) a2;
            this.g = sm6;
            if (sm6 != null) {
                sm6.b().a(getViewLifecycleOwner(), new b(this));
                sm6 sm62 = this.g;
                if (sm62 != null) {
                    sm62.c();
                    this.h = new qw6<>(this, wz4);
                    f1();
                    ee7.a((Object) wz4, "binding");
                    return wz4.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        p = null;
        q = null;
        r = null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        sm6 sm6 = this.g;
        if (sm6 != null) {
            sm6.c();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new c(this));
                a2.w.setOnClickListener(new d(this));
                a2.v.setOnClickListener(new e(this));
                a2.t.setOnClickListener(new f(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.x.a("lightSleep", i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void q(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.B.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void r(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.C.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        qw6<wz4> qw6 = this.h;
        if (qw6 != null) {
            wz4 a2 = qw6.a();
            if (a2 != null) {
                a2.D.setBackgroundColor(i2);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }
}
