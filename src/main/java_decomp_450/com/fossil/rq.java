package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rq {
    @DexIgnore
    public static final a a = a.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ a a; // = new a();

        @DexIgnore
        public final rq a(long j) {
            return new sq(j, null, null, 6, null);
        }
    }

    @DexIgnore
    Bitmap a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    Bitmap b(int i, int i2, Bitmap.Config config);
}
