package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk4 implements Factory<wx6> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public wk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static wk4 a(wj4 wj4) {
        return new wk4(wj4);
    }

    @DexIgnore
    public static wx6 b(wj4 wj4) {
        wx6 n = wj4.n();
        c87.a(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public wx6 get() {
        return b(this.a);
    }
}
