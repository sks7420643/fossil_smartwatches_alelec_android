package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ig1 {
    SIMPLE_MOVEMENT((byte) 1);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public ig1(byte b) {
        this.a = b;
    }
}
