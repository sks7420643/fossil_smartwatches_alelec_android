package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr4 extends he {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<mn4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, Boolean>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Integer, jn4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<dn4>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<Boolean, Boolean, Boolean>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<String, String, Integer>> j; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<Object> k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ ro4 m;
    @DexIgnore
    public /* final */ ch5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {267, 273}, m = "calculateTopNear")
    public static final class a extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gr4 gr4, fb7 fb7) {
            super(fb7);
            this.this$0 = gr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((List<jn4>) null, (List<jn4>) null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jn4 $current;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(gr4 gr4, int i, jn4 jn4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$numberOfPlayers = i;
            this.$current = jn4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$numberOfPlayers, this.$current, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.e.a(w87.a(pb7.a(this.$numberOfPlayers), this.$current));
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$3", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $allPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gr4 gr4, List list, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$allPlayers = list;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$allPlayers, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.c.a(this.$allPlayers);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "fetchChallengeWithId")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(gr4 gr4, fb7 fb7) {
            super(fb7);
            this.this$0 = gr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchChallengeWithId$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(gr4 gr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$challengeId, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 a2 = this.this$0.m;
                String str = this.$challengeId;
                this.L$0 = yi7;
                this.label = 1;
                obj = ro4.a(a2, str, (String) null, this, 2, (Object) null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ gr4 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersHistory$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {217, 219}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ yn4 $historyChallenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, yn4 yn4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$historyChallenge = yn4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$historyChallenge, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i != 0) {
                    if (i == 1) {
                        r87 r87 = (r87) this.L$2;
                        List list = (List) this.L$1;
                    } else if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    yi7 yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    t87.a(obj);
                    yi7 yi72 = this.p$;
                    yn4 yn4 = this.$historyChallenge;
                    if (yn4 != null) {
                        List<jn4> k = yn4.k();
                        r87 a2 = this.this$0.a.a(k, 3, 3);
                        int size = k.size();
                        this.L$0 = yi72;
                        this.L$1 = k;
                        this.L$2 = a2;
                        this.label = 1;
                        if (this.this$0.a.a((List) a2.getFirst(), (List) a2.getSecond(), size, this) == a) {
                            return a;
                        }
                    } else {
                        f fVar = this.this$0;
                        gr4 gr4 = fVar.a;
                        String str = fVar.b;
                        this.L$0 = yi72;
                        this.label = 2;
                        if (gr4.b(str, this) == a) {
                            return a;
                        }
                    }
                }
                this.this$0.a.d.a(w87.a(pb7.a(false), null));
                return i97.a;
            }
        }

        @DexIgnore
        public f(gr4 gr4, String str) {
            this.a = gr4;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Object> apply(yn4 yn4) {
            ik7 unused = xh7.b(ie.a(this.a), qj7.b(), null, new a(this, yn4, null), 2, null);
            return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {242, 257}, m = "fetchFocusedPlayersOnServer")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(gr4 gr4, fb7 fb7) {
            super(fb7);
            this.this$0 = gr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersOnServer$playersWrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {243}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends hn4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(gr4 gr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends hn4>>> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 a2 = this.this$0.m;
                ArrayList a3 = w97.a((Object[]) new String[]{this.$challengeId});
                this.L$0 = yi7;
                this.label = 1;
                obj = ro4.a(a2, (List) a3, 3, 3, false, (fb7) this, 8, (Object) null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {104, 108, 112}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2$historyChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super yn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super yn4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.m.g(this.this$0.$historyId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(gr4 gr4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, this.$historyId, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00db A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00dc  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00e1 A[ADDED_TO_REGION] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 2
                r3 = 0
                r4 = 3
                r5 = 1
                if (r1 == 0) goto L_0x0049
                if (r1 == r5) goto L_0x0040
                if (r1 == r2) goto L_0x002b
                if (r1 != r4) goto L_0x0023
                java.lang.Object r0 = r13.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r13.L$1
                com.fossil.ko4 r1 = (com.fossil.ko4) r1
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00dd
            L_0x0023:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x002b:
                java.lang.Object r1 = r13.L$3
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r1 = r13.L$2
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r2 = r13.L$1
                com.fossil.ko4 r2 = (com.fossil.ko4) r2
                java.lang.Object r6 = r13.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r14)
                goto L_0x00b1
            L_0x0040:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                r6 = r1
                goto L_0x0071
            L_0x0049:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r14 = r13.p$
                com.fossil.gr4 r1 = r13.this$0
                com.fossil.ro4 r6 = r1.m
                java.lang.String r7 = r13.$historyId
                java.lang.String r1 = "running"
                java.lang.String r8 = "completed"
                java.lang.String r9 = "left_after_start"
                java.lang.String[] r8 = new java.lang.String[]{r1, r8, r9}
                r9 = 0
                r11 = 4
                r12 = 0
                r13.L$0 = r14
                r13.label = r5
                r10 = r13
                java.lang.Object r1 = com.fossil.ro4.a(r6, r7, r8, r9, r10, r11, r12)
                if (r1 != r0) goto L_0x006f
                return r0
            L_0x006f:
                r6 = r14
                r14 = r1
            L_0x0071:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                java.lang.Object r1 = r14.c()
                java.util.List r1 = (java.util.List) r1
                if (r1 == 0) goto L_0x0084
                boolean r7 = r1.isEmpty()
                if (r7 == 0) goto L_0x0082
                goto L_0x0084
            L_0x0082:
                r7 = 0
                goto L_0x0085
            L_0x0084:
                r7 = 1
            L_0x0085:
                if (r7 != 0) goto L_0x00b2
                com.fossil.gr4 r7 = r13.this$0
                com.fossil.r87 r7 = r7.a(r1, r4, r4)
                com.fossil.gr4 r8 = r13.this$0
                java.lang.Object r9 = r7.getFirst()
                java.util.List r9 = (java.util.List) r9
                java.lang.Object r10 = r7.getSecond()
                java.util.List r10 = (java.util.List) r10
                int r11 = r1.size()
                r13.L$0 = r6
                r13.L$1 = r14
                r13.L$2 = r1
                r13.L$3 = r7
                r13.label = r2
                java.lang.Object r2 = r8.a(r9, r10, r11, r13)
                if (r2 != r0) goto L_0x00b0
                return r0
            L_0x00b0:
                r2 = r14
            L_0x00b1:
                r14 = r2
            L_0x00b2:
                com.fossil.gr4 r2 = r13.this$0
                androidx.lifecycle.MutableLiveData r2 = r2.d
                java.lang.Boolean r7 = com.fossil.pb7.a(r3)
                r8 = 0
                com.fossil.r87 r7 = com.fossil.w87.a(r7, r8)
                r2.a(r7)
                com.fossil.ti7 r2 = com.fossil.qj7.b()
                com.fossil.gr4$i$a r7 = new com.fossil.gr4$i$a
                r7.<init>(r13, r8)
                r13.L$0 = r6
                r13.L$1 = r14
                r13.L$2 = r1
                r13.label = r4
                java.lang.Object r14 = com.fossil.vh7.a(r2, r7, r13)
                if (r14 != r0) goto L_0x00dc
                return r0
            L_0x00dc:
                r0 = r1
            L_0x00dd:
                com.fossil.yn4 r14 = (com.fossil.yn4) r14
                if (r14 == 0) goto L_0x00f9
                if (r0 == 0) goto L_0x00e9
                boolean r1 = r0.isEmpty()
                if (r1 == 0) goto L_0x00ea
            L_0x00e9:
                r3 = 1
            L_0x00ea:
                if (r3 != 0) goto L_0x00f9
                com.fossil.yn4 r14 = com.fossil.nt4.a(r14, r0)
                com.fossil.gr4 r0 = r13.this$0
                com.fossil.ro4 r0 = r0.m
                r0.a(r14)
            L_0x00f9:
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gr4.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$init$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {73, 74, 76}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ mn4 $challenge;
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(gr4 gr4, String str, String str2, int i, mn4 mn4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$historyId = str;
            this.$challengeId = str2;
            this.$numberOfPlayers = i;
            this.$challenge = mn4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, this.$historyId, this.$challengeId, this.$numberOfPlayers, this.$challenge, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006d A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 3
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x0031
                if (r1 == r4) goto L_0x0029
                if (r1 == r3) goto L_0x0021
                if (r1 != r2) goto L_0x0019
                java.lang.Object r0 = r7.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r8)
                goto L_0x0073
            L_0x0019:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x0021:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x005b
            L_0x0029:
                java.lang.Object r1 = r7.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r8)
                goto L_0x004c
            L_0x0031:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r8 = r7.p$
                java.lang.String r1 = r7.$historyId
                if (r1 != 0) goto L_0x006e
                com.fossil.gr4 r1 = r7.this$0
                java.lang.String r5 = r7.$challengeId
                int r6 = r7.$numberOfPlayers
                r7.L$0 = r8
                r7.label = r4
                java.lang.Object r1 = r1.a(r5, r6, r7)
                if (r1 != r0) goto L_0x004b
                return r0
            L_0x004b:
                r1 = r8
            L_0x004c:
                com.fossil.gr4 r8 = r7.this$0
                java.lang.String r4 = r7.$challengeId
                r7.L$0 = r1
                r7.label = r3
                java.lang.Object r8 = r8.b(r4, r7)
                if (r8 != r0) goto L_0x005b
                return r0
            L_0x005b:
                com.fossil.gr4 r8 = r7.this$0
                com.fossil.mn4 r3 = r7.$challenge
                java.lang.String r3 = r3.f()
                r7.L$0 = r1
                r7.label = r2
                java.lang.Object r8 = r8.a(r3, r7)
                if (r8 != r0) goto L_0x0073
                return r0
            L_0x006e:
                com.fossil.gr4 r8 = r7.this$0
                r8.a(r1)
            L_0x0073:
                com.fossil.i97 r8 = com.fossil.i97.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gr4.j.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$initDisplayPlayer$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {195}, m = "invokeSuspend")
    public static final class k extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(gr4 gr4, String str, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
            this.$challengeId = str;
            this.$numberOfPlayers = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            k kVar = new k(this.this$0, this.$challengeId, this.$numberOfPlayers, fb7);
            kVar.p$ = (yi7) obj;
            return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((k) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                hn4 e = this.this$0.m.e(this.$challengeId);
                if (e != null) {
                    List<jn4> d = e.d();
                    List<jn4> b = e.b();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d2 = this.this$0.a;
                    local.e(d2, "initDisplayPlayer - top: " + d + " - near: " + b);
                    gr4 gr4 = this.this$0;
                    int i2 = this.$numberOfPlayers;
                    this.L$0 = yi7;
                    this.L$1 = e;
                    this.L$2 = d;
                    this.L$3 = b;
                    this.label = 1;
                    if (gr4.a(d, b, i2, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                List list = (List) this.L$3;
                List list2 = (List) this.L$2;
                hn4 hn4 = (hn4) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126, 137}, m = "invokeSuspend")
    public static final class l extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Long>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $encryptedDataStr;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, fb7 fb7) {
                super(2, fb7);
                this.$encryptedDataStr = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$encryptedDataStr, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Long> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return pb7.a(PortfolioApp.g0.c().n(this.$encryptedDataStr));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mn4 $challenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(l lVar, mn4 mn4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = lVar;
                this.$challenge = mn4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$challenge, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ro4 a2 = this.this$0.this$0.m;
                    String f = this.$challenge.f();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = a2.a(f, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(gr4 gr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            l lVar = new l(this.this$0, fb7);
            lVar.p$ = (yi7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((l) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            mn4 mn4;
            jn4 jn4;
            Integer p;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                mn4 = (mn4) this.this$0.b.a();
                if (mn4 != null) {
                    this.this$0.d.a(w87.a(null, pb7.a(true)));
                    ti7 b2 = qj7.b();
                    b bVar = new b(this, mn4, null);
                    this.L$0 = yi7;
                    this.L$1 = mn4;
                    this.label = 1;
                    obj = vh7.a(b2, bVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                mn4 = (mn4) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                String str = (String) this.L$4;
                Long l = (Long) this.L$3;
                ko4 ko4 = (ko4) this.L$2;
                mn4 mn42 = (mn4) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.d.a(w87.a(null, pb7.a(false)));
                this.this$0.f.a(new r87(pb7.a(true), null));
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko42 = (ko4) obj;
            if (ko42.c() != null) {
                MutableLiveData h = this.this$0.j;
                String f = ((mn4) ko42.c()).f();
                String w = PortfolioApp.g0.c().w();
                r87 r87 = (r87) this.this$0.e.a();
                h.a(new v87(f, w, pb7.a((r87 == null || (jn4 = (jn4) r87.getSecond()) == null || (p = jn4.p()) == null) ? 0 : p.intValue())));
                this.this$0.n.a(pb7.a(false));
                Long c = this.this$0.n.c();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = this.this$0.a;
                local.e(d, "leaveChallenge - consider sync data - endTimeOfUnsetChallenge: " + c);
                if (c != null && c.longValue() == 0) {
                    String s = ((mn4) ko42.c()).s();
                    ti7 a3 = qj7.a();
                    a aVar = new a(s, null);
                    this.L$0 = yi7;
                    this.L$1 = mn4;
                    this.L$2 = ko42;
                    this.L$3 = c;
                    this.L$4 = s;
                    this.label = 2;
                    if (vh7.a(a3, aVar, this) == a2) {
                        return a2;
                    }
                    this.this$0.d.a(w87.a(null, pb7.a(false)));
                    this.this$0.f.a(new r87(pb7.a(true), null));
                    return i97.a;
                }
                this.this$0.n.a(pb7.a(0L));
                this.this$0.d.a(w87.a(null, pb7.a(false)));
                this.this$0.f.a(new r87(pb7.a(true), null));
                return i97.a;
            }
            this.this$0.d.a(w87.a(null, pb7.a(false)));
            this.this$0.f.a(new r87(pb7.a(false), ko42.a()));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$refresh$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {91, 92, 95}, m = "invokeSuspend")
    public static final class m extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(gr4 gr4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gr4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            m mVar = new m(this.this$0, fb7);
            mVar.p$ = (yi7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((m) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            mn4 mn4;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi72 = this.p$;
                mn4 = (mn4) this.this$0.b.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = this.this$0.a;
                local.e(d, "refresh - challenge: " + mn4);
                if (this.this$0.l != null) {
                    gr4 gr4 = this.this$0;
                    String b = gr4.l;
                    if (b != null) {
                        this.L$0 = yi72;
                        this.L$1 = mn4;
                        this.label = 3;
                        if (gr4.c(b, this) == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (mn4 != null) {
                    gr4 gr42 = this.this$0;
                    String f = mn4.f();
                    this.L$0 = yi72;
                    this.L$1 = mn4;
                    this.label = 1;
                    if (gr42.b(f, this) == a) {
                        return a;
                    }
                    yi7 = yi72;
                }
                this.this$0.d.a(w87.a(pb7.a(false), null));
                return i97.a;
            } else if (i == 1) {
                mn4 = (mn4) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2 || i == 3) {
                mn4 mn42 = (mn4) this.L$1;
                yi7 yi73 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.d.a(w87.a(pb7.a(false), null));
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            gr4 gr43 = this.this$0;
            String f2 = mn4.f();
            this.L$0 = yi7;
            this.L$1 = mn4;
            this.label = 2;
            if (gr43.a(f2, this) == a) {
                return a;
            }
            this.this$0.d.a(w87.a(pb7.a(false), null));
            return i97.a;
        }
    }

    @DexIgnore
    public gr4(ro4 ro4, ch5 ch5) {
        ee7.b(ro4, "challengeRepository");
        ee7.b(ch5, "shared");
        this.m = ro4;
        this.n = ch5;
        String simpleName = gr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCOverviewLeaderBoardVie\u2026el::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().e(this.a, "leave challenge");
        ik7 unused = xh7.b(ie.a(this), null, null, new l(this, null), 3, null);
    }

    @DexIgnore
    public final void l() {
        String p;
        mn4 a2 = this.b.a();
        if (a2 != null && (p = a2.p()) != null) {
            int hashCode = p.hashCode();
            if (hashCode != -1402931637) {
                if (hashCode == 1550783935 && p.equals("running")) {
                    if (this.l == null) {
                        long b2 = vt4.a.b();
                        Date e2 = a2.e();
                        if (b2 < (e2 != null ? e2.getTime() : 0)) {
                            this.g.a(ot4.a.d());
                            return;
                        }
                    }
                    this.g.a(ot4.a.c());
                }
            } else if (p.equals("completed")) {
                this.g.a(ot4.a.c());
            }
        }
    }

    @DexIgnore
    public final void m() {
        ik7 unused = xh7.b(ie.a(this), null, null, new m(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<r87<Integer, jn4>> b() {
        return au4.a(this.e);
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> c() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<List<Object>> d() {
        return au4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Object> e() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<v87<String, String, Integer>> f() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> g() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, Boolean>> h() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<dn4>> i() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<v87<Boolean, Boolean, Boolean>> j() {
        return this.h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object b(java.lang.String r11, com.fossil.fb7<? super com.fossil.i97> r12) {
        /*
            r10 = this;
            boolean r0 = r12 instanceof com.fossil.gr4.g
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.gr4$g r0 = (com.fossil.gr4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gr4$g r0 = new com.fossil.gr4$g
            r0.<init>(r10, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0059
            if (r2 == r4) goto L_0x004d
            if (r2 != r3) goto L_0x0045
            java.lang.Object r11 = r0.L$5
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$4
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$3
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r11 = r0.L$2
            com.fossil.ko4 r11 = (com.fossil.ko4) r11
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r11 = r0.L$0
            com.fossil.gr4 r11 = (com.fossil.gr4) r11
            com.fossil.t87.a(r12)
            goto L_0x00ed
        L_0x0045:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "call to 'resume' before 'invoke' with coroutine"
            r11.<init>(r12)
            throw r11
        L_0x004d:
            java.lang.Object r11 = r0.L$1
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r2 = r0.L$0
            com.fossil.gr4 r2 = (com.fossil.gr4) r2
            com.fossil.t87.a(r12)
            goto L_0x0074
        L_0x0059:
            com.fossil.t87.a(r12)
            com.fossil.ti7 r12 = com.fossil.qj7.b()
            com.fossil.gr4$h r2 = new com.fossil.gr4$h
            r5 = 0
            r2.<init>(r10, r11, r5)
            r0.L$0 = r10
            r0.L$1 = r11
            r0.label = r4
            java.lang.Object r12 = com.fossil.vh7.a(r12, r2, r0)
            if (r12 != r1) goto L_0x0073
            return r1
        L_0x0073:
            r2 = r10
        L_0x0074:
            com.fossil.ko4 r12 = (com.fossil.ko4) r12
            java.lang.Object r5 = r12.c()
            java.util.List r5 = (java.util.List) r5
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r2.a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "fetchFocusedPlayersOnServer: "
            r8.append(r9)
            r8.append(r5)
            java.lang.String r8 = r8.toString()
            r6.e(r7, r8)
            r6 = 0
            if (r5 != 0) goto L_0x00ad
            com.portfolio.platform.data.model.ServerError r11 = r12.a()
            androidx.lifecycle.MutableLiveData<com.fossil.r87<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r12 = r2.i
            java.lang.Boolean r0 = com.fossil.pb7.a(r6)
            com.fossil.r87 r11 = com.fossil.w87.a(r0, r11)
            r12.a(r11)
            goto L_0x00ed
        L_0x00ad:
            boolean r7 = r5.isEmpty()
            r4 = r4 ^ r7
            if (r4 == 0) goto L_0x00ed
            java.lang.Object r4 = r5.get(r6)
            com.fossil.hn4 r4 = (com.fossil.hn4) r4
            java.util.List r4 = r4.d()
            java.lang.Object r7 = r5.get(r6)
            com.fossil.hn4 r7 = (com.fossil.hn4) r7
            java.util.List r7 = r7.b()
            java.lang.Object r8 = r5.get(r6)
            com.fossil.hn4 r8 = (com.fossil.hn4) r8
            java.lang.Integer r8 = r8.c()
            if (r8 == 0) goto L_0x00d8
            int r6 = r8.intValue()
        L_0x00d8:
            r0.L$0 = r2
            r0.L$1 = r11
            r0.L$2 = r12
            r0.L$3 = r5
            r0.L$4 = r4
            r0.L$5 = r7
            r0.label = r3
            java.lang.Object r11 = r2.a(r4, r7, r6, r0)
            if (r11 != r1) goto L_0x00ed
            return r1
        L_0x00ed:
            com.fossil.i97 r11 = com.fossil.i97.a
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gr4.b(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object c(String str, fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new i(this, str, null), fb7);
        if (a2 == nb7.a()) {
            return a2;
        }
        return i97.a;
    }

    @DexIgnore
    public final LiveData<mn4> a() {
        return au4.a(this.b);
    }

    @DexIgnore
    public final void a(mn4 mn4, String str) {
        this.l = str;
        if (mn4 != null) {
            Integer h2 = mn4.h();
            int intValue = h2 != null ? h2.intValue() : 0;
            String f2 = mn4.f();
            this.b.a(mn4);
            ik7 unused = xh7.b(ie.a(this), null, null, new j(this, str, f2, intValue, mn4, null), 3, null);
        }
    }

    @DexIgnore
    public final void a(tt4 tt4) {
        ee7.b(tt4, "option");
        int i2 = fr4.a[tt4.ordinal()];
        if (i2 == 1) {
            this.h.a(new v87<>(true, false, false));
        } else if (i2 == 2) {
            this.h.a(new v87<>(false, true, false));
        } else if (i2 == 3) {
            this.h.a(new v87<>(false, false, true));
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, int i2, fb7<? super i97> fb7) {
        Object a2 = vh7.a(qj7.b(), new k(this, str, i2, null), fb7);
        if (a2 == nb7.a()) {
            return a2;
        }
        return i97.a;
    }

    @DexIgnore
    public final void a(String str) {
        this.k = ge.b(this.m.f(str), new f(this, str));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r6, com.fossil.fb7<? super com.fossil.mn4> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.gr4.d
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.gr4$d r0 = (com.fossil.gr4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gr4$d r0 = new com.fossil.gr4$d
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.gr4 r6 = (com.fossil.gr4) r6
            com.fossil.t87.a(r7)
            goto L_0x0054
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.ti7 r7 = com.fossil.qj7.b()
            com.fossil.gr4$e r2 = new com.fossil.gr4$e
            r4 = 0
            r2.<init>(r5, r6, r4)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = com.fossil.vh7.a(r7, r2, r0)
            if (r7 != r1) goto L_0x0053
            return r1
        L_0x0053:
            r6 = r5
        L_0x0054:
            com.fossil.ko4 r7 = (com.fossil.ko4) r7
            java.lang.Object r7 = r7.c()
            com.fossil.mn4 r7 = (com.fossil.mn4) r7
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r6.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "fetchChallengeWithId - challenge: "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r3 = " - _challengeLive: "
            r2.append(r3)
            androidx.lifecycle.MutableLiveData<com.fossil.mn4> r3 = r6.b
            java.lang.Object r3 = r3.a()
            com.fossil.mn4 r3 = (com.fossil.mn4) r3
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            if (r7 == 0) goto L_0x008f
            androidx.lifecycle.MutableLiveData<com.fossil.mn4> r6 = r6.b
            r6.a(r7)
        L_0x008f:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gr4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009f, code lost:
        if (r2 != null) goto L_0x00d7;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0147 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.util.List<com.fossil.jn4> r12, java.util.List<com.fossil.jn4> r13, int r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r11 = this;
            boolean r0 = r15 instanceof com.fossil.gr4.a
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.gr4$a r0 = (com.fossil.gr4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.gr4$a r0 = new com.fossil.gr4$a
            r0.<init>(r11, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            r5 = 0
            if (r2 == 0) goto L_0x0069
            if (r2 == r4) goto L_0x004c
            if (r2 != r3) goto L_0x0044
            java.lang.Object r12 = r0.L$4
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r12 = r0.L$3
            com.fossil.jn4 r12 = (com.fossil.jn4) r12
            int r12 = r0.I$0
            java.lang.Object r12 = r0.L$2
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r12 = r0.L$1
            java.util.List r12 = (java.util.List) r12
            java.lang.Object r12 = r0.L$0
            com.fossil.gr4 r12 = (com.fossil.gr4) r12
            com.fossil.t87.a(r15)
            goto L_0x0148
        L_0x0044:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x004c:
            java.lang.Object r12 = r0.L$3
            com.fossil.jn4 r12 = (com.fossil.jn4) r12
            int r13 = r0.I$0
            java.lang.Object r14 = r0.L$2
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r2 = r0.L$1
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r4 = r0.L$0
            com.fossil.gr4 r4 = (com.fossil.gr4) r4
            com.fossil.t87.a(r15)
            r9 = r2
            r2 = r12
            r12 = r9
            r10 = r14
            r14 = r13
            r13 = r10
            goto L_0x0122
        L_0x0069:
            com.fossil.t87.a(r15)
            if (r12 == 0) goto L_0x00a2
            java.util.Iterator r15 = r12.iterator()
        L_0x0072:
            boolean r2 = r15.hasNext()
            if (r2 == 0) goto L_0x009c
            java.lang.Object r2 = r15.next()
            r6 = r2
            com.fossil.jn4 r6 = (com.fossil.jn4) r6
            java.lang.String r6 = r6.d()
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r7 = r7.w()
            boolean r6 = com.fossil.ee7.a(r6, r7)
            java.lang.Boolean r6 = com.fossil.pb7.a(r6)
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x0072
            goto L_0x009d
        L_0x009c:
            r2 = r5
        L_0x009d:
            com.fossil.jn4 r2 = (com.fossil.jn4) r2
            if (r2 == 0) goto L_0x00a2
            goto L_0x00d7
        L_0x00a2:
            if (r13 == 0) goto L_0x00d6
            java.util.Iterator r15 = r13.iterator()
        L_0x00a8:
            boolean r2 = r15.hasNext()
            if (r2 == 0) goto L_0x00d2
            java.lang.Object r2 = r15.next()
            r6 = r2
            com.fossil.jn4 r6 = (com.fossil.jn4) r6
            java.lang.String r6 = r6.d()
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r7 = r7.w()
            boolean r6 = com.fossil.ee7.a(r6, r7)
            java.lang.Boolean r6 = com.fossil.pb7.a(r6)
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x00a8
            goto L_0x00d3
        L_0x00d2:
            r2 = r5
        L_0x00d3:
            com.fossil.jn4 r2 = (com.fossil.jn4) r2
            goto L_0x00d7
        L_0x00d6:
            r2 = r5
        L_0x00d7:
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r6 = r11.a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "calculateTopNear - _currentPlayerLive - numberOfPlayers: "
            r7.append(r8)
            r7.append(r14)
            java.lang.String r8 = " - top: "
            r7.append(r8)
            r7.append(r12)
            java.lang.String r8 = " - near: "
            r7.append(r8)
            r7.append(r13)
            java.lang.String r7 = r7.toString()
            r15.e(r6, r7)
            if (r2 == 0) goto L_0x0121
            com.fossil.tk7 r15 = com.fossil.qj7.c()
            com.fossil.gr4$b r6 = new com.fossil.gr4$b
            r6.<init>(r11, r14, r2, r5)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.I$0 = r14
            r0.L$3 = r2
            r0.label = r4
            java.lang.Object r15 = com.fossil.vh7.a(r15, r6, r0)
            if (r15 != r1) goto L_0x0121
            return r1
        L_0x0121:
            r4 = r11
        L_0x0122:
            if (r12 == 0) goto L_0x0129
            java.util.List r15 = com.fossil.nt4.a(r12, r13)
            goto L_0x012a
        L_0x0129:
            r15 = r5
        L_0x012a:
            com.fossil.tk7 r6 = com.fossil.qj7.c()
            com.fossil.gr4$c r7 = new com.fossil.gr4$c
            r7.<init>(r4, r15, r5)
            r0.L$0 = r4
            r0.L$1 = r12
            r0.L$2 = r13
            r0.I$0 = r14
            r0.L$3 = r2
            r0.L$4 = r15
            r0.label = r3
            java.lang.Object r12 = com.fossil.vh7.a(r6, r7, r0)
            if (r12 != r1) goto L_0x0148
            return r1
        L_0x0148:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.gr4.a(java.util.List, java.util.List, int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final r87<List<jn4>, List<jn4>> a(List<jn4> list, int i2, int i3) {
        FLogger.INSTANCE.getLocal().e(this.a, "topAndNearFromPlayers - players: " + list);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        try {
            int size = list.size();
            int i4 = 0;
            T t = null;
            for (T t2 : list) {
                int i5 = i4 + 1;
                if (i4 >= 0) {
                    T t3 = t2;
                    if (i4 < i2) {
                        arrayList.add(t3);
                        if (ee7.a((Object) t3.d(), (Object) PortfolioApp.g0.c().w())) {
                            t = t3;
                        }
                    }
                    i4 = i5;
                } else {
                    w97.c();
                    throw null;
                }
            }
            if (t != null) {
                int i6 = i3 + i2;
                while (i2 < i6) {
                    if (i2 < size) {
                        arrayList2.add(list.get(i2));
                    }
                    i2++;
                }
            } else if (size > i2) {
                for (int i7 = i2; i7 < size; i7++) {
                    if (ee7.a((Object) list.get(i7).d(), (Object) PortfolioApp.g0.c().w())) {
                        if (i7 == size - 1) {
                            int i8 = i7 - i3;
                            while (true) {
                                i8++;
                                if (i8 >= size) {
                                    break;
                                } else if (i8 > i2 - 1) {
                                    arrayList2.add(list.get(i8));
                                }
                            }
                        } else if (i7 == i2) {
                            int i9 = i7 + i3;
                            for (int i10 = i7; i10 < i9; i10++) {
                                if (i10 < size) {
                                    arrayList2.add(list.get(i10));
                                }
                            }
                        } else {
                            int i11 = (i7 + i3) - 1;
                            for (int i12 = i7 - (i3 / 2); i12 < i11; i12++) {
                                arrayList2.add(list.get(i12));
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return w87.a(arrayList, arrayList2);
    }
}
