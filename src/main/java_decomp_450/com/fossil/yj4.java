package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj4 implements Factory<qd5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public yj4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static yj4 a(wj4 wj4) {
        return new yj4(wj4);
    }

    @DexIgnore
    public static qd5 b(wj4 wj4) {
        qd5 a2 = wj4.a();
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public qd5 get() {
        return b(this.a);
    }
}
