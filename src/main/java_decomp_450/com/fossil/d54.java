package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d54 extends v54.c {
    @DexIgnore
    public /* final */ w54<v54.c.b> a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    @Override // com.fossil.v54.c
    public w54<v54.c.b> a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.c
    public String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.c)) {
            return false;
        }
        v54.c cVar = (v54.c) obj;
        if (this.a.equals(cVar.a())) {
            String str = this.b;
            if (str == null) {
                if (cVar.b() == null) {
                    return true;
                }
            } else if (str.equals(cVar.b())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        String str = this.b;
        return hashCode ^ (str == null ? 0 : str.hashCode());
    }

    @DexIgnore
    public String toString() {
        return "FilesPayload{files=" + this.a + ", orgId=" + this.b + "}";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.c.a {
        @DexIgnore
        public w54<v54.c.b> a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.v54.c.a
        public v54.c.a a(w54<v54.c.b> w54) {
            if (w54 != null) {
                this.a = w54;
                return this;
            }
            throw new NullPointerException("Null files");
        }

        @DexIgnore
        @Override // com.fossil.v54.c.a
        public v54.c.a a(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.c.a
        public v54.c a() {
            String str = "";
            if (this.a == null) {
                str = str + " files";
            }
            if (str.isEmpty()) {
                return new d54(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public d54(w54<v54.c.b> w54, String str) {
        this.a = w54;
        this.b = str;
    }
}
