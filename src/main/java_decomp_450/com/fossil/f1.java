package com.fossil;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f1 extends ActionMode {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ androidx.appcompat.view.ActionMode b;

    @DexIgnore
    public f1(Context context, androidx.appcompat.view.ActionMode actionMode) {
        this.a = context;
        this.b = actionMode;
    }

    @DexIgnore
    public void finish() {
        this.b.a();
    }

    @DexIgnore
    public View getCustomView() {
        return this.b.b();
    }

    @DexIgnore
    public Menu getMenu() {
        return new x1(this.a, (v7) this.b.c());
    }

    @DexIgnore
    public MenuInflater getMenuInflater() {
        return this.b.d();
    }

    @DexIgnore
    public CharSequence getSubtitle() {
        return this.b.e();
    }

    @DexIgnore
    public Object getTag() {
        return this.b.f();
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.b.g();
    }

    @DexIgnore
    public boolean getTitleOptionalHint() {
        return this.b.h();
    }

    @DexIgnore
    public void invalidate() {
        this.b.i();
    }

    @DexIgnore
    public boolean isTitleOptional() {
        return this.b.j();
    }

    @DexIgnore
    public void setCustomView(View view) {
        this.b.a(view);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(CharSequence charSequence) {
        this.b.a(charSequence);
    }

    @DexIgnore
    public void setTag(Object obj) {
        this.b.a(obj);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(CharSequence charSequence) {
        this.b.b(charSequence);
    }

    @DexIgnore
    public void setTitleOptionalHint(boolean z) {
        this.b.a(z);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(int i) {
        this.b.a(i);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(int i) {
        this.b.b(i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ActionMode.Callback {
        @DexIgnore
        public /* final */ ActionMode.Callback a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ ArrayList<f1> c; // = new ArrayList<>();
        @DexIgnore
        public /* final */ SimpleArrayMap<Menu, Menu> d; // = new SimpleArrayMap<>();

        @DexIgnore
        public a(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.a = callback;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean a(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.a.onCreateActionMode(b(actionMode), a(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean b(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.a.onPrepareActionMode(b(actionMode), a(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean a(androidx.appcompat.view.ActionMode actionMode, MenuItem menuItem) {
            return this.a.onActionItemClicked(b(actionMode), new s1(this.b, (w7) menuItem));
        }

        @DexIgnore
        public android.view.ActionMode b(androidx.appcompat.view.ActionMode actionMode) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                f1 f1Var = this.c.get(i);
                if (f1Var != null && f1Var.b == actionMode) {
                    return f1Var;
                }
            }
            f1 f1Var2 = new f1(this.b, actionMode);
            this.c.add(f1Var2);
            return f1Var2;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public void a(androidx.appcompat.view.ActionMode actionMode) {
            this.a.onDestroyActionMode(b(actionMode));
        }

        @DexIgnore
        public final Menu a(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            x1 x1Var = new x1(this.b, (v7) menu);
            this.d.put(menu, x1Var);
            return x1Var;
        }
    }
}
