package com.fossil;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mc {
    @DexIgnore
    public /* final */ ArrayList<Fragment> a; // = new ArrayList<>();
    @DexIgnore
    public /* final */ HashMap<String, lc> b; // = new HashMap<>();

    @DexIgnore
    public void a(List<String> list) {
        this.a.clear();
        if (list != null) {
            for (String str : list) {
                Fragment b2 = b(str);
                if (b2 != null) {
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "restoreSaveState: added (" + str + "): " + b2);
                    }
                    a(b2);
                } else {
                    throw new IllegalStateException("No instantiated fragment for (" + str + ")");
                }
            }
        }
    }

    @DexIgnore
    public void b(lc lcVar) {
        Fragment e = lcVar.e();
        for (lc lcVar2 : this.b.values()) {
            if (lcVar2 != null) {
                Fragment e2 = lcVar2.e();
                if (e.mWho.equals(e2.mTargetWho)) {
                    e2.mTarget = e;
                    e2.mTargetWho = null;
                }
            }
        }
        this.b.put(e.mWho, null);
        String str = e.mTargetWho;
        if (str != null) {
            e.mTarget = b(str);
        }
    }

    @DexIgnore
    public void c(Fragment fragment) {
        synchronized (this.a) {
            this.a.remove(fragment);
        }
        fragment.mAdded = false;
    }

    @DexIgnore
    public void d() {
        this.b.clear();
    }

    @DexIgnore
    public ArrayList<kc> e() {
        ArrayList<kc> arrayList = new ArrayList<>(this.b.size());
        for (lc lcVar : this.b.values()) {
            if (lcVar != null) {
                Fragment e = lcVar.e();
                kc k = lcVar.k();
                arrayList.add(k);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "Saved state of " + e + ": " + k.r);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public ArrayList<String> f() {
        synchronized (this.a) {
            if (this.a.isEmpty()) {
                return null;
            }
            ArrayList<String> arrayList = new ArrayList<>(this.a.size());
            Iterator<Fragment> it = this.a.iterator();
            while (it.hasNext()) {
                Fragment next = it.next();
                arrayList.add(next.mWho);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "saveAllState: adding fragment (" + next.mWho + "): " + next);
                }
            }
            return arrayList;
        }
    }

    @DexIgnore
    public Fragment d(String str) {
        Fragment findFragmentByWho;
        for (lc lcVar : this.b.values()) {
            if (lcVar != null && (findFragmentByWho = lcVar.e().findFragmentByWho(str)) != null) {
                return findFragmentByWho;
            }
        }
        return null;
    }

    @DexIgnore
    public List<Fragment> c() {
        ArrayList arrayList;
        if (this.a.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.a) {
            arrayList = new ArrayList(this.a);
        }
        return arrayList;
    }

    @DexIgnore
    public void a(lc lcVar) {
        this.b.put(lcVar.e().mWho, lcVar);
    }

    @DexIgnore
    public lc e(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        if (!this.a.contains(fragment)) {
            synchronized (this.a) {
                this.a.add(fragment);
            }
            fragment.mAdded = true;
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    @DexIgnore
    public List<Fragment> b() {
        ArrayList arrayList = new ArrayList();
        for (lc lcVar : this.b.values()) {
            if (lcVar != null) {
                arrayList.add(lcVar.e());
            } else {
                arrayList.add(null);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Fragment c(String str) {
        if (str != null) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                Fragment fragment = this.a.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (lc lcVar : this.b.values()) {
            if (lcVar != null) {
                Fragment e = lcVar.e();
                if (str.equals(e.mTag)) {
                    return e;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Fragment b(int i) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            Fragment fragment = this.a.get(size);
            if (fragment != null && fragment.mFragmentId == i) {
                return fragment;
            }
        }
        for (lc lcVar : this.b.values()) {
            if (lcVar != null) {
                Fragment e = lcVar.e();
                if (e.mFragmentId == i) {
                    return e;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public void a(int i) {
        Iterator<Fragment> it = this.a.iterator();
        while (it.hasNext()) {
            lc lcVar = this.b.get(it.next().mWho);
            if (lcVar != null) {
                lcVar.a(i);
            }
        }
        for (lc lcVar2 : this.b.values()) {
            if (lcVar2 != null) {
                lcVar2.a(i);
            }
        }
    }

    @DexIgnore
    public Fragment b(String str) {
        lc lcVar = this.b.get(str);
        if (lcVar != null) {
            return lcVar.e();
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.b.values().removeAll(Collections.singleton(null));
    }

    @DexIgnore
    public Fragment b(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.a.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
                Fragment fragment2 = this.a.get(indexOf);
                if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(String str) {
        return this.b.containsKey(str);
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (!this.b.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments:");
            for (lc lcVar : this.b.values()) {
                printWriter.print(str);
                if (lcVar != null) {
                    Fragment e = lcVar.e();
                    printWriter.println(e);
                    e.dump(str2, fileDescriptor, printWriter, strArr);
                } else {
                    printWriter.println("null");
                }
            }
        }
        int size = this.a.size();
        if (size > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size; i++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(this.a.get(i).toString());
            }
        }
    }
}
