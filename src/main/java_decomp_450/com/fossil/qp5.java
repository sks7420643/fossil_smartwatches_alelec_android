package com.fossil;

import com.fossil.ng;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp5 extends ng.d<GoalTrackingSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        ee7.b(goalTrackingSummary, "oldItem");
        ee7.b(goalTrackingSummary2, "newItem");
        return ee7.a(goalTrackingSummary, goalTrackingSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        ee7.b(goalTrackingSummary, "oldItem");
        ee7.b(goalTrackingSummary2, "newItem");
        return zd5.d(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
