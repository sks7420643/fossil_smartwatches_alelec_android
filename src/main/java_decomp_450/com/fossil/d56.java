package com.fossil;

import android.content.Intent;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.a56;
import com.fossil.fl4;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d56 extends b56 {
    @DexIgnore
    public LiveData<List<HybridPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<HybridPreset> g; // = new ArrayList<>();
    @DexIgnore
    public HybridPreset h;
    @DexIgnore
    public MutableLiveData<String> i; // = this.n.d();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int k; // = 2;
    @DexIgnore
    public r87<Boolean, ? extends List<HybridPreset>> l; // = w87.a(false, null);
    @DexIgnore
    public Boolean m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ c56 o;
    @DexIgnore
    public /* final */ MicroAppRepository p;
    @DexIgnore
    public /* final */ HybridPresetRepository q;
    @DexIgnore
    public /* final */ a56 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$createNewPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {291}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $newPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(HybridPreset hybridPreset, fb7 fb7, b bVar) {
                super(2, fb7);
                this.$newPreset = hybridPreset;
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$newPreset, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    d56 d56 = this.this$0.this$0;
                    d56.c(d56.j() + 1);
                    HybridPresetRepository e = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$newPreset;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (e.upsertHybridPreset(hybridPreset, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(d56 d56, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d56;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (pb7.a(((HybridPreset) obj2).isActive()).booleanValue()) {
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                if (hybridPreset != null) {
                    HybridPreset cloneFrom = HybridPreset.Companion.cloneFrom(hybridPreset);
                    ti7 b = this.this$0.c();
                    a aVar = new a(cloneFrom, null, this);
                    this.L$0 = yi7;
                    this.L$1 = hybridPreset;
                    this.L$2 = hybridPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (vh7.a(b, aVar, this) == a2) {
                        return a2;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$3;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset4 = (HybridPreset) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.o.w();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<a56.d, a56.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset a;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset b;
        @DexIgnore
        public /* final */ /* synthetic */ d56 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d56$c$a$a")
            /* renamed from: com.fossil.d56$c$a$a  reason: collision with other inner class name */
            public static final class C0036a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0036a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0036a aVar = new C0036a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0036a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        HybridPresetRepository e = this.this$0.this$0.c.q;
                        HybridPreset hybridPreset = this.this$0.this$0.a;
                        if (hybridPreset != null) {
                            String id = hybridPreset.getId();
                            this.L$0 = yi7;
                            this.label = 1;
                            if (e.deletePresetById(id, this) == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 b = this.this$0.c.c();
                    C0036a aVar = new C0036a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(b, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.c.o.m();
                this.this$0.c.o.b(this.this$0.c.j());
                c cVar = this.this$0;
                boolean unused = cVar.c.b(cVar.b);
                return i97.a;
            }
        }

        @DexIgnore
        public c(HybridPreset hybridPreset, HybridPreset hybridPreset2, d56 d56, String str) {
            this.a = hybridPreset;
            this.b = hybridPreset2;
            this.c = d56;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(a56.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.c.e(), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void a(a56.b bVar) {
            ee7.b(bVar, "errorValue");
            this.c.o.m();
            int b2 = bVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                c56 l = this.c.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    l.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.o.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    HybridPresetRepository e = this.this$0.this$0.q;
                    String id = this.this$0.$preset.getId();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (e.deletePresetById(id, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HybridPreset hybridPreset, fb7 fb7, d56 d56, String str) {
            super(2, fb7);
            this.$preset = hybridPreset;
            this.this$0 = d56;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.$preset, fb7, this.this$0, this.$nextActivePresetId$inlined);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "mView.showDeleteSuccessfully " + (this.this$0.j() - 1));
            this.this$0.o.b(this.this$0.j() - 1);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(HybridPreset hybridPreset, fb7 fb7, e eVar) {
                super(2, fb7);
                this.$it = hybridPreset;
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    HybridPresetRepository e = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$it;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (e.upsertHybridPreset(hybridPreset, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(d56 d56, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d56;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$presetId, this.$name, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (pb7.a(ee7.a((Object) ((HybridPreset) obj2).getId(), (Object) this.$presetId)).booleanValue()) {
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                HybridPreset clone = hybridPreset != null ? hybridPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    ti7 b = this.this$0.c();
                    a aVar = new a(clone, null, this);
                    this.L$0 = yi7;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (vh7.a(b, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements fl4.e<a56.d, a56.b> {
        @DexIgnore
        public /* final */ /* synthetic */ d56 a;

        @DexIgnore
        public f(d56 d56) {
            this.a = d56;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(a56.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.o.m();
            this.a.o.e(0);
        }

        @DexIgnore
        public void a(a56.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.o.m();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.a());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                c56 l = this.a.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    l.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.o.l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1", f = "HomeHybridCustomizePresenter.kt", l = {63}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d56 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends MicroApp>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.p.getAllMicroApp(this.this$0.this$0.n.c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements zd<String> {
            @DexIgnore
            public /* final */ /* synthetic */ g a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<T> implements zd<List<? extends HybridPreset>> {
                @DexIgnore
                public /* final */ /* synthetic */ b a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d56$g$b$a$a")
                /* renamed from: com.fossil.d56$g$b$a$a  reason: collision with other inner class name */
                public static final class C0037a<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return bb7.a(Boolean.valueOf(t2.isActive()), Boolean.valueOf(t.isActive()));
                    }
                }

                @DexIgnore
                public a(b bVar) {
                    this.a = bVar;
                }

                @DexIgnore
                /* renamed from: a */
                public final void onChanged(List<HybridPreset> list) {
                    if (list != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                        List a2 = ea7.a((Iterable) list, (Comparator) new C0037a());
                        boolean a3 = ee7.a(a2, this.a.a.this$0.g) ^ true;
                        int itemCount = this.a.a.this$0.o.getItemCount();
                        if (a3 || a2.size() != itemCount - 1) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("HomeHybridCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                            if (this.a.a.this$0.k == 2) {
                                this.a.a.this$0.a(a2);
                            } else {
                                this.a.a.this$0.l = w87.a(true, a2);
                            }
                        } else {
                            ArrayList<T> h = this.a.a.this$0.g;
                            ArrayList arrayList = new ArrayList(x97.a(h, 10));
                            for (T t : h) {
                                arrayList.add(this.a.a.this$0.a((HybridPreset) t));
                            }
                            if (!arrayList.isEmpty()) {
                                boolean a4 = xg5.a(xg5.b, ((yr5) this.a.a.this$0.o).getContext(), (List) ((ez5) arrayList.get(0)).b(), false, false, false, (Integer) null, 56, (Object) null);
                                if (true ^ ee7.a(Boolean.valueOf(a4), this.a.a.this$0.m)) {
                                    this.a.a.this$0.m = Boolean.valueOf(a4);
                                    this.a.a.this$0.o.n(arrayList);
                                }
                            }
                        }
                    }
                }
            }

            @DexIgnore
            public b(g gVar) {
                this.a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$0.k);
                if (TextUtils.isEmpty(str) || FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.a.this$0.o.a(true);
                    return;
                }
                d56 d56 = this.a.this$0;
                HybridPresetRepository e = d56.q;
                if (str != null) {
                    d56.e = e.getPresetListAsLiveData(str);
                    this.a.this$0.e.a((LifecycleOwner) this.a.this$0.o, new a(this));
                    this.a.this$0.o.a(false);
                    return;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(d56 d56, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d56;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                r2 = 1
                if (r1 == 0) goto L_0x001f
                if (r1 != r2) goto L_0x0017
                java.lang.Object r0 = r6.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0050
            L_0x0017:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x001f:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r7 = r6.p$
                com.fossil.d56 r1 = r6.this$0
                java.util.ArrayList r1 = r1.f
                boolean r1 = r1.isEmpty()
                if (r1 == 0) goto L_0x0055
                com.fossil.d56 r1 = r6.this$0
                java.util.ArrayList r1 = r1.f
                com.fossil.d56 r3 = r6.this$0
                com.fossil.ti7 r3 = r3.c()
                com.fossil.d56$g$a r4 = new com.fossil.d56$g$a
                r5 = 0
                r4.<init>(r6, r5)
                r6.L$0 = r7
                r6.L$1 = r1
                r6.label = r2
                java.lang.Object r7 = com.fossil.vh7.a(r3, r4, r6)
                if (r7 != r0) goto L_0x004f
                return r0
            L_0x004f:
                r0 = r1
            L_0x0050:
                java.util.Collection r7 = (java.util.Collection) r7
                r0.addAll(r7)
            L_0x0055:
                com.fossil.d56 r7 = r6.this$0
                androidx.lifecycle.MutableLiveData r7 = r7.i
                com.fossil.d56 r0 = r6.this$0
                com.fossil.c56 r0 = r0.o
                if (r0 == 0) goto L_0x0085
                com.fossil.yr5 r0 = (com.fossil.yr5) r0
                com.fossil.d56$g$b r1 = new com.fossil.d56$g$b
                r1.<init>(r6)
                r7.a(r0, r1)
                com.fossil.d56 r7 = r6.this$0
                com.fossil.a56 r7 = r7.r
                r7.e()
                com.fossil.nj5 r7 = com.fossil.nj5.d
                com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r0 = new com.misfit.frameworks.buttonservice.communite.CommunicateMode[r2]
                r1 = 0
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LINK_MAPPING
                r0[r1] = r2
                r7.a(r0)
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
            L_0x0085:
                com.fossil.x87 r7 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment"
                r7.<init>(r0)
                throw r7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d56.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public d56(PortfolioApp portfolioApp, c56 c56, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, a56 a56, ch5 ch5) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(c56, "mView");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(hybridPresetRepository, "mHybridPresetRepository");
        ee7.b(a56, "mSetHybridPresetToWatchUseCase");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.n = portfolioApp;
        this.o = c56;
        this.p = microAppRepository;
        this.q = hybridPresetRepository;
        this.r = a56;
    }

    @DexIgnore
    public final void c(int i2) {
        this.j = i2;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        try {
            LiveData<List<HybridPreset>> liveData = this.e;
            c56 c56 = this.o;
            if (c56 != null) {
                liveData.a((yr5) c56);
                this.i.a((LifecycleOwner) this.o);
                this.r.g();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "Exception when remove observer.");
        }
    }

    @DexIgnore
    @Override // com.fossil.b56
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.b56
    public void i() {
        HybridPreset hybridPreset;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.h);
        xg5 xg5 = xg5.b;
        c56 c56 = this.o;
        if (c56 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (xg5.a(xg5, ((yr5) c56).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null) && (hybridPreset = this.h) != null) {
            this.o.k();
            this.r.a(new a56.c(hybridPreset), new f(this));
        }
    }

    @DexIgnore
    public final int j() {
        return this.j;
    }

    @DexIgnore
    public void k() {
        this.o.a(this);
    }

    @DexIgnore
    public final void l() {
        ArrayList<HybridPreset> arrayList = this.g;
        ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(a((HybridPreset) it.next()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.g.size() + " uiData " + arrayList2);
        if (this.m == null && !this.g.isEmpty() && !arrayList2.isEmpty()) {
            xg5 xg5 = xg5.b;
            c56 c56 = this.o;
            if (c56 != null) {
                this.m = Boolean.valueOf(xg5.a(xg5, ((yr5) c56).getContext(), (List) ((ez5) arrayList2.get(0)).b(), false, false, false, (Integer) null, 56, (Object) null));
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
        }
        this.o.n(arrayList2);
    }

    @DexIgnore
    public final boolean b(HybridPreset hybridPreset) {
        if (hybridPreset == null) {
            return true;
        }
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        for (T t : buttons) {
            if (pe5.c.c(t.getAppId())) {
                arrayList.add(t);
            }
        }
        HybridPresetAppSetting hybridPresetAppSetting = null;
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) it.next();
                if (!pe5.c.e(hybridPresetAppSetting2.getAppId())) {
                    hybridPresetAppSetting = hybridPresetAppSetting2;
                    break;
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting);
        if (hybridPresetAppSetting == null) {
            return true;
        }
        this.o.M(hybridPresetAppSetting.getAppId());
        return false;
    }

    @DexIgnore
    public final ez5 a(HybridPreset hybridPreset) {
        String str;
        T t;
        String str2;
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        List<xg5.a> a2 = ve5.a.a(hybridPreset);
        Iterator<HybridPresetAppSetting> it = buttons.iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                t = it2.next();
                if (ee7.a((Object) t.getId(), (Object) component2)) {
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                String id = t2.getId();
                String icon = t2.getIcon();
                if (icon != null) {
                    str2 = icon;
                } else {
                    str2 = "";
                }
                arrayList.add(new fz5(id, str2, ig5.a(PortfolioApp.g0.c(), t2.getNameKey(), t2.getName()), component1, null, 16, null));
            }
        }
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "convertPresetToHybridPresetConfigWrapper");
        String id2 = hybridPreset.getId();
        String name = hybridPreset.getName();
        if (name != null) {
            str = name;
        } else {
            str = "";
        }
        return new ez5(id2, str, arrayList, a2, hybridPreset.isActive());
    }

    @DexIgnore
    public final void b(String str) {
        T t;
        T t2;
        xg5 xg5 = xg5.b;
        c56 c56 = this.o;
        if (c56 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (xg5.a(xg5, ((yr5) c56).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
            List<HybridPreset> a2 = this.e.a();
            if (a2 != null) {
                a2.isEmpty();
            }
            Iterator<T> it = this.g.iterator();
            while (true) {
                t = null;
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (ee7.a((Object) t2.getId(), (Object) str)) {
                    break;
                }
            }
            T t3 = t2;
            if (t3 != null) {
                Iterator<T> it2 = this.g.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (next.isActive()) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                this.o.k();
                FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + ((Object) t4) + " set preset " + ((Object) t3) + " as active first");
                this.r.a(new a56.c(t3), new c(t4, t3, this, str));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.b56
    public void a(int i2) {
        if (this.g.size() > i2) {
            this.j = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "changePresetPosition " + this.j);
            HybridPreset hybridPreset = this.g.get(this.j);
            this.h = hybridPreset;
            if (hybridPreset != null) {
                this.o.s(hybridPreset.isActive());
                return;
            }
            return;
        }
        this.o.s(false);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.k = i2;
        if (i2 == 2 && this.l.getFirst().booleanValue()) {
            List<HybridPreset> list = (List) this.l.getSecond();
            if (list != null) {
                a(list);
            }
            this.l = w87.a(false, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.b56
    public void a(String str, String str2) {
        ee7.b(str, "name");
        ee7.b(str2, "presetId");
        ik7 unused = xh7.b(e(), null, null, new e(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.b56
    public void a(String str) {
        ee7.b(str, "nextActivePresetId");
        HybridPreset hybridPreset = this.h;
        if (hybridPreset == null) {
            return;
        }
        if (hybridPreset.isActive()) {
            b(str);
        } else {
            ik7 unused = xh7.b(e(), null, null, new d(hybridPreset, null, this, str), 3, null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.o.d(0);
        }
    }

    @DexIgnore
    public final void a(List<HybridPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "doShowingPreset");
        this.g.clear();
        this.g.addAll(list);
        int size = this.g.size();
        int i2 = this.j;
        if (size > i2 && i2 > 0) {
            this.h = this.g.get(i2);
        }
        l();
    }
}
