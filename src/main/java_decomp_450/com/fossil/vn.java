package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vn<T> implements pn<T> {
    @DexIgnore
    public /* final */ List<String> a; // = new ArrayList();
    @DexIgnore
    public T b;
    @DexIgnore
    public eo<T> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(List<String> list);

        @DexIgnore
        void b(List<String> list);
    }

    @DexIgnore
    public vn(eo<T> eoVar) {
        this.c = eoVar;
    }

    @DexIgnore
    public void a(a aVar) {
        if (this.d != aVar) {
            this.d = aVar;
            a(aVar, this.b);
        }
    }

    @DexIgnore
    public abstract boolean a(zo zoVar);

    @DexIgnore
    public abstract boolean b(T t);

    @DexIgnore
    public void a(Iterable<zo> iterable) {
        this.a.clear();
        for (zo zoVar : iterable) {
            if (a(zoVar)) {
                this.a.add(zoVar.a);
            }
        }
        if (this.a.isEmpty()) {
            this.c.b(this);
        } else {
            this.c.a((pn) this);
        }
        a(this.d, this.b);
    }

    @DexIgnore
    public void a() {
        if (!this.a.isEmpty()) {
            this.a.clear();
            this.c.b(this);
        }
    }

    @DexIgnore
    public boolean a(String str) {
        T t = this.b;
        return t != null && b(t) && this.a.contains(str);
    }

    @DexIgnore
    public final void a(a aVar, T t) {
        if (!this.a.isEmpty() && aVar != null) {
            if (t == null || b(t)) {
                aVar.b(this.a);
            } else {
                aVar.a(this.a);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pn
    public void a(T t) {
        this.b = t;
        a(this.d, t);
    }
}
