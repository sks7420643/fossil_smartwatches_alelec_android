package com.fossil;

import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixElementStatus;
import com.google.maps.model.Duration;
import com.google.maps.model.LatLng;
import com.google.maps.model.TrafficModel;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn5 {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = gn5.class.getSimpleName();
        ee7.a((Object) simpleName, "DurationUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final long a(String str, TravelMode travelMode, boolean z, double d, double d2) {
        String str2;
        ee7.b(str, "destinationAddress");
        ee7.b(travelMode, "travelMode");
        FLogger.INSTANCE.getLocal().d(a, "executeUseCase");
        GeoApiContext.Builder builder = new GeoApiContext.Builder();
        Access a2 = ng5.a().a(PortfolioApp.g0.c());
        if (a2 == null || (str2 = a2.getN()) == null) {
            str2 = "";
        }
        DistanceMatrixApiRequest departureTime = DistanceMatrixApi.newRequest(builder.apiKey(str2).build()).origins(new LatLng(d, d2)).destinations(str).mode(travelMode).units(Unit.IMPERIAL).trafficModel(TrafficModel.BEST_GUESS).departureTime(new DateTime(System.currentTimeMillis()));
        if (z) {
            departureTime.avoid(DirectionsApi.RouteRestriction.TOLLS);
        }
        try {
            DistanceMatrix distanceMatrix = (DistanceMatrix) departureTime.await();
            DistanceMatrixElement distanceMatrixElement = distanceMatrix.rows[0].elements[0];
            Duration duration = null;
            DistanceMatrixElementStatus distanceMatrixElementStatus = distanceMatrixElement != null ? distanceMatrixElement.status : null;
            if (distanceMatrixElementStatus == null || distanceMatrixElementStatus != DistanceMatrixElementStatus.OK) {
                return -1;
            }
            DistanceMatrixElement distanceMatrixElement2 = distanceMatrix.rows[0].elements[0];
            Duration duration2 = distanceMatrixElement2 != null ? distanceMatrixElement2.durationInTraffic : null;
            if (duration2 != null) {
                return duration2.inSeconds;
            }
            DistanceMatrixElement distanceMatrixElement3 = distanceMatrix.rows[0].elements[0];
            if (distanceMatrixElement3 != null) {
                duration = distanceMatrixElement3.duration;
            }
            if (duration != null) {
                return duration.inSeconds;
            }
            return -1;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.d(str3, "Exception ex=" + e);
            e.printStackTrace();
        }
    }
}
