package com.fossil;

import android.util.SparseIntArray;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gq5 extends io5<fq5> {
    @DexIgnore
    void A();

    @DexIgnore
    void A0();

    @DexIgnore
    void J0();

    @DexIgnore
    void O(boolean z);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(SparseIntArray sparseIntArray);

    @DexIgnore
    void a(Alarm alarm, boolean z);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void e(boolean z);

    @DexIgnore
    void l0();

    @DexIgnore
    void s(String str);

    @DexIgnore
    void t(boolean z);
}
