package com.fossil;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.gcm.PendingCallback;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class be2 extends Service {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public int b;
    @DexIgnore
    public ExecutorService c;
    @DexIgnore
    public Messenger d;
    @DexIgnore
    public ComponentName e;
    @DexIgnore
    public zd2 f;
    @DexIgnore
    public tk2 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(21)
    public class a extends sk2 {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            Messenger messenger;
            if (!y92.a(be2.this, message.sendingUid, "com.google.android.gms")) {
                Log.e("GcmTaskService", "unable to verify presence of Google Play Services");
                return;
            }
            int i = message.what;
            if (i == 1) {
                Bundle data = message.getData();
                if (!data.isEmpty() && (messenger = message.replyTo) != null) {
                    String string = data.getString("tag");
                    ArrayList parcelableArrayList = data.getParcelableArrayList("triggered_uris");
                    long j = data.getLong("max_exec_duration", 180);
                    if (!be2.this.a(string)) {
                        be2.this.a(new b(string, messenger, data.getBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY), j, parcelableArrayList));
                    }
                }
            } else if (i != 2) {
                if (i != 4) {
                    String valueOf = String.valueOf(message);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 31);
                    sb.append("Unrecognized message received: ");
                    sb.append(valueOf);
                    Log.e("GcmTaskService", sb.toString());
                    return;
                }
                be2.this.a();
            } else if (Log.isLoggable("GcmTaskService", 3)) {
                String valueOf2 = String.valueOf(message);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("ignoring unimplemented stop message for now: ");
                sb2.append(valueOf2);
                Log.d("GcmTaskService", sb2.toString());
            }
        }
    }

    @DexIgnore
    public abstract int a(de2 de2);

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final boolean a(String str) {
        boolean z;
        synchronized (this.a) {
            z = !this.f.a(str, this.e.getClassName());
            if (z) {
                String packageName = getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 44 + String.valueOf(str).length());
                sb.append(packageName);
                sb.append(" ");
                sb.append(str);
                sb.append(": Task already running, won't start another");
                Log.w("GcmTaskService", sb.toString());
            }
        }
        return z;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (intent == null || !v92.h() || !"com.google.android.gms.gcm.ACTION_TASK_READY".equals(intent.getAction())) {
            return null;
        }
        return this.d.getBinder();
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.f = zd2.a(this);
        this.c = pk2.a().a(10, new fe2(this), 10);
        this.d = new Messenger(new a(Looper.getMainLooper()));
        this.e = new ComponentName(this, be2.class);
        uk2.a();
        getClass();
        this.g = uk2.a;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        List<Runnable> shutdownNow = this.c.shutdownNow();
        if (!shutdownNow.isEmpty()) {
            int size = shutdownNow.size();
            StringBuilder sb = new StringBuilder(79);
            sb.append("Shutting down, but not all tasks are finished executing. Remaining: ");
            sb.append(size);
            Log.e("GcmTaskService", sb.toString());
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            a(i2);
            return 2;
        }
        try {
            intent.setExtrasClassLoader(PendingCallback.class.getClassLoader());
            String action = intent.getAction();
            if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(action)) {
                String stringExtra = intent.getStringExtra("tag");
                Parcelable parcelableExtra = intent.getParcelableExtra(Constants.CALLBACK);
                Bundle bundleExtra = intent.getBundleExtra(AppLinkData.ARGUMENTS_EXTRAS_KEY);
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("triggered_uris");
                long longExtra = intent.getLongExtra("max_exec_duration", 180);
                if (!(parcelableExtra instanceof PendingCallback)) {
                    String packageName = getPackageName();
                    StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 47 + String.valueOf(stringExtra).length());
                    sb.append(packageName);
                    sb.append(" ");
                    sb.append(stringExtra);
                    sb.append(": Could not process request, invalid callback.");
                    Log.e("GcmTaskService", sb.toString());
                    return 2;
                } else if (a(stringExtra)) {
                    a(i2);
                    return 2;
                } else {
                    a(new b(stringExtra, ((PendingCallback) parcelableExtra).a, bundleExtra, longExtra, parcelableArrayListExtra));
                }
            } else if ("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(action)) {
                a();
            } else {
                StringBuilder sb2 = new StringBuilder(String.valueOf(action).length() + 37);
                sb2.append("Unknown action received ");
                sb2.append(action);
                sb2.append(", terminating");
                Log.e("GcmTaskService", sb2.toString());
            }
            a(i2);
            return 2;
        } finally {
            a(i2);
        }
    }

    @DexIgnore
    public final void a(int i) {
        synchronized (this.a) {
            this.b = i;
            if (!this.f.a(this.e.getClassName())) {
                stopSelf(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Bundle b;
        @DexIgnore
        public /* final */ List<Uri> c;
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public /* final */ ge2 e;
        @DexIgnore
        public /* final */ Messenger f;

        @DexIgnore
        public b(String str, IBinder iBinder, Bundle bundle, long j, List<Uri> list) {
            ge2 ge2;
            this.a = str;
            if (iBinder == null) {
                ge2 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gcm.INetworkTaskCallback");
                if (queryLocalInterface instanceof ge2) {
                    ge2 = (ge2) queryLocalInterface;
                } else {
                    ge2 = new he2(iBinder);
                }
            }
            this.e = ge2;
            this.b = bundle;
            this.d = j;
            this.c = list;
            this.f = null;
        }

        @DexIgnore
        public final void a(int i) {
            synchronized (be2.this.a) {
                try {
                    if (be2.this.f.c(this.a, be2.this.e.getClassName())) {
                        be2.this.f.b(this.a, be2.this.e.getClassName());
                        if (!a() && !be2.this.f.a(be2.this.e.getClassName())) {
                            be2.this.stopSelf(be2.this.b);
                        }
                        return;
                    }
                    if (a()) {
                        Messenger messenger = this.f;
                        Message obtain = Message.obtain();
                        obtain.what = 3;
                        obtain.arg1 = i;
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("component", be2.this.e);
                        bundle.putString("tag", this.a);
                        obtain.setData(bundle);
                        messenger.send(obtain);
                    } else {
                        this.e.c(i);
                    }
                    be2.this.f.b(this.a, be2.this.e.getClassName());
                    if (!a() && !be2.this.f.a(be2.this.e.getClassName())) {
                        be2.this.stopSelf(be2.this.b);
                    }
                } catch (RemoteException unused) {
                    String valueOf = String.valueOf(this.a);
                    Log.e("GcmTaskService", valueOf.length() != 0 ? "Error reporting result of operation to scheduler for ".concat(valueOf) : new String("Error reporting result of operation to scheduler for "));
                    be2.this.f.b(this.a, be2.this.e.getClassName());
                    if (!a() && !be2.this.f.a(be2.this.e.getClassName())) {
                        be2.this.stopSelf(be2.this.b);
                    }
                } catch (Throwable th) {
                    be2.this.f.b(this.a, be2.this.e.getClassName());
                    if (!a() && !be2.this.f.a(be2.this.e.getClassName())) {
                        be2.this.stopSelf(be2.this.b);
                    }
                    throw th;
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x004c, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
            a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0050, code lost:
            throw r2;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r8 = this;
                com.fossil.je2 r0 = new com.fossil.je2
                java.lang.String r1 = r8.a
                java.lang.String r1 = java.lang.String.valueOf(r1)
                int r2 = r1.length()
                java.lang.String r3 = "nts:client:onRunTask:"
                if (r2 == 0) goto L_0x0015
                java.lang.String r1 = r3.concat(r1)
                goto L_0x001a
            L_0x0015:
                java.lang.String r1 = new java.lang.String
                r1.<init>(r3)
            L_0x001a:
                r0.<init>(r1)
                com.fossil.de2 r1 = new com.fossil.de2     // Catch:{ all -> 0x004a }
                java.lang.String r3 = r8.a     // Catch:{ all -> 0x004a }
                android.os.Bundle r4 = r8.b     // Catch:{ all -> 0x004a }
                long r5 = r8.d     // Catch:{ all -> 0x004a }
                java.util.List<android.net.Uri> r7 = r8.c     // Catch:{ all -> 0x004a }
                r2 = r1
                r2.<init>(r3, r4, r5, r7)     // Catch:{ all -> 0x004a }
                com.fossil.be2 r2 = com.fossil.be2.this     // Catch:{ all -> 0x004a }
                com.fossil.tk2 r2 = r2.g     // Catch:{ all -> 0x004a }
                java.lang.String r3 = "onRunTask"
                int r4 = com.fossil.xk2.a     // Catch:{ all -> 0x004a }
                r2.a(r3, r4)     // Catch:{ all -> 0x004a }
                com.fossil.be2 r2 = com.fossil.be2.this     // Catch:{ all -> 0x0046 }
                int r1 = r2.a(r1)     // Catch:{ all -> 0x0046 }
                r8.a(r1)
                r1 = 0
                a(r1, r0)
                return
            L_0x0046:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x0048 }
            L_0x0048:
                r1 = move-exception
                throw r1
            L_0x004a:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x004c }
            L_0x004c:
                r2 = move-exception
                a(r1, r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.be2.b.run():void");
        }

        @DexIgnore
        public b(String str, Messenger messenger, Bundle bundle, long j, List<Uri> list) {
            this.a = str;
            this.f = messenger;
            this.b = bundle;
            this.d = j;
            this.c = list;
            this.e = null;
        }

        @DexIgnore
        public final boolean a() {
            return this.f != null;
        }

        @DexIgnore
        public static /* synthetic */ void a(Throwable th, je2 je2) {
            if (th != null) {
                try {
                    je2.close();
                } catch (Throwable th2) {
                    yk2.a(th, th2);
                }
            } else {
                je2.close();
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        try {
            this.c.execute(bVar);
        } catch (RejectedExecutionException e2) {
            Log.e("GcmTaskService", "Executor is shutdown. onDestroy was called but main looper had an unprocessed start task message. The task will be retried with backoff delay.", e2);
            bVar.a(1);
        }
    }
}
