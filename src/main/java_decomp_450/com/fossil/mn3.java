package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mn3 extends tf2 implements jn3 {
    @DexIgnore
    public mn3() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.tf2
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 3:
                a((i02) vf2.a(parcel, i02.CREATOR), (in3) vf2.a(parcel, in3.CREATOR));
                break;
            case 4:
                d((Status) vf2.a(parcel, Status.CREATOR));
                break;
            case 5:
            default:
                return false;
            case 6:
                e((Status) vf2.a(parcel, Status.CREATOR));
                break;
            case 7:
                a((Status) vf2.a(parcel, Status.CREATOR), (GoogleSignInAccount) vf2.a(parcel, GoogleSignInAccount.CREATOR));
                break;
            case 8:
                a((tn3) vf2.a(parcel, tn3.CREATOR));
                break;
            case 9:
                a((nn3) vf2.a(parcel, nn3.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
