package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr7 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public nr7 f;
    @DexIgnore
    public nr7 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public nr7() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    @DexIgnore
    public final nr7 a(nr7 nr7) {
        ee7.b(nr7, "segment");
        nr7.g = this;
        nr7.f = this.f;
        nr7 nr72 = this.f;
        if (nr72 != null) {
            nr72.g = nr7;
            this.f = nr7;
            return nr7;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final nr7 b() {
        nr7 nr7 = this.f;
        if (nr7 == this) {
            nr7 = null;
        }
        nr7 nr72 = this.g;
        if (nr72 != null) {
            nr72.f = this.f;
            nr7 nr73 = this.f;
            if (nr73 != null) {
                nr73.g = nr72;
                this.f = null;
                this.g = null;
                return nr7;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final nr7 c() {
        this.d = true;
        return new nr7(this.a, this.b, this.c, true, false);
    }

    @DexIgnore
    public nr7(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        ee7.b(bArr, "data");
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public final nr7 a(int i) {
        nr7 nr7;
        if (i > 0 && i <= this.c - this.b) {
            if (i >= 1024) {
                nr7 = c();
            } else {
                nr7 = or7.c.a();
                byte[] bArr = this.a;
                byte[] bArr2 = nr7.a;
                int i2 = this.b;
                s97.a(bArr, bArr2, 0, i2, i2 + i, 2, (Object) null);
            }
            nr7.c = nr7.b + i;
            this.b += i;
            nr7 nr72 = this.g;
            if (nr72 != null) {
                nr72.a(nr7);
                return nr7;
            }
            ee7.a();
            throw null;
        }
        throw new IllegalArgumentException("byteCount out of range".toString());
    }

    @DexIgnore
    public final void a() {
        int i = 0;
        if (this.g != this) {
            nr7 nr7 = this.g;
            if (nr7 == null) {
                ee7.a();
                throw null;
            } else if (nr7.e) {
                int i2 = this.c - this.b;
                if (nr7 != null) {
                    int i3 = 8192 - nr7.c;
                    if (nr7 != null) {
                        if (!nr7.d) {
                            if (nr7 != null) {
                                i = nr7.b;
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        if (i2 <= i3 + i) {
                            nr7 nr72 = this.g;
                            if (nr72 != null) {
                                a(nr72, i2);
                                b();
                                or7.c.a(this);
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
        } else {
            throw new IllegalStateException("cannot compact".toString());
        }
    }

    @DexIgnore
    public final void a(nr7 nr7, int i) {
        ee7.b(nr7, "sink");
        if (nr7.e) {
            int i2 = nr7.c;
            if (i2 + i > 8192) {
                if (!nr7.d) {
                    int i3 = nr7.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = nr7.a;
                        s97.a(bArr, bArr, 0, i3, i2, 2, (Object) null);
                        nr7.c -= nr7.b;
                        nr7.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            byte[] bArr2 = this.a;
            byte[] bArr3 = nr7.a;
            int i4 = nr7.c;
            int i5 = this.b;
            s97.a(bArr2, bArr3, i4, i5, i5 + i);
            nr7.c += i;
            this.b += i;
            return;
        }
        throw new IllegalStateException("only owner can write".toString());
    }
}
