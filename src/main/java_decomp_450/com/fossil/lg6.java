package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.be5;
import com.fossil.ov3;
import com.fossil.rg6;
import com.fossil.sg6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg6 extends go5 implements kg6, View.OnClickListener {
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public /* final */ String A;
    @DexIgnore
    public HashMap B;
    @DexIgnore
    public /* final */ Calendar f; // = Calendar.getInstance();
    @DexIgnore
    public qw6<a65> g;
    @DexIgnore
    public jg6 h;
    @DexIgnore
    public sg6 i;
    @DexIgnore
    public rg6 j;
    @DexIgnore
    public int p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final lg6 a(Date date) {
            ee7.b(date, "date");
            lg6 lg6 = new lg6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            lg6.setArguments(bundle);
            return lg6;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ lg6 a;

        @DexIgnore
        public b(lg6 lg6, a65 a65) {
            this.a = lg6;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.z) && !TextUtils.isEmpty(this.a.A)) {
                int parseColor = Color.parseColor(this.a.z);
                int parseColor2 = Color.parseColor(this.a.A);
                gVar.b(2131230963);
                if (i == this.a.p) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ lg6 a;
        @DexIgnore
        public /* final */ /* synthetic */ a65 b;

        @DexIgnore
        public c(lg6 lg6, a65 a65) {
            this.a = lg6;
            this.b = a65;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "onPageScrolled " + i);
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.A)) {
                int parseColor = Color.parseColor(this.a.A);
                TabLayout.g b4 = this.b.w.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.z) && this.a.p != i) {
                int parseColor2 = Color.parseColor(this.a.z);
                TabLayout.g b5 = this.b.w.b(this.a.p);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.p = i;
            ViewPager2 viewPager2 = this.b.O;
            ee7.a((Object) viewPager2, "binding.rvHeartRateSleep");
            viewPager2.setCurrentItem(i);
        }
    }

    @DexIgnore
    public lg6() {
        String b2 = eh5.l.a().b("nonBrandSurface");
        String str = "#FFFFFF";
        this.t = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("backgroundDashboard");
        this.u = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("secondaryText");
        this.v = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("primaryText");
        this.w = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("nonBrandDisableCalendarDay");
        this.x = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandNonReachGoal");
        this.y = Color.parseColor(b7 != null ? b7 : str);
        this.z = eh5.l.a().b("disabledButton");
        this.A = eh5.l.a().b("primaryColor");
    }

    @DexIgnore
    @Override // com.fossil.kg6
    public void E() {
        a65 a2;
        ConstraintLayout constraintLayout;
        qw6<a65> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (constraintLayout = a2.r) != null) {
            constraintLayout.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.B;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "SleepDetailFragment";
    }

    @DexIgnore
    @Override // com.fossil.kg6
    public void j(List<sg6.b> list) {
        a65 a2;
        TabLayout tabLayout;
        a65 a3;
        TabLayout tabLayout2;
        ee7.b(list, "listOfSleepSessionUIData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetailChart - baseModel=" + list);
        if (list.size() > 1) {
            qw6<a65> qw6 = this.g;
            if (!(qw6 == null || (a3 = qw6.a()) == null || (tabLayout2 = a3.w) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            qw6<a65> qw62 = this.g;
            if (!(qw62 == null || (a2 = qw62.a()) == null || (tabLayout = a2.w) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        sg6 sg6 = this.i;
        if (sg6 != null) {
            sg6.a(list);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("SleepDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362636:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362637:
                    jg6 jg6 = this.h;
                    if (jg6 != null) {
                        jg6.j();
                        return;
                    }
                    return;
                case 2131362705:
                    jg6 jg62 = this.h;
                    if (jg62 != null) {
                        jg62.i();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        a65 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        a65 a65 = (a65) qb.a(layoutInflater, 2131558622, viewGroup, false, a1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        Date date = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            date = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Calendar calendar = this.f;
        ee7.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        ee7.a((Object) a65, "binding");
        a(a65);
        qw6<a65> qw6 = new qw6<>(this, a65);
        this.g = qw6;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        jg6 jg6 = this.h;
        if (jg6 != null) {
            jg6.g();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        jg6 jg6 = this.h;
        if (jg6 != null) {
            Calendar calendar = this.f;
            ee7.a((Object) calendar, "mCalendar");
            Date time = calendar.getTime();
            ee7.a((Object) time, "mCalendar.time");
            jg6.a(time);
        }
        jg6 jg62 = this.h;
        if (jg62 != null) {
            jg62.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        jg6 jg6 = this.h;
        if (jg6 != null) {
            jg6.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.fossil.kg6
    public void c(ArrayList<rg6.a> arrayList) {
        a65 a2;
        ConstraintLayout constraintLayout;
        ee7.b(arrayList, "sleepHeartRateDatumData");
        qw6<a65> qw6 = this.g;
        if (!(qw6 == null || (a2 = qw6.a()) == null || (constraintLayout = a2.r) == null)) {
            constraintLayout.setVisibility(0);
        }
        rg6 rg6 = this.j;
        if (rg6 != null) {
            rg6.a(arrayList);
        }
    }

    @DexIgnore
    public final void a(a65 a65) {
        String str;
        a65.H.setOnClickListener(this);
        a65.I.setOnClickListener(this);
        a65.K.setOnClickListener(this);
        a65.r.setBackgroundColor(this.u);
        a65.q.setBackgroundColor(this.t);
        a65.u.setBackgroundColor(this.t);
        be5.a aVar = be5.o;
        jg6 jg6 = this.h;
        if (aVar.a(jg6 != null ? jg6.h() : null)) {
            str = eh5.l.a().b("dianaSleepTab");
        } else {
            str = eh5.l.a().b("hybridSleepTab");
        }
        this.q = str;
        this.r = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.s = eh5.l.a().b("onDianaSleepTab");
        this.i = new sg6(new ArrayList());
        ViewPager2 viewPager2 = a65.P;
        ee7.a((Object) viewPager2, "rvSleeps");
        viewPager2.setAdapter(this.i);
        TabLayout tabLayout = a65.w;
        ee7.a((Object) tabLayout, "binding.cpiSleep");
        tabLayout.setBackgroundTintList(ColorStateList.valueOf(this.t));
        new ov3(a65.w, a65.P, new b(this, a65)).a();
        viewPager2.a(new c(this, a65));
        viewPager2.setCurrentItem(this.p);
        this.j = new rg6(new ArrayList());
        ViewPager2 viewPager22 = a65.O;
        ee7.a((Object) viewPager22, "rvHeartRateSleep");
        viewPager22.setAdapter(this.j);
        viewPager22.setCurrentItem(this.p);
        viewPager22.setUserInputEnabled(true);
    }

    @DexIgnore
    public void a(jg6 jg6) {
        ee7.b(jg6, "presenter");
        this.h = jg6;
    }

    @DexIgnore
    @Override // com.fossil.kg6
    public void a(Date date, boolean z2, boolean z3, boolean z4) {
        a65 a2;
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDay - date=" + date + ", isCreateAt=" + z2 + ", isToday=" + z3 + ", isDateAfter=" + z4);
        Calendar calendar = this.f;
        ee7.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        int i2 = this.f.get(7);
        qw6<a65> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            FlexibleTextView flexibleTextView = a2.B;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(this.f.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.I;
                ee7.a((Object) rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.I;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.K;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(ig5.a(getContext(), 2131886616));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.C;
                ee7.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(xe5.b.b(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.K;
            ee7.a((Object) rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.C;
            ee7.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(xe5.b.b(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.kg6
    public void a(MFSleepDay mFSleepDay) {
        a65 a2;
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetail - sleepDay=" + mFSleepDay);
        qw6<a65> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            ee7.a((Object) a2, "binding");
            View d = a2.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (mFSleepDay != null) {
                i2 = mFSleepDay.getSleepMinutes();
                i3 = mFSleepDay.getGoalMinutes();
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (i2 > 0) {
                int i4 = i2 / 60;
                int i5 = i2 - (i4 * 60);
                FlexibleTextView flexibleTextView = a2.z;
                ee7.a((Object) flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(String.valueOf(i4));
                FlexibleTextView flexibleTextView2 = a2.x;
                ee7.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                String a3 = ig5.a(context, 2131886834);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026Abbreviations_Hours__Hrs)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                    if (i5 > 0) {
                        FlexibleTextView flexibleTextView3 = a2.A;
                        ee7.a((Object) flexibleTextView3, "binding.ftvDailyValue2");
                        we7 we7 = we7.a;
                        Locale locale = Locale.US;
                        ee7.a((Object) locale, "Locale.US");
                        String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
                        ee7.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView3.setText(format);
                        FlexibleTextView flexibleTextView4 = a2.y;
                        ee7.a((Object) flexibleTextView4, "binding.ftvDailyUnit2");
                        String a4 = ig5.a(context, 2131886836);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                        if (a4 != null) {
                            String lowerCase2 = a4.toLowerCase();
                            ee7.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                            flexibleTextView4.setText(lowerCase2);
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        FlexibleTextView flexibleTextView5 = a2.A;
                        ee7.a((Object) flexibleTextView5, "binding.ftvDailyValue2");
                        flexibleTextView5.setText("");
                        FlexibleTextView flexibleTextView6 = a2.y;
                        ee7.a((Object) flexibleTextView6, "binding.ftvDailyUnit2");
                        flexibleTextView6.setText("");
                    }
                    ViewPager2 viewPager2 = a2.P;
                    ee7.a((Object) viewPager2, "binding.rvSleeps");
                    viewPager2.setVisibility(0);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView7 = a2.z;
                ee7.a((Object) flexibleTextView7, "binding.ftvDailyValue");
                flexibleTextView7.setText("");
                FlexibleTextView flexibleTextView8 = a2.x;
                ee7.a((Object) flexibleTextView8, "binding.ftvDailyUnit");
                String a5 = ig5.a(context, 2131886671);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (a5 != null) {
                    String upperCase = a5.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView8.setText(upperCase);
                    FlexibleTextView flexibleTextView9 = a2.A;
                    ee7.a((Object) flexibleTextView9, "binding.ftvDailyValue2");
                    flexibleTextView9.setText("");
                    FlexibleTextView flexibleTextView10 = a2.y;
                    ee7.a((Object) flexibleTextView10, "binding.ftvDailyUnit2");
                    flexibleTextView10.setText("");
                    ViewPager2 viewPager22 = a2.P;
                    ee7.a((Object) viewPager22, "binding.rvSleeps");
                    viewPager22.setVisibility(8);
                    TabLayout tabLayout = a2.w;
                    ee7.a((Object) tabLayout, "binding.cpiSleep");
                    tabLayout.setVisibility(8);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i6 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.C.setTextColor(this.t);
                a2.B.setTextColor(this.t);
                a2.x.setTextColor(this.t);
                a2.z.setTextColor(this.t);
                a2.y.setTextColor(this.t);
                a2.A.setTextColor(this.t);
                RTLImageView rTLImageView = a2.K;
                ee7.a((Object) rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.I;
                ee7.a((Object) rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                ee7.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView11 = a2.C;
                ee7.a((Object) flexibleTextView11, "binding.ftvDayOfWeek");
                flexibleTextView11.setSelected(true);
                FlexibleTextView flexibleTextView12 = a2.B;
                ee7.a((Object) flexibleTextView12, "binding.ftvDayOfMonth");
                flexibleTextView12.setSelected(true);
                View view = a2.L;
                ee7.a((Object) view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView13 = a2.z;
                ee7.a((Object) flexibleTextView13, "binding.ftvDailyValue");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = a2.x;
                ee7.a((Object) flexibleTextView14, "binding.ftvDailyUnit");
                flexibleTextView14.setSelected(true);
                String str = this.s;
                if (str != null) {
                    a2.C.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.L.setBackgroundColor(Color.parseColor(str));
                    a2.K.setColorFilter(Color.parseColor(str));
                    a2.I.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.q;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.B.setTextColor(this.w);
                a2.C.setTextColor(this.v);
                a2.x.setTextColor(this.y);
                a2.z.setTextColor(this.w);
                a2.y.setTextColor(this.y);
                a2.A.setTextColor(this.w);
                View view2 = a2.L;
                ee7.a((Object) view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.K;
                ee7.a((Object) rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.I;
                ee7.a((Object) rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i7 = this.y;
                a2.L.setBackgroundColor(i7);
                a2.K.setColorFilter(i7);
                a2.I.setColorFilter(i7);
                String str3 = this.r;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.B.setTextColor(this.w);
                a2.C.setTextColor(this.v);
                a2.z.setTextColor(this.x);
                a2.x.setTextColor(this.x);
                a2.y.setTextColor(this.x);
                a2.A.setTextColor(this.x);
                RTLImageView rTLImageView5 = a2.K;
                ee7.a((Object) rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.I;
                ee7.a((Object) rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                ee7.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView15 = a2.C;
                ee7.a((Object) flexibleTextView15, "binding.ftvDayOfWeek");
                flexibleTextView15.setSelected(false);
                FlexibleTextView flexibleTextView16 = a2.B;
                ee7.a((Object) flexibleTextView16, "binding.ftvDayOfMonth");
                flexibleTextView16.setSelected(false);
                View view3 = a2.L;
                ee7.a((Object) view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView17 = a2.z;
                ee7.a((Object) flexibleTextView17, "binding.ftvDailyValue");
                flexibleTextView17.setSelected(false);
                FlexibleTextView flexibleTextView18 = a2.x;
                ee7.a((Object) flexibleTextView18, "binding.ftvDailyUnit");
                flexibleTextView18.setSelected(false);
                int i8 = this.y;
                a2.L.setBackgroundColor(i8);
                a2.K.setColorFilter(i8);
                a2.I.setColorFilter(i8);
                String str4 = this.r;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i6 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.M;
                ee7.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView19 = a2.G;
                ee7.a((Object) flexibleTextView19, "binding.ftvProgressValue");
                flexibleTextView19.setText(ig5.a(context, 2131887288));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.M;
                ee7.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i6);
                FlexibleTextView flexibleTextView20 = a2.G;
                ee7.a((Object) flexibleTextView20, "binding.ftvProgressValue");
                flexibleTextView20.setText(i6 + "%");
            }
            if (i3 < 60) {
                FlexibleTextView flexibleTextView21 = a2.D;
                ee7.a((Object) flexibleTextView21, "binding.ftvGoalValue");
                we7 we72 = we7.a;
                String a6 = ig5.a(context, 2131886568);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
                String format2 = String.format(a6, Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
                ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                flexibleTextView21.setText(format2);
                return;
            }
            FlexibleTextView flexibleTextView22 = a2.D;
            ee7.a((Object) flexibleTextView22, "binding.ftvGoalValue");
            we7 we73 = we7.a;
            String a7 = ig5.a(context, 2131886728);
            ee7.a((Object) a7, "LanguageHelper.getString\u2026ecord_Title__OfNumberHrs)");
            String format3 = String.format(a7, Arrays.copyOf(new Object[]{re5.a(((float) i3) / 60.0f, 1)}, 1));
            ee7.a((Object) format3, "java.lang.String.format(format, *args)");
            flexibleTextView22.setText(format3);
        }
    }
}
