package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zg0 implements Serializable {
    TOP_PRESS,
    TOP_HOLD,
    TOP_SHORT_PRESS_RELEASE,
    TOP_LONG_PRESS_RELEASE,
    TOP_SINGLE_CLICK,
    TOP_DOUBLE_CLICK,
    MIDDLE_PRESS,
    MIDDLE_HOLD,
    MIDDLE_SHORT_PRESS_RELEASE,
    MIDDLE_LONG_PRESS_RELEASE,
    MIDDLE_SINGLE_CLICK,
    MIDDLE_DOUBLE_CLICK,
    BOTTOM_PRESS,
    BOTTOM_HOLD,
    BOTTOM_SHORT_PRESS_RELEASE,
    BOTTOM_LONG_PRESS_RELEASE,
    BOTTOM_SINGLE_CLICK,
    BOTTOM_DOUBLE_CLICK;
    
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final zg0 a(String str) {
            zg0[] values = zg0.values();
            for (zg0 zg0 : values) {
                if (ee7.a((Object) yz0.a(zg0), (Object) str)) {
                    return zg0;
                }
            }
            return null;
        }
    }
}
