package com.fossil;

import com.fossil.qm;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo {
    @DexIgnore
    public String a;
    @DexIgnore
    public qm.a b; // = qm.a.ENQUEUED;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public cm e;
    @DexIgnore
    public cm f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public am j;
    @DexIgnore
    public int k;
    @DexIgnore
    public yl l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public long p;
    @DexIgnore
    public boolean q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements t3<List<c>, List<qm>> {
        @DexIgnore
        /* renamed from: a */
        public List<qm> apply(List<c> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (c cVar : list) {
                arrayList.add(cVar.a());
            }
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public qm.a b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b != bVar.b) {
                return false;
            }
            return this.a.equals(bVar.a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public String a;
        @DexIgnore
        public qm.a b;
        @DexIgnore
        public cm c;
        @DexIgnore
        public int d;
        @DexIgnore
        public List<String> e;
        @DexIgnore
        public List<cm> f;

        @DexIgnore
        public qm a() {
            cm cmVar;
            List<cm> list = this.f;
            if (list == null || list.isEmpty()) {
                cmVar = cm.c;
            } else {
                cmVar = this.f.get(0);
            }
            return new qm(UUID.fromString(this.a), this.b, this.c, this.e, cmVar, this.d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (this.d != cVar.d) {
                return false;
            }
            String str = this.a;
            if (str == null ? cVar.a != null : !str.equals(cVar.a)) {
                return false;
            }
            if (this.b != cVar.b) {
                return false;
            }
            cm cmVar = this.c;
            if (cmVar == null ? cVar.c != null : !cmVar.equals(cVar.c)) {
                return false;
            }
            List<String> list = this.e;
            if (list == null ? cVar.e != null : !list.equals(cVar.e)) {
                return false;
            }
            List<cm> list2 = this.f;
            List<cm> list3 = cVar.f;
            if (list2 != null) {
                return list2.equals(list3);
            }
            if (list3 == null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            qm.a aVar = this.b;
            int hashCode2 = (hashCode + (aVar != null ? aVar.hashCode() : 0)) * 31;
            cm cmVar = this.c;
            int hashCode3 = (((hashCode2 + (cmVar != null ? cmVar.hashCode() : 0)) * 31) + this.d) * 31;
            List<String> list = this.e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            List<cm> list2 = this.f;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode4 + i;
        }
    }

    /*
    static {
        im.a("WorkSpec");
        new a();
    }
    */

    @DexIgnore
    public zo(String str, String str2) {
        cm cmVar = cm.c;
        this.e = cmVar;
        this.f = cmVar;
        this.j = am.i;
        this.l = yl.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = str;
        this.c = str2;
    }

    @DexIgnore
    public long a() {
        long j2;
        boolean z = false;
        if (c()) {
            if (this.l == yl.LINEAR) {
                z = true;
            }
            if (z) {
                j2 = this.m * ((long) this.k);
            } else {
                j2 = (long) Math.scalb((float) this.m, this.k - 1);
            }
            return this.n + Math.min(18000000L, j2);
        }
        long j3 = 0;
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j4 = this.n;
            if (j4 == 0) {
                j4 = this.g + currentTimeMillis;
            }
            if (this.i != this.h) {
                z = true;
            }
            if (z) {
                if (this.n == 0) {
                    j3 = this.i * -1;
                }
                return j4 + this.h + j3;
            }
            if (this.n != 0) {
                j3 = this.h;
            }
            return j4 + j3;
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.g;
    }

    @DexIgnore
    public boolean b() {
        return !am.i.equals(this.j);
    }

    @DexIgnore
    public boolean c() {
        return this.b == qm.a.ENQUEUED && this.k > 0;
    }

    @DexIgnore
    public boolean d() {
        return this.h != 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zo)) {
            return false;
        }
        zo zoVar = (zo) obj;
        if (this.g != zoVar.g || this.h != zoVar.h || this.i != zoVar.i || this.k != zoVar.k || this.m != zoVar.m || this.n != zoVar.n || this.o != zoVar.o || this.p != zoVar.p || this.q != zoVar.q || !this.a.equals(zoVar.a) || this.b != zoVar.b || !this.c.equals(zoVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null ? zoVar.d != null : !str.equals(zoVar.d)) {
            return false;
        }
        if (this.e.equals(zoVar.e) && this.f.equals(zoVar.f) && this.j.equals(zoVar.j) && this.l == zoVar.l) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31;
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j2 = this.g;
        long j3 = this.h;
        long j4 = this.i;
        long j5 = this.m;
        long j6 = this.n;
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.j.hashCode()) * 31) + this.k) * 31) + this.l.hashCode()) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + (this.q ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return "{WorkSpec: " + this.a + "}";
    }

    @DexIgnore
    public zo(zo zoVar) {
        cm cmVar = cm.c;
        this.e = cmVar;
        this.f = cmVar;
        this.j = am.i;
        this.l = yl.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = zoVar.a;
        this.c = zoVar.c;
        this.b = zoVar.b;
        this.d = zoVar.d;
        this.e = new cm(zoVar.e);
        this.f = new cm(zoVar.f);
        this.g = zoVar.g;
        this.h = zoVar.h;
        this.i = zoVar.i;
        this.j = new am(zoVar.j);
        this.k = zoVar.k;
        this.l = zoVar.l;
        this.m = zoVar.m;
        this.n = zoVar.n;
        this.o = zoVar.o;
        this.p = zoVar.p;
        this.q = zoVar.q;
    }
}
