package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rn3> CREATOR; // = new qn3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ b72 b;

    @DexIgnore
    public rn3(int i, b72 b72) {
        this.a = i;
        this.b = b72;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, (Parcelable) this.b, i, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public rn3(b72 b72) {
        this(1, b72);
    }
}
