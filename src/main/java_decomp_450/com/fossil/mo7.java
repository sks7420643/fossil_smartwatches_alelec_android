package com.fossil;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mo7 implements Closeable {
    @DexIgnore
    public Reader reader;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mo7 {
        @DexIgnore
        public /* final */ /* synthetic */ ho7 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;
        @DexIgnore
        public /* final */ /* synthetic */ ar7 c;

        @DexIgnore
        public a(ho7 ho7, long j, ar7 ar7) {
            this.a = ho7;
            this.b = j;
            this.c = ar7;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public long contentLength() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ho7 contentType() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ar7 source() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Reader {
        @DexIgnore
        public /* final */ ar7 a;
        @DexIgnore
        public /* final */ Charset b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public Reader d;

        @DexIgnore
        public b(ar7 ar7, Charset charset) {
            this.a = ar7;
            this.b = charset;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.io.Reader, java.lang.AutoCloseable
        public void close() throws IOException {
            this.c = true;
            Reader reader = this.d;
            if (reader != null) {
                reader.close();
            } else {
                this.a.close();
            }
        }

        @DexIgnore
        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) throws IOException {
            if (!this.c) {
                Reader reader = this.d;
                if (reader == null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(this.a.u(), ro7.a(this.a, this.b));
                    this.d = inputStreamReader;
                    reader = inputStreamReader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    @DexIgnore
    private Charset charset() {
        ho7 contentType = contentType();
        return contentType != null ? contentType.a(ro7.i) : ro7.i;
    }

    @DexIgnore
    public static mo7 create(ho7 ho7, String str) {
        Charset charset = ro7.i;
        if (ho7 != null && (charset = ho7.a()) == null) {
            charset = ro7.i;
            ho7 = ho7.b(ho7 + "; charset=utf-8");
        }
        yq7 yq7 = new yq7();
        yq7.a(str, charset);
        return create(ho7, yq7.x(), yq7);
    }

    @DexIgnore
    public final InputStream byteStream() {
        return source().u();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength <= 2147483647L) {
            ar7 source = source();
            try {
                byte[] f = source.f();
                ro7.a(source);
                if (contentLength == -1 || contentLength == ((long) f.length)) {
                    return f;
                }
                throw new IOException("Content-Length (" + contentLength + ") and stream length (" + f.length + ") disagree");
            } catch (Throwable th) {
                ro7.a(source);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + contentLength);
        }
    }

    @DexIgnore
    public final Reader charStream() {
        Reader reader2 = this.reader;
        if (reader2 != null) {
            return reader2;
        }
        b bVar = new b(source(), charset());
        this.reader = bVar;
        return bVar;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        ro7.a(source());
    }

    @DexIgnore
    public abstract long contentLength();

    @DexIgnore
    public abstract ho7 contentType();

    @DexIgnore
    public abstract ar7 source();

    @DexIgnore
    public final String string() throws IOException {
        ar7 source = source();
        try {
            return source.a(ro7.a(source, charset()));
        } finally {
            ro7.a(source);
        }
    }

    @DexIgnore
    public static mo7 create(ho7 ho7, byte[] bArr) {
        yq7 yq7 = new yq7();
        yq7.write(bArr);
        return create(ho7, (long) bArr.length, yq7);
    }

    @DexIgnore
    public static mo7 create(ho7 ho7, br7 br7) {
        yq7 yq7 = new yq7();
        yq7.a(br7);
        return create(ho7, (long) br7.size(), yq7);
    }

    @DexIgnore
    public static mo7 create(ho7 ho7, long j, ar7 ar7) {
        if (ar7 != null) {
            return new a(ho7, j, ar7);
        }
        throw new NullPointerException("source == null");
    }
}
