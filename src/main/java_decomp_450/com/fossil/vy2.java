package com.fossil;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy2 extends AbstractList<String> implements tw2, RandomAccess {
    @DexIgnore
    public /* final */ tw2 a;

    @DexIgnore
    public vy2(tw2 tw2) {
        this.a = tw2;
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final void a(tu2 tu2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ String get(int i) {
        return (String) this.a.get(i);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, java.lang.Iterable
    public final Iterator<String> iterator() {
        return new xy2(this);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final ListIterator<String> listIterator(int i) {
        return new yy2(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final Object zzb(int i) {
        return this.a.zzb(i);
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final List<?> zzd() {
        return this.a.zzd();
    }

    @DexIgnore
    @Override // com.fossil.tw2
    public final tw2 zze() {
        return this;
    }
}
