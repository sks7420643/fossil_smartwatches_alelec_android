package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kw5 extends iw5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public DNDScheduledTimeModel g;
    @DexIgnore
    public DNDScheduledTimeModel h;
    @DexIgnore
    public /* final */ jw5 i;
    @DexIgnore
    public /* final */ DNDSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kw5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kw5$b$a$a")
            /* renamed from: com.fossil.kw5$b$a$a  reason: collision with other inner class name */
            public static final class RunnableC0106a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel a;
                @DexIgnore
                public /* final */ /* synthetic */ a b;

                @DexIgnore
                public RunnableC0106a(DNDScheduledTimeModel dNDScheduledTimeModel, a aVar) {
                    this.a = dNDScheduledTimeModel;
                    this.b = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.a);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kw5$b$a$b")
            /* renamed from: com.fossil.kw5$b$a$b  reason: collision with other inner class name */
            public static final class RunnableC0107b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel a;
                @DexIgnore
                public /* final */ /* synthetic */ a b;

                @DexIgnore
                public RunnableC0107b(DNDScheduledTimeModel dNDScheduledTimeModel, a aVar) {
                    this.a = dNDScheduledTimeModel;
                    this.b = aVar;
                }

                @DexIgnore
                public final void run() {
                    this.b.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.a);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    if (this.this$0.this$0.f == 0) {
                        DNDScheduledTimeModel c = this.this$0.this$0.g;
                        if (c == null) {
                            return null;
                        }
                        c.setMinutes(this.this$0.this$0.e);
                        this.this$0.this$0.j.runInTransaction(new RunnableC0106a(c, this));
                        return i97.a;
                    }
                    DNDScheduledTimeModel b = this.this$0.this$0.h;
                    if (b == null) {
                        return null;
                    }
                    b.setMinutes(this.this$0.this$0.e);
                    this.this$0.this$0.j.runInTransaction(new RunnableC0107b(b, this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kw5 kw5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kw5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kw5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends DNDScheduledTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends DNDScheduledTimeModel>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.j.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kw5 kw5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kw5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) obj) {
                if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$0.f) {
                    jw5 g = this.this$0.i;
                    kw5 kw5 = this.this$0;
                    g.a(kw5.b(kw5.f));
                    this.this$0.i.a(dNDScheduledTimeModel.getMinutes());
                }
                if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                    this.this$0.g = dNDScheduledTimeModel;
                } else {
                    this.this$0.h = dNDScheduledTimeModel;
                }
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = kw5.class.getSimpleName();
        ee7.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public kw5(jw5 jw5, DNDSettingsDatabase dNDSettingsDatabase) {
        ee7.b(jw5, "mView");
        ee7.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.i = jw5;
        this.j = dNDSettingsDatabase;
    }

    @DexIgnore
    @Override // com.fossil.iw5
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    public final String b(int i2) {
        if (i2 == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886122);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return a2;
        }
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886120);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return a3;
    }

    @DexIgnore
    @Override // com.fossil.iw5
    public void a(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
    @Override // com.fossil.iw5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r6, java.lang.String r7, boolean r8) {
        /*
            r5 = this;
            java.lang.String r0 = "hourValue"
            com.fossil.ee7.b(r6, r0)
            java.lang.String r0 = "minuteValue"
            com.fossil.ee7.b(r7, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.kw5.k
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "updateTime: hourValue = "
            r2.append(r3)
            r2.append(r6)
            java.lang.String r3 = ", minuteValue = "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r3 = ", isPM = "
            r2.append(r3)
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            r0 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r1 = "Integer.valueOf(hourValue)"
            com.fossil.ee7.a(r6, r1)     // Catch:{ Exception -> 0x0054 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0054 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0052 }
            java.lang.String r1 = "Integer.valueOf(minuteValue)"
            com.fossil.ee7.a(r7, r1)     // Catch:{ Exception -> 0x0052 }
            int r7 = r7.intValue()     // Catch:{ Exception -> 0x0052 }
            goto L_0x0073
        L_0x0052:
            r7 = move-exception
            goto L_0x0056
        L_0x0054:
            r7 = move-exception
            r6 = 0
        L_0x0056:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.kw5.k
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Exception when parse time e="
            r3.append(r4)
            r3.append(r7)
            java.lang.String r7 = r3.toString()
            r1.e(r2, r7)
            r7 = 0
        L_0x0073:
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            boolean r1 = android.text.format.DateFormat.is24HourFormat(r1)
            r2 = 12
            if (r1 == 0) goto L_0x0095
            if (r8 == 0) goto L_0x008b
            if (r6 != r2) goto L_0x0088
            r0 = 12
            goto L_0x008f
        L_0x0088:
            int r0 = r6 + 12
            goto L_0x008f
        L_0x008b:
            if (r6 != r2) goto L_0x008e
            goto L_0x008f
        L_0x008e:
            r0 = r6
        L_0x008f:
            int r0 = r0 * 60
            int r0 = r0 + r7
            r5.e = r0
            goto L_0x00a8
        L_0x0095:
            if (r8 != 0) goto L_0x009c
            if (r6 != r2) goto L_0x009a
            goto L_0x00a3
        L_0x009a:
            r0 = r6
            goto L_0x00a3
        L_0x009c:
            if (r6 != r2) goto L_0x00a1
            r0 = 12
            goto L_0x00a3
        L_0x00a1:
            int r0 = r6 + 12
        L_0x00a3:
            int r0 = r0 * 60
            int r0 = r0 + r7
            r5.e = r0
        L_0x00a8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kw5.a(java.lang.String, java.lang.String, boolean):void");
    }
}
