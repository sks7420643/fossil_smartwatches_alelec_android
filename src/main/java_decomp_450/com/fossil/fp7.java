package com.fossil;

import com.fossil.ep7;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp7 {
    @DexIgnore
    public /* final */ nn7 a;
    @DexIgnore
    public ep7.a b;
    @DexIgnore
    public no7 c;
    @DexIgnore
    public /* final */ vn7 d;
    @DexIgnore
    public /* final */ qn7 e;
    @DexIgnore
    public /* final */ co7 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ ep7 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public bp7 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ip7 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends WeakReference<fp7> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public a(fp7 fp7, Object obj) {
            super(fp7);
            this.a = obj;
        }
    }

    @DexIgnore
    public fp7(vn7 vn7, nn7 nn7, qn7 qn7, co7 co7, Object obj) {
        this.d = vn7;
        this.a = nn7;
        this.e = qn7;
        this.f = co7;
        this.h = new ep7(nn7, i(), qn7, co7);
        this.g = obj;
    }

    @DexIgnore
    public ip7 a(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            ip7 a2 = a(chain.e(), chain.a(), chain.b(), okHttpClient.w(), okHttpClient.C(), z).a(okHttpClient, chain, this);
            synchronized (this.d) {
                this.n = a2;
            }
            return a2;
        } catch (IOException e2) {
            throw new dp7(e2);
        }
    }

    @DexIgnore
    public ip7 b() {
        ip7 ip7;
        synchronized (this.d) {
            ip7 = this.n;
        }
        return ip7;
    }

    @DexIgnore
    public synchronized bp7 c() {
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        ep7.a aVar;
        return this.c != null || ((aVar = this.b) != null && aVar.b()) || this.h.a();
    }

    @DexIgnore
    public void e() {
        bp7 bp7;
        Socket a2;
        synchronized (this.d) {
            bp7 = this.j;
            a2 = a(true, false, false);
            if (this.j != null) {
                bp7 = null;
            }
        }
        ro7.a(a2);
        if (bp7 != null) {
            this.f.b(this.e, bp7);
        }
    }

    @DexIgnore
    public void f() {
        bp7 bp7;
        Socket a2;
        synchronized (this.d) {
            bp7 = this.j;
            a2 = a(false, true, false);
            if (this.j != null) {
                bp7 = null;
            }
        }
        ro7.a(a2);
        if (bp7 != null) {
            po7.a.a(this.e, (IOException) null);
            this.f.b(this.e, bp7);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket g() {
        bp7 bp7 = this.j;
        if (bp7 == null || !bp7.k) {
            return null;
        }
        return a(false, false, true);
    }

    @DexIgnore
    public no7 h() {
        return this.c;
    }

    @DexIgnore
    public final cp7 i() {
        return po7.a.a(this.d);
    }

    @DexIgnore
    public String toString() {
        bp7 c2 = c();
        return c2 != null ? c2.toString() : this.a.toString();
    }

    @DexIgnore
    public Socket b(bp7 bp7) {
        if (this.n == null && this.j.n.size() == 1) {
            Socket a2 = a(true, false, false);
            this.j = bp7;
            bp7.n.add(this.j.n.get(0));
            return a2;
        }
        throw new IllegalStateException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r0.a(r9) != false) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        return r0;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.bp7 a(int r4, int r5, int r6, int r7, boolean r8, boolean r9) throws java.io.IOException {
        /*
            r3 = this;
        L_0x0000:
            com.fossil.bp7 r0 = r3.a(r4, r5, r6, r7, r8)
            com.fossil.vn7 r1 = r3.d
            monitor-enter(r1)
            int r2 = r0.l     // Catch:{ all -> 0x001f }
            if (r2 != 0) goto L_0x0013
            boolean r2 = r0.e()     // Catch:{ all -> 0x001f }
            if (r2 != 0) goto L_0x0013
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            return r0
        L_0x0013:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            boolean r1 = r0.a(r9)
            if (r1 != 0) goto L_0x001e
            r3.e()
            goto L_0x0000
        L_0x001e:
            return r0
        L_0x001f:
            r4 = move-exception
            monitor-exit(r1)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fp7.a(int, int, int, int, boolean, boolean):com.fossil.bp7");
    }

    @DexIgnore
    public final bp7 a(int i2, int i3, int i4, int i5, boolean z) throws IOException {
        Socket g2;
        Socket socket;
        bp7 bp7;
        bp7 bp72;
        boolean z2;
        no7 no7;
        boolean z3;
        ep7.a aVar;
        synchronized (this.d) {
            if (this.l) {
                throw new IllegalStateException("released");
            } else if (this.n != null) {
                throw new IllegalStateException("codec != null");
            } else if (!this.m) {
                bp7 bp73 = this.j;
                g2 = g();
                socket = null;
                if (this.j != null) {
                    bp72 = this.j;
                    bp7 = null;
                } else {
                    bp7 = bp73;
                    bp72 = null;
                }
                if (!this.k) {
                    bp7 = null;
                }
                if (bp72 == null) {
                    po7.a.a(this.d, this.a, this, null);
                    if (this.j != null) {
                        bp72 = this.j;
                        no7 = null;
                        z2 = true;
                    } else {
                        no7 = this.c;
                    }
                } else {
                    no7 = null;
                }
                z2 = false;
            } else {
                throw new IOException("Canceled");
            }
        }
        ro7.a(g2);
        if (bp7 != null) {
            this.f.b(this.e, bp7);
        }
        if (z2) {
            this.f.a(this.e, bp72);
        }
        if (bp72 != null) {
            this.c = this.j.f();
            return bp72;
        }
        if (no7 != null || ((aVar = this.b) != null && aVar.b())) {
            z3 = false;
        } else {
            this.b = this.h.c();
            z3 = true;
        }
        synchronized (this.d) {
            if (!this.m) {
                if (z3) {
                    List<no7> a2 = this.b.a();
                    int size = a2.size();
                    int i6 = 0;
                    while (true) {
                        if (i6 >= size) {
                            break;
                        }
                        no7 no72 = a2.get(i6);
                        po7.a.a(this.d, this.a, this, no72);
                        if (this.j != null) {
                            bp72 = this.j;
                            this.c = no72;
                            z2 = true;
                            break;
                        }
                        i6++;
                    }
                }
                if (!z2) {
                    if (no7 == null) {
                        no7 = this.b.c();
                    }
                    this.c = no7;
                    this.i = 0;
                    bp72 = new bp7(this.d, no7);
                    a(bp72, false);
                }
            } else {
                throw new IOException("Canceled");
            }
        }
        if (z2) {
            this.f.a(this.e, bp72);
            return bp72;
        }
        bp72.a(i2, i3, i4, i5, z, this.e, this.f);
        i().a(bp72.f());
        synchronized (this.d) {
            this.k = true;
            po7.a.b(this.d, bp72);
            if (bp72.e()) {
                socket = po7.a.a(this.d, this.a, this);
                bp72 = this.j;
            }
        }
        ro7.a(socket);
        this.f.a(this.e, bp72);
        return bp72;
    }

    @DexIgnore
    public void a(boolean z, ip7 ip7, long j2, IOException iOException) {
        bp7 bp7;
        Socket a2;
        boolean z2;
        this.f.b(this.e, j2);
        synchronized (this.d) {
            if (ip7 != null) {
                if (ip7 == this.n) {
                    if (!z) {
                        this.j.l++;
                    }
                    bp7 = this.j;
                    a2 = a(z, false, true);
                    if (this.j != null) {
                        bp7 = null;
                    }
                    z2 = this.l;
                }
            }
            throw new IllegalStateException("expected " + this.n + " but was " + ip7);
        }
        ro7.a(a2);
        if (bp7 != null) {
            this.f.b(this.e, bp7);
        }
        if (iOException != null) {
            this.f.a(this.e, po7.a.a(this.e, iOException));
        } else if (z2) {
            po7.a.a(this.e, (IOException) null);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket a(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.n = null;
        }
        if (z2) {
            this.l = true;
        }
        bp7 bp7 = this.j;
        if (bp7 == null) {
            return null;
        }
        if (z) {
            bp7.k = true;
        }
        if (this.n != null) {
            return null;
        }
        if (!this.l && !this.j.k) {
            return null;
        }
        a(this.j);
        if (this.j.n.isEmpty()) {
            this.j.o = System.nanoTime();
            if (po7.a.a(this.d, this.j)) {
                socket = this.j.g();
                this.j = null;
                return socket;
            }
        }
        socket = null;
        this.j = null;
        return socket;
    }

    @DexIgnore
    public void a() {
        ip7 ip7;
        bp7 bp7;
        synchronized (this.d) {
            this.m = true;
            ip7 = this.n;
            bp7 = this.j;
        }
        if (ip7 != null) {
            ip7.cancel();
        } else if (bp7 != null) {
            bp7.b();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0055  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.io.IOException r7) {
        /*
            r6 = this;
            com.fossil.vn7 r0 = r6.d
            monitor-enter(r0)
            boolean r1 = r7 instanceof com.fossil.fq7     // Catch:{ all -> 0x0069 }
            r2 = 0
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x0023
            com.fossil.fq7 r7 = (com.fossil.fq7) r7     // Catch:{ all -> 0x0069 }
            com.fossil.tp7 r7 = r7.errorCode     // Catch:{ all -> 0x0069 }
            com.fossil.tp7 r1 = com.fossil.tp7.REFUSED_STREAM     // Catch:{ all -> 0x0069 }
            if (r7 != r1) goto L_0x001c
            int r7 = r6.i     // Catch:{ all -> 0x0069 }
            int r7 = r7 + r4
            r6.i = r7     // Catch:{ all -> 0x0069 }
            if (r7 <= r4) goto L_0x004a
            r6.c = r3     // Catch:{ all -> 0x0069 }
            goto L_0x0048
        L_0x001c:
            com.fossil.tp7 r1 = com.fossil.tp7.CANCEL     // Catch:{ all -> 0x0069 }
            if (r7 == r1) goto L_0x004a
            r6.c = r3     // Catch:{ all -> 0x0069 }
            goto L_0x0048
        L_0x0023:
            com.fossil.bp7 r1 = r6.j     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x004a
            com.fossil.bp7 r1 = r6.j     // Catch:{ all -> 0x0069 }
            boolean r1 = r1.e()     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x0033
            boolean r1 = r7 instanceof com.fossil.sp7     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x004a
        L_0x0033:
            com.fossil.bp7 r1 = r6.j     // Catch:{ all -> 0x0069 }
            int r1 = r1.l     // Catch:{ all -> 0x0069 }
            if (r1 != 0) goto L_0x0048
            com.fossil.no7 r1 = r6.c     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x0046
            if (r7 == 0) goto L_0x0046
            com.fossil.ep7 r1 = r6.h     // Catch:{ all -> 0x0069 }
            com.fossil.no7 r5 = r6.c     // Catch:{ all -> 0x0069 }
            r1.a(r5, r7)     // Catch:{ all -> 0x0069 }
        L_0x0046:
            r6.c = r3     // Catch:{ all -> 0x0069 }
        L_0x0048:
            r7 = 1
            goto L_0x004b
        L_0x004a:
            r7 = 0
        L_0x004b:
            com.fossil.bp7 r1 = r6.j     // Catch:{ all -> 0x0069 }
            java.net.Socket r7 = r6.a(r7, r2, r4)     // Catch:{ all -> 0x0069 }
            com.fossil.bp7 r2 = r6.j     // Catch:{ all -> 0x0069 }
            if (r2 != 0) goto L_0x005b
            boolean r2 = r6.k     // Catch:{ all -> 0x0069 }
            if (r2 != 0) goto L_0x005a
            goto L_0x005b
        L_0x005a:
            r3 = r1
        L_0x005b:
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            com.fossil.ro7.a(r7)
            if (r3 == 0) goto L_0x0068
            com.fossil.co7 r7 = r6.f
            com.fossil.qn7 r0 = r6.e
            r7.b(r0, r3)
        L_0x0068:
            return
        L_0x0069:
            r7 = move-exception
            monitor-exit(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fp7.a(java.io.IOException):void");
    }

    @DexIgnore
    public void a(bp7 bp7, boolean z) {
        if (this.j == null) {
            this.j = bp7;
            this.k = z;
            bp7.n.add(new a(this, this.g));
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final void a(bp7 bp7) {
        int size = bp7.n.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (bp7.n.get(i2).get() == this) {
                bp7.n.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }
}
