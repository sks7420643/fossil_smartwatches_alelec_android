package com.fossil;

import com.fossil.cy3;
import com.fossil.dy3;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oz3<K, V> extends ux3<K, V> {
    @DexIgnore
    public static /* final */ oz3<Object, Object> EMPTY; // = new oz3<>(null, null, by3.EMPTY_ENTRY_ARRAY, 0, 0);
    @DexIgnore
    public static /* final */ double MAX_LOAD_FACTOR; // = 1.2d;
    @DexIgnore
    public /* final */ transient cy3<K, V>[] e;
    @DexIgnore
    public /* final */ transient cy3<K, V>[] f;
    @DexIgnore
    public /* final */ transient Map.Entry<K, V>[] g;
    @DexIgnore
    public /* final */ transient int h;
    @DexIgnore
    public /* final */ transient int i;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient ux3<V, K> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends ux3<V, K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends dy3<V, K> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.oz3$b$a$a")
            /* renamed from: com.fossil.oz3$b$a$a  reason: collision with other inner class name */
            public class C0154a extends tx3<Map.Entry<V, K>> {
                @DexIgnore
                public C0154a() {
                }

                @DexIgnore
                @Override // com.fossil.tx3
                public vx3<Map.Entry<V, K>> delegateCollection() {
                    return a.this;
                }

                @DexIgnore
                @Override // java.util.List
                public Map.Entry<V, K> get(int i) {
                    Map.Entry entry = oz3.this.g[i];
                    return yy3.a(entry.getValue(), entry.getKey());
                }
            }

            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.iy3
            public zx3<Map.Entry<V, K>> createAsList() {
                return new C0154a();
            }

            @DexIgnore
            @Override // com.fossil.dy3, com.fossil.iy3
            public int hashCode() {
                return oz3.this.i;
            }

            @DexIgnore
            @Override // com.fossil.dy3, com.fossil.iy3
            public boolean isHashCodeFast() {
                return true;
            }

            @DexIgnore
            @Override // com.fossil.dy3
            public by3<V, K> map() {
                return b.this;
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
            public j04<Map.Entry<V, K>> iterator() {
                return asList().iterator();
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.by3
        public iy3<Map.Entry<V, K>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        @Override // java.util.Map, com.fossil.by3
        public K get(Object obj) {
            if (!(obj == null || oz3.this.f == null)) {
                for (cy3 cy3 = oz3.this.f[sx3.a(obj.hashCode()) & oz3.this.h]; cy3 != null; cy3 = cy3.getNextInValueBucket()) {
                    if (obj.equals(cy3.getValue())) {
                        return (K) cy3.getKey();
                    }
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.by3
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return inverse().size();
        }

        @DexIgnore
        @Override // com.fossil.ux3, com.fossil.by3
        public Object writeReplace() {
            return new c(oz3.this);
        }

        @DexIgnore
        @Override // com.fossil.ux3, com.fossil.ux3
        public ux3<K, V> inverse() {
            return oz3.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ ux3<K, V> forward;

        @DexIgnore
        public c(ux3<K, V> ux3) {
            this.forward = ux3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.forward.inverse();
        }
    }

    @DexIgnore
    public oz3(cy3<K, V>[] cy3Arr, cy3<K, V>[] cy3Arr2, Map.Entry<K, V>[] entryArr, int i2, int i3) {
        this.e = cy3Arr;
        this.f = cy3Arr2;
        this.g = entryArr;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    public static void a(Object obj, Map.Entry<?, ?> entry, cy3<?, ?> cy3) {
        while (cy3 != null) {
            by3.checkNoConflict(!obj.equals(cy3.getValue()), "value", entry, cy3);
            cy3 = cy3.getNextInValueBucket();
        }
    }

    @DexIgnore
    public static <K, V> oz3<K, V> fromEntries(Map.Entry<K, V>... entryArr) {
        return fromEntryArray(entryArr.length, entryArr);
    }

    @DexIgnore
    public static <K, V> oz3<K, V> fromEntryArray(int i2, Map.Entry<K, V>[] entryArr) {
        cy3[] cy3Arr;
        cy3 cy3;
        int i3 = i2;
        jw3.b(i3, entryArr.length);
        int a2 = sx3.a(i3, 1.2d);
        int i4 = a2 - 1;
        cy3[] createEntryArray = cy3.createEntryArray(a2);
        cy3[] createEntryArray2 = cy3.createEntryArray(a2);
        if (i3 == entryArr.length) {
            cy3Arr = entryArr;
        } else {
            cy3Arr = cy3.createEntryArray(i2);
        }
        int i5 = 0;
        int i6 = 0;
        while (i5 < i3) {
            Map.Entry<K, V> entry = entryArr[i5];
            K key = entry.getKey();
            V value = entry.getValue();
            bx3.a(key, value);
            int hashCode = key.hashCode();
            int hashCode2 = value.hashCode();
            int a3 = sx3.a(hashCode) & i4;
            int a4 = sx3.a(hashCode2) & i4;
            cy3 cy32 = createEntryArray[a3];
            qz3.checkNoConflictInKeyBucket(key, entry, cy32);
            cy3 cy33 = createEntryArray2[a4];
            a(value, entry, cy33);
            if (cy33 == null && cy32 == null) {
                cy3 = (entry instanceof cy3) && ((cy3) entry).isReusable() ? (cy3) entry : new cy3(key, value);
            } else {
                cy3 = new cy3.a(key, value, cy32, cy33);
            }
            createEntryArray[a3] = cy3;
            createEntryArray2[a4] = cy3;
            cy3Arr[i5] = cy3;
            i6 += hashCode ^ hashCode2;
            i5++;
            i3 = i2;
        }
        return new oz3<>(createEntryArray, createEntryArray2, cy3Arr, i4, i6);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? iy3.of() : new dy3.b(this, this.g);
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3
    public V get(Object obj) {
        cy3<K, V>[] cy3Arr = this.e;
        if (cy3Arr == null) {
            return null;
        }
        return (V) qz3.get(obj, cy3Arr, this.h);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public int hashCode() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.g.length;
    }

    @DexIgnore
    @Override // com.fossil.ux3, com.fossil.ux3
    public ux3<V, K> inverse() {
        if (isEmpty()) {
            return ux3.of();
        }
        ux3<V, K> ux3 = this.j;
        if (ux3 != null) {
            return ux3;
        }
        b bVar = new b();
        this.j = bVar;
        return bVar;
    }
}
