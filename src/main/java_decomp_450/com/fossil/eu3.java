package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oa;
import com.fossil.v1;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eu3 implements v1 {
    @DexIgnore
    public /* final */ View.OnClickListener A; // = new a();
    @DexIgnore
    public NavigationMenuView a;
    @DexIgnore
    public LinearLayout b;
    @DexIgnore
    public v1.a c;
    @DexIgnore
    public p1 d;
    @DexIgnore
    public int e;
    @DexIgnore
    public c f;
    @DexIgnore
    public LayoutInflater g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList p;
    @DexIgnore
    public Drawable q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            boolean z = true;
            eu3.this.c(true);
            r1 itemData = ((NavigationMenuItemView) view).getItemData();
            eu3 eu3 = eu3.this;
            boolean a2 = eu3.d.a(itemData, eu3, 0);
            if (itemData == null || !itemData.isCheckable() || !a2) {
                z = false;
            } else {
                eu3.this.f.a(itemData);
            }
            eu3.this.c(false);
            if (z) {
                eu3.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends l {
        @DexIgnore
        public b(View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<l> {
        @DexIgnore
        public /* final */ ArrayList<e> a; // = new ArrayList<>();
        @DexIgnore
        public r1 b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c() {
            f();
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(l lVar, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) ((RecyclerView.ViewHolder) lVar).itemView;
                navigationMenuItemView.setIconTintList(eu3.this.p);
                eu3 eu3 = eu3.this;
                if (eu3.i) {
                    navigationMenuItemView.setTextAppearance(eu3.h);
                }
                ColorStateList colorStateList = eu3.this.j;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = eu3.this.q;
                da.a(navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                g gVar = (g) this.a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(gVar.b);
                navigationMenuItemView.setHorizontalPadding(eu3.this.r);
                navigationMenuItemView.setIconPadding(eu3.this.s);
                eu3 eu32 = eu3.this;
                if (eu32.u) {
                    navigationMenuItemView.setIconSize(eu32.t);
                }
                navigationMenuItemView.setMaxLines(eu3.this.w);
                navigationMenuItemView.a(gVar.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) ((RecyclerView.ViewHolder) lVar).itemView).setText(((g) this.a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                f fVar = (f) this.a.get(i);
                ((RecyclerView.ViewHolder) lVar).itemView.setPadding(0, fVar.b(), 0, fVar.a());
            }
        }

        @DexIgnore
        public Bundle c() {
            Bundle bundle = new Bundle();
            r1 r1Var = this.b;
            if (r1Var != null) {
                bundle.putInt("android:menu:checked", r1Var.getItemId());
            }
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                e eVar = this.a.get(i);
                if (eVar instanceof g) {
                    r1 a2 = ((g) eVar).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        gu3 gu3 = new gu3();
                        actionView.saveHierarchyState(gu3);
                        sparseArray.put(a2.getItemId(), gu3);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        @DexIgnore
        public r1 d() {
            return this.b;
        }

        @DexIgnore
        public int e() {
            int i = eu3.this.b.getChildCount() == 0 ? 0 : 1;
            for (int i2 = 0; i2 < eu3.this.f.getItemCount(); i2++) {
                if (eu3.this.f.getItemViewType(i2) == 0) {
                    i++;
                }
            }
            return i;
        }

        @DexIgnore
        public final void f() {
            if (!this.c) {
                this.c = true;
                this.a.clear();
                this.a.add(new d());
                int i = -1;
                int size = eu3.this.d.n().size();
                boolean z = false;
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    r1 r1Var = eu3.this.d.n().get(i3);
                    if (r1Var.isChecked()) {
                        a(r1Var);
                    }
                    if (r1Var.isCheckable()) {
                        r1Var.c(false);
                    }
                    if (r1Var.hasSubMenu()) {
                        SubMenu subMenu = r1Var.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                this.a.add(new f(eu3.this.y, 0));
                            }
                            this.a.add(new g(r1Var));
                            int size2 = this.a.size();
                            int size3 = subMenu.size();
                            boolean z2 = false;
                            for (int i4 = 0; i4 < size3; i4++) {
                                r1 r1Var2 = (r1) subMenu.getItem(i4);
                                if (r1Var2.isVisible()) {
                                    if (!z2 && r1Var2.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (r1Var2.isCheckable()) {
                                        r1Var2.c(false);
                                    }
                                    if (r1Var.isChecked()) {
                                        a(r1Var);
                                    }
                                    this.a.add(new g(r1Var2));
                                }
                            }
                            if (z2) {
                                a(size2, this.a.size());
                            }
                        }
                    } else {
                        int groupId = r1Var.getGroupId();
                        if (groupId != i) {
                            i2 = this.a.size();
                            z = r1Var.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                ArrayList<e> arrayList = this.a;
                                int i5 = eu3.this.y;
                                arrayList.add(new f(i5, i5));
                            }
                        } else if (!z && r1Var.getIcon() != null) {
                            a(i2, this.a.size());
                            z = true;
                        }
                        g gVar = new g(r1Var);
                        gVar.b = z;
                        this.a.add(gVar);
                        i = groupId;
                    }
                }
                this.c = false;
            }
        }

        @DexIgnore
        public void g() {
            f();
            notifyDataSetChanged();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            return this.a.size();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemViewType(int i) {
            e eVar = this.a.get(i);
            if (eVar instanceof f) {
                return 2;
            }
            if (eVar instanceof d) {
                return 3;
            }
            if (eVar instanceof g) {
                return ((g) eVar).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public l onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                eu3 eu3 = eu3.this;
                return new i(eu3.g, viewGroup, eu3.A);
            } else if (i == 1) {
                return new k(eu3.this.g, viewGroup);
            } else {
                if (i == 2) {
                    return new j(eu3.this.g, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new b(eu3.this.b);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void onViewRecycled(l lVar) {
            if (lVar instanceof i) {
                ((NavigationMenuItemView) ((RecyclerView.ViewHolder) lVar).itemView).d();
            }
        }

        @DexIgnore
        public final void a(int i, int i2) {
            while (i < i2) {
                ((g) this.a.get(i)).b = true;
                i++;
            }
        }

        @DexIgnore
        public void a(r1 r1Var) {
            if (this.b != r1Var && r1Var.isCheckable()) {
                r1 r1Var2 = this.b;
                if (r1Var2 != null) {
                    r1Var2.setChecked(false);
                }
                this.b = r1Var;
                r1Var.setChecked(true);
            }
        }

        @DexIgnore
        public void a(Bundle bundle) {
            r1 a2;
            View actionView;
            gu3 gu3;
            r1 a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    e eVar = this.a.get(i2);
                    if ((eVar instanceof g) && (a3 = ((g) eVar).a()) != null && a3.getItemId() == i) {
                        a(a3);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                f();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    e eVar2 = this.a.get(i3);
                    if (!(!(eVar2 instanceof g) || (a2 = ((g) eVar2).a()) == null || (actionView = a2.getActionView()) == null || (gu3 = (gu3) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(gu3);
                    }
                }
            }
        }

        @DexIgnore
        public void a(boolean z) {
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements e {
    }

    @DexIgnore
    public interface e {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements e {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements e {
        @DexIgnore
        public /* final */ r1 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public g(r1 r1Var) {
            this.a = r1Var;
        }

        @DexIgnore
        public r1 a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends bh {
        @DexIgnore
        public h(RecyclerView recyclerView) {
            super(recyclerView);
        }

        @DexIgnore
        @Override // com.fossil.bh, com.fossil.g9
        public void a(View view, oa oaVar) {
            super.a(view, oaVar);
            oaVar.a(oa.b.a(eu3.this.f.e(), 0, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends l {
        @DexIgnore
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(pr3.design_navigation_item, viewGroup, false));
            ((RecyclerView.ViewHolder) this).itemView.setOnClickListener(onClickListener);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j extends l {
        @DexIgnore
        public j(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(pr3.design_navigation_item_separator, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class k extends l {
        @DexIgnore
        public k(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(pr3.design_navigation_item_subheader, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class l extends RecyclerView.ViewHolder {
        @DexIgnore
        public l(View view) {
            super(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(a2 a2Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    public void b(int i2) {
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        Bundle bundle = new Bundle();
        if (this.a != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.a.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        c cVar = this.f;
        if (cVar != null) {
            bundle.putBundle("android:menu:adapter", cVar.c());
        }
        if (this.b != null) {
            SparseArray<? extends Parcelable> sparseArray2 = new SparseArray<>();
            this.b.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @DexIgnore
    public int d() {
        return this.b.getChildCount();
    }

    @DexIgnore
    public Drawable e() {
        return this.q;
    }

    @DexIgnore
    public int f() {
        return this.r;
    }

    @DexIgnore
    public void g(int i2) {
        this.h = i2;
        this.i = true;
        a(false);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public int getId() {
        return this.e;
    }

    @DexIgnore
    public int h() {
        return this.w;
    }

    @DexIgnore
    public ColorStateList i() {
        return this.j;
    }

    @DexIgnore
    public ColorStateList j() {
        return this.p;
    }

    @DexIgnore
    public final void k() {
        int i2 = (this.b.getChildCount() != 0 || !this.v) ? 0 : this.x;
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, i2, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Context context, p1 p1Var) {
        this.g = LayoutInflater.from(context);
        this.d = p1Var;
        this.y = context.getResources().getDimensionPixelOffset(lr3.design_navigation_separator_vertical_padding);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        this.j = colorStateList;
        a(false);
    }

    @DexIgnore
    public void d(int i2) {
        this.s = i2;
        a(false);
    }

    @DexIgnore
    public void e(int i2) {
        if (this.t != i2) {
            this.t = i2;
            this.u = true;
            a(false);
        }
    }

    @DexIgnore
    public void f(int i2) {
        this.w = i2;
        a(false);
    }

    @DexIgnore
    public void h(int i2) {
        this.z = i2;
        NavigationMenuView navigationMenuView = this.a;
        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(i2);
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        if (this.v != z2) {
            this.v = z2;
            k();
        }
    }

    @DexIgnore
    public int g() {
        return this.s;
    }

    @DexIgnore
    public w1 a(ViewGroup viewGroup) {
        if (this.a == null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) this.g.inflate(pr3.design_navigation_menu, viewGroup, false);
            this.a = navigationMenuView;
            navigationMenuView.setAccessibilityDelegateCompat(new h(this.a));
            if (this.f == null) {
                this.f = new c();
            }
            int i2 = this.z;
            if (i2 != -1) {
                this.a.setOverScrollMode(i2);
            }
            this.b = (LinearLayout) this.g.inflate(pr3.design_navigation_item_header, (ViewGroup) this.a, false);
            this.a.setAdapter(this.f);
        }
        return this.a;
    }

    @DexIgnore
    public void c(int i2) {
        this.r = i2;
        a(false);
    }

    @DexIgnore
    public void c(boolean z2) {
        c cVar = this.f;
        if (cVar != null) {
            cVar.a(z2);
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z2) {
        c cVar = this.f;
        if (cVar != null) {
            cVar.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z2) {
        v1.a aVar = this.c;
        if (aVar != null) {
            aVar.a(p1Var, z2);
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.a.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.f.a(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.b.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    @DexIgnore
    public void a(r1 r1Var) {
        this.f.a(r1Var);
    }

    @DexIgnore
    public r1 a() {
        return this.f.d();
    }

    @DexIgnore
    public View a(int i2) {
        View inflate = this.g.inflate(i2, (ViewGroup) this.b, false);
        a(inflate);
        return inflate;
    }

    @DexIgnore
    public void a(View view) {
        this.b.addView(view);
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.p = colorStateList;
        a(false);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.q = drawable;
        a(false);
    }

    @DexIgnore
    public void a(la laVar) {
        int g2 = laVar.g();
        if (this.x != g2) {
            this.x = g2;
            k();
        }
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, navigationMenuView.getPaddingTop(), 0, laVar.d());
        da.a(this.b, laVar);
    }
}
