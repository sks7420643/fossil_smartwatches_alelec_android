package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo5 extends vg {
    @DexIgnore
    public RecyclerView f;
    @DexIgnore
    public int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ vo5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(vo5 vo5) {
            this.a = vo5;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            ee7.b(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0) {
                try {
                    View c = this.a.c(recyclerView.getLayoutManager());
                    vo5 vo5 = this.a;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager == null) {
                        ee7.a();
                        throw null;
                    } else if (c != null) {
                        vo5.a(layoutManager.l(c));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("BlurLinearSnapHelper", "attachToRecyclerView - e=" + e);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            ee7.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            this.a.c(recyclerView.getLayoutManager());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final void a(int i) {
        this.g = i;
    }

    @DexIgnore
    public final void b(RecyclerView.m mVar, View view) {
        View view2;
        if (view != null && mVar != null) {
            view.setAlpha(1.0f);
            int i = -1;
            int i2 = 0;
            int e = mVar.e() - 1;
            if (e >= 0) {
                while (true) {
                    if (!ee7.a(mVar.d(i2), view)) {
                        if (i2 == e) {
                            break;
                        }
                        i2++;
                    } else {
                        i = i2;
                        break;
                    }
                }
            }
            if (i >= 0) {
                View view3 = null;
                if (i > 0 && i < mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = mVar.d(i + 1);
                } else if (i == 0) {
                    view2 = mVar.d(i + 1);
                } else if (i == mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = null;
                } else {
                    view2 = null;
                }
                if (view3 != null) {
                    view3.setAlpha(0.8f);
                }
                if (view2 != null) {
                    view2.setAlpha(0.8f);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.eh, com.fossil.vg
    public View c(RecyclerView.m mVar) {
        View c = super.c(mVar);
        b(mVar, c);
        return c;
    }

    @DexIgnore
    public final int d() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.eh
    public void a(RecyclerView recyclerView) {
        super.a(recyclerView);
        this.f = recyclerView;
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new b(this));
        }
    }
}
