package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad5 {
    @DexIgnore
    public /* final */ gs6 a;
    @DexIgnore
    public /* final */ is6 b;
    @DexIgnore
    public /* final */ ul5 c;
    @DexIgnore
    public /* final */ rl5 d;

    @DexIgnore
    public ad5(gs6 gs6, is6 is6, ul5 ul5, rl5 rl5) {
        ee7.b(gs6, "mGetDianaDeviceSettingUseCase");
        ee7.b(is6, "mGetHybridDeviceSettingUseCase");
        ee7.b(ul5, "mHybridSyncUseCase");
        ee7.b(rl5, "mDianaSyncUseCase");
        this.a = gs6;
        this.b = is6;
        this.c = ul5;
        this.d = rl5;
    }

    @DexIgnore
    public final fl4<es6, fs6, ds6> a(String str) {
        ee7.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = zc5.a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4 || i == 5) {
                return this.a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final fl4<km5, lm5, jm5> b(String str) {
        ee7.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = zc5.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4 || i == 5) {
                return this.d;
            }
        }
        return this.d;
    }
}
