package com.fossil;

import com.fossil.ot1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ut1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(xt1 xt1);

        @DexIgnore
        public abstract a a(Integer num);

        @DexIgnore
        public abstract ut1 a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract a c(long j);
    }

    @DexIgnore
    public static a a(String str) {
        ot1.b bVar = new ot1.b();
        bVar.a(str);
        return bVar;
    }

    @DexIgnore
    public abstract Integer a();

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract xt1 d();

    @DexIgnore
    public abstract byte[] e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public static a a(byte[] bArr) {
        ot1.b bVar = new ot1.b();
        bVar.a(bArr);
        return bVar;
    }
}
