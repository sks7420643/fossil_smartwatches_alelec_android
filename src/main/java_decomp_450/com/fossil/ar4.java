package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar4 implements Factory<zq4> {
    @DexIgnore
    public /* final */ Provider<bp4> a;

    @DexIgnore
    public ar4(Provider<bp4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ar4 a(Provider<bp4> provider) {
        return new ar4(provider);
    }

    @DexIgnore
    public static zq4 a(bp4 bp4) {
        return new zq4(bp4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public zq4 get() {
        return a(this.a.get());
    }
}
