package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz0 extends k60 {
    @DexIgnore
    public static /* final */ us0 f; // = new us0(null);
    @DexIgnore
    public /* final */ qa1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ay0 c;
    @DexIgnore
    public /* final */ lk0 d;
    @DexIgnore
    public /* final */ mw0 e;

    @DexIgnore
    public /* synthetic */ sz0(qa1 qa1, String str, ay0 ay0, lk0 lk0, mw0 mw0, int i) {
        qa1 = (i & 1) != 0 ? qa1.a : qa1;
        str = (i & 2) != 0 ? "" : str;
        lk0 = (i & 8) != 0 ? new lk0(null, oi0.a, null, 5) : lk0;
        mw0 = (i & 16) != 0 ? null : mw0;
        this.a = qa1;
        this.b = str;
        this.c = ay0;
        this.d = lk0;
        this.e = mw0;
    }

    @DexIgnore
    public static /* synthetic */ sz0 a(sz0 sz0, qa1 qa1, String str, ay0 ay0, lk0 lk0, mw0 mw0, int i) {
        if ((i & 1) != 0) {
            qa1 = sz0.a;
        }
        if ((i & 2) != 0) {
            str = sz0.b;
        }
        if ((i & 4) != 0) {
            ay0 = sz0.c;
        }
        if ((i & 8) != 0) {
            lk0 = sz0.d;
        }
        if ((i & 16) != 0) {
            mw0 = sz0.e;
        }
        return sz0.a(qa1, str, ay0, lk0, mw0);
    }

    @DexIgnore
    public final sz0 a(qa1 qa1, String str, ay0 ay0, lk0 lk0, mw0 mw0) {
        return new sz0(qa1, str, ay0, lk0, mw0);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(yz0.a(jSONObject, r51.p, yz0.a(this.a)), r51.M0, yz0.a(this.c));
            mw0 mw0 = this.e;
            if (mw0 != null && !mw0.a()) {
                yz0.a(jSONObject, r51.j3, this.e.getLogName());
            }
            if (this.d.b != oi0.a) {
                yz0.a(jSONObject, r51.j4, this.d.a());
            }
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof sz0)) {
            return false;
        }
        sz0 sz0 = (sz0) obj;
        return ee7.a(this.a, sz0.a) && ee7.a(this.b, sz0.b) && ee7.a(this.c, sz0.c) && ee7.a(this.d, sz0.d) && ee7.a(this.e, sz0.e);
    }

    @DexIgnore
    public int hashCode() {
        qa1 qa1 = this.a;
        int i = 0;
        int hashCode = (qa1 != null ? qa1.hashCode() : 0) * 31;
        String str = this.b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        ay0 ay0 = this.c;
        int hashCode3 = (hashCode2 + (ay0 != null ? ay0.hashCode() : 0)) * 31;
        lk0 lk0 = this.d;
        int hashCode4 = (hashCode3 + (lk0 != null ? lk0.hashCode() : 0)) * 31;
        mw0 mw0 = this.e;
        if (mw0 != null) {
            i = mw0.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("Result(requestId=");
        b2.append(this.a);
        b2.append(", requestUuid=");
        b2.append(this.b);
        b2.append(", resultCode=");
        b2.append(this.c);
        b2.append(", commandResult=");
        b2.append(this.d);
        b2.append(", responseStatus=");
        b2.append(this.e);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public sz0(qa1 qa1, String str, ay0 ay0, lk0 lk0, mw0 mw0) {
        this.a = qa1;
        this.b = str;
        this.c = ay0;
        this.d = lk0;
        this.e = mw0;
    }
}
