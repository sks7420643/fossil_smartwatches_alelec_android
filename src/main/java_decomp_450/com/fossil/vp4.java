package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp4 extends he {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, Boolean>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<r87<List<mo4>, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<dn4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Date>> f; // = new MutableLiveData<>();
    @DexIgnore
    public qn4 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public List<String> i; // = new ArrayList();
    @DexIgnore
    public /* final */ MutableLiveData<r87<List<mo4>, String>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<mn4, Long, String>> k; // = new MutableLiveData<>();
    @DexIgnore
    public List<mo4> l;
    @DexIgnore
    public String m; // = "now";
    @DexIgnore
    public /* final */ xo4 n;
    @DexIgnore
    public /* final */ ro4 o;
    @DexIgnore
    public /* final */ ch5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1", f = "BCInviteFriendViewModel.kt", l = {287, 296}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$friendsWrapper$1", f = "BCInviteFriendViewModel.kt", l = {287}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends un4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends un4>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    xo4 d = this.this$0.this$0.n;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = d.c(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vp4$b$b")
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$localFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.vp4$b$b  reason: collision with other inner class name */
        public static final class C0219b extends zb7 implements kd7<yi7, fb7<? super List<un4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0219b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0219b bVar = new C0219b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<un4>> fb7) {
                return ((C0219b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vp4 vp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00c8  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00da  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 2
                r3 = 1
                r4 = 0
                if (r1 == 0) goto L_0x0030
                if (r1 == r3) goto L_0x0028
                if (r1 != r2) goto L_0x0020
                java.lang.Object r0 = r8.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r8.L$1
                com.fossil.ko4 r0 = (com.fossil.ko4) r0
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x00c0
            L_0x0020:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x0028:
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x005a
            L_0x0030:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r1 = r8.p$
                com.fossil.vp4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.a
                java.lang.Boolean r5 = com.fossil.pb7.a(r3)
                com.fossil.r87 r5 = com.fossil.w87.a(r5, r4)
                r9.a(r5)
                com.fossil.ti7 r9 = com.fossil.qj7.b()
                com.fossil.vp4$b$a r5 = new com.fossil.vp4$b$a
                r5.<init>(r8, r4)
                r8.L$0 = r1
                r8.label = r3
                java.lang.Object r9 = com.fossil.vh7.a(r9, r5, r8)
                if (r9 != r0) goto L_0x005a
                return r0
            L_0x005a:
                com.fossil.ko4 r9 = (com.fossil.ko4) r9
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                java.lang.String r5 = com.fossil.vp4.q
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "fetchFriends - friendsWrapper: "
                r6.append(r7)
                r6.append(r9)
                java.lang.String r6 = r6.toString()
                r3.d(r5, r6)
                java.lang.Object r3 = r9.c()
                java.util.List r3 = (java.util.List) r3
                com.fossil.vp4 r5 = r8.this$0
                androidx.lifecycle.MutableLiveData r5 = r5.a
                r6 = 0
                java.lang.Boolean r6 = com.fossil.pb7.a(r6)
                com.fossil.r87 r6 = com.fossil.w87.a(r6, r4)
                r5.a(r6)
                if (r3 == 0) goto L_0x00a6
                com.fossil.vp4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.d
                java.util.List r0 = com.fossil.uc5.a(r3)
                com.fossil.r87 r0 = com.fossil.w87.a(r0, r4)
                r9.a(r0)
                goto L_0x00eb
            L_0x00a6:
                com.fossil.ti7 r5 = com.fossil.qj7.b()
                com.fossil.vp4$b$b r6 = new com.fossil.vp4$b$b
                r6.<init>(r8, r4)
                r8.L$0 = r1
                r8.L$1 = r9
                r8.L$2 = r3
                r8.label = r2
                java.lang.Object r1 = com.fossil.vh7.a(r5, r6, r8)
                if (r1 != r0) goto L_0x00be
                return r0
            L_0x00be:
                r0 = r9
                r9 = r1
            L_0x00c0:
                java.util.List r9 = (java.util.List) r9
                boolean r1 = r9.isEmpty()
                if (r1 == 0) goto L_0x00da
                com.fossil.vp4 r9 = r8.this$0
                androidx.lifecycle.MutableLiveData r9 = r9.d
                com.portfolio.platform.data.model.ServerError r0 = r0.a()
                com.fossil.r87 r0 = com.fossil.w87.a(r4, r0)
                r9.a(r0)
                goto L_0x00eb
            L_0x00da:
                com.fossil.vp4 r0 = r8.this$0
                androidx.lifecycle.MutableLiveData r0 = r0.d
                java.util.List r9 = com.fossil.uc5.a(r9)
                com.fossil.r87 r9 = com.fossil.w87.a(r9, r4)
                r0.a(r9)
            L_0x00eb:
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vp4.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1", f = "BCInviteFriendViewModel.kt", l = {310, 311, 312, 313, 319, 325, 328}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$friends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<un4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<un4>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$suggestedFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<? extends mo4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $combinationFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(List list, List list2, fb7 fb7) {
                super(2, fb7);
                this.$friends = list;
                this.$combinationFriends = list2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.$friends, this.$combinationFriends, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends mo4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return nt4.d(this.$friends, this.$combinationFriends);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vp4 vp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0109  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0133 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0153 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0154  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0169  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x016d  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01b5  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x01b9  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x021d A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0242  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 0
                switch(r1) {
                    case 0: goto L_0x00c1;
                    case 1: goto L_0x00b9;
                    case 2: goto L_0x00ab;
                    case 3: goto L_0x009a;
                    case 4: goto L_0x0081;
                    case 5: goto L_0x0058;
                    case 6: goto L_0x002f;
                    case 7: goto L_0x0012;
                    default: goto L_0x000a;
                }
            L_0x000a:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x0012:
                java.lang.Object r0 = r13.L$5
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r13.L$4
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r13.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r13.L$2
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r0 = r13.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r13.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r14)
                goto L_0x021e
            L_0x002f:
                java.lang.Object r1 = r13.L$8
                java.lang.StringBuilder r1 = (java.lang.StringBuilder) r1
                java.lang.Object r3 = r13.L$7
                java.lang.String r3 = (java.lang.String) r3
                java.lang.Object r4 = r13.L$6
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = (com.misfit.frameworks.buttonservice.log.ILocalFLogger) r4
                java.lang.Object r5 = r13.L$5
                java.util.List r5 = (java.util.List) r5
                java.lang.Object r6 = r13.L$4
                java.util.List r6 = (java.util.List) r6
                java.lang.Object r7 = r13.L$3
                java.util.List r7 = (java.util.List) r7
                java.lang.Object r8 = r13.L$2
                com.fossil.r87 r8 = (com.fossil.r87) r8
                java.lang.Object r9 = r13.L$1
                java.util.List r9 = (java.util.List) r9
                java.lang.Object r10 = r13.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r14)
                goto L_0x01ef
            L_0x0058:
                java.lang.Object r1 = r13.L$8
                java.lang.StringBuilder r1 = (java.lang.StringBuilder) r1
                java.lang.Object r3 = r13.L$7
                java.lang.String r3 = (java.lang.String) r3
                java.lang.Object r4 = r13.L$6
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = (com.misfit.frameworks.buttonservice.log.ILocalFLogger) r4
                java.lang.Object r5 = r13.L$5
                java.util.List r5 = (java.util.List) r5
                java.lang.Object r6 = r13.L$4
                java.util.List r6 = (java.util.List) r6
                java.lang.Object r7 = r13.L$3
                java.util.List r7 = (java.util.List) r7
                java.lang.Object r8 = r13.L$2
                com.fossil.r87 r8 = (com.fossil.r87) r8
                java.lang.Object r9 = r13.L$1
                java.util.List r9 = (java.util.List) r9
                java.lang.Object r10 = r13.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r14)
                goto L_0x01a3
            L_0x0081:
                java.lang.Object r1 = r13.L$3
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r3 = r13.L$2
                com.fossil.r87 r3 = (com.fossil.r87) r3
                java.lang.Object r4 = r13.L$1
                java.util.List r4 = (java.util.List) r4
                java.lang.Object r5 = r13.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r14)
                r7 = r1
                r8 = r3
                r9 = r4
                r10 = r5
                goto L_0x0159
            L_0x009a:
                java.lang.Object r1 = r13.L$2
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r3 = r13.L$1
                java.util.List r3 = (java.util.List) r3
                java.lang.Object r4 = r13.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r14)
                goto L_0x0134
            L_0x00ab:
                java.lang.Object r1 = r13.L$1
                java.util.List r1 = (java.util.List) r1
                java.lang.Object r3 = r13.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r14)
                r4 = r3
                r3 = r1
                goto L_0x011b
            L_0x00b9:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00fb
            L_0x00c1:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r1 = r13.p$
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
                java.lang.String r3 = com.fossil.vp4.q
                java.lang.String r4 = "fetchUnInvitedFilterFriends"
                r14.d(r3, r4)
                com.fossil.vp4 r14 = r13.this$0
                androidx.lifecycle.MutableLiveData r14 = r14.a
                r3 = 1
                java.lang.Boolean r4 = com.fossil.pb7.a(r3)
                com.fossil.r87 r4 = com.fossil.w87.a(r4, r2)
                r14.a(r4)
                com.fossil.ti7 r14 = com.fossil.qj7.b()
                com.fossil.vp4$c$a r4 = new com.fossil.vp4$c$a
                r4.<init>(r13, r2)
                r13.L$0 = r1
                r13.label = r3
                java.lang.Object r14 = com.fossil.vh7.a(r14, r4, r13)
                if (r14 != r0) goto L_0x00fb
                return r0
            L_0x00fb:
                java.util.List r14 = (java.util.List) r14
                com.fossil.vp4 r3 = r13.this$0
                com.fossil.qn4 r4 = com.fossil.vp4.a(r3)
                java.lang.String r4 = r4.c()
                if (r4 == 0) goto L_0x0242
                r13.L$0 = r1
                r13.L$1 = r14
                r5 = 2
                r13.label = r5
                java.lang.Object r3 = r3.a(r4, r13)
                if (r3 != r0) goto L_0x0117
                return r0
            L_0x0117:
                r4 = r1
                r12 = r3
                r3 = r14
                r14 = r12
            L_0x011b:
                r1 = r14
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r14 = r1.getFirst()
                com.fossil.hj7 r14 = (com.fossil.hj7) r14
                r13.L$0 = r4
                r13.L$1 = r3
                r13.L$2 = r1
                r5 = 3
                r13.label = r5
                java.lang.Object r14 = r14.c(r13)
                if (r14 != r0) goto L_0x0134
                return r0
            L_0x0134:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                java.lang.Object r14 = r14.c()
                java.util.List r14 = (java.util.List) r14
                java.lang.Object r5 = r1.getSecond()
                com.fossil.hj7 r5 = (com.fossil.hj7) r5
                r13.L$0 = r4
                r13.L$1 = r3
                r13.L$2 = r1
                r13.L$3 = r14
                r6 = 4
                r13.label = r6
                java.lang.Object r5 = r5.c(r13)
                if (r5 != r0) goto L_0x0154
                return r0
            L_0x0154:
                r7 = r14
                r8 = r1
                r9 = r3
                r10 = r4
                r14 = r5
            L_0x0159:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                java.lang.Object r14 = r14.c()
                r6 = r14
                java.util.List r6 = (java.util.List) r6
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                if (r7 == 0) goto L_0x016d
                r5.addAll(r7)
                goto L_0x01b3
            L_0x016d:
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r14.getLocal()
                java.lang.String r3 = com.fossil.vp4.q
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r14 = "fetchUnInvitedFilterFriends - pendingError: "
                r1.append(r14)
                java.lang.Object r14 = r8.getFirst()
                com.fossil.hj7 r14 = (com.fossil.hj7) r14
                r13.L$0 = r10
                r13.L$1 = r9
                r13.L$2 = r8
                r13.L$3 = r7
                r13.L$4 = r6
                r13.L$5 = r5
                r13.L$6 = r4
                r13.L$7 = r3
                r13.L$8 = r1
                r11 = 5
                r13.label = r11
                java.lang.Object r14 = r14.c(r13)
                if (r14 != r0) goto L_0x01a3
                return r0
            L_0x01a3:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                com.portfolio.platform.data.model.ServerError r14 = r14.a()
                r1.append(r14)
                java.lang.String r14 = r1.toString()
                r4.e(r3, r14)
            L_0x01b3:
                if (r6 == 0) goto L_0x01b9
                r5.addAll(r6)
                goto L_0x01ff
            L_0x01b9:
                com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r14.getLocal()
                java.lang.String r3 = com.fossil.vp4.q
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r14 = "fetchUnInvitedFilterFriends - joinedError: "
                r1.append(r14)
                java.lang.Object r14 = r8.getSecond()
                com.fossil.hj7 r14 = (com.fossil.hj7) r14
                r13.L$0 = r10
                r13.L$1 = r9
                r13.L$2 = r8
                r13.L$3 = r7
                r13.L$4 = r6
                r13.L$5 = r5
                r13.L$6 = r4
                r13.L$7 = r3
                r13.L$8 = r1
                r11 = 6
                r13.label = r11
                java.lang.Object r14 = r14.c(r13)
                if (r14 != r0) goto L_0x01ef
                return r0
            L_0x01ef:
                com.fossil.ko4 r14 = (com.fossil.ko4) r14
                com.portfolio.platform.data.model.ServerError r14 = r14.a()
                r1.append(r14)
                java.lang.String r14 = r1.toString()
                r4.e(r3, r14)
            L_0x01ff:
                com.fossil.ti7 r14 = com.fossil.qj7.a()
                com.fossil.vp4$c$b r1 = new com.fossil.vp4$c$b
                r1.<init>(r9, r5, r2)
                r13.L$0 = r10
                r13.L$1 = r9
                r13.L$2 = r8
                r13.L$3 = r7
                r13.L$4 = r6
                r13.L$5 = r5
                r3 = 7
                r13.label = r3
                java.lang.Object r14 = com.fossil.vh7.a(r14, r1, r13)
                if (r14 != r0) goto L_0x021e
                return r0
            L_0x021e:
                java.util.List r14 = (java.util.List) r14
                com.fossil.vp4 r0 = r13.this$0
                androidx.lifecycle.MutableLiveData r0 = r0.d
                com.fossil.r87 r14 = com.fossil.w87.a(r14, r2)
                r0.a(r14)
                com.fossil.vp4 r14 = r13.this$0
                androidx.lifecycle.MutableLiveData r14 = r14.a
                r0 = 0
                java.lang.Boolean r0 = com.fossil.pb7.a(r0)
                com.fossil.r87 r0 = com.fossil.w87.a(r0, r2)
                r14.a(r0)
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            L_0x0242:
                com.fossil.ee7.a()
                throw r2
                switch-data {0->0x00c1, 1->0x00b9, 2->0x00ab, 3->0x009a, 4->0x0081, 5->0x0058, 6->0x002f, 7->0x0012, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vp4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel", f = "BCInviteFriendViewModel.kt", l = {335, 335}, m = "loadInvitedPlayers")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vp4 vp4, fb7 fb7) {
            super(fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super hj7<? extends r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vp4$e$a$a")
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCInviteFriendViewModel.kt", l = {338}, m = "invokeSuspend")
            /* renamed from: com.fossil.vp4$e$a$a  reason: collision with other inner class name */
            public static final class C0220a extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0220a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0220a aVar = new C0220a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
                    return ((C0220a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ro4 c = this.this$0.this$0.this$0.c();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = ro4.a(c, str, new String[]{"waiting"}, 0, this, 4, null);
                            if (obj == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCInviteFriendViewModel.kt", l = {337}, m = "invokeSuspend")
            public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<List<? extends jn4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(this.this$0, fb7);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super ko4<List<? extends jn4>>> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ro4 c = this.this$0.this$0.this$0.c();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = ro4.a(c, str, new String[]{"invited"}, 0, this, 4, null);
                            if (obj == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    return w87.a(xh7.a(yi7, null, null, new b(this, null), 3, null), xh7.a(yi7, null, null, new C0220a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(vp4 vp4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$challengeId, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super hj7<? extends r87<? extends hj7<? extends ko4<List<? extends jn4>>>, ? extends hj7<? extends ko4<List<? extends jn4>>>>>> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                return xh7.a(this.p$, qj7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    if (this.this$0.this$0.h) {
                        r87 r87 = (r87) this.this$0.this$0.d.a();
                        List<mo4> list = r87 != null ? (List) r87.getFirst() : null;
                        if (!(list == null || list.isEmpty())) {
                            qn4 a2 = vp4.a(this.this$0.this$0);
                            ArrayList arrayList = new ArrayList(x97.a(list, 10));
                            for (mo4 mo4 : list) {
                                arrayList.add(mo4.b());
                            }
                            Object[] array = arrayList.toArray(new String[0]);
                            if (array != null) {
                                a2.a((String[]) array);
                            } else {
                                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String r = vp4.q;
                    local.e(r, "onInviteMoreFriends - launch - ids: " + vp4.a(this.this$0.this$0).d());
                    ro4 c = this.this$0.this$0.c();
                    f fVar = this.this$0;
                    String str = fVar.$it;
                    String[] d = vp4.a(fVar.this$0).d();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.a(str, d, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, fb7 fb7, vp4 vp4) {
            super(2, fb7);
            this.$it = str;
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.$it, fb7, this.this$0);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.a.a(w87.a(null, pb7.a(true)));
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            this.this$0.a.a(w87.a(null, pb7.a(false)));
            if (ko4.c() != null) {
                this.this$0.c.a(w87.a(pb7.a(true), null));
            } else {
                this.this$0.c.a(w87.a(pb7.a(false), ko4.a()));
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1", f = "BCInviteFriendViewModel.kt", l = {232}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $searchingFriends;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$searchingFriends = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$searchingFriends, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.j.a(w87.a(this.$searchingFriends, this.this$0.$key));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(vp4 vp4, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$key, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x004c A[SYNTHETIC] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r11.label
                r2 = 1
                if (r1 == 0) goto L_0x0020
                if (r1 != r2) goto L_0x0018
                java.lang.Object r0 = r11.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r11.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r12)
                goto L_0x00b2
            L_0x0018:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L_0x0020:
                com.fossil.t87.a(r12)
                com.fossil.yi7 r12 = r11.p$
                com.fossil.vp4 r1 = r11.this$0
                java.util.List r1 = r1.l
                r3 = 0
                if (r1 == 0) goto L_0x0037
                boolean r1 = r1.isEmpty()
                if (r1 == 0) goto L_0x0035
                goto L_0x0037
            L_0x0035:
                r1 = 0
                goto L_0x0038
            L_0x0037:
                r1 = 1
            L_0x0038:
                if (r1 != 0) goto L_0x00b2
                com.fossil.vp4 r1 = r11.this$0
                java.util.List r1 = r1.l
                r4 = 0
                if (r1 == 0) goto L_0x00ae
                java.util.ArrayList r5 = new java.util.ArrayList
                r5.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x004c:
                boolean r6 = r1.hasNext()
                if (r6 == 0) goto L_0x0098
                java.lang.Object r6 = r1.next()
                r7 = r6
                com.fossil.mo4 r7 = (com.fossil.mo4) r7
                java.lang.String r8 = r7.a()
                java.lang.String r9 = ""
                if (r8 == 0) goto L_0x0062
                goto L_0x0063
            L_0x0062:
                r8 = r9
            L_0x0063:
                java.lang.String r10 = r11.$key
                boolean r8 = com.fossil.nh7.a(r8, r10, r2)
                if (r8 != 0) goto L_0x0089
                java.lang.String r8 = r7.c()
                if (r8 == 0) goto L_0x0072
                r9 = r8
            L_0x0072:
                java.lang.String r8 = r11.$key
                boolean r8 = com.fossil.nh7.a(r9, r8, r2)
                if (r8 != 0) goto L_0x0089
                java.lang.String r7 = r7.e()
                java.lang.String r8 = r11.$key
                boolean r7 = com.fossil.nh7.a(r7, r8, r2)
                if (r7 == 0) goto L_0x0087
                goto L_0x0089
            L_0x0087:
                r7 = 0
                goto L_0x008a
            L_0x0089:
                r7 = 1
            L_0x008a:
                java.lang.Boolean r7 = com.fossil.pb7.a(r7)
                boolean r7 = r7.booleanValue()
                if (r7 == 0) goto L_0x004c
                r5.add(r6)
                goto L_0x004c
            L_0x0098:
                com.fossil.tk7 r1 = com.fossil.qj7.c()
                com.fossil.vp4$g$a r3 = new com.fossil.vp4$g$a
                r3.<init>(r11, r5, r4)
                r11.L$0 = r12
                r11.L$1 = r5
                r11.label = r2
                java.lang.Object r12 = com.fossil.vh7.a(r1, r3, r11)
                if (r12 != r0) goto L_0x00b2
                return r0
            L_0x00ae:
                com.fossil.ee7.a()
                throw r4
            L_0x00b2:
                com.fossil.i97 r12 = com.fossil.i97.a
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vp4.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartLater$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(vp4 vp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.f.a(ot4.a.b());
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartNow$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(vp4 vp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.e.a(ot4.a.h());
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1", f = "BCInviteFriendViewModel.kt", l = {261, 272}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ mn4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, mn4 mn4, fb7 fb7) {
                super(2, fb7);
                this.this$0 = jVar;
                this.$challenge = mn4;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$challenge, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Object> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Long a;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.c().b();
                    String s = this.$challenge.s();
                    if (s != null && (a = pb7.a(PortfolioApp.g0.c().n(s))) != null) {
                        return a;
                    }
                    this.this$0.this$0.i().a(pb7.a(true));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$result$1", f = "BCInviteFriendViewModel.kt", l = {261}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(j jVar, int i, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = jVar;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ro4 c = this.this$0.this$0.c();
                    qn4 a2 = vp4.a(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.a(a2, i2, str, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(vp4 vp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            String str;
            int i;
            Long l;
            Object a2 = nb7.a();
            int i2 = this.label;
            if (i2 == 0) {
                t87.a(obj);
                yi7 = this.p$;
                str = PortfolioApp.g0.c().c();
                i = be5.o.f(str) ? 2 : 3;
                ti7 b2 = qj7.b();
                b bVar = new b(this, i, str, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.I$0 = i;
                this.label = 1;
                obj = vh7.a(b2, bVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i2 == 1) {
                i = this.I$0;
                str = (String) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i2 == 2) {
                mn4 mn4 = (mn4) this.L$3;
                ko4 ko4 = (ko4) this.L$2;
                String str2 = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.c.a(w87.a(pb7.a(true), null));
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko42 = (ko4) obj;
            mn4 mn42 = (mn4) ko42.c();
            this.this$0.a.a(w87.a(null, pb7.a(false)));
            if (mn42 != null) {
                this.this$0.k.a(new v87(mn42, pb7.a(vp4.a(this.this$0).h().getTime()), this.this$0.m));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = vp4.q;
                local.e(r, "startChallenge - createdChallenge: " + mn42);
                this.this$0.i().b(0);
                ch5 i3 = this.this$0.i();
                Date e = mn42.e();
                if (e == null || (l = pb7.a(e.getTime())) == null) {
                    l = pb7.a(0L);
                }
                i3.a(l);
                ti7 a3 = qj7.a();
                a aVar = new a(this, mn42, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.I$0 = i;
                this.L$2 = ko42;
                this.L$3 = mn42;
                this.label = 2;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
                this.this$0.c.a(w87.a(pb7.a(true), null));
                return i97.a;
            }
            this.this$0.c.a(w87.a(pb7.a(false), ko42.a()));
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = vp4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCInviteFriendViewModel::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public vp4(xo4 xo4, ro4 ro4, ch5 ch5) {
        ee7.b(xo4, "friendRepository");
        ee7.b(ro4, "challengeRepository");
        ee7.b(ch5, "shared");
        this.n = xo4;
        this.o = ro4;
        this.p = ch5;
    }

    @DexIgnore
    public static final /* synthetic */ qn4 a(vp4 vp4) {
        qn4 qn4 = vp4.g;
        if (qn4 != null) {
            return qn4;
        }
        ee7.d("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void m() {
        List<mo4> list = this.l;
        if (list != null) {
            this.d.a(w87.a(list, null));
        }
        this.l = null;
    }

    @DexIgnore
    public final void n() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        StringBuilder sb = new StringBuilder();
        sb.append("onInviteMoreFriends - ids: ");
        qn4 qn4 = this.g;
        if (qn4 != null) {
            sb.append(qn4.d());
            sb.append(" - cachedInvitedAll: ");
            sb.append(this.h);
            local.e(str, sb.toString());
            qn4 qn42 = this.g;
            if (qn42 != null) {
                if (!(qn42.d().length == 0) || this.h) {
                    qn4 qn43 = this.g;
                    if (qn43 != null) {
                        String c2 = qn43.c();
                        if (c2 != null) {
                            ik7 unused = xh7.b(ie.a(this), null, null, new f(c2, null, this), 3, null);
                            return;
                        }
                        return;
                    }
                    ee7.d("cachedDraft");
                    throw null;
                }
                this.c.a(w87.a(true, null));
                return;
            }
            ee7.d("cachedDraft");
            throw null;
        }
        ee7.d("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void o() {
        qn4 qn4 = this.g;
        if (qn4 != null) {
            if (qn4.c() != null) {
                b();
            } else {
                a();
            }
        } else {
            ee7.d("cachedDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void p() {
        this.m = "later";
        ik7 unused = xh7.b(ie.a(this), qj7.a(), null, new h(this, null), 2, null);
    }

    @DexIgnore
    public final void q() {
        this.m = "now";
        ik7 unused = xh7.b(ie.a(this), qj7.a(), null, new i(this, null), 2, null);
    }

    @DexIgnore
    public final void a(qn4 qn4) {
        qn4 qn42;
        vp4 vp4;
        if (qn4 != null) {
            vp4 = this;
            qn42 = qn4;
        } else {
            qn42 = new qn4(null, null, null, null, null, 0, 0, null, null, false, null, 2047, null);
            vp4 = this;
        }
        vp4.g = qn42;
        if ((qn4 != null ? qn4.c() : null) != null) {
            b();
        } else {
            a();
        }
    }

    @DexIgnore
    public final void b(String str) {
        qn4 qn4 = this.g;
        if (qn4 != null) {
            qn4.d(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = q;
            StringBuilder sb = new StringBuilder();
            sb.append("startChallenge - draft: ");
            qn4 qn42 = this.g;
            if (qn42 != null) {
                sb.append(qn42);
                local.e(str2, sb.toString());
                this.a.a(w87.a(null, true));
                ik7 unused = xh7.b(ie.a(this), null, null, new j(this, null), 3, null);
                return;
            }
            ee7.d("cachedDraft");
            throw null;
        }
        ee7.d("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final ro4 c() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<v87<mn4, Long, String>> d() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Date>> f() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, Boolean>> g() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<r87<List<mo4>, String>> h() {
        return this.j;
    }

    @DexIgnore
    public final ch5 i() {
        return this.p;
    }

    @DexIgnore
    public final LiveData<List<dn4>> j() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<r87<Boolean, ServerError>> k() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<r87<List<mo4>, ServerError>> l() {
        return this.d;
    }

    @DexIgnore
    public final void a(mo4 mo4) {
        ee7.b(mo4, "friend");
        int i2 = 0;
        if (!mo4.f() && this.h) {
            this.h = false;
            qn4 qn4 = this.g;
            if (qn4 != null) {
                qn4.a(false);
                this.b.a((Boolean) false);
                r87<List<mo4>, ServerError> a2 = this.d.a();
                List<mo4> first = a2 != null ? a2.getFirst() : null;
                if (first != null) {
                    this.i.clear();
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        this.i.add(it.next().b());
                    }
                }
            } else {
                ee7.d("cachedDraft");
                throw null;
            }
        }
        if (mo4.f()) {
            if (!this.i.contains(mo4.b())) {
                this.i.add(mo4.b());
            }
        } else {
            this.i.remove(mo4.b());
        }
        qn4 qn42 = this.g;
        if (qn42 != null) {
            Object[] array = this.i.toArray(new String[0]);
            if (array != null) {
                qn42.a((String[]) array);
                List<mo4> list = this.l;
                if (list != null) {
                    Iterator<mo4> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            i2 = -1;
                            break;
                        } else if (ee7.a((Object) it2.next().b(), (Object) mo4.b())) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        list.get(i2).a(mo4.f());
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = q;
                StringBuilder sb = new StringBuilder();
                sb.append("cachedDraft: ");
                qn4 qn43 = this.g;
                if (qn43 != null) {
                    sb.append(qn43);
                    local.e(str, sb.toString());
                    return;
                }
                ee7.d("cachedDraft");
                throw null;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        ee7.d("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void b() {
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void a(boolean z) {
        if (this.h != z) {
            r87<List<mo4>, ServerError> a2 = this.d.a();
            List<mo4> first = a2 != null ? a2.getFirst() : null;
            this.h = z;
            if (z) {
                if (first != null) {
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        it.next().a(true);
                    }
                }
                qn4 qn4 = this.g;
                if (qn4 != null) {
                    qn4.a(true);
                } else {
                    ee7.d("cachedDraft");
                    throw null;
                }
            } else {
                if (this.i.isEmpty()) {
                    if (first != null) {
                        Iterator<T> it2 = first.iterator();
                        while (it2.hasNext()) {
                            it2.next().a(false);
                        }
                    }
                } else if (first != null) {
                    for (T t : first) {
                        t.a(this.i.contains(t.b()));
                    }
                }
                qn4 qn42 = this.g;
                if (qn42 != null) {
                    qn42.a(false);
                } else {
                    ee7.d("cachedDraft");
                    throw null;
                }
            }
            if (first != null) {
                this.d.a(w87.a(first, null));
            }
        }
    }

    @DexIgnore
    public final void a(Date date) {
        ee7.b(date, "date");
        SimpleDateFormat simpleDateFormat = zd5.o.get();
        String format = simpleDateFormat != null ? simpleDateFormat.format(date) : null;
        if (format != null) {
            b(format);
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "key");
        r87<List<mo4>, ServerError> a2 = this.d.a();
        this.l = a2 != null ? a2.getFirst() : null;
        ik7 unused = xh7.b(ie.a(this), qj7.a(), null, new g(this, str, null), 2, null);
    }

    @DexIgnore
    public final void a() {
        ik7 unused = xh7.b(ie.a(this), null, null, new b(this, null), 3, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e A[PHI: r7 
      PHI: (r7v2 java.lang.Object) = (r7v5 java.lang.Object), (r7v1 java.lang.Object) binds: [B:19:0x006b, B:10:0x0028] A[DONT_GENERATE, DONT_INLINE], RETURN] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r6, com.fossil.fb7<? super com.fossil.r87<? extends com.fossil.hj7<com.fossil.ko4<java.util.List<com.fossil.jn4>>>, ? extends com.fossil.hj7<com.fossil.ko4<java.util.List<com.fossil.jn4>>>>> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.vp4.d
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.vp4$d r0 = (com.fossil.vp4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.vp4$d r0 = new com.fossil.vp4$d
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0048
            if (r2 == r4) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.vp4 r6 = (com.fossil.vp4) r6
            com.fossil.t87.a(r7)
            goto L_0x006e
        L_0x0034:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003c:
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r2 = r0.L$0
            com.fossil.vp4 r2 = (com.fossil.vp4) r2
            com.fossil.t87.a(r7)
            goto L_0x005f
        L_0x0048:
            com.fossil.t87.a(r7)
            com.fossil.vp4$e r7 = new com.fossil.vp4$e
            r2 = 0
            r7.<init>(r5, r6, r2)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r4
            java.lang.Object r7 = com.fossil.dl7.a(r7, r0)
            if (r7 != r1) goto L_0x005e
            return r1
        L_0x005e:
            r2 = r5
        L_0x005f:
            com.fossil.hj7 r7 = (com.fossil.hj7) r7
            r0.L$0 = r2
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.c(r0)
            if (r7 != r1) goto L_0x006e
            return r1
        L_0x006e:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vp4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
