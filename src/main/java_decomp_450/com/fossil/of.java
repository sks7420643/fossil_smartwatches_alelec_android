package com.fossil;

import com.fossil.lf;
import com.fossil.pf;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class of<Key, Value> extends jf<Key, Value> {
    @DexIgnore
    public /* final */ Object mKeyLock; // = new Object();
    @DexIgnore
    public Key mNextKey; // = null;
    @DexIgnore
    public Key mPreviousKey; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Key, Value> extends a<Key, Value> {
        @DexIgnore
        public /* final */ lf.d<Value> a;
        @DexIgnore
        public /* final */ of<Key, Value> b;

        @DexIgnore
        public b(of<Key, Value> ofVar, int i, Executor executor, pf.a<Value> aVar) {
            this.a = new lf.d<>(ofVar, i, executor, aVar);
            this.b = ofVar;
        }

        @DexIgnore
        @Override // com.fossil.of.a
        public void a(List<Value> list, Key key) {
            if (!this.a.a()) {
                if (this.a.a == 1) {
                    this.b.setNextKey(key);
                } else {
                    this.b.setPreviousKey(key);
                }
                this.a.a(new pf<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key, Key key2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Key, Value> extends c<Key, Value> {
        @DexIgnore
        public /* final */ lf.d<Value> a;
        @DexIgnore
        public /* final */ of<Key, Value> b;

        @DexIgnore
        public d(of<Key, Value> ofVar, boolean z, pf.a<Value> aVar) {
            this.a = new lf.d<>(ofVar, 0, null, aVar);
            this.b = ofVar;
        }

        @DexIgnore
        @Override // com.fossil.of.c
        public void a(List<Value> list, Key key, Key key2) {
            if (!this.a.a()) {
                this.b.initKeys(key, key2);
                this.a.a(new pf<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Key> {
        @DexIgnore
        public e(int i, boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<Key> {
        @DexIgnore
        public /* final */ Key a;

        @DexIgnore
        public f(Key key, int i) {
            this.a = key;
        }
    }

    @DexIgnore
    private Key getNextKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mNextKey;
        }
        return key;
    }

    @DexIgnore
    private Key getPreviousKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mPreviousKey;
        }
        return key;
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
        Key nextKey = getNextKey();
        if (nextKey != null) {
            loadAfter(new f<>(nextKey, i2), new b(this, 1, executor, aVar));
        } else {
            aVar.a(1, pf.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pf.a<Value> aVar) {
        Key previousKey = getPreviousKey();
        if (previousKey != null) {
            loadBefore(new f<>(previousKey, i2), new b(this, 2, executor, aVar));
        } else {
            aVar.a(2, pf.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, pf.a<Value> aVar) {
        d dVar = new d(this, z, aVar);
        loadInitial(new e(i, z), dVar);
        dVar.a.a(executor);
    }

    @DexIgnore
    @Override // com.fossil.jf
    public final Key getKey(int i, Value value) {
        return null;
    }

    @DexIgnore
    public void initKeys(Key key, Key key2) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
            this.mNextKey = key2;
        }
    }

    @DexIgnore
    public abstract void loadAfter(f<Key> fVar, a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadBefore(f<Key> fVar, a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadInitial(e<Key> eVar, c<Key, Value> cVar);

    @DexIgnore
    public void setNextKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mNextKey = key;
        }
    }

    @DexIgnore
    public void setPreviousKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
        }
    }

    @DexIgnore
    @Override // com.fossil.jf
    public boolean supportsPageDropping() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public final <ToValue> of<Key, ToValue> map(t3<Value, ToValue> t3Var) {
        return mapByPage((t3) lf.createListFunction(t3Var));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public final <ToValue> of<Key, ToValue> mapByPage(t3<List<Value>, List<ToValue>> t3Var) {
        return new yf(this, t3Var);
    }
}
