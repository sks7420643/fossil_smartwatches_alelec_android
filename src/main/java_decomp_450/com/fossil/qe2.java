package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qe2 {
    @DexIgnore
    public static qe2 a;

    @DexIgnore
    public static synchronized qe2 a() {
        qe2 qe2;
        synchronized (qe2.class) {
            if (a == null) {
                a = new me2();
            }
            qe2 = a;
        }
        return qe2;
    }

    @DexIgnore
    public abstract re2<Boolean> a(String str, boolean z);
}
