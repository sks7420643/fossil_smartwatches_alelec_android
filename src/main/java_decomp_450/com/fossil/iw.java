package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.fossil.k30;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iw implements ComponentCallbacks2, q30 {
    @DexIgnore
    public static /* final */ r40 r; // = ((r40) r40.b(Bitmap.class).I());
    @DexIgnore
    public /* final */ aw a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ p30 c;
    @DexIgnore
    public /* final */ v30 d;
    @DexIgnore
    public /* final */ u30 e;
    @DexIgnore
    public /* final */ x30 f;
    @DexIgnore
    public /* final */ Runnable g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ k30 i;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<q40<Object>> j;
    @DexIgnore
    public r40 p;
    @DexIgnore
    public boolean q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            iw iwVar = iw.this;
            iwVar.c.a(iwVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements k30.a {
        @DexIgnore
        public /* final */ v30 a;

        @DexIgnore
        public b(v30 v30) {
            this.a = v30;
        }

        @DexIgnore
        @Override // com.fossil.k30.a
        public void a(boolean z) {
            if (z) {
                synchronized (iw.this) {
                    this.a.d();
                }
            }
        }
    }

    /*
    static {
        r40 r40 = (r40) r40.b(t20.class).I();
        r40 r402 = (r40) ((r40) r40.b(iy.b).a(ew.LOW)).a(true);
    }
    */

    @DexIgnore
    public iw(aw awVar, p30 p30, u30 u30, Context context) {
        this(awVar, p30, u30, new v30(), awVar.d(), context);
    }

    @DexIgnore
    public synchronized void a(r40 r40) {
        this.p = (r40) ((r40) r40.clone()).d();
    }

    @DexIgnore
    public hw<Bitmap> b() {
        return a(Bitmap.class).a((k40<?>) r);
    }

    @DexIgnore
    public hw<Drawable> c() {
        return a(Drawable.class);
    }

    @DexIgnore
    public List<q40<Object>> d() {
        return this.j;
    }

    @DexIgnore
    public synchronized r40 e() {
        return this.p;
    }

    @DexIgnore
    public synchronized void f() {
        this.d.b();
    }

    @DexIgnore
    public synchronized void g() {
        f();
        for (iw iwVar : this.e.a()) {
            iwVar.f();
        }
    }

    @DexIgnore
    public synchronized void h() {
        this.d.c();
    }

    @DexIgnore
    public synchronized void i() {
        this.d.e();
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public synchronized void onDestroy() {
        this.f.onDestroy();
        for (c50<?> c50 : this.f.c()) {
            a(c50);
        }
        this.f.b();
        this.d.a();
        this.c.b(this);
        this.c.b(this.i);
        this.h.removeCallbacks(this.g);
        this.a.b(this);
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public synchronized void onStart() {
        i();
        this.f.onStart();
    }

    @DexIgnore
    @Override // com.fossil.q30
    public synchronized void onStop() {
        h();
        this.f.onStop();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.q) {
            g();
        }
    }

    @DexIgnore
    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.d + ", treeNode=" + this.e + "}";
    }

    @DexIgnore
    public synchronized boolean b(c50<?> c50) {
        n40 a2 = c50.a();
        if (a2 == null) {
            return true;
        }
        if (!this.d.a(a2)) {
            return false;
        }
        this.f.b(c50);
        c50.a((n40) null);
        return true;
    }

    @DexIgnore
    public final void c(c50<?> c50) {
        boolean b2 = b(c50);
        n40 a2 = c50.a();
        if (!b2 && !this.a.a(c50) && a2 != null) {
            c50.a((n40) null);
            a2.clear();
        }
    }

    @DexIgnore
    public hw<Drawable> a(String str) {
        return c().a(str);
    }

    @DexIgnore
    public iw(aw awVar, p30 p30, u30 u30, v30 v30, l30 l30, Context context) {
        this.f = new x30();
        this.g = new a();
        this.h = new Handler(Looper.getMainLooper());
        this.a = awVar;
        this.c = p30;
        this.e = u30;
        this.d = v30;
        this.b = context;
        this.i = l30.a(context.getApplicationContext(), new b(v30));
        if (v50.c()) {
            this.h.post(this.g);
        } else {
            p30.a(this);
        }
        p30.a(this.i);
        this.j = new CopyOnWriteArrayList<>(awVar.f().b());
        a(awVar.f().c());
        awVar.a(this);
    }

    @DexIgnore
    public hw<Drawable> a(Uri uri) {
        return c().a(uri);
    }

    @DexIgnore
    public hw<Drawable> a(Integer num) {
        return c().a(num);
    }

    @DexIgnore
    public hw<Drawable> a(Object obj) {
        return c().a(obj);
    }

    @DexIgnore
    public <ResourceType> hw<ResourceType> a(Class<ResourceType> cls) {
        return new hw<>(this.a, this, cls, this.b);
    }

    @DexIgnore
    public void a(c50<?> c50) {
        if (c50 != null) {
            c(c50);
        }
    }

    @DexIgnore
    public synchronized void a(c50<?> c50, n40 n40) {
        this.f.a(c50);
        this.d.b(n40);
    }

    @DexIgnore
    public <T> jw<?, T> b(Class<T> cls) {
        return this.a.f().a(cls);
    }
}
