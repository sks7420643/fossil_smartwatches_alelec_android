package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface gb7 extends ib7.b {
    @DexIgnore
    public static final b m = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends ib7.b> E a(gb7 gb7, ib7.c<E> cVar) {
            ee7.b(cVar, "key");
            if (cVar instanceof db7) {
                db7 db7 = (db7) cVar;
                if (!db7.a(gb7.getKey())) {
                    return null;
                }
                E e = (E) db7.a(gb7);
                if (!(e instanceof ib7.b)) {
                    return null;
                }
                return e;
            } else if (gb7.m != cVar) {
                return null;
            } else {
                if (gb7 != null) {
                    return gb7;
                }
                throw new x87("null cannot be cast to non-null type E");
            }
        }

        @DexIgnore
        public static ib7 b(gb7 gb7, ib7.c<?> cVar) {
            ee7.b(cVar, "key");
            if (!(cVar instanceof db7)) {
                return gb7.m == cVar ? jb7.INSTANCE : gb7;
            }
            db7 db7 = (db7) cVar;
            return (!db7.a(gb7.getKey()) || db7.a(gb7) == null) ? gb7 : jb7.INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ib7.c<gb7> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();
    }

    @DexIgnore
    void a(fb7<?> fb7);

    @DexIgnore
    <T> fb7<T> b(fb7<? super T> fb7);
}
