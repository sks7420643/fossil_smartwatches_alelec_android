package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw6 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ rw6 b; // = new rw6();

    /*
    static {
        String simpleName = rw6.class.getSimpleName();
        ee7.a((Object) simpleName, "BackendURLUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return a(PortfolioApp.g0.c(), i);
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i) {
        ee7.b(portfolioApp, "app");
        int m = portfolioApp.v().m();
        boolean z = false;
        if (m == 0 ? !mh7.b("release", "release", true) : m == 1) {
            z = true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        if (i != 5) {
                            if (i != 6) {
                                return "";
                            }
                            if (z) {
                                return a(portfolioApp, ol4.A.x());
                            }
                            return a(portfolioApp, ol4.A.w());
                        } else if (z) {
                            return ol4.A.c();
                        } else {
                            return ol4.A.b();
                        }
                    } else if (z) {
                        return ol4.A.g();
                    } else {
                        return ol4.A.f();
                    }
                } else if (z) {
                    return ol4.A.r();
                } else {
                    return ol4.A.q();
                }
            } else if (z) {
                return ol4.A.p();
            } else {
                return ol4.A.o();
            }
        } else if (z) {
            return ol4.A.l();
        } else {
            return ol4.A.i();
        }
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, String str) {
        String g = portfolioApp.g();
        Locale locale = Locale.getDefault();
        ee7.a((Object) locale, "Locale.getDefault()");
        String language = locale.getLanguage();
        if (ee7.a((Object) language, (Object) "zh")) {
            we7 we7 = we7.a;
            Locale locale2 = Locale.getDefault();
            ee7.a((Object) locale2, "Locale.getDefault()");
            language = String.format("%s_%s", Arrays.copyOf(new Object[]{language, locale2.getCountry()}, 2));
            ee7.a((Object) language, "java.lang.String.format(format, *args)");
        }
        we7 we72 = we7.a;
        String format = String.format(str + "/%s?locale=%s", Arrays.copyOf(new Object[]{g, language}, 2));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i, String str) {
        ee7.b(portfolioApp, "app");
        ee7.b(str, "version");
        int m = portfolioApp.v().m();
        boolean z = false;
        if (m == 0 ? !mh7.b("release", "release", true) : m == 1) {
            z = true;
        }
        if (i != 0) {
            return a(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return ol4.A.m();
                }
            } else if (str.equals("1")) {
                return ol4.A.l();
            }
            return ol4.A.n();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return ol4.A.j();
            }
        } else if (str.equals("1")) {
            return ol4.A.i();
        }
        return ol4.A.k();
    }
}
