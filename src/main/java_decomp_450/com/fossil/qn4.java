package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.Arrays;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String[] i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public Date p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qn4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public qn4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new qn4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public qn4[] newArray(int i) {
            return new qn4[i];
        }
    }

    @DexIgnore
    public qn4() {
        this(null, null, null, null, null, 0, 0, null, null, false, null, 2047, null);
    }

    @DexIgnore
    public qn4(String str, String str2, String str3, String str4, String str5, int i2, int i3, String str6, String[] strArr, boolean z, Date date) {
        ee7.b(str2, "name");
        ee7.b(str4, "type");
        ee7.b(str5, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        ee7.b(str6, SampleRaw.COLUMN_START_TIME);
        ee7.b(strArr, "ids");
        ee7.b(date, "startTrackingDate");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = i2;
        this.g = i3;
        this.h = str6;
        this.i = strArr;
        this.j = z;
        this.p = date;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "<set-?>");
        this.b = str;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final void d(String str) {
        ee7.b(str, "<set-?>");
        this.h = str;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qn4)) {
            return false;
        }
        qn4 qn4 = (qn4) obj;
        return ee7.a(this.a, qn4.a) && ee7.a(this.b, qn4.b) && ee7.a(this.c, qn4.c) && ee7.a(this.d, qn4.d) && ee7.a(this.e, qn4.e) && this.f == qn4.f && this.g == qn4.g && ee7.a(this.h, qn4.h) && ee7.a(this.i, qn4.i) && this.j == qn4.j && ee7.a(this.p, qn4.p);
    }

    @DexIgnore
    public final String f() {
        return this.e;
    }

    @DexIgnore
    public final String g() {
        return this.h;
    }

    @DexIgnore
    public final Date h() {
        return this.p;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (((((hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31) + this.f) * 31) + this.g) * 31;
        String str6 = this.h;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String[] strArr = this.i;
        int hashCode7 = (hashCode6 + (strArr != null ? Arrays.hashCode(strArr) : 0)) * 31;
        boolean z = this.j;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode7 + i3) * 31;
        Date date = this.p;
        if (date != null) {
            i2 = date.hashCode();
        }
        return i5 + i2;
    }

    @DexIgnore
    public final int i() {
        return this.f;
    }

    @DexIgnore
    public final String j() {
        return this.d;
    }

    @DexIgnore
    public final boolean m() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeDraft(id=" + this.a + ", name=" + this.b + ", des=" + this.c + ", type=" + this.d + ", privacy=" + this.e + ", target=" + this.f + ", duration=" + this.g + ", startTime=" + this.h + ", ids=" + Arrays.toString(this.i) + ", isInviteAll=" + this.j + ", startTrackingDate=" + this.p + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeStringArray(this.i);
        parcel.writeValue(Boolean.valueOf(this.j));
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ qn4(java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, int r18, int r19, java.lang.String r20, java.lang.String[] r21, boolean r22, java.util.Date r23, int r24, com.fossil.zd7 r25) {
        /*
            r12 = this;
            r0 = r24
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = r2
            goto L_0x000a
        L_0x0009:
            r1 = r13
        L_0x000a:
            r3 = r0 & 2
            java.lang.String r4 = ""
            if (r3 == 0) goto L_0x0012
            r3 = r4
            goto L_0x0013
        L_0x0012:
            r3 = r14
        L_0x0013:
            r5 = r0 & 4
            if (r5 == 0) goto L_0x0018
            goto L_0x0019
        L_0x0018:
            r2 = r15
        L_0x0019:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x0020
            java.lang.String r5 = "activity_best_result"
            goto L_0x0022
        L_0x0020:
            r5 = r16
        L_0x0022:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0029
            java.lang.String r6 = "public_with_friend"
            goto L_0x002b
        L_0x0029:
            r6 = r17
        L_0x002b:
            r7 = r0 & 32
            r8 = 0
            if (r7 == 0) goto L_0x0032
            r7 = 0
            goto L_0x0034
        L_0x0032:
            r7 = r18
        L_0x0034:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x003a
            r9 = 0
            goto L_0x003c
        L_0x003a:
            r9 = r19
        L_0x003c:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x0041
            goto L_0x0043
        L_0x0041:
            r4 = r20
        L_0x0043:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x004a
            java.lang.String[] r10 = new java.lang.String[r8]
            goto L_0x004c
        L_0x004a:
            r10 = r21
        L_0x004c:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0051
            goto L_0x0053
        L_0x0051:
            r8 = r22
        L_0x0053:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x005d
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            goto L_0x005f
        L_0x005d:
            r0 = r23
        L_0x005f:
            r13 = r12
            r14 = r1
            r15 = r3
            r16 = r2
            r17 = r5
            r18 = r6
            r19 = r7
            r20 = r9
            r21 = r4
            r22 = r10
            r23 = r8
            r24 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn4.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String[], boolean, java.util.Date, int, com.fossil.zd7):void");
    }

    @DexIgnore
    public final void a(String str) {
        this.c = str;
    }

    @DexIgnore
    public final void b(int i2) {
        this.f = i2;
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "<set-?>");
        this.e = str;
    }

    @DexIgnore
    public final String[] d() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public qn4(android.os.Parcel r17) {
        /*
            r16 = this;
            r0 = r17
            java.lang.String r1 = "parcel"
            com.fossil.ee7.b(r0, r1)
            java.lang.String r3 = r17.readString()
            java.lang.String r4 = r17.readString()
            r1 = 0
            if (r4 == 0) goto L_0x007d
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.ee7.a(r4, r2)
            java.lang.String r5 = r17.readString()
            java.lang.String r6 = r17.readString()
            if (r6 == 0) goto L_0x0079
            com.fossil.ee7.a(r6, r2)
            java.lang.String r7 = r17.readString()
            if (r7 == 0) goto L_0x0075
            com.fossil.ee7.a(r7, r2)
            int r8 = r17.readInt()
            int r9 = r17.readInt()
            java.lang.String r10 = r17.readString()
            if (r10 == 0) goto L_0x0071
            com.fossil.ee7.a(r10, r2)
            java.lang.String[] r11 = r17.createStringArray()
            if (r11 == 0) goto L_0x006d
            java.lang.String r1 = "parcel.createStringArray()!!"
            com.fossil.ee7.a(r11, r1)
            java.lang.Class r1 = java.lang.Boolean.TYPE
            java.lang.ClassLoader r1 = r1.getClassLoader()
            java.lang.Object r0 = r0.readValue(r1)
            if (r0 == 0) goto L_0x0065
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r12 = r0.booleanValue()
            r13 = 0
            r14 = 1024(0x400, float:1.435E-42)
            r15 = 0
            r2 = r16
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        L_0x0065:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Boolean"
            r0.<init>(r1)
            throw r0
        L_0x006d:
            com.fossil.ee7.a()
            throw r1
        L_0x0071:
            com.fossil.ee7.a()
            throw r1
        L_0x0075:
            com.fossil.ee7.a()
            throw r1
        L_0x0079:
            com.fossil.ee7.a()
            throw r1
        L_0x007d:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public final void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final int b() {
        return this.g;
    }

    @DexIgnore
    public final void a(String[] strArr) {
        ee7.b(strArr, "<set-?>");
        this.i = strArr;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.j = z;
    }
}
