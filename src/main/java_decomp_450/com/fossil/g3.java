package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.fossil.c7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ TypedArray b;
    @DexIgnore
    public TypedValue c;

    @DexIgnore
    public g3(Context context, TypedArray typedArray) {
        this.a = context;
        this.b = typedArray;
    }

    @DexIgnore
    public static g3 a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new g3(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    @DexIgnore
    public Drawable b(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return this.b.getDrawable(i);
        }
        return t0.c(this.a, resourceId);
    }

    @DexIgnore
    public Drawable c(int i) {
        int resourceId;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return h2.b().a(this.a, resourceId, true);
    }

    @DexIgnore
    public String d(int i) {
        return this.b.getString(i);
    }

    @DexIgnore
    public CharSequence e(int i) {
        return this.b.getText(i);
    }

    @DexIgnore
    public int f(int i, int i2) {
        return this.b.getLayoutDimension(i, i2);
    }

    @DexIgnore
    public int g(int i, int i2) {
        return this.b.getResourceId(i, i2);
    }

    @DexIgnore
    public static g3 a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new g3(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    @DexIgnore
    public int d(int i, int i2) {
        return this.b.getInt(i, i2);
    }

    @DexIgnore
    public int e(int i, int i2) {
        return this.b.getInteger(i, i2);
    }

    @DexIgnore
    public CharSequence[] f(int i) {
        return this.b.getTextArray(i);
    }

    @DexIgnore
    public boolean g(int i) {
        return this.b.hasValue(i);
    }

    @DexIgnore
    public static g3 a(Context context, int i, int[] iArr) {
        return new g3(context, context.obtainStyledAttributes(i, iArr));
    }

    @DexIgnore
    public int c(int i, int i2) {
        return this.b.getDimensionPixelSize(i, i2);
    }

    @DexIgnore
    public TypedArray a() {
        return this.b;
    }

    @DexIgnore
    public float b(int i, float f) {
        return this.b.getFloat(i, f);
    }

    @DexIgnore
    public Typeface a(int i, int i2, c7.a aVar) {
        int resourceId = this.b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        return c7.a(this.a, resourceId, this.c, i2, aVar);
    }

    @DexIgnore
    public int b(int i, int i2) {
        return this.b.getDimensionPixelOffset(i, i2);
    }

    @DexIgnore
    public void b() {
        this.b.recycle();
    }

    @DexIgnore
    public boolean a(int i, boolean z) {
        return this.b.getBoolean(i, z);
    }

    @DexIgnore
    public int a(int i, int i2) {
        return this.b.getColor(i, i2);
    }

    @DexIgnore
    public ColorStateList a(int i) {
        int resourceId;
        ColorStateList b2;
        if (!this.b.hasValue(i) || (resourceId = this.b.getResourceId(i, 0)) == 0 || (b2 = t0.b(this.a, resourceId)) == null) {
            return this.b.getColorStateList(i);
        }
        return b2;
    }

    @DexIgnore
    public float a(int i, float f) {
        return this.b.getDimension(i, f);
    }
}
