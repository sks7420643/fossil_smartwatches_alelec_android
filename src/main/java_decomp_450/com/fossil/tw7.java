package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import com.fossil.fx7;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tw7 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<byte[], i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ nx7 $resultHandler;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(nx7 nx7, Bitmap bitmap) {
            super(1);
            this.$resultHandler = nx7;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(byte[] bArr) {
            invoke(bArr);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(byte[] bArr) {
            this.$resultHandler.a(bArr);
            Bitmap bitmap = this.$bitmap;
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public tw7(Context context) {
        ee7.b(context, "context");
        this.b = context;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public final boolean b() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @DexIgnore
    public final fx7 c() {
        if (this.a || Build.VERSION.SDK_INT < 29) {
            return ex7.f;
        }
        return cx7.e;
    }

    @DexIgnore
    public final List<bx7> a(int i, long j, boolean z, ax7 ax7) {
        ee7.b(ax7, "option");
        List<bx7> a2 = c().a(this.b, i, j, ax7);
        if (!z) {
            return a2;
        }
        int i2 = 0;
        for (bx7 bx7 : a2) {
            i2 += bx7.b();
        }
        return ea7.b((Collection) v97.a(new bx7("isAll", "Recent", i2, i, true)), (Iterable) a2);
    }

    @DexIgnore
    public final List<zw7> b(String str, int i, int i2, int i3, long j, ax7 ax7) {
        String str2 = str;
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        if (ee7.a((Object) str, (Object) "isAll")) {
            str2 = "";
        }
        return c().a(this.b, str2, i2, i3, i, j, ax7);
    }

    @DexIgnore
    public final List<zw7> a(String str, int i, int i2, int i3, long j, ax7 ax7) {
        String str2 = str;
        ee7.b(str, "galleryId");
        ee7.b(ax7, "option");
        if (ee7.a((Object) str, (Object) "isAll")) {
            str2 = "";
        }
        return fx7.b.a(c(), this.b, str2, i, i2, i3, j, ax7, null, 128, null);
    }

    @DexIgnore
    public final void a(String str, int i, int i2, int i3, nx7 nx7) {
        ee7.b(str, "id");
        ee7.b(nx7, "resultHandler");
        try {
            if (!b()) {
                zw7 b2 = c().b(this.b, str);
                if (b2 == null) {
                    nx7.a(nx7, "The asset not found!", null, null, 6, null);
                } else {
                    lx7.a.a(this.b, b2.i(), i, i2, i3, nx7.a());
                }
            } else {
                zw7 b3 = c().b(this.b, str);
                Bitmap a2 = c().a(this.b, str, i, i2, b3 != null ? Integer.valueOf(b3.j()) : null);
                lx7.a.a(this.b, a2, i, i2, i3, new b(nx7, a2));
            }
        } catch (Exception e) {
            Log.e("PhotoManagerPlugin", "get thumb error", e);
            nx7.a("201", "get thumb error", e);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z, boolean z2, nx7 nx7) {
        ee7.b(str, "id");
        ee7.b(nx7, "resultHandler");
        zw7 b2 = c().b(this.b, str);
        if (b2 == null) {
            nx7.a(nx7, "The asset not found", null, null, 6, null);
        } else if (!fx7.a.e()) {
            nx7.a(pc7.a(new File(b2.i())));
        } else {
            byte[] a2 = c().a(this.b, b2, z2);
            nx7.a(a2);
            if (z) {
                c().a(this.b, b2, a2);
            }
        }
    }

    @DexIgnore
    public final void a() {
        c().k();
    }

    @DexIgnore
    public final bx7 a(String str, int i, long j, ax7 ax7) {
        ee7.b(str, "id");
        ee7.b(ax7, "option");
        if (!ee7.a((Object) str, (Object) "isAll")) {
            return c().a(this.b, str, i, j, ax7);
        }
        List<bx7> a2 = c().a(this.b, i, j, ax7);
        if (a2.isEmpty()) {
            return null;
        }
        int i2 = 0;
        for (bx7 bx7 : a2) {
            i2 += bx7.b();
        }
        return new bx7("isAll", "Recent", i2, i, true);
    }

    @DexIgnore
    public final void a(String str, boolean z, nx7 nx7) {
        ee7.b(str, "id");
        ee7.b(nx7, "resultHandler");
        nx7.a(c().a(this.b, str, z));
    }

    @DexIgnore
    public final List<String> a(List<String> list) {
        ee7.b(list, "ids");
        return c().a(this.b, list);
    }

    @DexIgnore
    public final zw7 a(byte[] bArr, String str, String str2) {
        ee7.b(bArr, "image");
        ee7.b(str, "title");
        ee7.b(str2, "description");
        return c().a(this.b, bArr, str, str2);
    }

    @DexIgnore
    public final zw7 a(String str, String str2, String str3) {
        ee7.b(str, "path");
        ee7.b(str2, "title");
        ee7.b(str3, Constants.DESC);
        if (!new File(str).exists()) {
            return null;
        }
        return c().a(this.b, new FileInputStream(str), str2, str3);
    }

    @DexIgnore
    public final void a(String str, nx7 nx7) {
        ee7.b(str, "id");
        ee7.b(nx7, "resultHandler");
        nx7.a(Boolean.valueOf(c().a(this.b, str)));
    }

    @DexIgnore
    public final Map<String, Double> a(String str) {
        ee7.b(str, "id");
        ub c = c().c(this.b, str);
        double[] b2 = c != null ? c.b() : null;
        if (b2 == null) {
            return oa7.c(w87.a(Constants.LAT, Double.valueOf(0.0d)), w87.a("lng", Double.valueOf(0.0d)));
        }
        return oa7.c(w87.a(Constants.LAT, Double.valueOf(b2[0])), w87.a("lng", Double.valueOf(b2[1])));
    }
}
