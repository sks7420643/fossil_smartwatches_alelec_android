package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sh1 extends qj1 {
    @DexIgnore
    public UUID[] A; // = new UUID[0];
    @DexIgnore
    public qk1[] B; // = new qk1[0];
    @DexIgnore
    public long C; // = ButtonService.CONNECT_TIMEOUT;

    @DexIgnore
    public sh1(ri1 ri1) {
        super(qa1.d, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.v81, com.fossil.sj1
    public void c(eo0 eo0) {
        ay0 ay0;
        JSONObject jSONObject;
        ((v81) this).v = sz0.a(((v81) this).v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        wr1 wr1 = ((v81) this).f;
        if (wr1 != null) {
            wr1.i = true;
        }
        wr1 wr12 = ((v81) this).f;
        if (!(wr12 == null || (jSONObject = wr12.m) == null)) {
            yz0.a(jSONObject, r51.j, yz0.a(ay0.a));
        }
        if (((v81) this).v.c == ay0.a) {
            a(eo0);
        }
        sz0 sz0 = ((v81) this).v;
        if (!(this.A.length == 0)) {
            ay0 = ay0.a;
        } else {
            ay0 = ay0.r;
        }
        a(sz0.a(sz0, null, null, ay0, null, null, 27));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.m1, yz0.a(((v81) this).y.getBondState())), r51.e5, Integer.valueOf(((v81) this).y.x.getType()));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(yz0.a(yz0.a(super.h(), r51.m1, yz0.a(((v81) this).y.getBondState())), r51.e5, Integer.valueOf(((v81) this).y.x.getType())), r51.e1, yz0.a(this.A)), r51.f1, yz0.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new b41(((v81) this).y.w);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(eo0 eo0) {
        b41 b41 = (b41) eo0;
        this.A = b41.j;
        this.B = b41.k;
        ((v81) this).g.add(new zq0(0, null, null, yz0.a(yz0.a(new JSONObject(), r51.e1, yz0.a(this.A)), r51.f1, yz0.a(this.B)), 7));
    }
}
